Unit ReCreate;

{- this unit does the work for the program RCF }

INTERFACE

Uses
	Globals,
	CopyMove,
	DelDir,
	WildCard,
	CRC,
	TpCrt,
	Dos,
	TpDate,
	TpMemChk,
	TpString;

Procedure BuildList(src, trg: String);
Procedure AbortError(msg: String);
Procedure CreateActionFile;
Procedure FlushActionBuffer;
Procedure PerformOps;

Type
	actions = (	NoAction,		{- take no action }
					DeleteAFile,		{- delete file }
					DeleteADir,		{- delete directory }
					CopyNewFile,	{- doesnt exist on source, copy file }
					CopyAFile,		{- file changed, copy file }
					CopyADir			{- copy directory }
				);

Const
	actionBuffSize = 20;
	actionStrs	: Array[actions] Of String[40] =
						(	'No Action',
							'Delete File',
							'Delete Directory',
							'Copy File (doesnt exist on target)',
							'Copy File (file changed)',
							'Copy Directory');

Type
	actionRec = Record
		action	: actions;
		fname		: String[12];		{- name of file/directory}
		fpath		: String[80];		{- path of source }
		tpath		: String[80];		{- source path }
	End;

	actionFileType	= File of ActionRec;
	actionListBuf	= Array[1..ActionBuffSize] Of ActionRec;

Var
	actionList	: actionListBuf;
	actionFile	: actionFileType;
	stats			: 	Record
							dFiles,
							dDirs,
							cFiles,
							cDirs		: LongInt;
						End;


IMPLEMENTATION

Const
	NoMem		= 'Not enough memory.';

Type
	filePtr = ^FileRec;
	fileRec = Record
		name	: String[12];
		action: actions;
		time	: LongInt;
		attr	: Byte;
		path	: ^String;
		next	: FilePtr;
	End;

Var
	DirRec		: SearchRec;
	fstr			: String[80];
	tstr			: String[80];
	numActions	: Integer;

Procedure AbortError(msg: String);
Begin
	writeln;
	writeln('ERROR: ', msg);
	writeln('Program aborted.');
	Halt(1);
End;

Procedure CreateActionFile;
{- create action file on origen directory }
Var
	path	: String;
Begin
	path:= JustPathName(ParamStr(0));
	Assign(actionFile, AddBackSlash(path) + 'ACTIONS.RCF');
	ReWrite(actionFile);
	If IoResult <> 0 Then
		AbortError('Cannot open action file.');
	numActions:= 0;
End;

Procedure FlushActionBuffer;
Var
	k	: Integer;
Begin
	Seek(actionFile, FileSize(actionFile));
	For k:= 1 To numActions Do Begin
		write(actionFile, actionList[k]);
	End;
	numactions:= 0;
End;

Procedure AddAction(act: actions; frec: FilePtr; src, trg: String);
Begin
	If numActions = ActionBuffSize Then
		FlushActionBuffer;
	Inc(numActions);
	With actionList[numActions] Do Begin
		action:= act;
		fname:= frec^.name;
		fpath:= src;
		tpath:= trg;
	End;
	Case act Of
		DeleteAFile	: Inc(stats.dFiles);
		DeleteADir	: Inc(stats.dDirs);
		CopyAFile, CopyNewFile	: Inc(stats.cFiles);
		CopyADir		: Inc(stats.cDirs);
	End;
End;

Procedure AddItemToList(Var head, last: filePtr; Var item: SearchRec;
								fpath: String);
{- add an item to the list }
Var
	aux	: FilePtr;
	temp	: FilePtr;
Begin
	If not GetMemCheck(aux, SizeOf(filerec)) Then
		AbortError(noMem);
	If not GetMemCheck(aux^.path, Length(fpath)+1) Then
		AbortError(noMem);

	With aux^ Do Begin
		name:= item.name;
		time:= item.time;
		action:= NoAction;
		If Boolean(item.attr and Directory) Then		{- if dir bit set }
			attr:= Directory
		Else
			attr:= item.attr;
		path^:= fpath;
		next:= Nil;
	End;
	If head = Nil Then Begin
		head:= aux;
		last:= head;
	End
	Else Begin
		last^.next:= aux;
		last:= aux;
	End;
End;

Function FindName(Var list: FilePtr; nameToFind: String;
							attr: Byte): Boolean;
Var
	temp	: FilePtr;
	found	: Boolean;
Begin
	temp:= list;
	found:= False;
	While (temp <> Nil) and not found Do Begin
		If (attr = Directory) and (temp^.attr = Directory) Then Begin
			found:= temp^.name = nameToFind;
		End
		Else
			If (temp^.attr <> Directory) Then Begin
				found:= temp^.name = nameTofind
		End;
		If found Then
			list:= temp;
		temp:= temp^.next;
	End;
	FindName:= found;
End;

Procedure PurgeList(Var list: FilePtr; NotDirs: Boolean);
{- remove list from memory }
Var
	temp		: FilePtr;
	newlist,
	newtail	: FilePtr;
Begin
	newlist:= Nil;
	If list <> Nil Then Begin
		While list <> Nil Do Begin
			temp:= list;
			list:= list^.next;
			If NotDirs and (temp^.attr = Directory) Then Begin
				If newlist = Nil Then Begin
					newlist:= temp;
					newtail:= temp;
				End
				Else Begin
					newtail^.next:= temp;
					newtail:= temp;
				End;
			End
			Else Begin
				FreeMemCheck(temp^.path, Length(temp^.path^)+1);
				FreeMemCheck(temp, SizeOf(FileRec));
			End;
		End;
		If newlist <> Nil Then Begin
			newTail^.next:= Nil;
			list:= newlist;
		End;
	End;
End;

Function FileChanged(src, trg: LongInt): Boolean;
{- return true if src is newer than trg }
Var
	srcTime,
	trgTime		: DateTime;
	d1, d2		: Date;
Begin
	UnPackTime(src, srcTime);
	UnPackTime(trg, trgTime);
	d1:= DMYtoDate(srcTime.day, srctime.month, srcTime.year);
	d2:= DMYtoDate(trgTime.day, trgtime.month, trgTime.year);
	If d1 > d2 Then
		FileChanged:= True
	Else
		If d1 = d2 Then
			Filechanged:= HMStoTime(srcTime.hour, srctime.min, srcTime.sec) >
							  HMStoTime(trgTime.hour, trgtime.min, trgTime.sec)
		Else
			FileChanged:= False;
End;

Procedure BuildList(src, trg: String);
{- build a list of actions to perform }
Var
	flist, tlist	: filePtr;
	fflist, ttlist	: FilePtr;
	last				: FilePtr;
Begin
	write(stErr, Pad(InsertCommas(Long2Str(MemAvail)), 7), #8#8#8#8#8#8#8);
	flist:= Nil;
	tlist:= Nil;
	last:= Nil;

	fstr:= src + '*.*';
	tstr:= trg + '*.*';

	FindFirst(fstr, AnyFile, dirrec);     	{- get list of source files/dirs }
	While DosError = 0 Do Begin
		If (dirrec.name[1] <> '.') and ((dirrec.attr and 8) <> 8) Then
			AddItemToList(flist, last, dirrec, src);
		FindNext(dirrec);
	End;

	FindFirst(tstr, AnyFile, dirrec);     	{- get list of target files/dirs }
	While DosError = 0 Do Begin
		If (dirrec.name[1] <> '.') and ((dirrec.attr and 8) <> 8) Then
			AddItemToList(tlist, last, dirrec, trg);
		FindNext(dirrec);
	End;

	fflist:= flist;
	ttlist:= tlist;

   {- compare target to source}
	If not doNotDelete Then Begin
		{- no need to check the target if Delete is disallowed by user }
		While tlist <> Nil Do Begin
			flist:= fflist;
			If tlist^.attr = Directory Then Begin
				If not FindName(flist, tlist^.name, Directory) Then Begin
					AddAction(DeleteADir, tlist, trg, '**');
				End;
			End
			Else Begin
				If not FindName(flist, tlist^.name, AnyFile) Then
					If IgnoreAttr or
						(not IgnoreAttr and ((tList^.attr And 7) = 0)) Then
							AddAction(DeleteAFile, tlist, trg, '**');
			End;
			tlist:= tlist^.next;
		End;
		flist:= fflist;
		tlist:= ttlist;
   End;

	fflist:= flist;
	ttlist:= tlist;

	{- compare source to target }
	While flist <> Nil Do Begin
		tlist:= ttlist;
		If flist^.attr = Directory Then Begin
			If not FindName(tlist, flist^.name, Directory) Then Begin
				{- must create a directory }
				AddAction(CopyADir, flist, src, trg);
				flist^.action:= CopyADir;	{- flag so no recursion occurs on this }
													{  directory }
			End;
		End
		Else Begin
			If not FindName(tlist, flist^.name, AnyFile) Then
				AddAction(CopyNewFile, flist, src, trg)
			Else
				If FileChanged(flist^.time, tlist^.time) Then
					AddAction(CopyAFile, flist, src, trg);
		End;
		flist:= flist^.next;
	End;

	flist:= fflist;

	PurgeList(ttlist, False);
	PurgeList(flist, True);
	fflist:= flist;
	{- recursive section }
	While (flist <> Nil) and (flist^.attr = Directory) Do Begin
		If flist^.action = NoAction Then Begin	{- recurse through dir }
			BuildList(	AddBackSlash(src+flist^.name),
							AddBackSlash(trg+flist^.name));
		End;
		flist:= flist^.next;
	End;
	PurgeList(fflist, False);
End;

Procedure PerformCopy(src, trg: String);
{- perform copy and verify if needed }
Var
	result	: Integer;
	crcSrc, crcTrg	: Word;
Begin
	If (numIncludes > 0) and not MatchWildCards(includes, numIncludes, src) Then
		Exit;
	If MatchWildCards(excludes, numExcludes, src) Then
		Exit;
	If not QuietMode or Redirected Then Begin
		writeln('Copying file - ');
		writeln('  Source: ', src);
		writeln('  Target: ', trg);
	End;
   result:= CopyFile(src, trg);
	If result <> 0 Then
		writeln('ERROR (',result,') while copying file.')
	Else Begin
		If verify Then Begin
			write('Verifying...');
			If CRCComputed(src, CRCsrc) Then Begin
				If CRCComputed(trg, CRCtrg) Then Begin
					If CRCsrc <> CRCtrg Then
						writeln('ERROR: Checksum mismatch')
					Else
						writeln('OK');
				End
				Else
					writeln('ERROR: Cannot calcuate CRC on target file');
			End
			Else
				writeln('ERROR: Cannot calcuate CRC on source file.');
		End;
	End;
	If not quietMode or Redirected Then
		writeln;
End;

Procedure CopyEntireDirectory(src, trg: String);
{- copy an entire directory }
Var
	flist		: filePtr;
	fflist	: FilePtr;
	last		: FilePtr;
Begin
	If Length(trg) > 3 Then Begin
		MkDir(trg);
		If IoResult <> 0 Then Begin
		End;
	End;

	trg:= AddBackSlash(trg);
	src:= AddBackSlash(src);

	flist:= Nil;
	last:= Nil;

	fstr:= src + '*.*';
	tstr:= trg + '*.*';

	FindFirst(fstr, AnyFile, dirrec);     	{- get list of source files/dirs }
	While DosError = 0 Do Begin
		If (dirrec.name[1] <> '.') and ((dirrec.attr and 8) <> 8) Then
			AddItemToList(flist, last, dirrec, src);
		FindNext(dirrec);
	End;

	fflist:= flist;
	{- recursive section }
	While (flist <> Nil) Do Begin
		If flist^.attr = Directory Then Begin	{- recurse through dir }
			CopyEntireDirectory(src+flist^.name, trg+flist^.name)
		End
		Else
			PerformCopy(src+flist^.name, trg+flist^.name);
		flist:= flist^.next;
	End;

	PurgeList(fflist, False);
End;

Procedure PerformOps;
{- }
Var
	act	: ActionRec;
Begin
	Reset(actionFile);
	While not Eof(actionFile) Do Begin
		Read(actionFile, act);
		With act Do
			Case action Of
				CopyNewFile,
				CopyAFile	:	PerformCopy(fpath+fname, tpath+fname);
				CopyADir		:	CopyEntireDirectory(fpath+fname, tpath+fname);
				DeleteADir	:	DeleteDirectory(AddBackSlash(fpath+fname), igNoreAttr);
				DeleteAFile	:	KillFile(fpath+fname, igNoreAttr);
			End;
	End;
End;

Begin
	FillChar(stats, SizeOf(Stats), #0);
END.
