Unit CRC;

Interface

Uses DOS;

Function CRCComputed(FileName : PathStr; Var CRC : Word) : Boolean;

Implementation

{** uses BLOCKCRC.ASM **}
{$L BlockCRC}

Const
	BufSize	= 10000;

Type
	BufType	= Array[1..BufSize] Of Char;

Function BlockCRC(Old: Integer; p: BufType; l: Word): Integer; External;

Function CRCComputed(FileName : PathStr; Var CRC : Word) : Boolean;
{****************************************************************************}
{* Tries to compute a CRC for the specified file and returns whether the    *}
{* operation was successful or not. If successful, the computed value is    *}
{* returned in the parameter CRC. The specifed filename can include a path. *}
{* NOTE: The CRC returned is adjusted to represent a file which is padded   *}
{* to a sector multiple (128) with Control Z's (for CPM compatibility).     *}
{****************************************************************************}

Var
	f				: File;
	OldAttr		: Word;
	Buffer		: BufType;
	NumRead		: Word;

Begin
	CRCComputed:= False;
	Assign(f, FileName);
	{** save old file attribute so it can be reset **}
	GetFAttr(f, OldAttr);
	If (DosError <> 0) Then
		Exit;
	{** strip out file attributes **}
	SetFAttr(f, 0);
	If (DosError <> 0) Then
		Exit;
	{$I-}
	{** open file **}
	Reset(f, 1);
	{$I+}
	If (IOResult <> 0) Then Begin
		{** reset old file attribute before exiting **}
		SetFAttr(f, OldAttr);
		Exit;
	End;
	{** reset old file attribute **}
	SetFAttr(f, OldAttr);
	If (DosError <> 0) Then Begin
		Close(f);
		Exit;
	End;
	crc:= 0;
	Repeat
		{$I-}
		{** get next block to compute **}
		BlockRead(f, Buffer, BufSize, NumRead);
		{$I+}
		If (IOResult <> 0) Then Begin
			Close(f);
			Exit;
		End
		Else Begin
			{** pad last block to a sector multiple (for CPM compatibility) **}
			If (NumRead < BufSize) And ((NumRead And 127) <> 0) Then
			Repeat
				Inc(NumRead);
				Buffer[NumRead]:= Chr(26);
			Until ((NumRead And 127) = 0);
			{** compute CRC for this block **}
			CRC:= BlockCRC(CRC, Buffer, NumRead);
		End;
	Until (NumRead < BufSize);
	Close(f);
	CRCComputed:= True;
End;

End.
