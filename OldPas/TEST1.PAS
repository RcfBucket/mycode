uses
  Objects,
  Strings,
  WinTypes,
  WinProcs,
  ODialogs,
  OWindows;

Const
  maxLines   = 88;
  maxWidth   = 300;

  id_HScroll   = 1000;
  id_VScroll   = 1001;

type
  PScrBuf = ^TScrBuf;
  TScrBuf = Array[0..maxLines*maxWidth] of Char;

  PLineObj = ^TLineObj;
  TLineObj = Object(TObject)
    buf : PChar;
    constructor init(ALine: PChar);
  end;


  PScrObj = ^TScrObj;
  TScrObj = object(TObject)
    buf   : PCollection;
    x, y  : Integer;
    lines : Integer;
    constructor Init;
    destructor Done; virtual;
    function GetLine(aBuf: PChar; strSize, lineNum: Integer): Integer;
    function XPos: Integer;
    function YPos: Integer;
    function SetPos(col, row: Integer): Boolean;
    function Numlines: Integer;
  end;

  TScrollApp = object(TApplication)
    procedure InitMainWindow; virtual;
  end;

  PScrollWindow = ^TScrollWindow;
  TScrollWindow = object(TWindow)
    cxChar, cyChar, cxCaps : Integer;
    hscroll, vscroll : PScrollBar;
    chHScroll, cwVScroll : Integer;

    constructor Init(ATitle: PChar);
    procedure SetupWindow; virtual;
    procedure Paint(PaintDC: HDC; var paintInfo: TPaintStruct); virtual;
    procedure wmChar(var msg: TMessage); virtual wm_First + wm_Char;
    procedure wmSize(var msg: TMessage); virtual wm_First + wm_Size;
  end;


var
  buf: PScrObj;


constructor TLineObj.init(ALine: PChar);
begin
  inherited init;
  GetMem(buf, MaxWidth + 1);
  If ALine <> nil then
  begin
    StrLCopy(buf, ALine, maxWidth);
    if Strlen(ALine) > maxWidth then
      buf[maxWidth]:= #0
    else
      buf[Strlen(ALine)]:= #0;
  end
  else
    StrCopy(buf, '');
end;

constructor TScrObj.init;
begin
  inherited Init;
  buf:= New(PCollection, Init(maxLines, 20));
  SetPos(0, 0);
end;

destructor TScrObj.Done;
begin
  Dispose(buf, Done);
  inherited Done;
end;

function TScrObj.XPos: Integer;
begin
  XPos:= x;
end;

function TScrObj.YPos: Integer;
begin
  YPos:= x;
end;

function TScrObj.SetPos(col, row: Integer): Boolean;
begin
  x:= col;
  y:= row;
end;

function TScrObj.GetLine(aBuf: PChar; strSize, lineNum: Integer): Integer;
begin
  if (lineNum <= lines) and (lineNum > 0) then
  begin

  end;
end;

function TScrObj.Numlines: Integer;
begin
  Numlines:= buf^.count;
end;

procedure TScrollApp.InitMainWindow;
begin
  mainWindow:= New(PScrollWindow, Init('Test'));
end;

constructor TScrollWindow.Init(ATitle: PChar);
var
  dc  : HDC;
  tm  : TTextMetric;
  k   : Integer;
  tstr: String;
  r     : TRect;
begin
  inherited init(Nil, ATitle);
  attr.style:= attr.style or WS_VSCROLL or ws_HSCROLL;

(*  chHScroll:= GetSystemMetrics(SM_CYHScroll);
  cwVScroll:= GetSystemMetrics(SM_CXVScroll);

  hScroll:= New(PScrollBar, init(@self, id_HScroll, 0, 0, 0, 0, true));
  vScroll:= New(PScrollBar, init(@self, id_HScroll, 0, 0, 0, 0, False));
*)
  dc:= GetDC(HWindow);
  GetTextMetrics(dc, tm);
  ReleaseDC(HWindow, dc);
  with tm do
  begin
    cxChar:= tmAveCharWidth;
    cyChar:= tmHeight + tmExternalLeading;
    if (tmPitchAndFamily and 1) <> 0 then
      cxCaps:= 3
    else
      cxCaps:= 2;
    cxCaps:= cxCaps * cxChar Div 2;
  end;
end;

procedure TScrollWindow.setupWindow;
begin
  inherited Setupwindow;
end;

procedure TScrollWindow.wmSize(Var msg: TMessage);
var
  r : TRect;
begin
  inherited wmSize(msg);
(*  GetClientRect(HWindow, r);
  MoveWindow(hScroll^.HWindow, r.left, r.bottom - chHScroll, r.right, chHScroll, True);
  MoveWindow(vScroll^.HWindow, r.right-cwVScroll, r.top, cwVScroll, r.bottom-chHScroll, True);
*)
end;

procedure TScrollWindow.wmChar;
var
  pstr  : Array[0..255] of char;
  r     : TRect;
  k     : Integer;
  dc    : HDC;
begin
(*  with msg do begin
    Move(wParam, pstr[0], sizeof(wparam));
    pstr[SizeOF(wparam)]:= #0;
    StrCat(buf, pstr);
    GetClientRect(HWindow, r);
    InvalidateRect(HWindow, @r, False);
  end;
*)
end;

procedure TScrollWindow.Paint(paintDC: HDC; var paintInfo: TPaintStruct);
var
  rect : TRect;
begin
{  GetClientRect(HWindow, rect);
  MoveTo(paintDC, rect.left, cyChar);
  LineTo(paintDC, rect.right, cyChar);}
(*  DrawText(paintdc, buf, StrLen(buf), paintInfo.rcPaint, DT_LEFT or DT_WORDBREAK);*)
end;

var
  scrollApp : TScrollApp;

begin
  buf:= New(PScrObj, init);
  scrollApp.Init('ScrollApp');
  scrollApp.Run;
  scrollApp.Done;
  Dispose(buf, Done);
end.