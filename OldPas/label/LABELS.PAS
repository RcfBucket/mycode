Unit Labels;
{- Label definition stuff}

INTERFACE

Uses
	Globals,
	TpCrt,
	TpPick,
	TpString,
	TpMenu,
	TpEntry;

Procedure AddALabel(Var mnu: Menu);
Procedure EditALabel(Var mnu: Menu);
Procedure LoadALabel;
Procedure DeleteALabel;
Procedure CopyALabel;
Procedure CheckBeforeExit;

IMPLEMENTATION

Type
	LabelPickList =	Record
								name: LabelName;
								rec : LongInt;
							End;

Var
	lrec	: LabelRec;
	plist	: Array[1..maxLabels] Of LabelPickList;

Procedure BuildLabelList;
Var
	count	: Word;
	index	: Word;
Begin
	FillChar(plist, SizeOf(plist), #0);
	Seek(LabelFile, 1);
	count:= 1;
	index:= 1;
	While not Eof(LabelFile) Do Begin
		Read(LabelFile, lrec);
		If not lrec.deleted Then Begin
			With plist[index] Do Begin
				name:= lrec.name;
				rec:= count;
			End;
			Inc(index);
		End;
		Inc(count);
	End;
End;

{$F+}
Function LabelChoice(I : Word): String;
{- Return a label name given an index}
Begin
	LabelChoice:= ' '+plist[i].name;
End;
{$F-}

Function NumLabels: Word;
Begin
	Seek(LabelFile, 0);
	Read(LabelFile, lrec);
	NumLabels:= lrec.num;
End;

Function SelectLabel: LongInt;
{- select a label from file and return it's record number}
Var
	choice: Word;
	b		: Boolean;
	len, x: Byte;
Begin
	If NumLabels > 0 Then Begin
		BuildLabelList;
		Choice := 1;
		PickMatrix := 1;
		PickSrch:= StringPickSrch;
		len:= SizeOf(LabelName)-1;
		x:= (ScreenWidth Div 2) - (len Div 2) - 2;
		B:= PickWindow(@LabelChoice, NumLabels,
							x,7, x+len+3,19, True, PickColors,
						' Defined Labels ', Choice);
		If PickCmdNum = PKSSelect Then Begin	{- label selected}
			SelectLabel:= plist[choice].rec;
		End
		Else
			SelectLabel:= 0;
	End
	Else Begin
		ErrorMsg('No labels have been defined');
		SelectLabel:= 0;
	End;
End;

Function NextLabelAvail(Var appending: Boolean): LongInt;
{- return next label record number available}
Begin
	Reset(LabelFile);
	Read(LabelFile, lrec);
	If lrec.next = 0 Then Begin		{- no records deleted, append to file}
		NextLabelAvail:= FileSize(LabelFile);
		appending:= True;
	End
	Else Begin
		appending:= False;
		NextLabelAvail:= lrec.next;
	End;
End;

Procedure IncLabels;
Begin
	Reset(LabelFile);
	Read(LabelFile, lrec);
	Inc(lrec.num);
	Reset(LabelFile);
	Write(LabelFile, lrec);
End;

Procedure SaveLabelRec(Var buf: LabelRec; index: LongInt);
{- Save label record to file}
Begin
	Seek(LabelFile, index);
	{$I-}
	Write(LabelFile, buf);
	{$I+}
	If IoResult <> 0 Then
		ErrorMsg('Unable to write to label file...['+labelFname+']');
End;

Function OpenLabelFile: Boolean;
Begin
	Assign(LabelFile, labelFname);
	{$I-}
	Reset(LabelFile);
	{$I+}
	If IoResult <> 0 Then Begin
		OpenLabelFile:= False;
		ErrorMsg('Unable to access label file...['+labelFname+']');
	End
	Else
		OpenLabelFile:= True;
End;

Procedure CheckBeforeExit;
Begin
	If OpenLabelFile Then Begin
		If labelloaded Then Begin
			SaveLabelRec(labl, labelindex);		{- save if any changes made}
		End;
		Close(labelfile);
	End;
End;

Function TrimSpace(s: String): String;
Var
	k	: Integer;
Begin
	k:= Length(s);
	While (s[k] = ' ') and (k > 0) Do
		Dec(k);
	TrimSpace:= Copy(s, 1, k);
End;

Function EditLabelRec(Var rec: LabelRec): Boolean;
{- return true if [F10]-Save was opted, False is [Esc] pressed}
Var
	esr	: ESrecord;
	escmd	: EStype;
	dumbstr: String;
	tnum	: Byte;
	k		: Byte;
	tchars: FrameArray;
Begin
	If AddEntryCommand(ESUser1, 1, $2000, 0) Then	{- ALTD};
	If AddEntryCommand(ESUser2, 1, $1700, 0) Then	{- ALTI};

	InitESrecord(esr);
	FrameWindow(1,1, ScreenWidth,ScreenHeight-1, WinFrameAttr, WinHeaderAttr,
					' Label Definition ');
	Str(NumLabels, dumbstr);
	FastWrite('Number of labels: '+dumbstr, 3,57, ProtectAttr);
	FastWrite('[^L]-Lbl Num, [^T]-Tot Lbls, [^D]-Date, [^C]-Center, [^R]-Right, [F10]-Save.',
					ScreenHeight-2,3, ProtectAttr);

	With rec.setrec Do Begin
		tnum:= (ScreenWidth Div 2) - (width Div 2);
		tchars:= FrameChars;
		SetFrameChars('�','�','�','�','�','�');
		FrameChars:= tchars;
		FrameWindow(tnum-1,5, tnum+(width),
						6+(height),WinFrameAttr,WinHeaderAttr, ' Label ');
	End;

	With rec Do Begin
		SetWrapMode(esr, WrapAtEdges);
		SetAutoAdvance(off);
		SetCursorToEnd(off);
		SetTrimBlanks(on);
		SetClearFirstChar(off);
		AddStringField(esr, 'Label name:', 3,3, '', 3,15, SizeOf(name)-1,
							0, Nil,name);

		SetTrimBlanks(off);
		For k:= 1 To setrec.height Do
			AddStringField(esr, '', 5+k,tnum-1, CharStr('X', MaxWidth),
								5+k,tnum, setrec.width, 0, Nil, rec.labelbuf[k]);

		tnum:= 0;
		Repeat
			escmd:= EditScreen(esr, tnum, False);
			Case EScmd OF
				ESDone:
					If name = '' Then Begin
						ErrorMsg('Label name is required to save.');
						escmd:= 0;
						tnum:= 0;
					End
					Else
						For k:= 1 To Maxlines Do
							labelbuf[k]:= TrimSpace(labelbuf[k]);
				ESUser1:
					Begin
						tnum:= esr.currentid;
						If (tnum >= 1) and (tnum <= Maxlines) Then Begin
							For k:= tnum To maxlines-1 Do
								labelbuf[k]:= labelbuf[k+1];
							labelbuf[Maxlines]:= '';
							DrawEditScreen(esr);
						End;
					End;
				ESUser2:
					Begin
						tnum:= esr.currentid;
						If (tnum >= 1) and (tnum <= Maxlines) Then Begin
							For k:= maxlines DownTo tnum+1 Do
								labelbuf[k]:= labelbuf[k-1];
							labelbuf[tnum]:= '';
							DrawEditScreen(esr);
						End;
					End;
			End;
		Until escmd In [ESquit, ESDone];
		ClrScr;
		DisposeEditScreen(esr);
	End;
	EditLabelRec:= (escmd = ESDone);
	If AddEntryCommand(ESNone, 1, $2000, 0) Then	{- ALTD};
	If AddEntryCommand(ESNone, 1, $1700, 0) Then	{- ALTI};

End;

Procedure AddALabel(Var mnu: Menu);
Var
	buf		: LabelRec;
	appending: Boolean;
	index		: LongInt;
	nextindex: LongInt;
Begin
	CheckBeforeExit;
	If OpenLabelFile Then Begin
		If NumLabels < MaxLabels Then Begin
			index:= NextLabelAvail(appending);
			If not appending Then Begin
				Seek(LabelFile, index);
				Read(LabelFile, lrec);
				nextindex:= lrec.next;
			End;
			EraseMenu(mnu, False);
			FillChar(buf, SizeOf(buf), #0);
			ClearSetup(buf.setrec);
			buf.deleted:= False;
			If EditLabelRec(buf) Then Begin
				SaveLabelRec(buf, index);
				If not appending Then Begin
					Reset(LabelFile);
					Read(LabelFile, lrec);
					lrec.next:= nextindex;
					Reset(LabelFile);
					Write(LabelFile, lrec);
				End;
				IncLabels;
				Inc(labelsdefined);
				If not LabelLoaded Then Begin
					labelloaded:= True;
					labl:= buf;
					menuredraw:= True;
					labelindex:= index;
				End;
			End;
		End
		Else
			ErrorMsg('Maximum number of labels have been added.');
		Close(LabelFile);
	End;
End;

Procedure EditALabel(Var mnu: Menu);
Var
	buf		: LabelRec;
Begin
	If labelloaded Then Begin
		If OpenLabelFile Then Begin
			EraseMenu(mnu, False);
			buf:= labl;
			If EditLabelRec(buf) Then Begin
				SaveLabelRec(buf, labelindex);
				labelchanged:= True;
				labl:= buf;
			End;
			menuredraw:= True;
			Close(labelfile);
		End;
	End
	Else
		ErrorMsg('No label has been loaded');
End;

Procedure LoadALabel;
Var
	tnum	: LongInt;
Begin
	CheckBeforeExit;
	If OpenLabelFile Then Begin
		tnum:= SelectLabel;
		If tnum <> 0 Then Begin
			Seek(labelfile, tnum);
			Read(labelfile, labl);
			labelindex:= tnum;
			labelloaded:= True;
			menuredraw:= true;
		End;
		Close(labelfile);
	End;
End;

Procedure DeleteALabel;
Var
	header	: LabelRec;
	buf		: LabelRec;
	index		: LongInt;
Begin
	If OpenLabelFile Then Begin
		index:= SelectLabel;
		If index <> 0 Then Begin
			Reset(labelfile);
			Read(labelfile, header);

			Seek(labelfile, index);
			Read(labelfile, buf);

			buf.deleted:= True;
			buf.next:= header.next;

			header.next:= index;
			Dec(header.num);

			Seek(labelfile, index);
			Write(labelfile, buf);

			Reset(labelfile);
			Write(labelfile, header);

			If index = labelindex Then Begin
				FillChar(labl, Sizeof(labl), #0);
				labelloaded:= False;
			End;
			MenuRedraw:= True;
		End;
		Close(labelFile);
		Dec(labelsdefined);
	End;
End;

Procedure CopyALabel;
Var
	copyfrom	: LabelRec;
	buf		: LabelRec;
	index		: LongInt;
	nextindex: LongInt;
	appending: Boolean;
Begin
	If OpenLabelFile Then Begin
		If NumLabels < MaxLabels Then Begin
			index:= SelectLabel;
			If index <> 0 Then Begin
				Seek(LabelFile, index);
				Read(LabelFile, copyfrom);		{- get source}

				index:= NextLabelAvail(appending);

				If not appending Then Begin
					Seek(LabelFile, index);
					Read(LabelFile, buf);
					nextindex:= buf.next;
				End;

				SaveLabelRec(copyfrom, index);

				If not appending Then Begin
					Reset(LabelFile);
					Read(LabelFile, buf);

					buf.next:= nextindex;

					Reset(LabelFile);
					Write(LabelFile, buf);
				End;

				IncLabels;
				Inc(labelsdefined);
			End;
		End
		Else
			ErrorMsg('Maximum number of labels have been added.');
		Close(LabelFile);
	End;
End;


END.