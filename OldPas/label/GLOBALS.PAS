Unit Globals;

INTERFACE

Uses
	TpCrt,
	TpWindow,
	TpMenu,
	TPPick,
	TpDate,
	TpInline;

Type
	IdString		= String[16];
	PrinterName = String[40];
	LabelName	= String[40];

Const
	Maxlines		= 16;						{- maximum number of lines in label }
	MaxWidth		= 60;						{- maximum width of label }
	MaxPrinters	= 20;						{- maximum number of printers}
	MaxCopies	= 99;						{- maximum number of copies to print}
	SafeBuf		= 100;					{- safety buffer for memo buffer }
	setFname		= 'LABEL.SET';			{- default settings file name }
	labelFname	= 'LABEL.DAT';			{- label data file}
	PrintFname	= 'LABEL.PRN';			{- printer def file}
	Version		= 'Version 3.1';		{- version number}
	{  3.0   - Initial version
		3.1	- changed print routine to allow user to ESC out of printing
	}
	maxlabels	= 100;					{- maximum number of labels allowed}

	PromptAttr	= LightCyan;
	StringAttr	= Cyan Shl 4 or Black;
	FieldAttr	= Cyan;
	WinBackAttr	= Cyan;
	WinFrameAttr= LightBlue;
	WinHeaderAttr = Green;
	ProtectAttr	= White;

	AllHotKeys	: Set of Char = [^Q,^L,^G,^E,^S];
	IDstr			: IdString = ' LABELER  1.000 ';	{- program id string}

Type
	Fonts			= (Cpi10, Cpi12, Cpi17);
	Lines			= (Lpi6, Lpi8);
	PrintStr		=	String[40];
	FontArray	= Array[Fonts] Of PrintStr;
	LineArray	= Array[Lines] Of PrintStr;

	PrintRec	=	Record
						name		: PrinterName;
						deleted	: Boolean;
						num		: Word;
						Next		: LongInt;			{- next in file}
						recnum	: LongInt;			{- used for comparisons}
						cpistrs	: FontArray;
						lpistrs	: LineArray;
					End;

	SetupRec	=	Record
						id			: IdString;
						copies	: Byte;				{- number of copies to print}
						margin	: Byte;				{- left margin }
						lspace	: Byte;
						cpi		: Fonts;				{- currently selected font}
						lpi		: Lines;				{- currently selected lpi}
						width		: Byte;
						height	: Byte;
						datemask	: DateString;
						printer	: PrintRec;
					End;

	MemoType		= Array[1..MaxLines*MaxWidth + SafeBuf] Of Char;
	LabelType	= Array[1..MaxLines] Of String[80];

	LabelRec =	Record
						name		: LabelName;		{- label descr }
						deleted	: Boolean;			{- deleted flag}
						Next		: LongInt;			{- flag used to keep track of
																deleted records }
						num		: Word;				{- number of labels }
						labelbuf	: LabelType;
						setrec	: SetupRec;			{- label setup}
					End;

	{- record zero of LABEL file will contain the beginning of the link to
		deleted records, the program id and number of labels defined
		Record 0 Format:
					Next	- Next deleted record in deleted chain, 0 if none deleted
					num	- Number of active labels in file.
					name	- Program id. MUST EXIST!}

Const
	FontStrs	: Array[Fonts] of String[10] = (' 10 cpi', ' 12 cpi', ' 17 cpi');
	LPIStrs	: Array[Lines] of String[10] = (' 6 lpi', ' 8 lpi');

Var
	main					: Menu;			{- menu structure}
	LabelLoaded			: Boolean;		{- True if label loaded or initialized}
	labelchanged		: Boolean;		{- True if current label changed }
	LabelIndex			: LongInt; 		{- label index for current label}
	nextAvail			: LongInt;		{- keeps track of deleted records}
	labelsdefined		: Word;			{- number of labels defined}
	labl					: LabelRec;		{- label buffer}
	LabelFile 			: File of LabelRec;
	PrinterFile			: File of PrintRec;
	MenuRedraw			: Boolean;
	covers				: Pointer;
	PickColors			: PickColorArray;	{- colors for TPPICK}
	TodaysDate			: Date;
	todaysmonth			: String[3];

Procedure ErrorMsg(msg: String);
Procedure ClearSetup(Var rec: SetupRec);

IMPLEMENTATION

Procedure AbortProg(msg: String);
{- abort program after displaying message}
Begin
	TextAttr:= LightGray;
	Clrscr;
	NormalCursor;
	TextAttr:= Yellow;
	writeln(msg);
	writeln('Program aborting!'#7);
	TextAttr:= LightGray;
	writeln(^M^J);
	Halt(1);
End;

Procedure ClearSetup(Var rec: SetupRec);
{- clear global setup record}
Begin
	FillChar(rec, SizeOf(rec), #0);	{- clear record}
	With rec Do Begin
		copies:= 1;
		margin:= 0;
		width:= 40;
		height:= 9;
		lspace:= 3;
		id:= IDstr;
		datemask:= 'dd/ttt/yy';
	End;
End;

Procedure Init;
{- initialize program and files if nessessary}
Var
	ch	: Char;
	flx	: FlexAttrs;
	prec	: PrintRec;
Begin
	flx[0]:= LightGray;		{- assign flex write params}
	flx[1]:= Yellow;

	{- Initialize printer file}

	{$I-}
	Assign(PrinterFile, printFname);
	Reset(PrinterFile);
	{$I+}
	If IoResult = 0 Then Begin 		{- file exists}
		{$I-}
		Read(PrinterFile, prec);
		{$I+}
		If (prec.name <> IDstr) or (IoResult <> 0) Then Begin
			writeln;
			writeln('Invalid PRINTER file...['+printFname+']'#7);
			FlexWrite('P)urge file and continue or A)bort....(P,A): [A]',
							WhereY, 1, flx);
			ch:= ' ';
			Gotoxy(47,WhereY);
			TextAttr:= White;
			write(ch,#8);
			Repeat
				ch:= UpCase(ReadKey);
				If Ord(ch) > 31 Then
					write(ch,#8);
			Until ch In [#27, 'A', 'P'];
			If ch <> 'P' Then
				AbortProg('');
			FillChar(prec, SizeOf(prec), #0);
			prec.name:= IDstr;
			prec.next:= 0;
			Rewrite(PrinterFile);
			Write(PrinterFile, prec);
		End;
	End
	Else Begin
		FillChar(prec, SizeOf(prec), #0);
		prec.name:= IDStr;
		prec.deleted:= False;
		prec.num:= 0;
		Rewrite(PrinterFile);
		write(PrinterFile, prec);
	End;
	Close(PrinterFile);

	{- Initialize data file }

	{$I-}
	Assign(LabelFile, labelFname);
	Reset(LabelFile);
	{$I+}
	If IoResult = 0 Then Begin 		{- file exists}
		{$I-}
		Read(LabelFile, labl);		{- get header record [0] }
		{$I+}
		If (labl.name <> IDstr) or (IoResult <> 0) Then
			AbortProg('Invalid data file format...'+labelFname);
	End
	Else Begin
		FillChar(labl, SizeOf(labl), #0);	{- clear record}
		With labl Do Begin
			next:= 0;								{- flag none deleted}
			num:= 0;									{- no labels yet}
			name:= IDstr;							{- flag header ID}
			ClearSetup(setrec);
		End;
		Rewrite(labelfile);
		Write(LabelFile, labl);					{- save header record }
	End;
	Close(labelFile);

	nextavail		:= labl.next;			{- init global vars}
	labelsdefined	:= labl.num;
End;

Procedure ErrorMsg(msg: String);
{- display an error message}
Const
	press	: String[40] = '[Press Any Key]';
Var
	win	: WindowPtr;
	x		: Byte;
	len	: Byte;
	xy, scanlines: Word;
	ch		: Char;
Begin
	GetCursorState(xy, scanlines);
	HiddenCursor;
	len:= MaxWord(Length(msg), Length(press));
	x:= (ScreenWidth Div 2) - (Length(msg) Div 2) - 3;
	If not MakeWindow(win, x,10, x+Length(msg)+6,15, True,True,False,
							Red Shl 4 or Black, Red Shl 4 or White,
							Red Shl 4 or Yellow, ' Error ')
		Then
			AbortProg('Not enough memory.');

	If DisplayWindow(win) Then ;
	FastCenter(msg, 2, Red shl 4 or Yellow);
	FastCenter(press, 4, Red shl 4);
	If SaveWindow(1,ScreenHeight, ScreenWidth,ScreenHeight, False, covers) Then ;
	FastFill(ScreenWidth, ' ', ScreenHeight, 1, PromptAttr);
	ch:= ReadKey;
	RestoreWindow(1,ScreenHeight, ScreenWidth,ScreenHeight, False, covers);
	KillWindow(win);
	RestoreCursorState(xy, scanlines);
End;

Procedure SetupDate;
Var
	d,m,y	: Integer;
Begin
	TodaysDate:= Today;
	DateToDMY(todaysdate, d,m,y);
	todaysmonth:= MonthString[m];
End;

Begin
	labelloaded		:= False;
	labelchanged	:= False;
	menuredraw		:= False;
	GetMem(covers, ScreenWidth*2);		{- allocate line buffer}

	Init;
	SetupDate;

	SetHorizontalPick;
	PickColors[WindowAttr]:= LightGray Shl 4 or Blue;
	PickColors[FrameAttr]:= LightGray Shl 4 or White;
	PickColors[HeaderAttr]:= LightGray Shl 4 or Yellow;
	PickColors[SelectAttr]:= Blue Shl 4 or LightGray;

	SoundFlagW:= False;
	Explode:= False;
END.

