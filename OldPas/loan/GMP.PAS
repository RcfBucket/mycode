Program GMP(Input,Output);

Var
	Amount,Interest,Payment,Balance,Total,IntThisMonth,Extra: Real;
	Month, Year, PmtNum: Integer;

Begin
	Amount:= 85427;
	Interest:= 10.75;
	Payment:= 608.91;
	Extra:= 0;
	Month:= 6;
	Year:= 1990;

	Interest:= Interest / 100;
	Balance:= Amount;
	PmtNum:= 0;
	Total:= 0;
	Repeat
		PmtNum:= PmtNum + 1;
		IntThisMonth:= Round(Interest/12 * Balance * 100) / 100;
		Balance:= Balance + IntThisMonth;
		If (Balance < Payment+Extra) Then
			Payment:= Balance - Extra;
		Balance:= Balance - Payment - Extra;
		Total:= Total + Payment + Extra;
		Write('#',PmtNum:3);
		If (Month < 10) Then
			Write('   0',Month:1,'/01/',Year:4)
		Else
			Write('   ',Month:2,'/01/',Year:4);
		Write('   Interest=$',IntThisMonth:7:2);
		Write('   Payment=$',Payment+Extra:7:2);
		Write('   Balance=$',Balance:9:2);
		Writeln;
		If (PmtNum Mod 12 = 0) And (PmtNum <= 60) Then
			Payment:= Round(Payment * 1.075 * 100) / 100;
		Month:= Month + 1;
		If (Month = 13) Then Begin
			Month:= 1;
			Year:= Year + 1;
		End;
	Until Balance = 0;
	Writeln('Total of payments is $',Total:10:2);
End.
