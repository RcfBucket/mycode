Unit WildCard;

{- wildcard support }

INTERFACE

Uses
	Dos;

Function IsMatch(mask, fpath: String): Boolean;

IMPLEMENTATION

Function IsMatch(mask, fpath: String): Boolean;
{- return true if fname matched wild card
	fpath must be a valid file name/path }
Var
	w, f	: Byte;
	wName,
	fname	: NameStr;
	wExt,
	ext	: ExtStr;
	match	: Boolean;

	Function MatchName(wild, name: NameStr): Boolean;
	Var
		matches	: Boolean;
	Begin
		If wild = name Then Begin
			MatchName:= True;
			Exit;
		End;
		If Length(wild) > Length(name) Then Begin
			FillChar(name[Length(name)+1], Length(wild)-Length(name), ' ');
			Inc(Byte(name[0]), Length(wild)-Length(name));
		End;

		f:= 1;
		w:= 1;
		matches:= wild <> '';		{- assume a match }
		While matches and (f <= Length(name)) Do Begin		{- check filename }
			If wild[w] = '*' Then 	{- rest of name matches }
				f:= Length(name)+1
			Else Begin
				If (wild[w] <> '?') and (name[f] <> wild[w]) Then
					matches:= False
				Else Begin
					Inc(w); Inc(f);
					If ((w > Length(wild)) and (f < Length(name))) Then
						matches:= False;
				End;
			End;
		End;
		MatchName:= matches;
	End;

Begin
	fSplit(fpath, fpath, fname, ext);
	fSplit(mask, fpath, wName, wExt);
	If ext <> '' Then
		Delete(ext, 1, 1);		{- remove '.' from extension }
	If wExt <> '' Then
		Delete(wExt, 1, 1);		{- remove '.' from wild card extension }
	If (wExt = '') and (Pos('.', mask) = 0) Then
		wExt:= '*';

	match:= MatchName(wName, fname);
	If match Then
		match:= MatchName(wExt, ext);
	IsMatch:= match;
End;

END.