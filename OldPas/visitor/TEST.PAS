Uses
	TpDate;

Procedure CheckDate(Var tstr: String);
var
	tstr2	: String[80];
	k		: Integer;
Begin
	K:= Pos('-', tstr);
	tstr2:= '00-00-00';
	If k = 2 Then
		tstr2[2]:= tstr[1]
	Else If k = 3 Then
		Move(tstr[1], tstr2[1], 2);
	Delete(tstr, 1,k);
	k:= Pos('-', tstr);
	If k = 2 Then
		tstr2[5]:= tstr[1]
	Else If k = 3 Then
		Move(tstr[1], tstr2[4], 2);
	Delete(tstr, 1, k);
	tstr[0]:= #2;
	Move(tstr[1], tstr2[7], 2);
	tstr:= tstr2;
End;

Var
	tstr	: String;
	d		: Date;
Begin
	writeln; writeln; writeln;
	tstr:= '12-4-91';
	CheckDate(tstr);
	d:= DateStringToDate('mm-dd-yy', tstr);
	writeln(DateToDateString('mm-dd-yy', d));
End.