{$R-,S-,I-,X+,F+}

uses
	strs,
	wildcard,
	tpdate,
	dos,
	visitor;

Const
	version			= '1.02';
	{- 1.00			Initial version 										}
	{- 1.01			Added date /D parameter								}
	{- 1.02			fixed size printing error                    }

Var
	masks				: Array[1..10] of String[12];
	numMasks	: Integer;
	path		: String;
	tDate		: Date;

Function PrnFile(Var src, trg: String; Var fRec: vFileRec): vRetType;
Var
	tstr	: String[30];
	t		: DateTime;
	d		: Date;
Begin
	With fRec Do Begin
		UnpackTime(time, t);
		d:= DMYToDate(t.day, t.month, t.year);
		If d >= tDate Then Begin
			tstr:= DMYtoDateString('yy/mm/dd', t.day, t.month, t.year);

			write(Pad(JustFileName(fname), 9), Pad(JustFileExtent(fname),6), tstr, ' ');
			tstr:= 	TimeToTimeString('hh:mm:ss',
							HMSToTime(t.hour, t.min, t.sec)
						);
			write(' ',tstr, '  ');
			write(LeftPad(InsCommas(Long2Str(frec.size)), 12), '  ');
			write(src);
			writeln;
		End;
	End;
	PrnFile:= vContinue;
End;

Function GoodFile(Var fname: String): Boolean;
Var
	k		: Integer;
	found	: Boolean;
Begin
	k:= 1;
	found:= False;
	While not found and (k <= NumMasks) Do Begin
		found:= IsMatch(masks[k], fname);
		Inc(k);
	End;
	GoodFile:= found;
End;

Function CntDirs(Var src, trg: String; fileFound: LongInt): vRetType;
Begin
	CntDirs:= vContinue;
End;

Procedure Title;
Begin
	writeln;
	writeln('FDIR     Version ', version);
	writeln;
End;

Procedure Usage(msg: String);
Begin
	Title;
	writeln('Usage : FDIR path  [options]');
	writeln;
	writeln('Options:   /Dmm-dd-yy    Display only files with date or greater');
	writeln;
	If msg <> '' Then
		writeln(msg);
	Halt;
End;

Procedure PurgeToNext(Var st: String; quoted: Boolean);
{- purge until next delimiter, assumed st = '/X....'. This routine will
	purge to end of line or next delimiter}
Var
	k	: Integer;
Begin
	k:= 3;
	While (k <= Length(st)) and ((not quoted and (st[k] <> '/')) or
		 (quoted and (st[k] <> '"'))) Do
		Inc(k);
	Delete(st, 1, k-1);
End;

Procedure CheckDate(Var tstr: String);
var
	tstr2	: String[80];
	k		: Integer;
Begin
	K:= Pos('-', tstr);
	tstr2:= '00-00-00';
	If k = 2 Then
		tstr2[2]:= tstr[1]
	Else If k = 3 Then
		Move(tstr[1], tstr2[1], 2);
	Delete(tstr, 1,k);
	k:= Pos('-', tstr);
	If k = 2 Then
		tstr2[5]:= tstr[1]
	Else If k = 3 Then
		Move(tstr[1], tstr2[4], 2);
	Delete(tstr, 1, k);
	tstr[0]:= #2;
	Move(tstr[1], tstr2[7], 2);
	tstr:= tstr2;
End;

Procedure ParseCommands(pstr: String);
{- parse command line and set any specified parameters. pstr contains all
	commands passed on command line.

	Parameter usage:

	EXTSTAT pathName [switches]

	/H or /?     = Show usage
	/Dmm-dd-yy   = Only show files with dates >= mm-dd/yy
}
Var
	k		: Integer;
	tstr	: String;
	opt	: Char;
Begin
	k:= 1;
	pStr:= StUpCase(Trim(pstr));
	While Length(pstr) > 0 Do Begin
		If pStr[1] = '/' Then Begin
			opt:= pStr[2];
			Case opt Of
				'H',
				'?'	: Usage('');
				'D'	: Begin
							k:= 3;
							tstr:= '';
							While (k <= Length(pstr)) and
									not (pstr[k] In [' ','/']) Do Begin
								tstr:= tstr + pstr[k];
								Inc(k);
							End;
							PurgeToNext(pStr, False);
							CheckDate(tstr);
							tDate:= DateStringToDate('MM-DD-YY', tstr);
							If tdate = BadDate Then
								Usage('ERROR: Invalid Date');
						  End;
			Else
				PurgeToNext(pstr, False);
			End;
		End
		Else Begin
			k:= Pos('/', pstr);
			If k = 0 Then
				path:= pstr
			Else
				path:= Copy(pstr, 1, k-1);
			Delete(pstr, 1, Length(path));
		End;
	End;
End;

Procedure AddCmd(Var st: String; st2add: String);
{- add a cmd line parameter to a string. If inside of a quote then add a
	space between }
Var
	inquote	: Boolean;
	k			: Integer;
Begin
	inquote:= False;
	For k:= 1 To Length(st) Do
		If st[k] = '"' Then
			inquote:= not inquote;
	If inquote Then
		st:= st + ' ' + st2add
	Else
		st:= st + st2add;
End;

Procedure GetCommandLine;
Var
	cstr	: String;
	i		: Integer;
Begin
	cstr:= '';
	i:= 1;
	While (i <= ParamCount) Do Begin 		{- get ALL parameters in string }
		AddCmd(cstr, ParamStr(i));
		Inc(i);
	End;
	ParseCommands(cstr);
End;

Begin
	Assign(output, '');
	Rewrite(output);
	tDate:= 0;
	path:= '';
	GetCommandLine;
	If path = '' Then Begin
		Usage('ERROR: path is required');
	End;
	Title;
	writeln('Base path : ', path);
	If tDate <> 0 Then Begin
		writeln('Criteria  : Date greater or equal to ',
					DateToDateString('mm-dd-yy', tDate));
	End
	Else
		writeln('Criteria  : none');
	writeln;
	nummasks:= 1;
	masks[1]:= '*.*';
	Visit(path, '', True, PrnFile, CntDIrs, GoodFile);
end.
