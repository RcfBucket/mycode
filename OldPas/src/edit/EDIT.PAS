Uses
	TpCrt,
	Strs,
	Editor;

Var
	f		: String;

Begin
	DirectVideo:= False;
	editYH:= 18;
	editYL:= 2;
	EditMsgRow:= 24;
	editFrame:= True;
	editTabExp:= 3;
	Clrscr;
	f:= ParamStr(1);
	If f = '' Then Begin
		write('File name: ');
		readln(f);
		If f = '' Then
			Halt;
	End;
	FastWrite('Editing file: '+StUpCase(f), 25, 1, LightCyan);
	EditFile(f);
End.