uses
	Crt;

Type
	Directions = (left, right, up, down);

	VidRec = Record
		ch	: Char;
		at	: Byte;
	End;

Const
	check		: Array[directions] of Set of 1..11 =
						([2,3,5,7,8,9,11], 	{left}
						[2,4,6,7,8,9,10], 	{right}
						[1,3,4,7,8,10,11], 	{up}
						[1,5,6,7,9,10,11]); 	{down}

	AllLineChars	: Array[1..11] of Char =
							('_','_','_','_','_','_','_','_','_','_','_');

	DeltaX			: Array[directions] of ShortInt = (-1,1,0,0);
	DeltaY			: Array[directions] of ShortInt = (-1,1,0,0);

	CharTable		: Array[Directions, 0..15] Of Byte =
							(	(02,02,02,02,06,06,09,09,04,04,08,08,10,10,07,07),
								(02,02,02,02,05,09,05,09,03,08,03,08,11,07,11,07),
								(01,06,05,09,01,06,05,09,01,10,11,07,01,10,11,07),
								(01,04,03,08,01,10,11,07,01,04,03,08,01,10,11,07));

	UpArrow	= 'H';
	DnArrow	= 'P';
	LArrow	= 'K';
	RArrow	= 'M';
	ESC		= #27;

Function ValidChar(dir: Direction; row, col: Integer): Boolean;
Var
	videoMem		:
Begin
End.
