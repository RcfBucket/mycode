{$IFDEF Debug}
	{$D+,L+}
{$ELSE}
   {$D-,L-}
{$ENDIF}


Unit Files;

{- This unit contains the master file list and functions to operate on
	that list }

INTERFACE

Uses
	Strs,
	Dos,
	MemLib,
	Utils,
	Global,
	Objects;

Type
	{- node containing path name for a file }
	PPathObj = ^TPathObj;
	TPathObj = Object(TObject)
		pName	: PString;
		count	: Word;		{- number of files referencing path }
		next	: PPathObj;
		prev	: PPathObj;
		{}
		constructor Init(APath: String);
		destructor Done; virtual;
	End;

	PFileObj = ^TFileObj;
	TFileObj = Object(TObject)
		fname		: String[8];
		ext		: String[3];
		attr		: Byte;
		fDate		: Word;
		fTime		: Word;
		size		: LongInt;
		flags		: Byte;		{- flags are not used in this unit but can be
										used by other units to do with as they like,
										ie, set marked files ... }
		fileNum	: Integer;	{- ordinal number of file in linked list. the
										fileNum'th file }
		path		: PPathObj;
		next		: PFileObj;
		prev		: PFileObj;
		{}
		constructor Init(Var srec: SearchRec);
		destructor  Done; virtual;
		Function DOSFileName: String; {- return a valid dos file name }
		Function DOSFilePath: String; {- return path name }
		Procedure CvtToSearchRec(Var srec: SearchRec);
	End;

	PFileList = ^TFileList;
	TFileList = Object(TObject)
		fileHead		: PFileObj; {- pointers to list of files objects      R/O }
		fileTail		: PFileObj;	{- pointer to end of file list            R/O }
		pathHead		: PPathObj; {- pointer to list of path names objects  R/O }
		pathTail		: PPathObj;	{- pointer to end of path name list       R/O }

		totfiles		: Word;		{- total number of files in list          R/O }
		totbytes		: Comp;		{- total bytes in file list               R/O }
		totDirs		: Word;		{- total number of dirs                   R/O }

		fcount		: Integer;	{- used to count files as their inserted, used
											to set fileNum field that shows what the
											'unsorted' index of the file is }

		constructor Init;
		destructor Done; virtual;
		function   AddFile(path: PathStr; srec: SearchRec): PFileObj;
		function   AddNewFile(path: PathStr; Var srec: SearchRec): PFileObj;
		procedure  RemoveFile(fptr: PFileObj);
		procedure  ClearList;
		function   FindPath(path: PathStr): PPathObj;

		private
		procedure RemovePathRef(fptr: PFileObj);
		function AddPathName(path: PathStr): PPathObj;
		function AddPrim(path: PathStr; srec: SearchRec; FindCount: Boolean): PFileObj;
	End;


IMPLEMENTATION

{-----------------------------------------------------[ TFileList ]--}

Constructor TFileList.Init;
Begin
	inherited Init;
	fileHead:= Nil;
	fileTail:= Nil;
	pathHead:= Nil;
	pathTail:= Nil;

	totfiles:= 0;
	totbytes:= 0;
	totDirs:= 0;
	fcount:= 0;
End;

Destructor TFileList.Done;
Begin
	ClearList;
	inherited Done;
End;

Procedure TFileList.ClearList;
{- remove all items from the list and dispose all memory used by items }
Var
	aux1		: PFileObj;
	aux2		: PPathObj;
Begin
	{- free up path list }
	While pathHead <> Nil Do Begin
		aux2:= pathHead;
		pathHead:= pathHead^.next;
		Dispose(aux2, Done);
	End;
	pathHead:= Nil;
	pathTail:= Nil;
	While filehead <> Nil Do Begin
		aux1:= filehead;
		filehead:= filehead^.next;
		Dispose(aux1, Done);
	End;
	totfiles:= 0;
	totbytes:= 0;
	totDirs:= 0;
	fcount:= 0;
End;

Procedure TFileList.RemovePathRef(fptr: PFileObj);
{- remove reference to path list. If this is the only file using the
	referenced path entry then remove it, otherwise just reduce the reference
	count in path entry }
Var
	p		: PPathObj;
Begin
	p:= fptr^.path;
	If p^.count <= 1 Then Begin
		If p = pathhead Then				{- update head/tail pointers }
			pathHead:= pathHead^.next;
		If p = pathtail Then
			pathtail:= pathtail^.prev;

		Dispose(p, Done);
	End
	Else
		Dec(p^.count);
End;

Procedure TFileList.RemoveFile(fptr: PFileObj);
Begin
	If fptr <> Nil Then Begin
		{- update directory totals }
		If IsAttr(fptr^.attr, Directory) Then
			Dec(totDirs)
		Else Begin
			Dec(totFiles);
			totBytes:= totbytes - fptr^.size;
		End;

		RemovePathRef(fptr);
		If fptr = filehead Then				{- update head / tail }
			fileHead:= fileHead^.next;
		If fptr = filetail Then
			filetail:= filetail^.prev;

		If fptr^.prev <> Nil Then			{- relink }
			fptr^.prev^.next:= fptr^.next;
		If fptr^.next <> Nil Then
			fptr^.next^.prev:= fptr^.prev;

		Dispose(fptr, Done);
	End;
End;

Function TFileList.FindPath(path: PathStr): PPathObj;
{- find specified path in path list, return Nil if not found, return
	pointer to item if found }
Var
	p		: PPathObj;
	found	: Boolean;
Begin
	p:= pathHead;
	found:= False;
	While (p <> Nil) and not found Do Begin
		found:= p^.pname^ = path;
		If not found Then
			p:= p^.next;
	End;
	FindPath:= p;
End;

Function TFileList.AddPathName(path: PathStr): PPathObj;
Var
	aux	: PPathObj;
Begin
	path:= AddBackSlash(StUpCase(path));
	aux:= FindPath(path);
	If aux = Nil Then Begin 	{- path not found, add it to list. }
		{- if getmem fails aux will = Nil }
		aux:= New(PPathObj, Init(path));
		If aux <> Nil Then Begin
			If pathhead = Nil Then Begin
				pathHead:= aux;
				pathTail:= aux;
			End
			Else Begin
				aux^.prev:= pathTail;	{- link up }
				pathTail^.next:= aux;
				pathTail:= aux;			{- reassign tail }
			End;
		End;
	End;
	AddPathName:= aux;
End;

Function TFileList.AddPrim(path: PathStr; srec: SearchRec;
									FindCount: Boolean): PFileObj;
{- if findCount then find first free file number from list, otherwist assign
	fcount + 1 to fileNum }
Var
	fptr	: PFileObj;
	pptr	: PPathObj;
	found	: Boolean;
	last	: Integer;
	temp	: PFileObj;
Begin
	AddPrim:= Nil;		{- assume the worst }
	pptr:= AddPathName(path);
	If pptr <> Nil Then Begin
		fptr:= New(PFileObj, Init(srec));
		If fptr <> Nil Then Begin
			fptr^.path:= pptr;	{- point file entry to its path record }
			Inc(pptr^.count);		{- inc reference count to path name }

			If findCount Then Begin
				temp:= fileHead;
				found:= False;
				last:= temp^.fileNum;
				While not found and (temp <> Nil) Do Begin
					If temp^.next^.fileNum > last+1 Then Begin
						found:= True;
						fptr^.fileNum:= last+1;
					End;
					temp:= temp^.next;
					last:= temp^.fileNum;
				End;
				If not found Then Begin
					Inc(fcount);
					fptr^.fileNum:= fcount;
				End;
			End
			Else Begin
				Inc(fcount);
				fptr^.fileNum:= fcount;
			End;

			If filehead = Nil Then Begin
				fileHead:= fptr;
				fileTail:= fptr;
			End
			Else Begin
				fptr^.prev:= filetail;
				fileTail^.next:= fptr;
				fileTail:= fptr;
			End;

			If IsAttr(fptr^.attr, Directory) Then
				Inc(totDirs)
			Else Begin
				Inc(totFiles);
				totBytes:= totbytes + fptr^.size;
			End;
			AddPrim:= fptr;
		End;
	End;
End;

Function TFileList.AddFile(path: PathStr; srec: SearchRec): PFileObj;
{- add a file to the list. Return Nil if not enough memory avail otherwise
	return a pointer to the object added. This routine should only be used
	when initially filling an empty list. If it is not the fileNum field in
	TFileObj will not be maintained properly. Use AddNewFile to add a file
	once the list is initially created }
Begin
	AddFile:= AddPrim(path, srec, False);
End;

Function TFileList.AddNewFile(path: PathStr; Var srec: SearchRec): PFileObj;
{- this routine will add the file to the list. It should be used after the
	initial list is built from a directory read. This routine will set the
	proper fileNum field in the file object (as if it were actually added
	to the directory). Nil will be returned if not enough memory otherwise
	a pointer to the object added will be returned }
Begin
	AddNewFile:= AddPrim(path, srec, True);
End;

{--------------------------------------------------------[ TFileObj ]--}

Constructor TFileObj.Init(Var srec: SearchRec);
Var
	tstr	: PathStr;
	tstr2	: ExtStr;
Begin
	inherited Init;
	size:= sRec.size;
	attr:= sRec.attr;
	fTime:= sRec.time and $FFFF;
	fDate:= (sRec.time Shr 16) and $FFFF;
	fSplit(srec.name, tstr, fname, tstr2);
	ext:= StripRange(tstr2, '.', '.');	{- remove '.' from extent }
End;

Destructor TFileObj.Done;
{- ensures that the link chain is maintained when object is disposed of }
Begin
	inherited Done;
	If prev <> Nil Then			{- relink }
		prev^.next:= next;
	If next <> Nil Then
		next^.prev:= prev;
End;

Function TFileObj.DOSFileName: String;
{- return a valid dos file name from fp }
Var
	tstr	: String[15];
Begin
	tstr:= fname;
	If ext <> '' Then
		tstr:= tstr + '.' + ext;
	DOSFileName:= tstr;
End;

Function TFileObj.DOSFilePath: String;
{- return path name from file rec }
Begin
	If path^.pname <> Nil Then
		DOSFilePath:= path^.pname^
	Else
		DOSFilePath:= '<no path>'
End;

Procedure TFileObj.CvtToSearchRec(Var srec: SearchRec);
{- convert file object info to a dos SearchRec format }
Begin
	srec.attr:= attr;
	srec.time:= (LongInt(fdate) shl 16) or ftime;
	srec.size:= size;
	srec.name:= DOSFileName;
End;

{--------------------------------------------------------[ TFileObj ]--}

Constructor TPathObj.Init(APath: String);
Begin
	inherited Init;
	pname:= NewStr(APath);
End;

Destructor TPathObj.Done;
{- disposes object and ensures that the link chain is maintained }
Begin
	DisposeStr(pname);
	If prev <> Nil Then			{- relink }
		prev^.next:= next;
	If next <> Nil Then
		next^.prev:= prev;

	inherited Done;
End;

END.

