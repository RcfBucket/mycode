{$IFDEF Debug}
	{$D+,L+}
{$ELSE}
	{$D-,L-}
{$ENDIF}

Unit MsgLib;

INTERFACE

Uses
	VisnObjs,
	NewViews,
	Tone,
	Global,
	Strs,
	Objects, Drivers, Dialogs, Views, App;

Const
	WrapMsgWidth		= 65;		{- width messages will be wrapped at }
										{- 30 <= WrapMsgWidth <= 76 }

	cmRetry		= 6000;		{- commands returned by Msg }
	cmSkip		= 6001;
	cmYesToAll	= 6002;
	cmAbort		= 6003;

	{- message button flags }
	mfOk			= $0001;
	mfRetry		= $0002;
	mfSkip		= $0004;
	mfYes			= $0008;
	mfNo			= $0010;
	mfYesToAll	= $0020;
	mfCancel		= $0040;
	mfAbort		= $0080;
	mfNoFirst	= $8000;		{- put no before a yes }

	{- shortcut flags }
	mfYesNoCancel 			= $0058;
	mfOkCancel	  			= $0041;
	mfSkipRetryCancel		= $0046;

	{- msg title flags }
	mfWarning	= 1;
	mfError		= 2;
	mfInfo		= 3;
	mfConfirm	= 4;

Function Msg(AMsg1, AMsg2: String; params: Pointer;
				 AMsgType, AMsgOpts: Word): Word;
Function MakeWaitWindow(const msg: String): PDotWindow;

IMPLEMENTATION

Type
	PWaitMsg = ^TWaitMsg;
	TWaitMsg = Object(TDotWindow)
		constructor Init(msg: String);
		procedure SizeLimits(Var min, max: TPoint); virtual;
	End;

Const
	msgTitles		: Array[1..4] of String[15] =
							('Warning', 'Error', 'Information', 'Confirm');

	maxButtons		= 7;
	msgButtons		: Array[0..MaxButtons] of String[12] =
								(	'O~k~',
									'~R~etry',
									'~S~kip',
									'~Y~es',
									'~N~o',
									'Yes to ~A~ll',
									'~C~ancel',
									'~A~bort'
								);

	msgCmds			: Array[0..MaxButtons] of Word =
								(	cmOk,
									cmRetry,
									cmSkip,
									cmYes,
									cmNo,
									cmYesToAll,
									cmCancel,
									cmAbort
								);

Type
	PMsgBox = ^TMsgBox;
	TMsgBox = Object(TDotDialog)
		constructor Init(bounds: TRect; AMsgType: Word);
		procedure HandleEvent(Var event: TEvent); virtual;
	End;

Function Msg(AMsg1, AMsg2: String; params: Pointer; AMsgType, AMsgOpts: Word): Word;
{- put a msg on the screen. At most, 4 buttons may be defined. If more than
	4 are requested then only the first four will be used. Params is used
	for AMsg1. (See FormatStr for params) }
Const
	{- mfCheck masks all possible buttons in mfXXXX }
	mfCheck		= $00FF;		{- flag to check to see if buttons exist in opts}
Var
	d		: PMsgBox;
	r		: TRect;
	v		: PView;
	k		: Integer;
	x		: Integer;
	butCnt: Integer;
	list	: Array[0..MaxButtons] of PView;
	yesAt, noAt : Integer;
	tstr	: String;
Begin
	DeskTop^.GetExtent(r);
	r.Assign(0,r.b.y - 12, wrapMsgWidth+4,0);
	r.b.y:= r.a.y + 10;
	If AMsg2 = '' Then Begin
		Inc(r.a.y, 3);
	End;
	d:= New(PMsgBox, Init(r, AMsgType));
	With d^ Do Begin
		r.Assign(2,2,2+wrapMsgWidth,3);
		If params <> Nil Then
			FormatStr(tstr, AMsg1, params^)
		Else
			tstr:= AMsg1;
		v:= New(PStaticBright, Init(R, ^C+tstr, jNone));
		Insert(v);

		If AMsg2 <> '' Then Begin
			r.Assign(2,4,2+wrapMsgWidth,6);
			v:= New(PStaticNorm, Init(r, ^C+AMsg2, jNone));
			Insert(v);
		End;
		x:= -3;
		butcnt:= 0;
		r.Assign(0, 0, 14, 2);
		yesAt:= -1;
		noAt:= -1;
		k:= 0;
		If mfCheck and AMsgOpts = 0 Then	{- ensure at least one button defined}
			AMsgOpts:= mfOK;
		While (k <= maxButtons) and (butcnt <4) Do Begin
			If AMsgOpts and ($1 Shl k) <> 0 Then Begin
				v:= New(PButton2, Init(r, msgButtons[k], msgCmds[k], bfNormal));
				Inc(x, v^.Size.x + 3);
				If (1 Shl k) and mfYes <> 0 Then
					yesAt:= butcnt;
				If (1 Shl k) and mfNo <> 0 Then
					noAt:= butcnt;
				list[butcnt]:= v;
				Inc(butcnt);
			End;
			Inc(k);
		End;
		{- swap Yes and no if specified }
		If (AMsgOpts and mfNoFirst <> 0) and (yesAt >= 0) and (noAt >= 0) Then Begin
			v:= list[yesAt];
			list[yesAt]:= list[noAt];
			list[noAt]:= v;
		End;
		x:= (size.x - x) Shr 1;		{- start of first button }
		For k:= 0 To butcnt - 1 Do Begin
			v:= list[k];
			Insert(v);
			v^.MoveTo(x, size.Y - 3);
			Inc(x, v^.Size.X + 3);
		End;
		SelectNext(False);
	End;

	Msg:= Application^.ExecuteDialog(d, Nil);
End;

{---------------------------------------------------[ TMsgBox ]--}

Constructor TMsgBox.Init(bounds: TRect; AMsgType: Word);
Begin
	inherited Init(bounds, msgTitles[AMsgType]);
	If (AMsgType = mfError) or (AMsgType = mfWarning) Then Begin
		palette:= dpErrDialog;
		If defaults.preferences and dfltBeepOnError <> 0 Then
			ErrorBeep;
	End
	Else
		palette:= dpMsgDialog;
	options:= options and not ofCentered or ofCenterX;
	{- dont allow close button }
	flags:= flags and not wfClose;
End;

Procedure TMsgBox.HandleEvent(Var event: TEvent);
Begin
	If (event.what = evKeyboard) and (event.keyCode = kbEsc) Then
		ClearEvent(event);
	inherited HandleEvent(event);
	If (event.what = evCommand) Then Begin
		Case Event.Command of
			cmRetry, cmSkip, cmYesToAll, cmAbort:
				If State and sfModal <> 0 Then Begin
					EndModal(Event.Command);
					ClearEvent(Event);
				End;
		End;
	End;
End;

Constructor TWaitMsg.Init(msg: String);
Var
	k	: Integer;
	r	: TRect;
Begin
	k:= Length(msg);
	If k > 60 Then
		k:= 60;
	If k < 20 Then
		k:= 20;
	r.Assign(0,0, k+8, 5);
	inherited Init(r, '', wnNoNumber);
	options:= options or ofCentered;
	palette:= wpCyanWindow;
	flags:= flags and not wfGrow and not wfZoom and not wfClose;
	SetState(sfShadow, Shadows);
	GetExtent(r);
	r.Grow(-1,-1);
	r.a.y:= 2; r.b.y:= r.a.y+1;
	Insert(New(PWinStatic, Init(r, ^C+msg, jNone)));
End;

Procedure TWaitMsg.SizeLimits(Var min, max: TPoint);
Begin
	inherited SizeLimits(min, max);
	min.x:= 20;
	min.y:= 5;
End;

Function MakeWaitWindow(const msg: String): PDotWindow;
Var
	p	: PDotWindow;
Begin
	p:= New(PWaitMsg, Init(msg));
	Application^.InsertWindow(p);
	MakeWaitWindow:= p;
End;

END.