{$IFDEF Debug}
	{$D+,L+}
{$ELSE}
   {$D-,L-}
{$ENDIF}


Unit Status;

{- This unit provides a base object that can be derived from to show status
	information. It is useful for displaying status messages for long
	operations and provides the capability of aborting the operation. The
	status box can be moved around while processing is being performed.

	See bottom of file for details.
}

INTERFACE

Uses
	Strs,
	VisnObjs,
	NewViews,
	Tone,
	Menus, MsgBox, App, Drivers, Dialogs, Views, Objects;

Type
	{- file status object is a generic status object to use when processing
		files. it is initialize with the total number of files
	}
	PStatusBox = ^TStatusBox;
	TStatusBox = Object(TDotDialog)
		private
		abortCurrentop	: Boolean;
		abortCMD			: Word;
		oldStatus		: PStatusLine;
		{}
		public
		constructor Init(bounds: TRect; ATitle: TTitleStr);
		destructor Done; virtual;
		procedure InsertControls; virtual;

		procedure UpdatePrior(p1, p2, p3, p4: Pointer); virtual;
		procedure UpdateAfter(p1, p2, p3, p4: Pointer); virtual;

		procedure Idle; virtual;
		procedure HandleEvent(Var event: TEvent); virtual;
		function  Valid(cmd: Word): Boolean; virtual;
		function  AbortOperation: Boolean; virtual;
		function  OKToAbort: Boolean; virtual;
	End;


IMPLEMENTATION

{-----------------------------------------------------[ TStatusBox ]--}

Constructor TStatusBox.Init(bounds: TRect; ATitle: TTitleStr);
Var
	r		: TRect;
Begin
	inherited Init(bounds, ATitle);

	InsertControls;

	SelectNext(False);

	abortCurrentOp:= False;		{- set abort flag to false }
	CtrlBreakHit:= False;		{- ensure no pending breaks }
	abortCmd:= 0;					{- set command use to abort operation to 0 }
	oldstatus:= statusline;
	statusline:= Nil;

	application^.GetExtent(r);
	r.a.y:= r.b.y - 1;
	statusline:= New(PStatusLine, Init(R,
		NewStatusDef(0, 0,
			NewStatusKey('~Ctrl-Break~ to Abort', 0, cmCancel,
			nil),
		NewStatusDef(1, 1,
			NewStatusKey('* Current operation paused while window is being moved.',
							 0, 0,
			nil),
		Nil))));
	application^.Insert(statusline);
End;

Destructor TStatusBox.Done;
Var
	e	: TEvent;
Begin
	Dispose(statusline, Done);
	statusline:= oldstatus;
	inherited Done;
End;

Procedure TStatusBox.InsertControls;
Var
	r			: TRect;
	control	: PView;
Begin
	GetExtent(r);
	r.Assign(0, r.b.y-3, 10, r.b.y-1);
	control:= New(PButton2, Init(R, 'Cancel', cmCancel, bfNormal));
	control^.options:= control^.options or ofCenterX;
	Insert(control);
End;

Function TStatusBox.AbortOperation: Boolean; Assembler;
ASM
	les	di,self
	mov	al, es:byte ptr abortcurrentOp[di]
End;

Procedure TStatusBox.UpdatePrior(p1, p2, p3, p4: Pointer);
{- by default does nothing with parameters. descendents should update
	display }
Begin
	Idle;
End;

Procedure TStatusBox.UpdateAfter(p1, p2, p3, p4: Pointer);
{- by default does nothing with parameters. descendents should update
	display }
Begin
	Idle;
End;

Procedure TStatusBox.HandleEvent(Var event: TEvent);
var
	cmd: Word;
Begin
	inherited HandleEvent(event);
	If (event.what and evCommand <> 0) Then Begin
		Valid(event.command);
		ClearEvent(event);
	End;
End;

Function TStatusBox.Valid(cmd: Word): Boolean;
Begin
	Valid:= True;
	If not abortCurrentOp Then Begin
		If (cmd = cmClose) or (cmd = cmCancel) Then Begin
			abortcurrentop:= OKToAbort;
			If abortCurrentOp Then
				abortCmd:= cmd;
			Valid:= False;
		End;
	End;
End;

Function TStatusBox.OKToAbort: Boolean;
Begin
	BoundsBeep;
	OKToAbort:= MessageBox(^C'Abort current operation?', Nil,
								  mfConfirmation or mfYesButton or mfNoButton)
					= cmYes;
End;

Procedure TStatusBox.idle;
Const
	InBreak  : Boolean = False;
Var
	e		: TEvent;
begin
	If abortCurrentOP Then Exit;	{- aperation aborted }
	If not inBreak Then Begin
		inBreak:= True;
		If CtrlBreakHit Then Begin
			CtrlBreakHit:= False;
			While eventavail Do
				GetEvent(e);		{- clear pending events }
			abortCurrentOp:= OKToAbort;
			If abortCurrentOp Then
				abortCmd:= $FFFF;
			CtrlBreakHit:= False;
		End
		Else If eventAvail Then Begin
			GetEvent(e);
			HandleEvent(e);
		End;
		inBreak:= False;
	End;
End;


(**** DOCUMENTATION **************************************************

	The default object, initializes an empty dialog box with only a cancel
	button. Descendents, can however, change this behavior by overriding the
	InsertControls method and inserting the desired controls.

	By default, any attempt to close the status dialog, or Ctrl-Break or
	selecting the cancel button will prompt to confirm the abort, if yes is
	selected, then the operation is aborted, otherwise, it continues.

	* Once the user aborts, the dialog will be DEAD. And will not respond to
	  events. The calling process should detect that the operation has been
	  aborted and dispose of the status box.

	A sample usage of this object is as follows:

		Begin
			p:= New(PStatusBox, Init(bounds, Title));
			application^.InsertWindow(p);

			While not p^.AbortOperation Do Begin
				p^.UpdatePrior(...);
				If not p^.AbortOperation Then Begin
					ProcessYourStuff;
					...
					p^.UpdateAfter(...);
				End;
			End;
			If desired Then CheckAbortCmd (p^.abortCMD)
			Dispose(p, Done);
		End;

	RULE AND LIMITATIONS:
	An application object to be up and running. After creating the status
	object, it must be modeless, INSERTED (app^.InsertWindow). If execDialog
	is called this object won't work. The main processing loop of the
	operation to be performed must check the AbortOperation method to see if
	it needs to abort the operation. In order for the status object to check
	if the user aborted, at least one or both of the UpdatePrior/UpdateAfter
	methods must be called during processing. The dialog must only be closed
	and disposed of by the routine that created it. NO user operation should
	be allowed to close the dialog (ie, selecting the Close icon on the
	dialog).  NO method in the status box object should close the dialog.
	The Dispose(.., DONE) method should be the ONLY thing that destroys the
	status box.

	Here's how it works:
		When initialized, the status box will call it's inherited Init. Then
		it will call InsertControls. By default, one cancel button is placed
		in the center and at the bottom of the status box. Next, the
		applications status line is temporarily replace by a new one with
		one option (Ctrl-Break to abort). This avoids the problem with the
		user clicking on the main status line while processing is being
		performed (if that is done, then when processing completes, the
		option clicked will be performed by the applications status line. The
		status line is replaced when the status box is disposed of.

		Once up and running UpdatePrior/UpdateAfter call an idle method in the
		status box to see if pending events are available. Idle checks two
		things -

		Ctrl-Break  If Ctrl-Break is hit, the OkToAbort method is called.
						If true is returned then the operation is aborted.
						AbortCmd is set to $FFFF.

						* The Ctrl-Break on the temorary status line is the same
						  as selecting Cancel.

		Any Event	If an event occurs, (ie, dragging the status box,
						selecting cancel, pressing Esc) then Idle gets the event
						and passes it to the Status box handleevent. HandleEvent
						will automatically handle things like dragging the window
						around. For commands, (ie, Cancel,Close), handleEvent will
						call the Valid method passing the command. Valid first
						checks to see if the operation has already been aborted,
						if not, it checks the command. If the command is an abort
						command (Close/Cancel) then it prompts the user to confirm.
						If yes then the abortFlag is set and Valid returns False.
						THIS IS VERY IMPORTANT!
							If valid decides to abort the operation, it must return
							false so the status box does not close. This gives the
							main processing routine a chance to check the AbortFlag
							and quit processing and close the dialog itself.

	Customizing the status box.

		UpdatePrior/UpdateAfter methods -

		The main processing loop (the one that created the status box) should
		call one or both of these methods. At least one MUST be called in order
		for the status box to get an oportunity to check events.
		UpdatePrior is meant to be called before some task is executed, and
		UpdateAfter is meant to be called after the task is executed. Both
		methods take 4 pointers as paramters. By default, these parameters
		are ignored. But descending objects can make use of them as they wish.

		InsertControls method -

		Override this method if you wish to insert different controls like a
		status message. If you want just one cancel button be sure to call
		the default (inherited) InsertControls method. If you wish to add
		other buttons that can stop the operation (ie, pause) you must also
		add to the valid() method (see Valid).

**********************)

END.

