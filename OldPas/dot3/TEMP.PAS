uses
	dos;

Var
	resVal	: Word;

Function ResolvePath(path: String): String;
{- uses undocumented function $60, to return a fully qualified path for
	a version of the same path.
	AH 		= 60h
	DS:SI    = Pointer to path string
	ES:DI		= Pointer to destination string to store result
}
Var
	newPath	: String;
	res		: Word;
Begin
	ASM
		push	ds
		lea	si, path
		mov	dx, si			{ save SI }
		inc	dx					{ DX => path[1] }
		xor	ah, ah
		mov	al, ss:[si]		{ AL = length(path) }
		add	si, ax
		inc	si
		mov	byte ptr ss:[si], 0	{ add terminating null to path }

		cld

		mov	ax, ss
		mov	ds, ax
		mov	es, ax
		lea	di, newpath
		mov	si, dx
		mov	ah, 60h
		int	21h
		mov	[res], ax
		jnc	@NoErr
		les	si, @result
		xor	al,al
		stosb
		jmp	@Done
	@NoErr:
		lea	si, newPath
		lodsb
		xor	ah,ah
		mov	cx, ax
		les	di, @result
		inc	di

	@Done:
		pop	ds
	END;
	resVal:= res;
End;

Function TrueName(path: String): String;
Var
	newPath	: String;
	i 			: Byte;
	res		: Word;
Begin
	path[Length(path)+1] := #0;
	ASM
		push	ds
		mov	ax,ss
		mov	ds,ax
		mov	es,ax
		lea	si,path
		inc	si
		lea	di,newpath
		inc	di
		mov	ah,60h
		int 	21h
		jc 	@@1
		mov 	ax,0
	@@1:
		mov 	[res],ax
		pop 	ds
	END;
	resval:= res;
	If res <> 0 Then
		TrueName:= ''
	Else Begin
		i:= 1;
		While newPath[i] <> #0 Do
			Inc(i);
		newPath[0]:= Chr(i-1);
		TrueName:= NewPath;
	End;
End;

Var
	tstr	: String;
begin
	{d:\ is the subst'ed from c:\usr .}
	Writeln(TrueName('F:\'));
	fillchar(tstr, sizeof(tstr), #254);
	tstr:= 'F:\';
	Repeat
		writeln;
		write('Path: '); readln(tstr);
		writeln(TrueName(tstr), '   Result: ', resVal);
	Until tstr = '';
end.
