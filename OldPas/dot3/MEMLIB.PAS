{$IFDEF Debug}
	{$D+,L+}
{$ELSE}
   {$D-,L-}
{$ENDIF}


{$DEFINE ShowDump}

Unit MemLib;

{- Memory allocation functions }

INTERFACE

Uses
	Tone,
	WaitLib,
	Memory;

Function GetMemCheck(Var P; Bytes: Word): Boolean;
Procedure FreeMemCheck(Var P; Bytes : Word);

IMPLEMENTATION

{$IFDEF ShowDump}
Var
	startMem		: LongInt;
	exitSave		: Pointer;
{$ENDIF}

Function GetMemCheck(var P; Bytes: Word): Boolean;
{- allocate heap space, returning true if successful. Returns False if
	the allocation will dip into the free pool and p will = Nil }
Begin
	Pointer(p):= MemAlloc(bytes);
	GetMemCheck:= (Pointer(p) <> nil);
End;

Procedure FreeMemCheck(var P; Bytes : Word);
{- deallocate heap space}
Begin
	If Pointer(p) <> nil Then Begin
		FreeMem(Pointer(p), Bytes);
		Pointer(p):= nil;
	End;
End;

{$IFDEF ShowDump}

Procedure ExitRoutine; Far;
Begin
	ExitProc:= exitSave;
	If startMem <> MemAvail Then Begin
		writeln;
		writeln('Starting memory: ', startMem);
		writeln('Ending memory  : ', MemAvail);
		writeln('Difference     : ', startMem - Memavail);
		BoundsBeep;
		Wait(10);
		BoundsBeep;
	End;
End;

{$ENDIF}

BEGIN
{$IFDEF ShowDump}
	exitSave:= ExitProc;
	ExitProc:= @ExitRoutine;
	startMem:= MemAvail;
{$ENDIF}
END.
