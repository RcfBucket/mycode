{$IFDEF Debug}
	{$D+,L+}
{$ELSE}
	{$D-,L-}
{$ENDIF}

Unit Keys;

INTERFACE

Uses
	Strs,
	Objects, Drivers, Views, Dialogs, App,
	MsgLib,
	VisnObjs,
	Global,
	inpLib,
	NewViews;

Function KeyName(key: Word): KeyStrType;
Procedure ChangeKeys;
Function MenuBarHotKey(key: Word): Boolean;
Procedure SetKeyStrings;
Procedure ResetDefaultCmds;
Procedure SetDefaultCmds;
Procedure SetCustCommands;
Procedure KillCustCommands;
Function IsSpecialKey(key: Word): Boolean;

IMPLEMENTATION

Const
	cmDefaults			= 7010;
	cmEdit				= 7011;
	cmChangeScroll		= 7012;
	cmDelete				= 7013;
	cmSearch				= 7014;
	cmPrintKeys			= 7015;
	cm1					= 7020;
	cm2					= 7021;
	cm3					= 7022;
	cm4					= 7023;
	cm5					= 7024;
	cmSrchCommand		= 7030;
	cmSrchDescription	= 7031;


Type
	PKeyViewer = ^TKeyViewer;
	TKeyViewer = object(TListViewer)
		dispDX	: Integer;
		{}
		function GetText(item: Integer; maxLen: Integer): String; virtual;
		constructor Init(bounds: TRect; VBar: PScrollBar);
		procedure ChangeDelta(newOfs: Integer);
		procedure HandleEvent(Var event: TEvent); virtual;
	End;

	PKeyScroll = ^TKeyScroll;
	TKeyScroll = object(TScrollBar)
		procedure HandleEvent(Var event: TEvent); virtual;
	End;

	PKeyDialog = ^TKeyDialog;
	TKeyDialog = Object(TDotDialog)
		keyList	: PKeyViewer;
		{}
		constructor Init;
		procedure HandleEvent(Var event: TEvent); virtual;
		procedure EditCurrent;
		function  CheckKey(Var key: Word; cur: Integer): Boolean;
		procedure DoSearch;
		function  FindOne(AStart: Integer; AKey: Word; ACmd, ADescr: String): Integer;
	End;

	PKeySearch = ^TKeySearch;
	TKeySearch = Object(TDotDialog)
		constructor Init;
		procedure HandleEvent(Var event: TEvent); virtual;
	End;

Var
	exitSave		: Pointer;
	specialKey	: Boolean;

Function KeyUpCase(key: Word): Word;
{- convert a key code to its equivelent uppercase }
Var
	k		: Char absolute key;
Begin
	If (k >= 'a') and (k <= 'z') Then
		k:= Upcase(k);

	KeyUpCase:= key;
End;

Function ProtectedKey(key: Word): Boolean;
{- return true if key is protected }
Begin
	ProtectedKey:= (key = kbPgDn) 		or (key = kbPgUp) 				or
						(key = kbCtrlPgUp)	or (key = kbCtrlPgDn)			or
						(key = kbUp) 			or (key = kbDown) 				or
						(key = kbLeft) 		or (key = kbRight) 				or
						(key = kbHome)			or (key = kbEnd)					or
						(key = kbTab)			or (key = kbShiftTab)			or
                  (key = kbEsc)        or (key = kbEnter);
End;


Function MenuBarHotKey(key: Word): Boolean;
{- return true if key is a key that would be used to activate the
	menubar }
Begin
	MenuBarHotKey:= 	(key = kbAltSpace) or (key = kbAltF) or
							(key = kbAltS) or	(key = kbAltD) or (key = kbAltP) or
							(key = kbAltO) or (key = kbAltW) or (key = kbAltH);
End;

Function StripControls(s: String): String;
{- strip '~' characters from s }
Begin
	StripControls:= StripRange(s, '~', '~');
End;

Procedure SetCustKeys;
{- set the default custom keystrokes }
Begin
	cmds[cmdCust1].keyStroke:= kbAlt1;
	cmds[cmdCust2].keyStroke:= kbAlt2;
	cmds[cmdCust3].keyStroke:= kbAlt3;
	cmds[cmdCust4].keyStroke:= kbAlt4;
	cmds[cmdCust5].keyStroke:= kbAlt5;
End;

Procedure SetCustCommands;
{- set the default custom commands }
Begin
	With cmds[cmdCust1] Do Begin
		cmd:= cmCust1;
		help:= 1005;
		textStr:= NewStr('~1~ '+defaults.custcmds[1].menuText);
		helpStr:= NewStr(defaults.custCmds[1].helpStr);
	End;
	With cmds[cmdCust2] Do Begin
		cmd:= cmCust2;
		help:= 1006;
		textStr:= NewStr('~2~ '+defaults.custcmds[2].menuText);
		helpStr:= NewStr(defaults.custCmds[2].helpStr);
	End;
	With cmds[cmdCust3] Do Begin
		cmd:= cmCust3;
		help:= 1007;
		textStr:= NewStr('~3~ '+defaults.custcmds[3].menuText);
		helpStr:= NewStr(defaults.custCmds[3].helpStr);
	End;
	With cmds[cmdCust4] Do Begin
		cmd:= cmCust4;
		help:= 1008;
		textStr:= NewStr('~4~ '+defaults.custcmds[4].menuText);
		helpStr:= NewStr(defaults.custCmds[4].helpStr);
	End;
	With cmds[cmdCust5] Do Begin
		cmd:= cmCust5;
		help:= 1009;
		textStr:= NewStr('~5~ '+defaults.custcmds[5].menuText);
		helpStr:= NewStr(defaults.custCmds[5].helpStr);
	End;
End;

Procedure KillCustCommands;
Var
	k		: Integer;
Begin
	For k:= cmdCust1 To cmdCust5 Do Begin
		DisposeStr(cmds[k].textStr);
		DisposeStr(cmds[k].helpStr);
	End;
End;

Procedure SetKeyStrings;
{- update the cmds with new keyStr's based on their keystrokes }
Var
	k		: Integer;
Begin
	For k:= 1 To MaxCommands Do
		cmds[k].keyStr:= KeyName(cmds[k].keyStroke);
End;

Procedure SetDefaultCmds;
{- set up commands. records 1..LastChangeable will contain the changeable
	commands }
Begin
	FillChar(cmds, SizeOf(cmds), #0);
	ClearMenuOverrides;

	With cmds[cmdSystemMenu] Do Begin
		cmd:= cmSystemMenu;
		keyStroke:= kbNoKey;
		help:= 1000;
		textStr:= NewStr('~�~');
		helpStr:= NewStr('System Commands');
	End;
	With cmds[cmdClearDesk] Do Begin
		cmd:= cmClearDesk;
		keyStroke:= kbCtrlBack;
		help:= 1001;
		textStr:= NewStr('C~l~ear all');
		helpStr:= NewStr('Close all windows');
	End;
	With cmds[cmdDosShell] Do Begin
		cmd:= cmDosShell;
		keyStroke:= kbF9;
		help:= 1002;
		textStr:= NewStr('~D~OS shell');
		helpStr:= NewStr('Run a DOS shell');
	End;
	With cmds[cmdGetInfo] Do Begin
		cmd:= cmGetInfo;
		keyStroke:= kbNokey;
		help:= 1003;
		textStr:= NewStr('~G~et info');
		helpStr:= NewStr('Show machine information');
	End;

	SetCustCommands;
	SetCustKeys;

{- file commands }
	With cmds[cmdFileMenu] Do Begin
		cmd:= cmFileMenu;
		keyStroke:= kbNokey;
		help:= 1020;
		textStr:= NewStr('~F~iles');
		helpStr:= NewStr('File commands');
	End;
	With cmds[cmdFileAttr] Do Begin
		cmd:= cmFileAttr;
		keyStroke:= kbCtrlA;
		help:= 1021;
		textStr:= NewStr('~A~ttributes');
		helpStr:= NewStr('Change attributes of files');
	End;
	With cmds[cmdFileBrowse] Do Begin
		cmd:= cmFileBrowse;
		keyStroke:= kbCtrlV;
		help:= 1022;
		textStr:= NewStr('~V~iew');
		helpStr:= NewStr('View contents of the current file in Text or Hex mode');
	End;
	With cmds[cmdFileCopy] Do Begin
		cmd:= cmFileCopy;
		keyStroke:= kbCtrlC;
		help:= 1023;
		textStr:= NewStr('~C~opy');
		helpStr:= NewStr('Copy files');
	End;
	With cmds[cmdFileDelete] Do Begin
		cmd:= cmFileDelete;
		keyStroke:= kbCtrlD;
		help:= 1024;
		textStr:= NewStr('~D~elete');
		helpStr:= NewStr('Delete files');
	End;
	With cmds[cmdFileEdit] Do Begin
		cmd:= cmFileEdit;
		keyStroke:= kbCtrlE;
		help:= 1025;
		textStr:= NewStr('~E~dit');
		helpStr:= NewStr('Edit the current file');
	End;
	With cmds[cmdFileMove] Do Begin
		cmd:= cmFileMove;
		keyStroke:= kbCtrlM;
		help:= 1026;
		textStr:= NewStr('~M~ove');
		helpStr:= NewStr('Move files to a new directory');
	End;
	With cmds[cmdFileRename] Do Begin
		cmd:= cmFileRename;
		keyStroke:= kbCtrlR;
		help:= 1027;
		textStr:= NewStr('~R~ename');
		helpStr:= NewStr('Rename the current file');
	End;
	With cmds[cmdFileTimeDate] Do Begin
		cmd:= cmFileTimeDate;
		keyStroke:= kbCtrlT;
		help:= 1028;
		textStr:= NewStr('~T~ime/date');
		helpStr:= NewStr('Change the date and time files');
	End;

{- subdirectory menu }
	With cmds[cmdSubDirMenu] Do Begin
		cmd:= cmSubDirMenu;
		keyStroke:= kbNokey;
		help:= 1040;
		textStr:= NewStr('~S~ubdirs');
		helpStr:= NewStr('Directory commands');
	End;
	With cmds[cmdDirAttr] Do Begin
		cmd:= cmDirAttr;
		keyStroke:= kbNoKey;
		help:= 1041;
		textStr:= NewStr('~A~ttributes');
		helpStr:= NewStr('Change attributes of files is directory structure');
	End;
	With cmds[cmdDirCopy] Do Begin
		cmd:= cmDirCopy;
		keyStroke:= kbNoKey;
		help:= 1042;
		textStr:= NewStr('~C~opy');
		helpStr:= NewStr('Copy directories');
	End;
	With cmds[cmdDirDelete] Do Begin
		cmd:= cmDirDelete;
		keyStroke:= kbNoKey;
		help:= 1043;
		textStr:= NewStr('~D~elete');
		helpStr:= NewStr('Delete directories');
	End;
	With cmds[cmdDirMake] Do Begin
		cmd:= cmDirMake;
		keyStroke:= kbNoKey;
		help:= 1044;
		textStr:= NewStr('Ma~k~e');
		helpStr:= NewStr('Make (create) a sub-directory');
	End;
	With cmds[cmdDirMove] Do Begin
		cmd:= cmDirMove;
		keyStroke:= kbNoKey;
		help:= 1045;
		textStr:= NewStr('~M~ove');
		helpStr:= NewStr('Move directories to a new location');
	End;
	With cmds[cmdDirRename] Do Begin
		cmd:= cmDirRename;
		keyStroke:= kbNoKey;
		help:= 1046;
		textStr:= NewStr('~R~ename');
		helpStr:= NewStr('Rename the current directory');
	End;
	With cmds[cmdDirUtilization] Do Begin
		cmd:= cmDirUtilization;
		keyStroke:= kbNoKey;
		help:= 1047;
		textStr:= NewStr('~U~tilization');
		helpStr:= NewStr('Report the disk utilization of a directory structure');
	End;
	With cmds[cmdTreeMenu] Do Begin
		cmd:= cmTreeMenu;
		keyStroke:= kbNoKey;
		help:= 1048;
		textStr:= NewStr('~T~ree');
		helpStr:= NewStr('Directory tree operations');
	End;
		With cmds[cmdReadTree] Do Begin
			cmd:= cmReadTree;
			keyStroke:= kbAltT;
			help:= 1049;
			textStr:= NewStr('Read ~t~ree');
			helpStr:= NewStr('Display a directory tree for a specified drive');
		End;
		With cmds[cmdExpandAll] Do Begin
			cmd:= cmExpandAll;
			keyStroke:= kbNoKey;
			help:= 1050;
			textStr:= NewStr('~E~xpand all');
			helpStr:= NewStr('Expand all levels of directory tree');
		End;
      With cmds[cmdColapseLev] Do Begin
         cmd:= cmColapseLev;
			keyStroke:= kbNoKey;
			help:= 1051;
			textStr:= NewStr('Colapse at ~l~evel');
			helpStr:= NewStr('Colapse all directories at current level');
		End;
		With cmds[cmdExpandCur] Do Begin
			cmd:= cmExpandCur;
			keyStroke:= kbGrayPlus;
			help:= 1052;
			textStr:= NewStr('~E~xpand current');
			helpStr:= NewStr('Expand current directory');
		End;
		With cmds[cmdExpandNode] Do Begin
			cmd:= cmExpandNode;
			keyStroke:= kbGrayStar;
			help:= 1053;
			textStr:= NewStr('Expand ~n~ode');
			helpStr:= NewStr('Expand current directory and all sub-directories');
		End;
		With cmds[cmdColapseCur] Do Begin
			cmd:= cmColapseCur;
			keyStroke:= kbGrayMinus;
			help:= 1054;
			textStr:= NewStr('~C~olapse');
			helpStr:= NewStr('Colapse current directory');
		End;


{- Disk commands }
	With cmds[cmdDiskMenu] Do Begin
		cmd:= cmDiskMenu;
		keyStroke:= kbNoKey;
		help:= 1060;
		textStr:= NewStr('~D~isk');
		helpStr:= NewStr('Disk commands');
	End;
	With cmds[cmdDiskRead] Do Begin
		cmd:= cmDiskRead;
		keyStroke:= kbAltR;
		help:= 1061;
		textStr:= NewStr('~R~ead');
		helpStr:= NewStr('Read a directory into the CURRENT window');
	End;
	With cmds[cmdDiskReadNew] Do Begin
		cmd:= cmDiskReadNew;
		keyStroke:= kbAltN;
		help:= 1062;
		textStr:= NewStr('~N~ew');
		helpStr:= NewStr('Read a directory into a NEW window');
	End;
	With cmds[cmdDiskInfo] Do Begin
		cmd:= cmDiskInfo;
		keyStroke:= kbAltI;
		help:= 1063;
		textStr:= NewStr('~I~nfo');
		helpStr:= NewStr('Show information about a disk');
	End;
	With cmds[cmdDiskUtilization] Do Begin
		cmd:= cmDiskUtilization;
		keyStroke:= kbAltU;
		help:= 1064;
		textStr:= NewStr('~U~tilization');
		helpStr:= NewStr('Report the disk utilization for an entire disk');
	End;
	With cmds[cmdDiskVolume] Do Begin
		cmd:= cmDiskVolume;
		keyStroke:= kbAltV;
		help:= 1065;
		textStr:= NewStr('~V~olume');
		helpStr:= NewStr('Change the volume label of a disk');
	End;

{- print commands }
	With cmds[cmdPrintMenu] Do Begin
		cmd:= cmPrintMenu;
		keyStroke:= kbNoKey;
		help:= 1080;
		textStr:= NewStr('~P~rint');
		helpStr:= NewStr('Print commands');
	End;
	With cmds[cmdPrintFile] Do Begin
		cmd:= cmPrintFile;
		keyStroke:= kbNoKey;
		help:= 1081;
		textStr:= NewStr('~F~ile');
		helpStr:= NewStr('Print the current file');
	End;
	With cmds[cmdPrintDir] Do Begin
		cmd:= cmPrintDir;
		keyStroke:= kbNoKey;
		help:= 1082;
		textStr:= NewStr('~D~irectory');
		helpStr:= NewStr('Print the displayed directory');
	End;
	With cmds[cmdPrintOpts] Do Begin
		cmd:= cmPrintOpts;
		keyStroke:= kbNoKey;
		help:= 1083;
		textStr:= NewStr('~O~ptions');
		helpStr:= NewStr('Select options for printing files and directories');
	End;

{- options }
	With cmds[cmdOptionsMenu] Do Begin
		cmd:= cmOptionsMenu;
		keyStroke:= kbNoKey;
		help:= 1200;
		textStr:= NewStr('~O~ptions');
		helpStr:= NewStr('Commands to change default settings');
	End;
	With cmds[cmdColors] Do Begin
		cmd:= cmColors;
		keyStroke:= kbNoKey;
		help:= 1201;
		textStr:= NewStr('~C~olors');
		helpStr:= NewStr('Customize colors');
	End;
	With cmds[cmdDirectories] Do Begin
		cmd:= cmDirectories;
		keyStroke:= kbNoKey;
		help:= 1202;
		textStr:= NewStr('~D~irectories');
		helpStr:= NewStr('Set system directories');
	End;
	With cmds[cmdVideo] Do Begin
		cmd:= cmVideo;
		keyStroke:= kbNoKey;
		help:= 1203;
		textStr:= NewStr('~V~ideo options');
		helpStr:= NewStr('Select video display options');
	End;
	With cmds[cmdPreferences] Do Begin
		cmd:= cmPreferences;
		keyStroke:= kbNoKey;
		help:= 1204;
		textStr:= NewStr('~P~references');
		helpStr:= NewStr('Set program preferences');
	End;
	With cmds[cmdFileListDef] Do Begin
		cmd:= cmFileListDef;
		keyStroke:= kbNoKey;
		help:= 1205;
		textStr:= NewStr('~F~ile lists');
		helpStr:= NewStr('Select defaults for file list windows');
	End;
	With cmds[cmdMouseOpts] Do Begin
		cmd:= cmMouseOpts;
		keyStroke:= kbNoKey;
		help:= 1206;
		textStr:= NewStr('~M~ouse');
		helpStr:= NewStr('Set preferences for the mouse');
	End;
	With cmds[cmdKeyboard] Do Begin
		cmd:= cmKeyboard;
		keyStroke:= kbNokey;
		help:= 1207;
		textStr:= NewStr('~K~eyboard');
		helpStr:= NewStr('Customize menu hot keys');
	End;
	With cmds[cmdCustCmds] Do Begin
		cmd:= cmCustCmds;
		keyStroke:= kbNoKey;
		help:= 1208;
		textStr:= NewStr('Custom command~s~');
		helpStr:= NewStr('Define custom menu commands');
	End;

{- list commands }
	With cmds[cmdListMenu] Do Begin
		cmd:= cmListMenu;
		keyStroke:= kbNoKey;
		help:= 1220;
		textStr:= NewStr('~L~ist');
		helpStr:= NewStr('Commands to change file list behavior');
	End;
	With cmds[cmdListCompare] Do Begin
		cmd:= cmListCompare;
		keyStroke:= kbCtrlO;
		help:= 1221;
		textStr:= NewStr('C~o~mpare');
		helpStr:= NewStr('Compare files displayed in windows (marking files that are different)');
	End;
	With cmds[cmdListSort] Do Begin
		cmd:= cmListSort;
		keyStroke:= kbCtrlS;
		help:= 1222;
		textStr:= NewStr('~S~ort');
		helpStr:= NewStr('Select sort criteria for current window');
	End;
	With cmds[cmdListDisplay] Do Begin
		cmd:= cmListDisplay;
		keyStroke:= kbCtrlY;
		help:= 1223;
		textStr:= NewStr('~D~isplay');
		helpStr:= NewStr('Toggle single/multiple column list display');
	End;
	With cmds[cmdListStatus] Do Begin
		cmd:= cmListStatus;
		keyStroke:= kbShiftF2;
		help:= 1224;
		textStr:= NewStr('S~t~atistics ON/OFF');
		helpStr:= NewStr('Turn file statistics in window ON or OFF');
	End;
	With cmds[cmdListShowStat] Do Begin
		cmd:= cmListShowStat;
		keyStroke:= kbF2;
		help:= 1225;
		textStr:= NewStr('S~h~ow list statistics');
		helpStr:= NewStr('Show file statistics (bytes, files, masks, etc.) for current list');
	End;
	With cmds[cmdListFind] Do Begin
		cmd:= cmListFind;
		keyStroke:= kbCtrlF;
		help:= 1226;
		textStr:= NewStr('~F~ind');
		helpStr:= NewStr('Find an file in the file list');
	End;
	With cmds[cmdListParent] Do Begin
		cmd:= cmListParent;
		keyStroke:= kbCtrlLeft;
		help:= 1227;
		textStr:= NewStr('~P~arent');
		helpStr:= NewStr('Read and display the parent directory');
	End;
	With cmds[cmdListRoot] Do Begin
		cmd:= cmListRoot;
		keyStroke:= kbCtrlBackSlash;
		help:= 1228;
		textStr:= NewStr('R~o~ot');
		helpStr:= NewStr('Read and display the root directory');
	End;
	With cmds[cmdListCurrent] Do Begin
		cmd:= cmListCurrent;
		keyStroke:= kbCtrlRight;
		help:= 1229;
		textStr:= NewStr('~C~hild');
		helpStr:= NewStr('Read and display the directory being highlighted');
	End;
	With cmds[cmdListMarkWild] Do Begin
		cmd:= cmListMarkWild;
		keyStroke:= kbAltM;
		help:= 1230;
		textStr:= NewStr('~M~ark by wildcard');
		helpStr:= NewStr('Mark files matching a wildcard');
	End;
	With cmds[cmdListUnMarkWild] Do Begin
		cmd:= cmListUnMarkWild;
		keyStroke:= kbAltU;
		help:= 1231;
		textStr:= NewStr('~U~n-mark by wildcard');
		helpStr:= NewStr('Un-Mark files matching a wildcard');
	End;
	With cmds[cmdListSwapMark] Do Begin
		cmd:= cmListSwapMark;
		keyStroke:= kbCtrlW;
		help:= 1232;
		textStr:= NewStr('S~w~ap marks');
		helpStr:= NewStr('Swap all marks (marked to unmarked, unmarked to marked)');
	End;
	With cmds[cmdListToggleMark] Do Begin
		cmd:= cmListToggleMark;
		keyStroke:= kbIns;
		help:= 1233;
		textStr:= NewStr('~T~oggle current mark');
		helpStr:= NewStr('Toggle mark of current file or directory');
	End;
	With cmds[cmdListRangeMark] Do Begin
		cmd:= cmListRangeMark;
		keyStroke:= kbNoKey;
		help:= 1234;
		textStr:= NewStr('~R~ange marks');
		helpStr:= NewStr('Mark files/directories based on selective criteria');
	End;

{- window commands }
	With cmds[cmdWindowMenu] Do Begin
		cmd:= cmWindowMenu;
		keyStroke:= kbNoKey;
		help:= 1240;
		textStr:= NewStr('~W~indow');
		helpStr:= NewStr('Window related commands');
	End;
	With cmds[cmdNext] Do Begin
		cmd:= cmNext;
		keyStroke:= kbF6;
		help:= 1241;
		textStr:= NewStr('~N~ext');
		helpStr:= NewStr('Make the next window active');
	End;
	With cmds[cmdZoom] Do Begin
		cmd:= cmZoom;
		keyStroke:= kbF5;
		help:= 1242;
		textStr:= NewStr('~Z~oom');
		helpStr:= NewStr('Enlarge or restore the size of the active window');
	End;
	With cmds[cmdResize] Do Begin
		cmd:= cmResize;
		keyStroke:= kbCtrlF5;
		help:= 1243;
		textStr:= NewStr('~R~esize/move');
		helpStr:= NewStr('Change the size or move the active window');
	End;
	With cmds[cmdCascade] Do Begin
		cmd:= cmCascade;
		keyStroke:= kbNoKey;
		help:= 1244;
		textStr:= NewStr('C~a~scade');
		helpStr:= NewStr('Arrange windows on desktop by Cascading');
	End;
	With cmds[cmdTile] Do Begin
		cmd:= cmTile;
		keyStroke:= kbNoKey;
		help:= 1245;
		textStr:= NewStr('~T~ile');
		helpStr:= NewStr('Arrange windows on desktop by Tiling');
	End;
	With cmds[cmdClose] Do Begin
		cmd:= cmClose;
		keyStroke:= kbAltF3;
		help:= 1246;
		textStr:= NewStr('~C~lose');
		helpStr:= NewStr('Close the active window');
	End;

{- help menu }
	With cmds[cmdHelpMenu] Do Begin
		cmd:= cmHelpMenu;
		keyStroke:= kbNokey;
		help:= 1260;
		textStr:= NewStr('~H~elp');
		helpStr:= NewStr('Online Help System');
	End;
	With cmds[cmdHelpContents] Do Begin
		cmd:= cmHelpContents;
		keyStroke:= kbNoKey;
		help:= 1261;
		textStr:= NewStr('~C~ontents');
		helpStr:= NewStr('Show table of contents for online help');
	End;
	With cmds[cmdHelpIndex] Do Begin
		cmd:= cmHelpIndex;
		keyStroke:= kbNoKey;
		help:= 1262;
		textStr:= NewStr('~I~ndex');
		helpStr:= NewStr('Show index for online help');
	End;
	With cmds[cmdAbout] Do Begin
		cmd:= cmAbout;
		keyStroke:= kbNoKey;
		help:= 1263;
		textStr:= NewStr('~A~bout');
		helpStr:= NewStr('Show version and copyright information');
	End;

{- miscellaneous commands }
	With cmds[cmdUpdate] Do Begin
		cmd:= cmUpdate;
		keyStroke:= kbNoKey;
		help:= 1270;
		textStr:= NewStr('~U~pdate');
		helpStr:= NewStr('Save all of your current settings to disk');
	End;
	With cmds[cmdQuit] Do Begin
		cmd:= cmQuit;
		keyStroke:= kbAltX;
		help:= 1271;
		textStr:= NewStr('E~x~it');
		helpStr:= NewStr('Exit the program');
	End;
	With cmds[cmdMenu] Do Begin
		cmd:= cmMenu;
		keyStroke:= kbF10;
		help:= 1272;
		textStr:= NewStr('Menu');
		helpStr:= NewStr('Activate the menu bar');
	End;
	With cmds[cmdHelpTopic] Do Begin
		cmd:= cmHelp;
		keyStroke:= kbF1;
		help:= 1273;
		textStr:= NewStr('Help');
      helpStr:= NewStr('Get help on current topic');
	End;
	SetKeyStrings;
End;

Procedure KillCmds;
Var
	k		: Integer;
Begin
	For k:= 1 To MaxCommands Do Begin
		DisposeStr(cmds[k].textStr);
		DisposeStr(cmds[k].helpStr);
	End;
End;

Procedure ResetDefaultCmds;
Begin
	KillCmds;
	SetDefaultCmds;
End;

Function IsSpecialKey(key: Word): Boolean;
{- return true if key is anything other than a normal ascii key }
Begin
	KeyName(key);
	IsSpecialKey:= specialKey;
End;

Function KeyName(key: Word): KeyStrType;
{- return the text representation of key and sets global [specialKey] if
	key is anything other than a regular ASCII key }
Begin
	SpecialKey:= True;		{- assume special key }
	If key = kbNokey Then
		KeyName:= ''
	Else
		Case key of
			kbAltA			: KeyName:= 'Alt-A';
			kbAltB			: KeyName:= 'Alt-B';
			kbAltC			: KeyName:= 'Alt-C';
			kbAltD			: KeyName:= 'Alt-D';
			kbAltE			: KeyName:= 'Alt-E';
			kbAltF			: KeyName:= 'Alt-F';
			kbAltG			: KeyName:= 'Alt-G';
			kbAltH			: KeyName:= 'Alt-H';
			kbAltI			: KeyName:= 'Alt-I';
			kbAltJ			: KeyName:= 'Alt-J';
			kbAltK			: KeyName:= 'Alt-K';
			kbAltL			: KeyName:= 'Alt-L';
			kbAltM			: KeyName:= 'Alt-M';
			kbAltN			: KeyName:= 'Alt-N';
			kbAltO			: KeyName:= 'Alt-O';
			kbAltP			: KeyName:= 'Alt-P';
			kbAltQ			: KeyName:= 'Alt-Q';
			kbAltR			: KeyName:= 'Alt-R';
			kbAltS			: KeyName:= 'Alt-S';
			kbAltT			: KeyName:= 'Alt-T';
			kbAltU			: KeyName:= 'Alt-U';
			kbAltV			: KeyName:= 'Alt-V';
			kbAltW			: KeyName:= 'Alt-W';
			kbAltX			: KeyName:= 'Alt-X';
			kbAltY			: KeyName:= 'Alt-Y';
			kbAltZ			: KeyName:= 'Alt-Z';

			kbAltEqual 		: KeyName:= 'Alt-Equal';
			kbAltMinus		: KeyName:= 'Alt-Minus';
			kbAltSpace		: KeyName:= 'Alt-Space';
			kbBack			: KeyName:= 'Backspace';
			kbCtrlBack		: KeyName:= 'Ctrl-Backspace';
			kbAltBack		: KeyName:= 'Alt-Backspace';
			kbCtrlDel		: KeyName:= 'Ctrl-Delete';
			kbCtrlEnd		: KeyName:= 'Ctrl-End';
			kbCtrlEnter		: KeyName:= 'Ctrl-Enter';
			kbCtrlHome		: KeyName:= 'Ctrl-Home';
			kbCtrlIns		: KeyName:= 'Ctrl-Insert';
			kbCtrlLeft		: KeyName:= 'Ctrl-Left';
			kbCtrlPgDn		: KeyName:= 'Ctrl-Page Down';
			kbCtrlPgUp		: KeyName:= 'Ctrl-Page Up';
			kbCtrlPrtSc		: KeyName:= 'Ctrl-PrintScreen';
			kbCtrlRight		: KeyName:= 'Ctrl-Right';
			kbDel				: KeyName:= 'Delete';
			kbDown			: KeyName:= 'Down';
			kbEnd				: KeyName:= 'End';
			kbEnter			: KeyName:= 'Enter';
			kbEsc				: KeyName:= 'Esc';
			kbGrayMinus		: KeyName:= 'Gray Minus';
			kbGrayStar		: KeyName:= 'Gray Star';
			kbHome			: KeyName:= 'Home';
			kbIns				: KeyName:= 'Insert';
			kbLeft			: KeyName:= 'Left';
			kbPgDn			: KeyName:= 'Page Down';
			kbPgUp			: KeyName:= 'Page Up';
			kbGrayPlus		: KeyName:= 'Gray Plus';
			kbRight			: KeyName:= 'Right';
			kbShiftDel		: KeyName:= 'Shift-Delete';
			kbShiftIns		: KeyName:= 'Shift-Insert';
			kbShiftTab		: KeyName:= 'Shift-Tab';
			kbTab				: KeyName:= 'Tab';
			kbUp				: KeyName:= 'Up';

			kbAlt1			: KeyName:= 'Alt-1';
			kbAlt2			: KeyName:= 'Alt-2';
			kbAlt3			: KeyName:= 'Alt-3';
			kbAlt4			: KeyName:= 'Alt-4';
			kbAlt5			: KeyName:= 'Alt-5';
			kbAlt6			: KeyName:= 'Alt-6';
			kbAlt7			: KeyName:= 'Alt-7';
			kbAlt8			: KeyName:= 'Alt-8';
			kbAlt9			: KeyName:= 'Alt-9';
			kbAlt0			: KeyName:= 'Alt-0';

			kbF1				: KeyName:= 'F1';
			kbF2				: KeyName:= 'F2';
			kbF3				: KeyName:= 'F3';
			kbF4				: KeyName:= 'F4';
			kbF5				: KeyName:= 'F5';
			kbF6				: KeyName:= 'F6';
			kbF7				: KeyName:= 'F7';
			kbF8				: KeyName:= 'F8';
			kbF9				: KeyName:= 'F9';
			kbF10				: KeyName:= 'F10';

			kbShiftF1		: KeyName:= 'Shift-F1';
			kbShiftF2		: KeyName:= 'Shift-F2';
			kbShiftF3		: KeyName:= 'Shift-F3';
			kbShiftF4		: KeyName:= 'Shift-F4';
			kbShiftF5		: KeyName:= 'Shift-F5';
			kbShiftF6		: KeyName:= 'Shift-F6';
			kbShiftF7		: KeyName:= 'Shift-F7';
			kbShiftF8		: KeyName:= 'Shift-F8';
			kbShiftF9		: KeyName:= 'Shift-F9';
			kbShiftF10		: KeyName:= 'Shift-F10';

			kbCtrlF1			: KeyName:= 'Ctrl-F1';
			kbCtrlF2			: KeyName:= 'Ctrl-F2';
			kbCtrlF3			: KeyName:= 'Ctrl-F3';
			kbCtrlF4			: KeyName:= 'Ctrl-F4';
			kbCtrlF5			: KeyName:= 'Ctrl-F5';
			kbCtrlF6			: KeyName:= 'Ctrl-F6';
			kbCtrlF7			: KeyName:= 'Ctrl-F7';
			kbCtrlF8			: KeyName:= 'Ctrl-F8';
			kbCtrlF9			: KeyName:= 'Ctrl-F9';
			kbCtrlF10		: KeyName:= 'Ctrl-F10';

			kbAltF1			: KeyName:= 'Alt-F1';
			kbAltF2			: KeyName:= 'Alt-F2';
			kbAltF3			: KeyName:= 'Alt-F3';
			kbAltF4			: KeyName:= 'Alt-F4';
			kbAltF5			: KeyName:= 'Alt-F5';
			kbAltF6			: KeyName:= 'Alt-F6';
			kbAltF7			: KeyName:= 'Alt-F7';
			kbAltF8			: KeyName:= 'Alt-F8';
			kbAltF9			: KeyName:= 'Alt-F9';
			kbAltF10			: KeyName:= 'Alt-F10';
		Else
			If (Lo(key) >= 0) and (Lo(key) <= 31) Then
				KeyName:= 'Ctrl-' + Chr(Ord('@')+Lo(key))
			Else Begin
				If Lo(key) = 32 Then
					KeyName:= 'Space'
				Else
					KeyName:= Chr(Lo(key));
				specialKey:= False;
			End;
		End;
End;

Procedure ChangeKeys;
Var
	d	: PKeyDialog;
Begin
	d:= New(PKeyDialog, Init);
	Application^.ExecuteDialog(d, Nil);
	SetKeyStrings;
	Message(Application, evBroadcast, cmRebuildMenu, Nil);
End;


{------------------------------------------------------[ TKeyDialog ]--}

Constructor TKeyDialog.Init;
Var
	p		: PView;
	d		: PKeyDialog;
	s		: PScrollBar;
	keyScrl: PKeyScroll;
	r		: TRect;
	c		: Word;
Begin
	R.Assign(0,1,80,21);
	inherited Init(r, 'Keyboard Definitions');
	flags:= flags and not wfClose;

	R.Assign(1,2,size.x-2,3);
	Insert(New(PStaticText, Init(r, Pad(' Key', KeyStrWidth+4)+'Description')));

	{- vertical scrollbar }
	R.Assign(79,3,80,15);
	s:= New(PScrollBar, Init(r));
	Insert(s);

	{- key list }
	R.Assign(1,3,79,15);
	keyList:= New(PKeyViewer, Init(r, s));
	keyList^.helpCTX:= 2102;
	Insert(keyList);

	{- horizontal scroll }
	R.Assign(21,15,76,16);
	keyScrl:= New(PKeyScroll, Init(r));
	Insert(keyScrl);
	keyScrl^.options:= keyScrl^.options or ofPreprocess;
	keyScrl^.SetParams(0, 0, 20, 5, 1);

	{- Buttons }

	R.Assign(1,17,13,19);
	p:= New(PButton2, Init(r, 'Edit', cmEdit, bfDefault));
	p^.helpCTX:= 2103;
	Insert(p);

	R.Assign(14,17,26,19);
	p:= New(PButton2, Init(r, 'Clear', cmDelete, bfNormal));
	p^.helpCTX:= 2104;
	Insert(p);

	R.Assign(27,17,39,19);
	p:= New(PButton2, Init(r, 'Search', cmSearch, bfNormal));
	p^.helpCTX:= 0;
	Insert(p);

	R.Assign(40,17,52,19);
	p:= New(PButton2, Init(r, 'Print', cmPrintKeys, bfNormal));
	p^.helpCTX:= 0;
	Insert(p);

	R.Assign(53,17,65,19);
	p:= New(PButton2, Init(r, 'Defaults', cmDefaults, bfNormal));
	p^.helpCTX:= 2105;
	Insert(p);

	R.Assign(66,17,78,19);
	p:= New(PButton2, Init(R, 'DONE', cmOk, bfNormal));
	p^.helpCTX:= 2018;
	Insert(p);

	SelectNext(False);
End;

Procedure TKeyDialog.HandleEvent(Var event: TEvent);
Var
	temp		: CmdRec;
	p			: PView;
Begin
	inherited HandleEvent(event);
	If (event.what and evCommand <> 0) Then Begin
		Case event.command of
			cmDefaults	: 	ResetDefaultCmds;
			cmEdit		:	EditCurrent;
			cmDelete		:  cmds[keyList^.focused+1].keyStroke:= kbNokey;
			cmSearch		:	DoSearch;
		Else
			Exit;
		End;
		keyList^.Draw;
		ClearEvent(event);
	End
	Else If event.what and evBroadCast <> 0 Then Begin
		Case event.command of
			cmListItemSelected:
				EditCurrent;
			cmChangeScroll:
				keyList^.ChangeDelta(PKeyScroll(event.infoPtr)^.value+1);
		End;
	End;
End;

Function TKeyDialog.FindOne(AStart: Integer; AKey: Word; ACmd, ADescr: String): Integer;
{- find an item. If AKey <> kbNokey, then search by keystroke,
	if ACmd <> '' then search by command, if ADescr <> '' then
	search by description. Search starts at AStart }
Var
	k		: Integer;
	found	: Boolean;
	tstr	: String;
	ret	: Integer;

	Function IsMatch(idx: Integer): Boolean;
	Begin
		IsMatch:= False;
		If AKey <> kbNokey Then
			IsMatch:= cmds[idx].keyStroke = AKey
		Else If ACmd <> '' Then Begin
			tstr:= StUpcase(Trim(StripControls(cmds[idx].textStr^)));
			IsMatch:= Pos(ACmd, tstr) = 1;
		End
		Else If ADescr <> '' Then Begin
			tstr:= StUpcase(Trim(cmds[idx].helpStr^));
			IsMatch:= Pos(ADescr, tstr) > 0;
		End;
	End;

Begin
	ret:= -1;
	k:= AStart + 1;
	ACmd:= StUpCase(ACmd);
	ADescr:= StUpCase(ADescr);
	While (k <> AStart) and (ret = -1) Do Begin
		If IsMatch(k) Then
			ret:= k;
		Inc(k);
		If k >= keyList^.Range Then
			k:= 1;
	End;
	If ret = -1 Then Begin  {- check AStart to see if it matches cuz it wasnt}
		If IsMatch(AStart) Then	{- checked in previous loop }
			ret:= AStart;
	End;

	FindOne:= ret;
End;

Procedure TKeyDialog.DoSearch;
Const
	ADStr	: String[60] = '';
	ACStr	: String[27] = '';
Var
	d		: PDotDialog;
	cmd	: Word;
	key	: Word;
	tstr	: String;
	anew	: Integer;
	cur	: Integer;
Begin
	d:= New(PKeySearch, Init);
	cmd:= Application^.ExecuteDialog(d, Nil);
	If cmd <> cmCancel Then Begin
		cur:= keyList^.focused + 1;
		aNew:= -2;
		tstr:= '';
		Case cmd of
			cmSrchDescription:
				If inpString('Search by Description', ADStr, 60) Then
					anew:= FindOne(cur, kbNokey, '', ADStr);
			cmSrchCommand:
				If inpString('Search by Command', ACStr, 27) Then
						anew:= FindOne(cur, kbNokey, ACStr, '');
		End;
		If aNew > 0 Then Begin
			keyList^.FocusItem(Pred(ANew));
			keyList^.Draw;
		End
		Else If aNew = -1 Then
			Msg('Item not found', '', Nil, mfError, mfOK);
	End;
End;

Function TKeyDialog.CheckKey(Var key: Word; cur: Integer): Boolean;
Var
	ok		: Boolean;
	c		: Word;
	k		: Integer;
Begin
	ok:= True;	{- assume ok }
	If not IsSpecialKey(key) Then
		key:= KeyUpcase(key);

	{- check to see if key is a special key that cannot be redefined }
	If ok and ProtectedKey(key) Then Begin
		Msg('The [' + KeyName(key) + '] key cannot be redefined.','', Nil,
				mfError, mfOK);
		ok:= False;
	End;

	{- check to see if key is already being used }
	If ok Then Begin
		k:= 1;
		While (k <= LastChangeable) Do Begin
			If (k <> cur) and (cmds[k].keyStroke = key) and
				(cmds[k].keyStroke <> kbNokey) Then Begin
				c:= Msg('['+KeyName(key)+
						'] is currently being used by : ' +
							Trim(StripControls(cmds[k].textStr^)),
						'Do you wish to overwrite the current definition ?', Nil,
						mfConfirm, mfYes or mfNo);
				ok:= c = cmYes;
				If ok Then
					cmds[k].keyStroke:= kbNokey;
				k:= LastChangeable + 1;
			End;
			Inc(k);
		End;
	End;

	{- check to see if key is a menubar key }
	If ok and MenuBarHotKey(key) Then Begin
		c:= Msg(KeyName(key) + ' is used internally to activate the menubar.',
				'If you redefine this key, it can''t be used to activate the menubar. '+
				'Do you still wish to redefine this key ?', Nil,
				mfConfirm, mfYes or mfNo);

		ok:= c = cmYes;
		If ok Then Begin
			k:= 1;
			While k <= menuOverrides Do Begin
				{- clear any existing override definitions for cur cmd }
				If defaults.menuoverride[k].newCmd = cmds[cur].cmd Then
					defaults.menuoverride[k].newCmd:= 0;

				{- redefine override }
				If defaults.menuOverride[k].keystroke = key Then Begin
					defaults.menuoverride[k].newCmd:= cmds[cur].cmd;
				End;
				Inc(k);
			End;
		End;
	End;

	CheckKey:= ok;
End;

Procedure TKeyDialog.EditCurrent;
Var
	p		: PStaticText;
	p2		: PStaticHighlight;
	event	: TEvent;
	r		: TRect;
	cur	: Integer;
Begin
	cur:= keyList^.focused + 1;
	GetExtent(r);
	r.Grow(-1, -1);
	r.a.y:= r.b.y - 4;
	p:= New(PStaticText, Init(r,
		^M'  Press a new hot key for the selected item :'+
		^M'  Press [   ] to cancel.'
		));
	p^.SetState(sfFocused, True);
	p^.ShowCursor;
	p^.SetCursor(46,1);
	Insert(p);

	Inc(r.a.y,2);
	r.b.y:= r.a.y + 1;
	r.a.x:= r.a.x + 9;
	r.b.x:= r.a.x + 3;
	p2:= New(PStaticHighlight, Init(r, 'Esc', jNone));
	Insert(p2);
	Repeat
{		StatusLine^.Update;}
		GetMouseEvent(event);
		If event.what and evMouse <> 0 Then Begin
			If (p2^.MouseInView(event.Where)) and
				(event.what and evMouseDown <> 0) Then
				event.what:= evKeyDown
			Else
				ClearEvent(event);
		End
		Else Begin
			GetKeyEvent(event);
			If (event.what and evKeyDown <> 0) and (event.keyCode <> kbEsc) Then Begin
				If not CheckKey(event.keyCode, cur) Then
					ClearEvent(event)
				Else Begin
					cmds[cur].keyStroke:= event.keyCode;
					keyList^.Draw;
				End;
				p^.ShowCursor;
			End;
		End;
	Until (event.what and evKeyDown <> 0);
	ClearEvent(event);
	p^.HideCursor;
	Dispose(p2, Done);
	Dispose(p, Done);
End;

{-------------------------------------------------------[ TKeyViewer ]--}

Procedure TKeyViewer.HandleEvent(Var event: TEvent);
Var
	k		: Integer;
	found	: Boolean;
	c		: Word;
Begin
	If (event.what and evKeyDown <> 0) and not (IsSpecialKey(event.keyCode)) Then
		event.keyCode:= KeyUpCase(event.keyCode);

	If (event.What and evKeyDown <> 0) or
		((event.what and evCommand <> 0) and
			((event.Command = cmResize) or (event.command = cmClose) or
				(event.Command = cmQuit))) Then Begin
		If event.what and evCommand <> 0 Then Begin
			event.what:= evKeyDown;
			Case event.command of
				cmClose	:	event.keyCode:= cmds[cmdClose].keyStroke;
				cmResize	:	event.keycode:= cmds[cmdResize].keystroke;
				cmQuit	:	event.keyCode:= cmds[cmdQuit].keyStroke;
			End;
		End;

		k:= 1;
		found:= False;
		While not found and (k <= LastChangeable) Do Begin
			found:= cmds[k].keyStroke = event.keyCode;
			If not found Then
				Inc(k);
		End;
		If found Then Begin
			FocusItem(Pred(k));
			Draw;
			ClearEvent(event);
		End;
	End;

	inherited HandleEvent(event);
End;

Procedure TKeyViewer.ChangeDelta(newOfs: Integer);
Begin
	dispDX:= newOfs;
	Draw;
End;

Function TKeyViewer.GetText(item: Integer; maxLen: Integer): String;
Var
	tstr	: String[80];
	tstr2	: String[80];
Begin
	Inc(item);
	tstr:= KeyName(cmds[item].KeyStroke);
	If tstr = '' Then
		tstr:= '---';
	If cmds[item].helpStr <> Nil Then
		tstr2:= Copy(cmds[item].helpStr^, dispDX, 255)
	Else
		tstr2:= 'Item: ' + Long2Str(item) + ' has no HELP available';
	tstr:= Pad(tstr, KeyStrWidth) + ' � ' + tstr2;
	GetText:= Pad(tstr, maxLen);
End;

Constructor TKeyViewer.Init(bounds: TRect; VBar: PScrollBar);
Begin
	TListViewer.Init(bounds, 1, Nil, VBar);
	dispDX:= 1;
	SetRange(LastChangeAble);
End;

{-------------------------------------------------------[ TKeyScroll ]--}

Procedure TKeyScroll.HandleEvent(Var event: TEvent);
Var
	temp	: Integer;
Begin
	If (event.What and evKeyDown <> 0) Then
		If (event.keyCode = kbCtrlLeft) or (event.keyCode = kbCtrlRight) or
			(CtrlToArrow(event.keyCode) <> event.keyCode) Then
			Exit;
	temp:= value;
	TScrollBar.HandleEvent(event);
	If temp <> value Then
		Message(Owner, evBroadCast, cmChangeScroll, @Self);
End;

{-------------------------------------------------------[ TKeySearch ]--}

Constructor TKeySearch.Init;
Var
	r			: TRect;
	control	: PView;
Begin
	R.Assign(0,0,55,11);
	inherited Init(r, 'Search');

	R.Assign(4,2,14,3);
	Control := New(PStaticText, Init(R, 'Search by:'));
	Insert(Control);

	R.Assign(3,4,18,6);
	Control := New(PButton2, Init(R, '~D~escription', cmSrchDescription, bfNormal));
	Insert(Control);

	R.Assign(19,4,34,6);
	Control:= New(PButton2, Init(R, 'C~o~mmand', cmSrchCommand, bfNormal));
	Insert(Control);

	R.Assign(40,4,52,6);
	Control := New(PButton2, Init(R, '~C~ancel', cmCancel, bfNormal));
	Insert(Control);

	R.Assign(2,7,53,10);
	Control:= New(PStaticText, Init(R,
					'Note:  You can search by defined keystrokes by'^M+
					'       pressing the keystroke while in the key '^M+
					'       definition list.'));
	Insert(Control);

	R.Assign(1,6,54,7);
	Control := New(PStaticText, Init(R, CharStr('�', size.x)));
	Insert(Control);

	SelectNext(False);
End;

Procedure TKeySearch.HandleEvent(Var event: TEvent);
Begin
	inherited HandleEvent(event);
	If event.what = evCommand Then Begin
		Case event.command of
			cmSrchCommand, cmSrchDescription:
				If State and sfModal <> 0 Then Begin
					EndModal(Event.Command);
					ClearEvent(Event);
				End;
		End;
	End;
End;

Procedure ExitRoutine; Far;
{- free up memory on exit }
Begin
	ExitProc:= exitSave;
	KillCmds;
End;

BEGIN
	exitSave:= exitProc;
	exitProc:= @ExitRoutine;
	SetDefaultCmds;
END.
