Uses
	Dos,
	Objects;

{- this demo is used to calculate how long it takes to find an item in
	a collection }

Const
	maxnum	= MaxCollectionSize;

Type
	PRec = ^TRec;
	TRec = Object(TObject)
   	s	: PString;
   	constructor Init(AStr: String);
      destructor Done; virtual;
   End;


Constructor TRec.Init(AStr: String);
Begin
	inherited Init;
   s:= NewStr(AStr);
End;

Destructor TRec.Done;
Begin
   DisposeStr(s);
	inherited Done;
End;

Var
	list	: PCollection;
	k		: Integer;
	tstr	: String[80];

Procedure FindItems;

Var
   r		: PRec;
	astr	: String;
	h1,m1,s1,s100, h2,m2,s2,s200: word;

	Function Matches(Item: Pointer): Boolean; far;
	Begin
		Matches:= PRec(item)^.s^ = astr;
   End;

Begin
	Repeat
   	write('Enter number to find: ');
      readln(astr);
      If astr <> '' Then Begin
			GetTime(h1, m1, s1, s100);
			r:= list^.FirstThat(@Matches);
			GetTime(H2, m2, s2, s200);
         If r <> Nil Then Begin
         	write('Number found  : ',r^.s^, ' at ', list^.indexOf(r));
            writeln('  Start: ', s1, '  End: ', s2);
         End
         Else
         	writeln('Number not found.');
      End;
	Until astr = '';
End;


Begin
	writeln('Start memory: ', MemAvail);
	list:= New(PCollection, Init(maxnum, 100));
   For k:= 1 To maxnum Do Begin
   	Str(k, tstr);
   	list^.Insert(New(PRec, Init(tstr)));
   End;
   writeln('Done building list');
   writeln('Number of items in list: ', list^.count);

   FindItems;

	Dispose(list, Done);

   writeln('Ending memory: ', Memavail);
   readln;
End.