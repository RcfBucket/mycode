uses
  strsw,
  wincrt,
	misclib;
var
  l : longint;
  c : char;
  bit : word;

procedure Dump(num: longint);
var
  k  : integer;
begin
  for k:= 15 downto 0 do
    if testbit(num, k) then
      write('1')
    else
      write('0');
  write('   ', num);
  writeln;
end;

begin
  l:= 0;
  repeat
    dump(l);
    writeln;
    write('Test Set Clear toGgle  Quit : ');
    c:= upcase(readkey);
    writeln;
    if c in ['T','C','S','G'] then
    begin
      write('Enter bit: ');
      readln(bit);
    end;
    case c of
      'T'  : begin writeln; writeln('Test bit ', bit, '  ', TestBit(l, bit)); writeln; end;
      'S'  : SetBit(l, bit);
      'C'  : ClearBit(l, bit);
      'G'  : ToggleBit(l, bit);
    end;
  until c = 'Q';
  DoneWinCrt;
end.
