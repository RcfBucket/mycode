(*****************************************************************************)
(*                                                                           *)
(*    Turbo Pascal 5.0 - 6.0                                                 *)
(*    Quick Pascal 1.0                                                       *)
(*    B-Tree Isam Base support routines                                      *)
(*    Copyright (C) 1987-1991                                                *)
(*    Dipl.Math. Ralf Nagel                                                  *)
(*    Copyright (C) Enz EDV-Beratung GmbH 1989-1991                          *)
(*    Ver 5.23                                                               *)
(*                                                                           *)
(*****************************************************************************)

{--Conditional defines}
  {$I BTDefine.Inc}     {General user defined symbols}

{--Compiler options}
  {$I IsamOpt.Inc}      {User defined options}


Unit BaseSupp;

InterFace

Uses
{$IFDEF TurboWindowsUsed}
  WinDos;
{$ELSE}
  Dos;
{$ENDIF}

Type
  GenRegisters = Record
    Case Integer Of
      0: (AX, BX, CX, DX, BP, SI, DI, DS, ES, Flags: Word);
      1: (AL, AH, BL, BH, CL, CH, DL, DH: Byte);
  End;

Var
  AdrMsDos,
  AdrIntr,
  AdrDosVersion,
  AdrSetIntVec,
  AdrGetIntVec,
  AdrGetTime,
  AdrGetDate    : Pointer;


  Procedure CallMsDos ( Var Regs : GenRegisters ); Inline
      ( $FF / $1E / AdrMsDos ); {call far dword ptr [AdrMsDos]}
    {-Call MsDos via AdrMsDos and allows to use type GenRegisters}


  Procedure CallIntr ( IntNo : Byte; Var Regs : GenRegisters ); Inline
      ( $FF / $1E / AdrIntr ); {call far dword ptr [AdrIntr]}
    {-Call Intr via AdrMsDos and allows to use type GenRegisters}


  Function CallDosVersion : Word; Inline
      ( $FF / $1E / AdrDosVersion ); {call far dword ptr [AdrDosVersion]}
    {-Call DosVersion via AdrDosVersion}


  Procedure CallSetIntVec ( IntNo : Byte; Vector : Pointer ); Inline
      ( $FF / $1E / AdrSetIntVec ); {call far dword ptr [AdrSetIntVec]}
    {-Call SetIntVec via AdrSetIntVec}


  Procedure CallGetIntVec ( IntNo : Byte; Var Vector : Pointer ); Inline
      ( $FF / $1E / AdrGetIntVec ); {call far dword ptr [AdrGetIntVec]}
    {-Call GetIntVec via AdrGetIntVec}


  Procedure CallGetTime ( Var Hour, Minute, Second, Sec100 : Word ); Inline
      ( $FF / $1E / AdrGetTime ); {call far dword ptr [AdrGetTime]}
    {-Call GetTime via AdrGetTime}


  Procedure CallGetDate ( Var Year, Month, Day, DayOfWeek : Word ); Inline
      ( $FF / $1E / AdrGetDate ); {call far dword ptr [AdrGetDate]}
    {-Call GetDate via AdrGetDate}


  Procedure DefaultRegisters ( Var Regs : GenRegisters );


Implementation

  Procedure DefaultRegisters ( Var Regs : GenRegisters );

  Begin
    Regs.DS := DSeg;
    Regs.ES := DSeg;
  End;


Begin
  AdrMsDos := @MsDos;
  AdrIntr := @Intr;
  AdrDosVersion := @DosVersion;
  AdrSetIntVec := @SetIntVec;
  AdrGetIntVec := @GetIntVec;
  AdrGetTime := @GetTime;
  AdrGetDate := @GetDate;
End.
