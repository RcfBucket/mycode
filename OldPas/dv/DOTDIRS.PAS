{$A+,B-,E+,F+,G-,I-,N-,O+,R-,S-,V-,X+}
{$D+,L+}

Unit DotDirs;

INTERFACE

Uses
  DotMem,
  Dos,
  DotDrivs,
  DotWild,
  DotData,
  DotMisc,
  DotError,
  DotRange,
  Drivers,
  Strs,
  app,
  views,
  objects;

Const
  flMarked      = $0001;
  flDeleted    = $0002;

  opChangeAttrs  = 1;
  opCopyFiles    = 2;
  opDeleteFiles  = 3;
  opMoveFiles    = 4;

Type
  PPathRec = ^PathRec;
  PathRec = Record
    pName  : PString;
    next  : PPathRec;
  End;

  PFileRec = ^FileRec;
  FileRec = Record      {- make size a multiple of 8 for efficiency }
    fname  : String[8];
    ext   : String[3];
    attr  : byte;
    fDate  : Word;
    fTime  : Word;
    size  : longint;
    flags  : Word;      {- Bit    Meaning }
                      {-  0     Marked  }
                      {-  1     Deleted (flag to delete from list) }
    PPath  : PPathRec;
    next  : PFileRec;
  End;

  FileKeyFunc = Procedure(f1, f2: PFileRec; Var s1, s2: String);

  PFiles = ^TFiles;
  TFiles = object(TSortedCollection)
    masks       : MaskArray;
    fileHead,
    fileTail    : PFileRec;
    pathHead,
    pathTail    : PPathRec;
    totFiles,
    totDirs,
    totbytes    : longint;
    mFiles,
    mbytes,
    mDirs       : longint;
    sort1,                 {- sort key 1 }
    sort2       : byte;    {- sort key 2 }
    sortOrder   : byte;    {- ascending(0)/descending(1) }
    sortOpts    : byte;    {- group by path }
    allDirs     : boolean; {- true if entire tree is read }
    {}
    FileKey1    : FileKeyFunc;  {- routine to extract file keys from list }
    FileKey2    : FileKeyFunc;  {- routine to extract file keys from list }
    {}
    constructor Init;
    procedure ReadDirectory(readPath: String; AMasks: MaskArray; recurs: boolean);
    procedure GetAFile(index: Word; Var fRec: PFileRec);
    function GetAPath(index: Word): String;
    procedure MarkItem(itemNum: Integer);
    procedure ChangeSort(dsort1, dsort2, dorder, dopts: byte);
    destructor Done; virtual;
    procedure Marks(cmd: Integer);
    procedure MarkUnMarkWild(mStr: String; markOn, inclDirs: boolean);
    procedure CalcMarkedbytes;
    procedure MarkByRange(Var ARange: MarkRangeRec; markOn: boolean);
    procedure PerformOp(op: Integer; iNum: Word; marked: boolean; Var p1, p2);
    PRIVATE
    procedure DoOperation(op: Integer; p: PFileRec; Var p1,p2);
    procedure ToggleTheMark(Var item: PFileRec);
    function Compare(rec1, rec2: Pointer): Integer; virtual;
    procedure BuildList;
  End;

Function GetFileName(p: PFileRec): String;

IMPLEMENTATION

Var
  DoAll        : boolean;
  abort        : boolean;
  tstr1, tstr2  : String[100];

{- Global working vars. These are only valid just after a call to ReadDir.
  after that they are undefined }
Var
  fRec    : SearchRec;
  pTail    : PPathRec;
  pHead    : PPathRec;
  fTail    : PFileRec;
  fHead    : PFileRec;
  memError  : boolean;
  doSubDirs: boolean;
  numDirs  : longint;
  numFiles  : longint;
  numbytes  : longint;
  mList    : MaskArray;

Function GetFileName(p: PFileRec): String;
{- return a file name from p }
Var
  tstr  : String[50];
Begin
  If p <> Nil Then Begin
    tstr:= p^.fName;
    If p^.ext <> '' Then
      tstr:= tstr + '.' + p^.ext;
  End
  Else
    tstr:= '';
  GetFileName:= tstr;
End;

Procedure InitReadDir(recurs: boolean; masks: MaskArray);
{- initialize globals variables for ReadDirectory }
Begin
  pTail:= Nil;
  pHead:= Nil;
  fTail:= Nil;
  fHead:= Nil;
  memError:= False;
  doSubdirs:= recurs;
  numDirs  := 0;
  numFiles  := 0;
  numbytes  := 0;
  mList:= masks;
End;

Procedure SplitFilename(fullName: String; Var fname, ext: String);
{- split fname into [fname].[ext] and also padding for program }
Var
  dir  : PathStr;
Begin
  fSplit(fullname, dir, fname, ext);
  If ext[1] = '.' Then
    Delete(ext, 1, 1);
End;

Procedure AddDirEntry(var drec   : SearchRec;
                      var fHead  : PFileRec;
                          path   : PPathRec);
{- add a directory entry to the list }
Var
  aux  : PFileRec;
  k    : byte;
  tstr  : String[1];
  tstr2  : String[4];
Begin
  If GetMemCheck(aux, SizeOf(FileRec)) Then Begin
    With aux^ Do Begin
      flags:= 0;    {- no flags set }

      size:= dRec.size;
      attr:= dRec.attr;
      fTime:= dRec.time and $FFFF;
      fDate:= (dRec.time Shr 16) and $FFFF;

      fSplit(drec.name, tstr, fname, tstr2);
      If tstr2[1] = '.' Then
        Delete(tstr2, 1, 1);
      ext:= tstr2;
      pPath:= path;
      next:= Nil;

      If fHead = Nil Then Begin
        fHead:= aux;
        fTail:= fHead;
      End
      Else Begin
        fTail^.next:= aux;
        fTail:= aux;
      End;
    End;
  End
  Else Begin
    memError:= True;
  End;
End;

Function AddToPathList(path: PathStr; Var pHead: PPathRec): boolean;
{- add a path to the path list. Point global [pTail] to the one added }
Var
  aux  : PPathRec;
Begin
  If GetMemCheck(aux, SizeOf(PathRec)) Then Begin
    aux^.pname:= NewStr(StUpCase(path));
    aux^.next:= Nil;
    If phead = Nil Then Begin
      pHead:= aux;
      pTail:= pHead;
    End
    Else Begin
      pTail^.next:= aux;
      pTail:= aux;
    End;
    AddToPathList:= True;
  End
  Else Begin
    AddToPathList:= False;
    memError:= True;
  End;
End;

Function FileNameFromRec(rec: PFileRec): String;
{- return a valid file name from rec }
Var
  tstr  : String[15];
Begin
  tstr:= rec^.fname;
  If rec^.ext <> '' Then
    tstr:= tstr + '.' + rec^.ext;
  FileNameFromRec:= tstr;
End;

Function GoodFile(name: String; attr: byte): boolean;
Var
  found    : boolean;
  k      : Integer;
Begin
  If not (attr and VolumeId <> 0) and (name[1] <> '.') Then Begin
    If mList[0].numMasks = 0 Then
      GoodFile:= True
    Else Begin
      {- if a directory or no masks then its a goodfile }
      found:= (attr and Directory <> 0) or (mList[0].numMasks = 0);
      k:= 1;
      While (k <= mList[0].numMasks) and not found Do Begin
        found:= IsMatch(mList[k].mask, name);
        Inc(k);
      End;
      GoodFile:= found;
    End;
  End
  Else
    GoodFile:= False;
End;

Procedure ReadTheDir(path: PathStr);
{- this routine will read the directory starting at path.
  It uses the global variables [doReadSubDirs] and [currentDir].
  If doReadSubDirs = True then subdirectories will be read also.
  [path] is assumed to be valid and have a "\" appended at the end.
  It is also assumed that an InitReadDir has been performed.

  The global variables fHead, pHead, numDirs etc will contain all required
  data after procedure is finished }

Var
  fstart,
  fend    : PFileRec;
Begin
  fStart:= Nil;
  If AddToPathList(path, pHead) Then Begin
    FindFirst(path + starDotStar, AnyFile, frec);
    While (DosError = 0) and not MemError Do Begin
      If GoodFile(frec.name, frec.Attr) Then Begin
        AddDirEntry(frec, fHead, pTail);
        If not MemError Then Begin
          If (frec.attr and Directory <> 0) Then Begin
            Inc(numDirs);
          End
          Else Begin
            Inc(numbytes, frec.size);
            Inc(numFiles);
          End;
        End;
        If fstart = Nil Then
          fstart:= fTail;
      End;
      FindNext(frec);
    End;

    fEnd:= fTail;

    If doSubDirs and not MemError Then Begin
      While (fStart <> Nil) and (fStart <> fEnd) and not MemError Do Begin
        If fStart^.attr and Directory <> 0 Then
          ReadTheDir(AddBackSlash( path + FileNameFromRec(fStart)));
        fStart:= fStart^.next;
      End;

      {- read last dir in list (if any) }
      If (fStart <> Nil) and (fStart^.attr and Directory <> 0) and
        not MemError Then
          ReadTheDir(AddBackSlash( path + FileNameFromRec(fStart)));
    End;
  End
  Else  {- memory error }
    Exit;
End;

Procedure PurgeFileList(Var f: PFileRec; Var p: PPathRec);
{- purge all memory used by the file list [f] and the path list [p] }
Var
  aux1    : PFileRec;
  aux2    : PPathRec;
Begin
  {- free up path list }
  While p <> Nil Do Begin
    aux2:= p;
    p:= p^.next;
    DisposeStr(aux2^.pName);
    FreeMemCheck(aux2, SizeOf(PathRec));
  End;
  p:= Nil;
  While f <> Nil Do Begin
    aux1:= f;
    f:= f^.next;
    FreeMemCheck(aux1, SizeOf(FileRec));
  End;
End;

{--------------------------------------------------------------[ TFiles ]--}

Procedure UnsortedKeys(rec1, rec2: PFileRec; Var s1, s2: String);
{- rec1 is always less than rec2 }
Begin
  s2:= #1;
  s1:= #0;
End;

Procedure FileNameKeys(rec1, rec2: PFileRec; Var s1, s2: String);
Begin
  s1:= Pad(rec1^.fname,8);
  s2:= Pad(rec2^.fname,8);
End;

Procedure ExtKeys(rec1, rec2: PFileRec; Var s1, s2: String);
Begin
  s1:= Pad(rec1^.ext,3);
  s2:= Pad(rec2^.ext,3);
End;

Procedure StoreLong(k: longint; Var numStr: String); Assembler;
ASM
  les    di,numStr
  mov    al,4
  cld
  stosb
  mov    ax, word ptr k+2
  xchg    al,ah
  stosw
  mov    ax, word ptr k
  xchg    al,ah
  stosw
END;

Procedure SizeKeys(rec1, rec2: PFileRec; Var s1, s2: String);
Begin
  StoreLong(rec1^.size, s1);
  StoreLong(rec2^.size, s2);
End;

Procedure TimeKeys(rec1, rec2: PFileRec; Var s1, s2: String);
Begin
  StoreLong(rec1^.ftime, s1);
  StoreLong(rec2^.ftime, s2);
End;

Procedure DateKeys(rec1, rec2: PFileRec; Var s1, s2: String);
Begin
  StoreLong(rec1^.fDate, s1);
  StoreLong(rec2^.fDate, s2);
End;

Procedure MarkedKeys(rec1, rec2: PFileRec; Var s1, s2: String);
Begin
  s1:= Chr(Ord(rec1^.flags and flMarked = 0));
  s2:= Chr(Ord(rec2^.flags and flMarked = 0));
End;

Procedure SetSortKey(Var keyFunc: FileKeyFunc; sortType: byte);
{- assign [keyFunc] function to the proper `GetKey' routine for sortType }
Begin
  Case sortType of
    dfltSortFiles  :  keyFunc:= FileNameKeys;
    dfltSortExt    :  keyFunc:= ExtKeys;
    dfltSortSize  :  keyFunc:= SizeKeys;
    dfltSortDate  :  keyFunc:= DateKeys;
    dfltSortTime  :  keyFunc:= TimeKeys;
    dfltSortMarked  :  keyFunc:= MarkedKeys;
  Else
    keyFunc:= UnsortedKeys;
  End;
End;

Procedure TFiles.BuildList;
{- build sorted collection from fileHead. Does not remove any current items
  from the collection list }
Var
  aux  : PFileRec;
  p    : PStatusMsg;
Begin
  p:= New(PStatusMsg, Init('Sorting ... '));
  Application^.Insert(p);

  DeleteAll;    {- clear current collection }
  aux:= fileHead;
  While aux <> Nil Do Begin
    If not allDirs or (allDirs and (aux^.Attr and Directory = 0)) Then
      Insert(aux);
    aux:= aux^.next;
  End;
  Dispose(p, Done);
End;

Procedure TFiles.ChangeSort(dsort1, dsort2, dorder, dopts: byte);
Var
  aux  : PFileRec;
Begin
  sort1:= dsort1;
  sort2:= dsort2;
  sortOrder:= dorder;
  sortOpts:= dOpts;

  SetSortKey(FileKey1, sort1);
  SetSortKey(FileKey2, sort2);

  BuildList;  {- rebuild collection }
End;

Constructor TFiles.Init;
{- initialize a file list. Path is the initial path to read. Masks is a
  string of AMasks ('mask1, mask2, etc') all separated by commas that are
  valid. recurs is True if all subdirectories are to be read }
Begin
  TSortedCollection.Init(200, 50);

  duplicates:= True;    {- always set to true! }

  {- assign global working vars to TFiles object }
  FillChar(masks, SizeOf(masks), #0);
  fileHead:= Nil;
  fileTail:= Nil;
  pathHead:= Nil;
  pathTail:= Nil;
  totFiles:= 0;
  totDirs:= 0;
  totbytes:= 0;
  mFiles:= 0;
  mbytes:= 0;
  mDirs:= 0;
  allDirs:= False;

  {- set up sort stuff }
  sort1:= defaults.dSortType1;
  sort2:= defaults.dSortType2;
  sortOrder:= defaults.dSortOrder;
  sortOpts:= defaults.dSortOpts;
  SetSortKey(FileKey1, sort1);
  SetSortKey(FileKey2, sort2);
End;

Destructor TFiles.Done;
Begin
  PurgeFileList(fileHead, pathHead);
  DeleteAll;
  TSortedCollection.Done;
End;

Function TFiles.Compare(rec1, rec2: Pointer): Integer;
Const
  key1  : String[100] = '';
  key2  : String[100] = '';
  k1    : Pointer = @key1;
  k2    : Pointer = @key2;
Var
  r1    : PFileRec absolute rec1;
  r2    : PFileRec absolute rec2;
  is1  : boolean;
  is2  : boolean;
Begin
  is1:= r1^.attr and Directory <> 0;
  is2:= r2^.attr and Directory <> 0;
  If (is1 and not is2) Then
    Compare:= -1
  Else If is2 and not is1 Then
    Compare:= 1
  Else Begin
    FileKey1(r1, r2, key1, key2);
    If sort2 <> dfltNoSort Then Begin
      FileKey2(r1, r2, tstr1, tstr2);
      key1:= key1 + tstr1;
      key2:= key2 + tstr2;
    End;
    If (allDirs) and (sortOpts and dfltSortGroup <> 0) Then Begin
      key1:= Pad(r1^.ppath^.pName^,80) + key1;
      key2:= Pad(r2^.ppath^.pName^, 80) + key2;
    End;
    ASM
      PUSH    DS
      CLD
      LDS    SI,k1
      LES    DI,k2
      LODSB
      MOV    AH,ES:[DI]
      INC    DI
      MOV    CL,AL
      CMP    CL,AH
      JBE    @@1
      MOV    CL,AH
    @@1:
      XOR    CH,CH
      REP    CMPSB
      JE      @@2
      MOV    AL,DS:[SI-1]
      MOV    AH,ES:[DI-1]
      POP    DS
    @@2:
      cmp    al,ah
      je      @@4
      jb      @@3
      mov    ax,1
      jmp    @@5
    @@3:
      mov    ax,-1
      jmp    @@5
    @@4:
      xor    ax,ax      {- AX <= 0 }
      jmp    @@6
    @@5:
      les    di,self
      cmp    es:byte ptr sortorder[di],0
      je      @@6
      not    ax
    @@6:
      mov    @Result,ax
    END;
  End;
End;

Procedure TFiles.GetAFile(index: Word; Var frec: PFileRec);
{- return the [index'th] file }
Begin
  fRec:= At(index);
End;

Function TFiles.GetAPath(index: Word): String;
{- return the [index`th] file`s path }
Begin
  GetAPath:= PFileRec(At(index))^.ppath^.pname^;
End;

Procedure TFiles.ToggleTheMark(Var item: PFileRec);
{- toggle mark of an item in file list }
Var
  marked: boolean;    {- current marked status }
  markIt: boolean;    {- T=Mark, F=Unmark }
  dx    : Integer;
Begin
  With item^ Do Begin
    {- This next little bit will calculate the delta (dx) value to add
      to the marked totals. It works like this...
      marked is setup to be the current marked state (P)
      markit is the new marked state (N).
           (P)    (N)    (N)-(P)   Delta (dx)
           ---    ---    -------   -----------
           T=1    T=1     1 - 1  =   0
                F=0     0 - 1  =   -1
           F=0    T=1     1 - 0  =   1
                F=0     0 - 0  =   0

         This produces the following results:
          If (P) and (N) Then         dont need to adjust totals  (nochange)
          If (P) and not (N) Then     decrement totals            (unmark  )
          If not (P) and (N) Then     increment totals            (mark    )
          If not (P) and not (N) Then dont need to adjust totals  (nochange)
      }
    marked:= (flags and flMarked <> 0);
    markIt:= not marked;
    dx:= Integer(markIt) - Integer(marked);

    {- set the flags field (dont change other flag bits --> $FE) }
    flags:= (flags and $FE) or Integer(markIt);

    {- adjust the marked totals }
    If attr and Directory <> 0 Then Begin
      Inc(mDirs, dx);
    End
    Else Begin
      Inc(mFiles, dx);
      Inc(mbytes, longint(dx) * size);
    End;
  End;
End;

Procedure TFiles.MarkItem(itemNum: Integer);
{- toggle mark of an item in file list }
Var
  aux  : PFileRec;
  marked: boolean;    {- current marked status }
  markIt: boolean;    {- T=Mark, F=Unmark }
  dx    : Integer;
Begin
  If count = 0 Then Exit;
  aux:= At(itemNum);
  If aux = Nil Then Exit;
  ToggleTheMark(aux);
End;

Procedure TFiles.MarkUnMarkWild(mStr: String; markOn, inclDirs: boolean);
Var
  mList  : MaskArray;
  tstr  : String[20];

  Procedure CheckMark(item: PFileRec); far;
  Var
    k    : Integer;
  Begin
    k:= 1;
    With item^ Do Begin
      While k <= mList[0].numMasks Do Begin
        If ((attr and Directory <> 0) and inclDirs) or
          (attr and Directory = 0) Then Begin
          tstr:= fname;
          If ext <> '' Then
            tstr:= tstr + '.' + ext;
          If IsMatch(mList[k].mask, tstr) Then Begin
            k:= MaxInt-1;
            If markOn Then
              flags:= flags or flMarked
            Else
              flags:= flags and not flMarked;
          End;
        End;
        Inc(k);
      End;
    End;  {- with }
  End;

Begin
  mStr:= Trim(mStr);
  If mStr = '' Then
    mStr:= starDotStar;
  MakeMaskArray(mList, mStr);
  If mList[0].nummasks > 0 Then Begin
    ForEach(@CheckMark);
    CalcMarkedbytes;
  End;
End;

Procedure TFiles.MarkByRange(Var ARange: MarkRangeRec; markOn: boolean);

  Procedure CheckMark(item: PFileRec); far;
  Var
    doIt  : boolean;
  Begin
    doIt:= False;
    With item^, ARange Do Begin
      If (fromSize + toSize >= 0) and (attr and Directory = 0) Then
        doIt:= (size >= fromSize) and (size <= toSize);

      If not doIt and (fromDate + toDate <> 0) Then
        doIt:= (fdate >= fromDate) and (fdate <= toDate);

      If not doIt and (fromTime + toTime <> 0) Then
        doIt:= (ftime >= fromTime) and (ftime <= toTime);

      If not doIt Then
        doIt:= attr and rangeAttr <> 0;

      If doIt Then
        If markOn Then
          flags:= flags or flMarked
        Else
          flags:= flags and not flMarked;
    End;  {- with }
  End;

Begin
  ForEach(@CheckMark);
  CalcMarkedbytes;
End;

Procedure TFiles.CalcMarkedbytes;
{- recalculate all marked bytes }

  Procedure CheckIt(item: PFileRec); far;
  Begin
    With item^ Do Begin
      If flags and flMarked <> 0 Then Begin
        If attr and Directory <> 0 Then
          Inc(mDirs)
        Else Begin
          Inc(mFiles);
          Inc(mbytes, size);
        End;
      End;
    End;
  End;

Begin
  mDirs:= 0;
  mFiles:= 0;
  mbytes:= 0;
  ForEach(@CheckIt);
End;

Procedure TFiles.Marks(cmd: Integer);

  Procedure MarkIt(item: PFileRec); far;
  Begin
    item^.flags:= item^.flags or flMarked;
  End;

  Procedure ClearIt(item: PFileRec); far;
  Begin
    With item^ Do Begin
      flags:= flags and not flMarked;
    End;
  End;

  Procedure SwapIt(item: PFileRec); far;
  Begin
    item^.flags:= item^.flags xor flMarked;
  End;

Begin
  Case cmd Of
    cmMark  :   Begin
              ForEach(@MarkIt);
              mbytes:= totbytes;
              mFiles:= totFiles;
              mDirs:= totDirs;
            End;
    cmSwap  :   Begin
              ForEach(@SwapIt);
              mbytes:= totbytes - mbytes;
              mFiles:= totFiles - mFiles;
              mDirs:= totDirs - mDirs;
            End;
    cmClear  :   Begin
              ForEach(@ClearIt);
              mbytes:= 0;
              mFiles:= 0;
              mDirs:= 0;
            End;
  End;
End;

Procedure TFiles.ReadDirectory(readPath: String; AMasks: MaskArray; recurs: boolean);
{- Read a directory. Path is the initial path to read. recurs is True if
  all subdirectories are to be read }
Begin
  PurgeFileList(fileHead, pathHead);    {- clear any existing stuff }

  InitReadDir(recurs, AMasks);

  ReadTheDir(AddBackSlash(readPath));

  If memError Then
    ErrorMsg(erWarning, erOk,
      'Not enough memory to read all files.',
      'Some files may be missing from the list');

  {- assign global working vars to TFiles object }
  allDirs:= recurs;
  If allDirs Then
    numDirs:= 0;
  masks:= MList;
  fileHead:= fHead;
  fileTail:= fTail;
  pathHead:= pHead;
  pathTail:= pTail;
  totFiles:= numFiles;
  totDirs:= numDirs;
  totbytes:= numbytes;
  mFiles:= 0;
  mDirs:= 0;
  mbytes:= 0;
  BuildList;
End;

Function ChangeAttrPrim(fname: String; attr: byte): Integer;
Var
  f    : File;
Begin
  Assign(f, fname);
  SetFAttr(f, attr);
  ChangeAttrPrim:= DosError;
End;

Function GetAttrPrim(fname: String; Var attr: Word): Integer;
Var
  f    : File;
Begin
  Assign(f, fname);
  GetFAttr(f, attr);
  GetAttrPrim:= DosError;
End;

Function ChangeAttr(fname: String; toSet, toUnset: byte): byte;
Var
  attr  : Word;
  k    : Integer;
Begin
  attr:= 0;
  k:= GetAttrPrim(fname, attr);
  If k = 0 Then Begin
    attr:= (attr or toSet) and toUnset;
    k:= ChangeAttrPrim(fname, attr);
    If k <> 0 Then
      ErrorMsg(erError, erOk, GetMsg(k), 'Cannot change attribute');
  End
  Else
    ErrorMsg(erError, erOk, GetMsg(k), 'Cannot change attribute');
  ChangeAttr:= attr;
End;

Function DeleteFilePrim(fname: String): Integer;
Var
  f    : File;
Begin
  Assign(f, fname);
  Erase(f);
  DeleteFilePrim:= IoResult;
End;

Procedure DeleteAFile(fname: String);
Var
  c    : Word;
  k    : Integer;
Begin
  k:= DeleteFilePrim(fname);
  If k <> 0 Then Begin
    If k = 5 Then Begin
      If not DoAll Then Begin
        c:= ErrorMsg(erError, erNoYes or erAll or erCancel,
                'File is Read/Only.',
                'Do you still wish to delete the file ?');
        doAll:= c = cmAll;
      End
      Else
        c:= cmYes;
      If (c = cmYes) or (c = cmAll) Then Begin
        k:= ChangeAttrPrim(fname, 0);
        If k <> 0 Then
          c:= ErrorMsg(erError, erOk or erCancel, GetMsg(k), sCantDelete)
        Else Begin
          k:= DeleteFilePrim(fname);
          If k <> 0 Then
            c:= ErrorMsg(erError, erOk or erCancel, GetMsg(k), sCantDelete);
        End;
      End;
    End
    Else
      c:= ErrorMsg(erError, erOk or erCancel, GetMsg(k), sCantDelete);
    abort:= c = cmCancel;
  End;
End;

Procedure TFiles.DoOperation(op: Integer; p: PFileRec; Var p1,p2);
Var
  tstr  : String;
  fname  : String;
Begin
  If p^.attr and Directory <> 0 Then Exit;

  tstr:= AddBackSlash(p^.PPath^.pname^);
  fname:= GetFileName(p);
  Case op Of
    opChangeAttrs  :  p^.attr:= ChangeAttr(tstr+fname, byte(p1), byte(p2));
    opDeleteFiles  :  Begin
                  DeleteAFile(tstr + fname);
                  p^.flags:= p^.flags or flDeleted;
                End;
  End;
End;

Procedure TFiles.PerformOp(op: Integer; iNum: Word; marked: boolean; Var p1, p2);
{- perform an operation on the current or marked files }
Var
  k    : Integer;
  stat  : POpStatus;
  r    : TRect;
  l    : longint;
  tstr  : String[80];
  tstr2  : String[80];
  c    : Word;
  stat2  : PStatusMsg2;

  Procedure CheckDeleted;
  Var
    k    : Word;
    p    : PFileRec;
  Begin
    k:= 0;
    While k <= Pred(count) Do Begin
      p:= At(k);
      With p^ Do Begin
        If flags and flDeleted <> 0 Then Begin
          If flags and flMarked <> 0 Then Begin
            Dec(mFiles);
            Dec(mbytes, size);
          End;
          Dec(totFiles);
          Dec(totbytes, size);
          AtDelete(k);
          k:= 0;  {- start back at beginning }
        End
        Else
          Inc(k);
      End;
    End;
  End;

  Procedure DoMarked(item: PFileRec); Far;
  Var
    event    : TEvent;
    check    : boolean;
  Begin
    If abort Then Exit;
    check:= False;
    GetMouseEvent(event);
    check:= (event.what and evMouseDown <> 0);
    If not check Then Begin
      GetKeyEvent(event);
      check:= (event.what = evKeyDown) and (event.keyCode = kbEsc)
    End;
    If (check) Then Begin
      c:= ErrorMsg(erWarning, erYesNo, 'Operation Interrupted',
              'Do you wish to cancel ?');
      abort:= c = cmYes;
      If abort Then Exit;
    End;
    With item^ Do Begin
      If (flags and flMarked <> 0) and (attr and Directory = 0) Then Begin
        stat^.UpdateFile(GetFileName(item));
        stat^.UpDateSource(item^.PPath^.pname^);
        DoOperation(op, item, p1, p2);
        With stat^ Do Begin
          UpDateCounts(1, 1);
        End;
      End;
    End;
  End;

Begin
  If count = 0 Then Exit;
  doAll:= False;
  abort:= False;
  stat2:= New(PStatusMsg2, Init('Press [Esc] or click mouse to abort'));
  Application^.Insert(stat2);

  tstr:= '';
  tstr:= '';
  abort:= False;
  Case op of
    opChangeAttrs  :   Begin
                  tstr:= 'Changing Attributes';
                  tstr2:= 'Percent of files changed'
                End;
    opDeleteFiles  :   Begin
                  tstr:= 'Deleteing Files';
                  tstr2:= 'Percent of files deleted';
                End;
  End;

  If marked Then Begin
    stat:= New(POpStatus, Init(tstr, tstr2, '','','', mFiles, mFiles));
    Application^.Insert(stat);
  End;

  If marked Then Begin
    ForEach(@DoMarked);
  End
  Else
    DoOperation(op, At(iNum), p1, p2);

  If op = opDeleteFiles Then
    CheckDeleted;

  If marked Then Begin
    Dispose(stat, Done);
  End;
  Dispose(stat2, Done);
End;

END.
