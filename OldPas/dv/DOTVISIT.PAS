{$A+,B-,E+,F+,G-,I-,N-,O+,R-,S-,V-,X-}
{$D-,L-}

Unit DotVisit;

{- Use this unit to recursively search through a directory structure }

{- Purpose:

	Visit is used to traverse through a directory or an entire directory
	stucture.

	As Visit encounters files, it calls a user supplied routine and passes
	the information along. It then proceeds on to the next file and so on
	until the entire tree (if resursion is on) until all files have been
	visited.

	The user supplied function can stop the visit process at any time. The
	user defined routines must be declared as FAR.
}


INTERFACE

Uses
	Dos,
	Strs,
	DotData,
	DotMem;

Type
	vRetType = (vAbort, vContinue);	{- return types for Visitor Proc }

	vFileRec = Record
		Attr	: Byte;		{- DONT change the order of these fields }
		Time	: Longint;
		Size	: Longint;
		fname	: String[12];
	End;

	VisitorFProc = Function(Var src, trg: String; Var fRec: vFileRec): vRetType;
	{- This is the type of the function that Visit calls for each file
		it encounters. For each file, vFileRec is sent to the function.
		If the function returns "vContinue" then the visit process will
		proceed to the next file. If the function returns "vAbort" then
		the Visit process will be terminated
	}

	VisitorDProc = Function(Var src, trg: String; total: LongInt): vRetType;
	{- this is the type of the function that visit calls every time is gets
		done traversing a directory. The directory that just got processed
		is passed to the function and the number of files and dirs found
		that met the Match criteria is sent.
		Return values of the function are as described above for Fproc.
	}

	VisitorMatch = Function(Var fname: String): Boolean;
	{- This function should return true if the file in [fname] is a valid
		file. If false is returned, then the file is not processed (sent)
		to VisitorFProc }

Function Visit(	src, trg: String;
						recurs: Boolean;
						vFProc: VisitorFProc;
						vDproc: VisitorDproc;
						vMatch: VisitorMatch): Integer;

Function VisitDirs(	src, trg: String;
							recurs: Boolean;
							vFProc: VisitorFProc;
							vDproc: VisitorDproc;
							vMatch: VisitorMatch): Integer;


IMPLEMENTATION

Type
	FilePtr = ^FileRec;
	FileRec = Record
		fRec	: vFileRec;
		path	: ^String;
		next	: FilePtr;
	End;


Var
	fstr			: String[80];		{- temporary vars to make the recursion }
	dirrec		: SearchRec;  		{- faster }
	visitRecurs	: Boolean;
	sendDirs		: Boolean;
	vProcFcall	: VisitorFProc;
	vProcDcall	: VisitorDproc;
	vProcMatch	: VisitorMatch;
	retCode		: Integer;
	vRetval		: vRetType;

{-----------------------------------------------------------------------}
{- The following routines are called recursively. They are also         }
{- declared Near ($F-) for speed.                                       }
{-----------------------------------------------------------------------}

Procedure AddItemToList(Var head, last: filePtr; Var item: SearchRec;
								fpath: String); Near;
{- add an item to the list }
Var
	aux	: FilePtr;
	temp	: FilePtr;
Begin
	If not GetMemCheck(aux, SizeOf(filerec)) Then Begin
		retCode:= 8;
		Exit;
	End;

	If not GetMemCheck(aux^.path, Length(fpath)+1) Then Begin
		retCode:= 8;
		Exit;
	End;

	With aux^ Do Begin
		Move(item.Attr, aux^.frec, SizeOf(vFileRec));
		path^:= fpath;
		next:= Nil;
	End;

	If head = Nil Then Begin
		head:= aux;
		last:= head;
	End
	Else Begin
		If (item.attr and Directory) <> 0 Then Begin
			last^.next:= aux;
			last:= aux;
		End
		Else Begin
			aux^.next:= head;
			head:= aux;
		End;
	End;
End;

Procedure PurgeList(Var list: FilePtr; NotDirs: Boolean); Near;
{- remove list from memory }
Var
	temp		: FilePtr;
	newlist,
	newtail	: FilePtr;
Begin
	newlist:= Nil;
	If list <> Nil Then Begin
		While list <> Nil Do Begin
			temp:= list;
			list:= list^.next;
			If NotDirs and (temp^.frec.attr = Directory) Then Begin
				If newlist = Nil Then Begin
					newlist:= temp;
					newtail:= temp;
				End
				Else Begin
					newtail^.next:= temp;
					newtail:= temp;
				End;
			End
			Else Begin
				FreeMemCheck(temp^.path, Length(temp^.path^)+1);
				FreeMemCheck(temp, SizeOf(FileRec));
			End;
		End;
		If newlist <> Nil Then Begin
			newTail^.next:= Nil;
			list:= newlist;
		End;
	End;
End;

Procedure VisitorPrim(src, trg: String); Near;
{- visit a directory structure }
Var
	flist			: filePtr;
	fflist		: FilePtr;
	last			: FilePtr;
	filesFound	: LongInt;
Begin
	filesFound:= 0;
	trg:= AddBackSlash(trg);
	src:= AddBackSlash(src);

	flist:= Nil;
	last:= Nil;

	fstr:= src + starDotStar;

	FindFirst(fstr, AnyFile, dirrec);     	{- get list of source files/dirs }
	While DosError = 0 Do Begin
		With dirRec Do Begin
			If (name[1] <> '.') and ((attr and 8) <> 8) and
				(	(attr and Directory = Directory) or
					(vProcMatch(name))
				) Then Begin
					AddItemToList(flist, last, dirrec, src);
					If retCode = 0 Then
						Inc(filesFound);
				End;
		End;
		FindNext(dirrec);
	End;

	fflist:= flist;
	If retCode = 0 Then Begin
		{- recursive section }
		While (flist <> Nil) and (vRetVal = vContinue) and (retCode = 0) Do Begin
			If flist^.frec.attr = Directory Then Begin	{- recurse through dir }
				If VisitRecurs Then
					VisitorPrim(src+flist^.frec.fname, trg + flist^.frec.fname);
				If sendDirs Then
					vRetVal:= vProcFCall(src, trg, flist^.frec);
			End
			Else
				vRetVal:= vProcFCall(src, trg, flist^.frec);
			flist:= flist^.next;
		End;
	End;

	PurgeList(fflist, False);
	If (vRetVal <> vAbort) and (retCode = 0) Then
		vRetVal:= vProcDcall(src, trg, filesFound);
End;

{------------------------------------------------------------------------}
{- End of recursive section                                              }
{------------------------------------------------------------------------}

Function Visit(	src, trg: String;
						recurs: Boolean;
						vFProc: VisitorFProc;
						vDproc: VisitorDproc;
						vMatch: VisitorMatch): Integer;

{- Visit all files starting from directory [src].

	[trg] is used if you wish to map the src dir being processed to a target.
	This is useful for copying files.
}

Begin
	visitRecurs:= recurs;
	sendDirs:= False;
	vRetVal:= vContinue;
	vProcFCall:= vFProc;
	vProcDCall:= vDProc;
	vProcMAtch:= vMatch;
	retCode:= 0;
	VisitorPrim(src, trg);
	Visit:= retCode;
End;

Function VisitDirs(	src, trg: String;
							recurs: Boolean;
							vFProc: VisitorFProc;
							vDproc: VisitorDproc;
							vMatch: VisitorMatch): Integer;

{- Visit all files starting from directory [src]. Also pass dirs to fProc
	function unlike Visit.

	[trg] is used if you wish to map the src dir being processed to a target.
	This is useful for copying files.
}

Begin
	visitRecurs:= recurs;
	sendDirs:= True;
	vRetVal:= vContinue;
	vProcFCall:= vFProc;
	vProcDCall:= vDProc;
	vProcMAtch:= vMatch;
	retCode:= 0;
	VisitorPrim(src, trg);
	VisitDirs:= retcode;
End;


END.
