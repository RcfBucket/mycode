{$A+,B-,E+,F+,G-,I-,N-,O-,R-,S-,V-,X+}
{$D-,L-}

Unit DotOvr;		{- overlay initialization }

INTERFACE

Uses
	Drivers,
	DotError,
	DotData,
	Overlay;

Procedure InitializeOverlay;

IMPLEMENTATION

Var
	SaveOvrRead		: OvrReadFunc;

Function OvrReadCheck(ovrSeg: Word): Integer;
Var
	errNum	: Integer;
	ch			: Char;
Begin
	errNum:= SaveOvrRead(ovrSeg);
	If errNum <> 0 Then Begin
		ErrorMsg(erError, erOk, 'Cannot read overlay file','Program aborting...');
		Halt;
	End;
	OvrReadCheck:= errNum;
End;

Procedure InitializeOverlay;
Begin
	OvrInit('DOT.OVR');
	If ovrResult <> ovrOk Then Begin
		PrintStr('Cannot initialize overlay manager');
		Halt;
	End;

	OvrInitEMS;		{- attempt to use EMS }

	SaveOvrRead:= OvrReadBuf;
	OvrReadBuf:= OvrReadCheck;
End;

BEGIN
	PrintStr(CRLF + sProgTitle + sSpace2 + sVersion + sSpace +
				sVersionNum + sSpace + sCopyRight + CRLF);
	InitializeOverlay;
END.

