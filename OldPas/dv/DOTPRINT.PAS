{$A+,B-,E+,F+,G-,I-,N-,O+,R-,S-,V-,X+}
{$D-,L-}

Unit DotPrint;

INTERFACE

Uses
	Strs,
	DotMisc,
	DotData,
	DotError,
	DotInp,
	drivers,
	objects,
	dialogs,
	views,
	app;

Procedure SetPrintOpts;

IMPLEMENTATION

Type
	PPrintOptDiag = ^TPrintOptDiag;
	TPrintOptDiag = object(TDialog2)
		data	: 	Record
						dPrintType		: Word;
						dPrintOpts		: Word;
						linesPP			: String[2];
						pwidth			: String[3];
						lMargin			: String[2];
						tMargin			: String[2];
						cCodes			: String[25];
						fPrintType		: Word;
						fPrintOpts		: Word;
						lSpacing			: String[1];
						tabSpacing		: String[1];
					End;
		procedure SetData(Var rec); virtual;
		procedure SetDefaultOpts;
		Function ValidateOptions: Boolean;
		procedure HandleEvent(Var event: TEvent); virtual;
	End;

Procedure TPrintOptDiag.HandleEvent(Var event: TEvent);
Begin
	If (event.what and evCommand <> 0) and (event.command = cmOK) Then Begin
		If not ValidateOptions Then
			ClearEvent(event);
	End;
	TDialog2.HandleEvent(event);
End;

Procedure TPrintOptDiag.SetData(Var rec);
{- set internal record for the dialog box }
Begin
	FillChar(data, SizeOf(data), #0);
	With data Do Begin
		dPrintType		:= defaults.dPrintType;
		dPrintOpts		:= defaults.dPrintOpts;
		linesPP			:= Long2Str(defaults.linesPP);
		pWidth			:= Long2Str(defaults.pageWidth);
		lMargin			:= Long2Str(defaults.leftMargin);
		tMargin			:= Long2Str(defaults.topMargin);
		cCodes			:= defaults.controlCodes;
		fPrintType		:= defaults.fPrintType;
		fPrintOpts		:= defaults.fPrintOpts;
		lSpacing			:= Long2Str(defaults.lSpacing);
		tabSpacing		:= Long2Str(defaults.tabSpacing);
	End;
	TDialog2.SetData(data);
End;

Procedure TPrintOptDiag.SetDefaultOpts;
{- set the default options from the internal data }
Begin
	TDialog2.GetData(data);
	With data Do Begin
		defaults.dPrintType	:= dPrintType;
		defaults.dPrintOpts	:= dPrintOpts;
		defaults.linesPP		:= Str2Long(linesPP);
		defaults.pageWidth	:= Str2Long(pWidth);
		defaults.leftMargin	:= Str2Long(lMargin);
		defaults.topMargin	:= Str2Long(tMargin);
		defaults.controlCodes:= cCodes;
		defaults.fPrintType	:= fPrintType;
		defaults.fPrintOpts	:= fPrintOpts;
		defaults.lSpacing		:= Str2Long(lSpacing);
		defaults.tabSpacing  := Str2Long(tabSpacing);
	End;
End;

Function TPrintOptDiag.ValidateOptions: Boolean;
Var
	ok		: Boolean;
	tstr	: String[80];
	range	: String[80];
Begin
	GetData(data);
	ok:= True;
	With data Do Begin
		If ok and ((Str2Long(linesPP) < 25) or (Str2Long(linesPP) > 99)) Then Begin
			tstr:= sLinesPP;
			range:= '(25-99)';
			ok:= False;
		End;
		If ok and ((Str2Long(lSpacing) < 1) or (Str2Long(lSpacing) > 9)) Then Begin
			tstr:= sLineSpacing;
			range:= '(1-9)';
			ok:= False;
		End;
		If ok and ((Str2Long(tabSpacing) < 1) or (Str2Long(tabSpacing) > 9)) Then Begin
			tstr:= sTabSpacing;
			range:= '(1-9)';
			ok:= False;
		End;
		If ok and ((Str2Long(pWidth) < 40) or (Str2Long(pWidth) > 132)) Then Begin
			tstr:= sPageWidth;
			range:= '(40-132)';
			ok:= False;
		End;
		If ok and (Str2Long(lMargin) > (Str2Long(pWidth) Div 2)) Then Begin
			tstr:= sLeftMargin;
			range:= '(0-' + Long2Str( Str2Long(pWidth) Div 2) +
						'). ' + sCantExceedW;
			ok:= False;
		End;
		If ok and (Str2Long(tMargin) > (Str2Long(linesPP) Div 2)) Then Begin
			tstr:= sTopMargin;
			range:= '(0-' + Long2Str( Str2Long(linesPP) Div 2) +
						'). ' + sCantExceedH;
			ok:= False;
		End;
	End;
	If not ok Then Begin
		tstr:= Trim(StUpCase(StripRange(StripControls(tstr), ':',':')));
		ErrorMsg(erError, erOk, sInvalidNumber+' � '+tstr, sLegalRange+range);
	End;
	ValidateOptions:= ok;
End;

Procedure SetPrintOpts;
Var
	d		: PPrintOptDiag;
	p		: PView;
	r		: TRect;
	k		: Integer;
Begin
	r.Assign(0,0, 65,17);
	d:= New(PPrintOptDiag, Init(R, StripControls(sDirPrintOpts)));
	With d^ Do Begin
		{- DIRECTORY OPTIONS }
		r.Assign(3, 3, 30, 5);
		p:= 	New(PRadioButtons2, Init(R,
					NewSItem(sWideListing,
					NewSItem(sCondensed,
					nil)))
				);
		Insert(p);
		r.Assign(2, 2, 30, 3);
		Insert(New(PLabel2, Init(R, sDirPrintType, p)));

		r.Assign(3, 7, 30, 8);
		p:= 	New(PCheckBoxes2, Init(r,
					NewSItem(sPrintCheckSum,
					nil))
				);
		Insert(p);
		r.Assign(2,6, 30,7);
		Insert(New(PLabel2, Init(r, sDirPrintOpts, p)));

		{- PAGE OPTIONS }
		k:= 9;
		r.Assign(3,k, 30,k+1);
		Insert(New(PStaticText, Init(r, sPageOptions)));
		Inc(k);

		r.Assign(21,k, 26,k+1);
		p:= New(PCustInput, Init(r, 2, inpMixedCase, numberChars));
		Insert(p);
		r.Assign(3,k, 21,k+1);
		Insert(New(PLabel2, Init(r, sLinesPP, p)));
		Inc(k);

		r.Assign(21,k, 26,k+1);
		p:= New(PCustInput, Init(r, 3, inpMixedCase, numberChars));
		Insert(p);
		r.Assign(3,k, 21,k+1);
		Insert(New(PLabel2, Init(r, sPageWidth, p)));
		Inc(k);

		r.Assign(21,k, 26,k+1);
		p:= New(PCustInput, Init(r, 2, inpMixedCase, numberChars));
		Insert(p);
		r.Assign(3,k, 21,k+1);
		Insert(New(PLabel2, Init(r, sLeftMargin, p)));
		Inc(k);

		r.Assign(21,k, 26,k+1);
		p:= New(PCustInput, Init(r, 2, inpMixedCase, numberChars));
		Insert(p);
		r.Assign(3,k, 21,k+1);
		Insert(New(PLabel2, Init(r, sTopMargin, p)));
		Inc(k);

		r.Assign(21,k, 36,k+1);
		p:= New(PCustInput, Init(r, 25, inpMixedCase, numberChars+'#$'));
		Insert(p);
		r.Assign(3,k, 21,k+1);
		Insert(New(PLabel2, Init(r, sControlCodes, p)));

		{- FILE OPTIONS }
		r.Assign(40, 3, 62, 5);
		p:= 	New(PRadioButtons2, Init(R,
					NewSItem(sText,
					NewSItem(sHex,
					nil)))
				);
		Insert(p);
		r.Assign(40, 2, 62, 3);
		Insert(New(PLabel2, Init(R, sFilePrintMode, p)));

		r.Assign(40, 7, 62, 9);
		p:= 	New(PCheckBoxes2, Init(r,
					NewSItem(sLineNumbers,
					NewSItem(sWrapLongLines,
					nil)))
				);
		Insert(p);
		r.Assign(40,6, 62,7);
		Insert(New(PLabel2, Init(r, sFilePrintOpts, p)));

		k:= 10;
		r.Assign(56,k, 59,k+1);
		p:= New(PCustInput, Init(r, 1, inpMixedCase, numberChars));
		Insert(p);
		r.Assign(40,k, 56,k+1);
		Insert(New(PLabel2, Init(r, sLineSpacing, p)));
		Inc(k);

		r.Assign(56,k, 59,k+1);
		p:= New(PCustInput, Init(r, 1, inpMixedCase, numberChars));
		Insert(p);
		r.Assign(40,k, 56,k+1);
		Insert(New(PLabel2, Init(r, sTabSpacing, p)));

		GetExtent(r);
		r.Assign(r.b.x-25, r.b.y-3, r.b.x-15, r.b.y-1);
		Insert(New(PButton2, Init(r, sOk, cmOK, bfDefault)));

		GetExtent(r);
		r.Assign(r.b.x-13, r.b.y-3, r.b.x-3, r.b.y-1);
		Insert(New(PButton2, Init(R, sCancel, cmCancel, bfNormal)));

		SelectNext(False);
		SetData(data);
	End;
	If Application^.ValidView(d) <> Nil Then Begin
		If Application^.ExecView(d) = cmOk Then Begin
			d^.GetData(d^.data);
			d^.SetDefaultOpts;
		End;
		Dispose(d, Done);
	End;
End;

END.
