{$A+,B-,E+,F+,G-,I-,N-,O+,R-,S-,V-,X+}
{$D-,L-}

Unit DotOpts;

{- routines to set miscellaneous features }

INTERFACE

Uses
	strs,
	Drivers,
	Memory,
	Objects,
	Views,
	DotMouse,
	Dialogs,
	DotInp,
	DotData,
	DotPick,
	DotColor,
	DotKeys,
	DotError,
	DotMisc,
	App;

Procedure SetFileListDefaults;
Procedure SetEnvironment;
Procedure SetDirectories;
Procedure SetDisplay;
Procedure SetMouse;
Procedure ActivateDefaults;
Procedure UpdateDefaults;
Procedure ReadDefaults;
Procedure SetColors;
Function GetSortOptions(Var Sort1, Sort2, Order, Opts: Byte): Boolean;

IMPLEMENTATION

Const
	cmSortOptions	= 7000;

Type
	PSortOpts = ^TSortOpts;
	TSortOpts = object(TRadioButtons2)
		constructor Init(x, y: Word; lbl1: String);
	End;

	PSortType = ^TSortType;
	TSortType = object(TRadioButtons2)
		constructor Init(x, y: Word);
	End;

	PFileOptDiag = ^TFileOptDiag;
	TFileOptDiag = object(TDialog2)
		procedure HandleEvent(Var event: TEvent); virtual;
	End;

Procedure SetEnvironment;
Var
	d		: PDialog2;
	p		: PView;
	r		: TRect;
	c		: Word;
	rec	: 	Record
					opts	: Word;
					ch		: String[5];
				End;
Begin
	r.Assign(0,0, 63,17);
	d:= New(PDialog2, Init(R, StripControls(sEnvironment)));
	With d^ Do Begin
		rec.opts:= defaults.preferences;
		rec.ch:= defaults.backgroundCh;

		r.Assign(3, 3, 60, 11);
		p:= 	New(PCheckBoxes2, Init(R,
					NewSItem(sPrompt,
					NewSItem(sVerify,
					NewSItem(sSwapToDisk,
					NewSItem(sPauseAfterDOS,
					NewSItem(sAutoSave,
					NewSItem(sBeepOnError,
					NewSItem(sOriginal,
					NewSItem(sShadowed,
					nil)))))))))
				);
		p^.HelpCTX:= 2001;	{- 2001-2016 are for this cluster }
		Insert(p);
		r.Assign(2, 2, 60, 3);
		Insert(New(PLabel2, Init(R, sPreferences, p)));

		c:= CStrLen(sBackgroundChar);
		r.Assign(3+c+2,12, 3+c+2+3, 13);
		p:= New(PCustInput, Init(r, 1, inpMixedCase, ''));
		p^.helpCTX:= 2017;
		Insert(p);
		r.Assign(3,12, 3+c+2, 13);
		Insert(New(PLabel2, Init(R, sBackgroundChar, p)));

		GetExtent(r);
		r.Assign(r.b.x-25, r.b.y-3, r.b.x-15, r.b.y-1);
		p:= New(PButton2, Init(r, sOk, cmOK, bfDefault));
		p^.helpCtx:= 2018;
		Insert(p);

		GetExtent(r);
		r.Assign(r.b.x-13, r.b.y-3, r.b.x-3, r.b.y-1);
		p:= New(PButton2, Init(R, sCancel, cmCancel, bfNormal));
		p^.helpCtx:= 2019;
		Insert(p);

		SelectNext(False);

		SetData(rec);
	End;
	If Application^.ValidView(d) <> Nil Then Begin
		c:= Application^.ExecView(d);
		If c <> cmCancel Then Begin
			d^.GetData(rec);
			defaults.preferences:= rec.opts;
			defaults.backGroundCh:= rec.ch[1];
		End;
		Dispose(d, Done);
		ActivateDefaults;
	End;
End;

Constructor TSortOpts.Init(x, y: Word; lbl1: String);
Var
	r		: TRect;
Begin
	r.Assign(x, y, x+22, y+7);
	TRadioButtons.Init(r,
		NewSItem(lbl1,
		NewSItem('File name',
		NewSItem('Extension',
		NewSItem('Size',
		NewSItem('Date',
		NewSItem('Time',
		NewSItem('Marked files',
		nil)))))))
	);
	helpCTX:= 2020;
End;

Constructor TSortType.Init(x, y: Word);
Var
	r		: TRect;
Begin
	r.Assign(x, y, x+16, y+2);
	TRadioButtons.Init(r,
		NewSItem('~A~scending',
		NewSItem('~D~escending',
		nil))
	);
	helpCTX:= 2037;
End;

Function GetSortOptions(Var sort1, sort2, Order, Opts: Byte): Boolean;
{- return sort parameters }
Var
	d		: PDialog2;
	p		: PView;
	r		: TRect;
	c		: Word;
	rec	: 	Record
					sorts1: Word;
					sorts2: Word;
					odr	: Word;
					opts	: Word;
				End;
Begin
	GetSortOptions:= False;
	r.Assign(0,0, 69,14);
	d:= New(PDialog2, Init(R, 'Sort Options'));
	With d^ Do Begin
		FillChar(rec, SizeOf(rec), #0);
		rec.opts:= opts;
		rec.sorts1:= sort1;
		rec.sorts2:= sort2;
		rec.odr:= order;

		p:= New(PSortOpts, Init(3, 3, 'Unsorted'));
		Insert(p);
		r.Assign(3, 2, 25, 3);
		Insert(New(PLabel2, Init(R, '~P~rimary Key', p)));

		p:= New(PSortOpts, Init(27, 3, 'Not used'));
		Insert(p);
		r.Assign(27, 2, 49, 3);
		Insert(New(PLabel2, Init(R, '~S~econdary key', p)));

		r.Assign(50, 3, 66, 5);
		p:= New(PSortType, Init(50, 3));
		Insert(p);

		r.Assign(50, 2, 66, 3);
		p:= New(PLabel2, Init(R, 'sort ~O~rder', p));
		Insert(p);

		r.Assign(3, 11, 32, 12);
		p:= New(PCheckBoxes2, Init(R,
					NewSItem('~G~roup sort by directory',
					nil))
				);
		p^.helpCTX:= 2040;
		Insert(p);

		{- buttons }

		GetExtent(r);
		r.Assign(r.b.x-25, r.b.y-3, r.b.x-15, r.b.y-1);
		p:= New(PButton2, Init(r, sOk, cmOK, bfDefault));
		p^.helpCTX:= 2018;
		Insert(p);

		GetExtent(r);
		r.Assign(r.b.x-13, r.b.y-3, r.b.x-3, r.b.y-1);
		p:= New(PButton2, Init(R, sCancel, cmCancel, bfNormal));
		p^.helpCTX:= 2019;
		Insert(p);

		SelectNext(False);
		SetData(rec);
	End;
	If Application^.ValidView(d) <> Nil Then Begin
		c:= Application^.ExecView(d);
		If c <> cmCancel Then Begin
			d^.GetData(rec);
			If (rec.sorts1 = dfltUnsorted) or (rec.sorts1 = rec.sorts2) Then
				rec.sorts2:= dfltNoSort;
			opts:= rec.opts;
			sort1:= rec.sorts1;
			sort2:= rec.sorts2;
			order:= rec.odr;
			GetSortOptions:= True;
		End;
		Dispose(d, Done);
	End;
End;

Procedure SetSortDefaults;
Begin
	With defaults Do 
		GetSortOptions(dSortType1, dSortType2, dSortOrder, dSortOpts);
End;

Procedure TFileOptDiag.HandleEvent(Var event: TEvent);
Begin
	TDialog2.HandleEvent(event);
	If (event.what and evCommand <> 0) and
		(event.command = cmSortOptions) Then Begin
		SetSortDefaults;
		ClearEvent(event);
	End;
End;

Procedure SetFileListDefaults;
Var
	d		: PFileOptDiag;
	p		: PView;
	r		: TRect;
	c		: Word;
	rec	: Word;
Begin
	r.Assign(0,0, 40,13);
	d:= New(PFileOptDiag, Init(R, StripControls(sFileListDef)));
	With d^ Do Begin
		rec:= defaults.dListOpts;

		r.Assign(2, 3, 38, 8);
		p:= 	New(PCheckBoxes2, Init(R,
					NewSItem('~U~ppercase directory names',
					NewSItem('Uppercase ~F~ile names',
					NewSItem('~W~ide list display',
					NewSItem('S~h~ow file statistics',
					NewSItem('~2~4 hour time format',
					nil))))))
				);
		p^.helpCTX:= 2050;
		Insert(p);
		r.Assign(2, 2, 38, 3);
		Insert(New(PLabel2, Init(R, '~L~ist options', p)));

		{- buttons }

		GetExtent(r);
		r.Assign(r.b.x-39, r.b.y-3, r.b.x-23, r.b.y-1);
		p:= New(PButton2, Init(r, '~S~ort options', cmSortOptions, bfNormal));
		p^.HelpCTX:= 2067;
		Insert(p);

		GetExtent(r);
		r.Assign(r.b.x-22, r.b.y-3, r.b.x-14, r.b.y-1);
		p:= New(PButton2, Init(r, sOk, cmOK, bfDefault));
		p^.helpCTX:= 2018;
		Insert(p);

		GetExtent(r);
		r.Assign(r.b.x-13, r.b.y-3, r.b.x-3, r.b.y-1);
		p:= New(PButton2, Init(R, sCancel, cmCancel, bfNormal));
		Insert(p);
		p^.HelpCTX:= 2019;

		SelectNext(False);
		SetData(rec);
	End;
	If Application^.ValidView(d) <> Nil Then Begin
		c:= Application^.ExecView(d);
		If c <> cmCancel Then Begin
			d^.GetData(rec);
			defaults.dListOpts:= rec;
		End;
		Dispose(d, Done);
		ActivateDefaults;
	End;
End;

Procedure SetDisplay;
Var
	d		: PDialog2;
	p		: PView;
	r		: TRect;
	c		: Word;
	rec	: 	Record
					modes	: Word;
					vMode	: Word;
				End;
Begin
	FillChar(rec, SizeOf(rec), #0);
	Case defaults.displayMode and not smFont8x8 of
		smMono	:	rec.modes:= 2;
		smBW80	:	rec.modes:= 1;
		Else
			rec.modes:= 0;		{- assume color }
	End;
	If defaults.displayMode and smFont8x8 <> 0 Then
		rec.vMode:= rec.vMode or 1;
	If defaults.snowCheck Then
		rec.vMode:= rec.vMode or 2;

	r.Assign(0,0, 33,15);
	d:= New(PDialog2, Init(R, StripControls(sDisplay)));
	With d^ Do Begin
		r.Assign(3, 3, 30, 6);
		p:= 	New(PRadioButtons2, Init(R,
					NewSItem(sColor,
					NewSItem(sBlackWhite,
					NewSItem(sMonoChrome,
					nil))))
				);
		p^.helpCTX:= 2070;
		Insert(p);
		r.Assign(2, 2, 30, 3);
		Insert(New(PLabel2, Init(R, sVideoMode, p)));

		r.Assign(3, 8, 30, 10);
		p:= 	New(PCheckBoxes2, Init(R,
					NewSItem(s43LineMode,
					NewSItem(sCheckSnow,
					Nil)))
				);
		p^.helpCTX:= 2080;
		Insert(p);
		r.Assign(2, 7, 30, 8);
		Insert(New(PLabel2, Init(R, sDisplay, p)));

		GetExtent(r);
		r.Assign(r.b.x-25, r.b.y-3, r.b.x-15, r.b.y-1);
		p:= New(PButton2, Init(r, sOk, cmOK, bfDefault));
		p^.helpCTX:= 2018;
		Insert(p);

		GetExtent(r);
		r.Assign(r.b.x-13, r.b.y-3, r.b.x-3, r.b.y-1);
		p:= New(PButton2, Init(R, sCancel, cmCancel, bfNormal));
		p^.helpCTX:= 2019;
		Insert(p);


		SelectNext(False);
		SetData(rec);
	End;
	If Application^.ValidView(d) <> Nil Then Begin
		c:= Application^.ExecView(d);
		If c = cmOK Then Begin
			d^.GetData(rec);
			Case rec.modes of
				0	:	defaults.displayMode:= smCO80;
				1	:	defaults.displayMode:= smBW80;
				2	:	defaults.displayMode:= smMono;
			End;
			If rec.vMode and 1 <> 0 Then
				defaults.displayMode:= defaults.displayMode or smFont8x8;
			defaults.snowCheck:= (rec.vMode and 2 <> 0);
		End;
		Dispose(d, Done);
		ActivateDefaults;
	End;
End;

Procedure SetDirectories;
Var
	d		: PDialog2;
	p		: PView;
	r		: TRect;
	c		: Word;
	rec	: String[79];
Begin
	r.Assign(0,0, 63,11);
	d:= New(PDialog2, Init(R, StripControls(sDirectories)));
	With d^ Do Begin
		rec:= defaults.swapPath;

		r.Assign(3, 3, 60, 4);
		p:= New(PFileInput, Init(r, 79, sInvalidSwapDir, []));
		p^.helpCTX:= 2068;
		Insert(p);
		r.Assign(2, 2, 60, 3);
		Insert(New(PLabel2, Init(R, sSwapDir, p)));

		GetExtent(r);
		r.Assign(r.b.x-25, r.b.y-3, r.b.x-15, r.b.y-1);
		p:= New(PButton2, Init(r, sOk, cmOK, bfDefault));
		p^.helpCTX:= 2018;
		Insert(p);

		GetExtent(r);
		r.Assign(r.b.x-13, r.b.y-3, r.b.x-3, r.b.y-1);
		p:= New(PButton2, Init(R, sCancel, cmCancel, bfNormal));
		p^.helpCTX:= 2019;
		Insert(p);

		SelectNext(False);
		SetData(rec);
	End;
	If Application^.ValidView(d) <> Nil Then Begin
		c:= Application^.ExecView(d);
		If c = cmOk Then Begin
			d^.GetData(rec);
			defaults.swapPath:= rec;
		End;
		Dispose(d, Done);
	End;
End;

Procedure ActivateDefaults;
Var
	screenChanged	: Boolean;
	temp				: Word;
	event				: TEvent;
Begin
	screenChanged:= False;
	With defaults Do Begin
		temp:= ScreenMode;		{- get 'current' screen mode }
		MouseReverse:= leftHandedM;
		DoubleDelay:= mouseDelay;

		deskTop^.background^.pattern:= backgroundCh;

		If displayMode <> screenMode Then Begin
			If not HiResScreen Then Begin
				ErrorMsg(erWarning, erOk, s43LineNotSupported, '');
				displayMode:= (displayMode and not smFont8x8);
			End;
			Application^.SetScreenMode(displayMode);
			screenChanged:= True;
		End;

		{- do not set snow checking on for monochrome }
		If (screenMode and not smFont8x8) <> smMono Then
			CheckSnow:= snowCheck;
	End;

	DoneMemory;
	If screenChanged Then Begin
		{- reinitialize mouse. The mouse disappears when changing from
			Mono mode to Color or Black and white }
		ASM
			cmp		MouseEvents, 0			{- check for mouse }
			je			@@NoMouse
			xor		ax,ax						{- call mouse hardware reset }
			int		33h
			@@Nomouse:
		END;
		InitEvents;
	End;
	Application^.Redraw;
	If screenChanged and
			((temp and smFont8x8) <>
				(defaults.displayMode and smFont8x8)) Then Begin
		{- changed from 43/50 to 25 line mode or visa-versa. ReTile wins}
		event.what:= evCommand;
		event.command:= cmCascade;
		Application^.PutEvent(event);
	End;
End;

Procedure SetMouse;
Var
	d:	PDialog2;
Begin
	d:= New(PMouseDialog, Init);
	If Application^.ValidView(D) <> nil Then Begin 
		d^.SetData(MouseReverse);
		If Application^.ExecView(D) <> cmCancel Then Begin
			d^.GetData(MouseReverse);
			defaults.LeftHandedM:= mouseReverse;
			defaults.mouseDelay:= DoubleDelay;
		End;
		Dispose(d, Done);
  End;
End;

(*
Procedure StoreValsInEXE;
{- this routine will store values following DOTid2 (see DOTHEAD.PAS)
	into the .EXE file }
Label
	ExitPoint;
Var
	defOffset	: LongInt;
	cp				: ClonePack;
	ch				: Char;
Begin
	defOffset:= InitForCloning(AddBackSlash(progPath)+'DOT.EXE', cp,
										dotID2, SizeOf(DOTid2));
	If CloneError <> 0 Then Begin
		ErrorMsg(erError, erOk,
			'Cannot find DOT.EXE in specified program path',
			'Some options cannot be saved');
		Goto exitPoint;
	End;

	If defOffset = 0 Then Begin
		ErrorMsg(erError, erOk,
			'Cannot find ID string in EXE file.',
			'Some options cannot be saved');
		Goto ExitPoint;
	End;

	StoreDefaults(cp, defOffset, DotId2, BytesInHead2);
ExitPoint:
	CloseForCloning(cp);
End;
*)

Procedure UpdateDefaults;
{- write Defaults,Application^.Palette and key definitions to opt file }
Var
	f			: File;
	j			: Integer;
	k			: Integer;
	tstr		: String;
	temp		: Integer;
Begin
	Assign(f, AddBackslash(progPath) + sConfigFile);
	Rewrite(f, 1);
	k:= IoResult;
	If k <> 0 Then
		ErrorMsg(erError, erOk, GetMsg(k), 'Cannot write options to disk')
	Else Begin
		BlockWrite(f, DotId[1], SizeOf(dotId)-1);
		k:= IoResult;

		If k = 0 Then Begin
			BlockWrite(f, defaults, SizeOf(defaults));
			k:= IoResult;
		End;

		If k = 0 Then Begin
			temp:= appPalette;

			appPalette:= apColor;
			tstr:= Application^.GetPalette^;
			BlockWrite(f, tstr, SizeOf(string));
			k:= IoResult;

			If k = 0 Then Begin
				appPalette:= apBlackWhite;
				tstr:= Application^.GetPalette^;
				BlockWrite(f, tstr, SizeOf(string));
				k:= IoResult;
			End;

			If k = 0 Then Begin
				appPalette:= apMonochrome;
				tstr:= Application^.GetPalette^;
				BlockWrite(f, tstr, SizeOf(string));
				k:= IoResult;
			End;

			appPalette:= temp;
		End;

		If k = 0 Then Begin
			j:= 1;
			While (j <= LastChangeable) and (k = 0) Do Begin
				BlockWrite(f, cmds[j].keyStroke, SizeOf(Word));
				k:= IoResult;
				Inc(j);
			End;
		End;

		Close(f);
		If IoResult <> 0 Then ;
	End;
End;

Procedure ReadDefaults;
{- read Defaults,Application^.Palette and key definitions to opt file }
Var
	f			: File;
	k,j		: Integer;
	tstr		: String;
	temp		: Integer;
Begin
	k:= -1;
	Assign(f, AddBackSlash(ProgPath) + sConfigFile);
	Reset(f, 1);
	If IoResult = 0 Then Begin
		BlockRead(f, tstr[1], SizeOf(dotId)-1);
		tstr[0]:= Chr(SizeOf(dotId)-1);
		k:= IoResult;

		If k = 0 Then Begin
			If tstr <> dotId Then k:= -1;
			If k <> 0 Then
				ErrorMsg(erError, erOk, 'Invalid configuration file',
							'Using defaults');
		End;

		If k = 0 Then Begin
			BlockRead(f, defaults, SizeOf(defaults));
			k:= IoResult;
		End;
		If k = 0 Then Begin
			temp:= appPalette;
			BlockRead(f, tstr, SizeOf(string));
			k:= IoResult;
			If k = 0 Then Begin
				appPalette:= apColor;
				Application^.GetPalette^:= tstr;
			End;

			BlockRead(f, tstr, SizeOf(string));
			k:= IoResult;
			If k = 0 Then Begin
				appPalette:= apBlackWhite;
				Application^.GetPalette^:= tstr;
			End;

			BlockRead(f, tstr, SizeOf(string));
			k:= IoResult;
			If k = 0 Then Begin
				appPalette:= apMonochrome;
				Application^.GetPalette^:= tstr;
			End;
			appPalette:= temp;
		End;

		If k = 0 Then Begin
			j:= 1;
			While (j <= LastChangeable) and (k = 0) Do Begin
				BlockRead(f, cmds[j].keyStroke, SizeOf(Word));
				k:= IoResult;
				Inc(j);
			End;
		End;

		Close(f);
		If IoResult <> 0 Then ;
	End;
End;

Procedure SetColors;
Var
	c	: Word;
	d	: PColorDialog;
Begin
	d:= New(PColorDialog, Init('',
			ColorGroup('Desktop',
				ColorItem('Color',             1, 
				nil),
			ColorGroup('Menus',
				ColorItem('Normal',            2,
				ColorItem('Disabled',          3,
				ColorItem('Shortcut',          4,
				ColorItem('Selected',          5,
				ColorItem('Selected disabled', 6,
				ColorItem('Shortcut selected', 7, 
				nil)))))),
			ColorGroup('Dialogs',
				ColorItem('Frame/background',  33,
				ColorItem('Frame icons',       34,
				ColorItem('Scroll bar page',   35,
				ColorItem('Scroll bar icons',  36,
				ColorItem('Normal text',       37,
				ColorItem('Highlighted Text',  64,

				ColorItem('Label normal',      38,
				ColorItem('Label selected',    39,
				ColorItem('Label shortcut',    40,

				ColorItem('Button normal',     41,
				ColorItem('Button default',    42,
				ColorItem('Button selected',   43,
				ColorItem('Button disabled',   44,
				ColorItem('Button shortcut',   45,
				ColorItem('Button shadow',     46,

				ColorItem('Cluster normal',    47,
				ColorItem('Cluster selected',  48,
				ColorItem('Cluster shortcut',  49,

				ColorItem('Input normal',      50,
				ColorItem('Input selected',    51,
				ColorItem('Input arrow',       52,

				ColorItem('History button',    53,
				ColorItem('History sides',     54,
				ColorItem('History bar page',  55,
				ColorItem('History bar icons', 56,

				ColorItem('List normal',       57,
				ColorItem('List focused',      58,
				ColorItem('List selected',     59,
				ColorItem('List divider',      60,

				ColorItem('Information pane',  61, 
				nil)))))))))))))))))))))))))))))),
			ColorGroup('Errors',
				ColorItem('Frame/background',  	93,
				ColorItem('Text',			      	97,

				ColorItem('Button normal',     	101,
				ColorItem('Button default',    	102,
				ColorItem('Button selected',   	103,
				ColorItem('Button disabled',   	104,
				ColorItem('Button shortcut',   	105,
				ColorItem('Button shadow',     	106,
				nil)))))))),
			ColorGroup('File list',
				ColorItem('Frame inactive',		107,
				ColorItem('Frame/background',		108,
				ColorItem('Frame icons',       	109,
				ColorItem('Scroll bar page',   	110,
				ColorItem('Scroll bar icons',  	111,

				ColorItem('Normal text',       	112,
				ColorItem('Highlighted Text',		74,
				ColorItem('File path',           75,

				ColorItem('List normal',       	132,
				ColorItem('List focused',      	133,
				ColorItem('List selected',     	134,
				ColorItem('List divider',      	135,

				nil)))))))))))),
			ColorGroup('Windows',
				ColorItem('Frame incative',		16,
				ColorItem('Frame/background',		17,
				ColorItem('Frame icons',       	18,
				ColorItem('Scroll bar page',   	19,
				ColorItem('Scroll bar icons',  	20,
				ColorItem('Normal text',       	21,
				ColorItem('Selected text',			22,
				ColorItem('Percent complete',		81,
				nil)))))))),
			nil))))))));

	If Application^.ValidView(D) <> nil Then Begin
		d^.SetData(Application^.GetPalette^);
		c:= Application^.ExecView(d);
		If c <> cmCancel Then Begin
			If c = cmOk Then
				Application^.GetPalette^:= d^.pal
			Else If c = cmDefaultColors Then Begin
				Case appPalette of
					apColor		:	Application^.GetPalette^:= CDotColor +
																			CAddColor +
																			CErrorColor +
																			CListColor;
					apBlackWhite:	Application^.GetPalette^:= CDotBlackWhite +
																			CAddBW +
																			CErrorBlackWhite +
																			CListBlackWhite;
					apMonochrome:	Application^.GetPalette^:= CDotMonochrome +
																			CAddMono +
																			CErrorMonochrome +
																			CListMonochrome;
				End;
			End;
			DoneMemory;  {- Dispose all group buffers }
			Application^.ReDraw;      {- Redraw application with new palette }
		End;
		Dispose(d, Done);
	End;
End;

END.
