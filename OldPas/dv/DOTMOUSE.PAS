{$A+,B-,E+,F+,G-,I-,N-,O+,R-,S-,V-,X-}
{$D-,L-}

Unit DotMouse;

INTERFACE

Uses
	DotData,
	DotMisc,
	Drivers,
	Objects,
	Views,
	Dialogs;

Const
	CClickTester = #7#8;

Type
	{ TClickTester }

	{Palette layout}
	{ 0 = Unclicked }
	{ 1 = Clicked }

	PClickTester = ^TClickTester;
	TClickTester = object(TStaticText)
		Clicked: Boolean;
		constructor Init(var Bounds: TRect; AText: String);
		function GetPalette: PPalette; virtual;
		procedure HandleEvent(var Event: TEvent); virtual;
		procedure Draw; virtual;
	End;

	{ TMouseDialog }

	PMouseDialog = ^TMouseDialog;
	TMouseDialog = object(TDialog2)
		MouseScrollBar: PScrollBar;
		OldDelay: Word;
		constructor Init;
		constructor Load(var S: TStream);
		procedure HandleEvent(var Event: TEvent); virtual;
		procedure Store(var S: TStream);
	End;

IMPLEMENTATION

{ TClickTester }

constructor TClickTester.Init(var Bounds: TRect; AText: String);
Begin
	TStaticText.Init(Bounds, AText);
	clicked:= False;
End;

Function TClickTester.GetPalette: PPalette;
Const
	P: String[Length(CClickTester)] = CClickTester;
Begin
	GetPalette := @P;
End;

Procedure TClickTester.HandleEvent(var Event: TEvent);
Begin
	TStaticText.HandleEvent(Event);
	if (Event.What = evMouseDown) then begin
		if Event.Double then begin
			Clicked:= not Clicked;
			DrawView;
		End;
		ClearEvent(Event);
	End;
End;

Procedure TClickTester.Draw;
Var
	B: TDrawBuffer;
	C: Byte;
Begin
	If Clicked Then
		c:= GetColor(2)
	Else
		c:= GetColor(1);
	MoveChar(B, ' ', C, Size.X);
	MoveStr(B, Text^, C);
	WriteLine(0, 0, Size.X, 1, B);
End;

{ TMouseDialog }

constructor TMouseDialog.Init;
Var
	r	: TRect;
	p	: PView;
Begin
	R.Assign(0, 0, 29, 15);
	TDialog2.Init(R, 'Mouse options');

	r.Assign(3,4, 26, 5);
	MouseScrollBar := New(PScrollBar, Init(R));
	MouseScrollBar^.SetParams(1, 1, 20, 5, 1);
	MouseScrollBar^.Options := MouseScrollBar^.Options or ofSelectable;
	MouseScrollBar^.SetValue(DoubleDelay);
	mouseScrollBar^.helpCTX:= 2101;
	Insert(MouseScrollBar);

	r.Assign(2, 2, 25, 3);
	p:= New(PLabel2, Init(R, '~M~ouse double click', MouseScrollBar));
	Insert(p);

	r.Assign(3,5, 25, 6);
	Insert(New(PStaticText, Init(r, 'Fast    Medium   Slow')));

	r.Assign(3,7, 25, 8);
	Insert(New(PClickTester, Init(R, 'Click here to test')));

	r.Assign(3,9,26,10);
	p:= New(PCheckBoxes2, Init(R,
			NewSItem('~L~eft handed mouse', nil)));
	p^.HelpCTX:= 2100;
	Insert(p);

	OldDelay:= DoubleDelay;

	GetExtent(r);
	r.Assign(r.b.x-24, r.b.y-3, r.b.x-14, r.b.y-1);
	p:= New(PButton2, Init(r, sOK, cmOK, bfDefault));
	p^.helpCTX:= 2018;
	Insert(p);

	GetExtent(r);
	r.Assign(r.b.x-13, r.b.y-3, r.b.x-3, r.b.y-1);
	p:= New(PButton2, Init(R, sCancel, cmCancel, bfNormal));
	p^.helpCTX:= 2019;
	Insert(p);

	SelectNext(False);
End;

constructor TMouseDialog.Load(var S: TStream);
Begin
	TDialog2.Load(S);
	GetSubViewPtr(S, MouseScrollBar);
	MouseScrollBar^.SetValue(DoubleDelay);
End;

Procedure TMouseDialog.HandleEvent(var Event: TEvent);
Begin
	TDialog2.HandleEvent(Event);
	Case Event.What of
		evCommand:
			If Event.Command = cmCancel Then
				DoubleDelay:= OldDelay;
		evBroadcast:
			If Event.Command = cmScrollBarChanged Then Begin
				DoubleDelay:= MouseScrollBar^.Value;
				ClearEvent(Event);
			End;
	End;
End;

Procedure TMouseDialog.Store(var S: TStream);
Begin
	TDialog2.Store(S);
	PutSubViewPtr(S, MouseScrollBar);
End;

end.
