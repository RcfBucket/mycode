{$X+}

Uses
	Strs,
	App,
	Drivers,
	Menus,
	Views,
	Objects,
	Dialogs,
	VisnObjs,
	Resobjs;

Const
	cmTest			= 100;
	cmClear			= 100;
	cmAddToList		= 101;
	cmChecks			= 102;
	cmMemoUpdate	= 103;

	id_Input			= 2;
	id_Rad			= 15;
	id_RadLabel		= 18;
	id_Check			= 16;
	id_CheckLabel	= 17;
	id_InputLabel	= 3;
	id_InputHist	= 19;
	id_Percent		= 6;
	id_ListBox		= 12;
	id_ListLabel   = 13;
	id_ListScroll	= 14;

	id_Memo			= 7;
	id_MemoLabel	= 8;
	id_MemoScroll	= 11;

	id_ListItem		= 100;
	id_ListText		= 101;
	id_Memory		= 1001;

Type
	TTestApp = Object(TApplication)
		procedure InitMenuBar; virtual;
		procedure InitStatusLine; virtual;
		procedure HandleEvent(Var event: TEvent); virtual;
	End;

	PTestMemo = ^TTestMemo;
	TTestMemo = Object(TResMemo)
		pBar	: PResPercentBar;
		{}
		constructor InitResource(AParent: PWindow;
								AnID, ALabelID, AHScrollID, AVScrollID: Integer);
		procedure HandleEvent(Var event: TEvent); virtual;
		procedure ClearAll;
		procedure ShowInfo;
	End;

	PTestDlg = ^TTestDlg;
	TTestDlg = Object(TResDialog)
		list	: PStringCollection;
		inp	: PResInput;
		lb		: PResListBox;
		memo	: PTestMemo;
		memTxt: PResStatic;
		chk	: PResCheckBoxes;
		rad	: PResRadioButtons;
		{}
		procedure SetupWindow; virtual;
		procedure ClearAll;
		procedure AddToList;
		procedure Checks;
		procedure ListSelect;
		procedure HandleEvent(Var event: TEvent); virtual;
		destructor Done; virtual;
		function Valid(command: Word): Boolean; virtual;
	End;

Constructor TTestMemo.InitResource(AParent: PWindow; AnID, ALabelID, AHScrollID, AVScrollID: Integer);
Begin
	pBar:= Nil;
	TResMemo.InitResource(AParent, AnID, ALabelID, AHScrollID, AVScrollID);
	pBar:= New(PResPercentBar, InitResource(AParent, id_Percent));
	pBar^.ChangeBar(bufSize, bufLen);
End;

Procedure TTestMemo.ShowInfo;
Var
	lines	: Word;
	d		: PResDialog;
	s		: PResStatic;
Begin
	lines:= NumLines;
	d:= New(PResDialog, Init('MEMOINFO', True));

	s:= New(PResStatic, InitResource(d, 101));
	s^.SetText(Long2Str(lines));
	s:= New(PResStatic, InitResource(d, 102));
	s^.SetText(Long2Str(bufSize));
	s:= New(PResStatic, InitResource(d, 103));
	s^.SetText(Long2Str(bufLen));
	s:= New(PResStatic, InitResource(d, 104));
	s^.SetText(Long2Str(Trunc(bufLen/bufSize*100.0))+'%');
	s:= New(PResStatic, InitResource(d, 105));
	If modified Then
		s^.SetText('Yes')
	Else
		s^.SetText('No');

	s:= New(PResStatic, InitResource(d, 106));
	s^.SetText(Long2Str(1+curPos.y) + ' : ' + Long2Str(1+curPos.x));

	DeskTop^.ExecView(d);
	Dispose(d, Done);
End;

Procedure TTestMemo.HandleEvent(Var event: TEvent);
Begin
	TResMemo.HandleEvent(event);
	If pBar <> Nil Then
		pBar^.ChangeBar(bufSize, bufLen);
End;

Procedure TTestMemo.ClearAll;
Begin
	SetSelect(0, bufLen, True);
	DeleteSelect;
	pBar^.ChangeBar(bufSize, bufLen);
End;

Function TTestDlg.Valid(command: Word): Boolean;
Begin
	If TResDialog.Valid(command) Then
		Valid:= True
	Else
		Valid:= False;
End;

Destructor TTestDlg.Done;
Begin
	Dispose(list, Done);
	TResDialog.Done;
End;

Procedure TTestDlg.SetupWindow;
Begin
	memTxt:= New(PResStatic, InitResource(@Self, id_Memory));
	inp:= New(PResInput, InitResource(@self, id_Input, id_InputLabel, id_InputHist));
	rad:= New(PResRadioButtons, InitResource(@self, id_Rad, id_RadLabel));
	chk:= New(PResCheckBoxes, InitResource(@self, id_Check, id_CheckLabel));
	memo:= New(PTestMemo, InitResource(@self, id_Memo, id_MemoLabel, id_MemoScroll, 0));
	list:= New(PStringCollection, Init(10, 8));
	list^.duplicates:= True;
	lb:= New(PResListBox, InitResource(@self, id_ListBox, id_ListLabel, id_ListScroll));
	lb^.NewList(list);
	inp^.Select;
End;

Procedure TTestDlg.ClearAll;
Var
	tstr	: String;
	w		: Word;
Begin
	list^.FreeAll;
	lb^.SetRange(0);
	tstr:= '';
	inp^.SetData(tstr);

	w:= 0;
	chk^.SetData(w);
	rad^.SetData(w);
	memo^.ClearAll;
	ReDraw;
	inp^.Select;
End;

Procedure TTestDlg.AddToList;
Var
	tstr	: String;
Begin
	GetDlgItemText(id_Input, tstr);
	If tstr = '' Then
		Exit;
	list^.Insert(NewStr(tstr));
	lb^.SetRange(list^.count);
	lb^.FocusItem(0);
	lb^.DrawView;
End;

Procedure TTestDlg.ListSelect;
Var
	d		: PResDialog;
	s		: PResStatic;
Begin
	d:= New(PResDialog, Init('LISTSELECT', True));
	s:= New(PResStatic, InitResource(d, id_ListItem));
	s^.SetText(Long2Str(lb^.Focused));
	s:= New(PResStatic, InitResource(d, id_ListText));
	s^.SetText(lb^.GetText(lb^.Focused, 255));
	DeskTop^.ExecView(d);
	Dispose(d, Done);
End;

Procedure TTestDlg.Checks;
Var
	d		: PResDialog;
	w		: Word;
	s		: PResStatic;
Begin
	d:= New(PResDialog, Init('CHECKS', True));
	chk^.GetData(w);
	s:= New(PResStatic, InitResource(d, 101));
	If w and 1 <> 0 Then s^.SetText('Checked');
	s:= New(PResStatic, InitResource(d, 102));
	If w and 2 <> 0 Then s^.SetText('Checked');
	s:= New(PResStatic, InitResource(d, 103));
	If w and 4 <> 0 Then s^.SetText('Checked');

	rad^.GetData(w);
	s:= New(PResStatic, InitResource(d, 201 + w));
	s^.SetText('Selected');

	Desktop^.ExecView(d);
	Dispose(d, Done);
End;

Procedure TTestDlg.HandleEvent(Var event: TEvent);
Begin
	TResDialog.HandleEvent(event);
	If memTxt <> Nil Then
		memTxt^.SetText(InsCommas(Long2Str(Memavail)));
	If event.what and evCommand <> 0 Then Begin
		Case event.command of
			cmClear		: ClearAll;
			cmAddToList	: AddToList;
			cmChecks		: Checks;
			Else
				Exit;
		End;
		ClearEvent(event);
	End
	Else If event.what and evBroadCast <> 0 Then Begin
		Case event.command of
			cmListItemSelected	: 	ListSelect;
			Else
				Exit;
		End;
		ClearEvent(event);
	End
	Else If (event.what = evKeyDown) and (event.KeyCode = kbF3) Then
		memo^.ShowInfo;
End;

Procedure ErrorMsg(msg1: String);
Var
	d		: PDialog;
	r		: TRect;
	v		: PView;
Begin
	r.Assign(0,0,60,8);
	d:= New(PDialog, Init(r, 'Error'));
	d^.options:= d^.options or ofCentered;

	r.Assign(2,2, 58,4);
	d^.Insert(New(PStaticText, Init(r, ^C+msg1)));

	r.Assign(0,5, 10,7);
	v:= New(PButton2, Init(r, 'OK', cmOK, bfDefault));
	v^.options:= v^.options or ofCenterX;
	d^.Insert(v);

	Application^.ExecView(d);
	Dispose(d, Done);
End;

Procedure Test;
Var
	d		: PTestDlg;
Begin
	d:= New(PTestDlg, Init('EXAMPLE', True));
	If Application^.ValidView(d) <> Nil Then Begin
		DeskTop^.ExecView(d);
		Dispose(d, Done);
	End
	Else
		ErrorMsg('Cannot load resource');
End;

Procedure TTestApp.InitMenuBar;
Var
	R	: TRect;
Begin
	GetExtent(R);
	R.B.Y := R.A.Y + 1;
	menuBar:= New(PMenuBar, Init(R, NewMenu(
		NewItem('~T~est', '', 0, cmTest, 0,
		Nil)
	)));
End;

Procedure TTestApp.InitStatusLine;
Var
	r	: TRect;
Begin
	GetExtent(R);
	R.A.Y := R.B.Y - 1;
	StatusLine:= New(PStatusLine, Init(R,
		NewStatusDef(0, $FFFF,
			NewStatusKey('', kbAltF3, cmClose,
			NewStatusKey('', kbF10, cmMenu,
			NewStatusKey('', kbCtrlF5, cmResize,
			NewStatusKey('', kbF6, cmNext,
			NewStatusKey('', kbShiftF6, cmPrev,
			NewStatusKey('~Alt-X~ Exit', kbAltX, cmQuit,
			nil)))))),
		nil)
	));
End;

Procedure TTestApp.HandleEvent(Var event: TEvent);
Begin
	TApplication.HandleEvent(event);
	If (event.what and evCommand <> 0) Then Begin
		Case event.command of
			cmTest	:	Test;
			Else
				Exit;
		End;
		ClearEvent(event);
	End;
End;

Var
	testApp		: TTestApp;

Begin
	InitResourceFile('RESEXAMP');
	testApp.Init;
	testApp.Run;
	testApp.Done;
	CloseResourceFile;
End.
