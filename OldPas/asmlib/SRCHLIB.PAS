Unit SrchLib;

{- search functions }

INTERFACE

function Search(var Buffer; BufLength : Word;
                var Match; MatLength : Word) : Word;
 {-Search through Buffer for Match. BufLength is length of range to search.
   MatLength is length of string to match. Returns number of bytes searched
   to find Match, $FFFF if not found.}

function SearchUC(var Buffer; BufLength : Word;
                  var Match; MatLength : Word) : Word;
 {-Search through Buffer for Match, CASE-INSENSITIVE.
   Otherwise same as Search.}


IMPLEMENTATION

{$L search.obj}
function Search(var Buffer; BufLength : Word;
                var Match; MatLength : Word) : Word;
	External;

function SearchUC(var Buffer; BufLength : Word;
                  var Match; MatLength : Word) : Word;
	External;

END.
