Unit MoveLib;

INTERFACE

Procedure MoveFast(var Src, Dest; Count : Word);
{- Move Count bytes from Src to Dest, moving by word where possible}

IMPLEMENTATION

{$L movefast}
Procedure MoveFast(var Src, Dest; Count : Word); external;

END.
