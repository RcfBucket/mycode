{$F+,A+}

Unit InpLib;

INTERFACE

Uses
	kbdI,
	Strs,
	TpCrt;

Type
	InpCaseType = (inpLoCase, inpNoCase, inpUpCase);

Const
	hardspace	= Chr(225);		{- used in masks to denote blank space w/o input}

	AllSym		= '~`!@#$%^&*()-_=+[]{}|\;:''"<>,./?';
	AlphaOnly	= 'ABCDEFGHIJKLMNOPQRSTUVWXYZ ';
	NumerOnly	= '1234567890';
	FileSym		= '~`!@#$%^&()-_{}''';
	foriegnOnly	= ' กขฃคฅฆง';

	inpAutoClear	: Boolean 	= True;     { autoclear input field }
	inpCase			: InpCaseType = inpNoCase;
	inpFillChar		: Char		= ' ';	{ input pad fill character }
	inpColor			: Byte		= 7;		{ color of input }
	inpScrlColor	: Byte		= 7;		{ color of scroll lft/rght indicators }
	inpACcolor		: Byte		= 7*16;	{ color of auto clear field }
	inpResetColor	: Byte		= 7;		{ color to reset input to, 0 for no reset }
	inpShowScroll	: Boolean	= False;	{ show scroll chars if needed }
	inpScrlLeft		: Char		= 'ฎ';
	inpScrlRight	: Char		= 'ฏ';
	inpLegal			: String		= '';		{ legal characters }
	inpExitVals		: String		= '';		{ exit keys for input }
	inpLeftJust		: Boolean	= True;	{ left justify input (takes precedence
														over right just }
	inpRightJust	: Boolean	= False;	{ right justify input }
	inpTrim			: Boolean	= True;
	inpInsert		: Boolean	= True;
	inpAutoExit		: Boolean	= False;	{ auto exit on end of fields }

Procedure ClearInpParams;
Procedure SetInputColors(inp, ac, scrl, resetclr: Byte);

Function EditString(	x, y				: Byte;
							maxlength,
							boxlen 			: Integer;
							Var retstr		: String;
							mask				: String
						): Char;
Procedure ShowInpCursor;

Var
	iPosi	: Integer;
	iBposi: Integer;

IMPLEMENTATION

Function Min(x,y: Integer): Integer;
Begin
	If x < y Then
		Min:= x
	Else
		Min:= y;
End;

Function Max(x,y: Integer): Integer;
Begin
	If x > y Then
		Max:= x
	Else
		Max:= y;
End;

Procedure ShowInpCursor;
{- show a cursor based on value of inpCursor }
Begin
	If inpInsert Then
		FatCursor
	Else
		BlockCursor;
End;

Function EditString(	x, y				: Byte;
							maxlength,
							boxlen 			: Integer;
							Var retstr		: String;
							mask				: String
						): Char;
Const
	alphanum = ['A'..'Z','a'..'z','0'..'9'];

Var
	key								: Char;			{ keyboard codes }
	len		 						: Integer;		{ true length of input }
	posi								: Integer;
	boxpos							: Integer;
	k,k2,temp 						: Integer;		{ working vars }
	returnKey						: Char;			{ value on return }
	space								: Boolean;		{ true if spaces are allowed }
	done								: Boolean;
	cf									: Boolean;		{ clear flag }

	Procedure CheckScroll;
	Begin
		If inpShowScroll Then Begin
			If (posi-boxpos+1 > 1) Then
				FastWrite(inpScrlLeft, y, x-1, inpscrlColor)
			Else
				FastWrite(' ', y, x-1, inpscrlcolor);
			If (posi+(boxlen-boxpos) < len) Then
				FastWrite(inpScrlRIght, y, x+boxlen, inpscrlcolor)
			Else
				FastWrite(' ', y, x+boxlen, inpscrlcolor)
		End;
	End;

Begin
	posi:= iPosi;
	boxpos:= iBPosi;
	If (maxlength < boxlen) or (mask <> '') Then
		boxlen:= maxlength;

	If (posi > maxlength) Then
		posi:= maxlength;

	If (boxpos > boxlen) Then
		boxpos:= boxlen;

	If (inpLegal = '') or (Pos(' ', inpLegal) > 0) Then
		space:= True
	Else
		space:= False;

	If mask = '' Then Begin
		len:= Min(Length(retstr), maxlength);
		retstr:= PadCh(Copy(retstr,1,maxlength), inpFillChar, maxlength);
	End
	Else Begin
		retstr:= Pad(Copy(retstr,1,maxlength), maxlength);
		mask:= Pad(Copy(mask,1,maxlength), maxlength);
		len:= maxlength;
		done:= False;
		For k:= maxlength Downto 1 Do Begin
			If mask[k] = hardspace Then
				retstr[k]:= ' '
			Else
				If mask[k] <> ' ' Then
					retstr[k]:= mask[k]
				Else
					If (retstr[k] = ' ') and not done Then
						retstr[k]:= inpFillChar
					Else
						done:= True;
		End;
	End;

	If mask = '' Then Begin
		If posi = 0 Then Begin
			If inpAutoClear Then Begin
				posi:= 1;
				boxpos:= 1;
			End
			Else Begin
				If posi = 0 Then
					posi:= Min(len+1, maxlength)
				Else
					posi:= Min(posi, maxlength);
				boxpos:= Min(posi, boxlen);
			End;
		End;
   End
	Else Begin
		If inpAutoClear Then Begin
			posi:= Pos(' ',mask);
			boxpos:= posi;
		End
		Else
			If (posi = 0) or (mask[posi] <> ' ') Then Begin
				posi:= maxlength;
				While ((mask[posi] <> ' ') or (retstr[posi] = inpFillChar)) and
							(posi > 1) Do
					Dec(posi);
				k:= posi;
				While (retstr[k] <> inpFillChar) and (k < maxlength) Do
					Inc(k);
				If mask[k] = ' ' Then
					posi:= k;
				boxpos:= posi;
			End;
	End;

	ShowInpCursor;

	If inpAutoClear and (len > 0) Then
		k2:= inpACcolor
	Else
		k2:= inpColor;

	FastWrite(PadCh(Copy(retstr,posi-boxpos+1,boxLen), inpFillChar, boxLen),
					y, x, k2);

	CheckScroll;

	done:= False;
	If iposi = 0 Then
		cf:= False
	Else
		cf:= True;

	Repeat
		GotoXY(x+boxpos-1, y);
		key:= GetAKey;
		If key = SpecialKey Then Begin
			key:= GetSpecialKey;
			If (key = k_Enter) or (key = k_Esc) or (Pos(key, inpExitVals) > 0) Then Begin
				{- any special exit keys here }
				returnKey:= key;
				done:= True;
			End
			Else Begin
				Case key Of
					k_Left:	Begin
									If inpAutoExit and (posi = 1) Then Begin
										returnKey:= k_Left;
										done:= True;
									End
									Else Begin
										cf:= True;
										If mask <> '' Then Begin
											If Pos(' ',mask) < posi Then Begin
												Dec(posi);
												While (mask[posi] <> ' ') Do
													Dec(posi);
												boxpos:= posi;
											End;
										End
										Else Begin
											If (posi > 1) Then
												Dec(posi);
											If (boxpos > 1) Then
												Dec(boxpos);
										End;
									End;
								End;
					k_Right:	Begin
									If inpAutoExit and (posi = MaxLength) Then Begin
										returnKey:= k_Right;
										done:= True;
									End
									Else Begin
										cf:= True;
										If mask <> '' Then Begin
											If (posi < maxlength) Then Begin
												k:= posi+1;
												While (k < maxlength) and (mask[k] <> ' ') Do
													Inc(k);
												If mask[k] = ' ' Then Begin
													boxpos:= k;
													posi:= k;
												End;
											End;
										End
										Else Begin
											If (posi < maxlength) Then
												Inc(posi);
											If (boxpos < boxlen) Then
												Inc(boxpos);
											If not space Then Begin
												posi:= Min(len+1, posi);
												boxpos:= Min(len+1, boxpos);
											End;
										End;
									End;
								End;
					k_Home:	Begin
									cf:= True;
									If mask <> '' Then Begin
										posi:= Pos(' ',mask);
										boxpos:= posi;
									End
									Else Begin
										posi:= 1;
										boxpos:= 1;
									End;
								End;
					k_End:		Begin
										cf:= True;
										If mask = '' Then Begin
											posi:= Min(maxlength, len+1);
											boxpos:= Min(boxlen, len+1);
										End
										Else Begin
											posi:= maxlength;
											While ((mask[posi]<>' ') or (retstr[posi]=inpFillChar))
													and (posi > 1) Do
												Dec(posi);
											k:= posi;
											While (retstr[k] <> inpFillChar) and (k < maxlength) Do
												Inc(k);
											If mask[k] = ' ' Then
												posi:= k;
											boxpos:= posi;
										End;
									End;
					k_Ins:		Begin
										cf:= True;
										inpInsert:= Not inpInsert;
										ShowInpCursor;
									End;
					k_Del:		Begin
										cf:= True;
										If mask = '' Then Begin
											If (posi <= len) Then Begin
												Delete(retstr, posi, 1);
												retstr:= retstr + inpFillChar;
												Dec(len);
											End;
										End
										Else Begin
											k:= posi;
											k2:= posi;
											While (k < maxlength) Do Begin
												Inc(k);
												If (mask[k] = ' ') Then Begin
													retstr[k2]:= retstr[k];
													k2:= k;
												End;
											End;
											If (mask[k2] = ' ') Then
												retstr[k2]:= inpFillChar;
										End;
									End;
					k_BS:			Begin
										cf:= True;
										If posi > 1 Then Begin
											If mask = '' Then Begin
												If posi <= len+1 Then Begin
													If posi = boxpos Then
														boxpos:= Max(boxpos - 1, 1);
													Dec(posi);
													Delete(retstr, posi, 1);
													retstr:= retstr + inpFillChar;
													Dec(len);
												End
												Else Begin
													If posi = boxpos Then
														boxpos:= Max(boxpos-1, 1);
													Dec(posi);
												End;
											End
											Else Begin
												k:= posi - 1;
												While (k > 0) and (mask[k] <> ' ') Do
													Dec(k);
												If (k > 0) Then Begin
													k2:= posi;
													posi:= k;
													boxpos:= Max(posi, 1);
													While (k <= maxlength) Do Begin
														If mask[k] = ' ' Then Begin
															If (mask[k2] = ' ') and (k2 <= maxlength) Then Begin
																retstr[k]:= retstr[k2];
																Repeat
																	Inc(k2);
																Until (mask[k2] = ' ') or (k2 = maxlength);
															End
															Else
																retstr[k]:= inpFillChar;
														End;
														Inc(k);
													End;
												End;
											End;
										End;
									End;
					k_ctlLeft:    Begin
										cf:= True;
										If mask = '' Then Begin
											If (posi > 1) Then Begin
												k:= posi - 1;
												While ( (retstr[k] = inpFillChar) or not (retstr[k] in alphanum))
														and (k > 1) Do
													Dec(k);
												Dec(k);
												While (retstr[k] in alphanum) and (k > 1) Do
													Dec(k);
												If k = 1 Then Begin
													boxpos:= 1;
													posi:= 1;
												End
												Else Begin
													boxpos:= Max(boxpos - (posi - (k+1)), 1);
													posi:= k+1;
												End;
											End;
										End
										Else Begin
											If posi > 1 Then Begin
												Dec(posi);
												While (mask[posi] <> ' ') and
														(posi > 1) Do
													Dec(posi);
												While (mask[posi] = ' ') and
														(posi > 1) Do
													Dec(posi);
												While (mask[posi] <> ' ') Do
													Inc(posi);
												boxpos:= posi;
											End;
										End;
									End;
					k_ctlRight:	Begin
										cf:= True;
										If mask = '' Then Begin
											If (posi < len) Then Begin
												k:= posi + 1;
												While (retstr[k] in alphanum) and (k < len) Do
													Inc(k);
												Inc(k);
												While not (retstr[k] in alphanum) and (k < len+1) Do
													Inc(k);
												boxpos:= Min(boxpos + (k - posi), boxlen);
												posi:= Min((Min(len+1, (maxlength))), k);
											End;
										End
										Else Begin
											If posi < maxlength Then Begin
												Inc(posi);
												While (mask[posi] <> ' ') and
														(posi < maxlength) Do
													Inc(posi);
												While (mask[posi] = ' ') and
														(posi < maxlength) Do
													Inc(posi);
												While mask[posi] <> ' ' Do
													Dec(posi);
												boxpos:= posi;
											End;
										End;
									End;
					k_CtlHome:	If mask = '' Then Begin
										retstr:= Copy(retstr,posi,255);
										len:= Max(len - (posi - 1), 0);
										retstr:= PadCh(retstr, inpFillChar, maxlength);
										posi:= 1;
										boxpos:= 1;
										cf:= True;
									End
									Else Begin
										cf:= True;
										For k:= 1 to maxlength Do Begin
											If mask[k] = ' ' Then Begin
												If (mask[posi] = ' ') and (posi <= maxlength) Then Begin
													retstr[k]:= retstr[posi];
													Repeat
														Inc(posi);
													Until (mask[posi] = ' ') or (posi = maxlength);
												End
												Else
													retstr[k]:= inpFillChar;
											End;
										End;
										posi:= Pos(' ',mask);
										boxpos:= posi;
									End;
					k_ctlEnd:	Begin
										cf:= True;
										For k:= posi To maxlength Do Begin
											If (mask = '') or (mask[k] = ' ') Then
												retstr[k]:= inpFillChar;
											If (mask = '') Then
												len:= posi - 1;
										End;
									End;
					k_ctlBS:		Begin
										cf:= True;
										For k:= 1 to maxlength Do
											If (mask = '') or (mask[k] = ' ') Then
												retstr[k]:= inpFillChar;
										If mask = '' Then Begin
											len:= 0;
											posi:= 1;
											boxpos:= 1;
										End
										Else Begin
											posi:= Pos(' ', mask);
											boxpos:= posi;
										End;
									End;
				End;	{- case }
			End;
		End
		Else Begin
			If (inpCase <> inpNoCase) Then Begin
				If inpCase = inpLoCase Then
						key:= LoCaseMac(key)
					Else
						key:= UpCaseMac(key);
			End;
			If ((key > #0) and (inpLegal = '')) or
				(Pos(UpCaseMac(key), inpLegal) > 0)  Then Begin
				If not cf and inpAutoClear Then Begin    { autoclear }
					If mask = '' Then Begin
						FillChar(retstr[1], maxlength, inpFillChar);
						len:= 0;
						posi:= 1;
						boxpos:= 1;
					End
					Else Begin
						For k:= 1 to maxlength Do
							If (mask[k] = ' ') Then
								retstr[k]:= inpFillChar;
						posi:= Pos(' ',mask);
						boxpos:= posi;
					End;
					cf:= True;
				End;
				If (inpInsert) and (mask = '') Then Begin		{ insert mode }
					If len <> maxlength Then Begin
						Delete(retstr, Length(retstr), 1);
						Insert(key, retstr, posi);
						retstr[0]:= Chr(maxlength);
						If len+1 < posi Then Begin
							FillChar(retstr[len+1], posi-len-1, ' ');
							len:= posi;
						End
						Else Begin
							If len < maxlength Then
								Inc(len);
						End;
						posi:= Min(posi+1,maxlength);
						boxpos:= Min(boxpos+1,boxlen);
					End
					Else Begin
						retstr[posi]:= key;
					End;
				End
				Else Begin							{overwrite mode}
					If mask = '' Then Begin
						retstr[posi]:= key;
						If len+1 < posi Then Begin
							FillChar(retstr[len+1], posi-len-1, ' ');
							len:= posi;
						End;
						If posi = len+1 Then
							Inc(len);
						If posi < maxlength Then
							Inc(posi);
						boxpos:= Min(boxpos+1, boxlen);
					End
					Else Begin						{ mask input }
						If not inpInsert Then Begin	{ overwrite }
							retstr[posi]:= key;
							If posi = len+1 Then
								Inc(len);
							If (posi < maxlength) Then Begin
								k:= posi+1;
								While (k < maxlength) and (mask[k] <> ' ') Do
									Inc(k);
								If mask[k] = ' ' Then Begin
									boxpos:= k;
									posi:= k;
								End;
							End;
							If Pos(inpFillChar, retstr) < posi Then
								For k:= posi-1 Downto 1 Do
									If (retstr[k] = inpFillChar) Then
										retstr[k]:= ' ';
						End
						Else Begin			{ insert mode w/mask }
							k:= maxlength;
							While (mask[k] <> ' ') Do
								Dec(k);
							If retstr[k] <> inpFillChar Then
								k:= posi
							Else Begin
								k2:= maxlength + 1;
								For k:= maxlength Downto posi Do Begin
									If (mask[k] = ' ') Then Begin
										If (k2 = maxlength + 1) Then
											k2:= k
										Else Begin
											retstr[k2]:= retstr[k];
											If (k = posi) Then Begin
												boxpos:= k2;
												posi:= k2;
											End;
											k2:= k;
										End;
									End;
								End;
							End;
							retstr[k]:= key;
							If Pos(inpFillChar, retstr) < posi Then
							For k:= posi-1 Downto 1 Do
								If (retstr[k] = inpFillChar) Then
									retstr[k]:= ' ';
						End;
					End;
				End;
			End;
      End;
		FastWrite(PadCh(Copy(retstr, posi-boxpos+1, boxlen), inpFillChar, boxlen),
						y,x, inpColor);
		CheckScroll;
   Until done;
	If inpshowscroll Then Begin
		FastWrite(' ', y, x-1, inpscrlcolor);
		FastWrite(' ', y, x+boxlen, inpscrlcolor)
	End;
	HiddenCursor;
	retstr:= Replace(retstr, inpFillChar, ' ');
	If inpTrim and (mask = '') Then
		retstr:= TrimTrail(retstr);
	If inpRightJust and (retStr <> '') and (mask = '') Then
		retStr:= LeftPadCh(retstr, ' ', maxLength);
	If inpLeftJust and (mask = '') Then
		retstr:= TrimLead(retstr);
	If inpResetColor = 0 Then
		k:= inpColor
	Else
		k:= inpResetColor;
	FastWrite(Pad(Copy(retstr, 1, boxlen), boxlen), y,x, k);
   iPosi:= posi;
	iBposi:= boxpos;
	EditString:= returnKey;
End;

Procedure ClearInpParams;
Begin
	inpAutoClear:= True;
	inpCase:= inpNoCase;
	inpFillChar:= #250;
	inpShowScroll:= False;
	inpScrlRight:= 'ฏ';
	inpScrlLeft:= 'ฎ';
	inpLegal:= '';
	inpExitVals:= '';
	iPosi:= 0;
	iBposi:= 0;
	inpLeftJust:= True;
	inpLeftJust:= False;
	inpTrim:= True;
End;

Procedure SetInputColors(inp, ac, scrl, resetclr: Byte);
Begin
	inpColor:= inp;
	inpACcolor:= ac;
	inpscrlColor:= scrl;
	inpResetColor:= resetclr;
End;

Begin
	ClearInpParams;
	SetInputColors(7,7,7,0);
END.
