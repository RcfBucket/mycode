.model tpascal

PUBLIC Pack6BitKey
PUBLIC Unpack6BitKey

.code

;****************************************************** Macros and Equates

SetZero	MACRO Reg
	xor	reg,reg			;reg = 0
	ENDM

ChkZero	MACRO Reg
	or      reg,reg			;reg = 0?
	ENDM

BitsPerChar     equ     dh
BPCdelta        equ     dl
BitCount        equ     bh
BitsLeft        equ     bh
CharsLeft       equ     bl

;****************************************************** Pack6

Pack6   PROC NEAR

	CMP	AL,'z'                  ;greater than 'z'?
	JA 	P6zeroIt                ;map to 0
	CMP	AL,'a'                  ;less than 'a'?
	JB 	P6checkAtoZ             ;check for 'A'-'Z'
	SUB	AL,60                   ;in range 'a'-'z' -- map to 37-62
	RET

P6checkAtoZ:
        CMP     AL,'A'                  ;less than 'A'?
        JB      P6check0to9             ;check for '0'-'9'
        CMP     AL,'Z'                  ;greater than 'Z'?
        JA      P6zeroIt                ;map to 0
        SUB     AL,54                   ;in range 'A'-'Z' -- map to 11-36
        RET

P6check0to9:
        CMP     AL,'0'                  ;less than '0'?
        JB      P6zeroIt                ;map to 0
        CMP     AL,'9'                  ;greater than '9'?
        JA      P6zeroIt                ;map to 0
        SUB     AL,47                   ;in range '0'-'9' -- map to 1-10
        RET

P6zeroIt:
        SetZero AL
        RET

Pack6   ENDP

;****************************************************** Pack6BitKey

;function Pack6BitKey(Src : string; Len : Byte) : string;
;Pack the Source string into sequences of 6 bits (max length = Len)

PLen    EQU     BYTE  PTR SS:[BX+4]
PSrc    EQU     DWORD PTR SS:[BX+6]
PDest   EQU     DWORD PTR SS:[BX+10]

Pack6BitKey	PROC FAR

        MOV     BitsPerChar,6           ;number of bits per character = 6
        MOV     BPCdelta,2              ;BPCdelta = 8-6

        MOV     BX,SP                   ;Set up stack frame
        PUSH    DS                      ;Save DS and BP
        PUSH    BP
        CLD                             ;Go forward
        LDS     SI,PSrc                 ;DS:SI -> Src[0]
        LES     DI,PDest                ;ES:DI -> Result[0]
        SetZero AH                      ;AX = Len
        MOV     AL,PLen
        STOSB                           ;Set the result's length byte
        MOV     BP,DI                   ;BP = Ofs(Result[Len+1])
        ADD     BP,AX
        SetZero AH                      ;AH = Next byte to plug in
        LODSB                           ;AL = Length(Src)
        MOV     CharsLeft,AL            ;BL = # of chars left in Src
        SetZero BitCount                ;BH = # of bits in AH

PMain:
        CMP     DI,BP                   ;string full?
        JAE     PExit                   ;if so, we're done
        ChkZero CharsLeft               ;any characters left in Src?
        JZ      PFinish
        LODSB                           ;load next character
        DEC     CharsLeft               ;decrement loop counter
        CALL    Pack6						 ;pack character in AL

        MOV     CL,BPCdelta             ;shift bottom of AL into top
        SHL     AL,CL
        SetZero CH                      ;set loop count
        MOV     CL,BitsPerChar
PLoop:
        CMP     BitCount,8              ;do we have 8 bits yet?
        JB      PNext
        MOV     ES:[DI],AH              ;store character in AH at ES:DI
        INC     DI                      ;advance string index
        SetZero BitCount                ;Reset BH to 0
PNext:
        ROL     AL,1                    ;rotate high bit of AL into CF
        RCL     AH,1                    ;rotate it from CF into low bit of AH
        INC     BitCount                ;increment counter
        LOOP    PLoop                   ;next bit

        JMP     SHORT PMain             ;do it again

PFinish:
        ChkZero BitCount                ;anything left in AH?
        JZ      PZeroPad                ;if not, pad with 0's
        MOV     CL,8                    ;move the bits in AH up into place
        SUB     CL,BitCount
        SHL     AH,CL
        MOV     ES:[DI],AH              ;store character in AH at ES:DI
        INC     DI                      ;advance string index

PZeroPad:                               ;pad to end of string with zeros
        MOV     CX,BP                   ;CX = # of zeros to write
        SUB     CX,DI
        SetZero AL                      ;AL = 0
        REP     STOSB                   ;fill with zeros if CX > 0

PExit:
        POP     BP                      ;restore DS and BP
        POP     DS
        RET     6                       ;remove parameters and return

Pack6BitKey	ENDP

;****************************************************** Unpack6

Unpack6 PROC NEAR

        ChkZero AH                      ;AH = 0?
        JZ      U6space                 ;if so, return a space
        CMP     AH,37                   ;less than 37?
        JB      U6checkAtoZ             ;check for 'A'-'Z'
        ADD     AH,60                   ;in range 37-62 -- map to 'a'-'z'
        RET

U6checkAtoZ:
        CMP     AH,11                   ;less than 11?
        JB      U60to9
        ADD     AH,54                   ;in range 37-62 -- map to 'a'-'z'
        RET

U60to9:
        ADD     AH,47                   ;in range 1-10 -- map to '0'-'9'
        RET

U6space:
        MOV     AH,' '
        RET

Unpack6 ENDP

;****************************************************** Unpack6BitKey

;function Unpack6BitKey(Src : string) : string;
;Unpack a key created by Pack6BitKey

USrc    EQU     DWORD PTR SS:[BX+4]
UDest   EQU     DWORD PTR SS:[BX+8]

Unpack6BitKey	 PROC FAR

        MOV     BitsPerChar,6           ;number of bits per character = 5

        MOV     BX,SP                   ;Set up stack frame
        PUSH    DS                      ;Save DS
        CLD                             ;Go forward
        LDS     SI,USrc                 ;DS:SI -> Src[0]
        LES     DI,UDest                ;ES:DI -> Result[0]
        LODSB                           ;AL = Length(Src)
        MOV     CharsLeft,AL            ;BL = # of chars left in Src
        SetZero BitsLeft                ;BH = # of bits in AL
        PUSH    DI                      ;save offset of Result[0]

UMain:
        ChkZero CharsLeft               ;any characters left in Src?
        JZ      UFinish

        ;rotate next packed char into AH

        SetZero CH                      ;set loop count
        MOV     CL,BitsPerChar
        SetZero AH                      ;AH is empty
ULoop:
        ChkZero BitsLeft                ;any bits left in AL?
        JNZ     UNext
        LODSB                           ;reload AL
        DEC     CharsLeft               ;decrement counter
        MOV     BitsLeft,8              ;Reset BH to 8
UNext:
        ROL     AL,1                    ;rotate high bit of AL into CF
        RCL     AH,1                    ;rotate it from CF into low bit of AH
        DEC     BitsLeft                ;decrement bit counter
        LOOP    ULoop                   ;next bit

        CALL    Unpack6			;unpack char in AH
        INC     DI                      ;advance string index
        MOV     ES:[DI],AH              ;store the unpacked character
        JMP     SHORT UMain             ;do it again

UFinish:
        CMP     BitsLeft,BitsPerChar    ;full character left in AL?
        JB      UExit                   ;if not, we're done
        MOV     CL,8                    ;move the bits in AL down into place
        SUB     CL,BitsLeft
        SHR     AL,CL
        MOV     AH,AL                   ;move it into AH
        CALL    Unpack6			;unpack char in AH
        INC     DI                      ;advance string index
        MOV     ES:[DI],AH              ;store character in AH at ES:DI

UExit:
        MOV     AX,DI                   ;AX = DI
        POP     DI                      ;Restore pointer to length byte
        SUB     AX,DI                   ;Get length of our string
        STOSB                           ;Set length byte
        POP     DS                      ;restore DS
        RET     4                       ;remove parameter and return

Unpack6BitKey	ENDP

        END
