.MODEL	TPASCAL

PUBLIC	StripLast

wpt	equ	word ptr

.CODE
;========================================================== StripLast
;
;  Function StripLast(fname: String): String;
;
;  Removes the file-name of fname and returns result. Trailing '\' will be
;  removed.
;--------------------------------------------------------------------
;
;     2       4        4       4
;   | BP | RetAddr | fName | Result |	<----- Stack Usage
;   `-------------------------------'
;                  ^=+6    ^=+10
;--------------------------------------------------------------------
StripLast	PROC FAR

; Parameters
fname		equ	dword ptr	ss:[bp+6]
resultPtr	equ	dword ptr	ss:[bp+10]
pSize		equ	4 

	push	bp
	mov	bp,sp			; get pointer into stack
	push	ds			; save Turbo's DS in DX

	std				; direction <-
	lds	si,fname		; DS:SI -> sPtr
	xor	ah,ah			; clear AH
	lodsb				; get length of string into AL
	mov	cx,ax			; length of fname -> CX
	jcxz	done			; nothing to convert
	add	si,ax			; si -> end of string
	inc	si			;

	lodsb				; get last byte of fname
	cmp	al,"\"			; is it a "\"
	je	startStrip		; yes, remove trailing \
	cmp	al,":"			; is is a ":"
	je	done			; yes, no translation
startStrip:
	dec	cx			; yes, remove trailing "\"
	jcxz	done			; is length 0?
StripLoop:
	lodsb				; get next char
	cmp	al,"\"			; is it a "\"
	je	done1			; yes, remove it and quit
	cmp	al,":"			; is it a ":"
	je	done 			; yes, quit
	loop	StripLoop		; not done yet, CX=CX-1 and loop
; Nothing was found here
	jmp	done
done1:
	lodsb
	cmp	al,":"
	je	done
	dec	cx			; remove final "\" if prev char<>":"
done:
	cld				; go forward now ->
	les	di,resultPtr		; get address of function result
	mov	si,WPT fname		; get string address
	inc	si			; skip the length byte

	mov	al,cl			; length in AL
	stosb
	rep	movsb			; and now move the rest of the string

	pop	ds
	pop	bp
	ret	pSize
StripLast	ENDP


	END
