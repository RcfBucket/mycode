using System;
using System.Collections;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.Caching;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using PHI.UI.Eureka;
using PHI.DataMgmt.Constants;
using Microsoft.Win32;

namespace Dialogs
{
	public class DialogSelectGeneric : System.Web.UI.Page
	{
	  private string controlName = "";
	  private string pageFrom = "";
	  private int type = DialogType.County;
    protected System.Web.UI.HtmlControls.HtmlInputText searchText;
	  private Lookup lookup;
	  	  
		private void Page_Load(object sender, System.EventArgs e)
		{
		  //if (Request.QueryString["xmlBool"] != null)
		  //  dataLoaded = System.Convert.ToBoolean(Request.QueryString["xmlBool"]);

      if (Request.QueryString["control"] != null)
        controlName = Request.QueryString["control"];

      if (Request.QueryString["from"] != null)
        pageFrom = Request.QueryString["from"];
        		  
		  lookup = GetLookup();
		  
		  Page.RegisterStartupScript("Startup", "<script>try { document.getElementById('searchText').focus(); } catch (e) { }</script>");
		}
		
		public string BuildOptionList()
		{
		  return lookup.FetchOptionList();
		}
		
		private Lookup GetLookup()
		{
		  int type = System.Convert.ToInt32(Request.QueryString["type"]);
		  if (type == DialogType.County)
		    return new CountyLookup(Request.QueryString, Cache);
		  else if ((type == DialogType.Place) || 
		           (type == DialogType.HistBehav2) ||
		           (type == DialogType.HistBehav3) ||
		           (type == DialogType.Hospital))
		    return new Lookup(Request.QueryString, Cache);
		  else if (type == DialogType.Site)
		    return new SiteLookup(Request.QueryString, Cache);
		  else
		    throw new Exception("DialogSelectGeneric.GetLookup: invalid lookup dialog type value: " + type.ToString());
		}
		
		public string From
		{
		  get { return pageFrom; }
		}
		
		public string ControlName
		{
		  get { return controlName; }
		}
		
		public int ReturnShort
		{
		  get 
		  {
		    if (Request.QueryString["flag2"] != null)
		      return System.Convert.ToInt32(Request.QueryString["flag2"]);
		    else
		      return 0;
		  }
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
      this.Load += new System.EventHandler(this.Page_Load);

    }
		#endregion
	}
	
  public class DialogType
  {
    public static int County     = 1;
    public static int Place      = 2;
    public static int Site       = 3;
    public static int HistBehav3 = 4;
    public static int HistBehav2 = 5;
    public static int Hospital   = 6;
  }
  
	class Lookup
	{
    protected Hashtable options;
    //protected DataTable table;
    protected NameValueCollection QueryString;
    protected int type = 0;
    protected Cache cache;
    
	  public Lookup(NameValueCollection queryString, Cache cacheObject)
	  {
	    QueryString = queryString;
	    cache = cacheObject;
	    type = System.Convert.ToInt32(QueryString["type"]);
	    
	    if (cache.Get("DialogOptions") == null)
	    {
	      AddToSession();	      
	    }
	    else
	    {
	      options = (Hashtable) cache.Get("DialogOptions");
	      if (!options.ContainsKey("DialogOptions" + type.ToString()))
	      {
	        AddToSession();
	      }
	    }
	  }
	  
	  protected void AddToSession()
	  {
	    DataTable table = LoadData();
	    string optionList = BuildOptionList(table);
	    
      if (options == null)
        options = new Hashtable();
		    
      options.Add("DialogOptions" + type.ToString(), optionList);
      cache.Insert("DialogOptions", options, new CacheDependency(GetPath + @"\Cache.xml"));
	  }
	  
    private string GetPath
    {
      get 
      {
        RegistryKey key = Registry.LocalMachine.OpenSubKey(PHI.UI.Eureka.Constants.RegistryKeys.Eureka);
        return (string) key.GetValue("WebConfigPath");
      }
    }
	  
    protected int Flag1
    {
      get 
      { 
        if (QueryString["flag1"] != null)
          return System.Convert.ToInt32(QueryString["flag1"]);
        else
          return -1;
      }
    }
    
    protected int Sort
    {
      get 
      { 
        if (QueryString["bool"] != null)
          return System.Convert.ToInt32(QueryString["bool"]);
        else
          return -1;
      }
    }
    
    public string FetchOptionList()
    {
      return (string) options["DialogOptions" + type.ToString()];    
    }
	  
	  protected virtual string BuildOptionList(DataTable table)
    {
      string currentValue = QueryString["value"];
      string optionList = "<option value=''>- No Label -</option>";
      DataRow[] rows;
	    
      if (Sort == 1)
        rows = table.Select("", "data");
      else
        rows = table.Select("");
	      
      foreach (DataRow row in rows)
      {
        if ((string) row["code"] == currentValue)
          optionList += string.Format("<option value='{0}' abbr='{1}' selected='true'>{0} - {2}</option>", row["code"], row["group"], row["data"]);
        else
          optionList += string.Format("<option value='{0}' abbr='{1}'>{0} - {2}</option>", row["code"], row["group"], row["data"]);
      }
      
      return optionList;
    }
	  
    private DataTable LoadData()
    {
      DataTable table = new DataTable("DialogOptions" + type.ToString());
      table.Columns.Add("code");
      table.Columns.Add("group");
      table.Columns.Add("data");
      DataRow row;
		  
      using (SqlConnection conn = new SqlConnection(Settings.ConnectionString))
      {
        SqlCommand cmd = new SqlCommand("EurekaSP_GetDialogDropDownOptions", conn);
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.Parameters.Add("@Type", SqlDbType.Int).Value = type;
        cmd.Parameters.Add("@Flag1", SqlDbType.Int).Value = Flag1;
		    
        conn.Open();
        using (SqlDataReader reader = cmd.ExecuteReader())
        {
          while (reader.Read())
          {
            row = table.NewRow();
            row["code"] = reader.GetString(0); // lbl_code
            row["group"] = reader.GetString(1); // lbl_group
            row["data"] = reader.GetString(2); // lbl_data
            
            table.Rows.Add(row);
          }
        }
      }
      
      return table;
    }
	}
	
	//----------------------------------------------------------------------------------------------
	// CountyLookup
	//----------------------------------------------------------------------------------------------
	class CountyLookup : Lookup
	{
	  public CountyLookup(NameValueCollection queryString, Cache cacheObject): base(queryString, cacheObject)
	  {	    
	  }
	  
	  protected override string BuildOptionList(DataTable table)
	  {
	    string currentValue = QueryString["value"];
	    string state = QueryString["state"];
	    string optionList = "<option value='' abbr=''>- No Label -</option>";
	    DataRow[] rows;
	    
	    if (System.Convert.ToInt32(QueryString["bool"]) == 1)
	      rows = table.Select("", "data");
	    else
	      rows = table.Select("");
	      
	    foreach (DataRow row in rows)
	    {
	      if ((((string) row["code"] == currentValue) && ((string) row["code"] != "000")) ||
	          (((string) row["code"] == currentValue) && ((string) row["code"] == "000") && ((string) row["group"] == state)))
	        optionList += string.Format("<option value='{0}' abbr='{1}' selected='true'>{0} - {2}</option>", row["code"], row["group"], row["data"]);
	      else
  	      optionList += string.Format("<option value='{0}' abbr='{1}'>{0} - {2}</option>", row["code"], row["group"], row["data"]);
	    }
	      
	    return optionList;
	  }
	}
  
  //----------------------------------------------------------------------------------------------
  // SiteLookup
  //----------------------------------------------------------------------------------------------
  class SiteLookup : Lookup
  {
    public SiteLookup(NameValueCollection queryString, Cache cacheObject): base(queryString, cacheObject)
    {	   
    }
	  
    protected override string BuildOptionList(DataTable table)
    {
      string currentValue = QueryString["value"];
      string optionList = "";
      DataRow[] rows;

      /* TODO: find the page that uses this and change it!!!
      if ((FNumber != FNumbers.SITE) && 
          (FNumber != FNumbers.HISTOLOGY_TYPEBEHAVIOR_3) &&
          (FNumber != FNumbers.ICDO3_BEHAVIOR_3))
        optionList = "<option value=''></option>";
      
      if ((FNumber == FNumbers.SITE) ||
          (FNumber == FNumbers.HISTOLOGY_TYPEBEHAVIOR_3) ||
          (FNumber == FNumbers.HISTOLOGY_TYPEBEHAVIOR_2))
        optionList += "<option value='xx' style='background-Color:#ffdddd'>- No Label -</option>"; 
      */
	    
      if (QueryString["bool"] == "True")
        rows = table.Select("", "data");
      else
        rows = table.Select("");
	      
      foreach (DataRow row in rows)
      {
        if ((string) row["code"] == currentValue)
          optionList += string.Format("<option value='{0}' abbr='{1}' selected='true'>{0} - {2}</option>", row["code"], row["group"], row["data"]);
        else
          optionList += string.Format("<option value='{0}' abbr='{1}'>{0} - {2}</option>", row["code"], row["group"], row["data"]);
      }
	      
      return optionList;
    }
  }
}
