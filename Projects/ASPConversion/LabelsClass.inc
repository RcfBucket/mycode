<%
  const lcTypeVE = 0
  const lcTypeConsolidation = 1
  
  Class clsLabelsCache
    Private Func
    Private dictQueries
    Private arrConsolidationNames
    Private arrConsolidationOnly
    Private arrVENames
    Private arrNSO 'elements which cannot have a blank option in their drop down list
      
    Private Sub Class_Initialize
      Func = lcTypeVE
      set dictQueries = Server.CreateObject("Scripting.Dictionary")
      arrConsolidationNames = Array("Sex", "Spanish_Origin", "Race", "Vital_Status", "Laterality", "ICDO3_Conv_Flag", "DX_Conf", "Sum_Stage", "Tum_Markers", "Report_Source", "Religion", "Marital_Status", "Tum_Status", "FU_Last_Type_Tum", "Class_Of_Case", "CasefindingSource", "Pay_Source", "ACOS_Approved", "TNM_Coder", "TNM_Edition", "Protocol_Part", "Type_Admis", "FU_Last_Type_Pat", "AmbiguousTerminology", "MultTumRptAsOnePrim")
      arrConsolidationOnly = Array("Place", "County", "IHSLink")
      arrVENames = Array("FU_Next_Type", "Pat_No_Contact")
      ' No Space Allowed
      arrNSO = Array("Site", "Laterality", "Hist_Type_3", "ICDO3_Conv_Flag", "DX_Conf", "Sum_Stage", "Tum_Markers", "Sex", "Religion", "Race", "Marital_Status", "Hosp_Ref_From", "Hosp_Ref_To", "Class_Of_Case", "Report_Source", "FU_Hosp_Last", "FU_Last_Type_Pat", "FU_Last_Type_Tum", "Vital_Status", "Tum_Status", "Pat_No_Contact", "Spanish_Origin", "Protocol_Part")
    End Sub
       
    Private Sub Class_Terminate
      set dictQueries = Nothing
    End Sub
       
    Private Function RecordsetToStream(rs)
      dim stm
      
      set stm = Server.CreateObject("ADODB.Stream")
      rs.Save stm, adPersistXML
      stm.Position = 0
      set RecordsetToStream = stm
    end Function
      
    Private Function StreamToRecordset(stm)
      dim rs
      
      stm.Position = 0
      set rs = Server.CreateObject("ADODB.Recordset")
      rs.Open stm
      set StreamToRecordset = rs
    end Function
       
    Public Sub Initialize(conn)
      dim cmd
      dim rs
       
      set cmd = Server.CreateObject("ADODB.Command")
      cmd.CommandText = "dbo.GuiSP_FetchFieldLabels"
      cmd.CommandType = adCmdStoredProc
       
      cmd.Parameters.Append cmd.CreateParameter("@Function", adInteger, adParamInput, 0, Func)
      set cmd.ActiveConnection = conn
      set rs = cmd.Execute
       
      dim ct
      for ct = 0 to UBound(arrConsolidationNames)
        dictQueries.Add arrConsolidationNames(ct), StreamToRecordset( RecordsetToStream(rs) )
        set rs = rs.NextRecordSet
      next
       
      if Func = lcTypeVE then
        for ct = 0 to UBound(arrVENames) 
          dictQueries.Add arrVENames(ct), StreamToRecordset( RecordsetToStream(rs) )
          set rs = rs.NextRecordSet
        next
      end if
      
      if Func = lcTypeConsolidation then
        for ct = 0 to UBound(arrConsolidationOnly) 
          dictQueries.Add arrConsolidationOnly(ct), StreamToRecordset( RecordsetToStream(rs) )
          set rs = rs.NextRecordSet
        next
      end if
      
      set rs = Nothing
      set cmd = Nothing
    End Sub 
       
    Public Sub saveQueries
      dim ct
      dim rsXML
       
      response.write "<input type='hidden' name='CacheFunction' value='" & Func & "'>" 
      for ct = 0 to UBound(arrConsolidationNames)
        rsXML = RecordsetToStream( Me.Recordset(arrConsolidationNames(ct)) ).ReadText
        response.write "<input type='hidden' name='" & arrConsolidationNames(ct) & "' value=""" & Server.HTMLEncode(rsXML) & """>"
      next
       
      if Func = lcTypeVE then
        for ct = 0 to UBound(arrVENames)
          rsXML = RecordsetToStream( Me.Recordset(arrVENames(ct)) ).ReadText
          response.write "<input type='hidden' name='" & arrVENames(ct) & "' value=""" & Server.HTMLEncode(rsXML) & """>"
        next
      end if
    End Sub
      
    Public Sub loadQueries
      dim stm
      dim ct
         
      Func = CInt( Request.Form("CacheFunction") )
         
      set stm = Server.CreateObject("ADODB.Stream")
      For ct = 0 to UBound(arrConsolidationNames) 
        stm.Open
        stm.WriteText Request.Form( arrConsolidationNames(ct) )
        dictQueries.Add arrConsolidationNames(ct), StreamToRecordset(stm)
        stm.Close
      Next
       
      if Func = lcTypeVE then
        For ct = 0 to UBound(arrVENames) 
          stm.Open
          stm.WriteText Request.Form( arrVENames(ct) )
          dictQueries.Add arrVENames(ct), StreamToRecordset(stm)
          stm.Close
        Next
      end if
      set stm = Nothing
    End Sub

    Public Property Let Screen(Value)
      Func = Value
    End Property
      
    Public Property Get Recordset(sQueryName)
      set Recordset = dictQueries(sQueryName)
      Recordset.MoveFirst
    End Property
    
    Public Function FieldLabel(sQueryName, sCode)
      dim rs
       
      set rs = Me.RecordSet(sQueryName)
      rs.Find "LBL_CODE = '" & trim(sCode) & "'" 
      if rs.EOF then
        if trim(sCode) <> "" then
          FieldLabel = trim(sCode) & " - No Label"
        else
          FieldLabel = ""
        end if
      else
        FieldLabel = rs("LBL_CODE") & " - " & rs("LBL_DATA")
      end if
    End Function
    
    Public Function CountyFieldLabel(sQueryName, sCode, sState)
      dim rs
       
      set rs = Me.RecordSet(sQueryName)
      if trim(sCode) <> 000 then
        rs.Find "LBL_CODE = '" & trim(sCode) & "'" 
        if rs.EOF then
          if trim(sCode) <> "" then
            CountyFieldLabel = trim(sCode) & " - No Label"
          else
            CountyFieldLabel = ""
          end if
        else
          CountyFieldLabel = rs("LBL_CODE") & " - " & rs("LBL_DATA")
        end if
      else
        rs.MoveFirst
        While not rs.EOF
          if trim(sCode) = 000 and rs("LBL_CODE") = trim(sCode) and trim(rs("LBL_GROUP")) = trim(sState) then
            CountyFieldLabel = rs("LBL_CODE") & " - " & rs("LBL_DATA")
            exit function
          else
            CountyFieldLabel = ""
          end if
          rs.MoveNext
        wend
      end if
    End Function




    Public Function ISHLinkSeletedValue(sQueryName, sSelected, iLength)
      dim ct
      dim rs
      dim sResult
      dim sOption
      
      sResult = ""       
      set rs = Me.Recordset(sQueryName)
      while not rs.EOF
        sOption = Trim(rs("LBL_DATA"))
        if iLength > 0 then
          sOption = Mid(sOption, 1, iLength)
        end if
          
        if rs("LBL_CODE") = trim(sSelected)  then
				  sResult =  rs("LBL_CODE") & " - " & sOption 
				end if
       rs.MoveNext
      wend     
		  ISHLinkSeletedValue = sResult
    End Function 'ISHLinkSeletedValue
    
    Public Function DropDownOptions(sQueryName, sSelected, iLength)
      dim ct
      dim rs
      dim sResult
      dim sOption
      dim blankAllowed
      dim invalidOption
      
      blankAllowed = true
      invalidOption = true
      
      sResult = "<option value=''></option>"
      for ct = 0 to UBound(arrNSO)
        if sQueryName = arrNSO(ct) then
          sResult = ""
          blankAllowed = false
          exit for
        end if
      next
       
      set rs = Me.Recordset(sQueryName)
      while not rs.EOF
        sOption = Trim(rs("LBL_DATA"))
        if iLength > 0 then
          sOption = Mid(sOption, 1, iLength)
        end if
          
        sResult = sResult & "<option value='" & rs("LBL_CODE") & "'"
        if rs("LBL_CODE") = trim(sSelected) then
          sResult = sResult & " selected"
          invalidOption = false
        end if
        sResult = sResult & ">" & rs("LBL_CODE") & " - " & sOption & "</option>"
        rs.MoveNext
      wend
      
      if invalidOption and blankAllowed = false and trim(sSelected) <> "" then
        dim strNoLabel
        strNoLabel = sSelected & " - No Label"
        if iLength > 0 then
          strNoLabel = LEFT(strNoLabel, iLength+4)
        end if
        sResult = sResult & "<option value='" & sSelected & "' selected style='background-Color:#ffdddd'>" & strNoLabel & "</option>"
      end if
      
      DropDownOptions = sResult
    End Function
        
  End Class
%> 