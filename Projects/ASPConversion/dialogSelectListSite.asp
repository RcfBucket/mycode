<%@language="VBScript"%>
<%option explicit%>
<!--#include virtual="/Eureka/Asp_Inc/dsn.inc"-->
<!--#include virtual="/Eureka/Asp_Inc/adovbs.inc"-->
<!--#include virtual="/Eureka/Asp_Inc/LabelsClass.inc"-->
<%
  dim useXML
	useXML = cBool(request("xmlbool"))
	
  function GetDropDownOptions(ItemID, SelectedValue, Length, SortOnLabel, usingXML)
    dim retval
    if ItemID = "F00152" or ItemID = "F02503" or ItemID = "F02504" then
      retval = ""
    else
      retval = "<option value=''></option>"
    end if  
    
    if ItemID = "F00152" or ItemID = "F02503" or ItemID = "F00033" then
      retval = retval & "<option value='xx' style='background-Color:#ffdddd'>- No Label -</option>"
    end if
    
    if usingXML = false then
      if SelectedValue = "" then
        SelectedValue = -1
      end if
      
      ' fetch all of the drop down options
      dim spDropDown
      set spDropDown = server.createobject("adodb.command")
      spDropDown.CommandText = "dbo.GuiSP_GetDialogDropDownOptions"
      spDropDown.CommandType = adCmdStoredProc
      
      dim connObj
      set connObj = server.createObject("ADODB.Connection")
      connObj.open cDSN      
      set spDropDown.ActiveConnection = connObj
      spDropDown.Parameters.Append spDropDown.CreateParameter("@ItemID", adVarChar, adParamInput, 6, ItemID)
      spDropDown.Parameters.Append spDropDown.CreateParameter("@SortOnLabel", adBoolean, adParamInput, 0, SortOnLabel)
      spDropDown.Parameters.Append spDropDown.CreateParameter("@Length", adInteger, adParamInput, 2, Length)
      spDropDown.Parameters.Append spDropDown.CreateParameter("@SelectedValue", adVarChar, adParamInput, 8, SelectedValue)  
      ' create a stream object
      dim stmOut
      set stmOut = server.createobject("adodb.stream")
      stmOut.Open
      stmOut.LineSeparator = adCRLF
      spDropDown.Properties.Item("Output Stream") = stmOut
      spDropDown.Execute , , adExecuteStream
      
      stmOut.Position = 0
      retval = retval & stmOut.ReadText      
      stmOut.Close
      
      connObj.close      
      set stmOut = Nothing
      set connObj = Nothing
      set spDropDown = Nothing
    end if
    
    GetDropDownOptions = retval
  end function
  
  dim sElementName
  dim sFNumber
  dim sElementValue
  dim sLength
  dim sortBool
  dim sIndex
  dim sFrom
  dim sTumorCount
  
  sElementName = request.querystring("name")
  sFNumber      = request.querystring("fnumber")
  sElementValue = request.querystring("value")
  if sElementValue = "" then
    sElementValue = "xx"
  end if
  sLength = request.querystring("number")
  sortBool = request.querystring("bool")
  sFrom = request.querystring("from")
%>

<html>
<head>
	<title>Select List - <%=sElementName%></title>
<link rel="stylesheet" href="/Eureka/Css/eureka.css" />

<script language="JavaScript" src="/Eureka/Dialogs/dropDownListSearchFunc.js"></script>
<script language="JavaScript" src="/Eureka/Scripts/selectDialogFunctions.js"></script>
<script language="JavaScript"> 
function setValue(thisField)
{
  try
  {
    if (thisField.selectedIndex == 0) 
      return;

    var from = "<%=sFrom%>";
    if (from == "cons")
    {
      var mainField = dialogArguments.frmConsolidation.sel<%=sElementName%>;
      var val = thisField.options[thisField.options.selectedIndex].text;
    }
    else if (from == "ucClassification")
    {
      var mainField = dialogArguments.document.all.ucClassification_txt<%=sElementName%>;
      var val = thisField.options[thisField.options.selectedIndex].text;
    }
    else if (from == "cfmanualentry")
    {
      var mainField = dialogArguments.CfManualEntry.txt<%=sElementName%>;
      var val = thisField.options[thisField.options.selectedIndex].text;
    }
    else if (from == "cfdeathclearance")
    {
      var mainField = dialogArguments.CfDeathClearance.txt<%=sElementName%>;    
      var val = thisField.options[thisField.options.selectedIndex].text;
    }
    else if (from == "newcaseentry")
    {
      var mainField = dialogArguments.frmNewCaseEntry.txt<%=sElementName%>;
      var val = thisField.options[thisField.options.selectedIndex].value;
    }
    else
    {
      var mainField = dialogArguments.frmVisualEditing.txt<%=sElementName%>;
      var val = thisField.options[thisField.options.selectedIndex].value;
    }
    mainField.value = val;
    mainField.fireEvent("onChange");
  }
  catch (err)
  {
    alert("setValue: " + err);    
  }
}

function load(selOption)
{
  var options;
  options = "<%=GetDropDownOptions(sFNumber, sElementValue, sLength, sortBool, useXML)%>";
  if (dialogArguments.document.all["siteXML"].innerHtml == "")
  {
    dialogArguments.document.all["siteXML"].async = false;
    dialogArguments.document.all["siteXML"].loadXML(options);
  }
  else
    options = options + dialogArguments.document.all["siteXML"].innerHtml;

  var xsl = new ActiveXObject("MSXML2.DOMDocument.4.0");
  var xml = new ActiveXObject("MSXML2.DOMDocument.4.0");
        
  xsl.validateOnParse = false;  
  xsl.async = false;
  xsl.setProperty("SelectionLanguage", "XPath");
  xsl.setProperty("SelectionNamespaces", "xmlns:xsl='http://www.w3.org/1999/XSL/Transform'");
  xsl.load(xslSelectList.XMLDocument);
  xsl.documentElement.selectSingleNode("//xsl:variable/@select").nodeValue = selOption;
  
  if (xsl.parseError.errorCode == 0) 
  {
    xml.validateOnParse = false;
    xml.async = false;
    xml.loadXML("<options>" + options + "</options>");
    document.all.divSelect.innerHTML = xml.transformNode(xsl);
  }
  else
  {
    document.all.divSelect.innerHTML = xml.parseError.reason;
  }
}
</script>
</head>
<body id="dialog_body" onLoad=load("'<%=trim(sElementValue)%>'");>
  <form name="frmSelect" onsubmit="return closeWindow();">
    <xml id="xslSelectList" src="/Eureka/Xslt/selectListSite.xsl" />

    <div class="UserClickLayer">
		<table class="DataLayer" ID="Table1">
			<tr>
				<td align="center">
					<table ID="Table2">
						<tr>
							<td class="label">Search:</td>
							<td>
                <input type="text" name="searchText" size="38" onkeyup="findListMatch(document.all.selSite);" 
                  onchange="changed(document.all.selSite);" ID="searchText">
              </td>
						</tr>
					</table>
				</td>
			</tr>
		    <tr>
				<td>
					<div id="divSelect">
					</div>
				</td>
			</tr>
			<tr>
				<td>&nbsp;</td>
			</tr>
		</table>
		<table cellpadding="0" border="0" ID="Table3">
      <tr>
        <td align="center">
          <input type="submit" class="formButton" value="Close">
        </td>
      </tr>
    </table>
 	  <div id="hidDiv" style="visibility:hidden;">
	    <span style="position:absolute;left:10;bottom:5;">
        <!--don't remove the helper field-->	
        <input type="text" name="helper" size=1>
		  </span>
      </div>
    </div>
  </form>
</body>