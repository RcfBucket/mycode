<%@language="VBScript"%>
<%option explicit%>

<!--#include virtual="/Eureka/Asp_Inc/dsn.inc"-->
<!--#include virtual="/Eureka/Asp_Inc/adovbs.inc"-->
<!-- #include virtual="/Eureka/Asp_Inc/LabelsClass.inc"-->

<%
  dim sElementName
  sElementName = request.querystring("name")
  
  dim xmlBool
  xmlBool = cBool(request("xmlbool"))
  
  function GetPlaceDropDownOptions(connObj, SelectedValue, SortOnLabel, bool)
    if bool = true then
      GetPlaceDropDownOptions = ""
    else
      if not IsNumeric(SelectedValue) then
      SelectedValue = -1
      end if
      
      ' create and quality a command object
      dim spDropDown
      set spDropDown = server.createobject("adodb.command")
      spDropDown.CommandText = "dbo.GuiSP_GetDialogCountriesDropDownOptions"
      spDropDown.CommandType = adCmdStoredProc
      Set spDropDown.ActiveConnection = connObj
      spDropDown.Parameters.Append spDropDown.CreateParameter("@SelectedValue", adVarChar, adParamInput, 20, SelectedValue)
      spDropDown.Parameters.Append spDropDown.CreateParameter("@SortOnLabel", adBoolean, adParamInput, 0, SortOnLabel)
      
      ' create a stream object
      dim stmOut
      set stmOut = server.createobject("adodb.stream")
      stmOut.Open
      stmOut.LineSeparator = adCRLF
      spDropDown.Properties.Item("Output Stream") = stmOut
      spDropDown.Execute , , adExecuteStream
      
      dim firstOption
      firstOption = ""
      firstOption = firstOption & "<option value='' style='background-Color:#ffdddd'>- No Label -</option>"
      
      stmOut.Position = 0
      GetPlaceDropDownOptions = firstOption & stmOut.ReadText
      stmOut.Close
      
      set stmOut = Nothing
      set spDropDown = Nothing
    end if
  end function
  
  
  dim sElementValue
  dim sLength
  dim sortBool
  dim sFrom
  
  sElementValue = trim(request.querystring("value"))
  if sElementValue = "" then
    sElementValue = "xx"
  end if
  sortBool = cBool(request.querystring("bool"))
  sFrom = request.querystring("from")
  
  dim connObj
  set connObj = server.createObject("ADODB.Connection")
  connObj.open cDSN
%>

<html>
<head>
	<title>Select List - <%=sElementName%></title>
	<link type="text/css" rel="stylesheet" href="/Eureka/Css/eureka.css">

<script language="JavaScript" src="/Eureka/Dialogs/dropDownListSearchFunc.js"></script>
<script language="JavaScript" src="/Eureka/Scripts/selectDialogFunctions.js"></script>
<script language="JavaScript"> 
function setValue(thisField){
  if (thisField.selectedIndex == 0) return;
  var from = "<%=sFrom%>";
  if (from == "cons")
    var mainField = dialogArguments.frmConsolidation.sel<%=sElementName%>;
  else if (from == "newcaseentry")
    var mainField = dialogArguments.frmNewCaseEntry.txt<%=sElementName%>;
  else  
    var mainField = dialogArguments.frmVisualEditing.txt<%=sElementName%>;

  mainField.value = thisField.options[thisField.options.selectedIndex].text;
  mainField.fireEvent("onChange");
}

function test(selOption){
  var str;
  if (dialogArguments.document.all["placeXML"].xml == ""){
    str = "<options><%=GetPlaceDropDownOptions(connObj, sElementValue, sortBool, xmlBool)%></options>";
    dialogArguments.document.all["placeXML"].async = false;
    var result = dialogArguments.document.all["placeXML"].loadXML(str);
  }
  var xsl = new ActiveXObject("MSXML2.DOMDocument.4.0");
  var xml = new ActiveXObject("MSXML2.DOMDocument.4.0");
        
  xsl.validateOnParse = false;  
  xsl.async = false;
  xsl.setProperty("SelectionLanguage", "XPath");
  xsl.setProperty("SelectionNamespaces", "xmlns:xsl='http://www.w3.org/1999/XSL/Transform'");
  xsl.load(xslSelectList.XMLDocument);
  xsl.documentElement.selectSingleNode("//xsl:variable/@select").nodeValue = selOption;
  
  if (xsl.parseError.errorCode == 0) {
    xml.validateOnParse = false;
    xml.async = false;
    xml.load(dialogArguments.document.all["placeXML"].XMLDocument);
    if ("<%=sElementName%>" == "Place_Of_Death"){
      var node = xml.createElement("option");
      xml.documentElement.appendChild(node);
    }
    
    document.all.divSelect.innerHTML = xml.transformNode(xsl);
  }
  else{
    document.all.divSelect.innerHTML = xml.parseError.reason;
  }
}
</script>
</head>
<body id="dialog_body" onLoad=test("'<%=trim(sElementValue)%>'");>
<form name="frmSelect" onsubmit="return closeWindow();">
  <xml id="xslSelectList" src="/Eureka/Xslt/selectListPlace.xsl" />
  <div class="UserClickLayer">
    <table class="DataLayer">
      <tr>
        <td class="label" align="center">
          <table>
            <tr>
              <td class="label">Search</td>
              <td>
                <input type="text" class="InputText" name="SearchText" size="38" onkeyup="findListMatch(document.all.selPlaceBirth);" 
                    onchange="changed(document.all.selPlaceBirth);" ID="searchText">
              </td>
            </tr>
          </table>
        </td>
      </tr>
      <tr>
        <td>
          <div id="divSelect">
          </div>
        </td>
      </tr>
  			<tr>
  				<td>&nbsp;</td>
  			</tr>
    </table>
		<table cellpadding="0" border="0" ID="Table3">
      <tr>
        <td align="center">
          <input type="submit" class="formButton" value="Close">
        </td>
      </tr>
    </table>
  </div>
  <div id="hidDiv" style="visibility:hidden;">
  	<span style="position:absolute;left:10;bottom:5;">
  		<!--don't remove the helper field-->
  		<input type="text" name="helper" size=1>
  	</span>
  </div>
</form>
</body>

<%
  connObj.close
  set connObj = Nothing
%>