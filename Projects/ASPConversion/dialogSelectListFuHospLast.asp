<%@language="VBScript"%>
<%option explicit%>

<!--#include virtual="/Eureka/Asp_Inc/dsn.inc"-->
<!--#include virtual="/Eureka/Asp_Inc/adovbs.inc"-->
<!-- #include virtual="/Eureka/Asp_Inc/LabelsClass.inc"-->

<%
  dim xmlBool
  xmlBool = cBool(request("xmlbool"))
  
  function GetHospDropDownOptions(connObj, SelectedValue, SortOnLabel, bool)
    if bool = true then
      GetHospDropDownOptions = ""
    else
      if not IsNumeric(SelectedValue) then
      SelectedValue = -1
      end if
      
      ' create and quality a command object
      dim spDropDown
      set spDropDown = server.createobject("adodb.command")
      spDropDown.CommandText = "dbo.GuiSP_GetDialogHospDropDownOptions"
      spDropDown.CommandType = adCmdStoredProc
      Set spDropDown.ActiveConnection = connObj
      spDropDown.Parameters.Append spDropDown.CreateParameter("@SelectedValue", adVarChar, adParamInput, 20, SelectedValue)
      spDropDown.Parameters.Append spDropDown.CreateParameter("@SortOnLabel", adBoolean, adParamInput, 0, SortOnLabel)
      
      ' create a stream object
      dim stmOut
      set stmOut = server.createobject("adodb.stream")
      stmOut.Open
      stmOut.LineSeparator = adCRLF
      spDropDown.Properties.Item("Output Stream") = stmOut
      spDropDown.Execute , , adExecuteStream
      
      stmOut.Position = 0
      dim noLabelOption
      noLabelOption = "<option value='' style='background-Color:#ffdddd'> - No Label</option>"
      GetHospDropDownOptions = noLabelOption & stmOut.ReadText
      stmOut.Close
      
      set stmOut = Nothing
      set spDropDown = Nothing
    end if
  end function
  
  dim sElementName
  dim sElementValue
  dim sLength
  dim sortBool
  dim sFrom
  dim sIndex
  
  sElementName = request.querystring("name")
  sElementValue = trim(request.querystring("value"))
  if sElementValue = "" then
    sElementValue = "xx"
  end if
  sortBool = cBool(request.querystring("bool"))
  sFrom = request.querystring("from")
  sIndex = request.querystring("index")
  
  dim connObj
  set connObj = server.createObject("ADODB.Connection")
  connObj.open cDSN
%>

<html>
<head>
	<title>Select List - <%=sElementName%></title>
	<link type="text/css" rel="stylesheet" href="/Eureka/Css/eureka.css">

<script language="JavaScript" src="/Eureka/Dialogs/dropDownListSearchFunc.js"></script>
<script language="JavaScript" src="/Eureka/Scripts/selectDialogFunctions.js"></script>
<script language="JavaScript"> 
function setValue(thisField){
  if (thisField.selectedIndex == 0) return;
  var from = "<%=sFrom%>";
  if (from == "cons"){
    var counter = "<%=sIndex%>";
    var mainField = dialogArguments.frmConsolidation.sel<%=sElementName%>[counter];
  }
  else if (from == "cfmanualentry"){
    var mainField = dialogArguments.CfManualEntry.txt<%=sElementName%>;
    var hidValue = dialogArguments.CfManualEntry.hid<%=sElementName%>;
    hidValue.value = thisField.options[thisField.options.selectedIndex].value;
  }
  else if (from == "cfdeathclearance"){
    var mainField = dialogArguments.CfDeathClearance.txt<%=sElementName%>;
    var hidValue = dialogArguments.CfDeathClearance.hid<%=sElementName%>;
    hidValue.value = thisField.options[thisField.options.selectedIndex].value;
  }
  else if (from == "newcaseentry"){
    var mainField = dialogArguments.frmNewCaseEntry.txt<%=sElementName%>;
  }
  else{  
    var mainField = dialogArguments.frmVisualEditing.txt<%=sElementName%>;
  }
  mainField.value = thisField.options[thisField.options.selectedIndex].text;
  mainField.fireEvent("onChange");
}

function test(selOption){
  var str;
  if (dialogArguments.document.all["fuHospLastXML"].xml == ""){
    str = "<options><%=GetHospDropDownOptions(connObj, sElementValue, sortBool, xmlBool)%></options>";
    dialogArguments.document.all["fuHospLastXML"].async = false;
    var result = dialogArguments.document.all["fuHospLastXML"].loadXML(str);
  }
  var xsl = new ActiveXObject("MSXML2.DOMDocument.4.0");
  var xml = new ActiveXObject("MSXML2.DOMDocument.4.0");
        
  xsl.validateOnParse = false;  
  xsl.async = false;
  xsl.setProperty("SelectionLanguage", "XPath");
  xsl.setProperty("SelectionNamespaces", "xmlns:xsl='http://www.w3.org/1999/XSL/Transform'");
  xsl.load(xslSelectList.XMLDocument);
  xsl.documentElement.selectSingleNode("//xsl:variable/@select").nodeValue = selOption;
  
  if (xsl.parseError.errorCode == 0) {
    xml.validateOnParse = false;
    xml.async = false;
    xml.load(dialogArguments.document.all["fuHospLastXML"].XMLDocument);
    
    document.all.divSelect.innerHTML = xml.transformNode(xsl);
  }
  else{
    document.all.divSelect.innerHTML = xml.parseError.reason;
  }
}
</script>
</head>
<body id="dialog_body" onLoad=test("'<%=trim(sElementValue)%>'");>
<form name="frmSelect" onsubmit="return closeWindow();">
  <xml id="xslSelectList" src="/Eureka/Xslt/selectListFuHospLast.xsl" />
  <div class="UserClickLayer">
    <table class="DataLayer">
      <tr>
        <td align="center">
          <table>
            <tr>
              <td class="label">Search:</td>
              <td>
                <input type="text" name="searchText" size="38" onkeyup="findListMatch(document.all.selFuHospLast);" 
                  onchange="changed(document.all.selFuHospLast);" ID="searchText">
              </td>
            </tr>
          </table>
        </td>
      </tr>
  		    <tr>
        <td>
          <div id="divSelect">
          </div>
        </td>
      </tr>
  	<tr>
  		<td>&nbsp;</td>
  	</tr>
    </table>
		<table cellpadding="0" border="0" ID="Table3">
      <tr>
        <td align="center">
          <input type="submit" class="formButton" value="Close">
        </td>
      </tr>
    </table>
    <div id="hidDiv" style="visibility:hidden;">
       <span style="position:absolute;left:10;bottom:5;">
  	    <!--don't remove the helper field-->
        <input type="text" name = "helper" size = 1>
      </span>
    </div>
  </div>
</form>
</body>

<%
  connObj.close
  set connObj = Nothing
%>