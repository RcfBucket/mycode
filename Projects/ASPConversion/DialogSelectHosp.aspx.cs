using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using PHI.UI.Eureka;
namespace Dialogs
{
	/// <summary>
	/// Summary description for DialogSelectHosp.
	/// </summary>
	public class DialogSelectHosp : System.Web.UI.Page
	{
		protected System.Web.UI.WebControls.ListBox lstHosp;
		protected System.Web.UI.WebControls.Button btnSearch;
		protected System.Web.UI.WebControls.TextBox txtSearch;
		protected System.Data.SqlClient.SqlConnection conn;
		protected System.Web.UI.WebControls.Label lblMsg;
		protected System.Web.UI.WebControls.Label lblError;
		protected System.Web.UI.WebControls.Label lblItem;
		protected System.Data.SqlClient.SqlDataAdapter daHosp;
		protected System.Data.SqlClient.SqlCommand sqlSelectCommand1;
		protected System.Web.UI.HtmlControls.HtmlForm Form1;
		protected Dialogs.dsList dsList1;
	
		private void Page_Load(object sender, System.EventArgs e)
		{
			// Put user code to initialize the page here
			if (!IsPostBack)
			{
				SqlConnection conn = new SqlConnection(Settings.ConnectionString);
				conn.Open();
				daHosp.SelectCommand.Connection = conn;
				DataRow row = dsList1.Tables[0].NewRow();
				row[0] = "";
        dsList1.Tables[0].Rows.Add(row);
				daHosp.Fill(dsList1);
			  lstHosp.DataSource = dsList1;
			  lstHosp.DataBind();
		  	lblItem.Text = lstHosp.Items[1].Value;  //debug
  			Session["dsHosp1"] = dsList1;
		} 
		else
	{
		lstHosp.DataSource = (Dialogs.dsList)Session["dsList1"];
	}

		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.dsList1 = new Dialogs.dsList();
			this.daHosp = new System.Data.SqlClient.SqlDataAdapter();
			this.sqlSelectCommand1 = new System.Data.SqlClient.SqlCommand();
			((System.ComponentModel.ISupportInitialize)(this.dsList1)).BeginInit();
			// 
			// dsList1
			// 
			this.dsList1.DataSetName = "dsList";
			this.dsList1.Locale = new System.Globalization.CultureInfo("en-US");
			// 
			// daHosp
			// 
			this.daHosp.SelectCommand = this.sqlSelectCommand1;
			this.daHosp.TableMappings.AddRange(new System.Data.Common.DataTableMapping[] {
																																										 new System.Data.Common.DataTableMapping("Table", "GuiSP_GetDBInqHospList", new System.Data.Common.DataColumnMapping[] {
																																																																																																						 new System.Data.Common.DataColumnMapping("ReportingSource", "ReportingSource")})});
			// 
			// sqlSelectCommand1
			// 
			this.sqlSelectCommand1.CommandText = "[GuiSP_GetDBInqHospList]";
			this.sqlSelectCommand1.CommandType = System.Data.CommandType.StoredProcedure;
			this.sqlSelectCommand1.Parameters.Add(new System.Data.SqlClient.SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, false, ((System.Byte)(0)), ((System.Byte)(0)), "", System.Data.DataRowVersion.Current, null));
			this.txtSearch.TextChanged += new System.EventHandler(this.txtSearch_TextChanged);
			this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
			this.lstHosp.SelectedIndexChanged += new System.EventHandler(this.lstHosp_SelectedIndexChanged);
			this.Load += new System.EventHandler(this.Page_Load);
			((System.ComponentModel.ISupportInitialize)(this.dsList1)).EndInit();

		}
		#endregion

		private void btnSearch_Click(object sender, System.EventArgs e)
		{
			 string SearchBase = GetFullRptSrcName(txtSearch.Text.Trim());
			ListItem item = new ListItem(SearchBase);
			int index = -1;
			index = lstHosp.Items.IndexOf(item);
			if (index > -1 )
			{
				lblMsg.Text = "";
				lstHosp.SelectedIndex = index;
        lstHosp_SelectedIndexChanged(sender, e);
			}
			else 
			{
				lblMsg.Text = "Reporting Source was not found.";
			}
		}

		private void txtSearch_TextChanged(object sender, System.EventArgs e)
		{
			btnSearch_Click(sender, e);
		}

		private string GetFullRptSrcName(string SearchText)
		{
			string SearchFound = "none";
			SqlConnection conn = new SqlConnection(Settings.ConnectionString);
			conn.Open();
			SqlCommand cmd = new SqlCommand("GuiSP_GetDBInqHospMatch",conn);
			cmd.CommandType = CommandType.StoredProcedure;
			cmd.Parameters.Add("@InputText", SqlDbType.VarChar, 250).Value  =  SearchText;
			SqlDataReader reader;
			try
			{
			 	reader = cmd.ExecuteReader();
				if (reader.Read())
					 SearchFound = reader.GetString(0).ToString();
			}
			catch (Exception e)
			{
				lblError.Text = e.ToString(); 
			}
			finally
			{
				conn.Close();
			}
			return SearchFound; 
		}

		private void lstHosp_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			Response.Write ("<Script Language='javascript'>" 
				+ "window.opener.SetValue('" +lstHosp.SelectedItem.ToString()+ "'); "
				+ "</Script>");	
		} //lstHosp_SelectedIndexChanged

	}
}
