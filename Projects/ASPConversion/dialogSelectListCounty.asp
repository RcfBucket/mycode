<%@language="VBScript"%>
<%option explicit%>

<!--#include virtual="/Eureka/Asp_Inc/dsn.inc"-->
<!--#include virtual="/Eureka/Asp_Inc/adovbs.inc"-->
<!-- #include virtual="/Eureka/Asp_Inc/LabelsClass.inc"-->

<%
  dim xmlBool
  xmlBool = cBool(request("xmlbool"))
  
  function GetCountyDropDownOptions(connObj, SelectedValue, SortOnLabel, bool)
    if bool = true then
      GetCountyDropDownOptions = ""
    else
      if not IsNumeric(SelectedValue) then
        SelectedValue = -1
      end if
      
      ' create and quality a command object
      dim spDropDown
      set spDropDown = server.createobject("adodb.command")
      spDropDown.CommandText = "dbo.GuiSP_GetDialogCountiesDropDownOptions"
      spDropDown.CommandType = adCmdStoredProc
      Set spDropDown.ActiveConnection = connObj
      spDropDown.Parameters.Append spDropDown.CreateParameter("@SelectedValue", adVarChar, adParamInput, 20, SelectedValue)
      spDropDown.Parameters.Append spDropDown.CreateParameter("@SortOnLabel", adBoolean, adParamInput, 0, SortOnLabel)
      
      ' create a stream object
      dim stmOut
      set stmOut = server.createobject("adodb.stream")
      stmOut.Open
      stmOut.LineSeparator = adCRLF
      spDropDown.Properties.Item("Output Stream") = stmOut
      spDropDown.Execute , , adExecuteStream
      
      dim firstOption
      firstOption = ""
      firstOption = firstOption & "<option value='' style='background-Color:#ffdddd'>- No Label -</option>"
      
      stmOut.Position = 0
      GetCountyDropDownOptions = firstOption & stmOut.ReadText
      stmOut.Close
      
      set stmOut = Nothing
      set spDropDown = Nothing
    end if
  end function
  
  dim sElementName
  dim sElementValue
  dim sState
  dim sLength
  dim sortBool
  dim sFrom
  
  sElementName = request.querystring("name")
  sElementValue = trim(request.querystring("value"))
  sState = request.querystring("state")
  if sElementValue = "" then
    sElementValue = "xx"
  end if
  sortBool = cBool(request.querystring("bool"))
  sFrom = request.querystring("from")
  
  dim connObj
  set connObj = server.createObject("ADODB.Connection")
  connObj.open cDSN
%>

<html>
<head>
	<title>Select List - <%=sElementName%></title>
	<link type="text/css" rel="stylesheet" href="/Eureka/Css/eureka.css">

	<script language="JavaScript" src="/Eureka/Dialogs/dropDownListSearchFunc.js"></script>
  <script language="JavaScript" src="/Eureka/Scripts/selectDialogFunctions.js"></script>
	<script language="JavaScript"> 
	function whichBrowser()
	{
	  var agt = navigator.userAgent.toLowerCase();
	  
	  if (agt.indexOf("opera") != -1)
	    return "opera";
	  else if (agt.indexOf("firefox") != -1)
	    return "firefox";
	  else if (agt.indexOf("msie") != -1)
	    return "msie";
	  else
	    alert("This browser is not supported!");
	}
	
  function getXmlDocument(xmlText)
	{
	  if (whichBrowser() == "msie")
	  {
	    var doc = new ActiveXObject("MSXML2.DOMDocument.4.0");
  		var res;
      doc.async = false;
      res = doc.loadXML(xmlText);
      if (!res)
        alert(doc.parseError.reason);
      else
        return doc;
	  }
	  else
	  { 
	    var parser = new DOMParser(); 
	    return parser.parseFromString(xmlText, "text/xml");
	  }
	}
	
	function setValue(thisField)
	{
	  try
	  {
	    if (whichBrowser() != "msie")
	      dialogArguments = window.opener;
  	    
		  if (thisField.selectedIndex == 0)
		  { 
			  return;
		  }

		  var from = "<%=sFrom%>";
		  if (from == "cons")
		  {
			  var mainField = dialogArguments.document.getElementById("sel<%=sElementName%>");
		  }
		  else if (from == "newcaseentry")
		  {
		  alert(dialogArguments.document.getElementById("txt<%=sElementName%>"));
			  var mainField = dialogArguments.document.getElementById("txt<%=sElementName%>");
		  }
		  else
		  {
			  var mainField = dialogArguments.document.getElementById("txt<%=sElementName%>");
		  }

		  mainField.value = thisField.options[thisField.options.selectedIndex].text;
		  if (whichBrowser == "msie")
		    mainField.fireEvent("onChange");
		}
		catch (e)
		{
		  alert("dialogSelectListCounty.setValue: " + e);
		}
	}

	function test(selOption, sState)
	{
	  if (whichBrowser() != "msie")
	  {
      dialogArguments = window.opener;
    
      var str = "<options><%=GetCountyDropDownOptions(connObj, sElementValue, sortBool, xmlBool)%></options>";
      var xmlDom = getXmlDocument(str);
      var req = new XMLHttpRequest();
      req.open("GET", "/Eureka/Xslt/selectListCounty.xsl", false);
      req.send(null);
      var xsl = req.responseXML;
      var xslParser = new XSLTProcessor();
      xslParser.importStylesheet(xsl);
      var val = xslParser.transformToFragment(xmlDom, document);
      document.getElementById("divSelect").appendChild(val);
    }
    else
    {    
		  var str;
		  if (dialogArguments.document.getElementById("countyXML").xml == "")
		  {
			  str = "<options><%=GetCountyDropDownOptions(connObj, sElementValue, sortBool, xmlBool)%></options>";
			  dialogArguments.document.getElementById("countyXML").async = false;
			  var result = dialogArguments.document.getElementById("countyXML").loadXML(str);
		  }
  		  
		  var xsl = new ActiveXObject("MSXML2.DOMDocument.4.0");
		  var xml = new ActiveXObject("MSXML2.DOMDocument.4.0");
  		  
		  xsl.validateOnParse = false;  
		  xsl.async = false;
		  xsl.setProperty("SelectionLanguage", "XPath");
		  xsl.setProperty("SelectionNamespaces", "xmlns:xsl='http://www.w3.org/1999/XSL/Transform'");
		  xsl.load(xslSelectList.XMLDocument);
		  xsl.documentElement.selectSingleNode("//xsl:variable[@name='selected']/@select").nodeValue = "'" + selOption + "'";
		  xsl.documentElement.selectSingleNode("//xsl:variable[@name='state']/@select").nodeValue = "'" + sState + "'";

		  if (xsl.parseError.errorCode == 0) 
		  {
			  xml.validateOnParse = false;
			  xml.async = false;
			  xml.load(dialogArguments.document.getElementById("countyXML").XMLDocument);

			  document.getElementById("divSelect").innerHTML = xml.transformNode(xsl);
		  }
		  else
		  {
			  document.getElementById("divSelect").innerHTML = xml.parseError.reason;
		  }
		}
	}
	
	function changeit()
	{
	  changed(document.getElementById('selCounty'));
	}
	</script>
</head>
<body id="dialog_body" onLoad="test('<%=trim(sElementValue)%>', '<%=trim(sState)%>');">
	<form name="frmSelect" onsubmit="return closeWindow();">
		<xml id="xslSelectList" src="/Eureka/Xslt/selectListCounty.xsl" />
		<div class="UserClickLayer">
		<table class="DataLayer">
			<tr>
				<td align="center">
					<table>
						<tr>
							<td class="label">Search:</td>
							<td>
                <input type="text" name="searchText" size="38" onkeyup="findListMatch(document.getElementById('selCounty'));" 
                  onchange="changeit();" ID="searchText">
              </td>
						</tr>
					</table>
				</td>
			</tr>
		    <tr>
				<td>
					<div id="divSelect">
					</div>
				</td>
			</tr>
			<tr>
				<td>&nbsp;</td>
			</tr>
		</table>
		<table cellpadding="0" border="0" ID="Table3">
      <tr>
        <td align="center">
          <input type="submit" class="formButton" value="Close">
        </td>
      </tr>
    </table>
  	<div id="hidDiv" style="visibility:hidden;">
			<span style="position:absolute;left:10;bottom:5;">
				<!--don't remove the helper field-->
				<input type="text" name="helper" size=1>
			</span>
		</div>
		</div>
	</form>
</body>

<%
  connObj.close
  set connObj = Nothing
%>