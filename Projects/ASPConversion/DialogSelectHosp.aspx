<%@ Page language="c#" Codebehind="DialogSelectHosp.aspx.cs" AutoEventWireup="false" Inherits="Dialogs.DialogSelectHosp" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>DialogSelectHosp</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="/Eureka/Css/eureka.css" rel="stylesheet">
	</HEAD>
	<body MS_POSITIONING="GridLayout"  class="DataLayer">
		<table>
			<tr>
				<td>
					<form id="Form1" method="post" runat="server" >
						<input style="Z-INDEX: 106; LEFT: 168px; POSITION: absolute; TOP: 344px" onclick="javascript:window.close();"
							type="button" value="Close">
						<asp:textbox id="txtSearch" style="Z-INDEX: 107; LEFT: 32px; POSITION: absolute; TOP: 24px" runat="server"
							Width="304px"></asp:textbox><asp:button id="btnSearch" style="Z-INDEX: 108; LEFT: 344px; POSITION: absolute; TOP: 24px"
							runat="server" Width="64px" Text="Search"></asp:button>
				</td>
			</tr>
			<tr>
				<td><asp:label id="lblMsg" style="Z-INDEX: 109; LEFT: 40px; POSITION: absolute; TOP: 48px" runat="server"
						Width="360px" Font-Size="Small" ForeColor="Red" Height="11px"></asp:label></td>
			</tr>
			<tr>
				<td>
					<asp:ListBox id=lstHosp style="Z-INDEX: 110; LEFT: 32px; POSITION: absolute; TOP: 72px" runat="server" Width="376px" Height="272px" AutoPostBack="True" DataSource="<%# dsList1 %>" DataTextField="ReportingSource" DataMember="GuiSP_GetDBInqHospList">
					</asp:ListBox></td>
			</tr>
			<tr>
				<td>
					<asp:Label id="lblError" style="Z-INDEX: 111; LEFT: 56px; POSITION: absolute; TOP: 424px" runat="server"
						Width="256px" Height="152px"></asp:Label>
					<asp:Label id="lblItem" style="Z-INDEX: 112; LEFT: 368px; POSITION: absolute; TOP: 416px" runat="server"
						Width="440px" Height="144px" Visible="False"></asp:Label></FORM>
				</td>
			</tr>
		</table>
	</body>
</HTML>
