<%@ Page language="c#" Codebehind="DialogSelectGeneric.aspx.cs" AutoEventWireup="false" Inherits="Dialogs.DialogSelectGeneric" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
  <HEAD>
    <title>DialogSelectGeneric</title>
    <meta name="GENERATOR" Content="Microsoft Visual Studio .NET 7.1">
    <meta name="CODE_LANGUAGE" Content="C#">
    <meta name="vs_defaultClientScript" content="JavaScript">
    <meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
    <link type="text/css" rel="stylesheet" href="/Eureka/Css/eureka.css">
    <script language="JavaScript" src="/Eureka/Dialogs/dropDownListSearchFunc.js"></script>
    <script language="JavaScript" src="/Eureka/Scripts/selectDialogFunctions.js"></script>
    <script language="javascript" src="/Eureka/Scripts/browserIndependence.js"></script>
  </HEAD>
  <body MS_POSITIONING="GridLayout" id="dialog_body">
    <script language="javascript">
  function setValue(thisField)
	{
	  try
	  {
	    var returnshort = <%=ReturnShort%>;
	    
		  if (thisField.selectedIndex == 0)
			  return;
			  	  
	    if (whichBrowser() != "msie")
	      dialogArguments = window.opener;
  	    
      var mainField = dialogArguments.document.getElementById("<%=ControlName%>");
      
      if (returnshort == 1)
        mainField.value = thisField.options[thisField.options.selectedIndex].value;
      else
		    mainField.value = thisField.options[thisField.options.selectedIndex].text;
		    
		  if (whichBrowser == "msie")
		    mainField.fireEvent("onChange");
		  else
		  {
		    dialogArguments.changeColor();
		    dialogArguments.changeTextLabel(mainField);
		  }
		}
		catch (e)
		{
		  alert("DialogSelectGeneric.setValue: " + e);
		}
	}
    </script>
    <form id="frmSelect" method="post" runat="server" onsubmit="return closeWindow();">
      <div class="UserClickLayer">
        <div class="DataLayer">
          <div style="MARGIN-BOTTOM: 5px">
            <label class="label">Search:</label> <input type="text" name="searchText" size="38" onkeyup="findListMatch(document.getElementById('selSearch'));"
              onchange="changed(document.getElementById('selSearch'));" ID="searchText" runat="server">
          </div>
          <div>
            <select id="selSearch" style="WIDTH: 350px" size="10" onchange="setValue(this);">
              <%=BuildOptionList()%>
            </select>
          </div>
        </div>
        <div><INPUT class="formButton" type="button" value="Close" onclick="window.close();"></div>
      </div>
    </form>
  </body>
</HTML>
