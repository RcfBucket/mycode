unit Unit1;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Cards, ComCtrls, StdCtrls, ExtCtrls, Buttons,
  Spin, cxStyles, cxCustomData, DateUtils,
  cxGraphics, cxFilter, cxData, cxDataStorage, cxEdit, cxGridCustomTableView,
  cxGridTableView, cxControls, cxGridCustomView, cxClasses, cxGridLevel, cxGrid;

type
  TPlayerType = (ptPlayer, ptDealer);

  TCardInfo = class
    Suit  : TCardSuit;
    Value : TCardValue;
    order : integer;
    function CardAmount(aceAsOne: boolean): integer;
  end;

  TPlayerCard = class
    card    : TCard;
    cardInfo: TCardInfo;
    constructor Create(aOwner: TWinControl; aCardInfo: TCardInfo;
                                aLeft, aTop: integer;
                                aFaceUp: boolean);
    destructor Destroy; override;
  end;

  TPlayer = class
  private
    fOwner     : TWinControl;
    fCards      : TList;
    fTotal     : integer;
    function GetTotalVisible: boolean;
    procedure SetTotalVisible(const Value: boolean);
    procedure SetTotal(const Value: integer);
    function CalcTotal: integer;
  public
    lblTotal: TLabel;
    startX, startY: integer;

    //
    constructor Create(aOwner: TWinControl);
    destructor Destroy; override;
    procedure AddCard(aCardInfo: TCardInfo; faceUp: boolean = true);
    procedure Clear;
    procedure ShowAllCards;
    property TotalVisible: boolean read GetTotalVisible write SetTotalVisible;
    property Total: integer read fTotal write SetTotal;
  end;

  TForm1 = class(TForm)
    BtnReset: TButton;
    BtnDblDown: TButton;
    BtnSurrender: TButton;
    edBet: TEdit;
    Label3: TLabel;
    Label4: TLabel;
    lblAccount: TLabel;
    BtnStand: TButton;
    BtnHitDeal: TBitBtn;
    Label7: TLabel;
    Label8: TLabel;
    Button1: TButton;
    lblTotalCards: TLabel;
    Label10: TLabel;
    pnlGameBoard: TPanel;
    Label1: TLabel;
    Label2: TLabel;
    Card1: TCard;
    Card2: TCard;
    lblPlayerTotal: TLabel;
    lblDealerTotal: TLabel;
    BtnSplit: TButton;
    Label6: TLabel;
    Label11: TLabel;
    Label12: TLabel;
    Edit1: TEdit;
    Edit2: TEdit;
    Edit3: TEdit;
    Label9: TLabel;
    Edit6: TEdit;
    crdShuf1: TCard;
    crdShuf2: TCard;
    Button2: TButton;
    Button3: TButton;
    CheckBox1: TCheckBox;
    CheckBox2: TCheckBox;
    Button4: TButton;
    Button5: TButton;
    chkDealFaceUp: TCheckBox;
    Button6: TButton;
    Button7: TButton;
    Label13: TLabel;
    lblPlayCounter: TLabel;
    lblBlink: TLabel;
    UpDown1: TUpDown;
    cxGrid1Level1: TcxGridLevel;
    cxGrid1: TcxGrid;
    tvCards: TcxGridTableView;
    tvCardsColumn1: TcxGridColumn;
    tvCardsColumn2: TcxGridColumn;
    tvCardsColumn3: TcxGridColumn;
    cxGrid2: TcxGrid;
    tvShuffle: TcxGridTableView;
    cxGridColumn1: TcxGridColumn;
    cxGridColumn2: TcxGridColumn;
    cxGridColumn3: TcxGridColumn;
    cxGridLevel1: TcxGridLevel;
    procedure FormCreate(Sender: TObject);
    procedure BtnResetClick(Sender: TObject);
    procedure edBetExit(Sender: TObject);
    procedure gridCellLoaded(Sender: TObject; DataCol, DataRow: Integer; var Value: Variant);
    procedure Button1Click(Sender: TObject);
    procedure gridOrderCellLoaded(Sender: TObject; DataCol, DataRow: Integer; var Value: Variant);
    procedure FormDestroy(Sender: TObject);
    procedure CheckBox1Click(Sender: TObject);
    procedure CheckBox2Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure Button4Click(Sender: TObject);
    procedure gridOrderRowChanged(Sender: TObject; OldRow,
      NewRow: Integer);
    procedure Button3Click(Sender: TObject);
    procedure Button5Click(Sender: TObject);
    procedure Button7Click(Sender: TObject);
    procedure Button6Click(Sender: TObject);
    procedure BtnHitDealClick(Sender: TObject);
    procedure BtnDblDownClick(Sender: TObject);
    procedure BtnStandClick(Sender: TObject);
    procedure BtnSurrenderClick(Sender: TObject);
    procedure UpDown1Changing(Sender: TObject; var AllowChange: Boolean);
    procedure Edit6Change(Sender: TObject);
    procedure Edit3Change(Sender: TObject);
    procedure Edit2Change(Sender: TObject);
    procedure Edit1Change(Sender: TObject);
  private
    fDealer : TPlayer;
    fPlayer : TPlayer;
    fAllCards: TList;
    fCurrentCardIdx: integer;  // current available card in deck (-1
    fWager  : integer;    // players wager amount
    fAccount: integer;    // players account
    fDecks  : integer;    // number of decks
    fCurrentBet: integer;
    fPlayCounter: integer;
    procedure SetWager(const Value: integer);
    procedure SetAccount(const Value: integer);
    procedure ResetAll;
    procedure SetDecks(const Value: integer);
    procedure ShuffleDecks;
    function CardByOrder(aOrderIdx: integer): TCardInfo;
    procedure SetCurrentBet(const Value: integer);
    procedure SetCurrentCardIdx(const Value: integer);
    function NextCard: TCardInfo;
    procedure StartGame;
    procedure PlayerBusted;
    procedure SetPlayCounter(const Value: integer);
    function CheckPlayerProgress: boolean;
    procedure PlayDealer;
    procedure EndGame;
    procedure SetBlinkMsg(const Value: string);
    procedure SetMsg(const Value: string);
    procedure FillCardList;
    procedure FillShuffle;
  public
    property Wager: integer read fWager write SetWager;
    property Account: integer read fAccount write SetAccount;
    property Decks: integer read fDecks write SetDecks;
    property CurrentBet: integer read fCurrentBet write SetCUrrentBet;
    property CurrentCardIdx: integer read fCurrentCardIdx write SetCurrentCardIdx;
    property PlayCounter: integer read fPlayCounter write SetPlayCounter;
    property Msg: string write SetMsg;
    property BlinkMsg: string write SetBlinkMsg; 
  end;

var
  Form1: TForm1;

implementation

{$R *.DFM}

const
  MaxDecks            = 8;
  BaseAccountAmount   = 200;
  BaseWagerAmount     = 4;    // keep an even number.

  CardWidth   = 50;
  CardHeight  = 75;
  CardFont    = 14;
  CardSymbol  = 18;

  CardOffset  = 35;

  colSuit     = 1;
  colValue    = 2;
  colOrder    = 3;

  CardValueArray: array[TCardValue] of string = (
    'A', '2', '3', '4', '5', '6', '7', '8', '9', '10', 'J', 'Q', 'K');
  SuitValueArray: array[TCardSuit] of string = (
    'Club', 'Diamond', 'Heart', 'Spade');


procedure SoftSleep(mSecs: integer);
begin
  Application.ProcessMessages;
  Sleep(mSecs);
end;

procedure TForm1.SetAccount(const Value: integer);
begin
  fAccount:= Value;
  lblAccount.Caption:= FloatToStrF(fAccount, ffCurrency, 10, 0);
end;

procedure TForm1.SetWager(const Value: integer);
begin
  if (value >= 2) and (value <= Account) then
  begin
    fWager:= Value;
  end;
  edBet.text:= FloatToStrF(fWager, ffCurrency, 10, 0);
  edBet.Modified:= false;
end;

procedure TForm1.FormCreate(Sender: TObject);
begin
  Randomize;

  PlayCounter:= 0;

  fPlayer:= TPlayer.Create(pnlGameBoard);
  fPlayer.startX:= card1.Left;
  fPlayer.startY:= card1.Top;
  fPlayer.lblTotal:= lblPlayerTotal;

  fDealer:= TPlayer.Create(pnlGameBoard);
  fDealer.startX:= card2.Left;
  fDealer.startY:= card2.Top;
  fDealer.lblTotal:= lblDealerTotal;

  fAllCards:= TList.Create;
  ResetAll;
end;

procedure TForm1.FormDestroy(Sender: TObject);
var
  k   : integer;
begin
  fPlayer.Free;
  fDealer.Free;

  for k:= 0 to fAllCards.Count - 1 do
    TCardInfo(fAllCards[k]).Free;
  fAllCards.Free;
end;

procedure TForm1.ShuffleDecks;
var
  k       : integer;
  count   : integer;
  num     : integer;
  tmp     : TCardInfo;
  Start   : TDateTime;
  EndTime : TDateTime;
begin
  // clear existing cards
  for k:= 0 to fAllCards.Count-1 do
  begin
    TCardInfo(fAllCards[k]).order:= -1;
  end;

  // now actually shuffle
  count:= 0;
  repeat
    num:= Random(fAllCards.Count);
    tmp:= fAllCards[num];
    if tmp.order = -1 then
    begin
      tmp.order:= count;
      Inc(count);
    end;
  until count = fAllCards.Count;


  crdShuf1.Visible:= true;
  crdShuf2.Visible:= true;
  Msg:= '-- Shuffling --';
  Start:= Time;
  EndTime:= IncSecond(start, 2);
  while Time <= endTime do
  begin
    crdShuf1.Suit:= TCardSuit(Random(Ord(High(TCardSuit))));
    crdShuf1.Value:= TCardValue(Random(Ord(High(TCardValue))));
    crdShuf2.Suit:= TCardSuit(Random(Ord(High(TCardSuit))));
    crdShuf2.Value:= TCardValue(Random(Ord(High(TCardValue))));
    application.ProcessMessages;
    Sleep(175);
  end;
  crdShuf1.Visible:= false;
  crdShuf2.Visible:= false;
  CurrentBet:= fCurrentBet; // restore label
  CurrentCardIdx:= 0;
  FillCardList;
  FillShuffle;
end;

procedure TForm1.BtnResetClick(Sender: TObject);
begin
  ResetAll;
end;

procedure TForm1.ResetAll;
begin
  PlayCounter:= 0;
  Account:= BaseAccountAmount;
  Wager:= BaseWagerAmount;
  CurrentBet:= 0;
  Decks:= 2;
  CurrentCardIdx:= -1;

  BtnStand.Enabled:= false;
  BtnDblDown.Enabled:= false;
  BtnSurrender.Enabled:= false;
  BtnSplit.Enabled:= false;
  fPlayer.Clear;
  fDealer.Clear;
end;

(*procedure TForm1.OvcSpinner1Click(Sender: TObject; State: TOvcSpinState;
  Delta: Double; Wrap: Boolean);
begin
  if state = ssDownBtn then
    Delta:= Delta * -1;
  Wager:= Wager + Trunc(Delta);
end;
*)

procedure TForm1.edBetExit(Sender: TObject);
begin
  if edBet.Modified then
  begin
    Wager:= StrToInt(edBet.text);
    edBet.Modified:= false;
  end;
end;

procedure TForm1.SetDecks(const Value: integer);

  procedure BuildADeck;
  var
    s   : TCardSuit;
    v   : TCardValue;
    tmp : TCardInfo;
  begin
    for s:= Low(TCardSuit) to High(TCardSuit) do
    begin
      for v:= Low(TCardValue) to High(TCardValue) do
      begin
        tmp:= TCardInfo.Create;
        tmp.Suit:= s;
        tmp.Value:= v;
        tmp.order:= -1;
        fAllCards.Add(tmp);
      end;
    end;
  end;

var
  k     : integer;
begin
  // First - free up any existing cards.
  for k:= 0 to fAllCards.Count - 1 do
    TCardInfo(fAllCards[k]).Free;
  fAllCards.Clear;
  lblTotalCards.Caption:= '0';

  if (value >= 1) and (value <= MaxDecks) then
  begin
    fDecks:= Value;
    for k:= 1 to fDecks do
      BuildADeck;
  end;
  FillCardList;
  FillShuffle;

  CurrentCardIdx:= -1;
end;

procedure TForm1.FillShuffle;
var
  card : TCardInfo;
  k: integer;
begin
  tvShuffle.BeginUpdate;
  tvShuffle.DataController.RecordCount:= fAllCards.Count;
  lblTotalCards.Caption:= IntToStr(fAllCards.Count);
  for k:= 0 to fAllCards.Count - 1 do
  begin
    card:= CardByOrder(k);
    if card <> nil then
    begin
      tvShuffle.DataController.Values[k, 0]:= SuitValueArray[card.Suit];
      tvShuffle.DataController.Values[k, 1]:= CardValueArray[card.Value];
      tvShuffle.DataController.Values[k, 2]:= IntToStr(card.order);
    end
    else
      tvShuffle.DataController.Values[k, 0]:= 'not shuffled';
  end;
  tvShuffle.EndUpdate;
end;

procedure TForm1.FillCardList;
var
  card : TCardInfo;
  k: integer;
begin
  tvCards.BeginUpdate;
  tvCards.DataController.RecordCount:= fAllCards.Count;
  label10.Caption:= IntToStr(fAllCards.Count);
  for k:= 0 to fAllCards.Count - 1 do
  begin
    card:= TCardInfo(fAllCards[k]);
    tvCards.DataController.Values[k, 0]:= SuitValueArray[card.Suit];
    tvCards.DataController.Values[k, 1]:= CardValueArray[card.Value];
    tvCards.DataController.Values[k, 2]:= IntToStr(card.order);
  end;
  tvCards.EndUpdate;
end;

procedure TForm1.gridCellLoaded(Sender: TObject; DataCol,
  DataRow: Integer; var Value: Variant);
var
  card  : TCardInfo;
begin
  card:= fAllCards[dataRow-1];
  case DataCol of
    colSuit:  value:= SuitValueArray[card.Suit];
    colValue: value:= CardValueArray[card.Value];
    colOrder: value:= IntToStr(card.order);
  end;
end;

procedure TForm1.gridOrderCellLoaded(Sender: TObject; DataCol,
  DataRow: Integer; var Value: Variant);
var
  card  : TCardInfo;
begin
  card:= CardByOrder(dataRow-1);
  value:= '';
  if card <> nil then
  begin
    case DataCol of
      colSuit:  value:= SuitValueArray[card.Suit];
      colValue: value:= CardValueArray[card.Value];
      colOrder: value:= IntToStr(card.order);
    end;
  end;
end;

function TForm1.NextCard: TCardInfo;
// return the next card, shuffling if needed.
begin
  if (CurrentCardIdx = -1) then
  begin
    ShuffleDecks;
    CurrentCardIdx:= 0;
  end;
  result:= CardByOrder(CurrentCardIdx);
  if CurrentCardIdx + 1 >= fAllCards.Count then
    CurrentCardIdx:= -1
  else
    CurrentCardIdx:= CurrentCardIdx + 1;
end;

function TForm1.CardByOrder(aOrderIdx: integer): TCardInfo;
// find the card for the given order index.
var
  k   : integer;
  tmp : TCardInfo;
begin
  result:= nil;
  k:= 0;
  while (k < fAllCards.Count) and (result = nil) do
  begin
    tmp:= fAllCards[k];
    if tmp.order = aOrderIdx then
      result:= tmp;
    Inc(k);
  end;
end;

procedure TForm1.Button1Click(Sender: TObject);
begin
  ShuffleDecks;
end;


procedure TForm1.setcurrentbet(const Value: integer);
begin
  Msg:= FloatToStrF(value, ffCurrency, 10, 0);
  fCurrentBet := Value;
end;

{ TCardInfo }

function TCardInfo.CardAmount(aceAsOne: boolean): integer;
begin
  if value = cvAce then
  begin
    if aceAsOne then
      result:= 1
    else
      result:= 11;
  end
  else
  begin
    if value <= cvTen then
      result:= Ord(value) + 1
    else
      result:= 10;
  end;
end;

procedure TForm1.SetCurrentCardIdx(const Value: integer);
begin
  if (value >= 0) and (value < fAllCards.Count) then
  begin
    fCurrentCardIdx:= Value;
    tvShuffle.DataController.FocusedRecordIndex:= CurrentCardIdx;
  end
  else if value = -1 then
  begin
    fCurrentCardIdx:= value;
    tvShuffle.DataController.FocusedRecordIndex:= 0;
  end;
end;

{ TPlayerCard }

constructor TPlayer.Create(aOwner: TWinControl);
begin
  inherited Create;
  fOwner:= aOwner;
  fCards:= TList.Create;
end;

destructor TPlayer.Destroy;
begin
  inherited;
  Clear;
  fcards.Free;
end;

function TPlayer.CalcTotal: integer;
var
  k   : integer;
  aces: integer;
  tmp : TPlayerCard;
  plainTotal: integer;
begin
  result:= 0;
  aces:= 0;
  for k:= 0 to fcards.Count - 1 do
  begin
    tmp:= fcards[k];
    if tmp.cardInfo.Value <> cvAce then
      Inc(result, tmp.cardInfo.CardAmount(false))
    else
      Inc(aces);
  end;
  plainTotal:= result;

  // now all is summed up except for aces.
  for k:= 1 to aces do
  begin
    if result + 11 > 21 then
      Inc(result, 1)
    else
      Inc(result, 11);
  end;
  if (aces > 0) and (result > 21) then
    result:= plainTotal + aces;
end;

function TPlayer.GetTotalVisible: boolean;
begin
  if lblTotal <> nil then
    result:= lblTotal.Visible
  else
    result:= false;
end;

procedure TPlayer.SetTotal(const Value: integer);
begin
  fTotal:= Value;
  if lblTotal <> nil then
  begin
    if fTotal > 21 then
      lblTotal.Caption:= Format('Busted: %d', [fTotal])
    else
      lblTotal.Caption:= IntToStr(fTotal);
  end;
end;

procedure TPlayer.SetTotalVisible(const Value: boolean);
begin
  if lblTotal <> nil then
    lblTotal.Visible:= value;
end;

procedure TForm1.CheckBox1Click(Sender: TObject);
begin
  fPlayer.TotalVisible:= checkBox1.Checked;
end;

procedure TForm1.CheckBox2Click(Sender: TObject);
begin
  fDealer.TotalVisible:= checkBox2.Checked;
end;

procedure TPlayer.Clear;
var
  k   : integer;
begin
  for k:= 0 to fcards.Count - 1 do
    TPlayerCard(fcards[k]).Free;
  fcards.Clear;
  Total:= 0;
end;

procedure TPlayer.AddCard(aCardInfo: TCardInfo; faceUp: boolean = true);
var
  tmpCard   : TPlayerCard;
  x,y       : integer;
begin
  if fcards.count > 0 then
  begin
    tmpCard:= fcards[fcards.Count-1];
    x:= tmpCard.card.Left + CardOffset;
    y:= tmpCard.Card.Top;
  end
  else
  begin
    x:= startX;
    y:= startY;
  end;

  tmpCard:= TPlayerCard.Create(fOwner, aCardInfo, x,y, faceUp);
  fcards.Add(tmpCard);
  Total:= CalcTotal;
end;

{ TPlayerCard }

constructor TPlayerCard.Create(aOwner: TWinControl; aCardInfo: TCardInfo;
                                aLeft, aTop: integer;
                                aFaceUp: boolean);
begin
  inherited Create;
  card:= TCard.Create(aOwner);
  card.FaceUp:= aFaceUp;
  card.Left:= aLeft;
  card.Top:= aTop;
  card.Width:= cardWidth;
  card.FontSize:= CardFont;
  card.SymbolSize:= CardSymbol;
  card.Height:= CardHeight;
  card.Parent:= aOwner;
  cardInfo:= TCardInfo.Create;
  cardInfo.Suit:= aCardInfo.Suit;
  cardInfo.Value:= aCardInfo.Value;
  card.Suit:= cardInfo.Suit;
  card.Value:= cardInfo.Value;
end;

destructor TPlayerCard.Destroy;
begin
  card.Free;
  cardInfo.Free;
  inherited;
end;

procedure TForm1.Button4Click(Sender: TObject);
begin
  fPlayer.Clear;
end;

procedure TForm1.Button2Click(Sender: TObject);
begin
  fPlayer.AddCard(NextCard, chkDealFaceUp.Checked);
end;

procedure TForm1.Button3Click(Sender: TObject);
begin
  fDealer.AddCard(NextCard, chkDealFaceUp.Checked);
end;

procedure TForm1.gridOrderRowChanged(Sender: TObject; OldRow,
  NewRow: Integer);
begin
  fCurrentCardIdx:= NewRow-1;
  edit6.text:= IntToStr(fCurrentCardIdx);
end;

procedure TForm1.Button5Click(Sender: TObject);
begin
  fDealer.Clear;
end;

procedure TPlayer.ShowAllCards;
var
  k   : integer;
begin
  for k:= 0 to fCards.Count - 1 do
    TPlayerCard(fCards[k]).card.FaceUp:= true;
end;

procedure TForm1.Button7Click(Sender: TObject);
begin
  fPlayer.ShowAllCards;
end;

procedure TForm1.Button6Click(Sender: TObject);
begin
  fDealer.ShowAllCards;
end;

procedure TForm1.BtnHitDealClick(Sender: TObject);
begin
  if PlayCounter = 0 then
    StartGame
  else
  begin
    PlayCounter:= PlayCounter + 1;
    fPlayer.AddCard(NextCard, true);
    BtnDblDown.Enabled:= false;
    BtnSurrender.Enabled:= false;
    BtnSplit.Enabled:= false;
    BtnStand.Enabled:= true;
    if not CheckPlayerProgress then
      PlayDealer;
  end;
end;

procedure TForm1.BtnStandClick(Sender: TObject);
begin
  PlayDealer;
end;

procedure TForm1.SetPlayCounter(const Value: integer);
begin
  fPlayCounter := Value;
  lblPlayCounter.Caption:= IntToStr(fPlayCounter);
end;

procedure TForm1.BtnDblDownClick(Sender: TObject);
begin
  fPlayer.AddCard(NextCard, true);
  CurrentBet:= CurrentBet * 2;
  if CheckPlayerProgress then
    PlayDealer;
end;

function TForm1.CheckPlayerProgress: boolean;
begin
  if fPlayer.Total > 21 then
  begin
    PlayerBusted;
    result:= false;
  end
  else
  begin
    result:= true;
  end;
end;

procedure TForm1.PlayDealer;
begin
  fDealer.ShowAllCards;
  if fPlayer.Total <= 21 then
  begin
    while (fDealer.Total < 17) do
    begin
      fDealer.AddCard(NextCard);
    end;
  end;
  EndGame;
end;

procedure TForm1.Edit1Change(Sender: TObject);
begin
  Account:= StrToInt(edit1.Text);
end;

procedure TForm1.Edit2Change(Sender: TObject);
begin
  Decks:= StrToInt(edit2.text);
end;

procedure TForm1.Edit3Change(Sender: TObject);
begin
  CurrentBet:= StrToInt(edit3.text);
end;

procedure TForm1.Edit6Change(Sender: TObject);
begin
  CurrentCardIdx:= StrToInt(edit6.text);
end;

procedure TForm1.EndGame;
begin
  PlayCounter:= 0;
  // see who won and adjust totals.
  if ((fPlayer.Total <= 21) and (fPlayer.Total > fDealer.Total)) or (fDealer.Total > 21) then
  begin
    BlinkMsg:= 'WINNER!';
    Account:= Account + CurrentBet;
  end
  else if fPlayer.Total < fDealer.Total then
  begin
    BlinkMsg:= 'LOSER!';
    Account:= Account - CurrentBet;
  end
  else
    BlinkMsg:= 'Draw';
end;

procedure TForm1.StartGame;
// Deal out the initial cards
begin
  CurrentBet:= Wager;
  fPlayer.Clear;
  fDealer.Clear;
  fPlayer.AddCard(NextCard, true);
  SoftSleep(150);
  fDealer.AddCard(NextCard, true);
  SoftSleep(150);
  fPlayer.AddCard(NextCard, true);
  SoftSleep(150);
  fDealer.AddCard(NextCard, false);
  PlayCounter:= 2;
  if fPlayer.Total = 21 then
  begin
    // BLACK JACK!
    PlayCounter:= 0;
    fDealer.ShowAllCards;
    Account:= Account + CurrentBet + (CurrentBet div 2);
    BlinkMsg:= 'Black Jack!';
  end
  else
  begin
    BtnDblDown.Enabled:= true;
    BtnSurrender.Enabled:= true;
    BtnSplit.Enabled:= true;
    BtnStand.Enabled:= true;
  end;
end;

procedure TForm1.UpDown1Changing(Sender: TObject; var AllowChange: Boolean);
begin
 CurrentCardIdx:= StrToInt(edit6.text);
end;

procedure TForm1.PlayerBusted;
var
  k   : integer;
begin
  Msg:= 'BUSTED';
  fDealer.ShowAllCards;
  application.ProcessMessages;
  Sleep(750);
  BlinkMsg:= Format('BUSTED  %d -> %d', [Account, Account - CurrentBet]);
  for k:= 1 to CurrentBet do
  begin
    Account:= Account - 1;
    Sleep(200);
    application.ProcessMessages;
  end;
  BtnDblDown.Enabled:= false;
  BtnSurrender.Enabled:= false;
  BtnSplit.Enabled:= false;
  BtnStand.Enabled:= false;
  PlayCounter:= 0;
end;


procedure TForm1.BtnSurrenderClick(Sender: TObject);
begin
  // player only loses half of his bet if surrendering
  Account:= Account - CurrentBet div 2;
end;

procedure TForm1.SetBlinkMsg(const Value: string);
begin
  lblBlink.Caption:= value;
end;

procedure TForm1.SetMsg(const Value: string);
begin
  lblBlink.Caption:= value;
end;

end.
