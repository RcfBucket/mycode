unit Main;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Cards, ComCtrls, StdCtrls, OvcBase, OvcSC, ExtCtrls, NubEdit, Buttons,
  Grids_ts, TSGrid, StDate;

type
  TPlayerType = (ptPlayer, ptDealer);

  TCardInfo = class
    Suit  : TCardSuit;
    Value : TCardValue;
    order : integer;
    function CardAmount(aceAsOne: boolean): integer;
  end;

  TPlayerCard = class
    cardInfo   : TCardInfo;
    card       : TCard;
    x, y       : integer;
  private
    function GetVisible: boolean;
    procedure SetVisible(const Value: boolean);
    function GetLeft: integer;
    procedure SetLeft(const Value: integer);
    function GetTop: integer;
    procedure SetTop(const Value: integer);
  public
    constructor Create(aOwner: TComponent);
    destructor Destroy; override;
    property Visible: boolean read GetVisible write SetVisible;
    property Left: integer read GetLeft write SetLeft;
    property Top: integer read GetTop write SetTop;
  end;

  TFrmMain = class(TForm)
    BtnReset: TButton;
    BtnDblDown: TButton;
    BtnSurrender: TButton;
    edBet: TEdit;
    Label3: TLabel;
    Label4: TLabel;
    lblAccount: TLabel;
    OvcSpinner1: TOvcSpinner;
    BtnStand: TButton;
    BitBtn1: TBitBtn;
    Label5: TLabel;
    edDecks: TNubEdit;
    grid: TtsGrid;
    gridShuffle: TtsGrid;
    Label7: TLabel;
    Label8: TLabel;
    Button1: TButton;
    Label9: TLabel;
    Label10: TLabel;
    pnlGame: TPanel;
    Label1: TLabel;
    Label2: TLabel;
    Card1: TCard;
    Card6: TCard;
    Card5: TCard;
    Card4: TCard;
    Card2: TCard;
    Card3: TCard;
    Card7: TCard;
    Card12: TCard;
    Card11: TCard;
    Card10: TCard;
    Card9: TCard;
    Card8: TCard;
    lblPlayerTotal: TLabel;
    lblDealerTotal: TLabel;
    BtnSplit: TButton;
    procedure FormCreate(Sender: TObject);
    procedure BtnResetClick(Sender: TObject);
    procedure OvcSpinner1Click(Sender: TObject; State: TOvcSpinState; Delta: Double; Wrap: Boolean);
    procedure edBetExit(Sender: TObject);
    procedure edDecksReturn(Sender: TObject);
    procedure gridCellLoaded(Sender: TObject; DataCol, DataRow: Integer; var Value: Variant);
    procedure Button1Click(Sender: TObject);
    procedure gridShuffleCellLoaded(Sender: TObject; DataCol, DataRow: Integer; var Value: Variant);
    procedure FormDestroy(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure BtnStandClick(Sender: TObject);
  private
    fDealer : TList;
    fPlayer : TList;
    fAllCards: TList;
    fCurrentCard: integer;  // current available card in deck (-1
    fDealerCards  : integer;   // current number of cards played for dealer
    fPlayerCards  : integer;   // current number of cards played for player
    fPlayerTotal  : integer;
    fDealerTotal  : integer;

    fWager  : integer;    // players wager amount
    fAccount: integer;    // players account
    fDecks  : integer;    // number of decks
    fDeckLimit: integer;
    fCurrentBet: integer;
    procedure SetWager(const Value: integer);
    procedure SetAccount(const Value: integer);
    procedure ResetAll;
    procedure SetDecks(const Value: integer);
    procedure ShuffleDecks;
    function CardByOrder(aOrder: integer): TCardInfo;
    procedure DisplayRandomCards;
    procedure TurnCards(aFaceUp: boolean);
    procedure SetCurrentBet(const Value: integer);
    procedure DealCard(aPlayer: TPlayerType; flipIt: boolean = true);
    procedure SetPlayerTotal(const Value: integer);
    procedure SetDealerTotal(const Value: integer);
    procedure PlayDealer;
    procedure BuildCardList;
  public
    property Wager: integer read fWager write SetWager;
    property Account: integer read fAccount write SetAccount;
    property Decks: integer read fDecks write SetDecks;
    property CurrentBet: integer read fCurrentBet write SetCUrrentBet;
    property PlayerTotal: integer read fPlayerTotal write SetPlayerTotal;
    property DealerTotal: integer read fDealerTotal write SetDealerTotal;
  end;

var
  FrmMain: TFrmMain;

implementation

{$R *.DFM}

const
  MaxDecks            = 8;
  BaseAccountAmount   = 200;
  BaseWagerAmount     = 5;
  BaseDecks           = 2;

  colSuit     = 1;
  colValue    = 2;
  colOrder    = 3;

  CardValueArray: array[TCardValue] of string = (
    'A', '2', '3', '4', '5', '6', '7', '8', '9', '10', 'J', 'Q', 'K');
  SuitValueArray: array[TCardSuit] of string = (
    'Club', 'Diamond', 'Heart', 'Spade');

procedure TFrmMain.SetAccount(const Value: integer);
begin
  fAccount:= Value;
  lblAccount.Caption:= FloatToStrF(fAccount, ffCurrency, 10, 0);
end;

procedure TFrmMain.SetWager(const Value: integer);
begin
  if (value > 0) and (value <= Account) then
  begin
    fWager:= Value;
  end;
  edBet.text:= FloatToStrF(fWager, ffCurrency, 10, 0);
  edBet.Modified:= false;
end;

procedure TFrmMain.FormCreate(Sender: TObject);
var
  count : integer;

  procedure SetupPlayers;
  begin
    fPlayer.Add(card1);
    fPlayer.Add(card2);
    fPlayer.Add(card3);
    fPlayer.Add(card4);
    fPlayer.Add(card5);
    fPlayer.Add(card6);

    fDealer.Add(card7);
    fDealer.Add(card8);
    fDealer.Add(card9);
    fDealer.Add(card10);
    fDealer.Add(card11);
    fDealer.Add(card12);
  end;

begin
  Randomize;
  grid.Col[colSuit].Heading:= 'Suit';
  grid.Col[colValue].Heading:= 'Value';
  grid.Col[colOrder].Heading:= 'Order';

  fPlayer:= TList.Create;
  fDealer:= TList.Create;
  fAllCards:= TList.Create;
  fCurrentCard:= -1;

  grid.Rows:= fCards.count;
  label10.Caption:= IntToStr(grid.Rows);

  SetupPlayers;
  ResetAll;
end;

procedure TFrmMain.BuildCardList;
// build a list of all cards in the current number of decks.

  procedure BuildADeck;
  var
    s   : TCardSuit;
    v   : TCardValue;
    tmp : TCardInfo;
  begin
    for s:= Low(TCardSuit) to High(TCardSuit) do
    begin
      for v:= Low(TCardValue) to High(TCardValue) do
      begin
        tmp:= TCardInfo.Create;
        tmp.Suit:= s;
        tmp.Value:= v;
        tmp.order:= -1;
        fCards.Add(tmp);
      end;
    end;
  end;
var
  deck  : integer;
begin
  // First - free up any existing cards.
  for count:= 0 to fAllCards.Count - 1 do
    TCardInfo(fAllCards[count]).Free;
  fAllCards.Clear;

  for count:= 1 to MaxDecks do
    BuildADeck;
end;

procedure TFrmMain.FormDestroy(Sender: TObject);
var
  k   : integer;
begin
  for k:= 0 to fCards.Count - 1 do
    TCardInfo(fCards[k]).Free;
  fCards.Free;

  fPlayer.Free;
  fDealer.Free;
end;

procedure TFrmMain.TurnCards(aFaceUp: boolean);
var
  k   : integer;
begin
  for k:= 0 to fPlayer.Count - 1 do
    TCard(fPlayer[k]).FaceUp:= aFaceUp;
  for k:= 0 to fPlayer.Count - 1 do
    TCard(fDealer[k]).FaceUp:= aFaceUp;
end;

procedure TFrmMain.PlayDealer;
var
  done    : boolean;
begin
  done:= false;
  TCard(fDealer[1]).FaceUp:= true;  // turn up hidden card

  if DealerTotal < PlayerTotal then
  begin
    while not done and (DealerTotal < 17) do
    begin
      DealCard(ptDealer);
      Application.ProcessMessages;
      Sleep(500);
    end;
  end;

  // See who won.
  if DealerTotal <= 21 then
  begin
    if DealerTotal = PlayerTotal then
      pnlGame.Caption:= 'Draw'
    else if DealerTotal > PlayerTotal then
    begin
      pnlGame.Caption:= 'Loss';
      Account:= Account - CurrentBet;
    end
    else
    begin
      pnlGame.Caption:= 'Win';
      Account:= Account + CurrentBet*2;
    end;
  end
  else
  begin
    Account:= Account + CurrentBet*2;
    pnlGame.Caption:= 'Win'
  end;

  fPlayerCards:= 0;
  fDealerCards:= 0;
end;

procedure TFrmMain.ShuffleDecks;
var
  k       : integer;
  count   : integer;
  num     : integer;
  tmp     : TCardInfo;
  Start   : TStTime;
  EndTime : TStTime;
begin
  // clear exiting cards
  for k:= 0 to fCards.Count-1 do
  begin
    with TCardInfo(fCards[k]) do
    begin
      order:= -1;
    end;
  end;

  // now actually shuffle
  count:= 0;
  repeat
    num:= Random(fCards.Count);
    tmp:= fCards[num];
    if tmp.order = -1 then
    begin
      tmp.order:= count;
      Inc(count);
    end;
  until count = fCards.Count;


  pnlGame.caption:= '-- Shuffling --';
  Start:= CurrentTime;
  EndTime:= IncTime(start, 0, 0, 1);
  TurnCards(true);
  while CurrentTime <= endTime do
  begin
    DisplayRandomCards;
    application.ProcessMessages;
    Sleep(150);
  end;
  CurrentBet:= fCurrentBet; // restore label

  TurnCards(false);
  fPlayerCards:= 0;
  fDealerCards:= 0;

  gridShuffle.RefreshData(roNone, rpNone);
  grid.RefreshData(roNone, rpNone);
end;

procedure TFrmMain.DisplayRandomCards;

  procedure DoOne(aList: TList);
  var
    k     : integer;
    card  : TCard;
  begin
    for k:= 0 to fPlayer.Count - 1 do
    begin
      card:= aList[k];
      card.Suit:= TCardSuit(Random(Ord(High(TCardSuit))));
      card.Value:= TCardValue(Random(Ord(High(TCardValue))));
      application.ProcessMessages;
    end;
  end;

begin
  DoOne(fPlayer);
  DoOne(fDealer);
end;

procedure TFrmMain.BtnResetClick(Sender: TObject);
begin
  ResetAll;
end;

procedure TFrmMain.ResetAll;
begin
  fDealerCards:= 0;
  fPlayerCards:= 0;
  Account:= BaseAccountAmount;
  Wager:= BaseWagerAmount;
  CurrentBet:= 0;
  PlayerTotal:= 0;
  Decks:= BaseDecks;
  TurnCards(false);
  fCurrentCard:= -1;

  BtnStand.Enabled:= false;
  BtnDblDown.Enabled:= false;
  BtnSurrender.Enabled:= false;
  BtnSplit.Enabled:= false;
end;

procedure TFrmMain.OvcSpinner1Click(Sender: TObject; State: TOvcSpinState;
  Delta: Double; Wrap: Boolean);
begin
  if state = ssDownBtn then
    Delta:= Delta * -1;
  Wager:= Wager + trunc(Delta);
end;

procedure TFrmMain.edBetExit(Sender: TObject);
begin
  if edBet.Modified then
  begin
    Wager:= StrToInt(edBet.text);
    edBet.Modified:= false;
  end;
end;

procedure TFrmMain.edDecksReturn(Sender: TObject);
begin
  Decks:= StrToInt(edDecks.text);
end;

procedure TFrmMain.SetDecks(const Value: integer);
begin
  if (value >= 1) and (value <= MaxDecks) then
  begin
    fDecks:= Value;
    fDeckLimit:= fDecks * 52;
    gridShuffle.Rows:= fDeckLimit;
    fCurrentCard:= -1;
    label9.Caption:= IntToStr(fDeckLimit);
  end;
  edDecks.text:= IntToStr(fDecks);
end;

procedure TFrmMain.gridCellLoaded(Sender: TObject; DataCol,
  DataRow: Integer; var Value: Variant);
var
  card  : TCardInfo;
begin
  card:= fCards[dataRow-1];
  case DataCol of
    colSuit:  value:= SuitValueArray[card.Suit];
    colValue: value:= CardValueArray[card.Value];
    colOrder: value:= IntToStr(card.order);
  end;
end;

procedure TFrmMain.gridShuffleCellLoaded(Sender: TObject; DataCol,
  DataRow: Integer; var Value: Variant);
var
  card  : TCardInfo;
begin
  card:= CardByOrder(dataRow-1);
  value:= '';
  if card <> nil then
  begin
    case DataCol of
      colSuit:  value:= SuitValueArray[card.Suit];
      colValue: value:= CardValueArray[card.Value];
      colOrder: value:= IntToStr(card.order);
    end;
  end;
end;

function TFrmMain.CardByOrder(aOrder: integer): TCardInfo;
// find the card for the given order.
var
  k   : integer;
  tmp : TCardInfo;
begin
  result:= nil;
  k:= 0;
  while (k < fCards.Count) and (result = nil) do
  begin
    tmp:= fCards[k];
    if tmp.order = aOrder then
      result:= tmp;
    Inc(k);
  end;
end;

procedure TFrmMain.Button1Click(Sender: TObject);
begin
  ShuffleDecks;
end;


procedure TFrmMain.setcurrentbet(const Value: integer);
begin
  pnlGame.Caption:= FloatToStrF(value, ffCurrency, 10, 0);
  fCurrentBet := Value;
end;

procedure TFrmMain.BitBtn1Click(Sender: TObject);
var
  k   : integer;
begin
  if fCurrentCard = -1 then
  begin
    ShuffleDecks;
    fCurrentCard:= 0;
    gridShuffle.CurrentDataRow:= fCurrentCard + 1;
  end;

  // if first deal for hand?
  if fPlayerCards = 0 then
  begin
    TurnCards(false);

    BtnStand.Enabled:= true;
    BtnSplit.Enabled:= true;
    BtnDblDown.Enabled:= true;
    BtnSurrender.Enabled:= true;

    CurrentBet:= Wager;
    PlayerTotal:= 0;
    DealerTotal:= 0;

    DealCard(ptPlayer);
    DealCard(ptDealer);
    DealCard(ptPlayer);
    DealCard(ptDealer, false);
  end
  else
  begin
    // Hear I am receiving a "hit"
    DealCard(ptPlayer);

    // Disable unavailable options.
    BtnDblDown.Enabled:= false;
    BtnSurrender.Enabled:= false;
    BtnSplit.Enabled:= false;

    // now check for "bust"
    if PlayerTotal > 21 then
    begin
      pnlGame.Caption:= '-- BUST --';
      fPlayerCards:= 0;
      fDealerCards:= 0;
      TCard(fDealer[1]).FaceUp:= true;
      Account:= Account - CurrentBet;
    end;
  end;
end;

procedure TFrmMain.DealCard(aPlayer: TPlayerType; flipIt: boolean = true);
var
  tmp   : TCardInfo;
  card  : TCard;
begin
  if fCurrentCard >= fDeckLimit then
  begin
    ShuffleDecks;
    fCurrentCard:= 0;
    gridShuffle.CurrentDataRow:= fCurrentCard + 1;
  end;

  if aPlayer = ptDealer then
  begin
    tmp:= CardByOrder(fCurrentCard);
    card:= fDealer[fDealerCards];
    card.Suit:= tmp.Suit;
    card.Value:= tmp.Value;
    card.FaceUp:= flipIt;
    Inc(fDealerCards);
    Inc(fCurrentCard);
    DealerTotal:= DealerTotal + tmp.CardAmount(true);
  end
  else
  begin
    tmp:= CardByOrder(fCurrentCard);
    card:= fPlayer[fPlayercards];
    card.Suit:= tmp.Suit;
    card.Value:= tmp.Value;
    card.FaceUp:= true;
    Inc(fPlayerCards);
    Inc(fCurrentCard);
    PlayerTotal:= PlayerTotal + tmp.CardAmount(true);
  end;
  gridShuffle.CurrentDataRow:= fCurrentCard + 1;
end;

procedure TFrmMain.SetPlayerTotal(const Value: integer);
begin
  fPlayerTotal:= Value;
  lblPlayerTotal.Caption:= IntToStr(value);
end;

procedure TFrmMain.SetDealerTotal(const Value: integer);
begin
  fDealerTotal:= Value;
  lblDealerTotal.Caption:= IntToStr(value);
end;

{ TCardInfo }

function TCardInfo.CardAmount(aceAsOne: boolean): integer;
begin
  if value = cvAce then
  begin
    if aceAsOne then
      result:= 1
    else
      result:= 11;
  end
  else
  begin
    if value <= cvTen then
      result:= Ord(value) + 1
    else
      result:= 10;
  end;
end;

procedure TFrmMain.BtnStandClick(Sender: TObject);
begin
  PlayDealer;
end;

{ TPlayerCard }

constructor TPlayerCard.Create(aOwner: TComponent);
begin
  inherited Create;
  card:= TCard.Create(aOwner);
end;

destructor TPlayerCard.Destroy;
begin
  inherited;
  card.Free;
end;

function TPlayerCard.GetLeft: integer;
begin
  result:= card.Left;
end;

function TPlayerCard.GetTop: integer;
begin
  result:= card.Top;
end;

function TPlayerCard.GetVisible: boolean;
begin
  result:= card.Visible;
end;

procedure TPlayerCard.SetLeft(const Value: integer);
begin
  card.Left:= value;
end;

procedure TPlayerCard.SetTop(const Value: integer);
begin
  card.Top:= value;
end;

procedure TPlayerCard.SetVisible(const Value: boolean);
begin
  card.visible:= value;
end;

end.
