�
 TFRMMAIN 0f  TPF0TFrmMainFrmMainLeft7ToprWidthHeightCaption
Black JackColor	clBtnFaceFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style OldCreateOrderPositionpoDesktopCenterOnCreate
FormCreate	OnDestroyFormDestroyPixelsPerInch`
TextHeight TLabelLabel3LeftTopWidth HeightCaptionWager  TLabelLabel4LeftTop0Width(HeightCaptionAccount  TLabel
lblAccountLeftHTop0Width=HeightCaption
lblAccountFont.CharsetDEFAULT_CHARSET
Font.ColorclBlueFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFont  TLabelLabel5LefthTop4WidthHeightCaptionDecks  TLabelLabel7LeftTopHWidth)HeightCaption	All Cards  TLabelLabel8LeftTopHWidthFHeightCaptionCurrent Shuffle  TLabelLabel9Left`TopHWidth HeightCaptionLabel9  TLabelLabel10LeftHTopHWidth&HeightCaptionLabel10  TPanelpnlGameLeftTopWidthqHeight
BevelInner	bvLoweredCaption$0Color �@ Font.CharsetDEFAULT_CHARSET
Font.ColorclYellowFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFontTabOrder TLabelLabel1LeftTopWidthHeightCaptionDealerFont.CharsetDEFAULT_CHARSET
Font.ColorclWhiteFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont  TLabelLabel2LeftTop� WidthHeightCaptionPlayerFont.CharsetDEFAULT_CHARSET
Font.ColorclWhiteFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont  TLabellblPlayerTotalLeftWTop� WidthHeight	AlignmenttaRightJustifyCaption0Font.CharsetDEFAULT_CHARSET
Font.ColorclWhiteFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont  TLabellblDealerTotalLeftWTopWidthHeight	AlignmenttaRightJustifyCaption0Font.CharsetDEFAULT_CHARSET
Font.ColorclWhiteFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont  TCardCard1LeftTop� Width1HeightI	BackColorclSilverFaceUpSuitcsHeartValuecvAce
SymbolSizeFontSize  TCardCard6Left0Top� Width1HeightI	BackColorclSilverFaceUpSuitcsHeartValuecvAce
SymbolSizeFontSize  TCardCard5Left� Top� Width1HeightI	BackColorclSilverFaceUpSuitcsHeartValuecvAce
SymbolSizeFontSize  TCardCard4Left� Top� Width1HeightI	BackColorclSilverFaceUpSuitcsHeartValuecvAce
SymbolSizeFontSize  TCardCard2LeftITop� Width1HeightI	BackColorclSilverFaceUpSuitcsHeartValuecvAce
SymbolSizeFontSize  TCardCard3Left� Top� Width1HeightI	BackColorclSilverFaceUpSuitcsHeartValuecvAce
SymbolSizeFontSize  TCardCard7LeftTopWidth1HeightI	BackColorclSilverFaceUpSuitcsSpadeValuecvAce
SymbolSizeFontSize  TCardCard12Left0TopWidth1HeightI	BackColorclSilverFaceUpSuitcsSpadeValuecvAce
SymbolSizeFontSize  TCardCard11Left� TopWidth1HeightI	BackColorclSilverFaceUpSuitcsSpadeValuecvAce
SymbolSizeFontSize  TCardCard10Left� TopWidth1HeightI	BackColorclSilverFaceUpSuitcsSpadeValuecvAce
SymbolSizeFontSize  TCardCard9Left� TopWidth1HeightI	BackColorclSilverFaceUpSuitcsSpadeValuecvAce
SymbolSizeFontSize  TCardCard8LeftITopWidth1HeightI	BackColorclSilverFaceUpSuitcsSpadeValuecvAce
SymbolSizeFontSize   TButtonBtnResetLeft�TopWidth[HeightCaption&ResetTabOrderOnClickBtnResetClick  TButton
BtnDblDownLeft�Top(Width[HeightCaption&Double DownTabOrder  TButtonBtnSurrenderLeft�TopHWidth[HeightCaption
S&urrenderTabOrder  TEditedBetLeftHTopWidthAHeightTabOrderText1OnExit	edBetExit  TOvcSpinnerOvcSpinner1Left� TopWidthHeightAcceleration
AutoRepeat	Delta       ��?	DelayTime�
ShowArrows	StylestDiagonalVerticalWrapMode	OnClickOvcSpinner1Click  TButtonBtnStandLeft�Top� Width[HeightCaption&StandTabOrderOnClickBtnStandClick  TBitBtnBitBtn1Left�Top� Width[Height1Caption	&Hit/DealDefault	TabOrderOnClickBitBtn1Click
Glyph.Data
�   �   BM�       v   (               p                       �  �   �� �   � � ��  ��� ���   �  �   �� �   � � ��  ��� ����������������� �������0������� ������� ������������ ��� �������� ������������ p������ ���������Layout
blGlyphTop  TNubEditedDecksLeft�Top0WidthIHeightClearOnFirstKey
NubinAlignnubTopNubinGlyphColorclGray
NubinWidthNubinGlyphShape	nubCircleNubinEnabled	WantReturns	OnReturnedDecksReturnOnNubinClickedDecksReturn
AutoSelect	BorderStylebsSingleCharCaseecNormalColorclWindowHideSelection	ImeMode
imDontCare	MaxLength 
OEMConvertPasswordChar ReadOnlyTabOrderTabStop	Text2  TtsGridgridLeftTopXWidth� Height� CheckBoxStylestCheck	ColMovingColsColSelectModecsNoneGridMode	gmListBoxHeadingFont.CharsetDEFAULT_CHARSETHeadingFont.ColorclWindowTextHeadingFont.Height�HeadingFont.NameMS Sans SerifHeadingFont.Style ParentShowHint
ResizeRowsrrNone	RowMovingRowSelectModersSingleShowHint	TabOrder ThumbTracking	OnCellLoadedgridCellLoadedColPropertiesDataColCol.DisplayColCol.HeadingSuit	Col.WidthM DataColCol.DisplayColCol.HeadingValue DataColCol.DisplayColCol.HeadingOrder    TtsGridgridShuffleLeftTopXWidth� Height� CheckBoxStylestCheck	ColMovingColsColSelectModecsNoneGridMode	gmListBoxHeadingFont.CharsetDEFAULT_CHARSETHeadingFont.ColorclWindowTextHeadingFont.Height�HeadingFont.NameMS Sans SerifHeadingFont.Style ParentShowHint
ResizeRowsrrNone	RowMovingRows RowSelectModersSingleShowHint	TabOrderThumbTracking	OnCellLoadedgridShuffleCellLoadedColPropertiesDataColCol.DisplayColCol.HeadingSuit	Col.WidthM DataColCol.DisplayColCol.HeadingValue DataColCol.DisplayColCol.HeadingOrder    TButtonButton1Left� Top WidthYHeightCaptionShuffle DecksTabOrder	OnClickButton1Click  TButtonBtnSplitLeft�TophWidth[HeightCaptionSpli&tTabOrder   