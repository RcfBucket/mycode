unit Main;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, ComCtrls, ExtCtrls, PrnData, PrintPrevDlg, TwipLib;

type
  TForm1 = class(TForm)
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    chkNumGrreater: TCheckBox;
    rdoOperation: TRadioGroup;
    Edit1: TEdit;
    Edit2: TEdit;
    Edit3: TEdit;
    Edit4: TEdit;
    Label4: TLabel;
    Label5: TLabel;
    udNumLo: TUpDown;
    udNumHi: TUpDown;
    udDenLo: TUpDown;
    udDenHi: TUpDown;
    BtnGo: TButton;
    Edit5: TEdit;
    udCount: TUpDown;
    pEng: TPrintPrevDlg;
    Label6: TLabel;
    Edit6: TEdit;
    udProbPerLine: TUpDown;
    BtnCancel: TButton;
    edProbWidth: TEdit;
    edSpacing: TEdit;
    Label8: TLabel;
    Label9: TLabel;
    edTestNum: TEdit;
    Label10: TLabel;
    Bevel1: TBevel;
    Label7: TLabel;
    edRowSpacing: TEdit;
    edSeed: TEdit;
    Label11: TLabel;
    Label12: TLabel;
    edLastSeed: TEdit;
    procedure BtnGoClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtnCancelClick(Sender: TObject);
  private
    function Operator: string;
    function OperatorDesc: string;
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

{$R *.DFM}

function GetRandomNum(aLow, aHigh: integer): integer;
begin
  if aLow <= aHigh then
  begin
    repeat
      result:= Random(aHigh+1);
    until (result >= aLow);
  end
  else
    result:= aLow;
end;

function TForm1.Operator: string;
begin
  case rdoOperation.ItemIndex of
    0: result:= '+';
    1: result:= '-';
    2: result:= 'x';
    3: result:= '�';
    else
      result:= '?';
  end;
end;

function TForm1.OperatorDesc: string;
begin
  case rdoOperation.ItemIndex of
    0: result:= 'Addition';
    1: result:= 'Subtraction';
    2: result:= 'Multiplication';
    3: result:= 'Division';
    else
      result:= '?';
  end;
end;

procedure TForm1.BtnGoClick(Sender: TObject);
var
  n, d  : integer;
  count : integer;
  data  : TPrintData;
  row   : integer;
  wid   : integer;
  spac  : integer;
  seed  : integer;
begin
  if edSeed.Text <> '' then
    RandSeed:= StrToInt(edSeed.Text);

  seed:= RandSeed;
  edLastSeed.Text:= IntToStr(seed);

  data:= TPrintData.Create;
  try
    row:= 0;
    data.body.AddRow;
    data.body.AddCell('Name _________________________________', LT400, c_Bold, 12);
    data.body.AddCell(Format('Test %s: %s',[edTestNum.Text, OperatorDesc]), LT300, c_Bold or c_RIght or c_PageRight, 12);
    data.body.AddRow;
    data.body.AddRow;
    data.body.AddCell('Minutes', LT250, c_Center);
    data.body.AddCell('', LT50);
    data.body.AddCell('Score', LT50, c_Center);
    data.body.AddRow(r_Normal, 0, 20);
    data.body.AddCell('1', LT50, c_BorderAll or c_Center or c_VCenter);
    data.body.AddCell('2', LT50, c_BorderAll or c_Center or c_VCenter);
    data.body.AddCell('3', LT50, c_BorderAll or c_Center or c_VCenter);
    data.body.AddCell('4', LT50, c_BorderAll or c_Center or c_VCenter);
    data.body.AddCell('5', LT50, c_BorderAll or c_Center or c_VCenter);
    data.body.AddCell('', LT50);
    data.body.AddCell('', LT50, c_BorderAll);
    data.body.AddRow;
    data.footer.AddRow;
    data.Footer.AddCell(Format('Seed: %d', [seed]), LT400, c_Normal, 0, -2);

    for count:= 0 to udCount.Position-1 do
    begin
      repeat
        n:= GetRandomNum(udNumLo.Position, udNumHi.Position);
        d:= GetRandomNum(udDenLo.Position, udDenHi.Position);
      until (not chkNumGrreater.Checked) or
            (chkNumGrreater.Checked and (n > d));

      with data.body do
      begin
        if (count mod udProbPerLine.Position) = 0 then
        begin
          if count > 0 then
          begin
            AddRow(r_Normal, 0, StrToInt(edRowSpacing.Text));
          end;
          AddRow;
          AddCell(Chr(Ord('A') + row), LT50, c_VCenter);
          Inc(row);
        end;
        wid:= StrToInt(edProbWidth.text);
        spac:= StrToInt(edSpacing.text);
        AddCell(Format('  %d'#10' %s %d', [n,operator,d]), wid, c_BorderBottom or c_Right or c_WordWrap);
        AddCell('', spac);
      end;
    end;
    pEng.Execute(data);
  finally
    data.Free;
  end;
end;

procedure TForm1.FormCreate(Sender: TObject);
begin
  Randomize;
  edLastSeed.Text:= 'n/a';
end;

procedure TForm1.BtnCancelClick(Sender: TObject);
begin
  Close
  
end;

end.
