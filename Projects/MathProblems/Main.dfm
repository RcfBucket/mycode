object Form1: TForm1
  Left = 290
  Top = 123
  BorderIcons = [biSystemMenu, biMinimize]
  BorderStyle = bsSingle
  Caption = 'Math Problem Creator'
  ClientHeight = 345
  ClientWidth = 387
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poDesktopCenter
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 16
    Top = 28
    Width = 84
    Height = 13
    Caption = 'Numerator Range'
  end
  object Label2: TLabel
    Left = 16
    Top = 60
    Width = 95
    Height = 13
    Caption = 'Denominator Range'
  end
  object Label3: TLabel
    Left = 16
    Top = 100
    Width = 139
    Height = 13
    Caption = 'Number of problems to create'
  end
  object Label4: TLabel
    Left = 256
    Top = 28
    Width = 13
    Height = 13
    Caption = 'To'
  end
  object Label5: TLabel
    Left = 256
    Top = 60
    Width = 13
    Height = 13
    Caption = 'To'
  end
  object Label6: TLabel
    Left = 16
    Top = 124
    Width = 85
    Height = 13
    Caption = 'Problems Per Line'
  end
  object Label8: TLabel
    Left = 191
    Top = 228
    Width = 77
    Height = 13
    Caption = 'Column Spacing'
  end
  object Label9: TLabel
    Left = 199
    Top = 252
    Width = 69
    Height = 13
    Caption = 'Problem Width'
  end
  object Label10: TLabel
    Left = 207
    Top = 180
    Width = 61
    Height = 13
    Caption = 'Test Number'
  end
  object Bevel1: TBevel
    Left = 16
    Top = 296
    Width = 359
    Height = 9
    Shape = bsBottomLine
  end
  object Label7: TLabel
    Left = 204
    Top = 204
    Width = 64
    Height = 13
    Caption = 'Row Spacing'
  end
  object Label11: TLabel
    Left = 144
    Top = 276
    Width = 124
    Height = 13
    Caption = 'Seed (blank for new seed)'
  end
  object Label12: TLabel
    Left = 16
    Top = 318
    Width = 48
    Height = 13
    Caption = 'Last Seed'
  end
  object chkNumGrreater: TCheckBox
    Left = 16
    Top = 152
    Width = 225
    Height = 17
    Caption = 'Numerator larger than denominator'
    TabOrder = 12
  end
  object rdoOperation: TRadioGroup
    Left = 16
    Top = 176
    Width = 113
    Height = 113
    Caption = 'Operation'
    ItemIndex = 0
    Items.Strings = (
      'Addition'
      'Subtraction'
      'Multiplication'
      'Division')
    TabOrder = 13
  end
  object Edit1: TEdit
    Left = 168
    Top = 24
    Width = 57
    Height = 21
    TabOrder = 0
    Text = '0'
  end
  object Edit2: TEdit
    Left = 288
    Top = 24
    Width = 57
    Height = 21
    TabOrder = 2
    Text = '18'
  end
  object Edit3: TEdit
    Left = 168
    Top = 56
    Width = 57
    Height = 21
    TabOrder = 4
    Text = '0'
  end
  object Edit4: TEdit
    Left = 288
    Top = 56
    Width = 57
    Height = 21
    TabOrder = 6
    Text = '18'
  end
  object udNumLo: TUpDown
    Left = 225
    Top = 24
    Width = 17
    Height = 21
    Associate = Edit1
    Max = 1000
    TabOrder = 1
  end
  object udNumHi: TUpDown
    Left = 345
    Top = 24
    Width = 17
    Height = 21
    Associate = Edit2
    Max = 1000
    Position = 18
    TabOrder = 3
  end
  object udDenLo: TUpDown
    Left = 225
    Top = 56
    Width = 17
    Height = 21
    Associate = Edit3
    Max = 1000
    TabOrder = 5
  end
  object udDenHi: TUpDown
    Left = 345
    Top = 56
    Width = 17
    Height = 21
    Associate = Edit4
    Max = 1000
    Position = 18
    TabOrder = 7
  end
  object BtnGo: TButton
    Left = 216
    Top = 312
    Width = 75
    Height = 25
    Caption = 'Go'
    Default = True
    TabOrder = 14
    OnClick = BtnGoClick
  end
  object Edit5: TEdit
    Left = 168
    Top = 96
    Width = 57
    Height = 21
    TabOrder = 8
    Text = '100'
  end
  object udCount: TUpDown
    Left = 225
    Top = 96
    Width = 17
    Height = 21
    Associate = Edit5
    Min = 1
    Position = 100
    TabOrder = 9
  end
  object Edit6: TEdit
    Left = 168
    Top = 120
    Width = 57
    Height = 21
    TabOrder = 10
    Text = '10'
  end
  object udProbPerLine: TUpDown
    Left = 225
    Top = 120
    Width = 17
    Height = 21
    Associate = Edit6
    Min = 1
    Position = 10
    TabOrder = 11
  end
  object BtnCancel: TButton
    Left = 302
    Top = 312
    Width = 75
    Height = 25
    Cancel = True
    Caption = 'Close'
    TabOrder = 15
    OnClick = BtnCancelClick
  end
  object edProbWidth: TEdit
    Left = 288
    Top = 248
    Width = 89
    Height = 21
    TabOrder = 19
    Text = '650'
  end
  object edSpacing: TEdit
    Left = 288
    Top = 224
    Width = 89
    Height = 21
    TabOrder = 18
    Text = '360'
  end
  object edTestNum: TEdit
    Left = 288
    Top = 176
    Width = 89
    Height = 21
    TabOrder = 16
    Text = '1'
  end
  object edRowSpacing: TEdit
    Left = 288
    Top = 200
    Width = 89
    Height = 21
    TabOrder = 17
    Text = '31'
  end
  object edSeed: TEdit
    Left = 288
    Top = 272
    Width = 89
    Height = 21
    TabOrder = 20
  end
  object edLastSeed: TEdit
    Left = 80
    Top = 317
    Width = 57
    Height = 21
    BorderStyle = bsNone
    Color = clBtnFace
    ReadOnly = True
    TabOrder = 21
    Text = 'edLastSeed'
  end
  object pEng: TPrintPrevDlg
    StartPage = -1
    EndPage = -1
    PrintDlgOnPreview = True
    ShowHints = True
    Left = 320
    Top = 104
  end
end
