unit Unit1;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, ShellAPI;

type
  TForm1 = class(TForm)
    Memo1: TMemo;
    Memo2: TMemo;
    Button1: TButton;
    Button2: TButton;
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

{$R *.DFM}

procedure TForm1.Button1Click(Sender: TObject);
var
  srcFiles, dstFiles: string;
  k   : integer;
  fileOp : TSHFileOpStruct;
begin
  srcFiles:= '';
  dstFiles:= '';
  for k:= 0 to memo1.Lines.Count - 1 do
  begin
    srcFiles:= srcFiles + memo1.Lines[k] + #0;
  end;
  srcFiles:= srcFiles + #0;
  for k:= 0 to memo2.Lines.Count - 1 do
  begin
    dstFiles:= dstFiles + memo2.Lines[k] + #0;
  end;
  dstFiles:= dstFiles + #0;

  fileOp.wnd:= handle;
  fileOp.wFunc:= FO_COPY;
  fileOp.pFrom:= PChar(srcFiles);
  fileOp.pto:= PChar(dstFiles);

  fileOp.fFlags:= FOF_NOCONFIRMATION or FOF_FILESONLY or FOF_MULTIDESTFILES or
                  FOF_NOCONFIRMMKDIR;
  fileOp.fAnyOperationsAborted:= False;
  fileOp.hNameMappings:= nil;
  fileOp.lpszProgressTitle:= nil;

  SHFileOperation(fileOp);

end;

procedure TForm1.Button2Click(Sender: TObject);
var
  delFiles : string;
  k   : integer;
  fileOp : TSHFileOpStruct;
begin
  delFiles:= '';
  for k:= 0 to memo1.Lines.Count - 1 do
  begin
    delFiles:= delFiles + memo1.Lines[k] + #0;
  end;
  delFiles:= delFiles + #0;

  fileOp.wnd:= handle;
  fileOp.wFunc:= FO_DELETE;
  fileOp.pFrom:= PChar(delFiles);
  fileOp.pto:= nil;
  fileOp.fFlags:= FOF_FILESONLY;
  fileOp.fAnyOperationsAborted:= False;
  fileOp.hNameMappings:= nil;
  fileOp.lpszProgressTitle:= nil;

  SHFileOperation(fileOp);

end;

end.
