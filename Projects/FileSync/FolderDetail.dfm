object FrmFolderDetail: TFrmFolderDetail
  Left = 470
  Top = 110
  BorderStyle = bsDialog
  Caption = 'Folder Detail'
  ClientHeight = 314
  ClientWidth = 347
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = True
  Position = poScreenCenter
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Bevel1: TBevel
    Left = 8
    Top = 56
    Width = 329
    Height = 217
    Shape = bsFrame
  end
  object Label18: TLabel
    Left = 129
    Top = 96
    Width = 180
    Height = 13
    Alignment = taRightJustify
    AutoSize = False
    Caption = '. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . '
  end
  object Label17: TLabel
    Left = 86
    Top = 80
    Width = 223
    Height = 13
    Alignment = taRightJustify
    AutoSize = False
    Caption = 
      '. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . ' +
      '. . . . . . .'
  end
  object Source: TLabel
    Left = 8
    Top = 8
    Width = 33
    Height = 13
    Caption = 'Source'
  end
  object Destination: TLabel
    Left = 8
    Top = 24
    Width = 54
    Height = 13
    Caption = 'Destination'
  end
  object LblSource: TEllipsisLabel
    Left = 80
    Top = 8
    Width = 257
    Height = 13
    AutoSize = False
    Caption = 'LblSource'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object LblDest: TEllipsisLabel
    Left = 80
    Top = 24
    Width = 257
    Height = 13
    AutoSize = False
    Caption = 'LblDest'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label1: TLabel
    Left = 20
    Top = 64
    Width = 92
    Height = 13
    Caption = 'Source Information'
  end
  object Label2: TLabel
    Left = 36
    Top = 80
    Width = 48
    Height = 13
    Caption = 'Total Files'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object label26: TLabel
    Left = 36
    Top = 96
    Width = 83
    Height = 13
    Caption = 'Files to be copied'
  end
  object Label4: TLabel
    Left = 36
    Top = 112
    Width = 163
    Height = 13
    Caption = 'Files to be copied over newer files'
  end
  object Label6: TLabel
    Left = 20
    Top = 136
    Width = 113
    Height = 13
    Caption = 'Destination Information'
  end
  object Label7: TLabel
    Left = 36
    Top = 152
    Width = 48
    Height = 13
    Caption = 'Total Files'
  end
  object Label8: TLabel
    Left = 36
    Top = 168
    Width = 88
    Height = 13
    Caption = 'Files to be deleted'
  end
  object Label9: TLabel
    Left = 36
    Top = 184
    Width = 175
    Height = 13
    Caption = 'Newer files (that will be overwritten)'
  end
  object LblSrcTotal: TLabel
    Left = 220
    Top = 80
    Width = 100
    Height = 13
    Alignment = taRightJustify
    AutoSize = False
    Caption = 'LblSrcTotal'
    Transparent = True
  end
  object lblSrcCopied: TLabel
    Left = 220
    Top = 96
    Width = 100
    Height = 13
    Alignment = taRightJustify
    AutoSize = False
    Caption = 'lblSrcCopied'
    Transparent = True
  end
  object Label19: TLabel
    Left = 201
    Top = 112
    Width = 108
    Height = 13
    Alignment = taRightJustify
    AutoSize = False
    Caption = '. . . . . . . . . . . . . . . . . . .'
  end
  object Label10: TLabel
    Left = 90
    Top = 152
    Width = 219
    Height = 13
    Alignment = taRightJustify
    AutoSize = False
    Caption = 
      '. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . ' +
      '. . . . .'
  end
  object LblDstTotal: TLabel
    Left = 220
    Top = 152
    Width = 100
    Height = 13
    Alignment = taRightJustify
    AutoSize = False
    Caption = 'LblDstTotal'
    Transparent = True
  end
  object Label23: TLabel
    Left = 126
    Top = 168
    Width = 183
    Height = 13
    Alignment = taRightJustify
    AutoSize = False
    Caption = '. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .'
  end
  object LblDstDelete: TLabel
    Left = 220
    Top = 168
    Width = 100
    Height = 13
    Alignment = taRightJustify
    AutoSize = False
    Caption = 'LblDstDelete'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlue
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    Transparent = True
  end
  object Label25: TLabel
    Left = 204
    Top = 184
    Width = 105
    Height = 13
    Alignment = taRightJustify
    AutoSize = False
    Caption = '. . . . . . . . . . . . . . . . .'
  end
  object LblDstNewer: TLabel
    Left = 220
    Top = 184
    Width = 100
    Height = 13
    Alignment = taRightJustify
    AutoSize = False
    Caption = 'LblDstNewer'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlue
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    Transparent = True
  end
  object lblSrcNewer: TLabel
    Left = 220
    Top = 112
    Width = 100
    Height = 13
    Alignment = taRightJustify
    AutoSize = False
    Caption = 'lblSrcNewer'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlue
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    Transparent = True
  end
  object Label14: TLabel
    Left = 8
    Top = 40
    Width = 63
    Height = 13
    Caption = 'Folder Action'
  end
  object LblAction: TLabel
    Left = 80
    Top = 40
    Width = 54
    Height = 13
    Caption = 'LblAction'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlue
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label5: TLabel
    Left = 36
    Top = 224
    Width = 77
    Height = 13
    Caption = 'Files unchanged'
  end
  object Label20: TLabel
    Left = 124
    Top = 224
    Width = 185
    Height = 13
    Alignment = taRightJustify
    AutoSize = False
    Caption = '. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .'
  end
  object lblUnchanged: TLabel
    Left = 220
    Top = 224
    Width = 100
    Height = 13
    Alignment = taRightJustify
    AutoSize = False
    Caption = 'lblUnchanged'
    Transparent = True
  end
  object Label21: TLabel
    Left = 200
    Top = 240
    Width = 109
    Height = 13
    Alignment = taRightJustify
    AutoSize = False
    Caption = '. . . . . . . . . . . . . . . . . . .'
  end
  object Label11: TLabel
    Left = 36
    Top = 240
    Width = 164
    Height = 13
    Caption = 'Files forced to Unchanged by user'
  end
  object Label15: TLabel
    Left = 24
    Top = 208
    Width = 87
    Height = 13
    Caption = 'Other Information'
  end
  object LblForced: TLabel
    Left = 220
    Top = 240
    Width = 100
    Height = 13
    Alignment = taRightJustify
    AutoSize = False
    Caption = 'LblForced'
    Transparent = True
  end
  object BitBtn1: TBitBtn
    Left = 121
    Top = 280
    Width = 105
    Height = 27
    Caption = 'Close'
    Kind = bkCancel
    NumGlyphs = 2
    TabOrder = 0
  end
end
