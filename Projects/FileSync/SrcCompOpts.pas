unit SrcCompOpts;

interface


uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, ComCtrls, SCM, Registry, Globals;

type
  TSCMOptions = record
    charsToIgnore     : string;
    ConvertTabs       : boolean;
    IgnoreCase        : boolean;
    IgnoreWhiteSpace  : boolean;
    minLinesForMatch  : integer;
    ShowFileInfo      : boolean;
    TabSize           : integer;
    UseLineNumbers    : boolean;
    Width             : integer;
    optimize          : boolean;
    fontName          : string;
    fontSize          : integer;
    externalCompare   : string;
    externalArgs      : string;
    UseExternalComp   : boolean;
  end;

  TFrmSrcCompOpts = class(TForm)
    BtnCancel: TBitBtn;
    BtnOK: TBitBtn;
    BtnDefaults: TBitBtn;
    BtnFont: TBitBtn;
    FontDialog1: TFontDialog;
    grpInternal: TGroupBox;
    ChkShowFileInfo: TCheckBox;
    ChkOptimize: TCheckBox;
    ChkCaseSensitive: TCheckBox;
    ChkDispLineNums: TCheckBox;
    ChkIgnoreWS: TCheckBox;
    ChkConvertTabs: TCheckBox;
    Label1: TLabel;
    Label3: TLabel;
    Label2: TLabel;
    Label4: TLabel;
    edCharsToIgnore: TEdit;
    edTabSize: TEdit;
    udTabSize: TUpDown;
    edMinLines: TEdit;
    udMinLines: TUpDown;
    edDispWidth: TEdit;
    udDispWidth: TUpDown;
    rdoExternal: TRadioButton;
    rdoInternal: TRadioButton;
    grpExternal: TGroupBox;
    Label5: TLabel;
    edArguments: TEdit;
    Label6: TLabel;
    Label7: TLabel;
    edExternalProgram: TEdit;
    Button1: TButton;
    OpenDialog1: TOpenDialog;
    procedure FormCreate(Sender: TObject);
    procedure BtnOKClick(Sender: TObject);
    procedure BtnDefaultsClick(Sender: TObject);
    procedure BtnFontClick(Sender: TObject);
    procedure OptionClicked(Sender: TObject);
    procedure Button1Click(Sender: TObject);
  private
    procedure SetupScreen;
  public
    { Public declarations }
  end;

procedure SetSCMOptions(aSCM: TSCM);
procedure SaveSCMOptsToReg;
procedure ReadSCMOptsFromReg;
procedure SetSrcCompFont(aFont: TFont);
function UseExternalCompare: boolean;
procedure ExternalCompareOpts(var aExecutable, aArgs: string);

implementation

{$R *.DFM}

var
  scmOpts   : TSCMOptions;

function UseExternalCompare: boolean;
begin
  result:= scmOpts.UseExternalComp;
end;

procedure ExternalCompareOpts(var aExecutable, aArgs: string);
begin
  aExecutable:= scmOpts.externalCompare;
  aArgs:= scmOpts.externalArgs;
end;

procedure SetSrcCompFont(aFont: TFont);
begin
  aFont.Name:= scmOpts.fontName;
  aFont.Size:= scmOpts.fontSize;
end;

procedure SetDefaults;
begin
  scmOpts.charsToIgnore:= '';
  scmOpts.ConvertTabs:= true;
  scmOpts.IgnoreCase:= true;
  scmOpts.IgnoreWhiteSpace:= false;
  scmOpts.minlinesformatch:= 3;
  scmOpts.ShowFileInfo:= true;
  scmOpts.TabSize:= 3;
  scmOpts.UseLineNumbers:= true;
  scmOpts.Width:= 128;
  scmOpts.optimize:= true;
  scmOpts.FontName:= 'Lucida Console';
  scmOpts.fontSize:= 10;
  scmOpts.UseExternalComp:= false;
  scmOpts.externalCompare:= '';
  scmOpts.externalArgs:= '';
end;

procedure SetSCMOptions(aSCM: TSCM);
begin
  aSCM.charsToIgnore     := scmOpts.charsToIgnore;
  aSCM.ConvertTabs       := scmOpts.ConvertTabs;
  aSCM.IgnoreCase        := scmOpts.IgnoreCase;
  aSCM.IgnoreWhiteSpace  := scmOpts.IgnoreWhiteSpace;
  aSCM.minLinesForMatch  := scmOpts.minlinesformatch;
  aSCM.ShowFileInfo      := scmOpts.ShowFileInfo;
  aSCM.TabSize           := scmOpts.TabSize;
  aSCM.UseLineNumbers    := scmOpts.UseLineNumbers;
  aSCM.DisplayWidth      := scmOpts.Width;
  aSCM.optimize          := scmOpts.optimize;
end;

procedure TFrmSrcCompOpts.FormCreate(Sender: TObject);
begin
  SetupScreen;
end;

procedure TFrmSrcCompOpts.OptionClicked(Sender: TObject);
begin
  edArguments.Enabled:= rdoExternal.Checked;
  edExternalProgram.Enabled:= rdoExternal.Checked;
  Label5.Enabled:= rdoExternal.Checked;
  label6.Enabled:= rdoExternal.Checked;
  label7.Enabled:= rdoExternal.Checked;

  edCharsToIgnore.Enabled:= rdoInternal.Checked;
  edMinLines.Enabled:= rdoInternal.Checked;
  edTabSize.Enabled:= rdoInternal.Checked;
  edDispWidth.Enabled:= rdoInternal.Checked;
  udTabSize.Enabled:= rdoInternal.Checked;
  udMinLines.Enabled:= rdoInternal.Checked;
  udDispWidth.Enabled:= rdoInternal.Checked;
  label1.Enabled:= rdoInternal.Checked;
  label2.Enabled:= rdoInternal.Checked;
  label3.Enabled:= rdoInternal.Checked;
  label4.Enabled:= rdoInternal.Checked;
  ChkShowFileInfo.Enabled:= rdoInternal.Checked;
  ChkOptimize.Enabled:= rdoInternal.Checked;
  ChkCaseSensitive.Enabled:= rdoInternal.Checked;
  ChkDispLineNums.Enabled:= rdoInternal.Checked;
  ChkIgnoreWS.Enabled:= rdoInternal.Checked;
  ChkConvertTabs.Enabled:= rdoInternal.Checked;
  BtnFont.Enabled:= rdoInternal.Checked;
end;

procedure TFrmSrcCompOpts.SetupScreen;
// extract scm options from structure to screen
begin
  edCharsToIgnore.Text:= scmOpts.CharsToIgnore;
  udTabSize.Position:= scmOpts.TabSize;
  udMinLines.Position:= scmOpts.MinLinesForMatch;
  udDispWidth.Position:= scmOpts.Width;
  ChkShowFileInfo.Checked:= scmOpts.ShowFileInfo;
  ChkConvertTabs.Checked:= scmOpts.ConvertTabs;
  ChkIgnoreWS.Checked:= scmOpts.IgnoreWhiteSpace;
  ChkCaseSensitive.Checked:= not scmOpts.ignorecase;
  ChkDispLineNums.checked:= scmOpts.UseLineNumbers;
  ChkOptimize.checked:= scmOpts.optimize;
  FontDialog1.Font.Name:= scmOpts.fontName;
  FontDialog1.Font.Size:= scmOpts.fontSize;
  edExternalProgram.Text:= scmOpts.externalCompare;
  edArguments.Text:= scmOpts.externalArgs;
  if scmOpts.UseExternalComp then
    rdoExternal.Checked:= true
  else
    rdoInternal.Checked:= true;
  OptionClicked(nil);
  BtnFont.Enabled:= rdoInternal.Checked;
end;

procedure TFrmSrcCompOpts.BtnOKClick(Sender: TObject);
begin
  scmOpts.ShowFileInfo:= chkShowFileInfo.checked;
  scmOpts.CharsToIgnore:= edCharsToIgnore.Text;
  scmOpts.TabSize:= udTabSize.Position;
  scmOpts.MinLinesForMatch:= udMinLines.Position;
  scmOpts.ConvertTabs:= ChkConvertTabs.Checked;
  scmOpts.IgnoreWhiteSpace:= ChkIgnoreWS.Checked;
  scmOpts.IgnoreCase:= not ChkCaseSensitive.Checked;
  scmOpts.UseLineNumbers:= ChkDispLineNums.checked;
  scmOpts.Optimize:= ChkOptimize.checked;
  scmOpts.Width:= udDispWidth.Position;
  scmOpts.fontName:= FontDialog1.Font.Name;
  scmOpts.fontSize:= FontDialog1.Font.Size;
  scmOpts.externalCompare:= edExternalProgram.Text;
  scmOpts.externalArgs:= edArguments.Text;
  scmOpts.UseExternalComp:= rdoExternal.Checked;
  ModalResult:= mrOK;
end;

procedure TFrmSrcCompOpts.Button1Click(Sender: TObject);
begin
  if OpenDialog1.Execute(handle) then
  begin
    edExternalProgram.Text:= OpenDialog1.FileName;
  end;
end;

procedure SaveSCMOptsToReg;
// save option to the registry
var
  r   : TRegistry;
begin
  r:= TRegistry.Create;
  r.OpenKey(SyncRegKeyOptionsBase + 'SourceCompare', true);
  r.WriteString('CharsToIgnore', scmOpts.charsToIgnore);
  r.WriteBool('ConvertTabs', scmOpts.ConvertTabs);
  r.WriteBool('IgnoreCase', scmOpts.IgnoreCase);
  r.WriteBool('IgnoreWhiteSpace', scmOpts.IgnoreWhiteSpace);
  r.WriteInteger('MinLinesForMatch', scmOpts.minLinesForMatch);
  r.WriteBool('ShowFileInfo', scmOpts.ShowFileInfo);
  r.WriteInteger('TabSize', scmOpts.TabSize);
  r.WriteBool('UseLineNumbers', scmOpts.UseLineNumbers);
  r.WriteInteger('Width', scmOpts.Width);
  r.WriteBool('Optimize', scmOpts.Optimize);
  r.WriteString('FontName', scmOpts.fontName);
  r.WriteInteger('FontSize', scmOpts.fontSize);
  r.WriteString('ExternalCompare', scmOpts.externalCompare);
  r.WriteString('ExternalArgs', scmOpts.externalArgs);
  r.WriteBool('UseExternalCompare', scmOpts.UseExternalComp);
  r.CloseKey;
  r.Free;
end;

procedure ReadSCMOptsFromReg;
var
  r   : TRegistry;
  vals: TStringList;
  k   : integer;
  keyName: string;
begin
  r:= TRegistry.Create;
  try
    if r.OpenKey(SyncRegKeyOptionsBase + 'SourceCompare', true) then
    begin
      vals:= TStringList.Create;
      try
        r.GetValueNames(vals);
        for k:= 0 to vals.Count - 1 do
        begin
          keyName:= UpperCase(vals[k]);
          try
            if keyName = 'CHARSTOIGNORE' then
              scmOpts.CharsToIgnore:= r.ReadString(vals[k])
            else if keyName = 'CONVERTTABS' then
              scmOpts.ConvertTabs:= r.ReadBool(vals[k])
            else if keyName = 'IGNORECASE' then
              scmOpts.IgnoreCase:= r.ReadBool(vals[k])
            else if keyName = 'IGNOREWHITESPACE' then
              scmOpts.IgnoreWhiteSpace:= r.ReadBool(vals[k])
            else if keyName = 'MINLINESFORMATCH' then
              scmOpts.minLinesForMatch:= r.ReadInteger(vals[k])
            else if keyName = 'SHOWFILEINFO' then
              scmOpts.ShowFileInfo:= r.ReadBool(vals[k])
            else if keyName = 'TABSIZE' then
              scmOpts.TabSize:= r.ReadInteger(vals[k])
            else if keyName = 'USELINENUMBERS' then
              scmOpts.UselineNumbers:= r.ReadBool(vals[k])
            else if keyName = 'WIDTH' then
              scmOpts.Width:= r.ReadInteger(vals[k])
            else if keyName = 'OPTIMIZE' then
              scmOpts.optimize:= r.ReadBool(vals[k])
            else if keyName = 'FONTNAME' then
              scmOpts.fontName:= r.ReadString(keyName)
            else if keyName = 'FONTSIZE' then
              scmOpts.fontSize:= r.ReadInteger(keyName)
            else if keyName = 'USEEXTERNALCOMPARE' then
              scmOpts.UseExternalComp:= r.ReadBool(vals[k])
            else if keyName = 'EXTERNALCOMPARE' then
              scmOpts.externalCompare:= r.ReadString(vals[k])
            else if keyName = 'EXTERNALARGS' then
              scmOpts.externalArgs:= r.ReadString(vals[k]);
          except
            // eat up reg exception
          end;
        end;
      finally
        vals.Free;
      end;
    end;
    r.CloseKey;
  finally
    r.Free;
  end;
end;

procedure TFrmSrcCompOpts.BtnDefaultsClick(Sender: TObject);
begin
  SetDefaults;
  SetupScreen;
end;

procedure TFrmSrcCompOpts.BtnFontClick(Sender: TObject);
begin
  fontDialog1.Execute;
end;

initialization
  SetDefaults;
  ReadSCMOptsFromReg;
finalization
  SaveSCMOptsToReg;
end.
