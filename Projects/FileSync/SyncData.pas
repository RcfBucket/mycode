unit SyncData;                                                           {- rcf}

INTERFACE

uses
  Windows, Classes, SysUtils, Dialogs, Controls, ShellAPI, Forms, BuildForm,
  FileCtrl, WildCard;

const
  FolderListCapacity  = 100;
  FileListCapacity    = 100;

type
  TCheckCancelEvent = procedure(var doContinue: boolean) of object;

  TSyncAction = (actNoChange,
                 actCreate, actUpdate,        // folder actions
                 actDelete,                   // folder/file actions
                 actCopy, actOverwriteNewer,  // file actions
                 actUserNochanged,            // user actions
                 actUserUndoNochange);

  TSyncCompareType = (syncDateTime, syncDateByte, syncByteOnly, syncSuper);
  { File sync type:
    syncDateTimeSize : Only compare files based on date/time/size
    syncDateByte     : If files are same size but date/time differ, do a byte compare
    syncByteOnly     : Always perform a byte compare
    syncSuper        : If file size/date/time are same then perform byte compare
  }

const
  ActionStrs : array[TSyncAction] of string =
                ('No Change', 'Create', 'Update', 'Delete', 'Copy',
                 'Overwrite newer', 'User No changed', 'UserUndo');

type
  TSyncFoundFile = procedure(name: string) of object;
  TSyncFoundFolder = procedure(name: string) of object;
  TCompareFolderEvent = procedure(name: string) of object;
  TCompareFileEvent = procedure(name: string; action: TSyncCompareType) of object;

  TSyncFolder = class;    // forward

  TSyncFile = class
  private
    sRec        : TSearchRec;
    fAction     : TSyncAction;
    fParent     : TSyncFolder;
    fMarked     : boolean;
    fOrigAct    : TSyncAction;
    function GetFileDate: TDateTime;
    procedure SetAction(value: TSyncAction);
    function GetSize: integer;
  public
    constructor Create(aParentFolder: TSyncFolder);
    property Parent: TSyncFolder read fParent write fParent;
    property FileName: TFileName read sRec.Name;
    property FileDate: TDateTime read GetFileDate;
    property FileSize: integer read GetSize;
    property SyncAction: TSyncAction read fAction write SetAction;
    property Marked: boolean read fMarked write fMarked;
  end;

  TSyncFolder = class
  private
    fFileList    : TList;
    fFolderName  : string;
    fAction      : TSyncAction;
    fOrigAct     : TSyncAction;
    fMarked      : boolean;
    //
    function GetFileCount: integer;
    function GetFolderSize: integer;
    function GetFile(idx: integer): TSyncFile;
    procedure SetAction(value: TSyncAction);
  protected
    function FileExists(aFileName: string): integer;
  public
    constructor Create;
    destructor Destroy; override;
    procedure AddFile(aFileRec: TSearchRec);
    function FileIndex(aFile: TSyncFile): integer;
    //*** IMPORTANT- Do not call this function from the TSync classes Source
    //               or Dest properties. Use the RemoveFile/Folder methods of
    //               the TSync class.!!!
    procedure RemoveFile(fileIdx: integer);

    // Properties
    property FileCount: integer
      read GetFileCount;

    property Files[idx: integer]: TSyncFile
      read GetFile;

    property FolderName: string
      read fFolderName
      write fFolderName;

    property FolderSize: integer
      read GetFolderSize;

    property SyncAction: TSyncAction
      read fAction write SetAction;

    property Marked: boolean
      read fMarked write fMarked;
  end;

  // maintains a list of folders and their files
  TSyncList = class
  private
    fIncludeMasks   : TFileMasks;
    fExcludeMasks   : TFileMasks;
    fExcludeFolders : TFileMasks;
    fFolderList     : TList;
    fBaseFolder     : string;
    fReadSubFolders : boolean;
    fReadSysFolders : boolean;
    fOnFile         : TSyncFoundFile;
    fOnFolder       : TSyncFoundFolder;
    //
    procedure ReadFiles(aFolder: string);
    function GetFolderCount: integer;
    function GetFolder(idx: integer): TSyncFolder;
    function GetFolderNameNoBase(idx: integer): string;
    procedure SetBaseFolder(value: string);
  public
    constructor Create;
    destructor Destroy; override;
    procedure Clear;
    procedure Execute;
    function AddFolder(aFolderName: string): TSyncFolder;
    function FolderExistsNoBase(aFolder: string): integer;
    function FolderIndex(aFolder: TSyncFolder): integer;
    function IsSubFolder(baseIdx, folderIdx: integer): boolean;
    function HasSubFolders(aFolder: TSyncFolder): boolean;

    //*** IMPORTANT- Do not call this function from the TSync classes Source
    //               or Dest properties. Use the RemoveFile/Folder methods of
    //               the TSync class.!!!
    procedure RemoveFolder(folderIdx: integer);

    // Properties
    property FolderCount: integer
      read GetFolderCount;

    property Folders[idx: integer]: TSyncFolder
      read GetFolder;

    property FolderNameNoBase[idx: integer]: string
      read GetFolderNameNoBase;

    property BaseFolder: string
      read fBaseFolder
      write SetBaseFolder;

    property ProcessSubFolders: boolean
      read fReadSubFolders
      write fReadSubFolders
      default true;

    property ProcessSystemFolders: boolean
      read fReadSysFolders
      write fReadSysFolders
      default false;

    // Events
    property OnFolder: TSyncFoundFolder
      read fOnFolder
      write fOnFolder;

    property OnFile: TSyncFoundFile
      read fOnFile
      write fOnFile;

    property IncludeMasks: TFileMasks read fIncludeMasks;
    property ExcludeMasks: TFileMasks read fExcludeMasks;
    property ExcludeFolders: TFileMasks read fExcludeFolders;
  end;

  // In the following: Merged means taking SRC and DEST file or folder names
  // and merging them into one list and sorting the list.  This gives the
  // ability to sync them up in a side by side manner. The list is a union
  // of all names in the source and dest.

  // class to hold syncronization information for SRC/DEST folders
  TSync = class
  private
    fMergedFolders  : TStringList; // union of all folder names
    fMergedFiles    : TStringList; // merged files (cache) see fLastFileMergeFolderIdx
    fSrc            : TSyncList;
    fDst            : TSyncList;
    fCompareType    : TSyncCompareType;
    fOnCompareFolder: TCompareFolderEvent;
    fOnCompareFile: TCompareFileEvent;
    fOnCheckCancel: TCheckCancelEvent;

    // Since this object holds information for all files in source and dest, it
    // would take too much memory to hold a merged file list for all folders
    // So this object uses a cache concept. Holding a merged file list for
    // just the most recent access. The cache is determind by the following
    // data member. If -1 then the cache is empty, otherwise it holds the index
    // of the fMergedFolders list of the folder name that was last cached.
    fLastFileMergeFolderIdx : integer;

    // Other cache members
    // Get Sync File Cache
    lastSrcFoldIdx : integer;
    lastSrcFileIdx : integer;
    lastSrcFile    : TSyncFile;

    lastDstFoldIdx : integer;
    lastDstFileIdx : integer;
    lastDstFile    : TSyncFile;

    // Get Sync Folder Cache
    lastSrcIdx     : integer;
    lastSrcFolder  : TSyncFolder;
    lastDstIdx     : integer;
    lastDstFolder  : TSyncFolder;

    function GetMergedFolderCount: integer;
    function GetFolderAction(idx: integer): TSyncAction;
    procedure SetFolderAction(idx: integer; value: TSyncAction);
    function GetFileAction(folderIdx, fileIdx: integer): TSyncAction;
    procedure SetFileAction(folderIdx, fileIdx: integer; value: TSyncAction);
    function GetSyncFolderPrim(mergeIdx: integer; aList: TSyncList): TSyncFolder;

    procedure MergeFiles(aFolderIdx: integer);
    function GetMergedFileCount(folderIdx: integer): integer;
    function GetMergedFiles(folderIdx: integer): TStringList;
    procedure CompareFolders(aFolderIdx: integer);
    function ByteCompare(s, d : string): boolean;
(*    function GetHasSubFolders(folderIdx: integer): boolean;*)
    function GetSyncFolderMark(folderIdx: integer): boolean;
    procedure SetSyncFolderMark(folderIdx: integer; value: boolean);
    function GetActionsExist: boolean;
    procedure ClearCache;
    function CanContinue: boolean;
  public
    constructor Create;
    destructor Destroy; override;
    procedure Clear;
    function PerformSync(wnd: HWND): boolean;
    procedure AnalyzeFolders;
    procedure RemoveSyncFolder(folderIdx: integer);
    procedure RemoveSyncFile(folderIdx, fileIdx: integer);
    property OnCheckCancel: TCheckCancelEvent read fOnCheckCancel write fOnCheckCancel;
    //
    // Be sure NOT to call the remove functions from these exported properties!
    property Source: TSyncList read fSrc;
    property Dest: TSyncList read fDst;

    // The following return the SRC or DST TSyncFolder objects for the merged
    // folder name represented by idx
    function GetSyncSourceFolder(idx: integer): TSyncFolder;
    function GetSyncDestFolder(idx: integer): TSyncFolder;

    // Merged folder properties
    property MergedFolderCount: integer
      read GetMergedFolderCount;

    property MergedFolders: TStringList
      read fMergedFolders;

    property Marked[folderIdx: integer]: boolean
      read GetSyncFolderMark
      write SetSyncFolderMark;

    property FolderAction[idx: integer]: TSyncAction
      read GetFolderAction
      write SetFolderAction;

    // Merged file properties
    function GetSyncSourceFile(afolderIdx, fileidx: integer): TSyncFile;
    function GetSyncDestFile(afolderIdx, fileidx: integer): TSyncFile;

(*    property HasSubFolders[folderIdx: integer]: boolean
      read GetHasSubFolders;*)

    property MergedFileCount[FolderIndex: integer]: integer
      read GetMergedFileCount;

    property MergedFiles[FolderIndex: integer]: TStringList
      read GetMergedFiles;

    property FileAction[SyncFolderIdx, fileIdx: integer]: TSyncAction
      read GetFileAction
      write SetFileAction;

    property ActionsExist: boolean
      read GetActionsExist;

    property CompareType: TSyncCompareType
      read fCompareType write fCompareType;

    property OnCompareFolder: TCompareFolderEvent
      read fOnCompareFolder
      write fOnCompareFolder;

    property OnCompareFile: TCompareFileEvent
      read fOnCompareFile
      write fOnCompareFile;
  end;

function CompareFiles(f1name, f2name: string; PadToSector: boolean): boolean;
function AppendToPath(const path, toAppend: string): string;

IMPLEMENTATION

uses
  Globals, System.IOUtils;

function AppendToPath(const path, toAppend: string): string;
// append to a path. Ensures that the backslash is added to separate the toAppend
// entry
begin
  if path[Length(path)] = '\' then
    result:= path + toAppend
  else
    result:= path + '\' + toAppend;
end;

{--------------------------------------------------------------[ TSync ]--}


constructor TSync.Create;
begin
  inherited Create;
  fOnCheckCancel:= nil;
  ClearCache;   // empty cache
  fCompareType:= syncDateTime;

  fSrc:= TSyncList.Create;
  fDst:= TSyncList.Create;

  fMergedFolders:= TStringList.Create;
  fMergedFolders.Duplicates:= dupError;
  fMergedFolders.Sorted:= true;

  fMergedFiles:= TStringList.Create;
  fMergedFiles.Duplicates:= dupError;
  fMergedFiles.Sorted:= true;
end;

destructor TSync.Destroy;
begin
  fMergedFolders.Free;
  fMergedFiles.Free;
  fSrc.Free;
  fDst.Free;
  inherited Destroy;
end;

procedure TSync.ClearCache;
// clear out all caching vars
begin
  // Get Sync File cache
  lastSrcFoldIdx:= -1;
  lastSrcFileIdx:= -1;
  lastSrcFile:= nil;

  lastDstFoldIdx:= -1;
  lastDstFileIdx:= -1;
  lastDstFile:= nil;


  // Get Sync Folder Cache
  lastSrcIdx:= -1;
  lastSrcFolder:= nil;
  lastDstIdx:= -1;
  lastDstFolder:= nil;

  fLastFileMergeFolderIdx:= -1;
end;

function TSync.GetMergedFolderCount: integer;
begin
  result:= fMergedFolders.count;
end;

procedure TSync.Clear;
begin
  ClearCache;
  fSrc.Clear;
  fDst.Clear;
  fMergedFolders.Clear;
  fMergedFiles.Clear;
end;

function TSync.GetSyncSourceFile(afolderIdx, fileidx: integer): TSyncFile;
var
  s   : TSyncFolder;
  k   : integer;
begin
  if (fileIdx = lastSrcFileIdx) and (aFolderIdx = lastSrcFoldIdx) and (lastSrcFile <> nil) then
  begin
    result:= lastSrcFile;   // use cache
  end
  else
  begin
    result:= nil;
    MergeFiles(aFolderIdx);    // check cache
    s:= GetSyncFolderPrim(aFolderIdx, fSrc);
    if s <> nil then
    begin
      k:= s.FileExists(fMergedFiles[fileIdx]);
      if k >= 0 then
        result:= s.Files[k];
    end;
    lastSrcFile:= result;     // set new cache
    lastSrcFoldIdx:= aFolderIdx;
    lastSrcFileIdx:= FileIdx;
  end;
end;

function TSync.GetSyncDestFile(afolderIdx, fileidx: integer): TSyncFile;
var
  d   : TSyncFolder;
  k   : integer;
begin
  if (fileIdx = lastDstFileIdx) and (aFolderIdx = lastDstFoldIdx) and (lastDstFile <> nil) then
  begin
    result:= lastDstFile;   // use cache
  end
  else
  begin
    result:= nil;
    MergeFiles(aFolderidx);    // check cache
    d:= GetSyncFolderPrim(aFolderidx, fDst);
    if d <> nil then
    begin
      k:= d.FileExists(fMergedFiles[fileIdx]);
      if k >= 0 then
        result:= d.Files[k];
    end;
    lastDstFile:= result;   // setup new cache
    lastDstFoldIdx:= aFolderIdx;
    lastDstFileIdx:= FileIdx;
  end;
end;

function TSync.GetSyncFolderPrim(mergeIdx: integer; aList: TSyncList): TSyncFolder;
// returns a TSyncFolder for the folder represented by mergeIdx in the
// merged folder list
var
  fIdx  : integer;
begin
  if (mergeIdx < 0) or (mergeIdx >= fMergedFolders.Count) then
    result:= nil
  else
  begin
    fIdx:= aList.FolderExistsNoBase(fMergedFolders[mergeIdx]);
    if fIdx >= 0 then
      result:= aList.Folders[fIdx]
    else
      result:= nil;
  end;
end;

function TSync.GetSyncDestFolder(idx: integer): TSyncFolder;
// return the src folder that corresponds tp the folder in the merged list
// at index
begin
  if (lastDstIdx = idx) and (lastDstFolder <> nil) then  // check cache
    result:= lastDstFolder
  else
  begin
    result:= GetSyncFolderPrim(idx, fDst);
    lastDstIdx:= idx;   // setup cache
    lastDstFolder:= result;
  end;
end;

function TSync.GetSyncSourceFolder(idx: integer): TSyncFolder;
// return the src folder that corresponds to the folder in the merged list
// at index
begin
  if (lastSrcIdx = idx) and (lastSrcFolder <> nil) then
    result:= lastSrcFolder
  else
  begin
    result:= GetSyncFolderPrim(idx, fSrc);
    lastSrcIdx:= idx;   // setup cache
    lastSrcFolder:= result;
  end;
end;

procedure TSync.SetFileAction(folderIdx, fileIdx: integer; value: TSyncAction);
var
  s, d  : TSyncFile;
begin
  s:= GetSyncSourceFile(folderIdx, fileidx);
  if (s <> nil) then
    s.SyncAction:= value
  else
  begin
    d:= GetSyncDestFile(folderIdx, fileidx);
    if (d <> nil) then
      d.SyncAction:= value
    else
      raise Exception.Create('Index out of range error in merged file list (GetSyncFileAction)');
  end;
end;

function TSync.GetFileAction(folderIdx, fileIdx: integer): TSyncAction;
var
  s, d  : TSyncFile;
begin
  s:= GetSyncSourceFile(folderIdx, fileidx);
  if (s <> nil) then
    result:= s.SyncAction
  else
  begin
    d:= GetSyncDestFile(folderIdx, fileidx);
    if (d <> nil) then
      result:= d.SyncAction
    else
      raise Exception.Create('Index out of range error in merged file list (GetSyncFileAction)');
  end;
end;

procedure TSync.SetFolderAction(idx: integer; value: TSyncAction);
var
  s, d  : TSyncFolder;
begin
  s:= GetSyncSourceFolder(idx);
  if s <> nil then
    s.SyncAction:= value
  else
  begin
    d:= GetSyncDestFolder(idx);
    if (d <> nil) then
      d.SyncAction:= value
    else
      raise Exception.Create('Index out of range error in merged folder list (GetSyncFolderAction)');
  end;
end;

function TSync.GetFolderAction(idx: integer): TSyncAction;
// returns the action for a folder. This folder idx is the index in the
// merged (synced) folder list not the SRC or DST folder lists
var
  s, d  : TSyncFolder;
begin
  s:= GetSyncSourceFolder(idx);
  if (s <> nil) then
    result:= s.SyncAction
  else
  begin
    d:= GetSyncDestFolder(idx);
    if (d <> nil) then
      result:= d.SyncAction
    else
      raise Exception.Create('Index out of range error in merged folder list (GetSyncFolderAction)');
  end;
end;

function TSync.PerformSync(wnd: HWND): boolean;
var
  srcFiles  : string;
  dstFiles  : string;
  delFiles  : string;
  createDirs: TStringList;
  fileOp    : TSHFileOpStruct;
  copyCount : integer;
  delCount  : integer;
  delCheck  : TStringList;

  procedure BuildCopyList;
  var
    k         : integer;
    src, dst  : TSyncFolder;
    srcNoBase : string;
    action    : TSyncAction;
    fIdx      : integer;
  begin
    copyCount:= 0;
    srcFiles:= '';
    dstFiles:= '';
    // start at the bottom of the sync list, to ensure that sub-folders are
    // handled first
    for k:= MergedFolderCount-1 downto 0 do
    begin
      action:= FolderAction[k];
      if (action = actUpdate) or (action = actCreate) or (action = actOverwriteNewer) then
      begin
        src:= GetSyncSourceFolder(k);
        if src <> nil then
        begin
          dst:= GetSyncDestFolder(k);

          srcNoBase:= source.FolderNameNoBase[source.FolderIndex(src)];

          if action = actCreate then
          begin
            if src.FileCount > 0 then
            begin
              for fIdx:= 0 to src.FileCount - 1 do
              begin
                Inc(copyCount);
                srcFiles:= srcFiles + AppendToPath(src.FolderName, src.Files[fIdx].FileName) + #0;
                dstFiles:= dstFiles +
                  AppendToPath(AppendToPath(dest.BaseFolder, srcNoBase),
                                src.Files[fIdx].FileName) + #0;
              end;
            end
            else
            begin
              createDirs.Add(AppendToPath(dest.BaseFolder, srcNoBase));
(*              Inc(copyCount);
              srcFiles:= srcFiles + src.FolderName;
              srcFiles:= srcFiles + #0;

              dstFiles:= dstFiles + AppendToPath(Dest.BaseFolder, srcNoBase);
              dstFiles:= dstFiles + #0;*)
            end;
          end
          else if (action = actUpdate) or (action = actOverwriteNewer) then
          begin
            for fIdx:= 0 to src.FileCount - 1 do
            begin
              if (src.Files[fIdx].SyncAction = actCopy) or
                 (src.Files[fIdx].SyncAction = actOverwriteNewer) then
              begin
                Inc(copyCount);
                srcFiles:= srcFiles + AppendToPath(src.FolderName, src.Files[fIdx].FileName) + #0;
                dstFiles:= dstFiles + AppendToPath(dst.FolderName, src.Files[fIdx].FileName) + #0;
              end;
            end;
          end;
        end;
      end;
    end;
    srcFiles:= srcFiles + #0; // add a double null terminator
    dstFiles:= dstFiles + #0;
  end;

  function AlreadyInList(aFolder: string): boolean;
  var
    k : integer;
  begin
    k:= 0;
    result:= false;
    while (k < delCheck.count) and not result do
    begin
      if Pos(delCheck[k], aFolder) = 1 then
        result:= true;
      Inc(k);
    end;
  end;

  procedure BuildDeleteList;
  var
    dst       : TSyncFolder;
    action    : TSyncAction;
    idx       : integer;
    fIdx      : integer;
    tstr  : string;
  begin
    delCheck.Clear;
    delFiles:= '';
    delCount:= 0;
    for idx:= 0 to MergedFolderCount - 1 do
    begin
      action:= GetFolderAction(idx);
      if (action in [actUpdate, actDelete, actOverwriteNewer])  then
      begin
        dst:= GetSyncDestFolder(idx);
        if (dst <> nil) then
        begin
          // if folder action is delete then remove whole folder.
          if dst.SyncAction = actDelete then
          begin
            if not AlreadyInList(dst.FolderName) then
            begin
              delCheck.Add(AppendToPath(dst.FolderName, ''));
              Inc(delCount);
              delFiles:= delFiles + dst.FolderName + #0;
            end;
          end
          else
          begin
            for fIdx:= 0 to dst.FileCount - 1 do
            begin
              action:= dst.Files[fIdx].SyncAction;
              if action = actDelete then
              begin
                tstr:= dst.Files[fIdx].FileName;
                Inc(delCount);
                delFiles:= delFiles + AppendToPath(dst.FolderName, tstr) + #0;
              end;
            end;
          end;
        end;
      end;
    end;
    delFiles:= delFiles + #0; // add a double null terminator
  end;

  procedure DumpIt;
  var
    list: TFileStream;
  begin
    if not TDirectory.Exists('C:\ProgramData\FileSync') then
      ForceDirectories('C:\ProgramData\FileSync');
    list:= TFileStream.Create('C:\ProgramData\FileSync\src.txt', fmCreate, fmShareDenyNone);
    list.Write(srcFiles[1], ByteLength(srcFiles));
    list.Free;
  end;

var
  frm   : TFrmBuild;
  okToGo: boolean;
  idx   : integer;
  tstr  : string;
  errCode: integer;
begin
  result:= false;
  screen.Cursor:= crHourGlass;
  frm:= TFrmBuild.Create(application);
  createDirs:= TStringList.Create;
  delCheck:= TStringList.Create;
  try
    frm.HideButtons(true);
    frm.Show;
    application.ProcessMessages;

    // build a string list of Source#0Dest combinations for SHFileOperation to copy.
    BuildCopyList;
    frm.Label2.caption:= IntToStr(copyCount);
    frm.Label8.caption:= IntToStr(createDirs.Count);
    frm.Label3.Visible:= true;
    frm.Label4.Visible:= true;
    application.ProcessMessages;

    BuildDeleteList;
    frm.Label4.caption:= IntToStr(delCount);

    frm.Hide;
    frm.HideButtons(false);
    frm.srcCopy:= srcFiles;
    frm.dstCopy:= dstFiles;
    frm.delFiles:= delFiles;
    frm.creates:= createDirs;
    if frm.ShowModal = mrYes then
    begin
      frm.Hide;
      okToGo:= true;
      if okToGo and (frm.chkCreate.Checked) then
      begin
        for idx:= 0 to createDirs.Count - 1 do
        begin
           SysUtils.ForceDirectories(createDirs[idx]);
        end;
      end;

      if okToGo and frm.ChkDelete.Checked and (StrLen(PChar(delFiles)) > 0) then
      begin
        // Time to delete
        fileOp.wnd:= wnd;
        fileOp.wFunc:= FO_DELETE;
        fileOp.pFrom:= PChar(delFiles);
        fileOp.pto:= nil;
        fileOp.fFlags:= FOF_FILESONLY or FOF_ALLOWUNDO;
        fileOp.fAnyOperationsAborted:= False;
        fileOp.hNameMappings:= nil;
        fileOp.lpszProgressTitle:= nil;

        okToGo:= SHFileOperation(fileOp) = 0;
        if okToGo and not frm.ChkCopy.checked then
          result:= true;
      end;

      if okToGo and frm.ChkCopy.Checked and (StrLen(PChar(srcFiles)) > 0) then
      begin
        fileOp.wnd:= wnd;
        fileOp.wFunc:= FO_COPY;
        fileOp.pFrom:= PChar(srcFiles);
        fileOp.pto:= PChar(dstFiles);
dumpit;
        fileOp.fFlags:= FOF_FILESONLY or FOF_MULTIDESTFILES or FOF_NOCONFIRMMKDIR;
        fileOp.fAnyOperationsAborted:= False;
        fileOp.hNameMappings:= nil;
        fileOp.lpszProgressTitle:= nil;

        errCode:= SHFileOperation(fileOp);
        okToGo:= errCode = 0;
        if okToGo then
          result:= true
        else
        begin
          tstr:= GetLastErrorString;
          if (errCode = 222) or (tstr = 'bob') then
            halt;
        end;
      end;
    end;
  finally
    frm.Free;
    createDirs.Free;
    delCheck.Free;
    screen.Cursor:= crDefault;
  end;
end;

procedure TSync.AnalyzeFolders;
var
  k     : integer;
  s, d  : TSyncFolder;
  tstr  : string;
begin
  fMergedFolders.Clear;

  // first merge src/dest folders
  for k:= 0 to fSrc.FolderCount - 1 do
    fMergedFolders.Add(UpperCase(fSrc.FolderNameNoBase[k]));

  // merge dest into list
  for k:= 0 to fDst.FolderCount - 1 do
  begin
    tstr:= UpperCase(fDst.FolderNameNoBase[k]);
    if fMergedFolders.IndexOf(tstr) = -1 then
      fMergedFolders.Add(tstr);
  end;

  // set initial actions for folders
  for k:= 0 to fMergedFolders.count - 1 do
  begin
    s:= GetSyncSourceFolder(k);
    d:= GetSyncDestFolder(k);
    if s = nil then
      d.SyncAction:= actDelete
    else if d = nil then
      s.SyncAction:= actCreate;
  end;

  // Now for each synced folder
  for k:= 0 to fMergedFolders.count - 1 do
  begin
    CompareFolders(k);
  end;
end;

function TSync.CanContinue: boolean;
var
  doContinue: boolean;
begin
  if Assigned(fOnCheckCancel) then
  begin
    fOnCheckCancel(doContinue);
    result:= doContinue;
  end
  else
    result:= false;
end;

procedure TSync.CompareFolders(aFolderIdx: integer);
var
  k       : integer;
  s, d    : TSyncFile;
  src, dst: TSyncFolder;
begin
  MergeFiles(aFolderIdx);  // check cache

  src:= GetSyncSourceFolder(aFolderIdx);
  dst:= GetSyncDestFolder(aFolderIdx);

  k:= 0;
  while (CanContinue) and (k < fMergedFiles.count) do // go thru every file in folders
  begin
    if Assigned(fOnCompareFolder) and (src <> nil) then
      fOnCompareFolder(src.FolderName);

    d:= GetSyncDestFile(aFolderIdx, k);
    s:= GetSyncSourceFile(aFolderIdx, k);

    if Assigned(fOnCompareFile) and (s <> nil) then
      fOnCompareFile(s.FileName, syncDateTime);

    if s = nil then
    begin
      d.SyncAction:= actDelete;
      if src = nil then
      begin
        if dst.SyncAction = actNoChange then
          dst.SyncAction:= actUpdate;
      end
      else if src.SyncAction = actNoChange then
        src.SyncAction:= actUpdate;
    end
    else if d = nil then
    begin
      s.SyncAction:= actCopy;
      if src = nil then
      begin
        if dst.SyncAction = actNoChange then
          dst.SyncAction:= actUpdate;
      end
      else if src.SyncAction = actNoChange then
        src.SyncAction:= actUpdate;
    end
    else
    begin
      // time to physically compare files.
      case fCompareType of
      syncDateTime:
        begin
          if (s.FileSize <> d.FileSize) or (s.FileDate <> d.FileDate) then
          begin
            if d.FileDate > s.FileDate then
            begin
              src.SyncAction:= actOverwriteNewer;
              s.SyncAction:= actOverwriteNewer;
            end
            else
            begin
              if src.SyncAction = actNochange then
                src.SyncAction:= actUpdate;
              s.SyncAction:= actCopy;
            end;
          end;
        end;
      syncDateByte:
        begin
          if s.FileSize <> d.FileSize then
          begin
            if src.SyncAction = actNochange then
              src.SyncAction:= actUpdate;
            if d.FileDate > s.FileDate then
            begin
              src.SyncAction:= actOverwriteNewer;
              s.SyncAction:= actOverwriteNewer;
            end
            else
              s.SyncAction:= actCopy;
          end
          else if (s.FileDate <> d.FileDate) then
          begin
            if Assigned(fOnCompareFile) then
              fOnCompareFile(s.FileName, syncByteOnly);

            if not ByteCompare(AppendToPath(src.FolderName, s.fileName),
                               AppendToPath(dst.FolderName, d.fileName)) then
            begin
              if src.SyncAction = actNoChange then
                src.SyncAction:= actUpdate;
              if d.FileDate > s.FileDate then
              begin
                src.SyncAction:= actOverwriteNewer;
                s.SyncAction:= actOverwriteNewer;
              end
              else
                s.SyncAction:= actCopy;
            end;
          end;
        end;
      syncSuper:
        begin
          if (s.FileSize <> d.FileSize) or (s.FileDate <> d.FileDate) then
          begin
            if d.FileDate > s.FileDate then
            begin
              src.SyncAction:= actOverwriteNewer;
              s.SyncAction:= actOverwriteNewer;
            end
            else
            begin
              if src.SyncAction = actNochange then
                src.SyncAction:= actUpdate;
              s.SyncAction:= actCopy;
            end;
          end
          else  // Perform a byte compare now
          begin
            if Assigned(fOnCompareFile) and (s <> nil) then
              fOnCompareFile(s.FileName, syncByteOnly);
            if not ByteCompare(AppendToPath(src.FolderName, s.fileName),
                               AppendToPath(dst.FolderName, d.fileName)) then
            begin
              if src.SyncAction = actNoChange then
                src.SyncAction:= actUpdate;
              if d.FileDate > s.FileDate then
              begin
                src.SyncAction:= actOverwriteNewer;
                s.SyncAction:= actOverwriteNewer;
              end
              else
                s.SyncAction:= actCopy;
            end;
          end;
        end;
      syncByteOnly:
        begin
          if Assigned(fOnCompareFile) and (s <> nil) then
            fOnCompareFile(s.FileName, syncByteOnly);
          if not ByteCompare(AppendToPath(src.FolderName, s.fileName),
                             AppendToPath(dst.FolderName, d.fileName)) then
          begin
            if src.SyncAction = actNoChange then
              src.SyncAction:= actUpdate;
            if d.FileDate > s.FileDate then
            begin
              src.SyncAction:= actOverwriteNewer;
              s.SyncAction:= actOverwriteNewer;
            end
            else
              s.SyncAction:= actCopy;
          end;
        end;
      else
        s.SyncAction:= actNoChange;  // physically compare here.
      end;
    end;
    Inc(k);
  end;
end;

function CompareFiles(f1name, f2name: string; PadToSector: boolean): boolean;
{- compare f1 to f2 }
{- Return true if files compare, false if not }
var
  f1, f2   : file;
  buf1,
  buf2     : ^byte;
  diff     : boolean;
  numRead1 : integer;
  numRead2 : integer;
  bufSize  : integer;
  tBufSize : integer;
  OldFMode : byte;
begin
  OldFMode:= FileMode;
  FileMode:= 0; {open for read-only}

  AssignFile(f1, f1name);
  AssignFile(f2, f2name);

  bufSize:= 256000;
  buf1:= nil;
  buf2:= nil;

  // Truncate to a 128 byte multiple - NOTE: THIS IS IMPORTANT!
  bufSize:= bufSize And Not 127;

  try
    GetMem(buf1, bufSize);
    GetMem(buf2, bufSize);

    Reset(f1, 1);
    Reset(f2, 1);

    diff:= False;     {- assume files are the same }
    repeat
      BlockRead(f1, buf1^, bufSize, numRead1);
      BlockRead(f2, buf2^, bufSize, numRead2);
      if PadToSector then
      begin
        { pad buffers to sector multiples (128) with <Ctl Z> characters }
        FillChar(pointer(integer(Buf1) + NumRead1)^, (((NumRead1 + 127) and not 127) - NumRead1), #26);
        NumRead1:= (NumRead1 + 127) And Not 127;
        FillChar(pointer(integer(Buf2) + NumRead2)^, (((NumRead2 + 127) and not 127) - NumRead2), #26);
        NumRead2:= (NumRead2 + 127) And Not 127;
      end;
      if (numRead1 > 0) And (numRead2 > 0) Then
      begin
        if (numRead1 < numRead2) then
          tBufSize:= numRead1
        else
          tBufSize:= numRead2;
        if not CompareMem(buf1, buf2, tBufSize) then
        begin
          diff:= True;
        end
        else
        begin
          if (numRead1 <> numRead2) Then
            diff:= True;
        end;
      End;
    until diff or Eof(f1) or Eof(f2);
  finally
    fileMode:= oldFMode;
    if buf1 <> nil then
      FreeMem(buf1, BufSize);
    if buf2 <> nil then
      FreeMem(buf2, BufSize);
    CloseFile(f1);
    CloseFile(f2);
  end;
  CompareFiles:= not diff;
End;

function TSync.ByteCompare(s, d : string): boolean;
// return true if s and d are and exact match
begin
  result:= CompareFiles(s, d, false);
end;

function TSync.GetSyncFolderMark(folderIdx: integer): boolean;
// get mark of sync folder
var
  fold  : TSyncFolder;
begin
  fold:= GetSyncSourceFolder(folderIdx);
  if fold <> nil then
    result:= fold.marked
  else
  begin
    fold:= GetSyncDestFolder(folderIdx);
    if fold <> nil then
      result:= fold.Marked
    else
      raise Exception.Create('Invalid folder index - GetSyncFolderMark');
  end;
end;

procedure TSync.SetSyncFolderMark(folderIdx: integer; value: boolean);
// set mark to value for sync folders
var
  fold  : TSyncFolder;
begin
  fold:= GetSyncSourceFolder(folderIdx);
  if fold <> nil then
    fold.Marked:= value;

  fold:= GetSyncDestFolder(folderIdx);
  if fold <> nil then
    fold.Marked:= value;
end;

function TSync.GetActionsExist: boolean;
// return true if any actions exist for current sync list
var
  k, j   : integer;
  fCount : integer;
begin
  result:= false;
  if MergedFolderCount > 0 then
  begin
    k:= 0;
    while (k < MergedFolderCount) and (result = false) do
    begin
      if (FolderAction[k] in [actCreate, actDelete, actUpdate, actOverwriteNewer]) then
      begin
        fCount:= MergedFileCount[k];
        if fCount > 0 then
        begin
          // check to see if any files in folder have actions
          // if not then no actions to take on folder are necessary
          j:= 0;
          while (j < fCount) and (result = false) do
          begin
            result:= (FileAction[k,j] in [actCopy, actOverwriteNewer, actDelete]);
            Inc(j);
          end;
        end
        else
          result:= true;  // if no files in folder then action exists for folder
      end;
      Inc(k);
    end;
  end
end;

procedure TSync.RemoveSyncFolder(folderIdx: integer);
// remove the synced source and destination folders associated with folderIdx
var
  s, d    : TSyncFolder;
  idx     : integer;
begin
  if (folderIdx >= 0) and (folderIdx < fMergedFolders.count) then
  begin
    ClearCache;

    s:= GetSyncSourceFolder(folderIdx);
    d:= GetSyncDestFolder(folderIdx);
    if s <> nil then
    begin
      idx:= Source.FolderIndex(s);
      Source.RemoveFolder(idx);
    end;

    if d <> nil then
    begin
      idx:= Dest.FolderIndex(d);
      Dest.RemoveFolder(idx);
    end;

    fMergedFolders.Delete(folderIdx);
  end;
end;

procedure TSync.RemoveSyncFile(folderIdx, fileIdx: integer);
// remove the file associated with the synced source and destination folders
// associated with folderIdx
var
  src, dst  : TSyncFolder;
  s, d      : TSyncFile;
  idx       : integer;
begin
  src:= GetSyncSourceFolder(folderIdx);
  dst:= GetSyncDestFolder(folderIdx);
  s:= GetSyncSourceFile(folderIdx, fileIdx);
  d:= GetSyncDestFile(folderIdx, fileIdx);
  if src <> nil then
  begin
    idx:= src.FileIndex(s);
    if idx >= 0 then
      src.RemoveFile(idx);
  end;
  ClearCache;
  if dst <> nil then
  begin
    idx:= dst.FileIndex(d);
    if idx >= 0 then
      dst.RemoveFile(idx);
  end;
  ClearCache;
end;

function TSync.GetMergedFileCount(folderIdx: integer): integer;
// return the number of files in the merged list for the specified merged folder
begin
  MergeFiles(folderIdx);  // check cache
  result:= fMergedFiles.Count;
end;

function TSync.GetMergedFiles(folderIdx: integer): TStringList;
// return the merged files list for the specified merged folder
begin
  MergeFiles(folderIdx);
  result:= fMergedFiles;
end;

procedure TSync.MergeFiles(aFolderIdx: integer);
// for the merged folder specified by aFolderIdx, first check the cache to
// see if a merged file list already exists, otherwise, build a merged list
// and set the cache. After calling this function, fMergedFiles will contain
// the merged files for the specfied folder
var
  s, d    : TSyncFolder;
  k       : integer;
  tstr    : string;
begin
  // no cache hit? merge again.
  if (fLastfileMergeFolderIdx = -1) or (aFolderIdx <> fLastFileMergeFolderIdx) then
  begin
    fLastFileMergeFolderIdx:= aFolderIdx;   // set new cache
    fMergedFiles.Clear;
    s:= GetSyncSourceFolder(aFolderIdx);
    d:= GetSyncDestFolder(aFolderIdx);
    if s <> nil then
      for k:= 0 to s.FileCount - 1 do
        fMergedFiles.Add(UpperCase(s.Files[k].FileName));
    if d <> nil then
      for k:= 0 to d.FileCount - 1 do
      begin
        tstr:= UpperCase(d.Files[k].FileName);
        if fMergedFiles.IndexOf(tstr) = -1 then
          fMergedFiles.Add(tstr);
      end;
  end;
end;

{----------------------------------------------------------[ TSyncFile ]--}

constructor TSyncFile.Create(aParentFolder: TSyncFolder);
begin
  inherited Create;
  fParent:= aParentFolder;  // parent folder (if any)
  fAction:= actNochange;
  fOrigAct:= actNoChange;
  fMarked:= false;
end;

function TSyncFile.GetFileDate: TDateTime;
// return the data and time of stored file info
begin
  result:= sRec.TimeStamp; // FileDateToDateTime(sRec.time); {-? Should I check for valid sRec }
end;

function TSyncFile.GetSize: integer;
begin
  result:= sRec.Size;
end;

procedure TSyncFile.SetAction(value: TSyncAction);
// set the action for the file.
begin
  if value = actUserNoChanged then
    fAction:= value
  else if value = actUserUndoNochange then
    fAction:= fOrigAct // revert to original action
  else
  begin
    fAction:= value;
    fOrigAct:= value; // save original action for retrival later
  end;
end;

{---------------------------------------------------------[ TSyncFolder ]--}
{   fFileList   : TList;
    fFolderName : string;
    function GetFileCount: integer;
    function GetFolderSize: integer;
    function GetFile(idx: integer): TSyncFile;
  protected
    constructor Create;
    destructor Destroy; override;
  public
    procedure AddFile(aFile: TSearchRec);
    property FileCount: integer read GetFileCount;
    property Files[idx: integer]: TSyncFile read GetFile;
    property FolderName: string fFolderName write fFolderName;
    property FolderSize: integer read GetFolderSize;
}

constructor TSyncFolder.Create;
begin
  inherited;
  fAction:= actNochange;
  fOrigAct:= actNoChange;
  fFileList:= TList.Create;
  fFileList.capacity:= FileListCapacity;
  fFolderName:= '';
  fMarked:= false;
end;

destructor TSyncFolder.Destroy;
var
  k   : integer;
begin
  if fFileList <> nil then
  begin
    for k:= 0 to fFileList.count - 1 do
      TSyncFile(fFileList[k]).Free;
    fFileList.Free;
  end;
  inherited Destroy;
end;

function TSyncFolder.GetFileCount: integer;
// return the number of files in the folder
begin
  result:= fFileList.count;
end;

function TSyncFolder.FileExists(aFileName: string): integer;
// return the index of the TSyncFile if it exists in the file list,
// or return -1 if it does not.
var
  found : boolean;
  idx   : integer;
begin
  idx:= 0;
  found:= false;
  aFileName:= UpperCase(aFileName);
  while not found and (idx < fFileList.Count) do
  begin
    if UpperCase(TSyncFile(fFileList[idx]).FileName) = aFileName then
      found:= true
    else
      Inc(idx);
  end;
  if found then
    result:= idx
  else
    result:= -1;
end;

function TSyncFolder.FileIndex(aFile: TSyncFile): integer;
// return the index of the specified file, -1 if not found
begin
  result:= fFileList.IndexOf(aFile);
end;

procedure TSyncFolder.RemoveFile(fileIdx: integer);
// remove the specified file from the file list
var
  f   : TSyncFile;
begin
  f:= Files[fileIdx];
  if f <> nil then
  begin
    fFileList.Delete(fileIdx);
    f.Free;
  end;
end;

function TSyncFolder.GetFolderSize: integer;
// return the number of bytes in the folder
var
  k   : integer;
  sum : integer;
begin
  sum:= 0;
  for k:= 0 to fFileList.count - 1 do
    Inc(sum, Files[k].FileSize);
  result:= sum;
end;

function TSyncFolder.GetFile(idx: integer): TSyncFile;
// return a TSyncFile for the specified index. Nill if idx is invalid
begin
  try
    result:= TSyncFile(fFileList[idx]);
  except
    raise Exception.Create('Invalid file index specified in TSyncFolder');
  end;
end;

procedure TSyncFolder.AddFile(aFileRec: TSearchRec);
// add a file to the folder object
var
  f     : TSyncFile;
begin
  f:= TSyncFile.Create(self);
  f.sRec:= aFileRec;
  fFileList.Add(f);
end;

procedure TSyncFolder.SetAction(value: TSyncAction);
begin
  if value = actUserNoChanged then
    fAction:= value
  else if value = actUserUndoNochange then
    fAction:= fOrigAct // revert to original action
  else
  begin
    fAction:= value;
    fOrigAct:= value; // save for later retrieval
  end;
end;

{---------------------------------------------------------[ TSyncList ]--}

constructor TSyncList.Create;
begin
  inherited Create;
  fIncludeMasks:= TFileMasks.Create;
  fIncludeMasks.BlankMaskMatchesAll:= true;
  fExcludeMasks:= TFileMasks.Create;
  fExcludeFolders:= TFileMasks.Create;
  fFolderList:= TList.Create;
  fFolderList.Capacity:= FolderListCapacity;
  fBaseFolder:= '';
  fReadSubFolders:= true;
  fReadSysFolders:= false;
end;

destructor TSyncList.Destroy;
begin
  Clear;
  fIncludeMasks.Free;
  fExcludeMasks.Free;
  fExcludeFolders.Free;
  fFolderList.Free;
  inherited Destroy;
end;

procedure TSyncList.ReadFiles(aFolder: string);
// read files starting from aFolder (recursing if necessary)
var
  sRec    : TSearchRec;
  found   : integer;
  fold    : TSyncFolder;
  tstr    : string;
begin
  tstr:= ExtractFileName(aFolder);
  if not fExcludeFolders.IsMatch(tstr) then
  begin
    fold:= AddFolder(aFolder);
    if Assigned(fOnFolder) then
      fOnFolder(aFolder);

    found:= FindFirst(AppendToPath(aFolder, '*.*'), faAnyFile, sRec);
    try
      while found = 0 do
      begin
        if (sRec.Name <> '.') and (sRec.name <> '..') then
        begin
          if sRec.Attr and faDirectory <> 0 then
          begin
            if ProcessSubFolders then
            begin
              // if folder is a sys folder and process sys folders is false then skip
              if (sRec.Attr and faSysFile = 0) or
                 ((sRec.Attr and faSysFile <> 0) and (ProcessSystemFolders)) then
              begin
                ReadFiles(AppendToPath(aFolder, sRec.Name));
              end;
            end;
          end
          else
          begin
            if fIncludeMasks.IsMatch(sRec.Name) and not fExcludeMasks.IsMatch(srec.Name) then
            begin
              fold.AddFile(sRec);
              if Assigned(fOnFile) then
                fOnFile(sRec.name);
            end;
          end;
        end;
        found:= FindNext(sRec);
      end;
    finally
      FindClose(sRec);
    end;
  end;
end;

function TSyncList.GetFolderCount: integer;
// return the number of folders in the list
begin
  result:= fFolderList.Count;
end;

function TSyncList.GetFolder(idx: integer): TSyncFolder;
begin
  try
    result:= TSyncFolder(fFolderList[idx]);
  except
    raise Exception.Create('Invalid folder index specified in TSyncList');
  end;
end;

function TSyncList.GetFolderNameNoBase(idx: integer): string;
// return the name of a folder with out the base folder, ie, truncates the
// base folder name from the name of the specified folder
var
  tstr  : string;
  tstr2 : string;
begin
  tstr:= Folders[idx].FolderName;
  tstr2:= UpperCase(tstr);
  if Pos(UpperCase(BaseFolder), tstr2) = 1 then
  begin
    Delete(tstr, 1, Length(baseFolder));
    // make sure no leading '\' exists.
    if (Length(tstr) > 1) and (tstr[1] = '\') then
      Delete(tstr, 1, 1);
    result:= tstr;
  end
  else
    raise Exception.Create('Invalid operation in GetFolderNameNoBase');
end;

procedure TSyncList.SetBaseFolder(value: string);
// this will clear any currently contained folders
begin
  Clear;
  fBaseFolder:= value;
end;

procedure TSyncList.Clear;
var
  k   : integer;
begin
  for k:= 0 to FolderCount - 1 do
    Folders[k].Free;
  fFolderList.Clear;
end;

procedure TSyncList.RemoveFolder(folderIdx: integer);
var
  f   : TSyncFolder;
begin
  f:= Folders[folderIdx];
  if f <> nil then
  begin
    fFolderList.Delete(folderIdx);
    f.Free;
  end;
end;

function TSyncList.IsSubFolder(baseIdx, folderIdx: integer): boolean;
// return true if the folder specified by folderIdx is a sub folder of the
// folder at baseIdx
var
  b, f  : string;
begin
  b:= UpperCase(AppendToPath(Folders[baseIdx].FolderName, '')); // force "\" to end of line
  f:= UpperCase(AppendToPath(Folders[folderIdx].FolderName, ''));
  result:= Pos(b, f) = 1;
end;

function TSyncList.HasSubFolders(aFolder: TSyncFolder): boolean;
// return true if the specified folder has sub folders.
var
  found   : boolean;
  k       : integer;
  idx     : integer;
begin
  found:= false;
  k:= 0;
  idx:= FolderIndex(aFolder);
  while not found and (k < fFolderList.count) do
  begin
    if idx <> k then
      found:= IsSubFolder(idx, k);
    if not found then
      Inc(k);
  end;
  result:= found;
end;

procedure TSyncList.Execute;
// read the folders starting at the base folder
begin
  Clear;    // clear out any existing folder information
  if baseFolder <> '' then
    ReadFiles(BaseFolder)
  else
    raise Exception.Create('No base folder specified.');
end;

function TSyncList.AddFolder(aFolderName: string): TSyncFolder;
begin
  result:= TSyncFolder.Create;
  result.FolderName:= aFolderName;
  fFolderList.Add(result);
end;

function TSyncList.FolderIndex(aFolder: TSyncFolder): integer;
// return the index of the specified folder, -1 if not found
begin
  result:= fFolderList.IndexOf(aFolder);
end;

function TSyncList.FolderExistsNoBase(aFolder: string): integer;
// search the folder list to see if a folder name exists.
// The base folder portion of the folder names is ignored in this search
// This can be used to find things like FolderExistsNoBase('Program Files')
// searching for just the folder name without it's entire path
var
  found   : boolean;
  idx     : integer;
begin
  found:= false;
  idx:= 0;
  while not found and (idx < FolderCount) do
  begin
    if UpperCase(FolderNameNoBase[idx]) = UpperCase(afolder) then
      found:= true
    else
      Inc(idx);
  end;
  if found then
    result:= idx
  else
    result:= -1;
end;

END.





