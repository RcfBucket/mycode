unit Unit1;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, StStrL;

type
  TForm1 = class(TForm)
    Edit1: TEdit;
    Edit2: TEdit;
    Label1: TLabel;
    Label2: TLabel;
    Button1: TButton;
    Label3: TLabel;
    Label4: TLabel;
    procedure Button1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

{$R *.DFM}

function MatchWildcard(mask, fileName: string): boolean;
// return true if fileName matches current wildcard list.
// fileName must be a valid filename/path.
Var
	w, f	: Byte;
	wName, wExt : string;
	fName, fExt : string;
	match	: Boolean;

	function MatchName(wild, name: string): boolean;
	var
		matches	: Boolean;
	Begin
		If (wild = name) then
      result:= true
    else
    begin
      If Length(wild) > Length(name) then
        PadL(name, Length(wild));

      f:= 1;
      w:= 1;
      matches:= (wild <> '');		{- assume a match }
      While matches and (f <= Length(name)) Do Begin		{- check filename }
        If wild[w] = '*' Then 	{- rest of name matches }
          f:= Length(name)+1
        else Begin
          If (wild[w] <> '?') and (name[f] <> wild[w]) Then
            matches:= False
          else Begin
            Inc(w); Inc(f);
            If ((w > Length(wild)) and (f < Length(name))) Then
              matches:= False;
          end;
        end;
      end;
      MatchName:= matches;
    end;
	end;

begin
	fileName:= UpperCase(fileName);
	mask:= UpperCase(mask);

  fName:= JustFileNameL(fileName);
  fExt:= JustExtensionL(fileName);

  wName:= JustFileNameL(mask);
  wExt:= JustExtensionL(mask);

(*	fSplit(fileName, fileName, fName, fExt);
	fExt:= StripRange(fExt, '.', '.');	{- remove '.' from extension }

	fSplit(mask, fileName, wName, wExt);
	wext:= StripRange(wext, '.', '.');	{- remove '.' from wild card extension }
*)
	If (wExt = '') and (Pos('.', mask) = 0) Then
		wExt:= '*';

	match:= MatchName(wName, fName);
	If match Then
		match:= MatchName(wExt, fExt);
	MatchWildcard:= match;
end;

procedure TForm1.Button1Click(Sender: TObject);
begin
  label3.caption:= JustFileNameL(edit1.text);
  label4.caption:= JustExtensionL(edit1.text);
end;

end.
