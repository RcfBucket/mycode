object FrmShowFiles: TFrmShowFiles
  Left = 631
  Top = 127
  BorderIcons = [biSystemMenu, biMaximize]
  Caption = 'Sanity Check - Files to Copy/Delete'
  ClientHeight = 232
  ClientWidth = 524
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = True
  Position = poScreenCenter
  OnCreate = FormCreate
  OnResize = FormResize
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object PageControl: TPageControl
    Left = 0
    Top = 0
    Width = 524
    Height = 232
    ActivePage = TabSheet3
    Align = alClient
    TabOrder = 0
    OnChange = PageControlChange
    object TabSheet1: TTabSheet
      Caption = 'To &Copy...'
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 0
      object grdCopyFiles: TStringGrid
        Left = 0
        Top = 0
        Width = 524
        Height = 215
        Align = alClient
        ColCount = 2
        DefaultRowHeight = 16
        FixedCols = 0
        Options = [goFixedVertLine, goFixedHorzLine, goRowSelect, goThumbTracking]
        TabOrder = 0
        ColWidths = (
          258
          261)
      end
    end
    object TabSheet2: TTabSheet
      Caption = 'To &Delete...'
      ImageIndex = 1
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 0
      object grdDelFiles: TStringGrid
        Left = 0
        Top = 0
        Width = 524
        Height = 215
        Align = alClient
        ColCount = 1
        DefaultRowHeight = 16
        FixedCols = 0
        Options = [goFixedVertLine, goFixedHorzLine, goRowSelect, goThumbTracking]
        TabOrder = 0
        ColWidths = (
          517)
      end
    end
    object TabSheet3: TTabSheet
      Caption = 'To C&reate...'
      ImageIndex = 2
      object grdCreates: TStringGrid
        Left = 0
        Top = 0
        Width = 516
        Height = 204
        Align = alClient
        ColCount = 1
        DefaultRowHeight = 16
        FixedCols = 0
        Options = [goFixedVertLine, goFixedHorzLine, goRowSelect, goThumbTracking]
        TabOrder = 0
        ColWidths = (
          517)
      end
    end
  end
end
