unit ReleaseNotes;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, ExtCtrls;

type
  TFrmRelNotes = class(TForm)
    Memo1: TMemo;
    Panel1: TPanel;
    BtnClose: TButton;
    procedure BtnCloseClick(Sender: TObject);
    procedure Panel1Resize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FrmRelNotes: TFrmRelNotes;

implementation

{$R *.DFM}

procedure TFrmRelNotes.BtnCloseClick(Sender: TObject);
begin
  Close;
end;

procedure TFrmRelNotes.Panel1Resize(Sender: TObject);
begin
  BtnClose.Left:= (Panel1.Width div 2) - (BtnClose.Width div 2);
end;

procedure TFrmRelNotes.FormCreate(Sender: TObject);
var
  hMenu : THandle;
begin
  // Remove system menu
  hMenu:= GetSystemMenu(Handle, False);
  DeleteMenu(hMenu, SC_MINIMIZE, MF_BYCOMMAND);
end;

end.
