unit Report;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, ComCtrls, SyncData, Filter, ToolWin, ExtCtrls, ActnList,
  ImgList;

type
  TFrmReport = class(TForm)
    edReport: TRichEdit;
    ToolBar1: TToolBar;
    BtnPrint: TToolButton;
    BtnClose: TToolButton;
    ToolButton3: TToolButton;
    Panel1: TPanel;
    Label1: TLabel;
    TrackBar1: TTrackBar;
    ImageList1: TImageList;
    ActionList1: TActionList;
    ActPrint: TAction;
    ActClose: TAction;
    procedure TrackBar1Change(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure ActPrintExecute(Sender: TObject);
    procedure ActCloseExecute(Sender: TObject);
  private
    fSync   : TSync;
    procedure SetSync(value: TSync);
  public
    property SyncPtr: TSync write SetSync;
  end;

var
  FrmReport: TFrmReport;

implementation

uses GenReport;


{$R *.DFM}

procedure TFrmReport.SetSync(value: TSync);
var
  k, j      : integer;
  src, dst  : TSyncFolder;
  s, d      : TSyncFile;
  act       : TSyncAction;
  frm       : TFrmGenReport;
begin
  fSync:= value;
  edReport.Clear;
  if fSync = nil then
    exit;

  screen.Cursor:= crHourGlass;
  frm:= TFrmGenReport.Create(application);
  frm.Show;
  try
    edReport.Lines.BeginUpdate;
    edReport.Clear;
    edReport.Paragraph.Tab[0]:= 75;
    edReport.Paragraph.Tab[1]:= 200;
    edReport.Paragraph.Tab[2]:= 300;

    for k:= (fSync.MergedFolderCount-1) downto 0 do
    begin
      if CheckFolderFilter(fSync.FolderAction[k]) then
      begin
        frm.Label2.Caption:= IntToStr(k);
        application.ProcessMessages;
        src:= fsync.GetSyncSourceFolder(k);
        dst:= fsync.GetSyncDestFolder(k);
        act:= fsync.FolderAction[k];

        edReport.SelAttributes.Color:= clBlue;
        edReport.SelAttributes.Style:= [fsBold];
        edReport.lines.Add(ActionStrs[act]);

        edReport.SelAttributes.Color:= clBlack;
        edReport.SelAttributes.Style:= [];

        // print out affected files
        if src <> nil then
        begin
          edReport.Lines.Add('Source:'#9 + src.FolderName);
          if dst <> nil then
            edReport.Lines.Add('Destination:'#9 + dst.FolderName);

          for j:= 0 to fsync.MergedFileCount[k] - 1 do
          begin
            if CheckFileFilter(fsync.FileAction[k, j]) then
            begin
              s:= fsync.GetSyncSourceFile(k, j);
              d:= fsync.GetSyncDestFile(k, j);
              if (s <> nil) and (d <> nil) then
              begin
                edReport.Lines.Add('  ' + ActionStrs[fsync.FileAction[k,j]] + #9 +
                             s.FileName + #9 + d.FileName);
              end
              else if d <> nil then
              begin
                edReport.Lines.Add('  ' + ActionStrs[fsync.FileAction[k,j]] + #9#9 + d.FileName);
              end
              else  // s <> nil
                edReport.Lines.Add('  ' + ActionStrs[fsync.FileAction[k,j]] + #9 +
                             s.FileName + #9 + s.FileName);
            end;
          end;
        end
        else
        begin
            edReport.Lines.Add('Destination:'#9 + dst.FolderName);
          // deleting folder, show files involved
          for j:= 0 to fsync.MergedFileCount[k] - 1 do
          begin
            if CheckFileFilter(fsync.FileAction[k, j]) then
            begin
              d:= fsync.GetSyncDestFile(k, j);
              edReport.Lines.Add('  ' + ActionStrs[fsync.FileAction[k,j]] + #9#9 + d.FileName)
            end;
          end;
        end;

        edReport.Lines.Add('');
      end;
    end;
    edReport.Lines.EndUpdate;
    edReport.SelStart:= 0;
    edReport.SelLength:= 0;
  finally
    frm.Free;
    trackBar1.Position:= 200;
    screen.Cursor:= crDefault;
  end;
end;

procedure TFrmReport.TrackBar1Change(Sender: TObject);
begin
  edReport.HideSelection:= true;
  edReport.SelStart:= 0;
  edReport.SelLength:= Length(edReport.text);
  edReport.Paragraph.Tab[1]:= trackbar1.position;
  edReport.SelLength:= 0;
//  edReport.Visible:= true;
//    edReport.Paragraph.Tab[2]:= 300;
//
end;

procedure TFrmReport.FormResize(Sender: TObject);
begin
  Panel1.Width:= Width;
end;

procedure TFrmReport.FormCreate(Sender: TObject);
var
  hMenu  : THandle;
begin
  // Remove system menu
  hMenu:= GetSystemMenu(Handle, False);
  DeleteMenu(hMenu, SC_MINIMIZE, MF_BYCOMMAND);
end;

procedure TFrmReport.ActPrintExecute(Sender: TObject);
begin
  edReport.Print('File Sync Report');
end;

procedure TFrmReport.ActCloseExecute(Sender: TObject);
begin
  Close;
end;

end.
