unit Globals;

interface

uses
  SysUtils, Windows, Forms, Classes, Registry;

const
  SyncRegKeyBase    = 'Software\Sliq Systems\FileSync2.0\';
  SyncRegKeyOptionsBase = SyncRegKeyBase + 'Settings\';
  SyncRegKeyProfileBase = SyncRegKeyBase + 'Profiles\';

  strDestinationHistory = 'DestinationHistory';
  strSourceHistory = 'SourceHistory';

type
  TProfileHistory = class
  private
    fSrcHistory : TStringList;
    fDstHistory : TStringList;
    constructor Create;
  public
    destructor Destroy; override;
  end;

  TProfiles = class
  private
    fProfiles : TStringList;
    fCurrentProfileIdx: integer;
    function GetSrcHistory(profileIdx: integer): TStringList;
    function GetDstHistory(profileIdx: integer): TStringList;
    function GetCount: integer;
    function GetProfileStr(index: integer): string;
  public
    constructor Create;
    destructor Destroy; override;
    procedure Save;
    procedure Load;
    procedure DeleteProfile(idx: integer);
    function AddProfile(aStr: string): integer;
    property ProfileFolders: TStringlist read fProfiles;
    property Count: integer read GetCount;
    property ProfileStr[index: integer]: string read GetProfileStr; default;
    property SrcHistory[profileIdx: integer]: TStringList read GetSrcHistory;
    property DstHistory[profileIdx: integer]: TStringList read GetDstHistory;
    property CurrentProfileIdx : integer read fCurrentProfileIdx write fCurrentProfileIdx;
  end;


procedure SaveFormPosition(aForm: TForm);
procedure ReadFormPosition(aForm: TForm);
function SyncRegKeyProfile(aProfile: string): string;
function GetLastErrorString: string;

var
//  profileStr        : TStringList;
  profiles          : TProfiles;

implementation

const
  DefaultProfileName = 'DefaultProfile';

function GetLastErrorString: string;
// return a string representing the last windows system error
var
  tstr    : string;
  err     : integer;
begin
  err:= GetLastError;
  SetLength(tstr, 1024);
  if FormatMessage(FORMAT_MESSAGE_FROM_SYSTEM,
                nil,
                err,
                0,
                PChar(tstr),
                1024,
                nil) <> 0 then
  begin
    SetLength(tstr, StrLen(PChar(tstr)));
    result:= tstr;
  end
  else
    result:= 'System ID - ' + IntToStr(err);
end;

function SyncRegKeyProfile(aProfile: string): string;
// return a proper key value (including profile)
begin
  result:= SyncRegKeyProfileBase + aProfile + '\';
end;

procedure ReadFormPosition(aForm: TForm);
var
  r   : TRegistry;
  rect: TRect;
  s   : TWindowState;
begin
  r:= TRegistry.Create;
  try
    if r.OpenKey(SyncRegKeyOptionsBase + 'Forms\' + aForm.Name, false) then
    begin
      if r.ValueExists('Position') then
      try
        r.ReadBinaryData('Position', rect, sizeof(rect));
        aForm.Left:= rect.left;
        aForm.Top:= rect.Top;
        if (aForm.BorderStyle in [bsSizeable, bsSizeToolWin]) then
        begin
          aForm.Width:= rect.right;
          aForm.Height:= rect.Bottom;
        end;

        if r.ValueExists('State') then
        begin
          s:= TWindowState(r.ReadInteger('State'));
          if s = wsMaximized then
            aForm.WindowState:= wsMaximized;
        end;
      except
      end;
      r.CloseKey;
    end;
  finally
    r.Free;
  end;
end;

procedure SaveFormPosition(aForm: TForm);
var
  r   : TRegistry;
  rect: TRect;
begin
  r:= TRegistry.Create;
  try
    r.OpenKey(SyncRegKeyOptionsBase + 'Forms\' + aForm.Name, true);

    rect.left:= aForm.Left;
    rect.top:= aForm.Top;
    rect.Right:= aForm.Width;
    rect.Bottom:= aForm.Height;

    r.WriteBinaryData('Position', rect, sizeof(rect));
    r.WriteInteger('State', Ord(aForm.WindowState));
    r.CloseKey;
  finally
    r.Free;
  end;
end;


{ TProfiles }

constructor TProfiles.Create;
begin
  fProfiles:= TStringList.Create;
  fProfiles.CaseSensitive:= false;
  Load;
end;

destructor TProfiles.Destroy;
var
  k : integer;
  history: TProfileHistory;
begin
  for k:= 0 to fProfiles.Count - 1 do
  begin
    history:= TProfileHistory(fProfiles.Objects[k]);
    history.Free;
  end;
  fProfiles.Free;
  inherited;
end;

procedure TProfiles.Load;
var
  r   : TRegistry;
  s   : TStringList;
  k   : integer;
  j   : integer;
  hist: TProfileHistory;
  profStr: string;
  tstr  : string;
begin
  r:= TRegistry.Create;
  s:= TStringList.Create;

  r.OpenKey(SyncRegKeyBase, true);
  try
    fCurrentProfileIdx:= r.ReadInteger('CurrentProfile');
  except
    fCurrentProfileIdx:= 0;
  end;
  r.CloseKey;

  try
    try
      r.OpenKey(SyncRegKeyProfileBase, true);
      r.GetKeyNames(s);
      fProfiles.Clear;
      hist:= TProfileHistory.Create;
      fProfiles.AddObject(DefaultProfileName, hist);
      for k:= 0 to s.Count - 1 do
      begin
        if UpperCase(s[k]) <> UpperCase(DefaultProfileName) then
        begin
          hist:= TProfileHistory.Create;
          fProfiles.AddObject(s[k], hist);
        end;
      end;
    except

    end;
    r.CloseKey;

    // Now load histories for each profile
    try
      for k:= 0 to fProfiles.Count - 1 do
      begin
        s.Clear;
        profStr:= fProfiles[k];
        r.OpenKey(SyncRegKeyProfile(profStr) + strSourceHistory, true);
        r.GetValueNames(s);
        hist:= TProfileHistory(fProfiles.objects[k]);
        for j:= 0 to s.Count - 1 do
        begin
          tstr:= r.ReadString(s[j]);
          tstr:= Trim(tstr);
          if Length(tstr) > 0 then
          begin
            hist.fSrcHistory.Add(tstr);
          end;
        end;
        r.CloseKey;
      end;
    except
    end;

    try
      for k:= 0 to fProfiles.Count - 1 do
      begin
        s.Clear;
        profStr:= fProfiles[k];
        r.OpenKey(SyncRegKeyProfile(profStr) + strDestinationHistory, true);
        r.GetValueNames(s);
        hist:= TProfileHistory(fProfiles.objects[k]);
        for j:= 0 to s.Count - 1 do
        begin
          tstr:= r.ReadString(s[j]);
          tstr:= Trim(tstr);
          if Length(tstr) > 0 then
          begin
            hist.fDstHistory.Add(tstr);
          end;
        end;
        r.CloseKey;
      end;
    except
    end;

    if (fCurrentProfileIdx < 0) and (fCurrentProfileIdx >= fProfiles.Count) then
      fCurrentProfileIdx:= 0;
  finally
    r.Free;
    s.Free;
  end;
end;

function TProfiles.GetDstHistory(profileIdx: integer): TStringList;
begin
  if (profileIdx >= 0) and (profileIdx < fProfiles.Count) then
  begin
    result:= TProfileHistory(fProfiles.Objects[profileIdx]).fDstHistory;
  end
  else
    result:= nil;
end;

function TProfiles.GetSrcHistory(profileIdx: integer): TStringList;
begin
  if (profileIdx >= 0) and (profileIdx < fProfiles.Count) then
  begin
    result:= TProfileHistory(fProfiles.Objects[profileIdx]).fSrcHistory;
  end
  else
    result:= nil;
end;

function TProfiles.GetCount: integer;
begin
  result:= fProfiles.Count;
end;

function TProfiles.GetProfileStr(index: integer): string;
begin
  if (index >= 0) and (index < fProfiles.Count) then
    result:= fProfiles[index]
  else
    result:= '';
end;

procedure TProfiles.DeleteProfile(idx: integer);
var
  hist  : TProfileHistory;
begin
  if (idx >= 0) and (idx < fProfiles.Count) then
  begin
    hist:= TProfileHistory(fProfiles.Objects[idx]);
    hist.Free;
    fProfiles.Delete(idx);
    if idx <= fCurrentProfileIdx then
      Dec(fCurrentProfileIdx);
  end;
end;


function TProfiles.AddProfile(aStr: string): integer;
var
  hist : TProfileHistory;
  idx  : integer;
begin
  // Check for duplicates first. if found then dont bother adding.
  idx:= fProfiles.IndexOf(aStr);
  if idx < 0 then
  begin
    hist:= TProfileHistory.Create;
    result:= fProfiles.AddObject(aStr, hist);
  end
  else
  begin
    result:= idx;
  end;
end;

procedure TProfiles.Save;
var
  r   : TRegistry;
  hist: TStringList;
  histIdx: integer;
  profIdx: integer;
begin
  // first delete the entire profile node and remove all existing historys
  r:= TRegistry.Create;
  try
    r.DeleteKey(SyncRegKeyProfileBase);
  finally
    r.CloseKey;
  end;

  r.OpenKey(SyncRegKeyBase, true);
  try
    r.WriteInteger('CurrentProfile', fCurrentProfileIdx);
  except
  end;
  r.CloseKey;

  for profIdx:= 0 to profiles.Count - 1 do
  begin
    // Source History
    r.OpenKey(SyncRegKeyProfileBase + profiles[profIdx] + '\' + strSourceHistory, true);
    hist:= SrcHistory[profIdx];
    for histIdx:= 0 to hist.Count - 1 do
    begin
      r.WriteString('Folder' + IntToStr(histIdx+1), hist[histIdx]);
    end;
    r.CloseKey;

    // Destination History
    r.OpenKey(SyncRegKeyProfileBase + profiles[profIdx] + '\' + strDestinationHistory, true);
    hist:= DstHistory[profIdx];
    for histIdx:= 0 to hist.Count - 1 do
    begin
      r.WriteString('Folder' + IntToStr(histIdx+1), hist[histIdx]);
    end;
    r.CloseKey;

  end;
  r.Free;
end;

{ TProfileHistory }

constructor TProfileHistory.Create;
begin
  fSrcHistory:= TStringList.Create;
  fSrcHistory.CaseSensitive:= false;

  fDstHistory:= TStringList.Create;
  fDstHistory.CaseSensitive:= false;
end;

destructor TProfileHistory.Destroy;
begin
  fSrcHistory.Free;
  fDstHistory.Free;

  inherited;
end;

INITIALIZATION
  profiles:= TProfiles.Create;
FINALIZATION
  profiles.Free;
end.
