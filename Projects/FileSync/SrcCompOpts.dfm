object FrmSrcCompOpts: TFrmSrcCompOpts
  Left = 280
  Top = 107
  BorderStyle = bsDialog
  Caption = 'Source Compare Options'
  ClientHeight = 435
  ClientWidth = 431
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = True
  Position = poOwnerFormCenter
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object BtnCancel: TBitBtn
    Left = 320
    Top = 400
    Width = 97
    Height = 27
    Kind = bkCancel
    NumGlyphs = 2
    TabOrder = 2
  end
  object BtnOK: TBitBtn
    Left = 216
    Top = 400
    Width = 97
    Height = 27
    Caption = 'OK'
    Default = True
    Glyph.Data = {
      DE010000424DDE01000000000000760000002800000024000000120000000100
      0400000000006801000000000000000000001000000010000000000000000000
      80000080000000808000800000008000800080800000C0C0C000808080000000
      FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
      3333333333333333333333330000333333333333333333333333F33333333333
      00003333344333333333333333388F3333333333000033334224333333333333
      338338F3333333330000333422224333333333333833338F3333333300003342
      222224333333333383333338F3333333000034222A22224333333338F338F333
      8F33333300003222A3A2224333333338F3838F338F33333300003A2A333A2224
      33333338F83338F338F33333000033A33333A222433333338333338F338F3333
      0000333333333A222433333333333338F338F33300003333333333A222433333
      333333338F338F33000033333333333A222433333333333338F338F300003333
      33333333A222433333333333338F338F00003333333333333A22433333333333
      3338F38F000033333333333333A223333333333333338F830000333333333333
      333A333333333333333338330000333333333333333333333333333333333333
      0000}
    NumGlyphs = 2
    TabOrder = 3
    OnClick = BtnOKClick
  end
  object BtnDefaults: TBitBtn
    Left = 8
    Top = 400
    Width = 97
    Height = 27
    Caption = '&Defaults'
    Glyph.Data = {
      42010000424D4201000000000000760000002800000011000000110000000100
      040000000000CC00000000000000000000001000000010000000000000000000
      BF0000BF000000BFBF00BF000000BF00BF00BFBF0000C0C0C000808080000000
      FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00777777777777
      77777000000070000000000007777000000070FFFFFFFFFF07777000000070FC
      CFCCCCCF07777000000070FFFFFFFFFF07777000000070FCCFCCCCCF07777000
      000070FFFFFFFFFF07777000000070FFFFFFF0FF07777000000070F00FFF0B0F
      07770000000070F0F0F0B0F000700000000070FF0B0B0F0FBF00000000007000
      00F0F0FBFBF0000000007777770B0FBFBFB00000000077777770FBFBFB000000
      0000777777770000007000000000777777777777777770000000777777777777
      777770000000}
    TabOrder = 0
    OnClick = BtnDefaultsClick
  end
  object BtnFont: TBitBtn
    Left = 112
    Top = 400
    Width = 97
    Height = 27
    Caption = '&Font'
    Glyph.Data = {
      4E010000424D4E01000000000000760000002800000012000000120000000100
      040000000000D800000000000000000000001000000000000000000000000000
      8000008000000080800080000000800080008080000080808000C0C0C0000000
      FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00888888888888
      88888800000088888888888800078800000088888888888DDDD0780000008078
      8880070D788D880000008007770A08DD78888800000088AAAAAA78DD78887800
      0000880080A0880D788D78000000888A7AA7888DD1DD7800000088800A088877
      0180880000008888AA7870CC07888800000088880088CCC00078880000008888
      888800888C0888000000888888870CC0CC0888000000888888080C7800888800
      0000888888070C78C078880000008888880CCC0CC08888000000888888800880
      888888000000888888888888888888000000}
    TabOrder = 1
    OnClick = BtnFontClick
  end
  object grpInternal: TGroupBox
    Left = 16
    Top = 200
    Width = 401
    Height = 185
    Caption = 'Internal Compare Options'
    TabOrder = 4
    object Label1: TLabel
      Left = 16
      Top = 28
      Width = 99
      Height = 13
      Caption = 'Characters to ignore'
    end
    object Label3: TLabel
      Left = 16
      Top = 76
      Width = 39
      Height = 13
      Caption = 'Tab size'
    end
    object Label2: TLabel
      Left = 16
      Top = 52
      Width = 146
      Height = 13
      Caption = 'Minimum lines to make a match'
    end
    object Label4: TLabel
      Left = 16
      Top = 100
      Width = 65
      Height = 13
      Caption = 'Display Width'
    end
    object ChkShowFileInfo: TCheckBox
      Left = 16
      Top = 128
      Width = 129
      Height = 17
      Caption = 'Show file information'
      TabOrder = 0
    end
    object ChkOptimize: TCheckBox
      Left = 224
      Top = 160
      Width = 129
      Height = 17
      Caption = 'Optimize results'
      TabOrder = 1
    end
    object ChkCaseSensitive: TCheckBox
      Left = 224
      Top = 128
      Width = 129
      Height = 17
      Caption = 'Case sensitive'
      TabOrder = 2
    end
    object ChkDispLineNums: TCheckBox
      Left = 224
      Top = 144
      Width = 129
      Height = 17
      Caption = 'Display Line Numbers'
      TabOrder = 3
    end
    object ChkIgnoreWS: TCheckBox
      Left = 16
      Top = 160
      Width = 129
      Height = 17
      Caption = 'Ignore Whitespace'
      TabOrder = 4
    end
    object ChkConvertTabs: TCheckBox
      Left = 16
      Top = 144
      Width = 129
      Height = 17
      Caption = 'Convert Tabs'
      TabOrder = 5
    end
    object edCharsToIgnore: TEdit
      Left = 224
      Top = 24
      Width = 160
      Height = 21
      TabOrder = 6
    end
    object edTabSize: TEdit
      Left = 224
      Top = 72
      Width = 145
      Height = 21
      ReadOnly = True
      TabOrder = 7
      Text = '1'
    end
    object udTabSize: TUpDown
      Left = 369
      Top = 72
      Width = 16
      Height = 21
      Associate = edTabSize
      Min = 1
      Max = 15
      Position = 1
      TabOrder = 8
    end
    object edMinLines: TEdit
      Left = 224
      Top = 48
      Width = 145
      Height = 21
      ReadOnly = True
      TabOrder = 9
      Text = '1'
    end
    object udMinLines: TUpDown
      Left = 369
      Top = 48
      Width = 16
      Height = 21
      Associate = edMinLines
      Min = 1
      Max = 32767
      Position = 1
      TabOrder = 10
    end
    object edDispWidth: TEdit
      Left = 224
      Top = 96
      Width = 145
      Height = 21
      ReadOnly = True
      TabOrder = 11
      Text = '40'
    end
    object udDispWidth: TUpDown
      Left = 369
      Top = 96
      Width = 16
      Height = 21
      Associate = edDispWidth
      Min = 40
      Max = 256
      Position = 40
      TabOrder = 12
    end
  end
  object rdoExternal: TRadioButton
    Left = 8
    Top = 8
    Width = 217
    Height = 17
    Caption = 'Use External Compare Tool'
    TabOrder = 5
    OnClick = OptionClicked
  end
  object rdoInternal: TRadioButton
    Left = 8
    Top = 176
    Width = 169
    Height = 17
    Caption = 'Use Internal Compare Tool'
    Checked = True
    TabOrder = 6
    TabStop = True
    OnClick = OptionClicked
  end
  object grpExternal: TGroupBox
    Left = 22
    Top = 31
    Width = 401
    Height = 121
    Caption = 'External Compare Options'
    TabOrder = 7
    object Label5: TLabel
      Left = 8
      Top = 24
      Width = 53
      Height = 13
      Caption = 'Executable'
    end
    object Label6: TLabel
      Left = 8
      Top = 72
      Width = 52
      Height = 13
      Caption = 'Arguments'
    end
    object Label7: TLabel
      Left = 144
      Top = 92
      Width = 193
      Height = 13
      Caption = '%s = Source File, %d = Destination File'
    end
    object edArguments: TEdit
      Left = 8
      Top = 88
      Width = 121
      Height = 21
      TabOrder = 0
      Text = '%s  %r'
    end
    object edExternalProgram: TEdit
      Left = 10
      Top = 40
      Width = 347
      Height = 21
      TabOrder = 1
      Text = '"C:\Program Files (x86)\WinMerge\WinMergeU.exe"'
    end
    object Button1: TButton
      Left = 363
      Top = 40
      Width = 21
      Height = 21
      Caption = '...'
      TabOrder = 2
      OnClick = Button1Click
    end
  end
  object FontDialog1: TFontDialog
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    Options = []
    Left = 328
    Top = 160
  end
  object OpenDialog1: TOpenDialog
    Left = 264
    Top = 160
  end
end
