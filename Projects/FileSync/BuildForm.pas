unit BuildForm;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, ExtCtrls, Buttons, Grids;

type
  TFrmBuild = class(TForm)
    Panel1: TPanel;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    BtnYes: TButton;
    BtnNo: TButton;
    ChkCopy: TCheckBox;
    ChkDelete: TCheckBox;
    chkCreate: TCheckBox;
    Label7: TLabel;
    Label8: TLabel;
    BtnShowIt: TButton;
    procedure BtnShowItClick(Sender: TObject);
  private
    { Private declarations }
  public
    srcCopy, dstCopy : string;
    delFiles         : string;
    creates          : TStringList;
    procedure HideButtons(doHide: boolean);

  end;

implementation

uses Sanity;

{$R *.DFM}

procedure TFrmBuild.HideButtons(doHide: boolean);
begin
  if doHide then
  begin
    BtnShowIt.visible:= false;
    chkDelete.Visible:= false;
    chkCopy.Visible:= false;
    BtnYes.Enabled:= false;
    BtnNo.Enabled:= false;
    BtnYes.visible:= false;
    BtnNo.visible:= false;
    height:= 90
  end
  else
  begin
    BtnShowIt.visible:= true;
    chkDelete.Visible:= true;
    chkCopy.Visible:= true;
    BtnYes.Enabled:= true;
    BtnNo.Enabled:= true;
    BtnYes.visible:= true;
    BtnNo.visible:= true;
    height:= 215;
  end;
end;

procedure TFrmBuild.BtnShowItClick(Sender: TObject);

  procedure ParseIt(const aTitle, abuf: string; grd: TStringGrid; aCol: integer);
  var
    tstr    : string;
    k       : integer;
    r       : integer;
  begin
    k:= 1;
    tstr:= '';
    r:= 1;
    grd.RowCount:= 2;
    grd.FixedRows:= 1;
    grd.Cells[aCol, 0]:= aTitle;
    while (k <= Length(aBuf)) do
    begin
      if aBuf[k] <> #0 then
        tstr:= tstr + aBuf[k]
      else
      begin
        if r > 1 then
          grd.RowCount:= r;
        grd.Cells[aCol, r]:= tstr;
        tstr:= '';
        Inc(r);
      end;
      Inc(k);
    end;
  end;

  procedure DoCreates(grd: TStringGrid);
  var
    k   : integer;
  begin
    with grd do
    begin
      RowCount:= 2;
      Cells[0,0]:= 'Empty Folders to be Created';
      if creates.Count > 0 then
      begin
        RowCount:= 1 + creates.Count;
        for k:= 0 to creates.Count - 1 do
          Cells[0,k+1]:= creates[k];
      end;
    end;
  end;

begin
  with TFrmShowFiles.Create(self) do
  begin
    ParseIt('Files to be deleted', delFiles, grdDelFiles, 0);
    ParseIt('Copy From...', srcCopy, grdCopyFiles, 0);
    ParseIt('Copy To...', dstCopy, grdCopyFiles, 1);
    DoCreates(grdCreates);
    ShowModal;
    Free;
  end;
end;

end.
