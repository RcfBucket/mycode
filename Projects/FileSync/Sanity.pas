unit Sanity;

INTERFACE

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, ComCtrls, ExtCtrls, Grids;

type
  TFrmShowFiles = class(TForm)
    PageControl: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    TabSheet3: TTabSheet;
    grdCopyFiles: TStringGrid;
    grdDelFiles: TStringGrid;
    grdCreates: TStringGrid;
    procedure PageControlChange(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FrmShowFiles: TFrmShowFiles;

IMPLEMENTATION

{$R *.DFM}


procedure TFrmShowFiles.PageControlChange(Sender: TObject);
begin
  FormResize(self);
end;

procedure TFrmShowFiles.FormResize(Sender: TObject);
begin
  if pageControl.ActivePage = tabSheet3 then
    grdCreates.ColWidths[0]:= grdCreates.Width - 5
  else if pageControl.ActivePage = tabSheet2 then
    grdDelFiles.ColWidths[0]:= grdDelFiles.Width - 5
  else
  begin
    grdCopyFiles.ColWidths[0]:= (grdCopyFiles.Width div 2) - 11;
    grdCopyFiles.ColWidths[1]:= (grdCopyFiles.Width div 2) - 11;
  end;
end;

procedure TFrmShowFiles.FormCreate(Sender: TObject);
var
  hMenu : THandle;
begin
  // Remove system menu
  hMenu:= GetSystemMenu(Handle, False);
  DeleteMenu(hMenu, SC_MINIMIZE, MF_BYCOMMAND);
end;

procedure TFrmShowFiles.FormShow(Sender: TObject);
begin
  pageControl.ActivePage:= tabSheet1;
end;

END.
