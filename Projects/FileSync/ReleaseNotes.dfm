object FrmRelNotes: TFrmRelNotes
  Left = 381
  Top = 150
  BorderIcons = [biSystemMenu, biMaximize]
  Caption = 'FileSync - Release Notes'
  ClientHeight = 382
  ClientWidth = 483
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = True
  Position = poScreenCenter
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Memo1: TMemo
    Left = 0
    Top = 0
    Width = 483
    Height = 341
    Align = alClient
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Lucida Console'
    Font.Style = []
    Lines.Strings = (
      'File Sync'
      'Release History'
      '============================================================='
      '4.00 -- 8/28/2009'
      '* Converted to unicode'
      '* Added Exclude Folders option'
      '* Added external source compare utility'
      ''
      '2.00 -- 12/12/2003'
      '* Added include /exclude masks'
      '* Added ability to save and restore options to a disk file.'
      ''
      '1.13 ---'
      ''
      '1.12'
      '* Added menus to all the screens.'
      ''
      '* Fixed bug caused by passing an empty string to'
      '  shFileOperation in Win2000.'
      ''
      '1.11'
      '* Added FOF_FILEUNDO to the shFileOperation for Delete. This'
      '  should put any deleted files in the recycle bin (as long'
      '  as the files are not deleted from a removable media).'
      ''
      '1.10'
      '* Fixed a bug where the sanity check window was reporting'
      '  the file delete list in the Create grid.'
      ''
      '1.9'
      '* Whew. Finally found that elusive bug that would not catch'
      '  all files needing to be deleted.  This bug would of made'
      '  you glad you didn'#39't pay for this product.'
      ''
      '1.8'
      '* Added DropFile support for the two file name combo boxes.'
      ''
      '* Fixed a bug that caused incorrect action types (when using'
      '  Byte compare if file size same'
      ''
      '* Fixed that damn label text (finally) in the options tab.'
      ''
      '* Added a new sync option - Super Sync. This will do a byte'
      '  compare if the Date/Time/Size of the files match.'
      ''
      '1.7'
      '* Updated for Delphi 4.  Fixed an error with the thread/state/'
      '  timer model that I was using.  All should be well now.'
      ''
      '1.6'
      '* OK, That delete bug should now be fixed.'
      ''
      '1.5'
      '* Fixed a bug that was occuring when creating empty folders.'
      '  The SHFileOperation was not enjoying this feature so I added'
      '  some code that will generate the empty folders outside of'
      '  the SHFileOperation. The sanity check window now reflects'
      '  these new "Empty" folders in it'#39's list.'
      '  Still pending is a problem when deleting folders that have'
      '  child folders. Currently they refuse to be removed.'
      '  This will be fixed in 1.6'
      ''
      '1.4'
      '* Changed the order of processing to perform delete actions '
      
        '  before performing the Copy actions. (thanks to bill for this o' +
        'ne)'
      ''
      '1.3'
      '* Fixed a bug that caused the sanity check window to not show'
      '  any files to delete when the folder action was OverwriteNewer.'
      '* Changed release history window to have a read-only edit box'
      '  (thanks to Kevin-the-tester for this one)'
      ''
      '1.2   01/27/1998'
      '* Added print capability to Source compare.'
      '* Added display width to the source compare options.'
      '* Changed tab order of main screen.'
      ''
      '1.1   01/15/1998'
      '* Added support for user profiles.'
      '* Note: The old registry keys should be deleted before running'
      '        this ver.'
      '        Remove,'
      '        HKEY_CURRENT_USER\Software\Sliq Systems\FileSync'
      ''
      '1.0   11/30/1997'
      'Initial release')
    ParentFont = False
    ReadOnly = True
    ScrollBars = ssBoth
    TabOrder = 0
    WordWrap = False
  end
  object Panel1: TPanel
    Left = 0
    Top = 341
    Width = 483
    Height = 41
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 1
    OnResize = Panel1Resize
    object BtnClose: TButton
      Left = 208
      Top = 8
      Width = 75
      Height = 25
      Cancel = True
      Caption = 'Close'
      TabOrder = 0
      OnClick = BtnCloseClick
    end
  end
end
