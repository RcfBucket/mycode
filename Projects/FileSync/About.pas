unit About;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, ExtCtrls, RCFLib;

type
  TFrmAbout = class(TForm)
    Image1: TImage;
    Label1: TLabel;
    lblCopyright: TLabel;
    Button1: TButton;
    LblVersion: TLabel;
    lblBuild: TLabel;
    Label5: TLabel;
    Bevel1: TBevel;
    BtnReleaseNotes: TButton;
    procedure FormShow(Sender: TObject);
    procedure BtnReleaseNotesClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

implementation

uses ReleaseNotes;

{$R *.DFM}

procedure TFrmAbout.FormShow(Sender: TObject);
var
  v : TVersionInfo;
begin
  v:= GetVersionInfo(ParamStr(0));
  lblBuild.Caption:= Format('Build %d', [v.Build]);
  lblVersion.Caption:= Format('Version %s', [v.VersionStr]);
  lblCopyright.Caption:= v.LegalCopyright;
end;

procedure TFrmAbout.BtnReleaseNotesClick(Sender: TObject);
begin
  with TFrmRelNotes.Create(application) do
  begin
    ShowModal;
    Free;
  end;
end;

end.
