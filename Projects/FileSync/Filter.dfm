object FrmFilter: TFrmFilter
  Left = 289
  Top = 131
  BorderStyle = bsDialog
  Caption = 'Filter Results'
  ClientHeight = 279
  ClientWidth = 269
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = True
  Position = poOwnerFormCenter
  ShowHint = True
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object grpFolders: TGroupBox
    Left = 8
    Top = 96
    Width = 161
    Height = 105
    Caption = 'Folder Actions'
    TabOrder = 1
    object BtnFolders: TSpeedButton
      Tag = 1
      Left = 136
      Top = 13
      Width = 17
      Height = 17
      Hint = 'Check/Uncheck all'
      Caption = 'X'
      Flat = True
      OnClick = BtnFilesClick
    end
    object chkUpdate: TCheckBox
      Left = 8
      Top = 37
      Width = 97
      Height = 17
      Caption = 'Update'
      TabOrder = 1
    end
    object chkOverwriteFolder: TCheckBox
      Left = 8
      Top = 80
      Width = 121
      Height = 17
      Caption = 'Overwrite newer files'
      TabOrder = 3
    end
    object chkCreate: TCheckBox
      Left = 8
      Top = 16
      Width = 97
      Height = 17
      Caption = 'Create'
      TabOrder = 0
    end
    object chkDeleteFolder: TCheckBox
      Left = 8
      Top = 59
      Width = 97
      Height = 17
      Caption = 'Delete'
      TabOrder = 2
    end
  end
  object grpFiles: TGroupBox
    Left = 8
    Top = 8
    Width = 161
    Height = 81
    Caption = 'File Actions'
    TabOrder = 0
    object BtnFiles: TSpeedButton
      Tag = 1
      Left = 136
      Top = 13
      Width = 17
      Height = 17
      Hint = 'Check/Uncheck all'
      Caption = 'X'
      Flat = True
      OnClick = BtnFilesClick
    end
    object chkFileCopy: TCheckBox
      Left = 8
      Top = 16
      Width = 97
      Height = 17
      Caption = 'Copy'
      TabOrder = 0
    end
    object chkFileDelete: TCheckBox
      Left = 8
      Top = 36
      Width = 97
      Height = 17
      Caption = 'Delete'
      TabOrder = 1
    end
    object chkOverwriteFile: TCheckBox
      Left = 8
      Top = 56
      Width = 97
      Height = 17
      Caption = 'Overwrite newer'
      TabOrder = 2
    end
  end
  object BtnOK: TBitBtn
    Left = 180
    Top = 56
    Width = 81
    Height = 27
    Caption = 'OK'
    Default = True
    Glyph.Data = {
      DE010000424DDE01000000000000760000002800000024000000120000000100
      0400000000006801000000000000000000001000000010000000000000000000
      80000080000000808000800000008000800080800000C0C0C000808080000000
      FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
      3333333333333333333333330000333333333333333333333333F33333333333
      00003333344333333333333333388F3333333333000033334224333333333333
      338338F3333333330000333422224333333333333833338F3333333300003342
      222224333333333383333338F3333333000034222A22224333333338F338F333
      8F33333300003222A3A2224333333338F3838F338F33333300003A2A333A2224
      33333338F83338F338F33333000033A33333A222433333338333338F338F3333
      0000333333333A222433333333333338F338F33300003333333333A222433333
      333333338F338F33000033333333333A222433333333333338F338F300003333
      33333333A222433333333333338F338F00003333333333333A22433333333333
      3338F38F000033333333333333A223333333333333338F830000333333333333
      333A333333333333333338330000333333333333333333333333333333333333
      0000}
    NumGlyphs = 2
    TabOrder = 3
    OnClick = BtnOKClick
  end
  object BtnCancel: TBitBtn
    Left = 180
    Top = 88
    Width = 81
    Height = 27
    Kind = bkCancel
    NumGlyphs = 2
    TabOrder = 4
  end
  object GrpOther: TGroupBox
    Left = 8
    Top = 208
    Width = 161
    Height = 61
    Caption = 'Other Actions'
    TabOrder = 2
    object BtnOther: TSpeedButton
      Tag = 1
      Left = 136
      Top = 13
      Width = 17
      Height = 17
      Hint = 'Check/Uncheck all'
      Caption = 'X'
      Flat = True
      OnClick = BtnFilesClick
    end
    object ChkForceNochange: TCheckBox
      Left = 8
      Top = 36
      Width = 145
      Height = 17
      Caption = 'User forced No Change'
      TabOrder = 1
    end
    object chkNoChange: TCheckBox
      Left = 8
      Top = 16
      Width = 97
      Height = 17
      Caption = 'No Change'
      TabOrder = 0
    end
  end
  object BtnDefaults: TBitBtn
    Left = 180
    Top = 16
    Width = 81
    Height = 27
    Caption = '&Defaults'
    Glyph.Data = {
      42010000424D4201000000000000760000002800000011000000110000000100
      040000000000CC00000000000000000000001000000010000000000000000000
      BF0000BF000000BFBF00BF000000BF00BF00BFBF0000C0C0C000808080000000
      FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00777777777777
      77777000000070000000000007777000000070FFFFFFFFFF07777000000070FC
      CFCCCCCF07777000000070FFFFFFFFFF07777000000070FCCFCCCCCF07777000
      000070FFFFFFFFFF07777000000070FFFFFFF0FF07777000000070F00FFF0B0F
      07770000000070F0F0F0B0F000700000000070FF0B0B0F0FBF00000000007000
      00F0F0FBFBF0000000007777770B0FBFBFB00000000077777770FBFBFB000000
      0000777777770000007000000000777777777777777770000000777777777777
      777770000000}
    TabOrder = 5
    OnClick = BtnDefaultsClick
  end
end
