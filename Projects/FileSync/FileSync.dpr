program FileSync;

uses
  Forms,
  Main in 'Main.pas' {FrmMain},
  Analyze in 'Analyze.pas' {FrmAnalyze},
  SyncData in 'SyncData.pas',
  Results in 'Results.pas' {FrmResults},
  Report in 'Report.pas' {FrmReport},
  Filter in 'Filter.pas' {FrmFilter},
  FileDetail in 'FileDetail.pas' {FrmFileDetail},
  FolderDetail in 'FolderDetail.pas' {FrmFolderDetail},
  SrcCompare in 'SrcCompare.pas' {FrmSourceCompare},
  BuildForm in 'BuildForm.pas' {FrmBuild},
  SrcCompOpts in 'SrcCompOpts.pas' {FrmSrcCompOpts},
  About in 'About.pas' {FrmAbout},
  Sanity in 'Sanity.pas' {FrmShowFiles},
  GenReport in 'GenReport.pas' {FrmGenReport},
  Globals in 'Globals.pas',
  ProfileEditor in 'ProfileEditor.pas' {FrmProfiles},
  ReleaseNotes in 'ReleaseNotes.pas' {FrmRelNotes},
  MaskEdit in 'MaskEdit.pas' {FrmMaskEdit},
  RCFLib in '..\..\Lib\RCFLib.pas';

{$R *.RES}

begin
  Application.Initialize;
  Application.Title := 'File Sync';
  Application.CreateForm(TFrmMain, FrmMain);
  Application.Run;
end.
