unit SCM;

interface

uses
  Classes, SysUtils;

type
  TSCM = class(TComponent)
  private
    FFileName1, FFileName2: string;
    FFileDate1, FFileDate2: TDateTime;
    FDisplayText1, FDisplayText2 : TStringList;
    FCompareText1, FCompareText2 : TStringList;
    FDiffText : TStringList;
    FCurPos1, FCurPos2, FMatchPos1, FMatchPos2 : integer;
    FMinLinesForMatch : integer;
    FOptimize: boolean;
    FConvertTabs : boolean;
    FTabSize : integer;
    FIgnoreCase : boolean;
    FIgnoreWhiteSpace : boolean;
    FUseLineNumbers : boolean;
    FCharsToIgnore : string;
    FStringToIgnore: string;
    FShowFileInfo: boolean;
    FDisplayWidth: integer;
    FNumDiffs: integer;
    TabSpaces: string;
    function PadStr(const inStr: string; PadChar: char; PadLen: integer): string;
    function ExpandTabs(const Source: String): string;
    procedure PreprocessStrings;
    procedure ReSynch;
    procedure DumpDiff;
    function GetDiffCount: integer;
    function GetDiffText(strNo: integer): string;
  public
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
    procedure AssignDiffsTo(Source: TPersistent);
    procedure Execute;
    property DiffCount: integer read GetDiffCount;
    property DiffText[strNo: integer]: string read GetDiffText;
    property NumDiffs: integer read FNumDiffs;
  published
    property FileName1: string read FFileName1 write FFileName1;
    property FileName2: string read FFileName2 write FFileName2;
    property MinLinesForMatch: integer read FMinLinesForMatch write FMinLinesForMatch;
    property ConvertTabs: boolean read FConvertTabs write FConvertTabs;
    property TabSize: integer read FTabSize write FTabSize;
    property IgnoreCase: boolean read FIgnoreCase write FIgnoreCase;
    property IgnoreWhiteSpace: boolean read FIgnoreWhiteSpace write FIgnoreWhiteSpace;
    property UseLineNumbers: boolean read FUseLineNumbers write FUseLineNumbers;
    property CharsToIgnore: string read FCharsToIgnore write FCharsToIgnore;
    property StringToIgnore: string read FStringToIgnore write FStringToIgnore;
    property ShowFileInfo: boolean read FShowFileInfo write FShowFileInfo;
    property DisplayWidth: integer read FDisplayWidth write FDisplayWidth;
    property Optimize: boolean read FOptimize write FOptimize;
  end;

procedure Register;

implementation

uses
  Forms, Controls;

function TSCM.PadStr(const inStr: string; PadChar: char; PadLen: integer): string;
var
  tInt: integer;
  inLen: integer;
begin
  inLen:= Length(inStr);
  Result:= inStr;
  for tInt:= inLen+1 to PadLen do
    Result:= Result + PadChar;
end;

function TSCM.ExpandTabs(const Source: String): string;
var
	i	: Integer;
	tStr : string;

begin
  if FConvertTabs then
  begin
    tStr:= '';
    for i:= 1 to Length(Source) do
      If Source[i] = Chr(9) then
      begin
        if (FTabSize > 0) then
          begin
            tStr:= Concat(tStr, TabSpaces);
            tStr:= Copy(tStr,1,((Length(tStr) Div FTabSize) * FTabSize));
          end;
      end
      else
        tStr:= Concat(tStr, Source[i]);
  end
  else
    tstr:= source;
	Result:= tStr;
end;

procedure TSCM.PreprocessStrings;

  procedure DoIt(aCompareStringList, aDisplayStringList: TStringList);
  var
    strNo: integer;
    chrNo: integer;
    len: integer;
    tStr: string;
    curChar: char;
  begin
    strNo:= 0;
    while strNo < aCompareStringList.Count do
    begin
      if FIgnoreCase then
        aCompareStringList[strNo]:= AnsiUpperCase(aCompareStringList[strNo]);
      if (FCharsToIgnore <> '') or (FIgnoreWhiteSpace) then
      begin
        tStr:= '';
        len:= Length(aCompareStringList[strNo]);
        for chrNo:= 1 to len do
        begin
          curChar:= aCompareStringList[strNo][chrNo];
          if  ((not FIgnoreWhiteSpace) or (FIgnoreWhiteSpace and (Ord(curChar) > 32))) and
              (AnsiPos(curChar, FCharsToIgnore) = 0) then
                tStr:= tStr + curChar;
        end;
        aCompareStringList[strNo]:= tStr;
      end;
      if (FStringToIgnore <> '') and (AnsiPos(FStringToIgnore, aCompareStringList[strNo]) > 0) then
      begin
        aCompareStringList.Delete(strNo);
        aDisplayStringList.Delete(strNo);
      end
      else
        Inc(strNo);
    end;
  end;

begin
  if FIgnoreCase then
    FCharsToIgnore:= AnsiUpperCase(FCharsToIgnore);

  DoIt(FCompareText1, fDisplayText1);
  DoIt(FCompareText2, fDisplayText2);
end;

procedure TSCM.ReSynch;
var
  t1m1, t1m2, t2m1, t2m2 : integer;

  procedure ResynchOneWay(var lFMatchPos1,lFMatchPos2: integer; lFCurPos1, lFCurPos2 : integer; lFCompareText1, lFCompareText2 : TStringList);
  var
    MatchLines            : integer;
    tPos1, tPos2          : integer;
  begin
    tPos1:= lFCurPos1;
    lFMatchPos1:= lFCompareText1.Count;
    lFMatchPos2:= lFCompareText2.Count;
    MatchLines:= 0;
    { while not eof (left file) and not enough matching lines found }
    While (tPos1 < lFCompareText1.Count) and (MatchLines < FMinLinesForMatch) do
    begin
      tPos2:= lFCurPos2;
      MatchLines:= 0;
      { while not eof (either file) and not enough matching lines found }
      While (tPos1 < lFCompareText1.Count) and (tPos2 < lFCompareText2.Count) and (MatchLines < FMinLinesForMatch) do
      begin
        if (lFCompareText1[tPos1] = lFCompareText2[tPos2]) then
        begin
          Inc(MatchLines);
          { mark first line of matching "block" }
          if (MatchLines = 1) then
          begin
            lFMatchPos1:= tPos1;
            lFMatchPos2:= tPos2;
          end;
          Inc(tPos1);
        end
        { if working on matching "block", and non-matching line encountered, reset variables }
        else if (MatchLines > 0) then
        begin
          MatchLines:= 0;
          tPos1:= lFMatchPos1;
          tPos2:= lFMatchPos2;
          lFMatchPos1:= lFCompareText1.Count;
          lFMatchPos2:= lFCompareText2.Count;
        end;
        Inc(tPos2);
      end;
      Inc(tPos1);
    end;
  end;

begin
  {calculate resynch positions (left to right) }
  ResynchOneWay(t1m1, t1m2, FCurPos1, FCurPos2, FCompareText1, FCompareText2);

  {calculate resynch positions (right to left) }
  ResynchOneWay(t2m2, t2m1, FCurPos2, FCurPos1, FCompareText2, FCompareText1);

  { figure out which resynch direction was better and use that one}
  if (t1m1+t1m2 <= t2m1+t2m2) then
  begin
    FMatchPos1:= t1m1;
    FMatchPos2:= t1m2;
  end
  else
  begin
    FMatchPos1:= t2m1;
    FMatchPos2:= t2m2;
  end;
end;

procedure TSCM.DumpDiff;
var
  tStr: string;
begin
  if (FCurPos1 <> FMatchPos1) or (FCurPos2 <> FMatchPos2) then
  begin
    Inc(FNumDiffs);
    FDiffText.Add('');
    tStr:= '';
    if FShowFileInfo then
      tStr:= '** ' + DateTimeToStr(FFileDate1) + '  ' + FFileName1 + ' ';
    tStr:= PadStr(tStr, '*', FDisplayWidth);
    FDiffText.Add(Copy(tStr, 1, FDisplayWidth));
    while (FCurPos1 <> FMatchPos1) do
    begin
      tStr:= '|';
      if FUseLineNumbers then
        tStr:= tStr + Format('%6d |',[integer(FDisplayText1.Objects[FCurPos1])]); // line number stored in object pointer
      tStr:= tStr + ExpandTabs(FDisplayText1[FCurPos1]);
      FDiffText.Add(Copy(tStr, 1, FDisplayWidth));
      Inc(FCurPos1);
    end;
    tStr:= '';
    if FShowFileInfo then
      tStr:= '** ' + DateTimeToStr(FFileDate2) + '  ' + FFileName2 + ' ';
    tStr:= PadStr(tStr, '*', FDisplayWidth);
    FDiffText.Add(Copy(tStr, 1, FDisplayWidth));
    while (FCurPos2 <> FMatchPos2) do
    begin
      tStr:= '|';
      if FUseLineNumbers then
        tStr:= tStr + Format('%6d |',[integer(FDisplayText2.Objects[FCurPos2])]); // line number stored in object pointer
      tStr:= tStr + ExpandTabs(FDisplayText2[FCurPos2]);
      FDiffText.Add(Copy(tStr, 1, FDisplayWidth));
      Inc(FCurPos2);
    end;
    tStr:= '';
    tStr:= PadStr(tStr, '*', FDisplayWidth);
    FDiffText.Add(Copy(tStr, 1, FDisplayWidth));
  end;
end;

function TSCM.GetDiffCount: integer;
begin
  Result:= FDiffText.Count;
end;

function TSCM.GetDiffText(strNo: integer): string;
begin
  Result:= FDiffText[strNo];
end;

procedure TSCM.Execute;
var
  FileRec: TSearchRec;
  Done: boolean;
  TempMinLines: integer;
  TempDiffText: TStringList;
  TempNumDiffs: integer;
  lineNum: integer;
begin
  Screen.Cursor:= crHourglass;
  TempMinLines:= FMinLinesForMatch;
  TempDiffText:= TStringList.Create;
  TempNumDiffs:= 0;
  try
    FindFirst(FFileName1, faAnyFile And Not (faDirectory), FileRec);
    FFileDate1:= FileDateToDateTime(FileRec.Time);
    FindClose(FileRec);
    FindFirst(FFileName2, faAnyFile And Not (faDirectory), FileRec);
    FFileDate2:= FileDateToDateTime(FileRec.Time);
    FindClose(FileRec);
    FDisplayText1.Clear;
    FDisplayText2.Clear;
    FCompareText1.Clear;
    FCompareText2.Clear;
    FDisplayText1.LoadFromFile(FFileName1);
    // Add original line numbers to the stringlist
    for lineNum:= 1 to fDisplayText1.Count do
    begin
      fDisplayText1.Objects[lineNum-1]:= TObject(lineNum);
    end;
    FDisplayText2.LoadFromFile(FFileName2);
    // Add original line numbers to the stringlist
    for lineNum:= 1 to fDisplayText2.Count do
    begin
      fDisplayText2.Objects[lineNum-1]:= TObject(lineNum);
    end;
    FCompareText1.Assign(FDisplayText1);
    FCompareText2.Assign(FDisplayText2);
    PreprocessStrings;
    TabSpaces:= '';
    TabSpaces:= PadStr(TabSpaces, ' ', FTabSize);
    if Optimize then
      FMinLinesForMatch:= 1;
    repeat
      FDiffText.Clear;
      FNumDiffs:= 0;
      FCurPos1:= 0;
      FCurPos2:= 0;
      While (FCurPos1 < FCompareText1.Count) and (FCurPos2 < FCompareText2.Count) do
      begin
        if (FCompareText1[FCurPos1] = FCompareText2[FCurPos2]) then
        begin
          Inc(FCurPos1);
          Inc(FCurPos2);
        end
        else
        begin
          ReSynch;
          DumpDiff;
        end;
      end;
      FMatchPos1:= FCompareText1.Count;
      FMatchPos2:= FCompareText2.Count;
      DumpDiff;
      Done:= True;
      if Optimize then
      begin
        // Compare ACTUAL difference size (DiffText minus the extra 4 context and blank lines per difference)
        if (FMinLinesForMatch = 1) or ((FDiffText.Count - (fNumDiffs*4)) < (TempDiffText.Count - (TempNumDiffs*4))) then
        begin
          TempDiffText.Clear;
          TempDiffText.Assign(FDiffText);
          TempNumDiffs:= FNumDiffs;
          Inc(FMinLinesForMatch);
          Done:= False;
        end
        else
        begin
          FDiffText.Clear;
          FDiffText.Assign(TempDiffText);
          FNumDiffs:= TempNumDiffs;
        end;
      end;
    until Done;
  finally
    Screen.Cursor:= crDefault;
    TempDiffText.Free;
    FMinLinesForMatch:= TempMinLines;
  end;
end;

constructor TSCM.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
  FDisplayText1:= TStringList.Create;
  FDisplayText2:= TStringList.Create;
  FCompareText1:= TStringList.Create;
  FCompareText2:= TStringList.Create;
  FDiffText:= TStringList.Create;
  FMinLinesForMatch:= 3;
  FConvertTabs:= True;
  FTabSize:= 8;
  FIgnoreCase:= False;
  FIgnoreWhiteSpace:= False;
  FUseLineNumbers:= True;
  FCharsToIgnore:= '';
  FStringToIgnore:= '';
  FShowFileInfo:= True;
  FDisplayWidth:= 128;
  FOptimize:= False;
  FNumDiffs:= 0;
end;

destructor TSCM.Destroy;
begin
  FDisplayText1.Free;
  FDisplayText2.Free;
  FCompareText1.Free;
  FCompareText2.Free;
  FDiffText.Free;
  inherited Destroy;
end;

procedure TSCM.AssignDiffsTo(Source: TPersistent);
begin
  Source.Assign(FDiffText);
end;

procedure Register;
begin
  RegisterComponents('Custom', [TScm]);
end;

end.
