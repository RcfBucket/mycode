unit Results;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, SyncData, Grids, ComCtrls, ExtCtrls, Buttons,
  EllipsisLabel, Menus, ToolWin, SCM, Globals, ActnList, ImgList, SrcCompOpts, ProcessLib;

type
  TFrmResults = class(TForm)
    Panel4: TPanel;
    Label1: TLabel;
    Label2: TLabel;
    mnuFilePop: TPopupMenu;
    Details1: TMenuItem;
    Nochangeitem1: TMenuItem;
    UndoForceNoChange1: TMenuItem;
    LblDestBase: TEllipsisLabel;
    LblSourceBase: TEllipsisLabel;
    mnuFolderPop: TPopupMenu;
    ShowDetails1: TMenuItem;
    ForceNoChange1: TMenuItem;
    UndoForceNoChange2: TMenuItem;
    N1: TMenuItem;
    Sourcecompare1: TMenuItem;
    ToolBar1: TToolBar;
    BtnGo: TToolButton;
    BtnSourceCompare: TToolButton;
    BtnFilter: TToolButton;
    BtnReport: TToolButton;
    BtnDetails: TToolButton;
    BtnForceNoChange: TToolButton;
    BtnUndoNoChange: TToolButton;
    BtnClose: TToolButton;
    ToolButton9: TToolButton;
    ToolButton10: TToolButton;
    ImageList1: TImageList;
    ActionList1: TActionList;
    ActGO: TAction;
    ActSourceCompare: TAction;
    ActFilter: TAction;
    ActReport: TAction;
    ActDetails: TAction;
    ActForceNoChange: TAction;
    ActUndoNoChange: TAction;
    ActClose: TAction;
    PageControl: TPageControl;
    TabFolders: TTabSheet;
    TabFiles: TTabSheet;
    lvFolders: TListView;
    Panel2: TPanel;
    Label3: TLabel;
    Label4: TLabel;
    LblSource: TEllipsisLabel;
    LblDest: TEllipsisLabel;
    lvFiles: TListView;
    MainMenu1: TMainMenu;
    File1: TMenuItem;
    ActGO1: TMenuItem;
    SourceCompare2: TMenuItem;
    ActReport1: TMenuItem;
    ShowDetails2: TMenuItem;
    Action1: TMenuItem;
    ForceNoChange2: TMenuItem;
    UndoForceNoChange3: TMenuItem;
    View1: TMenuItem;
    N2: TMenuItem;
    ActClose1: TMenuItem;
    N3: TMenuItem;
    ActFilter1: TMenuItem;
    TabDebug: TTabSheet;
    Button1: TButton;
    BtnSrcFiles: TButton;
    udSrcFileIdx: TUpDown;
    edSrcFileIdx: TEdit;
    BtnMergedFiles: TButton;
    BtnDstFiles: TButton;
    edDstFileIdx: TEdit;
    udDstFileIdx: TUpDown;
    edMergeIdx: TEdit;
    udMergeIdx: TUpDown;
    procedure FormShow(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure mnuFilePopPopup(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure lvFoldersEditing(Sender: TObject; Item: TListItem; var AllowEdit: Boolean);
    procedure lvFoldersKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure lvFoldersDblClick(Sender: TObject);
    procedure lvFilesDblClick(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure BtnGOClick(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure ActGOExecute(Sender: TObject);
    procedure ActSourceCompareExecute(Sender: TObject);
    procedure ActFilterExecute(Sender: TObject);
    procedure ActReportExecute(Sender: TObject);
    procedure ActDetailsExecute(Sender: TObject);
    procedure ActForceNoChangeExecute(Sender: TObject);
    procedure ActUndoNoChangeExecute(Sender: TObject);
    procedure ActCloseExecute(Sender: TObject);
    procedure PageControlChanging(Sender: TObject; var AllowChange: Boolean);
    procedure BtnSrcFilesClick(Sender: TObject);
    procedure BtnDstFilesClick(Sender: TObject);
    procedure BtnMergedFilesClick(Sender: TObject);
  private
    procedure ClearGrid(filesOnly: Boolean);
    procedure FillFolderGrid;
    procedure FillFileGrid;
    function FileIndex(Item: TListItem): integer;
    function FolderIndex(Item: TListItem): integer;
    procedure NoChangeSelected(theAct: TSyncAction);
  public
    sync: TSync;
  end;

implementation

uses Report, Filter, FileDetail, FolderDetail, SrcCompare;
{$R *.DFM}
(* const
  FilesInSource = 'Files in source %s';
  FilesInDest   = 'Files in destination %s'; *)

procedure TFrmResults.ClearGrid(filesOnly: Boolean);
begin
  if not filesOnly then
  begin
    // clear folder grid
    lvFolders.Clear;
  end;
  LblSource.caption:= '';
  LblDest.caption:= '';
  lvFiles.Clear; // clear file grid
end;

procedure TFrmResults.FormShow(Sender: TObject);
begin
  LblSourceBase.caption:= sync.Source.BaseFolder;
  LblDestBase.caption:= sync.Dest.BaseFolder;

  PageControl.ActivePage:= TabFolders;

  FillFolderGrid;
  lvFolders.ItemFocused:= lvFolders.Items[0];
  lvFolders.Selected:= lvFolders.Items[0];
  ReadFormPosition(self);
end;

procedure TFrmResults.FillFolderGrid;
var
  k: integer;
  s: TSyncFolder;
  d: TSyncFolder;
  i: TListItem;
  act: TSyncAction;
begin
  screen.Cursor:= crHourGlass;
  try
    ClearGrid(false);
    if sync.MergedFolderCount > 0 then
    begin
      for k:= 0 to sync.MergedFolderCount - 1 do
      begin
        s:= sync.GetSyncSourceFolder(k);
        d:= sync.GetSyncDestFolder(k);
        act:= sync.FolderAction[k];
        if CheckFolderFilter(act) then
        begin
          // TListItem
          i:= lvFolders.Items.Add;
          i.Data:= pointer(k); // data will equal the index of folder
          i.caption:= ActionStrs[act];
          if s <> nil then
            i.SubItems.Add(s.FolderName)
          else
            i.SubItems.Add('- - -');
          if d <> nil then
            i.SubItems.Add(d.FolderName)
          else
            i.SubItems.Add('- - -');
        end;
      end;
    end;
  finally
    screen.Cursor:= crDefault;
  end;
end;

procedure TFrmResults.FormResize(Sender: TObject);
begin
  LblSourceBase.Width:= Panel4.Width - LblSourceBase.left - 8;
  LblDestBase.Width:= Panel4.Width - LblSourceBase.left - 8;

  LblSource.Width:= Panel4.Width - LblSource.left - 8;
  LblDest.Width:= Panel4.Width - LblDest.left - 8;

end;

procedure TFrmResults.mnuFilePopPopup(Sender: TObject);
begin
  if PageControl.ActivePage = TabFolders then
  begin
    Details1.Enabled:= lvFolders.Items.Count > 0;
    Nochangeitem1.Enabled:= lvFolders.Items.Count > 0;
    UndoForceNoChange1.Enabled:= lvFolders.Items.Count > 0;
  end
  else
  begin
    Details1.Enabled:= lvFiles.Items.Count > 0;
    Nochangeitem1.Enabled:= lvFiles.Items.Count > 0;
    UndoForceNoChange1.Enabled:= lvFiles.Items.Count > 0;
  end;
end;

procedure TFrmResults.FillFileGrid;
var
  listItem: TListItem;
  k: integer;
  folderIdx: integer;
  s: TSyncFile;
  d: TSyncFile;
  action: TSyncAction;
  tstr: string;
begin
  ClearGrid(true); // clear file grid.
  LblSource.caption:= '';
  LblDest.caption:= '';

  listItem:= lvFolders.ItemFocused;
  if listItem <> nil then
  begin
    folderIdx:= FolderIndex(listItem);

    LblSource.caption:= listItem.SubItems[0];
    LblDest.caption:= listItem.SubItems[1];

    // fill up file listview with data for selected folder
    lvFiles.Items.Clear;
    if sync.MergedFileCount[folderIdx] > 0 then
    begin
      lvFiles.Items.BeginUpdate;
      for k:= 0 to sync.MergedFileCount[folderIdx] - 1 do
      begin
        action:= sync.FileAction[folderIdx, k];
        if CheckFileFilter(action) then
        begin
          listItem:= lvFiles.Items.Add;
          listItem.Data:= pointer(k); // set folder index in data
          listItem.caption:= ActionStrs[action];
          s:= sync.GetSyncSourceFile(folderIdx, k);
          if s <> nil then
          begin
            listItem.SubItems.Add(s.FileName);
            listItem.SubItems.Add(FormatFloat('#,##0', s.FileSize));
            listItem.SubItems.Add(FormatDateTime('mm/dd/yy  hh:nn:ss a/p', s.FileDate));
          end
          else
          begin
            listItem.SubItems.Add('- - -');
            listItem.SubItems.Add('- - -');
            listItem.SubItems.Add('- - -');
          end;
          if action = actDelete then
            tstr:= 'X'
          else if action in [actCopy, actOverwriteNewer] then
            tstr:= '-->'
          else
            tstr:= '';
          listItem.SubItems.Add(tstr);
          d:= sync.GetSyncDestFile(folderIdx, k);
          if d <> nil then
          begin
            listItem.SubItems.Add(d.FileName);
            listItem.SubItems.Add(FormatFloat('#,##0', d.FileSize));
            listItem.SubItems.Add(FormatDateTime('mm/dd/yy  hh:nn:ss a/p', d.FileDate));
          end
          else
          begin
            listItem.SubItems.Add('- - -');
            listItem.SubItems.Add('- - -');
            listItem.SubItems.Add('- - -');
          end;
        end;
      end;
      lvFiles.Items.EndUpdate;
      lvFiles.ItemFocused:= lvFiles.Items[0];
      lvFiles.Selected:= lvFiles.Items[0];
    end;
  end;
end;

procedure TFrmResults.FormCreate(Sender: TObject);
var
  hMenu: THandle;
begin
  // Remove system menu
  hMenu:= GetSystemMenu(Handle, false);
  DeleteMenu(hMenu, SC_MINIMIZE, MF_BYCOMMAND);

  lvFiles.Clear;
  lvFolders.Clear;
{$IFNDEF DEBUG}
  TabDebug.TabVisible:= false;
{$ENDIF}
end;

procedure TFrmResults.lvFoldersEditing(Sender: TObject; Item: TListItem; var AllowEdit: Boolean);
begin
  AllowEdit:= false; // disable inline-editing of ListView
end;

function TFrmResults.FileIndex(Item: TListItem): integer;
// return the proper index into the sync merge file list from the specified
// row in the file list view.  This is stored in the Data property of the file
// list items.
begin
  if Item <> nil then
    result:= integer(Item.Data)
  else
    result:= -1;
end;

function TFrmResults.FolderIndex(Item: TListItem): integer;
// return the proper index into the sync merge folders list from the specified
// row in the folder list.  This is stored in the Data property of the folder
// list items.
begin
  if Item <> nil then
    result:= integer(Item.Data)
  else
    result:= -1;
end;

procedure TFrmResults.NoChangeSelected(theAct: TSyncAction);
var
  liFold: TListItem;
  liFile: TListItem;
  foldIdx: integer;
  k: integer;

  procedure NoChangeFiles(aFoldIdx: integer);
  var
    j: integer;
  begin
    for j:= 0 to sync.MergedFileCount[aFoldIdx] - 1 do
    begin
      sync.FileAction[aFoldIdx, j]:= theAct;
    end;
  end;

begin
  Assert((theAct = actUserNoChanged) or (theAct = actUserUndoNochange), 'Invalid action passed to NoChangeSelected');

  if PageControl.ActivePage = TabFiles then
  begin
    liFold:= lvFolders.ItemFocused;
    if (lvFiles.SelCount > 0) and (liFold <> nil) then
    begin
      foldIdx:= FolderIndex(liFold);
      for k:= 0 to lvFiles.Items.Count - 1 do
      begin
        liFile:= lvFiles.Items[k];
        if liFile.Selected then
        begin
          sync.FileAction[foldIdx, FileIndex(liFile)]:= theAct;
          // liFile.Caption:= ActionStrs[sync.FileAction[foldIdx, FileIndex(liFile)]];
        end;
      end;
      FillFileGrid;
    end;
  end
  else
  begin
    if lvFolders.SelCount > 0 then
    begin
      for k:= 0 to lvFolders.Items.Count - 1 do
      begin
        liFold:= lvFolders.Items[k];
        if liFold.Selected then
        begin
          sync.FolderAction[FolderIndex(liFold)]:= theAct;
          liFold.caption:= ActionStrs[sync.FolderAction[FolderIndex(liFold)]];
          NoChangeFiles(FolderIndex(liFold));
        end;
      end;
      FillFolderGrid;
    end;
  end;
end;

procedure TFrmResults.lvFoldersKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
var
  pt: TPoint;
  k: integer;
begin
  if (Char(Key) = 'A') and (ssCtrl in Shift) then
  begin
    with Sender as TListView do
      for k:= 0 to Items.Count - 1 do
        Items[k].Selected:= true;
  end
  else if (ssAlt in Shift) and (Key = VK_F10) then
  begin
    pt:= ClientToScreen(Point(10, 125));
    mnuFilePop.Popup(pt.x, pt.y);
  end
  else if (Key = VK_RETURN) and (Shift = []) then
    lvFoldersDblClick(self);
end;

procedure TFrmResults.lvFoldersDblClick(Sender: TObject);
begin
  PageControl.SelectNextPage(true);
  // PageControl.ActivePage:= TabFiles;
end;

procedure TFrmResults.lvFilesDblClick(Sender: TObject);
begin
  ActSourceCompare.Execute;
end;

procedure TFrmResults.FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  if Key = VK_ESCAPE then
    Close;
end;

procedure TFrmResults.BtnGOClick(Sender: TObject);
begin
  if sync.ActionsExist then
  begin
    if sync.PerformSync(Handle) then
      Close;
  end
  else
    MessageDlg('No actions to perform.', mtInformation, [mbOK], 0);
end;

procedure TFrmResults.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin
  SaveFormPosition(self);
end;

procedure TFrmResults.ActGOExecute(Sender: TObject);
begin
  if sync.ActionsExist then
  begin
    if sync.PerformSync(Handle) then
      Close;
  end
  else
    MessageDlg('No actions to perform.', mtInformation, [mbOK], 0);
end;

procedure TFrmResults.ActSourceCompareExecute(Sender: TObject);
var
  src, dst: TSyncFolder;
  s, d: TSyncFile;
  folderIdx: integer;
  cmp, arg: string;
  proc: TProcess;
begin
  if PageControl.ActivePage = TabFiles then
  begin
    folderIdx:= FolderIndex(lvFolders.ItemFocused);
    if folderIdx >= 0 then
    begin
      src:= sync.GetSyncSourceFolder(folderIdx);
      dst:= sync.GetSyncDestFolder(folderIdx);
      if (src <> nil) and (dst <> nil) then
      begin
        s:= sync.GetSyncSourceFile(folderIdx, FileIndex(lvFiles.ItemFocused));
        d:= sync.GetSyncDestFile(folderIdx, FileIndex(lvFiles.ItemFocused));

        if UseExternalCompare then
        begin
          if (s <> nil) and (d <> nil) then
          begin
            ExternalCompareOpts(cmp, arg);
            proc:= TProcess.Create;
            proc.name:= cmp;
            arg:= StringReplace(arg, '%s', IncludeTrailingPathDelimiter(src.FolderName) + s.FileName, [rfIgnoreCase]);
            arg:= StringReplace(arg, '%d', IncludeTrailingPathDelimiter(dst.FolderName) + d.FileName, [rfIgnoreCase]);
            proc.CommandLine:= arg;
            proc.WaitForCompletion:= false;
            proc.Execute;
            proc.Free;
          end;
        end
        else
        begin
          if (s <> nil) and (d <> nil) then
          begin
            with TFrmSourceCompare.Create(self) do
            begin
              lblSrcName.caption:= s.FileName;
              lblSrcSize.caption:= FormatFloat('#,##0', s.FileSize);
              lblSrcDate.caption:= FormatDateTime('mm/dd/yy  hh:nn:ss a/p', s.FileDate);
              lblSrcPath.caption:= src.FolderName;

              lblDstName.caption:= d.FileName;
              lblDstSize.caption:= FormatFloat('#,##0', d.FileSize);
              lblDstDate.caption:= FormatDateTime('mm/dd/yy  hh:nn:ss a/p', d.FileDate);
              lblDstPath.caption:= dst.FolderName;
              ShowModal;
              Free;
            end;
          end;
        end;
      end;
    end;
  end;
end;

procedure TFrmResults.ActFilterExecute(Sender: TObject);
begin
  with TFrmFilter.Create(application) do
  begin
    if ShowModal = mrOK then
    begin
      FillFolderGrid;
      if lvFolders.Items.Count > 0 then
        lvFolders.ItemFocused:= lvFolders.Items[0];
      PageControl.ActivePage:= TabFolders;
      lvFolders.SetFocus;
    end;
    Free;
  end;
end;

procedure TFrmResults.ActReportExecute(Sender: TObject);
begin
  with TFrmReport.Create(application) do
  begin
    SyncPtr:= sync;
    ShowModal;
    Free;
  end;
end;

procedure TFrmResults.ActDetailsExecute(Sender: TObject);
var
  folderIdx: integer;
  s, d: TSyncFolder;

  function CountFiles(aFold: TSyncFolder; anAct: TSyncAction): integer;
  var
    k: integer;
    sum: integer;
  begin
    sum:= 0;
    for k:= 0 to aFold.FileCount - 1 do
    begin
      if aFold.Files[k].SyncAction = anAct then
        Inc(sum);
    end;
    result:= sum;
  end;

  function CountGlobalFiles(aFoldIdx: integer; anAct: TSyncAction): integer;
  // count number of files from merged folder list for anAct
  var
    k: integer;
    sum: integer;
  begin
    sum:= 0;
    for k:= 0 to sync.MergedFileCount[aFoldIdx] - 1 do
    begin
      if sync.FileAction[aFoldIdx, k] = anAct then
        Inc(sum);
    end;
    result:= sum;
  end;

begin
  if PageControl.ActivePage = TabFiles then
  begin
    if lvFolders.Selected <> nil then
    begin
      folderIdx:= FolderIndex(lvFolders.Selected);
      with TFrmFileDetail.Create(application) do
      begin
        s:= sync.GetSyncSourceFolder(folderIdx);
        d:= sync.GetSyncDestFolder(folderIdx);
        SrcPath:= s;
        DstPath:= d;
        SrcFile:= sync.GetSyncSourceFile(folderIdx, FileIndex(lvFiles.ItemFocused));
        DstFile:= sync.GetSyncDestFile(folderIdx, FileIndex(lvFiles.ItemFocused));
        action:= sync.FileAction[folderIdx, FileIndex(lvFiles.ItemFocused)];
        ShowModal;
        Free;
      end;
    end;
  end
  else
  begin
    if lvFolders.Selected <> nil then
    begin
      folderIdx:= FolderIndex(lvFolders.ItemFocused);
      with TFrmFolderDetail.Create(application) do
      begin
        lblAction.caption:= ActionStrs[sync.FolderAction[folderIdx]];
        s:= sync.GetSyncSourceFolder(folderIdx);
        if s <> nil then
        begin
          LblSource.caption:= s.FolderName;
          SetNum(lblSrcTotal, s.FileCount);
          SetNum(lblSrcCopied, CountFiles(s, actCopy));
          SetNum(lblSrcNewer, CountFiles(s, actOverwriteNewer));
        end;
        d:= sync.GetSyncDestFolder(folderIdx);
        if d <> nil then
        begin
          LblDest.caption:= d.FolderName;
          SetNum(lblDstTotal, d.FileCount);
          SetNum(lblDstDelete, CountFiles(d, actDelete));
          lblDstNewer.caption:= lblSrcNewer.caption;
        end;
        SetNum(lblUnchanged, CountGlobalFiles(folderIdx, actNoChange));
        SetNum(lblForced, CountGlobalFiles(folderIdx, actUserNoChanged));
        ShowModal;
        Free;
      end;
    end;
  end;
end;

procedure TFrmResults.ActForceNoChangeExecute(Sender: TObject);
begin
  NoChangeSelected(actUserNoChanged);
end;

procedure TFrmResults.ActUndoNoChangeExecute(Sender: TObject);
begin
  NoChangeSelected(actUserUndoNochange);

end;

procedure TFrmResults.ActCloseExecute(Sender: TObject);
begin
  Close;
end;

procedure TFrmResults.PageControlChanging(Sender: TObject; var AllowChange: Boolean);
begin
  AllowChange:= true;
  if PageControl.ActivePage = TabFolders then
  begin
    FillFileGrid;
  end;
end;

procedure TFrmResults.BtnSrcFilesClick(Sender: TObject);
(* var
  k   : integer;
  idx : integer;
  f   : TSyncFile; *)
begin
  (* grid.rows:= 0;
    idx:= udSrcFileIdx.Position;
    grid.Rows:= sync.Source.Folders[idx].FileCount;
    for k:= 0 to sync.Source.Folders[idx].FileCount-1 do
    begin
    f:= sync.Source.Folders[idx].Files[k];
    grid.Cell[1,k+1]:= f.FileName;
    grid.Cell[2,k+1]:= ActionStrs[f.SyncAction];
    grid.Cell[3,k+1]:= Format('%d', [f.FileSize]);
    grid.Cell[4,k+1]:= DateTimeToStr(f.FileDate);
    end; *)
end;

procedure TFrmResults.BtnDstFilesClick(Sender: TObject);
(*
  var
  k   : integer;
  idx : integer;
  f   : TSyncFile;
*)
begin
  (* grid.rows:= 0;
    idx:= udDstFileIdx.Position;
    grid.Rows:= sync.Dest.Folders[idx].FileCount;
    for k:= 0 to sync.Dest.Folders[idx].FileCount-1 do
    begin
    f:= sync.dest.Folders[idx].Files[k];
    grid.Cell[1,k+1]:= f.FileName;
    grid.Cell[2,k+1]:= ActionStrs[f.SyncAction];
    grid.Cell[3,k+1]:= Format('%d', [f.FileSize]);
    grid.Cell[4,k+1]:= DateTimeToStr(f.FileDate);
    end; *)
end;

procedure TFrmResults.BtnMergedFilesClick(Sender: TObject);
(*
  var
  idx : integer;
  k   : integer;
  action: TSyncAction;
  s, d: TSyncFile;
*)
begin
  (*
    grid.Rows:= 0;
    idx:= udMergeIdx.Position;

    grid.Rows:= sync.MergedFileCount[idx];
    for k:= 0 to sync.MergedFileCount[idx]-1 do
    begin
    action:= sync.FileAction[idx, k];
    grid.Cell[1,k+1]:= ActionStrs[action];

    s:= sync.GetSyncSourceFile(idx, k);
    if s <> nil then
    begin
    grid.Cell[2,k+1]:= s.FileName;
    end
    else
    begin
    grid.Cell[2,k+1]:= '- - -';
    end;

    d:= sync.GetSyncDestFile(idx, k);
    if d <> nil then
    begin
    grid.Cell[3,k+1]:= d.FileName;
    end
    else
    begin
    grid.Cell[3,k+1]:= '- - -';
    end;
    end;
  *)
end;

end.
