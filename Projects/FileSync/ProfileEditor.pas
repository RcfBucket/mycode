unit ProfileEditor;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, checklst, Globals, Registry, ComCtrls, ImgList, Math, Menus,
  ToolWin, ActnList, ExtCtrls;

type
  TFrmProfiles = class(TForm)
    tvProfiles: TTreeView;
    ImageList1: TImageList;
    ActionList1: TActionList;
    ActCancel: TAction;
    ActClean: TAction;
    ActCopy: TAction;
    ActEdit: TAction;
    ActRemove: TAction;
    ActNew: TAction;
    PopupMenu1: TPopupMenu;
    Copy2: TMenuItem;
    Edit2: TMenuItem;
    New2: TMenuItem;
    Remove2: TMenuItem;
    Panel1: TPanel;
    BtnClean: TButton;
    BtnCopy: TButton;
    BtnRename: TButton;
    BtnRemove: TButton;
    BtnAdd: TButton;
    BtnClose: TButton;
    ActOK: TAction;
    Button2: TButton;
    procedure FormCreate(Sender: TObject);
    procedure BtnRenameClick(Sender: TObject);
    procedure BtnCopyClick(Sender: TObject);
    procedure tvProfilesClick(Sender: TObject);
    procedure lvContentsEditing(Sender: TObject; Item: TListItem; var AllowEdit: Boolean);
    procedure Button1Click(Sender: TObject);
    procedure tvProfilesEdited(Sender: TObject; Node: TTreeNode; var S: String);
    procedure tvProfilesEditing(Sender: TObject; Node: TTreeNode; var AllowEdit: Boolean);
    procedure ActCancelExecute(Sender: TObject);
    procedure ActEditExecute(Sender: TObject);
    procedure ActCleanExecute(Sender: TObject);
    procedure ActCopyExecute(Sender: TObject);
    procedure ActRemoveExecute(Sender: TObject);
    procedure ActNewExecute(Sender: TObject);
    procedure PopupMenu1Popup(Sender: TObject);
    procedure tvProfilesDblClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure ActOKExecute(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    fEditing : boolean;
    function RemoveKey(aKey: string): boolean;
    function MoveKey(aOldKey, aNewKey: string; deleteOld: boolean): boolean;
    procedure MoveCopy(moving: boolean);
    procedure AddNewProfile(aProfileName: string);
  public
    { Public declarations }
  end;

var
  FrmProfiles: TFrmProfiles;

implementation

{$R *.DFM}

procedure TFrmProfiles.FormCreate(Sender: TObject);
var
  k   : integer;
  j   : integer;
  node: TTreeNode;
  subNode: TTreeNode;
  histNode: TTreeNode;
begin
  Constraints.MinHeight:= Height;
  Constraints.MinWidth:= Width;
  ActEdit.Enabled:= false;
  ActClean.Enabled:= true;
  ActCopy.Enabled:= false;
  ActRemove.Enabled:= false;
  ActNew.Enabled:= false;

  fEditing:= false;
  tvProfilesClick(self);

  tvProfiles.Items.Clear;
  for k:= 0 to profiles.Count - 1 do
  begin
    node:= tvProfiles.Items.Add(nil, profiles[k]);
    node.ImageIndex:= ifthen(k = 0, 0, 1);
    node.SelectedIndex:= node.ImageIndex;

    subNode:= tvProfiles.Items.AddChildObject(node, 'Source History', pointer(profiles.SrcHistory[k]));

    // now add source histories to tree
    for j:= 0 to profiles.SrcHistory[k].Count - 1 do
    begin
      histNode:= tvProfiles.Items.AddChild(subNode, profiles.SrcHistory[k][j]);
      histNode.ImageIndex:= 1;
      histNode.SelectedIndex:= 1;
    end;


    subNode:= tvProfiles.Items.AddChildObject(node, 'Destination History', pointer(profiles.DstHistory[k]));
    // now add destination histories to tree
    for j:= 0 to profiles.DstHistory[k].Count - 1 do
    begin
      histNode:= tvProfiles.Items.AddChild(subNode, profiles.DstHistory[k][j]);
      histNode.ImageIndex:= 1;
      histNode.SelectedIndex:= 1;
    end;
  end;
end;

function TFrmProfiles.RemoveKey(aKey: string): boolean;
var
  s   : TStringList;
  k   : integer;
  r   : TRegistry;
  isOK: boolean;
begin
  result:= false;
  s:= TStringList.Create;
  r:= TRegistry.Create;
  try
    isOK:= true;
    if r.OpenKey(aKey, false) then
    begin
      r.GetKeyNames(s);  // get any sub keys.
      k:= 0;
      while isOK and (k < s.count) do
      begin
        isOK:= RemoveKey(aKey + '\' + s[k]);
        Inc(k);
      end;
    end;
    r.CloseKey;
    if isOK then
    begin
      if r.DeleteKey(aKey) then
        result:= true;
    end;
  finally
    r.Free;
    s.Free;
  end;
end;

function TFrmProfiles.MoveKey(aOldKey, aNewKey: string; deleteOld: boolean): boolean;
var
  s   : TStringList;
  k   : integer;
  r   : TRegistry;
  isOK: boolean;
begin
  result:= false;
  s:= TStringList.Create;
  r:= TRegistry.Create;
  try
    isOK:= true;
    if r.OpenKey(aOldKey, false) then
    begin
      r.GetKeyNames(s);  // get any sub keys.
      k:= 0;
      while isOK and (k < s.count) do
      begin
        isOK:= MoveKey(aOldKey + '\' + s[k], aNewKey + '\' + s[k], deleteOld);
        Inc(k);
      end;
    end;
    r.CloseKey;
    if isOK then
    begin
      try
        r.MoveKey(aOldKey, aNewKey, deleteOld);
        if deleteOld then
          r.DeleteKey(aOldKey); // this is needed for NT for some reason???
        result:= true;
      except
        result:= false;
      end;
    end;
  finally
    r.Free;
    s.Free;
  end;
end;

procedure TFrmProfiles.MoveCopy(moving: boolean);
begin
end;

procedure TFrmProfiles.BtnRenameClick(Sender: TObject);
begin
  MoveCopy(true);
end;

procedure TFrmProfiles.BtnCopyClick(Sender: TObject);
begin
  MoveCopy(false);
end;

procedure TFrmProfiles.tvProfilesClick(Sender: TObject);
var
  node: TTreeNode;
begin
  node:= tvProfiles.Selected;
  if node <> nil then
  begin
    fEditing:= false;
    case node.Level of
      0:
      begin
        ActCopy.Enabled:= true;
        ActNew.Enabled:= true;
        if node.Index > 0 then
        begin
          ActEdit.Enabled:= true;
          ActRemove.Enabled:= true;
        end
        else  // default profile selected
        begin
          ActEdit.Enabled:= false;
          ActRemove.Enabled:= false;
        end;
      end;
      1:
      begin
        ActEdit.enabled:= false;
        ActCopy.Enabled:= false;
        ActNew.Enabled:= true;
        ActRemove.Enabled:= false;
      end;
      2:
      begin
        ActEdit.enabled:= true;
        ActCopy.Enabled:= true;
        ActNew.Enabled:= true;
        ActRemove.Enabled:= true;
      end;
    end;
  end;
end;

procedure TFrmProfiles.lvContentsEditing(Sender: TObject; Item: TListItem; var AllowEdit: Boolean);
begin
  allowEdit:= true;
end;

procedure TFrmProfiles.Button1Click(Sender: TObject);
var
  selNode : TTreeNode;
begin
  selNode:= tvProfiles.Selected;
  if selNode <> nil then
  begin
      tvProfiles.ReadOnly:= false;
      selNode.EditText;
  end;
end;

procedure TFrmProfiles.tvProfilesEdited(Sender: TObject; Node: TTreeNode; var S: String);
var
  idx : integer;
  hist: TStringList;
  histIdx: integer;
begin
  fEditing:= false;
  if Node.Level = 0 then
  begin
    // editing profile name
    idx:= node.index;
    profiles.ProfileFolders[idx]:= s; // assign new text to profile string
  end
  else if node.Level = 2 then
  begin
    // editing a history
    hist:= TStringList(node.Parent.Data);
    histIdx:= node.index;
    hist[histIdx]:= s;
  end;
end;

procedure TFrmProfiles.tvProfilesEditing(Sender: TObject; Node: TTreeNode; var AllowEdit: Boolean);
begin
  if node.level = 0 then
  begin
    allowedit:= node.index > 0;
  end
  else
    allowEdit:= node.Level <> 1;

  fEditing:= allowEdit;
end;

procedure TFrmProfiles.ActCancelExecute(Sender: TObject);
begin
  if not fEditing or (ActiveControl <> tvProfiles) then
    ModalResult:= mrCancel
  else if tvProfiles.Selected <> nil then
    tvProfiles.Selected.EndEdit(true);
  fEditing:= false;
end;

procedure TFrmProfiles.ActEditExecute(Sender: TObject);
var
  selNode : TTreeNode;
begin
  selNode:= tvProfiles.Selected;
  if selNode <> nil then
  begin
    selNode.EditText;
  end;
end;

procedure TFrmProfiles.ActCleanExecute(Sender: TObject);
begin
  // Clean entire tree

end;

procedure TFrmProfiles.AddNewProfile(aProfileName: string);
var
  idx : integer;
  node: TTreeNode;
  newNode: TTreeNode;
  oldProfCount: integer;
  num: integer;
  tstr: string;
begin
  node:= tvProfiles.Selected;
  if node <> nil then
  begin
    if node.Level = 0 then  // Ok to create new profile
    begin
      oldProfCount:= profiles.Count;
      num:= 1;
      tstr:= aProfileName;
      repeat
        idx:= profiles.AddProfile(tstr);
        if oldProfCount = profiles.Count then
        begin
          tstr:= Format('%s (%d)', [aProfileName, num]);
          Inc(num);
        end;
      until oldProfCount <> profiles.Count;

      if oldProfCount <> profiles.Count then
      begin
        newNode:= tvProfiles.Items.AddChild(nil, tstr);
        newNode.ImageIndex:= 1;
        newNode.SelectedIndex:= 1;
        if idx <> newNode.Index then
        begin
          tvProfiles.Items.Delete(newNode);
          profiles.DeleteProfile(idx);
        end
        else
        begin
          // Now add Source and Destination historys node for profile
          tvProfiles.Items.AddChildObject(newNode, 'Source History', profiles.SrcHistory[idx]);
          tvProfiles.Items.AddChildObject(newNode, 'Destination History', profiles.SrcHistory[idx]);
          tvProfiles.Selected:= newNode;
        end;
      end;
    end;
  end;
end;

procedure TFrmProfiles.ActCopyExecute(Sender: TObject);
// Copy current item
var
  node: TTreeNode;
  hist: TStringList;
begin
  if not fEditing then
  begin
    node:= tvProfiles.Selected;
    if node <> nil then
    begin
      if node.Level = 0 then  // copying a profile
      begin
        AddNewProfile('Copy of ' + node.text);
      end
      else if node.Level = 2 then
      begin
        // selected a folder node
        hist:= TStringList(node.parent.data);
        if hist <> nil then
        begin
          hist.Add('New Folder');
          node:= tvProfiles.Items.AddChild(node.Parent, 'New Folder');
          node.SelectedIndex:= 1;
          node.ImageIndex:= 1;
        end;
      end;
    end;
  end;
end;

procedure TFrmProfiles.ActRemoveExecute(Sender: TObject);
// Remove current item
var
  idx : integer;
  node: TTreeNode;
  hist: TStringList;
begin
  node:= tvProfiles.Selected;
  if node <> nil then
  begin
    if node.Level = 0 then  // deleting a profile
    begin
      profiles.DeleteProfile(node.Index);
      tvProfiles.Items.Delete(node);
    end
    else if node.Level = 2 then // deleting a history entry
    begin
      hist:= TStringList(node.Parent.Data);
      idx:= node.index;
      hist.Delete(idx);
      tvProfiles.Items.Delete(node);
    end;
  end;
end;

procedure TFrmProfiles.ActNewExecute(Sender: TObject);
var
  selNode : TTreeNode;
  child: TTreeNode;
  hist: TStringList;
begin
  // Create a new item in the current level
  selNode:= tvProfiles.Selected;
  if selnode <> nil then
  begin
    case selnode.Level of
      0:
      begin
        AddNewProfile('<New Profile>');
      end;
      1:
      begin
        selnode.Expand(false);
        child:= tvProfiles.Items.AddChild(selnode, 'New Folder');
        child.SelectedIndex:= 1;
        child.ImageIndex:= 1;
        hist:= TStringList(selnode.Data);
        hist.Add('New Folder');
        tvProfiles.Selected:= child;
      end;
      2:
      begin
        selnode:= selnode.Parent;
        child:= tvProfiles.Items.AddChild(selnode, 'New Folder');
        child.SelectedIndex:= 1;
        child.ImageIndex:= 1;
        hist:= TStringList(selnode.Data);
        hist.Add('New Folder');
        tvProfiles.Selected:= child;
      end;
    end;
    tvProfilesClick(nil);
  end;
end;

procedure TFrmProfiles.PopupMenu1Popup(Sender: TObject);
var
  node : TTreeNode;
begin
  node:= tvProfiles.Selected;
  if node <> nil then
  begin
    tvProfiles.Selected:= node;
  end;
  tvProfilesClick(self);
end;

procedure TFrmProfiles.tvProfilesDblClick(Sender: TObject);
begin
  if (tvProfiles.Selected <> nil) and (tvProfiles.Selected.Level = 2) then
    tvProfiles.Selected.EditText;
end;

procedure TFrmProfiles.FormShow(Sender: TObject);
begin
  tvProfiles.Selected:= tvProfiles.Items[0];
  tvProfilesClick(self);
end;

procedure TFrmProfiles.ActOKExecute(Sender: TObject);
begin
  ModalResult:= mrOK;
end;

procedure TFrmProfiles.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  if modalResult = mrOK then
    profiles.Save
  else
    profiles.Load;
end;

END.
