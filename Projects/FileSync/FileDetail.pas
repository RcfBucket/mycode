unit FileDetail;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, SyncData, Buttons, ExtCtrls, EllipsisLabel;

type
  TFrmFileDetail = class(TForm)
    Label1: TLabel;
    Label2: TLabel;
    LblSourceName: TLabel;
    LblSourceSize: TLabel;
    LblSourceTime: TLabel;
    LblDestName: TLabel;
    LblDestSize: TLabel;
    LblDestTime: TLabel;
    Label9: TLabel;
    LblExplain: TLabel;
    LblAction: TLabel;
    BtnClose: TBitBtn;
    BtnCompare: TBitBtn;
    Bevel1: TBevel;
    Bevel2: TBevel;
    Bevel3: TBevel;
    LblSrcPath: TEllipsisLabel;
    LblDstPath: TEllipsisLabel;
    procedure BtnCloseClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure BtnCompareClick(Sender: TObject);
  private
    fSrcFile  : TSyncFile;
    fDstFile  : TSyncFile;
    fAction   : TSyncAction;
    procedure SetSrcFile(value: TSyncFile);
    procedure SetDstFile(value: TSyncFile);
    procedure SetDstPath(value: TSyncFolder);
    procedure SetSrcPath(value: TSyncFolder);
    procedure SetAction(value: TSyncAction);
  public
    property SrcFile: TSyncFile write SetSrcFile;
    property DstFile: TSyncFile write SetDstFile;
    property SrcPath: TSyncFolder write SetSrcPath;
    property DstPath: TSyncFolder write SetDstPath;
    property Action: TSyncAction write SetAction;
  end;

implementation

{$R *.DFM}

procedure TFrmFileDetail.SetSrcFile(value: TSyncFile);
begin
  fSrcFile:= value;
  if fSrcFile <> nil then
  begin
    lblSourceName.caption:= fSrcFile.FileName;
    lblSourceSize.caption:= FormatFloat('#,##0', fSrcFile.FileSize);
    lblSourceTime.Caption:= FormatDateTime('mm/dd/yy  hh:nn:ss a/p', fSrcFile.FileDate);
  end
  else
  begin
    lblSourceName.caption:= 'Does not exist.';
    lblSourceSize.caption:= '- - -';
    lblSourceTime.caption:= '- - -';
  end;
end;

procedure TFrmFileDetail.SetDstFile(value: TSyncFile);
begin
  fDstFile:= value;
  if fDstFile <> nil then
  begin
    lblDestName.caption:= fDstFile.FileName;
    lblDestSize.caption:= FormatFloat('#,##0', fDstFile.FileSize);
    lblDestTime.Caption:= FormatDateTime('mm/dd/yy  hh:nn:ss a/p', fDstFile.FileDate);
  end
  else
  begin
    lblDestName.caption:= 'Does not exist.';
    lblDestSize.caption:= '- - -';
    lblDestTime.caption:= '- - -';
  end;
end;

procedure TFrmFileDetail.SetDstPath(value: TSyncFolder);
begin
  if value <> nil then
    lblDstPath.Caption:= value.FolderName
  else
    lblDstPath.caption:= '- - -';
end;

procedure TFrmFileDetail.SetSrcPath(value: TSyncFolder);
begin
  if value <> nil then
    lblSrcPath.Caption:= value.FolderName
  else
    lblSrcPath.caption:= '- - -';
end;

procedure TFrmFileDetail.SetAction(value: TSyncAction);
begin
  fAction:= value;
  lblAction.caption:= ActionStrs[value];
  case faction of
    actNochange:
      lblExplain.caption:= 'Files do not appear to be different according to compare settings.';
    actOverwriteNewer:
      lblExplain.caption:= 'The destination file has a newer date than the source file and is going to be overwritten.';
    actCopy:
      if fDstFile = nil then
        lblExplain.caption:= 'There is no file in the destinaion, so the source will be created there.'
      else
        lblExplain.caption:= 'The files have changed so the source file will be copied over the destination.';
    actDelete:
      lblExplain.caption:= 'The destination file no longer exists in the source folder so it will be deleted.';
    actUserNochanged:
      lblExplain.caption:= 'User modified original action to No Change.';
    else
      lblExplain.caption:= 'NO EXPLANATION - IM CONFUSED RIGHT NOW';
  end;
end;

procedure TFrmFileDetail.BtnCloseClick(Sender: TObject);
begin
  Close;
end;

procedure TFrmFileDetail.FormShow(Sender: TObject);
begin
//  BtnCompare.Enabled:= lblSourceSize.caption = lblDestSize.Caption;
  BtnCompare.Hint:= 'Binary Compare files.'+#13'Valid only when file sizes are the same';
end;

procedure TFrmFileDetail.BtnCompareClick(Sender: TObject);
var
  f1name, f2name : string;
begin
  if lblSourceSize.caption = lblDestSize.Caption then
  begin
    f1Name:= AppendToPath(lblSrcPath.Caption, lblSourceName.caption);
    f2Name:= AppendToPath(lblDstPath.caption, lblDestName.caption);
    if CompareFiles(f1name, f2name, false) then
      application.MessageBox('Files compare the same.', 'Information', MB_ICONINFORMATION or MB_OK)
    else
      application.MessageBox('Files do NOT compare the same.', 'Information', MB_ICONINFORMATION or MB_OK);
  end
  else
    MessageBox(handle, 'Files are not the same size or one file is missing.', 'Error', MB_ICONEXCLAMATION or MB_OK);
end;

end.
