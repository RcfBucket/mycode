object FrmGenReport: TFrmGenReport
  Left = 405
  Top = 206
  BorderIcons = []
  BorderStyle = bsToolWindow
  Caption = 'File Sync'
  ClientHeight = 66
  ClientWidth = 176
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = True
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 12
    Top = 24
    Width = 110
    Height = 13
    Caption = 'Generating Report . . .'
  end
  object Label2: TLabel
    Left = 120
    Top = 24
    Width = 6
    Height = 13
    Caption = '0'
  end
end
