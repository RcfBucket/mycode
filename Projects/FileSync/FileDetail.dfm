object FrmFileDetail: TFrmFileDetail
  Left = 352
  Top = 190
  BorderStyle = bsDialog
  Caption = 'File Sync Details'
  ClientHeight = 286
  ClientWidth = 298
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = True
  Position = poScreenCenter
  ShowHint = True
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 16
    Top = 16
    Width = 65
    Height = 13
    Caption = 'Source File'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label2: TLabel
    Left = 16
    Top = 96
    Width = 89
    Height = 13
    Caption = 'Destination File'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object LblSourceName: TLabel
    Left = 24
    Top = 56
    Width = 73
    Height = 13
    Caption = 'LblSourceName'
  end
  object LblSourceSize: TLabel
    Left = 24
    Top = 72
    Width = 65
    Height = 13
    Caption = 'LblSourceSzie'
  end
  object LblSourceTime: TLabel
    Left = 160
    Top = 72
    Width = 68
    Height = 13
    Caption = 'LblSourceTime'
  end
  object LblDestName: TLabel
    Left = 24
    Top = 136
    Width = 59
    Height = 13
    Caption = 'lblDestName'
  end
  object LblDestSize: TLabel
    Left = 24
    Top = 152
    Width = 54
    Height = 13
    Caption = 'LblDestSize'
  end
  object LblDestTime: TLabel
    Left = 160
    Top = 152
    Width = 57
    Height = 13
    Caption = 'LblDestTime'
  end
  object Label9: TLabel
    Left = 16
    Top = 176
    Width = 37
    Height = 13
    Caption = 'Action'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object LblExplain: TLabel
    Left = 16
    Top = 200
    Width = 265
    Height = 33
    AutoSize = False
    Caption = 'LblExplain'
    WordWrap = True
  end
  object LblAction: TLabel
    Left = 88
    Top = 176
    Width = 44
    Height = 13
    Caption = 'LblAction'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlue
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object Bevel1: TBevel
    Left = 16
    Top = 32
    Width = 265
    Height = 9
    Shape = bsTopLine
  end
  object Bevel2: TBevel
    Left = 16
    Top = 112
    Width = 265
    Height = 9
    Shape = bsTopLine
  end
  object Bevel3: TBevel
    Left = 16
    Top = 192
    Width = 265
    Height = 9
    Shape = bsTopLine
  end
  object LblSrcPath: TEllipsisLabel
    Left = 24
    Top = 40
    Width = 257
    Height = 13
    AutoSize = False
    Caption = 'LblSrcPath'
  end
  object LblDstPath: TEllipsisLabel
    Left = 24
    Top = 120
    Width = 257
    Height = 13
    AutoSize = False
    Caption = 'LblDstPath'
  end
  object BtnClose: TBitBtn
    Left = 160
    Top = 248
    Width = 113
    Height = 27
    Cancel = True
    Caption = 'Close'
    Glyph.Data = {
      F6000000424DF600000000000000760000002800000010000000100000000100
      0400000000008000000000000000000000001000000010000000000000000000
      8000008000000080800080000000800080008080000080808000C0C0C0000000
      FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00887788888888
      8888891178888897888889111788891178888911117891111788889111171111
      1788888911111111788888889111111788888888811111788888888889111178
      8888888891111178888888891117111788888891117891117888889117888911
      1788888918888891118888888888888919888888888888888888}
    ModalResult = 1
    TabOrder = 1
    OnClick = BtnCloseClick
  end
  object BtnCompare: TBitBtn
    Left = 16
    Top = 248
    Width = 113
    Height = 27
    Caption = '&Byte Compare'
    Glyph.Data = {
      F6000000424DF600000000000000760000002800000010000000100000000100
      0400000000008000000000000000000000001000000010000000000000000000
      80000080000000808000800000008000800080800000C0C0C000808080000000
      FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00777777777777
      777700000000777770070FFFFFF077774EC70F8888F07774ECC70FFFFFF0774E
      CC770F888800087CC7770FFFF07E808800000F880FE7E80FFFF00FFF0EFE7E08
      88F00FFF0FEFE70FFFF0000000FEF08888F0777777000FFFFFF0777777770F88
      F000777777770FFFF0F0777777770FFFF0077777777700000077}
    TabOrder = 0
    OnClick = BtnCompareClick
  end
end
