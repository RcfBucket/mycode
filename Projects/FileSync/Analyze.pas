unit Analyze;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, ExtCtrls, SyncData, EllipsisLabel, WildCard;

type
{$IFDEF MSWINDOWS}
  TThreadNameInfo = record
    FType: LongWord;     // must be 0x1000
    FName: PChar;        // pointer to name (in user address space)
    FThreadID: LongWord; // thread ID (-1 indicates caller thread)
    FFlags: LongWord;    // reserved for future use, must be zero
  end;
{$ENDIF}
  TNamedThread = class(TThread)
  private
    fName       : string;
    procedure SetName;
  public
    constructor Create(CreateSuspended: Boolean); overload;
    constructor Create(CreateSuspended: Boolean; aName: string); overload;
    procedure Execute; override;
    property Name: string read fName write fName;
  end;

  TSearchThread = class(TNamedThread)
    fileCount   : integer;
    folderCount : integer;
    sList       : TSyncList;
    curDir      : string;
    complete    : boolean;
    //
    constructor Create(CreateSuspended: Boolean); overload;
    constructor Create(CreateSuspended: Boolean; aName: string); overload;
    destructor Destroy; override;
    procedure Execute; override;
    procedure FileFound(name: string);
    procedure FolderFound(name: string);
  end;

  TCompareThread = class(TNamedThread)
    curFolder   : string;
    curFile     : string;
    curCompareType: TSyncCompareType;
    sync        : TSync;
    complete    : boolean;
    //
    constructor Create(CreateSuspended: Boolean); overload;
    constructor Create(CreateSuspended: Boolean; aName: string); overload;
    destructor Destroy; override;
    procedure Execute; override;
    procedure FileCompare(name: string; compareType: TSyncCompareType);
    procedure FolderCompare(name: string);
  private
    procedure CheckContinue(var doContinue: boolean);
  end;

  TFrmAnalyze = class(TForm)
    Label2: TLabel;
    Label3: TLabel;
    lblSrcFolders: TLabel;
    lblSrcFiles: TLabel;
    BtnCancel: TBitBtn;
    Timer1: TTimer;
    Label1: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    lblDstFiles: TLabel;
    lblDstFolders: TLabel;
    Label9: TLabel;
    Label6: TLabel;
    lblCompareFile: TLabel;
    Bevel1: TBevel;
    Label7: TLabel;
    lblStat: TLabel;
    Label8: TLabel;
    lblSrcCur: TLabel;
    lblDstCur: TLabel;
    lblCompareFolder: TLabel;
    procedure FormShow(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure Timer1Timer(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
  private
    searcher  : TSearchThread;
    comparer  : TCompareThread;
    analState : integer;  // state of analyzer (see Timer event below)
    procedure ThreadDone(sender: TObject);
  public
    sync      : TSync;
  end;

IMPLEMENTATION

{$R *.DFM}


{---------------------------------------------------[ TSearchThread ]--}

constructor TSearchThread.Create(CreateSuspended: Boolean);
begin
  inherited;
  fileCount:= 0;
  folderCount:= 0;
  complete:= false;
end;

constructor TSearchThread.Create(CreateSuspended: Boolean; aName: string);
begin
  inherited Create(CreateSuspended, aName);

  fileCount:= 0;
  folderCount:= 0;
  complete:= false;
end;

destructor TSearchThread.Destroy;
begin
  inherited;
end;

procedure TSearchThread.FileFound(name: string);
begin
  Inc(fileCount);
end;

procedure TSearchThread.FolderFound(name: string);
begin
  curDir:= name;
  Inc(folderCount);
end;

procedure TSearchThread.Execute;
begin
  inherited;
  
  if (sList = nil) then
  begin
    MessageDlg('SyncList not initialized (TSearchThread)', mtError, [mbOK], 0);
    Terminate;
  end
  else
  begin
    sList.OnFile:= FileFound;
    sList.OnFolder:= FolderFound;
    fileCount:= 0;
    folderCount:= 0;
    ReturnValue:= 1;
    try
      try
        sList.Execute;
      except
        ReturnValue:= -1;
      end;
    finally
      sList.OnFolder:= nil;
      sList.OnFile:= nil;
    end;
    complete:= true;
  end; 
end;

{---------------------------------------------------[ TCompareThread ]--}

constructor TCompareThread.Create(CreateSuspended: Boolean);
begin
  inherited;
  complete:= false;
end;

constructor TCompareThread.Create(CreateSuspended: Boolean; aName: string);
begin
  inherited Create(CreateSuspended, aName);
  complete:= false;
end;

destructor TCompareThread.Destroy;
begin
  inherited;
end;

procedure TCompareThread.FileCompare(name: string; compareType: TSyncCompareType);
begin
  curFile:= name;
  curCompareType:= compareType;
end;

procedure TCompareThread.FolderCompare(name: string);
begin
  curFolder:= name;
end;

procedure TCompareThread.CheckContinue(var doContinue: boolean);
begin
  doContinue:= not Terminated;
end;

procedure TCompareThread.Execute;
begin
  inherited;
  if (sync = nil) then
  begin
    MessageDlg('SyncList not initialized (TCompareThread)', mtError, [mbOK], 0);
    Terminate;
  end;
  sync.OnCompareFile:= FileCompare;
  sync.OnCompareFolder:= FolderCompare;
  curFolder:= '';
  curFile:= '';
  ReturnValue:= 1;
  try
    try
      sync.OnCheckCancel:= CheckContinue;
      sync.AnalyzeFolders;
    except
      ReturnValue:= -1;
    end;
  finally
    sync.OnCompareFile:= nil;
    sync.OnCompareFolder:= nil;
    sync.OnCheckCancel:= nil;
  end;
  complete:= true;
end;

{-----------------------------------------------------[ TFrmAnalyze ]--}

procedure TFrmAnalyze.FormCreate(Sender: TObject);
begin
  {$IFDEF DEBUG}
    label8.visible:= true;
    lblStat.visible:= true;
  {$ENDIF}
  lblSrcCur.caption:= '';
  lblSrcFolders.caption:= '0';
  lblSrcFiles.caption:= '0';
  lbldstCur.caption:= '';
  lbldstFolders.caption:= '0';
  lbldstFiles.caption:= '0';
  analState:= 0;
end;

procedure TFrmAnalyze.FormDestroy(Sender: TObject);
begin
  if searcher <> nil then
  begin
    searcher.Suspend;
    searcher.Free;
  end;
  if comparer <> nil then
  begin
    comparer.Suspend;
    comparer.Free;
  end;
end;

procedure TFrmAnalyze.FormShow(Sender: TObject);
begin
  label7.caption:= '';
  lblSrcFiles.Caption:= '0';
  lblSrcFolders.Caption:= '0';

  lblDstFiles.Caption:= '0';
  lblDstFolders.Caption:= '0';

  lblCompareFile.caption:= '- - -';
  lblCompareFolder.caption:= '- - -';
  timer1.Enabled:= true;
end;

procedure TFrmAnalyze.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
var
  saveTimer : boolean;
begin
  saveTimer:= timer1.Enabled;
  timer1.Enabled:= false;

  CanClose:= false;

  if analState <> 99 then   // process not complete
  begin
    if (searcher <> nil) then
      searcher.Suspend;
    if (comparer <> nil) then
      comparer.Suspend;

    if MessageDlg('Cancel operation?', mtConfirmation, [mbYes,mbNo], 0) = mrYes then
    begin
      CanClose:= true;
      if searcher <> nil then
      begin
        searcher.Resume;
        searcher.Terminate;
      end;
      if comparer <> nil then
      begin
        comparer.Resume;
        comparer.Terminate;
      end;
    end
    else
    begin
      if searcher <> nil then
        searcher.Resume;
      if comparer <> nil then
        comparer.Resume;
    end;
  end
  else
    CanClose:= true;

  if not CanClose then
    timer1.Enabled:= SaveTimer;
end;

{- Analysis takes place in states. The process continues until the state
   reaches complete.  States are looked at in the timer event. It is here
   that the state is evaluated.

   State  Description
   -----  --------------------
     0    Not started. Create and execute the SRC folder search thread.
     1    SRC folder search running.
     2    SRC folder search complete. Thread destroyed. DST folder search thread
          created and executed.
     3    DST folder search running
     4    DST folder search complete. Thread destroyed. Folder analysis is
          started.
     5    Analysis is running
     6    Compare complete. Cleanup
     99   Time to close analysis window
}

var
  eNum : integer = 0;

procedure TFrmAnalyze.Timer1Timer(Sender: TObject);
const
  strs  : array[0..3] of string = ('|', '/','---','\');
begin
{$IFDEF DEBUG}
  lblStat.caption:= strs[eNum];
  if eNum < 3 then
    Inc(eNum)
  else
    eNum:= 0;
  label8.caption:= IntToStr(analState);
{$ENDIF}

  timer1.Enabled:= false;
  case analState of
    0:  // process not stated yet
      begin
        analState:= 1;
        searcher:= TSearchThread.Create(true, 'SourceSearcher');
        searcher.OnTerminate:= ThreadDone;
        searcher.sList:= sync.Source;
        searcher.Resume;  // start the thread
      end;

    1:  // SRC folder search running.
      begin
        lblSrcFiles.Caption:= IntToStr(searcher.FileCount);
        lblSrcFolders.caption:= IntToStr(searcher.FolderCount);
        lblSrcCur.caption:= searcher.CurDir;
      end;

    2:  // SRC folder search complete.
      begin
        analState:= 3;

        FreeAndNil(searcher); // free src searcher
        searcher:= TSearchThread.Create(true, 'DestSearcher');
        searcher.OnTerminate:= ThreadDone;

        searcher.sList:= sync.Dest;
        searcher.Resume;  // start the thread
      end;

    3:  // DST folder search running
      begin
        lblDstFiles.Caption:= IntToStr(searcher.FileCount);
        lblDstFolders.caption:= IntToStr(searcher.FolderCount);
        lblDstCur.caption:= searcher.CurDir;
      end;
    4:
      begin // DST folder search complete.
        FreeAndNil(searcher); // free src searcher

        analState:= 5;

        comparer:= TCompareThread.Create(true, 'Comparer');
        comparer.OnTerminate:= ThreadDone;
        comparer.sync:= sync;
        comparer.Resume;
      end;
    5:
      begin
        lblCompareFolder.caption:= comparer.curFolder;
        lblCompareFile.caption:= comparer.curFile;
        if comparer.curCompareType = syncByteOnly then
          label7.caption:= '(Byte compare)'
        else
          label7.caption:= '(Date/Time/Size compare)';
      end;
    6:
      begin
        FreeAndNil(comparer);
        ModalResult:= mrOK;
        analState:= 99;
      end;
    else
      begin
        MessageDlg('Analysis state corrupted. Aborting process', mtError, [mbOK], 0);
        analState:= 99;
        Close;
      end;
  end;
  if (analState <= 6) then
    timer1.Enabled:= true;
end;

procedure TFrmAnalyze.ThreadDone(sender: TObject);
begin
  timer1.Enabled:= false;

  // triggered when a thread completes.
  // just increment the state to the next level and the timer will deal with
  // cleanup
  if searcher <> nil then
  begin
    if searcher.ReturnValue = -1 then
    begin
      if analState = 1 then
        MessageDlg('Unexpected error searching SOURCE folders', mtError, [mbCancel], 0)
      else if analState = 3 then
        MessageDlg('Unexpected error searching DESTINATION folders', mtError, [mbCancel], 0)
      else
        MessageDlg('Unexpected error searching folders', mtError, [mbCancel], 0);
      analState:= 99;
      Close;
    end
    else
    begin
      if analState = 1 then
      begin
        // get final counts
        lblSrcCur.caption:= 'Complete';
        lblSrcFiles.Caption:= IntToStr(searcher.FileCount);
        lblSrcFolders.caption:= IntToStr(searcher.FolderCount);
      end
      else if analState = 3 then
      begin
        lblDstCur.caption:= 'Complete';
        lblDstFiles.Caption:= IntToStr(searcher.FileCount);
        lblDstFolders.caption:= IntToStr(searcher.FolderCount);
      end;

//      searcher.Free;    // The thread is finished.
//      searcher:= nil;

      Inc(analState);   // bump up the state and resume timer.
      Timer1.Enabled:= true;
    end;
  end
  else if comparer <> nil then
  begin
    if comparer.ReturnValue = -1 then
    begin
      MessageDlg('Unexpected error comparing files', mtError, [mbCancel], 0);
      analState:= 99;
      Close;
    end
    else
    begin
//      comparer.Free;  // the thread is finished
//      comparer:= nil;

      Inc(analState);
      timer1.Enabled:= true;
    end;
  end;
end;

{ TNamedThread }

constructor TNamedThread.Create(CreateSuspended: Boolean);
begin
  fName:= 'TNamedThread';
  inherited;
end;

constructor TNamedThread.Create(CreateSuspended: Boolean; aName: string);
begin
  fName:= aName;
  inherited Create(CreateSuspended);
end;

procedure TNamedThread.Execute;
begin
  SetName;
end;

procedure TNamedThread.SetName;
{$IFDEF MSWINDOWS}
var
  ThreadNameInfo: TThreadNameInfo;
{$ENDIF}
begin
{$IFDEF MSWINDOWS}
  ThreadNameInfo.FType := $1000;
  ThreadNameInfo.FName := PChar(fName);
  ThreadNameInfo.FThreadID := $FFFFFFFF;
  ThreadNameInfo.FFlags := 0;

  try
    RaiseException( $406D1388, 0, sizeof(ThreadNameInfo) div sizeof(LongWord), @ThreadNameInfo );
  except
  end;
{$ENDIF}
end;

END.


