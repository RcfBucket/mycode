unit SrcCompare;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, ComCtrls, ToolWin, SCM, ExtCtrls, EllipsisLabel, Menus,
  ActnList, ImgList;

type
  TFrmSourceCompare = class(TForm)
    rEdDiffs: TRichEdit;
    FontDialog1: TFontDialog;
    Panel1: TPanel;
    Label1: TLabel;
    Label2: TLabel;
    LblSrcPath: TEllipsisLabel;
    LblDstPath: TEllipsisLabel;
    LblSrcName: TLabel;
    LblDstName: TLabel;
    LblSrcSize: TLabel;
    LblDstSize: TLabel;
    LblSrcDate: TLabel;
    LblDstDate: TLabel;
    PopupMenu1: TPopupMenu;
    Options1: TMenuItem;
    Font1: TMenuItem;
    sb: TStatusBar;
    ToolBar1: TToolBar;
    ToolButton1: TToolButton;
    ToolButton2: TToolButton;
    ToolButton3: TToolButton;
    ToolButton4: TToolButton;
    ToolButton5: TToolButton;
    ActionList1: TActionList;
    ImageList1: TImageList;
    ActFont: TAction;
    ActOptions: TAction;
    ActPrint: TAction;
    ActClose: TAction;
    procedure FormShow(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure FormCreate(Sender: TObject);
    procedure ActFontExecute(Sender: TObject);
    procedure ActOptionsExecute(Sender: TObject);
    procedure ActPrintExecute(Sender: TObject);
    procedure ActCloseExecute(Sender: TObject);
  private
    SCM1: TSCM;
    procedure ShowDiffs;
  public
    { Public declarations }
  end;

var
  FrmSourceCompare: TFrmSourceCompare;

implementation

uses SrcCompOpts, SyncData, Globals;

{$R *.DFM}

procedure TFrmSourceCompare.ShowDiffs;
var
  k   : integer;
  idx : integer;
  tstr: string;
  temp: integer;
begin
  SetSrcCompFont(rEdDiffs.Font);
  temp:= rEdDiffs.SelStart;
  rEdDiffs.Lines.BeginUpdate;
  rEdDiffs.SelStart:= 0;
  rEdDiffs.SelLength:= Length(rEdDiffs.Text);
  rEdDiffs.SelAttributes.Name:= rEdDiffs.Font.Name;
  rEdDiffs.SelAttributes.Size:= rEdDiffs.Font.Size;
  rEdDiffs.Lines.EndUpdate;
  rEdDiffs.SelStart:= temp;

  screen.Cursor:= crHourGlass;
  try
    rEdDiffs.Lines.BeginUpdate;
    rEdDiffs.Clear;
    rEdDiffs.SelStart:= 0;
    rEdDiffs.SelLength:= 0;
    for k:= 0 to scm1.DiffCount - 1 do
    begin
      tstr:= scm1.DiffText[k];
      rEdDiffs.Lines.Add(tstr);
      if (Length(tstr) > 0) and ('|' = tstr[1]) then
      begin
        temp:= rEdDiffs.SelStart;
        idx:= SendMessage(rEdDiffs.handle, EM_LINEINDEX, k, 0);
        Inc(idx, 1);
        rEdDiffs.SelStart:= idx;
        rEdDiffs.SelLength:= Length(scm1.DiffText[k]) - 1;
        rEdDiffs.SelAttributes.Color:= clBlue;
        rEdDiffs.SelStart:= temp;
        rEdDiffs.SelLength:= 0;
      end;
    end;
    rEdDiffs.SelStart:= 0;
    rEdDiffs.SelLength:= 0;
    rEdDiffs.Lines.EndUpdate;
  finally
    screen.Cursor:= crDefault;
  end;
  sb.Panels[0].text:= 'Differences found : ' + IntToStr(scm1.NumDiffs);
end;

procedure TFrmSourceCompare.FormShow(Sender: TObject);
begin
  ReadFormPosition(self);

  SetSrcCompFont(rEdDiffs.Font);

  scm1.FileName1:= AppendToPath(lblSrcPath.caption, lblSrcName.caption);
  scm1.FileName2:= AppendToPath(lblDstPath.caption, lblDstName.caption);
  SetSCMOptions(scm1);  // get options from global settings
  scm1.Execute;
  ShowDiffs;;
end;

procedure TFrmSourceCompare.FormResize(Sender: TObject);
begin
  lblSrcPath.Width:= panel1.width - lblSrcPath.left - 8;
  lblDstPath.Width:= panel1.width - lblDstPath.left - 8;
end;

procedure TFrmSourceCompare.FormCloseQuery(Sender: TObject;
  var CanClose: Boolean);
begin
  SaveFormPosition(self);
end;

procedure TFrmSourceCompare.FormCreate(Sender: TObject);
var
  hMenu : THandle;
begin
  // Remove system menu
  SCM1:= TSCM.Create(self);
  hMenu:= GetSystemMenu(Handle, False);
  DeleteMenu(hMenu, SC_MINIMIZE, MF_BYCOMMAND);
end;

procedure TFrmSourceCompare.ActFontExecute(Sender: TObject);
var
  temp  : integer;
begin
  fontdialog1.Font.Name:= rEdDiffs.SelAttributes.Name;
  fontdialog1.Font.Size:= rEdDiffs.SelAttributes.Size;
  fontdialog1.Font.Style:= rEdDiffs.SelAttributes.Style;
  if fontDialog1.Execute then
  begin
    temp:= rEdDiffs.SelStart;
    rEdDiffs.Lines.BeginUpdate;
    rEdDiffs.SelStart:= 0;
    rEdDiffs.SelLength:= Length(rEdDiffs.Text);
    with rEdDiffs.SelAttributes do
    begin
      Name:= fontDialog1.Font.Name;
      Size:= fontDialog1.Font.Size;
      Style:= fontDialog1.Font.Style;
    end;
    rEdDiffs.SelStart:= temp;
    rEdDiffs.SelLength:= 0;
    rEdDiffs.Lines.EndUpdate;
  end;
end;

procedure TFrmSourceCompare.ActOptionsExecute(Sender: TObject);
{var
  okToGo  : boolean;}
begin
  with TFrmSrcCompOpts.Create(self) do
  begin
    if ShowModal = mrOK then
    begin
      SetSCMOptions(scm1);
      scm1.Execute;
      ShowDiffs;
    end;
    Free;
  end;
end;

procedure TFrmSourceCompare.ActPrintExecute(Sender: TObject);
begin
  rEdDiffs.Print('File Sync - Source Compare');
end;

procedure TFrmSourceCompare.ActCloseExecute(Sender: TObject);
begin
  Close;
end;

END.

