unit USBMain;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ContNrs;

type
  TUSBController = class
  private
    fname : string;
    fHandle: THandle;
  end;

  TUSBControllers = class
  private
    fControllers: TObjectList;
    function GetCount: integer;
    procedure SetCount(const Value: integer);
  public
    constructor Create;
    destructor Destroy; override;
    property Count: integer read GetCount;
  end;


  TForm1 = class(TForm)
    BtnControllers: TButton;
    Memo1: TMemo;
    BtnRootHub: TButton;
    procedure BtnControllersClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

{$R *.dfm}

procedure TForm1.BtnControllersClick(Sender: TObject);
begin
end;

{ TUSBControllers }

constructor TUSBControllers.Create;
var
  k : integer;
  hostName: string;
  handle: THandle;
begin
  fControllers:= TObjectList.Create;
  k:= 0;
  repeat
    hostName:= '\\.\HCD' + IntToStr(k);
    handle:= CreateFile(PCHar(hostName), GENERIC_WRITE, 2, 0, 3, 0, 0);
    if handle <> INVALID_HANDLE_VALUE then
    begin
      CloseHandle(handle);
    end
    else
    begin
      memo1.Lines.Add(Format('HCD%d is NOT valid : %u', [k, handle]));
      memo1.Lines.Add('Iteration Stopped');
    end;
    Inc(k);
  until (handle = INVALID_HANDLE_VALUE);
end;
end;

destructor TUSBControllers.Destroy;
begin
  fControllers.Free;
  inherited;
end;

function TUSBControllers.GetCount: integer;
begin
  result:= fControllers.Count;
end;

procedure TUSBControllers.SetCount(const Value: integer);
begin
end;

end.
