object Form1: TForm1
  Left = 192
  Top = 107
  Width = 637
  Height = 328
  Caption = 'Form1'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object BtnControllers: TButton
    Left = 16
    Top = 16
    Width = 153
    Height = 25
    Caption = 'Iterate USB Controllers'
    TabOrder = 0
    OnClick = BtnControllersClick
  end
  object Memo1: TMemo
    Left = 376
    Top = 8
    Width = 249
    Height = 289
    Lines.Strings = (
      'Memo1')
    TabOrder = 1
  end
  object BtnRootHub: TButton
    Left = 16
    Top = 48
    Width = 153
    Height = 25
    Caption = 'BtnRootHub'
    TabOrder = 2
  end
end
