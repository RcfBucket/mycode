﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace TestWebApp
{
  public partial class WebForm1 : System.Web.UI.Page
  {
    protected void Page_Load(object sender, EventArgs e)
    {
      Response.Write("ASPX Page<br /><br />");

      int numrows = 3;
      int numcells = 3;
      Table1.Rows.Clear();
      for (int j = 0; j < numrows; j++)
      {
        TableRow r = new TableRow();
        for (int i = 0; i < numcells; i++)
        {
          TableCell c = new TableCell();
          c.Width = Unit.Percentage(25);
          c.BorderStyle = BorderStyle.Solid;
          c.Controls.Add(new LiteralControl("row "
              + j.ToString() + ", cell " + i.ToString()));
          r.Cells.Add(c);
        }
        Table1.Rows.Add(r);
      }

    }
  }
}
