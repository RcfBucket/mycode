﻿<%@ Language="javascript" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
<head>
  <link href="StyleSheet.css" rel="stylesheet" type="text/css" /> 
  <title>ASP Tests</title>
</head>
<body>
  <%
    var name = "";
    name = Request("username");
    if (Request("username") != "") {
      name = Request("username");
    }
    Response.Write("Hi from ASP : " + name + " <<< ");
    if (name == "bob") {
      //Response.Redirect("htmlpage.htm");
      Response.Write("You passed the test <br />");
      Response.Write("<br /><a href=\"TestAsp.asp\">Return to ASP Page</a>");
      Response.End;
    }
  %>
  <hr />
  <p>
  Query String: <% Response.Write(Request.QueryString); %><br />
  Request.Form("username"):&nbsp <% Response.Write(Request.Form("username")); %> <br />
  Request("username"):&nbsp <% Response.Write(name); %> 
  </p>
  
  <form action="<%Request.ServerVariables("SCRIPT_NAME") %>" method="get" >
  <p>Enter Name:
    <input type="text" name="username" value="<%=name%>" />
    <input type="submit" value="Get Method" />
  </p>
  </form>
  
  <form action="<%Request.ServerVariables("SCRIPT_NAME")%>" method="post" >
  <p>Enter Name:
    <input type="text" name="username" value="<%=name%>" />
    <input type="submit" value="Post Method" />
  </p>
  </form>  
</body>
</html>
