unit Main;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, MPlayer, MMSystem, ExtCtrls;

type
  TForm1 = class(TForm)
    Label1: TLabel;                          // "ID Calculator
    Edit1: TEdit;                            // "$00000000"
    BitBtn1: TBitBtn;                        // "Calculate"
    Label2: TLabel;                          // "Disc ID"
    ListBox1: TListBox;                      // contains the trackpositions
    MediaPlayer: TMediaPlayer;               // for "talking" with the CD ROM
    Label3: TLabel;                          // "CD TOC"
    BitBtn2: TBitBtn;
    BitBtn3: TBitBtn;
    ListBox2: TListBox;                        // "Exit"
    procedure BitBtn1Click(Sender: TObject); // Calculate !
    procedure BitBtn2Click(Sender: TObject);
    procedure BitBtn3Click(Sender: TObject);
    procedure FormCreate(Sender: TObject); // Exit
  private
    { Private-Deklarationen }
  public
    { Public-Deklarationen }
  end;

TOC = record        // declares my CD TOC
  min: LongInt;
  sec: LongInt;
  frames: LongInt;
end;

const advanced = false;
      simple = true;

var
  Form1: TForm1;
  cdToc: array[0..100] of Toc; // The CD TOC itself
  x: word;                     // for a loop in the main procedure
  mode: boolean = simple;

implementation

{$R *.DFM}


procedure TForm1.BitBtn1Click(Sender: TObject);

  procedure read_cdtoc;        // reads the Table of contains
    ////////////////////////////////////////////////////////////////////////////
    FUNCTION GetTrackPos (TrackNo: Longint): LongInt; // Win95 APIs to access the CDROM Drive
    VAR
      ReturnCode: Longint;
      mciStatus: TMCI_Status_Parms;
    BEGIN
      mciStatus.dwTrack := TrackNo; // 1st Parameter: loading the TrackNumber
      mciStatus.dwItem := MCI_STATUS_POSITION; // 2nd Parameter: I want TrackPositions

      mciSendCommand (Mediaplayer.DeviceID, mci_Status, MCI_STATUS_ITEM OR
                      MCI_TRACK OR MCI_WAIT, Longint(@mciStatus));  // calls the API Function
      GetTrackpos := mciStatus.dwReturn;
    END;

    var i: byte;
        offset_m, offset_s, offset_f, pos : Longint;

    begin
      Mediaplayer.TimeFormat:=tfMSF;
      For i:=0 to Mediaplayer.Tracks - 1 do
         begin
           offset_m:=MCI_MSF_Minute(GetTrackPos(i+1));  // Trackoffsets
           offset_s:=MCI_MSF_Second(GetTrackPos(i+1));
           offset_f:=MCI_MSF_Frame(GetTrackPos(i+1)+1);

           pos:=(offset_m * 60 * 75) + (offset_s * 75) + offset_f;
           cdToc[i].Frames:=pos;

           pos:=pos div 75; // yields the Trackpos in Seconds
           cdToc[i].Min:=pos div 60; // trackpos in minutes
           cdToc[i].Sec:=pos mod 60; // trackpos in seconds
         end;
    end;

    ////////////////////////////////////////////////////////////////////////////

    function cddb_sum(n: LongInt): LongInt;
      var ret: LongInt;
          s: string;

      begin
        ret:=0;
        while (n > 0) do
          begin
            ret:=ret + n mod 10;
            n:=n div 10;
          end;
        cddb_sum:=ret;
      end;

    ////////////////////////////////////////////////////////////////////////////

  procedure CalculateID;
    var tot_tracks: longint;
        i: Longint;
        n: LongInt;          // CD TOC
        ret: Longint;
        value: string;       // the result
        disclen: LongInt;
        Offset_m,
        Offset_s,
        Offset_f,
        Len_m,
        Len_s,
        Len_f : LongInt;

    begin
      tot_tracks:=Mediaplayer.Tracks;  // how many tracks ?
      Listbox2.Items.Add('Tracks on CD...' + IntToStr(tot_tracks));

      i:=0; n:=0; ret:=0;

      for i := 0 to tot_tracks - 1 do
        begin
          n:=n + cddb_sum(cdToc[i].min * 60 + cdToc[i].sec);
        end;

      // the following is for calculating the disclength
      Mediaplayer.Timeformat:=tfMSF;

      // 1st: get the trackpos of the lasttrack
      offset_m:=MCI_MSF_Minute(Mediaplayer.TrackPosition[tot_Tracks]);
      offset_s:=MCI_MSF_Second(Mediaplayer.TrackPosition[Tot_tracks]);
      offset_f:=MCI_MSF_Frame(Mediaplayer.TrackPosition[Tot_Tracks]);
      // 2nd: get the tracklen of the lasttrack
      Len_m:=MCI_MSF_Minute(Mediaplayer.TrackLength[Tot_Tracks]);
      Len_s:=MCI_MSF_Second(Mediaplayer.TrackLength[Tot_Tracks]);
      Len_f:=MCI_MSF_Frame(Mediaplayer.TrackLength[Tot_Tracks]);
      // 3rd: get the leadout pos by adding length to offset
      disclen:=((offset_m * 60 * 75) + (offset_s * 75) + offset_f +
               (len_m * 60 * 75) + (Len_s * 75) + len_f + 1);
      // 4th: by dividing by 75 and substracting the Trackpos of the first track
      // you get the disclenght in seconds

      Listbox2.Items.Add('');
      Listbox2.Items.Add('Offset of');
      Listbox2.Items.Add('Lead-Out...$' + IntToHex(disclen,8));

      disclen:=disclen div 75;     // (yields the Lead-out position)

      disclen:=disclen - (MCI_MSF_Minute(Mediaplayer.TrackPosition[1]) * 60 +
                          MCI_MSF_Second(Mediaplayer.TrackPosition[1]));

      n:=n mod 255;
      n:=n shl 24;

      disclen:=disclen shl 8;

      Listbox2.Items.Add('');
      Listbox2.Items.Add('Parameter');
      Listbox2.Items.Add('n...$' + IntToHex(n,8));
      Listbox2.Items.Add('d...$' + IntToHex(disclen,8));
      Listbox2.Items.Add('t...$' + IntToHex(tot_tracks,8));
      ret:=(n or disclen or tot_tracks);  // the cddb id

      Value:=IntToHex(ret, 8);

      For i:=1 TO length(value) do        // downcases all letters
        begin
          IF (Value[i] > #64) AND (Value[i] < #91) THEN
            begin
              Value[i]:=Chr(Ord(Value[i]) + 32);
            end;
        end;
      Edit1.Text:='$' + Value;            // outputs the cddb id
    end;

    ////////////////////////////////////////////////////////////////////////////

begin
  For x:=0 to 100 do   // initialises the CD TOC Var
    begin
      cdToc[x].min:=0;
      cdToc[x].sec:=0;
      cdToc[x].frames:=0;
    end;
  Mediaplayer.FileName:='R:';
  Mediaplayer.DeviceType:=dtCDAudio;  // I want CD Audio !
  Mediaplayer.Open;
  read_cdtoc;                         // reads the CD TOC and stores it in the CD TOC
  ListBox1.Items.Clear;               // to show each trackposition
  Listbox2.Items.Clear;
  Listbox2.Items.Add('protocol...'); // initializes the Protocolbox.
  Listbox2.Items.Add('');
  Listbox1.Items.Add('     offsets...');
  ListBox1.Items.Add('trk' + '  ' + 'min' + '  ' + 'sec' + '  ' + 'frames');
  For x:=0 TO 100 do
    begin
      IF NOT (cdToc[x].frames = 0) THEN
        ListBox1.Items.Add(FormatFloat('00',x+1) + '   '+
                           FormatFloat('00', cdToc[x].Min) +
                           '   ' +
                           FormatFloat('00', (cdToc[x].Sec)) +
                           '   ' +
                           FormatFloat('0000', (cdToc[x].Frames)));
      end;
  CalculateID;                        // the calculating itself
  Mediaplayer.Close;
end;

procedure TForm1.BitBtn2Click(Sender: TObject);
begin
  Application.Terminate;
end;

procedure TForm1.BitBtn3Click(Sender: TObject);
begin
  IF Mode = simple THEN
    begin
      Form1.Width:=394;
      BitBtn1.Left:=104;
      BitBtn2.Left:=104;
      BitBtn3.Left:=104;
      BitBtn3.Caption:='<< Simple';
      Label1.Left:=124;
      Mode:=advanced;
    end
  ELSE
    begin
      Form1.Width:=221;
      BitBtn1.Left:=12;
      BitBtn2.Left:=12;
      BitBtn3.Left:=12;
      BitBtn3.Caption:='Advanced >>';
      Label1.Left:=24;
      Mode:=simple;
    end;
end;

procedure TForm1.FormCreate(Sender: TObject);
begin
  mode:=simple;
end;

end.

{
Annotations:

  I had great problems to get the things right. I needed much time before my
  program has computed a right cddb id. I have to thank Steve and the programmers
  of CD Valet and CD Notify for helping me.
  Because of the problems I had with the cddb routines I want to offer you my
  code for free.

  For Win95 developper it might be useful to know that Win95 or my Delphi-Compiler
  is not able to return the right tracklength and trackpositions. To add '1' to
  the frames it will go right.

  disclen:=((offset_m * 60 * 75) + (offset_s * 75) + offset_f +
           (len_m * 60 * 75) + (Len_s * 75) + len_f + 1);
                                                    ^^^
  Please read the cddb manual for more details.

  The cddb ID itself:

  $3304b605
  $33       04b6          05
  The CDTOC the disclen   5 tracks
  Trackpos  in seconds

  Please contact me if you have any questions on how to program your own cddb app.
  eMail: Pu.derBaer@t-online.de

}
