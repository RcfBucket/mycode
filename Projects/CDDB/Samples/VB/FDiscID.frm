VERSION 5.00
Begin VB.Form FDiscID 
   Caption         =   "Compute CDDB Discid"
   ClientHeight    =   1875
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   8370
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   ScaleHeight     =   1875
   ScaleWidth      =   8370
   StartUpPosition =   3  'Windows Default
   Begin VB.TextBox txtQuery 
      Height          =   375
      Left            =   135
      Locked          =   -1  'True
      TabIndex        =   5
      Top             =   720
      Width           =   8160
   End
   Begin VB.CommandButton btnExit 
      Caption         =   "&Close"
      Height          =   495
      Left            =   7020
      TabIndex        =   7
      Top             =   1185
      Width           =   1215
   End
   Begin VB.CommandButton btnCalc 
      Caption         =   "Calculate"
      Default         =   -1  'True
      Height          =   495
      Left            =   135
      TabIndex        =   6
      Top             =   1185
      Width           =   1215
   End
   Begin VB.TextBox txtID 
      Height          =   330
      Left            =   5685
      Locked          =   -1  'True
      TabIndex        =   3
      Top             =   90
      Width           =   2640
   End
   Begin VB.ComboBox cboDrives 
      Height          =   315
      ItemData        =   "FDiscID.frx":0000
      Left            =   1755
      List            =   "FDiscID.frx":0002
      Sorted          =   -1  'True
      Style           =   2  'Dropdown List
      TabIndex        =   1
      Top             =   90
      Width           =   1995
   End
   Begin VB.Label Label3 
      Caption         =   "Query string"
      Height          =   495
      Left            =   120
      TabIndex        =   4
      Top             =   480
      Width           =   1215
   End
   Begin VB.Label Label2 
      Caption         =   "Computed disc id"
      Height          =   495
      Left            =   4050
      TabIndex        =   2
      Top             =   90
      Width           =   1560
   End
   Begin VB.Label Label1 
      Caption         =   "Select drive from list"
      Height          =   285
      Left            =   120
      TabIndex        =   0
      Top             =   90
      Width           =   1800
   End
End
Attribute VB_Name = "FDiscID"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
' FDiscID.FRM
' Copyright (c) 1998, Fredrik Lindberg
'
' Test form to calculate CDDB DISC-ID
' The real code is in the CCd.CLS class module
'
Option Explicit

Private Sub btnCalc_Click()
   Dim MyCD As New CCd
   
   MyCD.Init cboDrives.Text
   txtID.Text = MyCD.DiscID
   txtQuery.Text = MyCD.QueryString
   
End Sub

Private Sub btnExit_Click()
   Unload Me
End Sub

Private Sub Form_Load()
   
   ' Fill this combobox with whatever values
   ' you need to be able to access the CDROM drive
   cboDrives.AddItem "D:"
   cboDrives.AddItem "E:"
   cboDrives.AddItem "F:"
   cboDrives.AddItem "G:"
   cboDrives.AddItem "H:"
   cboDrives.AddItem "I:"
   cboDrives.AddItem "R:"
   cboDrives.ListIndex = 0

End Sub
