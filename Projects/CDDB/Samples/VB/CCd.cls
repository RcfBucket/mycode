VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CCd"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
' CCd.CLS
' Copyright (c) 1998, Fredrik Lindberg
'
' Class interface to CD Audio records for computing
' CDDB disc id. It has been tested with Windows 98.
'
' Please feel free to use this code in your own projects.
'
' History
' 981112 Fredrik (fridden@hotmail.com) Initial coding
' 981126 Rick <activeasp@mpinet.net> Added sample QueryString
' 990128 Fredrik Corrected the Querystring with a lot of help from
' ...... ....... Thomas W Holt Jr and Steve Scherf at CDDB (Thanx!!)
'
Option Explicit

' *********************************************************
' Various types needed for use by mciSendCommand() function
' *********************************************************


' Structure needed for opening the CDROM device
Private Type MCI_OPEN_PARMS
   dwCallback As Long
   wDeviceID As Long
   lpstrDeviceType As String
   lpstrElementName As String
   lpstrAlias As String
End Type


' This structure is used when setting time format to be returned
Private Type MCI_SET_PARMS
   dwCallback As Long
   dwTimeFormat As Long
   dwAudio As Long
End Type

' This structure is used when accessing various status information
Private Type MCI_STATUS_PARMS
   dwCallback As Long
   dwReturn As Long
   dwItem As Long
   dwTrack As Integer
End Type

' The actual API function used when accessing the CD drive
Private Declare Function mciSendCommand Lib "winmm.dll" Alias "mciSendCommandA" _
   (ByVal wDeviceID As Long, ByVal uMessage As Long, ByVal dwParam1 As Long, ByRef dwParam2 As Any) As Long

' Error codes
Private Const MMSYSERR_NOERROR = 0

' Constants used together with mciSendCommand
Private Const MCI_CLOSE = &H804
Private Const MCI_FORMAT_MSF = 2
Private Const MCI_OPEN = &H803
Private Const MCI_OPEN_ELEMENT = &H200&
Private Const MCI_OPEN_TYPE = &H2000&
Private Const MCI_SET = &H80D
Private Const MCI_SET_TIME_FORMAT = &H400&

Private Const MCI_STATUS_ITEM = &H100&
Private Const MCI_STATUS_LENGTH = &H1&
Private Const MCI_STATUS_NUMBER_OF_TRACKS = &H3&
Private Const MCI_STATUS_POSITION = &H2&
Private Const MCI_TRACK = &H10&
Private Const MCI_STATUS = &H814

' Some instances of the structures declared above
Private mciOpenParms As MCI_OPEN_PARMS
Private mciSetParms As MCI_SET_PARMS
Private mciStatusParms As MCI_STATUS_PARMS

' Some own types needed
Private Type TTrackInfo
   Minutes As Long
   Seconds As Long
   Frames As Long
   FrameOffset As Long           ' Starting location in frames (used by QueryString)
End Type

' Private storage
Private m_Error As Long          ' Error code from API call
Private m_CID As String          ' Computed disc id
Private m_Drive As String        ' Drive letter
Private m_DeviceID As Long       ' Device Id
Private m_NTracks As Integer     ' Number of tracks in CD
Private m_Length As Long         ' Length of CD in seconds
Private m_Tracks() As TTrackInfo ' Track info for each and every track on the CD
                                 ' Zero based. Last index used for storing lead-out
                                 ' position information.

' ******************************************************************
' Initialize the class
' ******************************************************************
Private Sub Class_Initialize()
   m_CID = "(unavailable)"
   m_Drive = ""
   m_Error = 0
   m_DeviceID = -1
   m_NTracks = 0
End Sub

' ******************************************************************
' DiscID
' ******************************************************************
Public Property Get DiscID() As String
   DiscID = m_CID
End Property

' ******************************************************************
' ErrorCode
' ******************************************************************
Public Property Get ErrorCode() As Long
   Error = m_Error
End Property

' ******************************************************************
' Init - Initialize the new object. This will open the device
'        and retrieve the information we want
' ******************************************************************
Public Sub Init(sDrive As String)
   Dim p1 As Integer
   
   m_Error = MMSYSERR_NOERROR
   m_Drive = sDrive
   
   ' Open the CD
   If OpenCD Then
      Call LoadCDInfo
      CloseCD
   End If
   
End Sub

' ******************************************************************
' Class_Terminate
' ******************************************************************
Private Sub Class_Terminate()
   If m_DeviceID <> -1 Then
      CloseCD
   End If
End Sub


' *************************************************************************
' OpenCD - Open the CD Driver for use, we also set the time format
'           Returns device id for the opened CD
' *************************************************************************
Private Function OpenCD() As Boolean
   Dim sCode As Long, wDeviceID As Long
   
   OpenCD = False
   
   ' Setup the parameters used with mciSendCommand
   mciOpenParms.lpstrDeviceType = "cdaudio"
   mciOpenParms.lpstrElementName = m_Drive
   
   sCode = mciSendCommand(0, MCI_OPEN, (MCI_OPEN_TYPE Or MCI_OPEN_ELEMENT), mciOpenParms)
   If sCode <> MMSYSERR_NOERROR Then
      m_Error = sCode
      Exit Function
   End If
   
   ' Ok, save the device id for future calls
   m_DeviceID = mciOpenParms.wDeviceID
   
   ' Note the following code could just as well have been inside
   ' the LoadCDInfo sub instead.
   
   ' Set time format to minute/seconds/frame (MSF) format
   mciSetParms.dwTimeFormat = MCI_FORMAT_MSF
   sCode = mciSendCommand(m_DeviceID, MCI_SET, MCI_SET_TIME_FORMAT, mciSetParms)
   If sCode <> MMSYSERR_NOERROR Then
      m_Error = sCode
      sCode = mciSendCommand(m_DeviceID, MCI_CLOSE, 0, 0)  ' Dont forget to close it
      Exit Function
   End If

   ' We are done
   OpenCD = True
End Function

' *************************************************************************
' CloseCD - Close the CD
' *************************************************************************
Private Sub CloseCD()
   m_Error = mciSendCommand(m_DeviceID, MCI_CLOSE, 0, 0)
   m_DeviceID = -1
End Sub

' ******************************************************************************
' Private function to load all the info needed
' ******************************************************************************
Private Function LoadCDInfo() As Boolean
   Dim sCode As Long
   Dim p1 As Long, dwPosM As Long, dwPosS As Long, dwPosF As Long
   Dim dwLenM As Long, dwLenS As Long, dwLenF As Long, dwpos As Long
   Dim sum As Long, p2 As Long
      
   On Error Resume Next
   
   LoadCDInfo = False
   
   ' First get number of tracks
   mciStatusParms.dwItem = MCI_STATUS_NUMBER_OF_TRACKS
   sCode = mciSendCommand(m_DeviceID, MCI_STATUS, MCI_STATUS_ITEM, mciStatusParms)
   If sCode <> MMSYSERR_NOERROR Then
      m_Error = sCode
      Exit Function
   End If
   
   m_NTracks = mciStatusParms.dwReturn
   
   ' Allocate enough room for all the tracks, plus the extra info
   ' saved in the last element
   ReDim m_Tracks(m_NTracks + 1) As TTrackInfo
   
   ' Loop through all the tracks and get starting position
   For p1 = 1 To m_NTracks
      mciStatusParms.dwItem = MCI_STATUS_POSITION
      mciStatusParms.dwTrack = p1
      
      sCode = mciSendCommand(m_DeviceID, MCI_STATUS, MCI_STATUS_ITEM Or MCI_TRACK, mciStatusParms)
      If sCode <> MMSYSERR_NOERROR Then
         m_Error = sCode
         Exit Function
      End If
      
      ' We right shift the bits here, but I cheat and divide with some
      ' constants instead.
      '
      ' Note that m_Tracks() is zero based!
      '
      m_Tracks(p1 - 1).Frames = (mciStatusParms.dwReturn \ 65536) And &HFF
      m_Tracks(p1 - 1).Seconds = (mciStatusParms.dwReturn \ 256) And &HFF
      m_Tracks(p1 - 1).Minutes = (mciStatusParms.dwReturn) And &HFF
      
      ' I am saving the Frame Offset of the track for easy retrieval in the
      ' Query string function.
      m_Tracks(p1 - 1).FrameOffset = (m_Tracks(p1 - 1).Minutes * 60 * 75) + _
                                   (m_Tracks(p1 - 1).Seconds * 75) + _
                                   (m_Tracks(p1 - 1).Frames)
                                   
   Next p1
   
   ' Get total length of CD in seconds
   mciStatusParms.dwItem = MCI_STATUS_LENGTH
   mciStatusParms.dwTrack = m_NTracks
   
   sCode = mciSendCommand(m_DeviceID, MCI_STATUS, MCI_STATUS_ITEM Or MCI_TRACK, mciStatusParms)
   If sCode <> MMSYSERR_NOERROR Then
      m_Error = sCode
      Exit Function
   End If
   
   ' We now have the length of the last track
   dwLenM = (mciStatusParms.dwReturn) And &HFF
   dwLenS = (mciStatusParms.dwReturn \ 256) And &HFF
   dwLenF = ((mciStatusParms.dwReturn \ 65536) And &HFF) + 1
   
   ' Get the starting position of the last track
   dwPosM = m_Tracks(m_NTracks - 1).Minutes
   dwPosS = m_Tracks(m_NTracks - 1).Seconds
   dwPosF = m_Tracks(m_NTracks - 1).Frames
   
   ' Add them together to get the total length of the CD
   dwpos = (dwPosM * 60 * 75) + (dwPosS * 75) + dwPosF + _
           (dwLenM * 60 * 75) + (dwLenS * 75) + dwLenF
           
   
   ' Save it in the last element of m_Tracks() for later retrieval
   m_Tracks(m_NTracks).Frames = dwpos Mod 75
   dwpos = dwpos \ 75
   m_Tracks(m_NTracks).Seconds = dwpos Mod 60
   dwpos = dwpos \ 60
   m_Tracks(m_NTracks).Minutes = dwpos
   
   
   ' Now calculate the length by subtracting the starting position of the first
   ' track and the value calculated above
   m_Length = ((m_Tracks(m_NTracks).Minutes * 60) + (m_Tracks(m_NTracks).Seconds)) - _
              ((m_Tracks(0).Minutes * 60) + (m_Tracks(0).Seconds))
              
   
   ' Start calculating the CDDB Id.
   sum = 0
   For p1 = 0 To m_NTracks - 1
   
      ' Get current track position in seconds
      p2 = m_Tracks(p1).Minutes * 60 + m_Tracks(p1).Seconds
      
      ' Add each digit in P2 together and save in the "sum"
      Do While p2 > 0
         sum = sum + (p2 Mod 10)
         p2 = p2 \ 10
      Loop
   Next p1
   
   ' Now, sum contains the sum of all digits calculated from the
   ' length in seconds of each and every track
   
   ' Finally put the figures together. Once again I cheat to avoid overflow
   ' and other awful things when dealing with VBs Signed longs.
   m_CID = LCase$(LeftZeroPad(Hex$(sum Mod &HFF), 2) & LeftZeroPad(Hex$(m_Length), 4) & LeftZeroPad(Hex$(m_NTracks), 2))
   
   LoadCDInfo = True
End Function

' *****************************************************************************************
' QueryString
' *****************************************************************************************
Public Function QueryString() As String
   Dim p1 As Integer, s As String
    
   s = "cddb query " & m_CID & " " & m_NTracks
   
   For p1 = 0 To m_NTracks - 1
      s = s & " " & Format$(m_Tracks(p1).FrameOffset)
   Next
   
   
   ' Now here is a tricky part! The "length" used in the query is really the
   ' offset of the lead out in seconds, and not the same as the length used in
   ' calculating the disc id! We convert the offset to seconds.
   QueryString = s & " " & Format$(m_Tracks(m_NTracks).Minutes * 60) + (m_Tracks(m_NTracks).Seconds)
End Function

' *****************************************************************************************
' LeftZeroPad - Pad with zeros in the beginning of the string, if needed
' *****************************************************************************************
Private Function LeftZeroPad(s As String, n As Integer) As String
   If Len(s) < n Then
      LeftZeroPad = String$(n - Len(s), "0") & s
   Else
      LeftZeroPad = s
   End If
End Function
