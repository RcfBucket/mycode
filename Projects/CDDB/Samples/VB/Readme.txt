This is a Visual Basic 6 project for calculating
CDDB DISC-IDs.

To use this code in VB5 do as follows:

Create a new empty project in VB5
Add the file from my sample to the new project

That should work just fine.

The code to calculate the DISCID is taken from
the discid.c program found on CDDB's site
written by Thomas Chevereau.

Fredrik Lindberg
fridden@hotmail.com
Västerhöjdsgymnasiet Skövde

