unit CDDBMain;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, ScktComp;

type
  TForm1 = class(TForm)
    cs: TClientSocket;
    Memo1: TMemo;
    Button1: TButton;
    Button2: TButton;
    Memo2: TMemo;
    Button3: TButton;
    Memo3: TMemo;
    procedure csRead(Sender: TObject; Socket: TCustomWinSocket);
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure Button3Click(Sender: TObject);
  private
    procedure csHelp(Sender: TObject; Socket: TCustomWinSocket);
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

{$R *.DFM}

type
  TCDDB = class
  private
    fUserName: string;
    fPort         : integer;
    fClientName   : string;
    fHostName     : string;
    fSoftwareVer  : string;
    fClient       : TClientSocket;
    procedure SetUserName(const Value: string);
    procedure SetPort(const Value: integer);
    procedure SetClientName(const Value: string);
    procedure SetHostName(const Value: string);
    procedure SetSoftwareVer(const Value: string);
  public
    constructor Create; 
    property Port: integer read fPort write SetPort;
    property UserName: string read fUserName write SetUserName;
    property HostName: string read fHostName write SetHostName;
    property ClientName: string read fClientName write SetClientName;
    property SoftwareVersion: string read fSoftwareVer write SetSoftwareVer;
  end;


procedure TForm1.csHelp(Sender: TObject; Socket: TCustomWinSocket);
begin
  memo3.text:= cs.Socket.ReceiveText;
end;

procedure TForm1.csRead(Sender: TObject; Socket: TCustomWinSocket);
begin
  memo1.text:= memo1.text + cs.Socket.ReceiveText;
end;

procedure TForm1.Button1Click(Sender: TObject);
begin
  cs.Open;
  cs.OnRead:= csHelp;
  cs.Socket.SendText('cddb help'#10#13);
  cs.OnRead:= csRead;
end;

procedure TForm1.Button3Click(Sender: TObject);
begin
  cs.Socket.SendText(memo2.text);
end;

procedure TForm1.Button2Click(Sender: TObject);
begin
  cs.Socket.SendText('quit');
  cs.Close;
end;

{ TCDDB }

constructor TCDDB.Create;
begin
  inherited;
  fClient:= TClientSocket.Create(nil);
end;

procedure TCDDB.SetClientName(const Value: string);
begin
  fClientName := Value;
end;

procedure TCDDB.SetHostName(const Value: string);
begin
  fHostName := Value;
end;

procedure TCDDB.SetPort(const Value: integer);
begin
  fPort := Value;
end;

procedure TCDDB.SetSoftwareVer(const Value: string);
begin
  fSoftwareVer := Value;
end;

procedure TCDDB.SetUserName(const Value: string);
begin
  fUserName := Value;
end;


end.
