unit GridLib;

{---------------------------------------------------------------------------}
{ Multi purpose grid control (listbox)                                      }
{                                                                           }
{ This library supplies a grid control that can be used much like a list    }
{ box with exception that resizeable columns and column headers are         }
{ provided.                                                                 }
{                                                                           }
{ Date  Who    What                                                         }
{ ----- ------ -----------------------------------------------------------  }
{ 10/94 RCF    First stab at creating a custom control!                     }
{---------------------------------------------------------------------------}
{- NOTES:                                                                   }
{
   The following list box styles are mandatory for this grid to work
   properly. If a value of ON or OFF is specified, you must ensure
   that the setting is made as required. Optional means its up to
   you if you want to use the style.


   Attributes     : Tab Stop                  Optional
                    Disabled                  Optional
                    Group                     Optional
                    Border                    Optional
                    Visible                   Optional
   ScrollBar      : Vertical                  Optional
                    Horizontal                Optional
   OwnerDraw      : Variable (fixed may be used if the font will
                              not be changed at any time other
                              than SetupWindow.
   ListBox Styles : Notify                    ON
                    Sort                      optional
                    Multiple Select           optional
                    Don't Redraw              OFF
                    Tab Stops                 OFF
                    Integral Height           OFF
                    Multi Column              OFF
                    Pass keyboard input       optional
                    Extend Select             optional
                    Has Strings               OFF
                    Scroll bar always         optional

   IMPORTANT:
   =====================================================================
   The parent window owning a grid MUST call the following methods in
   response to their respective messages (which are unfortunately sent to
   the parent). Failure to call these methods will result in the grid
   not functioning properly.

   Message          Grid method to call (passing the untouched TMessage)
   ---------------- ----------------------------------------------------
   WM_DRAWITEM     DrawItem(msg: TMessage);
     Allows the grid to perform it's drawing.

   WM_MEASUREITEM  MeasureItem(msg: TMessage);
     Allows the grid to tell windows it's measurements.

   WM_CHARTOITEM   ChatToItem(msg: TMessage);
     Provides the ability to seek for items based on a keypress.

}

INTERFACE

Uses
  WinTypes,
  Win31,
  Biglist,
  PrnPrev,
  ODialogs,
  OWindows,
  Objects;

{$R GRIDLIB.RES}

const
  MaxGridColumns  = 25;
  MaxColumnData   = 255;  {- maximum number of characters in a column }

  HPageScroll     = 80;
  HCharScroll     = 40;

type
  TGridAlign = (Grid_Left, Grid_Center, Grid_Right);

  {- Row data has an array from 0..MaxGridColumns -1 for column data.
     Column [MaxGridColumns] is used for the grid sort string. }
  PRowData = ^TRowData;
  TRowData = object(TObject)
    itemData  : longint;  {- item data for row }
    data      : array[0..MaxGridColumns] of PChar;
    enabled   : boolean;
    constructor Init(aKey: PChar);
    destructor Done; virtual;
  end;

  TColData = record
    header   : PChar;    {- header text }
    alignment: word;     {- alignment for column }
    width    : integer;  {- width of column }
  end;

  PSortedRows = ^TSortedRows;
  TSortedRows = object(TLSortedCollection)
    constructor Init(aLimit, ADelta: longint);
    function Compare(key1, key2: Pointer): integer; virtual;
    function KeyOf(Item: Pointer): Pointer; virtual;
  end;

  PGrid = ^TGrid;
  TGrid = object(TListBox)
    keyDown   : boolean;    {- true if key currently down, false if up }
    {}
    constructor Init(aParent: PWindowsObject; anID, x, y, w, h, aCols: integer; bold: boolean);
    constructor InitResource(aParent: PWindowsObject; anID: integer; aCols: integer; bold: boolean);
    destructor Done; virtual;
    procedure SetupWindow; virtual;

    procedure StretchColumn(aCol: integer);
    procedure RightMouse(aCol: integer; aRow: longint); virtual;

    procedure EnableHeader(enabled: boolean);
    procedure EnableRow(aRow: longint; enabled: boolean);
    procedure EnableThreeD(enabled: boolean);
    procedure EnableResize(enabled: boolean);
    procedure EnableRightMouse(enabled: boolean);

    function  GetHeaderEnabled: boolean;
    procedure GetHeader(aCol: integer; aHeaderTxt: PChar; maxLen: integer);
    function  GetData(aCol: integer; aRow: longint; aStr: PChar; maxLen: integer): boolean;
    function  GetColumnWidth(aCol: integer): integer;
    function  GetSearchColumn: integer;
    function  GetRowEnabled(aRow: longint): boolean;
    function  GetResizeEnabled: boolean;
    function  GetItemData(aRow: longint): longInt; virtual;
    function  GetRowCount: longint;
    function  GetColumnCount: integer;
    function  GetRightMouseEnabled: boolean;

    function  SetHeader(aCol: integer; AHeaderTxt: PChar): boolean;
    function  SetColumnWidth(aCol, aWidth: integer): boolean;
    procedure SetAvgCharWidth(aCol, numChars: integer);
    procedure SetMaxCharWidth(aCol, numChars: integer);
    procedure OptColumnWidth(aCol: integer);
    procedure OptimizeGrid(stretchCol: integer);
    procedure SetRedraw(on: boolean);
    function  GetRedraw: boolean;
    function  SetAlignment(aCol: integer; anAlignment: TGridAlign): boolean;
    function  SetData(aCol: integer; aRow: longint; aStr: PChar): boolean;
    function  SetItemData(aRow, itemData: longint): boolean; virtual;
    function  SetSearchColumn(aCol: integer): boolean;

    procedure GetPrintBody(pInfo: PPrintInfo; useBorder: boolean);
    procedure GetPrintHeader(aPrnData: PPrintData; useBorder: boolean);

    function  FindString(st: PChar; idxStart: longint; exact: boolean): longint;
    function  SwapRows(row1, row2: longint): boolean;
    procedure ClearList; virtual;
    function BuildKeyStr(aCol: integer; aRow: longint; keyBuf: PChar; maxLen: integer; numeric: boolean): PChar; virtual;
    function  Sort(aCol, maxLen: integer; numeric: boolean): boolean; virtual;

    {- following routines must be called from parent in response to the
       WM_DRAWITEM, WM_MEASUREITEM, WM_CHARTOITEM messages respectively }
    procedure DrawItem(var msg: TMessage);
    procedure MeasureItem(var msg: TMessage);
    procedure CharToItem(var msg: TMessage);
    procedure WMKeyDown(var msg: TMessage); virtual WM_FIRST + WM_KEYDOWN;

    private
    isBold      : boolean;
    rMouseOK    : boolean;
    cols        : array[0..(MaxGridColumns - 1)] of TColData;
    rows        : PLCollection;
    numCols     : integer;
    headerHeight: integer;
    rowHeight   : integer;
    extent      : integer;  {- total extent of grid (total pixel width }
    physHScroll : integer;  {- actual pixels scrolled to the left }
    curClientX  : integer;  {- current client width }
    curWindowX  : integer;  { current window width minus borders }
    {- following fields are used when dragging column widths }
    captureON   : boolean;  {- true if capturing mouse movements }
    moveDC      : HDC;
    moveR       : TRect;
    moveOldROP  : integer;
    movePOS     : integer;
    moveOldPen  : HPen;
    moveCol     : integer;  {- column being resized }
    font        : HFont;
    sorting     : boolean;
    isFixed     : boolean;  {- true if fixed owner draw selected }
    searchCol   : integer;
    grayColor   : TColorRef;
    winBrush    : HBrush;
    showHeader  : boolean;
    redraw      : boolean;
    allowResize : boolean;
    threeDHead  : boolean;
    avgMult     : real;   {- used for SetAvgColumn widht }
    function Initialize(aCols: integer; bold: boolean): boolean;
    function InitColumns: boolean;
    function SetDataPrim(aCol: integer; aRow: longint; aStr: PChar): boolean;
    function GetDataPrim(aCol: integer; aRow: longint; aStr: PChar; maxLen: integer): boolean;
    procedure DrawHeader;
    function MouseOnLine: integer;
    function CalcColPos(aCol: integer): integer;
    function CountColWidths: integer;
    procedure WMHScroll(var msg: TMessage); virtual       WM_FIRST + WM_HSCROLL;
    procedure WMNCCalcSize(var msg: TMessage); virtual    WM_FIRST + WM_NCCALCSIZE;
    procedure WMEraseBkGnd(var msg: TMessage); virtual    WM_FIRST + WM_ERASEBKGND;
    procedure WMNCHitTest(var msg: TMessage); virtual     WM_FIRST + WM_NCHITTEST;
    procedure WMNCLButtonDown(var msg: TMessage); virtual WM_FIRST + WM_NCLBUTTONDOWN;
    procedure WMRButtonDown(var msg: TMessage); virtual   WM_FIRST + WM_RBUTTONDOWN;
    procedure WMRButtonUp(var msg: TMessage); virtual     WM_FIRST + WM_RBUTTONUP;
    procedure WMSize(var msg: TMessage); virtual          WM_FIRST + WM_SIZE;
    procedure WMMove(var msg: TMessage); virtual          WM_FIRST + WM_MOVE;
    procedure WMMouseMove(var msg: TMessage); virtual     WM_FIRST + WM_MOUSEMOVE;
    procedure WMLButtonDown(var msg: TMessage); virtual   WM_FIRST + WM_LBUTTONDOWN;
    procedure WMLButtonUp(var msg: TMessage); virtual     WM_FIRST + WM_LBUTTONUP;
    procedure WMSetCursor(var msg: TMessage); virtual     WM_FIRST + WM_SETCURSOR;
    procedure WMPaint(var msg: TMessage); virtual         WM_FIRST + WM_PAINT;
    procedure WMSetFont(var msg: TMessage); virtual       WM_FIRST + WM_SETFONT;
    procedure WMKeyUP(var msg: TMessage); virtual         WM_FIRST + WM_KEYUP;
    procedure LBAddString(var msg: TMessage); virtual     WM_FIRST + LB_ADDSTRING;
    procedure LBDeleteString(var msg: TMessage); virtual  WM_FIRST + LB_DELETESTRING;
    procedure LBInsertString(var Msg: TMessage); virtual  WM_FIRST + LB_INSERTSTRING;
    procedure LBGetItemData(var msg: TMessage); virtual   WM_FIRST + LB_GETITEMDATA;
    procedure LBSetItemData(var msg: TMessage); virtual   WM_FIRST + LB_SETITEMDATA;
    procedure LBSelectString(var msg: TMessage); virtual  WM_FIRST + LB_SELECTSTRING;
    procedure LBFindString(var msg: TMessage); virtual    WM_FIRST + LB_FINDSTRING;
    procedure LBFindExact(var msg: TMessage); virtual     WM_FIRST + LB_FINDSTRINGEXACT;
    procedure Paint(dc: HDC; var pi: TPaintStruct); virtual;
    procedure ResetGrid;
    procedure GetExtentAndMetrics(aStr: PChar; var te: longint; var tm: TTextMetric);
    procedure SetHorExtent(anExtent: integer);
  end;

IMPLEMENTATION

uses
  ApiTools,
  DMSDebug,
  DMSErr,
  INILib,
  IntlLib,
  MScan,
  WinProcs,
  Strings,
  StrsW;

{$I GRIDLIB.INC}

var
  hCurs       : HCursor;    {- resize cursor }
  unBoldFont  : HFont;      {- un bold font handle }
  exitSave    : Pointer;    {- exit routine save address }

{------------------------------------------------------------[ TSortedRows ]--}

constructor TSortedRows.Init(aLimit, ADelta: longint);
begin
  inherited Init(aLimit, aDelta);
  duplicates:= true;
end;

function TSortedRows.KeyOf(Item: Pointer): Pointer;
{- the string in the last column is the sort string. This data is outside of
   the normal grid columns }
begin
  KeyOf:= PRowData(item)^.data[MaxGridColumns];
end;

function TSortedRows.Compare(key1, key2: Pointer): integer;
begin
  Compare:= StrComp(key1, key2);
end;

{---------------------------------------------------------------[ TRowData ]--}

constructor TRowData.Init(AKey: PChar);
var
  k  : integer;

  procedure CleanUp(cleanTo: integer);
  var
    j   : integer;
  begin
    for j:= 0 to cleanTo do
      StrDispose(data[j]);
  end;

begin
  {- initialize all columns to nil string }
  {- for the key column [MaxGridColumns], set it to the value of AKey }
  for k:= 0 to MaxGridColumns do
  begin
    if (k = MaxGridColumns) and (AKey <> nil) and (StrLen(AKey) > 0) then
      data[k]:= StrNew(AKey)
    else
    begin
      GetMem(data[k], 1);
      if data[k] <> nil then
        data[k]^:= #0;
    end;
    if data[k] = nil then
    begin
      CleanUp(k-1);
      fail;
    end;
  end;
  itemData:= 0;
  enabled:= true;
  inherited Init;
end;

destructor TRowData.Done;
var
  k : integer;
begin
  inherited Done;
  {- get rid of all strings in row (if any) }
  for k:= 0 to MaxGridColumns do
    StrDispose(data[k]);
end;

{----------------------------------------------------------------[ TGrid ]--}

constructor TGrid.Init(AParent: PWindowsObject; AnID, x, y, w, h, aCols: integer; bold: boolean);
begin
  inherited Init(aParent, anID, x, y, w, h);
  if not Initialize(aCols, bold) then
  begin
    inherited Done;
    fail;
  end;
end;

constructor TGrid.InitResource(AParent: PWindowsObject; AnID, aCols: integer; bold: boolean);
begin
  inherited InitResource(AParent, AnID);
  if not Initialize(aCols, bold) then
  begin
    inherited Done;
    fail
  end;
end;

destructor TGrid.Done;
var
  k  : integer;
begin
  inherited Done;
  if rows <> nil then
    Dispose(rows, Done);
  for k:= 0 to (numCols - 1) do
    StrDispose(cols[k].header);
  DeleteObject(winBrush);
end;

procedure TGrid.SetupWindow;
var
  r      : TRect;
  k      : integer;
  w      : integer;
  wStyle : longint;
  pstr   : array[0..10] of char;
begin
  inherited SetupWindow;
  isFixed:= GetWindowLong(HWindow, GWL_STYLE) and LBS_OWNERDRAWFIXED <> 0;
  sorting:= GetWindowLong(HWindow, GWL_STYLE) and LBS_SORT <> 0;
  searchCol:= 0;
  if sorting then
    rows:= New(PSortedRows, Init(500, 100))
  else
    rows:= New(PLCollection, Init(500, 100));
  if rows = nil then
  begin
    FatalError('Fatal Error', 'Cannot initialize row data for grid.');
    Halt;
  end;

  GetClientRect(HWindow, r);
  curClientX:= r.right - r.left + 1;
  GetWindowRect(HWindow, r);
  curWindowX:= r.right - r.left + 1 - (GetSystemMetrics(SM_CXBORDER) * 2);

  pstr[1]:= #0;
  for k:= 0 to numCols - 1 do
  begin
    pstr[0]:= Char(Ord('A') + k);
    SetHeader(k, pstr);
  end;

  if not isBold then
    SendMessage(HWindow, WM_SETFONT, unBoldFont, 0);

  SetHorExtent(CountColWidths);
  ResetGrid;
end;

function TGrid.Initialize(aCols: integer; bold: boolean): boolean;
{- initialize the grid. returns true if oK, false otherwise }
var
  dc  : HDC;
  tm  : TTextMetric;
  ret : boolean;
begin
  ret:= true;
  attr.style:= attr.style or LBS_OWNERDRAWVARIABLE
                          or WS_HSCROLL
                          or LBS_NOTIFY
                          and not LBS_NOREDRAW
                          and not LBS_MULTICOLUMN
                          and not LBS_USETABSTOPS
                          and not LBS_HASSTRINGS
                          and not LBS_SORT;
  font:= 0;
  isBold:= bold;
  keyDown:= false;
  avgMult:= INIAvgColumnFluff;

  grayColor:= GetSysColor(COLOR_GRAYTEXT);
  winBrush:= CreateSolidBrush(GetSysColor(COLOR_WINDOW));
  ret:= winBrush <> 0;

  if ret then
  begin
    if aCols > MaxGridColumns then
      numCols:= MaxGridColumns
    else if aCols < 1 then
      numCols:= 1
    else
      numCols:= aCols;
    FillChar(cols, sizeof(cols), 0);

    {- calculate row heights }
    dc:= GetDC(HWindow);
    GetTextMetrics(dc, tm);
    ReleaseDC(HWindow, dc);
    rowHeight:= tm.tmHeight + tm.tmExternalLeading;
    headerHeight:= rowHeight + INIGridHeaderPad;

    showHeader:= true;
    extent:= 0;
    curClientX:= 0;
    curWindowX:= 0;
    physHScroll:= 0;
    captureON:= false;
    redraw:= true;
    allowResize:= true;
    threeDHead:= true;
    ret:= InitColumns;
  end;
  rows:= nil;
  rMouseOK:= false;
  Initialize:= ret;
end;

function TGrid.GetColumnCount: integer;
begin
  GetColumnCount:= numCols;
end;

function TGrid.GetRowCount: longint;
begin
  GetRowCount:= rows^.count;
end;

procedure TGrid.WMNCCalcSize(var msg: TMessage);
var
  lpnCSP : PNCCalcSize_Params;
  k      : integer;
begin
  lpnCSP:= PNCCalcSize_Params(msg.lParam);
  DefWndProc(msg);
  if showHeader then
  begin
    Inc(lpnCSP^.rgrc[0].top, headerHeight);
  end;
end;

procedure TGrid.LBDeleteString(var Msg: TMessage);
var
  k   : integer;
  row : integer;
begin
  row:= integer(msg.wParam);
  if (row >= 0) and (row < rows^.count) then
  begin
    for k:= row to rows^.count - 1 do
    begin
      SwapRows(k, k+1);
    end;
    UpdateWindow(HWindow);
    rows^.AtFree(rows^.count - 1);
    DefWndProc(msg);
  end
  else
    msg.Result:= LB_ERR;
end;

procedure TGrid.LBGetItemData(var msg: TMessage);
begin
  msg.result:= GetItemData(msg.wParam);
end;

procedure TGrid.LBSetItemData(var msg: TMessage);
begin
  if SetItemData(msg.wParam, msg.lParam) then
    msg.result:= 0
  else
    msg.result:= LB_ERR;
end;

procedure TGrid.LBAddString(var msg: TMessage);
var
  p     : PRowData;
  pstr  : PChar;
begin
  pstr:= PChar(msg.lParam);
  p:= New(PRowData, Init(pstr));
  if p = nil then
    msg.result:= LB_ERRSPACE
  else
  begin
    rows^.Insert(p);
    if rows^.status <> LCE_OK then
    begin
      msg.result:= LB_ERRSPACE;
      Dispose(p, Done);
    end
    else
    begin
      msg.lParam:= 0; {nil}
      DefWndProc(msg);
      if redraw then
        DrawHeader;
      if msg.result >= 0 then
        msg.result:= rows^.lastInsert;
    end;
  end;
end;

procedure TGrid.LBInsertString(var Msg: TMessage);
var
  k     : integer;
  index : integer;
  p     : PRowData;
begin
  index:= integer(msg.wParam);
  if (index < 0) or (index >= GetCount) then
     index:= rows^.count;
  p:= New(PRowData, Init(PChar(msg.lParam)));
  if p = nil then
    msg.result:= LB_ERRSPACE
  else
  begin
    rows^.AtInsert(index, p);
    if rows^.status <> LCE_OK then
    begin
      msg.result:= LB_ERRSPACE;
      Dispose(p, Done);
    end
    else
    begin
      msg.wParam:= index;   {- readjust msg for default }
      msg.lParam:= longint(p);
      DefWndProc(msg);
    end;
  end;
end;

function TGrid.FindString(st: PChar; idxStart: longint; exact: boolean): longint;
var
  pstr  : PChar;
  p     : PChar;
  p2    : array[0..200] of char;
  sel   : longint;
  found : boolean;
  ret   : longint;
begin
  ret:= LB_ERR;   {- assume the worst }
  pstr:= StrNew(st);
  if pstr <> nil then
  begin
    AnsiUpper(pstr);
    if rows^.count > 0 then
    begin
      sel:= idxStart + 1;
      found:= false;
      if (sel < rows^.count) and (sel >= 0) then
      begin
        found:= false;
        while not found and (sel < rows^.count) do
        begin
          GetDataPrim(searchCol, sel, p2, 200);
          AnsiUpper(p2);
          if exact then
            found:= StrComp(p2, pstr) = 0
          else
          begin
            p:= StrPos(p2, pstr);
            found:= (p <> nil) and (p = p2);
          end;
          if not found then
            Inc(sel);
        end;
      end;
      if found then
        ret:= sel;
    end;
    StrDispose(pstr);
  end;
  FindString:= ret;
end;

procedure TGrid.LBSelectString(var msg: TMessage);
begin
  msg.result:= FindString(PChar(msg.lParam), integer(msg.wParam), false);
  if msg.result <> LB_ERR then
    SetSelindex(integer(msg.Result));
end;

procedure TGrid.LBFindString(var msg: TMessage);
begin
  msg.Result:= FindString(PChar(msg.lParam), integer(msg.wParam), false);
end;

procedure TGrid.LBFindExact(var msg: TMessage);
begin
  msg.Result:= FindString(PChar(msg.lParam), integer(msg.wParam), true);
end;

procedure TGrid.WMHScroll;

  procedure ScrollBottom;
  begin
    physHScroll:= extent;
  end;

  procedure ScrollTop;
  begin
    physHScroll:= 0;
  end;

  procedure ScrollLineDown;
  begin
    Inc(physHScroll, HCharScroll);
  end;

  procedure ScrollLineUp;
  begin
    Dec(physHScroll, HCharScroll);
  end;

  procedure ScrollPageDown;
  begin
    Inc(physHScroll, hPageScroll);
  end;

  procedure ScrollPageUp;
  begin
    Dec(physHScroll, hPageScroll);
  end;

  procedure ScrollThumbTrack;
  begin
    physHScroll:= msg.lParamLo;
  end;

begin
  case msg.wParam of
    SB_BOTTOM       : ScrollBottom;
    SB_TOP          : ScrollTop;
    SB_ENDSCROLL    : ;
    SB_LINEDOWN     : ScrollLineDown;
    SB_LINEUP       : ScrollLineUp;
    SB_PAGEDOWN     : ScrollPageDown;
    SB_PAGEUP       : ScrollPageUp;
    SB_THUMBPOSITION: ;
    SB_THUMBTRACK   : ScrollThumbTrack;
  end;
  if physHScroll < 0 then
    physHScroll:= 0;
  if physHScroll > (extent - curClientX + 1) then
    physHScroll:= extent - curClientX + 1;
  SetScrollPos(HWindow, SB_HORZ, physHScroll, True);
  SetCursor(LoadCursor(0, IDC_ARROW));
  InvalidateRect(HWindow, nil, True);
  UpdateWindow(HWindow);
  msg.result:= 0;
end;

procedure TGrid.WMSize(var msg: TMessage);
var
  r : TRect;
begin
  inherited WMSize(msg);
  GetClientRect(HWindow, r);
  curClientX:= r.right - r.left + 1;
  if curClientX > extent then
    physHScroll:= 0;
  InvalidateRect(HWindow, nil, true);
  SetScrollPos(HWindow, SB_HORZ, physHScroll, True);
end;

procedure TGrid.WMMove(var msg: TMessage);
begin
  DefWndProc(msg);
  InvalidateRect(HWindow, nil, True);
end;

procedure TGrid.StretchColumn(aCol: integer);
{ stretch the specified column to fit in the client area. if total column
  widths are less than the client width then the specified column will be
  stretched to fit }
var
  w           : integer;
  vScrollBar  : boolean;
begin
  w:= CountColWidths;
  if (w < curWindowX) then
  begin
    if  (w <= (curWindowX - GetSystemMetrics(SM_CXVSCROLL))) and
        (not INIGridStretchFull) then
      w:= curWindowX - w + cols[aCol].width - GetSystemMetrics(SM_CXVSCROLL)
    else
      w:= curWindowX - w + cols[aCol].width - 1;
    SetColumnWidth(aCol, w);
  end;
end;

procedure TGrid.Paint(dc: HDC; var pi: TPaintStruct);
var
  ir     : TRect;
  w      : integer;
  max    : integer;
  curCol : integer;

  procedure DrawLine;
  begin
    if cols[curCol].width > 0 then
    begin
      ir.left:= w ;
      Inc(w, cols[curCol].width);
      ir.right:= w - physHScroll - 1;

      if (ir.right > 0) and (ir.right < max) then
      begin
        MoveTo(dc, ir.right, ir.top);
        LineTo(dc, ir.right, ir.bottom);
      end;
    end;
  end;

begin
  w:= 0;
  curCol:= 0;
  GetClientRect(HWindow, ir);
  max:= ir.right;
  for curCol:= 0 to numCols - 1 do
    DrawLine;
end;

procedure TGrid.wmSetFont(var msg: TMessage);
var
  k    : integer;
  dc   : HDC;
  tm   : TTextMetric;
  temp : integer;
  sz   : TRect;
begin
  DefWndProc(msg);

  font:= msg.WParam;

  dc:= GetDC(HWindow);
  SelectObject(dc, font);
  GetTextMetrics(dc, tm);

  rowHeight:= tm.tmHeight + tm.tmExternalLeading;
  headerHeight:= rowHeight + INIGridHeaderPad;

  ReleaseDC(HWindow, dc);

  {- the following, forces the grid to redraw. For some
     reason, without them the grid does not redraw properly }
  ResetGrid;
{  temp:= GetColumnwidth(0);
  SetColumnWidth(0, 1000);
  SetColumnWidth(0, temp);}

  SendMessage(HWindow, LB_RESETCONTENT, 0, 0);
  for k:= 1 to rows^.count do
    inherited AddString(rows^.At(k-1));
end;

procedure TGrid.wmPaint(var msg: TMessage);
var
  dc : HDC;
  ps : TPaintStruct;
begin
  DefWndProc(msg);
  dc:= GetDC(HWindow);
  Paint(dc, ps);

  ReleaseDC(HWindow, dc);
end;

function TGrid.MouseOnLine: integer;
{- return the column number if a mouse is on its right hand line }
{  return -1 if not on a line }
var
  pt     : TPoint;
  w      : integer;
  r      : TRect;
  aCol   : integer;
  found  : boolean;

  function CheckPos: boolean;
  begin
    Inc(w, cols[aCol].width);
    CheckPos:= (Abs(pt.x - w + 1) <= 1) and (cols[aCol].width > 0);
  end;

begin
  aCol:= -1;
  w:= 0;
  GetCursorPos(pt);
  ScreenToClient(HWindow, pt);
  GetClientRect(HWindow, r);
  if (pt.y < r.bottom) and (pt.x < r.right) then
  begin
    pt.x:= physHScroll + pt.x;
    if pt.x > 0 then
    begin
      aCol:= 0;
      found:= false;
      while not found and (aCol < numCols) do
      begin
        if CheckPos then
          found:= true
        else
          Inc(aCol);
      end;
      if not found then
        aCol:= -1;
    end;
  end;
  MouseOnLine:= aCol;
end;

procedure TGrid.WMSetCursor(var msg: TMessage);
begin
  if not captureON then
  begin
    if allowResize and (MouseOnLine <> -1) then
      SetCursor(hCurs)
    else
      DefWndProc(msg);
  end;
end;

procedure TGrid.wmLButtonDown(var msg: TMessage);
var
  pt  : TPoint;
  pen : HPen;
begin
  if captureOn then exit;

  if allowResize then
  begin
    moveCol:= MouseOnLine;
    if moveCol <> -1 then
    begin
      GetCursorPos(pt);
      ScreenToClient(hWindow, pt);
      pt.x:= CalcColPos(moveCol);
      captureON:= true;
      SetCapture(HWindow);
      moveDC:= GetWindowDC(HWindow);
      moveOldROP := SetROP2(moveDC, R2_NOTXORPEN);
      GetClientRect(HWindow, moveR);

      {- set moveR up so that left = limit for x movement, right = limit for right movement
         top and bottom define the length of line }
      Inc(moveR.top);
      if showHeader then
        Inc(moveR.bottom, headerHeight+1)
      else
        Inc(moveR.bottom);
      moveR.left:= CalcColPos(moveCol-1) + 1;

      pen:= CreatePen(PS_SOLID, 0, RGB($ff,0,0));
      moveOldPen:= SelectObject(moveDC, pen);
      MoveTo(moveDC, pt.x, moveR.top);
      LineTo(moveDC, pt.x, moveR.bottom);
      movePOS:= pt.x;
    end
    else
      DefWndProc(msg);
  end
  else
    DefWndProc(msg);
end;

procedure TGrid.WMMouseMove(var msg: TMessage);
var
  pt  : TPoint;
begin
  DefWndProc(msg);
  if allowResize then
  begin
    GetCursorPos(pt);
    ScreenToClient(hWindow, pt);

    Inc(pt.x);
    if captureON then
    begin
      if pt.x < moveR.left+2 then
        pt.x:= moveR.left+2;
      if pt.x > moveR.right then
        pt.x:= moveR.right;
      MoveTo(moveDC, movePos, moveR.top);
      LineTo(moveDC, movePOS, moveR.bottom);
      movePos:= pt.x;
      MoveTo(moveDC, movePos, moveR.top);
      LineTo(moveDC, movePOS, moveR.bottom);
    end;
  end;
end;

procedure TGrid.wmLButtonUp(var msg: TMessage);
begin
  if captureON then
  begin
    captureON:= false;
    MoveTo(moveDC, movePOS, moveR.top);
    LineTo(moveDC, movePOS, moveR.bottom);
    SetROP2(moveDC, moveOldROP);
    DeleteObject(SelectObject(moveDC, moveOldPen));
    ReleaseDC(HWindow, moveDC);
    ReleaseCapture;

    Dec(cols[moveCol].width, (CalcColPos(moveCol) - movePos));

    SetHorExtent(CountColWidths);
    ResetGrid;
    InvalidateRect(HWindow, nil, true);
  end;
  DefWndProc(msg);
end;

procedure TGrid.WMKeyUP(var msg: TMessage);
begin
  keyDown:= false;
  DefWndProc(msg);
end;

procedure TGrid.WMKeyDown(var msg: TMessage);
{- need to process VK_NEXT, since the default windows behaviour does not
   process page down for variable height list boxes. }
var
  r        : TRect;
  maxInWin : integer;
  cur      : longint;
  bot      : longint;
  top      : longint;
  pstr: array[0..100] of char;
begin
  keyDown:= true;
  if not isFixed and (msg.wParam = VK_NEXT) then
  begin
    cur:= GetSelIndex;
    if (cur < rows^.count) and (rows^.count > 0) then
    begin
      if cur = -1 then
        cur:= SendMessage(HWindow, LB_GETCARETINDEX, 0, 0);
      GetClientRect(HWindow, r);
      maxInWin:= (r.bottom + 1) div rowHeight;  {- max entries in window }
      Dec(maxInWin, 2);
      top:= cur;
      if cur + maxInWin >= rows^.count then
        bot:= rows^.count - 1
      else
        bot:= top + maxInWin;
      SendMessage(HWindow, WM_SETREDRAW, word(false), 0);
      SetSelIndex(integer(bot));
      SendMessage(HWindow, LB_SETTOPINDEX, Word(top), 0);
      SendMessage(HWindow, WM_SETREDRAW, word(true), 0);
      InvalidateRect(HWindow, nil, true);
    end;
  end
  else
    DefWndProc(msg);
end;

procedure TGrid.MeasureItem(var msg: TMessage);
{- tell windows the height of a row }
var
  p  : PMeasureItemStruct;
begin
  if HWindow = 0 then exit;
  p:= PMeasureItemStruct(msg.lParam);

  {- ensure the message was meant for me }
  if (p^.ctlType <> ODT_LISTBOX) or (msg.wParam <> GetID) then
    exit;

  DefWndProc(msg);
  p^.itemHeight:= rowHeight;
  msg.result:= 1;
end;

procedure TGrid.WMNCLButtonDown(var msg: TMessage);
begin
  WMLButtonDown(msg);
end;

procedure TGrid.RightMouse(aCol: integer; aRow: longint);
{- called when the mouse is right clicked on the grid. The right mouse
   must have been enabled for this function to be called. See
   EnableRightMouse method }
{$IFDEF DEBUG}
var
  pstr,p2: array[0..100] of char;
begin
  StrCopy(pstr, 'Column: ');
  str(aCol, p2);
  strcat(pstr, p2);
  strcat(pstr, #10);
  str(cols[aCol].width, p2);
  strcat(pstr, 'Width: ');
  strcat(pstr, p2);
  strcat(pstr, ' (pixels)'#10);
  strcat(pstr, '  Row: ');
  str(aRow, p2);
  strcat(pstr, p2);
  MessageBox(GetParent(HWindow), pstr, 'Right Click', MB_APPLMODAL);
{$ELSE}
begin
{$ENDIF}
end;

procedure TGrid.WMRButtonUp(var msg: TMessage);
begin
  if captureOn then exit;
  if rMouseOK then begin
    msg.message:= WM_LBUTTONUP;
    WMLButtonUp(msg);
    msg.result:= 0;
  end;
end;

procedure TGrid.WMRButtonDown(var msg: TMessage);
var
  pt     : TPoint;
  w      : integer;
  r      : TRect;
  aCol   : integer;
  found  : boolean;
begin
  if captureOn then exit;
  if rMouseOK then begin
    aCol:= -1;
    w:= 0;
    pt:= TPoint(msg.lParam);
    GetClientRect(HWindow, r);
    pt.x:= physHScroll + pt.x;
    if pt.x > 0 then
    begin
      aCol:= 0;
      w:= cols[0].width;
      found:= false;
      while not found and (aCol < numCols) do
      begin
        if pt.x < w then
          found:= true
        else
        begin
          Inc(aCol);
          Inc(w, cols[aCol].width);
        end;
      end;
      if not found then
        aCol:= -1;
    end;
    if aCol = -1 then
    begin
      {- not clicked on a valid column }
    end
    else
    begin
      msg.message:= WM_LBUTTONDOWN;  {- force focus to the row selected by right mouse }
      inherited WMLButtonDown(msg);
      msg.result:= 0;
      RightMouse(aCol, GetSelIndex);
    end;
  end
  else
    DefWndProc(msg);
end;

procedure TGrid.wmNCHitTest(var msg: TMessage);
var
  r : TRect;
begin
  GetWindowRect(HWindow, r);
  if (msg.LParamHi < r.top+headerHeight) and showHeader then
    msg.Result:= HTCLIENT
{  else if (msg.lParamLo >= r.right-1) then
    msg.result:= HTRIGHT}
  else
    DefWndProc(msg);
end;

function TGrid.CountColWidths: integer;
{- return total width of all columns in list box }
var
  c     : integer;
  total : integer;
begin
  total:= 0;
  for c:= 0 to numCols - 1 do
    Inc(total, cols[c].width);
  CountColWidths:= total;
end;

procedure TGrid.CharToItem(var msg: TMessage);
var
  k     : integer;
  sel   : integer;
  found : boolean;
  ch    : Char;
begin
  if msg.lParamLo <> HWindow then exit;

  msg.result:= 0;
  sel:= GetSelIndex;

  {What if no one is selected?}
  if (rows^.count > 0) and (sel >=0) then
  begin
    k:= sel + 1;
    if k >= rows^.count then
      k:= 0;
    found:= false;
    ch:= Upcase(char(msg.wParam));
    while not found and (k <> sel) do
    begin
      found:= Upcase(PRowData(rows^.At(k))^.data[searchCol][0]) = ch;
      if not found then
      begin
        Inc(k);
        if k = rows^.count then
          k:= 0;
      end;
    end;
    msg.result:= k;
  end;
end;

procedure TGrid.DrawItem(var msg: TMessage);
var
  ds    : PDrawItemStruct;
  p     : array[0..MaxColumnData] of char;
  p2    : array[0..2] of char;
  iR    : TRect;
  w     : integer;
  curCol: integer;
  ps    : TPaintStruct;
  dc    : HDC;
  cRect : TRect;

  procedure DrawIt;  {- draw a cell }
  var
    k  : integer;
    fmt: Word;
  begin
    with ds^ do
    begin
      if (integer(itemID) >= 0) and (cols[curCol].width > 0) then
      begin
        ir.left:= w - physHScroll;
        k:= cols[curCol].width;
        Inc(w, k);

        ir.Right:= w - physHScroll;
        if (ir.right >= cRect.left) and (ir.left <= cRect.right) then
        begin
          GetDataPrim(curCol, itemID, p, MaxColumnData); {- make row 1 based }
          if GetRowEnabled(itemID) then
          begin
            if (itemState and ODS_SELECTED) <> 0 then begin
              SetTextColor(hDC, GetSysColor(color_HighlightText));
              SetBkColor(hDC, GetSysColor(COLOR_HIGHLIGHT));
            end
            else begin
              SetTextColor(hDC, GetSysColor(color_WindowText));
              SetBkColor(hDC, GetSysColor(COLOR_WINDOW));
            end;
          end
          else
          begin
            SetTextColor(hdc, GetSysColor(COLOR_GRAYTEXT));
            SetBkColor(hdc, GetSysColor(COLOR_WINDOW));
          end;

          {- use ExtText to fill rectangle }
          ExtTextOut(hDC, ir.left+2, ir.top, ETO_CLIPPED or ETO_OPAQUE,
                     @ir, p2 , StrLen(p2), nil);
          Dec(ir.right, 2);
          Inc(ir.left, 2);
          fmt:= DT_NOPREFIX or DT_SINGLELINE or cols[curCol].alignment;
          {- use DrawText to format and print the text }
          DrawText(hdc, p, strlen(p), ir, fmt);
        end;
      end;
    end;
  end;

begin
if not redraw then exit;
  ds:= PDrawItemStruct(msg.lParam);

  GetClientRect(HWindow, cRect);

  {- ensure that the message was meant for me }
  if ds^.hWndItem <> HWindow Then exit;

  w:= 0;
  curCol:= 0;
  iR:= ds^.rcItem;
  strcopy(p2, '');

  for curCol:= 0 to numCols - 1 do
    DrawIt;

  with ds^ do
  begin
    {- paint remaining 'empty' area past last column, if any }
    ir.left:= w - physHScroll;
    ir.right:= ds^.rcItem.right;
    StrCopy(p, '');
    if (ir.right >= cRect.left) and (ir.left <= cRect.right) then
    begin
      SetTextColor(hDC, GetSysColor(color_WindowText));
      SetBkColor(hDC, GetSysColor(COLOR_WINDOW));
      ExtTextOut(ds^.hdc, ir.left, ir.top, ETO_CLIPPED or ETO_OPAQUE, @ir, p, 0, nil);
    end;

    {- check for focus and draw focus rect }
    if ((itemAction and ODA_FOCUS <> 0) and (itemState and ODS_FOCUS <> 0)) or
       ((itemState and ODS_SELECTED) <> 0) then
    begin
      if GetCount = 0 then
      begin
        Dec(rcItem.right);
        dc:= GetDC(HWindow);
        rcItem.bottom:= rowHeight;
        rcItem.top:= 0;
        SelectObject(dc, GetStockObject(GRAY_BRUSH));
        DrawFocusRect(dc, rcItem);
        ReleaseDC(HWindow, dc);
      end
      else
        DrawFocusRect(hdc, rcItem);
    end;
    if (GetCount = 0) and (itemState and ODS_FOCUS = 0) then
    begin
      { wipe out old focus rect ??? }
      DrawHeader;
    end;

  end;
  dc:= GetDC(HWindow);
  Paint(dc, ps);
  ReleaseDC(HWindow, dc);
  msg.result:= Ord(true);
end;

procedure TGrid.DrawHeader;
var
  dc       : HDC;
  r        : TRect;
  w        : integer;
  ir       : TRect;
  k        : integer;
  dkPen    : HPen;
  rgn      : HRGN;
  maxInWin : integer;
  topIdx   : integer;
  curCol   : integer;

  procedure DrawColHeader;
  var
    oPen  : HPen;
  begin
    if (cols[curCol].width > 0) then
    begin
      ir.left:= w - physHScroll + 1; {- make one based }
      Inc(w, cols[curCol].width);      {- inc to next col pos }
      ir.right:= w - physHScroll;

      if (ir.right >= r.left) and (ir.left <= r.right) then
      begin
        MoveTo(dc, ir.right, ir.top);  {- draw seperator line }
        LineTo(dc, ir.right, ir.bottom);

        {- draw header text }
        DrawText(dc, cols[curCol].header, strlen(cols[curCol].header), ir, DT_SINGLELINE or DT_CENTER or DT_VCENTER);

        {- make 3d appearance }
        if threeDHead then
        begin
          oPen:= SelectObject(dc, GetStockObject(WHITE_PEN));
          MoveTo(dc, ir.left, ir.bottom-2);
          LineTo(dc, ir.left, ir.top+1);
          LineTo(dc, ir.right-1, ir.top+1);
          SelectObject(dc, dkPen);
          LineTo(dc, ir.right-1, ir.bottom-2);
          LineTo(dc, ir.left, ir.bottom-2);
          SelectObject(dc, oPen);
        end;
      end;
    end;
  end;

begin
if not redraw then exit;
  GetWindowRect(HWindow, r);
  w:= r.right - r.left;  {- width of window }
  r.left:= 0;
  r.top:= 0;
  r.right:= w;
  r.bottom:= headerHeight + 1;

  {- get device context and create drawing tools }
  dc:= GetWindowDC(HWindow);
  if font <> 0 then
    SelectObject(dc, font);
  rgn:= CreateRectRgn(1, 1, r.right-1, r.bottom);
  SelectClipRgn(dc, rgn);
  dkPen:= CreatePen(PS_SOLID, 0, RGB(128, 128, 128));

  {- draw gray header }
  if showHeader then
  begin
    SelectObject(dc, GetStockObject(LTGRAY_BRUSH));
    Rectangle(dc, r.left, r.top, r.right, r.bottom);

    ir:= r;

    SetBkColor(dc, RGB(192, 192, 192));
    SetBkMode(dc, Transparent);
    w:= 0;
    for curCol:= 0 to numCols - 1 do
      DrawColHeader;
  end;

  SelectClipRgn(dc, 0);
  DeleteObject(rgn);

  {- clear any garbage at the end of the list box }
  k:= GetCount;
  GetClientRect(HWindow, r);
  maxInWin:= (r.bottom+1) div rowHeight;  {- max entries in window }
  topIdx:= SendMessage(HWindow, LB_GETTOPINDEX, 0, 0);
  if (k - topIdx) <= maxInWin then
  begin
    r.top:= (k - topIdx) * rowHeight + 1;

    if showHeader then
    begin
      Inc(r.top, headerHeight);
      Inc(r.bottom, headerHeight);
    end;
    SelectObject(dc, GetStockObject(NULL_PEN));
    SelectObject(dc, winBrush);
    Rectangle(dc, r.left+1, r.top, r.right+2, r.bottom+2);
    SelectObject(dc, GetStockObject(BLACK_PEN));
  end;

  DeleteObject(dkPen);
  ReleaseDC(HWindow,DC);
end;

procedure TGrid.WMEraseBkGnd(var msg: TMessage);
begin
  DrawHeader;
  msg.result:= 1;
end;

procedure TGrid.EnableRightMouse(enabled: boolean);
begin
  rMouseOK:= enabled;
end;

function TGrid.GetRightMouseEnabled: boolean;
begin
  GetRightMouseEnabled:= rMouseOK;
end;

procedure TGrid.EnableThreeD(enabled: boolean);
begin
  threeDHead:= enabled;
  InvalidateRect(HWindow, nil, true);
end;

function TGrid.GetResizeEnabled: boolean;
begin
  GetResizeEnabled:= allowResize;
end;

procedure TGrid.EnableResize(enabled: boolean);
begin
  allowResize:= enabled;
end;

function TGrid.GetHeaderEnabled: boolean;
begin
  GetHeaderEnabled:= showHeader;
end;

procedure TGrid.EnableHeader(enabled: boolean);
begin
  showHeader:= enabled;
  ResetGrid;
end;

procedure TGrid.GetExtentAndMetrics(aStr: PChar; var te: longint; var tm: TTextMetric);
{- called by AvgCharWidth and MaxCharWidth }
var
  dc    : HDC;
  w     : integer;
begin
  dc:= GetDC(HWindow);
  if font = 0 then
    SelectObject(dc, GetStockObject(SYSTEM_FONT))
  else
    SelectObject(dc, font);

  { low word contains the width }
  te:= (GetTextExtent(dc, aStr, StrLen(aStr)+1) and $FFFF);
  GetTextMetrics(dc, tm);
  ReleaseDC(HWindow, dc);
end;

procedure TGrid.SetAvgCharWidth(aCol, numChars: integer);
{- given a font, and a maximum number of characters, this function will
   attempt to calculate a reasonable width in characters }
{ Because the tmAveCharWidth field of TTextMetric is based on on the
  characters "a" - "z", an extra 25% is added to the reported length if based
  on this field. This will help account for "A" - "Z" and "0" - "9".}
var
  tm : TTextMetric;
  te : longint;
  num : integer;
begin
  GetExtentAndMetrics(cols[aCol].header, te, tm);

  {- round average column width up by the amount specified in the INI file.
     default is 33% }
  num:= integer(round(numChars * tm.tmAveCharWidth * avgMult));
  if (te > num) then
    SetColumnWidth(aCol, te)
  else
    SetColumnWidth(aCol, num);
end;

procedure TGrid.SetMaxCharWidth(aCol, numChars: integer);
{- given a font, and a maximum number of characters, this function will
   attempt to calculate a reasonable width in characters }
var
  tm    : TTextMetric;
  te    : longint;
begin
  GetExtentAndMetrics(cols[aCol].header, te, tm);
  if (te > (numChars * tm.tmMaxCharWidth)) then
    SetColumnWidth(aCol, te)
  else
    SetColumnWidth(aCol, numChars * tm.tmMaxCharWidth);
end;

function TGrid.SetColumnWidth(aCol, AWidth: integer): boolean;
begin
  if (aCol >= 0) and (aCol < numCols) and (AWidth >= 0) then
  begin
    cols[aCol].width:= aWidth;
    SetHorExtent(CountColWidths);
    ResetGrid;
    InvalidateRect(HWindow, nil, true);
    SetColumnWidth:= true
  end
  else
    SetColumnWidth:= false;
end;

procedure TGrid.OptColumnWidth(aCol: integer);
var
  dc    : HDC;
  te    : longint;
  w     : longint;
  aRow  : longint;
  aStr  : array[0..maxColumnData] of char;
begin
  dc:= GetDC(HWindow);
  if font = 0 then
    SelectObject(dc, GetStockObject(SYSTEM_FONT))
  else
    SelectObject(dc, font);
  { low word contains the width }
  w:= (GetTextExtent(dc, cols[aCol].header, StrLen(cols[aCol].header)+1) and $FFFF);
  for aRow:= 0 to rows^.count-1 do
  begin
    GetDataPrim(aCol, aRow, aStr, maxColumnData);
    { low word contains the width }
    te:= (GetTextExtent(dc, aStr, StrLen(aStr)+1) and $FFFF);
    if (te > w) then
      w:= te;
  end;
  ReleaseDC(HWindow, dc);
  SetColumnWidth(aCol, w);
end;

procedure TGrid.OptimizeGrid(stretchCol: integer);
{ All non-hidden (width > 0) columns will be optimized, then the specified   }
{ column will be stretched. If no stretch is desired, pass a negative number }
{ for strechCol.                                                             }
{----------------------------------------------------------------------------}
{       NOTE: HIDDEN COLUMNS (ZERO-WIDTH) WILL NOT BE OPTIMIZED!!!           }
{----------------------------------------------------------------------------}
var
  aCol : integer;
begin
  for aCol:= 1 to GetColumnCount-1 do
  begin
    if (GetColumnWidth(aCol) > 0) then
      OptColumnWidth(aCol);
  end;
  if (stretchCol >= 0) then
    StretchColumn(stretchCol);
end;

function TGrid.GetColumnWidth(aCol: integer): integer;
begin
  if (aCol >= 0) and (aCol < NumCols) then
    GetColumnWidth:= cols[aCol].width
  else
    GetColumnWidth:= 0;
end;

function TGrid.SetItemData(aRow, itemData: longint): boolean;
begin
  if (aRow < rows^.count) and (aRow >= 0) then
  begin
    PRowData(rows^.At(aRow))^.itemData:= itemData;
    SetItemData:= false;
  end
  else
    SetItemData:= false;
end;

function TGrid.GetItemData(aRow: longint): longInt;
{- return item data for a row }
begin
  if (aRow < rows^.count) and (aRow >= 0) then
  begin
    GetItemData:= PRowData(rows^.At(aRow))^.itemData;
  end
  else
    GetItemData:= 0;
end;

function TGrid.InitColumns: boolean;
{- initialize column data. return true if successful, false otherwise }
var
  k   : integer;
  err : boolean;

  procedure CleanUp(cleanFrom: integer);
  var
    j   : integer;
  begin
    for j:= 0 to cleanFrom do
      FreeMem(cols[j].header, 1);
  end;

begin
  err:= false;
  for k:= 0 to (numCols - 1) do
  begin
    GetMem(cols[k].header, 1);
    if cols[k].header = nil then
    begin
      err:= true;
      CleanUp(k-1);
      break;
    end
    else
    begin
      cols[k].header^:= #0;
      cols[k].alignment:= DT_LEFT;
      cols[k].width:= 0;
    end;
  end;
  if not err then
  begin
    extent:= CountColWidths;
    if HWindow <> 0 then
      SetHorExtent(extent);
  end;
  InitColumns:= not err;
end;

procedure TGrid.ClearList;
begin
  rows^.FreeAll;
  SendMessage(HWindow, LB_RESETCONTENT, 0, 0);
end;

function TGrid.BuildKeyStr(aCol: integer; aRow: longint; keyBuf: PChar; maxLen: integer; numeric: boolean): PChar;
{- generic function to build the key string to sort by.  The default behaviour
   is to extract the data from the specified column and then (if numeric) right
   justify it }
var
  tBuf: PChar;
begin
  GetMem(tBuf, maxLen+1);
  GetDataPrim(aCol, aRow, tBuf, maxLen);
  if numeric then
    LeftPad(keyBuf, tBuf, ' ', maxLen)
  else
    StrLCopy(keyBuf, tBuf, maxLen);
  FreeMem(tBuf, maxLen+1);
  BuildKeyStr:= keyBuf;
end;

function TGrid.Sort(aCol, maxLen: integer; numeric: boolean): boolean;
var
  retVal  : boolean;
  pstr1   : PChar;
  pstr2   : PChar;
  long1   : longint;
  long2   : longint;
  err     : boolean;

  procedure QSort(l,r: longint);
  var
    i,j : longint;
  begin
    i:= l;
    j:= r;
    BuildKeyStr(aCol, ((l+r) Div 2), pstr1, maxLen, numeric);
    repeat
      while (StrIComp(BuildKeyStr(aCol, i, pstr2, maxLen, numeric), pstr1) < 0) do
        Inc(i);
      while (StrIComp(pstr1, BuildKeyStr(aCol, j, pstr2, maxLen, numeric)) < 0) do
        Dec(j);
      if (i <= j) then begin
        SwapRows(i, j);
        Inc(i);
        Dec(j);
      end;
    until (i > j);
    if (l < j) then
      QSort(l, j);
    if (i < r) then
      QSort(i, r);
  end;

begin {QuickSort};
  retVal:= True;
  GetMem(pstr1, maxLen+1);
  GetMem(pstr2, maxLen+1);
  if (pstr1 = nil) or (pstr2 = nil) then
    retVal:= False
  else
  begin
    QSort(0, rows^.count-1);
    FreeMem(pstr1, maxLen+1);
    FreeMem(pstr2, maxLen+1);
  end;
  Sort:= retVal;
end;

function TGrid.SetAlignment(aCol: integer; anAlignment: TGridAlign): boolean;
{- set alignment type for a column }
begin
  if (aCol >= 0) and (aCol < numCols) then
  begin
    case anAlignment of
      grid_Center: cols[aCol].alignment:= DT_CENTER;
      grid_Right:  cols[aCol].alignment:= DT_RIGHT;
      else
        cols[aCol].alignment:= DT_LEFT;
    end;
    InvalidateRect(HWindow, nil, true);
    UpdateWindow(HWindow);
    SetAlignment:= true;
  end
  else
    SetAlignment:= false;
end;

function TGrid.SetDataPrim(aCol: integer; aRow: longint; aStr: PChar): boolean;
var
  r   : PRowData;
begin
  r:= rows^.At(aRow);
  StrDispose(r^.data[aCol]);
  if (aStr = nil) or (StrLen(aStr) = 0) then
  begin
    GetMem(r^.data[aCol], 1);
    r^.data[aCol]^:= #0;
  end
  else
    r^.data[aCol]:= StrNew(aStr);
  if r^.data[aCol] = nil then
    SetDataPrim:= false
  else
    SetDataPrim:= true;

  InvalidateRect(HWindow, nil, False);
end;

function TGrid.SetData(aCol: integer; aRow: longint; aStr: PChar): boolean;
begin
  if (aCol >= 0) and (aRow >= 0) and (aCol < NumCols) and (aRow < rows^.count) then
    SetData:= SetDataPrim(aCol, aRow, aStr)
  else
    SetData:= false;
end;

function TGrid.GetDataPrim(aCol: integer; aRow: longint; aStr: PChar; maxLen: integer): boolean;
var
  r : PRowData;
begin
  r:= rows^.At(aRow);
  StrLCopy(aStr, r^.data[aCol], maxLen);
  GetDataPrim:= True;
end;

function TGrid.GetData(aCol: integer; aRow: longint; aStr: PChar; maxLen: integer): boolean;
begin
  if (aCol >= 0) and (aRow >= 0) and (aCol < NumCols) and (aRow < rows^.count) then
    GetData:= GetDataPrim(aCol, aRow, aStr, maxLen)
  else
  begin
    StrCopy(aStr, '');
    GetData:= false;
  end;
end;

function TGrid.SetHeader(aCol: integer; aHeaderTxt: PChar): boolean;
begin
  SetHeader:= false;
  if (aCol >= 0) and (aCol < NumCols) then
  begin
    StrDispose(cols[aCol].header);
    if (aHeaderTxt = nil) or (StrLen(aHeaderTxt) = 0) then
    begin
      GetMem(cols[aCol].header, 1);
      cols[aCol].header^:= #0;
    end
    else
      cols[aCol].header:= StrNew(aHeaderTxt);
    SetHeader:= cols[aCol].header <> nil;
    InvalidateRect(HWindow, nil, true);
  end;
end;

procedure TGrid.GetHeader(aCol: integer; aHeaderTxt: PChar; maxLen: integer);
begin
  StrLCopy(aHeaderTxt, cols[aCol].header, maxLen);
end;

procedure TGrid.SetRedraw(on: boolean);
begin
  redraw:= on;
  if redraw then
  begin
    InvalidateRect(HWindow, nil, true);
    UpdateWindow(HWindow);
  end;
end;

function TGrid.GetRedraw: boolean;
begin
  GetRedraw:= redraw;
end;

function TGrid.SwapRows(row1, row2: longint): boolean;
{- swap the two specified rowss }
var
  k      : integer;
  pstr   : PChar;
  r1, r2 : PRowData;
  temp   : longint;
  tenabl : boolean;
  tData  : PChar;
begin
  if (row1 >= 0) and (row1 < rows^.count) and
     (row2 >= 0) and (row2 < rows^.count) and
     (row1 <> row2) then
  begin
    r1:= rows^.At(row1);
    r2:= rows^.At(row2);

    temp:= r1^.itemData;
    tenabl:= r1^.enabled;
    r1^.itemData:= r2^.itemData;
    r1^.enabled:= r2^.enabled;
    r2^.itemData:= temp;
    r2^.enabled:= tenabl;
    for k:= 0 to numCols - 1 do
    begin
      tData:= r1^.data[k];
      r1^.data[k]:= r2^.data[k];
      r2^.data[k]:= tData;
    end;
    EnableRow(row1, r1^.enabled);
    EnableRow(row2, r2^.enabled);

    InvalidateRect(HWindow, nil, false);
    SwapRows:= true;
  end
  else
    SwapRows:= false;
end;

function TGrid.CalcColPos(aCol: integer): integer;
{- for a given column, calculate its position on the screen }
var
  k, c : integer;
begin
  c:= 0;
  for k:= 0 to aCol do
    Inc(c, cols[k].width);
  CalcColPos:= c - physHScroll;
end;

function TGrid.GetSearchColumn: integer;
begin
  GetSearchColumn:= searchCol;
end;

function TGrid.SetSearchColumn(aCol: integer): boolean;
begin
  if (aCol >= 0) and (aCol < numCols) then
  begin
    searchCol:= aCol;
    SetSearchColumn:= true;
  end
  else
    SetSearchColumn:= false;
end;

procedure TGrid.GetPrintBody(pInfo: PPrintInfo; useBorder: boolean);
var
  c, row: integer;
  w     : longint;
  f     : longint;
  dc    : HDC;
  LTpPX : integer;
  LTpPY : integer;
  pstr  : array[0..255] of char;
  pstr2 : array[0..255] of char;
begin
  {- get cell flag }
  f:= c_Normal;
  if useBorder then
    f:= f or c_BorderLeft or c_BorderRight;

  {- get cell width in LTwips }
  dc:= GetDC(HWindow);
  SetLogicalTwips(dc, 1.0);
  LTwipsPerPixel(dc, LTpPX, LTpPY);
  ReleaseDC(HWindow, dc);

  for row:= 0 to rows^.count - 1 do
  begin
    pInfo^.body^.AddRow(r_Normal, 0);
    for c:= 0 to numCols - 1 do
    begin
      if cols[c].width > 0 then
      begin
        GetDataPrim(c, row, pstr, 255);
        StrCopy(pstr2, ' ');
        StrLCat(pstr2, pstr, SizeOf(pstr2)-1);
        w:= cols[c].width * LTpPX + LT25;
        if useBorder and (row = rows^.count - 1) then
          f:= f or c_BorderBottom;
        pInfo^.body^.AddCell(pstr2, w, f, 0);
      end;
    end;
  end;
end;

procedure TGrid.GetPrintHeader(aPrnData: PPrintData; useBorder: boolean);
{- this function copies the grid header into the print data object }
var
  k     : integer;
  w     : longint;
  f     : longint;
  dc    : HDC;
  LTpPX : integer;
  LTpPY : integer;
  pstr  : array[0..255] of char;
begin
  if useBorder then
    aPrnData^.AddRow(r_BorderBottom, 0)
  else
    aPrnData^.AddRow(r_Normal, 0);

  {- get cell width in LTwips }
  dc:= GetDC(HWindow);
  SetLogicalTwips(dc, 1.0);
  LTwipsPerPixel(dc, LTpPX, LTpPY);
  ReleaseDC(HWindow, dc);

  for k:= 0 to numCols - 1 do
  begin
    with cols[k] do
    begin
      if width > 0 then
      begin
        w:= width * LTpPX + LT25;
        StrCopy(pstr, ' ');
        StrLCat(pstr, header, SizeOf(pstr)-1);
        aPrnData^.AddCell(pstr, w, c_Bold, 0);
      end;
    end;
  end;
end;

procedure TGrid.ResetGrid;
var
  th  : THandle;
begin
  th:= BeginDeferWindowPos(1);
  DeferWindowPos(th, HWindow, HWND_TOP, attr.x, attr.y, attr.w, attr.h, SWP_NOZORDER or SWP_NOMOVE);
  EndDeferWindowPos(th);
end;

procedure TGrid.EnableRow(aRow: longint; enabled: boolean);
begin
  if (aRow >= 0) and (aRow < rows^.count) then
  begin
    PRowData(rows^.At(aRow))^.enabled:= enabled;
    InvalidateRect(HWindow, nil, false);
  end;
end;

function TGrid.GetRowEnabled(aRow: longint): boolean;
begin
  if (aRow >= 0) and (aRow < rows^.count) then
  begin
    GetRowEnabled:= PRowData(rows^.At(aRow))^.enabled;
  end
  else
    GetRowEnabled:= false;
end;

procedure TGrid.SetHorExtent(anExtent: integer);
var
  r   : TRect;
begin
  GetClientRect(HWindow, r);
  curClientX:= r.right - r.left + 1;

  extent:= anExtent;
  SendMessage(HWindow, LB_SETHORIZONTALEXTENT, extent, 0);
  HandleEvents(HWindow);

  if extent < curClientX then
    extent:= curClientX;

  SetScrollRange(HWindow, SB_HORZ, 0, extent - curClientX, false);
  HandleEvents(HWindow);

  SetScrollPos(HWindow, SB_HORZ, physHScroll, redraw);
  HandleEvents(HWindow);
end;

procedure Initialize;
var
  lf      : TLogFont;
begin
  MakeScreenFont(lf, false, false);
  unBoldfont:= CreateFontIndirect(lf);

  hCurs:= LoadCursor(HInstance, MakeIntresource(CSR_LEFTRIGHT));
  if hCurs = 0 then
  begin
    ShowError(nil, IDS_GRIDERR1, nil, MOD_GRIDLIB, MOD_GRIDLIB, 0);
    Halt;
  end;
end;

procedure ExitRoutine; far;
begin
  exitProc:= exitSave;
  DestroyCursor(hCurs);
  if unBoldFont <> 0 then
    DeleteObject(unBoldFont);
end;

BEGIN
  Initialize;
  exitSave:= ExitProc;
  ExitProc:= @ExitRoutine;
END.

{- rcf }
