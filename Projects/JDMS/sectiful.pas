{$F+}
unit SectIFull;

{- isolate detail (full) section driver }

INTERFACE

{- this module interfaces via SECTREG }

IMPLEMENTATION

uses
  DMSDebug,
  DBLib,
  DMSErr,
  DMString,
  MScan,
  DBFile,
  DBTypes,
  ctllib,
  SectReg,
  SpinEdit,
  ApiTools,
  IntlLib,
  gridlib,
  DlgLib,
  RptUtil,
  RptShare,
  BoxWin,
  OWindows,
  ODialogs,
  WinProcs,
  WinTypes,
  DBIDS,
  Strings;

{$I RPTGEN.INC}

type
  PSectIsoFullOpts = ^TSectIsoFullOpts;
  TSectIsoFullOpts = record
    isoPerSect      : integer;     {- number of isos per section }
    separators      : integer;     {- border style -1=none, or PS_xxx constant }
    isoWidth        : longint;     {- width of isolate column }
    detail          : TDetailList; {- isolate detail columns }
    options         : TSectOpt;    {- section options/font }
    font            : TRptFont;    {- section font }
    isoFlds         : TRptFldList; {- selected iso field numbers }
    lblWidth        : longint;     {- width of isolate field label }
    valWidth        : longint;     {- width of isolate field value }
    subSpacing      : longint;     {- spacing between isolate sub columns }
  end;

  PSectIsoFull = ^TSectIsoFull;
  TSectIsoFull = object(TBoxWin)
    isoCfg   : TSectIsoFullOpts;
    isoDB    : PDBFile;
    constructor Init(AParent: PWindowsObject; ATitle: PChar;
                     rect: TRect; AnOrigin: TPoint; AZoom: Real;
                     APage: TRptPage; aFont: TRptFont);
    procedure DefineBox(aParent: PWindowsObject); virtual;
    procedure ResetDefaults; virtual;
    function GetBoxType: integer; virtual;

    function SavePrep: boolean; virtual;
    function SaveData(var dataSeq: longint): boolean; virtual;
    procedure SaveComplete; virtual;
    function LoadData(aBoxSeq: longint): boolean; virtual;
  end;

  PIsoFullEdit = ^TIsoFullEdit;
  TIsoFullEdit = object(TCenterDlg)
    colSep    : PComboBox;
    grid      : PCheckGrid;
    cfg       : PSectIsoFullOpts;
    tCfg      : TSectIsoFullOpts;
    sectWidth : longint;
    isoPer    : PSpinInt;
    width     : PSpinReal;
    subSpace  : PSpinReal;
    {}
    constructor Init(AParent: PWindowsObject; var aCfg: TSectIsoFullOpts; aSectWidth: longint);
    procedure SetupWindow; virtual;
    procedure IDOptions(var msg: TMessage);     virtual ID_FIRST + IDC_OPTIONS;
    procedure IDFont(var msg: TMessage);        virtual ID_FIRST + IDC_FONT;
    procedure IDUP(var msg: TMessage);          virtual ID_FIRST + IDC_UP;
    procedure IDDown(var msg: TMessage);        virtual ID_FIRST + IDC_DOWN;
    procedure IDIsoFlds(var msg: TMessage);     virtual ID_FIRST + IDC_ISOFLDS;
    procedure IDWidth(var msg: TMessage);       virtual ID_FIRST + IDC_WIDTH;
    procedure WMDrawItem(var msg: TMessage);    virtual WM_FIRST + WM_DRAWITEM;
    procedure WMMeasureItem(var msg: TMessage); virtual WM_FIRST + WM_MEASUREITEM;
    procedure WMCharToItem(var msg: TMessage);  virtual WM_FIRST + WM_CHARTOITEM;
    function CanClose: boolean; virtual;
  end;

  PIsoFldsDlg = ^TIsoFldsDlg;
  TIsoFldsDlg = object(TDMSDlg)
    selFlds   : PGrid;
    isoFlds   : PGrid;
    cfg       : PSectIsoFullOpts;
    tcfg      : TSectIsoFullOpts;
    lblWidth  : PSpinReal;
    valWidth  : PSpinReal;
    db        : PDBReadDesc;
    curIsoWidth: longint;
    constructor Init(aParent: PWindowsObject; aCfg: PSectIsoFullOpts; curIsoWid: longint);
    destructor Done; virtual;
    procedure SetupWindow; virtual;
    procedure WMDrawItem(var msg: TMessage);    virtual WM_FIRST + WM_DRAWITEM;
    procedure WMMeasureItem(var msg: TMessage); virtual WM_FIRST + WM_MEASUREITEM;
    procedure WMCharToItem(var msg: TMessage);  virtual WM_FIRST + WM_CHARTOITEM;
    procedure WMCommand(var msg: TMessage);     virtual WM_FIRST + WM_COMMAND;
    procedure Add(var msg: TMessage);           virtual ID_FIRST + IDC_ADD;
    procedure Remove(var msg: TMessage);        virtual ID_FIRST + IDC_REMOVE;
    procedure Up(var msg: TMessage);            virtual ID_FIRST + IDC_UP;
    procedure Down(var msg: TMessage);          virtual ID_FIRST + IDC_DOWN;
    procedure DoEnable;
    function CanClose: boolean; virtual;
  end;

var
  sectionName : array[0..30] of char;

{---------------------------------[ TSectIsoSum ]--}

constructor TSectIsoFull.Init(AParent: PWindowsObject; ATitle: PChar;
                          rect: TRect; AnOrigin: TPoint; AZoom: Real;
                          APage: TRptPage; AFont: TRptFont);
begin
  isoDB:= nil;
  inherited Init(AParent, ATitle, rect, AnOrigin, AZoom, APage, AFont);
end;

procedure TSectIsoFull.ResetDefaults;
var
  k  : integer;
begin
  FillChar(isoCfg, sizeof(isoCfg), 0);
  with isoCfg do
  begin
    separators:= PS_SOLID;
    isoPerSect:= 3;

    lblWidth:= 1440;
    valWidth:= 1440;
    subSpacing:= 1440 div 8;  {- 1/8 inch }

    MakeDefaultRFont(font);
    with options do
    begin
      bkMode:= OPAQUE;
      border:= -1;  {- none }
    end;

    isoFlds[0]:= 1; {- count }
    isoFlds[1]:= DBISOIso; {- isolate field}

    { 0-drug abbreviation, 1-Drug name, 2-Alternate drug abbrev,
      3-MIC, 4- Interpretation }
    for k:= 1 to 5 do
      detail[k].detailType:= k - 1;  {- set up all five MIC information types }

    detail[1].visible:= true; {- drug }
    detail[1].width:= 1440;   {- 1 inch }

    detail[4].visible:= true; {- MIC }
    detail[4].width:= (1440 * 3) div 4;  {- 3/4 inch }

    detail[5].visible:= true; {- interp }
    detail[5].width:= (1440 * 3) div 4;  {- 3/4 inch }

    isoWidth:= 0;
    for k:= 1 to 5 do
      if detail[k].visible then
      begin
        if isoWidth > 0 then
          Inc(isoWidth, subSpacing);
        Inc(isoWidth, detail[k].width);
      end;

    if isoWidth = 0 then
      isoWidth:= 1440*2;  {- some default }
  end;
end;

function TSectIsoFull.GetBoxType: integer;
begin
  GetBoxType:= SectIsoFullID;
end;

function TSectIsoFull.SavePrep: boolean;
{- open database files and prepare to save }
begin
  isoDB:= New(PDBFile, Init(DBRptIsoFulFile, '', dbOpenNormal));
  if isoDB = nil then
  begin
    ShowError(GetParentObj, IDS_CANTOPENIFULL, nil, dbLastOpenError, MOD_RPTGEN, 1);
  end;
  SavePrep:= isoDB <> nil;
end;

function TSectIsoFull.SaveData(var dataSeq: longint): boolean;
var
  ret   : boolean;
  sz    : TLRect;
begin
  ret:= true;
  if isoDB <> nil then
  begin
    GetSize(sz);

    with isoDB^, dbr^ do
    begin
      PutField(DBRptIsoFullIsoPerSect,  @isoCfg.isoPerSect);
      PutField(DBRptIsoFullSeparators,  @isoCfg.separators);
      PutField(DBRptIsoFullIsoWidth,    @isoCfg.isoWidth);
      PutField(DBRptIsoFullDetail,      @isoCfg.detail);
      PutField(DBRptIsoFullIsoFlds,     @isoCfg.isoFLds);
      PutField(DBRptIsoFullLblWidth,    @isoCfg.lblWidth);
      PutField(DBRptIsoFullValWidth,    @isoCfg.valWidth);
      PutField(DBRptIsoFullSubSpace,    @isoCfg.subSpacing);
      PutField(DBRptIsoFullFont,        @isoCfg.font);
      PutField(DBRptIsoFullOptions,     @isoCfg.options);
      PutField(DBRptIsoFullSize,        @sz);
      dbc^.InsertRec(dbr);
      ret:= dbc^.dbErrorNum = 0;
      if not ret then
      begin
        ShowError(GetParentObj, IDS_CANTINSERTIFULL, nil, dbc^.dbErrornum, MOD_RPTGEN, 0);
        dataSeq:= -1;
      end
      else
        dataSeq:= GetSeqValue;
    end;
  end
  else
    ret:= false;
  SaveData:= ret;
end;

procedure TSectIsoFull.SaveComplete;
begin
  MSDisposeObj(isoDB);
end;

function TSectIsoFull.LoadData(aBoxSeq: longint): boolean;
var
  db    : PDBFile;
  ret   : boolean;
  sz    : TLRect;
begin
  db:= New(PDBFile, Init(DBRptIsoFulFile, '', dbOpenNormal));
  if db = nil then
  begin
    ret:= false;
    ShowError(GetParentObj, IDS_CANTOPENIFULL, nil, dbLastOpenError, MOD_RPTGEN, 2);
  end
  else
  begin
    with db^, dbr^ do
    begin
      boxSeq:= aBoxSeq;
      dbc^.GetSeq(dbr, boxSeq);
      if dbc^.dbErrorNum = 0 then
      begin
        GetField(DBRptIsoFullIsoPerSect, @isoCfg.isoPerSect, sizeof(isoCfg.isoPerSect));
        GetField(DBRptIsoFullSeparators, @isoCfg.separators, sizeof(isoCfg.separators));
        GetField(DBRptIsoFullIsoWidth,   @isoCfg.isoWidth, sizeof(isoCfg.isoWidth));
        GetField(DBRptIsoFullDetail,     @isoCfg.detail, sizeof(isoCfg.detail));
        GetField(DBRptIsoFullIsoFlds,    @isoCfg.isoFLds, sizeof(isoCfg.isoFlds));
        GetField(DBRptIsoFullLblWidth,   @isoCfg.lblWidth, sizeof(isoCfg.lblWidth));
        GetField(DBRptIsoFullValWidth,   @isoCfg.valWidth, sizeof(isoCfg.valWidth));
        GetField(DBRptIsoFullSubSpace,   @isoCfg.subSpacing, sizeof(isoCfg.subSpacing));
        GetField(DBRptIsoFullFont,       @isoCfg.font, sizeof(isoCfg.font));
        GetField(DBRptIsoFullOptions,    @isoCfg.options, sizeof(isoCfg.options));
        GetField(DBRptIsoFullSize,       @sz, sizeOf(sz));

        SetSize(sz);
      end
      else
      begin
        ShowError(GetParentObj, IDS_CANTLOCSECTRECT, nil, dbc^.dbErrorNum, MOD_RPTGEN, 3);
        CloseWindow;  {- delete the section from the report }
        boxSeq:= -1;
        ret:= false;
      end;
    end;
    MSDisposeObj(db);
  end;
  LoadData:= ret;
end;

procedure TSectIsoFull.DefineBox(aParent: PWindowsObject);
var
  p  : PIsoFullEdit;
  sz : TLRect;
begin
  GetSize(sz);
  p:= New(PIsoFullEdit, Init(aParent, isoCfg, sz.right));
  if application^.ExecDialog(p) = IDOK then
  begin
    SetDisplayFont(isoCfg.Font);
  end;
end;


{-------------------------[ TIsoFullEdit ]--}

constructor TIsoFullEdit.Init(aParent: PWindowsObject; var ACfg: TSectIsoFullOpts; aSectWidth: longint);
var
  c   : PControl;
  max : real;
  k   : integer;
begin
  inherited Init(aParent, MakeIntResource(DLG_ISOFULL));
  for k:= 400 to 403 do
    c:= New(PLStatic, InitResource(@self, k, 0, false));

  c:= New(PLStatic, InitResource(@self, 300, 0, false));
  c:= New(PLStatic, InitResource(@self, 301, 0, false));

  isoPer:= New(PSpinInt, InitResource(@self, IDC_ISOPERSECT, 0, 0, 1, maxIsoPerSect, 1));
  colSep:= New(PComboBox, InitResource(@self, IDC_COLUMNSEP, 0));

  LTwipsToUnits(14*1440, max);
  width:= New(PSpinReal, InitResource(@self, IDC_ISOWIDTH, 0, 0, 0, max, 0.1));
  subSpace:= New(PSpinReal, InitResource(@self, IDC_MICSPACE, 0, 0, 0, max, 0.1));

  grid:= New(PCheckGrid, InitResource(@self, IDC_MICINFO, 3, false, 0));

  sectWidth:= aSectWidth;
  cfg:= @aCfg;
  tCfg:= aCfg;
end;

procedure TIsoFullEdit.SetupWindow;
var
  k        : integer;
  j        : longint;
  pstr, p2 : array[0..100] of char;
  num      : real;
begin
  inherited SetupWindow;

  UnitsStr(true, p2, 100);
  for k:= 300 to 301 do
    SendDlgItemMsg(301, WM_SETTEXT, 0, longint(@p2));

  {- setup grid columns }
  grid^.SetSearchColumn(1);
  grid^.SetHeader(0, '');
  grid^.SetHeader(1, SR(IDS_FIELD, p2, 100));
  SR(IDS_WIDTH, pstr, 100);
  StrCat(pstr, ' (');
  StrCat(pstr, p2);
  StrCat(pstr, ')');
  grid^.SetHeader(2, pstr);

  grid^.SetColumnWidth(0, 20);
  grid^.SetColumnWidth(1, 180);
  grid^.SetColumnWidth(2, 10);
  grid^.StretchColumn(2);
  grid^.SetAlignment(0, GRID_CENTER);
  grid^.SetAlignment(2, GRID_RIGHT);

  {- fill MIC sub columns }
  for k:= 1 to 5 do
  begin
    with cfg^.detail[k] do
    begin
      j:= grid^.AddString(nil);

      if visible then
        grid^.SetCheck(j, true);

      grid^.SetData(1, j, SR(IDS_MICFldInfo + detailType, pstr, 100));

      LTwipsToUnits(width, num);
      IntlRealToStr(num, pstr, 100);
      grid^.SetData(2, j, pstr);

      grid^.SetItemData(j, detailType);
    end;
  end;
  grid^.SetSelIndex(0);

  FillLineTypes(HWindow, colSep);
  colSep^.SetSelIndex(0);
  {- select the separater }
  for k:= 0 to colSep^.GetCount - 1 do
  begin
    j:= SendDlgItemMsg(IDC_COLUMNSEP, CB_GETITEMDATA, k, 0);
    if integer(j) = cfg^.separators then
      colSep^.SetSelIndex(k);
  end;

  isoPer^.SetNum(tcfg.isoPerSect);

  LTwipsToUnits(tcfg.isoWidth, num);
  width^.SetNum(num);
  LTwipsToUnits(tcfg.subSpacing, num);
  subSpace^.SetNum(num);
end;

procedure TIsoFullEdit.IDOptions(var msg: TMessage);
begin
  GetSectOpt(@self, tCfg.options);
end;

procedure TIsoFullEdit.IDFont(var msg: TMessage);
begin
  GetAFont(HWindow, tcfg.font);
end;

function TIsoFullEdit.CanClose: boolean;
var
  ret      : boolean;
  pstr     : array[0..200] of char;
  p2       : array[0..100] of char;

  procedure CheckSubCols;
  {- check isolate sub column widths }
  var
    k     : integer;
    tot   : longint;
    dt    : longint;
    num   : real;
    lt    : longint;
    err   : boolean;
    count : integer;
  begin
    tot:= 0;
    k:= 0;
    count:= 0;
    while ret and (k < grid^.GetRowCount) do
    begin
      dt:= grid^.GetItemData(k);
      tcfg.detail[k+1].detailType:= integer(grid^.GetItemData(k));
      if grid^.GetCheck(k) then
      begin
        tcfg.detail[k+1].visible:= true;
        grid^.GetData(2, k, pstr, sizeof(pstr)-1);
        num:= IntlStrToReal(pstr, err);
        if err then
        begin
          ret:= false;
          ShowError(@self, IDS_INVNUMFORMAT, nil, 0, MOD_RPTGEN, 2);
          FocusCtl(HWindow, grid^.GetID);
          grid^.SetSelIndex(k);
        end
        else
        begin
          UnitsToLTwips(num, lt);
          Inc(tot, lt);
          Inc(count);
          tcfg.detail[k+1].width:= lt;
        end;
      end
      else
      begin
        tcfg.detail[k+1].visible:= false;
        tcfg.detail[k+1].width:= 0;
      end;
      Inc(k);
    end;
    if ret then
    begin
      if count > 0 then
        Inc(tot, tcfg.subSpacing * (count-1));
      if tot > tcfg.isoWidth then
      begin
        ret:= false;
        ShowError(@self, IDS_IFULLWIDTHERR, nil, 0, MOD_RPTGEN, 0);
      end;
    end;
  end;

begin
  ret:= ExtractNum(@self, isoPer, IDS_ERRISOPER, tcfg.isoPerSect, 1, MaxIsoPerSect);

  if ret then
    ret:= ExtractWidth(@self, width, IDS_ERRWIDTH, tcfg.isoWidth, 0, 1440*14);

  if ret then
  begin
    tcfg.separators:= SendDlgItemMsg(IDC_COLUMNSEP, CB_GETITEMDATA, colSep^.GetSelIndex, 0);
  end;

  if ret then
    ret:= ExtractWidth(@self, subSpace, IDS_ERRSPACING, tcfg.subSpacing, 0, 1440);

  if ret then
    CheckSubCols;

  if ret and ((tcfg.isoWidth * tcfg.isoPerSect) > sectWidth) then
    ret:= YesNoMsg(HWindow, SR(IDS_WARNING, p2, sizeof(p2)-1),
                            SR(IDS_TOTALWIDTHERR, pstr, sizeof(pstr)-1));

  if ret then
    cfg^:= tcfg;

  CanClose:= ret;
end;

procedure TIsoFullEdit.WMDrawItem(var msg: TMessage);
begin
  grid^.DrawItem(msg);
end;

procedure TIsoFullEdit.WMCharToItem(var msg: TMessage);
begin
  grid^.CharToItem(msg);
end;

procedure TIsoFullEdit.WMMeasureItem(var msg: TMessage);
begin
  grid^.MeasureItem(msg);
end;

procedure TIsoFullEdit.IDUp(var msg: TMessage);
var
  k   : integer;
begin
  k:= grid^.GetSelIndex;
  if k > 0 then
  begin
    grid^.SwapRows(k, k-1);
    grid^.SetSelIndex(k - 1);
  end;
end;

procedure TIsoFullEdit.IDDown(var msg: TMessage);
var
  k   : integer;
begin
  k:= grid^.GetSelIndex;
  if (k <> -1) and (k < grid^.GetCount - 1) then
  begin
    grid^.SwapRows(k, k + 1);
    grid^.SetSelIndex(k + 1);
  end;
end;

procedure TIsoFullEdit.IDIsoFlds(var msg: TMessage);
var
  isoWid : longint;
begin
  if width^.FldIsBlank or not width^.NumIsValid or
     (width^.GetDouble < 0) then
    isoWid:= 0
  else
  begin
    UnitsToLTwips(width^.GetDouble, isoWid);
  end;
  application^.ExecDialog(new(PisoFldsDlg, init(@self, @tcfg, isoWid)));
end;

procedure TIsoFullEdit.IDWidth(var msg: TMessage);
var
  pstr  : array[0..100] of char;
  p2    : array[0..100] of char;
  row   : integer;
  w     : longint;
  num   : Real;
  code  : integer;
begin
  row:= grid^.GetSelIndex;
  if row <> -1 then
  begin
    grid^.GetData(2, row, pstr, sizeof(pstr)-1);
    Val(pstr, num, code);
    UnitsToLTwips(num, w);

    {- get MIC column name }
    grid^.GetData(1, row, pstr, sizeof(pstr)-1);
    StrCat(pstr, ' ');
    StrCat(pstr, SR(IDS_COLUMN, p2, sizeof(p2)-1));

    if GetWidth(@self, pstr, w, sectWidth) then
    begin
      LTwipsToUnits(w, num);
      IntlRealToStr(num, pstr, sizeof(pstr)-1);
      grid^.SetData(2, row, pstr);
    end;
  end
  else
    MessageBeep(0);
end;

{-----------------------------------------[ TIsoFldsDlg ]--}

constructor TIsoFldsDlg.Init(aParent: PWindowsObject; aCfg: PSectIsoFullOpts; curIsoWid: longint);
var
  c   : PControl;
  num : real;
  k   : integer;
begin
  inherited Init(aParent, MakeIntResource(DLG_ISOFLDS));

  LTwipsToUnits(curIsoWid, num);

  isoFlds:= New(PGrid, InitResource(@self, IDC_ISOFLDS, 1, false));
  selFlds:= New(PGrid, InitResource(@self, IDC_SELFLDS, 1, false));

  lblWidth:= New(PSpinReal, InitResource(@self, IDC_LBLWIDTH, 0, 0, 0, num, 0.1));
  valWidth:= New(PSpinReal, InitResource(@self, IDC_VALWIDTH, 0, 0, 0, num, 0.1));

  for k:= 300 to 303 do
    c:= New(PLStatic, InitResource(@self, k, 0, false));

  cfg:= aCfg;
  tcfg:= cfg^;
  db:= New(PDBReadDesc, Init(dbIsoFile, ''));
  curIsoWidth:= curIsoWid;
end;

destructor TIsoFldsDlg.Done;
begin
  inherited Done;
  MSDisposeObj(db);
end;

procedure TIsoFldsDlg.SetupWindow;
var
  pstr    : array[0..100] of char;
  j       : longint;
  num     : real;
begin
  inherited SetupWindow;

  isoFlds^.SetHeader(0, SR(IDS_ISOFLDS, pstr, sizeof(pstr)-1));
  selFlds^.SetHeader(0, SR(IDS_SELECTEDFLDS, pstr, sizeof(pstr)-1));

  isoFlds^.StretchColumn(0);
  selFlds^.StretchColumn(0);

  UnitsStr(true, pstr, 100);
  SendDlgItemMsg(300, WM_SETTEXT, 0, longint(@pstr));
  SendDlgItemMsg(301, WM_SETTEXT, 0, longint(@pstr));

  FillIsoFlds(db, isoFlds, selFlds, tcfg.isoFlds);

  LTwipsToUnits(tcfg.lblWidth, num);
  lblWidth^.SetNum(num);
  LTwipsToUnits(tcfg.valWidth, num);
  valWidth^.SetNum(num);

  DoEnable;
end;

procedure TIsoFldsDlg.DoEnable;
var
  enabl : boolean;
begin
  enabl:= selFlds^.GetRowCount > 0;
  EnableWindow(GetDlgItem(HWindow, IDC_UP), enabl);
  EnableWindow(GetDlgItem(HWindow, IDC_DOWN), enabl);
  EnableWindow(GetDlgItem(HWindow, IDC_REMOVE), enabl);

  enabl:= isoFlds^.GetRowCount > 0;
  EnableWindow(GetDlgItem(HWindow, IDC_ADD), enabl);
  SendMessage(HWindow, WM_NEXTDLGCTL, 0, 0);
  SendMessage(HWindow, WM_NEXTDLGCTL, 1, 0);
end;

procedure TIsoFldsDlg.WMDrawItem(var msg: TMessage);
begin
  isoFlds^.DrawItem(msg);
  selFlds^.DrawItem(msg);
end;

procedure TIsoFldsDlg.WMCharToItem(var msg: TMessage);
begin
  isoFlds^.CharToItem(msg);
  selFlds^.CharToItem(msg);
end;

procedure TIsoFldsDlg.WMMeasureItem(var msg: TMessage);
begin
  isoFlds^.MeasureItem(msg);
  selFlds^.MeasureItem(msg);
end;

procedure TIsoFldsDlg.Remove(var msg: TMessage);
var
  pstr  : array[0..100] of char;
  k     : integer;
  row   : integer;
  id    : Longint;
begin
  row:= selFlds^.GetSelIndex;
  if row = -1 then
    MessageBeep(0)
  else
  begin
    id:= selFlds^.GetItemData(row);

    pstr[0]:= chr(id + ord('@'));
    pstr[1]:= #0;
    k:= isoFlds^.AddString(pstr);

    selFlds^.GetData(0, row, pstr, 100);
    isoFlds^.SetData(0, k, pstr);
    isoFlds^.SetItemData(k, id);

    selFlds^.DeleteString(row);
    if row > 0 then
      Dec(row);
    selFlds^.SetSelIndex(row);
    DoEnable;
  end;
end;

procedure TIsoFldsDlg.Add(var msg: TMessage);
var
  pstr  : array[0..100] of char;
  k     : integer;
  row   : integer;
  units : Real;
  id    : Longint;
  w     : integer;
begin
  row:= isoFlds^.GetSelIndex;
  if row = -1 then
    MessageBeep(0)
  else
  begin
    isoFlds^.GetData(0, row, pstr, 100);
    id:= isoFlds^.GetItemData(row);  {- get field number }

    k:= selFlds^.AddString(pstr);
    selFlds^.SetData(0, k, pstr);
    selFlds^.SetItemData(k, id);

    isoFlds^.DeleteString(row);
    if row > 0 then
      Dec(row);
    isoFlds^.SetSelIndex(row);
    DoEnable;
  end;
end;

procedure TIsoFldsDlg.Down(var msg: TMessage);
var
  row  : integer;
begin
  row:= selFlds^.GetSelIndex;
  if (row <> -1) and (row < selFlds^.GetCount-1) then
  begin
    selFlds^.SwapRows(row, row+1);
    selFlds^.SetSelIndex(row+1);
  end;
end;

procedure TIsoFldsDlg.Up(var msg: TMessage);
var
  row  : integer;
begin
  row:= selFlds^.GetSelIndex;
  if row > 0 then
  begin
    selFlds^.SwapRows(row, row-1);
    selFlds^.SetSelIndex(row-1);
  end;
end;

procedure TIsoFldsDlg.WMCommand(var msg: TMessage);
begin
  inherited wmCommand(msg);
  if msg.lParamLo <> 0 then  {- is message from control? }
  begin
    if msg.lParamHi = LBN_DBLCLK then
    begin
      if msg.wParam = IDC_ISOFLDS then
        Add(msg)
      else if msg.wParam = IDC_SELFLDS then
        Remove(msg);
    end;
  end;
end;

function TIsoFldsDlg.CanClose: boolean;
var
  k        : longint;
  ret      : boolean;
  num      : real;
  p1       : array[0..50] of char;
  subst    : PErrSubst;
begin
  ret:= true;

  if ret then
    ret:= ExtractWidth(@self, lblWidth, IDS_ERRWIDTH, tcfg.lblWidth, 0, curIsoWidth);
  if ret then
    ret:= ExtractWidth(@self, valWidth, IDS_ERRWIDTH, tcfg.valWidth, 0, curIsoWidth);

  if ret then
  begin
    if tcfg.lblWidth + tcfg.valWidth > curIsoWidth then
    begin
      LTwipsToUnits(curIsoWidth, num);
      IntlRealToStr(num, p1, sizeof(p1)-1);
      subst:= New(PErrSubst, Init(p1));
      UnitsStr(false, p1, sizeof(p1)-1);
      subst^.AddSubstStr(p1);
      ShowError(@self, IDS_INVLABELVALWIDTHS, subst, 0, MOD_RPTGEN, 0);
      MSDisposeObj(subst);
      ret:= false;
    end;
  end;

  if ret then
  begin
    tcfg.isoFlds[0]:= selFlds^.GetRowCount;
    for k:= 0 to selFlds^.GetRowCount do
      tcfg.isoFlds[k+1]:= integer(selFlds^.GetItemData(k));

    cfg^:= tcfg;
  end;
  CanClose:= ret;
end;

{----------------------------[ Regsitered initialization function ]--}

function SectIsoFullDelete(action: integer; aRptSeq, aSectSeq: longint): boolean;
const
  db  : PDBFile = nil;
var
  ret   : boolean;
begin
  ret:= true;
  if (action and ACTION_OPEN <> 0) then
  begin
    db:= New(PDBFile, Init(DBRptIsoFulFile, '', dbOpenNormal));
    if db = nil then
    begin
      ShowError(nil, IDS_CANTOPENIFULL, nil, dbLastOpenError, MOD_RPTGEN, 3);
      ret:= false;
    end;
  end;

  if ret and (action and ACTION_DELETE <> 0) and (db <> nil) then
  begin
    with db^ do
    begin
      dbc^.GetSeq(dbr, aSectSeq);
      if dbc^.dbErrorNum = 0 then
        dbc^.DeleteRec(dbr);
    end;
  end;

  if ret and (action and ACTION_CLOSE <> 0) then
  begin
    MSDisposeObj(db);
  end;

  SectIsoFullDelete:= ret;
end;

function SectIsoFullInit(aParent: PWindowsObject; r: TRect; origin: TPoint;
                      aZoom: Real; page: TRptPage; aFont: TRptFont): PBoxWin;
begin
  SectIsoFullInit:= New(PSectIsoFull, Init(aParent, sectionName, r, origin, azoom, page, aFont));
end;


BEGIN
  sections^.RegisterSection(SectIsoFullInit, SectIsoFullDelete, SR(IDS_ISOFull,
                            sectionName, sizeof(sectionName)), SectIsoFullID);
END.

{- rcf }
