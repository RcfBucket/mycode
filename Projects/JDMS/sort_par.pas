{----------------------------------------------------------------------------}
{  Module Name  : Sort_Par.PAS                                               }
{  Programmer   : Steven Mercier                                             }
{  Date Created : June, 26, 1998                                             }
{  Requirements : S3.28 [ref. a]                                             }
{                                                                            }
{  Purpose - This module provides an object for holding the parameters used  }
{    by the sort engine of the Sort_Eng unit. See the Sort_Eng unit and the  }
{    Sort_Dlg unit for more information.                                     }
{                                                                            }
{  Assumptions - None.                                                       }
{                                                                            }
{  Referenced Documents                                                      }
{    a. S3.28 Design Document, Revision 1                                    }
{                                                                            }
{  Revision History - This project is under version control, use it to view  }
{    revision history.                                                       }
{                                                                            }
{----------------------------------------------------------------------------}

unit Sort_Par;

INTERFACE

const
  {--------------------------------------------------------------------------}
  { This constant defines the maximum number of sort levels the sort engine
  { will handle. The code in this unit and in the SoreEng unit should handle
  { any value greater than 0, however, it's currently set to 3 to because it
  { is only used by the sort dialog box in the user interface, which uses a
  { fixed format consisting of three levels. Note: the sort dialog box unit
  { has its own constant defining the number of levels it can handle.
  {--------------------------------------------------------------------------}
  SE_MAXLEVELS = 3;

type
  {--------------------------------------------------------------------------}
  { This type defines a level. A level is characterized as having a zero-
  { based user defined key number and a normal or reverse sort order.
  {--------------------------------------------------------------------------}
  LevelDefinition = record
    KeyNumber: integer;
    Reverse: boolean;
  end;


  {--------------------------------------------------------------------------}
  { This object defines a set of operating parameters for the sort engine. The
  { parameters define a sort of zero or more levels based on the definitions
  { above. The engine will output an unsorted list when zero sort levels are
  { specified.
  {--------------------------------------------------------------------------}
  PSortEngineParameters = ^SortEngineParameters;
  SortEngineParameters = object
    NumberOfLevels: 0..SE_MAXLEVELS;
    Levels: array [1..SE_MAXLEVELS] of LevelDefinition;
    procedure LoadFromINI( SectionName: PChar; HighestKeyNumber: integer );
    procedure StoreToINI( SectionName: PChar );
    function CheckValid( var InvalidLevel: integer ): boolean;
    function isValid: boolean;
    procedure Clear;
  end;



IMPLEMENTATION

uses
  Strings,
  IntlLib,
  INILib,
  WinProcs;


{--------------------------------------------------------------------------}
{ This procedure sets the parameters to zero sort levels. The purpose of
{ this operation is to deselect sorting.
{--------------------------------------------------------------------------}

procedure SortEngineParameters.Clear;
begin
  NumberOfLevels := 0;
end;



{--------------------------------------------------------------------------}
{ This procedure validates the input parameters and reports the first
{ invalid level, if any. The key numbers are not validated because thay are
{ user defined. The Reverse fields are always valid so thay are not tested.
{ Note that levels beyond NumberOfLevels are not maintained, thus do not
{ require validation.
{--------------------------------------------------------------------------}

function SortEngineParameters.CheckValid( var InvalidLevel: integer ):
  boolean;

  {--------------------------------------------------------------------------}
  { This function detects the condition where the list at a specified level
  { has selected the same key as one of the lists above it.
  {--------------------------------------------------------------------------}
  function SameAsAbove( level: integer; index: integer ): boolean;
  var
    AboveIndex: integer;
    Same: boolean;
  begin
    Same := false;
    if level > 1 then
    begin
      AboveIndex := levels[ level-1 ].KeyNumber;
      if index = AboveIndex then
        Same := true
      else if level > 2 then
        Same := SameAsAbove( level - 1, index );
    end;
    SameAsAbove := Same;
  end;

var
  level: integer;
  Valid: boolean;

begin { CheckValid }

  { Step 1. Validate level specifier }
  Valid := NumberOfLevels in [0..SE_MAXLEVELS];

  { Step 2: Validate data at specified levels, reporting first invalid level. }
  InvalidLevel := 0;
  level := 2;
  while Valid and ( level <= NumberOfLevels ) do
  begin
    Valid := not SameAsAbove( level, levels[ level ].KeyNumber );
    if not Valid then
      InvalidLevel := level;
    Inc( level );
  end;

  { Return overall result }
  CheckValid := Valid;

end;



{--------------------------------------------------------------------------}
{ This function is the same as CheckValid but discards level information.
{--------------------------------------------------------------------------}

function SortEngineParameters.isValid: boolean;
var
  level: integer;
begin
  isValid := CheckValid( level );
end;



const
  {--------------------------------------------------------------------------}
  { Text for ture/false in the INI file
  {--------------------------------------------------------------------------}
  INI_TRUE = 'TRUE';
  INI_FALSE = 'FALSE';


  {--------------------------------------------------------------------------}
  { INI key names for fields in SoreEngineParameters. The length of these
  { strings should not exceed the maximums assumed in the code.
  {--------------------------------------------------------------------------}
  INIKN_NumberOfLevels = 'NumberOfLevels';
  INIKN_REVERSE = 'Reverse';
  INIKN_KeyNumber = 'KeyNumber';



{----------------------------------------------------------------------------}
{ This procedure makes a prefix for level-based INI file keys. It assumes
{ the size of the prefix buffer is at least 9 bytes.
{----------------------------------------------------------------------------}

procedure MakePrefix( prefix: PChar; level: integer );
var
  levelAsStr: array [0..7] of char;
begin
  Str( level, levelAsStr );
  StrCopy( prefix, 'L' );
  StrCat( prefix, levelAsStr );
  StrCat( prefix, '-' );
end;



{----------------------------------------------------------------------------}
{ This function builds a level-based INI file key from its prefix and name.
{ It assumes the size of the key buffer, in bytes, is at least: length of the
{ prefix + length of name + 1. The INIKN_ constants, defined above, should be
{ used for the name.
{----------------------------------------------------------------------------}

function BuildKey( Key, Prefix, KeyName: PChar ): PChar;
begin
  StrCopy( Key, Prefix );
  BuildKey := StrCat( Key, KeyName );
end;



{----------------------------------------------------------------------------}
{ This procedure loads the object with values found in a specified section
{ of the INI file. Defaults are loaded when a value is not found or is
{ invalid.
{
{ Inputs:
{   SectionName - an INI file section name to be used for loading and storing
{     the parameters.
{   HighestKeyNumber - the highest key number in the user defined key set.
{----------------------------------------------------------------------------}

procedure SortEngineParameters.LoadFromINI( SectionName: PChar;
  HighestKeyNumber: integer );

const
  StrLen = 100;
var
  pStr: array[0..StrLen-1] of char;

  {--------------------------------------------------------------------------}
  { This procedure reads a boolean key from the profile
  {--------------------------------------------------------------------------}
  function BooleanINI( key: PChar; default: boolean ): boolean;
  begin
    GetPrivateProfileString( SectionName, key, '', pStr, StrLen,
      INIFileName );
    if StrIComp( pStr, INI_TRUE ) = 0 then
      BooleanINI := true
    else if StrIComp( pStr, INI_FALSE ) = 0 then
      BooleanINI := false
    else
      BooleanINI := default;
  end;

  {--------------------------------------------------------------------------}
  { This procedure reads an integer key from the profile
  {--------------------------------------------------------------------------}
  function IntegerINI( key: PChar; default, min, max: integer ): integer;
  var
    i: longint;
    code: integer;
  begin
    GetPrivateProfileString( SectionName, key, '', pStr, StrLen,
      INIFileName );
    Val( pStr, i, code );
    if ( code = 0 ) and ( i >= min ) and ( i <= max ) then
      IntegerINI := i
    else
      IntegerINI := default;
  end;

var
  level: integer;
  prefix: array [0..15] of char;
  key: array [0..63] of char;

begin { GetSortParametersINI }

  NumberOfLevels := IntegerINI( INIKN_NumberOfLevels, 0, 0, SE_MAXLEVELS );
  level := 1;
  while level <= NumberOfLevels do
    with Levels[ Level ] do
    begin

      { Build and read keys }
      MakePrefix( prefix, level );
      Reverse := BooleanINI( BuildKey( key, prefix, INIKN_Reverse ), FALSE );
      KeyNumber := IntegerINI( BuildKey( key, prefix, INIKN_KeyNumber ), 0,
        0, HighestKeyNumber );

      { Set up next iteration }
      Inc( level );

    end;

end;



{----------------------------------------------------------------------------}
{ This procedure writes given parmameters to the profile (INI file). To
{ insure consistency beteween the INI file and the master parameters
{ variable, this function should only be called by SetSortParameters.
{----------------------------------------------------------------------------}

procedure SortEngineParameters.StoreToINI( SectionName: PChar );

  {--------------------------------------------------------------------------}
  { This procedure writes a boolean key to the profile
  {--------------------------------------------------------------------------}
  procedure BooleanINI( key: PChar; flag: boolean );
  var
    pStr: PChar;
  begin
    if flag then
      pStr := INI_TRUE
    else
      pStr := INI_FALSE;
    WritePrivateProfileString( SectionName, key, pStr, INIFileName );
  end;

  {--------------------------------------------------------------------------}
  { This procedure writes an integer key to the profile
  {--------------------------------------------------------------------------}
  procedure IntegerINI( key: PChar; value: integer );
  const
    StrLen = 20;
  var
    pStr: array[0..StrLen-1] of char;
  begin
    Str( value, pStr );
    WritePrivateProfileString( SectionName, key, pStr, INIFileName );
  end;

  {--------------------------------------------------------------------------}
  { This procedure deletes a key from the profile.
  {--------------------------------------------------------------------------}
  procedure DeleteINI( key: PChar );
  begin
    WritePrivateProfileString( SectionName, key, nil, INIFileName );
  end;

var
  level: integer;
  prefix: array [0..15] of char;
  key1,
  key2: array [0..63] of char;

begin { SetSortParametersINI }

  IntegerINI( INIKN_NumberOfLevels, NumberOfLevels );
  level := 1;
  while level <= SE_MAXLEVELS do
    with Levels[ Level ] do
    begin

      { Build keys }
      MakePrefix( prefix, level );
      BuildKey( key1, prefix, INIKN_Reverse );
      BuildKey( key2, prefix, INIKN_KeyNumber );

      { Update or delete key }
      if level <= NumberOfLevels then
      begin
        BooleanINI( key1, Reverse );
        IntegerINI( key2, KeyNumber );
      end
      else
      begin
        DeleteINI( key1 );
        DeleteINI( key2 );
      end;

      Inc( level );
    end;

end;

end.