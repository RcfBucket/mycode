unit UIQEORD;

{- User Interface : Quick Order Entry }

INTERFACE

uses
  oWindows;

procedure UIQuickOrdEntry(aParent: PWindowsObject);

IMPLEMENTATION

uses
  ApiTools,
  AS4RDR,
  DBFile,
  DBIDS,
  DBLib,
  DBTypes,
  DMSErr,
  DMString,
  DlgLib,
  GridLib,
  INILib,
  IntlLib,
  PrnPrev,
  ListLib,
  HWConfig,
  ResLib,
  Trays,
  BarCodes,
  MScan,
  ODialogs,
  Sessions,
  Status,
  UserMsgs,
  Strings,
  StrsW,
  UICommon,
  UISpec,
  UIPat,
  UIIso,
  UIFlds,
  UIIDMIC,
  Win31,
  WinProcs,
  WinTypes;

{$R UIQEORD.RES}
{$I UICOMMON.INC}
{$I UIQEORD.INC}

type
  PUIQEDlg = ^TUIQEDlg;
  TUIQEDlg = object(TUICommonDlg)
    grid      : PGrid;
    dbSpec    : PDBFile;
    dbPat     : PDBFile;
    dbIso     : PDBFile;
    sFld, cFld, iFld, pFld, oFld, srcFld: PUIFld;
    tempIso   : PDBRec;
    tempPat   : PDBRec;
    tempSpec  : PDBRec;
    listRec   : PMnemListRec;
    results   : PResults;

    origSpec, origPat, origIso: PDBRec;
    pushSeq   : TSeqNum;

    isoMode, specMode, patMode: integer;
    sModeTxt, iModeTxt: PStatic;
    lastRow   : longint;
    {}
    constructor Init(aParent: PWindowsObject);
    destructor Done; virtual;
    procedure SetupWindow; virtual;
    procedure WMCharToItem(var msg: TMessage);  virtual WM_FIRST + WM_CHARTOITEM;
    procedure WMDrawItem(var msg: TMessage);    virtual WM_FIRST + WM_DRAWITEM;
    procedure WMMeasureItem(var msg: TMessage); virtual WM_FIRST + WM_MEASUREITEM;
    procedure WMLostFocus(var msg: TMessage);   virtual WM_FIRST + WM_LOSTFOCUS;
    procedure WMFldList(var msg: TMessage);     virtual WM_FIRST + WM_FLDLIST;
    procedure WMCtlColor(var msg: TMessage);    virtual WM_FIRST + WM_CTLCOLOR;
    procedure WMFillMyGrid(var msg: TMessage);  virtual WM_FIRST + WM_FILLMYGRID;
    procedure DoSpecCollExit(newFocID: word);
    procedure SetRowData(row: longint);
    procedure FillSpecimen;
    procedure FillIsolate;
    function CheckIsoSess(anIso: PDBRec): boolean;
    procedure FillGrid;
    procedure PushPos;
    procedure PopPos;
    procedure SetModuleID; virtual;
    procedure ClearPrim;
    procedure ClearFields; virtual;
    procedure EnableFlds;
    procedure EnableButtons; virtual;
    procedure ShowTitle;
    procedure LocateIsoInGrid;
    function GetRowSeq(aCol: integer; aRow: longint): TSeqNum;
    procedure LoadLine(aNewRow, aLastRow: longint);
    procedure IDMove(var msg: TMessage); virtual ID_FIRST + IDC_MOVE;
    procedure IDSession(var msg: Tmessage); virtual ID_FIRST + IDC_SESSION;
    procedure IDGridMsg(var msg: TMessage); virtual ID_FIRST + IDC_LIST;
    procedure ReloadPat(aSeq: TSeqNum);
    procedure ReloadSpec(aSeq: TSeqNum);
    procedure ReloadIsolate(aSeq: TSeqNum);
    procedure IDSpecimen(var msg: TMessage); virtual ID_FIRST + IDC_SPECIMEN;
    procedure IDPatient(var msg: TMessage);  virtual ID_FIRST + IDC_PATIENT;
    procedure IDIsolate(var msg: TMessage);  virtual ID_FIRST + IDC_ISOLATE;
    procedure IDMIC(var msg: TMessage);      virtual ID_FIRST + IDC_IDMIC;
    procedure IDAS4(var msg: TMessage);      virtual ID_FIRST + IDC_AS4;
    procedure IDBarCodes(var msg: TMessage); virtual ID_FIRST + IDC_BARCODES;
    procedure DeleteData; virtual;
    function CheckSave(clearAfter: boolean): boolean; virtual;
    function SaveData(clearAfter: boolean): boolean; virtual;
    function DataChanged: boolean; virtual;
    procedure ShowModes;
    procedure RecallLastSpecimen; virtual;
    procedure PrintData; virtual;
    procedure SaveLastSpecimen;
    procedure WMUserKey(var msg: TMessage); virtual WM_FIRST + WM_USERKEY;
  end;

  PQEGrid = ^TQEGrid;
  TQEGrid = object(TGrid)
    function BuildKeyStr(aCol: integer; aRow: longint; keyBuf: PChar; maxLen: integer; numeric: boolean): PChar; virtual;
    procedure WMKeyDown(var msg: TMessage); virtual WM_FIRST + WM_KEYDOWN;
  end;

const
  grid_Collect  = 0;
  grid_SpecID   = 1;
  grid_Isolate  = 2;
  grid_Order    = 3;
  grid_TestDate = 4;
  grid_PatID    = 5;
  {- NOTE: if any of the following are changed, then the grid_First/LastSeqCol
           constants must be changed as well}
  grid_SpecSeq  = 6;
  grid_IsoSeq   = 7;
  grid_OrderSeq = 8;
  grid_PatSeq   = 9;

  grid_FirstSeqCol  = grid_SpecSeq;  {- used for range checking }
  grid_LastSeqCol   = grid_PatSeq;

function BuildSort(isoRec, spcRec: PDBRec; pstr: PChar; maxLen: integer): PChar;
{- generic function to build a sort key from the iso/spec records }
var
  p1, p2  : array[0..50] of char;
  d       : longint;

  procedure GetSpecID;
  {- append specID to pstr }
  begin
    spcRec^.GetField(DBSpecSpecID, @p2, sizeof(p1)-1);
    Pad(p1, p2, ' ', spcRec^.Desc^.FieldSizeForStr(DBSpecSpecID));
    StrLCat(pstr, p1, maxLen);
  end;

  procedure GetCollectDate;
  {- append collect data to pstr }
  begin
    spcRec^.GetField(DBSPECCollectDate, @d, sizeof(d));
    d:= MaxLongint - d;   {- force descending sort order }
    Str(d, p2);
    LeftPad(p1, p2, '0', 12);
    StrLCat(pstr, p1, maxLen);
  end;

begin
  StrCopy(pstr, '');

  if INISpecimenListSort = 2 then
  begin
    GetCollectDate;
    GetSpecID;
  end
  else
  begin
    GetSpecID;
    GetCollectDate;
  end;

  isoRec^.GetField(DBIsoIso, @p1, sizeof(p1)-1);
  StrLCat(pstr, p1, maxLen);

  BuildSort:= pstr;
end;

procedure TQEGrid.WMKeyDown(var msg: TMessage);
begin
  CheckUserKey(parent, msg);
  inherited WMKeyDown(msg);
end;

function TQEGrid.BuildKeyStr(aCol: integer; aRow: longint; keyBuf: PChar; maxLen: integer; numeric: boolean): PChar;
var
  p1, p2  : array[0..50] of char;
  d       : TIntlDate;
  err     : boolean;

  procedure GetSpecID;
  begin
    GetData(grid_SpecID, aRow, p2, 50);
    Pad(p1, p2, ' ', PUIQEDlg(parent)^.dbSpec^.dbr^.Desc^.FieldSizeForStr(DBSpecSpecID));
    StrLCat(keyBuf, p1, maxLen);
  end;

  procedure GetCollect;
  begin
    GetData(grid_Collect, aRow, p2, 50);
    d:= IntlStrToDate(p2, err);
    d:= MaxLongint - d;   {- make descending order }
    Str(d, p2);
    LeftPad(p1, p2, '0', 12);
    StrLCat(keyBuf, p1, maxLen);
  end;

begin
  {- key is specID/collect/iso for INISpecimenListSort = 1 otherwise it is
     collect/specID/iso }
  StrCopy(keyBuf, '');
  if INISpecimenListSort = 2 then
  begin
    GetCollect;
    GetSpecID;
  end
  else
  begin
    GetSpecID;
    GetCollect;
  end;

  GetData(grid_Isolate, aRow, p2, 50);
  Pad(p1, p2, ' ', PUIQEDlg(parent)^.dbIso^.dbr^.Desc^.FieldSizeForStr(DBISOIso));
  StrLCat(keyBuf, p1, maxLen);

  BuildKeyStr:= keyBuf;
end;

constructor TUIQEDlg.Init(aParent: PWindowsObject);
var
  subst   : PErrSubst;
begin
  if not inherited Init(aParent, MakeIntResource(DLG_UIQEORD), 0) then
    fail;

  dbSpec:= nil;
  dbPat:= nil;
  dbIso:= nil;
  tempIso:= nil;
  tempPat:= nil;
  tempSpec:= nil;
  listRec:= nil;
  origSpec:= nil;
  origPat:= nil;
  origIso:= nil;
  results:= nil;

  dbSpec:= New(PDBFile, Init(DBSpecFile, '', dbOpenNormal));
  if dbSpec = nil then
  begin
    subst:= New(PErrSubst, Init(DBSpecFile));
    DoFail(IDS_ERROROPEN, dbLastOpenError, subst);
    MSDisposeObj(subst);
    Fail;
  end;
  {- get spec file in proper sort order }
  if INISpecimenListSort = 2 then
    dbSpec^.dbc^.SetCurKeyNum(DBSPEC_DATE_KEY)
  else
    dbSpec^.dbc^.SetCurKeyNum(DBSPEC_ID_KEY);
  dbSpec^.dbc^.GetFirst(dbSpec^.dbr);

  dbPat:= New(PDBFile, Init(DBPatFile, '', dbOpenNormal));
  if dbPat = nil then
  begin
    subst:= New(PErrSubst, Init(DBPatFile));
    DoFail(IDS_ERROROPEN, dbLastOpenError, subst);
    MSDisposeObj(subst);
    Fail;
  end;

  dbIso:= New(PDBFile, Init(DBIsoFile, '', dbOpenNormal));
  if dbIso = nil then
  begin
    subst:= New(PErrSubst, Init(DBIsoFile));
    DoFail(IDS_ERROROPEN, dbLastOpenError, subst);
    MSDisposeObj(subst);
    Fail;
  end;

  tempIso:= New(PDBRec, Init(dbIso^.dbc));
  if tempIso = nil then
  begin
    DoFail(IDS_CANTALLOCREC, 0, nil);
    Fail;
  end;
  tempPat:= New(PDBRec, Init(dbPat^.dbc));
  if tempPat = nil then
  begin
    DoFail(IDS_CANTALLOCREC, 0, nil);
    Fail;
  end;
  tempSpec:= New(PDBRec, Init(dbSpec^.dbc));
  if tempSpec = nil then
  begin
    DoFail(IDS_CANTALLOCREC, 0, nil);
    Fail;
  end;
  origSpec:= New(PDBRec, Init(dbSpec^.dbc));
  if origSpec = nil then
  begin
    DoFail(IDS_CANTALLOCREC, 0, nil);
    Fail;
  end;
  origPat:= New(PDBRec, Init(dbPat^.dbc));
  if origPat = nil then
  begin
    DoFail(IDS_CANTALLOCREC, 0, nil);
    Fail;
  end;
  origIso:= New(PDBRec, Init(dbIso^.dbc));
  if origIso = nil then
  begin
    DoFail(IDS_CANTALLOCREC, 0, nil);
    Fail;
  end;

  listRec:= New(PMnemListRec, Init);
  if listRec = nil then
  begin
    DoFail(IDS_CANTALLOCLISTREC, 0, nil);
    Fail;
  end;
  results:= New(PResults, Init);
  if results = nil then
  begin
    DoFail(IDS_CANTINITRESULTS, 0, nil);
    Fail;
  end;

  sFld:= LinkUIFld(DBSpecSpecID,      true,  false, 0, IDC_EDSPECID,   IDC_LBLSPECID,   dbSpec^.dbd);
  cFld:= LinkUIFld(DBSpecCollectDate, true,  false, 0, IDC_EDCOLLECT,  IDC_LBLCOLLECT,  dbSpec^.dbd);
  iFld:= LinkUIFld(DBIsoIso,          true,  false, 0, IDC_EDISO,      IDC_LBLISO,      dbIso^.dbd);
  oFld:= LinkUIFld(DBIsoOrdRef,       false, false, 0, IDC_EDORDER,    IDC_LBLORDER,    dbIso^.dbd);
  srcFld:= LinkUIFld(DBSpecSrc,       false, false, 0, IDC_EDSOURCE,   IDC_LBLSOURCE,   dbSpec^.dbd);
  pFld:= LinkUIFld(DBPatPatID,        false, false, 0, IDC_EDPATID,    IDC_LBLPATID,    dbPat^.dbd);

  specMode:= MODE_NONE;
  isoMode:= MODE_NONE;
  patMode:= MODE_NONE;
  lastRow:= -1;
  pushSeq:= 0;

  sModeTxt:= New(PStatic, InitResource(@self, IDC_MODESPEC, 0));
  iModeTxt:= New(PStatic, InitResource(@self, IDC_MODEISO, 0));

  grid:= New(PQEGrid, InitResource(@self, IDC_LIST, 10, false));
end;

destructor TUIQEDlg.Done;
begin
  inherited Done;
  MSDisposeObj(dbSpec);
  MSDisposeObj(dbPat);
  MSDisposeObj(dbIso);
  MSDisposeObj(tempIso);
  MSDisposeObj(tempPat);
  MSDisposeObj(tempSpec);
  MSDisposeObj(listRec);
  MSDisposeObj(origSpec);
  MSDisposeObj(origPat);
  MSDisposeObj(origIso);
  MSDisposeObj(results);
end;

procedure TUIQEDlg.SetupWindow;
var
k:integer;
  fi    : TFldInfo;
  pstr  : array[0..100] of char;
begin
  inherited SetupWindow;

  dbSpec^.dbd^.FieldInfo(DBSPECSpecID, fi);
  grid^.SetHeader(grid_SpecID, fi.fldName);
  grid^.SetAvgCharWidth(grid_SpecID, dbSpec^.dbd^.FieldSizeForStr(DBSpecSpecID));

  dbSpec^.dbd^.FieldInfo(DBSPECCollectDate, fi);
  grid^.SetHeader(grid_Collect, fi.fldName);
  grid^.SetAvgCharWidth(grid_Collect, dbSpec^.dbd^.FieldSizeForStr(DBSpecCollectDate));

  dbIso^.dbd^.FieldInfo(DBISOIso, fi);
  grid^.SetHeader(grid_Isolate, fi.fldName);
  grid^.SetAvgCharWidth(grid_Isolate, dbIso^.dbd^.FieldSizeForStr(DBISOIso));

  dbPat^.dbd^.FieldInfo(DBPatPatID, fi);
  grid^.SetHeader(grid_patid, fi.fldName);
  grid^.SetAvgCharWidth(grid_PatID, dbPat^.dbd^.FieldSizeForStr(DBPatPatID));
  grid^.SetColumnWidth(grid_PatID, grid^.GetColumnWidth(grid_PatID) + GetSystemMetrics(SM_CXVSCROLL));

  dbIso^.dbd^.FieldInfo(DBISOTstDate, fi);
  grid^.SetHeader(grid_TestDate, fi.fldName);
  grid^.SetAvgCharWidth(grid_TestDate, dbIso^.dbd^.FieldSizeForStr(DBISOTstDate));

  dbIso^.dbd^.FieldInfo(DBISOOrdRef, fi);
  grid^.SetHeader(grid_Order, fi.fldName);
  grid^.StretchColumn(grid_Order);

  ShowTitle;
  PostMessage(HWindow, WM_FILLMYGRID, 0, 0);
end;

procedure TUIQEDlg.WMFillMyGrid(var msg: TMessage);
begin
  FillGrid;
  ClearPrim;
end;

procedure TUIQEDlg.SetModuleID;
begin
  modID:= MOD_UIQEORD;
end;

procedure TUIQEDlg.PushPos;
{- save information for the current row so it can be rehighlighted with PopPos }
var
  row   : longint;
begin
  row:= grid^.GetSelIndex;
  if row >= 0 then
    pushSeq:= GetRowSeq(grid_IsoSeq, row)
  else
    pushSeq:= 0;
end;

procedure TUIQEDlg.PopPos;
{- restore selection to the row specified by pushSeq. PushSeq contains the seq
   number of the isolate that is to be found }
var
  row   : longint;
  seq   : TSeqNum;
begin
  if pushSeq > 0 then
  begin
    for row:= 0 to grid^.GetRowCount - 1 do
    begin
      if GetRowSeq(grid_IsoSeq, row) = pushSeq then
      begin
        grid^.SetSelIndex(row);
        LoadLine(row, lastRow);
        lastRow:= row;
        break;
      end;
    end;
  end;
end;

procedure TUIQEDlg.ClearPrim;

  procedure ClearFlds(aFld: PUIFld); far;
  begin
    aFld^.Clear;
  end;

begin
  flds^.ForEach(@ClearFlds);
  dbIso^.dbr^.ClearRecord;
  dbSpec^.dbr^.ClearRecord;
  dbPat^.dbr^.ClearRecord;
  origSpec^.ClearRecord;
  origPat^.ClearRecord;
  origIso^.ClearRecord;
  specMode:= MODE_NONE;
  patMode:= MODE_NONE;
  isoMode:= MODE_NONE;
  ShowModes;
end;

procedure TUIQEDlg.ClearFields;
begin
  ClearPrim;
  grid^.SetSelIndex(-1);
  lastRow:= -1;
  EnableFlds;
  EnableButtons;
  sFld^.FocusFld;
end;

procedure TUIQEDlg.EnableButtons;
begin
  EnableWindow(GetItemHandle(IDC_UISAVE),   isoMode > MODE_NONE);
  EnableWindow(GetItemHandle(IDC_PATIENT), (patMode = MODE_EDIT));
  EnableWindow(GetItemHandle(IDC_SPECIMEN),(specMode = MODE_EDIT));
  EnableWindow(GetItemHandle(IDC_IDMIC),   (isoMode = MODE_EDIT) and not oFld^.IsEmpty);
  EnableWindow(GetItemHandle(IDC_ISOLATE), (isoMode = MODE_EDIT));
  EnableWindow(GetItemHandle(IDC_MOVE),    (isoMode = MODE_EDIT));
  EnableWindow(GetItemHandle(IDC_SESSION), (isoMode <> MODE_NEW) and
                                           (specMode <> MODE_NEW));
  EnableWindow(GetItemHandle(IDC_UIDELETE),(isoMode = MODE_EDIT));
  EnableWindow(GetItemHandle(IDC_AS4),      HWExists(pkAS4));
end;

procedure TUIQEDlg.EnableFlds;
{- If a specimen mode has NOT been established, then disable all fields except
   for spec/collect date.
   If a specimen mode has been established, then enable/disable fields based on
   the different modes (specMode,PatMode,isoMode) }

  function IsEnabled(fld: PUIFld): boolean; far;
  begin
    IsEnabled:= fld^.IsEnabled;
  end;

var
  fld   : PUIFld;
  hw    : HWnd;
begin
  hw:= GetFocus;

  if specMode = MODE_NONE then
  begin
    iFld^.Enable(false);
    oFld^.Enable(false);
    srcFld^.Enable(false);
    pFld^.Enable(false);
  end
  else
  begin
    iFld^.Enable(true);
    oFld^.Enable(oFld^.IsEmpty);  {- order cannot be modified }
    srcFld^.Enable(true);
    pFld^.Enable(true);
  end;

  if (hw <> 0) and (hw <> grid^.HWindow) then
  begin
    if not IsWindowEnabled(hw) then
    begin
      fld:= flds^.FirstThat(@IsEnabled);
      if fld = nil then
        FocusCtl(HWindow, IDC_LIST)   {- no fields enabled, focus grid }
      else
        fld^.FocusFld;
    end;
  end;
end;

procedure TUIQEDlg.ShowTitle;
{- show the title with the current session }
var
  p1, p2: array[0..200] of char;
  seq   : TSeqNum;
begin
  seq:= GetCurrentSession(p1, sizeof(p1)-1);
  if seq = 0 then {- are sessions disabled }
    SR(IDS_SESSIONSDISABLED, p1, sizeof(p1)-1);
  if baseTitle <> nil then
  begin
    StrCopy(p2, baseTitle);
    StrCat(p2, ' --- ');
  end
  else
    StrCopy(p2, '');
  StrCat(p2, p1);
  SetWindowText(HWindow, p2);
end;

procedure TUIQEDlg.LocateIsoInGrid;
{- using the data in dbSpec and dbIso, try to locate a grid row that contains
   the same seqs. If found, then highlight the row }
var
  found   : boolean;
  row     : longint;
  iSeq    : TSeqNum;
begin
  found:= false;
  row:= 0;
  iSeq:= dbIso^.dbr^.GetSeqValue;
  while not found and (row < grid^.GetRowCount) do
  begin
    if (GetRowSeq(grid_IsoSeq, row) = iSeq) then
      found:= true
    else
      Inc(row);
  end;
  if found then
  begin
    grid^.SetSelIndex(row);
    lastRow:= grid^.GetSelIndex;
  end;
end;

function TUIQEDlg.GetRowSeq(aCol: integer; aRow: longint): TSeqNum;
{- return a seq number for the specified column from the grid }
var
  pstr  : array[0..100] of char;
  seq   : TSeqNum;
  code  : integer;
begin
  if (aCol < grid_FirstSeqCol) or (aCol > grid_LastSeqCol) then
    seq:= 0
  else
  begin
    grid^.GetData(aCol, aRow, pstr, 100);
    if StrLen(pstr) = 0 then
      StrCopy(pstr, '0');
    Val(pstr, seq, code);
    if code <> 0 then
      seq:= 0;
  end;
  GetRowSeq:= seq;
end;

procedure TUIQEDlg.LoadLine(aNewRow, aLastRow: longint);
{- get the data from the grid for the specified line and load the edit controls }
var
  seq     : TSeqNum;
  ret     : boolean;
  p1, p2  : array[0..200] of char;

  procedure LoadIt(fld: PUIFld); far;
  begin
    if fld^.GetDBD = dbSpec^.dbd then
      fld^.XFerRecToCtl(dbSpec^.dbr)
    else if fld^.GetDBD = dbIso^.dbd then
      fld^.XFerRecToCtl(dbIso^.dbr)
    else if fld^.GetDBD = dbPat^.dbd then
      fld^.XFerRecToCtl(dbPat^.dbr)
  end;

  function GetOne(aCol: integer; aMsg: word; rec: PDBRec; var aMode: integer): boolean;
  begin
    GetOne:= true;
    seq:= GetRowSeq(aCol, aNewRow);
    if seq > 0 then
    begin
      if listObj^.FindSeq(rec, seq) then
        aMode:= MODE_EDIT
      else
      begin
        ShowError(@self, aMsg, nil, listObj^.errorNum, modID, 0);
        GetOne:= false;
      end;
    end;
  end;

  procedure CheckChanges;
  {- assumes the iso/spec records have been loaded from the database. Compares
     the record data with the grid data. if different then the grid is refilled
     with the new data }
  var
    sret   : boolean;

    function Compare(gridCol, dbFld: integer; dbRec: PDBRec): boolean;
    {- compare grid item with record item, return false if different }
    begin
      grid^.GetData(gridCol, aNewRow, p1, sizeof(p1)-1);
      dbRec^.GetFieldAsStr(dbFld, p2, sizeof(p2)-1);
      Compare:= StrComp(p1, p2) = 0;
    end;

  begin
    sret:= Compare(grid_Isolate, DBIsoIso, dbIso^.dbr);
    if sret then
      sret:= Compare(grid_SpecID, DBSpecSpecID, dbSpec^.dbr);
    if sret then
      sret:= Compare(grid_Collect, DBSpecCollectDate, dbSpec^.dbr);
    if sret then
      sret:= Compare(grid_PatID, DBPatPatID, dbPat^.dbr);
    if not sret then
    begin
      InfoMsg(HWindow, SR(IDS_INFORMATION, p1, sizeof(p1)-1),
                       SR(IDS_DATAMODIFIED, p2, sizeof(p2)-1));
      PushPos;
      ReloadIsolate(dbIso^.dbr^.GetSeqValue);
      grid^.Sort(0, 100, false);
      PopPos;
    end;
  end;


begin
  if not CheckSave(false) then
  begin
    grid^.SetSelIndex(aLastRow);
    exit;
  end;
  WaitCursor(true);

  ret:= true;

  ClearPrim;

  {- first load the isolate record and the specimen record and see if they have
     been changed from the data that is displayed in the grid }
  ret:= GetOne(grid_IsoSeq, IDS_ERRLODISO, dbIso^.dbr, isoMode);
  if ret then
    ret:= GetOne(grid_SpecSeq, IDS_ERRLODSPEC, dbSpec^.dbr, specMode);
  if ret then
  begin
    dbSpec^.dbr^.GetField(DBSpecPatRef, @seq, sizeof(seq));
    if seq > 0 then
    begin
      if listObj^.FindSeq(dbPat^.dbr, seq) then
        patMode:= MODE_EDIT
      else
      begin
        ShowError(@self, IDS_ERRLODPAT, nil, listObj^.errorNum, modID, 1);
        ret:= false;
      end;
    end;
  end;

  if ret then {- check for changes in the database data to that displayed in the grid }
    CheckChanges;

  if ret then
  begin
    flds^.ForEach(@LoadIt);
    origSpec^.CopyRecord(dbSpec^.dbr);
    origPat^.CopyRecord(dbPat^.dbr);
    origIso^.CopyRecord(dbIso^.dbr);
    SaveLastSpecimen;
  end
  else
    ClearPrim;

  ShowModes;
  EnableFlds;
  EnableButtons;
  WaitCursor(false);
end;

procedure TUIQEDlg.SetRowData(row: longint);
{- set the data in dbIso/dbSpec/dbPat in the grid }
var
  pstr  : array[0..200] of char;
  tSeq  : TSeqNum;
begin
  {- Specimen ID }
  dbSpec^.dbr^.GetFieldAsStr(DBSpecSpecID, pstr, sizeof(pstr)-1);
  grid^.SetData(grid_SpecID, row, pstr);

  dbSpec^.dbr^.GetFieldAsStr(DBSpecCollectDate, pstr, sizeof(pstr)-1);
  grid^.SetData(grid_Collect, row, pstr);
  Str(dbSpec^.dbr^.GetSeqValue, pstr);
  grid^.SetData(grid_SpecSeq, row, pstr);

  if dbPat^.dbr^.GetSeqValue > 0 then
  begin
    dbPat^.dbr^.GetFieldAsStr(DBPatPatID, pstr, sizeof(pstr)-1);
    grid^.SetData(grid_PatID, row, pstr);
  end;
  Str(dbPat^.dbr^.GetSeqValue, pstr);
  grid^.SetData(grid_PatSeq, row, pstr);

  {- isolate ID }
  dbIso^.dbr^.GetFieldAsStr(DBIsoIso, pstr, sizeof(pstr)-1);
  grid^.SetData(grid_Isolate, row, pstr);
  Str(dbIso^.dbr^.GetSeqValue, pstr);
  grid^.SetData(grid_IsoSeq, row, pstr);

  {- isolate Test Date }
  dbIso^.dbr^.GetFieldAsStr(DBIsoTstDate, pstr, sizeof(pstr)-1);
  grid^.SetData(grid_TestDate, row, pstr);

  {- order ID }
  dbIso^.dbr^.GetField(DBIsoOrdRef, @tSeq, sizeof(tSeq));
  if listObj^.FindMnemSeq(DBOrdFile, listRec, tSeq) then
  begin
    grid^.SetData(grid_Order, row, listRec^.desc);
    Str(listRec^.seq, pstr);
    grid^.SetData(grid_OrderSeq, row, pstr);
  end
  else
    grid^.SetData(grid_OrderSeq, row, '0');
end;

procedure TUIQEDlg.FillGrid;
var
  stat    : PPctStatDlg;
  count   : integer;
  pstr    : array[0..100] of char;
  p2      : array[0..200] of char;
  isOK    : boolean;
  sessSeq : TSeqNum;
  tSeq    : TSeqNum;
  row     : longint;
begin
  grid^.ClearList;
  grid^.SetRedraw(false);

  sessSeq:= GetCurrentSession(pstr, sizeof(pstr)-1);

  if INISpecimenListSort = 2 then
  begin
    if sessSeq = 0 then
      dbIso^.dbc^.SetCurKeyNum(DBISO_CDateSpecID_KEY)
    else
      dbIso^.dbc^.SetCurKeyNum(DBISO_SessionCDateSpecID_KEY);
  end
  else
  begin
    if sessSeq = 0 then
      dbIso^.dbc^.SetCurKeyNum(DBISO_SpecIDCDate_KEY)
    else
      dbIso^.dbc^.SetCurKeyNum(DBISO_SessionSpecIDCDate_KEY);
  end;
  dbIso^.dbc^.GetFirst(dbIso^.dbr);

  dbIso^.dbr^.ClearRecord;
  if sessSeq = 0 then
    isOK:= dbIso^.dbc^.GetFirst(dbIso^.dbr)
  else
  begin
    dbIso^.dbr^.PutField(DBIsoSess, @sessSeq);
    isOK:= dbIso^.dbc^.GetFirstContains(dbIso^.dbr);
    if isOK then
    begin
      dbIso^.dbr^.GetField(DBIsoSess, @tSeq, sizeof(tseq));
      isOK:= (tSeq = sessSeq);
    end;
  end;

  if isOK then
  begin
    stat:= New(PPctStatDlg, Init(@self, MakeIntResource(DLG_FILL), true, 0));
    application^.MakeWindow(stat);
    SetFocus(stat^.HWindow);
    count:= 0;
    while isOK do
    begin
      Str(count, pstr);
      stat^.SetText(pstr, IDC_TEXT);

      {- locate specimen }
      dbIso^.dbr^.GetField(DBIsoSpecRef, @tSeq, sizeof(tSeq));
      if not dbSpec^.dbc^.GetSeq(dbSpec^.dbr, tSeq) then
        dbSpec^.dbr^.ClearRecord
      else
      begin
        dbSpec^.dbr^.GetField(DBSpecPatRef, @tseq, sizeof(tseq));
        if tSeq <> 0 then
        begin
          if not dbPat^.dbc^.GetSeq(dbPat^.dbr, tSeq) then
            dbPat^.dbr^.ClearRecord;
        end
        else
          dbPat^.dbr^.ClearRecord;
      end;

      {- at this point all records are loaded, now build a sort key }
      row:= grid^.AddString(BuildSort(dbIso^.dbr, dbSpec^.dbr, pstr, sizeof(pstr)-1));
      if (row < 0) then
      begin
        ShowError(@self, IDS_TOOMUCH, nil, 0, modID, 1);
        isOK:= false;
      end
      else
        SetRowData(row);

      Inc(count);
      if not stat^.CanContinue then
      begin
        if YesNoMsg(stat^.HWindow, SR(IDS_CONFIRM, pstr, sizeof(pstr)-1), SR(IDS_CANCELFILL, p2, sizeof(p2)-1)) then
          isOK:= false
        else
          stat^.SetContinue(true); {- reset continue flag }
      end;
      if isOK then
      begin
        isOK:= dbIso^.dbc^.GetNext(dbIso^.dbr);
        if isOK and (sessSeq <> 0) then
        begin
          dbIso^.dbr^.GetField(DBIsoSess, @tSeq, sizeof(tseq));
          isOK:= (tSeq = sessSeq);
        end;
      end;
    end;  {- while isOK }
    MSDisposeObj(stat);
  end
  else
  begin  {- no isolates in current session }
    InfoMsg(HWindow, SR(IDS_INFORMATION, pstr, sizeof(pstr)-1),
                     SR(IDS_NOISOINSESS, p2, sizeof(p2)-1));
  end;
  grid^.SetRedraw(true);
  sFld^.FocusFld;
end;

procedure TUIQEDlg.WMCharToItem(var msg: TMessage);
begin
  grid^.CharToItem(msg);
end;

procedure TUIQEDlg.WMDrawItem(var msg: TMessage);
begin
  grid^.DrawItem(msg);
end;

procedure TUIQEDlg.WMMeasureItem(var msg: TMessage);
begin
  grid^.MeasureItem(msg);
end;

procedure TUIQEDlg.IDMove(var msg: TMessage);
var
  row     : longint;
  origSeq : TSeqNum;
  seq     : TSeqNum;
  curSess : TSeqNum;
  pstr    : array[0..100] of char;
  fld     : PUIFld;
  errNum  : integer;

  function IsInvalid(aFld: PUIFld): boolean; far;
  begin
    errNum:= aFld^.Format;
    IsInvalid:= errNum <> 0;
  end;

begin
  if dbIso^.dbr^.GetSeqValue > 0 then
  begin
    {- first need to validate the fields }
    fld:= flds^.FirstThat(@IsInvalid);
    if (fld <> nil) then
    begin {- some other error }
      ShowUIFldError(@self, errNum, fld);
      exit;
    end;

    dbIso^.dbr^.GetField(DBISOSess, @origSeq, sizeof(origSeq));
    if application^.ExecDialog(New(PMoveIsoDlg, Init(@self, dbIso, listObj))) = IDOK then
    begin
      dbIso^.dbr^.GetField(DBISOSess, @seq, sizeof(seq));
      {- if the isolate session has actually changed then ...}
      if origSeq <> seq then
      begin
        {- save the isolate (without prompting). Save data will also attempt
           to reload the current line and in doing so, it will note that it has
           been removed from the current session and then remove it from the list }
        SaveData(true);
      end;
    end;
  end;
end;

procedure TUIQEDlg.IDSession(var msg: Tmessage);
var
  cur   : TSeqNum;
  p1    : array[0..20] of char;
begin
  cur:= GetCurrentSession(p1, sizeof(p1)-1);
  if listObj^.EditableMnemList(@self, DBSessFile, '') then
  begin
    if cur <> GetCurrentSession(p1, sizeof(p1)-1) then
    begin
      ShowTitle;
      FillGrid;
      ClearFields;
    end;
  end;
end;

procedure TUIQEDlg.IDGridMsg(var msg: TMessage);
const
  inHere  : boolean = false;

  procedure LoadRow;
  begin
    if grid^.GetSelIndex <> lastRow then
    begin
      LoadLine(grid^.GetSelIndex, lastRow);
      lastRow:= grid^.GetSelIndex;
    end;
  end;

begin
  DefWndProc(msg);
  if msg.lParamHi = LBN_SELCHANGE then
  begin
    if not inHere then
    begin
      inHere:= true;
      LoadRow;
      while inHere and grid^.keyDown do
        DoEvent(grid^.hWindow);
      grid^.keyDown:= false;
      LoadRow;
      inHere:= false;
    end;
  end
  else if msg.lParamHi = LBN_KILLFOCUS then
  begin
    if inHere then
    begin
      inHere:= false;
    end;
  end;
end;

procedure TUIQEDlg.SaveLastSpecimen;
{- save the just added/edited specimen to the lastSpec/CD fields }
var
  p1, p2  : array[0..50] of char;
begin
  dbSpec^.dbr^.GetFieldAsStr(DBSpecSpecID, p1, sizeof(p1)-1);
  dbSpec^.dbr^.GetFieldAsStr(DBSpecCollectDate, p2, sizeof(p2)-1);
  SetLastSpecimen(p1, p2);
end;

procedure TUIQEDlg.WMUserKey(var msg: TMessage);
begin
  {- need this line in case the user pressed a key in the grid. This caused the
     key down flag in the grid to remain true and then the logic in .IDGridMsg
     got messed up }
  grid^.keyDown:= false;
  inherited WMUserKey(msg);
end;

procedure TUIQEDlg.FillSpecimen;
{- assumes the specimen data is in the dbSpec^.dbr member }
var
  seq   : TSeqNum;
begin
  WaitCursor(true);

  origSpec^.CopyRecord(dbSpec^.dbr);
  sFld^.XFerRecToCtl(dbSpec^.dbr);
  cFld^.XFerRecToCtl(dbSpec^.dbr);
  srcFld^.XFerRecToCtl(dbSpec^.dbr);

  {- for specimens just loaded, clear all other fields }
  dbIso^.dbr^.ClearRecord;
  origIso^.ClearRecord;
  dbPat^.dbr^.ClearRecord;
  origPat^.ClearRecord;
  patMode:= MODE_NONE;
  isoMode:= MODE_NONE;
  iFld^.Clear;
  pFld^.Clear;
  oFLd^.Clear;
  dbSpec^.dbr^.GetField(DBSpecPatRef, @seq, sizeof(seq));
  if seq > 0 then
  begin
    if listObj^.FindSeq(dbPat^.dbr, seq) then
    begin
      origPat^.CopyRecord(dbPat^.dbr);
      pFld^.XFerRecToCtl(dbPat^.dbr);
      patMode:= MODE_EDIT;
    end;
  end;
  SaveLastSpecimen;
  grid^.SetSelIndex(-1);
  WaitCursor(false);
end;

procedure TUIQEDlg.FillIsolate;
begin
  origIso^.CopyRecord(dbIso^.dbr);
  iFld^.XFerRecToCtl(dbIso^.dbr);
  oFld^.XFerRecToCtl(dbIso^.dbr);
  LocateIsoInGrid;
end;

function TUIQEDlg.CheckIsoSess(anIso: PDBRec): boolean;
{- given an isolate loaded into anIso, check it's session to see if it is
   in the currently loaded session, if not the prompt user to see if they wish
   to move it to the current session. If they do, then change the isolates session
   and restore it. Then return the updated record buffer.
   Return false if user does not wish to move or the move was unsuccessful,
   otherwise return true }
var
  ret   : boolean;
  seq   : TSeqNum;
  cur   : TSeqNum;
  pstr  : array[0..200] of char;
  p2    : array[0..100] of char;
  row   : longint;
begin
  ret:= false;
  cur:= GetCurrentSession(pstr, sizeof(pstr)-1);
  anIso^.GetField(DBIsoSess, @seq, sizeof(seq));
  if (cur <> 0) and (seq <> cur) then
  begin
    if not YesNoMsg(HWindow, SR(IDS_CONFIRM, p2, sizeof(p2)-1),
                             SR(IDS_ISONOTINSESSION, pstr, sizeof(pstr)-1)) then
      ret:= false
    else
    begin
      seq:= anIso^.GetSeqValue;
      if dbIso^.dbc^.GetSeq(dbIso^.dbr, seq) then
      begin
        dbIso^.dbr^.PutField(DBIsoSess, @cur); {- set the current session in the iso rec }
        if dbIso^.dbc^.UpdateRec(dbIso^.dbr) then
        begin
          anIso^.PutField(DBIsoSess, @cur); {- change in the original also}
          row:= grid^.AddString(BuildSort(dbIso^.dbr, dbSpec^.dbr, pstr, sizeof(pstr)-1));
          if (row < 0) then
            ShowError(@self, IDS_TOOMUCH, nil, 0, modID, 1)
          else
          begin
            SetRowData(row);
            ret:= true;
          end;
        end
        else
          ShowError(@self, IDS_DBERROR, nil, dbIso^.dbc^.dbErrorNum, modID, 0);
        if not ret then  {- at this point the isolate buffer is out of sync with the current row }
          ClearFields;   { so just clear everything }
      end
      else
        ShowError(@self, IDS_DBERROR, nil, dbIso^.dbc^.dbErrorNum, modID, 0);
    end;
  end
  else
    ret:= true;
  CheckIsoSess:= ret;
end;

procedure TUIQEDlg.WMFldList(var msg: TMessage);
{- called when user selects LIST in a field }
var
  id    : word;
  pstr  : array[0..100] of char;
  p2    : array[0..100] of char;
  ret   : longint;    {- return value to the UIFld that created this message }
  seq   : TSeqNum;
begin
  ret:= -1;   {- tell UI field passing this message NOT to list (non-zero) }

  uiFldsProcessFocus:= false;   {- turn off field focus processing }

  id:= msg.wParam;
  if (id = IDC_EDSPECID) or (id = IDC_EDCOLLECT) then
  begin
    if CheckSave(false) then
    begin
      sFld^.XferCtlToRec(tempSpec);
      cFld^.XferCtlToRec(tempSpec);
      if listObj^.SelectList(@self, tempSpec) then
      begin
        dbSpec^.dbr^.CopyRecord(tempSpec);
        FillSpecimen;
        specMode:= MODE_EDIT;
        EnableFlds;
        ShowModes;
      end
      else if listObj^.errorNum <> 0 then
      begin
        if listObj^.errorNum = LISTERR_PARTIALKEYNOTFOUND then
          ShowError(@self, IDS_CANNOTLISTNOMATCH, nil, listObj^.errorNum, modID, 0)
        else
          ShowError(@self, IDS_CANNOTLISTBAD, nil, listObj^.errorNum, modID, 0);
      end;
    end;
  end
  else if id = IDC_EDISO then
  begin
    if (specMode <> MODE_EDIT) then
      MessageBeep(0)
    else if (isoMode = MODE_NONE) or CheckSave(false) then
    begin
      tempIso^.ClearRecord;
      seq:= dbSpec^.dbr^.GetSeqValue;
      tempIso^.PutField(DBIsoSpecRef, @seq);
      if listObj^.SelectList(@self, tempIso) then
      begin
        if CheckIsoSess(tempIso) then
        begin
          dbIso^.dbr^.CopyRecord(tempIso);
          isoMode:= MODE_EDIT;
          FillIsolate;
          ShowModes;
        end
        else
        begin
          iFld^.Clear;
          iFld^.FocusFld;
        end;
      end
      else if listObj^.errorNum <> 0 then
      begin
        if listObj^.errorNum = LISTERR_PARTIALKEYNOTFOUND then
          ShowError(@self, IDS_CANNOTLISTNOMATCH, nil, listObj^.errorNum, modID, 0)
        else
          ShowError(@self, IDS_CANNOTLISTBAD, nil, listObj^.errorNum, modID, 0);
      end;
      EnableButtons;
      EnableFlds;
    end;
  end
  else if id = IDC_EDPATID then
  begin
    pFld^.XFerCtlToRec(dbPat^.dbr);
    if listObj^.SelectList(@self, dbPat^.dbr) then
    begin
      pFld^.XFerRecToCtl(dbPat^.dbr);
      origPat^.CopyRecord(dbPat^.dbr);
      patMode:= MODE_EDIT;
      EnableButtons;
    end
    else if listObj^.errorNum <> 0 then
    begin
      if listObj^.errorNum = LISTERR_PARTIALKEYNOTFOUND then
        ShowError(@self, IDS_CANNOTLISTNOMATCH, nil, listObj^.errorNum, modID, 0)
      else
        ShowError(@self, IDS_CANNOTLISTBAD, nil, listObj^.errorNum, modID, 0);
    end;
  end
  else
    ret:= 0;    {- this routine is not handling the list. Let the UIfld handle it }

  uiFldsProcessFocus:= true;  {- turn focus processing back on }

  SetWindowLong(HWindow, DWL_MSGRESULT, ret);  {- set message result (for dialogs) }
end;

procedure TUIQEDlg.DoSpecCollExit(newFocID: word);
var
  id  : word;
begin
  if specMode = MODE_NONE then
  begin
    {- see if both key segments have been entered for spec }
    if not sFld^.IsEmpty and not cFld^.IsEmpty then
    begin
      sFld^.XFerCtlToRec(tempSpec);
      cFld^.XFerCtlToRec(tempSpec);
      if listObj^.FindKey(@self, tempSpec, true) then
      begin
        {- found a specimen, load screen }
        dbSpec^.dbr^.CopyRecord(tempSpec);
        FillSpecimen;
        specMode:= MODE_EDIT;
      end
      else
      begin
        origSpec^.ClearRecord;
        dbSpec^.dbr^.ClearRecord;
        specMode:= MODE_NEW;
      end;
      EnableFlds;
      ShowModes;
      if newFocID = IDC_LIST then
        iFld^.FocusFld;
    end;
  end;
end;

procedure TUIQEDlg.WMLostFocus(var msg: TMessage);
var
  fld     : PUIFld;
  err     : integer;
  seq     : TSeqNum;
begin
  fld:= PUIFld(msg.lParam); {- get field pointer to fld losing focus }
  {- first format field and see if it is valid }
  err:= fld^.Format;
  if (err <> 0) and (err <> UIErrRequired) then {- ignore required fields on field exit }
    ShowUIFldError(@self, err, fld)
  else
  begin
    if (fld^.GetID = IDC_EDSPECID) or (fld^.GetID = IDC_EDCOLLECT) then
    begin
      DoSpecCollExit(msg.wParam);
    end
    else if fld^.GetID = IDC_EDISO then
    begin
      if (isoMode = MODE_NONE) and not iFld^.IsEmpty then
      begin
        seq:= dbSpec^.dbr^.GetSeqValue;
        iFld^.XFerCtlToRec(tempIso);
        tempIso^.PutField(DBIsoSpecRef, @seq);
        if listObj^.FindKey(@self, tempIso, true) then
        begin
          if CheckIsoSess(tempIso) then
          begin
            dbIso^.dbr^.CopyRecord(tempIso);
            isoMode:= MODE_EDIT;
            FillIsolate;
          end
          else
          begin
            iFld^.Clear;
            iFld^.FocusFld;
          end;
        end
        else
        begin
          origIso^.ClearRecord;
          dbIso^.dbr^.ClearRecord;
          isoMode:= MODE_NEW;
        end;
        EnableButtons;
        ShowModes;
        EnableFlds;
      end;
    end
    else if fld^.GetID = IDC_EDPATID then
    begin
      if pFld^.IsEmpty then
      begin
        patMode:= MODE_NONE;
        origPat^.ClearRecord;
        dbPat^.dbr^.ClearRecord;
      end
      else
      begin
        pFld^.XFerCtlToRec(dbPat^.dbr);
        if listObj^.FindKey(@self, dbPat^.dbr, true) then
        begin
          origPat^.CopyRecord(dbPat^.dbr);
          pFld^.XFerRecToCtl(dbPat^.dbr);
          patMode:= MODE_EDIT;
        end
        else
        begin
          dbPat^.dbr^.ClearRecord;
          origPat^.ClearRecord;
          patMode:= MODE_NEW;
        end;
      end;
      EnableButtons;
      ShowModes;
    end;
  end;
  SetWindowLong(HWindow, DWL_MSGRESULT, 0);  {- set message result (for dialogs) }
end;

procedure TUIQEDlg.WMCtlColor(var msg: TMessage);
begin
  if (msg.lParamLo = sModeTxt^.HWindow) or
     (msg.lParamLo = iModeTxt^.HWindow) then
  begin
    SetTextColor(msg.wParam, RGB($FF,0,0));
    msg.result:= GetStockObject(WHITE_BRUSH);
    SetWindowLong(HWindow, DWL_MSGRESULT, msg.result);  {- set message result (for dialogs) }
  end
  else
    DefWndProc(msg);
end;

procedure TUIQEDlg.ReloadPat(aSeq: TSeqNum);
var
  row     : longint;
  patSeq  : TSeqNum;
  pid     : array[0..25] of char;
begin
  patSeq:= aSeq;
  if listObj^.FindSeq(dbPat^.dbr, patSeq) then
  begin
    WaitCursor(true);
    pFld^.XFerRecToCtl(dbPat^.dbr);
    dbPat^.dbr^.GetFieldAsStr(DBPatPatID, pID, sizeof(pid)-1);
    for row:= 0 to grid^.GetRowCount do
    begin
      aSeq:= GetRowSeq(grid_PatSeq, row);
      if aSeq = patSeq then
        grid^.SetData(grid_PatID, row, pid);
    end;
    origPat^.CopyRecord(dbPat^.dbr);
    WaitCursor(false);
  end
  else
  begin
    ClearFields;
  end;
end;

procedure TUIQEDlg.ReloadSpec(aSeq: TSeqNum);
{- reload a specimen that has been loaded into the edit fields }
var
  specSeq   : TSeqNum;
  patSeq    : TSeqNum;
  isoSeq    : TSeqNum;
  row       : longint;
  sID       : array[0..25] of char;
  pID       : array[0..25] of char;
  cDate     : array[0..25] of char;
  pSeqStr   : array[0..25] of char;

  procedure GetIt(aFld: PUIFld); far;
  begin
    {- dont worry about the isolate here - just reloading specimen and it's patient}
    if aFld^.GetDBD = dbSpec^.dbd then
      aFld^.XFerRecToCtl(dbSpec^.dbr)
    else if aFld^.GetDBD = dbPat^.dbd then
      aFld^.XFerRecToCtl(dbPat^.dbr);
  end;

begin
  specSeq:= aSeq;
  if listObj^.FindSeq(dbSpec^.dbr, specSeq) then
  begin
    WaitCursor(true);
    origSpec^.CopyRecord(dbSpec^.dbr);
    dbSpec^.dbr^.GetField(DBSPECPatRef, @patSeq, sizeof(patSeq));
    dbPat^.dbr^.ClearRecord;
    listObj^.FindSeq(dbPat^.dbr, patSeq);
    origPat^.CopyRecord(dbPat^.dbr);

    flds^.ForEach(@GetIt);
    {- now scan through grid and update all specimen/patient infor that matches
       the seq's being reloaded }
    dbSpec^.dbr^.GetFieldAsStr(DBSPECSpecID, sID, sizeof(sid)-1);
    dbSpec^.dbr^.GetFieldAsStr(DBSPECCollectDate, cDate, sizeof(cDate)-1);
    dbPat^.dbr^.GetFieldAsStr(DBPatPatID, pID, sizeof(pid)-1);
    if patSeq <> 0 then
      Str(patSeq, pSeqStr)
    else
      StrCopy(pSeqStr, '');
    for row:= 0 to grid^.GetRowCount - 1 do
    begin
      aSeq:= GetRowSeq(grid_SpecSeq, row);
      if aSeq = specSeq then
      begin
        grid^.SetData(grid_SpecID, row, sid);
        grid^.SetData(grid_Collect, row, cDate);
        grid^.SetData(grid_PatID, row, pid);
        grid^.SetData(grid_PatSeq, row, pSeqStr);
      end;
    end;
    specMode:= MODE_EDIT;
    ShowModes;
    WaitCursor(false);
  end
  else
  begin
    ShowError(@self, IDS_ERRLODSPEC, nil, listObj^.errorNum, modID, 2);
    ClearFields;
  end;
end;

procedure TUIQEDlg.ReloadIsolate(aSeq: TSeqNum);
{- reload specified isolate and it's specimen and patient data (updating the grid
   with the current values }
var
  isoSeq    : TSeqNum;
  sessSeq   : TSeqNum;
  row       : longint;
  pstr      : array[0..50] of char;
  p1        : array[0..200] of char;

  function FindIsolate: longint;
  var
    k     : longint;
    found : boolean;
    seq   : TSeqNum;
  begin
    k:= 0;
    found:= false;
    while not found and (k < grid^.GetRowCount) do
    begin
      if GetRowSeq(grid_IsoSeq, k) = isoSeq then
        found:= true
      else
        Inc(k);
    end;
    if not found then
      k:= 0;
    FindIsolate:= k;
  end;

begin
  isoSeq:= aSeq;
  if listObj^.FindSeq(dbIso^.dbr, isoSeq) then
  begin
    WaitCursor(true);

    {- reload any spec/pat changes }
    dbIso^.dbr^.GetField(DBISOSpecRef, @aSeq, sizeof(aSeq));
    ReloadSpec(aSeq);

    {- find the row holding the isolate }
    row:= FindIsolate;

    {- See if the isolate has changed to a different session. If so remove
       it from the list }
    sessSeq:= GetCurrentSession(pstr, 100);
    dbIso^.dbr^.GetField(DBISOSess, @aSeq, sizeof(aSeq));
    if (sessSeq <> 0) and (sessSeq <> aSeq) then {- isolate is no longer in current session }
    begin
      InfoMsg(HWindow, SR(IDS_INFORMATION, pstr, 50), SR(IDS_ISOMOVED, p1, 200));
      grid^.DeleteString(row);
      ClearFields;
    end
    else
    begin {- otherwise, update the isolate in the specified row }
      origIso^.CopyRecord(dbIso^.dbr);
      iFld^.XFerRecToCtl(dbIso^.dbr);
      oFld^.XFerRecToCtl(dbIso^.dbr);
      dbIso^.dbr^.GetFieldAsStr(DBIsoIso, pstr, sizeof(pstr)-1);
      grid^.SetData(grid_Isolate, row, pstr);
      dbIso^.dbr^.GetFieldAsStr(DBIsoTstDate, pstr, sizeof(pstr)-1);
      grid^.SetData(grid_TestDate, row, pstr);

      dbIso^.dbr^.GetField(DBIsoOrdRef, @aSeq, sizeof(aSeq));
      if listObj^.FindMnemSeq(DBOrdFile, listRec, aSeq) then
      begin
        grid^.SetData(grid_Order, row, listRec^.desc);
        Str(listRec^.seq, pstr);
        grid^.SetData(grid_OrderSeq, row, pstr);
      end
      else
        grid^.SetData(grid_OrderSeq, row, '');
      isoMode:= MODE_EDIT;
      ShowModes;
    end;
    WaitCursor(false);
  end
  else
  begin
    ShowError(@self, IDS_ERRLODISO, nil, listObj^.errorNum, modID, 2);
    ClearFields;
  end;
end;

procedure TUIQEDlg.IDSpecimen(var msg: TMessage);
var
  seq : TSeqNum;
begin
  seq:= dbSpec^.dbr^.GetSeqValue;
  if seq <> 0 then
  begin
    UISpecimenEntry(@self, seq);
    HandleEvents(hWindow);  {- allow for redraw }
    ReloadSpec(seq);
    EnableButtons;
  end;
end;

procedure TUIQEDlg.IDPatient(var msg: TMessage);
var
  seq   : TSeqNum;

begin
  msg.wParam:= 0;
  msg.lParam:= longint(pFld);
  WMLostFocus(msg);
  if patMode = MODE_NEW then
    ShowError(@self, IDS_NOPATIENT, nil, 0, modID, 0)
  else
  begin
    seq:= dbPat^.dbr^.GetSeqValue;
    if (seq <> 0) then
    begin
      UIPatientEntry(@self, seq);
      HandleEvents(hWindow);  {- allow for redraw }
      ReloadPat(seq);
    end;
  end;
end;

procedure TUIQEDlg.IDIsolate(var msg: TMessage);
var
  seq   : TSeqNum;
begin
  seq:= dbIso^.dbr^.GetSeqValue;
  if (seq <> 0) then
  begin
    UIIsoEntry(@self, 0, seq);
    HandleEvents(hWindow);
    ReloadIsolate(seq);
    EnableButtons;
  end;
end;

procedure TUIQEDlg.IDMIC(var msg: TMessage);
var
  seq   : TSeqNum;
begin
  seq:= dbIso^.dbr^.GetSeqValue;
  if (seq <> 0) then
  begin
    if CheckSave(false) then
    begin
      UIIDMicEntry(@self, seq, false);
      HandleEvents(hWindow);
      ReloadIsolate(seq);
      EnableButtons;
    end;
  end;
end;

procedure TUIQEDlg.IDAS4(var msg: TMessage);
var
  origSeq : TSeqNum;
  seq     : TSeqNum;
  oldSel  : integer;
  row     : longint;
  pstr    : array[0..50] of char;
  wait    : TWaitCursor;
begin
  if CheckSave(false) then
  begin
    origSeq:= dbIso^.dbr^.GetSeqValue;
    pushSeq:= origSeq;
    AS4ReadSession(@self, origSeq);
    HandleEvents(hWindow);
    wait.Init;
    grid^.SetRedraw(false);
    for row:= 0 to grid^.GetRowCount - 1 do   {- reload test dates }
    begin
      seq:= GetRowSeq(grid_IsoSeq, row);
      dbIso^.dbc^.GetSeq(tempIso, seq);
      tempIso^.GetFieldAsStr(DBIsoTstDate, pstr, sizeof(pstr)-1);
      grid^.SetData(grid_TestDate, row, pstr);
    end;
    grid^.SetRedraw(true);
    wait.Done;
    if origSeq > 0 then
      ReloadIsolate(origSeq);
    PopPos;
  end;
end;

procedure TUIQEDlg.IDBarCodes(var msg: TMessage);
var
  wap     : PDBFile;
  ses     : TSeqNum;
  tstr    : array[0..2] of char;
  tray    : PTraysObject;
begin
  if CheckSave(false) then
  begin
    pushSeq:= dbIso^.dbr^.GetSeqValue;
    tempIso^.CopyRecord(dbIso^.dbr);
    ses:= GetCurrentSession(tstr, sizeof(tstr)-1);
    {- set proper key value }
    if INISpecimenListSort = 2 then
    begin
      if ses = 0 then
        dbIso^.dbc^.SetCurKeyNum(DBISO_CDateSpecID_KEY)
      else
        dbIso^.dbc^.SetCurKeyNum(DBISO_SessionCDateSpecID_KEY);
    end
    else
    begin
      if ses = 0 then
        dbIso^.dbc^.SetCurKeyNum(DBISO_SpecIDCDate_KEY)
      else
        dbIso^.dbc^.SetCurKeyNum(DBISO_SessionSpecIDCDate_KEY);
    end;
    wap:= New(PDBFile, Init(DBWAPNLFile, '', dbOpenNormal));
    tray:= New(PTraysObject, Init);

    PrintBarcodes(@self, MakeIntResource(DLG_BARCODES), ses, dbIso^.dbr^.GetSeqValue,
                  tray, listObj, dbIso, wap);
    HandleEvents(hWindow);
    MSDisposeObj(tray);
    MSDisposeObj(wap);
    dbIso^.dbr^.CopyRecord(tempIso);

    if pushSeq > 0 then
      ReloadIsolate(pushSeq);
    PopPos;
    sFld^.FocusFld;
  end;
end;

procedure TUIQEDlg.DeleteData;
var
  p1, p2  : array[0..100] of char;
  err     : integer;
  row     : longint;
begin
  if (dbIso^.dbr^.GetSeqValue > 0) then
  begin
    if YesNoMsg(HWindow, SR(IDS_CONFIRM, p1, 100), SR(IDS_CONFIRMDELISO, p2, 100)) then
    begin
      err:= 0;
      WaitCursor(true);
      if dbIso^.dbc^.GetSeq(dbIso^.dbr, dbIso^.dbr^.GetSeqValue) then
      begin
        if not dbIso^.dbc^.DeleteRec(dbIso^.dbr) then
          err:= dbIso^.dbc^.dbErrorNum
        else
        begin
          row:= grid^.GetSelIndex;
          ClearFields;
          if row >= 0 then
            grid^.DeleteString(row);
        end;
      end
      else
        err:= dbIso^.dbc^.dbErrorNum;

      if err <> 0 then
        ShowError(@self, IDS_CANTDEL, nil, err, modID, 0);
      WaitCursor(false);
    end;
  end;
end;

function TUIQEDlg.CheckSave(clearAfter: boolean): boolean;
var
  ret     : boolean;
  pstr    : array[0..200] of char;
  p2      : array[0..100] of char;
  k       : integer;
begin
  ret:= true;
  if (specMode <> MODE_NONE) or (patMode <> MODE_NONE) or (isoMode <> MODE_NONE) then
  begin
    {- first see if any data has changed }
    if DataChanged then
    begin
      k:= YesNoCancelMsg(HWindow, SR(IDS_CONFIRM, p2, 50), SR(IDS_CONFIRMSAVE, pstr, 200));
      if k = IDYES then
        ret:= SaveData(clearAfter)
      else if k = IDNO then
      begin
        if clearAfter then
          ClearFields;
      end
      else {- cancelled }
        ret:= false;
    end
    else if clearAfter then
      ClearFields;
  end;
  CheckSave:= ret;
end;

function TUIQEDlg.SaveData(clearAfter: boolean): boolean;
{- save the data for the isolate. After saving an isolate, order will need to
   be created (NEW mode only) and results will have to be created as well }
var
  ret           : boolean;
  isoOrd        : PDBFile;
  extraTst      : integer;  {- indole. oxidase, betaHem (depending on family) }
  setFamily     : word;
  pstr          : array[0..100] of char;
  row           : longint;
  newOrderMade  : boolean;  {- this is set to true when a new order has been made
                               on the isolate }

  function ValidateScreen: boolean;
  {- check screen for validity }
  var
    sret    : boolean;
    fld     : PUIFld;
    errNum  : integer;
    aSet, aFam: byte;
    aTest   : integer;
    seq     : TSeqNum;

    function IsInvalid(aFld: PUIFld): boolean; far;
    begin
      errNum:= aFld^.Format;
      IsInvalid:= errNum <> 0;
    end;

  begin
    {- first see if any fields are invalid }
    sret:= true;
    fld:= flds^.FirstThat(@IsInvalid);
    if (fld <> nil) then
    begin {- some other error }
      ShowUIFldError(@self, errNum, fld);
      sRet:= false;
    end
    else
    begin
      {- automatic prompting for family and extra test stuff only occurs when
         a new isolate is being created }
      if newOrderMade then
      begin
        aSet:= ExtractSet(setFamily);
        aFam:= ExtractFamily(setFamily);
        if (aSet in [1, 2, 5, 21, 3]) then {- family and extra test info required }
        begin
          if aSet in [2, 5, 21] then  {- default to staph }
          begin
            aFam:= 2;
            setFamily:= MakeSetFamily(aSet, 2);
          end;
          WaitCursor(false);
          uiFldsProcessFocus:= false;
          oFld^.XFerCtlToRec(tempIso);
          tempIso^.GetField(DBIsoOrdRef, @seq, sizeof(seq));
          sret:= SelectFamily(@self, seq, setFamily, extraTst);
          HandleEvents(hWindow);  {- allow for redraw }
          uiFldsProcessFocus:= true;
          WaitCursor(true);
        end;
      end
      {Otherwise use the current SetFamily}
      else dbIso^.dbr^.GetField(DBIsoSetFamily, @setFamily,SizeOf(setFamily));
    end;
    ValidateScreen:= sret;
  end;

  function StartTransaction: boolean;
  var
    errNum  : integer;
  begin
    errNum:= DBBeginTransaction;
    if errNum <> 0 then
    begin
      ShowError(@self, IDS_TRANSERROR, nil, errNum, modID, 0);
      StartTransaction:= false;
    end
    else
      StartTransaction:= true;
  end;

  function LockRecord: boolean;
  {- lock records and place current content (in DB) of record in tempXXX.
     if the records are not found then the user is prompted to recreate in which
     case the appropriate mode (mode, patMode) is modified accordingly }
  var
    sret  : boolean;
  begin
    {- no need to lock if adding a new isolate }
    if specMode = MODE_EDIT then
    begin
      tempSpec^.CopyRecord(origSpec);
      sRet:= LockIt(dbSpec, tempSpec, IDS_SPECLOCKED, IDS_CANTLOCATEORIGSPEC, specMode);
    end;
(*    if sret and (patMode = MODE_EDIT) then*)
(*    begin*)
(*      tempPat^.CopyRecord(origPat);*)
(*      sRet:= LockIt(dbPat, tempPat, IDS_PATLOCKED, IDS_CANTLOCATEORIGPAT, mode);*)
(*    end;*)
    if sret and (isoMode = MODE_EDIT) then
    begin
      tempIso^.CopyRecord(origIso);
      sRet:= LockIt(dbIso, tempIso, IDS_ISOLOCKED, IDS_CANTLOCATEORIGISO, mode);
    end;
    LockRecord:= sret;
  end;

  function CheckModified: boolean;
  {- at this point the original records are locked. Now see if someone else has
     modified them since originally loaded. If so prompt for overwrite. Assumes
     the current contents of the record are placed in tempSpec/tempPat }
  var
    sret    : boolean;
    p1, p2  : array[0..200] of char;
  begin
    sret:= true;
    if isoMode = MODE_EDIT then    {- only valid if editing }
    begin
      if not origIso^.CompareRecord(tempIso) then
        sret:= YesNoMsg(HWindow, SR(IDS_WARNING, p1, sizeof(p1)-1), SR(IDS_ISOMODIFIED, p2, sizeof(p2)-1));
    end;
(*    if sret and (patMode = MODE_EDIT) then*)
(*    begin*)
(*      if not origPat^.CompareRecord(tempPat) then*)
(*        sret:= YesNoMsg(HWindow, SR(IDS_WARNING, p1, sizeof(p1)-1), SR(IDS_PATMODIFIED, p2, sizeof(p2)-1));*)
(*    end;*)
    if sret and (specMode = MODE_EDIT) then
    begin
      if not origSpec^.CompareRecord(tempSpec) then
        sret:= YesNoMsg(HWindow, SR(IDS_WARNING, p1, sizeof(p1)-1), SR(IDS_SPECMODIFIED, p2, sizeof(p2)-1));
    end;
    CheckModified:= sRet;
  end;

  function SaveIt: boolean;
  var
    sRet  : boolean;
    eNum  : integer;
    seq   : TSeqNum;
    p1    : array[0..100] of char;
    tBool : boolean;

    procedure XFerToRec(aFld: PUIFld); far;
    begin
      if aFld^.GetDBD = dbSpec^.dbd then
        aFld^.XFerCtlToRec(dbSpec^.dbr)
      else if aFld^.GetDBD = dbIso^.dbd then
        aFld^.XFerCtlToRec(dbIso^.dbr)
      else if aFld^.GetDBD = dbPat^.dbd then
        aFld^.XFerCtlToRec(dbPat^.dbr)
    end;

  begin
    sRet:= true;

    {- get seq from iso record (just in case the user moved the isolate }
    dbIso^.dbr^.GetField(DBIsoSess, @seq, sizeof(seq));

    {- first copy the original db contents of the records into the records that
       are going to be stored. This is because QE only allows editing of
       partial record data, therefore, I have to get the contents of the other
       (non-editable) fields into the work buffers}
    dbIso^.dbr^.CopyRecord(origIso);
    dbIso^.dbr^.PutField(DBISOSess, @seq);

    dbSpec^.dbr^.CopyRecord(origSpec);
    dbPat^.dbr^.CopyRecord(origPat);

    {- now transfer all fields to the working record buffers }
    flds^.ForEach(@XFerToRec);

    {- First, store patient (only necessary if NEW mode. EDIT mode does not
       allow changes to the patient record }
    if patMode = MODE_NEW then
    begin
      dbPat^.dbc^.InsertRec(dbPat^.dbr);

      eNum:= 0;
      case dbPat^.dbc^.dbErrorNum of
        MOD_BTRV + 5: eNum:= IDS_DUPPAT;
        else
          eNum:= IDS_PATSAVERROR;
      end;
      if dbPat^.dbc^.dbErrorNum <> 0 then
      begin
        ShowError(@self, eNum, nil, dbPat^.dbc^.dbErrorNum, modID, 0);
        sret:= false;
      end;
    end;

    {- then store specimen }
    if sRet then
    begin
      seq:= dbPat^.dbr^.GetSeqValue;
      dbSpec^.dbr^.PutField(DBSPECPatRef, @seq);  {- patient reference }

      if specMode = MODE_NEW then
      begin
        {- set default "Preliminary" status }
        listRec:= New(PMnemListRec, Init);
        if (listRec <> nil) and listObj^.FindMnemID(@self, DBStatFile, '1', listRec, true) then
          dbSpec^.dbr^.PutField(DBSpecStat, @listRec^.seq);
        dbSpec^.dbc^.InsertRec(dbSpec^.dbr);
      end
      else
        dbSpec^.dbc^.UpdateRec(dbSpec^.dbr);
      eNum:= dbSpec^.dbc^.dbErrorNum;

      if eNum <> 0 then
      begin
        {- special error handling for common errors }
        case eNum of
          MOD_BTRV + 5: eNum:= IDS_DUPSPEC;
          else
            eNum:= IDS_CANNOTSAVE;
        end;
        ShowError(@self, eNum, nil, dbSpec^.dbc^.dbErrorNum, modID, 0);
        sFld^.FocusFld;
        sret:= false;
      end;
    end;

    {- and finally store isolate }
    if sret then
    begin
      {- get specimen seq for isolate/specimen reference and ID/Collect date}
      seq:= dbSpec^.dbr^.GetSeqValue;
      dbIso^.dbr^.PutField(DBIsoSpecRef, @seq);

      dbSpec^.dbr^.GetField(DBSpecSpecID, @p1, sizeof(p1)-1);
      dbIso^.dbr^.PutField(DBIsoSpecID, @p1);
      dbSpec^.dbr^.GetFieldAsStr(DBSPECCollectDate, p1, sizeof(p1)-1);
      dbIso^.dbr^.PutFieldAsStr(DBISOCollectDate, p1);

      {- handle set/family field }
      dbIso^.dbr^.PutField(DBIsoSetFamily, @setFamily);

      {- clear XMit Flag }
      tBool:= false;
      dbIso^.dbr^.PutField(DBISOTransFlag, @tBool);

      if isoMode = MODE_NEW then
      begin
        {- get current session (if new isolate mode only) }
        seq:= GetCurrentSession(p1, sizeof(p1)-1);
        dbIso^.dbr^.PutField(DBIsoSess, @seq);
        dbIso^.dbc^.InsertRec(dbIso^.dbr);  {- add record }
      end
      else
        dbIso^.dbc^.UpdateRec(dbIso^.dbr);
      eNum:= dbIso^.dbc^.dbErrorNum;

      if eNum <> 0 then
      begin
        {- special error handling for common errors }
        case eNum of
          MOD_BTRV + 5: eNum:= IDS_DUPISO;
          else
            eNum:= IDS_CANNOTSAVE;
        end;
        ShowError(@self, eNum, nil, dbIso^.dbc^.dbErrorNum, modID, 99);
        sret:= false;
      end;
    end;

    {- update any isolate records that may be affected }
    if sRet and (specMode = MODE_EDIT) then
    begin
      eNum:= UpdateSpecIsos(dbIso, tempIso, dbSpec^.dbr);
      if eNum <> 0 then
      begin
        ShowError(@self, IDS_ERRUPDISOS, nil, eNum, modID, 0);
        sRet:= false;
      end;
    end;

    SaveIt:= sret;
  end;

  procedure UnlockRecords;
  begin
    if isoMode = MODE_EDIT then
      dbIso^.dbc^.UnlockRec(tempIso);
    if specMode = MODE_EDIT then
      dbSpec^.dbc^.UnlockRec(tempSpec);
(*    if patMode = MODE_EDIT then*)
(*      dbPat^.dbc^.UnlockRec(tempPat);*)
  end;

var
  seq   : TSeqNum;
  pr    : PDBRec;
  msg   : TMessage;
begin
  msg.wParam:= 0;   {- force patient mode to be established }
  msg.lParam:= longint(pFld);
  WMLostFocus(msg);
  msg.wParam:= 0;   {- force order to be established }
  msg.lParam:= longint(oFld);
  WMLostFocus(msg);

  SetLastSpecimen(nil, nil);  {- clear last specimen }

  isoOrd:= nil;
  extraTst:= 0;
  setFamily:= 0;

  WaitCursor(true);

  {- figure out if a new order has been placed on the isolate. }
  if origIso^.FieldIsEmpty(DBISOOrdRef) and not oFld^.IsEmpty then
  begin
    newOrderMade:= true;
    oFld^.XFerCtlToRec(tempIso);
    tempIso^.GetField(DBIsoOrdRef, @seq, sizeof(seq));
    if seq <> 0 then
    begin
      pr:= listObj^.PeekSeq(DBORDFile, seq);
      if pr <> nil then
      begin
        pr^.GetField(DBTstGrpSet, @setFamily, sizeof(setFamily));
        setFamily:= setFamily shl 8;
      end;
    end;
  end
  else
    newOrderMade:= false;

  ret:= true;
  if ret then ret:= ValidateScreen;
  if ret then ret:= StartTransaction;
  if ret then ret:= LockRecord;
  if ret then ret:= CheckModified;
  if ret then ret:= SaveIt;
  if ret and newOrderMade then
  begin
    ret:= CreateOrders(@self, dbIso, isoOrd, results);
    if ret then
      ret:= CreateResults(dbIso, results, setFamily, extraTst);
  end;
  UnlockRecords;
  if ret then
    DBEndTransaction
  else
    DBAbortTransaction;

   MSDisposeObj(isoOrd);

  if ret then
    SaveLastSpecimen;

  {- now that the data has been saved, add it to the grid if neccessary  }
  if ret then
  begin
    {- if a grid row is already selected then that row is being edited otherwise
       a new row needs to be added }
    if grid^.GetSelIndex < 0 then
    begin
      row:= grid^.AddString(BuildSort(dbIso^.dbr, dbSpec^.dbr, pstr, sizeof(pstr)-1));
      if (row < 0) then
        ShowError(@self, IDS_TOOMUCH, nil, 0, modID, 1)
      else
        SetRowData(row);
    end
    else
      row:= grid^.GetSelIndex;

    {- ensure that all data is updated in the grid }
    ReloadIsolate(dbIso^.dbr^.GetSeqValue);

    {- now resort }
    PushPos;
    grid^.Sort(0, 100, false);
    PopPos;
  end;

  {- and finally, clear fields }
  if ret and clearAfter then
    ClearFields;

  SaveData:= ret;

  WaitCursor(false);
end;

procedure TUIQEDlg.ShowModes;
var
  pstr  : array[0..50] of char;

  procedure ShowIt(aMode: integer; hw: HWnd; newID, editID: word);
  begin
    case aMode of
      MODE_EDIT : SR(editID, pstr, 50);
      MODE_NEW  : SR(newID, pstr, 50);
    else
      StrCopy(pstr, '');
    end;
    SendMessage(hw, WM_SETTEXT, 0, longint(@pstr));
  end;

begin
  ShowIt(specMode, sModeTxt^.HWindow, IDS_NEWSPEC, IDS_EDITSPEC);
  ShowIt(isoMode, iModeTxt^.HWindow, IDS_NEWISO, IDS_EDITISO);
end;

procedure TUIQEDlg.RecallLastSpecimen;
var
  p1, p2    : array[0..50] of char;
begin
  if (specMode = MODE_NONE) and (StrLen(lastSpec) > 0) and (StrLen(lastCD) > 0) then
  begin
    ClearFields;
    tempSpec^.PutFieldAsStr(DBSpecSpecID, lastSpec);
    tempSpec^.PutFieldAsStr(DBSpecCollectDate, lastCD);
    sFld^.XFerRecToCtl(tempSpec);
    cFld^.XFerRecToCtl(tempSpec);
    DoSpecCollExit(0); {- this will force iso fld to be focused }
    iFld^.FocusFld;
  end
  else
    MessageBeep(0);
end;

procedure TUIQEDlg.PrintData;
var
  pi      : PPrintInfo;
  pd      : PPrnPrevDlg;
  lf      : TLogFont;
  p1,p2   : array[0..100] of char;
  seq     : TSeqNum;
  tSeq    : TSeqNum;
  sessSeq : TSeqNum;
  isOK    : boolean;
  stat    : PPctStatDlg;
  count   : longint;
begin
  MakeScreenFont(lf, false, false);
  pi:= New(PPrintInfo, Init(lf, INIDefPointSize));

  with pi^.header^ do
  begin
    AddRow(r_Normal, 0);
    AddCell(SR(IDS_SESSIONLOG, p1, sizeof(p1)-1),  LT400, c_Bold, -4);
    AddCell(SR(IDS_PRINTEDON, p1, sizeof(p1)-1), LT200, c_Date or c_Right or c_PageRight, 0);
    AddRow(r_Normal, 0);
    sessSeq:= GetCurrentSession(p1, sizeof(p1)-1);
    if sessSeq = 0 then
      SR(IDS_SESSIONSDISABLED, p1, sizeof(p1)-1);
    AddCell(p1, LT400, c_Bold, 0);
    AddRow(r_Normal, 0);
    AddRow(r_BorderBottom, 0);
    dbSpec^.dbd^.FieldName(DBSpecSpecID, p1, sizeof(p1)-1);
    dbSpec^.dbd^.FieldName(DBSpecCollectDate, p2, sizeof(p2)-1);
    StrCat(p1, '/');
    StrCat(p1, p2);
    AddCell(p1, LT200, c_Normal, 0);
    dbIso^.dbd^.FieldName(DBIsoIso, p1, sizeof(p1)-1);
    AddCell(p1, LT100, c_Normal, 0);
    dbIso^.dbd^.FieldName(DBISOOrdRef, p1, sizeof(p1)-1);
    AddCell(p1, LT300, c_Normal, 0);
    dbPat^.dbd^.FieldName(DBPatPatID, p1, sizeof(p1)-1);
    AddCell(p1, LT200, c_Normal, 0);
  end;
  with pi^.footer^ do
  begin
    AddRow(r_CenterRow or r_BorderTop, 0);
    AddCell(SR(IDS_PAGEPOFN, p1, sizeof(p1)-1), LT300, c_Center or c_PageNum or c_NumPages, 0);
  end;

  if sessSeq > 0 then
  begin
    if INISpecimenListSort = 2 then
      dbIso^.dbc^.SetCurKeyNum(DBISO_SessionCDateSpecID_KEY)
    else
      dbIso^.dbc^.SetCurKeyNum(DBISO_SessionSpecIDCDate_KEY);
    dbIso^.dbc^.GetFirst(tempIso);

    tempIso^.ClearRecord;
    tempIso^.PutField(DBIsoSess, @sessSeq);
    isOK:= dbIso^.dbc^.GetFirstContains(tempIso);
    if isOK then
    begin
      count:= 0;
      stat:= New(PPctStatDlg, Init(@self, MakeIntResource(DLG_FILL), true, 0));
      application^.MakeWindow(stat);
(*      SetFocus(stat^.HWindow);*)

      tempIso^.GetField(DBIsoSess, @tSeq, sizeof(tseq));
      isOK:= (tSeq = sessSeq);
      while isOK and stat^.CanContinue do
      begin
        Inc(count);
        Str(count, p1);
        stat^.SetText(p1, IDC_TEXT);

        pi^.body^.AddRow(r_Normal, 0);

        tempIso^.GetField(DBIsoSpecRef, @tseq, sizeof(tSeq));
        if not dbSpec^.dbc^.GetSeq(tempSpec, tSeq) then
        begin
          tempIso^.GetFieldAsStr(DBIsoSpecID, p1, sizeOf(p1)-1);
          tempIso^.GetFieldAsStr(DBIsoSpecID, p2, sizeOf(p1)-1);
          strcat(p1, ' ');
          strcat(p1, p2);
          SR(IDS_ERRLODSPEC, p2, sizeof(p2)-1);
          StrCat(p2, ' -- ');
          strcat(p2, p1);
          pi^.body^.AddCell(p1, 0, c_Bold or c_Stretch, 0);
        end
        else
        begin
          tempSpec^.GetFieldAsStr(DBSpecSpecID, p1, sizeof(p1));
          pi^.body^.AddCell(p1, lt100, c_Normal, 0);
          tempSpec^.GetFieldAsStr(DBSpecCollectDate, p1, sizeof(p1));
          pi^.body^.AddCell(p1, lt100, c_Normal, 0);
          tempSpec^.GetField(DBSpecPatRef, @tSeq, sizeof(tSeq));
          if dbPat^.dbc^.GetSeq(tempPat, tSeq) then
            tempPat^.GetFieldAsStr(DBPatPatID, p2, sizeof(p2)-1)
          else
            StrCopy(p2, '');

          {- isolate num }
          tempIso^.GetFieldAsStr(DBIsoIso, p1, sizeof(p1)-1);
          pi^.body^.AddCell(p1, lt100, c_Normal, 0);

          {- order }
          tempIso^.GetField(DBIsoOrdRef, @tSeq, sizeof(tSeq));
          if listObj^.FindMnemSeq(DBOrdFile, listRec, tSeq) then
            StrCopy(p1, listRec^.desc)
          else
            StrCopy(p1, '');
          pi^.body^.AddCell(p1, lt300, c_Normal, 0);

          {- patient id }
          pi^.body^.AddCell(p2, lt200, c_Normal, 0);
        end;

        isOK:= dbIso^.dbc^.GetNext(tempIso);
        if isOK and (sessSeq <> 0) then
        begin
          tempIso^.GetField(DBIsoSess, @tSeq, sizeof(tseq));
          isOK:= (tSeq = sessSeq);
        end;
      end; {- while }
      MSDisposeObj(stat);
    end;
  end;

  pd:= New(PPrnPrevDlg, Init(@self, pi, 'Session Log'));
  application^.ExecDialog(pd);
  HandleEvents(hWindow);

  MSDisposeObj(pi);
end;

function TUIQEDlg.DataChanged: boolean;
{- compare the current data with the originally loaded data.  If any data has
   changed, then return true }

  function IsModified(aFld: PUIFld): boolean; far;
  begin
    if aFld^.GetDBD = dbiso^.dbd then
      IsModified:= aFld^.IsModified(origIso)
    else if aFld^.GetDBD = dbPat^.dbd then
      IsModified:= aFld^.IsModified(origPat)
    else if aFld^.GetDBD = dbSpec^.dbd then
      IsModified:= aFld^.IsModified(origSpec)
    else
      IsModified:= false;
  end;

var
  origSessSeq : TSeqNum;
  curSessSeq  : TSeqNum;
  ret         : boolean;
  seq         : TSeqNum;
begin
  ret:= false;
  if flds^.FirstThat(@IsModified) = nil then
  begin
    {- see if the session changed for the isolate }
    origIso^.GetField(DBIsoSess, @origSessSeq, sizeof(origSessSeq));
    dbIso^.dbr^.GetField(DBIsoSess, @curSessSeq, sizeof(curSessSeq));
    ret:= origSessSeq <> curSessSeq;

    if not ret then {- see if the selected patient is different from the origial on the spec}
    begin
      origSpec^.GetField(DBSpecPatRef, @seq, sizeof(seq));
      ret:= dbPat^.dbr^.GetSeqValue <> seq;
    end;
  end
  else
    ret:= true;
  DataChanged:= ret;
end;

procedure UIQuickOrdEntry(aParent: PWindowsObject);
begin
  application^.ExecDialog(New(PUIQEDlg, Init(aParent)));
end;

END.

{- rcf }
