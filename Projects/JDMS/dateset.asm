.MODEL large, pascal

public  DateSet
;
;	New DateSet with generic dates
;
;	December 13, 1987	CPD
;
.DATA
        extrn    ddlm   : byte
        extrn    didx   : byte
        extrn    dbgn   : byte
        extrn    dend   : byte
        extrn    dprev  : byte
        extrn    dthis  : byte
        extrn    dnext  : byte
        extrn    dday   : byte
        extrn    dmon   : byte
        extrn    dqtr   : byte
        extrn    dyr    : byte

.CODE
;
; Function DateSet( DateFmt : String ) : Boolean;
;
; Set the date format to be used by DatePack and DateUnpack.
; Return TRUE if the format is valid.
;
; Format is: <delim><xref1><xref2><xref3><beptndmqy>
;
; Where:
; <deilm> is the field delimitter used by DateUnpack
; <xref1> is the field number which is first in the date
; <xref2> is second, <xref3) is third. Legal values of <xrefx> are
;          0,1,2 where 0 = Day, 1=Month, 2=Year
; <beptndmqy> are the starting letters for
;			Begin, End
;			Previous, This, Next
;			Day, Month, Quarter, Year
;
; For US dates, the format would be: /102BEPTNDMQY
;

DateFmt         equ dword ptr ss:[bp+6]
PSize           equ 4

DateSet Proc Far
     push bp
	mov	bp,sp		; setup stack
	push	ds

	lds	si,DateFmt	; get ptr to format string
     cld
	lodsb			; get length
	cmp	al,13		; enough there?
	jl	bad_fmt

     lea  di, ddlm
	movsb			; get deilmitter
     mov  cx,3           ; get day, month, year position
idx_loop:
	lodsb			; get a chr
	cmp	al,'0'		; check for legal
	jl	bad_fmt
	cmp	al,'2'
	jg	bad_fmt
	sub	al,'0'
	stosb			; save in table
	loop	idx_loop

     mov  cx,9           ; get special chrs
	rep	movsb		; save the specials

     mov  ax,1           ; return success
	jmp	short ds_exit
bad_fmt:
	xor	ax,ax
ds_exit:
	pop	ds
	pop	bp
	ret	PSize
DateSet	Endp

	END
