{$A-}
unit Cvt18Msc;

INTERFACE

uses
  OWindows,
  ODialogs,
  DBFile,
  dbtypes,
  DBLib,
  IntlLib;

const
  continueJob            =  0;
  cancelJob              =  2;
  tryNewDisk             =  3;
  RestoreComplete        =  5;
  sem_FailCriticalErrors =  $0001;
  customDisk             = 'Custom Files';
  PanelOnly              =  1;
  PanelANDDrugs          =  2;
  AddDrugsOnly           =  3;
  NoResults              =  4;
  NullPanel              =  9;

type

  testCode      = array[0..5] of char;
  panelTestList = array[1..50] of testCode;
  addDrugCodes  = array[1..5] of testCode;

  {Panel ID look up}
  PPanelID = ^TPanelID;
  TPanelID = record
    panelDesc : array[0..33] of char;
    tstgrp    : array[0..9] of char;
    tstgrpRef : TSeqNum;
    tstgrpSet : byte;
    orderRef  : TSeqNum;
    orderId   : array[0..9] of char;
    orderDesc : array[0..33] of char;
    isolateID : array[0..3] of char;
    numTests  : integer;
    testID    : panelTestList;
  end;

  { Additonal Drugs from the result record }
  DrugRec = record
    drugAbbr : array[0..3] of char;
    drugName : array[0..16] of char;
  end;
  PDrugList =  ^drugList;
  drugList  = array[1..5] of DrugRec;

  addDrugType = record
    flag  : boolean;
    mic   : byte;
    susc  : byte;
    drug  : array[0..2] of char;
  end;

  OffDrug = record
    pn      : byte;
    offset  : integer;
    numMics : byte;
    micMap  : longInt;
    abbr    : array[0..2] of char;
    drugName: array[0..15] of char;
    recTyp  : char;
    unUsed  : array[0..8] of char;
  end;

  bitBio = array[0..5] of byte;
  biot   = record
    format : integer;
    bits   : BitBio;
  end;

  nameDesc = record
    orgNum  : integer;
    orgSC   : byte;
    orgMap  : array[0..7] of byte;
    orgFtn  : array[0..3] of integer;
    orgSpc  : array[0..2] of byte;
    orgName : array[0..15] of char;
  end;

  {version 18 conversion types}
  {record pointer structure for free text and interp data files}
  TRecPtr = record
    lDrn : word;
    hDrn : byte;
  end;

  micLevels = array[0..35] of byte;
  {record for the DM$DATA.DAT file}
  TV18DataRec = record
    case integer of
      1: (varNum       : byte;
          data         : array[0..94] of byte);
      2: (pVarNum      : byte;   {var = 0}
          pat_id       : array[0..11] of char;
          pat_lastName : array[0..15] of char;
          pat_firstName: array[0..15] of char;
          pat_DOB      : word;
          pat_sex      : byte;
          pat_race     : char;
          pat_dateIn   : word;
          pat_dateOut  : word;
          pat_physID   : array[0..2] of char;
          pat_ward     : array[0..2] of char;
          pat_roomNum  : array[0..4] of char;
          pat_pFlags   : integer;
          pat_freeText : TRecPtr;
          pat_institute: array[0..2] of char;
          pat_service  : array[0..2] of char;
          pat_extra    : array[0..20] of byte);
     3:  (sVarNum      : byte;   {var = 1}
          spec_patID   : array[0..11] of char;
          spec_id      : array[0..7] of char;
          spec_ward    : array[0..2] of char;
          spec_source  : array[0..2] of char;
          spec_collDate: word;
          spec_collTime: integer;
          spec_ordDate : word;
          spec_ordPhys : array[0..2] of char;
          spec_recvDate: word;
          spec_recvTime: integer;
          spec_extraA  : array[0..39] of byte;
          spec_sFlags  : integer;
          spec_freeText: TRecPtr;
          spec_techID  : array[0..2] of char;
          spec_extraB  : array[0..7] of byte);
      4: (rVarNum      : byte;    {var = 2}
          res_specID   : array[0..7] of char;
          res_isoNum   : array[0..2] of char;
          res_rVarTag  : byte;    {varTag = 0}
          res_WATrayId : integer;
          res_trayType : byte;
          res_testDate : word;
          res_ward     : array[0..2] of char;
          res_source   : array[0..2] of char;
          res_organism : integer;
          res_OrgFlags : byte;
          res_biotype  : Biot;
          res_xtraTests: integer;
          res_rFlags   : integer;
          res_extraC   : byte;
          res_MicLevels: micLevels;
          res_extraD   : array[0..2] of byte;
          res_freeText : TRecPtr;
          res_addDrugs : TRecPtr;
          res_interp   : TRecPtr;
          res_extraE   : array[0..5] of byte;
          res_sysFlag  : byte);
      5: (dVarNum      : byte;    {var = 2}
          drg_specID   : array[0..7] of char;
          drg_isoNum   : array[0..2] of char;
          drg_rVarTag  : byte;    {varTag = 1}
          drg_cnt      : byte;
          drg_drgType  : array[0..4] of addDrugType;
          drg_res      : array[0..50] of byte;
          drg_sysFlag  : byte);
      7: (iVarNum      : byte;    {var = 2}
          int_specID   : array[0..7] of char;
          int_isoNum   : array[0..2] of char;
          int_rVarTag  : byte;    {varTag = 3}
          int_Interp   : array[0..81] of byte;
          int_SysFlag  : byte);
  end;

  {record for the CUSTOM.FIL file, v18 mnemonics}
  TV18CustomRec = record
    case integer of
      1: (varNum     : byte;
          data       : array[0..30] of byte);
      2: (mVarNum    : byte;
          mnemonic   : array[0..2] of char;
          description: array[0..23] of char;
          flag       : byte;
          extra      : array[0..2] of byte);
  end;

  {record for the DM$FREE.DAT file}
  (* original rec format
               FDelFlag  : BYTE;        { Varient/delete flag   =2      }
               FSpecimen : Spec;        { specimen number       8(X)    }
               FIsolate  : IsoT;        { isolate number        99X     }
               FVarTag   : BYTE;        { varient tag           =2      }
               Txt       : TxtType;     { packed free text              }
               SeqNum    : BYTE;        { sequence number               }
               FLink     : RecPtr;      { link to next free text rec    }
               FSysFlags : BYTE;        { BIT MEANING                   }
                                        {  0 - archived                 }
  *)
  TV18FreeTextRec = record
    mVarNum       : byte;
    specimenID    : array[0..7] of char;
    isolateNum    : array[0..2] of char;
    varTag        : byte;
    freeTextLen   : byte;
    freeText      : array[0..76] of char;
    seqNum        : byte;
    extra         : array[0..3] of byte; {96 bytes}
  end;

  {record for mnemonic description retrieval}
  TV18MnemRec = record
    mnemonic    : array[0..3] of char;
    description : array[0..24] of char;
    flag        : byte;
  end;

var
  panels : array[1..20] of TPanelID;


procedure ByteToNCCLS(intrp: PChar; bVal: byte; isUD, isUrine: boolean; var isContraInd: boolean);
procedure ByteToMIC(mic: PChar; bVal: byte);
procedure UnpkBio(var biotype: Biot; biostr: pChar);
function  LoadIndexFiles(aDir: PChar): integer;
function  CreateIndexFiles(aDir: PChar): integer;
procedure GetV18Field(v30Str, v18Str: PChar; size: integer);
function  ConvertV18Date(cDate : word): TIntlDate;
function  ConvertV18Time(cTime : integer): TIntlTime;
procedure ConvertV18FreeText(aPath, mFT: PChar; rec: TRecPtr);
function  GetConversionType(panelNum: byte; addDrgRecNum: longInt): integer;
function Convert_Isolate( isoFile: PDBFile;
                          rPath: PChar;
                          mPath: PChar;
                          isoID: PChar;
                          iSpecSeqRef: TSeqNum;
                          iSet: byte;
                          iOrg: integer;
                          iTestDate: TIntlDate;
                          iOrder: TSeqNum;
                          iFTRec: TRecPtr;
                          iTnsFlg: boolean;
                          iSpecID: PChar;
                          iSpecCDate: TIntlDate;
                          aList: PListBox): TSeqNum;
procedure GetDrugNames(aPath: PChar; v18Rec: TV18DataRec; drugs: PDrugList);
procedure LoadPanelInfo;
procedure LoadTstGrpInfo(mPath: PChar);

IMPLEMENTATION

uses
  DBIDS,
  V18Date,
  Cvtcmp,
  CvtMisc,
  Mscan,
  Strings,
  StrSW;

type
  PByteArray = ^byteArray;
  byteArray  = Array[0..0] of byte;
  micStr     = array[0..4] of char;
  ncclsStr   = array[0..5] of char;

const
  maxInterpStrLen     = 4;
  RESmaxStrResult     = 25;
  greaterThan         = $20;
  lessThan            = $40;
  nrBit               = $80;

  mda : array[0..17] of micStr  = ('N/R', '.03', '.06', '.12', '.25', '0.5',
                                   '1', '2', '4', '6', '8', '16',
                                   '25', '32', '64', '100', '128', '256');

  ncTher : array[0..15] of ncclsStr =
   { 0     1     2   3   4    5    6   7     8    9     10     11     12     13   14    15 }
  ('N/A','TFG','MS','S','R','N/R','','BLac','I','****','****','****','****','IB','MSRA','');

{** uses BlkShift.asm **}
{$L BlkShift}
Procedure BlkShift(Target: PByteArray; Size: Integer; Count: Integer); far; external;


procedure ByteToNCCLS(intrp: PChar; bVal: byte; isUD, isUrine: boolean; var isContraInd: boolean);
begin
  {- user defined do not adhere to the regular nibble pairs of normal interps }
  if not isUD then
    if isUrine then     {- mask off proper interp nibble }
      bVal:= bVal and $0f
    else
      bVal:= (bVal shr 4) and $0f;

  if (bVal > 15) then
    StrCopy(intrp, '')
  else
    StrCopy(intrp, ncTher[bVal]);
  isContraInd:= (bVal = $F);
end;

procedure ByteToMIC(mic: PChar; bVal: byte);
{- convert byte value to MIC string }
var
  pstr  : array [0..RESmaxStrResult] of char;
  indx  : integer;
begin
  StrCopy(mic, '');
  if bVal = $FF then
    exit;
  if bVal and greaterThan <> 0 then
    StrCat(mic, '>')
  else if bVal and LessThan <> 0 then
    StrCat(mic, '<')
  else
    StrCat(mic, ' ');

  if bVal and nrBit <> 0 then   {- check N/R bit }
    indx:= 0
  else {- get MIC index into master dil array. Mask off greater/less than bits using $1F }
    indx:= bVal and $1F;

  StrPCopy(pstr, mda[indx]);
  TrimAll(pstr, pstr, ' ', RESmaxStrResult);
  StrLCat(mic, pstr, RESmaxStrResult);
end;


{ Convert a given biotype in internal format to a string. }
procedure UnpkBio(var biotype: Biot; bioStr: PChar);
var
  ch       : char;
  i        : integer;
  digNum   : integer;
  shiftBio : Biot;
  hypPos   : integer;
  tmpStr   : string;

begin
  tmpStr:=  '';
  digNum:= biotype.Format shr 8;
  ShiftBio:= biotype;  { Make a copy of the biotype }

  for i:= 1 TO digNum DO
  begin
    tmpStr:= CONCAT(CHR( (ShiftBio.Bits[0] and 7) + ORD('0') ), tmpStr);
    {shift next digit into low order position }
    BlkShift(addr(ShiftBio.Bits), sizeof(ShiftBio.Bits), -3);
  end;

  hypPos:= biotype.Format and $FF;
  repeat
    i:= hypPos shr 4;
    if i > 0 then
      insert( '-', tmpStr, i);  { insert hyphen }
    hypPos:= (hypPos shl 4) and $FF;
  until hypPos = 0;
  StrPCopy(bioStr, tmpStr);
end;

procedure GetV18Field(v30Str, v18Str: PChar; size: integer);
var
  pStr    : array[0..100] of char;
  charIdx : integer;
begin
  for charIdx:= 0 to size - 1 do
    pStr[charIdx]:= v18Str[charIdx];
  pStr[size]:= #0;
  TrimALL(v30Str, pStr, ' ', size);
end;

function ConvertV18Date(cDate : word): TIntlDate;
var
  dateFmt       : string;
  mCollDateStr  : string;
  mCurrCollDate : array[0..25] of char;
  tmpStr        : array[0..10] of char;
  m, d, y       : Word;
  code          : Word;
begin
  dateFmt:= '/102BEPTNMDQY';
  DateSet(dateFmt);
  DateUnpack(cDate, mCollDateStr, true);
  StrPCopy(mCurrCollDate, mCollDateStr);
  StrStrip(tmpStr, mCurrCollDate, '/', 5);
  Val(tmpStr, m, code);
  StrStrip(tmpStr, mCurrCollDate, '/', 5);
  Val(tmpStr, d, code);
  StrStrip(tmpStr, mCurrCollDate, '/', 5);
  Val(tmpStr, y, code);
  ConvertV18Date:= IntlPackDate(y, m, d);
end;

function ConvertV18Time(cTime : integer): TIntlTime;
begin
  if cTime = -1 then
    ConvertV18Time:= 0
  else
    ConvertV18Time:= IntlPackTime(cTime div 100, cTime mod 100, 0, 0);
end;

procedure ConvertV18FreeText(aPath, mFT: PChar; rec: TRecPtr);
var
  k         : integer;
  aFile     : array[0..100] of char;
  mFTFile   : file;
  mV18FTRec : TV18FreeTextRec;
  seekNum   : longInt;
  recNum    : longInt;
  pstr      : array[0..80] of char;
  p2        : array[0..80] of char;
  pc        : PChar;
begin
  recNum:= 0;
  move(rec, recNum, 3);
  StrCopy(mFt, '');
  if recNum <> 0 then
  begin
    StrCopy(aFile, aPath);
    StrCat(aFile, 'DM$FREE.DAT');
    Assign(mFTFile, aFile);
    Reset(mFTFile, 1);
    if IOResult <> 0 then
      CriticalError('Cannot open the V18.10 DM$FREE.DAT.');

    repeat
      seekNum:= (recNum-1) * longInt(96);
      Seek(mFTfile, seekNum);
      BlockRead(mFTFile, mV18FTRec, sizeof(mV18FTRec));

      recNum:= 0;
      Move(mV18FtRec.extra, recNum, 3);

      StrLCopy(pstr, mV18FtRec.freeText, mV18FtRec.freeTextLen);
      {- remove hokey backslashes in the source string }
      StripRange(p2, pstr, sizeof(p2)-1, #8, #8);
      {- now replace all #255's with a line feed }
      k:= 0;
      while k < strlen(p2) do
      begin
        if p2[k] = #255 then
        begin
          pc:= @p2[k+1];
          p2[k]:= #0;
          StrCat(mFt, p2);
          StrCopy(p2, pc);
          if StrLen(p2) > 0 then
            StrCat(mFt, #13#10);
          k:= 0;
        end
        else
          Inc(k);
      end;
      StrCat(mFt, p2);
    until recNum = 0;
    Close(mFTFile);
  end;
  mFt[120]:= #0; {- truncate to ensure that is does not exceed v3 free text len
                    of 120 bytes }
end;

function CreateIndexFiles(aDir: PChar): integer;
var
  retVal         : integer;
  d              : PDBReadWriteDesc;
  newdb          : PDBCreator;
  afield         : TFldInfo;
  akey           : TKeyInfo;
  aseg           : TSegmentInfo;

begin
  retVal:= continueJob;

  { Create patient index file }
  newdb:= New(PDBCreator, Init);
  if newdb = nil then
    CriticalError('Cannot create a V18.10 patient index file.');
  d:= New(PDBReadWriteDesc, Init('PAT18IDX', aDir));
  if d = nil then
    CriticalError('Cannot create a V18.10 patient index file description.');
  d^.SetFileDesc('Patient v18.10 Index File');
  d^.SetFileFlag(0);
  d^.SetAutoSeq(FALSE);
  d^.SetDataType(1);
  { Add field 1 patient file }
  StrCopy(afield.fldName, 'Patient ID');
  afield.userFlag:= 4;
  afield.fldType:= dbZCode;
  afield.fldSize:= 13;
  d^.AddField(afield);
  { Add field 2 patient file }
  StrCopy(afield.fldName, 'Rec Num');
  afield.userFlag:= 0;
  afield.fldType:= dbWord;
  afield.fldSize:= 0;
  d^.AddField(afield);
  { Add Key to patient file }
  StrCopy(akey.keyName, 'ID');
  akey.unique:= TRUE;
  akey.keyFlag:= 0;
  d^.AddKey(akey);
  { Add Key Seg to patient file }
  aseg.fldNum:= 1;
  aseg.descend:= FALSE;
  d^.AddKeySeg(1, aseg);
  newdb^.CreateTable(d);
  if newdb^.dbErrorNum <> 0 then
  begin
    LogHex(newdb^.dbErrorNum);
    LogStr(' - Cannot create patient index table. ');
    LogLn;
    CriticalError('Cannot create patient index table.');
  end;
  MSDisposeObj(newdb);
  MSDisposeObj(d);

  { Create specimen index file }
  newdb:= New(PDBCreator, Init);
  if newdb = nil then
    CriticalError('Cannot create a V18.10 specimen index file.');
  d:= New(PDBReadWriteDesc, Init('SPC18IDX', aDir));
  if d = nil then
    CriticalError('Cannot create a V18.10 specimen index file description.');
  d^.SetFileDesc('Specimen v18.10 Index File');
  d^.SetFileFlag(0);
  d^.SetAutoSeq(FALSE);
  d^.SetDataType(1);
  { Add field 1 specimen file }
  StrCopy(afield.fldName, 'Specimen ID');
  afield.userFlag:= 4;
  afield.fldType:= dbZCode;
  afield.fldSize:= 9;
  d^.AddField(afield);
  { Add field 2 specimen file }
  StrCopy(afield.fldName, 'Rec Num');
  afield.userFlag:= 0;
  afield.fldType:= dbWord;
  afield.fldSize:= 0;
  d^.AddField(afield);
  { Add Key to specimen file }
  StrCopy(akey.keyName, 'SpecID');
  akey.unique:= TRUE;
  akey.keyFlag:= 0;
  d^.AddKey(akey);
  { Add Key Seg to specimen file }
  aseg.fldNum:= 1;
  aseg.descend:= FALSE;
  d^.AddKeySeg(1, aseg);
  newdb^.CreateTable(d);
  if newdb^.dbErrorNum <> 0 then
  begin
    LogHex(newdb^.dbErrorNum);
    LogStr(' - Cannot create specimen index table. ');
    LogLn;
    CriticalError('Cannot create specimen index table.');
  end;
  MSDisposeObj(newdb);
  MSDisposeObj(d);

  { Create result index file }
  newdb:= New(PDBCreator, Init);
  if newdb = nil then
    CriticalError('Cannot create a V18.10 results index file.');
  d:= New(PDBReadWriteDesc, Init('RES18IDX', aDir));
  if d = nil then
    CriticalError('Cannot create a V18.10 results index file description.');
  d^.SetFileDesc('Result v18.10 Index File');
  d^.SetFileFlag(0);
  d^.SetAutoSeq(FALSE);
  d^.SetDataType(1);
  { Add field 1 result file }
  StrCopy(afield.fldName, 'Specimen ID');
  afield.userFlag:= 4;
  afield.fldType:= dbZCode;
  afield.fldSize:= 9;
  d^.AddField(afield);
  { Add field 2 result file }
  StrCopy(afield.fldName, 'Rec Num');
  afield.userFlag:= 0;
  afield.fldType:= dbWord;
  afield.fldSize:= 0;
  d^.AddField(afield);
  { Add Key to result file }
  StrCopy(akey.keyName, 'SpecID');
  akey.unique:= FALSE;
  akey.keyFlag:= 0;
  d^.AddKey(akey);
  { Add Key Seg to result file }
  aseg.fldNum:= 1;
  aseg.descend:= FALSE;
  d^.AddKeySeg(1, aseg);
  newdb^.CreateTable(d);
  if newdb^.dbErrorNum <> 0 then
  begin
    LogHex(newdb^.dbErrorNum);
    LogStr(' - Cannot create results index table. ');
    LogLn;
    CriticalError('Cannot create results index table.');
  end;
  MSDisposeObj(newdb);
  MSDisposeObj(d);

  { Create Mnemonic index file }
  newdb:= New(PDBCreator, Init);
  if newdb = nil then
    CriticalError('Cannot create a V18.10 mnemonic index file.');
  d:= New(PDBReadWriteDesc, Init('MNM18IDX', aDir));
  if d = nil then
    CriticalError('Cannot create a V18.10 mnemonic index file description.');
  d^.SetFileDesc('Mnemonic v18.10 Index File');
  d^.SetFileFlag(0);
  d^.SetAutoSeq(FALSE);
  d^.SetDataType(1);
  { Add field 1 Mnemonic file }
  StrCopy(afield.fldName, 'Mnem Code');
  afield.userFlag:= 4;
  afield.fldType:= dbInteger;
  afield.fldSize:= 0;
  d^.AddField(afield);
  { Add field 2 Mnemonic file }
  StrCopy(afield.fldName, 'Mnem ID');
  afield.userFlag:= 0;
  afield.fldType:= dbZString;
  afield.fldSize:= 4;
  d^.AddField(afield);
  { Add field 3 Mnemonic file }
  StrCopy(afield.fldName, 'Rec Num');
  afield.userFlag:= 0;
  afield.fldType:= dbWord;
  afield.fldSize:= 0;
  d^.AddField(afield);
  { Add Key to Mnemonic file }
  StrCopy(akey.keyName, 'MnemID');
  akey.unique:= TRUE;
  akey.keyFlag:= 0;
  d^.AddKey(akey);
  { Add Key Seg to Mnemonic file }
  aseg.fldNum:= 1;
  aseg.descend:= FALSE;
  d^.AddKeySeg(1, aseg);
  { Add Key Seg to Mnemonic file }
  aseg.fldNum:= 2;
  aseg.descend:= FALSE;
  d^.AddKeySeg(1, aseg);
  newdb^.CreateTable(d);
  if newdb^.dbErrorNum <> 0 then
  begin
    LogHex(newdb^.dbErrorNum);
    LogStr(' - Cannot create Mnemonic index table. ');
    LogLn;
    CriticalError('Cannot create Mnemonic index table.');
  end;
  MSDisposeObj(newdb);
  MSDisposeObj(d);

  CreateIndexFiles:= retVal;
end;

function LoadIndexFiles(aDir: PChar): integer;
var
  retVal         : integer;
  mDataFile      : file;
  mCustomFile    : file;
  mV18Rec        : TV18DataRec;
  mCustomRec     : TV18CustomRec;
  mPatIdxFile    : PDBFile;
  mSpecIdxFile   : PDBFile;
  mResIdxFile    : PDBFile;
  mMnemIdxFile   : PDBFile;
  recordNumber   : word;
  totalRecs      : word;
  path           : array[0..100] of char;

  { Store record in PATIENT BTrieve index file }
  procedure StorePatIdx(aRecNum: word);
  var
    tPatID  : array[0..12] of char;
  begin
    GetV18Field(tPatID, mV18Rec.pat_id, 12);
    mPatIdxFile^.dbr^.PutFieldAsStr(1, tPatID);
    mPatIdxFile^.dbr^.PutField(2, @aRecNum);
    if not mPatIdxFile^.dbc^.GetEQ(mPatIdxFile^.dbr) then {only insert id pat does not exist}
      mPatIdxFile^.dbc^.InsertRec(mPatIdxFile^.dbr);
  end;

  { Store record in SPECIMEN BTrieve index file }
  procedure StoreSpecIdx(aRecNum: word);
  var
    tSpecID  : array[0..8] of char;
  begin
    GetV18Field(tSpecID, mV18Rec.spec_id, 8);
    mSpecIdxFile^.dbr^.PutField(1, @tSpecID);
    mSpecIdxFile^.dbr^.PutField(2, @aRecNum);
    mSpecIdxFile^.dbc^.InsertRec(mSpecIdxFile^.dbr);
  end;

  { Store record in RESULT BTrieve index file }
  procedure StoreResIdx(aRecNum: word);
  var
    tSpecID   : array[0..8] of char;
  begin
    if mV18Rec.res_rVarTag = 0 then {result record}
    begin
      GetV18Field(tSpecID, mV18Rec.res_specID, 8);
      mResIdxFile^.dbr^.PutField(1, @tSpecID);
      mResIdxFile^.dbr^.PutField(2, @aRecNum);
      mResIdxFile^.dbc^.InsertRec(mResIdxFile^.dbr);
    end;
  end;

  procedure StoreMnemIdx(aRecNum: word);
  var
    tMnemCode  : integer;
    tMnemID    : array[0..3] of char;
  begin
    tMnemCode:= mCustomRec.mVarNum; {mnemonic type i.e ward, source ect.}
    if tMnemCode < 10 then
    begin
      GetV18Field(tMnemID, mCustomRec.mnemonic, 3);
      mMnemIdxFile^.dbr^.PutField(1, @tMnemCode);
      mMnemIdxFile^.dbr^.PutFieldAsStr(2, tMnemID);
      mMnemIdxFile^.dbr^.PutField(3, @aRecNum);
      mMnemIdxFile^.dbc^.InsertRec(mMnemIdxFile^.dbr);
    end;
  end;


begin  { LoadIndexFiles }
  retVal:= continueJob;

  { DM$DATA.DAT }
  StrCopy(path, aDir);
  StrCat(path, 'DM$DATA.DAT');
  Assign(mDataFile, path);
  Reset(mDataFile, 1);
  if IOResult <> 0 then
    CriticalError('Cannot open the V18.10 DM$DATA.DAT.');
  {do two reads to bypass header info}
  if not EOF(mDataFile) then
    BlockRead(mDataFile, mV18Rec, sizeof(mV18Rec));
  if not EOF(mDataFile) then
    BlockRead(mDataFile, mV18Rec, sizeof(mV18Rec));

  mPatIdxFile:=  New(PDBFile, Init('PAT18IDX', aDir, dbOpenNormal));
  if mPatIdxFile = nil then
    CriticalError('Cannot open the patient index file.');
  mSpecIdxFile:= New(PDBFile, Init('SPC18IDX', aDir, dbOpenNormal));
  if mSpecIdxFile = nil then
    CriticalError('Cannot open the specimen index file.');
  mResIdxFile:=  New(PDBFile, Init('RES18IDX', aDir, dbOpenNormal));
  if mResIdxFile = nil then
    CriticalError('Cannot open the results index file.');

  {write index files}
  recordNumber:= 3;
  totalRecs:= FileSize(mDataFile) div SizeOf(mV18Rec);

  {loop through all data records and check the variant record tag}
  while (not EOF(mDataFile)) and (recordNumber <= totalRecs) do
  begin
    {$I-}BlockRead(mDataFile, mV18Rec, SizeOf(mV18Rec));{$I+}

    {varNums of FF are deleted records and are ignored}
    case mV18Rec.varNum of
      0: StorePatIdx(recordNumber);
      1: StoreSpecIdx(recordNumber);
      2: StoreResIdx(recordNumber);
    end;
    Inc(recordNumber);
  end;
  Close(mDataFile);
  MSDisposeObj(mPatIdxFile);
  MSDisposeObj(mSpecIdxFile);
  MSDisposeObj(mResIdxFile);

  { CUSTOM.FIL }
  StrCopy(path, aDir);
  StrCat(path, 'CUSTOM.FIL');
  Assign(mCustomFile, path);
  Reset(mCustomFile, 1);
  if IOResult <> 0 then
    CriticalError('Cannot open the V18.10 CUSTOM.FIL.');
  {do four reads to bypass header info}
  if not EOF(mCustomFile) then
    BlockRead(mCustomFile, mCustomRec, sizeof(mCustomRec));
  if not EOF(mCustomFile) then
    BlockRead(mCustomFile, mCustomRec, sizeof(mCustomRec));
  if not EOF(mCustomFile) then
    BlockRead(mCustomFile, mCustomRec, sizeof(mCustomRec));
  if not EOF(mCustomFile) then
    BlockRead(mCustomFile, mCustomRec, sizeof(mCustomRec));
  mMnemIdxFile:= New(PDBFile, Init('MNM18IDX', adir, dbOpenNormal));
  if mMnemIdxFile = nil then
    CriticalError('Cannot open the mnemonic index table.');
  {store indexes for mnemonic and types}
  recordNumber:= 5;
  totalRecs:= FileSize(mCustomFile) div SizeOf(mCustomRec);
  while (not EOF(mCustomFile)) and (recordNumber <= totalRecs) do
  begin
    {$I-}BlockRead(mCustomFile, mCustomRec, SizeOf(mCustomRec));{$I+}
    StoreMnemIdx(recordNumber);
    Inc(RecordNumber);
  end;
  Close(mCustomFile);
  MSDisposeObj(mMnemIdxFile);

  LoadIndexFiles:= retVal;
end;

function  GetConversionType(panelNum: byte; addDrgRecNum: longInt): integer;
begin
  if (panelNum <> NullPanel) and (addDrgRecNum = 0) then
    GetConversionType:= PanelOnly
  else if (panelNum <> NullPanel) and (addDrgRecNum <> 0) then
    GetConversionType:= PanelANDDrugs
  else if (panelNum = NullPanel) and (addDrgRecNum <> 0) then
    GetConversionType:= AddDrugsOnly
  else
    GetConversionType:= NoResults;
end;

function GetOrganism(rPath, mPath: PChar; org: integer): TSeqNum;
var
  aOrgFile     : array[0..100] of char;
  aOrgLNFile   : array[0..100] of char;
  orgV18File   : file;
  orgLNV18File : file;
  orgRec       : nameDesc;
  orgFile      : PDBFile;
  seekNum      : longInt;
  recNum       : longInt;
  orgFound     : boolean;
  longName     : String[47];
  newOrgID     : array[0..5] of char;
  newOrgShort  : array[0..32] of char;
  newOrgLong   : array[0..64] of char;
  newOrgUD     : boolean;
  group        : byte;
  family       : byte;
begin
  if org <> 0 then
  begin
    orgFile:= New(PDBFile, Init('ORGANISM', mPath, dbOpenNormal));
    if orgFile = nil then
      CriticalError('Cannot open the organism file.');
    Str(org, newOrgId);
    orgFile^.dbr^.PutFieldAsStr(1, newOrgId);
    if orgFile^.dbc^.GetEQ(orgFile^.dbr) then
      GetOrganism:= orgFile^.dbr^.GetSeqValue
    else
    begin
      (* add v18 organism to v3.0 *)
      StrCopy(aOrgFile, rPath);
      StrCat(aOrgFile, 'ORG.NAM');
      Assign(orgV18File, aOrgFile);
      Reset(orgV18File, 1);
      if IOResult <> 0 then
        CriticalError('Cannot open the V18.10 ORG.NAM.');
      {loop through all organism records}
      recNum:= 0;
      orgFound:= false;
      while ((not EOF(orgV18File)) and (not orgFound))  do
      begin
        {$I-}BlockRead(orgV18File, orgRec, SizeOf(orgRec));{$I+}
        if orgRec.orgNum = org then
        begin
          orgFound:= true;
          (* Get the Organism long name *)
          StrCopy(aOrgLNFile, rPath);
          StrCat(aOrgLNFile, 'LONGNAME.TBL');
          Assign(orgLNV18File, aOrgLNFile);
          Reset(orgLNV18File, 1);
          if IOResult <> 0 then
            CriticalError('Cannot open the V18.10 ORG.NAM.');
          seekNum:= recNum * longInt(sizeof(longName));
          seek(orgLNV18File, seekNum);
          BlockRead(orgLNV18File, longName, sizeof(longName));
          Close(orgLNV18File);
          (* Add User define V18 organism to V3.0 *)
          GetV18Field( newOrgShort, orgRec.orgName, SizeOf(orgRec.orgName));
          StrPCopy(newOrgLong, longName);
          newOrgUD:= true;
          orgFile^.dbr^.ClearRecord;
          orgFile^.dbr^.PutFieldAsStr(DBORGOrg, newOrgId);
          orgFile^.dbr^.PutFieldAsStr(DBORGSName, newOrgShort);
          orgFile^.dbr^.PutFieldAsStr(DBORGLName, newOrgLong);
          orgFile^.dbr^.PutField(DBORGUDFlag, @newOrgUD);

          case org of
            600..604: begin group:= 1; family:= 1; end;
            605..609: begin group:= 1; family:= 2; end;
            610..614: begin group:= 1; family:= 2; end;
            615..619: begin group:= 2; family:= 1; end;
            620..624: begin group:= 2; family:= 2; end;
            625..629: begin group:= 2; family:= 2; end;
            630..634: begin group:= 3; family:= 1; end;
            635..639: begin group:= 3; family:= 2; end;
            640..644: begin group:= 3; family:= 3; end;
            645..649: begin group:= 3; family:= 4; end;
            650..654: begin group:= 4; family:= 1; end;
            655..659: begin group:= 6; family:= 1; end;
            else
            begin
              group:= 1;
              family:= 9;  { other }
            end;
          end;
          orgFile^.dbr^.PutField(DBORGSet, @group);
          orgFile^.dbr^.PutField(DBORGFamily, @family);

          if orgFile^.dbc^.InsertRec(orgFile^.dbr) then
            GetOrganism:= orgFile^.dbr^.GetSeqValue
          else
          begin
            GetOrganism:= 0;
            (*- log error to disk *)
            LogHex(orgFile^.dbc^.dbErrorNum);
            LogStr(' - Cannot create Organism record -- Organism ID = ');
            LogStr(newOrgId);
            LogLn;
          end;
        end;
        inc(recNum);
      end;
      Close(orgV18File);
    end;
    MSDisposeObj(orgFile);
  end
  else
    GetOrganism:= 0;
end;

function Convert_Isolate( isoFile: PDBFile;
                          rPath: PChar;
                          mPath: PChar;
                          isoID: PChar;
                          iSpecSeqRef: TSeqNum;
                          iSet: byte;
                          iOrg: integer;
                          iTestDate: TIntlDate;
                          iOrder: TSeqNum;
                          iFTRec: TRecPtr;
                          iTnsFlg: boolean;
                          iSpecID: PChar;
                          iSpecCDate: TIntlDate;
                          aList: PListBox): TSeqNum;
var
  iFreeText : array[0..180] of char;
  qcFlag    : boolean;
  iOrgSeqRef: TSeqNum;
  iSetFamily: word;
begin
  Convert_Isolate:= 0;

  iSetFamily:= MakeSetFamily(iSet, 1);
  qcFlag:= false;
  ConvertV18FreeText(rPath, iFreeText, iFTRec);
  iOrgSeqRef:= GetOrganism(rPath, mPath, iOrg);

  isoFile^.dbr^.ClearRecord;
  isoFile^.dbr^.PutFieldAsStr(1, isoID);
  isoFile^.dbr^.PutField(2, @iSpecSeqRef);
  isoFile^.dbr^.PutField(3, @iSetFamily);
  isoFile^.dbr^.PutField(4, @iOrgSeqRef);
  isoFile^.dbr^.PutField(5, @iTestDate);
  isoFile^.dbr^.PutField(6, @iOrder);
  isoFile^.dbr^.PutFieldAsStr(8, iFreeText);
  isoFile^.dbr^.PutField( 9, @iTnsFlg);
  isoFile^.dbr^.PutField(10, @qcFlag);
  isoFile^.dbr^.PutFieldAsStr(11, iSpecID);
  isoFile^.dbr^.PutField(12, @iSpecCDate);

  ShowCurrentIso(isoFile, aList);

  if not isoFile^.dbc^.InsertRec(isoFile^.dbr) then
  begin
    (*- log error to disk *)
    LogHex(isoFile^.dbc^.dbErrorNum);
    LogStr(' - Cannot create Isolate record -- Specimen ID = ');
    LogStr(iSpecID);
    LogStr('  Isolate Id = ');
    LogStr(isoID);
    LogLn;
  end
  else
    Convert_Isolate:= isoFile^.dbr^.GetSeqValue;

  if iOrder > 0 then
    CreateIsoOrders(isoFile);
end;

procedure GetDrugNames(aPath: PChar; v18Rec: TV18DataRec; drugs: PDrugList);
var
  indx          : integer;
  indx1         : integer;
  drgCnt        : integer;
  drugId        : array[0..30] of char;
  offId         : array[0..30] of char;
  offName       : array[0..30] of char;
  dFile         : file;
  aFile         : array[0..100] of char;
  drugFound     : boolean;
  drugRec       : OffDrug;
  duplicateDrug : boolean;
begin
  { set array entries to nil }
  for indx:= 1 to 5 do
  begin
    StrCopy(drugs^[indx].drugAbbr, '');
    StrCopy(drugs^[indx].drugName, '');
  end;

  StrCopy(aFile, aPath);
  StrCat(aFile, 'OFFLINE.FIL');
  Assign(dFile, aFile);
  Reset(dFile, 1);
  if IOResult <> 0 then
    CriticalError('Cannot open the V18.10 OFFLINE.FIL.');
  drgCnt:= 0;
  for indx:= 0 to v18Rec.drg_cnt-1 do
  begin
    GetV18Field(drugId, v18Rec.drg_drgType[indx].drug, 3);
    duplicateDrug:= false;
    indx1:= 1;
    while ((indx1 <= drgCnt) and (not duplicateDrug))  do
    begin
      if (StrComp(drugId, drugs^[indx1].drugAbbr) = 0) then
        duplicateDrug:= true;
      inc(indx1);
    end;
    if not duplicateDrug then
    begin
      seek(dFile, 0);
      drugFound:= false;
      while ((not EOF(dFile)) and (not drugFound))  do
      begin
        {$I-}BlockRead(dFile, drugRec, SizeOf(drugRec));{$I+}
        GetV18Field(offId, drugRec.abbr, 3);
        if (StrComp(drugId, offId) = 0) then
        begin
          drugFound:= true;
          GetV18Field(offName, drugRec.drugName, 16);
        end;
      end;
      inc(drgCnt);
      StrCopy(drugs^[drgCnt].drugAbbr, drugId);
      StrCopy(drugs^[drgCnt].drugName, offName);
    end;
  end;
  Close(dFile);
end;

procedure LoadPanelInfo;
var
  fileStr : array[0..100] of char;
  aFile   : Text;
  inStr   : array[0..200] of char;
  pId     : integer;
  code    : integer;
  tmpStr  : array[0..25] of char;
  chr     : PChar;
begin
  StrCopy(fileStr, curDMSDir);
  StrCat(fileStr, 'V18PANEL.TXT');
  Assign(aFile, fileStr);
  Reset(aFile);
  if IOResult <> 0 then
    CriticalError('Cannot open the V18.10 V18PANEL.TXT file.');

  while not eof(aFile) do
  begin
    readLn(aFile, inStr);
    chr:= strstrip(tmpStr, inStr, ',', 20);
    Val(tmpStr, pId, code);
    chr:= strstrip(panels[pID].tstgrp, inStr, ',', 20);
    chr:= strstrip(panels[pID].panelDesc, inStr, ',', 20);
    panels[pID].tstgrpRef:= 0;
    panels[pID].tstgrpSet:= 0;
    panels[pID].orderRef:= 0;
    StrCopy(panels[pID].orderId,'');
    StrCopy(panels[pID].orderDesc,'');
    StrCopy(panels[pID].isolateID,'');
    panels[pID].numTests:= 0;
    while (chr <> nil) do
    begin
      inc(panels[pID].numTests);
      chr:= strstrip(panels[pID].testID[panels[pID].numTests], inStr, ',', 20);
    end;
  end;
  Close(aFile);
end;

procedure LoadTstGrpInfo(mPath: PChar);
var
  cnt         : integer;
  testgrpFile : PDBFile;
  tstgrpSeq   : TSeqNum;
  tstgrpSet   : byte;
begin
  testgrpFile:= New(PDBFile, Init('TESTGRP', mPath, dbOpenNormal));
  if testgrpFile = nil then
    CriticalError('Cannot open the testgrp index file.');
  for cnt:= 1 to 20 do
  begin
    testgrpFile^.dbr^.PutFieldAsStr(1, panels[cnt].tstgrp);
    if testgrpFile^.dbc^.GetEQ(testgrpFile^.dbr) then
    begin
      tstgrpSeq:= testgrpFile^.dbr^.GetSeqValue;
      testgrpFile^.dbr^.GetField(3, @tstgrpSet, 1);
      panels[cnt].tstgrpRef:= tstgrpSeq;
      panels[cnt].tstgrpSet:= tstgrpSet;
    end;
  end;
  MSDisposeObj(testgrpFile);
end;



END.
