{----------------------------------------------------------------------------}
{  Module Name  : PURGE.PAS                                                  }
{  Programmer   : EJ                                                         }
{  Date Created : 04/17/95                                                   }
{                                                                            }
{  Purpose -                                                                 }
{  This module allows the user to purge data from the Nihongo databases.     }
{                                                                            }
{  Assumptions -                                                             }
{  None.                                                                     }
{                                                                            }
{  Initialization -                                                          }
{  None.                                                                     }
{                                                                            }
{  Revision History -                                                        }
{                                                                            }
{  Version  Date      Who    Did What                                        }
{  -------  --------  ------ ----------------------------------------------- }
{  1.00     04/17/95  EJ     Initial release.                                }
{                                                                            }
{----------------------------------------------------------------------------}

unit Purge;

INTERFACE

uses
  OWindows;

procedure PurgeData(aParent: PWindowsObject);

IMPLEMENTATION

uses
  WinTypes,
  DlgLib,
  CtlLib,
  APITools,
  MScan,
  DMSErr,
  DBIDs,
  DBTypes,
  DBErrors,
  DBFile,
  DBLib,
  MPSS,
  Status,
  DMSDebug;


{$R PURGE.RES}
{$I PURGE.INC}
{$R STATUS.RES}
{$I STATUS.INC}


type
  PPurgeDlg = ^TPurgeDlg;
  TPurgeDlg = object(TCenterDlg)
    constructor Init(aParent: PWindowsObject);
    destructor Done; virtual;
    procedure SetupWindow; virtual;
    function  CanClose: boolean; virtual;
    {}
    private
    dataGrp     : PRadioGroup;
    methodGrp   : PRadioGroup;
  end;

var
  purgeType  : integer;
  bUseMPSS   : boolean;
  purgeDlg   : PPurgeDlg;


{--------------------------------[ TPurgeDlg ]-------------------------------}
{----------------------------------------------------------------------------}
{  Initialize a PurgeDlg object.                                             }
{----------------------------------------------------------------------------}
constructor TPurgeDlg.Init(aParent: PWindowsObject);
begin
  inherited Init(aParent, MakeIntResource(DLG_PURGE));
  dataGrp := New(PRadioGroup, Init(@Self, IDC_PATIENTS, IDC_PATIENTS, FALSE));
  dataGrp^.AddToGroup(IDC_PATIENTS_NO_SPECS, IDC_PATIENTS_NO_SPECS);
  dataGrp^.AddToGroup(IDC_SPECIMENS, IDC_SPECIMENS);
  dataGrp^.AddToGroup(IDC_SPECIMENS_LEAVE_PATS, IDC_SPECIMENS_LEAVE_PATS);
  dataGrp^.AddToGroup(IDC_QC_ISOLATES, IDC_QC_ISOLATES);
  methodGrp := New(PRadioGroup, Init(@Self, IDC_USE_MPSS, longint(TRUE), FALSE));
  methodGrp^.AddToGroup(IDC_DELETE_ALL, longint(FALSE));
end;

{----------------------------------------------------------------------------}
{  Dispose a PurgeDlg object.                                                }
{----------------------------------------------------------------------------}
destructor TPurgeDlg.Done;
begin
  MSDisposeObj(methodGrp);
  MSDisposeObj(dataGrp);
  inherited Done;
end;

{----------------------------------------------------------------------------}
{  Setup a PurgeDlg object window.                                           }
{----------------------------------------------------------------------------}
procedure TPurgeDlg.SetupWindow;
begin
  inherited SetupWindow;
  dataGrp^.SetSelected(IDC_PATIENTS);
  methodGrp^.SetSelected(longint(TRUE));
end;

{----------------------------------------------------------------------------}
{  Get values from PurgeDlg object before allowing the dialog to close.      }
{----------------------------------------------------------------------------}
function  TPurgeDlg.CanClose: boolean;
var
  i  : integer;
begin
  purgeType := dataGrp^.SelectedVal;
  i := methodGrp^.SelectedVal;
  bUseMPSS := boolean(methodGrp^.SelectedVal);
  CanClose := inherited CanClose and (purgeType <> -1) and (i <> -1);
end;


{----------------------------------------------------------------------------}
{  Prompt the user to select data type to purge and whether to use MPSS      }
{  to select the data to purge.  The following options are available:        }
{                                                                            }
{    Patients                                                                }
{  This option will DELETE the patients along with their specimens,          }
{  isolates, isolate orders, and results                                     }
{                                                                            }
{    Patients w/o Specimens                                                  }
{  This option will DELETE the patients, with no specimens attached.         }
{  However, patients with specimens attached will not be deleted.            }
{                                                                            }
{    Specimens                                                               }
{  This option will DELETE the specimens along with their isolates,          }
{  isolate orders, results, and patients with no other specimens             }
{                                                                            }
{    Specimens (retain Patients)                                             }
{  This option will DELETE the specimens along with their isolates,          }
{  isolate orders, and results                                               }
{                                                                            }
{    QC Isolates                                                             }
{  This option will DELETE the QC isolates along with their isolate          }
{  orders and results                                                        }
{----------------------------------------------------------------------------}
procedure PurgeData(aParent: PWindowsObject);
var
  pStr    : array [0..200] of char;
  title   : array [0..100] of char;
  prgFile : PDBFile;
  altFile : PDBAssocFile;
  mpssObj : PMPSS;
  statDlg : PPctStatDlg;
  flg     : boolean;
  j, k    : longint;
begin
  SR(IDS_PURGE, title, 100);
  purgeDlg := New(PPurgeDlg, Init(aParent));
  prgFile := nil;
  statDlg := nil;
  if Application^.ExecDialog(purgeDlg) = IDOK then
  repeat
    statDlg := New(PPctStatDlg, Init(aParent, MakeIntResource(DLG_STATUS),
                                     TRUE, IDC_PERCENTBAR));
    Application^.MakeWindow(statDlg);
    statDlg^.SetText(title, 0);
    statDlg^.SetText('', IDC_TEXT1);
    statDlg^.SetText(SR(IDS_PERCENT_COMPLETE, pStr, 50), IDC_TEXT2);
    statDlg^.SetText('', IDC_TEXT3);
    statDlg^.ResetPct;
    case purgeType of
      IDC_PATIENTS:
      begin
        statDlg^.SetText(SR(IDS_PURGE_PATIENTS, pStr, 50), IDC_TEXT1);
        prgFile := New(PDBFile, Init(DBPATFile, '', dbOpenNormal));
        if prgFile = nil then
        begin
          ShowError(aParent, IDS_ERR_DATABASE, nil,
                    dbLastOpenError, MOD_PURGE, 1);
          break;
        end;
        if bUseMPSS then
        begin
          mpssObj:= New(PMPSS, Init(aParent, prgFile, IncludeUndefDates));
          if (mpssObj <> nil) and
             mpssObj^.MPSSLoadRules(aParent, SR(IDS_PURGE_PATIENTS, pStr, 200)) and
             NoYesMsg(aParent^.HWindow, title,
                      SR(IDS_PAT_PROMPT3, pStr, 200)) then
          begin
            if mpssObj^.MPSSGetFirst(prgFile) then
            begin
              repeat
                statDlg^.IncrPctLevel(mpssObj^.MPSSGetPercent);
                statDlg^.Update;
                altFile := New(PDBAssocFile, Init(prgFile^.dbr,
                               DBPATIENT_SPECIMEN_ASSOC, dbOpenNormal));
                if altFile = nil then
                begin
                  ShowError(aParent, IDS_ERR_DATABASE, nil,
                            dbLastOpenError, MOD_PURGE, 2);
                  Continue;
                end;
                if altFile^.dbc^.GetFirst(altFile^.dbr) then
                begin
                  repeat
                    if not altFile^.dbc^.DeleteRec(altFile^.dbr) and
                       (altFile^.dbc^.dbErrorNum <> dbErr_CantDeleteRecord) then
                    begin
                      ShowError(aParent, IDS_ERR_DELETE_SPEC, nil,
                                altFile^.dbc^.dbErrorNum, MOD_PURGE, 1);
                    end;
                  until not altFile^.dbc^.GetNext(altFile^.dbr);
                end;
                MSDisposeObj(altFile);

                if not prgFile^.dbc^.DeleteRec(prgFile^.dbr) and
                   (prgFile^.dbc^.dbErrorNum <> dbErr_CantDeleteRecord) then
                begin
                  ShowError(aParent, IDS_ERR_DELETE_PAT, nil,
                            prgFile^.dbc^.dbErrorNum, MOD_PURGE, 2);
                end;
              until not statDlg^.CanContinue or not mpssObj^.MPSSGetNext(prgFile);
            end;
            statDlg^.IncrPctLevel(mpssObj^.MPSSGetPercent);
            statDlg^.SetText(SR(IDS_COMPLETE, pStr, 50), IDC_TEXT3);
            statDlg^.Focus;

            while statDlg^.CanContinue do ;  {- wait for user to cancel }

            MSDisposeObj(mpssObj);
          end;
        end
        else
        begin
          if not NoYesMsg(aParent^.HWindow, title,
                          SR(IDS_PAT_PROMPT1, pStr, 200)) then
            break;

          j := 0;
          k := prgFile^.dbc^.NumRecords;
          if prgFile^.dbc^.GetFirst(prgFile^.dbr) then
          begin
            repeat
              Inc(j);
              statDlg^.CompPctLevel(j, k);
              statDlg^.Update;
              altFile := New(PDBAssocFile, Init(prgFile^.dbr,
                             DBPATIENT_SPECIMEN_ASSOC, dbOpenNormal));
              if altFile = nil then
              begin
                ShowError(aParent, IDS_ERR_DATABASE, nil,
                          dbLastOpenError, MOD_PURGE, 3);
                Continue;
              end;
              if altFile^.dbc^.GetFirst(altFile^.dbr) then
              begin
                repeat
                  if not altFile^.dbc^.DeleteRec(altFile^.dbr) and
                     (altFile^.dbc^.dbErrorNum <> dbErr_CantDeleteRecord) then
                  begin
                    ShowError(aParent, IDS_ERR_DELETE_SPEC, nil,
                              altFile^.dbc^.dbErrorNum, MOD_PURGE, 3);
                  end;
                until not altFile^.dbc^.GetNext(altFile^.dbr);
              end;
              MSDisposeObj(altFile);

              if not prgFile^.dbc^.DeleteRec(prgFile^.dbr) and
                 (prgFile^.dbc^.dbErrorNum <> dbErr_CantDeleteRecord) then
              begin
                ShowError(aParent, IDS_ERR_DELETE_PAT, nil,
                          prgFile^.dbc^.dbErrorNum, MOD_PURGE, 4);
              end;
            until not statDlg^.CanContinue or not prgFile^.dbc^.GetNext(prgFile^.dbr);
          end;
          statDlg^.CompPctLevel(j, k);
          statDlg^.SetText(SR(IDS_COMPLETE, pStr, 50), IDC_TEXT3);
          statDlg^.Focus;
          while statDlg^.CanContinue do ; {- wait for user to cancel }
        end;
      end;
      IDC_PATIENTS_NO_SPECS:
      begin
        statDlg^.SetText(SR(IDS_PURGE_PATIENTS, pStr, 50), IDC_TEXT1);
        prgFile := New(PDBFile, Init(DBPATFile, '', dbOpenNormal));
        if prgFile = nil then
        begin
          ShowError(aParent, IDS_ERR_DATABASE, nil,
                    dbLastOpenError, MOD_PURGE, 4);
          break;
        end;
        if bUseMPSS then
        begin
          mpssObj := New(PMPSS, Init(aParent, prgFile, IncludeUndefDates));
          if (mpssObj <> nil) and
             mpssObj^.MPSSLoadRules(aParent, SR(IDS_PURGE_PATIENTS, pStr, 200)) and
             NoYesMsg(aParent^.HWindow, title,
                      SR(IDS_PAT_PROMPT4, pStr, 200)) then
          begin
            if mpssObj^.MPSSGetFirst(prgFile) then
            begin
              repeat
                statDlg^.IncrPctLevel(mpssObj^.MPSSGetPercent);
                statDlg^.Update;
                if not prgFile^.dbc^.DeleteRec(prgFile^.dbr) and
                   (prgFile^.dbc^.dbErrorNum <> dbErr_CantDeleteRecord) then
                begin
                  ShowError(aParent, IDS_ERR_DELETE_PAT, nil,
                            prgFile^.dbc^.dbErrorNum, MOD_PURGE, 5);
                end;
              until not statDlg^.CanContinue or not mpssObj^.MPSSGetNext(prgFile);
            end;
            statDlg^.IncrPctLevel(mpssObj^.MPSSGetPercent);
            statDlg^.SetText(SR(IDS_COMPLETE, pStr, 50), IDC_TEXT3);
            statDlg^.Focus;

            while statDlg^.CanContinue do ; {- wait for user to cancel }

            MSDisposeObj(mpssObj);
          end;
        end
        else
        begin
          if not NoYesMsg(aParent^.HWindow, title,
                          SR(IDS_PAT_PROMPT2, pStr, 200)) then
            break;

          j := 0;
          k := prgFile^.dbc^.NumRecords;
          if prgFile^.dbc^.GetFirst(prgFile^.dbr) then
          begin
            repeat
              Inc(j);
              statDlg^.CompPctLevel(j, k);
              statDlg^.Update;
              if not prgFile^.dbc^.DeleteRec(prgFile^.dbr) and
                 (prgFile^.dbc^.dbErrorNum <> dbErr_CantDeleteRecord) then
              begin
                ShowError(aParent, IDS_ERR_DELETE_PAT, nil,
                          prgFile^.dbc^.dbErrorNum, MOD_PURGE, 6);
              end;
            until not statDlg^.CanContinue or not prgFile^.dbc^.GetNext(prgFile^.dbr);
          end;
          statDlg^.CompPctLevel(j, k);
          statDlg^.SetText(SR(IDS_COMPLETE, pStr, 50), IDC_TEXT3);
          statDlg^.Focus;
          while statDlg^.CanContinue do ; {- wait for user to cancel }
        end;
      end;
      IDC_SPECIMENS:
      begin
        statDlg^.SetText(SR(IDS_PURGE_SPECIMENS, pStr, 50), IDC_TEXT1);
        prgFile := New(PDBFile, Init(DBSPECFile, '', dbOpenNormal));
        if prgFile = nil then
        begin
          ShowError(aParent, IDS_ERR_DATABASE, nil,
                    dbLastOpenError, MOD_PURGE, 5);
          break;
        end;
        if bUseMPSS then
        begin
          mpssObj := New(PMPSS, Init(aParent, prgFile, IncludeUndefDates));
          if (mpssObj <> nil) and
             mpssObj^.MPSSLoadRules(aParent, SR(IDS_PURGE_SPECIMENS, pStr, 200)) and
             NoYesMsg(aParent^.HWindow, title,
                      SR(IDS_SPEC_PROMPT3, pStr, 200)) then
          begin
            if mpssObj^.MPSSGetFirst(prgFile) then
            begin
              repeat
                statDlg^.IncrPctLevel(mpssObj^.MPSSGetPercent);
                statDlg^.Update;
                altFile := New(PDBAssocFile, Init(prgFile^.dbr,
                               DBSPECIMEN_PATIENT_ASSOC, dbOpenNormal));

                if not prgFile^.dbc^.DeleteRec(prgFile^.dbr) and
                   (prgFile^.dbc^.dbErrorNum <> dbErr_CantDeleteRecord) then
                begin
                  ShowError(aParent, IDS_ERR_DELETE_SPEC, nil,
                            prgFile^.dbc^.dbErrorNum, MOD_PURGE, 7);
                end;
                if altFile = nil then
                begin
                  ShowError(aParent, IDS_ERR_DATABASE, nil,
                            dbLastOpenError, MOD_PURGE, 6);
                  Continue;
                end;
                if altFile^.dbc^.GetFirst(altFile^.dbr) then
                begin
                  if not altFile^.dbc^.DeleteRec(altFile^.dbr) and
                     (altFile^.dbc^.dbErrorNum <> dbErr_CantDeleteRecord) then
                  begin
                    ShowError(aParent, IDS_ERR_DELETE_PAT, nil,
                              altFile^.dbc^.dbErrorNum, MOD_PURGE, 8);
                  end;
                end;
                MSDisposeObj(altFile);
              until not statDlg^.CanContinue or not mpssObj^.MPSSGetNext(prgFile);
            end;
            statDlg^.IncrPctLevel(mpssObj^.MPSSGetPercent);
            statDlg^.SetText(SR(IDS_COMPLETE, pStr, 50), IDC_TEXT3);
            statDlg^.Focus;

            while statDlg^.CanContinue do ; {- wait for user to cancel }

            MSDisposeObj(mpssObj);
          end;
        end
        else
        begin
          if not NoYesMsg(aParent^.HWindow, title,
                          SR(IDS_SPEC_PROMPT1, pStr, 200)) then
            break;

          j := 0;
          k := prgFile^.dbc^.NumRecords;
          if prgFile^.dbc^.GetFirst(prgFile^.dbr) then
          begin
            repeat
              Inc(j);
              statDlg^.CompPctLevel(j, k);
              statDlg^.Update;
              altFile := New(PDBAssocFile, Init(prgFile^.dbr,
                             DBSPECIMEN_PATIENT_ASSOC, dbOpenNormal));

              if not prgFile^.dbc^.DeleteRec(prgFile^.dbr) and
                 (prgFile^.dbc^.dbErrorNum <> dbErr_CantDeleteRecord) then
              begin
                ShowError(aParent, IDS_ERR_DELETE_SPEC, nil,
                          prgFile^.dbc^.dbErrorNum, MOD_PURGE, 9);
              end;
              if altFile = nil then
              begin
                ShowError(aParent, IDS_ERR_DATABASE, nil,
                          dbLastOpenError, MOD_PURGE, 7);
                Continue;
              end;
              if altFile^.dbc^.GetFirst(altFile^.dbr) then
              begin
                if not altFile^.dbc^.DeleteRec(altFile^.dbr) and
                   (altFile^.dbc^.dbErrorNum <> dbErr_CantDeleteRecord) then
                begin
                  ShowError(aParent, IDS_ERR_DELETE_PAT, nil,
                            altFile^.dbc^.dbErrorNum, MOD_PURGE, 10);
                end;
              end;
              MSDisposeObj(altFile);
            until not statDlg^.CanContinue or not prgFile^.dbc^.GetNext(prgFile^.dbr);
          end;
          statDlg^.CompPctLevel(j, k);
          statDlg^.SetText(SR(IDS_COMPLETE, pStr, 50), IDC_TEXT3);
          statDlg^.Focus;
          while statDlg^.CanContinue do ; {- wait for user to cancel }
        end;
      end;
      IDC_SPECIMENS_LEAVE_PATS:
      begin
        statDlg^.SetText(SR(IDS_PURGE_SPECIMENS, pStr, 50), IDC_TEXT1);
        prgFile := New(PDBFile, Init(DBSPECFile, '', dbOpenNormal));
        if prgFile = nil then
        begin
          ShowError(aParent, IDS_ERR_DATABASE, nil,
                    dbLastOpenError, MOD_PURGE, 8);
          break;
        end;
        if bUseMPSS then
        begin
          mpssObj := New(PMPSS, Init(aParent, prgFile, IncludeUndefDates));
          if (mpssObj <> nil) and
             mpssObj^.MPSSLoadRules(aParent, SR(IDS_PURGE_SPECIMENS, pStr, 200)) and
             NoYesMsg(aParent^.HWindow, title,
                      SR(IDS_SPEC_PROMPT4, pStr, 200)) then
          begin
            if mpssObj^.MPSSGetFirst(prgFile) then
            begin
              repeat
                statDlg^.IncrPctLevel(mpssObj^.MPSSGetPercent);
                statDlg^.Update;
                if not prgFile^.dbc^.DeleteRec(prgFile^.dbr) and
                   (prgFile^.dbc^.dbErrorNum <> dbErr_CantDeleteRecord) then
                begin
                  ShowError(aParent, IDS_ERR_DELETE_SPEC, nil,
                            prgFile^.dbc^.dbErrorNum, MOD_PURGE, 11);
                end;
              until not statDlg^.CanContinue or not mpssObj^.MPSSGetNext(prgFile);
            end;
            statDlg^.IncrPctLevel(mpssObj^.MPSSGetPercent);
            statDlg^.SetText(SR(IDS_COMPLETE, pStr, 50), IDC_TEXT3);
            statDlg^.Focus;

            while statDlg^.CanContinue do ; {- wait for user to cancel }

            MSDisposeObj(mpssObj);
          end;
        end
        else
        begin
          if not NoYesMsg(aParent^.HWindow, title,
                          SR(IDS_SPEC_PROMPT2, pStr, 200)) then
            break;

          j := 0;
          k := prgFile^.dbc^.NumRecords;
          if prgFile^.dbc^.GetFirst(prgFile^.dbr) then
          begin
            repeat
              Inc(j);
              statDlg^.CompPctLevel(j, k);
              statDlg^.Update;
              if not prgFile^.dbc^.DeleteRec(prgFile^.dbr) and
                 (prgFile^.dbc^.dbErrorNum <> dbErr_CantDeleteRecord) then
              begin
                ShowError(aParent, IDS_ERR_DELETE_SPEC, nil,
                          prgFile^.dbc^.dbErrorNum, MOD_PURGE, 12);
              end;
            until not statDlg^.CanContinue or not prgFile^.dbc^.GetNext(prgFile^.dbr);
          end;
          statDlg^.CompPctLevel(j, k);
          statDlg^.SetText(SR(IDS_COMPLETE, pStr, 50), IDC_TEXT3);
          statDlg^.Focus;
          while statDlg^.CanContinue do ; {- wait for user to cancel }
        end;
      end;
      IDC_QC_ISOLATES:
      begin
        statDlg^.SetText(SR(IDS_PURGE_QC_ISOLATES, pStr, 50), IDC_TEXT1);
        prgFile := New(PDBFile, Init(DBISOFile, '', dbOpenNormal));
        if prgFile = nil then
        begin
          ShowError(aParent, IDS_ERR_DATABASE, nil,
                    dbLastOpenError, MOD_PURGE, 9);
          break;
        end;
        if bUseMPSS then
        begin
          mpssObj := New(PMPSS, Init(aParent, prgFile, IncludeUndefDates));
          if (mpssObj <> nil) and
             mpssObj^.MPSSLoadRules(aParent, SR(IDS_PURGE_QC_ISOLATES, pStr, 200)) and
             NoYesMsg(aParent^.HWindow, title,
                      SR(IDS_ISO_PROMPT2, pStr, 200)) then
          begin
            if mpssObj^.MPSSGetFirst(prgFile) then
            begin
              repeat
                statDlg^.IncrPctLevel(mpssObj^.MPSSGetPercent);
                statDlg^.Update;
                if prgFile^.dbr^.GetField(DBISOQCFlag, @flg, SizeOf(flg)) and
                   flg and
                   not prgFile^.dbc^.DeleteRec(prgFile^.dbr) and
                   (prgFile^.dbc^.dbErrorNum <> dbErr_CantDeleteRecord) then
                begin
                  ShowError(aParent, IDS_ERR_DELETE_ISO, nil,
                            prgFile^.dbc^.dbErrorNum, MOD_PURGE, 13);
                end;
              until not mpssObj^.MPSSGetNext(prgFile);
            end;
            statDlg^.IncrPctLevel(mpssObj^.MPSSGetPercent);
            statDlg^.SetText(SR(IDS_COMPLETE, pStr, 50), IDC_TEXT3);
            statDlg^.Focus;
            while statDlg^.CanContinue do ; {- wait for user to cancel }
            MSDisposeObj(mpssObj);
          end;
        end
        else
        begin
          if not NoYesMsg(aParent^.HWindow, title,
                          SR(IDS_ISO_PROMPT1, pStr, 200)) then
            break;

          j := 0;
          k := prgFile^.dbc^.NumRecords;
          if prgFile^.dbc^.GetFirst(prgFile^.dbr) then
          begin
            repeat
              Inc(j);
              statDlg^.CompPctLevel(j, k);
              statDlg^.Update;
              if prgFile^.dbr^.GetField(DBISOQCFlag, @flg, SizeOf(flg)) and
                 flg and
                 not prgFile^.dbc^.DeleteRec(prgFile^.dbr) and
                 (prgFile^.dbc^.dbErrorNum <> dbErr_CantDeleteRecord) then
              begin
                ShowError(aParent, IDS_ERR_DELETE_ISO, nil,
                          prgFile^.dbc^.dbErrorNum, MOD_PURGE, 14);
              end;
            until not statDlg^.CanContinue or not prgFile^.dbc^.GetNext(prgFile^.dbr);
          end;
          statDlg^.CompPctLevel(j, k);
          statDlg^.SetText(SR(IDS_COMPLETE, pStr, 50), IDC_TEXT3);
          statDlg^.Focus;
          {- while statDlg^.CanContinue do ;  wait for user to cancel }
        end;
      end;
      else
      begin
        ;(* error *)
      end;
    end;
  until TRUE;
  MSDisposeObj(prgFile);
  MSDisposeObj(statDlg);
end;

END.

