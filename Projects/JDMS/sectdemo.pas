{$F+}
unit SectDemo;

{- Demographics / Free Text section driver  }

{=============================================================================}

INTERFACE

{- this module interfaces via SECTREG }

IMPLEMENTATION

uses
  DMSDebug,
  StrsW,
  GridLib,
  RptShare,
  DbLib,
  DBTypes,
  DBErrors,
  DBFile,
  CtlLib,
  DlgLib,
  SectReg,
  ApiTools,
  RptUtil,
  BoxWin,
  RptData,
  SectFlds,
  StatBar,
  userMsgs,
  MScan,
  DMSErr,
  DBIDS,
  PageWin,
  Strings,
  Win31,
  WinProcs,
  WinTypes,
  Objects,
  OWindows,
  ODialogs;

{$I RPTGEN.INC}

const
  Grid_Filename = 0;
  Grid_Fileshow = 1;
  Grid_Fldname  = 2;

type
  {- describes the fields that are inserted into a demographic/free text section}
  PSectDemoFld = ^TSectDemoFld;
  TSectDemoFld = object(TObject)
    fldType     : integer;  {- 0 = free text, 1 = field label, 2 = field value }
    buf         : array[0..MaxRptFreeText] of char;  {- free text -or- field
                                                        label (depends on type) }
    fileName    : array[0..9] of char;
    fldNum      : integer;
    org         : TPoint;
    ext         : TPoint;
    opts        : TFldOpts;
    constructor Init;
    procedure ChangeBuf(aNewBuf: PChar);
  end;

  {- demographic section options / flds }
  PSectDemoOpts = ^TSectDemoOpts;
  TSectDemoOpts = record
    font        : TRptFont;
    opts        : TSectOpt;
    org         : TPoint;       {- origin of section }
    ext         : TPoint;       {- extent of section }
    flds        : PCollection;  {- collection of fields in section }
  end;

  {- box (SECTION) window - for insertion onto report format }
  PSectDemo = ^TSectDemo;
  TSectDemo = object(TBoxWin)
    demoCfg   : TSectDemoOpts;
    demoDB    : PDBFile; {- demographics database file }
    demoFldDB : PDBFile; {- demographics fields database }
    {}
    constructor Init(AParent: PWindowsObject; ATitle: PChar;
                     rect: TRect; AnOrigin: TPoint; AZoom: Real;
                     aPage: TRptPage; AFont: TRptFont);
    destructor Done; virtual;
    procedure DefineBox(aParent: PWindowsObject); virtual;
    procedure ResetDefaults; virtual;
    function GetBoxType: integer; virtual;

    function SavePrep: boolean; virtual;
    function SaveData(var dataSeq: longint): boolean; virtual;
    procedure SaveComplete; virtual;
    function LoadData(aBoxSeq: longint): boolean; virtual;
  end;

  {- demographics and free text section editor }
  PSectDemoEdit = ^TSectDemoEdit;
  TSectDemoEdit = object(TSectFldEdit)
    cfg     : PSectDemoOpts;
    tcfg    : TSectDemoOpts;
    {}
    constructor Init(aParent: PWindowsObject; var aCfg: TSectDemoOpts; aSize: TPoint);
    procedure SetupWindow; virtual;
    procedure IDFont(var msg: TMessage); virtual ID_FIRST + IDC_FONT;
    procedure IDOptions(var msg: TMessage); virtual ID_FIRST + IDC_OPTIONS;
    function InitPage(aSize: TPoint): PSectPage; virtual;
    function CanClose: boolean; virtual;
    procedure Cancel(var msg: TMessage); virtual ID_FIRST + ID_CANCEL;
    procedure CollectFields;
  end;

  {- page window object for TSectDemoEdit (specialized for Dialog boxes) }
  PDemoPage = ^TDemoPage;
  TDemoPage = object(TSectPage)
    fldList : PCollection;
    constructor Init(AParent: PWindowsObject; aPage: TRptPage;
                     aFont: PRptFont; aFldList: PCollection);
    function InitBox(rect: TRect; anOrigin: TPoint): PBoxWin; virtual;
    procedure UMBoxClose(var msg: TMessage); virtual WM_FIRST + UM_BOXCLOSING;
  end;

  {- specialized TBoxDlg (used in Dialog boxes). This object holds info
     on the demographic fields }
  PDemoFld = ^TDemoFld;
  TDemoFld = object(TSectFld)
    fld     : PSectDemoFld;
    constructor Init(AParent: PWindowsObject; rect: TRect; AnOrigin: TPoint; AZoom: Real;
                     aPage: TRptPage; AFont: TRptFont; aFld: PSectDemoFld);
    procedure DefineBox(aParent: PWindowsObject); virtual;
    procedure GetName(AName: PChar; maxlen: Integer); virtual;
    function  GetNameLength: integer; virtual;
    procedure DrawCaption(dc: HDC); virtual;
    function BuildBoxFont(aLogFont: TLogFont): HFont; virtual;
  end;

  {- dialog to get a field number from user }
  PDefineFldDlg = ^TDefineFldDlg;
  TDefineFldDlg = object(TCenterDlg)
    fld     : PSectDemoFld;
    grid    : PGrid;
    constructor Init(aParent: PWindowsObject; aFld: PSectDemoFld);
    procedure SetupWindow; virtual;
    function CanClose: boolean; virtual;
    procedure WMMeasureItem(var msg: TMessage); virtual WM_First + WM_MEASUREITEM;
    procedure WMDrawItem(var msg: TMessage); virtual WM_First + WM_DRAWITEM;
    procedure WMCharToItem(var msg: TMessage); virtual WM_First + WM_CHARTOITEM;
    procedure IDGrid(var msg: TMessage); virtual ID_FIRST + IDC_LIST;
  end;

var
  sectionName  : array[0..30] of char;

{----------------------------------------------------------[ TDemoFld ]--}

constructor TDemoFld.Init(AParent: PWindowsObject; rect: TRect; AnOrigin: TPoint; AZoom: Real;
                        aPage: TRptPage; AFont: TRptFont; aFld: PSectDemoFld);
begin
  inherited Init(aParent, '', rect, anOrigin, aZoom, aPage, aFont);
  fld:= aFld;
end;

procedure TDemoFld.DefineBox(aParent: PWindowsObject);
var
  d   : PDialog;
begin
  d:= New(PDefineFldDlg, Init(aParent, fld));
  application^.ExecDialog(d);
end;

function TDemoFld.BuildBoxFont(aLogFont: TLogFont): HFont;
begin
  if fld^.opts.bold then
    aLogFont.lfWeight:= FW_BOLD
  else
    aLogFont.lfWeight:= FW_NORMAL;

  if fld^.opts.underline then
    aLogFont.lfUnderline:= 1
  else
    aLogFont.lfUnderline:= 0;

  BuildBoxFont:= CreateFontIndirect(aLogFont);
end;

function RptFieldSize(fil: PChar; fld: integer): integer;
{- return the max number of characters that will be displayed in a report field.
   This function looks at the specified field, and if it is a mnemonic file
   reference then it returns the length for the mnemonic file description,
   otherwise, it returns the number of characters needed to display the field.
   Fil  - file number  ( 1 - PATIENT, 2 - SPECIMEN ) no other files are
          supported
   fld  - the field number for the specified file}
var
  dbd   : PDBReadDesc;
  fi    : TFldInfo;
begin
  RptFieldSize:= 0;
  if (stricomp(fil, DBPatFile) = 0) or (stricomp(fil, DBSpecFile) = 0) then
  begin
    dbd:= New(PDBReadDesc, Init(fil, ''));
    if dbd <> nil then
    begin
      dbd^.FieldInfo(fld, fi);
      if dbd^.dbErrorNum = 0 then
      begin
        if (fi.userFlag and DB_MNEMONIC) <> 0 then
        begin {- *** should look up the mnemonic file and see what the desc len is}
          RptFieldSize:= 40;
        end
        else
        begin
          RptFieldSize:= dbd^.FieldSizeForStr(fld);
        end;
      end;
    end;
  end;
end;

procedure TDemoFld.DrawCaption(dc: HDC);
{- draw field name on screen }
var
  r       : TRect;
  pstr    : PChar;
  strSize : integer;
  sz      : TLRect;
  LTpPX   : integer;
  LTpPY   : integer;
  k       : integer;
begin
  GetSize(sz);
  LTwipsPerPixel(dc, LTpPX, LTpPY);

  r.top:= 0;
  r.bottom:= sz.bottom - r.top;
  r.left:= LTpPX * 2;
  r.right:= sz.right - r.left;
  pstr:= nil;
  if fld^.fldType = 0 then
  begin
    strSize:= GetNameLength;
    GetMem(pstr, strSize + 1);
    GetName(pstr, strSize);
  end
  else
  begin
    {- if fld is the field label then use that, otherwise, figure out the
       proper length for the field }
    if fld^.fldType = 1 then
    begin
      strSize:= 255; {- some length of adequate size }
      GetMem(pstr, strSize + 1);
      StrLCopy(pstr, fld^.buf, strSize)  {- label is in buf }
    end
    else
    begin
      strSize:= RptFieldSize(fld^.fileName, fld^.fldNum);
      GetMem(pstr, strSize + 1);
      CharStr('X', pstr, strSize);
    end;
  end;

  DrawText(dc, pstr, strlen(pstr), r, fld^.opts.just or DT_WORDBREAK or DT_NOPREFIX);
  MSFreeMem(pstr, strSize + 1);
end;

procedure TDemoFld.GetName(aName: PChar; maxlen: integer);
var
  pstr  : array[0..100] of char;
  p2    : array[0..100] of char;
begin
  StrCopy(aName, '?');
  case fld^.fldType of
    0 : {- free text }
      begin
        if strlen(fld^.buf) = 0 then
          SR(IDS_FREETEXT2, aName, maxLen)
        else
          StrlCopy(aName, fld^.buf, maxLen);
      end;
    1 : {- field label }
      begin
        SR(IDS_LABEL, pstr, sizeof(pstr)-1);
        StrCat(pstr, ' - ');
        StrCat(pstr, fld^.buf);
        StrLCopy(aName, pstr, maxLen);
      end;
    2 : {- field value }
      begin
        SR(IDS_VALUE, pstr, sizeof(pstr)-1);
        StrCat(pstr, ' - ');
        strcat(pstr, fld^.buf);
        StrLCopy(aName, pstr, maxLen);
      end;
  end;
end;

function TDemoFld.GetNameLength: integer;
var
  pstr  : array[0..100] of char;
begin
  case fld^.fldType of
    0 : {- free text }
      if strLen(fld^.buf) = 0 then
      begin
        GetNameLength:= StrLen(SR(IDS_FREETEXT, pstr, 100)) + 2;
      end
      else
        GetNameLength:= StrLen(fld^.buf);
    1,2 : {- field label/name }
      begin
        GetName(pstr, 256);
        GetNameLength:= StrLen(pstr);
      end;
    else
      GetNameLength:= 0;
  end;
end;

{---------------------------------------------[ TSectDemoFld ]--}

constructor TSectDemoFld.Init;
begin
  inherited Init;
  fldType:= 0;
  ChangeBuf('');
  strcopy(fileName, '');
  fldNum:= 0;
  FillChar(org, sizeof(org), 0);
  FillChar(ext, sizeof(ext), 0);
  opts.just:= DT_LEFT;
  opts.bold:= false;
  opts.underline:= false;
end;

procedure TSectDemoFld.ChangeBuf(aNewBuf: PChar);
begin
  StrCopy(buf, aNewBuf);
end;

{---------------------------------------------[ TDefineFldDlg ]--}

constructor TDefineFldDlg.Init(aParent: PWindowsObject; aFld: PSectDemoFld);
var
  c  : PControl;
  lf : TLogFont;
begin
  fld:= aFld;
  grid:= nil;
  if fld^.fldType > 0 then  {- field label or value }
  begin
    inherited Init(aParent, MakeIntResource(DLG_FLDLIST));
    c:= New(PLStatic, InitResource(@self, 300, 0, false));
    grid:= New(PGrid, InitResource(@self, IDC_LIST, 3, false));
  end
  else  {- free text }
    inherited Init(aParent, MakeIntResource(DLG_FREETEXT));

  c:= New(PLRadioButton, InitResource(@self, IDC_LEFT, false));
  c:= New(PLRadioButton, InitResource(@self, IDC_CENTER, false));
  c:= New(PLRadioButton, InitResource(@self, IDC_RIGHT, false));
  c:= New(PLCheckBox, InitResource(@self, IDC_BOLD, false));
  c:= New(PLCheckBox, InitResource(@self, IDC_UNDERLINE, false));
end;

procedure TDefineFldDlg.SetupWindow;
var
  focRow  : integer;
  pstr    : array[0..100] of char;
  p1      : array[0..100] of char;

  procedure FillFlds(fil: PChar);
  var
    dbd  : PDBReadDesc;
    fi   : TFldInfo;
    fileI: TFileInfo;
    row  : longint;
    k    : integer;
    subst: PErrSubst;
  begin
    dbd:= New(PDBReadDesc, Init(fil, ''));
    if dbd = nil then
    begin
      subst:= New(PErrSubst, Init(fil));
      ShowError(@self, IDS_CANTACCESSDEF, subst, dbLastOpenError, MOD_RPTGEN, 1);
      MSDisposeObj(subst);
    end
    else
    begin
      for k:= 1 to dbd^.NumFields do
      begin
        dbd^.FieldInfo(k, fi);
        if fi.userFlag and db_Hidden = 0 then
        begin
          row:= grid^.AddString('');
          grid^.SetData(Grid_Filename, row, fil);
          if k = 1 then
          begin
            dbd^.FileInfo(fileI);
            grid^.SetData(Grid_Fileshow, row, fileI.desc);
          end;
          grid^.SetData(Grid_Fldname, row, fi.fldName);
          grid^.SetItemData(row, k);

          {- if k is pointing to the selected field for the correct file... }
          if (StrComp(fil, fld^.fileName) = 0) and (k = fld^.fldNum) then
            focRow:= row;
        end;
      end;
      MSDisposeObj(dbd);
    end;
  end;

begin
  inherited SetupWindow;
  if fld^.fldType = 0 then
    SendDlgItemMsg(IDC_FREETEXT, WM_SETTEXT, 0, longint(@fld^.buf))
  else
  begin
    GetWindowText(HWindow, pstr, 100);
    if fld^.fldType = 1 then
      SR(IDS_NAME, p1, sizeof(p1)-1)
    else
      SR(IDS_VALUE, p1, sizeof(p1)-1);
    AnsiUpper(p1);
    StrCat(pstr, ' - ');
    StrCat(pstr, p1);
    SetWindowText(HWindow, pstr);

    grid^.SetColumnWidth(grid_FileName, 0);
    grid^.SetColumnWidth(grid_FileShow, 150);
    grid^.StretchColumn(grid_FldName);
    grid^.SetHeader(grid_FileShow, SR(IDS_DATABASE, p1, sizeof(p1)-1));
    grid^.SetHeader(grid_FldName, SR(IDS_FIELD, p1, sizeof(p1)-1));
    grid^.SetSearchColumn(grid_FldName);

    focRow:= 0;
    FillFlds(DBPatFile);
    FillFlds(DBSpecFile);
    grid^.SetSelIndex(focRow);
  end;
  if fld^.opts.bold then
    SendDlgItemMsg(IDC_BOLD, BM_SETCHECK, BF_CHECKED, 0)
  else
    SendDlgItemMsg(IDC_BOLD, BM_SETCHECK, BF_UNCHECKED, 0);
  if fld^.opts.underline then
    SendDlgItemMsg(IDC_UNDERLINE, BM_SETCHECK, BF_CHECKED, 0)
  else
    SendDlgItemMsg(IDC_UNDERLINE, BM_SETCHECK, BF_UNCHECKED, 0);

  SendDlgItemMsg(IDC_LEFT, BM_SETCHECK, BF_UNCHECKED, 0);
  SendDlgItemMsg(IDC_CENTER, BM_SETCHECK, BF_UNCHECKED, 0);
  SendDlgItemMsg(IDC_RIGHT, BM_SETCHECK, BF_UNCHECKED, 0);
  case fld^.opts.just of
    DT_LEFT:
      SendDlgItemMsg(IDC_LEFT, BM_SETCHECK, BF_CHECKED, 0);
    DT_CENTER:
      SendDlgItemMsg(IDC_CENTER, BM_SETCHECK, BF_CHECKED, 0);
    DT_RIGHT:
      SendDlgItemMsg(IDC_RIGHT, BM_SETCHECK, BF_CHECKED, 0);
  end;
end;

procedure TDefineFldDlg.IDGrid(var msg: TMessage);
begin
  if (msg.lParamHi = LBN_DBLCLK) then
    OK(msg);
end;

procedure TDefineFldDlg.WMMeasureItem(var msg: TMessage);
begin
  if grid <> nil then
    grid^.MeasureItem(msg);
end;

procedure TDefineFldDlg.WMDrawItem(var msg: TMessage);
begin
  if grid <> nil then
    grid^.DrawItem(msg);
end;

procedure TDefineFldDlg.WMCharToItem(var msg: TMessage);
begin
  if grid <> nil then
    grid^.CharToItem(msg);
end;

function TDefineFldDlg.CanClose: boolean;
var
  ret   : boolean;
  tstr  : array[0..MaxRptFreeText] of char;
  k     : integer;
begin
  ret:= true;
  if fld^.fldType = 0 then
  begin
    SendDlgItemMsg(IDC_FREETEXT, WM_GETTEXT, MaxRptFreeText, longint(@tstr));
    fld^.ChangeBuf(tstr);
  end
  else
  begin
    k:= grid^.GetSelIndex;
    if k = -1 then
    begin
      ShowError(@self, IDS_MUSTSELFLD, nil, 0, MOD_RPTGEN, 1);
      ret:= false;
    end
    else
    begin
      grid^.GetData(grid_FileName, k, fld^.fileName, 8);
      grid^.GetData(grid_fldName, k, tstr, MaxRptFreeText);
      fld^.ChangeBuf(tstr);
      fld^.fldNum:= grid^.GetItemData(k);
    end;
  end;
  if ret then
  begin
    if SendDlgItemMsg(IDC_CENTER, BM_GETCHECK, 0,0) = BF_CHECKED then
      fld^.opts.just:= DT_CENTER
    else if SendDlgItemMsg(IDC_RIGHT, BM_GETCHECK, 0,0) = BF_CHECKED then
      fld^.opts.just:= DT_RIGHT
    else
      fld^.opts.just:= DT_LEFT;

    fld^.opts.bold:= (SendDlgItemMsg(IDC_BOLD, BM_GETCHECK, 0,0) = BF_CHECKED);
    fld^.opts.underline:= (SendDlgItemMsg(IDC_UNDERLINE, BM_GETCHECK, 0,0) = BF_CHECKED);
  end;
  CanClose:= ret;
end;

{---------------------------------------------[ TSectDemoEdit ]--}

constructor TSectDemoEdit.Init(aParent: PWindowsObject; var aCfg: TSectDemoOpts; aSize: TPoint);
begin
  cfg:= @aCfg;
  tcfg:= cfg^;
  inherited Init(aParent, DLG_FLDEDIT, sectionName, aSize, @tcfg.font);
end;

procedure TSectDemoEdit.SetupWindow;
var
  LTpPX, LTpPY  : integer;
  dc            : HDC;
  origin        : TPoint;

  procedure DisplayIt(fld: PSectDemoFld); far;
  var
    r   : TRect;
    p   : PDemoFld;
    org : TPoint;
  begin
    with pw^ do
    begin
      r.left:= (fld^.org.x + edgeX + page.margin.left) div LTpPX;
      r.top:=  (fld^.org.y + edgeY + page.margin.top) div LTpPY;

      r.right:= r.left + (fld^.ext.x div LTpPX);
      r.bottom:= r.top + (fld^.ext.y div LTpPY);

(*      r.left:= (org.x + page^.edgeX + page.margin.left) div LTpPX;
      r.top:=  (org.y + page^.edgeY + page^.page.margin.top) div LTpPY;
      r.right:= r.left + (ext.x div LTpPX);
      r.bottom:= r.top + (ext.y div LTpPY);
*)
{    constructor Init(AParent: PWindowsObject; rect: TRect; AnOrigin: TPoint; AZoom: Real;
                     aPage: TRptPage; AFont: TRptFont; aFld: PSectDemoFld);}
      GetOrigin(org);
      p:= New(PDemoFld, Init(pw, r, org, GetZoom, page, tcfg.font, fld));
      application^.MakeWindow(p);
      boxes^.Insert(p);
      SetFocus(p^.HWindow);
      UpdateFocusChild;
    end;
  end;

begin
  inherited SetupWindow;

  {- need to display all fields here ! }

  dc:= GetDC(HWindow);
  SetLogicalTwips(dc, 1.0);
  LTwipsPerPixel(dc, LTpPX, LTpPY);
  ReleaseDC(HWindow, dc);

  tcfg.flds^.ForEach(@DisplayIt);
end;

procedure TSectDemoEdit.IDOptions(var msg: TMessage);
begin
  GetSectOpt(@self, tCfg.opts);
end;

procedure TSectDemoEdit.IDFont(var msg: TMessage);
begin
  GetNewFont(tcfg.font);
end;

function TSectDemoEdit.InitPage(aSize: TPoint): PSectPage;
var
  page  : TRptPage;
begin
  FillChar(page, sizeof(page), 0);
  page.size.X:= aSize.x;
  page.size.Y:= aSize.y;
  InitPage:= New(PDemoPage, Init(@self, page, @tcfg.font, tcfg.flds));
end;

procedure TSectDemoEdit.CollectFields;
{- gather all field definitions from the child box field windows }

  procedure GetIt(p: PDemoFld); far;
  {- extract size from box field and update the field info record }
  var
    sz  : TLRect;
  begin
    p^.GetSize(sz);

    p^.fld^.org.x:= sz.left;
    p^.fld^.org.y:= sz.top;
    p^.fld^.ext.x:= sz.right;
    p^.fld^.ext.y:= sz.bottom;
  end;

begin
  with tcfg do
  begin
    pw^.boxes^.ForEach(@GetIt);
  end;
end;

function TSectDemoEdit.CanClose: boolean;
begin
  CollectFields;
  cfg^:= tcfg;
end;

procedure TSectDemoEdit.Cancel(var msg: TMessage);
begin
  if CanClose then
    inherited Cancel(msg);
end;

{----------------------------------------------[ TDemoPage ]--}

constructor TDemoPage.Init(AParent: PWindowsObject; aPage: TRptPage;
                           aFont: PRptFont; aFldList: PCollection);
begin
  inherited Init(aParent, aPage, aFont);
  fldList:= aFldList;
end;

function TDemoPage.InitBox(rect: TRect; anOrigin: TPoint): PBoxWin;
var
  sel  : integer;
  c    : PCollection;
  pstr : array[0..100] of char;
  fld  : PSectDemoFld;
  d    : PDialog;
begin
  InitBox:= nil;

  c:= New(PCollection, Init(10,10));
  c^.Insert(New(PAddFldTypes, Init(SR(IDS_FREETEXT, pstr, 100), 0)));
  c^.Insert(New(PAddFldTypes, Init(SR(IDS_FIELDNAME, pstr, 100), 1)));
  c^.Insert(New(PAddFldTypes, Init(SR(IDS_FIELDVALUE, pstr, 100), 2)));

  sel:= GetFieldType(parent, c);
  if sel >= 0 then
  begin
    fld:= New(PSectDemoFld, Init);
    fld^.fldType:= sel;
    d:= New(PDefineFldDlg, Init(parent, fld));
    if application^.ExecDialog(d) = IDOK then
    begin
      rect.bottom:= rect.top + GetFontHeight;
      InitBox:= New(PDemoFld,
        Init(@self, rect, anOrigin, GetZoom, page, pFont^, fld));
      fldList^.Insert(fld);
    end
    else
      MSDisposeObj(fld);
  end;
  MSDisposeObj(c);
end;

procedure TDemoPage.UMBoxClose(var msg: TMessage);
{- when a box closes, be sure to remove the fld definition object
   from the list of fields in the configuration for the section }
var
  bw    : PDemoFld;
  f     : PSectDemoFld;
  k   : integer;
begin
  bw:= PDemoFld(msg.lParam);

  k:= fldList^.IndexOf(bw^.fld);
  if k <> -1 then
  begin
    fldList^.AtFree(k);
  end;
  inherited UMBoxClose(msg);
end;

{---------------------------------------------[ TSectDemo ]--}

constructor TSectDemo.Init(AParent: PWindowsObject; ATitle: PChar;
                     rect: TRect; AnOrigin: TPoint; AZoom: Real;
                     aPage: TRptPage; AFont: TRptFont);
begin
  {- need to init flds before calling inherited Init because the ancestor
    calls ResetDefaults which uses flds and expects it to be valid }
  FillChar(demoCfg, sizeof(demoCfg), 0);
  demoCfg.flds:= New(PCollection, Init(20, 10));
  demoDB:= nil;
  demoFldDB:= nil;

  inherited Init(aParent, aTitle, rect, anOrigin, aZoom, aPage, aFont);
end;

destructor TSectDemo.Done;
begin
  inherited Done;
  MSDisposeObj(demoCfg.flds);
end;

procedure TSectDemo.DefineBox(aParent: PWindowsObject);
var
  sz    : TLRect;
  aSize : TPoint;
begin
  GetSize(sz);
  aSize.x:= integer(sz.right);
  aSize.y:= integer(sz.bottom);
  application^.ExecDialog(New(PSectDemoEdit, Init(aParent, demoCfg, aSize)));
  SetDisplayFont(demoCfg.Font);
end;

procedure TSectDemo.ResetDefaults;
begin
  with demoCfg do
  begin
    with opts do
    begin
      bkMode:= OPAQUE;
      border:= -1;  {- none }
    end;
    MakeDefaultRFont(font);
    flds^.FreeAll;
  end;
end;

function TSectDemo.GetBoxType: integer;
begin
  GetBoxType:= sectDemoFreeID;
end;

function TSectDemo.SavePrep: boolean;
{- open database files and prepare to save }
var
  pstr: array[0..200] of char;
begin
  demoFldDB:= nil;
  demoDB:= New(PDBFile, Init(DBRPTDemoSectsFile, '', dbOpenNormal));
  if demoDB = nil then
  begin
    ShowError(GetParentObj, IDS_CANTOPENDEMOSECT, nil, dbLastOpenError, MOD_RPTGEN, 1);
  end
  else
  begin
    demoFldDB:= New(PDBFile, Init(DBRPTDemoFldsFile, '', dbOpenNormal));
    if demoFldDB = nil then
    begin
      ShowError(GetParentObj, IDS_CANTOPENDEMOFLDS, nil, dbLastOpenError, MOD_RPTGEN, 1);
    end;
  end;
  SavePrep:= (demoDB <> nil) and (demoFldDB <> nil);
end;

function TSectDemo.SaveData(var dataSeq: longint): boolean;
var
  pstr: array[0..200] of char;
  sz  : TLRect;
  ret : boolean;

  procedure SaveFld(fld: PSectDemoFld); far;
  {- save data for specified field. Assumes boxSeq has the Seq number for the
     section record }
  begin
    if not ret then exit;
    with demoFldDB^ do
    begin
      dbr^.PutField(DBRptDemoFldsSectRef, @boxSeq);
      with fld^ do
      begin
        dbr^.PutField(DBRptDemoFldsFldType, @fldType);
        dbr^.PutField(DBRptDemoFldsBuf, @buf);
        dbr^.PutField(DBRptDemoFldsFileName, @fileName);
        dbr^.PutField(DBRptDemoFldsFldNum, @fldNum);
        dbr^.PutField(DBRptDemoFldsOrg, @org);
        dbr^.PutField(DBRptDemoFldsExt, @ext);
        dbr^.PutField(DBRptDemoFldsOpts, @opts);
      end;
      dbc^.InsertRec(dbr);
      if dbc^.dbErrorNum <> 0 then
      begin
        ret:= false;
        ShowError(GetParentObj, IDS_CANTINSERTDEMOFLD, nil, dbc^.dbErrorNum, MOD_RPTGEN, 0);
      end;
    end;
  end;

begin
  ret:= true;
  with demoDB^ do
  begin
    GetSize(sz);
    dbr^.PutField(DBRptDemoSectFont, @demoCfg.font);
    dbr^.PutField(DBRptDemoSectOptions, @demoCfg.opts);
    dbr^.PutField(DBRptDemoSectSize, @sz);

    dbc^.InsertRec(dbr);
    if dbc^.dbErrorNum = 0 then
      boxSeq:= dbr^.GetSeqValue  {- save new seq number }
    else
    begin
      ret:= false;
      ShowError(GetParentObj, IDS_CANTINSERTDEMOSECT, nil, dbc^.dbErrorNum, MOD_RPTGEN, 0);
    end;
    dataSeq:= boxSeq;

    if ret then  {- now save all fields for the section }
      demoCfg.flds^.ForEach(@SaveFld);
  end;
  SaveData:= ret;
end;

procedure TSectDemo.SaveComplete;
begin
  MSDisposeObj(demoDB);
  MSDisposeObj(demoFldDB);
end;

function TSectDemo.LoadData(aBoxSeq: longint): boolean;
var
  ret : boolean;
  r   : TLRect;
  db  : PDBFile;
  p1  : array[0..200] of char;

  procedure LoadFields;
  {- load fields from database. Assumes that boxSeq is the seq for the section }
  var
    seq    : longint;
    dbFlds : PDBFile;
    f      : PSectDemoFld;
  begin
    demoCfg.flds^.FreeAll;
    dbFlds:= New(PDBFile, Init(DBRPTDemoFldsFile, '', dbOpenNormal));
    if dbFlds = nil then
    begin
      ret:= false;
      ShowError(GetParentObj, IDS_CANTOPENDEMOFLDS, nil, dbLastOpenError, MOD_RPTGEN, 2);
    end
    else
    begin
      with dbFlds^ do
      begin
        seq:= boxSeq;
        dbr^.PutField(DBRptDemoFldsSectRef, @boxSeq);
        dbc^.GetEQ(dbr);
        while (dbc^.dbErrorNum = 0) and (seq = boxSeq) do
        begin
          f:= New(PSectDemoFld, Init);
          dbr^.GetField(DBRptDemoFldsFldType, @f^.fldType,  sizeof(f^.fldType));
          dbr^.GetField(DBRptDemoFldsBuf, @f^.buf,      sizeof(f^.buf));
          dbr^.GetField(DBRptDemoFldsFileName, @f^.fileName, sizeof(f^.fileName));
          dbr^.GetField(DBRptDemoFldsFldNum, @f^.fldNum,   sizeof(f^.fldNum));
          dbr^.GetField(DBRptDemoFldsOrg, @f^.org,      sizeof(f^.org));
          dbr^.GetField(DBRptDemoFldsExt, @f^.ext,      sizeof(f^.ext));
          dbr^.GetField(DBRptDemoFldsOpts, @f^.opts,     sizeof(f^.opts));
          demoCfg.flds^.Insert(f);
          dbc^.GetNext(dbr);
          if dbc^.dbErrorNum = 0 then
            dbr^.GetField(DBRptDemoFldsSectRef, @seq, sizeof(seq));
        end;
      end;

      MSDisposeObj(dbFlds);
    end;
  end;

begin
  inherited LoadData(aBoxSeq);
  ret:= true;

  db:= New(PDBFile, Init(DBRPTDemoSectsFile, '', dbOpenNormal));
  if db = nil then
  begin
    ShowError(nil, IDS_CANTOPENDEMOSECT, nil, dbLastOpenError, MOD_RPTGEN, 2);
    CloseWindow;  {- delete the section from the report }
    boxSeq:= -1;
    ret:= false;
  end
  else
  begin
    boxSeq:= aBoxSeq;

    with db^ do
    begin
      dbc^.GetSeq(dbr, boxSeq);
      if dbc^.dbErrorNum = 0 then
      begin
        dbr^.GetField(DBRptDemoSectFont, @demoCfg.font, sizeof(demoCfg.font));
        SetDisplayFont(demoCfg.font);

        dbr^.GetField(DBRptDemoSectOptions, @demoCfg.opts, sizeof(demoCfg.opts));

        dbr^.GetField(DBRptDemoSectSize, @r, sizeof(r));
        SetSize(r);

        {- now load all fields for the section }
        LoadFields;
      end
      else
      begin
        ShowError(nil, IDS_CANTLOCSECTRECT, nil, dbc^.dbErrorNum, MOD_RPTGEN, 1);
        CloseWindow;  {- delete the section from the report }
        boxSeq:= -1;
        ret:= false;
      end;
    end;
    MSDisposeObj(db);
  end;
  LoadData:= ret;
end;

{-----------------[ Section Registration ]--}

function SectDemoDelete(action: integer; aRptSeq, aSectSeq: longint): boolean;
{- section delete function (for section registration }
const
  dbSect : PDBFile = nil;
  dbFlds : PDBFile = nil;
var
  ret   : boolean;
begin
  ret:= true;
  if (action and ACTION_OPEN <> 0) then
  begin
    if dbSect = nil then
    begin
      dbSect:= New(PDBFile, Init(DBRPTDemoSectsFile, '', dbOpenNormal));
      if dbSect = nil then
      begin
        ShowError(nil, IDS_CANTOPENDEMOSECT, nil, dbLastOpenError, MOD_RPTGEN, 3);
        ret:= false;
      end;
    end;
    if dbFlds = nil then
    begin
      dbFlds:= New(PDBFile, Init(DBRPTDemoFldsFile, '', dbOpenNormal));
      if dbFlds = nil then
      begin
        ShowError(nil, IDS_CANTOPENDEMOFLDS, nil, dbLastOpenError, MOD_RPTGEN, 3);
        ret:= false;
      end;
    end;
  end;

  if ret then
  begin
    if (action and ACTION_DELETE <> 0) then
    begin
      if (dbSect = nil) or (dbFlds = nil) then
        ret:= false
      else
      begin
        if dbSect^.dbc^.GetSeq(dbSect^.dbr, aSectSeq) then
            dbSect^.dbc^.DeleteRec(dbSect^.dbr);

        dbFlds^.dbr^.PutField(DBRptDemoFldsSectRef, @aSectSeq);
        if dbFlds^.dbc^.GetEQ(dbFlds^.dbr) then
        begin
          repeat
            dbFlds^.dbc^.DeleteRec(dbFlds^.dbr);
          until (dbFlds^.dbc^.dbErrorNum <> 0) or not dbFlds^.dbc^.GetEQ(dbFlds^.dbr);
        end;
      end;
    end;
  end;

  if ret and (action and ACTION_CLOSE <> 0) then
  begin
    MSDisposeObj(dbSect);
    MSDisposeObj(dbFlds);
  end;

  SectDemoDelete:= ret;
end;

function SectDemoInit(aParent: PWindowsObject; r: TRect; origin: TPoint;
                      aZoom: Real; page: TRptPage; aFont: TRptFont): PBoxWin;
begin
  SectDemoInit:= New(PSectDemo, Init(aParent, sectionName, r, origin, azoom, page, aFont));
end;

BEGIN
  sections^.RegisterSection(SectDemoInit, SectDemoDelete, SR(IDS_DemoFree,
                          sectionName, sizeof(sectionName)), sectDemoFreeID);
END.

{- rcf }
