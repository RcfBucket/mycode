(****************************************************************************


uiiso.inc

produced by Borland Resource Workshop


*****************************************************************************)

const
  DLG_CLEAR           = 8641;
  DLG_UIISO           = 8640;

  IDC_AS4             = 101;
  IDC_BARCODES        = 102;
  IDC_CLEARALL        = 103;
  IDC_CLEARISO        = 104;
  IDC_MOVE            = 105;
  IDC_SPECIMEN        = 106;
  IDC_PATIENT         = 107;
  IDC_SESSION         = 108;
  IDC_IDMIC           = 109;

  IDC_MODE            = 600;
  IDC_ISOSESSION      = 601;

  IDC_EDISO           = 200;
  IDC_EDORDER         = 201;
  IDC_EDORG           = 202;
  IDC_FAMILYTXT       = 203;
  IDC_EDTESTDATE      = 204;
  IDC_EDXMIT          = 205;
  IDC_EDQC            = 206;
  IDC_EDFREETEXT      = 207;

  IDC_EDSPECID        = 208;
  IDC_EDCOLLECT       = 209;
  IDC_PATID           = 210;
  IDC_WARD            = 211;
  IDC_SOURCE          = 212;
  IDC_FIRST           = 213;
  IDC_LAST            = 214;

  IDC_LBLISOID        = 300;
  IDC_LBLORDER        = 301;
  IDC_LBLORG          = 302;
  IDC_LBLFAMILY       = 303;
  IDC_LBLTESTDATE     = 304;
  IDC_LBLFREETEXT     = 307;

  IDC_LBLSPECID       = 308;
  IDC_LBLCOLLECT      = 309;
  IDC_LBLPATID        = 310;
  IDC_LBLWARD         = 311;
  IDC_LBLSOURCE       = 312;
  IDC_LBLLAST         = 313;
  IDC_LBLFIRST        = 314;

  IDC_EDUSER1         = 400;
  IDC_EDUSER2         = 401;
  IDC_EDUSER3         = 402;
  IDC_EDUSER4         = 403;
  IDC_EDUSER5         = 404;
  IDC_EDUSER6         = 405;
  IDC_EDUSER7         = 406;
  IDC_EDUSER8         = 407;
  IDC_EDUSER9         = 408;
  IDC_EDUSER10        = 409;

  IDC_LBLUSER1        = 500;
  IDC_LBLUSER2        = 501;
  IDC_LBLUSER3        = 502;
  IDC_LBLUSER4        = 503;
  IDC_LBLUSER5        = 504;
  IDC_LBLUSER6        = 505;
  IDC_LBLUSER7        = 506;
  IDC_LBLUSER8        = 507;
  IDC_LBLUSER9        = 508;
  IDC_LBLUSER10       = 509;

	IDS_ISONOTBELONG	=	8640;
	IDS_XMITSET	=	8641;
	IDC_FAMILY	=	110;
