unit CtlLib;

{- Extended support for controls with bold/unbold fonts }

INTERFACE

uses
  Objects,
  ODialogs,
  OWindows,
  Winprocs,
  Wintypes;

type
  {- static text with bold/unbold }
  PLStatic = ^TLStatic;
  TLStatic = object(TStatic)
    isBold: boolean;
    constructor Init(aParent: PWindowsObject; anID: integer; aTitle: PChar;
                     x,y,w,h: integer; aTextLen: word;
                     bold: boolean);
    constructor InitResource(aParent: PWindowsObject; resourceID, aTextLen: word;
                             bold: boolean);
    procedure SetupWindow; virtual;
  end;

  PLRadioButton = ^TLRadioButton;
  TLRadioButton = object(TRadioButton)
    isBold: boolean;
    constructor Init(AParent: PWindowsObject; AnID: integer; ATitle: PChar;
                     X,Y,W,H: integer; AGroup: PGroupBox; bold: boolean);
    constructor InitResource(aParent: PWindowsObject; resourceID: word; bold: boolean);
    procedure SetupWindow; virtual;
  end;

  PLCheckBox = ^TLCheckBox;
  TLCheckBox = object(TCheckBox)
    isBold: boolean;
    constructor Init(AParent: PWindowsObject; AnID: Integer; ATitle: PChar;
                     X,Y,W,H: Integer; AGroup: PGroupBox; bold: boolean);
    constructor InitResource(aParent: PWindowsObject; resourceID: word; bold: boolean);
    procedure SetupWindow; virtual;
  end;

  {- if you create a TRadioGroup, you must also dispose of it. This is unlike
     the other controls (they do not need to be destroyed by the user) }
  PRadioGroup = ^TRadioGroup;
  TRadioGroup = object(TObject)
    constructor Init(aParent: PWindowsObject; anID: word; aRetVal: longint; bold: boolean);
    destructor Done; virtual;
    procedure AddToGroup(anID: word; aRetVal: longint);
    function  SelectedVal: longint;
    procedure SetSelected(aVal: longint);
    procedure Enable(aVal: longint; on: boolean);
    procedure EnableGroup(on: boolean);
    function  IsEnabled(aVal: longint): boolean;
    function  IsGroupEnabled: boolean;
    private
    btns    : PCollection;
    boldGrp : boolean;
    parent  : PWindowsObject;
    function FindButton(aVal: longint): Pointer;
  end;

  {- this object is to be used when collecting items for a TListEdit control. }
  {- The data in the item can be broken into columns by putting a TAB character
     into the string (buf) between the columns. }
  PListEditItem = ^TListEditItem;
  TListEditItem = object(TObject)
    buf   : PChar;
    constructor Init(aString: PChar);
    destructor Done; virtual;
  end;

  {- TListEdit - Provides a custom edit control that only allows data that the
     user specifies from a list.  The control will also list the data on request.
     List data passed in MUST be a collection of PListEditItem (above).
     Parameters:
     aList    - Collection of TListItemData. This list will be the list of valid
                data that the control will accept. All other data will be invalid.
     anItemHeader - This is a string to be put in the header of the grid when
                    data is listed for the edit control. If it is nil, then there
                    will be no header for the grid.
                    If more than one column is desired for the list display,
                    separate the columns headers in this string with tab chars.
                    This parameter must be specified if more than one column is
                    desired. Also, the data from the FIRST column is what will
                    be used to validate the user entered data. Other columns
                    are just there for show when listing.
     aListTitle   - This is the title that will be displayed in the list dialog
                    box.
     aCaseSensitive-This tells the control that data entered in the the edit
                    control is case sensitive against the list of data passed in
     aListKey     - This is the key that will activate the list should the user
                    press it. Ie, VK_F3 ...
  }
  PListEdit = ^TListEdit;
  TListEdit = object(TEdit)
    list        : PCollection;
    header      : PChar;
    title       : PChar;
    maxListLen  : integer;
    caseSens    : boolean;
    listKey     : word;
    cols        : integer;
    constructor InitResource(AParent: PWindowsObject; resourceID: word;
                             aList: PCollection; anItemHeader, aListTitle: PChar;
                             aCaseSensitive: boolean; aListKey: word);
    constructor Init(aParent: PWindowsObject; anId: integer; aTitle: PChar;
                     X,Y,W,H: integer; aList: PCollection;
                     anItemHeader, aListTitle: PChar; aCaseSensitive: boolean;
                     aListKey: word);
    destructor Done; virtual;
    procedure ListData;
    function IsValid: integer; virtual;
    function Format: integer; virtual;
    procedure WMKeyDown(var msg: TMessage); virtual WM_FIRST + WM_KEYDOWN;
    procedure WMRButtonDown(var msg: TMessage); virtual WM_FIRST + WM_RBUTTONDOWN;
    procedure ChangeList(aList: PCollection);
    procedure ChangeTitle(aListTitle: PChar);
    procedure DataSelected; virtual;
    private
    procedure Initialize(aList: PCollection; anItemHeader, aListTitle: PChar;
                         aCaseSensitive: boolean; aListKey: word);
  end;

  PFilterEdit = ^TFilterEdit;
  TFilterEdit = object(TEdit)
    filter  : PChar;
    delPressed : boolean;
    {}
    constructor InitResource(AParent: PWindowsObject; ResourceID, ATextLen: word;
                             filterChars: PChar);
    destructor Done; virtual;
    procedure WMChar(var msg: TMessage); virtual WM_FIRST + WM_CHAR;
    procedure WMKeydown(var msg: TMessage); virtual WM_FIRST + WM_KEYDOWN;
  end;

IMPLEMENTATION

uses
  DMSDebug,
  DMString,
  Strings,
  StrsW,
  DlgLib,
  GridLib,
  MScan,
  APITools;

{$R CTLLIB.RES}
{$I CTLLIB.INC}

var
  exitSave    : pointer;    {- exit proc save address }
  unBoldFont  : HFont;

type
  PRadioGrpInfo = ^TRadioGrpInfo;
  TRadioGrpInfo = object(TObject)
    retVal  : longint;
    btn     : PRadioButton;
  end;

  {- dialog used to list data for the TListEdit control }
  PListEditDlg = ^TListEditDlg;
  TListEditDlg = object(TCenterDlg)
    title     : PChar;
    listHeader: PChar;
    list      : PCollection;
    grid      : PGrid;
    res       : PChar;
    maxLen    : integer;
    {}
    constructor Init(aParent: PWindowsObject; aList: PCollection; aTitle, aListHeader: PChar;
                     aResult: PChar; aMaxLen, cols: integer);
    procedure SetupWindow; virtual;
    function CanClose: boolean; virtual;
    procedure WMCharToItem(var msg: TMessage);  virtual WM_FIRST + WM_CHARTOITEM;
    procedure WMDrawItem(var msg: TMessage);    virtual WM_FIRST + WM_DRAWITEM;
    procedure WMMeasureItem(var msg: TMessage); virtual WM_FIRST + WM_MEASUREITEM;
    procedure WMGrid(var msg: TMessage); virtual ID_FIRST + IDC_LISTEDITGRID;
  end;

{----------------------------------------------------[ TLStatic ] --}

constructor TLStatic.Init(AParent: PWindowsObject; AnID: Integer; ATitle: PChar;
                        x,y,w,h: Integer; ATextLen: Word;
                        bold: Boolean);
begin
  inherited Init(AParent, AnID, ATitle, x,y,w,h, ATextLen);
  isBold:= bold;
end;

constructor TLStatic.InitResource(AParent: PWindowsObject; resourceID, ATextLen: Word; bold: Boolean);
begin
  inherited InitResource(AParent, resourceID, ATextLen);
  isbold:= bold;
end;

procedure TLStatic.SetupWindow;
begin
  inherited SetupWindow;

  if not isBold then
    SendMessage(HWindow, WM_SETFONT, unBoldfont, 0);
end;

{----------------------------------------------------[ TLRadioButton ] --}

constructor TLRadioButton.Init(AParent: PWindowsObject; AnID: Integer; ATitle: PChar;
                               X,Y,W,H: Integer; AGroup: PGroupBox; bold: boolean);
begin
  inherited Init(aParent, anID, aTitle, x,y,w,h, aGroup);
  isBold:= bold;
end;

constructor TLRadioButton.InitResource(aParent: PWindowsObject; resourceID: word;
                                       bold: boolean);
begin
  inherited InitResource(aParent, resourceID);
  isbold:= bold;
end;

procedure TLRadioButton.SetupWindow;
begin
  inherited SetupWindow;

  if not isBold then
    SendMessage(HWindow, WM_SETFONT, unBoldfont, 0);
end;

{----------------------------------------------------[ TLCheckBox ] --}

constructor TLCheckBox.Init(AParent: PWindowsObject; AnID: integer;
                            aTitle: PChar; X,Y,W,H: integer;
                            aGroup: PGroupBox; bold: boolean);
begin
  inherited Init(aParent, anID, aTitle, x,y,w,h, aGroup);
  isBold:= bold;
end;

constructor TLCheckBox.InitResource(aParent: PWindowsObject; resourceID: word;
                                    bold: boolean);
begin
  inherited InitResource(aParent, resourceID);
  isbold:= bold;
end;

procedure TLCheckBox.SetupWindow;
begin
  inherited SetupWindow;

  if not isBold then
    SendMessage(HWindow, WM_SETFONT, unBoldfont, 0);
end;

{----------------------------------------------------------[ TRadioGroup ]--}

constructor TRadioGroup.Init(aParent: PWindowsObject; anID: word; aRetVal: longint;
                             bold: boolean);
begin
  inherited Init;

  btns:= New(PCollection, Init(20, 10));
  boldGrp:= bold;
  parent:= aParent;

  AddToGroup(anID, aRetVal);
end;

destructor TRadioGroup.Done;
begin
  inherited Done;
  MSDisposeObj(btns);  {- remove list of button information }
end;

procedure TRadioGroup.AddToGroup(anID: word; aRetVal: longint);
var
  grp   : PRadioGrpInfo;
begin
  grp:= New(PRadioGrpInfo, Init);
  grp^.retVal:= aRetVal;
  grp^.btn:= New(PLRadioButton, InitResource(parent, anID, boldGrp));
  btns^.Insert(grp);
end;

function TRadioGroup.SelectedVal: longint;
{- return the value of the selected radio button }

  function IsSelected(rg: PRadioGrpInfo): boolean; far;
  var
    k   : longint;
  begin
    IsSelected:= SendMessage(rg^.btn^.HWindow, BM_GETCHECK, 0, 0) = BF_CHECKED;
  end;

var
  b   : PRadioGrpInfo;
begin
  b:= btns^.FirstThat(@IsSelected);
  if b = nil then
    SelectedVal:= -1
  else
    SelectedVal:= b^.retVal;
end;

procedure TRadioGroup.SetSelected(aVal: longint);
{- check the radio button that has a value that matches aVal }
var
  b   : PRadioGrpInfo;

  procedure SetChecks(rg: PRadioGrpInfo); far;
  begin
    if rg = b then
      SendMessage(rg^.btn^.HWindow, BM_SETCHECK, BF_CHECKED, 0)
    else
      SendMessage(rg^.btn^.HWindow, BM_SETCHECK, BF_UNCHECKED, 0);
  end;

begin
  b:= FindButton(aVal);
  btns^.ForEach(@SetChecks);
end;

procedure TRadioGroup.Enable(aVal: longint; on: boolean);
{- enable/disable a radio button that has the specified value }
var
  b   : PRadioGrpInfo;
begin
  b:= FindButton(aVal);
  if b <> nil then
  begin
    {- if disabling the checked radio button, then uncheck it }
    if (not on) and (SelectedVal = aVal) then
      SendMessage(b^.btn^.HWindow, BM_SETCHECK, BF_UNCHECKED, 0);
    EnableWindow(b^.btn^.HWindow, on);
  end;
end;

procedure TRadioGroup.EnableGroup(on: boolean);
{- enable or disable the entire group of radio buttons }

  procedure EnableIt(rg: PRadioGrpInfo); far;
  begin
    EnableWindow(rg^.btn^.HWindow, on);
  end;

begin
  btns^.ForEach(@EnableIt);
end;

function TRadioGroup.IsGroupEnabled: boolean;
{- returns true if the entire group is enabled }

  function IsDisabled(rg: PRadioGrpInfo): boolean; far;
  begin
    IsDisabled:= not IsWindowEnabled(rg^.btn^.HWindow);
  end;
begin
  IsGroupEnabled:= (btns^.FirstThat(@IsDisabled) = nil);
end;

function TRadioGroup.IsEnabled(aVal: longint): boolean;
{- enable/disable a radio button that has the specified value }
var
  b   : PRadioGrpInfo;
begin
  b:= FindButton(aVal);
  if b <> nil then
    IsEnabled:= IsWindowEnabled(b^.btn^.HWindow)
  else
    IsEnabled:= false;
end;

function TRadioGroup.FindButton(aVal: longint): pointer;
{- find a radio button that has the specified value }

  function Matches(rg: PRadioGrpInfo): boolean; far;
  begin
    Matches:= rg^.retVal = aVal;
  end;

var
  b   : PRadioGrpInfo;
begin
  b:= btns^.FirstThat(@Matches);
  FindButton:= b;
end;

procedure Initialize;
var
  lf      : TLogFont;
begin
  MakeScreenFont(lf, false, false);
  unBoldfont:= CreateFontIndirect(lf);
end;

procedure ExitRoutine; far;
begin
  exitProc:= exitSave;
  if unBoldFont <> 0 then
    DeleteObject(unBoldFont);
end;

{-----------------------------------------------------------[ TListEditDlg ]--}

constructor TListEditDlg.Init(aParent: PWindowsObject; aList: PCollection; aTitle, aListHeader: PChar;
                              aResult: PChar; aMaxLen, cols: integer);
begin
  inherited Init(aParent, MakeIntResource(DLG_LISTEDIT));
  grid:= New(PGrid, InitResource(@self, IDC_LISTEDITGRID, cols, false));
  list:= aList;
  listHeader:= aListHeader;
  title:= aTitle;
  res:= aResult;
  maxLen:= aMaxLen;
end;

procedure TListEditDlg.SetupWindow;
var
  focRow    : integer;
  tRes      : PChar;
  k         : integer;

  function StripChar(dest, src: PChar): PChar;
  var
    ch  : PChar;
  begin
    StrCopy(dest, '');
    ch:= StrScan(src, #9);  {- find position of char in src }
    if ch <> nil then
    begin
      StrCopy(dest, ch+1);  {- copy remaining string to dest }
      ch^:= #0;
    end;
    StripChar:= ch;
  end;

  procedure SetData(item: PListEditItem); far;
  var
    row   : integer;
    pstr  : PChar;
    tstr, temp: PChar;
    size  : integer;
    ch    : PChar;
    col   : integer;
  begin
    size:= StrLen(item^.buf) + 1;
    GetMem(pstr, size);
    GetMem(tstr, size);
    GetMem(temp, size);

    row:= grid^.AddString('');
    ch:= item^.buf;
    col:= 0;
    StrCopy(tstr, item^.buf);
    while ch <> nil do
    begin
      ch:= StripChar(temp, tstr);
      grid^.SetData(col, row, tstr);
      TrimAll(pstr, tstr, ' ', size-1);
      if StrIComp(pstr, tres) = 0 then
        focRow:= row;
      StrCopy(tstr, temp);
      Inc(col);
    end;

    MSFreeMem(pstr, size);
    MSFreeMem(temp, size);
    MSFreeMem(tstr, size);
  end;

  procedure SetupListHeader;
  var
    ch    : PChar;
    tstr  : PChar;
    size  : integer;
    temp  : PChar;
    col   : integer;
  begin
    size:= StrLen(listHeader) + 1;
    GetMem(tstr, size);
    GetMem(temp, size);
    StrCopy(tstr, listHeader);
    ch:= tstr;
    col:= 0;
    while ch <> nil do
    begin
      ch:= StripChar(temp, tstr);
      grid^.SetHeader(col, tstr);
      Inc(col);
      StrCopy(tstr, temp);
    end;
    MSFreeMem(temp, size);
    MSFreeMem(tstr, size);
  end;

begin
  inherited SetupWindow;
  focRow:= -1;
  SetWindowText(hWindow, title);

  if listHeader = nil then
    grid^.EnableHeader(false)
  else
    SetupListHeader;

  GetMem(tRes, strlen(res)+1);
  TrimAll(tres, res, ' ', strlen(res));
  list^.ForEach(@SetData);
  MSFreeMem(tRes, strlen(res)+1);

  for k:= 0 to grid^.GetColumnCount - 2 do
  begin
    grid^.OptColumnWidth(k);
  end;
  grid^.StretchColumn(grid^.GetColumnCount - 1);

  grid^.SetSelIndex(focRow);
  EnableWindow(GetItemHandle(ID_OK), grid^.GetSelIndex >= 0)
end;

function TListEditDlg.CanClose: boolean;
begin
  if grid^.GetSelIndex >= 0 then
  begin
    grid^.GetData(0, grid^.GetSelIndex, res, maxLen);
    CanClose:= true;
  end
  else
    CanClose:= false;
end;

procedure TListEditDlg.WMGrid(var msg: TMessage);
begin
  if msg.lParamHI = LBN_DBLCLK then
    OK(msg)
  else if msg.lParamHI = LBN_SELCHANGE then
    EnableWindow(GetItemHandle(ID_OK), grid^.GetSelIndex >= 0)
  else
    DefWndProc(msg);
end;

procedure TListEditDlg.WMCharToItem(var msg: TMessage);
begin
  grid^.CharToItem(msg);
end;

procedure TListEditDlg.WMDrawItem(var msg: TMessage);
begin
  grid^.DrawItem(msg);
end;

procedure TListEditDlg.WMMeasureItem(var msg: TMessage);
begin
  grid^.MeasureItem(msg);
end;

{----------------------------------------------------------[ TListEditItem ]--}

constructor TListEditItem.Init(aString: PChar);
begin
  inherited Init;
  buf:= StrNew(aString);
end;

destructor TListEditItem.Done;
begin
  inherited Done;
  MSStrDispose(buf);
end;

{-------------------------------------------------------------[ TListEdit ]--}

constructor TListEdit.Init(aParent: PWindowsObject; anId: integer; aTitle: PChar;
                           X,Y,W,H: integer; aList: PCollection;
                           anItemHeader, aListTitle: PChar; aCaseSensitive: boolean;
                           aListKey: word);
var
  p1, p2  : array[0..100] of char;
begin
  if (aList = nil) or (aList^.count <= 0) then
  begin
    FatalError(SR(IDS_ERROR, p1, sizeof(p1)-1), SR(IDS_INVLISTEDITCOLL, p2, sizeof(p2)-1));
    Halt;
  end;
  Initialize(aList, anItemHeader, aListTitle, aCaseSensitive, aListKey);
  inherited Init(aParent, anID, aTitle, x,y,w,h, maxListLen, false);
end;

constructor TListEdit.InitResource(aParent: PWindowsObject; resourceID: word;
                                   aList: PCollection; anItemHeader, aListTitle: PChar;
                                   aCaseSensitive: boolean; aListKey: word);


begin
  Initialize(aList, anItemHeader, aListTitle, aCaseSensitive, aListKey);
  inherited InitResource(aParent, resourceID, maxListLen);
end;

destructor TListEdit.Done;
begin
  inherited Done;
  MSStrDispose(header);
  MSStrDispose(title);
end;

procedure TListEdit.Initialize(aList: PCollection; anItemHeader, aListTitle: PChar;
                     aCaseSensitive: boolean; aListKey: word);

  procedure CountCols;
  var
    k   : integer;
  begin
    cols:= 1;
    for k:= 0 to strlen(header) - 1 do
      if header[k] = #9 then
        Inc(cols);
  end;

begin
  title:= nil;
  header:= StrNew(anItemHeader);
  ChangeTitle(aListTitle);

  if header <> nil then
    CountCols
  else
    cols:= 1;

  maxListLen:= 0;
  ChangeList(aList);
  caseSens:= aCaseSensitive;
  listKey:= aListKey;
end;

procedure TListEdit.ChangeTitle(aListTitle: PChar);
begin
  MSStrDispose(title);
  title:= StrNew(aListTitle);
end;

procedure TListEdit.ChangeList(aList: PCollection);
{- change the list of data for the control }

  procedure CheckLen(item: PListEditItem); far;
  var
    ch    : PChar;
    tstr  : PChar;
  begin
    GetMem(tstr, Strlen(item^.buf)+1);
    StrCopy(tstr, item^.buf);
    ch:= StrScan(tstr, #9);
    if ch <> nil then
      ch^:= #0;
    if StrLen(tstr) >= maxListLen then
      maxListLen:= StrLen(tstr) + 1;
    MSFreeMem(tstr, StrLen(item^.buf)+1);
  end;

begin
  list:= aList;
  maxListLen:= 0;
  list^.ForEach(@CheckLen);
  if hWindow <> 0 then
  begin
    {- set new text length limit }
    SendMessage(hWindow, EM_LIMITTEXT, maxListLen, 0);
  end;
end;

procedure TListEdit.ListData;
var
  dlg     : PListEditDlg;
  pstr    : PChar;
  dataSel : boolean;
begin
  dataSel:= false;
  if list^.count > 0 then
  begin
    GetMem(pstr, maxListLen + 1);
    GetText(pstr, maxListLen);
    dlg:= New(PListEditDlg, Init(parent, list, title, header, pstr, maxListLen, cols));
    if application^.ExecDialog(dlg) = IDOK then
    begin
      SetText(pstr);
      dataSel:= true;
    end;
    MSFreeMem(pstr, maxListLen + 1);
  end;
  if dataSel then {- if data selected into edit control ... }
    DataSelected;
end;

procedure TListEdit.DataSelected;
{- this method is called when data has been selected from the list and placed
   into the edit control. Descendents can do what they will here. }
begin
  {- default does nothing }
end;

function TListEdit.IsValid: integer;
{- return 0 if valid, 1 if not }
var
  item  : PListEditItem;
  pstr  : PChar;
  p1    : PChar;

  function Matches(anItem: PListEditItem): boolean; far;
  var
    ch    : PChar;
  begin
    TrimAll(p1, anItem^.buf, ' ', maxListLen);
    ch:= StrScan(p1, #9);
    if ch <> nil then
      ch^:= #0;
    if caseSens then
      Matches:= StrComp(p1, pstr) = 0
    else
      Matches:= StrIComp(p1, pstr) = 0;
  end;

begin
  IsValid:= 0;
  if list^.count > 0 then
  begin
    {- if no list is specified then all entries are valid }
    GetMem(pstr, maxListLen + 1);
    GetMem(p1, maxListLen + 1);
    GetText(p1, maxListLen);
    TrimAll(pstr, p1, ' ', maxListLen);

    if StrLen(pstr) <> 0 then
    begin
      item:= list^.FirstThat(@Matches);
      if item = nil then
        IsValid:= 1
      else
        SetText(p1);
    end;
    MSFreeMem(pstr, maxListLen + 1);
    MSFreeMem(p1, maxListLen + 1);
  end;
end;

function TListEdit.Format: integer;
begin
  Format:= IsValid;
end;

procedure TListEdit.WMKeyDown(var msg: TMessage);
begin
  if msg.wParam = listKey then
    ListData
  else
    inherited WMKeyDown(msg);
end;

procedure TListEdit.WMRButtonDown(var msg: TMessage);
begin
  PostMessage(parent^.HWindow, WM_NEXTDLGCTL, HWindow, 1);
  ListData;
  msg.result:= 0;
end;

{----------------------------------------------------------[ TFilterEdit ]--}

constructor TFilterEdit.InitResource(AParent: PWindowsObject; ResourceID, ATextLen: word;
                                     filterChars: PChar);
begin
  inherited InitResource(aParent, resourceID, aTextLen);
  filter:= StrNew(filterChars);
  delPressed:= false;
end;

destructor TFilterEdit.Done;
begin
  inherited Done;
  MSStrDispose(filter);
end;

procedure TFilterEdit.WMChar(var msg: TMessage);
begin
  if not delPressed and
    ((msg.wParam = VK_BACK) or
     (StrScan(filter, char(msg.wParam)) <> nil)) then
    DefWndProc(msg)
  else if delPressed then
    DefWndProc(msg)
  else
  begin
    msg.wParam:= 0;
    msg.Result:= 0;
  end;
end;

procedure TFilterEdit.WMKeydown(var msg: TMessage);
begin
  if msg.wParam = VK_DELETE then
    delPressed:= true
  else
    delPressed:= false;
  inherited WMKeyDown(msg);
end;

BEGIN
  exitSave:= exitProc;
  exitProc:= @ExitRoutine;
  Initialize;
END.

{- rcf }
