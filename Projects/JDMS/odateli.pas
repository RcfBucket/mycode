{- This file is used by the OLD dbif unit. It is here only to support the old
   dbif and the convert program  (rcf) }
Unit oDateLI;

INTERFACE

Uses
	WinDos,
	TPDate;

Type
	DateOrderType = (
		DMY,
		MDY,
		YMD);

	DateDelimiterType = (
		Slash,
		Period,
		Hyphen,
		Comma);

Var
	DateDelimiter : DateDelimiterType;
	DateLeadingZeros : Boolean;
	DateFourDigitYear : Boolean;
	DateOrder : DateOrderType;

Procedure BreakDate(DateStr:String;Var Year,Month,Day: Integer);
Procedure BreakTime(TimeStr:String;Var Hour,Minute,Second: Integer);
Function CurrentDate:String;
Function CurrentTime:String;
Function ValidDate(DateStr:String):Boolean;
Function ValidTime(TimeStr:String):Boolean;
Function SubDateTime(Date1,Time1,Date2,Time2:String):LongInt;
Function MakeDateStr(year, month, day: Integer): String;

Implementation

Function MakeDateStr(year, month, day: Integer): String;
{- return a string in the correct format for the date specified }
Var
	YearStr	: String[10];
	MonthStr	: String[10];
	DayStr 	: String[10];
	Delimiter: String[1];
	k			: Integer;
Begin
	If Not DateFourDigitYear Then
		Year := Year Mod 100;

	If DateFourDigitYear Then
		Str(Year:4, YearStr)
	Else
		Str(year:2, YearStr);
	For k:= 1 To Length(yearStr) Do
		If yearStr[k] = ' ' Then yearStr[k]:= '0';

	Str(Month,MonthStr);
	Str(Day,DayStr);
	If DateLeadingZeros And (Length(MonthStr) = 1) Then
		MonthStr := '0' + MonthStr;
	If DateLeadingZeros And (Length(DayStr) = 1) Then
		DayStr := '0' + DayStr;

	Case DateDelimiter Of
		Slash : Delimiter := '/';
		Comma : Delimiter := ',';
		Period : Delimiter := '.';
		Hyphen : Delimiter := '-';
	End;
	MakeDateStr:= '';
	Case DateOrder Of
		DMY : MakeDateStr:= DayStr+Delimiter+MonthStr+Delimiter+YearStr;
		MDY : MakeDateStr:= MonthStr+Delimiter+DayStr+Delimiter+YearStr;
		YMD : MakeDateStr:= YearStr+Delimiter+MonthStr+Delimiter+DayStr;
	End;
End;


Procedure BreakDate(DateStr:String;Var Year,Month,Day: Integer);
Var
	TempStr : String;
	Code, Waste : Integer;
	L,M,R : Integer;
   TempLong : LongInt;
Begin
	TempStr := DateStr;
	Val(TempStr,TempLong,Code);
	If (TempLong > 10000) Or (TempLong < -10000) Or (Code > 5) Then
		Begin
			Day := -1;
         Month := -1;
         Year := -1;
       	Exit;
   	End;
	Val(TempStr,L,Code);
   If Code = 0 Then
     	Begin
        	Day := -1;
         Month := -1;
         Year := -1;
       	Exit;
		End;
	If Code = 1 Then
		L := 0
	Else
		Val(Copy(TempStr,1,Code-1),L,Waste);
	TempStr := Copy(TempStr,Code+1,255);
	Val(TempStr,TempLong,Code);
	If (TempLong > 10000) Or (TempLong < -10000) Or (Code > 5) Then
		Begin
			Day := -1;
         Month := -1;
         Year := -1;
       	Exit;
   	End;
	Val(TempStr,M,Code);
   If Code = 0 Then
     	Begin
        	Day := -1;
         Month := -1;
         Year := -1;
        	Exit;
      End;
	If Code = 1 Then
		M := 0
	Else
		Val(Copy(TempStr,1,Code-1),M,Waste);
	TempStr := Copy(TempStr,Code+1,255);
	Val(TempStr,TempLong,Code);
	If (TempLong > 10000) Or (TempLong < -10000) Or (Code > 5) Then
   	Begin
			Day := -1;
         Month := -1;
         Year := -1;
       	Exit;
   	End;
	Val(TempStr,R,Waste);
	Case DateOrder Of
		DMY :
			Begin
				Day := L;
				Month := M;
				Year := R
			End;
		MDY :
			Begin
				Month := L;
				Day := M;
				Year := R;
			End;
		YMD :
			Begin
				Year := L;
				Month := M;
				Day := R;
			End;
	End;
	If Year < 100 Then
		Inc(Year,1900);
End;

Procedure BreakTime;

Var
	TempStr : String;
	Code, Waste : Integer;
   TempLong : LongInt;

Begin
	TempStr := TimeStr;
	{Get the hour}
	Val(TempStr,TempLong,Code);
	If (TempLong > 10000) Or (TempLong < -10000) Or (Code > 4) Then
		Begin
			Hour := 0;
			Minute := 0;
			Second := 0;
      	Exit
      End;
	Val(TempStr,Hour,Code);
   If Code = 0 Then
   	Begin
       	Minute := 0;
         Second := 0;
         Exit
      End;
	If Code = 1 Then
		Hour := 0
	Else
		Val(Copy(TempStr,1,Code-1),Hour,Waste);
	TempStr := Copy(TempStr,Code+1,255);
   {Get the minute}
	Val(TempStr,TempLong,Code);
	If (TempLong > 10000) Or (TempLong < -10000) Or (Code > 4) Then
		Begin
			Hour := 0;
			Minute := 0;
			Second := 0;
      	Exit
      End;
	Val(TempStr,Minute,Code);
   If Code = 0 Then
     	Begin
         Second := 0;
         Exit
      End;
	If Code = 1 Then
		Minute := 0
	Else
		Val(Copy(TempStr,1,Code-1),Minute,Waste);
	TempStr := Copy(TempStr,Code+1,255);
	{Get the second}
	Val(TempStr,TempLong,Code);
	If (TempLong > 10000) Or (TempLong < -10000) Or (Code > 4) Then
		Begin
			Hour := 0;
			Minute := 0;
			Second := 0;
      	Exit
      End;
	Val(TempStr,Second,Waste);
End;


Function CurrentDate:String;
Var
	Year : Word;
	Month : Word;
	Day : Word;
	DOW : Word;
Begin
	GetDate(Year,Month,Day,DOW);
	CurrentDate:= MakeDateStr(year, month, day);
End;


Function CurrentTime:String;

Var
	Hour : Word;
	Minute : Word;
	Second : Word;
	HundSec : Word;
	HourStr : String[10];
	MinuteStr : String[10];
	SecondStr : String[10];

Begin
	GetTime(Hour,Minute,Second,HundSec);
	Str(Hour,HourStr);
	Str(Minute,MinuteStr);
	Str(Second,SecondStr);
	If Length(HourStr) = 1
		Then
			HourStr := '0' + HourStr;
	If Length(MinuteStr) = 1
		Then
			MinuteStr := '0' + MinuteStr;
	If Length(SecondStr) = 1
		Then
			SecondStr := '0' + SecondStr;
	CurrentTime := HourStr+':'+MinuteStr+':'+SecondStr;
End;


Function IsLeapYear(Year : Integer) : Boolean;

Begin
	IsLeapYear := (Year mod 4 = 0) and (Year mod 4000 <> 0) and
		((Year mod 100 <> 0) or (Year mod 400 = 0));
End;


Function DaysInMonth(Month, Year : Integer) : Integer;

Begin
	Case Month of
		1, 3, 5, 7, 8, 10, 12 :
			DaysInMonth := 31;
		4, 6, 9, 11 :
			DaysInMonth := 30;
		2 :
			DaysInMonth := 28+Ord(IsLeapYear(Year));
		Else
			DaysInMonth := 0;
	End;
End;


Function ValidDate;

Var
	Year, Month, Day : Integer;

Begin
	BreakDate(DateStr,Year,Month,Day);
	If (Day < 1) or (Year < MinYear) or (Year > MaxYear)
		Then
			ValidDate := False
		Else
			Case Month of
				1..12 : ValidDate := Day <= DaysInMonth(Month, Year);
				Else    ValidDate := False;
			End
End;


Function ValidTime(TimeStr:String):Boolean;

Var
	Hours, Minutes, Seconds : Integer;

Begin
	BreakTime(TimeStr,Hours,Minutes,Seconds);
	If (Hours < 0) or (Hours > 23) or
		(Minutes < 0) or (Minutes > 59) or
		(Seconds < 0) or (Seconds > 59)
		Then
			ValidTime := False
		Else
			ValidTime := True;
End;


Function SubDateTime;

Var
	Year1, Month1, Day1 : Integer;
	Year2, Month2, Day2 : Integer;
	Hour1, Minute1, Second1 : Integer;
	Hour2, Minute2, Second2 : Integer;
	JDate1, JDate2, JDateT : Word;
	JTime1, JTime2, JTimeT : LongInt;
	DateDif : Word;
	TimeDif : LongInt;
	DateDifInSecs : LongInt;
	TotDifInSecs : LongInt;
	TempStr : String;
	Code,Waste : Integer;

Begin
	{Turn Dates into Y, M, and D}
	BreakDate(Date1,Year1,Month1,Day1);
	BreakDate(Date2,Year2,Month2,Day2);
	{Turn Times into H, M, and S}
	BreakTime(Time1,Hour1,Minute1,Second1);
	BreakTime(Time2,Hour2,Minute2,Second2);
	{Turn Y, M, and D into julian}
	JDate1 := DMYToDate(Day1,Month1,Year1);
	JDate2 := DMYToDate(Day2,Month2,Year2);
	{Turn H, M, and S into seconds since midnight}
	JTime1 := HMSToTime(Hour1,Minute1,Second1);
	JTime2 := HMSToTime(Hour2,Minute2,Second2);
	{We want Date1, Time1 to be larger (more recent)}
	If (JDate1 < JDate2) Or
		((JDate1 = JDate2) And (JTime1 < JTime2))
		Then
			Begin
				JDateT := JDate1;
				JDate1 := JDate2;
				JDate2 := JDateT;
				JTimeT := JTime1;
				JTime1 := JTime2;
				JTime2 := JTimeT
			End;
	{If JTime2 is larger than JTime1, borrow a day from
	 JDate1 and add it (in seconds) to JTime1}
	If JTime1 < JTime2
		Then
			Begin
				Dec(JDate1);
				Inc(JTime1,86400);
			End;
	{Subtract Time}
	TimeDif := JTime1 - JTime2;
	{Subtract Dates}
	DateDif := JDate1 - JDate2;
	{Multiply by 86400 to get seconds}
	DateDifInSecs := DateDif * 86400;
	{Add difference between times}
	TotDifInSecs := DateDifInSecs+TimeDif;
	{If dates were too far apart (24,850) set result to MaxLongInt}
	If DateDif > 24850
		Then
			SubDateTime := MaxLongInt
		Else
			SubDateTime := TotDifInSecs
End;

END.
