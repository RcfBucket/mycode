{----------------------------------------------------------------------------}
{  Module Name  : DBERRORS.PAS                                               }
{  Programmer   : EJ                                                         }
{  Date Created : 11/02/94                                                   }
{                                                                            }
{  Purpose -                                                                 }
{  This module provides all constants and functions for handling             }
{  errors for the DBLib library.                                             }
{                                                                            }
{  Assumptions -                                                             }
{  Error constant ranges should not overlap other subsystems in the          }
{  application.                                                              }
{                                                                            }
{  Initialization -                                                          }
{  None.                                                                     }
{                                                                            }
{  Revision History -                                                        }
{                                                                            }
{  Version  Date      Who    Did What                                        }
{  -------  --------  ------ ----------------------------------------------- }
{  1.00     11/02/94  EJ     Initial release.                                }
{                                                                            }
{----------------------------------------------------------------------------}
unit dbErrors;

INTERFACE

uses
  MScan;

const
{  NOTE: }
{- errors in the ranges 1-199, 1000-1999 and 2000-2099 are BTRIEVE errors}
  dbErr_InvalidFieldName            = MOD_DB +  0;
  dbErr_InvalidFileName             = MOD_DB +  1;
  dbErr_InvalidFieldSize            = MOD_DB +  2;
  dbErr_MaxFieldsDefined            = MOD_DB +  3;
  dbErr_InvalidType                 = MOD_DB +  4;
  dbErr_InvalidFieldNumber          = MOD_DB +  5;
  dbErr_InvalidKeyNumber            = MOD_DB +  6;
  dbErr_InvalidAssocNumber          = MOD_DB +  7;
  dbErr_InvalidDelAssocNumber       = MOD_DB +  8;
  dbErr_InvalidAssoc                = MOD_DB +  9;
  dbErr_InvalidDelAssoc             = MOD_DB + 10;
  dbErr_MaxAssocsDefined            = MOD_DB + 11;
  dbErr_MaxDelAssocsDefined         = MOD_DB + 12;
  dbErr_MaxKeysDefined              = MOD_DB + 13;
  dbErr_MaxSegsDefined              = MOD_DB + 14;
  dbErr_NoFields                    = MOD_DB + 15;
  dbErr_NoKeys                      = MOD_DB + 16;
  dbErr_NoKeySegments               = MOD_DB + 17;
  dbErr_InvalidDefFile              = MOD_DB + 18;
  dbErr_FieldHasKeys                = MOD_DB + 19;
  dbErr_RecordTooSmall              = MOD_DB + 20;
  dbErr_MaxRecSizeExceeded          = MOD_DB + 21;
  dbErr_DefFileNotFound             = MOD_DB + 22;
{ dbErr_OpNotAllowedOnAutoInc       = MOD_DB + 23; }
  dbErr_InvalidDefVersion           = MOD_DB + 24; {- invalid def file version (incompatible) }
  dbErr_MaxKeyLenExceeded           = MOD_DB + 25; {- key length exceeded maximum key length }
  dbErr_CannotAllocateRecBuf        = MOD_DB + 26;
{ dbErr_NoKeysForAutoInc            = MOD_DB + 27; }
{ dbErr_OneAutoIncFldAllowed        = MOD_DB + 28; }
{ dbErr_AutoIncCantBeSeg            = MOD_DB + 29; }
{ dbErr_InvalidSegmentNum           = MOD_DB + 30; }
  dbErr_CannotOpenDefFile           = MOD_DB + 31;
  dbErr_CreatingDefFile             = MOD_DB + 32;
  dbErr_ReadingDefFile              = MOD_DB + 33;
  dbErr_WritingDefFile              = MOD_DB + 34;
  dbErr_InvalidRecBuf               = MOD_DB + 35;
  dbErr_IncompatibleRecStruct       = MOD_DB + 36;
  dbErr_CopyToFromSelf              = MOD_DB + 37;
  dbErr_CantDeleteDataFile          = MOD_DB + 38;
  dbErr_CantDeleteDefFile           = MOD_DB + 39;
  dbErr_AutoSeqNotAllowed           = MOD_DB + 40;
  dbErr_FieldNameExists             = MOD_DB + 41;
{ dbErr_FieldHasAssociations        = MOD_DB + 42; }
  dbErr_FileNameMismatch            = MOD_DB + 43;
  dbErr_InvalidFieldValue           = MOD_DB + 44;
  dbErr_IncompatibleObjects         = MOD_DB + 45;
  dbErr_InvalidParameter            = MOD_DB + 46;
  dbErr_InvalidOperation            = MOD_DB + 47;
  dbErr_CantDeleteRecord            = MOD_DB + 48;
  dbErr_CantRenameDataFile          = MOD_DB + 49;
  dbErr_CantRenameDefFile           = MOD_DB + 50;
  dbErr_OldBtrieveVersion           = MOD_DB + 64;

function dbErrorStr(dbErrNum: integer; eStr: PChar; maxLen: integer): PChar;

IMPLEMENTATION

uses
  DMSDebug,
  Strings;

function dbErrorStr(dbErrNum: integer; eStr: PChar; maxLen: integer): PChar;
var
  tstr: string;
  nstr: string[10];
begin
  case dbErrNum of
  0                           :  tstr:= '<No error>';
  dbErr_InvalidFieldName      :  tstr:= 'Invalid field name.';
  dbErr_InvalidFileName       :  tstr:= 'Invalid file name.';
  dbErr_InvalidFieldSize      :  tstr:= 'Invalid field size.';
  dbErr_MaxFieldsDefined      :  tstr:= 'Maximum number of fields defined.';
  dbErr_InvalidType           :  tstr:= 'Invalid field type.';
  dbErr_InvalidFieldNumber    :  tstr:= 'Invalid field number.';
  dbErr_InvalidKeyNumber      :  tstr:= 'Invalid key number.';
  dbErr_InvalidAssocNumber    :  tstr:= 'Invalid association number.';
  dbErr_InvalidDelAssocNumber :  tstr:= 'Invalid delete association number.';
  dbErr_InvalidAssoc          :  tstr:= 'Invalid association definition.';
  dbErr_InvalidDelAssoc       :  tstr:= 'Invalid delete association definition.';
  dbErr_MaxAssocsDefined      :  tstr:= 'Maximum number of associations defined.';
  dbErr_MaxDelAssocsDefined   :  tstr:= 'Maximum number of delete associations defined.';
  dbErr_MaxKeysDefined        :  tstr:= 'Maximum number of keys defined.';
  dbErr_MaxSegsDefined        :  tstr:= 'Maximum number of key segments defined.';
  dbErr_NoFields              :  tstr:= 'No fields are defined.';
  dbErr_NoKeys                :  tstr:= 'No keys are defined.';
  dbErr_NoKeySegments         :  tstr:= 'No key segments are defined for a key.';
  dbErr_InvalidDefFile        :  tstr:= 'Invalid definition file.';
  dbErr_FieldHasKeys          :  tstr:= 'Cannot delete a field with keys.';
  dbErr_RecordTooSmall        :  tstr:= 'Record size is too small.';
  dbErr_MaxRecSizeExceeded    :  tstr:= 'Maximum record size exceeded.';
  dbErr_DefFileNotFound       :  tstr:= 'Database definition file not found.';
{ dbErr_OpNotAllowedOnAutoInc :  tstr:= 'Operation not allowed on AutoInc fields.'; }
  dbErr_InvalidDefVersion     :  tstr:= 'Invalid definition file version.';
  dbErr_MaxKeyLenExceeded     :  tstr:= 'Maximum key length exceeded.';
  dbErr_CannotAllocateRecBuf  :  tstr:= 'Cannot allocate memory for record buffer.';
{ dbErr_NoKeysForAutoInc      :  tstr:= 'No more keys available for AutoInc field.';          }
{ dbErr_OneAutoIncFldAllowed  :  tstr:= 'Only one auto-inc field type allowed per record.';   }
{ dbErr_AutoIncCantBeSeg      :  tstr:= 'AutoInc fields cannot be part of a key segment.';    }
{ dbErr_InvalidSegmentNum     :  tstr:= 'Invalid segment number.';                            }
  dbErr_CannotOpenDefFile     :  tstr:= 'Cannot open the definition file.';
  dbErr_CreatingDefFile       :  tstr:= 'Cannot create the definition file.';
  dbErr_ReadingDefFile        :  tstr:= 'Error reading the definition file.';
  dbErr_WritingDefFile        :  tstr:= 'Error writing the defintion file.';
  dbErr_InvalidRecBuf         :  tstr:= 'Record buffer is invalid or not initialized.';
  dbErr_IncompatibleRecStruct :  tstr:= 'Record structures are incompatible.';
  dbErr_CopyToFromSelf        :  tstr:= 'Cannot copy record buffer over itself.';
  dbErr_CantDeleteDataFile    :  tstr:= 'Cannot delete the database file.';
  dbErr_CantDeleteDefFile     :  tstr:= 'Cannot delete the database definition file.';
  dbErr_AutoSeqNotAllowed     :  tstr:= 'Auto-Seq not defined. Operation not allowed.';
  dbErr_FieldNameExists       :  tstr:= 'Field already exists.';
{ dbErr_FieldHasAssociations  :  tstr:= 'Cannot delete a field that is part of an association.';  }
  dbErr_FileNameMismatch      :  tstr:= 'Internal file name does not match.';
  dbErr_InvalidFieldValue     :  tstr:= 'Invalid field value.';
  dbErr_IncompatibleObjects   :  tstr:= 'Incompatible objects.';
  dbErr_InvalidParameter      :  tstr:= 'Invalid parameter.';
  dbErr_InvalidOperation      :  tstr:= 'Invalid operation.';
  dbErr_CantDeleteRecord      :  tstr:= 'Cannot delete the database record.';
  dbErr_CantRenameDataFile    :  tstr:= 'Cannot rename the database file.';
  dbErr_CantRenameDefFile     :  tstr:= 'Cannot rename the database definition file.';
  else if (dbErrNum <= (2099 + MOD_BTRV)) and (dbErrNum >= MOD_BTRV) then
  begin
    Str(dbErrNum, nStr);
    tstr := 'BTRIEVE error: ' + nStr;
  end
  else
    tstr := 'Unknown error code';
  end;
  if Length(tstr) > maxLen-1 then
    tstr[0] := Chr(maxLen-1);
  StrPCopy(eStr, tstr);
  DBErrorStr := eStr;
end;

END.
