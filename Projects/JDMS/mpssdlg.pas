{----------------------------------------------------------------------------}
{  Module Name  : MPSSDLG.PAS                                                }
{  Programmer   : EJ                                                         }
{  Date Created : 04/04/95                                                   }
{                                                                            }
{  Purpose -                                                                 }
{  This module is used to access, display, and maintain the Nihongo          }
{  DMS MPSS database files (MPSSRULE.DBD and MPSSCOND.DBD).                  }
{                                                                            }
{  Assumptions -                                                             }
{  None.                                                                     }
{                                                                            }
{  Initialization -                                                          }
{  Retrieve string constants.                                                }
{                                                                            }
{  Revision History -                                                        }
{                                                                            }
{  Version  Date      Who    Did What                                        }
{  -------  --------  ------ ----------------------------------------------- }
{  1.00     04/04/95  EJ     Initial release                                 }
{                                                                            }
{----------------------------------------------------------------------------}

unit MPSSDlg;

INTERFACE

uses
  Objects,
  ODialogs,
  OWindows,
  WinTypes,
  DlgLib,
  GridLib,
  CtlLib,
  ListLib,
  UIFlds,
  DBFile,
  DBLib,
  MPSSObjs;

{$I MPSS.INC}

type
  PMPSSMainDialog = ^TMPSSMainDialog;
  TMPSSMainDialog = object(TCenterDlg)
    constructor Init(aParent: PWindowsObject; RulesPtr: PRules;
                     FilesPtr: PFiles; FieldsPtr: PFields;
                     DriveFile: PChar; ATitle: PChar);
    destructor Done; virtual;
    procedure SetupWindow; virtual;
    procedure RefreshListBox; virtual;
    procedure RuleList(var msg: TMessage); virtual id_First + IDC_RULELIST;
    procedure AddRule(var msg: TMessage); virtual id_First + IDC_ADD;
    procedure EditRule(var msg: TMessage); virtual id_First + IDC_EDIT;
    procedure DeleteRule(var msg: TMessage); virtual id_First + IDC_DELETE;
    function  CanClose: boolean; virtual;
    procedure EnableButtons; virtual;
    procedure DrawItem(var msg: TMessage); virtual wm_First + wm_DrawItem;
    procedure MeasureItem(var msg: TMessage); virtual wm_First + wm_MeasureItem;
    procedure CharToItem(var msg: TMessage); virtual wm_First + wm_CharToItem;
    {}
    private
    grid        : PGrid;
    ruleFH      : PDBFile;
    Rules       : PRules;
    Files       : PFiles;
    Fields      : PFields;
    DrivingFile : PChar;
    title       : PChar;
    ListBoxLine : array [0..50] of char;
    {}
    function  LoadRule: boolean; virtual;
  end;

  PMPSSDialog = ^TMPSSDialog;
  TMPSSDialog = object(TCenterDlg)
    constructor Init(aParent: PWindowsObject; RulesPtr: PRules;
                     FilesPtr: PFiles; FieldsPtr: PFields;
                     bEdit: boolean; dbFH: PDBFile);
    destructor Done; virtual;
    procedure SetupWindow; virtual;
    procedure RefreshListBox; virtual;
    procedure RuleList(var msg: TMessage); virtual id_First + IDC_RULELIST;
    procedure FieldList(var msg: TMessage); virtual id_First + IDC_FIELDLIST;
    procedure SaveRule(var msg: TMessage); virtual id_First + IDC_SAVE;
    procedure ClearRule(var msg: TMessage); virtual id_First + IDC_CLEAR;
    function  CanClose: boolean; virtual;
    procedure InsertLine(var msg: TMessage); virtual id_First + IDC_INSERT_LINE;
    procedure UpdateLine(var msg: TMessage); virtual id_First + IDC_UPDATE_LINE;
    procedure DeleteLine(var msg: TMessage); virtual id_First + IDC_DELETE_LINE;
    procedure EnableButtons; virtual;
    procedure DrawItem(var msg: TMessage); virtual wm_First + wm_DrawItem;
    procedure MeasureItem(var msg: TMessage); virtual wm_First + wm_MeasureItem;
    procedure CharToItem(var msg: TMessage); virtual wm_First + wm_CharToItem;
    {}
    private
    grid        : PGrid;
    fldGrid     : PGrid;
    lParen      : PLCheckBox;
    rParen      : PLCheckBox;
    relGrp      : PRadioGroup;
    opsGrp      : PRadioGroup;
    fEdit       : PUIFld;
    Rules       : PRules;
    Files       : PFiles;
    Fields      : PFields;
    ListBoxLine : array [0..255] of char;
    bEditing    : boolean;
    ruleFH      : PDBFile;
    mnmList     : PListObject;
    mnmListRec  : PMnemListRec;
    rec         : PDBRec;
    tstSubstFH  : PDBFile;
    tstList     : PCollection;
    pEBLStr     : PListEditItem;
    {}
    procedure UpdateCriteria; virtual;
    procedure UpdateValueField(val: PChar); virtual;
    procedure EnableOpsGrp(val: integer); virtual;
    function  CheckParens: boolean; virtual;
    function  CheckFields: boolean; virtual;
    function  ESBLCat(Index: Integer): boolean;
  end;

IMPLEMENTATION

{$R MPSS.RES}

uses
  WinProcs,
  Strings,
  MScan,
  APITools,
  DMSErr,
  DBIDs,
  DBTypes,
  DMSDebug;

const
  DESC_LEN   = 50;
  ID_LEN     = 8;

type
  PMPSSSaveDialog = ^TMPSSSaveDialog;
  TMPSSSaveDialog = object(TCenterDlg)
    Rules       : PRules;
    DrivingFile : PChar;
    ruleFH      : PDBFile;
    idEdit      : PUIFld;
    descEdit    : PUIFld;
    {}
    constructor Init(AParent: PWindowsObject; RulesPtr: PRules;
                     dbFH: PDBFile);
    destructor Done; virtual;
    procedure SetupWindow; virtual;
    function  CanClose: boolean; virtual;
  end;

var
  andStr : array [0..10] of char;
  orStr  : array [0..10] of char;


{----------------------------[ TMPSSMainDialog ]-----------------------------}
{----------------------------------------------------------------------------}
{  Initialize an MPSS Main Dialog object.                                    }
{----------------------------------------------------------------------------}
constructor TMPSSMainDialog.Init(aParent: PWindowsObject; RulesPtr: PRules;
                                 FilesPtr: PFiles; FieldsPtr: PFields;
                                 DriveFile: PChar; ATitle: PChar);
begin
  title := nil;
  ruleFH := nil;

  if not inherited Init(aParent, MakeIntResource(DLG_MPSSMAIN)) then
  begin
    ShowError(aParent, IDS_INITERR1, nil, 0, MOD_MPSS, 21);
    Fail;
  end;
  grid := New(PGrid, InitResource(@self, IDC_RULELIST, 2, FALSE));
  if grid = nil then
  begin
    ShowError(aParent, IDS_INITERR1, nil, 0, MOD_MPSS, 22);
    Fail;
  end;
  ruleFH := New(PDBFile, Init(DBMPSSRULEFile, '', dbOpenNormal));
  if ruleFH = nil then
  begin
    ShowError(aParent, IDS_INITERR1, nil, dbLastOpenError, MOD_MPSS, 23);
    Fail;
  end;
  Rules := RulesPtr;
  Files := FilesPtr;
  Fields := FieldsPtr;
  DrivingFile := DriveFile;
  title := StrNew(ATitle);
  if title = nil then
  begin
    ShowError(aParent, IDS_INITERR1, nil, 0, MOD_MPSS, 24);
    Fail;
  end;
end;

destructor TMPSSMainDialog.Done;
begin
  MSStrDispose(title);
  MSDisposeObj(ruleFH);
  inherited Done;
end;

procedure TMPSSMainDialog.SetupWindow;
begin
  inherited SetupWindow;
  SetWindowText(HWindow, title);
  SR(IDS_CODE, ListBoxLine, DESC_LEN);
  grid^.SetHeader(0, ListBoxLine);
  grid^.SetAvgCharWidth(0, StrLen(ListBoxLine)+2);
  SR(IDS_DESC, ListBoxLine, DESC_LEN);
  grid^.SetHeader(1, ListBoxLine);
  grid^.SetAvgCharWidth(1, StrLen(ListBoxLine)+2);
  grid^.StretchColumn(1);
  RefreshListBox;
end;

procedure TMPSSMainDialog.RefreshListBox;
var
  k           : integer;
  sel         : integer;
begin
  grid^.SetRedraw(FALSE);
  sel := grid^.GetSelIndex;
  if sel = -1 then
    sel := 0;
  grid^.ClearList;
  ruleFH^.dbr^.ClearRecord;
  ruleFH^.dbr^.PutFieldAsStr(DBMPSSRULEFName, DrivingFile);
  if ruleFH^.dbc^.GetFirstContains(ruleFH^.dbr) then
    repeat
      ruleFH^.dbr^.GetFieldAsStr(DBMPSSRULEID, ListBoxLine, ID_LEN);
      k := grid^.AddString(ListBoxLine);
      grid^.SetData(0, k, ListBoxLine);
      ruleFH^.dbr^.GetFieldAsStr(DBMPSSRULEDesc, ListBoxLine, DESC_LEN);
      grid^.SetData(1, k, ListBoxLine);
      grid^.SetItemData(k, ruleFH^.dbc^.GetPosition);
    until not ruleFH^.dbc^.GetNextContains(ruleFH^.dbr);
  grid^.SetRedraw(TRUE);
  grid^.SetSelIndex(sel);
  EnableButtons;
end;

procedure TMPSSMainDialog.RuleList(var msg: TMessage);
begin
  if msg.lParamHi = LBN_SELCHANGE then
    EnableButtons
  else
    DefWndProc(msg);
end;

procedure TMPSSMainDialog.AddRule(var msg: TMessage);
var
  MPSSDialog : PMPSSDialog;
begin
  Rules^[0].FH := nil;
  ruleFH^.dbr^.ClearRecord;
  ruleFH^.dbr^.PutFieldAsStr(DBMPSSRULEFName, DrivingFile);
  MPSSDialog := New(PMPSSDialog, Init(@self, Rules, Files, Fields, FALSE, ruleFH));
  if Application^.ExecDialog(MPSSDialog) = IDOK then
    EndDlg(IDOK)
  else
    RefreshListBox;
end;

procedure TMPSSMainDialog.EditRule(var msg: TMessage);
var
  MPSSDialog : PMPSSDialog;
  sel        : integer;
begin
  sel := grid^.GetSelIndex;
  if sel = lb_Err then
  begin
    MessageBeep(MB_ICONEXCLAMATION);
    Exit;
  end;
  if not ruleFH^.dbc^.GotoPosition(ruleFH^.dbr, grid^.GetItemData(sel)) then
  begin
    MessageBeep(MB_ICONEXCLAMATION);
    Exit;
  end;
  LoadRule;
  MPSSDialog := New(PMPSSDialog, Init(@self, Rules, Files, Fields, TRUE, ruleFH));
  if Application^.ExecDialog(MPSSDialog) = IDOK then
    EndDlg(IDOK)
  else
    RefreshListBox;
end;

procedure TMPSSMainDialog.DeleteRule(var msg: TMessage);
var
  sel     : integer;
  pstr    : PChar;
begin
  sel := grid^.GetSelIndex;
  if sel = lb_Err then
  begin
    MessageBeep(MB_ICONEXCLAMATION);
    Exit;
  end;
  GetMem(pstr, 256);
  if pstr = nil then
  begin
    ShowError(@self, IDS_INITERR1, nil, 0, MOD_MPSS, 25);
    Exit;
  end;
  SR(IDS_DELETE_RULE, pstr, 80);
  StrLCat(pstr, #10#10, 255);
  grid^.GetData(1, sel, ListBoxLine, DESC_LEN + 1);
  StrLCat(pstr, ListBoxLine, 255);
  if YesNoMsg(HWindow, title, pstr) then
  begin
    if ruleFH^.dbc^.GotoPosition(ruleFH^.dbr, grid^.GetItemData(sel)) then
      ruleFH^.dbc^.DeleteRec(ruleFH^.dbr);
    if ruleFH^.dbc^.dbErrorNum <> 0 then
      ShowError(@self, IDS_CANNOT_DELETE, nil, ruleFH^.dbc^.dbErrorNum, MOD_MPSS, 0);
    RefreshListBox;
  end;
  MSFreeMem(pstr, 256);
end;

function TMPSSMainDialog.CanClose: boolean;
var
  sel          : integer;
begin
  CanClose := FALSE;
  { Get the user input }
  sel := grid^.GetSelIndex;
  if sel = lb_Err then
  begin
    MessageBeep(MB_ICONEXCLAMATION);
    Exit;
  end;
  { Load the rules }
  if not ruleFH^.dbc^.GotoPosition(ruleFH^.dbr, grid^.GetItemData(sel)) then
  begin
    MessageBeep(MB_ICONEXCLAMATION);
    Exit;
  end;
  if not LoadRule then
  begin
    ShowError(@self, IDS_INVALID_RULE, nil, 0, 0, sel + 1);
    Exit;
  end;

  CanClose := TRUE;
end;

procedure TMPSSMainDialog.EnableButtons;
var
  bEnable      : boolean;
begin
  bEnable := (grid^.GetSelIndex <> lb_Err);
  EnableWindow(GetItemHandle(IDOK), bEnable);
  EnableWindow(GetItemHandle(IDC_EDIT), bEnable);
  EnableWindow(GetItemHandle(IDC_DELETE), bEnable);
end;

procedure TMPSSMainDialog.DrawItem(var msg: TMessage);
begin
  grid^.DrawItem(msg);
end;

procedure TMPSSMainDialog.MeasureItem(var msg: TMessage);
begin
  grid^.MeasureItem(msg);
end;

procedure TMPSSMainDialog.CharToItem(var msg: TMessage);
begin
  grid^.CharToItem(msg);
end;

function TMPSSMainDialog.LoadRule: boolean;
var
  condFH       : PDBAssocFile;
  ruleSeq      : TSeqNum;
  fileCounter  : word;
  fieldCounter : word;
  ruleCounter  : word;
  finished     : boolean;
  fileName     : FNString;
begin
  LoadRule := TRUE;
  condFH := New(PDBAssocFile, Init(ruleFH^.dbr, DBMPSSRULE_MPSSCOND_ASSOC, dbOpenNormal));
  if condFH = nil then
  begin
    ShowError(@self, IDS_INITERR1, nil, dbLastOpenError, MOD_MPSS, 26);
    Exit;
  end;
  ruleCounter := 0;
  finished := not condFH^.dbc^.GetFirst(condFH^.dbr);
  while (not finished) and (ruleCounter < MaxMPSSRules) do
  begin
    condFH^.dbr^.GetField(DBMPSSCONDAndOr, @Rules^[ruleCounter].AndOr, 1);
    condFH^.dbr^.GetField(DBMPSSCONDLParen, @Rules^[ruleCounter].LParen, 1);
    condFH^.dbr^.GetField(DBMPSSCONDRParen, @Rules^[ruleCounter].RParen, 1);
    condFH^.dbr^.GetField(DBMPSSCONDFileName, @fileName, 9);
    fileCounter := 1;
    while StrIComp(Files^[fileCounter].FileName, fileName) <> 0 do
    begin
      Inc(fileCounter);
      if Files^[fileCounter].FH = nil then
      begin
        LoadRule := FALSE;
        Break;
      end;
    end;
    Rules^[ruleCounter].FH := Files^[fileCounter].FH;
    { load file descs now }
    if Files^[fileCounter].FH <> nil then
      Files^[fileCounter].FH^.dbd^.FileDesc(Rules^[ruleCounter].FileDesc, DESC_LEN)
    else
      SR(IDS_BAD_FILE, Rules^[ruleCounter].FileDesc, DESC_LEN);

    condFH^.dbr^.GetField(DBMPSSCONDFieldName, @Rules^[ruleCounter].FieldName, 33);

    { Setup the fieldno for rule }
    fieldCounter := 1;
    while (Fields^[fieldCounter].FH <> Rules^[ruleCounter].FH) or
          (StrIComp(Fields^[fieldCounter].FieldName, Rules^[ruleCounter].FieldName) <> 0) do
    begin
      Inc(fieldCounter);
      if Fields^[fieldCounter].FH = nil then
      begin
        LoadRule := FALSE;
        Break;
      end;
    end;
    Rules^[ruleCounter].FieldNo := Fields^[fieldCounter].FieldNo;
    Rules^[ruleCounter].FieldType := Fields^[fieldCounter].FieldType;
    Rules^[ruleCounter].TestRef := Fields^[fieldCounter].TestRef;

    condFH^.dbr^.GetField(DBMPSSCONDOperator, @Rules^[ruleCounter].Operator, 1);
    condFH^.dbr^.GetFieldAsStr(DBMPSSCONDValue, Rules^[ruleCounter].Value, 255);
    case Rules^[ruleCounter].FieldType of
      dbInteger, dbLongInt, dbWord, dbBoolean, dbByte, dbShortInt,
      dbChar, dbDate, dbTime, dbSeqRef, dbAutoInc :
      begin
        Rules^[ruleCounter].lValue := 0;
        condFH^.dbr^.GetField(DBMPSSCONDBlock, @Rules^[ruleCounter].lValue, SizeOf(Rules^[ruleCounter].lValue));
      end;
      dbSingle :
      begin
        Rules^[ruleCounter].sValue := 0.0;
        condFH^.dbr^.GetField(DBMPSSCONDBlock, @Rules^[ruleCounter].sValue, SizeOf(Rules^[ruleCounter].sValue));
      end;
      dbDouble :
      begin
        Rules^[ruleCounter].dValue := 0.0;
        condFH^.dbr^.GetField(DBMPSSCONDBlock, @Rules^[ruleCounter].dValue, SizeOf(Rules^[ruleCounter].dValue));
      end;
      {- else   dbString, dbCode, dbZString, dbZCode, dbBlock, }
    end;
    finished := not condFH^.dbc^.GetNext(condFH^.dbr);
    Inc(ruleCounter)
  end;
  Rules^[ruleCounter].FH := nil;
  MSDisposeObj(condFH);
end;


constructor TMPSSDialog.Init(aParent: PWindowsObject; RulesPtr: PRules;
                             FilesPtr: PFiles; FieldsPtr: PFields;
                             bEdit: boolean; dbFH: PDBFile);
var
  c   : PControl;
begin
  tstList := nil;
  tstSubstFH := nil;
  fEdit := nil;
  mnmList := nil;
  mnmListRec := nil;
  relGrp := nil;
  opsGrp := nil;

  if not inherited Init(aParent, MakeIntResource(DLG_DEFRULE)) then
  begin
    ShowError(aParent, IDS_INITERR1, nil, 0, MOD_MPSS, 27);
    Fail;
  end;
  c := New(PLStatic, InitResource(@self, IDC_LBL1, 0, TRUE));
  c := New(PLStatic, InitResource(@self, IDC_LBL2, 0, FALSE));
  lParen := New(PLCheckBox, InitResource(@self, IDC_LPAREN, TRUE));
  rParen := New(PLCheckBox, InitResource(@self, IDC_RPAREN, TRUE));
  grid := New(PGrid, InitResource(@self, IDC_RULELIST, 6, FALSE));
  if grid = nil then
  begin
    ShowError(aParent, IDS_INITERR1, nil, 0, MOD_MPSS, 28);
    Fail;
  end;
  fldGrid := New(PGrid, InitResource(@self, IDC_FIELDLIST, 1, FALSE));
  if fldGrid = nil then
  begin
    ShowError(aParent, IDS_INITERR1, nil, 0, MOD_MPSS, 29);
    Fail;
  end;
  relGrp := New(PRadioGroup, Init(@self, IDC_AND, longint(MPSSAnd), FALSE));
  relGrp^.AddToGroup(IDC_OR, longint(MPSSOr));
  opsGrp := New(PRadioGroup, Init(@self, IDC_EQ, longint(MPSSEQ), FALSE));
  opsGrp^.AddToGroup(IDC_NE, longint(MPSSNE));
  opsGrp^.AddToGroup(IDC_GT, longint(MPSSGT));
  opsGrp^.AddToGroup(IDC_LT, longint(MPSSLT));
  opsGrp^.AddToGroup(IDC_GE, longint(MPSSGE));
  opsGrp^.AddToGroup(IDC_LE, longint(MPSSLE));
  opsGrp^.AddToGroup(IDC_IN, longint(MPSSIN));
  mnmList := New(PListObject, Init);
  mnmListRec := New(PMnemListRec, Init);
  if (mnmList = nil) or (mnmListRec = nil) then
  begin
    ShowError(aParent, IDS_INITERR1, nil, 0, MOD_MPSS, 30);
    Fail;
  end;
  Rules := RulesPtr;
  Files := FilesPtr;
  Fields := FieldsPtr;
  bEditing := bEdit;
  ruleFH := dbFH;
  rec := nil;

  tstSubstFH := New(PDBFile, Init(DBTSTSUBFile, '', dbOpenNormal));
  if tstSubstFH = nil then
  begin
    ShowError(aParent, IDS_INITERR1, nil, dbLastOpenError, MOD_MPSS, 31);
    Fail;
  end;

  {- build possible test values list }
  tstList := New(PCollection, Init(2,2));
  tstList^.Insert(New(PListEditItem, Init('+')));
  tstList^.Insert(New(PListEditItem, Init('-')));
  pEBLStr:=New(PListEditItem, Init('?'));
end;

destructor TMPSSDialog.Done;
begin
  if tstList^.IndexOf(pEBLStr)=-1
     then MSDisposeObj(pEBLStr);

  MSDisposeObj(tstList);
  MSDisposeObj(tstSubstFH);
  MSDisposeObj(fEdit);
  MSDisposeObj(mnmListRec);
  MSDisposeObj(mnmList);
  MSDisposeObj(relGrp);
  MSDisposeObj(opsGrp);
  inherited Done;
end;

procedure TMPSSDialog.SetupWindow;
var
  fieldCounter : word;
  fldName      : array [0..32] of char;
  k            : integer;
begin
  inherited SetupWindow;
  if bEditing then
    SR(IDS_EDIT_RULE, ListBoxLine, 255)
  else
    SR(IDS_ADD_RULE, ListBoxLine, 255);
  SetWindowText(HWindow, ListBoxLine);
  grid^.SetHeader(0, 'Rel');
  grid^.SetAlignment(1, Grid_Center);
  grid^.SetHeader(1, '(');
  grid^.SetHeader(2, 'Fieldname');
  grid^.SetAlignment(3, Grid_Center);
  grid^.SetHeader(3, 'Cnd');
  grid^.SetHeader(4, 'Value');
  grid^.SetHeader(5, ' )');
  grid^.EnableHeader(FALSE);
  grid^.SetAvgCharWidth(0, 5);
  grid^.SetAvgCharWidth(1, 2);
  grid^.SetAvgCharWidth(2, 25);
  grid^.SetAvgCharWidth(3, 4);
  grid^.SetAvgCharWidth(4, 28);
  grid^.SetAvgCharWidth(5, 4);
  grid^.StretchColumn(2);
  RefreshListBox;
  { fill fields list }
  SR(IDS_FIELDNAME, ListBoxLine, DESC_LEN);
  fldGrid^.SetHeader(0, ListBoxLine);
  fldGrid^.ClearList;
  fldGrid^.StretchColumn(0);
  fieldCounter := 1;
  while Fields^[fieldCounter].FH <> nil do
  begin
    StrLCopy(ListBoxLine, Fields^[fieldCounter].FileDesc, 255);
    StrLCat(ListBoxLine, '.', 255);
    StrLCopy(fldName, Fields^[fieldCounter].FieldName, 32);
    StrLCat(ListBoxLine, fldName, 255);
    k := fldGrid^.AddString('');
    fldGrid^.SetData(0, k, ListBoxLine);
    fldGrid^.SetItemData(k, longint(Fields^[fieldCounter].OpsType));
    Inc(fieldCounter);
  end;
  UpdateCriteria;
end;

procedure TMPSSDialog.RefreshListBox;
var
  ruleCounter  : word;
  andOrStr     : array [0..10] of char;
  operatorStr  : array [0..10] of char;
  sel          : integer;
  k            : integer;
begin
  grid^.SetRedraw(FALSE);
  sel := grid^.GetSelIndex;
  grid^.ClearList;
  ruleCounter := 0;
  while Rules^[ruleCounter].FH <> nil  do
  begin
    if ruleCounter = 0 then
      StrCopy(andOrStr, '')
    else if Rules^[ruleCounter].AndOr = MPSSAnd then
      StrCopy(andOrStr, andStr)
    else
      StrCopy(andOrStr, orStr);
    k := grid^.AddString('');
    grid^.SetData(0, k, andOrStr);
    grid^.SetItemData(k, longint(Rules^[ruleCounter].Operator));
    case Rules^[ruleCounter].Operator of
      MPSSEQ : StrCopy(operatorStr, '=');
      MPSSNE : StrCopy(operatorStr, '<>');
      MPSSLT : StrCopy(operatorStr, '<');
      MPSSGT : StrCopy(operatorStr, '>');
      MPSSLE : StrCopy(operatorStr, '<=');
      MPSSGE : StrCopy(operatorStr, '>=');
      MPSSIN : StrCopy(operatorStr, '@');
    end;
    if Rules^[ruleCounter].LParen then
      grid^.SetData(1, k, '(')
    else
      grid^.SetData(1, k, '');
    StrLCopy(ListBoxLine, Rules^[ruleCounter].FileDesc, 255);
    StrLCat(ListBoxLine, '.', 255);
    StrLCat(ListBoxLine, Rules^[ruleCounter].FieldName, 255);
    grid^.SetData(2, k, ListBoxLine);
    grid^.SetData(3, k, operatorStr);
    grid^.SetData(4, k, Rules^[ruleCounter].Value);
    if Rules^[ruleCounter].RParen then
      grid^.SetData(5, k, ' )')
    else
      grid^.SetData(5, k, '');
    Inc(ruleCounter);
  end;
  grid^.SetRedraw(TRUE);
  grid^.SetSelIndex(sel);
end;

procedure TMPSSDialog.RuleList(var msg: TMessage);
begin
  if msg.lParamHi = LBN_SELCHANGE then
  begin
    UpdateCriteria;
    EnableButtons;
  end
  else
    DefWndProc(msg);
end;

procedure TMPSSDialog.FieldList(var msg: TMessage);
begin
  if msg.lParamHi = LBN_SELCHANGE then
  begin
    lParen^.SetCheck(0);
    rParen^.SetCheck(0);
    EnableOpsGrp(fldGrid^.GetItemData(fldGrid^.GetSelIndex));
    UpdateValueField(nil);
    EnableButtons;
  end
  else
    DefWndProc(msg);
end;

procedure TMPSSDialog.SaveRule(var msg: TMessage);
var
  MPSSSave    : PMPSSSaveDialog;
begin
  if CheckParens and CheckFields then
  begin
    MPSSSave := New(PMPSSSaveDialog, Init(@self, Rules, ruleFH));
    if Application^.ExecDialog(MPSSSave) = IDOK then
    begin
      if not bEditing then
      begin
        bEditing := TRUE;
        SR(IDS_EDIT_RULE, ListBoxLine, 255);
        SetWindowText(HWindow, ListBoxLine);
      end;
      (* if a rule name field is added then update the rule name *)
    end;
  end;
end;

procedure TMPSSDialog.ClearRule(var msg: TMessage);
begin
  Rules^[0].FH := nil;
  RefreshListBox;
  UpdateCriteria;
  EnableButtons;
end;

function  TMPSSDialog.CanClose: boolean;
begin
  CanClose := CheckParens and CheckFields;
end;

procedure TMPSSDialog.InsertLine(var msg: TMessage);
var
  sel          : integer;
  k            : integer;
  err          : integer;
  fileName     : FNString;
  fi           : TFldInfo;
  ai           : TAssocInfo;
begin
  if fEdit = nil then
  begin
    MessageBeep(MB_ICONEXCLAMATION);
    Exit;
  end;

  sel := grid^.GetCount;
  if sel >= (MaxMPSSRules - 1) then
  begin
    MessageBeep(MB_ICONEXCLAMATION);
    Exit;
  end;
  k := fldGrid^.GetSelIndex;

  err := fEdit^.Format;
  if err <> 0 then
  begin
    ShowUIFldError(@self, err, fEdit);
    Exit;
  end;

  Inc(k);
  Rules^[sel].AndOr := TMPSSRel(relGrp^.SelectedVal);
  Rules^[sel].LParen := lParen^.GetCheck = 1;
  Rules^[sel].RParen := rParen^.GetCheck = 1;
  Rules^[sel].FH := Fields^[k].FH;
  StrLCopy(Rules^[sel].FileDesc, Fields^[k].FileDesc, SizeOf(Rules^[sel].FileDesc));
  StrLCopy(Rules^[sel].FieldName, Fields^[k].FieldName, SizeOf(Rules^[sel].FieldName));
  Rules^[sel].FieldNo := Fields^[k].FieldNo;
  Rules^[sel].FieldType := Fields^[k].FieldType;
  Rules^[sel].TestRef := Fields^[k].TestRef;
  Rules^[sel].Operator := TMPSSOp(opsGrp^.SelectedVal);
  StrCopy(Rules^[sel].Value, '');
  fEdit^.XferCtlToRec(rec);
  rec^.GetFieldAsStr(Rules^[sel].FieldNo, Rules^[sel].Value, 255);
  case Rules^[sel].FieldType of
    dbInteger, dbLongInt, dbWord, dbBoolean, dbByte, dbShortInt,
    dbChar, dbDate, dbTime, dbSeqRef, dbAutoInc :
    begin
      Rules^[sel].lValue := 0;
      rec^.GetField(Rules^[sel].FieldNo, @Rules^[sel].lValue, SizeOf(Rules^[sel].lValue));
      if Rules^[sel].FieldType = dbSeqRef then
      begin
        if Rules^[sel].FH^.dbd^.FieldInfo(Rules^[sel].FieldNo, fi) and
           Rules^[sel].FH^.dbd^.AssocInfo(fi.fldAssoc, ai) and
           mnmList^.FindMnemSeq(ai.fName, mnmListRec, Rules^[sel].lValue) then
          StrLCopy(Rules^[sel].Value, mnmListRec^.ID, 255)
        else
          StrCopy(Rules^[sel].Value, '');
      end;
    end;
    dbSingle :
    begin
      Rules^[sel].sValue := 0.0;
      rec^.GetField(Rules^[sel].FieldNo, @Rules^[sel].sValue, SizeOf(Rules^[sel].sValue));
    end;
    dbDouble :
    begin
      Rules^[sel].dValue := 0.0;
      rec^.GetField(Rules^[sel].FieldNo, @Rules^[sel].dValue, SizeOf(Rules^[sel].dValue));
    end;
    {- else   dbString, dbCode, dbZString, dbZCode, dbBlock, }
  end;
  Rules^[sel + 1].FH := nil;
  RefreshListBox;
  grid^.SetSelIndex(sel);
  UpdateCriteria;
  EnableButtons;
end;

procedure TMPSSDialog.UpdateLine(var msg: TMessage);
var
  sel          : integer;
  k            : integer;
  err          : integer;
  fileName     : FNString;
  fi           : TFldInfo;
  ai           : TAssocInfo;
begin
  sel := grid^.GetSelIndex;
  k := fldGrid^.GetSelIndex;
  if (sel = lb_Err) or (k = lb_Err) or (fEdit = nil) then
  begin
    MessageBeep(MB_ICONEXCLAMATION);
    Exit;
  end;

  err := fEdit^.Format;
  if err <> 0 then
  begin
    ShowUIFldError(@self, err, fEdit);
    Exit;
  end;

  Inc(k);
  Rules^[sel].AndOr := TMPSSRel(relGrp^.SelectedVal);
  Rules^[sel].LParen := lParen^.GetCheck = 1;
  Rules^[sel].RParen := rParen^.GetCheck = 1;
  Rules^[sel].FH := Fields^[k].FH;
  StrLCopy(Rules^[sel].FileDesc, Fields^[k].FileDesc, SizeOf(Rules^[sel].FileDesc));
  StrLCopy(Rules^[sel].FieldName, Fields^[k].FieldName, SizeOf(Rules^[sel].FieldName));
  Rules^[sel].FieldNo := Fields^[k].FieldNo;
  Rules^[sel].FieldType := Fields^[k].FieldType;
  Rules^[sel].TestRef := Fields^[k].TestRef;
  Rules^[sel].Operator := TMPSSOp(opsGrp^.SelectedVal);
  StrCopy(Rules^[sel].Value, '');
  fEdit^.XferCtlToRec(rec);
  rec^.GetFieldAsStr(Rules^[sel].FieldNo, Rules^[sel].Value, 255);
  case Rules^[sel].FieldType of
    dbInteger, dbLongInt, dbWord, dbBoolean, dbByte, dbShortInt,
    dbChar, dbDate, dbTime, dbSeqRef, dbAutoInc :
    begin
      Rules^[sel].lValue := 0;
      rec^.GetField(Rules^[sel].FieldNo, @Rules^[sel].lValue, SizeOf(Rules^[sel].lValue));
      if Rules^[sel].FieldType = dbSeqRef then
      begin
        if Rules^[sel].FH^.dbd^.FieldInfo(Rules^[sel].FieldNo, fi) and
           Rules^[sel].FH^.dbd^.AssocInfo(fi.fldAssoc, ai) and
           mnmList^.FindMnemSeq(ai.fName, mnmListRec, Rules^[sel].lValue) then
          StrLCopy(Rules^[sel].Value, mnmListRec^.ID, 255)
        else
          StrCopy(Rules^[sel].Value, '');
      end;
    end;
    dbSingle :
    begin
      Rules^[sel].sValue := 0.0;
      rec^.GetField(Rules^[sel].FieldNo, @Rules^[sel].sValue, SizeOf(Rules^[sel].sValue));
    end;
    dbDouble :
    begin
      Rules^[sel].dValue := 0.0;
      rec^.GetField(Rules^[sel].FieldNo, @Rules^[sel].dValue, SizeOf(Rules^[sel].dValue));
    end;
    {- else   dbString, dbCode, dbZString, dbZCode, dbBlock, }
  end;
  RefreshListBox;
  UpdateCriteria;
end;

procedure TMPSSDialog.DeleteLine(var msg: TMessage);
var
  sel          : integer;
begin
  sel := grid^.GetSelIndex;
  if sel <> lb_Err then
  begin
    repeat
      Rules^[sel] := Rules^[sel + 1];
      Inc(sel);
    until Rules^[sel - 1].FH  =  nil;
    RefreshListBox;
    if grid^.GetSelIndex = lb_Err then
      grid^.SetSelIndex(grid^.GetCount - 1);
    UpdateCriteria;
    EnableButtons;
  end;
end;

procedure TMPSSDialog.EnableButtons;
var
  bEnable      : boolean;
  lastFoc      : HWnd;
begin
  lastFoc := GetFocus;   {- save the current focus }
  bEnable := (grid^.GetCount > 0);
  EnableWindow(GetItemHandle(IDOK), bEnable);
  EnableWindow(GetItemHandle(IDC_SAVE), bEnable);
  EnableWindow(GetItemHandle(IDC_CLEAR), bEnable);
  bEnable := (grid^.GetSelIndex <> lb_Err);
  EnableWindow(GetItemHandle(IDC_DELETE_LINE), bEnable);
  EnableWindow(GetItemHandle(IDC_UPDATE_LINE), bEnable);
  bEnable := fldGrid^.GetSelIndex <> lb_Err;
  EnableWindow(GetItemHandle(IDC_INSERT_LINE), bEnable);
  if not IsWindowEnabled(lastFoc) then
    FocusCtl(HWindow, IDC_RULELIST);
end;

procedure TMPSSDialog.DrawItem(var msg: TMessage);
begin
  grid^.DrawItem(msg);
  fldGrid^.DrawItem(msg);
end;

procedure TMPSSDialog.MeasureItem(var msg: TMessage);
begin
  grid^.MeasureItem(msg);
  fldGrid^.MeasureItem(msg);
end;

procedure TMPSSDialog.CharToItem(var msg: TMessage);
begin
  grid^.CharToItem(msg);
  fldGrid^.CharToItem(msg);
end;

procedure TMPSSDialog.UpdateCriteria;
var
  sel          : integer;
  k            : integer;
begin
  sel := grid^.GetSelIndex;
  if sel = lb_Err then
  begin
    relGrp^.SetSelected(longint(MPSSAnd));
    lParen^.SetCheck(0);
    rParen^.SetCheck(0);
    fldGrid^.SetSelIndex(-1);
    EnableOpsGrp(0);
    opsGrp^.SetSelected(longint(MPSSEQ));
    if fEdit <> nil then
    begin
      MSDisposeObj(fEdit);
    end;
  end
  else
  begin
    grid^.GetData(0, sel, ListBoxLine, 10);
    if StrIComp(ListBoxLine, orStr) = 0 then
      relGrp^.SetSelected(longint(MPSSOr))
    else
      relGrp^.SetSelected(longint(MPSSAnd));
    grid^.GetData(1, sel, ListBoxLine, 10);
    lParen^.SetCheck(StrLen(ListBoxLine));
    grid^.GetData(2, sel, ListBoxLine, maxFileDescLen + maxFldNameLen + 1);
    k := fldGrid^.SetSelString(ListBoxLine, -1);
    if k = lb_Err then
    begin
      fldGrid^.SetSelIndex(-1);
      ShowError(@self, IDS_INVALID_FIELD, nil, 0, 0, sel + 1);
      EnableOpsGrp(0);
    end
    else
    begin
      EnableOpsGrp(fldGrid^.GetItemData(k));
    end;
    opsGrp^.SetSelected(grid^.GetItemData(sel));
    grid^.GetData(4, sel, ListBoxLine, 255);
    UpdateValueField(ListBoxLine);
    grid^.GetData(5, sel, ListBoxLine, 10);
    rParen^.SetCheck(StrLen(ListBoxLine));
  end;
end;

function TMPSSDialog.ESBLCat(Index: Integer): boolean;
var TestCat: Array [0..32] of char;
    Length : Word;
begin
  Length:=StrLen(Fields^[Index].FieldName);
  if Length>=5 then
  begin
    Move(Fields^[Index].FieldName[Length-5], TestCat, 6);
    ESBLCat:=StrComp(TestCat,' ESBL')=0;
  end
  else ESBLCat:=False;
end;

procedure TMPSSDialog.UpdateValueField(val: PChar);
var
  k            : integer;
begin
  if fEdit <> nil then
  begin
    MSDisposeObj(fEdit);
  end;
  k := fldGrid^.GetSelIndex + 1;
  if k = 0 then
    Exit;

  if Fields^[k].TestRef = -1 then
  begin
    rec := Fields^[k].FH^.dbr;
    fEdit := New(PUIFld, PostInit(@self, Fields^[k].FH^.dbd, mnmList, FALSE,
                                  Fields^[k].FieldNo, 300, 0, IDC_VALUE, 0));
  end
  else
  begin
    rec := tstSubstFH^.dbr;

    if Fields^[k].FieldNo = DBTSTSUBTest then
    begin

      if (tstList^.IndexOf(pEBLStr)=-1) then
      begin
        if ESBLCat(k)
        then tstList^.Insert(pEBLStr);
      end
      else if Not ESBLCat(k) then tstList^.Delete(pEBLStr);

      fEdit := New(PUIListFld, PostInit(@self, tstSubstFH^.dbd, FALSE,
                                        Fields^[k].FieldNo, 300, 0, IDC_VALUE, 0,
                                        Fields^[k].FieldName, nil, tstList));
    end
(*                               DBTSTSUBBiotype *)
    else
    begin
      fEdit := New(PUIFld, PostInit(@self, tstSubstFH^.dbd, mnmList, FALSE,
                                    Fields^[k].FieldNo, 300, 0, IDC_VALUE, 0));
    end;
  end;

  if val = nil then
  begin
    rec^.ClearField(Fields^[k].FieldNo);
    fEdit^.Clear;
  end
  else
  begin
    fEdit^.SetCtlAsStr(val);
    fEdit^.XferCtlToRec(rec);
  end;
end;

procedure TMPSSDialog.EnableOpsGrp(val: integer);
begin
  case val of
    0:
      begin
        opsGrp^.Enable(longint(MPSSNE), TRUE);
        opsGrp^.Enable(longint(MPSSLT), TRUE);
        opsGrp^.Enable(longint(MPSSGT), TRUE);
        opsGrp^.Enable(longint(MPSSLE), TRUE);
        opsGrp^.Enable(longint(MPSSGE), TRUE);
        opsGrp^.Enable(longint(MPSSIN), TRUE);
      end;
    1, 2:
      begin
        if TMPSSOp(opsGrp^.SelectedVal) > MPSSNE then
          opsGrp^.SetSelected(longint(MPSSEQ));
        opsGrp^.Enable(longint(MPSSNE), TRUE);
        opsGrp^.Enable(longint(MPSSLT), FALSE);
        opsGrp^.Enable(longint(MPSSGT), FALSE);
        opsGrp^.Enable(longint(MPSSLE), FALSE);
        opsGrp^.Enable(longint(MPSSGE), FALSE);
        opsGrp^.Enable(longint(MPSSIN), FALSE);
      end;
    else
      begin
        opsGrp^.SetSelected(longint(MPSSEQ));
        opsGrp^.Enable(longint(MPSSNE), FALSE);
        opsGrp^.Enable(longint(MPSSLT), FALSE);
        opsGrp^.Enable(longint(MPSSGT), FALSE);
        opsGrp^.Enable(longint(MPSSLE), FALSE);
        opsGrp^.Enable(longint(MPSSGE), FALSE);
        opsGrp^.Enable(longint(MPSSIN), FALSE);
      end;
  end;
end;

function TMPSSDialog.CheckParens: boolean;
var
  lp, pos      : integer;
  okay         : boolean;
  ruleCounter  : word;
begin
  okay := TRUE;
  lp := 0;
  pos := 0;
  ruleCounter := 0;
  while Rules^[ruleCounter].FH <> nil  do
  begin
    if Rules^[ruleCounter].LParen then
    begin
      pos := ruleCounter;
      if (lp = 0) then
        Inc(lp)
      else
      begin
        okay := FALSE;
        Break;
      end;
    end;
    if Rules^[ruleCounter].RParen then
      if (lp = 1) then
        Dec(lp)
      else
      begin
        pos := ruleCounter;
        okay := FALSE;
        Break;
      end;
    Inc(ruleCounter);
  end;
  if not okay or (lp <> 0) then
  begin
    okay := FALSE;
    FocusCtl(HWindow, IDC_RULELIST);
    grid^.SetSelIndex(pos);
    UpdateCriteria;
    ShowError(@self, IDS_UNMATCHED_PAREN, nil, 0, 0, pos + 1);
  end;
  CheckParens := okay;
end;

function TMPSSDialog.CheckFields: boolean;
var
  ruleCounter  : word;
begin
  CheckFields := TRUE;
  ruleCounter := 0;
  while Rules^[ruleCounter].FH <> nil  do
  begin
    if Rules^[ruleCounter].FieldNo = word(-1) then
    begin
      FocusCtl(HWindow, IDC_RULELIST);
      grid^.SetSelIndex(ruleCounter);
      UpdateCriteria;
      CheckFields := FALSE;
      Exit;
    end;
    Inc(ruleCounter);
  end;
end;


constructor TMPSSSaveDialog.Init(AParent: PWindowsObject; RulesPtr: PRules;
                                 dbFH: PDBFile);
begin
  idEdit := nil;
  descEdit := nil;

  if not inherited Init(AParent, MakeIntResource(DLG_SAVERULE)) then
  begin
    ShowError(aParent, IDS_INITERR1, nil, 0, MOD_MPSS, 32);
    Fail;
  end;
  Rules := RulesPtr;
  ruleFH := dbFH;
  idEdit := New(PUIFullEditFld, Init(@self, ruleFH^.dbd, nil, TRUE,
                                     DBMPSSRULEID, IDC_CODE, IDC_LBL1));
  descEdit := New(PUIFullEditFld, Init(@self, ruleFH^.dbd, nil, TRUE,
                                       DBMPSSRULEDesc, IDC_DESC, IDC_LBL2));
  if (idEdit = nil) or (descEdit = nil) then
  begin
    ShowError(aParent, IDS_INITERR1, nil, 0, MOD_MPSS, 33);
    Done;
    Fail;
  end;
end;

destructor TMPSSSaveDialog.Done;
begin
  MSDisposeObj(descEdit);
  MSDisposeObj(idEdit);
  inherited Done;
end;

procedure TMPSSSaveDialog.SetupWindow;
begin
  inherited SetupWindow;
  idEdit^.XferRecToCtl(ruleFH^.dbr);
  descEdit^.XferRecToCtl(ruleFH^.dbr);
end;

function  TMPSSSaveDialog.CanClose: boolean;
var
  ruleID       : array [0..ID_LEN] of char;
  tempID       : array [0..ID_LEN] of char;
  ruleSeqNum   : TSeqNum;
  condFH       : PDBFile;
  ruleCounter  : word;
  fileName     : FNString;
  err          : integer;
  block        : array [0..9] of byte;
begin
  CanClose := FALSE;
  { Get the user input }
  err := idEdit^.Format;
  if err <> 0 then
  begin
    ShowUIFldError(@self, err, idEdit);
    Exit;
  end;
  err := descEdit^.Format;
  if err <> 0 then
  begin
    ShowUIFldError(@self, err, descEdit);
    Exit;
  end;

  ruleFH^.dbr^.GetFieldAsStr(DBMPSSRULEID, tempID, ID_LEN);
  idEdit^.XferCtlToRec(ruleFH^.dbr);
  ruleFH^.dbr^.GetFieldAsStr(DBMPSSRULEID, ruleID, ID_LEN);
  if StrIComp(tempID, ruleID) <> 0 then
  begin
    { Verify ID does not exist }
    if ruleFH^.dbc^.GetEQ(ruleFH^.dbr) then
    begin
      ruleFH^.dbr^.PutFieldAsStr(DBMPSSRULEID, tempID);
      ruleFH^.dbc^.GetEQ(ruleFH^.dbr);
      ShowError(@self, IDS_IN_USE, nil, 0, 0, 0);
      FocusCtl(HWindow, IDC_CODE);
      Exit;
    end;
  end
  else
    ruleFH^.dbc^.DeleteRec(ruleFH^.dbr);

  { Save the Rule Set }
  descEdit^.XferCtlToRec(ruleFH^.dbr);
  ruleFH^.dbc^.InsertRec(ruleFH^.dbr);
  ruleSeqNum := ruleFH^.dbr^.GetSeqValue;

  { Save the rules }
  condFH := New(PDBFile, Init(DBMPSSCONDFile, '', dbOpenNormal));
  if condFH = nil then
  begin
    ShowError(@self, IDS_INITERR1, nil, 0, MOD_MPSS, 34);
    Exit;
  end;
  condFH^.dbr^.PutField(DBMPSSCONDRuleRef, @ruleSeqNum);
  ruleCounter := 0;
  while Rules^[ruleCounter].FH <> nil do
  begin
    condFH^.dbr^.PutField(DBMPSSCONDIndex, @ruleCounter);
    condFH^.dbr^.PutField(DBMPSSCONDAndOr, @Rules^[ruleCounter].AndOr);
    condFH^.dbr^.PutField(DBMPSSCONDLParen, @Rules^[ruleCounter].LParen);
    condFH^.dbr^.PutField(DBMPSSCONDRParen, @Rules^[ruleCounter].RParen);

    Rules^[ruleCounter].FH^.dbd^.FileName(fileName, 8);
    condFH^.dbr^.PutField(DBMPSSCONDFileName, @fileName);

    condFH^.dbr^.PutField(DBMPSSCONDFieldName, @Rules^[ruleCounter].FieldName);
    condFH^.dbr^.PutField(DBMPSSCONDOperator, @Rules^[ruleCounter].Operator);
    condFH^.dbr^.PutFieldAsStr(DBMPSSCONDValue, Rules^[ruleCounter].Value);
    FillChar(block, SizeOf(block), 0);
    case Rules^[ruleCounter].FieldType of
      dbInteger, dbLongInt, dbWord, dbBoolean, dbByte, dbShortInt,
      dbChar, dbDate, dbTime, dbSeqRef, dbAutoInc :
      begin
        Move(Rules^[ruleCounter].lValue, block, SizeOf(Rules^[ruleCounter].lValue));
      end;
      dbSingle :
      begin
        Move(Rules^[ruleCounter].sValue, block, SizeOf(Rules^[ruleCounter].sValue));
      end;
      dbDouble :
      begin
        Move(Rules^[ruleCounter].dValue, block, SizeOf(Rules^[ruleCounter].dValue));
      end;
      {- else   dbString, dbCode, dbZString, dbZCode, dbBlock, }
    end;
    condFH^.dbr^.PutField(DBMPSSCONDBlock, @block);
    condFH^.dbc^.InsertRec(condFH^.dbr);
    Inc(ruleCounter);
  end;
  MSDisposeObj(condFH);
  CanClose := TRUE;
end;


BEGIN
  SR(IDS_AND, andStr, 10);
  SR(IDS_OR, orStr, 10);
END.
