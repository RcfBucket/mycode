unit PanDump;

INTERFACE

uses
  Objects,
  OWindows,
  WinTypes,
  DlgLib,
  Trays,
  Biotype,
  PDLObj,
  AutoScan,
  PrnPrev,
  ResLib,
  DBLib,
  DBFile,
  WAUtil,
  WAGPIB;

const
  WANoWellVal = -2139062144;
  AS4NoWellVal = -32640;

type
  FilterRec = record
    filterStr : array[0..8] of char;
    calRange  : integer;
    calValue  : integer;
  end;

  ReadingsRec = record
    reading   : longint;
    outOfCtrl : boolean;
  end;

  PPanelDumpObj = ^TPanelDumpObj;
  TPanelDumpObj = object(TObject)
    fieldVal    : PChar;
    dbIso,
    dbSpec      : PDBFile;
    dbIsoDesc,
    dbSpecDesc  : PDBReadDesc;
    readTime,
    organism,
    biotype     : PChar;
    currentRow  : integer;
    loFilter,
    hiFilter    : byte;
    resList     : PResults;
    title       : PChar;
    reactions   : array[1..12] of byte;
    readings    : array[1..12] of ReadingsRec;
    filterArray : array[0..18] of FilterRec;
    tempStr,
    printStr    : array[0..255] of char;
    testIDFile  : text;
    printInfo   : PPrintInfo;
    printDlg    : PPrnPrevDlg;
    parent      : PWindowsObject;
    formData    : array[wellnum] of integer;
    rawData     : PRawData;
    MICData     : TMICData;
    printprocessed,
    printRaw    : boolean;
    {}
    constructor Init(aParent   : PWindowsObject;
                     isoFile,
                     specFile  : PDBFile;
                     res       : PResults);
    destructor Done; virtual;
    procedure PrintPanelDump; virtual;
    function  GetReadTime: boolean; virtual;
    function  GetOrganismAndBiotype : boolean; virtual;
    procedure SetUpHeader; virtual;
    procedure PrintKeyFields; virtual;
    procedure PrintPanelSpecifics; virtual;
    function  PrintData: boolean; virtual;
    procedure PrintTestIDs; virtual;
    function  PrintMICs: boolean; virtual;
    function  PrintEndPoints : boolean; virtual;
    function  PanelOK: boolean; virtual;
    procedure PrintRowOfReactions(NoWellVal: longint); virtual;
    procedure PrintRowOfReadings(rData   : boolean;
                                 filterStr : PChar); virtual;
    procedure StrReading(rData    : boolean;
                         readingIdx : integer;
                         readingStr : PChar); virtual;
  end;

  PWAPanelDump = ^TWAPanelDump;
  TWAPanelDump = object(TPanelDumpObj)
    constructor Init(aParent  : PWindowsObject;
                     isoFile,
                     specFile : PDBFile;
                     res      : PResults;
                     biot     : PWABiotypeObject;
                     pID      : integer);
    destructor Done; virtual;
    procedure GetReportData;
    procedure GetROIOffsets;
    function  GetPOSFlags: boolean;
    function  GetMICData: boolean;
    function  GetReadTime: boolean; virtual;
    function  GetOrganismAndBiotype: boolean; virtual;
    function  TestsNeeded: boolean;
    function  OKToShowID: boolean;
    function  PrintData: boolean; virtual;
    function  PrintMICs: boolean; virtual;
    function  PanelOK: boolean; virtual;
    {}
    private
    panelID      : integer;
    WAPSICmd     : PWAPSICmd;
    exceptions   : PExceptions;
    flags        : PPanelFlags;
    ROIOffset    : RDIArray;
    bioObj       : PWABiotypeObject;
    {}
    function  PrintProcessedData: boolean;
    function  PrintRawData: boolean;
    function  PrintRawReadings(which: integer): boolean;
    procedure InitFilterRec(which: integer);
    end;

  PAS4PanelDump = ^TAS4PanelDump;
  TAS4PanelDump = object(TPanelDumpObj)
    constructor Init(aParent     : PWindowsObject;
                     isoFile,
                     specFile    : PDBFile;
                     res         : PResults;
                     biot        : PBiotypeObject;
                     panelIsRead : boolean;
                     pdlObject   : PPDLObject;
                     AS4Object   : PAutoSCANObject);
    function  PrintCalibratedData : boolean; virtual;
    function  PrintProcessedData : boolean; virtual;
    function  PrintGainData : boolean; virtual;
    function  PrintRawData : boolean; virtual;
    function  PrintData : boolean; virtual;
    function  PrintMICs : boolean; virtual;
    private
    pdlObj        : PPDLObject;
    AS4Obj        : PAutoSCANObject;
    bioObj        : PBiotypeObject;
    currentFilter : byte;
    printCalibrated,
    printGains    : boolean;
    function  GetOrganismAndBiotype : boolean; virtual;
    procedure InitFilterRec(which : integer); virtual;
    function  PrintCalReadings(which : integer) : boolean; virtual;
    procedure PrintNotes; virtual;
    procedure StrReading(rData    : boolean;
                         readingIdx : integer;
                         readingStr : PChar); virtual;
    end;

{************************************************************************}
{************************************************************************}

IMPLEMENTATION

uses
  WinProcs,
  Strings,
  StrsW,
  Bits,
  MScan,
  APITools,
  IntlLib,
  DBIds,
  DBTypes,
  IDLib,
  INILib,
  PrnObj,
  DMSDebug;

{$I PANDUMP.INC}
{$R PANDUMP.RES}

const
  printStrLen = 120;

type
  PWADumpOptionsDlg = ^TWADumpOptionsDlg;
  TWADumpOptionsDlg = object(TCenterDlg)
    p, r  : ^boolean;
    {}
    constructor Init(aParent: PWindowsObject;
                     processed, raw : pointer);
    procedure OK(var  msg: TMessage); virtual ID_FIRST + IDOK;
  end;

  PAS4DumpOptionsDlg = ^TAS4DumpOptionsDlg;
  TAS4DumpOptionsDLg = object(TCenterDlg)
    p, r, c, g  : ^boolean;
    panelIsRead : boolean;
    {}
    constructor Init(aParent   : PWindowsObject;
                     panelRead : boolean;
                     processed,
                     raw,
                     calibrated,
                     gains     : pointer);
    procedure SetUpWindow; virtual;
    function  Run : integer; virtual;
    procedure OK(var  msg : TMessage); virtual ID_FIRST + IDOK;
  end;



procedure ConcatBlanks(howMany: integer; newStr: PChar);
{************************************************************************}
{*  Concats the specified number of blanks onto the end of the newStr   *}
{************************************************************************}
begin
  if howMany <= 0 then
    Exit;

  Inc(howMany, StrLen(newStr));
  if howMany >= printStrLen then
    howMany := printStrLen;
  Pad(newStr, newStr, ' ', howMany);
end;

procedure ConcatColon(maxSize: integer; newStr: PChar);
{************************************************************************}
{*  Concats a colon to newStr at maxsize-1 position                     *}
{************************************************************************}
begin
  if StrLen(newStr) > maxSize then
    Exit;
  FillChar(newStr[StrLen(newStr)], maxSize-StrLen(newStr), ' ');
  newStr[maxSize-2] := ':';
  newStr[maxSize] := #0;
end;

{ Methods for TPanelDumpObj }

constructor TPanelDumpObj.Init(aParent   : PWindowsObject;
                               isoFile,
                               specFile  : PDBFile;
                               res       : PResults);
{************************************************************************}
{************************************************************************}
var
  printFont : TLogFont;
begin
  fieldVal   := nil;
  readTime   := nil;
  organism   := nil;
  biotype    := nil;
  dbIso      := nil;
  dbSpec     := nil;
  dbIsoDesc  := nil;
  dbSpecDesc := nil;
  printInfo  := nil;
  title      := nil;

  MakeScreenFont(printFont, false, false);
  printInfo := New(PPrintInfo, Init(printFont, INIDefPointSize));
  if printInfo = nil then
  begin
    Done;
    Fail;
  end;

  {$IFOPT I+}
  {$DEFINE chki}
  {$ENDIF}
  {$I-}

  Assign(TestIdFile, 'DataList.TXT');
  Reset(TestIDFile);
  if IOResult <> 0 then
  begin
    Done;
    Fail;
  end;

  GetMem(fieldVal, PrintStrLen);
  GetMem(readTime, 32);
  GetMem(organism, 64);
  GetMem(biotype, 64);
  resList := res;
  dbIso := isoFile;
  dbSpec := specFile;
  dbIsoDesc := dbIso^.dbr^.Desc;
  dbSpecDesc := dbSpec^.dbr^.desc;
  GetMem(title, 120);
  StrCopy(title, '');
  parent := aParent;
end;

destructor TPanelDumpObj.Done;
{************************************************************************}
{************************************************************************}
begin
  MSFreeMem(fieldVal, PrintStrLen);
  MSFreeMem(readTime, 32);
  MSFreeMem(organism, 64);
  MSFreeMem(biotype, 64);
  MSDisposeObj(printInfo);
  MSFreeMem(title, 120);

  Close(testIDFile);
  if IOResult <> 0 then;

  {$IFDEF chki}
    {$I+}
    {$UNDEF chki}
  {$ENDIF}

  inherited Done;
end;

procedure TPanelDumpObj.PrintPanelDump;
{************************************************************************}
{************************************************************************}
var
  wc      : PWaitCursor;
begin
  wc := New(PWaitCursor, Init);
  if GetReadTime then
    if GetOrganismAndBiotype then;
  SetUpHeader;
  MSDisposeObj(wc);
  PrintData;
end;

function TPanelDumpObj.GetReadTime : boolean;
{************************************************************************}
{************************************************************************}
var
  rDate : TIntlDate;
  rTime : TIntlTime;
  tStr  : array[0..31] of char;
begin
  GetReadTime := true;
  rDate := IntlCurrentDate;
  rTime := IntlCurrentTime;
  IntlDateStr(rDate, readTime, 31);
  IntlTimeStr(rTime, false, tStr, 31);
  StrLCat(readTime, '  ', 31);
  StrLCat(readTime, tStr, 31);
end;

function TPanelDumpObj.GetOrganismAndBiotype : boolean;
{************************************************************************}
{************************************************************************}
begin
 {implemented by the descendents }
 organism[0] := chr(0);
 biotype[0] := chr(0);
 GetOrganismAndBiotype := true;
end;

procedure TPanelDumpObj.SetUpHeader;
{************************************************************************}
{************************************************************************}
var
  tStr,
  pStr    : array[0..120] of char;
  rptDate : TIntlDate;
  rptTime : TIntlTime;
begin
  printInfo^.header^.AddRow(0, 0);
  printInfo^.header^.AddCell(title, LT800, c_center or c_bold, -4);
  rptDate := IntlCurrentDate;
  IntlDateStr(rptDate, pStr, 32);
  StrLCat(pStr, '  ',127);
  rptTime := IntlCurrentTime;
  IntlTimeStr(rptTime, false, tStr, 32);
  StrLCat(pStr, tStr, 127);
  printInfo^.header^.AddRow(0, 0);
  printInfo^.header^.AddCell(pStr, LT800, c_bold or c_center, -2);
  printInfo^.header^.AddRow(0, 0);

  if (printProcessed) OR (printRaw) then {there is panel information}
  begin
    PrintKeyFields;
    PrintPanelSpecifics;
  end
  else
    printInfo^.header^.AddRow(r_borderBottom, 0);
end;

procedure TPanelDumpObj.PrintKeyFields;
{************************************************************************}
{************************************************************************}
begin
  printInfo^.header^.AddRow(0, 0);

  printStr[0] := chr(0);

  { specimen number }
  dbSpecDesc^.FieldName(DBSPECSpecID, fieldVal, 64);
  printInfo^.header^.AddCell(fieldVal, LT100, c_normal or c_left, 0);
  printInfo^.header^.AddCell(': ', LT08, c_normal or c_left, 0);
  dbSpec^.dbr^.GetFieldAsStr(DBSPECSpecID, fieldVal, 64);
  printInfo^.header^.AddCell(fieldVal, LT125, c_normal or c_left, 0);
  printInfo^.header^.AddCell(' ', LT08, c_normal or c_left, 0);

  { isolate number }
  dbIsoDesc^.FieldName(DBISOIso, fieldVal, 64);
  printInfo^.header^.AddCell(fieldVal, LT75, c_normal or c_left, 0);
  printInfo^.header^.AddCell(': ', LT08, c_normal or c_left, 0);
  dbIso^.dbr^.GetFieldAsStr(DBIsoIso, fieldVal, 64);
  printInfo^.header^.AddCell(fieldVal, LT150, c_normal or c_left, 0);
  printInfo^.header^.AddCell(' ', LT08, c_normal or c_left, 0);

  { Collect Date }
  dbSpecDesc^.FieldName(DBSPECCollectDate, fieldVal, 64);
  printInfo^.header^.AddCell(fieldVal, LT100, c_normal or c_left, 0);
  printInfo^.header^.AddCell(': ', LT08, c_normal or c_left, 0);
  dbSpec^.dbr^.GetFieldAsStr(DBSPECCollectDate, fieldVal, 64);
  printInfo^.header^.AddCell(fieldVal, LT150, c_normal or c_left, 0);
  printInfo^.header^.AddCell(' ', LT08, c_normal or c_left, 0);
end;

procedure TPanelDumpObj.PrintPanelSpecifics;
{************************************************************************}
{************************************************************************}
const
  ColOne     = 32;
  ColTwo     = 52;
  ColThree   = 88;
  LabelWidth = 15;
begin
  printInfo^.header^.AddRow(r_borderBottom, 0);

  printStr[0] := chr(0);

  { panel type }
  SR(IDS_PANELTYPE, fieldVal, labelWidth);
  printInfo^.header^.AddCell(fieldVal, LT100, c_normal or c_left, 0);
  printInfo^.header^.AddCell(': ', LT08, c_normal or c_left, 0);
  printInfo^.header^.AddCell(resList^.traysObj^.tfsrec.tName, LT125, c_normal or c_left, 0);
  printInfo^.header^.AddCell(' ', LT08, c_normal or c_left, 0);

  { organism }
  SR(IDS_ORGANISM,fieldVal, labelWidth);
  printInfo^.header^.AddCell(fieldVal, LT75, c_normal or c_left, 0);
  printInfo^.header^.AddCell(': ', LT08, c_normal or c_left, 0);
  StrCopy(fieldVal, organism);
  if fieldVal[0] = #0 then
    StrCopy(fieldVal, ' ');
  printInfo^.header^.AddCell(fieldVal, LT150, c_normal or c_left, 0);
  printInfo^.header^.AddCell(' ', LT08, c_normal or c_left, 0);

  { biotype }
  SR(IDS_BIOTYPE, fieldVal, PrintStrLen-1); {'Biotype'}
  printInfo^.header^.AddCell(fieldVal, LT100, c_normal or c_left, 0);
  printInfo^.header^.AddCell(': ', LT08, c_normal or c_left, 0);
  StrCopy(fieldVal, biotype);
  if fieldVal[0] = #0 then
    StrCopy(fieldVal, ' ');
  printInfo^.header^.AddCell(fieldVal, LT150, c_normal or c_left, 0);
  printInfo^.header^.AddCell(' ', LT08, c_normal or c_left, 0);
end;

function TPanelDumpObj.PrintData : boolean;
begin
  PrintData := false;
  if PrintMICS then
  begin
    printInfo^.body^.AddRow(r_borderBottom or r_StartGroup, 0);
    SR(IDS_MICS, printStr, PrintStrLen-1); { MICS : (Antimicrobics... }
    printInfo^.body^.AddRow(0, 0);
    printInfo^.body^.AddCell(printStr, LT800, c_normal, 0);
    printInfo^.body^.AddCell(':', LT08, c_normal, 0);
    PrintEndPoints;
  end;

  printDlg := New(PPrnPrevDlg, Init(parent, printInfo, title));
  application^.ExecDialog(printDlg);
  PrintData := true;
end;

procedure TPanelDumpObj.PrintTestIDs;
{************************************************************************}
{************************************************************************}
var
  i,j,
  panNum  : integer;
  okFine  : boolean;
  blankChar : pointer;
  readStr : string[120];
begin
(*printInfo^.body^.AddRow(0, 0);  assumes the row has been added prior to calling this function *)
  printStr[0] := chr(0);
  SR(IDS_TESTID, printStr, PrintStrLen-1); {Test ID}
  printInfo^.body^.AddCell(printStr, LT100, c_normal or c_left, 0);
  printInfo^.body^.AddCell(':', LT08, c_normal or c_left, 0);

  {$IFOPT I+}
  {$DEFINE chki}
  {$ENDIF}
  {$I-}

  Reset(testIDFile);
  okFine := IOResult = 0;
  panNum := resList^.traysObj^.tfsRec.trayNum;
  i := 1;
  { find the area in the file with the test ids for this panel }
  while (i <= (9*PanNum)) and (OKFine) do
  begin
    ReadLn(TestIDFile);
    if IOResult <> 0 then
      okFine := false;
    Inc(i);
  end;
  { pass by the panel name line }
  if okFine then
  begin
    ReadLn(TestIDFile);
    if IOResult <> 0 then
      okFine := false;
  end;
  if okFine then
  begin
    i := 0;
    { find the right row }
    while (i <= currentRow) and (okFine) do
    begin
      ReadLn(TestIDFile, readStr);

      While (Length(readStr) < (SizeOf(readStr)-1)) do
        readStr:= readStr + ' ';
      if IOResult <> 0 then
        okFine := false;

  {$IFDEF chki}
    {$I+}
    {$UNDEF chki}
  {$ENDIF}

      if i = currentRow then
      begin
        for j := 0 to 11 do
        begin
          FillChar(printStr[0], sizeOf(printStr), chr(0));
          move(readStr[(j*9)+1], printStr[0], 9);
          printInfo^.body^.AddCell(printStr, LT50, c_normal or c_left, 0);
        end;
      end;
      Inc(i);
    end;
  end;
  if not okFine then
  begin
    StrLCat(printStr, 'XXXXXXXXXXXXXXXXXXXXXXXX', PrintStrLen-1);
    printInfo^.body^.AddCell(printStr, LT700, c_normal or c_left, 0);
  end;
end;

function TPanelDumpObj.PrintMICs : boolean;
begin
  printMICs := true;
end;

function TPanelDumpObj.PrintEndPoints : boolean;
const
  MaxColWidth   = 7;
  MaxMICPerLine = 15;
var
  MICLine,
  DilLine    : Array [1..MaxMICLen] of PChar;
  OutStr     : PChar;
  drugDils   : DrugDilType;
  dilCnt     : integer;
  MaxCycles,
  i,
  j          : integer;
begin
  PrintEndPoints := true;

  If panelOK then
  Begin
    {Get some space for drugs and MICs...}
    GetMem(OutStr, 20);
    For i := 1 to MaxMICLen do
    Begin
      GetMem(MICLine[i], (MaxColWidth+1));
      StrPCopy(MICLine[i],'       ');
      GetMem(DilLine[i], (MaxColWidth+1));
      StrPCopy(DilLine[i],'       ');
    End;

    {Get the drug and dilution strings...}
    For i := 1 to resList^.traysObj^.DrugCnt do
    Begin
      {Get drug abbreviation for each...}
      StrPCopy(MICLine[i], resList^.traysObj^.dilList[i].drugName);
      ConcatBlanks((MaxColWidth-StrLen(MICLine[i])), MICLine[i]);
      {Get dilution for each...}
      StrPCopy(DilLine[i], resList^.traysObj^.dilList[i].drugDils[MICData[i]]);
      TrimLead(DilLine[i], DilLine[i], ' ', MaxColWidth);
      ConcatBlanks((MaxColWidth-StrLen(DilLine[i])), DilLine[i]);
    End;

    {Build the output...}
    MaxCycles := resList^.traysObj^.DrugCnt div MaxMICPerLine;
    For i := 1 to MaxCycles do
    Begin
      printInfo^.body^.AddRow(0, 0);
      For j := 1 to MaxMICPerLine do
      Begin
        StrCopy(OutStr,MICLine[(i-1)*MaxMICPerLine+j]);
        printInfo^.body^.AddCell(OutStr, lt50, c_normal or c_left, 0);
      End;
      printInfo^.body^.AddRow(0, 0);
      For j := 1 to MaxMICPerLine do
      Begin
        StrCopy(OutStr,DilLine[(i-1)*MaxMICPerLine+j]);
        printInfo^.body^.AddCell(OutStr, lt50, c_normal or c_left, 0);
      End;
    End;
    If (resList^.traysObj^.DrugCnt mod MaxMICPerLine) > 0 then
    Begin
      printInfo^.body^.AddRow(0, 0);
      For i := (MaxCycles*MaxMICPerLine+1) to resList^.traysObj^.DrugCnt do
      Begin
        StrCopy(OutStr,MICLine[i]);
        printInfo^.body^.AddCell(OutStr, lt50, c_normal or c_left, 0);
      End;
      printInfo^.body^.AddRow(0, 0);
      For i := (MaxCycles*MaxMICPerLine+1) to resList^.traysObj^.DrugCnt do
      Begin
        StrCopy(OutStr,DilLine[i]);
        printInfo^.body^.AddCell(OutStr, lt50, c_normal or c_left, 0);
      End;
    End;

    {Free up the memory...}
    MSFreeMem(OutStr, 20);
    For i := 1 to MaxMICLen do
    Begin
      MSFreeMem(MICLine[i], (MaxColWidth+1));
      MSFreeMem(DilLine[i], (MaxColWidth+1));
    End;
  End;
end;

function TPanelDumpObj.PanelOK : boolean;
begin
  PanelOK := true;
end;

procedure TPanelDumpObj.PrintRowOfReactions(NoWellVal: longint);
{************************************************************************}
{************************************************************************}
var
  i : integer;
  temp : string;
begin
  printStr[0] := chr(0);
  printInfo^.body^.AddRow(0, 0);
  StrCopy(printStr, '+/-');
  printInfo^.body^.AddCell(printStr, LT100, c_normal or c_left, 0);
  printInfo^.body^.AddCell(':', LT08, c_normal or c_left, 0);
  for i := 1 to 12 do
  begin
    if readings[i].reading <> NoWellVal then { is well number valid }
    begin
      Str(readings[i].reading, temp);
      if Length(temp) > 8 then { if number too big then well not yet valid }
        StrPCopy(tempStr, '*')
      else if TestBit(reactions[i], currentRow) then
        StrPCopy(tempStr, '+')
      else
        StrPCopy(tempStr, '-');
    end
    else
      StrPCopy(tempStr, ' ');
    printInfo^.body^.AddCell(tempStr, LT50, c_normal, 0);
  end;
end;

procedure TPanelDumpObj.PrintRowOfReadings(rData   : boolean;
                                           filterStr : PChar);
{************************************************************************}
{************************************************************************}
const
  colWidth = 9;
var
  i : integer;
begin
  printStr[0] := chr(0);
  printInfo^.body^.AddRow(0, 0);
  StrCopy(printStr, filterStr);
  printInfo^.body^.AddCell(printStr, LT100, c_normal or c_left, 0);
  printInfo^.body^.AddCell(':', LT08, c_normal or c_left, 0);
  for i := 1 to 12 do
  begin
    StrReading(rData, i, printStr);
    printInfo^.body^.AddCell(printStr, LT50, c_normal or c_left , 0);
  end;
end;

procedure TPanelDumpObj.StrReading(rData      : boolean;
                                   readingIdx : integer;
                                   readingStr : PChar);
{************************************************************************}
{************************************************************************}
var
  temp : string;
begin
  if rData then
  begin
    if readings[readingIdx].reading <> 0 then
    begin
      Str(readings[readingIdx].reading, temp);
      StrPCopy(readingStr, temp);
    end
    else StrPCopy(readingStr, '.....');
  end
  else
  begin
    if readings[readingIdx].reading = WANoWellVal then { is well number valid }
      StrPCopy(readingStr, '.....')
    else
    begin
      Str(readings[readingIdx].reading, temp);
      StrPCopy(readingStr, temp);
      if StrLen(readingStr) > 8 then { if number too big then well not yet valid }
        StrPCopy(readingStr, '*****');
    end;
  end;
end;


{ Methods for TWAPanelDump Object }

constructor TWAPanelDump.Init(aParent  : PWindowsObject;
                              isoFile,
                              specFile : PDBFile;
                              res      : PResults;
                              biot     : PWABiotypeObject;
                              pID      : integer);
{************************************************************************}
{************************************************************************}
var
  i       : integer;
  options : PWADumpOptionsDlg;
  wc      : PWaitCursor;
begin
  printProcessed := FALSE;
  printRaw := FALSE;
  options := nil;
  WAPSICmd := nil;
  exceptions := nil;
  flags := nil;

  if not inherited Init(aParent, isoFile, specFile, res) then
  begin
    Done;
    Fail;
  end;

  options := New(PWADumpOptionsDlg,Init(aParent, @printProcessed, @printRaw));
  if options = nil then
  begin
    Done;
    Fail;
  end;

  if Application^.ExecDialog(options) = IDCANCEL then
  begin
    Done;
    Fail;
  end;

  SR(IDS_WA_TITLE, title, 120);
  WAPSICmd := New(PWAPSICmd, Init(WAID));
  if WAPSICmd = nil then
  begin
    Done;
    Fail;
  end;

  exceptions := New(PExceptions, Init);
  if exceptions = nil then
  begin
    Done;
    Fail;
  end;
  panelID := pID;
  bioObj := biot;
  wc := New(PWaitCursor, Init);
  GetReportData;
  MSDisposeObj(wc);
end;

destructor TWAPanelDump.Done;
{************************************************************************}
{************************************************************************}
begin
  MSDisposeObj(WAPSICmd);
  MSDisposeObj(exceptions);
  MSDisposeObj(flags);
  inherited Done;
end;

procedure TWAPanelDump.GetReportData;
begin
  WAPSICmd^.SetParams(panelID);
  if WAPSICmd^.SendWAMessage then
  begin
    flags := New(PPanelFlags, Init(WAPSICmd^.recvMsg^.PSIFlags));
    GetROIOffsets;
    GetPOSFlags;
    GetMICData;
  end;
end;

procedure TWaPanelDump.GetROIOffsets;
var
  WAROICmd   : PWAROICmd;
  i       : integer;
  okSoFar : boolean;
begin
  WAROICmd := New(PWAROICmd, Init(WAID));
  WAROICmd^.SetParams(panelID);
  if WAROICmd^.SendWAMessage then
  begin
    for i := 1 to 12 do
      ROIOffset[i] := WAROICmd^.recvMsg^.ROIOffset[i];
  end;
  MSDisposeObj(WAROICmd);
end;

function TWAPanelDump.GetPOSFlags : boolean;
{************************************************************************}
{************************************************************************}
Const
  RNID3_IDRec  = 20;        {Identifies BDesc record number for RNID3 panels}
var
  WACmd : PWAPRICmd;
  i     : integer;
begin

  {Get the PDL var Did_Indole value for this panel}
  BioObj^.IndWasDispd := False;
  If (resList^.traysObj^.tfsRec.IDRec = RNID3_IDRec) then
    BioObj^.IndWasDispd := IndoleDispensedFlag(PanelID);

  GetPOSFlags := false;
  WACmd := New(PWAPRICmd, Init(WAID));
  WACmd^.SetParams(panelID);
  if WACmd^.SendWAMessage then
  begin
    for i := 1 to 12 do
      reactions[i] := WACmd^.recvMsg^.PRIBits[i];
    GetPOSFlags := true;
  end;
  MSDisposeObj(WACmd);
end;

function TWAPanelDump.GetMICData : boolean;
{************************************************************************}
{************************************************************************}
var
  WACmd : PWAPMICmd;
begin
  GetMICData := true;
  WACmd := New(PWAPMICmd, Init(WAID));
  WACmd^.SetParams(panelID);
  if WACmd^.SendWAMessage then
    Move(WACmd^.recvMsg^.PMIMics[1], micData[1], MaxMICLen)
  else
  begin
    GetMICData := false;
  end;
  MSDisposeObj(WACmd);
end;

function TWAPanelDump.GetReadTime : boolean;
{************************************************************************}
{************************************************************************}
begin
  FormatReadTime(WAPSICmd^.recvMsg^.PSILastRd, readTime, TRUE);
  GetReadTime := true;
end;

function TWAPanelDump.GetOrganismAndBiotype : boolean;
{************************************************************************}
{************************************************************************}
var
  orgNum   : integer;
  orgDB    : PDBFile;
begin
  GetOrganismAndBiotype := false;
  if inherited GetOrganismAndBiotype then
  begin
    if (bioObj <> nil) then
    begin
      if not TestsNeeded then
      begin
        StrPCopy(biotype, bioObj^.biotype);
        if bioObj^.GetTopID(orgNum, nil, nil, nil) then
        begin
          if orgNum = 0 then
          begin
            SR(IDS_RARE_BIOTYPE, organism, 63);
            exceptions^.noID := true;
          end
          else
          begin
            orgDB := New(PDBFile, Init(DBORGFile, '', DBOpenNormal));
            Str(orgNum, tempStr);
            orgDB^.dbr^.ClearRecord;
            if orgDB^.dbr^.PutFieldAsStr(DBORGOrg, tempStr) then
              if orgDB^.dbc^.GetEQ(orgDB^.dbr) then
                orgDB^.dbr^.GetFieldAsStr(DBORGSName, organism, 255);
            MSDisposeObj(orgDB);
          end;
        end
        else
          If (resList^.traysObj^.tfsrec.idRec <> (-1)) then
            SR(IDS_INVALID_BIOTYPE, organism, 63);
      end;
    end;

    if not OKToShowID then
      organism[0] := chr(0);

    GetOrganismAndBiotype := true;
  end;
end;

function TWaPanelDump.TestsNeeded: boolean;
var
  i : integer;
  MICOnly: boolean;
begin
  TestsNeeded := FALSE;
  MICOnly:=(resList^.traysObj^.tfsRec.idRec = -1);

  if not MICOnly then
  for i := 0 to bioObj^.MaxBitNum do
  begin
    if TestBioBit(bioObj^.neededResults, i) then
      if (i = bioObj^.oxidaseBitNum) then
      begin
        TestsNeeded := TRUE;
        exceptions^.needOxid := true
      end
      else
        if (i = bioObj^.indolebitNum) then
        begin
          TestsNeeded := TRUE;
          exceptions^.needIndole := true
        end
        else
        begin
          TestsNeeded := TRUE;
          exceptions^.undefinedBiotype := true;
        end;
  end;
end;

function TWAPanelDump.OKToShowID : boolean;
{**************************************************************************}
{**************************************************************************}
begin
  OKToShowID := false;
  if ((WAPSICmd^.recvMsg^.PSIStat = LampFailure) and
     not(resList^.traysObj^.TrayFlo)) then
    exit;
  if Exceptions^.AbortedMissing then
    exit;
  with flags^ do
    if SkippedWells or
       DelayedRead or
       BadDCB or
       BadOFB or
       NoDispense or
       BadNPC or
       BadBNAC or
       BadSterileWell or
       InvSterileWell or
       ISFGrowth then
      exit;
  with Exceptions^ do
    if NeedOxid or
       (*NoID or         {A NoID exception should display Very Rare Biotype as the org}}*)
       NeedIndole or
       NeedBLAC or
       NeedSts or
       NeedGmS or
       undefinedBiotype then
      exit;

  OKToShowID := true;
end;

function TWAPanelDump.PrintData : boolean;
{************************************************************************}
{************************************************************************}
var
  i : integer;
  wc      : PWaitCursor;
begin
  wc := New(PWaitCursor, Init);
  for i := 0 to 7 do
  begin
    currentRow := i;
    printInfo^.body^.AddRow(r_StartGroup, 0);
    PrintTestIDs;
    if printProcessed then
      PrintProcessedData;
    if printRaw then
      PrintRawData;
    printInfo^.body^.AddRow(r_EndGroup, 0);
  end;
  MSDisposeObj(wc);
  PrintData := inherited PrintData;
end;

function TWAPanelDump.PrintMICs : boolean;
begin
  PrintMICs := (not printRaw) and (printProcessed);
end;

function TWAPanelDump.PanelOK : boolean;
begin
  PanelOK := (WAPSICmd^.recvMsg^.psiStat = processComplete) or
             (WAPSICmd^.recvMsg^.psiStat = MICsAvailable) or
             ((WAPSICmd^.recvMsg^.psiStat = LampFailure) and
             (resList^.traysObj^.TrayFlo));
end;

function TWAPanelDump.PrintProcessedData : boolean;
{************************************************************************}
{************************************************************************}
var
  WAPDICmd : PWAPDICmd;
  labelStr : PChar;
  i        : integer;
begin
  PrintProcessedData := true;
  GetMem(labelStr, 16);
  { print the processed readings }
  SR(IDS_PROC, labelStr, 15); {Proc.}
  WAPDICmd := New(PWAPDICmd, Init(WAID));
  WAPDICmd^.SetParams(panelID, currentRow);
  if WAPDICmd^.SendWAMessage then
  begin
    for i := 1 to 12 do
    begin
      readings[i].reading := WAPDICmd^.recvMsg^.pdiData[i];
      readings[i].outOfCtrl := false;
    end;
    { print the +/- values }
    PrintRowOfReactions(WANoWellVal);
    PrintRowOfReadings(false, labelStr);
  end
  else
  begin
    PrintProcessedData := false;
  end;
  MSDisposeObj(WAPDICmd);
  MSFreeMem(labelStr, 16);
end;

function TWAPanelDump.PrintRawData : boolean;
{************************************************************************}
{************************************************************************}
begin
  PrintRawData := false;
  if PrintRawReadings(0) then
    if PrintRawReadings(1) then
      PrintRawData := true;
end;

function TWAPanelDump.PrintRawReadings(which : integer) : boolean;
{************************************************************************}
{************************************************************************}
const
  NoRead = 32639;
  NoURead = 0;
var
  i,
  filter,
  tempFilter : byte;
  WACmd      : PWARDICmd;
  FilterStr  : PChar;
  filterCntl,
  okFine     : boolean;
  tempword   : word;
  tempLongInt: longint;
begin
  PrintRawReadings := false;
  GetMem(FilterStr, 16);
  WACmd := New(PWARDICmd, Init(WAID));

  InitFilterRec(which);
  for filter := loFilter to hiFilter do
  begin
    tempFilter := filter;
    if tempFilter > 9 then
      tempFilter := tempFilter -10;
    filterCntl := (ROIOffset[tempFilter+1] = -1);
    WACmd^.SetParams(panelID, currentRow, filter);
    if WACmd^.SendWAMessage then
    begin
      for i := 1 to 12 do
      begin
        readings[i].outOfCtrl := false;
        if filterCntl then
        begin
          { sign extends the integer into a long integer }
          readings[i].reading := longint(WACmd^.recvMsg^.rdiData[i]);
          okFine := (readings[i].reading <> NoRead);
        end
        else
        begin
          { pads the high word with zeros, does not carry over the sign }
          tempword := word(WACmd^.recvMsg^.rdiData[i]);
          readings[i].reading := tempword;
          okFine := (readings[i].reading <> NoURead);
          tempword := word(ROIOffset[tempFilter+1]);
          tempLongInt := tempword;
          readings[i].reading := readings[i].reading - tempLongInt * 16;
        end;
        if not okFine then
          readings[i].reading := 0;
      end;
      PrintRowOfReadings(true, filterArray[filter].filterStr);
    end
    else
    begin
      MSFreeMem(FilterStr, 16);
      MSDisposeObj(WACmd);
      exit;
    end;
  end;
  MSDisposeObj(WACmd);
  MSFreeMem(FilterStr, 16);
  PrintRawReadings := true;
end;

procedure TWAPanelDump.InitFilterRec(which : integer);
{************************************************************************}
{************************************************************************}
var
  i : integer;
begin
  if resList^.traysObj^.TrayFlo then
    if which = 1 then
    begin
      loFilter := 18;
      hiFilter := 18;
      StrPCopy(filterArray[18].filterStr, 'F450F');
    end
    else
    begin
      loFilter := 8;
      hiFilter := 8;
      StrPCopy(filterArray[8].filterStr, 'I450F');
    end
  else
    if WA[WAID].waType = ClassicWA then
      if which = 1 then
      begin
        loFilter := 10;
        hiFilter := 15;
        StrPCopy(filterArray[10].filterStr, 'F620');
        StrPCopy(filterArray[11].filterStr, 'F560');
        StrPCopy(filterArray[12].filterStr, 'F505');
        StrPCopy(filterArray[13].filterStr, 'F440');
        StrPCopy(filterArray[14].filterStr, 'F590');
        StrPCopy(filterArray[15].filterStr, 'F620AIR');
      end
      else
      begin
        loFilter := 0;
        hiFilter := 5;
        StrPCopy(filterArray[0].filterStr, 'I620');
        StrPCopy(filterArray[1].filterStr, 'I560');
        StrPCopy(filterArray[2].filterStr, 'I505');
        StrPCopy(filterArray[3].filterStr, 'I440');
        StrPCopy(filterArray[4].filterStr, 'I590');
        StrPCopy(filterArray[5].filterStr, 'I620AIR');
      end
    else { Wa 40/96 has a different order }
      if which = 1 then
      begin
        loFilter := 10;
        hiFilter := 15;
        StrPCopy(filterArray[10].filterStr, 'F440');
        StrPCopy(filterArray[11].filterStr, 'F620');
        StrPCopy(filterArray[12].filterStr, 'F590');
        StrPCopy(filterArray[13].filterStr, 'F560');
        StrPCopy(filterArray[14].filterStr, 'F505');
        StrPCopy(filterArray[15].filterStr, 'F405');
      end
      else
      begin
        loFilter := 0;
        hiFilter := 5;
        StrPCopy(filterArray[0].filterStr, 'I440');
        StrPCopy(filterArray[1].filterStr, 'I620');
        StrPCopy(filterArray[2].filterStr, 'I590');
        StrPCopy(filterArray[3].filterStr, 'I560');
        StrPCopy(filterArray[4].filterStr, 'I505');
        StrPCopy(filterArray[5].filterStr, 'I405');
      end
end;


{ Methods for TAS4PanelDump Object }

constructor TAs4PanelDump.Init(AParent     : PWindowsObject;
                               isoFile,
                               specFile    : PDBFile;
                               res         : PResults;
                               biot        : PBiotypeObject;
                               panelIsRead : boolean;
                               pdlObject   : PPDLObject;
                               as4Object   : PAutoSCANObject);
{************************************************************************}
{************************************************************************}
var
  options : PAS4DumpOptionsDlg;
  i : integer;
begin
  printProcessed := false;
  printRaw := false;
  printCalibrated := false;
  printGains := false;

  if not inherited Init(AParent,isoFile, specFile, res) then
  begin
    Done;
    Fail;
  end;

  options := New(PAS4DumpOptionsDlg,Init(AParent, panelIsRead,
                                         ADDR(PrintProcessed), ADDR(printRaw),
                                         ADDR(printCalibrated), ADDR(printGains)));
  if options^.Run = IDCANCEL then
  begin
    Done;
    Fail;
  end;

  SR(IDS_AS4_TITLE, title, 120);
  bioObj := biot;
  PDLObj := PDLObject;
  AS4Obj := AS4Object;
  If Not AS4Obj^.InControl then
    SR(IDS_AS4_OutOfControl, title, 120);

  for i := 0 to MaxTRWell do
    formData[i] := PDLObj^.formData[i];
  rawData := PDLObj^.rawData;
  for i := 0 to 11 do
    reactions[i+1] := PDLOBJ^.posFlags[i];
  for i := 1 to MaxMICLen do
    MICData[i] := PDLObj^.MICData[i];
end;

function TAS4PanelDump.GetOrganismAndBiotype : boolean;
{************************************************************************}
{************************************************************************}
var
  orgNum   : integer;
  orgDB    : PDBFile;
begin
  GetOrganismAndBiotype := false;
  if inherited GetOrganismAndBiotype then
  begin
    if bioObj <> nil then
    begin
      if resList^.traysObj^.tfsRec.idRec <> -1 then
      begin
        StrPCopy(biotype, bioObj^.biotype);
        if bioObj^.GetTopID(orgNum, nil, nil, nil) then
        begin
          if orgNum = 0 then
            SR(IDS_RARE_BIOTYPE, organism, 63)
          else
          begin
            orgDB := New(PDBFile, Init(DBORGFile, '', DBOpenNormal));
            Str(orgNum, tempStr);
            orgDB^.dbr^.ClearRecord;
            if orgDB^.dbr^.PutFieldAsStr(DBORGOrg, tempStr) then
              if orgDB^.dbc^.GetEQ(orgDB^.dbr) then
                orgDB^.dbr^.GetFieldAsStr(DBORGSName, organism, 255);
            MSDisposeObj(orgDB);
          end;
        end
        else
          SR(IDS_INVALID_BIOTYPE, organism, 63);
      end;
    end;
    GetOrganismAndBiotype := true;
  end;
end;

function TAS4PanelDump.PrintProcessedData : boolean;
{************************************************************************}
{*  Print the AS4 calibrated and run values.                            *}
{************************************************************************}
var
  labelStr : PChar;
  i        : integer;
begin
  PrintProcessedData := false;
  GetMem(labelStr, 16);
  { print the processed readings }
  SR(IDS_PROC, labelStr, 15); {Proc.}
  for i := 1 to 12 do
  begin
    readings[i].reading := longint(formData[(currentRow)+((i-1)*8)]);
    readings[i].outOfCtrl := false;
  end;
  { print the +/- values }
  PrintRowOfReactions(AS4NoWellVal);
  PrintRowOfReadings(false, labelStr);
  PrintProcessedData := true;
  MSFreeMem(labelStr, 16);
end;

function TAS4PanelDump.PrintCalReadings(which : integer) : boolean;
{************************************************************************}
{*  Print the AS4 calibration values.                                   *}
{************************************************************************}
Const
  FlgRawData = 2;  {Indicates raw data is selected instead of cal or gain}
var
  i         : byte;
  filterStr : PChar;
  LocalHiFilter : Integer;
begin
  PrintCalReadings := false;
  GetMem(FilterStr, 16);

  InitFilterRec(which);
  currentFilter := loFilter;
  If (Which = FlgRawData) and (PDLObj^.IsWaterReady) then
    LocalHiFilter := HiFilter      {allow waterblank values to be displayed}
  Else
    LocalHiFilter := HiFilter - 1; {do not print waterblank row for calibration data}

  while currentFilter <= LocalHiFilter do
  begin
    for i := 1 to 12 do
    begin
      if which = 0 then
        readings[i].reading := longint(rawData^[cal, currentFilter, ((currentRow)+((i-1)*8))])
      else
        readings[i].reading := longint(rawData^[run, currentFilter, ((currentRow)+((i-1)*8))]);
      { Check for out of control values }
      readings[i].outOfCtrl := ABS(readings[i].reading-filterArray[currentFilter].calvalue)>
                                   filterArray[currentFilter].calrange;
    end;
    PrintRowOfReadings(true, filterArray[currentFilter].filterStr);
    currentFilter := currentFilter + 1;
  end;
  MSFreeMem(FilterStr, 16);
  PrintCalReadings := true;
end;

function TAS4PanelDump.PrintCalibratedData : boolean;
{************************************************************************}
{************************************************************************}
var
  calValues : boolean;
begin
  PrintCalibratedData := false;
  calValues := true;
  if PrintCalReadings(0) then
    PrintCalibratedData := true;
end;

function TAS4PanelDump.PrintRawData : boolean;
{************************************************************************}
{************************************************************************}
var
  calValues : boolean;
begin
  PrintRawData := false;
  calValues := false;
  if PrintCalReadings(2) then
    PrintRawData := true;
end;

function TAS4PanelDump.PrintGainData : boolean;
{************************************************************************}
{************************************************************************}
var
  i         : byte;
  filterStr : PChar;
begin
  PrintGainData := false;
  GetMem(FilterStr, 16);

  InitFilterRec(1);
  currentFilter := loFilter;
  while currentFilter <= (hiFilter - 1 {do not print waterblank row for gain data}) do
  begin
    for i := 1 to 12 do
      readings[i].reading := longint(AS4Obj^.dacvalues[currentFilter, ((currentRow)+((i-1)*8))]);
    PrintRowOfReadings(true, filterArray[currentFilter].filterStr);
    currentFilter := currentFilter + 1;
  end;
  MSFreeMem(FilterStr, 16);
  PrintGainData := true;
end;

procedure TAS4PanelDump.InitFilterRec(which : integer);
{************************************************************************}
{************************************************************************}
var
  i : integer;
begin
  loFilter := 0;
  hiFilter := 7; {Extended from 6 to 7 to allow printing of waterblank values}
  CASE which OF
    0 : begin
          StrPCopy(filterArray[0].filterStr, 'C620');
          StrPCopy(filterArray[1].filterStr, 'C560');
          StrPCopy(filterArray[2].filterStr, 'C505');
          StrPCopy(filterArray[3].filterStr, 'C470');
          StrPCopy(filterArray[4].filterStr, 'C440');
          StrPCopy(filterArray[5].filterStr, 'C590');
          StrPCopy(filterArray[6].filterStr, 'C590A');
          StrPCopy(filterArray[7].filterStr, 'C590AW');
        end;
    1 : begin
          StrPCopy(filterArray[0].filterStr, 'G620');
          StrPCopy(filterArray[1].filterStr, 'G560');
          StrPCopy(filterArray[2].filterStr, 'G505');
          StrPCopy(filterArray[3].filterStr, 'G470');
          StrPCopy(filterArray[4].filterStr, 'G440');
          StrPCopy(filterArray[5].filterStr, 'G590');
          StrPCopy(filterArray[6].filterStr, 'G590A');
          StrPCopy(filterArray[7].filterStr, 'G590AW');
        end;
    2 : begin
          StrPCopy(filterArray[0].filterStr, 'A620');
          StrPCopy(filterArray[1].filterStr, 'A560');
          StrPCopy(filterArray[2].filterStr, 'A505');
          StrPCopy(filterArray[3].filterStr, 'A470');
          StrPCopy(filterArray[4].filterStr, 'A440');
          StrPCopy(filterArray[5].filterStr, 'A590');
          StrPCopy(filterArray[6].filterStr, 'A590A');
          StrPCopy(filterArray[7].filterStr, 'A590AW');
        end;
  end;
  for i := loFilter to hiFilter do
    with AS4Obj^.FilterDesc[i] do
    begin
      filterArray[i].calValue := calValue;
      filterArray[i].calRange := calRange;
    end;
end;

procedure TAS4PanelDump.PrintNotes;
{************************************************************************}
{************************************************************************}
var
  tempValStr : string;
begin
  printInfo^.body^.AddRow(0, 0);
  printInfo^.body^.AddRow(0, 0);
  SR(IDS_NOTES, printStr, PrintStrLen-1); {NOTES:}
  printInfo^.body^.AddCell(printStr, LT100, c_normal or c_left, 0);
  if printcalibrated or printRaw or printGains then
  begin
    SR(IDS_ASTERISK, fieldVal, PrintStrLen-1); {Values with an asterisk are out of control. }
    printInfo^.body^.AddRow(0, 0);
    printInfo^.body^.AddCell('  ', LT50, c_normal or c_left, 0);
    printInfo^.body^.AddCell(fieldVal, LT700, c_normal or c_left, 0);
    SR(IDS_CALWARNING, fieldVal, PrintStrLen-1);
    printInfo^.body^.AddRow(0, 0);
    printInfo^.body^.AddCell('  ', LT50, c_normal or c_left, 0);
    printInfo^.body^.AddCell(fieldVal, LT700, c_normal or c_left, 0);
  end;
  { Print the threshhold for growth in MIC wells }
  SR(IDS_MICTHRESH, fieldVal, PrintStrLen-1);
  StrLCopy(printStr, fieldVal, PrintStrLen-1);
  Str(PDLObj^.PDLDATA[ORD(thresh)], tempValStr);
  StrPCopy(fieldVal, tempValStr);
  StrLCat(printStr, '  ', PrintStrLen-1);
  StrLCat(printStr, fieldVal, PrintStrLen-1);
  printInfo^.body^.AddRow(0, 0);
  printInfo^.body^.AddCell('  ', LT50, c_normal or c_left, 0);
  printInfo^.body^.AddCell(printStr, LT700, c_normal or c_left, 0);

  { print the average value for no growth wells }
  SR(IDS_AVERAGEGROWTH, fieldVal, PrintStrLen-1);
  StrLCopy(printStr, fieldVal, PrintStrLen-1);
  Str(PDLObj^.PDLDATA[ORD(avgng)], tempValStr);
  StrPCopy(fieldVal, tempValStr);
  StrLCat(printStr, '  ', PrintStrLen-1);
  StrLCat(printStr, fieldVal, PrintStrLen-1);
  printInfo^.body^.AddRow(0, 0);
  printInfo^.body^.AddCell('  ', LT50, c_normal or c_left, 0);
  printInfo^.body^.AddCell(printStr, LT700, c_normal or c_left, 0);

  SR(IDS_NGTHRESH, fieldVal, PrintStrLen-1);
  printInfo^.body^.AddRow(0, 0);
  printInfo^.body^.AddCell('  ', LT50, c_normal or c_left, 0);
  printInfo^.body^.AddCell(fieldVal, LT700, c_normal or c_left, 0);

  SR(IDS_UNUSEDWELLS, fieldVal, PrintStrLen-1);
  printInfo^.body^.AddRow(0, 0);
  printInfo^.body^.AddCell('  ', LT50, c_normal or c_left, 0);
  printInfo^.body^.AddCell(fieldVal, LT700, c_normal or c_left, 0);

  if printProcessed OR printRaw OR printGains then
  begin
    printInfo^.body^.AddRow(0, 0);
    printInfo^.body^.AddRow(0, 0);
    SR(IDS_TECHID, printStr, PrintStrLen-1); {Tech ID:}
    StrLCat(printStr, ' ____________________', PrintStrLen-1);
    printInfo^.body^.AddRow(0, 0);
    printInfo^.body^.AddCell(printStr, LT800, c_normal or c_left, 0);
  end;
end;

procedure TAS4PanelDump.StrReading(rData    : boolean;
                                   readingIdx : integer;
                                   readingStr : PChar);
{************************************************************************}
{************************************************************************}
var
  temp : string;
begin
  if rData then
  begin
    Str(readings[readingIdx].reading, temp);
    StrPCopy(readingStr, temp);
  end
  else
  begin
    if readings[readingIdx].reading = AS4NoWellVal then { well is not used ('empty' is defined in autoscan.pas as $8080}
      StrPCopy(readingStr, '.....')
    else
    begin
      Str(readings[readingIdx].reading, temp);
      if readings[readingIdx].outOfCtrl then
      begin
        if StrLen(printStr) < 6 then
          ConcatBlanks(6-StrLen(tempStr), tempStr);
        StrLCat(tempStr, '*', PrintStrLen-1);
      end;
      StrPCopy(readingStr, temp);
    end;
  end;
end;

function TAS4PanelDump.PrintMICs : boolean;
begin
  PrintMICs := (printProcessed) and (not printRaw) and
               (not printGains) and (not printCalibrated);

end;

function TAS4PanelDump.PrintData : boolean;
{************************************************************************}
{************************************************************************}
var
  i : integer;
begin
  for i := 0 to 7 do
  begin
    currentRow := i;
    printInfo^.body^.AddRow(r_StartGroup, 0);
    if (printProcessed) OR (printRaw) then
      PrintTestIDs;
    if printProcessed then
      PrintProcessedData;
    if printCalibrated then
      PrintCalibratedData;
    if printGains then
      PrintGainData;
    if printRaw then
      PrintRawData;
    printInfo^.body^.AddRow(r_EndGroup, 0);
  end;
  if printProcessed then
    PrintNotes;
  printData := inherited PrintData;
end;


constructor TWADumpOPtionsDlg.Init(aParent: PWindowsObject;
                                   processed, raw : pointer);
begin
  inherited Init(AParent, 'WADUMPOPTIONS');
  p := processed;
  r := raw;
end;

procedure TWADumpOPtionsDlg.OK(var msg: TMessage);
begin
  if SendDlgItemMsg(IDC_PROCESSED, bm_GetCheck, 0, 0) <> 0 then
    p^ := true;
  if SendDlgItemMsg(IDC_RAW, bm_GetCheck, 0, 0) <> 0 then
    r^ := true;
   inherited OK(msg);
end;


constructor TAS4DumpOPtionsDlg.Init(AParent   : PWindowsObject;
                                    panelRead : boolean;
                                    processed,
                                    raw,
                                    calibrated,
                                    gains     : pointer);
begin
  inherited Init(AParent, 'AS4DUMPOPTIONS');
  p := processed;
  r := raw;
  c := calibrated;
  g := gains;
  panelIsRead := panelRead;
end;

procedure TAS4DumpOptionsDlg.SetUpWindow;
begin
  if not panelIsRead then
  begin
    p^ := false;
    r^ := false;
    EnableWindow(GetItemHandle(IDC_PROCESSED), false);
    EnableWindow(GetItemHandle(IDC_RAW), false);
  end;

  EnableWindow(GetItemHandle(IDC_Calibrated), true);
  EnableWindow(GetItemHandle(IDC_Gains), true);

  inherited SetUpWindow;
end;

function TAS4DumpOptionsDlg.Run : integer;
begin
  with Parent^ do
    Run := Application^.ExecDialog(@self);
end;

procedure TAS4DumpOPtionsDlg.OK(var msg: TMessage);
begin
  if SendDlgItemMsg(IDC_PROCESSED, bm_GetCheck, 0, 0) <> 0 then
    p^ := true;
  if SendDlgItemMsg(IDC_RAW, bm_GetCheck, 0, 0) <> 0 then
    r^ := true;
  if SendDlgItemMsg(IDC_CALIBRATED, bm_GetCheck, 0, 0) <> 0 then
    c^ := true;
  if SendDlgItemMsg(IDC_GAINS, bm_GetCheck, 0, 0) <> 0 then
    g^ := true;
  inherited OK(msg);
end;


END.
