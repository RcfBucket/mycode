program Convert;

{- This is the main module for the Nihongo v3.00 convert program.
   This program will convert Nihongo v2.xx and Domestic v18.10 data files
   into the DMS }

uses
  MTask,
  CvtChk,
  IntlLib,
  UserMsgs,
  WinDos,
  Strings,
  SpinEdit,
  APITools,
  Ctl3d,
  IniLib,
  WinTypes,
  Win31,
  WinProcs,
  OWindows,
  ODialogs,
  DlgLib,
  MScan,
  DB2DBIF,
  CRCLib,
  CvtMisc,
  Cvt18,
  Cvt18Map,
  Cvt20,
  Cvt20Map;

{$R CONVERT.RES}
{$I CONVERT.INC}

type
  TCvtApp = object(T3dApplication)
    procedure InitMainWindow; virtual;
  end;

  PMainDlg = ^TMainDlg;
  TMainDlg = object(TCenterDlgWindow)
    constructor Init;
    procedure SetupWindow; virtual;
    function  GetClassName: PChar; virtual;
    procedure GetWindowClass(var aWndClass: TWndClass); virtual;
    procedure IDNext(var msg: TMessage); virtual ID_FIRST + IDC_NEXT;
    procedure IDLog(var msg: TMessage); virtual ID_FIRST + IDC_LOG;
    procedure WMDMSQuery(var msg: TMessage); virtual WM_FIRST + WM_MAINPROGQUERY;
  end;

  PGetDiskDlg = ^TGetDiskDlg;
  TGetDiskDlg = object(TCenterDlg)
    v20Selected : boolean;
    okToSkip    : boolean;
    {}
    constructor Init(aParent: PWindowsObject; isV20, isOKToSkip: boolean);
    procedure SetupWindow; virtual;
    procedure IDNext(var msg: TMessage); virtual ID_FIRST + IDC_NEXT;
    procedure IDSkip(var msg: TMessage); virtual ID_FIRST + IDC_SKIP;
    procedure IDCalc(var msg: TMessage); virtual ID_FIRST + IDC_CALCULATE;
    procedure EnableButtons; virtual;
    function CanClose: boolean; virtual;
    procedure Cancel(var msg: TMessage); virtual ID_FIRST + ID_CANCEL;
  end;

  PCalcDlg = ^TCalcDlg;
  TCalcDlg = object(TCenterDlg)
    spEd    : PSpinInt;
    txtMsg  : PStatic;
    txtreq  : PStatic;
    dbSize  : longint;
    ver18   : boolean;
    {}
    constructor Init(aParent: PWindowsObject; isV18: boolean);
    procedure SetupWindow; virtual;
    procedure DiskChanged;
  end;

  PSpinDisks = ^TSpinDisks;
  TSpinDisks = object(TSpinInt)
    parentAsTCalc : PCalcDlg;
    procedure SpinUp; virtual;
    procedure SpinDown; virtual;
    procedure WMChar(var msg: Tmessage); virtual WM_FIRST + WM_CHAR;
  end;

  PWorkDlg = ^TWorkDlg;
  TWorkDlg = object(TCenterDlg)
    txt   : PChar;
    constructor Init(aParent: PWindowsObject; aText: PChar);
    procedure SetupWindow; virtual;
    destructor Done; virtual;
  end;

constructor TWorkDlg.Init(aParent: PWindowsObject; aText: PChar);
begin
  inherited Init(aParent, MakeIntResource(DLG_WORKING));
  txt:= StrNew(aText);
end;

destructor TWorkDlg.Done;
begin
  StrDispose(txt);
  inherited Done;
end;

procedure TWorkDlg.SetupWindow;
begin
  inherited SetupWindow;
  SetDlgItemText(hWindow, IDC_TEXT, txt);
end;

procedure TCvtApp.InitMainWindow;
begin
  if INIUse3D then
    Register3dApp(true, true, true);
  mainWindow := New(PMainDlg, Init);
  if mainWindow = nil then
  begin
    FatalError('Error', 'Cannot initialize main window');
    Halt;
  end;
end;

constructor TMainDlg.Init;
begin
  inherited Init(nil, MakeIntResource(DLG_CVTMAIN));
end;

procedure TMainDlg.SetupWindow;
begin
  inherited SetupWindow;
  SendDlgItemMsg(IDC_V20BACKUP, BM_SETCHECK, BF_CHECKED, 0);
  SendDlgItemMsg(IDC_V18BACKUP, BM_SETCHECK, BF_UNCHECKED, 0);
end;

function TMainDlg.GetClassName: PChar;
begin
  GetClassName:= 'DMSConvert';
end;

procedure TMainDlg.GetWindowClass(var aWndClass: TWndClass);
begin
  inherited GetWindowClass(aWndClass);
  aWndClass.HIcon:= LoadIcon(HInstance, MakeIntResource(ICON_1));
end;

procedure TMainDlg.WMDMSQuery(var msg: TMessage);
{- respond to the task module and tell it who I am }
begin
  StrLCopy(PChar(msg.lParam), application^.name, msg.wParam);
  msg.result:= DMSMainProgRetCode;
  SetWindowLong(HWindow, DWL_MSGRESULT, msg.result);  {- set message result (for dialogs) }
end;

procedure TMainDlg.IDNext(var msg: TMessage);
var
  v20Checked  : boolean;
  dlg         : PDialog;
  start       : longint;
  ver         : TVersionTypes;
  crc         : word;     {- stored CRC for selected version }
  calcCRC     : word;     {- newly calculated CRC for OldDataDir }
  okToSkip    : boolean;
  path        : array[0..100] of char;
begin
  v20Checked:= SendDlgItemMsg(IDC_V20BACKUP, BM_GETCHECK, 0,0) = BF_CHECKED;
  if v20Checked then
    ver:= Version20
  else
    ver:= Version18;

  LogStart;

  {- at this point, check the OldDataDir and calculate the checksum for all files.
     if they match what is in the CRC file then skip can be enabled on the
     next step. }
  if GetLastCRC(ver, crc) then
  begin
    StrCopy(path, curDMSDir);
    StrCat(path, oldDataDir);
    dlg:= New(PWorkDlg, Init(@self, 'Calculating checksums, Please wait...'));
    application^.MakeWindow(dlg);
    if not GetDirCRC(path, calcCRC) then
      okToSkip:= false
    else
      okToSkip:= (calcCRC = crc);
    Dispose(dlg, Done);
  end
  else
    okToSkip:= false;

  ShowWindow(hWindow, SW_HIDE);
  application^.ExecDialog(
          New(PGetDiskDlg, Init(@self, v20Checked, okToSkip)));
  {- the only way to get to this point is that the user pressed exit somewhere
     in the chain of windows, so lets apease his/her request }
  CloseWindow;
end;

procedure TMainDlg.IDLog(var msg: TMessage);
begin
  ShowLogFile(@self);
end;

{----------------------------------------------------------[ TGetDiskDlg ] --}

constructor TGetDiskDlg.Init(aParent: PWindowsObject; isV20, isOKToSkip: boolean);
begin
  inherited Init(aParent, MakeIntResource(DLG_PROMPT4DISK));
  v20Selected:= isV20;
  okToSkip:= isOKToSkip;
end;

procedure TGetDiskDlg.SetupWindow;
var
  pstr  : array[0..200] of char;
  k     : longint;
begin
  inherited SetupWindow;
  k:= DiskFree(0);
  if IOResult <> 0 then
    StrCopy(pstr, '***')
  else
    IntlIntToStr(k, pstr, sizeof(pstr)-1);
  SendDlgItemMsg(IDC_FREESPACETXT, WM_SETTEXT, 0, longint(@pstr));
end;

procedure TGetDiskDlg.EnableButtons;
begin
  EnableWindow(GetItemHandle(IDC_SKIP), okToSKip);
end;

function TGetDiskDlg.CanClose: boolean;
begin
  CanClose:= ConfirmExit(hWIndow);
end;

procedure TGetDiskDlg.Cancel(var msg: TMessage);
begin
  if CanClose then
    inherited Cancel(msg);
end;

procedure TGetDiskDlg.IDCalc(var msg: TMessage);
begin
  application^.ExecDialog(New(PCalcDlg, Init(@self, not v20Selected)));
end;

procedure TGetDiskDlg.IDSkip(var msg: TMessage);
var
  pstr  : array[0..100] of char;
  dlg   : PWorkDlg;
begin
  if not okToSkip then exit;
  dlg:= New(PWorkDlg, Init(@self, 'Preparing Hard Disk, Please wait...'));
  application^.MakeWindow(dlg);
  if PrepareMergeDir = 0 then
  begin
    Dispose(dlg, Done);
    ShowWindow(hWindow, SW_HIDE);
    if v20selected then
    begin
      StrCopy(pstr, curDMSDir);
      StrCat(pstr, oldDataDir);
      if pstr[StrLen(pstr)-1] = '\' then
        pstr[StrLen(pstr)-1]:= #0;
      SetDbifDir(pstr);
      MapV20Fields(@self);
    end
    else
      MapV18Fields(@self);
    EndDlg(IDOK);
  end
  else
  begin
    Dispose(dlg, Done);
    ErrorMsg(hWindow, 'Error', 'Error preparing hard disk');
  end;
end;

procedure TGetDiskDlg.IDNext(var msg: TMessage);
begin
  ShowWindow(hWindow, SW_HIDE);
  if v20selected then
    Get20Data(@self)
  else
    Get18Data(@self);
  EndDlg(IDOK);
end;

procedure TSpinDisks.SpinUp;
begin
  inherited SpinUp;
  parentAsTCalc^.DiskChanged;
end;

procedure TSpinDisks.SpinDown;
begin
  inherited SpinDown;
  parentAsTCalc^.DiskChanged;
end;

procedure TSpinDisks.WMChar(var msg: TMessage);
begin
  inherited WMChar(msg);
  parentAsTCalc^.DiskChanged;
end;

constructor TCalcDlg.Init(aParent: PWindowsObject; isV18: boolean);

  procedure CalcDBSize;
  {- calculate the size of the existing database }
  var
    dstr  : array[0..100] of char;
    sr    : TSearchRec;
  begin
    StrCopy(dstr, CurDMSDir);
    StrCat(dstr, '*.DB?');
    FindFirst(dstr, faArchive, sr);
    dbSize:= 0;
    while DosError = 0 do
    begin
      Inc(dbSize, sr.size);
      FindNext(sr);
    end
  end;

begin
  inherited Init(aParent, MakeIntResource(DLG_CALCULATOR));
  ver18:= isV18;
  spEd:= New(PSpinDisks, InitResource(@self, IDC_DISKS, 4, 0, 0, 500, 1));
  PSpinDisks(spEd)^.parentAsTCalc:= @self;
  txtMsg:= New(PStatic, InitResource(@self, LBL_ENOUGHSPACEMSG, 0));
  txtReq:= New(PStatic, InitResource(@self, LBL_SPACEREQ, 0));
  CalcDBSize;
end;

procedure TCalcDlg.SetupWindow;
var
  pstr    : array[0..100] of char;
  tstr    : string[2];
  k       : longint;

begin
  inherited SetupWindow;
  txtMsg^.SetText(nil);
  txtReq^.SetText('0');
  spEd^.SetNum(0);
  GetDir(0, tstr);
  StrPCopy(pstr, tstr);
  SendDlgItemMsg(LBL_DRIVELETTER, WM_SETTEXT, 0, longint(@pstr));
  {$I-}
  k:= DiskFree(0);
  if IOResult <> 0 then
    StrCopy(pstr, '***')
  else
    IntlIntToStr(k, pstr, sizeof(pstr)-1);
  {$I+}
  SendDlgItemMsg(LBL_SPACEFREE, WM_SETTEXT, 0, longint(@pstr));
end;

procedure TCalcDlg.DiskChanged;
var
  k   : longint;
  pstr: array[0..100] of char;
begin
  if spEd^.NumInRange then
  begin
    k:= spEd^.GetLong;
    if ver18 then
      k:= Trunc(k * 1457664 * 3.5) + dbSize
    else
      k:= (k * 2 * 1457664) + dbSize;
    IntlIntToStr(k, pstr, sizeof(pstr)-1);
    txtReq^.SetText(pstr);
    if k <= DiskFree(0) then
      txtMsg^.SetText('You have enough disk space for the number of disks specified.')
    else
      txtMsg^.SetText('You DO NOT have enough disk space for the number of disks specified.');
  end
  else
  begin
    txtReq^.SetText('***');
    txtMsg^.SetText(nil);
  end;
end;

var
  cvtApp  : TCvtApp;
  exitSave: pointer;

procedure ExitRoutine; far;
var
  pstr  : array[0..100] of char;
  p2    : array[0..100] of char;
begin
  exitProc:= exitSave;
  if ParamStr(1) = 'DEBUG' then
    exit;
  StrCopy(pstr, curDMSDir);
  StrCat(pstr, mergeDataDir);
  if not CleanDir(pstr, '*.*') then
  begin
    StrCopy(p2, 'Error cleaning up merge directory: ');
    StrCat(p2, pstr);
    FatalError('Error', p2);
  end;
end;

BEGIN
  exitSave:= exitProc;
  exitProc:= @ExitRoutine;
  CheckDMSFiles;
  if OKToRun(MOD_CONVERT, false) then
  begin
    cvtApp.Init(GetModuleName(MOD_CONVERT));
    cvtApp.Run;
    cvtApp.Done;
  end;
END.

{- rcf }
