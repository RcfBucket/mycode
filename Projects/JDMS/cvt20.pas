{$I-}

unit Cvt20;

INTERFACE

uses
  OWindows;

procedure Get20Data(aParent: PWindowsObject);

IMPLEMENTATION

uses
  StrsW,
  DMSErr,
  APITools,
  MScan,
  CRCLib,
  Cvt20Map,
  UserMsgs,
  Strings,
  CvtMisc,
  Win31,
  WinProcs,
  WinDOS,
  WinTypes,
  ODialogs,
  DB2Dbif,
  Cvt20Lib,
  DlgLib;

{$R CVT20.RES}
{$I CVT20.INC}

const
  pathFlag      = $46;
  fileFlag      = $22;

  fileNameXref  = 'CVT~XREF.C$T'; {- name of file name xref file }

type
  {- backup header in CONTROL.xxx.
     Byte   Description
     ----   ------------------------
     0      ID byte?? value seems to always be $8B
     1-8    contains the string "BACKUP  "
     9-10   Disk number
     11-138 Zeros          }
  THeader = array[0..138] of byte;

  {- TDirRec describes entries in the CONTROL.xxx file. Entries can be
     path information or file information. if the record is a path record,
     then there is an addition 36 bytes following the record (see TPathTail).

     recLen       - This is the length of the record. It is $46 for path recs
                    and $22 for file entries. This field can be used as a
                    flag rather than a len, to tell what kind of record it is
     variant 1
      path - the path that the following files were backed up from.
     variant 2
      fileName    - the file name. This field is only 12 bytes. So be careful
                    when a file name is exactly 12 bytes long. Don't use this
                    field directly, first copy it to another string and ensure
                    that a length byte is set or a terminating null.
      tag         - This field tracks the blocks of the file. The values I've
                    seen are 3 and 2.
                    3 = The entire file exists on the current disk.
                    2 = The remainder of the file exists on the next disk of the backup.
      size        - This is the actual file size (in bytes)
      blockNum    - Tracks the number of "blocks" that the file has been split
                    into. Ie, if the file is contained entirely on one disk, this
                    number will be 1. If the file is split up on 3 disks, then
                    the first disk will be 1, 2nd disk will be 2, and the third
                    will be 3. and so on
      fOffset     - This is the byte offset into the BACKUP.xxx file for the
                    specified file. To locate the file in BACKUP.xxx, seek to
                    this offset.
      bytesOnDisk - This entry specifies the number of bytes of the file that
                    are actually on the disk. If the file spans to the next
                    disk, this will be the number of bytes of the file that
                    made it to the current disk.
      attr        - This is the original file attribute. This is a word value
                    so the Hi word may not have anything to do with the attribute.
                    It's alway zero anyway. The attr is in the lo word.
      dateTime    - This is a Packed date and time for the file. The standard
                    UnPackTime() function in WinDOS will extract the values.
  }
  TDirRec = record
    recLen  : byte;
    case integer of
      1:  (path       : array[0..32] of char);
      2:  (fileName   : array[0..11] of char;
           tag        : byte;     {- 3 = entire file is on disk
                                     2 = file is continued on next disk }
           size       : longint;
           blockNum   : integer;
           fOffset    : longint;
           bytesOnDisk: longint;
           attr       : word;
           dateTime   : longint;
          );
  end;

  {- Path tail is a 36 byte chunk that follows a path record. If the path is
     longer than 33 bytes, the remaining path is in here. Also the number of
     files contained on the current backup disk }
  TPathTail = record
    remainPath  : array[0..29] of char;
    numFiles    : integer;
    unknown     : longint;
  end;

  PCvt20Dlg = ^TCvt20Dlg;
  TCvt20Dlg = object(TCenterDlg)
    constructor Init(aParent: PWindowsObject);
    destructor Done; virtual;
    procedure SetupWindow; virtual;
    procedure IDNext(var msg: TMessage); virtual ID_FIRST + IDC_NEXT;
    procedure StartProcess(var msg: TMessage); virtual WM_FIRST + WM_FMPROCESS;
    function CanClose: boolean; virtual;
    procedure Cancel(var msg: TMessage); virtual ID_FIRST + ID_CANCEL;

    procedure EnableButtons; virtual;
    function MakeExt(num: integer): string;
    procedure PerformRestore;
    private
    started   : boolean;
    working   : boolean;
    diskNum   : integer;
    list      : PListBox;
  end;

constructor TCvt20Dlg.Init(aParent: PWindowsObject);
begin
  inherited Init(aParent, MakeIntResource(DLG_CVT20));
  list:= New(PListBox, InitResource(@self, IDC_STATLIST));
  started:= false;
  diskNum:= 1;
  working:= true;
end;

destructor TCvt20Dlg.Done;
begin
  inherited Done;
end;

procedure TCvt20Dlg.SetupWindow;
begin
  inherited SetupWindow;
  PostMessage(hWindow, WM_FMPROCESS, 0, 0);
end;

procedure TCvt20Dlg.EnableButtons;
begin
  EnableWindow(GetItemHandle(IDC_NEXT), not working);
  EnableWindow(GetItemHandle(ID_OK), not working);
end;

procedure TCvt20Dlg.StartProcess(var msg: TMessage);
var
  ret   : integer;
  pstr  : array[0..100] of char;
  tw    : TWaitCursor;

  procedure ClearADir(aDir: PChar);
  begin
    StrCopy(pstr, curDMSDir);
    StrCat(pstr, aDir);
    if not CleanDir(pstr, '*.*') then
      ret:= -1;
  end;

begin
  {- just a litte check to protect against an errant message sent to this
     window that happens to cooincide with the user message for this func }
  if not started then
  begin
    tw.Init;

    ret:= 0;
    started:= true;

    ClearCRC(Version20);

    ret:= PrepareMergeDir;

    {- now clear out the restore directory }
    if ret = 0 then
      ClearADir(oldDataDir);

    tw.Done;

    {- preparation complete }
    SR(IDS_PREPCOMPLETE, pstr, sizeof(pstr)-1);
    SetDlgItemText(hWindow, LBL_MSG, pstr);

    if ret = 0 then
      PerformRestore
    else
    begin
      ErrorMsg(hWindow, 'Error', 'Error preparing hard disk');
      working:= false;
      EndDlg(IDOK);
    end;
  end;
end;

procedure TCvt20Dlg.PerformRestore;
var
  p1, p2    : array[0..200] of char;
  okToCont  : boolean;
  tw        : TWaitCursor;
  dRec      : TDirRec;
  oldFMode  : integer;
  oldEMode  : integer;
  ctlFile   : file;     {- CONTROL.xxx file }
  xrefFile  : Text;
  crc       : word;
  diskError : boolean;
  moreDisks : boolean;
  destFile  : array[0..20] of char;

  function CheckDisk(dNum: integer): boolean;
  {- verify that the disk in the floppy drive matches the backup disk num
    specified. Function will open the CONTROL.XXX file and position file pointer
    on first dir entry if valid backup is in drive. Returns false if invalid
    disk found }
  var
    ret     : boolean;
    fname   : string[80];
    header  : THeader;
    numRead : word;
    num     : integer;
  begin
    fname:= 'A:\CONTROL.' + MakeExt(dNum);
    fileMode:= 0;
    Assign(ctlFile, fname);
    Reset(ctlFile, 1);
    if IOResult <> 0 then
    begin
      CheckDisk:= false;
      exit;
    end;

    ret:= false;
    BlockRead(ctlFile, header, sizeof(header), numRead);
    if numRead = sizeof(header) then
    begin
      fname[0]:= #8;
      Move(header[1], fname[1], 8);
      if fname = 'BACKUP  ' then
      begin
        Move(header[9], num, 2);
        if num = dNum then
          ret:= true;
      end;
    end;

    if not ret then
    begin
      Close(ctlFile);
      if IOResult <> 0 then ;
    end;
    CheckDisk:= ret;
  end;

  procedure ExtractFileName(aStr: PChar; maxLen: integer);
  {- extract file name - if file name does not contain valid chars then convert
     it to a valid file name and then store it in the file name XRef }
  var
    temp  : array[0..15] of char;
    k     : integer;
    tstr  : array[0..80] of char;
  begin
    FillChar(temp, sizeof(temp), 0);
    StrLCopy(temp, dRec.fileName, 12);

    for k:= 0 to StrLen(temp) - 1 do
    begin
      if Ord(temp[k]) >= 127 then
      begin
        writeln(xrefFile, temp);
        StrCopy(tstr, curDMSDir);
        StrCat(tstr, OldDataDir);
        GetUniqueFileName(tstr, temp);
        writeln(xrefFile, temp);
        break;
      end;
    end;
    StrLCopy(aStr, temp, maxLen);
  end;

  function ReadDRec: boolean;
  {- reads a directory entry from the CONTROL.xxx file }
  var
    numRead   : word;
    ret       : boolean;
    pTail     : TPathTail;
  begin
    ret:= false;
    BlockRead(ctlFile, dRec, sizeof(TDirRec), numRead);
    if numRead = sizeof(TDirRec) then
    begin
      {- path record read. Ignore it and also read the path tail }
      if drec.recLen = pathFlag then
      begin
        BlockRead(ctlFile, pTail, sizeof(TPathTail), numRead);
        if numRead = sizeof(TPathTail) then
        begin
          BlockRead(ctlFile, dRec, sizeof(TDirRec), numRead);
          if numRead = sizeof(TDirRec) then
            ret:= true;
        end;
      end
      else
        ret:= true;
    end;
    ReadDRec:= ret;
  end;

  function CopyBackFile(aDestName: PChar): boolean;
  {- copy file specified in Drec to destDir }
  const
    bufSize = 65520;
  var
    dest    : file;
    src     : file;
    buffer  : pointer;
    numread : word;
    numWrit : word;
    remains : longint;
    size    : word;
    ret     : boolean;

    function OpenBackFiles: boolean;
    {- open the BACKUP.xxx file and the destination file. If this dir entry
       is describing the 2nd (or greater) block for the file, open the dest
       file in append mode }
    var
      tstr    : array[0..100] of char;
      temp    : array[0..20] of char;
      err     : boolean;
    begin
      err:= true;
      FileMode:= 0;  {- read only }
      Assign(src, 'A:\BACKUP.' + MakeExt(diskNum));
      Reset(src, 1);
      if IoResult = 0 then
      begin
        {- now open the destination file }
        FileMode:= 2;  {- read/write }
        StrCopy(tstr, curDMSDir);
        StrCat(tstr, OldDataDir);
        StrCat(tstr, aDestName);
        Assign(dest, tstr);
        if dRec.blockNum > 1 then   {- if this is not the first block for the file}
        begin
          Reset(dest, 1);  {- open the file for appending }
          if IOResult = 0 then
          begin
            Seek(dest, FileSize(dest));   {- go to end of file }
            if IOResult = 0 then
              err:= false;
          end;
        end
        else
        begin
          Rewrite(dest, 1);
          if IoResult = 0 then
            err:= false;
        end;
      end;
      if err then
      begin
        Close(src);
        if IoResult <> 0 then ;
        Close(dest);
        if IoResult <> 0 then ;
      end;
      OpenBackFiles:= not err;
    end;

  begin  {- CopyBackFile }
    {- make sure that the record is pointing to a file entry }
    if drec.recLen <> FileFlag then
    begin
      CopyBackFile:= false;
      exit;
    end;

    ret:= OpenBackFiles;
    if ret then
    begin
      GetMem(buffer, bufSize);    {- get an I/O buffer }

      Seek(src, dRec.fOffset);    {- locate file in BACKUP.xxx }
      ret:= IoResult = 0;

      {- now start transfering the file }
      if ret then
      begin
        remains:= dRec.bytesOnDisk;
        repeat
          {- figure out the block transfer size. If the # of bytes remaining is
             larger than my max block size, then the file must be read in pieces}
          if remains > bufSize then
            size:= bufSize
          else
            size:= word(remains);

          BlockRead(src, buffer^, size, numRead);    {- read a piece }
          if numRead <> size then
            ret:= false
          else
          begin
            BlockWrite(dest, buffer^, size, numwrit);
            if numWrit <> size then
              ret:= false;  {- disk full ?}
          end;
          Dec(remains, size);
        until not ret or (remains <= 0);
      end;

      {- clean up }
      FreeMem(buffer, bufSize);
      Close(src);
      if IoResult <> 0 then ;
      Close(dest);
      if IoResult <> 0 then ;
    end;
    CopyBackFile:= ret;
  end;

  function UpdateFileNames: boolean;
  var
    ret         : boolean;
    oldF, newF  : string[80];
    pstr        : array[0..80] of char;

    procedure RemoveDot(var s: string);
    var
      k   : integer;
    begin
      k:= Pos('.', s);
      if k > 0 then
        s[0]:= Chr(k-1);
    end;

  begin
    StrCopy(pstr, curDMSDir);
    StrCat(pstr, oldDataDir);
    if pstr[StrLen(pstr)-1] = '\' then
      pstr[StrLen(pstr)-1]:= #0;
    SetDbifDir(pstr);

    ret:= true;
    Reset(xRefFile);
    if IoResult = 0 then
    begin
      repeat
        readln(xRefFile, oldF);
        if oldF <> '' then
        begin
          RemoveDot(oldF);
          readln(xRefFile, newF);
          RemoveDot(newF);
          ret:= DB2RenameFile(oldF, newF);
        end;
      until Eof(xrefFile) or (oldF = '') or not ret;
      Close(xrefFile);
    end;
    UpdateFileNames:= ret;
  end;

begin
  StrCopy(p1, curDMSDir);
  StrCat(p1, oldDataDir);
  AppendBackSlash(p1);
  StrCat(p1, fileNameXref);
  Assign(xrefFile, p1);
  Rewrite(xrefFile);
  if IoResult <> 0 then
  begin
    ErrorMsg(hWindow, 'Error', 'Cannot create file name XREF file');
    exit;
  end;

  moreDisks:= true;
  oldFMode:= FileMode;
  oldEMode:= SetErrorMode(SEM_FAILCRITICALERRORS);
  diskError:= false;
  repeat
    AddNumToString(IDS_INSERTDISK, diskNum, p2, sizeof(p2)-1);
    SR(IDS_TITLE, p1, sizeof(p1)-1);
    MessageBeep(0);
    okToCont:= PromptForDisk(hWindow, p1, p2, false);
    if okToCont then
    begin
      tw.Init;
      AddNumToString(IDS_RESTOREFLOPPY, diskNum, p1, sizeof(p1));
      SetDlgItemText(hWindow, LBL_MSG, p1);
      repeat
        if CheckDisk(diskNum) then
        begin
          diskError:= false;
          moreDisks:= false;
          repeat
            if ReadDRec then
            begin
              ExtractFileName(destFile, sizeof(destFile)-1);
              AddNameToList(destFile, list);
              moreDisks:= dRec.tag = 2;
              if not CopyBackFile(destFile) then
              begin
                StrCopy(p2, 'Cannot restore file ');
                StrCat(p2, destFile);
                StrCat(p2, ' from backup file.');
                ErrorMsg(hWindow, 'Error', p2);
                okToCont:= false;
              end;
            end
            else
            begin
              ErrorMsg(hWindow, 'Error', 'Cannot read backup control file.');
              okToCont:= false;
            end;
          until not okToCont or Eof(ctlFile);
          Close(ctlFile);
          if IOResult <> 0 then ;

          Inc(diskNum);
        end
        else
        begin
          diskError:= true;
          SR(IDS_INVALIDDISK, p1, sizeof(p1)-1);
          StrCat(p1, #10#10);
          AddNumToString(IDS_INSERTDISK, diskNum, p2, sizeof(p2));
          StrCat(p1, p2);
          MessageBeep(0);
          okToCont:= PromptForDisk(hWindow, 'Error', p1, true);
        end;
      until not okToCont or not diskError or not moreDisks;
      tw.Done;
    end;
  until not okToCont or not moreDisks;

  Close(xrefFile);
  if IoResult <> 0 then ;

  FileMode:= oldFMode;
  SetErrorMode(oldEMode);
  working:= false;

  FocusCtl(hWindow, IDC_NEXT);
  if not okToCont then
    EndDlg(IDOK)
  else
  begin
    {- data now on hard disk. Time to update CRCs and rename any dbif files that
       had invalid file names }
    tw.Init;
    if UpdateFileNames then
    begin
      StrCopy(p1, curDMSDir);
      StrCat(p1, oldDataDir);
      if GetDirCRC(p1, crc) then
        SetLastCRC(Version20, crc);
      tw.Done;
      SR(IDS_RESTORECOMP, p1, sizeof(p1)-1);
      SetDlgItemText(hWindow, LBL_MSG, p1);
      MessageBeep(0);
      EnableButtons;
    end
    else
    begin
      ErrorMsg(hWindow, 'Error', 'Unable to update file names.');
      EndDlg(idOK);
    end;
  end;
end;

procedure TCvt20Dlg.IDNext(var msg: TMessage);
begin
  ShowWindow(hWindow, SW_HIDE);
  MapV20Fields(@self);
  EndDlg(IDOK);
end;

function TCvt20Dlg.MakeExt(num: integer): string;
{- make an extension for num. ie, 1 = "001" }
var
  k       : integer;
  tstr    : string[10];
begin
  Str(num:3, tstr);   {- convert disk number to extension for CONTROL.xxx file}
  for k:= 1 to 3 do
    if tstr[k] = ' ' then
      tstr[k]:= '0';
  MakeExt:= tstr;
end;

function TCvt20Dlg.CanClose: boolean;
begin
  if not working then
    CanClose:= ConfirmExit(hWindow)
  else
    CanClose:= false;
end;

procedure TCvt20Dlg.Cancel(var msg: TMessage);
begin
  if CanClose then
    inherited Cancel(msg);
end;

procedure Get20Data(aParent: PWindowsObject);
var
  pstr  : array[0..100] of char;
begin
  application^.ExecDialog(New(PCvt20Dlg, Init(aParent)));
end;

END.

{- rcf}
