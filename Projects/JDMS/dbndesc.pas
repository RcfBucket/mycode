{----------------------------------------------------------------------------}
{  Module Name   : DBNameDesc                                                }
{  Programmer    : DWC                                                       }
{                                                                            }
{  Purpose -                                                                 }
{                                                                            }
{  Assumptions -                                                             }
{                                                                            }
{  Initialization -                                                          }
{                                                                            }
{  Revision History -                                                        }
{                                                                            }
{  Version  Date            Who       Did What                               }
{  -------  --------        ------    -------------------------------------- }
{  1.00     03/15/95        DWC       Initial release                        }
{                                                                            }
{----------------------------------------------------------------------------}
{$I-}

unit DBNDesc;

INTERFACE

uses
  MScan,
  Objects;

type
  PText = ^Text;

  PDBNameDescriptor = ^TDBNameDescriptor;
  TDBNameDescriptor = object(TObject)
    constructor Init(aDescFile, aPath, aPasFile: PChar; outf: PText; fMode: char);
    destructor Done; virtual;
    function  GetFileDescription(aFileNameID, aFileName: PChar): PChar;
    function  GetFieldDescription(aFieldID: PChar; aFieldNum: integer): PChar;
    function  GetFieldNumber(aFieldID: PChar): PChar;
    procedure MakeAssociation(aFileName: PChar; aFieldNum: integer);
    procedure SetBDFileName(aFileName: PChar);
    function  GetDelAssoc(aFieldID: PChar): PChar;
    procedure PrintAll(p: PCollection);
    procedure MakePas;
    procedure InsertUDField(k: integer; var fldNum: integer);
    procedure InsertDBKey(keydesc: PChar; var keyNum: integer);
    procedure InsertBlank;
    procedure InsertSpecialConst;

  private
    dbFileName   : array[0..255] of char;
    unitName     : PChar;
    dbIDSFile    : Text;
    descFile     : Text;
    outFile      : PText;
    filMode      : char;
    nameDescList : PCollection;
    pasUnit      : PCollection;
    procedure ReadDescriptionFile;
    procedure RemoveExtension;
    procedure LoadPasUnitCollection;
  end;

IMPLEMENTATION

uses
  Strsw,
  DBTypes,
  WinProcs,
  WinDos,
  Strings,
  DMSDebug;

const
  MaxStr = 50;

type
  PNameDesc = ^TNameDesc;
  TNameDesc = object(TObject)
    constructor Init(newNameID, newNameDesc: PChar);
    destructor  Done; virtual;
    procedure   PrintToScreen; virtual;
    private
    nameID   : PChar;
    nameDesc : Pchar;
  end;

  PIDUnit = ^TIDUnit;
  TIDUnit = object(TObject)
    constructor Init(newID, newConst: PChar);
    destructor  Done; virtual;
    procedure   Print(f1, f2: PText); virtual;
    procedure   PrintToScreen; virtual;
    private
    nameID    : PChar;
    nameConst : Pchar;
  end;

constructor TDBNameDescriptor.Init(aDescFile, aPath, aPasFile: PChar;
                                   outf: PText; fMode: char);
var
  tmp     : PChar;
  tmpfile : array[0..255] of char;
begin
  nameDescList:= New(PCollection, Init(150, 20));
  pasUnit:= New(PCollection, Init(150, 20));

  GetMem(unitName, MaxStr);
  StrCopy(unitName, aPasFile);

  outFile:= outf;

  Assign(descFile, aDescFile);
  Reset(descFile);

  aPasFile:= ansiUpper(aPasFile);
  if(StrPos(aPasFile, '.PAS') = nil) then
    StrCat(aPasfile, '.PAS');

  StrCopy(tmpFile, '');
  StrCat(tmpFile, aPath);
  StrCat(tmpFile, '\');
  StrCat(tmpFile, aPasFile);

  Assign (dbIDSFile, tmpFile);
  SetFAttr(dbIDSFile, 0);
  filMode:= fMode;
  if (fMode = 'O') then  (* OverWrite *)
    Rewrite(dbIDSFile)
  else
  begin
    Reset(dbIDSFile);
    LoadPasUnitCollection;
    ReWrite(dbIDSFile);
  end;
  ReadDescriptionFile;

  (*PrintAll*);
 end;

destructor TDBNameDescriptor.Done;
begin
  Close(descFile);
  Close(dbIDSFile);

  MSFreeMem(unitName, MaxStr);
  MSDisposeObj(pasUnit);
  MSDisposeObj(nameDescList);
end;

{------------------------[ GetFileDescription ]-------------------------}
{-----------------------------------------------------------------------}
{                                                                       }
{-----------------------------------------------------------------------}
function TDBNameDescriptor.GetFileDescription(aFileNameID,
                                                aFileName: PChar): PChar;

  function  MatchID(id: PNameDesc): Boolean; far;
  begin
    MatchID:= StrPos(id^.nameID, aFileNameID) <> nil;
  end;

var
  foundID   : PNameDesc;
  filePChar : array[0..255] of char;
begin
  foundID:= nameDescList^.FirstThat(@MatchID);
  if foundID = nil then
  begin
    GetFileDescription:= nil;
    (*writeln('No Description found');*)
  end
  else
  begin
    GetFileDescription:= foundID^.nameDesc;
    StrCopy(filePChar, '');
    StrCat(filePChar, '''');
    StrCat(filePChar, aFileName);
    StrCat(filePChar, '''');
    pasUnit^.Insert(New(PIDUnit, Init(aFileNameID, filePChar)));

    (*writeln('Found Description');*)
  end;
end;

{------------------------[ GetFieldDescription ]------------------------}
{-----------------------------------------------------------------------}
{                                                                       }
{-----------------------------------------------------------------------}
function TDBNameDescriptor.GetFieldDescription(aFieldID: PChar;
                                                aFieldNum: integer): PChar;

  function  MatchID(id: PNameDesc): Boolean; far;
  begin
    MatchID:= StrPos(id^.nameID, aFieldID) <> nil;
  end;

var
  foundID : PNameDesc;
  numPChar: array[0..6] of char;

begin
  foundID:= nameDescList^.FirstThat(@MatchID);
  if foundID = nil then
  begin
    GetFieldDescription:= nil;
    (*writeln('No Description found');*)
  end
  else
  begin
    GetFieldDescription:= foundID^.nameDesc;
    Str(aFieldNum, numPChar);

    pasUnit^.Insert(New(PIDUnit, Init(aFieldID, numPChar)));

    (* writeln('Found Description');*)
  end;
end;

{------------------------[ GetFieldNumber ]-----------------------------}
{-----------------------------------------------------------------------}
{                                                                       }
{-----------------------------------------------------------------------}
function TDBNameDescriptor.GetFieldNumber(aFieldID: PChar): PChar;

  function  MatchID(id: PIDUnit): Boolean; far;
  begin
    MatchID:= StrPos(id^.nameID, aFieldID) <> nil;
  end;

var
  foundID  : PIDUnit;
  tmpPChar : PChar;
begin
  foundID:= pasUnit^.FirstThat(@MatchID);
  if foundID = nil then (* No Description found *)
    tmpPChar:= aFieldID
  else (* Found Description *)
    tmpPChar:= foundID^.nameConst;
  GetFieldNumber:= tmpPChar;
end;

{------------------------[ MakeAssociation ]----------------------------}
{-----------------------------------------------------------------------}
{                                                                       }
{-----------------------------------------------------------------------}
procedure TDBNameDescriptor.MakeAssociation(aFileName: PChar;
                                             aFieldNum: integer);
var
  filePChar : array[0..255] of char;
  tempPChar : array[0..255] of char;
  numPChar  : array[0..6] of char;

begin
  StrCopy(tempPChar, aFileName);
  ansiUpper(tempPChar);

  StrCopy(filePChar, '');
  StrCat(filePChar, 'DB');
  StrCat(filePChar, dbFileName);
  StrCat(filePChar, '_');
  StrCat(filePChar, tempPChar);
  StrCat(filePChar, '_ASSOC');

  Str(aFieldNum, numPChar);

  pasUnit^.Insert(New(PIDUnit, Init(filePChar, numPChar)));
end;

{------------------------[ GetDelAssoc ]--------------------------------}
{-----------------------------------------------------------------------}
{                                                                       }
{-----------------------------------------------------------------------}
function TDBNameDescriptor.GetDelAssoc(aFieldID: PChar): PChar;

  function  MatchID(id: PNameDesc): Boolean; far;
  begin
    MatchID:= StrPos(id^.nameID, aFieldID) <> nil;
  end;

var
  foundID : PNameDesc;

begin
  foundID:= nameDescList^.FirstThat(@MatchID);
  if foundID = nil then
    GetDelAssoc:= nil
  else
    GetDelAssoc:= foundID^.nameDesc;
end;


{------------------------[ SetBDFileName ]------------------------------}
{-----------------------------------------------------------------------}
{                                                                       }
{-----------------------------------------------------------------------}
procedure TDBNameDescriptor.SetBDFileName(aFileName: PChar);
var
   tmpChar  : array[0..100] of char;
   tmpPChar : PChar;
begin
  StrCopy(tmpChar, aFileName);
  tmpPChar:= ansiUpper(tmpChar);
  StrCopy(dbFileName, tmpChar);
end;

{------------------------[ PrintAll ]-----------------------------------}
{-----------------------------------------------------------------------}
{                                                                       }
{-----------------------------------------------------------------------}
procedure TDBNameDescriptor.PrintAll(p: PCollection);

  procedure PrintItem(p: PIDUnit); far;
  begin
     p^.printToScreen;
  end;

begin
  Writeln;
  Writeln;
  Writeln('Fldnames.txt');
  Writeln;

  p^.ForEach(@PrintItem);
end;

{------------------------[ MakePas ]------------------------------------}
{-----------------------------------------------------------------------}
{                                                                       }
{-----------------------------------------------------------------------}
procedure TDBNameDescriptor.MakePas;

  procedure PrintLine(p: PIDUnit); far;
  begin
     p^.print(outFile, @dbIDSFile);
  end;

begin
  Writeln('File and Field Descriptions read successfully.');
  Writeln(outFile^, 'File and Field Descriptions read successfully.');

  Writeln('Creating Pascal unit file...');
  Writeln(outFile^, 'Creating Pascal unit file...');

  RemoveExtension;
  Writeln;
  Writeln(outFile^);
  Writeln(dbIDSFile);

  Writeln('unit  ', unitName, ';');
  Writeln(outFile^, 'unit  ', unitName, ';');
  Writeln(dbIDSFile, 'unit  ', unitName, ';');

  Writeln;
  Writeln(outFile^);
  Writeln(dbIDSFile);

  Writeln('interface');
  Writeln(outFile^, 'INTERFACE');
  Writeln(dbIDSFile, 'INTERFACE');

  Writeln;
  Writeln(outFile^);
  Writeln(dbIDSFile);

  Writeln('const');
  Writeln(outFile^, 'const');
  Writeln(dbIDSFile, 'const');

  Writeln;
  Writeln(outFile^);
  Writeln(dbIDSFile);

  pasUnit^.ForEach(@PrintLine);

  Writeln;
  Writeln(outFile^);
  Writeln(dbIDSFile);

  Writeln('IMPLEMENTATION');
  Writeln(outFile^, 'IMPLEMENTATION');
  Writeln(dbIDSFile, 'IMPLEMENTATION');

  Writeln;
  Writeln(outFile^);
  Writeln(dbIDSFile);

  Writeln('END.');
  Writeln(outFile^, 'END.');
  Writeln(dbIDSFile, 'END.');
end;

{------------------------[ InsertUDField ]------------------------------}
{-----------------------------------------------------------------------}
{                                                                       }
{-----------------------------------------------------------------------}
procedure TDBNameDescriptor.InsertUDField(k: integer; var fldNum: integer);
var
  tmp     : PChar;
  p       : PChar;
  d       : array[0..255] of char;
  tPChar  : array[0..255] of char;
  numPChar: array[0..6] of char;
begin
  if ((k and db_UserEdit) <> 0) then
  begin
    StrCopy(tPChar, dbFileName);
    tmp:= ansiUpper(tPChar);
    p:= StrPos(tPChar, 'FILE');

    StrLCopy(d, tPChar, p - tPchar);
    StrCat(d, 'UDBase');

    inc(fldNum);
    Str(fldNum, numPChar);

    pasUnit^.Insert(New(PIDUnit, Init(d, numPChar)));
  end;
end;

{------------------------[ InsertDBKey ]--------------------------------}
{-----------------------------------------------------------------------}
{                                                                       }
{-----------------------------------------------------------------------}
procedure TDBNameDescriptor.InsertDBKey(keydesc: PChar; var keyNum: integer);
var
  tmp    : PChar;
  p      : PChar;
  d       : array[0..255] of char;
  numPChar: array[0..6] of char;
begin
  tmp:= ansiUpper(dbFileName);
  p:= StrPos(tmp, 'FILE');

  StrLCopy(d, dbFileName, p - dbFileName);
  StrCat(d, '_');
  StrCat(d, keydesc);
  StrCat(d, '_KEY');

  Str(keyNum, numPChar);

  pasUnit^.Insert(New(PIDUnit, Init(d, numPChar)));
end;

{------------------------[ InsertBlank ]--------------------------------}
{-----------------------------------------------------------------------}
{                                                                       }
{-----------------------------------------------------------------------}
procedure TDBNameDescriptor.InsertBlank;
begin
  pasUnit^.Insert(New(PIDUnit, Init('  ', '  ')));
end;

{------------------------[ InsertSpecialConst ]-------------------------}
{-----------------------------------------------------------------------}
{                                                                       }
{-----------------------------------------------------------------------}
procedure TDBNameDescriptor.InsertSpecialConst;
begin
  if filMode = 'O' then
  begin
    pasUnit^.Insert(New(PIDUnit, Init('  ', '  ')));
    pasUnit^.Insert(New(PIDUnit, Init('DBMnemAbbr', '1')));
    pasUnit^.Insert(New(PIDUnit, Init('DBMnemDesc', '2')));
  end;
end;

{------------------------[ ReadDescriptionFile ]------------------------}
{-----------------------------------------------------------------------}
{                                                                       }
{-----------------------------------------------------------------------}
procedure TDBNameDescriptor.ReadDescriptionFile;
var
  tPChar    : array[0..255] of char;
  pcharDesc : array[0..255] of char;
  pcharID   : array[0..255] of char;
begin
  while not eof(descFile) do
  begin
    Readln(descFile, tPChar);
    If (strLComp('//', tPChar, 2 ) <> 0) and (tPchar[0] <> #0) then  { not a comment }
    begin
      (* writeln(tstr);*)
      TrimAll(tPChar, tPChar, ' ', 255);
      StrStrip(pcharID, tPChar, ',', 255);
      TrimAll(tPChar, tPChar, ' ', 255);
      TrimAll(pcharID, pcharID, ' ', 255);

      StrStrip(pcharDesc, tPChar, ',', 255);
      TrimAll(pcharDesc, pcharDesc, ' ', 255);

      NameDescList^.Insert(New(PNameDesc, Init(pcharID, pcharDesc)));

    end;
  end;
end;

{------------------------[ RemoveExtension ]----------------------------}
{-----------------------------------------------------------------------}
{                                                                       }
{-----------------------------------------------------------------------}
procedure TDBNameDescriptor.RemoveExtension;

var
  k : integer;
  temp : array[0..10] of char;
begin
  k:= 1;
  while (unitName[k] <> '.') and (k <= StrLen(unitName)) do
    Inc(k);
  StrLCopy(temp, unitName, k);
  StrCopy(unitName, temp);
end;

{------------------------[ LoadPasUnitCollection ]----------------------}
{-----------------------------------------------------------------------}
{                                                                       }
{-----------------------------------------------------------------------}
procedure TDBNameDescriptor.LoadPasUnitCollection;
var
  tPCHar    : array[0..255] of char;
  pcharId   : array[0..255] of char;
  pcharConst: array[0..255] of char;
begin
  Readln(dbIDSFile, tPChar);
  while not eof(dbIDSFile) and (StrPos(tPChar, 'const') = nil) do
    Readln(dbIDSFile, tPChar);

  Readln(dbIDSFile, tPChar);
  while not eof(dbIDSFile) and (StrPos(tPChar, 'IMPLEMENTATION') = nil) do
  begin
    if (StrLComp('//', tPChar, 2) <> 0) and (tPchar[0] <> #0) then
    begin
      StrStrip(pcharID, tPChar, '=', 255);
      TrimAll(tPChar, tPChar, ' ', 255);
      TrimAll(pcharID, pcharID, ' ', 255);

      StrStrip(pcharConst, tPChar, ';', 255);
      TrimAll(pcharConst, pcharConst, ' ', 255);

      pasUnit^.Insert(New(PIDUnit, Init(pcharID, pcharConst)));
    end;
    Readln(dbIDSFile, tPChar);
  end;
  PrintAll(pasUnit);
end;

{------------------------[ Init ]---------------------------------------}
{-----------------------------------------------------------------------}
{                                                                       }
{-----------------------------------------------------------------------}
constructor TNameDesc.Init(newNameID, newNameDesc: PChar);
begin
  GetMem(nameID, MaxStr);
  if (nameID = nil) then
  begin
    (*error;
    exit; *)
  end;
  StrCopy(nameID, newNameID);

  GetMem(nameDesc, MaxStr);
  if (nameDesc = nil) then
  begin
    (*error;
    exit; *)
  end;
  StrCopy(nameDesc, newNameDesc);
end;

{------------------------[ Done ]---------------------------------------}
{-----------------------------------------------------------------------}
{                                                                       }
{-----------------------------------------------------------------------}
destructor  TNameDesc.Done;
begin
  MSFreeMem(nameID, MaxStr);
  MSFreeMem(nameDesc, MaxStr);
end;

{------------------------[ PrintToScreen ]------------------------------}
{-----------------------------------------------------------------------}
{                                                                       }
{-----------------------------------------------------------------------}
procedure   TNameDesc.PrintToScreen;
begin
  Writeln('  ', nameID, '':25 - StrLen(nameID), nameDesc);
end;

{------------------------[ Init ]---------------------------------------}
{-----------------------------------------------------------------------}
{                                                                       }
{-----------------------------------------------------------------------}
constructor TIDUnit.Init(newID, newConst: PChar);
begin
  GetMem(nameID, MaxStr);
  if (nameID = nil) then
  begin
    (*error;
    exit; *)
  end;
  StrCopy(nameID, newID);

  GetMem(nameConst, MaxStr);
  if (nameConst = nil) then
  begin
    (*error;
    exit; *)
  end;
  StrCopy(nameConst, newConst);
end;

{------------------------[ Done ]---------------------------------------}
{-----------------------------------------------------------------------}
{                                                                       }
{-----------------------------------------------------------------------}
destructor TIDUnit.Done;
begin
  MSFreeMem(NameID, MaxStr);
  MSFreeMem(NameConst, MaxStr);
end;

{------------------------[ Print ]--------------------------------------}
{-----------------------------------------------------------------------}
{                                                                       }
{-----------------------------------------------------------------------}
procedure   TIDUnit.Print(f1, f2: PText);
begin
  if nameID[0] = ' ' then
  begin
    Writeln;
    Writeln(f1^);
    Writeln(f2^);
  end
  else
  begin
    Writeln('  ', nameID, '':35 - StrLen(nameID), '= ', nameConst,';');
    Writeln(f1^,'  ', nameID, '':35 - StrLen(nameID), '= ', nameConst, ';');
    Writeln(f2^,'  ', nameID, '':35 - StrLen(nameID), '= ', nameConst, ';');
  end;
end;

{------------------------[ PrintToScreen ]------------------------------}
{-----------------------------------------------------------------------}
{                                                                       }
{-----------------------------------------------------------------------}
procedure   TIDUnit.PrintToScreen;
begin
  Writeln('  ', nameID, '':35 - StrLen(nameID), '= ', nameConst, ';');
end;

END.
