{----------------------------------------------------------------------------}
{ Module Name  : EPIUSER.PAS                                                 }
{ Programmer   : EJ                                                          }
{ Date Created : 07/04/95                                                    }
{ Requirements : ..., S3.23 [ref. c], S3.28 [ref. d]                         }
{                                                                            }
{ Purpose - This unit provides a command-level procedure for generating a    }
{   Bacterial Incidence by Item Report. The report is for epidemiology.      }
{                                                                            }
{   This report compares bacterial incidence against an item such as         }
{   source, ward, or a user defined field.                                   }
{                                                                            }
{   The isolate database and its associated databases are the source of      }
{   input for the report. This input is reduced by the Multiple Parameter    }
{   Search System (MPSS) (with or withour user defined rules), local rules,  }
{   and further reduced by the Eliminate Duplicates Filter (EDFilter).       }
{                                                                            }
{ Assumptions                                                                }
{    a. A mono-spaced font is used for the report.                           }
{                                                                            }
{ Limitations                                                                }
{    a. Number of isolates processed is limited by EDFilter and other things.}
{                                                                            }
{ Special                                                                    }
{   To maintain the set of unit error numbers, whenever the Assert or        }
{   DoneOnError functions are added, search for Assert and DoneOnError and   }
{   renumber them with unique values.                                        }
{                                                                            }
{ Referenced Documents                                                       }
{    a. Software Specification for Nihongo Data Management System v4.00      }
{    b. Database Design Document, Revision 0                                 }
{    c. S3.23 Design Document, Revision 2                                    }
{    d. S3.28 Design Document, Revision 2                                    }
{    e. Turbo Pascal for Windows, Programmer's Guide, 1991                   }
{                                                                            }
{ Revision History - This project is under version control, use it to view   }
{    revision history.                                                       }
{                                                                            }
{----------------------------------------------------------------------------}

unit EpiUser;

INTERFACE

uses
  OWindows;

procedure EpiOrganismByUserField( pParent: PWindowsObject; useMPSS: boolean );


IMPLEMENTATION

uses
  APITools,
  DateLib,
  DBIDs,
  DBLib,
  DBFile,
  DBTypes,
  DMSErr,
  EDFilter,
  EpiFiles,
  IntlLib,
  ListLib,
  MPSS,
  MScan,
  Objects,
  PickFld,
  PrnPrev,
  Report,
  Strings,
  StrsW;

{$I PRTEPI.INC}



                        {----------------------}
                        {  Helper Functions    }
                        {----------------------}



{----------------------------------------------------------------------------}
{ This procedure handles assertions for this unit. If an assertion is
{ invalid, it calls show error with a number and halts. Otherwise, it does
{ nothing.
{
{ Inputs:
{   Valid - result of assertion, true if valid, false if invalid.
{   UnitErrorNumber - a number that uniquely identifies the assertion
{     within this unit.
{----------------------------------------------------------------------------}

procedure Assert( Valid: boolean; UnitErrorNumber: integer );
begin
  if not Valid then
  begin
    ShowError( nil, IDS_EPI_ERR, nil, MOD_EPIUSER + 1, MOD_EPIUSER,
      UnitErrorNumber );
    Halt;
  end;
end;



                        {----------------------}
                        {    Organism Line     }
                        {----------------------}



const
  {--------------------------------------------------------------------------}
  { Set the width of the report in character columns. The report width must
  { be acceptable to the TReport.Init constructor.
  {--------------------------------------------------------------------------}
  REPORT_WIDTH = 120;

  {--------------------------------------------------------------------------}
  { This constant specifies the left most position of the organism column.
  { The units are character columns where 1 is the left most position and
  { REPORT_WIDTH is the right most position on the page.
  {
  { Range: 0 < ORGANISM_COLUMN < REPORT_WIDTH
  {--------------------------------------------------------------------------}
  ORGANISM_LCOLUMN = 4;

  {--------------------------------------------------------------------------}
  { This constant specifies the right most position of the count column. Same
  { units as above.
  {
  { Range: ORGANISM_COLUMN < COUNT_COLUMN < REPORT_WIDTH
  {--------------------------------------------------------------------------}
  COUNT_RCOLUMN = 50;

  {--------------------------------------------------------------------------}
  { This constant specifies the right most position of the percent column.
  { Same units as above.
  {
  { Range: COUNT_COLUMN < PERCENT_COLUMN < REPORT_WIDTH
  {--------------------------------------------------------------------------}
  PERCENT_RCOLUMN = 65;

type
  {--------------------------------------------------------------------------}
  { This object represents a printable line of organism information.
  {--------------------------------------------------------------------------}
  PtrOrganismLine = ^TOrganismLine;
  TOrganismLine = object( TObject )

    {Interface}
    constructor Init( input_Organism: TSeqNum; input_Count,
      input_TotalCount: longint );
    procedure Print( pPD: PPrintData );

    {Implementation}
    private
    Organism: TSeqNum;
    Count: longint;
    TotalCount: integer;
  end;



{----------------------------------------------------------------------------}
{ The constructor initializes the line.
{----------------------------------------------------------------------------}

constructor TOrganismLine.Init( input_Organism: TSeqNum; input_Count,
  input_TotalCount: longint );
const
  MAXLONGINT = 2147483647; { As defined in ref. e }
  MAXCOUNT = MAXLONGINT div 1000;
begin
  { Prevent overflow and divide by zero in percent calculation }
  Assert( input_Count <= MAXCOUNT, 1 );
  Assert( input_TotalCount > 0, 2 );

  { Initialize object }
  Organism := input_Organism;
  Count := input_Count;
  TotalCount := input_TotalCount;
end;



{----------------------------------------------------------------------------}
{ This procedure writes the line to the printout object.
{----------------------------------------------------------------------------}

procedure TOrganismLine.Print( pPD: PPrintData );

  {--------------------------------------------------------------------------}
  { This procedure appends characters from the organism name to the end of
  { the given string. MaxLen < SizeOf( Str ).
  {--------------------------------------------------------------------------}
  procedure OrganismNameLCat( Str: PChar; MaxLen: Word );
  var
    List: TListObject;
    ListRec: TMnemListRec;
  begin
    List.Init;
    ListRec.Init;
    if List.FindMnemSeq( DBORGFile, @ListRec, Organism ) then
      StrLCat( Str, ListRec.desc, MaxLen )
    else
      StrLCat( Str, '*****', MaxLen );
    ListRec.Done;
    List.Done;
  end;

var
  ValueString: array [ 0..80 ] of char;
  PrintLine: array [ 0..REPORT_WIDTH ] of char;
  PercentTimes10: longint;

begin { Print }

  { Build column 1 - Organism }
  Pad( PrintLine, '', ' ', ORGANISM_LCOLUMN - 1 );
  OrganismNameLCat( PrintLine, SizeOf( PrintLine ) - 1 );

  { Build column 2 - Count }
  Str( Count, ValueString );
  Pad( PrintLine, PrintLine, ' ', COUNT_RCOLUMN - StrLen( ValueString ) );
  StrLCat( PrintLine, ValueString, SizeOf( PrintLine ) );

  { Build column 3 - Percent }
  PercentTimes10 := ( Count * 1000 ) div TotalCount;
  Str( PercentTimes10, ValueString );
  StrCat( ValueString, '.' );
  ValueString[ StrLen( ValueString ) - 1 ] :=
    ValueString[ StrLen( ValueString ) - 2 ];
  ValueString[ StrLen( ValueString ) - 2 ] := '.';
  StrCat( ValueString, '%' );
  Pad( PrintLine, PrintLine, ' ', PERCENT_RCOLUMN - StrLen( ValueString ) );
  StrLCat( PrintLine, ValueString, SizeOf( PrintLine ) );

  { Add line to print data }
  pPD^.AddRow( r_Normal, 0 );
  pPD^.AddCell( PrintLine, LT500, c_Stretch, 0 );

end;



{----------------------------------------------------------------------------}
{ This procedure is associated with TOrganismLine. It writes a matching
{ header to the print object. In C++ this would be a const member function.
{----------------------------------------------------------------------------}

procedure TOrganismLine_PrintHeader( pPD: PPrintData );
var
  ValueString: array [ 0..80 ] of char;
  PrintLine: array [ 0..REPORT_WIDTH ] of char;
begin

  {---- Line 1 - column headings }

  { Build column 1 - Organism }
  Pad( PrintLine, '', ' ', ORGANISM_LCOLUMN - 1 );
  SR( IDS_ORGANISM_LBL, ValueString, SizeOf( ValueString ) );
  StrLCat( PrintLine, ValueString, REPORT_WIDTH );
  Pad( PrintLine, PrintLine, ' ', 35 );

  { Build column 2 - Count }
  SR( IDS_NUM_ORG_LBL, ValueString, SizeOf( ValueString ) );
  Pad( PrintLine, PrintLine, ' ', COUNT_RCOLUMN - StrLen( ValueString ) );
  StrLCat( PrintLine, ValueString, REPORT_WIDTH );

  { Build column 3 - Percent }
  SR( IDS_PCT_TOT_LBL, ValueString, SizeOf( ValueString ) );
  Pad( PrintLine, PrintLine, ' ', PERCENT_RCOLUMN - StrLen( ValueString ) );
  StrLCat( PrintLine, ValueString, REPORT_WIDTH );

  { Add line to print data }
  pPD^.AddRow( r_Normal, 0 );
  pPD^.AddCell( PrintLine, LT500, c_Stretch, 0 );

  {---- Line 2 - blank }
  pPD^.AddRow( r_Normal, 0 );

end;



                        {----------------------}
                        {        Group         }
                        {----------------------}



type
  {--------------------------------------------------------------------------}
  { This object represents a printable section representing a group.
  {--------------------------------------------------------------------------}
  PGroupSection = ^TGroupSection;
  TGroupSection = object( TObject )

    {Interface}
    constructor Init( input_Group: integer; input_Count: longint );
    procedure Print( pPD: PPrintData );
    destructor Done; virtual;
    procedure Insert( Line: PtrOrganismLine );

    {Implementation}
    private
    Group: integer;
    Count: longint;
    Lines: TCollection;
  end;



{----------------------------------------------------------------------------}
{ The constructor initializes the section for a specific group (type and
{ total count).
{----------------------------------------------------------------------------}

constructor TGroupSection.Init( input_Group: integer; input_Count: longint );
begin
  Group := input_Group;
  Count := input_Count;
  Lines.Init( 10, 10 );
end;



{----------------------------------------------------------------------------}
{ The destructor cleans up.
{----------------------------------------------------------------------------}

destructor TGroupSection.Done;
begin
  Lines.Done;
  inherited Done;
end;



{----------------------------------------------------------------------------}
{ This procedure adds a line to the group.
{----------------------------------------------------------------------------}

procedure TGroupSection.Insert( Line: PtrOrganismLine );
begin
  Lines.Insert( Line );
end;



{----------------------------------------------------------------------------}
{ This procedure writes the section to the printout object.
{----------------------------------------------------------------------------}

procedure TGroupSection.Print( pPD: PPrintData );

  {--------------------------------------------------------------------------}
  { This procedure directs a line to print itself.
  {--------------------------------------------------------------------------}
  procedure PrintLine( p: PtrOrganismLine ); far;
  begin
    p^.Print( pPD );
  end;

var
  lbl: array [ 0..80 ] of char;
  printStr: array [ 0..REPORT_WIDTH ] of char;

begin { Print }

  { Print inserted lines }
  Lines.ForEach( @PrintLine );

  { Blank line }
  pPD^.AddRow( r_Normal, 0 );

  { Total line }
  pPD^.AddRow( r_Normal, 0 );
  Pad( printStr, '', ' ', 6 );
  case Group of
    1: SR( IDS_GN_TOT_LBL, lbl, 80 ); {Gram Neg Total}
    2: SR( IDS_GP_TOT_LBL, lbl, 80 ); {Gram Pos Total}
    3: SR( IDS_ANAEROBE_TOT_LBL, lbl, 80 ); {Anaerobe Total}
    4: SR( IDS_HNID_TOT_LBL, lbl, 80 ); {HNID Total}
    6: SR( IDS_YEAST_TOT_LBL, lbl, 80 ); {Yeast Total}
    8: SR( IDS_OTHER_TOT_LBL, lbl, 80 ); {Other Total}
    else
      StrCopy( lbl, '*****' );
  end;
  StrLCat( printStr, lbl, SizeOf( printStr ) );
  Str( count, lbl );
  Pad( printStr, printStr, ' ', 50 - StrLen( lbl ) );
  StrLCat( printStr, lbl, REPORT_WIDTH );

  pPD^.AddCell( printStr, LT500, c_Stretch, 0 );
  pPD^.AddRow( r_Normal, 0 );

end;



                        {----------------------}
                        {      Item Class      }
                        {----------------------}



type
  {--------------------------------------------------------------------------}
  { Tiis type defines the types of data supported by item class.
  {--------------------------------------------------------------------------}
  DataTypes = ( DT_STRING, DT_MNEMONIC, DT_DATE );

  {--------------------------------------------------------------------------}
  { This record defines the information needed to select an item class.
  {--------------------------------------------------------------------------}
  TItemClassInfo = record
    SRFieldNumber: integer;
    Name: array [ 0..maxFldNameLen ] of char;
    DataType: DataTypes;
  end;


  {--------------------------------------------------------------------------}
  { This object defines the class of items selected for the Bacterial
  { Incidence by item report, e.g., Specimen ID, Free Text, or Collection
  { Date. This object holds attributes that are common to each item object.
  {--------------------------------------------------------------------------}
  PtrItemClass = ^TItemClass;
  TItemClass = object

    {Interface}
    constructor Init( input_SRFieldNumber: integer );
    destructor Done;
    function RegisterItem( SpecimenRecord: TSeqNum ): TSeqNum;
    function ValueLCopy( Item: TSeqNum; Buffer: PChar; MaxLen: Word ): PChar;
    function Compare( Item1, Item2: TSeqNum ): integer;

    {Implementation}
    private
    Info: TItemClassInfo;
    itemFN: array [ 0..12 ] of char;
    itemDB: PDBFile;
    procedure GetAsMnemonic( Item: TSeqNum; Buffer: PChar;
      MaxLen: Word );
    procedure GetAsMnemonicDescription( Item: TSeqNum; Buffer: PChar;
      MaxLen: Word );
    procedure GetAsString( Item: TSeqNum; Buffer: PChar;
      MaxLen: Word );
  end;



{----------------------------------------------------------------------------}
{ The constructor creates an control object for a given field in a specimen
{ record.
{----------------------------------------------------------------------------}

constructor TItemClass.Init( input_SRFieldNumber: integer );
var
  db: PDBFile;
  fi: TFldInfo;
  adb: PDBAssocFile;
  Ok: boolean;
begin
  { Setup }
  db := New( PDBFile, Init( DBSPECFile, '', dbOpenNormal ) );
  Assert( db <> nil, 3 );

  { Load info }
  Info.SRFieldNumber := input_SRFieldNumber;
  Ok := db^.dbd^.FieldInfo( input_SRFieldNumber, fi );
  Assert( Ok, 4 );
  if ( fi.userFlag and db_Mnemonic ) <> 0 then
    Info.DataType := DT_MNEMONIC
  else if fi.fldType = dbDate then
    Info.DataType := DT_DATE
  else
    Info.DataType := DT_STRING;
  StrLCopy( Info.Name, fi.fldName, maxFldNameLen );

  { Open item file }
  case Info.DataType of

    DT_MNEMONIC :
    begin
      db^.dbd^.FieldInfo( Info.SRFieldNumber, fi );
      adb := New( PDBAssocFile, Init( db^.dbr, fi.fldAssoc, dbOpenNormal ) );
      Assert( adb <> nil, 5 );
      itemDB := New( PDBFile, Init( adb^.dbd^.FileName( itemFN, 12 ),
        '', dbOpenNormal ) );
      MSDisposeObj( adb );
    end;

    DT_STRING,
    DT_DATE :
    begin
      if not CreateEPITMP7 then
        Assert( false, 6 );
      itemDB := New( PDBFile, Init( 'EPITMP7', '', dbOpenExclusive ) );
    end;

    else
      Assert( false, 7 );
  end;
  Assert( itemDB <> nil, 8 );

  { Cleanup. MSDispose not needed, item goes out of scope }
  Dispose( db, Done );
end;



{----------------------------------------------------------------------------}
{ The destructor performes object cleanup.
{----------------------------------------------------------------------------}

destructor TItemClass.Done;
begin
  MSDisposeObj( itemDB );
end;



{----------------------------------------------------------------------------}
{ This function provides a sequence number for the chosen item in the specimen
{ record. The data file assiciated with this sequence number may vary so this
{ value can only be used with other methods of this class (such as Compare
{ and ValueLCopy).
{
{ Note: If constructing list is too slow, e.g., involves file I/O, then move
{ it to the constructor, initialize it once, shared it with other member
{ functions, otherwise, let it stay here to minimize scope.
{----------------------------------------------------------------------------}

function TItemClass.RegisterItem( SpecimenRecord: TSeqNum ): TSeqNum;
var
  dbr: PDBRec;
  List: TListObject;
  SequenceNumber: TSeqNum;
  itemVal: array [ 0..32 ] of char;
begin
  { given specimen record, return item's sequence number, creating one if
    needed }
  List.Init;
  dbr := List.PeekSeq( DBSPECFile, SpecimenRecord );
  case Info.DataType of

    DT_MNEMONIC :
      { return sequence number referenced in the field }
      dbr^.GetField( Info.SRFieldNumber, @SequenceNumber, SizeOf( TSeqNum ) );

    DT_STRING,
    DT_DATE :
    begin
      { Read field as string, store in item data file, and return sequence
        number }
      dbr^.GetFieldAsStr( Info.SRFieldNumber, itemVal, 32 );
      itemDB^.dbr^.PutFieldAsStr( DBEPI7Item, itemVal );
      if not itemDB^.dbc^.GetEQ( itemDB^.dbr ) then
        itemDB^.dbc^.InsertRec( itemDB^.dbr );
      SequenceNumber := itemDB^.dbr^.GetSeqValue;
    end;
    else
      Assert( false, 9 );
  end;
  List.Done;

  RegisterItem := SequenceNumber;
end;



{----------------------------------------------------------------------------}
{ This procedure gets the value of a mnemonic item as a string. This
{ procedure is not part of the object's interface, its only to be used
{ within the Compare and ValueLCopy methods.
{----------------------------------------------------------------------------}

procedure TItemClass.GetAsMnemonic( Item: TSeqNum; Buffer: PChar;
  MaxLen: Word );
var
  ListRec: TMnemListRec;
  List: TListObject;
begin
  List.Init;
  ListRec.Init;
  if List.FindMnemSeq( itemFN, @ListRec, Item ) then
    StrLCopy( Buffer, ListRec.ID, MaxLen )
  else
    StrLCopy( Buffer, '*****', MaxLen );
  ListRec.Done;
  List.Done;
end;



{----------------------------------------------------------------------------}
{ This procedure gets the value of a mnemonic item as a string. This
{ procedure is not part of the object's interface, its only to be used
{ within the Compare and ValueLCopy methods.
{----------------------------------------------------------------------------}

procedure TItemClass.GetAsMnemonicDescription( Item: TSeqNum; Buffer: PChar;
  MaxLen: Word );
var
  ListRec: TMnemListRec;
  List: TListObject;
begin
  List.Init;
  ListRec.Init;
  if List.FindMnemSeq( itemFN, @ListRec, Item ) then
    StrLCopy( Buffer, ListRec.desc, MaxLen )
  else
    StrLCopy( Buffer, '*****', MaxLen );
  ListRec.Done;
  List.Done;
end;



{----------------------------------------------------------------------------}
{ This procedure gets the value of a non mnemonic item as a string. This
{ procedure is not part of the object's interface, its only to be used
{ within the Compare and ValueLCopy methods.
{----------------------------------------------------------------------------}

procedure TItemClass.GetAsString( Item: TSeqNum; Buffer: PChar;
  MaxLen: Word );
var
  itemVal: array [ 0..32 ] of char;
begin
  if itemDB^.dbc^.GetSeq( itemDB^.dbr, Item ) then
  begin
    itemDB^.dbr^.GetFieldAsStr( DBEPI7Item, itemVal, 32 );
    StrLCopy( Buffer, itemVal, MaxLen );
  end
  else
    StrLCopy( Buffer, '*****', MaxLen );
end;



{----------------------------------------------------------------------------}
{ This function compares two items in the class for sorting purposes.
{----------------------------------------------------------------------------}

function TItemClass.Compare( Item1, Item2: TSeqNum ): integer;
var
  str1, str2: array [ 0..100 ] of char;
  date1, date2: TIntlDate;
  berr: boolean;
begin

  case Info.DataType of

    DT_MNEMONIC :
    begin
      GetAsMnemonic( Item1, str1, SizeOf( str1 ) - 1 );
      GetAsMnemonic( Item2, str2, SizeOf( str2 ) - 1 );
      Compare := StrComp( str1, str2 );
    end;

    DT_STRING :
    begin
      GetAsString( Item1, str1, SizeOf( str1 ) - 1 );
      GetAsString( Item2, str2, SizeOf( str2 ) - 1 );
      Compare := StrComp( str1, str2 );
    end;

    DT_DATE :
    begin
      GetAsString( Item1, str1, SizeOf( str1 ) - 1 );
      GetAsString( Item2, str2, SizeOf( str2 ) - 1 );
      date1 := IntlStrToDate( str1, berr );
      date2 := IntlStrToDate( str2, berr );
      Compare := CompareDates( date1, date2 );
    end;

    else
      Assert( false, 10 );
  end;
end;



{----------------------------------------------------------------------------}
{ This function copies characters of the item's value in string form to a
{ buffer.
{----------------------------------------------------------------------------}

function TItemClass.ValueLCopy( Item: TSeqNum; Buffer: PChar; MaxLen: Word ):
  PChar;
var
  ListRec: TMnemListRec;
  List: TListObject;
  itemVal: array [ 0..32 ] of char;
begin

  case Info.DataType of
    DT_MNEMONIC :
      GetAsMnemonicDescription( Item, Buffer, MaxLen );
    DT_STRING,
    DT_DATE :
      GetAsString( Item, Buffer, MaxLen );
  else
    Assert( false, 11 );
  end;

  ValueLCopy := Buffer;

end;



                        {----------------------}
                        {         Item         }
                        {----------------------}



type
  {--------------------------------------------------------------------------}
  { This object represents an item in the Bacterial Incidence by item report.
  { It's a simple object designed to hide the complexity created by a variable
  { item class. This object provides a level of abstraction so that higher
  { levels can use items without dealing with the variable class. It also
  { provides a small object so that many of them can be stored in a
  { collection.
  {--------------------------------------------------------------------------}
  PtrItem = ^TItem;
  TItem = object

    {Interface}
    constructor Init( input_pClass: PtrItemClass );
    constructor Copy( Item: TItem );
    procedure SetSequenceNumber( input_SequenceNumber: TSeqNum );
    function Compare( OtherItem: TItem ): integer;
    function ValueLCopy( Buffer: PChar; MaxLen: Word ): PChar;

    {Implementation}
    private
    pClass: PtrItemClass;     { a copy pointer to an item class object }
    SequenceNumber: TSeqNum;  { the item's class relative sequence number }
  end;



{----------------------------------------------------------------------------}
{ The constructor sets the item class. Call the Define method to specify an
{ item.
{----------------------------------------------------------------------------}

constructor TItem.Init( input_pClass: PtrItemClass );
begin
  SequenceNumber := 0;
  pClass := input_pClass;
end;



{----------------------------------------------------------------------------}
{ The copy constructor initilizes the object from another.
{----------------------------------------------------------------------------}

constructor TItem.Copy( Item: TItem );
begin
  Self := Item;
end;



{----------------------------------------------------------------------------}
{ This function sets the item's sequence number. An item is completely
{ defined when it has a class and a sequence number.
{----------------------------------------------------------------------------}

procedure TItem.SetSequenceNumber( input_SequenceNumber: TSeqNum );
begin
  SequenceNumber := input_SequenceNumber;
end;



{----------------------------------------------------------------------------}
{ This function compares this item to another for the purpose of sorting
{ items. Items must be of the same class.
{----------------------------------------------------------------------------}

function TItem.Compare( OtherItem: TItem ): integer;
begin
 Assert( pClass = OtherItem.pClass, 12 );
 Compare := pClass^.Compare( SequenceNumber, OtherItem.SequenceNumber );
end;



{----------------------------------------------------------------------------}
{ This function returns a copy of the item's value as a string.
{----------------------------------------------------------------------------}

function TItem.ValueLCopy( Buffer: PChar; MaxLen: Word ): PChar;
begin
  ValueLCopy := pClass^.ValueLCopy( SequenceNumber, Buffer, MaxLen );
end;



                        {----------------------}
                        {     Item Section     }
                        {----------------------}



type
  {--------------------------------------------------------------------------}
  { This object represents an item section. This is the major section in the
  { report.
  {--------------------------------------------------------------------------}
  PtrItemSection = ^TItemSection;
  TItemSection = object( TObject )

    {Interface}
    constructor Init( input_Item: TItem );
    procedure Print( pPD: PPrintData );
    destructor Done; virtual;
    procedure Insert( Subsection: PGroupSection );
    function Compare( pSection: PtrItemSection ): integer;

    {Implementation}
    private
    Item: TItem;
    Subsections: TCollection;
  end;



{----------------------------------------------------------------------------}
{ The constructor initializes the item section. It copies the input item so
{ it need not be maintained after the call.
{----------------------------------------------------------------------------}

constructor TItemSection.Init( input_Item: TItem );
begin
  Item.Copy( input_Item );
  Subsections.Init( 10, 10 );
end;



{----------------------------------------------------------------------------}
{ The destructor cleans up.
{----------------------------------------------------------------------------}

destructor TItemSection.Done;
begin
  Subsections.Done;
  inherited Done;
end;



{----------------------------------------------------------------------------}
{ This procedure adds a subsection to the body of the section.
{----------------------------------------------------------------------------}

procedure TItemSection.Insert( Subsection: PGroupSection );
begin
  Subsections.Insert( Subsection );
end;



{----------------------------------------------------------------------------}
{ This function determines if this section should go before or after another.
{ Use this function if the sections are to be sorted. It returns codes
{ defined by TSortedCollection.Compare.
{----------------------------------------------------------------------------}

function TItemSection.Compare( pSection: PtrItemSection ): integer;
begin
  Compare := Item.Compare( pSection^.Item );
end;



{----------------------------------------------------------------------------}
{ This procedure writes the drug line to the printout object.
{----------------------------------------------------------------------------}

procedure TItemSection.Print( pPD: PPrintData );


  {--------------------------------------------------------------------------}
  { This procedure directs a subsection to print itself.
  {--------------------------------------------------------------------------}
  procedure PrintSubsection( p: PGroupSection ); far;
  begin
    p^.Print( pPD );
  end;

var
  printStr: array [ 0..REPORT_WIDTH ] of char;
  Buffer: array [ 0..80 ] of char;

begin { Print }

  { Item line 1 - item class and name }
  StrLCopy( printStr, Item.pClass^.Info.Name, SizeOf( printStr ) );
  StrLCat( printStr, ' - ', SizeOf( printStr ) );
  StrLCat( printStr, Item.ValueLCopy( Buffer, SizeOf( Buffer ) - 1 ),
    SizeOf( printStr ) );
  pPD^.AddRow( r_StartGroup, 0 );
  pPD^.AddCell( printStr, LT500, c_Stretch, 0 );

  { Item line 2 - blank }
  pPD^.AddRow( r_Normal, 0 );

  { Print inserted sections }
  Subsections.ForEach( @PrintSubsection );

  { Item last line - blank }
  pPD^.AddRow( r_EndGroup, 0 );

end;



                        {----------------------}
                        { ItemSectionCollection}
                        {----------------------}



type
  {--------------------------------------------------------------------------}
  { This object provides a sorted collection for item sections. Item sections
  { inserted into this collection will be sorted using item section's Compare
  { method.
  {--------------------------------------------------------------------------}
  TItemSectionCollection = object( TSortedCollection )
    function Compare( Key1, Key2: Pointer ): Integer; virtual;
  end;



{----------------------------------------------------------------------------}
{ This override invokes the proper compare method. It assumes only item
{ sections are inserted into the collection.
{----------------------------------------------------------------------------}

function TItemSectionCollection.Compare( Key1, Key2: Pointer ): Integer;
var
  pItemSection1,
  pItemSection2: PtrItemSection;
begin

  pItemSection1 := PtrItemSection( Key1 );
  pItemSection2 := PtrItemSection( Key2 );

  Compare := pItemSection1^.Compare( pItemSection2 );
end;



                        {----------------------}
                        {    Report Object     }
                        {----------------------}



const
  {--------------------------------------------------------------------------}
  { This constant redefines the organism field in data file #3 as an item
  { field.
  {--------------------------------------------------------------------------}
  DBEPI3Item = DBEPI3Org;

type
  {--------------------------------------------------------------------------}
  { This is the report generator object.
  {--------------------------------------------------------------------------}
  PEpiItemReport = ^TEpiItemReport;
  TEpiItemReport = object( TReport )

    { Interface }
    constructor Init( aParent: PWindowsObject; useMPSS: boolean;
      input_pItemClass: PtrItemClass );
    destructor Done; virtual;
    procedure Generate;

    { Implementation }
    private
    mpssObj: PMPSS;
    isoDB,
    epi5DB,
    epi6DB,
    epi3DB: PDBFile;
    pItemClass: PtrItemClass; { a copy pointer to an item class object }
    listObj: PListObject;
    procedure InitHeadings; virtual;
    procedure PrintReport;
    procedure BuildTempFiles;
  end;


{----------------------------------------------------------------------------}
{ This constructor places the object in a defined initial state. It also has
{ the user select MPSS rules, if enabled. If construction is successful, the
{ next step should be a call to InitHeadings.
{----------------------------------------------------------------------------}

constructor TEpiItemReport.Init( aParent: PWindowsObject; useMPSS: boolean;
  input_pItemClass: PtrItemClass );

  {--------------------------------------------------------------------------}
  { This function shows an error and calls the destructor if the test value
  { is true. It passes the test value through to facilitate calling Fail.
  {--------------------------------------------------------------------------}
  function DoneOnError( test: boolean; localErrorNum: integer ): boolean;
  begin
    if test then
    begin
      ShowError( aParent, IDS_EPI_ERR, nil, MOD_EPIUSER+1, MOD_EPIUSER,
        localErrorNum );
      Done;
    end;
    DoneOnError := test;
  end;

  {--------------------------------------------------------------------------}
  { This function loads the report title.
  {--------------------------------------------------------------------------}
  function LoadTitle( Buffer: PChar; SizeOfBuffer: Word ): PChar;
  begin
    LoadTitle := SR( IDS_EPIITEM, Buffer, SizeOfBuffer );
    StrLCat( Buffer, ' - ', SizeOfBuffer );
    StrLCat( Buffer, pItemClass^.Info.Name, SizeOfBuffer );
    LoadTitle := Buffer;
  end;

var
  title: array[ 0..64 ] of char;
  code: integer;

begin { Init }

  {---- Place object in a fail-safe state. }
  mpssObj := nil;
  isoDB := nil;
  epi5DB := nil;
  epi6DB := nil;
  epi3DB := nil;
  listObj := nil;


  {Attributes}
  pItemClass := input_pItemClass;


  {---- Construct a report with title, column width, and no maximum lines. }
  if not inherited Init( aParent, LoadTitle( title, sizeof( title ) ),
    REPORT_WIDTH, 0 ) then
    Fail;

  {---- Arm the base class abort flag during the following initialization.
        If none fail, it will be disarmed at the end of this constructor. }
  abortPrinting := TRUE;

  {---- Create an isolate database object. }
  isoDB := New( PDBFile, Init( DBISOFile, '', dbOpenNormal ) );
  if DoneOnError( isoDB = nil, 50 ) then
    Fail;

  {---- Create a Multiple Parameter Search (MPSS) object driven by the
        isolate database. }
  mpssObj := New( PMPSS, Init( aParent, isoDB, IncludeUndefDates ) );
  if DoneOnError( mpssObj = nil, 51 ) then
    Fail;

  {---- If MPSS rules were specified, permit the user to select some or
        cancel. }
  if useMPSS and not mpssObj^.MPSSLoadRules( aParent, title ) then
  begin
    Done;
    Fail;
  end;

  {---- Create a temporary epi. 5 database. Then, create an object
        for the database. }
  if DoneOnError( not CreateEPITMP5, 52 ) then
    Fail;
  epi5DB := New( PDBFile, Init( 'EPITMP5', '', dbOpenExclusive ) );
  if DoneOnError( epi5DB = nil, 53 ) then
    Fail;

  {---- Create a temporary epi. 6 database. Then, create an object
        for the database. }
  if DoneOnError( not CreateEPITMP6, 54 ) then
    Fail;
  epi6DB := New( PDBFile, Init( 'EPITMP6', '', dbOpenExclusive ) );
  if DoneOnError( epi6DB = nil, 55 ) then
    Fail;

  {---- Create a temporary epi. 3 database. Then, create an object
        for the database. }
  if DoneOnError( not CreateEPITMP3, 56 ) then
    Fail;
  epi3DB := New( PDBFile, Init( 'EPITMP3', '', dbOpenExclusive ) );
  if DoneOnError( epi3DB = nil, 57 ) then
    Fail;

  {---- Construct list object }
  listObj := New( PListObject, Init );
  if DoneOnError( listObj = nil, 58 ) then
    Fail;

  {---- Construction complete, disarm the abort control. }
  abortPrinting := FALSE;

end;



{----------------------------------------------------------------------------}
{ This destructor disposes memory allocated to the object.
{----------------------------------------------------------------------------}

destructor TEpiItemReport.Done;
begin
  MSDisposeObj( listObj );
  MSDisposeObj( epi5DB );
  MSDisposeObj( epi6DB );
  MSDisposeObj( epi3DB );
  MSDisposeObj( mpssObj );
  MSDisposeObj( isoDB );
  inherited Done;
end;



{----------------------------------------------------------------------------}
{ This function sets up the report headings. It should be called after the
{ constructor. The next step should be a call AllowCancel (see base class)
{ and then call BuildTempFiles.
{----------------------------------------------------------------------------}

procedure TEpiItemReport.InitHeadings;
var
  micIndex: Word;
  lbl: array [ 0..80 ] of char;
begin
  { Keep the standard heading: date, time, title, and page }
  inherited InitHeadings;

  { Add the organism line heading }
  TOrganismLine_PrintHeader( prnInfo^.header );
end;



{----------------------------------------------------------------------------}
{ This function generates the body of the report. It should be called after
{ InitHeadings. The destructor should be called next.
{----------------------------------------------------------------------------}

procedure TEpiItemReport.PrintReport;

  {--------------------------------------------------------------------------}
  { This procedure sets the text and zeros the percentage for the abort
  { dialog.
  {--------------------------------------------------------------------------}
  procedure InitDialog( var percent: integer );
  var
    Title: array [ 0..80 ] of char;
  begin
    SR( IDS_FORMATING_RPT, Title, 80 );
    cancelDlg^.SetText( Title, 0 );
    percent := 0;
    cancelDlg^.SetText( '0%', IDC_LBL2 )
  end;

  {--------------------------------------------------------------------------}
  { This procedure updates the percentages in the abort dialog.
  {--------------------------------------------------------------------------}
  procedure UpdateDialog( var percent: integer; newPercent: integer );
  var
    PercentageString: array [ 0..7 ] of char;
  begin
    if newPercent <> percent then
    begin
      percent := newPercent;
      Str( percent, PercentageString );
      StrCat( PercentageString, '%' );
      cancelDlg^.SetText( PercentageString, IDC_LBL2 );
    end;
    List( '' );  { uses side effect of List to update abort dialog }
  end;

  {--------------------------------------------------------------------------}
  { This procedure directs an item section to print itself to the body of
  { the report.
  {--------------------------------------------------------------------------}
  procedure PrintItem( p: PtrItemSection ); far;
  begin
    p^.Print( prnInfo^.body );
  end;

var
  i,
  group,
  PercentProcessed,
  newPercentProcessed: integer;
  count,
  NumberOfOrganisms,
  NumberOfRecords,
  RecordsProcessed: longint;
  seq,
  orgSeq,
  itemSeq: TSeqNum;
  pLine: PtrOrganismLine;
  Items: TItemSectionCollection;
  pGroup: PGroupSection;
  pItemSection: PtrItemSection;
  Item: TItem;

begin { PrintReport }

  {---- Access initial record in the ... data file. Exit function if no
        records exist. }
  if not epi3DB^.dbc^.GetFirst( epi3DB^.dbr ) then
    Exit;

  {---- Transition the abort dialog box from the creating state to the
        formatting state. }
  InitDialog( PercentProcessed );
  NumberOfRecords := epi3DB^.dbc^.NumRecords;
  Assert( NumberOfRecords > 0, 13 );
  RecordsProcessed := 0;

  {---- Initialize item section loop: Create a list to hold the item sections
        and an item to hold the current item in the class. }
  Items.Init( NumberOfRecords, 4 );
  Item.Init( pItemClass );

  {---- Item section loop. In this loop, item sections are created, added to
        the list, and subsections are added. }
  repeat
    { Access next item, make an item object and an item section, and insert
      the section into the collection. }
    epi3DB^.dbr^.GetField( DBEPI3Item, @itemSeq, SizeOf( TSeqNum ) );
    Item.SetSequenceNumber( itemSeq );
    pItemSection := New( PtrItemSection, Init( Item ) );
    Items.Insert( pItemSection );
    { Initialize group loop }
    epi3DB^.dbr^.GetField( DBEPI3Count, @NumberOfOrganisms,
      SizeOf( NumberOfOrganisms ) );
    epi6DB^.dbr^.ClearRecord;
    epi6DB^.dbr^.PutField( DBEPI6Item, @itemSeq );
    if epi6DB^.dbc^.GetGE( epi6DB^.dbr ) then
    repeat
      { Add group }
      epi6DB^.dbr^.GetField( DBEPI6Item, @seq, SizeOf( seq ) );
      if seq <> itemSeq then
        Break;
      epi6DB^.dbr^.GetField( DBEPI6Group, @group, SizeOf( group ) );
      epi6DB^.dbr^.GetField( DBEPI6Count, @count, SizeOf( count ) );
      pGroup := New( PGroupSection, Init( Group, Count ) );
      pItemSection^.Insert( pGroup );
      { Add organisms to group }
      epi5DB^.dbr^.ClearRecord;
      epi5DB^.dbr^.PutField( DBEPI5Item, @itemSeq );
      epi5DB^.dbr^.PutField( DBEPI5Group, @group );
      if epi5DB^.dbc^.GetFirstContains( epi5DB^.dbr ) then
      repeat
        { Add organism }
        epi5DB^.dbr^.GetField( DBEPI5Org, @orgSeq, SizeOf( TSeqNum ) );
        epi5DB^.dbr^.GetField( DBEPI5Count, @Count, SizeOf( count ) );
        pLine := New( PtrOrganismLine, Init( orgSeq, Count,
          NumberOfOrganisms ) );
        pGroup^.Insert( pLine );
        { Allow user abort }
        List('');
      until ( not epi5DB^.dbc^.GetNextContains( epi5DB^.dbr ) ) or userAbort;
      { Allow user abort }
      List( '' );  { uses side effect of List to update userAbort }
    until ( not epi6DB^.dbc^.GetNext( epi6DB^.dbr ) ) or userAbort;
    { Allow user abort and update status dialog }
    Inc( RecordsProcessed );
    newPercentProcessed := ( RecordsProcessed * 100 ) div NumberOfRecords;
    UpdateDialog( PercentProcessed, newPercentProcessed );
  until ( not epi3DB^.dbc^.GetNext( epi3DB^.dbr ) ) or userAbort;

  {---- Print item sections }
  if not userAbort then
    Items.ForEach( @PrintItem );

  {---- Cleanup }
  Items.Done;
end;



{----------------------------------------------------------------------------}
{ This private function compiles the databases needed by PrintReport. This
{ procedure should be called after the call to AllowCancel. The PrintReport
{ procedure should be called next.
{----------------------------------------------------------------------------}

procedure TEpiItemReport.BuildTempFiles;

  {--------------------------------------------------------------------------}
  { This procedure sets the text and zeros the percentage for the abort
  { dialog.
  {--------------------------------------------------------------------------}
  procedure InitDialog( var percent: integer; Title: PChar );
  begin
    cancelDlg^.SetText( Title, 0 );
    percent := 0;
    cancelDlg^.SetText( '0%', IDC_LBL2 )
  end;

  {--------------------------------------------------------------------------}
  { This procedure updates the percentages in the abort dialog.
  {--------------------------------------------------------------------------}
  procedure UpdateDialog( var percent: integer; newPercent: integer );
  var
    lbl: array [ 0..80 ] of char;
  begin
    if newPercent <> percent then
    begin
      percent := newPercent;
      Str( percent, lbl );
      StrCat( lbl, '%' );
      cancelDlg^.SetText( lbl, IDC_LBL2 );
    end;
    List( '' );  { uses side effect of List to update abort dialog }
  end;

  {--------------------------------------------------------------------------}
  { This procedure performes a final update to the abort dialog.
  {
  { Does this need to set IDC_LBL2 to "100%" ?
  {--------------------------------------------------------------------------}
  procedure FinalDialogUpdate;
  begin
    List( '' );
  end;

type
  {--------------------------------------------------------------------------}
  { This type holds applicable fields from an isolate record.
  {--------------------------------------------------------------------------}
  TIsolate = record
    Specimen,
    Organism: TSeqNum;
    QCFlag: boolean;
  end;


  {--------------------------------------------------------------------------}
  { This function qualifies an isolate variable for the report. It rejects
  { isolates that are invalid for this report or have nothing to contribute
  { to the report. Currently, it rejects records with one or more of the
  { following conditions:
  {
  {   a.  Its QC flag is set (true)
  {   b.  Its organism reference is null (0)
  {
  {--------------------------------------------------------------------------}
  function isQualifiedIsolate( isolate: TIsolate ): boolean;

    {------------------------------------------------------------------------}
    { This function rejects null references.
    {------------------------------------------------------------------------}
    function valid( num: TSeqNum ): boolean;
    begin
      valid := num <> 0;
    end;

  begin { isQualifiedIsolate }

    with isolate do
    begin
      isQualifiedIsolate := not QCFlag and valid( Organism );
    end

  end;

  {--------------------------------------------------------------------------}
  { This procedure loads an isolate variable from an isolate record.
  {--------------------------------------------------------------------------}
  procedure LoadIsolate( IsolateRec: PDBRec; var isolate: TIsolate );

    {------------------------------------------------------------------------}
    { This function reads the QC flag from the isolate record.
    {------------------------------------------------------------------------}
    function getQCFlag: boolean;
    var
      QCFlag: boolean;
    begin
      IsolateRec^.GetField( DBISOQCFlag, @QCFlag, SizeOf( QCFlag ) );
      getQCFlag := QCFlag;
    end;

    {------------------------------------------------------------------------}
    { This function reads the organism reference from the isolate record.
    {------------------------------------------------------------------------}
    function getOrganism: TSeqNum;
    var
      Organism: TSeqNum;
    begin
      IsolateRec^.GetField( DBISOOrgRef, @Organism, SizeOf( Organism ) );
      getOrganism := Organism;
    end;

    {------------------------------------------------------------------------}
    { This function reads the specimen reference from the isolate record.
    {------------------------------------------------------------------------}
    function getSpecimen: TSeqNum;
    var
      Specimen: TSeqNum;
    begin
      isolateRec^.GetField( DBISOSpecRef, @Specimen, SizeOf( Specimen ) );
      getSpecimen := Specimen;
    end;

  begin { LoadIsolate }
    with isolate do
    begin
      QCFlag := getQCFlag;
      Organism := getOrganism;
      Specimen := getSpecimen;
    end
  end;

  {--------------------------------------------------------------------------}
  { This procedure adds data from the isolate to the report by updating the
  { temp files.
  {
  { This procedure needs to be broken down more.
  {--------------------------------------------------------------------------}
  procedure addToReport( isolate: TIsolate );
  var
    dbr: PDBRec;
    count: longint;
    itemSeq: TSeqNum;
    group: integer;
  begin

    itemSeq := pItemClass^.RegisterItem( isolate.Specimen );

    {Get the group/set from the org file}
    dbr := listObj^.PeekSeq( DBORGFile, isolate.Organism );
    dbr^.GetField( DBORGSet, @group, SizeOf( group ) );

    {Add the info to the org sum file}
    epi5DB^.dbr^.PutField( DBEPI5Item, @itemSeq );
    epi5DB^.dbr^.PutField( DBEPI5Group, @group );
    epi5DB^.dbr^.PutField( DBEPI5Org, @isolate.Organism );
    if epi5DB^.dbc^.GetEQ( epi5DB^.dbr ) then
    begin
      {Update record}
      epi5DB^.dbr^.GetField( DBEPI5Count, @count, SizeOf( count ) );
      Inc( count );
      epi5DB^.dbr^.PutField( DBEPI5Count, @count );
      epi5DB^.dbc^.UpdateRec( epi5DB^.dbr );
    end
    else
    begin
      {create record}
      count := 1;
      epi5DB^.dbr^.PutField( DBEPI5Count, @count );
      epi5DB^.dbc^.InsertRec( epi5DB^.dbr );
    end;

    {Add the info to the group sum file}
    epi6DB^.dbr^.PutField( DBEPI6Item, @itemSeq );
    epi6DB^.dbr^.PutField( DBEPI6Group, @group );
    if epi6DB^.dbc^.GetEQ( epi6DB^.dbr ) then
    begin
      epi6DB^.dbr^.GetField( DBEPI6Count, @count, SizeOf( count ) );
      Inc( count );
      epi6DB^.dbr^.PutField( DBEPI6Count, @count );
      epi6DB^.dbc^.UpdateRec( epi6DB^.dbr );
    end
    else
    begin
      count := 1;
      epi6DB^.dbr^.PutField( DBEPI6Count, @count );
      epi6DB^.dbc^.InsertRec( epi6DB^.dbr );
    end;

    {Add the info to the item sum file}
    epi3DB^.dbr^.PutField( DBEPI3Item, @itemSeq );
    if epi3DB^.dbc^.GetEQ( epi3DB^.dbr ) then
    begin
      epi3DB^.dbr^.GetField( DBEPI3Count, @count, SizeOf( count ) );
      Inc( count );
      epi3DB^.dbr^.PutField( DBEPI3Count, @count );
      epi3DB^.dbc^.UpdateRec( epi3DB^.dbr );
    end
    else
    begin
      count := 1;
      epi3DB^.dbr^.PutField( DBEPI3Count, @count );
      epi3DB^.dbc^.InsertRec( epi3DB^.dbr );
    end;
  end;

  {--------------------------------------------------------------------------}
  { This procedure adds the specified isolate to the report using the
  { addToReport procedure. The purpose of this procedure is to gather the
  { information needed by addToReport.
  {--------------------------------------------------------------------------}
  procedure compile( IsoSeq: TSeqNum );
  var
    Isolate: TIsolate;
    pIsoDB: PDBFile;
    pIsolate: PDBRec;
    ok: boolean;
  begin
    { Set up }
    pIsoDB := New( PDBFile, Init( DBISOFile, '', dbOpenNormal ) );
    Assert( pIsoDB <> nil, 14 );

    { Access the isolate record. }
    ok := pIsoDB^.dbc^.GetSeq( pIsoDB^.dbr, IsoSeq );
    Assert( ok, 15 );
    pIsolate := pIsoDB^.dbr;

    { Add to report }
    LoadIsolate( pIsolate, Isolate );
    addToReport( Isolate );

    { Cleanup. MSDispose not needed }
    Dispose( pIsoDB, Done );
  end;

var
  percent: integer;
  isolate: TIsolate;
  pEDFilter: PEDFilterObj;
  IsoSeq: TSeqNum;
  PassTitle: array [ 0..63 ] of char;

begin { BuildTempFiles }

  {---- Find qualified isolate records and insert them into the filter. }
  if mpssObj^.MPSSGetFirst( isoDB ) then
  begin
    InitDialog( percent, SR( IDS_SELECTING, PassTitle,
      SizeOf( PassTitle ) - 1 ) );
    pEDFilter := New( PEDFilterObj, Init );
    Assert( pEDFilter <> nil, 16 );
    repeat
      LoadIsolate( isoDB^.dbr, isolate );
      if isQualifiedIsolate( isolate ) then
      begin

          {---- Place isolate into the filter. }
          IsoSeq := isoDB^.dbr^.GetSeqValue;
          Assert( IsoSeq <> -1, 17 );
          pEDFilter^.Input( IsoSeq );

      end;
      UpdateDialog( percent, mpssObj^.MPSSGetPercent );
    until ( not mpssObj^.MPSSGetNext( isoDB ) ) or userAbort;
    FinalDialogUpdate;

    {---- If ok, process data in the filter to eliminate duplicates, then
          extract data from filter, using it to compile the report. }
    if not userAbort then
    begin
      pEDFilter^.Process;
      InitDialog( percent, SR( IDS_COMPILING, PassTitle,
        SizeOf( PassTitle ) - 1 ) );
      while pEDFilter^.Output( IsoSeq ) and not userAbort do
      begin
        compile( IsoSeq );
        UpdateDialog( percent, pEDFilter^.PercentOutput );
      end;
      FinalDialogUpdate;
    end;

    {---- Dispose the filter. }
    Dispose( pEDFilter, Done );
  end;
end;


{----------------------------------------------------------------------------}
{ After the object has been constructed, this public procedure steps through
{ the printing proces.
{----------------------------------------------------------------------------}

procedure TEpiItemReport.Generate;
begin
  InitHeadings;
  AllowCancel;
  BuildTempFiles;
  if not userAbort then
    PrintReport;
end;



                        {----------------------}
                        {   EXPORT procedure   }
                        {----------------------}



{----------------------------------------------------------------------------}
{ This command-level procedure steps the user through the process of
{ generating an Bacterial Incidence by Item report.
{
{ Inputs:
{   pParent - pointer to the parent window
{   useMPSS - true if the user wants to select MPSS rules
{
{----------------------------------------------------------------------------}

procedure EpiOrganismByUserField( pParent: PWindowsObject; useMPSS: boolean );
var
  pReport: PEpiItemReport;
  pWCur: PWaitCursor;
  ItemClass: TItemClass;
  SRFieldNumber: integer;
begin

  {---- Prompt user to chose a field from the specimen record or cancel, ... }
  SRFieldNumber := ChooseSpecimenField( pParent );
  if SRFieldNumber <> 0 then
  begin

    ItemClass.Init( SRFieldNumber );

    {---- Construct the generator within a wait style cursor. }
    pWCur :=  New( PWaitCursor, Init );
    pReport := New( PEpiItemReport, Init( pParent, useMPSS, @ItemClass ) );
    MSDisposeObj( pWCur );

    {---- If successful, use it to generate the report then dispose it. }
    if pReport <> nil then
    begin
      pReport^.Generate;
      MSDisposeObj( pReport );
    end;
    ItemClass.Done;
  end;
end;

END.