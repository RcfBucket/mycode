unit Final;

INTERFACE

uses
  DMSDebug,
  OWindows;

procedure FinalizeSpecimens(aParent: PWindowsObject);

IMPLEMENTATION

uses
  IniLib,
  Status,
  APITools,
  MPSS,
  MSCAN,
  WinTypes,
  DBLib,
  DBIds,
  DBTypes,
  DBFile;

{$I FINAL.INC}
{$R FINAL.RES}

procedure FinalizeSpecimens(aParent: PWindowsObject);
var
  mpssObj : PMPSS;
  dbSpec  : PDBFile;
  wait    : TWaitCursor;
  pStr    : array[0..64] of char;
  stat    : PPctStatDlg;
  isOK    : boolean;
begin
  dbSpec := New(PDBFile, Init(DBSpecFile, '', dbOpenNormal));
  stat:= nil;
  if dbSpec <> nil then
  begin
    mpssObj := new(PMPSS, Init(aParent, dbSpec,IncludeUndefDates));
    if mpssObj <> nil then
    begin
      if mpssObj^.MPSSLoadRules(aParent, SR(IDS_FSPECS, pStr, sizeof(pstr)-1)) then
      begin
        wait.Init;
        if mpssObj^.MPSSGetFirst(dbSpec) then
        begin
          wait.Done;
          if mpssObj^.totalRecords > 25 then
          begin
            stat:= New(PPctStatDlg, Init(aParent, MakeIntResource(DLG_FINAL), true, IDC_PERCENT));
            application^.MakeWindow(stat);
          end
          else
            wait.Init;
          repeat
            if (stat <> nil) and
               ((mpssObj^.mCurrentRecord mod INIGridStatusBlockSize = 0) or
                (mpssObj^.mCurrentRecord >= mpssObj^.totalRecords)) then
              stat^.CompPctLevel(mpssObj^.mCurrentRecord, mpssObj^.totalRecords);
            if dbSpec^.dbc^.GetLock(dbSpec^.dbr) then
            begin
              dbSpec^.dbr^.PutFieldAsStr(DBSpecStat, '2');
              dbSpec^.dbc^.UpdateRec(dbSpec^.dbr);
              dbSpec^.dbc^.UnlockRec(dbSpec^.dbr);
            end;
            if stat <> nil then
              isOK:= stat^.CanContinue;
          until not isOK or not mpssObj^.MPSSGetNext(dbSpec);
          if stat <> nil then
          begin
            stat^.CompPctLevel(1, 1);   {- force 100% }
            MSDisposeObj(stat)
          end
          else
            wait.Done;
        end
        else
          wait.Done;
      end;
      MSDisposeObj(mpssObj);
    end
    else
      Infomsg(aParent^.hwindow, '', SR(IDS_NOMPSSOBJ, pStr, 64));
    MSDisposeObj(dbSpec);
  end
  else
    Infomsg(aParent^.hwindow, '', SR(IDS_NOSPECFILE,pStr,64));
end;

END.
