Unit WLDMSErr;

INTERFACE

USES
	Strings,
	WObjects,
	WinTypes,
	WinProcs;

PROCEDURE SetError( errorNum : INTEGER );

Implementation

PROCEDURE SetError(errorNum : INTEGER);
{ Looks up the error number in the list of errors and displays the error
  text in a message box and waits for the user to acknowledge the error.}
VAR
	errStr : ARRAY[0..32] OF CHAR;
BEGIN
  Str(errorNum, errStr);
  StrCat(ADDR(errStr),' Error occurred!');
  MessageBox(0, errStr, 'DMS Error Monitor',  mb_TaskModal or mb_OK or mb_IconStop);
END;

END.
