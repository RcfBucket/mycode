Unit FileIO;

Interface

Uses
  Objects,
  ODialogs,
  OWindows,
  WinTypes,
  WinProcs,
  Strings,
  BtrAPIW;


Const


  B_Open          =    0;
  B_Close         =    1;
  B_Insert        =    2;
  B_Update        =    3;
  B_Delete        =    4;
  B_Get_EQU       =    5;
  B_Next          =    6;
  B_Prev          =    7;
  B_Get_GT        =    8;
  B_Get_GTEQ      =    9;
  B_Get_Low       =   12;
  B_Get_High      =   13;
  B_Begin_Trans   =   19;
  B_End_Trans     =   20;
  B_Abort_Trans   =   21;
  B_Get_Pos       =   22;
  B_Get_Dir       =   23;
  B_Unlock        =   27;
  B_Reset         =   28;
  B_Loc_EQU       =   55;

  BL_Get_EQU      =  105;
  BL_Next         =  106;
  BL_Prev         =  107;
  BL_Get_GT       =  108;
  BL_Get_GTEQ     =  109;
  BL_Get_Low      =  112;
  BL_Get_High     =  113;
  BL_Begin_Trans  =  119;
  BL_Get_Dir      =  123;

  BL_Get_EQU_NW   =  205;
  BL_Get_High_NW  =  213;
  BL_Get_Dir_NW   =  223;

  DupError        =  5;
  FileBusyError   = 84;

  id_BtrvStat     = 101;
  id_OpCode       = 102;
  id_FileNumber   = 103;
  id_Location     = 104;
  id_InternalID   = 105;

	ToIntMapFile = 1;
	ToDMSMapFile = 2;
	SendKeyFile = 3;
	MnemPrimFile = 4;
  AltPrimFile = 5;

  MaxMnemLen = 12;  { mnemonic field maximum length }
  MaxAltLen = 32;   { interface xref maximum length }
  MaxPatIDLen = 12;
  MaxSpecIDLen = 8;
  MaxIsoIDLen = 3;
  MaxDateLen = 10;
  MaxOrderIDLen = 8;

Type
	PToDMSMapRec = ^TToDMSMapRec;
	TToDMSMapRec = record
		case integer of
			1:(IntRecNo  : integer;
				 IntIdxNo  : integer;
				 DMSFileNo : integer;
				 DMSIdxNo  : integer);
			2:(Start     : integer);
			3:(RecPos    : longint);
		end;

	TToDMSMapKey = record
		case integer of
			1:(IntRecNo  : integer;
				 IntIdxNo  : integer;
				 DMSFileNo : integer;
				 DMSIdxNo  : integer);
			2:(Start     : integer);
		end;

	PToIntMapRec = ^TToIntMapRec;
	TToIntMapRec = record
		case integer of
			1:(DMSFileNo : integer;
				 DMSIdxNo  : integer;
				 IntRecNo  : integer;
				 IntIdxNo  : integer);
			2:(Start     : integer);
			3:(RecPos    : longint);
		end;

  TToIntMapKey = record
		case integer of
			1:(DMSFileNo : integer;
				 DMSIdxNo  : integer;
				 IntRecNo  : integer;
				 IntIdxNo  : integer);
			2:(Start     : integer);
		end;

	TIntKey = record
		case integer of
			1:(IntRecNo  : integer;
				 IntIdxNo  : integer);
			2:(Start     : integer);
    end;

	TDMSKey = record
		case integer of
			1:(DMSFileNo  : integer;
				 DMSIdxNo  : integer);
			2:(Start     : integer);
    end;

	PSendKeyRec = ^TSendKeyRec;
	TSendKeyRec = record
		case integer of
      1:(PatientID  : array[0..MaxPatIDLen] of char;
         CollectDate: array[0..MaxDateLen] of char;
         SpecimenID : array[0..MaxSpecIDLen] of char;
         IsolateID  : array[0..MaxIsoIDLen] of char);
      2:(Start     : integer);
			3:(RecPos    : longint);
		end;

  TSendKeyKey = record
		case integer of
      1:(PatientID  : array[0..MaxPatIDLen] of char;
         CollectDate: array[0..MaxDateLen] of char;
         SpecimenID : array[0..MaxSpecIDLen] of char;
         IsolateID  : array[0..MaxIsoIDLen] of char);
      2:(Start     : integer);
		end;

	PMnemPrimRec = ^TMnemPrimRec;
	TMnemPrimRec = record
		case integer of
			1:(MType     : array[0..2] of char;
         Mnemonic  : array[0..MaxMnemLen] of char;
         Alternate : array[0..MaxAltLen] of char;
				 IsPrim    : array[0..2] of char);
			2:(Start     : integer);
			3:(RecPos    : longint);
		end;

  TMnemPrimKey = record
		case integer of
			1:(MType     : array[0..2] of char;
         Mnemonic  : array[0..MaxMnemLen] of char;
         Alternate : array[0..MaxAltLen] of char);
			2:(Start     : integer);
		end;

	TMnemSearchKey = record
		case integer of
			1:(MType     : array[0..2] of char;
         Mnemonic  : array[0..MaxMnemLen] of char);
			2:(Start     : integer);
		end;

	PAltPrimRec = ^TAltPrimRec;
	TAltPrimRec = record
		case integer of
			1:(MType     : array[0..2] of char;
         Alternate : array[0..MaxAltLen] of char;
         Mnemonic  : array[0..MaxMnemLen] of char;
				 IsPrim    : array[0..2] of char);
			2:(Start     : integer);
			3:(RecPos    : longint);
		end;

  TAltPrimKey = record
		case integer of
			1:(MType     : array[0..2] of char;
         Alternate : array[0..MaxAltLen] of char;
         Mnemonic  : array[0..MaxMnemLen] of char);
			2:(Start     : integer);
		end;

	TAltSearchKey = record
		case integer of
			1:(MType     : array[0..2] of char;
         Alternate : array[0..MaxAltLen] of char);
			2:(Start     : integer);
		end;

	{ Database Error Dialog }
  PDBError = ^TDBError;
  TDBError = Object(TDialog)
    Procedure SetupWindow; virtual;
  End;

  { Btrieve Data Structs }
  BlockPosType       =Record
                        Case Integer Of
                         1:(Blk         : Packed array [1..128] of char);
                         2:(Start       : Integer);
                        End;

  SizeType           =Record
                        Case Integer Of
                         1:(Len         : Integer);
                         2:(Start       : Integer);
                        End;

  FileNameType       =Record
                       Case Integer Of
                         1:(Name        : Array [1..14] of Char);
                         2:(Start       : Integer);
                        End;

  BtrvDataType       =Record
                        BlockPos  : BlockPosType;
                        Size      : SizeType;
                        FileName  : FileNameType;
                       End;

  ByteSetType        =Set of Byte;





  Function CallDB(    Operator, FileNum   : Integer;
                  Var DataRecAddr, KeyAddr: Integer;
                      KeyNum              : Byte): Integer;
  Procedure CheckStatus(WindIn: HWnd; InTransaction: Boolean; ErrorCode: Word);
  Procedure OpenCloseFiles(TempWin: HWnd; Operator: Integer; FileSet: ByteSetType);
  Function BeginTrans: Integer;
  Function EndTrans: Integer;

Var
  BtrvStat      : Integer;
  LastDBOp      : Word;
  LastDBFile    : Word;
  LastLocation  : Word;
	BtrvData      : Array [1..5] of BtrvDataType;
  FatalErrorOccurred: Boolean;

Implementation


  Function CallDB(    Operator, FileNum   : Integer;
                  Var DataRecAddr, KeyAddr: Integer;
                      KeyNum              : Byte): Integer;
  Var
    TempLen : word;
    x       : word;
	Begin
    TempLen:= BtrvData[FileNum].Size.Len;
    If Operator = B_Open Then
      x:= BTRV(Operator, BtrvData[FileNum].BlockPos.Start, DataRecAddr, TempLen, BtrvData[FileNum].FileName.Start, 0)
    Else
      x:= BTRV(Operator, BtrvData[FileNum].BlockPos.Start, DataRecAddr, TempLen, KeyAddr, KeyNum);
    LastDBOp:= Operator;
    LastDBFile:= FileNum;
    CallDB:= x;
  End;

  Function BeginTrans: Integer;
  Var
    X : word;
  Begin
    BeginTrans:= BTRV(BL_Begin_Trans, X, X, X, X, 0);
  End;

  Function EndTrans: Integer;
  Var
    X : word;
  Begin
    EndTrans:= BTRV(B_End_Trans, X, X, X, X, 0);
  End;

  Procedure TDBError.SetupWindow;
  Var
    NumStr : Array [0..10] of Char;
  Begin
    inherited SetupWindow;
    Str(BtrvStat, NumStr);
    SetDlgItemText(HWindow, id_BtrvStat, NumStr);
    Str(LastDBOp, NumStr);
    SetDlgItemText(HWindow, id_OpCode, NumStr);
    Str(LastDBFile, NumStr);
    SetDlgItemText(HWindow, id_FileNumber, NumStr);
    Str(LastLocation, NumStr);
    SetDlgItemText(HWindow, id_Location, NumStr);
    Str(BtrvStat+LastDBOp+LastDBFile+LastLocation, NumStr);
    SetDlgItemText(HWindow, id_InternalID, NumStr);
  End;

  Procedure CheckStatus(WindIn: HWnd; InTransaction: Boolean; ErrorCode: Word);
  Var
    X         : word;
    ErrorDlg  : PDBError;
  Begin
    If BtrvStat <> 0 Then
    Begin
			FatalErrorOccurred:= True;
      LastLocation:= ErrorCode;
      If InTransAction Then
        x:= BTRV(B_ABORT_TRANS, x, x, x, x, 0);
      ErrorDlg:= New(PDBError, Init(Nil, 'DATABASE_ERROR'));
      Application^.ExecDialog(ErrorDlg);
      Application^.MainWindow^.CloseWindow;
      BtrvStat:= BtrvStop;
      Halt;
    End;
  End;

  Procedure OpenCloseFiles(TempWin: HWnd; Operator: Integer; FileSet: ByteSetType);
  Var
    x, y: Integer;
  Begin
		For x:= 1 to 5 Do
    Begin
      If X in FileSet Then
      Begin
        BtrvStat:= CallDB(Operator, x, y, y, 0);
        CheckStatus(TempWin, False, 1);
      End;
    End;
  End;

End.

