unit ChkSpace;
{----------------------------------------------------------------------------}
{  Module Name  : ChkSpace.PAS                                               }
{  Programmer   : MJS                                                        }
{  Date Created : 04/95                                                      }
{                                                                            }
{  Purpose -                                                                 }
{  This module houses the thresholds for the Database options feature.  This }
{  module implements the routines called by the Main DMS to determine        }
{  whether the user may proceed entering data into the database.             }
{                                                                            }
{  Assumptions -                                                             }
{  None                                                                      }
{                                                                            }
{  Initialization -                                                          }
{  Reads the initial thresholds stored in the DMS.ini file.                  }
{                                                                            }
{  Revision History -                                                        }
{                                                                            }
{  Version  Date      Who    Did What                                        }
{  -------  --------  ------ ----------------------------------------------- }
{  1.00     04/95     MJS    Created unit.                                   }
{                                                                            }
{----------------------------------------------------------------------------}

INTERFACE

uses
  Objects,
  DMSDebug,
  OWindows,
  WinTypes,
  WinProcs,
  Strings;

const
  minThreshold = 1;
  maxThreshold = 98;
var
  preventThreshold : byte;
  warningThreshold : byte;

function EnoughDiskSpace(aParent:PWindowsObject):boolean;

IMPLEMENTATION

uses
  WinDos,
  IniLib,
  APITools;

{$R CHKSPACE.RES}
{$I CHKSPACE.INC}

{---------------------------[ CheckPercent ]---------------------------------}
{  Checks the percentage of disk use of a specific file with the current     }
{  and displays a warning if the percentage is greater than the current      }
{  prevention threshold.                                                     }
{  Assumptions                                                               }
{    aParent is a valid windows object.                                      }
{  Errors returned                                                           }
{    none                                                                    }
{  Returns  : true if the percentage is below the threshold(and OK), or false}
{             otherwise.                                                     }
{  InParms  : the parent window to display the potential warning.            }
{             the percentage to check against the threshold.                 }
{  OutParms : none.                                                          }
{----------------------------------------------------------------------------}
function CheckPercent(aParent    : PWindowsObject;
                      percentage : longint) : boolean;
var
  tempMsg : array[0..255] of char;
begin
  CheckPercent := true;
  if (percentage > preventThreshold) then
  begin
    SR(IDS_PREVENTMSG, tempMsg, 255);
    ErrorMsg(aParent^.hWindow,'',tempMsg);
    CheckPercent := false;
  end
  else
    if (percentage > warningThreshold) then
    begin
      SR(IDS_WARNMSG, tempMsg, 255);
      ErrorMsg(aParent^.hWindow,'',tempMsg);
    end;
end;

{---------------------------[ EnoughDiskSpace ]------------------------------}
{  Determines the size of the hard disk and the amount of space that is free,}
{  determines the percentage and calls CheckPercent to determine if the      }
{  calculated percentage is greater than the prevention threshold.           }
{  Assumptions                                                               }
{    aParent is a valid windows object.                                      }
{  Errors returned                                                           }
{    none                                                                    }
{  Returns  : true if the percentage is below the threshold, or false        }
{             otherwise.                                                     }
{  InParms  : the parent window to display the potential warning.            }
{  OutParms : none.                                                          }
{----------------------------------------------------------------------------}
function EnoughDiskSpace(aParent:PWindowsObject): boolean;
var
  diskUsed        : longint;
  dSize           : longint;
  dFree           : longint;
  percentageUsed  : word;
begin
  dSize := DiskSize(0) div 1024;
  dFree := DiskFree(0) div 1024;
  diskUsed := dSize-dFree;
  percentageUsed := ((diskUsed*100) div dSize) + 1;
  EnoughDiskSpace := CheckPercent(aParent,percentageUsed);
end;

procedure CheckSpaceInit;
begin
  warningThreshold := ReadWarningThreshold;
  preventThreshold := ReadPreventionThreshold;
end;

BEGIN
  CheckSpaceInit;
END.


