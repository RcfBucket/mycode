unit ESBL;
{----------------------------------------------------------------------------}
{  Module Name  : ESBL.PAS                                                   }
{  Programmer   : FL                                                         }
{  Date Created : 04/98                                                      }
{                                                                            }
{  Purpose     -  Provide core ESBL functionality to the JDMS v4.            }
{                                                                            }
{  Assumptions -                                                             }
{  None.                                                                     }
{                                                                            }
{  Revision History -                                                        }
{                                                                            }
{  Version  Date      Who    Did What                                        }
{  -------  --------  ------ ----------------------------------------------- }
{  1.00     04/98     FL     Initial release.                                }
{                                                                            }
{----------------------------------------------------------------------------}

Interface

Uses
  OrgGroup,
  ResLib,
  Trays;

Const
  ESBLOrgs  ='ESBLOrgMapping';
  ESBLTest  = '.ESBL';
  ESBLCat   = 'ESBL';
  ESBLNA    = $00;
  ESBLCeph  = $FF;

  ESBLIntrStr = 'ESBL';
  EBLIntrStr  = 'EBL?';
  RStarIntrStr= 'R*';

Type
  ESBLMdis  = Array[1..MaxDrugs] of Byte;
  ESBLRes   = (rESBL, rEBL, rESBLNA);

  ESBLRecord=
  Record
    Drugs  : Integer;
    Mdis   : ESBLMdis;
  End;

  function  ESBLOrg(OrgNum: Integer) : boolean;
  function  ESBLMics(PanelType: Integer; var ESBLInfo: ESBLRecord) : boolean;
  function  ESBLMicRule(DrugMIC, ESBLMdi: Integer): Boolean;
  function  ESBLCriterion(Organism: Integer; Results: PResults): Boolean;


var
  ESBLIntrpCode,
  EBLIntrpCode,
  RStarIntrpCode: Integer;

Implementation

Uses
  IntrpMap,
  MScan,
  Screens,
  Strings;

Const
  ESBLFileName  ='ESBL.DAT';

  NRMask    = $80;
  GTMask    = $20;
  LTMask    = $40;
  GTStep    = $F0;
  LTStep    = $00;

  function ESBLOrg(OrgNum: Integer) : boolean;
  begin
    ESBLOrg:=ScreenApplies(ESBLSection,OrgNum);
  end;

  function ESBLMics(PanelType: Integer; var ESBLInfo: ESBLRecord) : boolean;
  Var ESBLFile : File of ESBLRecord;
  begin
    Assign(ESBLFile, ESBLFileName);
    Reset(ESBLFile);
    Seek(ESBLFile, PanelType);
    Read(ESBLFile, ESBLinfo);
    Close(ESBLFile);
    ESBLMics:=(IOResult=0) and (ESBLInfo.Drugs>0);
  end;

  Function ESBLMicRule(DrugMIC, ESBLMdi: Integer): Boolean;
  begin
    {Always consider the NR Bit!}
    if ((DrugMIC and NRMask)<>0) then
         DrugMIC:=LTStep
    else
    {Always consider the GT Bit!}
    if ((DrugMIC and GTMask)<>0) then
         DrugMIC:=GTStep
    else
    {Always consider the LT Bit!}
    if ((DrugMIC and LTMask)<>0) then
         DrugMIC:=LTStep
    else DrugMIC:=DrugMIC and $1F;

    {Compare the MIC to its min ESBL mdi}
    ESBLMicRule:=(ESBLMdi<>ESBLNA) and (DrugMIC >= ESBLMdi);
  end;

  function ESBLCriterion(Organism: Integer; Results: PResults): Boolean;
  var ESBLInfo : ESBLRecord;
      RDN      : Integer;
      MicStr   : Array [0..32] of Char;
      DrugMic  : Byte;
      Idx      : Integer;
      MicFound : Boolean;
      sDrugName: String;
      pDrugName: Array [0..100] of Char;
      TestGroup: Array [0..100] of Char;
      PanelType: Integer;
  begin
    ESBLCriterion:=False;
    If Results^.MicResultsExist then
    if ScreenOn(ESBLScreen) then
    If ESBLOrg(Organism) then
    begin
      MicFound:=False;
      Strcopy(TestGroup,'');
      {Inspect each panel in this order}
      While Results^.NextTestGroup(TestGroup, idx) and (Not MicFound) do
      begin
        PanelType:=Results^.TraysObj^.MapTestGroupID(TestGroup);
        If ESBLMics(PanelType, ESBLInfo) then
        begin
          {Traverse all drugs}
          RDN:=1;
          Results^.TraysObj^.LoadTrayByNum(PanelType);
          While (RDN<=ESBLInfo.Drugs) and (Not MicFound) do
          begin
            {Get this drug's result and compare it against its threshold mdi}
            With Results^.TraysObj^ do AdnToAbbrConvert(RdnToAdn(Rdn), sDrugName);
            StrPCopy(pDrugName,sDrugName);
            Results^.GetResult(pDrugName,'MIC',MicStr,32);
            Results^.MicToByte(MicStr,DrugMic);
            MicFound:= ESBLMicRule(DrugMic,ESBLInfo.Mdis[RDN]);
            RDN:=RDN+1;
          end;
        end;{if..}
      end;{while..}
      ESBLCriterion:=MicFound;
    end;{if ..}
  end;

Begin
  ESBLIntrpCode :=InterpStrToCode(ESBLIntrStr,false);
  EBLIntrpCode  :=InterpStrToCode(EBLIntrStr,false);
  RStarIntrpCode:=InterpStrToCode(RStarIntrStr,false);
End.