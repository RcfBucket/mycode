Unit RptData;

{- miscellaneous data structures and routines to manipulate the structures }

INTERFACE

uses
  DMSDebug,
  DBTypes,
  strings,
  prnObj,
  WinProcs,
  WinTypes;

type
  {- types used for display }
  TRptUnits = (RptInch, RptMM, RptCM);

  {- structure to hold report generator preferences }
  TPreference = record
    units     : TRptUnits;
  end;

var
  hFileMenu, hFileWindowMenu: HMenu;
  hRptMenu, hRptWindowMenu  : HMenu;

  preferences : TPreference;
  prn         : PPrinter;   {- global printer object used to get printer info }

IMPLEMENTATION

BEGIN
  preferences.units:= RptInch;
END.

{- rcf }
