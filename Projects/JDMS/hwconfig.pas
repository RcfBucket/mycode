unit HWConfig;

INTERFACE

uses
  APITools,
  OWindows,
  WinTypes,
  WinProcs,
  DlgLib,
  DMSDebug,
  Strings,
  MScan;

{$I HWCONFIG.INC}
{$R HWCONFIG.RES}

const
  NoPort         = 0;
  GPIBPort       = 1;
  COM1Port       = 3;
  COM2Port       = 4;
  COM3Port       = 5;
  AS4            = 1;
  WA             = 2;
  TWOWAY         = 3;
  BarCode        = 4;
  pkWalkAway     ='WAEXISTS';
  pkTWOWAY       ='TWOWAYPORT';
  pkAS4          ='AS4PORT';
  pkBarcodePtr   ='BarcodePrinter';
  NCItohPrntnxPtr = 'CItohPrntnx';
  NDadeBarcodePtr = 'DadeBarcode';
  DMSSection      = 'DMS';

type
  PConfigDialog = ^TConfigDialog;
  TConfigDialog = object(TCenterDlg)
    com1IsPrinter : boolean;
    com2IsPrinter : boolean;
    com3IsPrinter : boolean;
    AS4Port : integer;
    TWOWAYPort : integer;
    WAPort : integer;
    Restart: boolean;
    constructor Init(aParent : PWindowsObject;
                      aTitle: PChar);
    destructor Done; virtual;
    function CanClose : boolean; virtual;
    procedure SetUpWindow; virtual;
    function GetPortAssignment(keyName: PChar) : integer; virtual;
    procedure SetPortAssignment(keyName : PChar;
                                keyPort : integer); virtual;
    function ValidatePortAssignments : boolean; virtual;
    function SetPort(inst : integer) : boolean; virtual;
    function CheckWinIniFile(comPortStr:PChar): boolean;
    procedure SetOptions;
    function  GetBarcodePtrAssignment : string; virtual;
    procedure SetBarcodePtrAssignment(ThePrinter : string); virtual;
  end;

procedure ConfigureHW(aParent : PWindowsObject);
function HWExists(profileKey: PChar): boolean;

IMPLEMENTATION

function HWExists(profileKey: PChar): boolean;
var
  portAssigned : array[0..16] of char;
begin
  GetPrivateProfileString(appName, profileKey, 'NONE', portAssigned, 16, profFile);
  HWExists:= StrIComp(portAssigned, 'NONE') <> 0;
end;

constructor TConfigDialog.Init(aParent: PWindowsObject; aTitle: PChar);
{===================================================================}
{                                                                   }
{===================================================================}
begin
  if not inherited Init(aParent, aTitle) then
  begin
   Done;
   Fail;
  end;

  restart:= false;
  com1IsPrinter:= false;
  com2IsPrinter:= false;
  com3IsPrinter:= false;
end;

destructor TConfigDialog.Done;
{===================================================================}
{                                                                   }
{===================================================================}
begin
  inherited Done;
  if Restart then HALT;
end;

function TConfigDialog.CanClose: boolean;
{===================================================================}
{                                                                   }
{===================================================================}
var
  caption : array[0..50] of Char;
  warning : array[0..255] of Char;
begin
  CanClose:= false;
  if inherited CanClose then
  begin
    if ValidatePortAssignments then
    begin
      SR(IDS_WARNINGMSG, caption, sizeof(caption)-1);
      if WAPort = GPIBPort then
      begin
        SR(IDS_LPT1Msg, warning, sizeof(warning)-1);
        InfoMsg(hWindow, caption,warning);
      end;
      SR(IDS_EXITMSG, warning, sizeof(warning)-1);
      InfoMsg(hWindow, caption, warning);
      restart:= true;
      CanClose:= true;
    end
    else
      SetOptions;
  end;
end;

function TConfigDialog.GetPortAssignment(keyName: PChar) : integer;
{===================================================================}
{                                                                   }
{===================================================================}
var
  portAssigned : array[0..16] of char;
begin
  GetPortAssignment:= 0;  { default to none }
  GetPrivateProfileString(appName, keyName, 'NONE', portAssigned, sizeof(portAssigned)-1, profFile);
  if StrIComp(portAssigned, 'NONE') = 0 then
    GetPortAssignment:= noPort
  else if StrIComp(portAssigned, 'GPIB') = 0 then
    GetPortAssignment:= GPIBPort
  else if StrIComp(portAssigned, 'COM1') = 0 then
    GetPortAssignment:= Com1Port
  else if StrIComp(portAssigned, 'COM2') = 0 then
    GetPortAssignment:= Com2Port
  else if StrIComp(portAssigned, 'COM3') = 0 then
    GetPortAssignment:= Com3Port;
end;

function TConfigDialog.SetPort(inst : integer) : boolean;
begin
  SetPort:= false;
  case inst of
    Barcode:
      Begin
        If ( SendDlgItemMsg(IDC_DadeBarcodePtr, BM_GETCHECK, 0, 0) <> 0 ) Then
          SetBarcodePtrAssignment(NDadeBarcodePtr)
        Else
          SetBarcodePtrAssignment(NCItohPrntnxPtr);
      End;
    AS4:
      begin
        if SendDlgItemMsg(IDC_COM1AS4, BM_GETCHECK, 0, 0) <> 0 then
          if AS4Port = noPort then
            AS4Port:= COM1Port
          else
            SetPort:= true;
        if SendDlgItemMsg(IDC_COM2AS4, BM_GETCHECK, 0, 0) <> 0 then
          if AS4Port = noPort then
            AS4Port:= COM2Port
          else
            SetPort:= true;
        if SendDlgItemMsg(IDC_COM3AS4, BM_GETCHECK, 0, 0) <> 0 then
          if AS4Port = noPort then
            AS4Port:= COM3Port
          else
            SetPort:= true;
      end;
    WA:
      begin
        if SendDlgItemMsg(IDC_WAEXISTS, BM_GETCHECK, 0, 0) <> 0 then
          if WAPort = noPort then
            WaPort:= GPIBPort
          else
            SetPort:= true;
      end;
    TWOWAY:
      begin
        if SendDlgItemMsg(IDC_COM1TWOWAY, BM_GETCHECK, 0, 0) <> 0 then
          TWOWAYPort:= COM1Port;
        if SendDlgItemMsg(IDC_COM2TWOWAY, BM_GETCHECK, 0, 0) <> 0 then
          if TWOWAYPort = noPort then
            TWOWAYPort:= COM2Port
          else
            SetPort:= true;
      end;
  end; {- case }
end;

function TConfigDialog.ValidatePortAssignments: boolean;
{===================================================================}
{                                                                   }
{===================================================================}
var
  com1Used  : boolean;
  com2Used  : boolean;
  com3Used  : boolean;
  conflict  : boolean;
  pStr1     : array[0..100] of char;
  pStr2     : array[0..100] of char;
begin
  ValidatePortAssignments:= true;
  com1Used:= false;
  com2Used:= false;
  com3Used:= false;
  conflict:= false;
  AS4Port:= noPort;
  WAPort:= noPort;
  TWOWAYPort:= noPort;
  conflict:= SetPort(AS4);
  if not conflict then
    conflict:= SetPort(Barcode);
  if not conflict then
    conflict:= SetPort(WA);
  if not conflict then
    conflict:= SetPort(TWOWAY);
  if not conflict then
    case AS4Port of
      com1Port : com1Used:= true;
      com2Port : com2Used:= true;
      com3Port : com3Used:= true;
    end;
  if not conflict then
  begin
    case TWOWAYPort of
      com1Port :
        if com1Used then
          conflict:= true
        else
          com1Used:= true;
      com2Port :
        if com2Used then
          conflict:= true
    else
      com2Used:= true;
    end;
  end;
  if not conflict then
  begin
    SetPortAssignment(pkAS4,AS4Port);
    SetPortAssignment(pkWalkAway,WAPort);
    SetPortAssignment(pkTWOWAY,TWOWAYPort);
    exit;
  end;
  SR(IDS_CONFLICT, pStr1, sizeof(pstr1)-1);
  SR(IDS_WARNINGMSG, pStr2, sizeof(pstr2)-1);
  InfoMsg(hWindow, pstr2, pstr1);
(*  MessageBox(hWindow, pStr1, pStr2, mb_OK);*)
  ValidatePortAssignments:= false;
end;

procedure TConfigDialog.SetPortAssignment(keyName: PChar; keyPort : integer);
{===================================================================}
{                                                                   }
{===================================================================}
var
  portAssigned : array[0..16] of char;
begin
  case keyPort of
    noPort   :  WritePrivateProfileString(appName, keyName, 'NONE', profFile);
    GPIBPort :  WritePrivateProfileString(appName, keyName, 'GPIB', profFile);
    COM1Port :  WritePrivateProfileString(appName, keyName, 'COM1', profFile);
    COM2Port :  WritePrivateProfileString(appName, keyName, 'COM2', profFile);
    COM3Port :  WritePrivateProfileString(appName, keyName, 'COM3', profFile);
  end;
end;

procedure TConfigDialog.SetBarcodePtrAssignment(ThePrinter : string);
{===================================================================}
{                                                                   }
{===================================================================}
begin
  If ( ThePrinter=NDadeBarcodePtr ) Then
    WritePrivateProfileString(DMSSection, pkBarcodePtr, NDadeBarcodePtr, profFile)
  Else
    If ( ThePrinter=NCItohPrntnxPtr ) Then
      WritePrivateProfileString(DMSSection, pkBarcodePtr, NCItohPrntnxPtr, profFile)
    Else
      WritePrivateProfileString(DMSSection, pkBarcodePtr, NCItohPrntnxPtr, profFile);
end;

function TConfigDialog.GetBarcodePtrAssignment : string;
{===================================================================}
{                                                                   }
{===================================================================}
var
  PrinterAssigned : array[0..20] of char;
begin
  GetBarcodePtrAssignment := NCItohPrntnxPtr;  { default to CItoh/Printronix }
  GetPrivateProfileString(DMSSection, pkBarcodePtr, 'NONE', PrinterAssigned, sizeof(PrinterAssigned)-1, profFile);
  if PrinterAssigned = 'NONE' then
    GetBarcodePtrAssignment := NCItohPrntnxPtr
  Else
    GetBarcodePtrAssignment := PrinterAssigned;
end;

function TConfigDialog.CheckWinIniFile(comPortStr:PChar): boolean;
var
  buffer      : PChar;
  bufSize     : integer;
  portUsed    : array [0..63] of char;
  device      : array [0..63] of char;
  portIsRefed : boolean;
  position    : integer;
begin
  bufSize:= 1024;
  GetMem(buffer, bufSize);
  portIsRefed:= false;
  GetProfileString('devices', nil, '', buffer, bufSize-1);
  position:= 0;
  StrLCopy(device, buffer + position, SizeOf(device)-1);
  while (device[0] <> #0) and (not portIsRefed) do
  begin
    GetProfileString('devices', device, '', portUsed, 63);
    if StrPos(portUsed, comPortStr) = nil then
    begin
      position:= position + StrLen(device) + 1;
      StrLCopy(device, buffer + position, SizeOf(device)-1);
    end
    else
      portIsRefed:= true;
  end;
  CheckWinIniFile:= portIsRefed;
  FreeMem(buffer, bufSize);
end;

procedure TConfigDialog.SetOptions;
Var
  CurrentBarcodePtr : string;
begin
  AS4Port:= GetPortAssignment(pkAS4);
  TWOWAYPort:= GetPortAssignment(pkTWOWAY);
  WAPort:= GetPortAssignment(pkWalkAway);
  if com1IsPrinter then
  begin
    EnableWindow(GetItemHandle(IDC_COM1NONE), false);
    EnableWindow(GetItemHandle(IDC_COM1AS4), false);
    EnableWindow(GetItemHandle(IDC_COM1TWOWAY), false);
  end
  else
  begin
    SendDlgItemMsg(IDC_COM1NONE, BM_SETCHECK, 1, 0);
    SendDlgItemMsg(IDC_COM1AS4, BM_SETCHECK, 0, 0);
    SendDlgItemMsg(IDC_COM1TWOWAY, BM_SETCHECK, 0, 0);
  end;
  if com2IsPrinter then
  begin
    EnableWindow(GetItemHandle(IDC_COM2NONE), false);
    EnableWindow(GetItemHandle(IDC_COM2AS4), false);
    EnableWindow(GetItemHandle(IDC_COM2TWOWAY), false);
  end
  else
  begin
    SendDlgItemMsg(IDC_COM2NONE, BM_SETCHECK, 1, 0);
    SendDlgItemMsg(IDC_COM2AS4, BM_SETCHECK, 0, 0);
    SendDlgItemMsg(IDC_COM2TWOWAY, BM_SETCHECK, 0, 0);
  end;
  if com3IsPrinter then
  begin
    EnableWindow(GetItemHandle(IDC_COM3NONE), false);
    EnableWindow(GetItemHandle(IDC_COM3AS4), false);
  end
  else
  begin
    SendDlgItemMsg(IDC_COM3NONE, BM_SETCHECK, 1, 0);
    SendDlgItemMsg(IDC_COM3AS4, BM_SETCHECK, 0, 0);
  end;
  if WAPort = GPIBPort then
    SendDlgItemMsg(IDC_WAEXISTS,BM_SETCHECK, 1, 0)
  else
    SendDlgItemMsg(IDC_WAEXISTS, BM_SETCHECK, 0, 0);

  if AS4Port <> noPort then
    case AS4Port of
      COM1Port :
        if not com1IsPrinter then
        begin
          SendDlgItemMsg(IDC_COM1AS4, BM_SETCHECK, 1, 0);
          SendDlgItemMsg(IDC_COM1NONE, BM_SETCHECK, 0, 0);
        end;
      COM2Port :
        if not com2IsPrinter then
        begin
          SendDlgItemMsg(IDC_COM2AS4, BM_SETCHECK, 1, 0);
          SendDlgItemMsg(IDC_COM2NONE, BM_SETCHECK, 0, 0);
        end;
      COM3Port :
        if not com3IsPrinter then
        begin
          SendDlgItemMsg(IDC_COM3AS4, BM_SETCHECK, 1, 0);
          SendDlgItemMsg(IDC_COM3NONE, BM_SETCHECK, 0, 0);
        end;
    end;

  if TWOWAYPort <> noPort then
    case TWOWAYPort of
      COM1Port :
        if not com1IsPrinter then
        begin
          SendDlgItemMsg(IDC_COM1TWOWAY, BM_SETCHECK, 1, 0);
          SendDlgItemMsg(IDC_COM1NONE, BM_SETCHECK, 0, 0);
        end;
      COM2Port :
        if not com2IsPrinter then
        begin
          SendDlgItemMsg(IDC_COM2TWOWAY, BM_SETCHECK, 1, 0);
          SendDlgItemMsg(IDC_COM2NONE, BM_SETCHECK, 0, 0);
        end;
    end;

(*  If (Pos(NDadeBarcodePtr,GetBarcodePtrAssignment)>0) then*)
  CurrentBarcodePtr := GetBarcodePtrAssignment;
  If (Strcomp(NDadeBarcodePtr,Addr(CurrentBarcodePtr[1]))=0) then
    SendDlgItemMsg(IDC_DadeBarcodePtr,BM_SETCHECK, 1, 0)    {select the dade barcode printer}
  Else
    If (Strcomp(NCItohPrntnxPtr,Addr(CurrentBarcodePtr[1]))=0) then
      SendDlgItemMsg(IDC_DadeBarcodePtr,BM_SETCHECK, 0, 0)  {do not select the dade barcode printer}
                                                            {default to the CItoh/Printronix printer}
    Else
      SendDlgItemMsg(IDC_DadeBarcodePtr,BM_SETCHECK, 0, 0); {do not select the dade barcode printer}
                                                            {default to the CItoh/Printronix printer}
end;

procedure TConfigDialog.SetupWindow;
{===================================================================}
{                                                                   }
{===================================================================}
var
  caption : array[0..50] of char;
  warnStr : array[0..255] of char;
begin
  inherited SetUpWindow;
  com1IsPrinter:= CheckWinIniFile('COM1');
  com2IsPrinter:= CheckWinIniFile('COM2');
  com3IsPrinter:= CheckWinIniFile('COM3');
  SetOptions;
{  if com1IsPrinter or com2IsPrinter or com3IsPrinter then
  begin
    SR(IDS_WARNINGMSG, caption, sizeof(caption)-1);
    SR(IDS_COMMUSED, warnStr, sizeof(warnStr)-1);
    InfoMsg(hWindow, caption, warnStr);
  end; }
end;

procedure ConfigureHW(aParent : PWindowsObject);
var
  wndw : PConfigDialog;
begin
  wndw:= New(PConfigDialog, init(aParent, MakeIntResource(DLG_HWCONFIG)));
  if wndw <> nil then
    application^.ExecDialog(wndw);
end;

END.
