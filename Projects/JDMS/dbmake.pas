{----------------------------------------------------------------------------}
{  Module Name  : DBMAKE.PAS                                                 }
{  Programmer   : EJ                                                         }
{  Date Created : 11/01/94                                                   }
{                                                                            }
{  Purpose -                                                                 }
{  This module is used to create database files from a ASCII text file.      }
{                                                                            }
{  Assumptions -                                                             }
{  None.                                                                     }
{                                                                            }
{  Initialization -                                                          }
{  The log file DBMAKE.OUT is rewritten each time the program is run.        }
{                                                                            }
{  Revision History -                                                        }
{                                                                            }
{  Version  Date      Who    Did What                                        }
{  -------  --------  ------ ----------------------------------------------- }
{  1.00     11/01/94  EJ     Initial release                                 }
{                                                                            }
{----------------------------------------------------------------------------}


program DBMake;

uses
  MScan,
  WinCrt,
  Strings,
  StrsW,
  Objects,
  WaitLib,
  DBErrors,
  DBTypes,
  DBMkLib,
  DBNDesc,
  DBLib,
  DMSDebug;

var
  fil        : Text;
  sf         : array[0..150] of char;
  ff         : array[0..150] of char;
  pf         : array[0..150] of char;
  sd         : array[0..150] of char;
  oflag      : array[0..150] of char;
  oflg       : char;
  outFil     : Text;
  errCnt     : integer;
  displayStr : array[1..10] of string;
  autoExit   : boolean;

{----------------------------------------------------------------------------}
{  Writes a line of text to the screen and to an output file.                }
{                                                                            }
{  InParms : s is the string of text.                                        }
{----------------------------------------------------------------------------}
procedure DWriteln(s: string);
begin
  Writeln(s);
  Writeln(outFil, s);
end;

{----------------------------------------------------------------------------}
{  Writes a line of text and a number to the screen and an output file.      }
{                                                                            }
{  InParms : s is the string of text.                                        }
{            num is the number to write.                                     }
{----------------------------------------------------------------------------}
procedure DWritelnSNum(s: string; num: integer);
begin
  Writeln(s, num);
  Writeln(outFil, s, num);
end;

{----------------------------------------------------------------------------}
{  Writes a string and a number to the screen and an output file.            }
{                                                                            }
{  InParms : s is the string of text.                                        }
{            num is the number to write.                                     }
{----------------------------------------------------------------------------}
procedure DWriteSNum(s: string; num: integer);
begin
  Write(s, num);
  Write(outFil, s, num);
end;

{----------------------------------------------------------------------------}
{  Writes a number to the screen and an output file.                         }
{                                                                            }
{  InParms : num is the number to write.                                     }
{            w is the width to write.                                        }
{----------------------------------------------------------------------------}
procedure DWriteNum(n: integer; w: integer);
begin
  Write(n:w);
  Write(outfil, n:w);
end;

{----------------------------------------------------------------------------}
{  Writes a string to the screen and an output file.                         }
{                                                                            }
{  InParms : s is the string of text.                                        }
{----------------------------------------------------------------------------}
procedure DWrite(s: string);
begin
  Write(s);
  Write(outFil, s);
end;

{----------------------------------------------------------------------------}
{  Displays an error and line number, closes the DBMAKE text file,           }
{  and halts.                                                                }
{                                                                            }
{  InParms : stat is the status struct.                                      }
{----------------------------------------------------------------------------}
procedure Error(stat: TMkStatus);
var
  pstr: array[0..100] of char;
begin
  DWriteln('');
  DWrite('Error: ');

  DBMkLibErrStr(stat.errCode, pstr, 100);
  DWriteln(StrPas(pstr));
  DWriteln('');

  DWrite('File : ' + StrPas(sf) + '  ');
  DWritelnSNum('Line : ', stat.errLine);
  DWriteln('');

  Close(outFil);
  Halt(1);
end;

{----------------------------------------------------------------------------}
{  Writes a string and spaces to the screen and an output file.              }
{                                                                            }
{  InParms : s is the string of text.                                        }
{            padlen is the length of field to pad up to.                     }
{----------------------------------------------------------------------------}
procedure Writepad(s: string; padlen: integer);
var
  k  : integer;
begin
  DWrite(s);
  for k := 1 to padLen - Length(s) do
    DWrite(' ');
end;

{----------------------------------------------------------------------------}
{  Dumps information for a DBReadDesc object.                                }
{                                                                            }
{  InParms : d is a PDBReadDesc object.                                      }
{----------------------------------------------------------------------------}
procedure DumpInfo(d: PDBReadDesc);
var
  li   : TFileInfo;
  fi   : TFldInfo;
  ki   : TKeyInfo;
  ai   : TAssocInfo;
  di   : TDelAssocInfo;
  base : integer;
  k    : integer;
  f    : integer;
  s    : integer;
  pstr : array[0..100] of char;
  w    : word;
begin
(*base := d^.DataType * 100; *)
  base := 0;
  d^.FileInfo(li);
  DWriteln('File            : ' + StrPas(li.fName) +
           '    Path: ' + StrPas(d^.FilePath(pstr, 100)));
  DWriteln('Description     : ' + StrPas(li.desc));
  DWrite('File user flag  : 0x');
  w := li.userFlag;
  DWriteln(StrPas(HexStr(w, sizeof(w), pstr, 100)));
  DWrite('Auto Seq Numbers: ');
  if li.autoSeq then
    DWriteln('Yes')
  else
    DWriteln('No');
  DWritelnSNum('Record size     : ', d^.RecordSize);
  DWritelnSNum('Number of fields: ', d^.NumFields);
  DWritelnSNum('Number of keys  : ', d^.NumKeys);
  DWritelnSNum('Number of assocs: ', d^.NumAssocs);
  DWritelnSNum('Number of dels  : ', d^.NumDelAssocs);
  DWriteln('');
  DWriteln('Fld Name                      Size Type     Assn     Flag');
  DWriteln('--- ------------------------- ---- -------- ---- ----------');
  for f := 1 to d^.NumFields do
  begin
    DWritenum(f, 3);
    DWrite(' ');
    d^.FieldInfo(f + base, fi);
    Writepad(StrPas(fi.fldName), maxFldNameLen);
    DWrite(' ');
    DWritenum(fi.fldSize,4);
    DWrite(' ');
    Writepad(StrPas(DBTypeStr(fi.fldType, pstr, 100)), 8);
    DWrite(' ');
    DWritenum(fi.fldAssoc,4);
    DWrite(' ');
    HexStr(fi.userFlag, sizeof(fi.userflag), pstr, 100);
    DWrite('    0x');
    DWrite(StrPas(pstr));
    DWriteln('');
  end;
  DWriteln('');
  DWriteln('Key Name                      Unique     Flag   NumSegs KeyLen');
  DWriteln('--- ------------------------- ------ ---------- ------- ------');
  for k := 1 to d^.NumKeys do
  begin
    DWritenum(k, 3);
    DWrite(' ');
    d^.KeyInfo(k + base, ki);
    WritePad(StrPas(ki.keyName), maxKeyNameLen);
    DWrite(' ');
    if ki.unique then
      DWrite('YES    ')
    else
      DWrite('NO     ');
    HexStr(ki.keyFlag, sizeof(ki.keyflag), pstr, 100);
    DWrite('    0x');
    DWrite(StrPas(pstr)+' ');
    DWriteNum(ki.numSegs,7);
    DWrite(' ');
    DWritenum(d^.KeyLength(k), 6);
    DWriteln('');
  end;
  if d^.NumKeys > 0 then
  begin
    DWriteln('');
    DWriteln('Key segments [fldName(A/D], [...]  where A=Ascending, D=Descending');
    DWriteln('------------------------------------------------------------------');
    for k := 1 to d^.NumKeys do
    begin
      DWrite('Key: ');
      DWritenum(k, 3);
      DWrite(' -- ');
      d^.KeyInfo(k + base, ki);
      if ki.numSegs = 0 then
        DWrite('no segments defined')
      else
        for s := 1 to ki.numSegs do
        begin
          f := ki.segList[s].fldNum;
          d^.FieldInfo(f + base, fi);
          DWrite(StrPas(fi.fldName) + '(');
          if ki.segList[s].descend then
            DWrite('D)')
          else
            DWrite('A)');
          if s < ki.numSegs then
            DWrite(', ');
        end;
      DWriteln('');
    end;
  end;
  if d^.NumAssocs > 0 then
  begin
    DWriteln('');
    DWriteln('Asn Filename Key Cnt Fld1 Fld2 Fld3 Fld4 Const');
    DWriteln('--- -------- --- --- ---- ---- ---- ---- -------------------------');
  end;
  for f := 1 to d^.NumAssocs do
  begin
    DWritenum(f, 3);
    DWrite(' ');
    d^.AssocInfo(f + base, ai);
    Writepad(StrPas(ai.fName), maxFileNameLen);
    DWrite(' ');
    DWritenum(ai.keyNum,3);
    DWrite(' ');
    DWritenum(ai.numFields,3);
    for k := 1 to maxAssocFlds do
      DWritenum(ai.fldNum[k],5);
    DWrite(' ');
    Writepad(StrPas(ai.constFldValue), maxConstLen);
    DWriteln('');
  end;
  if d^.NumDelAssocs > 0 then
  begin
    DWriteln('');
    DWriteln('Del Filename Action   Rela Fieldname');
    DWriteln('--- -------- -------- ---- -------------------------');
  end;
  for f := 1 to d^.NumDelAssocs do
  begin
    DWritenum(f, 3);
    DWrite(' ');
    d^.DelAssocInfo(f + base, di);
    Writepad(StrPas(di.fName), maxFileNameLen);
    DWrite(' ');
    case di.delAction of
      DISALLOW_DELETE:
        DWrite('DISALLOW ');
      DELETE_ASSOCIATED:
        DWrite('DELETE   ');
      else
        DWrite('????     ');
    end;
    if di.bDownline then
      DWrite('Down ')
    else
      DWrite('Up   ');
    Writepad(StrPas(di.assocFldName), maxFldNameLen);
    DWriteln('');
  end;
  DWriteln('');
end;


{----------------------------------------------------------------------------}
{  Gets the name of a DBMAKE  pascal unit file  from either the              }
{  parameter list or by prompting the user.                                  }
{----------------------------------------------------------------------------}
procedure GetInput(parcnt : integer; dis1, dis2, default : string; input : PChar);
var
  tstr  : string;
  k     : integer;
begin
  if parCnt > ParamCount then
  begin
    autoexit:= false; {- if the user has to be prompted for params then dont auto exit}
    Writeln('');
    Writeln(dis1);
    Write(dis2);
    Readln(tstr);
    if ( length(tstr) = 0 ) then
      tstr := default;
  end
  else
  begin
    tstr:= ParamStr(parcnt);
  end;

  for k:= 1 to Length(tstr) do
    tstr[k] := Upcase(tstr[k]);
  StrPCopy(input,tstr);
end;

{----------------------------------------------------------------------------}
{  Reads the DBMAKE text file, creates database description files,           }
{  and creates BTRIEVE files.                                                }
{----------------------------------------------------------------------------}
procedure DoIt;
var
  status : TMkStatus;
  dbc    : PDBCreator;
  dbs    : PCollection;
  afile  : TFileInfo;

  procedure CreateIt(d: PDBReadWriteDesc); far;
  begin
    DWriteln('===================================================================');
    DumpInfo(d);
    dbc^.CreateTable(d);
    if dbc^.dbErrorNum <> 0 then
    begin
      DBErrorStr(dbc^.dbErrorNum, sf, 150);
      DWriteln('ERROR: '+StrPas(sf));
      Inc(errCnt);
    end
    else
    begin
(*    d^.FileInfo(afile);
      d^.FilePath(f, 100);
      StrCat(f, afile.fName);
      StrCat(f, '.DBT');
      WriteDBMakeFile( f, d, TRUE, TRUE ); *)
      DWriteln('FILE CREATED SUCCESSFULLY');
    end;
    DWriteln('===================================================================');
  end;

begin
  dbc:= New(PDBCreator, Init);
  dbs:= New(PCollection, Init(8, 8));
  oflg:= oflag[0];

  if ReadDBMakeFile(sf, ff, pf, sd, oflg, dbs, @outFil, status) then
  begin
    errCnt := 0;
    DWriteln('Description file read successfully.');
    DWriteln('Creating data files...');
    dbs^.ForEach(@CreateIt);
    DWriteNum(errCnt, 3);
    DWriteln(' errors occured.');
  end
  else
    Error(status);
  MSDisposeObj(dbs);
  MSDisposeObj(dbc);
end;


BEGIN
  autoExit:= true;
  displayStr[1] := 'Enter name of Field description file. (Default: fldnames.txt):';
  displayStr[2] := 'Enter name of Schema file. (Default: schema.txt):';
  displayStr[3] := 'Enter name of Pascal Unit file. (Default: dbids.pas):';
  displayStr[4] := 'Enter the Sub directory for the pascal unit. (Default: . {current}):';
  displayStr[5] := '''O''verwrite or ''A''ppend to pascal unit file. (Default:O):';

  Assign(outFil, 'DBMAKE.OUT');
  Rewrite(outFil);
  DWriteln('DBMake - Version '+dbversion);
  DWriteln('');
  GetInput(1, displayStr[1], 'Name: ','fldnames.txt', ff);
  GetInput(2, displayStr[2], 'Name: ','schema.txt', sf);
  GetInput(3, displayStr[3], 'Name: ','dbids.pas', pf);
  GetInput(4, displayStr[4], 'Name: ','.', sd);
  GetInput(5, displayStr[5], 'flag: ','O', oflag);
  DoIt;
  DWriteln('');
  DWriteln('Results have been output to DBMAKE.OUT');
  Close(outFil);
  if autoExit then
  begin
    MSWait(2000);
  end
  else
  begin
    Writeln;
    Write('Press enter to end...');
    Readln;
  end;
  DoneWinCrt;
END.

