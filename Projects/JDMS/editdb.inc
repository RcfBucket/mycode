(****************************************************************************


editdb.inc

produced by Borland Resource Workshop


*****************************************************************************)

const

	DLG_MAIN	=	$7031;
	DLG_PASSWORD	=	$7032;
	DLG_FILEINFO	=	$7033;
	DLG_EDITFILE	=	$7034;
	DLG_EDITREC	=	$7035;
	DLG_EDITFIELD	=	$7036;
	DLG_KEYSINFO	=	$7037;
	DLG_SETKEY	=	$7038;

	ICON_1	=	$7031;

	IDC_EDITFILE	=	101;
	IDC_FILEINFO	=	102;
	IDC_KEYSINFO	=	101;
	IDC_EDITREC	=	101;
	IDC_ADDREC	=	102;
	IDC_DELREC	=	103;
	IDC_READREC	=	104;
	IDC_SETKEY	=	105;
	IDC_EDITFIELD	=	110;
	IDC_PRINT	=	130;

	IDC_KEYLIST	=	200;
	IDC_FILELIST	=	200;
	IDC_FIELDLIST	=	200;
	IDC_RECORDLIST	=	200;
	IDC_PASSWORD	=	200;
	IDC_FILENAME	=	201;
	IDC_FILEDESC	=	202;
	IDC_FIELD	=	203;
	IDC_TYPE	=	204;
	IDC_SIZE	=	205;
	IDC_FLAG	=	206;
	IDC_VALUE	=	207;
	IDC_COUNT	=	208;
	IDC_SEQ	=	209;

	IDC_LBL1	=	501;
	IDC_LBL2	=	502;
	IDC_LBL3	=	503;
	IDC_LBL4	=	504;
	IDC_LBL5	=	505;
	IDC_LBL6	=	506;
	IDC_LBL7	=	507;
