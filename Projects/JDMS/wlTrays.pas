UNIT WLTrays;
{-----------------------------------------------------------------------}
{                                                                       }
{ written  :	03/03/83	by ??                                           }
{                                                                       }
{ update   :	05/23/84	by cd                                           }
{		11/12/84	by ??                                                    }
{		12/06/85 	by rr                                                 }
{		10/27/86	by bt	(major revision)                                   }
{		12/03/86	by bt	(added loading of mda)                             }
{		07/10/87	by bm	(major revision)                                   }
{		07/26/91	by rb (turboficated for use in windows                   }
{		09/10/91	by CMB Implement for KANJI DMS.                          }
{ copyright 1983, 1984, 1985, 1986, 1987 by MicroScan Travenol Labs     }
{ Copyright 1991 by Baxter Diagnostics Incorporated, Microscan.         }
{-----------------------------------------------------------------------}
INTERFACE
  USES Strings, WLMSCAN, WLDMSErr;
CONST
	MICLength = 4;
	MaxDil = 31;
	MaxWell = 300;
	MaxDilLen = 16;
	MaxMdaEntries = 31;	{ Maximum number of entries in an Master Dilution Array.}
	MaxExtra =   8;         { Max extra tests on a panel }
	MAXDrugs = 75;		{ Maximum absolute drugs in system	}
	MaxTray	= 75;		{ Max number of tray types		}
	MaxSystemDrugs = 99;	{ Max absolute drugs in the system }
	MaxMICLen=36;
	MaxBioChems = 47;	{  Max biochems in biotype }
        BioNameLen=4;

TYPE
	MICs=ARRAY[1..MaxMICLen] OF BYTE;
	IsoDilType = ARRAY[ 1..MaxMICLen ]      OF RECORD
					      Drug: STRING[4];
					      Dil: STRING[4];
					    END;
	AbbrStr		= STRING[3];
	MicStr		= STRING[MicLength];
	MicStrType	= STRING[7];
	MicAryType	= ARRAY[1..7] OF CHAR;
	TrayStr		= STRING[16];
        NameStr         = STRING[16];
	MdaTxtType	= ARRAY[0..39] OF MicStrType;
	MastDilArray = ARRAY[0..MaxMdaEntries] OF STRING[4];
	DrugDilType= ARRAY[ 0..MaxDilLen ]      OF STRING[4];
	TherMda = ARRAY[0..MaxMdaEntries] OF STRING[7]; { DMS 15 }
	XtraType = ARRAY[1..MaxExtra] OF BYTE;
	DrugRec = RECORD  {1+4=5}
		dilCnt	: BYTE;
		drugNum	: BYTE;
		END;
	MapPair	= RECORD
		iDrgNum	: BYTE;
		oDrgNum	: BYTE;
		END;
	AMapPair = RECORD
		iDrgAbr	: AbbrStr;
		oDrgAbr	: AbbrStr;
		END;
	MapTable	= ARRAY [0..20]       OF MapPair;
	AMapTable	= ARRAY [1..20]       OF AMapPair;
	AbrTable	= ARRAY [1..MaxSystemDrugs] OF AbbrStr;
	AdnTblType	= ARRAY [1..MaxDrugs] OF BYTE;
	DOrderType	= ARRAY [1..MaxSystemDrugs] OF BYTE;
	RtCntType	= ARRAY [1..MaxDrugs] OF BYTE;
	MdiMapTbl	= ARRAY [1..4, 0..31] OF BYTE;
	TrayRec = RECORD CASE INTEGER OF
	    0 : ( {1 + 1 + 17 + 1 + 1 + 2 + 1 + 8 + 300 + 150 + 1 + 1 = 484}
		  { Record (traynum + 2) }
		number	: BYTE;
		drugCnt	: BYTE;
		name	: TrayStr;
		probNum	: BYTE;
		group	: BYTE;
		idRec	: INTEGER;
		xtraCnt	: BYTE;
		xtraTbl	: XtraType;
		mic	: ARRAY[1..MaxWell] OF BYTE;
		drug	: ARRAY[1..MaxDrugs] OF DrugRec;
		sup	: BYTE;
		flag	: BYTE);
	    1 : ( {1 + 1 + 17 + (5*32=160) + (2*21=42) + 1 + 1 + 261 = 484}
		  { Record 0 }
		aNumber	: BYTE;
		trayCnt	: BYTE;
		aName	: TrayStr;
                mda	: MastDilArray;
		mapTbl	: MapTable;
		fstCust	: BYTE;
		custCnt	: BYTE;
		filler  : ARRAY[1..261] OF BYTE);
	    2 : ( {1 + 1 + 17 + 60 + 1 + 160 + 244 = 484}
		  { Record 1 }
		bNumber	: BYTE;
		bDrgCnt	: BYTE;
		bName	: TrayStr;
		tmStamp	: ARRAY[1..60] OF CHAR;
		aMapCnt	: BYTE;
		aMapTbl	: AMapTable;
		filler1 : ARRAY[1..244] OF BYTE);
	    3 : ( {1 + 1 + 17 + (99*4 = 396) + 69 = 484}
		  { Record 2 }
		cNumber	: BYTE;
		cDrgCnt	: BYTE;
		cName	: TrayStr;
		abrTbl	: AbrTable;
		filler2 : ARRAY[1..69] OF BYTE);
		END;
	DosageType	= RECORD CASE INTEGER OF
	    0:  ( {1 + 1 + 1 + 1 + 4 + 21 + 1 + 1 + 1 + 132 + 28 = 192}
		  { Record + (tray.number * 4) + ageGroup + maxTray + 1 }
		routeCnt	: BYTE;
		rtMap		: BYTE;
		micGroup	: BYTE;
		footNote	: CHAR;
		abbr		: AbbrStr;
		name		: STRING[20];
		altered		: BYTE;
		futNote2	: CHAR;
		filler1		: BYTE;
		dosage		: ARRAY[1..4] OF STRING[32];
		costCode	: ARRAY[1..4] OF STRING[6]);
	    1:  ( {1 + 1 + 1 + 75 + 75 + 39 = 192}
		  { Record trayNumber + 3 }
		id		: BYTE;
		trayNum		: BYTE;
		drugCnt		: BYTE;
		adnTbl		: AdnTblType;
		rtCnt		: RtCntType;
		filler		: ARRAY[1..39] OF BYTE);
	    2:  ( { 1 + 128 + 1 + 14 + 42 + 6 = 192 }
		  { Record 0 }
		id2		: BYTE;
		mdiMTbl		: MdiMapTbl;
		adnCnt		: BYTE;
		filler2		: ARRAY[1..20] OF BYTE;
		mapTbl		: MapTable);
	    3:  ( { 1 + 60 + 99 + 32 = 192}
		  { Record 3 }
		id3		: BYTE;
		tmStamp		: ARRAY[1..60] OF CHAR;
		drugOrder	: DOrderType;
		filler3		: ARRAY[1..32] OF BYTE);
	    4:  ( { 1 + (20 * 7 = 140) + 2 = 192 }
		  { Record 1 and 2 }
		id4		: BYTE;
		micText		: ARRAY[0..19] OF MicAryType;
		filler4		: ARRAY[1..51] OF BYTE);
		END;
	PMapBat=^TMapBat;
	TMapBat=OBJECT
		MapBatFile: TEXT;
		Error: BOOLEAN;
		BatIDList: ARRAY[1..MaxTray] OF PChar;
		CONSTRUCTOR INIT;
		DESTRUCTOR DONE;
		END;
	PPanelBitmap=^TPanelBitmap;
	TPanelBitmap=ARRAY[0..11] OF BYTE;
	PMICData=^TMICData;
	TMICData=ARRAY[1..MAXMicLen] of BYTE;
	PTraysObject=^TTraysObject;
	TTraysObject=OBJECT
		gMda		: MastDilArray;
		gTrayFile: FILE OF Trayrec;
		gTray		: TrayRec;
		gTrayMax	: INTEGER;
		gInitMap	: BOOLEAN;
		gInitAdnMap	: BOOLEAN;
		gInitAbr	: BOOLEAN;
		abrTbl		: AbrTable;
		lastAbr		: INTEGER;
		aMapCnt		: BYTE;
		aMapTbl		: AMapTable;
		adnMapCnt	: BYTE;
		adnMapTbl	: MapTable;
		MapBat: PMapBat;
		CONSTRUCTOR INIT;
		PROCEDURE TrayAbort(ErrNum: INTEGER);
		FUNCTION MapBatID(BatID: PChar): INTEGER;
		PROCEDURE LoadSTray(trayNum: INTEGER);
		PROCEDURE LoadTray(trayNum: INTEGER);
		FUNCTION GetWell(step, rdn: INTEGER): INTEGER;
		FUNCTION GetDsi(trayNum, rdn, mdi: INTEGER; VAR nr: BOOLEAN): BYTE;
		FUNCTION GetMdi(trayNum, rdn, step: INTEGER;  nr: BOOLEAN): BYTE;
		PROCEDURE GetMda(VAR table: MastDilArray);
		PROCEDURE GetMicText(mic: BYTE; VAR strg: MicStr);
		FUNCTION NRMic (x: BYTE): BOOLEAN;
		FUNCTION MicGT(first, second: BYTE): BOOLEAN;
		FUNCTION MicLT(first, second: BYTE): BOOLEAN;
		FUNCTION MicG  (x, y: BYTE): BOOLEAN;
		FUNCTION MicL  (x, y: BYTE): BOOLEAN;
		FUNCTION MicGE (x, y: BYTE): BOOLEAN;
		FUNCTION MicLE (x, y: BYTE): BOOLEAN;
		FUNCTION MicEQ (x, y: BYTE): BOOLEAN;
		FUNCTION MicNE (x, y: BYTE): BOOLEAN;
		FUNCTION LMic  (x: BYTE): BOOLEAN;
		FUNCTION GMic  (x: BYTE): BOOLEAN;
		PROCEDURE TrayName(trayNum: INTEGER; VAR tName: STRING);
		FUNCTION GetTGrp(trayNum: INTEGER): BYTE;
		PROCEDURE GetXtra(trayNum: INTEGER;  VAR xtraTbl: XtraType;  VAR XtraCnt: INTEGER);
		FUNCTION GetProbNum(trayNum: INTEGER): BYTE;
		FUNCTION TrayObsolete(trayNum: INTEGER): BOOLEAN;
		FUNCTION TrayHNID(trayNum: INTEGER): BOOLEAN;
		FUNCTION TrayBkpt(trayNum: INTEGER): BOOLEAN;
		FUNCTION TrayFlo(trayNum: INTEGER): BOOLEAN;
		FUNCTION GetIdRec(trayNum: INTEGER): INTEGER;
		FUNCTION GetTSup(trayNum: INTEGER): BYTE;
		PROCEDURE InitAbbrTable;
		PROCEDURE AdnToAbbrConvert(adn: INTEGER; VAR abr: STRING);
		FUNCTION AbbrToAdnConvert(VAR abr: STRING): INTEGER;
		PROCEDURE lpLoadAdnMap;
		PROCEDURE GetDils(trayNum, rdn: INTEGER;  VAR drgAbr: STRING;
		                  VAR drugDils: DrugDilType;  VAR dilCnt: INTEGER);
		PROCEDURE MapDrgAbbr(iAbr: STRING; VAR oAbr: STRING);
		FUNCTION MapAdn(adn: INTEGER): INTEGER;
		FUNCTION AdnMT(VAR lMapTbl: MapTable): INTEGER;
		PROCEDURE MicUnpack(trayNum: INTEGER;  VAR micLvls: mics;
		                    VAR micList: IsoDilType;  VAR micCnt: INTEGER);
		PROCEDURE TFSpec(trayNum: INTEGER;  VAR tName: STRING;  VAR tSet: INTEGER;
		                 VAR pSet: INTEGER;  VAR idRec: INTEGER);
		FUNCTION DrugCnt(trayNum: INTEGER): INTEGER;
	 Procedure GetDrugRDN(batID, testID: PChar; Var trayNum, RDN: Integer);
	 FUNCTION GetXtraWellNumber(VAR f : Text; testProc, testID : PChar; VAR wellNumber : INTEGER) : BOOLEAN;
		DESTRUCTOR DONE;
		END;

IMPLEMENTATION

CONSTRUCTOR TMapBat.INIT;
{-----------------------------------------------------------------------}
{-----------------------------------------------------------------------}
VAR BatID: STRING;  BatIndex: INTEGER; delimiter: CHAR;
BEGIN
Error := TRUE;
FOR BatIndex := 1 TO MaxTray DO BatIDList[BatIndex] := NIL;
Assign(MapBatFile, 'MAPBATID.TXT');
RESET(MapBatFile);
IF IORESULT <> 0 THEN EXIT;
WHILE NOT EOF(MapBatFile) DO
	BEGIN
	READLN(MapBatFIle, BatIndex, delimiter, BatID);
	IF IORESULT <> 0 THEN EXIT;
	IF (BatIndex < 1) OR (BatIndex > MaxTray) THEN EXIT;
	GETMEM(BatIDList[BatIndex], LENGTH(BatID)+1);
	StrPCopy(BatIDList[BatIndex], BatID);
	END;
Close(MapBatFile);
IF IORESULT <> 0 THEN EXIT;
Error := FALSE;
END;

DESTRUCTOR TMapBat.DONE;
{-----------------------------------------------------------------------}
{-----------------------------------------------------------------------}
VAR BatID: STRING;  BatIndex: INTEGER; delimiter: CHAR;
BEGIN
RESET(MapBatFile);
IF IORESULT <> 0 THEN EXIT;
WHILE NOT EOF(MapBatFile) DO
	BEGIN
	READLN(MapBatFIle, BatIndex, delimiter, BatID);
	IF IORESULT <> 0 THEN EXIT;
	IF (BatIndex < 1) OR (BatIndex > MaxTray) THEN EXIT;
	FreeMEM(BatIDList[BatIndex], LENGTH(BatID)+1);
	END;
Close(MapBatFile);
END;

CONSTRUCTOR TTraysObject.INIT;
{-----------------------------------------------------------------------}
{ INIT opens the tray record file and initializes variables for		}
{ the running of TrayLib routines.  This must be loaded in your root.	}
{									}
{ input  :	<none>							}
{ output :     <none>							}
{ The function returns the number of tray records or a	-1 on error.	}
{-----------------------------------------------------------------------}
VAR
	i,
	j,
	k	: INTEGER;
	filName	: ARRAY [0..30] OF CHAR;
BEGIN
Assign  (gTrayFile, 'TRAYREC.DAT');
IF IOResult <> 0 THEN TrayAbort(10);
Reset(gTrayFile);
IF IOResult <> 0 THEN TrayAbort(11);
gTrayMax := MaxTray;
gTray.number := 0;
LoadSTray(MaxTray+1);
gTrayMax := gTray.trayCnt;
gMda := gTray.mda;
gInitMap := FALSE;
gInitAdnMap := FALSE;
gInitAbr := FALSE;
MapBat := NEW(PMapBat, INIT);
IF MapBat^.Error THEN TrayAbort(12);
END;  { INIT }

DESTRUCTOR TTraysObject.DONE;
{-----------------------------------------------------------------------}
{-----------------------------------------------------------------------}
BEGIN
Close(gTrayFile);
DISPOSE(MapBat, DONE);
END;

PROCEDURE TTraysObject.TrayAbort(ErrNum: INTEGER);
{-----------------------------------------------------------------------}
{ purpose :	Error out when some condition has been met.		}
{ input   :	errnum	- The error message to be displayed.		}
{-----------------------------------------------------------------------}
BEGIN
SetError(ErrNum + err_TrayLib);		{ add error num to base constant }
HALT;
END;

Procedure TTraysObject.GetDrugRDN(batID, testID: PChar; Var trayNum, RDN: Integer);
{- given a battery id and a test ID return a tray number and a relative
	drug number. RDN of -1 will be returned if error }
Var
	tstr  : String[80];
	abbr  : String[20];
	k     : Integer;
	dils  : DrugDilType;
	dilCnt: Integer;
	found : Boolean;
Begin
	RDN:= -1;
	trayNum:= MapBatID(batID);
	tstr:= Copy(StrPas(testID), 1, 3);
	k:= 1;
   found:= False;
   While (k <= DrugCnt(trayNum)) and not found Do Begin
      GetDils(trayNum, k, abbr, dils, dilCnt);
      found:= (abbr = tstr);
      If not found Then
         Inc(k);
   End;
   If found Then
      RDN:= k;
End;

FUNCTION TTraysObject.MapBatID(BatID: PChar): INTEGER;
{-----------------------------------------------------------------------}
{-----------------------------------------------------------------------}
VAR i: INTEGER;
BEGIN
IF NOT MapBat^.Error THEN
  BEGIN
  FOR i := 1 TO MaxTray DO
	BEGIN
	IF MapBat^.BatIDList[i] <> NIL THEN
	  IF StrIComp(MapBat^.BatIDList[i], BatID) = 0
	  THEN	BEGIN
		MapBatID := i;
		EXIT;
		END;
	END;
  END;
TrayAbort(13);
END;

PROCEDURE TTraysObject.LoadSTray(trayNum: INTEGER);
{-----------------------------------------------------------------------}
{ purpose :	Load into the global tray record variable, the record	}
{		  whose drn corrisponds with trayNum			}
{ input   :	trayNum	- The tray id number and record number - 1	}
{-----------------------------------------------------------------------}
VAR recnum: INTEGER;
BEGIN
  IF (TrayNum < 1) OR (TrayNum > (MaxTray + 3)) THEN TrayAbort(2);
  IF (TrayNum > MaxTray)
    THEN recNum := TrayNum - (MaxTray + 1)
    ELSE recNum := TrayNum + 2;
  Seek(gTrayFile, recnum);
  IF IOResult <> 0 THEN TrayAbort(3);
  Read(gTrayFile, gTray);
  IF IOResult <> 0 THEN TrayAbort(4);
  IF ((trayNum <> -1) AND (trayNum <> gTray.number))
    THEN TrayAbort(5);
END;

PROCEDURE TTraysObject.LoadTray(trayNum: INTEGER);
{-----------------------------------------------------------------------}
{ purpose :	Load into the global tray record variable, the record	}
{		  whose drn corrisponds with trayNum			}
{ input   :	trayNum	- The tray id number and record number - 1	}
{-----------------------------------------------------------------------}
BEGIN
  IF (trayNum < 1) OR (trayNum > gTrayMax) THEN TrayAbort(1);
  LoadSTray(trayNum);
END;

FUNCTION TTraysObject.GetWell(step, rdn: INTEGER): INTEGER;
{-----------------------------------------------------------------------}
{ Purpose :	Find the well number for a given drug and step value	}
{ Input   :	step	- The dilution step for a given drug		}
{		rdn	- The drug number for a drug on a given tray	}
{ Output  :	GetWell	- The well number for that drug/step combo	}
{-----------------------------------------------------------------------}
VAR
	accum,
	i	: INTEGER;
BEGIN
  accum := 0;
  rdn := rdn - 1;
  FOR i := 1 TO rdn DO accum := accum + gTray.drug[i].dilCnt + 2;
  GetWell := accum + step + 1;
END;

{.}  FUNCTION TTraysObject.GetDsi(
{.}		trayNum,
{.}		rdn,
{.}		mdi	: INTEGER;
{.}	    VAR nr	: BOOLEAN
{.}		)	: BYTE;
{.----------------------------------------------------------------------}
{. GetDsi returns the dilution step number for the dilution referenced	}
{. by the traynum, drugnum, and master dilution index.			}
{.									}
{. input  :	trayNum	-  The number of the tray.			}
{.		rdn	-  The relative drug number for this tray.	}
{.		mdi	-  The dilution's index in the mda.		}
{. output :	nr	-  True to suppress reporting for the MIC.	}
{.		GetDsi	-  Function returns the dilution step.		}
{.----------------------------------------------------------------------}
VAR
	i,
	j,
	lastStep	: INTEGER;
	lMdi		: BYTE;
	lMdi2		: BYTE;
BEGIN
  GetDsi := 0;
  IF trayNum <> gTray.number THEN LoadTray(trayNum);
  IF (rdn < 1) OR (rdn > gTray.drugCnt) THEN EXIT;
  lMdi := mdi AND $1F;
  lMdi2 := mdi AND $7F;
  lastStep := gTray.drug[rdn].dilCnt;
  i := 0;
  j := GetWell(0, rdn);
  IF lMdi <> 0 THEN
    WHILE (lMdi2 <> gTray.mic[j]) AND (i <= lastStep) DO
    BEGIN
      j := j + 1;
      i := i + 1;
    END;
  nr := (mdi AND $80) <> 0;
  IF (mdi AND $20) <> 0
    THEN GetDsi := i + 1
    ELSE GetDsi := i;
END; { getdsi }

{.}  FUNCTION TTraysObject.GetMdi(
{.}		trayNum,
{.}		rdn,
{.}		step	: INTEGER;
{.}		nr	: BOOLEAN
{.}		)	: BYTE;
{.----------------------------------------------------------------------}
{. GetMdi returns the Master Dilution Array index for the dilution	}
{. referenced by the trayNum, rdn, and step input.			}
{.                                                                      }
{. input  :	trayNum	-  The number of the tray.			}
{.		rdn	-  The relative drug number for this tray.	}
{.		step	-  The number of the dilution in the drug's	}
{.			   dilution sequence.				}
{.		nr	-  True suppresses reporting of the MIC.	}
{. output :	<none>  -						}
{. The function returns the dilution's mda index or a 96 on error.	}
{.----------------------------------------------------------------------}
VAR
	i,
	result	: BYTE;
BEGIN
  result := $61;			{ set to return error }
  IF gTray.number <> trayNum THEN LoadTray(trayNum);
  IF (rdn >= 1) AND (rdn < (gTray.drugCnt + 2)) AND (step >= 0)
    AND (step < gTray.drug[rdn].dilCnt + 2) THEN
  BEGIN
    i := GetWell(step, rdn);
    result := gTray.mic[i];
  END;
  IF nr THEN result := result OR $80;		{ set N/R bit }
  GetMdi := result;
END; { getmdi }

{.}  PROCEDURE TTraysObject.GetMda(VAR table: MastDilArray);
{.----------------------------------------------------------------------}
{. GetMda loads the mda table into some other local copy.		}
{.									}
{. output :	table	-  Copy of the mda table.			}
{.----------------------------------------------------------------------}
BEGIN
  table := gMda;
END;

FUNCTION class(x, y: BYTE): INTEGER;
{-----------------------------------------------------------------------}
{									}
{-----------------------------------------------------------------------}
VAR
	relation: INTEGER;
	iX,
	iY	: BYTE;
BEGIN
  iX := x AND $1F;
  iY := y AND $1F;
  IF iX = iY THEN relation := 1
    ELSE IF iX > iY THEN relation := 2
      ELSE relation := 3;
  class := ((x SHR 5) * 16 + (y SHR 5)) * 16 + relation;
END;

{.}  FUNCTION TTraysObject.MicGT(first, second: BYTE): BOOLEAN;
{.----------------------------------------------------------------------}
{. MicGt returns true if the 1st mic has a greater value than the 2nd.	}
{.									}
{. input  :	first	-  Mic value one.				}
{.		second	-  Mic value two.				}
{. output :	MicGt	-  Function returns true if the first mic value	}
{.			   is greater than the second mic value.	}
{.----------------------------------------------------------------------}
VAR	code	: INTEGER;
BEGIN
  code := class(first, second);
  MicGT :=
	(code = $002) OR	{  x,  y, x>y }
	(code = $102) OR	{ >x,  y, x>y }
	(code = $101) OR	{ >x,  y, x=y }
	(code = $022) OR	{  x, <y, x>y }
	(code = $122) OR	{ >x, <y, x>y }
	(code = $121);		{ >x, <y, x=y }
END;

{.}  FUNCTION TTraysObject.MicLT(first, second: BYTE): BOOLEAN;
{.----------------------------------------------------------------------}
{. MicLt returns true if the 1st mic has a smaller value than the 2nd.	}
{.									}
{. input  :	first	-  Mic value one.				}
{.		second	-  Mic value two.				}
{. output :	MicGt	-  Function returns true if the first mic value	}
{.			   is smaller than the second mic value.	}
{.----------------------------------------------------------------------}
VAR	code	: INTEGER;
BEGIN
  code := class(first, second);
  MicLT :=
	(code = $003) OR	{  x,  y, x<y }
	(code = $203) OR	{ <x,  y, x<y }
	(code = $013) OR	{  x, >y, x<y }
	(code = $011) OR	{  x, >y, x=y }
	(code = $213) OR	{ <x, >y, x<y }
	(code = $211);		{ <x, >y, x=y }
END;

PROCEDURE TTraysObject.GetMicText(mic: BYTE; VAR strg: MicStr);
{-----------------------------------------------------------------------}
{ GetMicText builds mic string depending upon mdi and flags.		}
{ Input   :	mic	- The mdi and flags of some dilution step	}
{ Output  :	strg	- The printable string for the given mic	}
{-----------------------------------------------------------------------}
BEGIN
  IF ((mic AND $80) <> 0) THEN strg := gMda[0] ELSE
  BEGIN
    strg := gMda[(mic AND $1F)];
    IF ((mic AND $20) <> 0) THEN strg[1] := '>'
      ELSE IF ((mic AND $40) <> 0) THEN strg[1] := '<';
  END;
END;

{.}  FUNCTION TTraysObject.MicG  (x, y: BYTE): BOOLEAN;
{.----------------------------------------------------------------------}
{. MicG compares two mic's and returns a true if the first mdi is 	}
{. greater than the second.						}
{. Input:	x	-  The first mic				}
{.		y	-  The second mic				}
{. Output:	<none>							}
{. The function returns a true if x is greater than y			}
{.----------------------------------------------------------------------}
BEGIN
  MicG  := (x AND $1F) > (y AND $1F);
END;

{.}  FUNCTION TTraysObject.MicL  (x, y: BYTE): BOOLEAN;
{.----------------------------------------------------------------------}
{. MicL compares two mic's and returns a true if the first mdi is 	}
{. less than the second.						}
{. Input:	x	-  The first mic				}
{.		y	-  The second mic				}
{. Output:	<none>							}
{. The function returns a true if x is less than y			}
{.----------------------------------------------------------------------}
BEGIN
  MicL  := (x AND $1F) < (y AND $1F);
END;

{.}  FUNCTION TTraysObject.MicGE (x, y: BYTE): BOOLEAN;
{.----------------------------------------------------------------------}
{. MicGE compares two mic's and returns a true if the first mdi is 	}
{. greater than or equal to the second.					}
{. Input:	x	-  The first mic				}
{.		y	-  The second mic				}
{. Output:	<none>							}
{. The function returns a true if x is equal to or greater than y	}
{.----------------------------------------------------------------------}
BEGIN
  MicGE := (x AND $1F) >= (y AND $1F);
END;

{.}  FUNCTION TTraysObject.MicLE (x, y: BYTE): BOOLEAN;
{.----------------------------------------------------------------------}
{. MicLE compares two mic's and returns a true if the first mdi is 	}
{. less than or equal to the second.					}
{. Input:	x	-  The first mic				}
{.		y	-  The second mic				}
{. Output:	<none>							}
{. The function returns a true if x is less than or equal to y		}
{.----------------------------------------------------------------------}
BEGIN
  MicLE := (x AND $1F) <= (y AND $1F);
END;

{.}  FUNCTION TTraysObject.MicEQ (x, y: BYTE): BOOLEAN;
{.----------------------------------------------------------------------}
{. MicEQ compares two mic's and returns a true if the first mdi is 	}
{. equal to the second.							}
{. Input:	x	-  The first mic				}
{.		y	-  The second mic				}
{. Output:	<none>							}
{. The function returns a true if x is equal to y			}
{.----------------------------------------------------------------------}
BEGIN
  MicEQ := (x AND $1F) = (y AND $1F);
END;

{.}  FUNCTION TTraysObject.MicNE (x, y: BYTE): BOOLEAN;
{.----------------------------------------------------------------------}
{. MicNE compares two mic's and returns a true if the first mdi is 	}
{. not equal to the second.						}
{. Input:	x	-  The first mic				}
{.		y	-  The second mic				}
{. Output:	<none>							}
{. The function returns a true if x is greater than y			}
{.----------------------------------------------------------------------}
BEGIN
  MicNE := (x AND $1F) <> (y AND $1F);
END;

{.}  FUNCTION TTraysObject.NRMic (x: BYTE): BOOLEAN;
{.----------------------------------------------------------------------}
{. NRMic test if the NR bit is set in the passed in mic			}
{. Input:	x	-  The mic to be tested				}
{. Output:	<none>							}
{. The function returns a true if the nr bit is set in x		}
{.----------------------------------------------------------------------}
BEGIN
  NRMic := (x AND $80) <> 0;
END;

{.}  FUNCTION TTraysObject.LMic  (x: BYTE): BOOLEAN;
{.----------------------------------------------------------------------}
{. LMic test if the less than bit is set in the passed in mic		}
{. Input:	x	-  The mic to be tested				}
{. Output:	<none>							}
{. The function returns a true if the less than bit is set in x		}
{.----------------------------------------------------------------------}
BEGIN
  LMic  := (x AND $40) <> 0;
END;

{.}  FUNCTION TTraysObject.GMic  (x: BYTE): BOOLEAN;
{.----------------------------------------------------------------------}
{. GMic test if the greater than bit is set in the passed in mic	}
{. Input:	x	-  The mic to be tested				}
{. Output:	<none>							}
{. The function returns a true if the greater than bit is set in x	}
{.----------------------------------------------------------------------}
BEGIN
  gMic  := (x AND $20) <> 0;
END;

{.}  PROCEDURE TTraysObject.TrayName(trayNum: INTEGER; VAR tName: STRING);
{.----------------------------------------------------------------------}
{. TrayName returns the name of this tray.                              }
{.									}
{. input  :	trayNum	-  The number of the tray.			}
{. output :	tName	-  The name of the tray (16 character string).	}
{.----------------------------------------------------------------------}
BEGIN
  IF trayNum <> gTray.number THEN LoadTray(trayNum);
  tName := gTray.name;
END;

{.}  FUNCTION TTraysObject.GetTGrp(trayNum: INTEGER): BYTE;
{.----------------------------------------------------------------------}
{. GetTGrp returns the tray set group number for a tray.		}
{.									}
{. input  :	trayNum	-  The number of the tray.			}
{. output :	<none> 	-						}
{. The function returns the tray set group number for this tray:	}
{. 1 = Neg, 2 = Pos, 3 = Ana, 4 = HNID, 6 = yeast, 8 = none.		}
{.----------------------------------------------------------------------}
BEGIN
   IF trayNum <> gTray.number THEN LoadTray(trayNum);
   GetTGrp := gTray.group;
END;

{.}  PROCEDURE TTraysObject.GetXtra(
{.}		trayNum	: INTEGER;
{.}	    VAR xtraTbl	: XtraType;
{.}	    VAR XtraCnt	: INTEGER);
{.----------------------------------------------------------------------}
{. GetXtra returns a list of valid extra test for a tray.		}
{.									}
{. input  :	trayNum	-  The number of the tray.			}
{. output :	XtraTbl	-  A list of valid extra tests.			}
{.              XtraCnt -  The number of extra tests for this tray.	}
{.	4 = StS,  5 = Oxi,  7 = Bl,  8 = GmS,  9 = TFG.			}
{.----------------------------------------------------------------------}
BEGIN
   IF trayNum <> gTray.number THEN LoadTray(trayNum);
   XtraCnt := gTray.XtraCnt;
   XtraTbl := gTray.XtraTbl;
END;

{.}  FUNCTION TTraysObject.GetProbNum(trayNum: INTEGER): BYTE;
{.----------------------------------------------------------------------}
{. GetProbNum returns the probability number for this tray.             }
{.									}
{. input  :	trayNum	-  The number of the tray.			}
{. output :	<none>	-						}
{. The function returns the probability number for this tray.		}
{. 1 = Neg,  2 = Pos Frozen,  3 = Ana,  4 = HNID,  5 = Pos Dried,	}
{. 6 = Yeast,  8 = none,  20 = Rapid Neg,  21 = Rapid Pos.		}
{.----------------------------------------------------------------------}
BEGIN
  IF trayNum <> gTray.number THEN LoadTray(trayNum);
  GetProbNum := gTray.probNum;
END;

{.}  FUNCTION TTraysObject.TrayObsolete(trayNum: INTEGER): BOOLEAN;
{.----------------------------------------------------------------------}
{. TrayObsolete returns the state of the obsolete flag for this tray.   }
{.									}
{. input  :	trayNum	-  The number of the tray.			}
{. output :	<none>	-						}
{. Function returns true if the obso bit for this tray has been set.	}
{.----------------------------------------------------------------------}
BEGIN
  IF trayNum <> gTray.number THEN LoadTray(trayNum);
  TrayObsolete := (gTray.flag AND $01) <> 0;
END;

(*
{.}  FUNCTION TTraysObject.TrayXF4(trayNum: INTEGER): BOOLEAN;
{.----------------------------------------------------------------------}
{. TrayXF4 returns the state of the ** custom flag ** for this tray.	}
{.									}
{. input  :	trayNum	-  The number of the tray.			}
{. output :	<none>	-						}
{. Function returns true if the cust bit for this tray has been set.	}
{.----------------------------------------------------------------------}
BEGIN
  IF trayNum <> gTray.number THEN LoadTray(trayNum);
  TrayXF4 := (gTray.flag AND $10) <> 0;
END;
*)

{.}  FUNCTION TTraysObject.TrayHNID(trayNum: INTEGER): BOOLEAN;
{.----------------------------------------------------------------------}
{. TrayHNID returns the state of the HNID flag for this tray.		}
{.									}
{. input  :	trayNum	-  The number of the tray.			}
{. output :	<none>	-						}
{. Function returns true if the HNID bit for this tray has been set.	}
{.----------------------------------------------------------------------}
BEGIN
  IF trayNum <> gTray.number THEN LoadTray(trayNum);
  TrayHNID := (gTray.flag AND $08) <> 0;
END;

{.}  FUNCTION TTraysObject.TrayBkpt(trayNum: INTEGER): BOOLEAN;
{.----------------------------------------------------------------------}
{. TrayBkpt returns the state of the Break point flag for this tray.	}
{.									}
{. input  :	trayNum	-  The number of the tray.			}
{. output :	<none>	-						}
{. Function returns true if the Bkpt bit for this tray has been set.	}
{.----------------------------------------------------------------------}
BEGIN
  IF trayNum <> gTray.number THEN LoadTray(trayNum);
  TrayBkpt := (gTray.flag AND $02) <> 0;
END;

{.}  FUNCTION TTraysObject.TrayFlo(trayNum: INTEGER): BOOLEAN;
{.----------------------------------------------------------------------}
{. TrayFlo returns the state of the fluorogenic flag for this tray.	}
{.									}
{. input  :	trayNum	-  The number of the tray.			}
{. output :	<none>	-						}
{. Function returns true if the Flo bit for this tray has been set.	}
{.----------------------------------------------------------------------}
BEGIN
  IF trayNum <> gTray.number THEN LoadTray(trayNum);
  TrayFlo := (gTray.flag AND $04) <> 0;
END;

{.}  FUNCTION TTraysObject.GetIdRec(trayNum: INTEGER): INTEGER;
{.----------------------------------------------------------------------}
{. GetIdRec returns the Bdesc file record number for this tray.         }
{.									}
{. input  :	trayNum	-  The number of the tray.			}
{. output :	<none>	-						}
{. The function returns the BDesc record number	for this tray.		}
{.----------------------------------------------------------------------}
BEGIN
  IF trayNum <> gTray.number THEN LoadTray(trayNum);
  GetIdRec := gTray.idRec;
END;

{.}  FUNCTION TTraysObject.GetTSup(trayNum: INTEGER): BYTE;
{.----------------------------------------------------------------------}
{. GetTSup returns the therapy suppression for a tray.			}
{.									}
{. input  :	trayNum	-  The number of the tray.			}
{. output :	<none>	-						}
{. The function returns the therapy suppression byte for this tray.	}
{. 0 = No predefined source suppression, 2 = Urine source suppression	}
{. 8 = Blood suppression based on source level.				}
{.----------------------------------------------------------------------}
BEGIN
   IF trayNum <> gTray.number THEN LoadTray(trayNum);
   GetTSup := gTray.sup;
END;

(*
{.}  FUNCTION TTraysObject.TrayXF5(trayNum: INTEGER): BOOLEAN;
{.----------------------------------------------------------------------}
{. TrayXF5 returns the state of the phony flag for this tray.		}
{.									}
{. input  :	trayNum	-  The number of the tray.			}
{. output :	<none>	-						}
{. Function returns true if the XF5 bit for this tray has been set.	}
{.----------------------------------------------------------------------}
BEGIN
  IF trayNum <> gTray.number THEN LoadTray(trayNum);
  TrayXF5 := (gTray.flag AND $20) <> 0;
END;

{.}  FUNCTION TTraysObject.TrayXF6(trayNum: INTEGER): BOOLEAN;
{.----------------------------------------------------------------------}
{. TrayXF6 returns the state of the XF6 flag for this tray (undefined).	}
{.									}
{. input  :	trayNum	-  The number of the tray.			}
{. output :	<none>	-						}
{. Function returns true if the XF6 bit for this tray has been set.	}
{.----------------------------------------------------------------------}
BEGIN
  IF trayNum <> gTray.number THEN LoadTray(trayNum);
  TrayXF6 := (gTray.flag AND $40) <> 0;
END;

{.}  FUNCTION TTraysObject.TrayXF7(trayNum: INTEGER): BOOLEAN;
{.----------------------------------------------------------------------}
{. TrayXF7 returns the state of the XF7 flag for this tray (undefined).	}
{.									}
{. input  :	trayNum	-  The number of the tray.			}
{. output :	<none>	-						}
{. Function returns true if the XF7 bit for this tray has been set.	}
{.----------------------------------------------------------------------}
BEGIN
  IF trayNum <> gTray.number THEN LoadTray(trayNum);
  TrayXF7 := (gTray.flag AND $80) <> 0;
END;
*)

FUNCTION TTraysObject.GetXtraWellNumber(VAR f : Text; testProc, testID : PChar; VAR wellNumber : INTEGER) : BOOLEAN;
{.----------------------------------------------------------------------}
{. Returns the zero-based well number of the given extra test in        }
{. 'wellNumber'.  Returns FALSE in the test id is not found.            }
{.----------------------------------------------------------------------}
VAR panNum, i : INTEGER;
	 okFine : BOOLEAN;
	 testIDPos : POINTER;
	 readStr : STRING[128];
	 temp : ARRAY[0..127] OF Char;
BEGIN
  GetXtraWellNumber := FALSE;
  FillChar(temp[0],128,CHR(0));
  { get rid of the leading '.' }
  MOVE(testID[1],temp[0],StrLen(testID)-1);
  StrCopy(testID,temp);

  wellNumber := 0;
  Reset(f);
  panNum := MapBatID(testProc);
  okFine := TRUE;
  i := 1;
  { find the panel in the file }
  WHILE (i <= (9*PanNum)) AND (OKFine) DO
	 BEGIN
		ReadLn(f,readStr);
		IF IOResult <> 0 THEN okFine := FALSE;
		i := i + 1;
	 END;
  { pass by the panel name line }
  IF okFine THEN
	 BEGIN
		ReadLn(f,readStr);
		IF IOResult <> 0 THEN okFine := FALSE;
	 END;
  { Find the extra test id in the next 8 lines of the file }
  IF okFine THEN
	 BEGIN
		i := 0;
		{ find the right row }
		WHILE (i <= 7) AND (okFine) DO
		  BEGIN
			 ReadLn(f,readStr);
			 IF IOResult <> 0 THEN okFine := FALSE;
			 IF okFine THEN
				BEGIN
				  StrPCopy(temp,readStr);
				  testIDPos := StrPos(temp,testID);
				  IF testIDPos <> NIL THEN
					 BEGIN
						wellNumber := wellNumber + (((OFS(testIDPos^))-(OFS(temp[0]))) DIV 9 );
						GetXtraWellNumber := TRUE;
						EXIT;
					 END;
				END;
			 i := i + 1;
			 wellNumber := wellNumber + 12;
		  END;
	 END;
END;

FUNCTION TTraysObject.DrugCnt(trayNum: INTEGER): INTEGER;
{.----------------------------------------------------------------------}
{. DrugCnt returns the number of drugs for the given panel.		}
{.									}
{. input  :	trayNum	-  The number of the tray.			}
{. output :	<none>	-						}
{. Function returns the number of drugs on this tray or -1 on error.	}
{.----------------------------------------------------------------------}
BEGIN
  DrugCnt := 0;
  IF gTray.number <> trayNum THEN LoadTray(trayNum);
  DrugCnt := gTray.drugCnt;
END;

(*
{.}  PROCEDURE TTraysObject.MapTray(VAR mTbl: PanNumTbl);
{. MapTray maps from external to internal tray number.			}
{.									}
{. input  :	<none>	-						}
{. output :	mTab	-  Table of tray mappings.			}
{.----------------------------------------------------------------------}
VAR
	miscBuf	: STRING[30];
	i	: INTEGER;
BEGIN
  GetStrP(strBase+45, miscBuf);
  GetMiscStr(45, miscBuf);
  FOR i := 1 TO (SIZEOF(PanNumTbl)) DO
    mTbl[i] := ORD(miscBuf[i]) - ORD(' ');
END;


{.}  PROCEDURE TTraysObject.LdMapTray(VAR mTbl: PanNumTbl);
{.----------------------------------------------------------------------}
{. LdMapTray loads a panel number table into the misc.fil.		}
{.									}
{. input :	mTab	-  Table of tray mappings.			}
{.----------------------------------------------------------------------}
VAR
	miscBuf	: STRING[30];
	i	 : INTEGER;
BEGIN
  FillCh(ADDR(miscBuf), SIZEOF(miscBuf), ' ');
  miscBuf[0] := CHR(30);
  FOR i := 1 TO (SIZEOF(PanNumTbl)) DO
    miscBuf[i] := CHR(mTbl[i] + ORD(' '));
  PutMiscStr(45, miscBuf);
END;
*)

PROCEDURE TTraysObject.InitAbbrTable;
{-----------------------------------------------------------------------}
{  InitAbbrTable loads the abrTable from TrayRec.dat.			}
{-----------------------------------------------------------------------}
VAR	x	: INTEGER;
BEGIN
  x := gTray.number;
  LoadSTray((MaxTray + 3));
  lastAbr := gTray.cDrgCnt;
  abrTbl := gTray.abrTbl;
  LoadSTray(x);
  gInitAbr := TRUE;
END;

{.}  PROCEDURE TTraysObject.AdnToAbbrConvert(adn: INTEGER; VAR abr: STRING);
{.----------------------------------------------------------------------}
{. AdnToAbbrConvert loads the abr string from the abrTable.		}
{.    **** This procedure must be called in the root program.  ****	}
{.									}
{. input  :	adn	-  The absolute drug number of this drug.	}
{. output :	abr	-  The drugs abbreviation string.		}
{.----------------------------------------------------------------------}
BEGIN
  IF NOT gInitAbr THEN InitAbbrTable;
  IF (adn > lastAbr) OR (adn < 1 )
    THEN abr := 'XXX'
    ELSE abr := abrTbl[adn];
END;

{.}  FUNCTION TTraysObject.AbbrToAdnConvert(VAR abr: STRING): INTEGER;
{.----------------------------------------------------------------------}
{. AbbrToAdnConvert searches the abrTable for the abr string.		}
{.    **** This procedure must be called in the root program.  ****	}
{.									}
{. input  :	abr	-  The drugs abbreviation string.		}
{. output :	<none>	-						}
{. The function returns the absolute drug number for this abr.		}
{.----------------------------------------------------------------------}
VAR	i	: INTEGER;
BEGIN
  AbbrToAdnConvert := 0;
  IF NOT gInitAbr THEN InitAbbrTable;
  FOR i := (LENGTH(abr) + 1) TO 3 DO abr[i] := ' ';
  abr[0] := CHR(3);
  i := 1;
  WHILE abrTbl[i] <> abr DO
  BEGIN
    i := i + 1;
    IF i > lastAbr THEN EXIT;
  END;
  AbbrToAdnConvert := i;
END;

PROCEDURE TTraysObject.lpLoadAdnMap;
{-----------------------------------------------------------------------}
{ 									}
{-----------------------------------------------------------------------}
BEGIN
  LoadSTray((MaxTray + 1));
  gInitAdnMap := TRUE;
  adnMapTbl := gTray.MapTbl;
  adnMapCnt := adnMapTbl[0].iDrgNum;
END;

{.}  PROCEDURE TTraysObject.GetDils(
{.}		trayNum,
{.}		rdn	: INTEGER;
{.}	    VAR drgAbr	: STRING;
{.}	    VAR drugDils: DrugDilType;
{.}	    VAR dilCnt	: INTEGER);
{.----------------------------------------------------------------------}
{. GetDils returns all of the dilution values for a drug, name of drug,	}
{. and the number of dilution values.					}
{.    **** This procedure must be called in the root program.  ****	}
{.									}
{. input  :	trayNum	- The number of the tray.			}
{.		rdn	- Ther relative drug number for this tray.	}
{. output :	drgAbr	- Abbr. of this drug in the system.		}
{.		drugDils- String representations of the dilution values	}
{.			  for each dil. step.  (Array of string[4]).	}
{.		dilCnt	- Number of dilutions.  Return 0 on error.	}
{.----------------------------------------------------------------------}
VAR
	i,
	x	: INTEGER;
BEGIN
  dilCnt := 0;
  IF gTray.number <> trayNum THEN LoadTray(trayNum);
  IF (rdn > gTray.drugCnt) THEN EXIT;
  dilCnt := gTray.drug[rdn].dilCnt + 1;
  AdnToAbbrConvert(gTray.drug[rdn].drugNum, drgAbr);
  x := GetWell(0, rdn);
  FOR i := 0 TO (dilCnt+1) DO
  BEGIN
    GetMicText(gTray.mic[x], drugDils[i]);
    x := x + 1;
  END;
END;

{.}  PROCEDURE TTraysObject.MapDrgAbbr(iAbr: STRING; VAR oAbr: STRING);
{.----------------------------------------------------------------------}
{. MapDrgAbbr searches the drug abbreviation map table for the passed	}
{. in abr and returns the corrisponding abr if one is found.		}
{.    **** This procedure must be called in the root program.  ****	}
{.									}
{. input  :	iAbr	-  The abbreviation to be mapped.		}
{. output :	oAbr	-  The resulted abbreviation.			}
{.----------------------------------------------------------------------}
VAR	i	: INTEGER;
BEGIN
  oAbr := iAbr;
  IF NOT gInitMap THEN
  BEGIN
    LoadSTray((MaxTray + 2));
    gInitMap := TRUE;
    aMapCnt := gTray.aMapCnt;
    aMapTbl := gTray.aMapTbl;
  END;
  FOR i := 1 TO aMapCnt DO
    IF aMapTbl[i].iDrgAbr = iAbr THEN
    BEGIN
      oAbr := aMapTbl[i].oDrgAbr;
      EXIT;
    END;
END;

{.}  FUNCTION TTraysObject.MapAdn(adn: INTEGER): INTEGER;
{.----------------------------------------------------------------------}
{. MapAdn searches the adn map table for the passed-in adn and returns	}
{. the corrisponding adn if one is found.				}
{.    **** This procedure must be called in the root program.  ****	}
{.									}
{. input  :	adn	-  The adn to be mapped.			}
{. output :	<none>	-						}
{. The function returns the adn that this adn maps to (or itself).	}
{.----------------------------------------------------------------------}
VAR	i	: INTEGER;
BEGIN
  MapAdn := adn;
  IF NOT gInitAdnMap THEN lpLoadAdnMap;
  FOR i := 1 TO adnMapCnt DO
    IF adnMapTbl[i].iDrgNum = adn THEN
    BEGIN
      MapAdn := adnMapTbl[i].oDrgNum;
      EXIT;
    END;
END;

{.}  FUNCTION TTraysObject.AdnMT(VAR lMapTbl: MapTable): INTEGER;
{.----------------------------------------------------------------------}
{. AdnMT returns a copy of the adn map table.				}
{.    **** This procedure must be called in the root program.  ****	}
{.									}
{. input  :	<none>	-						}
{. output :	lMapTbl - A copy of the adn map table.			}
{. The function returns number of table entries.			}
{.----------------------------------------------------------------------}
VAR	i	: INTEGER;
BEGIN
  AdnMT := 0;
  IF NOT gInitAdnMap THEN lpLoadAdnMap;
  lMapTbl := adnMapTbl;
  AdnMT := adnMapCnt;
END;

{.}  PROCEDURE TTraysObject.MicUnpack(
{.}		trayNum	: INTEGER;
{.}	    VAR micLvls	: mics;
{.}	    VAR micList	: IsoDilType;
{.}	    VAR micCnt	: INTEGER);
{.----------------------------------------------------------------------}
{. MicUnpack returns the dilution text strings for the given mic values.}
{.    **** This procedure must be called in the root program.  ****	}
{.									}
{. input  :	trayNum	-  The number of the tray.			}
{.		micLvls	-  An array of mic values from results type,	}
{.			   one mic value per drug on this tray.		}
{. output :	micList -  List of name/dilution string pairs.		}
{.		micCnt	-  Number of drugs on this tray.		}
{.----------------------------------------------------------------------}
VAR	i	: INTEGER;
BEGIN
  IF trayNum <> gTray.number THEN LoadTray(trayNum);
  micCnt := gTray.drugCnt;
  IF micCnt = 0 THEN EXIT;
  FOR i := 1 TO micCnt DO
  BEGIN
    AdnToAbbrConvert(gTray.drug[i].drugNum, micList[i].drug);
    GetMicText(micLvls[i], micList[i].dil);
  END;
END;

{.}  PROCEDURE TTraysObject.TFSpec(
{.}		trayNum	: INTEGER;
{.}	    VAR tName	: STRING;
{.}	    VAR tSet	: INTEGER;
{.}	    VAR pSet	: INTEGER;
{.}	    VAR idRec	: INTEGER);
{.----------------------------------------------------------------------}
{. TFSpec returns tray information for a given tray.			}
{.									}
{. input  :	trayNum	-  The number of the tray.			}
{. output :	tname	-  The name of the tray (e.g. 'Gram Pos Combo).	}
{.		tset	-  The set number for the tray (pos, neg, etc.).}
{.		pSet	-  Probability dataBase number for the tray.	}
{.		idRec	-  The BDesc record number for the tray.	}
{.----------------------------------------------------------------------}
BEGIN
  IF trayNum <> gTray.number THEN LoadTray(trayNum);
  pSet := gTray.probNum;
  tSet := gTray.group;
  idRec := gTray.idRec;
  tName := gTray.name;
END;

(*
{.}  PROCEDURE TTraysObject.GetCust(VAR custCnt, fstCust: INTEGER);
{.----------------------------------------------------------------------}
{. GetCust returns the number of custom panels and the number of the	}
{. first custom panel.  The custom panels are assumed to be contiguous.	}
{.									}
{. input  :	<none>	-						}
{. output :	custCnt	- The number of custom panels.			}
{.		fstCust	- The panel number of the first custom panel.	}
{.----------------------------------------------------------------------}
VAR	i	: INTEGER;
BEGIN
  LoadSTray((MaxTray + 1));
  custCnt := gTray.custCnt;
  fstCust := gTray.fstCust;
END;

{.}  PROCEDURE TTraysObject.GetTStmp(VAR timeStamp: STRING);
{.----------------------------------------------------------------------}
{. GetTStmp returns the timeStamp stored in TrayRec.dat.		}
{.									}
{. Input:	<none>	-						}
{. Output:	timeStamp - The time and date in a string.		}
{.----------------------------------------------------------------------}
BEGIN
  LoadSTray((MaxTray + 2));
  MOVE(ADDR(gTray.tmStamp), ADDR(timeStamp[1]), SIZEOF(gTray.tmStamp));
  timeStamp[0] := CHR(SIZEOF(gTray.tmStamp));
END;
*)

END.
