unit Gather;

{- this unit is used to gather data for a specimen.  The main object exported
   by this module is "TGatherSpec".  From this object, all data pertaining to
   the specimen can be obtained (gram stain results, patient info, isolate info
   and results).  All other object are used by the TGatherSpec and should not
   be used by themselves. That is unless you have a specific need and fully
   understand the objects, then you could for example, create a TGatherGS object
   by itself.

   There is one other gather object that may be useful by itself and that is
   TGatherRes.  This object gathers all results for an isolate and orders them
   in the user defined drug order.  This object may be useful when only results
   are needed for an isolate
}

INTERFACE

uses
  Objects,
  GSDef,
  ListLib,
  DBFile,
  ResLib,
  Trays,
  DBLib,
  MScan,
  WinProcs,
  WinTypes,
  DBTypes;

type
  TGatherFldInfo = record
    name      : PChar;
    value     : PChar;
  end;
  TGatherFldList = array[1..maxFldsPerRecord] of TGatherFldInfo;

  PGatherObj = ^TGatherObj;
  TGatherObj = object(TObject)
    flds            : TGatherFldList;   {- array of fields }
    numFlds         : integer;          {- number of fields defined }
    db              : PDBFile;          {- db file pointer }
    listObj         : PListObject;
    listRec         : PMnemListRec;
    listObjCreated  : boolean;
    listRecCreated  : boolean;
    dataOK          : boolean;
    doYield         : boolean;  {- if true, then yield to application while loading}
    {}
    constructor Init(aListObj: PListObject; aListRec: PMnemListRec; fName: PChar; allowYield: boolean);
    destructor Done; virtual;
    function DataLoaded: boolean;
    function NumFields: integer; virtual;
    function GetField(fldNum: integer; buf: PChar; maxLen: integer): boolean; virtual;
    function GetFieldName(fldNum: integer; buf: PChar; maxLen: integer): boolean; virtual;
    function FieldStrLen(fldNum: integer): integer;
    function FieldNameStrLen(fldNum: integer): integer;
    function LoadField(fldNum: integer): boolean; virtual;
    function LoadRecord(aSeq: TSeqNum): integer; virtual;
    procedure Clear; virtual;
    procedure CheckYield;
    private
    function OpenGatherFile(fName: PChar): PDBFile; virtual;
  end;

  {- special record for holding gram stain results }
  TGSRec = record
    abbr    : array[0..MaxGSAbbrLen] of char;
    name    : array[0..MaxGSNameLen] of char;
    res     : array[0..MaxGSFreeText] of char;
    resType : char;
    seq     : TSeqNum;  {- slot seq number }
  end;
  TGSResults = array[0..MaxGSSlots] of TGSRec;

  {- gather object for gram stain results. This object is acts slightly differently
     than the normal record gather object, in that it does not really deal with
     field numbers but rather slot numbers. See each individual method for a
     more specific description }
  PGatherGS = ^TGatherGS;
  TGatherGS = object(TGatherObj)
    slots           : TGSResults;
    numSlots        : integer;
    {}
    constructor Init(aListObj: PListObject; aListRec: PMnemListRec; allowYield: boolean);
    function GetField(fldNum: integer; buf: PChar; maxLen: integer): boolean; virtual;
    function GetFieldName(fldNum: integer; buf: PChar; maxLen: integer): boolean; virtual;
    function GetFieldAbbr(fldNum: integer; buf: PChar; maxLen: integer): boolean; virtual;
    function NumFields: integer; virtual;
    function LoadRecord(aSeq: TSeqNum): integer; virtual;
    procedure Clear; virtual;
  end;

  {- results gathering stuff }
  PGatherResObj = ^TGatherResObj;
  TGatherResObj = object(TObject)
    drugSort    : array[0..10] of char;   {- padded sort string }
    drugSortIdx : longint;
    drugSeq     : TSeqNum;
    drugAbbr    : array[0..35] of char;
    drugAlt     : array[0..35] of char;
    name        : PChar;
    resIDX      : integer;  {- index into TResults for ID result or MIC result}
    ncclsIDX    : integer;  {- index into TResults for NCCLS result }
    suppressed  : boolean;
    constructor Init(aName: PChar; aDrugSortIdx: longint; aDrugSeq: TSeqNum; isSuppressed: boolean);
    destructor Done; virtual;
  end;

  PGatherResCollection = ^TGatherResCollection;
  TGatherResCollection = object(TSortedCollection)
    function KeyOf(item: Pointer): Pointer; virtual;
    function Compare(Key1, Key2: Pointer): Integer; virtual;
  end;

  {- the TGatherRes object collects all results for an isolate. The results are
     separated into two parts (each with corresponding functions). There is
     ID type test info (for all non-drug related test) and MIC/Interp test info
     for all drug related tests.  The MIC/Interp stuff is grouped together, so
     if you ask for results for a particular MIC index you get both the MIC and
     the interpretation.  Also, the MIC/interp results are sorted in the user
     specified sort order. Suppression information is also available for MIC
     type tests }
  PGatherRes = ^TGatherRes;
  TGatherRes = object(TObject)
    results   : PResults;             {- a packed results object }
    idRes     : PGatherResCollection; {- collection of ID gathered results }
    micRes    : PGatherResCollection; {- collection of MIC gathered results }
    doYield   : boolean;

    {}
    constructor Init(aResults: PResults;
                     aListObj: PListObject;
                     OrgNum  : TSeqNum;
                     allowYield,
                     xForceMultiDrugs,
                     xCollectXtras: boolean);

    destructor Done; virtual;
    procedure CheckYield;

    {- ID only test information }
    function NumIDResults: integer;
    function GetIDTestName(idx: integer; pstr: PChar; maxLen: integer): boolean;
    function GetIDResult(idx: integer; pstr: PChar; maxLen: integer): boolean;
    function GetIDTestCat(idx: integer; pstr: PChar;  maxLen: integer): boolean;
    function GetIDResIDX(idx: integer; var resIDX: integer): boolean;

    {- Drug related test information }
    function NumMICResults: integer;
    function GetMICDrugName(idx: integer; pstr: PChar; maxLen: integer): boolean;
    function GetMICDrugAbbr(idx: integer; pstr: PChar; maxLen: integer): boolean;
    function GetMICDrugAlt(idx: integer; pstr: PChar; maxLen: integer): boolean;
    function GetMICDrugSeq(idx: integer): TSeqNum;
    function GetMICDrugSortIdx(idx: integer): longint;
    function GetMICResult(idx: integer; mic, nccls: PChar; maxLen: integer): boolean;
    function GetMICResultMapped(idx: integer; mic, nccls: PChar; maxLen: integer): boolean;
    function GetMICResIDX(idx: integer; var MICidx, NCCLSidx: integer): boolean;
    function GetMICSuppressed(idx: integer): boolean;
    function GetContraIndicated(idx: integer): boolean;

    {- miscellaneous stuff }
    function FindDrugIndex(aDrugSeq: TSeqNum): integer;
    function FindDrug(aDrugSeq: TSeqNum): PGatherResObj;

    private
      ForceMultiDrugs: boolean;
      CollectXtras   : boolean;

      function ValidIDX(acoll: PGatherResCollection; idx: integer): PGatherResObj;
  end;

  {- this object is used to hold data for an isolate record }
  PIsoData = ^TIsoData;
  TIsoData = object(TObject)
    fldData : array[1..MaxFldsPerRecord] of PChar;
    numFlds : integer;
    results : PResults;
    resList : PGatherRes;
    {}
    constructor Init(var flds: TGatherFldList; aDBR: PDBRec; aListObj: PListObject;
                     doLoadResults, allowYield, xForceMultiDrugs, xCollectXtras: boolean);
    destructor Done; virtual;

  private
    ForceMultiDrugs: boolean;
    CollectXtras   : boolean;
  end;

  {- gather object for isolates. This object is acts slightly differently
     than the normal record gather object, in that it deals with 1 or more isolate
     records, depending on how many isolates exist on a specimen }
  PGatherIso = ^TGatherIso;
  TGatherIso = object(TGatherObj)
    constructor Init(aListObj: PListObject; aListRec: PMnemListRec;
                     doLoadResults, allowYield,
                     xForceMultiDrugs, xCollectXtras: boolean);
    destructor Done; virtual;
    function GetField(fldNum: integer; buf: PChar; maxLen: integer): boolean; virtual;
    function LoadRecord(aSeq: TSeqNum): integer; virtual;
    procedure Clear; virtual;

    {- additional isolate related functions }
    function GetNumIsolates: integer;
    function SetIsoIndex(idx: integer): boolean;
    function GetIsoIndex: integer;
    function GetResults: PGatherRes;
    private
    curIdx      : integer;
    isos        : PCollection;  {- collection of isolate record data }
    loadResults : boolean;

    ForceMultiDrugs: boolean;
    CollectXtras   : boolean;
  end;

  PGatherSpec = ^TGatherSpec;
  TGatherSpec = object(TGatherObj)
    patInfo   : PGatherObj;   {- patient info for specimen - nil if no patient
                                 exists for specimen }
    isoInfo   : PGatherIso;
    gsInfo    : PGatherGS;
    {}
    constructor Init(aListObj: PListObject; aListRec: PMnemListRec;
                     doLoadResults, allowYield, xForceMultiDrugs, xCollectXtras: boolean);
    destructor Done; virtual;
    function LoadRecord(aSeq: TSeqNum): integer; virtual;
    function LoadField(fldNum: integer): boolean; virtual;
    procedure Clear; virtual;
    private
      ForceMultiDrugs: boolean;
      CollectXtras   : boolean;
  end;

IMPLEMENTATION

uses
  StrsW,
  IntrpMap,
  ApiTools,
  Screens,
  Strings,
  Bits,
  DBIds,
  PnlView;

{----------------------------------------------------------[ TGatherObj ]--}

constructor TGatherObj.Init(aListObj: PListObject; aListRec: PMnemListRec; fName: PChar; allowYield: boolean);
{- gather data for a database record.
   aListObj   - A list object. If nil, one will be created. Otherwise, the
                passed listObj will be used
   fName      - File name to load
   aSeq       - Seq number of record to load }
var
  k       : integer;
  isOK    : boolean;
  pstr    : array[0..100] of char;
begin
  inherited Init;

  listObj:= nil;
  listRec:= nil;
  db:= nil;
  listRecCreated:= false;
  listObjCreated:= false;
  numFlds:= 0;
  isOK:= true;
  FillChar(flds, sizeof(flds), 0);
  dataOK:= false;
  doYield:= allowYield;

  {- open list object }
  if aListObj = nil then
  begin
    listObj:= New(PListObject, Init);
    if listObj = nil then
      isOK:= false
    else
      listObjCreated:= true;
  end
  else
    listObj:= aListObj;

  {- open mnemonic list record }
  if aListRec = nil then
  begin
    listRec:= New(PMnemListRec, Init);
    if listRec = nil then
      isOK:= false
    else
      listRecCreated:= true;
  end
  else
    listRec:= aListRec;

  {- open data file }
  if isOK then
  begin
    db:= OpenGatherFile(fname);
    isOK:= db <> nil;
  end;

  if isOK then
  begin
    {- Get Schema information about the file }
    numFlds:= db^.dbd^.NumFields;
    for k:= 1 to numFlds do
    begin
      db^.dbd^.FieldName(k, pstr, sizeof(pstr)-1);
      flds[k].name:= StrNew(pstr);
      flds[k].value:= nil;
    end;
  end;

  if not isOK then
  begin
    Done;
    fail;
  end;
end;

destructor TGatherObj.Done;
var
  k   : integer;
begin
  inherited Done;
  if listRecCreated and (listRec <> nil) then
    Dispose(listRec, Done);
  if listObjCreated and (listObj <> nil) then
    Dispose(listObj, Done);
  if db <> nil then
    Dispose(db, Done);
  for k:= 1 to numFlds do
  begin
    StrDispose(flds[k].name);
    StrDispose(flds[k].value);
  end;
end;

function TGatherObj.OpenGatherFile(fName: PChar): PDBFile;
begin
  OpenGatherFile:= New(PDBFile, Init(fName, '', dbOpenNormal));
end;

function TGatherObj.DataLoaded: boolean;
{- this function returns true when there is valid data in the gather object. This
   will only be after a successful LoadData call }
begin
  DataLoaded:= dataOK;
end;

function TGatherObj.LoadRecord(aSeq: TSeqNum): integer;
{- load and gather the specified data. This function destroys any existing gathered
   data in the object.
   Returns 0 if successful, otherwise an error number is returned }
var
  k     : integer;
  ret   : integer;
begin
  ret:= -1;
  Clear;
  if db^.dbc^.GetSeq(db^.dbr, aSeq) then
  begin
    dataOK:= true;
    ret:= 0;
    for k:= 1 to numFields do
    begin
      LoadField(k);
    end;
  end
  else
    ret:= db^.dbc^.dbErrorNum;
  LoadRecord:= ret;
end;

procedure TGatherObj.Clear;
var
  k   : integer;
begin
  dataOK:= false;
  db^.dbr^.ClearRecord;
  for k:= 1 to numFlds do
  begin
    StrDispose(flds[k].value);
    flds[k].value:= nil;
  end;
end;

procedure TGatherObj.CheckYield;
begin
  if doYield then
    HandleEvents(0);
end;

function TGatherObj.LoadField(fldNum: integer): boolean;
{- Loads a field from db^.dbr data member. If the field is a mnemonic ref then
   then field is expanded to its proper description }
var
  fi    : TFldInfo;
  ai    : TAssocInfo;
  seq   : TSeqNum;
  pstr  : PChar;
  size  : word;
begin
  LoadField:= false;
  if (fldNum > 0) and (fldNum <= numFlds) then
  begin
    MSStrDispose(flds[fldNum].value);
    if db^.dbd^.FieldInfo(fldNum, fi) then  {- get field information }
    begin
      if (fi.userFlag and DB_MNEMONIC) <> 0 then {- field is a mnemonic like field }
      begin
        if fi.fldAssoc > 0 then
        begin
          db^.dbd^.AssocInfo(fi.fldAssoc, ai);
          db^.dbr^.GetField(fldNum, @seq, sizeof(seq));
          if listObj^.FindMnemSeq(ai.fName, listRec, seq) then
          begin
            flds[fldNum].value:= StrNew(listRec^.desc);
            LoadField:= true;
          end;
        end;
      end
      else
      begin
        size:= db^.dbd^.FieldSizeForStr(fldNum);
        GetMem(pstr, size);
        db^.dbr^.GetFieldAsStr(fldNum, pstr, size-1);
        flds[fldNum].value:= StrNew(pstr);
        FreeMem(pstr, size);
        LoadField:= true;
      end;
    end;
  end;
  CheckYield;
end;

function TGatherObj.NumFields: integer;
begin
  NumFields:= numFlds;
end;

function TGatherObj.GetField(fldNum: integer; buf: PChar; maxLen: integer): boolean;
begin
  if (fldNum > 0) and (fldNum <= numFlds) and (buf <> nil) then
  begin
    if flds[fldNum].value = nil then
      StrCopy(buf, '')
    else
      StrLCopy(buf, flds[fldNum].value, maxLen);
    GetField:= true;
  end
  else
    GetField:= false;
end;

function TGatherObj.GetFieldName(fldNum: integer; buf: PChar; maxLen: integer): boolean;
begin
  if (fldNum > 0) and (fldNum <= numFlds) and (buf <> nil) then
  begin
    StrLCopy(buf, flds[fldNum].name, maxLen);
    GetFieldName:= true;
  end
  else
    GetFieldName:= false;
end;

function TGatherObj.FieldStrLen(fldNum: integer): integer;
{- return the space required to store the field VALUE for the specified field.
   The terminating null is accounted for in the return result }
begin
  if (fldNum > 0) and (fldNum <= numFlds) and (flds[fldNum].value <> nil) then
    FieldStrLen:= StrLen(flds[fldNum].value) + 1
  else
    FieldStrLen:= 0;
end;

function TGatherObj.FieldNameStrLen(fldNum: integer): integer;
{- return the space required to store the field NAME for the specified field.
   The terminating null is accounted for in the return result }
begin
  if (fldNum > 0) and (fldNum <= numFlds) and (flds[fldNum].name <> nil) then
    FieldNameStrLen:= StrLen(flds[fldNum].name) + 1
  else
    FieldNameStrLen:= 0;
end;

{---------------------------------------------------[ TGatherResCollection ]--}
{- this is a special collection for gathering results. It overrides the sorting
   mechanism of default collections to get the results in the defined drug
   sort order }

function TGatherResCollection.KeyOf(item: Pointer): Pointer;
begin
  keyOf:= @PGatherResObj(item)^.drugSort;
end;

function TGatherResCollection.Compare(Key1, Key2: Pointer): integer;
begin
  Compare:= StrComp(PChar(key1), PChar(key2));
end;

{----------------------------------------------------------[ TGatherResObj ]--}

constructor TGatherResObj.Init(aName: PChar; aDrugSortIdx: longint; aDrugSeq: TSeqNum; isSuppressed: boolean);
var
  pstr    : array[0..25] of char;
begin
  inherited Init;
  name:= StrNew(aName);
  drugSeq:= aDrugSeq;
  drugSortIdx:= aDrugSortIdx;
  Str(drugSortIdx, pstr);
  LeftPad(drugSort, pstr, '0', sizeof(drugSort)-1);
  suppressed:= isSuppressed;
  resIdx:= -1;
  ncclsIdx:= -1;
  strCopy(drugAlt, '');
  strCopy(drugAbbr, '');
end;

destructor TGatherResObj.Done;
begin
  inherited Done;
  StrDispose(name);
end;

{------------------------------------------------------------[ TGatherRes ]--}

constructor TGatherRes.Init(aResults: PResults;
                            aListObj: PListObject;
                            OrgNum  : TSeqNum;
                            allowYield,
                            xForceMultiDrugs,
                            xCollectXtras: boolean);

{- gather test results for an isolate.
   aResults - Must be initialized and must have already been loaded with results
              IMPORTANT: The results object passed in MUST NOT be disposed of
              while this object is still being used.
   aListObj - Can be passed in to avoid overhead or may be nil in which case
              this object will attempt to create a new one. }
Const
  GStaph = $01;
  GSyngy = $16;
  XtraTestStrings='.TFG.StS.GmS.BL.ESBL';

var
  listObj   : PListObject;
  pstr      : array[0..64] of char;
  FilterMult: Boolean;
  testDB    : PDBFile;
  idx       : integer;
  XtraIdx   : integer;

  procedure GetIT(aRes: PResRecObj); far;
  var
    row   : longint;
    rec   : PDBRec;
    dSort : longint;
    obj   : PGatherResObj;
    suppr : boolean;
    BioRes: PResRecObj;
    IsHNID: Boolean;

    procedure MapResToMaster;
    begin
      if aResults^.TraysObj^.MapDrgAbbr(aRes^.TestID,pStr) then
      begin
        TestDB^.dbr^.PutField(DBTSTID, @pStr);
        TestDB^.dbr^.PutField(DBTSTTstCat, @aRes^.TestCatID);
        TestDB^.dbc^.GetEQ(TestDB^.dbr);
        TestDB^.dbr^.GetField(DBTSTDrugRef, @aRes^.drugSeq, SizeOf(aRes^.drugSeq));
        StrCopy(aRes^.TestID,pStr);
      end;
    end;

    function IsBioType(Res: PResRecObj): Boolean; far;
    begin
      IsBioType:=StrComp(Res^.TestCatID, 'BIO')=0;
    end;

  begin
    CheckYield;
    Inc(idx);
    if (aRes^.drugSeq <> 0) then   {- MIC/NCCLS results }
    begin
      If ForceMultiDrugs or
         (not (FilterMult and
          Results^.GetFlag(aRes^.TESTID, 'NCCLS', REScontraIndicated))) then
      begin
        {Remap this drug at the DrugSeq level}
        If (not ForceMultiDrugs) and FilterMult
           then MapResToMaster;

        obj:= FindDrug(aRes^.drugSeq);  {- see if this drug seq is already collected }
        if obj = nil then
        begin
          rec:= listObj^.PeekSeq(DBDrugFile, aRes^.drugSeq);
          if rec <> nil then
          begin
            rec^.GetField(DBDrugName, @pstr, sizeof(pstr)-1);
            rec^.GetField(DBDrugSortNum, @dSort, sizeof(dSort));
            rec^.GetField(DBDRUGSupp, @suppr, sizeof(suppr));

            obj:= New(PGatherResObj, Init(pstr, dSort, aRes^.drugSeq, suppr));
            rec^.GetField(DBDrugAbbr, @pstr, sizeof(pstr)-1);

            StrLCopy(obj^.drugAbbr, pstr, sizeof(obj^.drugAbbr)-1);
            rec^.GetField(DBDrugAltAbbr, @pstr, sizeof(pstr)-1);
            StrLCopy(obj^.drugAlt, pstr, sizeof(obj^.drugAlt)-1);

            micRes^.Insert(obj);
          end;
        end;

        if (StrComp(aRes^.testCatID, 'MIC') = 0) then
           obj^.resIdx:= idx
        else if StrComp(aRes^.testCatID, 'NCCLS') = 0 then
                obj^.ncclsIdx:= idx;
      end;
    end
    else if (CollectXtras and (StrPos(XtraTestStrings,aRes^.testCatID) <> NIL)) then {- XtraTests }
    begin
      {Supress BL if the org is not in gStaph and the traytype is not an HNID}
      BioRes:=aResults^.FirstThat(@IsBioType);
      IsHNID:=(BioRes<>Nil) and (StrComp(BioRes^.TestGroupID,'HNID')=0);

      Suppr:=( (StrComp(aRes^.TestCatID,'BL')=0) and not CheckOrg(orgNum, GStaph) ) and
             ( Not IsHNID );

      {Suppress GmS or Sts, when the org is not in gSyngy}
      Suppr:=Suppr or
      ( ( (StrComp(aRes^.TestCatID,'GmS')=0)  or (StrComp(aRes^.TestCatID,'StS')=0) ) and
        not CheckOrg(orgNum, GSyngy) );

      if not Suppr then
      begin
        XtraIdx:=MaxInt-aRes^.TestSeq;
        obj:= FindDrug(XtraIdx);  {- see if this id has already been collected }
        if (obj=Nil) and (StrLen(aRes^.Result)>0) then
        begin
          obj:= New(PGatherResObj, Init(aRes^.testName, XtraIdx, XtraIdx, false));
          obj^.resIdx:= idx;

          StrCopy(obj^.drugAbbr, @aRes^.testID[1]);
          StrCopy(obj^.drugAlt, aRes^.testName);
          micRes^.Insert(obj);
        end;
      end;
    end
    else    {- non MIC results (id results) }
    begin
      obj:= New(PGatherResObj, Init(aRes^.testName, 0, 0, false));
      idRes^.Insert(obj);
      obj^.resIdx:= idx;
    end;
  end;

begin
  inherited Init;

  idRes:= nil;
  micRes:= nil;
  doYield:= allowYield;
  ForceMultiDrugs:=xForceMultiDrugs;
  CollectXtras:=xCollectXtras;
  FilterMult:=ScreenOn(FilterMultiDrugs);

  if aResults = nil then
  begin
    FatalError('Error', 'Invalid results object passed to TGatherRes.Init');
    Done;
    Fail;
  end;

  idRes:= New(PGatherResCollection, Init(25, 25));
  micRes:= New(PGatherResCollection, Init(25, 25));
  if (idRes = nil) or (micRes = nil) then
  begin
    FatalError('Error', 'Cannot allocate result collection (TGatherRes).');
    Done;
    Fail;
  end;

  idRes^.duplicates:= true;   {- allow for duplicates in the collection }
  micRes^.duplicates:= true;

  results:= aResults;
  results^.doYield:= allowYield;

  if aListObj = nil then
  begin
    listObj:= New(PListObject, Init);
    if listObj = nil then
    begin
      FatalError('Error', 'Cannot create list object in TGatherRes.');
      Done;
      fail;
    end;
  end
  else
    listObj:= aListObj;


 {Needed for drugref remapping}
  Idx:=-1;
  TestDB:= New(PDBFile, Init(DBTSTFile, '', dbOpenNormal));

  results^.ForEach(@GetIt);

  {Release the DB object}
  Dispose(TestDB,Done);

  if (aListObj = nil) and (listObj <> nil) then
    Dispose(listObj, Done);

end;

destructor TGatherRes.Done;
begin
  inherited Done;
  if idRes <> nil then
    Dispose(idRes, Done);
  if micRes <> nil then
    Dispose(micRes, Done);
end;

procedure TGatherRes.CheckYield;
begin
  if doYield then
    HandleEvents(0);
end;

function TGatherRes.NumIDResults: integer;
begin
  NumIDResults:= idRes^.count;
end;

function TGatherRes.NumMICResults: integer;
{- return number of MIC results }
begin
  NumMICResults:= micRes^.count;
end;

function TGatherRes.GetIDTestName(idx: integer; pstr: PChar; maxLen: integer): boolean;
var
  obj   : PGatherResObj;
begin
  obj:= ValidIDX(idRes, idx);
  if obj <> nil then
  begin
    StrLCopy(pstr, obj^.name, maxLen);
    GetIDTestName:= true;
  end
  else
    GetIDTestName:= false;
end;

function TGatherRes.GetIDTestCat(idx: integer; pstr: PChar;  maxLen: integer): boolean;
var
  obj   : PGatherResObj;
  res   : PResRecObj;
begin
  obj:= ValidIDX(idRes, idx);
  if obj <> nil then
  begin
    res:= results^.At(obj^.resIdx);
    StrLCopy(pstr, res^.testCatID, maxLen);
    GetIDTestCat:= true;
  end
  else
    GetIDTestCat:= false;
end;

function TGatherRes.GetIDResult(idx: integer; pstr: PChar; maxLen: integer): boolean;
var
  obj   : PGatherResObj;
  res   : PResRecObj;
  resIdx: integer;
begin
  GetIDResult:= false;
  obj:= ValidIDX(idRes, idx);
  if obj <> nil then
  begin
    if GetIDResIdx(idx, resIdx) then
    begin
      res:= results^.At(resIdx);
      StrLCopy(pstr, res^.result, maxLen);
      GetIDResult:= true;
    end;
  end;
end;

function TGatherRes.GetMICResult(idx: integer; mic, nccls: PChar; maxLen: integer): boolean;
var
  obj     : PGatherResObj;
  res     : PResRecObj;
  micIdx  : integer;
  ncclsIdx: integer;
begin
  GetMICResult:= false;
  obj:= ValidIDX(micRes, idx);
  if obj <> nil then
  begin
    if GetMICResIdx(idx, micIdx, ncclsIdx) then
    begin
      if micIDX >= 0 then
      begin
        res:= results^.At(micIdx);
        StrLCopy(mic, res^.result, maxLen);
      end
      else
        StrLCopy(mic, '---', maxLen);

      if ncclsIdx >= 0 then
      begin
        res:= results^.At(ncclsIdx);
        StrLCopy(nccls, res^.result, maxLen);
      end
      else
        StrLCopy(nccls, '   ', maxLen);

      GetMICResult:= true;
    end;
  end;
end;

function TGatherRes.GetMICResultMapped(idx: integer; mic, nccls: PChar; maxLen: integer): boolean;
{- this function is the same as GetMICResult except that it returns the mapped
   NCCLS result rather than the actual MIC result.
   Note: this function also adheres to contra indication rules in that if a result
         is contra indicated, then no MIC or NCCLS result will be returned, only blanks}
var
  pstr  : array[0..50] of char;
  ci    : boolean;
  micIdx, ncclsIdx: integer;
  aRes  : PResRecObj;
begin
  ci:= false;
  GetMICResult(idx, mic, nccls, maxLen);
  if GetMICResIdx(idx, micIdx, ncclsIdx) then
  begin
    if ncclsIdx >= 0 then
    begin
      aRes:= results^.At(ncclsIdx);
      ci:= TestBit(aRes^.flag, REScontraIndicated); {- check contra indicated }
      StrLCopy(pstr, nccls, sizeof(pstr)-1);
      if not InterpStrToMappedStr(pstr, nccls, maxLen, ci) then
        StrLCopy(nccls, '???', maxLen);
    end;
    if ci then
      StrCopy(nccls, '');
  end;
  if ci then
    StrCopy(mic, '');
end;

function TGatherRes.GetMICDrugName(idx: integer; pstr: PChar; maxLen: integer): boolean;
var
  obj      : PGatherResObj;
begin
  obj:= ValidIDX(micRes, idx);
  if obj <> nil then
  begin
    StrLCopy(pstr, obj^.name, maxLen);
    GetMICDrugName:= true;
  end
  else
    GetMICDrugName:= false;
end;

function TGatherRes.GetMICDrugAbbr(idx: integer; pstr: PChar; maxLen: integer): boolean;
var
  obj       : PGatherResObj;
  oAbbrPChar: array [0..64] of char;
begin
  obj:= ValidIDX(micRes, idx);
  if obj <> nil then
  begin
    StrLCopy(pstr, obj^.drugAbbr, maxLen);
    GetMICDrugAbbr:= true;
  end
  else
    GetMICDrugAbbr:= false;
end;

function TGatherRes.GetMICDrugAlt(idx: integer; pstr: PChar; maxLen: integer): boolean;
var
  obj   : PGatherResObj;
begin
  obj:= ValidIDX(micRes, idx);
  if obj <> nil then
  begin
    StrLCopy(pstr, obj^.drugAlt, maxLen);
    GetMICDrugAlt:= true;
  end
  else
    GetMICDrugAlt:= false;
end;

function TGatherRes.GetMICDrugSeq(idx: integer): TSeqNum;
{- return the drug seq number for a specified gathered index }
var
  obj   : PGatherResObj;
begin
  obj:= ValidIDX(micRes, idx);
  if obj <> nil then
    GetMICDrugSeq:= obj^.drugSeq
  else
    GetMICDrugSeq:= -1;
end;

function TGatherRes.GetMICDrugSortIdx(idx: integer): longint;
{- return the sort index for a specified drug result. Returns -1 if idx is invalid }
var
  obj   : PGatherResObj;
begin
  obj:= ValidIDX(micRes, idx);
  if obj <> nil then
    GetMICDrugSortIdx:= obj^.drugSortIdx
  else
    GetMICDrugSortIdx:= -1;
end;

function TGatherRes.GetIDResIDX(idx: integer; var resIDX: integer): boolean;
var
  obj   : PGatherResObj;
begin
  obj:= ValidIDX(idRes, idx);
  if obj = nil then
    GetIDResIDX:= false
  else
  begin
    resIdx:= obj^.resIDX;
    GetIDResIDX:= true;
  end;
end;

function TGatherRes.GetMICResIDX(idx: integer; var MICidx, NCCLSidx: integer): boolean;
{- returns indexes into the TResults (packed) object for the specified gathered
   index.  This is used to get at the packed results from a gathered index.
   If MICidx or NCCLSidx = -1 then there is no result for that test }
var
  obj   : PGatherResObj;
begin
  obj:= ValidIDX(micRes, idx);
  if obj = nil then
    GetMICResIDX:= false
  else
  begin
    MICidx:= obj^.resIDX;
    NCCLSidx:= obj^.ncclsIDX;
    GetMICResIDX:= true;
  end;
end;

function TGatherRes.GetMICSuppressed(idx: integer): boolean;
{- return true if the specified MIC/interp drug tests are suppressed }
var
  obj   : PGatherResObj;
begin
  obj:= ValidIDX(micRes, idx);
  if obj = nil then
    GetMICSuppressed:= false
  else
    GetMICSuppressed:= obj^.suppressed;
end;

function TGatherRes.GetContraIndicated(idx: integer): boolean;
var
  obj   : PGatherResObj;
  aRes  : PResRecObj;
  micIdx, ncclsIdx: integer;
begin
  GetContraIndicated:= false;
  obj:= ValidIDX(micRes, idx);
  if obj <> nil then
  begin
    GetMICResIdx(idx, micIdx, ncclsIdx);
    if ncclsIdx >= 0 then
    begin
      aRes:= results^.At(ncclsIdx);
      GetContraIndicated:= TestBit(aRes^.flag, REScontraIndicated);
    end;
  end;
end;

function TGatherRes.FindDrugIndex(aDrugSeq: TSeqNum): integer;
{- This function returns the gather index of a specified drug. The index
   returned can be used in the GetMICxxx methods. If the drug name is not
   in the list of drugs for this object, -1 is returned }
var
  obj   : PGatherResObj;
  ret   : integer;
begin
  obj:= FindDrug(aDrugSeq);
  if obj <> nil then
    ret:= micRes^.IndexOf(obj)
  else
    ret:= -1;
  FindDrugIndex:= ret;
end;

function TGatherRes.FindDrug(aDrugSeq: TSeqNum): PGatherResObj;
{- finds an object in the MICres collection that contains the drug seq num }
var
(*  k     : integer;*)
(*  obj   : PGatherResObj;*)
  ret   : PGatherResObj;

  function Matches(anObj: PGatherResObj): boolean; far;
  begin
    Matches:= (anObj^.drugSeq = aDrugSeq);
  end;

begin
(*  ret:= nil;*)
  ret:= micRes^.FirstThat(@Matches);
(*  for k:= 0 to micRes^.count - 1 do*)
(*  begin*)
(*    obj:= micRes^.At(k);*)
(*    if obj^.drugSeq = aDrugSeq then*)
(*    begin*)
(*      ret:= obj;*)
(*      break;*)
(*    end;*)
(*  end;*)
  FindDrug:= ret;
end;

function TGatherRes.ValidIDX(acoll: PGatherResCollection; idx: integer): PGatherResObj;
{- validate the index for the specified collection (micRes, idRes) and return
   a PGatherResObj for the specified index if valid, nil if not }
begin
  if (idx >= 0) and (idx < aColl^.count) then
    ValidIDX:= aColl^.At(idx)
  else
    ValidIDX:= nil;
end;

{----------------------------------------------------------[ TGatherSpec ]--}

constructor TGatherSpec.Init(aListObj: PListObject; aListRec: PMnemListRec;
                             doLoadResults, allowYield,
                             xForceMultiDrugs, xCollectXtras: boolean);
var
  isOK    : boolean;
begin
  patInfo:= nil;
  gsInfo:= nil;
  isoInfo:= nil;
  isOK:= true;
  ForceMultiDrugs:=xForceMultiDrugs;
  CollectXtras:=xCollectXtras;

  if inherited Init(aListObj, alistRec, DBSpecFile, allowYield) then
  begin
    {- at this point, the specimen gather object listObj and listRec should be
       valid. I can use these now and pass them to the subsequent gather objects}
    patInfo:= New(PGatherObj, Init(listObj, listRec, DBPatFile, allowYield));
    if patInfo = nil then
      isOK:= false;

    {- NOTE: the gs Info MAY be nil! This will happen when no gram stain def exists}
    if isOK then
      gsInfo:= New(PGatherGS, Init(listObj, listRec, allowYield));

    if isOK then
    begin
      isoInfo:= New(PGatherIso, Init(listObj, listRec,
                    doLoadResults, allowYield, ForceMultiDrugs, CollectXtras));
      isOK:= isoInfo <> nil;
    end;

    if not isOK then
    begin
      Done;
      fail;
    end;
  end
  else
    fail;
end;

destructor TGatherSpec.Done;
begin
  inherited Done;
  if patInfo <> nil then
    Dispose(patInfo, Done);
  if gsInfo <> nil then
    Dispose(gsInfo, Done);
  if isoInfo <> nil then
    Dispose(isoInfo, Done);
end;

function TGatherSpec.LoadField(fldNum: integer): boolean;
var
  seq   : TSeqNum;
begin
  if inherited LoadField(fldNum) then
  begin
    if fldNum = DBSpecPatRef then
    begin
      if db^.dbr^.GetField(DBSpecPatRef, @seq, sizeof(seq)) then
        patInfo^.LoadRecord(seq)
      else
        patInfo^.Clear;
    end;
  end
  else
    LoadField:= false;
end;

procedure TGatherSpec.Clear;
begin
  inherited Clear;
  patInfo^.Clear;
  if gsInfo <> nil then
    gsInfo^.Clear;
  isoInfo^.Clear;
end;

function TGatherSpec.LoadRecord(aSeq: TSeqNum): integer;
var
  ret   : integer;
begin
  ret:= 0;
  patInfo^.Clear;
  if gsInfo <> nil then
    gsInfo^.Clear;
  ret:= inherited LoadRecord(aSeq);

  {- load gram stain results }
  if gsInfo <> nil then
    gsInfo^.LoadRecord(aSeq);
  isoInfo^.LoadRecord(aSeq);

  LoadRecord:= ret;
end;

{------------------------------------------------------------[ TGatherGS ]--}

constructor TGatherGS.Init(aListObj: PListObject; aListRec: PMnemListRec; allowYield: boolean);
var
  pstr    : array[0..100] of char;
  slot    : byte;
  isOK    : boolean;
begin
  doYield:= allowYield;
  isOK:= true;

  numSlots:= 0;  {- numFlds is the same thing as number of slots for this object }
  FillChar(slots, sizeof(slots), 0);

  {- open the gram stain definition file }
  db:= New(PDBFile, Init(DBGSDefFile, '', dbOpenNormal));
  isOK:= db <> nil;

  if isOK then
  begin
    {- read the gram stain def and fill relative slot info }
    if db^.dbc^.GetFirst(db^.dbr) then
    begin
      repeat
        db^.dbr^.GetField(DBGSDefSlotNum, @slot, sizeof(slot));
        if (slot >= 0) and (slot < MaxGSSlots) then
        begin
          Inc(numSlots);
          db^.dbr^.GetFieldAsStr(DBGSDefAbbr, slots[slot].abbr, MaxGSAbbrLen);
          db^.dbr^.GetFieldAsStr(DBGSDefDesc, slots[slot].name, MaxGSNameLen);
          db^.dbr^.GetField(DBGSDefResType, @slots[slot].resType, sizeof(char));
          slots[slot].seq:= db^.dbr^.GetSeqValue;
        end
        else
          isOK:= false;
        CheckYield;
      until isOK and not db^.dbc^.GetNext(db^.dbr);
    end
    else
      isoK:= false;

    Dispose(db, Done);
    db:= nil;
  end;

  if isOK then
    isOK:= inherited Init(aListObj, aListRec, DBGSFile, allowYield);

  if not isOK then
    fail;
end;

function TGatherGS.GetField(fldNum: integer; buf: PChar; maxLen: integer): boolean;
{- this function has been modified from the ancestor to accomodate gs slots rather
   than fields.
   fldNum   - This is the equivelent of "Slot Number". This is a ZERO BASED number
}
begin
  if (fldNum >= 0) and (fldNum < numSlots) then
  begin
    strlcopy(buf, slots[fldNum].res, maxLen);
    GetField:= true;
  end
  else
    GetField:= false;
end;

function TGatherGS.GetFieldName(fldNum: integer; buf: PChar; maxLen: integer): boolean;
begin
  if (fldNum >= 0) and (fldNum < numSlots) then
  begin
    strlcopy(buf, slots[fldNum].name, maxLen);
    GetFieldName:= true;
  end
  else
    GetFieldName:= false;
end;

function TGatherGS.GetFieldAbbr(fldNum: integer; buf: PChar; maxLen: integer): boolean;
begin
  if (fldNum >= 0) and (fldNum < numSlots) then
  begin
    strlcopy(buf, slots[fldNum].abbr, maxLen);
    GetFieldAbbr:= true;
  end
  else
    GetFieldAbbr:= false;
end;

procedure TGatherGS.Clear;
var
  k   : integer;
begin
  inherited Clear;
  for k:= 0 to numSlots - 1 do
    StrCopy(slots[k].res, '');
end;

function TGatherGS.NumFields: integer;
begin
  NumFields:= numSlots;
end;

function TGatherGS.LoadRecord(aSeq: TSeqNum): integer;
{- load gram stain results for the specified specimen seq number
   aSeq - Specimen seq number }

  function FindSlotNum(aTstSeq: TSeqNum): integer;
  var
    slot  : integer;
  begin
    slot:= 0;
    while (slot < numSlots) and (slots[slot].seq <> aTstSeq) do
      Inc(slot);
    if slot < numSlots then
      FindSlotNum:= slot
    else
      FindSlotNum:= -1;
  end;

var
  k     : integer;
  seq   : TSeqNum;
  ret   : integer;
begin
  ret:= 0;
  Clear;
  db^.dbr^.PutField(DBGSSpecRef, @aSeq);
  if db^.dbc^.GetFirstContains(db^.dbr) then
  begin
    repeat
      db^.dbr^.GetField(DBGSGSTstRef, @seq, sizeof(seq));
      k:= FindSlotNum(seq);
      if k <> -1 then
      begin
        if slots[k].resType = GSFreeText then
          db^.dbr^.GetFieldAsStr(DBGSFreeText, slots[k].res, MaxGSFreeText)
        else
        begin
          db^.dbr^.GetField(DBGSGSMnemRef, @seq, sizeof(seq));
          if listObj^.FindMnemSeq(DBGSMnemFile, listRec, seq) then
            StrLCopy(slots[k].res, listRec^.desc, MaxGSFreeText)
          else if listObj^.errorNum <> 0 then
          begin
            ret:= listObj^.errorNum;
            Clear;
          end;
        end;
      end;
      CheckYield;
    until (ret <> 0) or not db^.dbc^.GetNextContains(db^.dbr);
  end;
  LoadRecord:= ret;
end;

{----------------------------------------------------------[ TGatherIso ]--}

constructor TGatherIso.Init(aListObj: PListObject; aListRec: PMnemListRec;
                            doLoadResults, allowYield,
                            xForceMultiDrugs, xCollectXtras: boolean);
begin
  if inherited Init(aListObj, aListRec, DBIsoFile, allowYield) then
  begin
    curIdx:= -1;
    ForceMultiDrugs:=xForceMultiDrugs;
    CollectXtras:=xCollectXtras;
    loadResults:= doLoadResults;
    isos:= New(PCollection, Init(5, 5));
    if isos = nil then
    begin
      Done;
      fail;
    end;
  end
  else
    fail;
end;

destructor TGatherIso.Done;
begin
  inherited Done;
  Dispose(isos, Done);
end;

function TGatherIso.GetField(fldNum: integer; buf: PChar; maxLen: integer): boolean;
{- return the data for the isolate specified by curIdx. The gather iso object
   collects for all isolates for a specimen, so there may be more than one.
   You must set curIdx (using SetIsoIndex) to the index of the isolate that
   is desired. isolate index is zero based }
var
  fld   : PIsoData;
begin
  if (curIdx >= 0) and (fldNum > 0) and (fldNum <= numFlds) then
  begin
    fld:= isos^.At(curIdx);
    if fld^.fldData[fldNum] = nil then
      StrCopy(buf, '')
    else
      StrLCopy(buf, fld^.fldData[fldNum], maxLen);
    GetField:= true;
  end
  else
    GetField:= false;
end;

function TGatherIso.LoadRecord(aSeq: TSeqNum): integer;
{- load all isolate records for the specified specimen seq number.  This function
   differs from its parent in that aSeq does *not* specify an isolate seq num
   but rather a specimen seq number. This function also sets the current isolate
   index to the first isolate found or -1 if no isolates are found.
   aSeq   - the specimen seq number for which to load all isolates }
var
  data    : PIsoData;
  ret     : integer;
  k       : integer;

begin
  ret:= -1;
  Clear;
  db^.dbr^.PutField(DBIsoSpecRef, @aSeq);
  if db^.dbc^.GetFirstContains(db^.dbr) then
  begin
    dataOK:= true;
    ret:= 0;
    repeat
      CheckYield;
      for k:= 1 to numFlds do
        LoadField(k);
      data:= New(PIsoData, Init(flds, db^.dbr, listObj,
                 loadResults, doYield, ForceMultiDrugs, CollectXtras));
      if data = nil then
        ret:= -1
      else
        isos^.Insert(data);
    until (ret <> 0) or not db^.dbc^.GetNextContains(db^.dbr);
  end
  else
    ret:= db^.dbc^.dbErrorNum;

  if (ret = 0) and (isos^.count > 0) then
    curIdx:= 0
  else
    curIdx:= -1;

  LoadRecord:= ret;
end;

procedure TGatherIso.Clear;
begin
  inherited Clear;
  isos^.FreeAll;
end;

{- additional isolate related functions }
function TGatherIso.GetNumIsolates: integer;
begin
  GetNumIsolates:= isos^.count;
end;

function TGatherIso.SetIsoIndex(idx: integer): boolean;
begin
  if (idx >= 0) and (idx < isos^.count) then
  begin
    curIdx:= idx;
    SetIsoIndex:= true;
  end
  else
    SetIsoIndex:= false;
end;

function TGatherIso.GetIsoIndex: integer;
{- return the current isolate index. The isolate index is the value that determines
   which isolate's data is returned when a GetField or GetResults is called.
   This index is ZERO BASED }
begin
  GetIsoIndex:= curIdx;
end;

function TGatherIso.GetResults: PGatherRes;
{- return a pointer to a results gather object for this isolate. }
var
  fld   : PIsoData;
begin
  if (curIdx >= 0) then
  begin
    fld:= isos^.At(curIdx);
    GetResults:= fld^.resList;
  end
  else
    GetResults:= nil;
end;

{--------------------------------------------------------------[ TIsoData ]--}

constructor TIsoData.Init(var flds: TGatherFldList; aDBR: PDBRec; aListObj: PListObject;
                          doLoadResults, allowYield,
                          xForceMultiDrugs, xCollectXtras: boolean);
var
  k     : integer;
  seq   : TSeqNum;
  OrgNum: Integer;
  Code  : INteger;
  dbOrg : PDBFile;
  pStr  : array [0..15] of char;
begin
  inherited Init;
  resList:= nil;
  results:= nil;
  ForceMultiDrugs:=xForceMultiDrugs;
  CollectXtras:=xCollectXtras;
  numFlds:= aDBR^.desc^.NumFields;
  FillChar(fldData, sizeof(fldData), 0);

  for k:= 1 to numFlds do
    fldData[k]:= StrNew(flds[k].value);

  if doLoadResults then
  begin
    results:= New(PResults, Init);
    if results <> nil then
    begin
      { organism }
      aDbr^.GetField( DBIsoOrgRef, @seq, sizeof( TSeqNum ) );
      if seq <> 0 then
      begin
        dbOrg:= New( PDBFile, Init( DBOrgFile, '', dbOpenNormal ) );
        dbOrg^.dbc^.GetSeq( dbOrg^.dbr, seq );
        dbOrg^.dbr^.GetFieldAsStr( DBOrgOrg, pStr, 15 );
        Val(pStr, orgNum, code);
        MSDisposeObj(dbOrg);
      end;

      results^.doYield:= allowYield;
      results^.LoadResults(aDBR);
      reslist:= New(PGatherRes, Init(results, aListObj, OrgNum, allowYield,
                    ForceMultiDrugs, CollectXtras));
      if resList = nil then
      begin
        Done;
        fail;
      end;
    end
    else
    begin
      Done;
      Fail;
    end;
  end;
end;

destructor TIsoData.Done;
var
  k   : integer;
begin
  inherited Done;
  for k:= 1 to numFlds do
    StrDispose(fldData[k]);
  if resList <> nil then
    Dispose(resList, Done);
  if results <> nil then
    Dispose(results, Done);
end;

END.

{- rcf }
