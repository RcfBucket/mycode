{*******************************************************}
{                                                       }
{  Prog:  MicroScan/ASTMLink Communications Program     }
{  By  :  Rick Robeson                                  }
{                                                       }
{  Copyright (c) 1992 by Baxter Diagnostics, Inc.       }
{                                                       }
{  Update History:                                      }
{                                                       }
{              rar - Developed for Nihongo DMS with ASTM}
{                    , MicroScan and Xon/Xoff protocols  }
{							for serial ports and disk file	  }
{                    Removed modem capabilities 	     }
{                                                       }
{ ver 1.1															  }
{*******************************************************}
Unit Intastm;

Interface

{DEFINE _TESTMODE}

Uses

  dbcscc,
  Objects,
  WinProcs,
  WinCrt,
  WinDos,
  WinTypes,
  Strings;

 Const

 {$IFDEF _TESTMODE }
  ASTMINI		= '\astmcomm\intcstm.ini';
 {$ELSE}
  ASTMINI		= 'intcstm.ini';
 {$ENDIF}

  { log file error constants }

  ReceiveMode 			= $80; { bit 7 } { these are log type (bitmapped) events }
  TransmitMode 		=   0;

  ReceiveShow 			= 100;  { these are debug log event codes}
  TransmitShow 		= 101;

  LISFrameCountErr 	= 102;  { these are log event error codes }
  LISMaxNakErr 		= 103;  { too many naks during transmit or receive }
  LISNotAvailable 	= 104;  { responded nak to enquire }
  LISNotResponding 	= 105;  { to an enquire }
  LISNoAckResponse 	= 106;  { no response to transmit record }
  LISStopped 			= 107;  { stopped sending without proper termination }
  LISRecordtoolong 	= 108;  { record received too long }
  LISBadChecksum 		= 109;  { bad checksum record received }
  LISBreak				= 108;  { Break interrupt received }
  LISFRame				= 111;  { framing error }
  LISMode				= 112;  { port mode setup error: shouldn't occur }
  LISOverrun			= 113;  { serial port data overrun error }
  LISOverflow			= 114;  { Windows input queue overflow error }
  LISParity				= 115;  { serial port parity error }
  LISXMTOverflow		= 116;  { windows output queue overflow error }
  LISUnknown			= 117;  { unknown or unexpected port error }
  LISXONError			= 118;  { Xoff received without Xon before timeout occurred }

Type

  ChFileType = file of char;

var

	first : boolean;
	outxfile : CHFileType;


  tickdelay,
  automscnt,
   autoscnt,
  autotime,
  nexttransmit,
	autofrequency : word;

  justtransmitted,
  deferupload,
	autotimer : boolean;

	 recs : word;

	 ClientReady : boolean;

(*   globhwnd : HWnd;     { this is the main window of the server module }*)

	 TickCount : Word;
	 IgnoreTicks : Boolean;
    InEscapeDelay : Boolean;
    loglevel : integer;

    FrameCount : byte;      { 0..7 frame counter }
    FrameTimeout,           { ACK wait timeout for next frame (Receive) }
    TimeOutSec : word;      { ACK Wait timeout for ENQ send and Frame Send }

    chksum : byte;

    commCID : integer;
    commDCB : TDCB;
    commDC  : HDC;

    RouteToDisk : Boolean;
    LogRcv  : Boolean;    {Shows state of log receive or transmit}

    InputDiskFile : ChFileType;
    OutputDiskFile : ChFileType;

    InputFileName : array[0..40] of char;
    OutputFileName : array[0..40] of char;
    LogDbgFileName : array[0..40] of char;

    DialString : array[0..80] of char;
	 HangupString : array[0..30] of char;
	 InitString : array[0..30] of char;
   CommPort : array[0..10] of char;   { port name, COM1, COMX, FILE ) }

   TxEmpty,
    timedout,
   AckReceived,
   RcvMode,               { Server is mid receive session }
    XmtMode : Boolean;     { Client/Server is in mid-session transmitting }

    LogDbgFile : ChFileType;  { Debug file }

    AtEORec : Boolean;   { At End of logical record marker }
    EORec : word;        { end of logical record character }
    EOFrame : word;      { end of frame marker }

    IconCount : Byte;
    commbufflength : byte;
    SPortSet : Array[0..40] of char;

    UseMicroScan,
    UseASTM,
   UseChkSum,
   UseXON,
    UseLog,
    UseSISO : Boolean;

    CharSetType : byte;
    MaxNakCount : byte;
    NakCount : Byte;

    AckWait,
    EnqWait : Boolean;

	 commBuffer : array[0..256] of char;
    LastRecord : array[0..256] of char;
    commBLen : integer;

    DebugON        : Boolean;
    Initialized : Boolean;
    inwaitforack : boolean;
    msdelay : integer;

    stx,
    etx,
    eot,
    enq,
    cr ,
    lf ,
    ack,
    nak,
    xon,
    xoff,
    si,
    so : byte;

	 Procedure	 Autotimeron;
	 Function 	NextTime:Word;
	 Function    CommOpen:boolean;
	 Procedure   GotoEof(var f : ChFileType);
    function    openCommfile : Boolean;
    procedure   closeCommfile;
    procedure   SendAck( Ack_Flag : boolean);
    procedure   CommCleanup;
	 Procedure   EscapeDelay(Seconds: integer);
	 Procedure	 Blink;
    Function	 WaitForAck:boolean;
    Procedure   Restart;
    Procedure   AddChar(ch : Char);
    Function    Hextodec(hexdigit : char):integer;
   Procedure   DecToHex(chksumm:integer; var chk1, chk2 : char);
    Function    Readchar(buf : pchar; len : integer):integer;
    Function    WriteCOmmCh(Buf:Pchar):integer;
	 Function    Writerecord(NxtRecord:Pchar):Boolean;
    Function    GetTimeStamp:Longint;
    Function	 Sendenq : boolean;
	 Procedure   ResetComm;
    Procedure   Terminate;
    Procedure   Flushdbg;
	 Procedure   UpdateLog(Buf:Pchar; xlen : integer);
	 Procedure 	 GetFileData; { get information from input disk file }
	 Procedure 	 SetClientReady( ReadyFlag : Boolean);
	 Procedure 	 AddSISO(recptr : Pchar);
	 Procedure   StripSISO(recptr : Pchar);
	 Function 	 Windows31 : boolean;
	 Function    IsDbcsFirstByte(ch : char) : boolean;
	 procedure   timeron;
	 procedure   timeroff;

	 procedure ReportFail( lerror : integer);
	 procedure ReportLineERror(lerror : Integer);
	 Procedure RcvRec(sTmp : Pchar);
	 Function  LogEvent(TimeStamp:Longint; logtype : byte;lognum : integer;
							  datainfo : pchar; logpos : longint):integer;

  procedure formattime(hour, min, sec, tsec : word; stime : pchar);

implementation

{$IFNDEF _TESTMODE }
uses
  intrecv,
  intglob,
	log;
{$ENDIF}

Procedure RcvRec(sTmp : Pchar);
var loop : integer;
begin

{$IFNDEF _TESTMODE}
 intrecv_recvrec(sTmp);
{$ENDIF}
end;

Function LogEvent(TimeStamp:Longint; logtype : byte;lognum : integer;
                  datainfo : pchar; logpos : longint):integer;

begin

{$IFNDEF _TESTMODE}
  Logevent:= LogFile^.logevent(TimeStamp, logtype, lognum, datainfo, logpos);
{$ENDIF}
end;

Procedure PrintError(lerror : integer);
begin


	if not debugon then
   	exit;
	case lerror of
		  LISFrameCountErr : write(' - Frame Count Error');
		  LISMaxNakErr 	 : write(' - Too Many Nak''s ');
  		  LISNotAvailable  :	write(' - Enq request denied');
  	     LISNotResponding :	write(' - No response from mainframe');
        LISNoAckResponse :	write(' - Expected ACK/NAK response');
        LISStopped 		 :	write(' - LIS stopped sending ');
        LISRecordtoolong :	write(' - Record length exceeded');
        LISBadChecksum 	 :	write(' - Bad Checksum');
        LISBreak			 :	write(' - Break Interrupt Received');
        LISFRame			 :	write(' - Framing Error ');
        LISMode			 :	write(' - Port setup mode error');
        LISOverrun		 :	write(' - Overrun error ');
        LISOverflow		 :	write(' - Overflow error');
        LISParity			 :	write(' - Parity error');
        LISXMTOverflow	 :	write(' - Buffer Transmit Overflow error');
        LISUnknown		 :	write(' - Unknown error occurred');
		  LISXONError		 :	write(' - XON not received error');
	end;


end;


Procedure ReportLineError(LError : Integer);
var x : integer;
	fileptr : longint;

begin

	  if rcvmode then
		x := receivemode
	  else
	   x := transmitmode;

	  if uselog then
		  fileptr := filepos(logdbgfile)
	  else
		  fileptr := -1;

  LogEvent(gettimestamp, ErrorBit, lerror, nil, fileptr);
	if debugon then
   begin

	  writeln('');
    write('Failure error: ', lerror);
	  printerror(lerror);
     writeln('');
	end;


end;

Procedure ReportFail(lError : Integer);
var x : integer;
	fileptr : longint;

begin

	  if rcvmode then
		x := receivemode
	  else
	   x := transmitmode;

	  if uselog then
		  fileptr := filepos(logdbgfile)
	  else
	  		  fileptr := -1;

    LogEvent(gettimestamp, ErrorBit, lerror, nil, fileptr);

	  if debugon then
     begin
	  	writeln('');
      write('Failure error: ', lerror);
	 	 printerror(lerror);
     	writeln('');
     end;

	  if rcvmode then
     	rcvrec('');	{ report line failure to record parser }

	  resetcomm;

end;

Procedure StripSISO(recptr : PChar);
var count, index : integer;
begin
	  index := 0;
	  count := 0;

	  if not usesiso then
		 exit;

	  while recptr[count] <> #0 do
	  begin
			if (recptr[count] = chr(SI))  or (Recptr[count] = chr(SO)) then
			  index := index + 1
			else
				if index <> 0 then
					recptr[count-index] := recptr[count];
			inc(count);
     end;
	  recptr[count-index] := #0;
end;

Function Windows31 : boolean;
var x : integer;
begin
	windows31 := false;
	exit;


	 x := getversion;
	 if ((x mod 256) >= 3) and ((x div 256) >= 10) then
			windows31 := true
	 else	windows31 := false;
end;

Function IsDbcsFirstByte(ch : char) : boolean;
begin

	{if windows31 then
   begin
		isdbcsfirstbyte := isdbcsleadbyte(ord(ch));
      exit;
	end;}

	if ((ch >= chr($81)) and (ch <= chr($9F))) or ((ch >= chr($E0)) and (ch <= chr($FC))) then
		IsDbcsFirstByte := true
	else
		IsDbcsFirstByte := false;

end;

Procedure AddSISO(recptr : Pchar);
var tmpbuf : array[0..512] of char;
  isdbcs, dbcs : boolean;
   count1, count2 : integer;

begin

  strcopy(tmpbuf, recptr);
	count1 := 0;
   count2 := 0;
	isdbcs := false;
	dbcs := false;

	while tmpbuf[count1] <> #0 do
	begin
		if isdbcs or IsDBCSFirstByte(tmpbuf[count1]) then
		begin
			if isdbcs then
         	isdbcs := false	{ second byte here so reset second byte flag }
			else
				isdbcs := true;	{ set flag for second byte to come through unmolested }

   		if not dbcs then
			begin
				recptr[count2] := chr(SO);
				inc(Count2);
				dbcs := true;
			end
		end
		else
		begin
			if dbcs then
			begin
				recptr[count2] := chr(SI);
				inc(Count2);
				dbcs := false;
			end;
		end;
		recptr[count2] := tmpbuf[count1];
		inc(count2);
      inc(Count1);
	end;

	recptr[count2] := #0;

end;


{========================================================================}


Procedure Restart; { Reinitialize from ini file and reopen interface files }

var TmpString, TmpString2 : Array[0..25] of char;
    x : integer;
    SBaud,
    SWord,
    SPortNum,
    SParity,
    SStop : Array[0..10] of char;
    tparity: array[0..2] of char;
    TempMsg: array[0..10] of char;

begin

    if Initialized then { second time in }
	 begin
		  setClientReady(false);
		  CommCleanup;
        Initialized := false;
	 end;

   msdelay := GetPrivateProfileInt('IntCstm', 'tickdelay', 52, ASTMINI);

   TimeoutSec := GetPrivateProfileInt('IntCstm', 'Timeout', 15, ASTMINI);
    TimeoutSec := TimeoutSec * round(1000 / MsDelay);

    GetPrivateProfileString('IntCstm', 'IntPort', 'COM1:', CommPort, SizeOf(CommPort), ASTMINI);

   GetPrivateProfileString('IntCstm', 'Debug', 'N', TmpString, sizeof(TmpString), ASTMINI);
	 if TmpString[0] = 'Y' then
		debugon := true
	 else
		Debugon := false;


    LoadString(HInstance, str_IntBase+str_File, TempMsg, SizeOf(TempMsg));
    if strcomp(CommPort, TempMsg) = 0 then
    begin
        RouteToDisk := true;
        GetPrivateProfileString('IntCstm', 'IntInFile', 'IntInput.Fil', InputFileName, sizeof(InputFileName), ASTMINI);
        GetPrivateProfileString('IntCstm', 'IntOutFile', 'IntOut.Fil', OutputFileName, sizeof(InputFileName), ASTMINI);
    	  MaxNakCount := 0;
    end
    else
	  begin
       RouteToDisk := false;

       GetPrivateProfileString('IntCstm', 'IntBaud', '9600', SBaud, SizeOf(SBaud), ASTMINI);
       GetPrivateProfileString('IntCstm', 'IntParity', 'N', SParity, SizeOf(SParity), ASTMINI); { Get all chars}
         GetPrivateProfileString('IntCstm', 'IntStop', '1', SStop, SizeOf(SStop), ASTMINI);     {now. compare with string file}
       GetPrivateProfileString('IntCstm', 'IntData', '8', SWord, SizeOf(SWord), ASTMINI);


       {get parity from string file}
       LoadString(HInstance, str_IntBase+str_Parity, TempMsg, SizeOf(TempMsg));

       if strcomp(SParity, TempMsg) = 0 then
         strcopy(tparity, 'N')
       else
       begin
         LoadString(HInstance, str_IntBase+str_ParityA, TempMsg, SizeOf(TempMsg));
         if strcomp(SParity, TempMsg) = 0 then
           strcopy(tparity, 'O')
         else
         begin
           LoadString(HInstance, str_IntBase+str_ParityB, TempMsg, SizeOf(TempMsg));
           if strcomp(SParity, TempMsg) = 0 then
             strcopy(tparity, 'E')
         end;
       end;

       SPortSet[0] := chr(0); {Initialize }

       strcat(SPortSet, CommPort);
       strcat(SportSet, SBaud);
       strcat(SportSet, PChar(', '));
       strcat(SPortSet, tparity);
       strcat(SPortSet, PChar(', '));
       strcat(SPortSet, SWord);
     strcat(SportSet, PChar(', '));
       strcat(SPortSet, SStop);
       CommPort[4] := Chr(0);   { kill ':' }
    end;

    GetPrivateProfileString('IntCstm', 'IntASTM', 'Yes', TmpString, SizeOf(TmpString), ASTMINI);
    if TmpString[0] = 'Y' then
		 UseASTM := True
    else
       UseASTM := False;

   GetPrivateProfileString('IntCstm', 'IntMicroScan', 'Yes', TmpString, SizeOf(TmpString), ASTMINI);
    if TmpString[0] = 'Y' then
		 UseMicroScan := True
    else
       UseMicroScan := False;

   GetPrivateProfileString('IntCstm', 'DialString', '', DialString, SizeOf(DialString), ASTMINI);
   GetPrivateProfileString('IntCstm', 'HangupString', '', HangupString, SizeOf(HangupString), ASTMINI);
   GetPrivateProfileString('IntCstm', 'InitString', '', InitString, sizeof(InitString), ASTMINI);

   GetPrivateProfileString('IntCstm', 'Chksum', 'Yes', TmpString, SizeOf(TmpString), ASTMINI);
	 if TmpString[0] = 'Y' then
		 UseChksum := True
    else
       UseChksum := False;

    EoRec := GetPrivateProfileint('IntCstm', 'EORec', 13, ASTMINI);
    EoFrame := GetPrivateProfileInt('IntCstm', 'EOFrame', 10, ASTMINI);

	 if usemicroscan then	{ used in addchar routine }
    begin
			EoRec := 10;
         EOFrame := 3;
	 end;

    GetPrivateProfileString('IntCstm', 'IntXON', 'No', TmpString, SizeOf(TmpString), ASTMINI);
    if TmpString[0] = 'Y' then
       UseXON := True
    else
       UseXON := False;

    LogLevel := GetPrivateProfileInt('IntCstm', 'LogLevel', 1, ASTMINI);
	 if LogLevel= 3 then
       UseLog := True
    else
       UseLog := False;

    GetPrivateProfileString('IntCstm', 'DBCS', 'None', TmpString, SizeOf(TmpString), ASTMINI);
    LoadString(HInstance, str_IntBase+str_JIS, TmpString2, SizeOf(TmpString2));
    if (StrComp(TmpString, TmpString2) = 0) then
      CharSetType := 2  { use JIS, expand later for multiple JIS sets }
    else
    begin
      LoadString(HInstance, str_IntBase+str_ShiftJIS, TmpString2, SizeOf(TmpString2));
      if (StrComp(TmpString, TmpString2) = 0) then
        CharSetType := 1  { shift JIS, expand later for multiple JIS character set types }
      else
        CharSetType := 0;  { ASCII/ANSI }
    end;

    GetPrivateProfileString('IntCstm', 'Conv', 'None', TmpString, SizeOf(TmpString), ASTMINI);
    LoadString(HInstance, str_IntBase+str_SISO, TmpString2, SizeOf(TmpString2));
    UseSISO:= (StrComp(TmpString, TmpString2) = 0);

    GetPrivateProfileString('IntCstm', 'LogDbgFile', 'IntDbg.fil', LogDbgFileName, SizeOf(LogDbgFileName), ASTMINI);

    if not routetodisk then
    MaxNakCount := GetPrivateProfileInt('IntCstm', 'NakCount', 6, ASTMINI);

    FrameTimeout := GetPrivateProfileInt('IntCstm', 'FrameTimeout', 30, ASTMINI);
	 FrameTimeout := FrameTimeout * round(1000 / MsDelay);


   stx := GetPrivateProfileInt('IntCstm', 'stx', 2, ASTMINI);
   etx := GetPrivateProfileInt('IntCstm', 'etx', 3, ASTMINI);
   eot := GetPrivateProfileInt('IntCstm', 'eot', 4, ASTMINI);
   enq := GetPrivateProfileInt('IntCstm', 'enq', 5, ASTMINI);
   cr  := GetPrivateProfileInt('IntCstm', 'cr', 13, ASTMINI);
   lf  := GetPrivateProfileInt('IntCstm', 'lf', 10, ASTMINI);
   ack := GetPrivateProfileInt('IntCstm', 'ack', 6, ASTMINI);
   nak := GetPrivateProfileInt('IntCstm', 'nak', 21, ASTMINI);
   xon := GetPrivateProfileInt('IntCstm', 'xon', 17, ASTMINI);
   xoff:= GetPrivateProfileInt('IntCstm', 'xoff', 19, ASTMINI);
   si  := GetPrivateProfileInt('IntCstm', 'si', 15, ASTMINI);
   so  := GetPrivateProfileInt('IntCstm', 'so', 14, ASTMINI);


    if not Initialized then
	 begin
		  first := true;
		  Initialized := true;
		  OpenCommFile;
		  CommOpen;
        AutoTimeron;
		  TimerOn;
		  setClientReady(true);
	 end;
end;

function converttime(time : pchar):word;
{ converts from hh:mm or h:mm format to minutes offset from midnight }
VAR loop, tmptime : word;

begin
	converttime := 0;

	if strlen(time) < 4 then exit;
	tmptime := 0;
	loop := 0;
	if (time[1] >= '0') and (time[1] <= '9') then { hh:mm format }
	begin
    tmptime := (ord(time[loop])-48) * 600;  { 0, 600, or 1200}
      inc(loop);
   end;
	tmptime := tmptime + ((ord(time[loop])-48) * 60);
	inc(loop);	{ get past ':'}
   inc(loop);
	tmptime := tmptime + ((ord(time[loop])-48)*10);
   inc(loop);
	tmptime := tmptime + (ord(time[loop])-48);
	converttime := tmptime;
end;

procedure formattime(hour, min, sec, tsec : word; stime : pchar);
var
	t : array[0..3] of word;
   tmpstr : array [0..15] of char;

begin
	t[0] := hour;
	t[1] := min;
	t[2] := sec;
   t[3] := tsec;
  wvsprintf(tmpstr, '%02u:%02u:%02u:%03u', t);
   strcopy(stime, tmpstr);
end;


Function NextTime : word;
var
  hhour, hmin, hsec, htsec : word;
	curtime : word;
	tmptime : integer;
   tmpstr : array[0..40] of char;
begin

	nexttime := 0;
	if not autotimer then exit;

  gettime(hhour, hmin, hsec, htsec);
	curtime := hhour * 60 + hmin;
	tmptime := autotime;
	while tmptime >= 0 do
		tmptime := tmptime - autofrequency;
	while (tmptime < (24*60)) and (tmptime <= curtime) do
		 tmptime := tmptime + autofrequency;
	if tmptime >= (24*60) then
   		tmptime := tmptime - (24*60);
	nexttime := tmptime;
	nexttransmit := tmptime; { set global variable }

	if debugon then
   begin
    formattime(nexttransmit div 60, nexttransmit mod 60, 0, 0, tmpstr);
    writeln('next autoupload at ', tmpstr);
	end;

end;

procedure autotimeron;
var timestr : array[0..10] of char;
begin
  { load from group AUTO, vars Enabled, Transmit Time, Frequency }
   GetPrivateProfileString('Auto', 'Enabled', 'N', timestr, 2, ASTMINI);
	if timestr[0] = 'Y' then
		autotimer := true
	else
		autotimer := false;

	if autotimer then
	begin
   		tickdelay := round(1000 / msdelay);
      GetPrivateProfileString('Auto', 'TransmitTime', '0:00', timestr, 7, ASTMINI);
         Autotime := converttime(timestr);
      AutoFrequency := GetPrivateProfileInt('Auto', 'Frequency', 30, ASTMINI);
	end;
	nexttime;
   deferupload := false;
	justtransmitted := false;

end;

procedure convert(InCode, OutCode , Buff : Pchar);   { convert pccode to shift-jis }
var loop : integer;
	dchar : longint;
	wchar : word;

begin

	loop := 0;
	while (Buff[loop] <> #0) do
	begin
		if isdbcsfirstbyte(Buff[loop]) then
		begin
       move(Buff[loop], wchar, 2);
			 wchar := swap(wchar);
          dchar := wchar;
       dchar := DBCSCC_ConvertDBCSCode(InCode, OutCode, dchar );
          move(dchar, Buff[loop], 2);
			 inc(loop);
		end;
      inc(loop);
   end;

end;

Procedure AddChar(ch : Char);
var
	Count : integer;
	recptr : pchar;
	chk2sum : byte;
  chk1, chk2 : char;

begin     { take in characters and build logical records }
          { strip out protocol before sending to client }
          { checksum validation and provide ACK/NAK response }

	 chk2sum := 0;

    if CommBLen >= 255 then
    begin
		if CommBLen = 255 then
			reportfail(LISrecordtoolong);
		CommBLen := 256;
      exit;
    end;

    CommBuffer[CommBLen] := ch;
    inc(CommBLen);
	 CommBuffer[CommBLen] := #0;

	 if ch = chr(eot) then { end of session }
	 begin
		CommBLen := 0;
		framecount := 0;
		if not routetodisk then
		begin
			rcvrec('');
			resetcomm;
		end;
      exit;
    end;

	 Count := 0;

		if not (useastm or usemicroscan) then
			ateorec := true;

    if (ch = chr(eoframe)) and (ateorec) then { end of frame }
    begin
		ateorec := false;
      if CommBuffer[Count] = chr(stx) then
      	inc(Count);
      inc(FrameCount);

      if FrameCount > 7 then
      	FrameCount := 0;

		if useastm and (CommBuffer[Count] <> chr(FrameCount + 48)) then
      	ReportFail(LISframecounterr)
      else
		begin
      	chksum := 0;
      	count := -1;
         if (Commbuffer[0] = chr(stx)) and useastm then
				inc(Count);

			if useastm or usemicroscan then
         begin
           repeat
         	inc(Count);
            chksum := (chksum + ord(CommBuffer[Count])) mod 256;
           until CommBuffer[Count] = chr(eorec);
			end;

			if useastm then         { pickup etx or etb }
			begin
         	inc(Count);
        chksum := (chksum + ord(Commbuffer[Count])) mod 256;
         end;

			if useastm or usemicroscan then
         begin
         	inc(Count);
          chk2sum := ((hextodec(CommBuffer[Count]) * 16) + hextodec(CommBuffer[Count+1])) mod 256;
         end;
         if usechksum and (chksum <> chk2sum) then
         begin { checksum error }
         	inc(NakCount);
				if (not routetodisk) and (NakCount > MaxNakCount) then
					ReportFail(LISMaxNakErr)
				else
            begin
					TickCount := 0;
					ReportLineError(LISBadChecksum);

					SendACK(False);
					if debugon then
               begin
            dectohex(chksum, chk1, chk2);
            writeln(' Checksum should be: ', chk1, chk2);
               end;
					framecount := framecount - 1;
					if framecount < 0 then
						framecount := 7;
            end;
         end
         else
         begin
				TickCount := 0;
				{send to client here}
            count := 0;
            recptr := commbuffer;
				if CommBuffer[count] = chr(stx) then
					recptr := recptr + 1;
				if useASTM then { skip frame count }
					recptr := recptr + 1;
				Count := 0;
				while CommBuffer[Count] <> chr(eorec) do
					inc(Count);

				if usemicroscan then	{ skip line feed (which is eorec) }
					CommBuffer[Count] := #0
				else
            if useASTM then
            	CommBuffer[Count+1] := #0;	{ flag end of logical record }

				if charsettype = 2 then
          convert('90', 'PC', CommBuffer);  { convert jis to pccode }

				if charsettype = 1 then
          convert('S90', 'PC', CommBuffer);   { convert shift-jis to pccode }

				if UseSISO then
					StripSISO(recptr);

            RcvRec(recptr);
            SendAck(True);

			end;
      end;
      CommBuffer[0] := chr(0);
      CommBLen := 0;
    end
    else
       if ch = chr(EoRec) then
          AtEoRec := true;

end;

Function HexToDec(hexdigit : char):integer;
begin

   HexToDec := 0;

   if ((hexdigit >= 'A') and (hexdigit <= 'F')) then
      HexToDec := ord(hexdigit) - ord('A') + 10
   else
   if ((hexdigit >= 'a') and (hexdigit <= 'f')) then
      HexToDec := ord(hexdigit) - ord('a') + 10
   else
   if (hexdigit >= '0') and (hexdigit <= '9') then
       HexToDec := ord(hexdigit) - ord('0');

end;

Procedure DecToHex(chksumm : integer; var chk1, chk2 : char);
var y, z : integer;
begin

   y := chksumm div 16;
   z := chksumm mod 16;

   if y > 9 then
      y := ord('A')-10+y
   else
      y := y + 48;

   if z > 9 then
      z := ord('A')-10+z
   else
      z := z + 48;

   Chk1 := chr(y);
   Chk2 := chr(z);

end;

Procedure GotoEof(var f : ChFileType);
var lsize : longint;

begin
     lsize := filesize(f);
     if filepos(f) <> lsize then
        seek(f, lsize);
end;


Function OpenCommFile : Boolean;
{--------------------------------------------------}
{ Opens Debug Log Files and/or RoutetoDisk Files   }
{--------------------------------------------------}
Var
  indx	 : integer;
Begin

  assign(LogDbgFile, LogDbgFileName);
  {$I-}
  reset(logdbgfile);
  {$I+}

  if ioresult <> 0 then
    Rewrite(LogDbgfile);
  gotoeof(logdbgfile);

  OpenCommFile := true;
 End;

Procedure CloseCommFile;
{--------------------------------------------------}
{                                                  }
{--------------------------------------------------}
Var
  Indx  : integer;
Begin

  {$I- }Close(LogDbgfile); {$I+}

  if RouteToDisk then
  begin
	  {$I-} Close(InputDiskFile);  {$I+}
	  {$I-} Close(OutputDiskFile); {$I+}
  end;

  if ioresult <> 0 then ;	{ clear flag }
End;

Procedure SendACK( Ack_Flag : Boolean);
{--------------------------------------------------}
{  Purpose    :                                    }
{  Responds to:                                    }
{  wParam     :                                    }
{  lParam     :                                    }
{  returns    :                                    }
{--------------------------------------------------}
var buf : array[0..5] of char;
Begin

  if not (useastm or usemicroscan) or routetodisk then
		exit;

  CommBuffer[0] := chr(0);
  CommBLen := 0;

  if Ack_Flag then
		Buf[0] := chr(Ack)
  else
		Buf[0] := chr(Nak);

   Buf[1] := #0;

	writeCommCh(Buf);
   ignoreticks := false;	{ allow input }

End;

Function ReadChar( Buf : PChar; len : Integer):Integer;
var xlen, loop : integer;
	ch : char;
Begin

	Buf[0] := #0;
	xlen := 0;
	if routetodisk then
	begin
    {$I-} read(Inputdiskfile, ch ); {$I+}
		if (ioresult <> 0) then
		begin
      	readchar := 0;
			resetcomm; 	{end of file}
 			exit;
      end
		else
      begin
      	xlen := 1;
			Buf[0] := ch;
      end;
	end
	else
   begin
     xlen := ReadCOMM(CommCID, Buf, len);
	  ReadChar := xlen;
	end;

   Buf[xlen] := #0;

   if (Uselog or debugon) and (xlen > 0) then
   begin
   	if (not logrcv) and uselog then
      begin
      LogEvent(GetTimeStamp, ReceiveMode, ReceiveShow, nil, filepos(logDbgFile));
         logrcv := true;
      end;
    UpdateLog(buf, xlen);
   end;
end;

Procedure GetFileData; { get information from input disk file }
begin

	if not clientready then exit;

  if not initialized then  { should be called before, but just in case... }
		restart;

	if RouteToDisk then
	begin
			if ioresult = 0 then ;

        assign(InputdiskFile, InputFileName);
        {$I-} reset(InputdiskFile); {$I+}

        if ioresult <> 0 then
             Rewrite(InputDiskFile);

		if filesize(InputDiskFile) > 0 then
		begin
  	 		reset(InputDiskFile);
			rcvmode := true;
		end;

  end;


end;

Function WriteCommCh( Buf : PChar ) : Integer;
var xlen, loop : integer;
Begin


	if routetodisk then
	begin
		for loop := 0 to (strlen(Buf)-1) do
      write(outputdiskfile, buf[loop]);
      if uselog or debugon then
      begin
      LogEvent(GetTimeStamp, TransmitMode, TransmitShow, nil, filepos(logDbgFile));
      updatelog(buf, strlen(buf));
      end
	end
	else
   begin
		ignoreticks := true;	{ kill input until ackwait }
   	xlen := strlen(Buf);
		txempty := false;
    xlen := writeCOMM(CommCID, Buf, xlen);

     WriteCommCh := xlen;


     if (Uselog or debugon) and (abs(xlen) > 0) then
     begin
         if logrcv and uselog then
         begin
        LogEvent(GetTimeStamp, TransmitMode, TransmitShow, nil, filepos(logDbgFile));
            logrcv := false;
         end;
         UpdateLog(buf, abs(xlen));
	  end;
	end;

end;

Procedure UpdateLog(Buf:Pchar; xlen : integer) ;

var
   loop, loop2 : integer;
   TmpStr : string[5];
   TmpCh : char;
  Hour, Min, Sec, Tsec : word;
  sHour, sMin, sSec, sTsec : string[4];

begin

{		if posttime then
		begin
      gettime(hour, min, sec, tsec);
      str(hour, shour);
      str(min, smin);
      str(sec, ssec);
      str(tsec, stsec);
			stime :
			posttime := false;
      end;

 }			for loop := 1 to xlen do
         begin
             if Buf[Loop-1] < ' ' then
             begin
					  Tmpch := '[';
					  if debugon then
							write(tmpch);
					  if uselog then
            write(logdbgfile, tmpch);

					  case Buf[Loop-1] of
                 #0: TmpStr := 'nul';
                 #1: TmpStr := 'soh';
                 #2: TmpStr := 'stx';
                 #3: TmpStr := 'etx';
					  #4: TmpStr := 'eot';
                 #5: TmpStr := 'enq';
                 #6: TmpStr := 'ack';
                 #7: TmpStr := 'bel';
                 #8: TmpStr := 'bs';
                 #9: TmpStr := 'ht';
                 #10: TmpStr := 'lf';
                 #11: TmpStr := 'vt';
                 #12: TmpStr := 'ff';
                 #13: TmpStr := 'cr';
                 #14: TmpStr := 'so';
                 #15: TmpStr := 'si';
                 #16: TmpStr := 'dle';
                 #17: TmpStr := 'dc1';
                 #18: TmpStr := 'dc2';
                 #19: TmpStr := 'dc3';
                 #20: TmpStr := 'dc4';
                 #21: TmpStr := 'nak';
                 #22: TmpStr := 'syn';
                 #23: TmpStr := 'etb';
                 #24: TmpStr := 'can';
                 #25: TmpStr := 'em';
                 #26: TmpStr := 'sub';
                 #27: TmpStr := 'esc';
                 #28: TmpStr := 'fs';
                 #29: TmpStr := 'gs';
                 #30: TmpStr := 'rs';
                 #31: TmpStr := 'us';
                 end;

					  for loop2 := 1 to length(TmpStr) do
					  begin
							if debugon then
								write(TmpStr[loop2]);
						if uselog then
                     write(logdbgfile, TmpStr[loop2]);
					  end;

					  tmpch := ']';
						if debugon then
							write(tmpch);
					  if uselog then
                 begin
              write(logdbgfile, tmpch);
							if Buf[loop-1] = #10 then
							begin
								tmpch := #13;
                write(logdbgfile, tmpch);
								tmpch := #10;
                write(logdbgfile, tmpch);
                     end;
                 end;
             end
				 else
				 begin
					if debugon then
						write(Buf[loop-1]);
					if uselog then
            write(logdbgfile, Buf[loop-1]);
             end;
			end;


end;

Function WriteRecord(NxtRecord : Pchar):Boolean;
var x, tmpchksum : byte;
   chk1, chk2 : char;
   offset, count : integer;
	 TmpStr : array[0..5] of char;
	 success : boolean;

begin

	offset := 0;
	writerecord := false;

	if not clientready then
		exit;

  if not initialized then  { should be called before, but just in case... }
		restart;

	  if not xmtmode then
     begin
		resetcomm;
		if not sendenq then
			exit;
	  end;

	  LastRecord[0] := #0;
	  inc(FrameCount);
	  if Framecount > 7 then
		 FrameCount := 0;

	  if useastm or usemicroscan then
     begin
		  LastRecord[0] := chr(stx);
        offset := 1;
		  if useastm then
		  begin
        	LastRecord[1] := chr(FrameCount + 48);
         offset := 2;
        end;
     end;

    strcopy(LastRecord+offset, NxtRecord);

	  x := strlen(LastRecord);

	  if useastm then
	  begin
			if lastrecord[x-1] <> chr(eorec) then
     		begin
				lastrecord[x-1] := chr(eorec);
         	lastrecord[x] := #0;
         end;
	  end;

	  if not useastm then { use microscan or Xon/XOFF }
	  begin
		 {lastrecord[x-1] := chr(cr);
		  if uselfchar then
        begin
					lastrecord[x] := chr(lf);
					lastrecord[x+1] := #0;
        end;
      }
	  end;

     tmpchksum := 0;

	  if charsettype = 2 then
        convert('PC', '90', LastRecord);  { convert pccode to jis }

	  if charsettype = 1 then
        convert('PC', 'S90', LastRecord);   { convert pccode to shift-jis }

		if UseSISO then
	  	  AddSISO(LastRecord);

	  if usemicroscan then
        count := -1
     else
        count := 0;

     repeat
          inc(Count);
          tmpchksum := (tmpchksum + ord(LastRecord[Count])) mod 256;
     until LastRecord[count] = chr(eorec);

	  if useastm or usemicroscan then
	  begin
		  if useastm then
      tmpchksum := (tmpchksum + ord(etx)) mod 256;
	  end;

       dectohex(tmpchksum, chk1, chk2);

	  if useastm then
     begin
     	LastRecord[Count+1] := chr(etx);
     	LastRecord[Count+2] := chk1;
     	LastRecord[Count+3] := chk2;
     	LastRecord[Count+4] := chr(cr);
     	LastRecord[Count+5] := chr(eoframe);
     	LastRecord[Count+6] := chr(0);
	  end
	  else
	  if usemicroscan then
	  begin
		LastRecord[Count+1] := chk1;
     	LastRecord[Count+2] := chk2;
		LastRecord[Count+3] := chr(etx);
		LastRecord[Count+4] := chr(0);
	  end;

	  repeat

     	writecommch(LastRecord);

		success := waitforack;

	  until success or (nakcount >= maxnakcount) or timedout;

	  if timedout then
	  begin
     		if useastm or usemicroscan then
				reportfail(LISNoAckResponse)
			else
				reportfail(LISXONError);

	  end
	  else
	  if (not routetodisk) and (nakcount >= maxnakcount) then
			reportfail(LISmaxnakerr);

	  WriteRecord := success;
	  timedout := false;
	  nakcount := 0;

end;

Function GetTimeStamp : Longint;
var
   timest : tDateTime;
   year,
   mon,
   day,
   dayofw,
  hour,
   min,
   sec,
   tsec : word;
   time : longint;
   loop : integer;

begin
     GetDate(Year, Mon, Day, Dayofw);
     GetTime(hour, min, sec, tsec);
     Timest.year := year;
     Timest.month := mon;
     Timest.day := day;
     Timest.hour := hour;
     Timest.min := min;
     Timest.sec := sec;

     PackTime(Timest, Time);
     GetTimeStamp := Time;

end;


Procedure SetClientReady( ReadyFlag : Boolean);
begin

	if not initialized then
		restart;

	if readyflag then
   begin
			{clearcommbreak(CommCID);}
         clientready := true;
   end
	else
   begin
			{setcommbreak(CommCID);}
			clientready := false;
   end;
end;


Function SendEnq : boolean;
var
	oldcount : integer;
	success : boolean;
   tmpbuf : array[0..5] of char;

begin
	  sendenq := false;

	  if not clientready then exit;

	  if RouteToDisk then
	  begin
		if ioresult = 0 then;

        assign(OutputdiskFile, OutputFileName);
        {$I-} reset(OutputDiskFile); {$I+}

		  if ioresult <> 0 then
             Rewrite(Outputdiskfile);

        GotoEof(OutputdiskFile);

	  end
	  else	resetcomm;

	  XmtMode := true;
	  CommBuffer[0] := #0;

	  if (not (Useastm or usemicroscan)) or routetodisk then
	  begin
        sendenq := true;	{ don't need to send enq }
		  exit;
	  end;

	repeat

	  if nakcount <> 0 then
	  		escapedelay(10);

	  timedout := false;
	  EnqWait := true;
	  tmpbuf[0] := chr(enq);
	  tmpbuf[1] := #0;
	  writecommch(tmpbuf);
	  success := WaitForAck;

	until success or timedout or (nakcount >= 3);

	  if nakcount >= 1 then
     begin
		  if timedout then
				reportfail(LISNotResponding)
		  else
				reportfail(LISNotAvailable);
	  end;
	  timedout := false;

     sendenq := success;

end;

Function WaitForAck : boolean;

var
	Stat : TComstat;

begin

	waitforack := false;

	tickcount := 0;	{ restart timer }

	inwaitforack := true;

	if routetodisk then
	begin
		AckWait := false;
		EnqWait := false;
		AckReceived := false;
		waitforack := true;
      inwaitforack := false;
		exit;
	end;

	if not (useastm or usemicroscan) then
	begin	{ Xon/Xoff mode or no protocol}
		 ignoreticks := false;
		 while (not timedout) and (not TxEmpty) do
			blink;
		 if txempty then
       		waitforack := true; { allow next record }
	end
	else
   begin	{ here are the true ack/nak players }
	  ackwait := true;
     ignoreticks := false;	{ allow input }

	  while (not timedout) and ((not TxEmpty) or (AckWait and not AckReceived)) do
	    	Blink;

	  if ackreceived then
       	waitforack := true;
	end;

	ackreceived := false;
	tickcount := 0;
	inwaitforack := false;
end;

procedure flushdbg;
begin
     if not uselog then
        exit;

	  {$I-} close(LogDbgFile); {$I+}
     if ioresult <> 0 then ;
     reset(LogDbgFile);
     GotoEof(LogDbgFile);

end;

procedure resetcomm;
begin

    Nakcount := 0;
    CommBuffer[0] := #0;
    CommBLen := 0;
    FrameCount := 0;
    tickcount := 0;
    rcvmode := false;
    xmtmode := false;
	 AckWait := false;
    EnqWait := false;
    ateorec := false;
	 LogRcv  := false;
	 timedout := false;
	 txempty := true;
	 flushdbg;
	 ackReceived := false;
    inescapedelay := false;

    if not routetodisk then
    begin
         flushcomm(CommCID, 0);
         flushcomm(CommCID, 1);
			{clearcommbreak(CommCID);}
         ignoreticks := false;
	 end
	 else
	 begin
		  {$I-} close(outputdiskfile); {$I+}
		  {$I-} close(inputdiskfile);  {$I+}
        if ioresult <> 0 then ;
	 end;
end;

procedure terminate;
var tmpbuf : array[0..5] of char;
begin
	  if not clientready then
		exit;
    if (useastm or usemicroscan) and (not routetodisk) then
      begin
			tmpbuf[0] := chr(eot);
      	tmpbuf[1] := #0;
			writecommch(tmpbuf);
         ignoreticks := false;
		   EscapeDelay(1);
		end;

	  resetcomm;
end;

procedure Blink;
Var
  peekMsg: TMsg;
begin
   while (PeekMessage(peekMsg, 0, 0, 0, pm_Remove)) do
    Begin
      TranslateMessage(peekMsg);
      DispatchMessage(peekMsg);
	 End;
end;

procedure EscapeDelay(seconds: integer);
{ Delay specified number of seconds. Used for modem esacpe delay. }
var ticks : integer;
Begin
  ignoreticks := false;	 {allow tick processing }

  ticks := seconds * round(1000 / MsDelay);

  TickCount := 0;
  InEscapeDelay := true;

  While TickCount < Ticks Do
  		Blink;

  InEscapeDelay := false;
End;


Function CommOpen : Boolean;
Var
  RetVal    : Integer;
  PStr,
  BStr      : Array [0..20] of Char;
  Buf : array[0..20] of char;
  TempMsg: array[0..40] of char;
  TempVerMsg: array[0..30] of char;
Begin


  if RouteToDisk then
  begin
      CommOpen := true;
      exit;
  end;

  CommOpen := False;

  RetVal := BuildCommDCB(SPortSet, commDCB);

  If RetVal < 0 Then
  begin
   LoadString(HInstance, str_IntBase+str_BuildComm, TempMsg, SizeOf(TempMsg));
   LoadString(HInstance, str_IntBase+str_IntMsg, TempVerMsg, SizeOf(TempVerMsg));
   MessageBox(globhwnd, TempMsg, TempVerMsg, mb_OK);
  end
  Else
   commCID := OpenComm(CommPort, 512, 512);
    str(commCID, Buf);
  If commCID < 0 Then
  Begin
   LoadString(HInstance, str_IntBase+str_OpenPort, TempMsg, SizeOf(TempMsg));
   LoadString(HInstance, str_IntBase+str_IntMsg, TempVerMsg, SizeOf(TempVerMsg));
   MessageBox(globhwnd, TempMsg, TempVerMsg, mb_OK);
    exit;
  End;
  CommDCB.ID:= CommCID;
  { $a0d8, d8a0, $051b, $1b05, or $0518 }
  if usexon then
  begin               { tdcb }
	 CommDCB.flags := $1b05; { fBinary + fparity + fOutX + fInX + fNull + fChEvt }
    CommDCB.XonChar := chr(Xon);
    CommDCB.XoffChar := chr(Xoff);
	 CommDCB.XonLim := 5;
	 CommDCB.XoffLim := 5;
  end
  else
  begin	{ no xon/xoff }
    CommDCB.flags := $1b05;
    CommDCB.XonChar := #0;
    CommDCB.XoffChar := #0;
  end;
  RetVal := SetCommState(CommDCB);  { set dcb values }
  SetCommEventMask(commCID, ev_RxChar+ev_Err+ev_break+ev_Txempty); { Received char?}

  resetcomm;

End;

Procedure Timeron;
var
   TempMsg: array[0..40] of char;
   TempVerMsg: array[0..30] of char;

Begin
  if SetTimer (globhwnd, 1, MsDelay, nil) = 0 then
  begin
   LoadString(HInstance, str_IntBase+str_TimerOn, TempMsg, SizeOf(TempMsg));
   LoadString(HInstance, str_IntBase+str_IntMsg, TempVerMsg, SizeOf(TempVerMsg));
   MessageBox(globhwnd, TempMsg, TempVerMsg, mb_OK);
  end;
End;

Procedure Timeroff;
var
  TempMsg: array[0..40] of char;
  TempVerMsg: array[0..30] of char;

Begin
  if not killTimer(globhwnd, 1) then
  begin
   LoadString(HInstance, str_IntBase+str_TimerOff, TempMsg, SizeOf(TempMsg));
   LoadString(HInstance, str_IntBase+str_IntMsg, TempVerMsg, SizeOf(TempVerMsg));
   MessageBox(globhwnd, TempMsg, TempVerMsg, mb_OK);
  end;
End;

Procedure CommCleanup;
Var
  RetVal  : Integer;
  Success : boolean;
  TempMsg: array[0..50] of char;
  TempVerMsg: array[0..30] of char;
Begin
  if Initialized then
  begin
  	 timeroff;
	 resetcomm;
	 if not routetodisk then
	 begin
		  RetVal := CloseComm(commCID);
        if retVal < 0 then
        begin
          LoadString(HInstance, str_IntBase+str_ClosePort, TempMsg, SizeOf(TempMsg));
          LoadString(HInstance, str_IntBase+str_IntMsg, TempVerMsg, SizeOf(TempVerMsg));
          MessageBox(globhwnd, TempMsg, TempVerMsg, mb_OK);
        end;
   end;
	 CloseCommFile;
    Initialized := false;
  end;

End;

begin

end.


