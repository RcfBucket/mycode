{$F+}
unit SECTICMP;

{- Isolate detail compressed - driver }

INTERFACE

{- this module interfaces via SECTREG }

IMPLEMENTATION

uses
  DMSDebug,
  DBFile,
  DBTypes,
  DBLib,
  ctllib,
  SectReg,
  SpinEdit,
  IntlLib,
  Gridlib,
  DlgLib,
  RptUtil,
  APITools,
  RptShare,
  DMSErr,
  DMString,
  MScan,
  BoxWin,
  OWindows,
  ODialogs,
  WinProcs,
  WinTypes,
  DBIDS,
  Strings;

{$I RPTGEN.INC}

type
  PSectIsoCompOpts = ^TSectIsoCompOpts;
  TSectIsoCompOpts = record
    separators      : integer;  {- border style -1=none, or PS_xxx constant }
    headers         : boolean;  {- column headers on/off }
    underlineHeader : boolean;  {- underline column headers }
    interps         : boolean;  {- show interps }
    MICs            : boolean;  {- show MICs }
    isoPerSect      : integer;  {- number of isos per section }
    isoWidth        : longint;  {- width of isolate column }
    micWidth        : longint;
    interpWidth     : longint;
    micOrder        : integer;  {- 0 = MIC first, 1 = Interpretation first }
    micSpace        : longint;  {- spacing between MIC and Interp sub columns }
    drugDisp        : integer;  {- drug display type (0=name, 1-abbrev, 2=alternate }
    drugWidth       : longint;
    options         : TSectOpt; {- section options/font }
    font            : TRptFont;
  end;

  {- box (SECTION) window - for insertion onto report format }
  PSectIsoComp = ^TSectIsoComp;
  TSectIsoComp = object(TBoxWin)
    isoCfg    : TSectIsoCompOpts;
    isoDB     : PDBFile;
    constructor Init(AParent: PWindowsObject; ATitle: PChar;
                     rect: TRect; AnOrigin: TPoint; AZoom: real;
                     APage: TRptPage; AFont: TRptFont);
    procedure DefineBox(aParent: PWindowsObject); virtual;
    procedure ResetDefaults; virtual;
    function GetBoxType: integer; virtual;

    function SavePrep: boolean; virtual;
    function SaveData(var dataSeq: longint): boolean; virtual;
    procedure SaveComplete; virtual;
    function LoadData(aBoxSeq: longint): boolean; virtual;
  end;

  PSectIsoCompEdit = ^TSectIsoCompEdit;
  TSectIsoCompEdit = object(TCenterDlg)
    colSep     : PComboBox;
    isoPer     : PSpinInt;
    cfg        : PSectIsoCompOpts;
    tCfg       : TSectIsoCompOpts;
    colWidthEd : PSpinreal;
    drgWidthEd : PSpinreal;
    micSpaceEd : PSpinReal;
    sectWidth  : longint;
    grid       : PCheckGrid;
    {}
    constructor Init(AParent: PWindowsObject; var ACfg: TSectIsoCompOpts; aSectWidth: longint);
    procedure SetupWindow; virtual;
    procedure idFont(var msg: TMessage); virtual ID_FIRST + IDC_FONT;
    procedure Options(var msg: TMessage); virtual ID_FIRST + IDC_OPTIONS;
    procedure IDOrder(var msg: TMessage); virtual ID_FIRST + IDC_ORDER;
    procedure IDWidth(var msg: TMessage); virtual ID_FIRST + IDC_WIDTH;
    function CanClose: Boolean; virtual;
    procedure WMDrawItem(var msg: TMessage);    virtual WM_FIRST + WM_DRAWITEM;
    procedure WMMeasureItem(var msg: TMessage); virtual WM_FIRST + WM_MEASUREITEM;
    procedure WMCharToItem(var msg: TMessage);  virtual WM_FIRST + WM_CHARTOITEM;
  end;

var
  sectionName : array[0..30] of char;


{---------------------------------[ TSectIsoSum ]--}

constructor TSectIsoComp.Init(AParent: PWindowsObject; ATitle: PChar;
                          rect: TRect; AnOrigin: TPoint; AZoom: real;
                          APage: TRptPage; AFont: TRptFont);
begin
  isoDB:= nil;

  inherited Init(AParent, ATitle, rect, AnOrigin, AZoom, APage, AFont);
end;

procedure TSectIsoComp.ResetDefaults;
begin
  FillChar(isoCfg, sizeof(isoCfg), 0);
  with isoCfg do
  begin
    MakeDefaultRFont(font);
    separators:= PS_SOLID;
    headers:= true;
    underlineHeader:= true;
    interps:= true;
    MICs:= true;
    isoPerSect:= 3;

    micWidth:= 1080; {- 3/4 inch }
    interpWidth:= 1080;
    micOrder:= 0;
    micSpace:= 1440 div 8;  {- 1/8 inch }
    isoWidth:= micWidth + interpWidth + micSpace;
    drugDisp:= 1;  {- abbreviation }
    drugWidth:= 720; { 1/2 inch}

    with options do
    begin
      bkMode:= OPAQUE;
      border:= -1;  {- none }
    end;
  end;
end;

procedure TSectIsoComp.DefineBox;
var
  p  : PSectIsoCompEdit;
  sz : TLRect;
begin
  GetSize(sz);
  p:= New(PSectIsoCompEdit, Init(GetParentObj, isoCfg, sz.right));
  if application^.ExecDialog(p) = IDOK then
  begin
    SetDisplayFont(isoCfg.Font);
  end;
end;

function TSectIsoComp.GetBoxType: integer;
begin
  GetBoxType:= SectIsoCompID;
end;

function TSectIsoComp.SavePrep: boolean;
{- open database files and prepare to save }
var
  pstr  : array[0..200] of char;
begin
  isoDB:= New(PDBFile, Init(DBRptIsoCmpFile, '', dbOpenNormal));
  if isoDB = nil then
  begin
    ShowError(GetParentObj, IDS_CANTOPENICMP, nil, dbLastOpenError, MOD_RPTGEN, 1);
  end;
  SavePrep:= isoDB <> nil;
end;

function TSectIsoComp.SaveData(var dataSeq: longint): boolean;
var
  pstr: array[0..200] of char;
  ret   : boolean;
  sz    : TLRect;
begin
  ret:= true;
  if isoDB <> nil then
  begin
    GetSize(sz);

    with isoDB^, dbr^ do
    begin
      PutField(DBRptIsoCompSep,         @isoCfg.separators);
      PutField(DBRptIsoCompHeaders,     @isoCfg.headers);
      PutField(DBRptIsoCompULHeader,    @isoCfg.underlineHeader);
      PutField(DBRptIsoCompInterps,     @isoCfg.interps);
      PutField(DBRptIsoCompMICs,        @isoCfg.MICs);
      PutField(DBRptIsoCompIsoPerSect,  @isoCfg.isoPerSect);
      PutField(DBRptIsoCompIsoWidth,    @isoCfg.isoWidth);
      PutField(DBRptIsoCompMicWidth,    @isoCfg.micWidth);
      PutField(DBRptIsoCompInterpWidth, @isoCfg.interpWidth);
      PutField(DBRptIsoCompMICOrder,    @isoCfg.micOrder);
      PutField(DBRptIsoCompMICSpace,    @isoCfg.micSpace);
      PutField(DBRptIsoCompDrugDisp,    @isoCfg.drugDisp);
      PutField(DBRptIsoCompDrugWidth,   @isoCfg.drugWidth);
      PutField(DBRptIsoCompFont,        @isoCfg.font);
      PutField(DBRptIsoCompOptions,     @isoCfg.options);
      PutField(DBRptIsoCompSize,        @sz);
      dbc^.InsertRec(dbr);
      ret:= dbc^.dbErrorNum = 0;
      if not ret then
      begin
        ShowError(GetParentObj, IDS_CANTINSERTICMP, nil, dbc^.dbErrorNum, MOD_RPTGEN, 0);
        dataSeq:= -1;
      end
      else
        dataSeq:= GetSeqValue;
    end;
  end
  else
    ret:= false;
  SaveData:= ret;
end;

procedure TSectIsoComp.SaveComplete;
begin
  MSDisposeObj(isoDB);
end;

function TSectIsoComp.LoadData(aBoxSeq: longint): boolean;
var
  db    : PDBFile;
  ret   : boolean;
  pstr  : array[0..200] of char;
  sz    : TLRect;
begin
  db:= New(PDBFile, Init(DBRptIsoCmpFile, '', dbOpenNormal));
  if db = nil then
  begin
    ret:= false;
    ShowError(GetParentObj, IDS_CANTOPENICMP, nil, dbLastOpenError, MOD_RPTGEN, 2);
  end
  else
  begin
    with db^, dbr^ do
    begin
      boxSeq:= aBoxSeq;
      dbc^.GetSeq(dbr, boxSeq);
      if dbc^.dbErrorNum = 0 then
      begin
        GetField(DBRptIsoCompSep,         @isoCfg.separators, sizeOf(isoCfg.separators));
        GetField(DBRptIsoCompHeaders,     @isoCfg.headers, sizeof(isoCfg.headers));
        GetField(DBRptIsoCompULHeader,    @isoCfg.underlineHeader, sizeof(isoCfg.underlineHeader));
        GetField(DBRptIsoCompInterps,     @isoCfg.interps, sizeof(isoCfg.interps));
        GetField(DBRptIsoCompMICs,        @isoCfg.MICs, sizeof(isoCfg.MICs));
        GetField(DBRptIsoCompIsoPerSect,  @isoCfg.isoPerSect, sizeof(isoCfg.isoPerSect));
        GetField(DBRptIsoCompIsoWidth,    @isoCfg.isoWidth, sizeof(isoCfg.isoWidth));
        GetField(DBRptIsoCompMicWidth,    @isoCfg.micWidth, sizeof(isoCfg.micWidth));
        GetField(DBRptIsoCompInterpWidth, @isoCfg.interpWidth, sizeof(isoCfg.interpWidth));
        GetField(DBRptIsoCompMICOrder,    @isoCfg.micOrder, sizeof(isoCfg.micOrder));
        GetField(DBRptIsoCompMICSpace,    @isoCfg.micSpace, sizeof(isoCfg.micSpace));
        GetField(DBRptIsoCompDrugDisp,    @isoCfg.drugDisp, sizeof(isoCfg.drugDisp));
        GetField(DBRptIsoCompDrugWidth,   @isoCfg.drugWidth, sizeof(isoCfg.drugWidth));
        GetField(DBRptIsoCompFont,        @isoCfg.font, sizeof(isoCfg.font));
        GetField(DBRptIsoCompOptions,     @isoCfg.options, sizeof(isoCfg.options));
        GetField(DBRptIsoCompSize,        @sz, sizeof(sz));

        SetSize(sz);
      end
      else
      begin
        ShowError(GetParentObj, IDS_CANTLOCSECTRECT, nil, dbc^.dbErrorNum, MOD_RPTGEN, 5);
        CloseWindow;  {- delete the section from the report }
        boxSeq:= -1;
        ret:= false;
      end;
    end;
    MSDisposeObj(db);
  end;
  LoadData:= ret;
end;


{-----------------------------------------------------[ TSectIsoCompEdit ]--}

constructor TSectIsoCompEdit.Init(AParent: PWindowsObject; var ACfg: TSectIsoCompOpts; aSectWidth: longint);
var
  max  : real;
  c    : PControl;
  k    : integer;
begin
  inherited Init(AParent, MakeIntResource(DLG_ISOCOMP));
  LTwipsToUnits(14*1440, max);
  sectWidth:= aSectWidth;
  isoPer:= New(PSpinInt, InitResource(@self, IDC_ISOPERSECT, 0, 0, 1, maxIsoPerSect, 1));
  colWidthed:= New(PSpinreal, InitResource(@self, IDC_ISOWIDTH, 0, 0, 0, max, 0.1));
  micSpaceEd:= New(PSpinreal, InitResource(@self, IDC_MICSPACE, 0, 0, 0, max, 0.1));

  colSep:= New(PComboBox, InitResource(@self, IDC_COLUMNSEP, 0));
  drgWidthed:= New(PSpinreal, InitResource(@self, IDC_DRUGWIDTH, 0, 0, 0, max, 0.1));

  for k:= 400 to 404 do
    c:= New(PLStatic, InitResource(@self, k, 0, false));
  for k:= 300 to 302 do
    c:= New(PLStatic, InitResource(@self, k, 0, false));

  c:= New(PLCheckBox, InitResource(@self, IDC_HEADERS, false));
  c:= New(PLCheckBox, InitResource(@self, IDC_UNDERLINEHEADER, false));
  c:= New(PLRadioButton, InitResource(@self, IDC_NAME, false));
  c:= New(PLRadioButton, InitResource(@self, IDC_ABBREV, false));
  c:= New(PLRadioButton, InitResource(@self, IDC_ALTERNATE, false));

  grid:= New(PCheckGrid, InitResource(@self, IDC_MICINTERP, 3, false, 0));

  cfg:= @ACfg;
  tCfg:= cfg^;
end;

procedure TSectIsoCompEdit.SetupWindow;
var
  p1,p2  : array[0..100] of char;
  k, j   : integer;
  num    : real;

  procedure CheckBoolean(id: Integer; value: Boolean);
  begin
    if value then
      SendDlgItemMsg(id, BM_SETCHECK, bf_Checked, 0)
    else
      SendDlgItemMsg(id, BM_SETCHECK, bf_Unchecked, 0);
  end;

  procedure SetNum(ed: PSpinreal; num: longint);
  {- set a number in an edit field, converting num from LTwips to the currently
     selected units }
  var
    units: real;
  begin
    LTwipsToUnits(num, units);
    ed^.SetNum(units);
  end;

begin
  inherited SetupWindow;
  UnitsStr(true, p2, sizeof(p2)-1);
  for k:= 300 to 302 do
    SendDlgItemMsg(k, WM_SETTEXT, 0, longint(@p2));

  FillLineTypes(HWindow, colSep);
  colSep^.SetSelIndex(0);

  for k:= 0 to colSep^.GetCount - 1 do
  begin
    j:= SendDlgItemMsg(IDC_COLUMNSEP, CB_GETITEMDATA, k, 0);
    if Integer(j) = tcfg.separators then
      colSep^.SetSelIndex(k);
  end;

  grid^.SetAlignment(0, grid_Center);
  grid^.SetAlignment(2, grid_Right);
  grid^.EnableHeader(false);

  with tcfg do
  begin
    k:= grid^.AddString('');
    grid^.SetItemData(k, 0);  {- mic = 0 }
    grid^.SetData(1, k, SR(IDS_MICS, p1, sizeof(p1)-1));
    LTwipsToUnits(MICwidth, num);
    IntlrealToStr(num, p1, sizeof(p1)-1);
    StrCat(p1, ' ');
    StrCat(p1, p2);
    grid^.SetData(2, k, p1);
    grid^.SetCheck(k, MICs);

    k:= grid^.AddString('');
    grid^.SetItemData(k, 1);  {- interp = 1 }
    grid^.SetData(1, k, SR(IDS_INTERPS, p1, sizeof(p1)-1));
    LTwipsToUnits(interpWidth, num);
    IntlrealToStr(num, p1, sizeof(p1)-1);
    StrCat(p1, ' ');
    StrCat(p1, p2);
    grid^.SetData(2, k, p1);
    grid^.SetCheck(k, interps);

    grid^.OptColumnWidth(1);
    grid^.SetAvgCharWidth(0, 3);
    grid^.StretchColumn(2);
    grid^.SetSelIndex(0);

    CheckBoolean(IDC_HEADERS, headers);
    CheckBoolean(IDC_UNDERLINEHEADER, underlineHeader);

    for k:= IDC_NAME to IDC_Alternate do
    begin
      SendDlgItemMsg(k, BM_SETCHECK, BF_UNCHECKED, 0);
      if k - IDC_NAME = drugDisp then
        SendDlgItemMsg(k, BM_SETCHECK, BF_CHECKED, 0);
    end;

    SetNum(colWidthEd, isoWidth);
    SetNum(drgWidthEd, drugWidth);
    SetNum(micSpaceEd, micSpace);

    isoPer^.SetNum(isoPerSect);
  end;
end;

procedure TSectIsoCompEdit.IDFont(var msg: TMessage);
begin
  GetAFont(HWindow, tCfg.font);
end;

procedure TSectIsoCompEdit.Options(var msg: TMessage);
begin
  GetSectOpt(@self, tCfg.options);
end;

procedure TSectIsoCompEdit.IDOrder(var msg: TMessage);
var
  row : integer;
begin
  row:= grid^.GetSelIndex;
  grid^.SwapRows(0, 1);
  if row = 0 then
    row:= 1
  else
    row:= 0;
  grid^.SetSelIndex(row);
  FocusCtl(hWindow, IDC_MICINTERP);
end;

procedure TSectIsoCompEdit.IDWidth(var msg: TMessage);
var
  pstr  : array[0..100] of char;
  p2    : array[0..100] of char;
  row   : integer;
  w     : longint;
  num   : real;
  code  : integer;
  pc    : PChar;
begin
  row:= grid^.GetSelIndex;
  if row <> -1 then
  begin
    grid^.GetData(2, row, pstr, sizeof(pstr)-1);
    pc:= StrScan(pstr, ' ');
    if pc <> nil then
      pc^:= #0;
    Val(pstr, num, code);
    UnitsToLTwips(num, w);

    if grid^.GetItemData(row) = 0 then
      SR(IDS_MICWIDTH, pstr, sizeof(pstr)-1)
    else
      SR(IDS_INTERPWIDTH, pstr, sizeof(pstr)-1);

    if GetWidth(@self, pstr, w, tcfg.isoWidth) then
    begin
      LTwipsToUnits(w, num);
      IntlrealToStr(num, pstr, sizeof(pstr)-1);
      StrCat(pstr, ' ');
      UnitsStr(true, p2, sizeof(p2)-1);
      StrCat(pstr, p2);
      grid^.SetData(2, row, pstr);
    end;
  end
  else
    MessageBeep(0);

  FocusCtl(hWindow, IDC_MICINTERP);
end;

function TSectIsoCompEdit.CanClose: Boolean;
var
  k   : integer;
  ret : boolean;
  pstr: array[0..200] of char;
  p2  : array[0..100] of char;

  procedure SetMICInterp(var bool: boolean; var num: longint; row: integer);
  var
    err   : boolean;
    pc    : PChar;
    r     : real;
  begin
    bool:= grid^.GetCheck(row);
    grid^.GetData(2, row, pstr, sizeof(pstr)-1);
    pc:= StrScan(pstr, ' ');
    if pc <> nil then
      pc^:= #0;
    r:= IntlStrToReal(pstr, err);
    if err then
    begin
      ret:= false;
      ShowError(@self, IDS_INVNUMFORMAT, nil, 0, MOD_RPTGEN, 1);
      FocusCtl(HWindow, IDC_MICINTERP);
      grid^.SetSelIndex(row);
    end
    else
      UnitsToLTwips(r, num);
  end;

begin
  ret:= true;
  with tcfg do
  begin
    ret:= ExtractNum(@self, isoPer, IDS_ERRISOPER, tcfg.isoPerSect, 1, MaxIsoPerSect);

    if ret then
    begin
      micOrder:= grid^.GetItemData(0);
      if micOrder = 0 then
      begin  {- mic is in row 0 }
        SetMICInterp(mics, micWidth, 0);
        if ret then
          SetMICInterp(interps, interpWidth, 1);
      end
      else
      begin {- mic is in row 1 }
        SetMICInterp(mics, micWidth, 1);
        if ret then
          SetMICInterp(interps, interpWidth, 0);
      end;
    end;

    if ret then
      ret:= ExtractWidth(@self, colWidthEd, IDS_ERRWIDTH, tcfg.isoWidth, 0, sectWidth);
    if ret then
      ret:= ExtractWidth(@self, drgWidthEd, IDS_ERRWIDTH, tcfg.drugWidth, 0, sectWidth);
    if ret then
      ret:= ExtractWidth(@self, micSpaceEd, IDS_ERRSPACING, tcfg.micSpace, 0, tcfg.isoWidth);

    if ret then
    begin
      Headers:= SendDlgItemMsg(IDC_HEADERS, BM_GETCHECK, 0, 0) = bf_checked;
      underlineHeader:= SendDlgItemMsg(IDC_UNDERLINEHEADER, BM_GETCHECK, 0, 0) = bf_checked;

      for k:= IDC_NAME to IDC_ALTERNATE do
        if SendDlgItemMsg(k, BM_GETCHECK, 0, 0) = BF_CHECKED then
          drugDisp:= k - IDC_NAME;

      separators:= SendDlgItemMsg(IDC_COLUMNSEP, CB_GETITEMDATA, colSep^.GetSelIndex, 0);
    end;

    {- all parameters are valid, now check to see if widths make sense }
    if ret then
    begin
      if (MICWidth + interpWidth + micSpace) > isoWidth then
      begin
        ret:= false;
        ErrorMsg(HWindow, SR(IDS_ERROR, p2, sizeof(p2)-1),
                          SR(IDS_MICSPACEERR, pstr, sizeof(pstr)-1));
      end;

      if ret and ((isoWidth * isoPerSect) + drugWidth > sectWidth) then
      begin
        ret:= YesNoMsg(HWindow, SR(IDS_WARNING, p2, sizeof(p2)-1),
                                SR(IDS_TOTALWIDTHERR, pstr, sizeof(pstr)-1));
      end;
    end;
  end;

  if ret then
    cfg^:= tCfg;
  CanClose:= ret;
end;

procedure TSectIsoCompEdit.WMDrawItem(var msg: TMessage);
begin
  grid^.DrawItem(msg);
end;

procedure TSectIsoCompEdit.WMCharToItem(var msg: TMessage);
begin
  grid^.CharToItem(msg);
end;

procedure TSectIsoCompEdit.WMMeasureItem(var msg: TMessage);
begin
  grid^.MeasureItem(msg);
end;

function SectIsoCompDelete(action: integer; aRptSeq, aSectSeq: longint): boolean;
const
  db  : PDBFile = nil;
var
  ret   : boolean;
begin
  ret:= true;
  if (action and ACTION_OPEN <> 0) then
  begin
    db:= New(PDBFile, Init(DBRptIsoCmpFile, '', dbOpenNormal));
    if db = nil then
    begin
      ShowError(nil, IDS_CANTOPENICMP, nil, dbLastOpenError, MOD_RPTGEN, 3);
      ret:= false;
    end;
  end;

  if ret and (action and ACTION_DELETE <> 0) and (db <> nil) then
  begin
    with db^ do
    begin
      dbc^.GetSeq(dbr, aSectSeq);
      if dbc^.dbErrorNum = 0 then
        dbc^.DeleteRec(dbr);
    end;
  end;

  if ret and (action and ACTION_CLOSE <> 0) then
  begin
    MSDisposeObj(db);
  end;

  SectIsoCompDelete:= ret;
end;

{----------------------------[ Regsitered initialization function ]--}

function SectIsoCompInit(aParent: PWindowsObject; r: TRect; origin: TPoint;
                      aZoom: real; page: TRptPage; aFont: TRptFont): PBoxWin;
begin
  SectIsoCompInit:= New(PSectIsoComp, Init(aParent, sectionName, r, origin, azoom, page, aFont));
end;


BEGIN
  sections^.RegisterSection(SectIsoCompInit, SectIsoCompDelete, SR(IDS_ISOCompressed,
                            sectionName, sizeof(sectionName)), SectIsoCompID);
END.

{- rcf }
