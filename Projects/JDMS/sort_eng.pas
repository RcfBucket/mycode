{----------------------------------------------------------------------------}
{  Module Name : Sort_Eng.PAS                                                }
{  Programmer  : Steven Mercier                                              }
{  Date Created: June, 8, 1998                                               }
{  Requirement : S3-28 [ref. b]                                              }
{                                                                            }
{  Purpose - This module is the sort engine for DMS reports. See S3-28       }
{    design document [ref. b] for background and design information. See     }
{    module PatReps for example of its use.                                  }
{                                                                            }
{  Assumptions -                                                             }
{    a. Number of records to sort is limited by MaxCollectionSize and        }
{       available system memory.                                             }
{    b. Because this unit operates directly on a number of data types,       }
{       their definition must not have changed without a coresponding        }
{       update to this unit, especially in the Compare function.             }
{                                                                            }
{  Referenced Documents                                                      }
{    a. Database Design Document, Revision 0                                 }
{    b. S3.28 Design Document, Revision 1                                    }
{    c. ObjectWindows Programming Guide                                      }
{                                                                            }
{  Revision History - This project is under version control, use it to view  }
{    revision history.                                                       }
{                                                                            }
{----------------------------------------------------------------------------}

unit Sort_Eng;

INTERFACE

uses
  Objects,
  Sort_Par;

type
  {--------------------------------------------------------------------------}
  { This type enumerates the set of data types that can be used as keys with
  { the sort engine. If a data type is redefined or a new type is added, the
  { code in this file must be updated.
  {
  { The identifiers map to data types as follows:
  {
  {   SEKT_INTL_DATE - this is a date stored in Btrieve format and having
  {     application defined limits and extensions described in ref a.
  {   SEKT_ZSTRING - this is a string stored as a null-terminated series of
  {     ASCII and double-byte characters as described in ref a.
  {   SEKT_LONGINT - this is a number stored as a Pascal Longint.
  {--------------------------------------------------------------------------}
  SEKeyType =
  (
    SEKT_INTL_DATE,
    SEKT_ZSTRING,
    SEKT_LONGINT
  );

const
  {--------------------------------------------------------------------------}
  { This constant defines the set of key types enumerated above. This set
  { must be updated when types are added to SEKeyType so that it defines
  { the entire set.
  {--------------------------------------------------------------------------}
  SEKeySet = [ SEKT_INTL_DATE..SEKT_LONGINT ];


type
  {--------------------------------------------------------------------------}
  { Implementation Object - do not use externally
  {
  { This object holds elements to be sorted.
  {--------------------------------------------------------------------------}
  PSortEngine = ^SortEngine;
  TSEList = object( TSortedCollection )
    private
    pParentEngine: PSortEngine;
    function Compare( pKey1, pKey2: Pointer ): integer; virtual;
    constructor Init( input_pParentEngine: PSortEngine );
  end;


  {--------------------------------------------------------------------------}
  { This type defines the sort engine.
  {--------------------------------------------------------------------------}
  SortEngine = object

    { public - the object's interface }
    constructor Init( input_Parameters: SortEngineParameters;
      input_MaxDataSize: Word );
    procedure Input( index: longint );
    procedure Sort;
    function Output( var index: longint ): boolean;
    destructor Done;

    { overrides }
    function GetItem( KeyNumber: integer; pItem: Pointer;
      MaxSize: Word ): boolean; virtual;
    function DataTypeOf( KeyNumber: integer ): SEKeyType; virtual;

    private
    ListOfElements: TSEList;
    Parameters: SortEngineParameters;
    isInputMode: boolean;    { true when engine is in input mode }
    outputIndex: integer;    { index to element in pIsos used by Output }
    MaxDataSize: Word;
    pDataBuffer: Pointer;

  end;

IMPLEMENTATION

uses
  Strings,
  IntlLib,
  DateLib,
  WinProcs, { only used in Assert }
  WinTypes; { only used in Assert }


{--------------------------------------------------------------------------}
{ This procedure handles assertions for this unit. If an assertion is
{ false, it halts the program. Otherwise, it does nothing.
{--------------------------------------------------------------------------}

procedure Assert( flag: boolean; localErrorNumber: integer );
var
    Message: array [0..100] of char;
    NumberStr: array [0..20] of char;
begin
  if not flag then
  begin
    StrCopy( Message, 'SORTENG.PAS #' );
    Str( localErrorNumber, NumberStr );
    StrCat( Message, NumberStr );
    MessageBox( 0, Message, 'Error', MB_OK );
    Halt;
  end;
end;



                        {-------------}
                        { SortElement }
                        {-------------}



{----------------------------------------------------------------------------}
{ This object holds an index and its associated sort data for use with a
{ list collection.
{----------------------------------------------------------------------------}
type
  PSortElement = ^TSortElement;
  TSortElement = object( TObject )
    Index: longint;
    pData: Pointer;
    DataSize: Word;
    constructor Init( input_Index: longint; input_pData: Pointer;
      input_DataSize: Word );
    destructor Done; virtual;
  end;



{----------------------------------------------------------------------------}
{ The constructor creates a sort element containing an index
{ and a copy of its sort data.
{----------------------------------------------------------------------------}

constructor TSortElement.Init( input_Index: longint;
  input_pData: Pointer; input_DataSize: Word );
const
  MAX_ALLOCATION_SIZE = 65521; {or 65528 depending on document}
begin

  { Initialize the base class }
  if not inherited Init then
    Fail;

  { Store the data }
  Assert( input_DataSize <= MAX_ALLOCATION_SIZE, 1 );
  DataSize := input_DataSize;
  Index := input_Index;
  if input_DataSize > 0 then
  begin
    Assert( input_pData <> nil, 2 );
    GetMem( pData, DataSize );
    Move( input_pData^, pData^, DataSize );
  end
  else
  begin
    pData := nil;
  end;
end;



{----------------------------------------------------------------------------}
{ The destructor deallocates the data buffer.
{----------------------------------------------------------------------------}

destructor TSortElement.Done;
begin
  if DataSize > 0 then
    FreeMem( pData, DataSize );
  inherited Done;
end;



                        {-------------}
                        { SortEngine  }
                        {-------------}



{----------------------------------------------------------------------------}
{ The constructor inputs the desired sorting parameteres and sets up the
{ engine to receive input. Construction places the engine in the input mode.
{
{ input_MaxDataSize - size of internal data beffer in bytes. Derived class
{   must determine size by multiplying largest field size by SE_MAXLEVELS.
{----------------------------------------------------------------------------}

constructor SortEngine.Init( input_Parameters: SortEngineParameters;
  input_MaxDataSize: Word );

begin { Init }

  Assert( input_Parameters.isValid, 3 );
  ListOfElements.Init( @Self );
  ListOfElements.Duplicates := true;
  Parameters := input_Parameters;
  isInputMode := true;

  { Set up buffer }
  MaxDataSize := input_MaxDataSize;
  GetMem( pDataBuffer, MaxDataSize );

end;



{----------------------------------------------------------------------------}
{ The destructor cleans up.
{----------------------------------------------------------------------------}

destructor SortEngine.Done;
begin
  if MaxDataSize > 0 then
  begin
    Assert( pDataBuffer <> nil, 4 );
    FreeMem( pDataBuffer, MaxDataSize );
  end;
  ListOfElements.Done
end;



{----------------------------------------------------------------------------}
{ This procedure sorts the input data and switches engine to output mode.
{ Note, in current implementation, the data is allready sorted. This
{ procedure must be called once, after the calls to Input, but before the
{ calls to Output.
{----------------------------------------------------------------------------}

procedure SortEngine.Sort;
begin
  Assert( isInputMode, 5 );
  isInputMode := false;
  outputIndex := 0;
end;



{----------------------------------------------------------------------------}
{ This function indicates the data type of a user defined key.
{----------------------------------------------------------------------------}

function SortEngine.DataTypeOf( KeyNumber: integer ): SEKeyType;
begin
  Abstract;
end;



{----------------------------------------------------------------------------}
{ This function copies an key field into a buffer. It returns true is the key
{ number is valid and it was successfully copied. This function must be
{ overriden.
{
{ Input:
{   KeyNumber - a user defined key field number
{   pItem - pointer to destination buffer
{   MaxSize - size of destination buffer in bytes
{ Output:
{   ActualSize - the actual number of bytes copied, if successful
{
{----------------------------------------------------------------------------}

function SortEngine.GetItem( KeyNumber: integer; pItem: Pointer;
  MaxSize: Word ): boolean;
begin
  Abstract;
end;



{----------------------------------------------------------------------------}
{ This procedure inserts a sort item into the engine. Call this function
{ while the engine is in the input mode. Call as many times as required to
{ input all items. The engine stores a complete copy of the data so the
{ caller need not preserve the input data after the call.
{
{ Inputs:
{   index - a unique user defined identification number for this input item
{   pData - a pointer to the data needed to sort the item, see below.
{   DataSize - the size of the data in bytes, must be zero or more
{
{ All input data must be related to the parameters passed to the constructor
{ as follows:
{
{ pData points to a series of memory variables packed end-to-end. There is
{ one variable per level as defined by the NumberOfLevels parameter. A
{ variable's type is defined by its coresponding DataType parameter.
{ Variables of dynamic length (such as PChar strings) are permitted as
{ long as there is a way to determine their size.
{
{ Each call to Input must pass a unique index. Otherwise, the sorted
{ indexes (from the Output function) would be useless.
{
{ If the data or the stated size does not fit the parameters
{
{ pData may be nil if DataSize is zero and no parameters were specified.
{----------------------------------------------------------------------------}

procedure SortEngine.Input( index: longint );

  {--------------------------------------------------------------------------}
  { This function returns the size of the item. An item's size is based on
  { its type and value.
  {--------------------------------------------------------------------------}
  function GetItemSize( pItem: Pointer; ItemType: SEKeyType ): Word;
  begin
    case ItemType of
      SEKT_INTL_DATE :
        GetItemSize := SizeOf( TIntlDate );
      SEKT_ZSTRING :
        GetItemSize := StrLen( PChar( pItem ) ) + 1;
      SEKT_LONGINT :
        GetItemSize := SizeOf( Longint )
    else
      Assert( FALSE, 6 );
    end;
  end;

  {--------------------------------------------------------------------------}
  { This procedure gathers data needed to sort indexes based on the sort
  { parameters. The data format is that required by the sorter's input method.
  {
  { Input:
  {   pData - pointer to a buffer of BUFFER_SIZE
  { Output:
  {   pData - pointer to data
  {   DataSize - size of data in bytes
  {--------------------------------------------------------------------------}
  procedure GatherData( pData: Pointer; var DataSize: Word );
  var
    pItem: Pointer;
    level,
    RemainingSize,
    ItemSize: Word;
    ok: boolean;
  begin
    {---- Initialize loop variables }
    DataSize := 0;
    pItem := pData;
    level := 1;
    RemainingSize := MaxDataSize;
    with Parameters do
      while level <= NumberOfLevels do
        with Levels[ Level ] do
        begin
          ok := GetItem( KeyNumber, pItem, RemainingSize );
          Assert( ok, 7 );
          ItemSize := GetItemSize( pItem, DataTypeOf( KeyNumber ) );
          DataSize := DataSize + ItemSize;
          Assert( DataSize <= MaxDataSize, 8 );
          RemainingSize := MaxDataSize - DataSize;
          pItem := PChar( pItem ) + ItemSize;
          Inc( Level );
        end;
  end;

  {--------------------------------------------------------------------------}
  { This function examines the given data to determine its required data
  { size, in bytes, considering the engine parameters. This function walks
  { through the data to add up the fixed and variable lenth data elements
  { and returns the total.
  {--------------------------------------------------------------------------}
  function RequiredDataSize( pData: Pointer ): Word;
  var
    Level: integer;
    Size,
    TotalSize: Word;
    pItem: Pointer;
  begin
    TotalSize := 0;
    Level := 1;
    pItem := pData;
    with Parameters do
      while Level <= NumberOfLevels do
        with Levels[ Level ] do
        begin

          { Accumulate size }
          Size := GetItemSize( pItem, DataTypeOf( KeyNumber ) );
          TotalSize := TotalSize + Size;

          { advance to next level }
          pItem := PChar( pItem ) + Size;
          Inc( Level );

        end;
    RequiredDataSize := TotalSize;
  end;

var
  pElement: PSortElement;
  DataSize: Word;

begin { Input }

  { Insure engine is in the input mode }
  Assert( isInputMode, 9 );

  GatherData( pDataBuffer, DataSize );

  { Insure data size fits the given data. }
  Assert( DataSize = RequiredDataSize( pDataBuffer ), 10 );

  { Construct an element based on the data and insert it into the list. }
  pElement := New( PSortElement, Init( index, pDataBuffer, DataSize ) );

  ListOfElements.Insert( pElement );

end;



{----------------------------------------------------------------------------}
{ Each call to this function extracts the next index in the programmed
{ sort order. The function returns true when it returns an index, false
{ after all have been removed.
{----------------------------------------------------------------------------}

function SortEngine.Output( var Index: longint ): boolean;
var
  pElement: PSortElement;
begin
  Assert( not isInputMode, 11 );
  Assert( outputIndex >= 0, 12 );

  {---- Return next element based on the output index, return false if done. }
  if outputIndex < ListOfElements.Count then
  begin
    pElement := ListOfElements.At( outputIndex );
    Index := pElement^.Index;
    Output := true;
    Inc( outputIndex );
  end
  else
    Output := false;
end;



                        {-------------}
                        {   TSEList   }
                        {-------------}



{----------------------------------------------------------------------------}
{ This constructor initializes the sort engine list.
{----------------------------------------------------------------------------}

constructor TSEList.Init( input_pParentEngine: PSortEngine );

const
  {--------------------------------------------------------------------------}
  { The following constants tune the list. These values are passed to the
  { list's base class constructor which is a TCollection. See
  { TCollection.Init in ref. c for details on setting these values.
  {--------------------------------------------------------------------------}
  INITIAL_ELEMENT_LIMIT = 1000;
  GROW_SIZE = 1000;

begin
  inherited Init( INITIAL_ELEMENT_LIMIT, GROW_SIZE );
  pParentEngine := input_pParentEngine;
end;



{----------------------------------------------------------------------------}
{ This override of TSortedCollection's Compare function provides the logic
{ to compare data from two list elements. This function compares two data
{ sets and returns the codes specified for TSortedCollection's Compare
{ method, i.e., a -1 if value1 < value2, 0 if value1 = value2, or 1 if
{ value1 > value2.
{----------------------------------------------------------------------------}

function TSEList.Compare( pKey1, pKey2: Pointer ): integer;

  {--------------------------------------------------------------------------}
  { This function compares two integers and returns the codes specified for
  { TSortedCollection's Compare method, i.e., a -1 if value1 < value2, 0 if
  { value1 = value2, or 1 if value1 > value2.
  {--------------------------------------------------------------------------}
  function CompareLongint( value1, value2: Longint ): integer;
  begin
    if value1 < value2 then
      CompareLongint := -1
    else if value1 = value2 then
      CompareLongint := 0
    else
      CompareLongint := 1;
  end;

var
  Level,
  Result: integer;
  pItem1,
  pItem2: Pointer;
  ItemSize1,
  ItemSize2: word;

type
  PIntlDate = ^TIntlDate;
  PLongint = ^Longint;

begin { Compare }

  { Set default result in case no levels are specified. }
  Result := 0;

  { Beginning with level 1, search levels for first non-zero result. If
    found, update result. }
  Level := 1;
  pItem1 := PSortElement( pKey1 )^.pData;
  pItem2 := PSortElement( pKey2 )^.pData;
  with pParentEngine^.Parameters do
    while ( Level <= NumberOfLevels ) and ( Result = 0 ) do
      with Levels[ Level ] do
      begin

        { compare items and note their size }
        case pParentEngine^.DataTypeOf( KeyNumber ) of
          SEKT_INTL_DATE :
          begin
            Result := CompareDates( PIntlDate( pItem1 )^,
              PIntlDate( pItem2 )^ );
            ItemSize1 := SizeOf( TIntlDate );
            ItemSize2 := SizeOf( TIntlDate );
          end;
          SEKT_ZSTRING :
          begin
            Result := StrComp( PChar( pItem1), PChar( pItem2 ) ); {will need to replace w/double-byte function }
            ItemSize1 := StrLen( PChar( pItem1 ) ) + 1;
            ItemSize2 := StrLen( PChar( pItem2 ) ) + 1;
          end;
          SEKT_LONGINT :
          begin
            Result := CompareLongint( PLongint( pItem1 )^,
              PLongint( pItem2 )^ );
            ItemSize1 := SizeOf( Longint );
            ItemSize2 := SizeOf( Longint );
          end;
        else
          Assert( FALSE, 13 );
        end;

        { handle the reverse sort mode }
        if Reverse then
          Result := -Result;

        { advance to next level }
        pItem1 := PChar( pItem1 ) + ItemSize1;
        pItem2 := PChar( pItem2 ) + ItemSize2;
        Inc( Level );
      end;

  { Return result. }
  Compare := Result;
end;


END.