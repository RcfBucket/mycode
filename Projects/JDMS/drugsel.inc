(****************************************************************************


drugsel.inc

produced by Borland Resource Workshop


*****************************************************************************)

const
	DLG_DRUG_SELECTION	=	$71C0;
	DLG_EDIT_SELECTION	=	$71C1;
	DLG_SAVE_SELECTION	=	$71C2;

	IDC_ADD	=	100;
	IDC_EDIT	=	102;
	IDC_DELETE	=	106;
	IDC_SAVE	=	104;
	IDC_CLEAR	=	105;

	IDC_COUNT	=	200;
	IDC_LIST	=	201;

	IDC_CODE	=	201;
	IDC_DESC	=	202;

	IDC_LBL1	=	501;
	IDC_LBL2	=	502;

 IDS_ADD              = $71C1;
 IDS_EDIT             = $71C2;
 IDS_DELETE           = $71C3;
 IDS_CANNOT_DELETE    = $71C4;
 IDS_IN_USE           = $71C5;
 IDS_INITERR1         = $71C6;
 IDS_TOO_MANY_SEL     = $71C7;
