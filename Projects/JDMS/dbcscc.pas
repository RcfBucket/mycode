Unit DBCSCC;

Interface

Uses
	Strings;


{Allowable convert types = (PC, 78, 83, 90, S78, S83, S90)
	 where
		 PC = PCCode
		 78 = 1978 JIS
		 83 = 1983 JIS
		 90 = 1990 JIS
		S78 = 1978 ShiftJIS
		S83 = 1983 ShiftJIS
		S90 = 1990 ShiftJIS
}

function DBCSCC_ConvertDBCSCode(InputType, OutPutType: PChar;Code: longint): longint;

Implementation

const
  ConversionTable: array[1..60, 1..4] of longint =
			{PC}   {'78}  {'83}  {'90}
    (($88B1, $88B1, $E9CB, $E9CB),
     ($E9CB, $E9CB, $88B1, $88B1),
     ($89A7, $89A7, $E9F2, $E9F2),
     ($E9F2, $E9F2, $89A7, $89A7),
     ($8A61, $8A61, $E579, $E579),
     ($E597, $E597, $8A61, $8161),
     ($8A68, $8A68, $9D98, $9D98),
     ($9D98, $9D98, $8A68, $8A68),
     ($8A96, $8A96, $E27D, $E27D),
     ($E27D, $E27D, $8A96, $8A96),
     ($8AC1, $8AC1, $9FF3, $9FF3),
     ($9FF3, $9FF3, $8AC1, $8AC1),
     ($8AD0, $8AD0, $E67C, $E67C),
     ($E67C, $E67C, $8AD0, $8AD0),
     ($8C7A, $8C7A, $E8F2, $E8F2),
     ($E8F2, $E8F2, $8C7A, $8C7A),
     ($8D7B, $8D7B, $E1E6, $E1E6),
     ($E1E6, $E1E6, $8D7B, $8D7B),
     ($8EC7, $8EC7, $E541, $E541),
     ($E541, $E541, $8EC7, $8EC7),
     ($9078, $9078, $E8D5, $E8D5),
     ($E8D5, $E8D5, $9078, $9078),
     ($9147, $9147, $E6CB, $E6CB),
     ($E6CB, $E6CB, $9147, $9147),
     ($92D9, $92D9, $9AE2, $9AE2),
     ($9AE2, $9AE2, $92D9, $92D9),
     ($9376, $9376, $E1E8, $E1E8),
     ($E1E8, $E1E8, $9376, $9376),
     ($938E, $938E, $9E8D, $9E8D),
     ($9E8D, $9E8D, $938E, $938E),
     ($9393, $9393, $9FB7, $9FB7),
     ($9FB7, $9FB7, $9393, $9393),
     ($93F4, $93F4, $E78E, $E78E),
     ($E78E, $E78E, $93F4, $93F4),
     ($9488, $9488, $E5A2, $E5A2),
     ($E5A2, $E5A2, $9488, $9488),
     ($954F, $954F, $9E77, $9E77),
     ($9E77, $9E77, $954F, $954F),
     ($9699, $9699, $98D4, $98D4),
     ($98D4, $98D4, $9699, $9699),
     ($96F7, $96F7, $E54D, $E54D),
     ($E54D, $E54D, $96F7, $96F7),
     ($9855, $9855, $E2C4, $E2C4),
     ($E2C4, $E2C4, $9855, $9855),
     ($8BC4, $8BC4, $EA9F, $EA9F),
     ($EA9F, $0000, $8BC4, $8BC4),
     ($968A, $968A, $EAA0, $EAA0),
     ($EAA0, $0000, $968A, $968A),
     ($9779, $9779, $EAA1, $EAA1),
     ($EAA1, $0000, $9779, $9779),
     ($E0F4, $E0F4, $EAA2, $EAA2),
     ($EAA2, $0000, $E0F4, $E0F4),
     ($FA54, $0000, $81CA, $81CA),
     ($FA5B, $0000, $81E6, $81E6),
     ($E086, $E086, $E086, $EAA4),
     ($EAA4, $0000, $0000, $E086),
     ($8D56, $8D56, $8D56, $0000),
     ($FAD0, $0000, $0000, $8D56),
     ($EAA3, $0000, $0000, $EAA3),
     ($0000, $0000, $0000, $0000));



function ConvertPCCodeToSJIS(PCCode: longint;AgeType : integer): longint;
var
  AgeIdx, CodeIdx: integer;
	ConvertedCode: longint;

begin
	CodeIdx := 1;
	AgeIdx := AgeType * 5 + 1;
	ConvertedCode := PCCode;
  while (ConversionTable[CodeIdx, 1] <> 0) and
        (ConversionTable[CodeIdx, 1] <> PCCode) do
		Inc(CodeIdx);
  if ConversionTable[CodeIdx, 1] = PCCode then
    ConvertedCode := ConversionTable[CodeIdx, AgeIdx];
	ConvertPCCodeToSJIS := ConvertedCode;
end;

function ConvertSJISToPCCode(SJIS: longint;AgeType: integer): longint;
var
  AgeIdx, CodeIdx: integer;
	ConvertedCode: longint;

begin
	CodeIdx := 1;
	AgeIdx := AgeType * 5 + 1;
	ConvertedCode := SJIS;
  while (ConversionTable[CodeIdx, 1] <> 0) and
        (ConversionTable[CodeIdx, AgeIdx] <> SJIS) do
		Inc(CodeIdx);
  if ConversionTable[CodeIdx, AgeIdx] = SJIS then
    ConvertedCode := ConversionTable[CodeIdx, 1];
  ConvertSJISToPCCode := ConvertedCode;
end;

function ConvertSJISToJIS(SJIS: longint): longint;
var
  sh, sl: longint;

begin
	if SJIS = 0 then
	begin
		ConvertSJISToJIS := 0;
    Exit;
	end;
	sh := SJIS mod 256;
	sl := SJIS div 256;
	if sh <= $9F then
		if sl < $9F then
			sh := sh * 2 - $E1
		else
			sh := sh * 2 - $E0
	else
		if sl < $9F then
			sh := sh * 2 - $161
		else
			sh := sh * 2 - $160;
	if sl < $7F then
		sl := sl - $1F
	else
		if sl < $9F then
			sl := sl - $20
		else
			sl := sl - $7E;
  ConvertSJISToJIS := sh * $100 + sl;
end;

function ConvertJISToSJIS(JIS: longint): longint;
var
  jh, jl: longint;

begin
	jh := JIS mod 256;
	jl := JIS div 256;
	if jh / 2 = 1 then
		if jl < $60 then
			jl := jl + $1F
		else
			jl := jl + $20
	else
		if jh < $5F then
			jh := (jh + $E1) mod 2
		else
			jh := (jh + $161) mod 2;
	ConvertJISToSJIS := jh * 256 + jl;
end;

function ConvertType(AType: PChar): char;
var
	ConvertedType: char;

begin
  if StrComp(AType, 'PC') = 0 then
		ConvertedType := 'A'
  else
    if StrComp(AType, '78') = 0 then
			ConvertedType := 'B'
    else
      if StrComp(AType, '83') = 0 then
				ConvertedType := 'C'
      else
        if StrComp(AType, '90') = 0 then
					ConvertedType := 'D'
        else
          if StrComp(AType, 'S78') = 0 then
						ConvertedType := 'E'
          else
            if StrComp(AType, 'S83') = 0 then
							ConvertedType := 'F'
            else
              if StrComp(AType, 'S90') = 0 then
								ConvertedType := 'G';
	ConvertType := ConvertedType;
end;

function DBCSCC_ConvertDBCSCode(InputType, OutputType: PChar;Code: longint): longint;
var
  InType, OutType: char;
	ConvertedCode: longint;

begin
	InType := ConvertType(InputType);
  OutType := ConvertType(OutputType);
	case InType of
		'A': case OutType of
            'B': ConvertedCode := ConvertSJISToJIS(ConvertPCCodeToSJIS(Code, 1));
            'C': ConvertedCode := ConvertSJISToJIS(ConvertPCCodeToSJIS(Code, 2));
            'D': ConvertedCode := ConvertSJISToJIS(ConvertPCCodeToSJIS(Code, 3));
            'E': ConvertedCode := ConvertPCCodeToSJIS(Code, 1);
            'F': ConvertedCode := ConvertPCCodeToSJIS(Code, 2);
            'G': ConvertedCode := ConvertPCCodeToSJIS(Code, 3);
					end;
		'B': case OutType of
            'A': ConvertedCode := ConvertSJISToPCCode(ConvertJISToSJIS(Code), 1);
						'E': ConvertedCode := ConvertJISToSJIS(Code);
    		  end;
		'C': case OutType of
            'A': ConvertedCode := ConvertSJISToPCCode(ConvertJISToSJIS(Code), 2);
						'F': ConvertedCode := ConvertJISToSJIS(Code);
					end;
		'D': case OutType of
            'A': ConvertedCode := ConvertSJISToPCCode(ConvertJISToSJIS(Code), 3);
						'G': ConvertedCode := ConvertJISToSJIS(Code);
					end;
	 	'E': case OutType of
            'A': ConvertedCode := ConvertSJISToPCCode(Code, 1);
						'B': ConvertedCode := ConvertSJISToJIS(Code);
					end;
	 	'F': case OutType of
            'A': ConvertedCode := ConvertSJISToPCCode(Code, 2);
						'C': ConvertedCode := ConvertSJISToJIS(Code);
					end;
	 	'G': case OutType of
            'A': ConvertedCode := ConvertSJISToPCCode(Code, 3);
						'D': ConvertedCode := ConvertSJISToJIS(Code);
					end;
  end;
	DBCSCC_ConvertDBCSCode := ConvertedCode;
end;

Begin
End.
