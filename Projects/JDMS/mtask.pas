unit MTASK;

{$F+}

{- This module exports two routines.

   OKToRun - This routine should be called before your app performs the
             Init/Run/Done sequence. If the function returns true, then it is
             OK to proceed, otherwise, your app should end.

   GetModuleName - This routine should be used to get the name to pass to your
                   application.Init() function and it must also be used as the
                   class name of your main window. (Use GetClassName to set the
                   class name for your app.

   IsDMSProgRunning - Returns true if any dms app is currently running }

INTERFACE

function OKToRun(moduleID: word; allowMultiInstance: boolean): boolean;
function GetModuleName(moduleID: word): PChar;
function IsDMSProgRunning: boolean;

const
  DMSMainProgRetCode  = $0ABADABA;

IMPLEMENTATION

uses
  APITools,
  WinProcs,
  WinTypes,
  UserMsgs,
  DMString,
  MScan,
  Strings;

{$R MTASK.RES}
{$I MTASK.INC}

const
  MOD_NAME_WA       = 'WA Main';                    {}
  MOD_NAME_DMSMAIN  = 'Dade DMS';
  MOD_NAME_RPTGEN   = 'Report Editor';              {}
  MOD_NAME_TWOWAY   = 'TwoWay Interface';
  MOD_NAME_PRTMAIN  = 'DMS Reports';                {}
  MOD_NAME_FILMAIN  = 'File Maintenance';
  MOD_NAME_EDSCHEMA = 'DMS Database Field Editor';  {}
  MOD_NAME_CUSTINTR = 'Therapy Breakpoint Editor';  {}
  MOD_NAME_CONVERT  = 'DMS Conversion';
  MOD_NAME_UNKNOWN  = 'unknown';

type
  {- record to hold contents of a found window (from EnumWinCallback) }
  PFindRec = ^TFindRec;
  TFindRec = record
    hw    : HWnd;
    name  : array[0..64] of char;
  end;

function EnumWinCallback(hw: HWnd; lParam: longint): bool; export;
{- lParam is assumed to be a pointer to a TFindRec buffer. where the name field
   is the name of the app to find. If the app is found, then the hw field of
   findBuf will be set to it's hwindow }
var
  findRec   : PFindRec;
  pstr      : array[0..64] of char;
  ret       : longint;
begin
  findRec:= PFindRec(lParam);
  {- send message to window and get it's name }
  StrCopy(pstr, '');
  ret:= SendMessage(hw, WM_MAINPROGQUERY, sizeof(pstr)-1, longint(@pstr));
  if ret = DMSMainProgRetCode then
  begin
    {- return non zero to continue search }
    if StrComp(pstr, findRec^.name) = 0 then
    begin
      findRec^.hw:= hw;
      EnumWinCallBack:= bool(0);  {- stop }
    end
    else
      EnumWinCallBack:= bool(1);  {- continue }
  end
  else
    EnumWinCallback:= bool(1);  {- return non-zero to tell windows to continue processing}
end;

function LocateDMSModule(modName: PChar): HWnd;
{- attempt to locate a running DMS program that matches the module name.
   Return 0 if not found or return the HWindow if running }
var
  enumInst    : TFarProc;
  findBuf     : TFindRec;
begin
  FillChar(findBuf, sizeof(findBuf), 0);
  StrLCopy(findBuf.name, modName, sizeof(findBuf.name)-1);

  enumInst:= MakeProcInstance(TFarProc(@EnumWinCallback), hInstance);
  EnumWindows(eNumInst, longint(@findBuf));
  FreeProcInstance(enumInst);
  LocateDMSModule:= findBuf.hw; {- if no window was found then hw will be 0 }
end;

function GetModuleName(moduleID: word): PChar;
{- return a module name for an applications Init function. All dms main program
   should get their names from this function }
begin
  if moduleID = MOD_WA then {- MOD_WA is out of case statement range }
    GetModuleName:= MOD_NAME_WA
  else
  begin
    case moduleID of
      MOD_DMSMAIN  : GetModuleName:= MOD_NAME_DMSMAIN;
      MOD_RPTGEN   : GetModuleName:= MOD_NAME_RPTGEN;
      MOD_TWOWAY   : GetModuleName:= MOD_NAME_TWOWAY;
      MOD_PRTMAIN  : GetModuleName:= MOD_NAME_PRTMAIN;
      MOD_FILMAIN  : GetModuleName:= MOD_NAME_FILMAIN;
      MOD_EDSCHEMA : GetModuleName:= MOD_NAME_EDSCHEMA;
      MOD_CUSTINTR : GetModuleName:= MOD_NAME_CUSTINTR;
      MOD_CONVERT  : GetModuleName:= MOD_NAME_CUSTINTR;
    else
      GetModuleName:= MOD_NAME_UNKNOWN;
    end;
  end;
end;

function ModuleActive(moduleID: longint; activate: boolean): boolean;
var
  modHW      : HWnd;
begin
  modHW:= LocateDMSModule(GetModuleName(moduleID));

  if activate and (modHW <> 0) and IsIconic(modHW) then
  begin
    ShowWindow(modHW, SW_RESTORE);
    SetFocus(modHW);
  end;
  ModuleActive:= (modHW <> 0);
end;

procedure FocusLastInstance(moduleID: word);
{- find an the last instance of the current program and bring to front }
var
  hw    : HWnd;
begin
  hw:= FindWindow(GetModuleName(moduleID), nil);
  if hw <> 0 then
  begin
    if IsIconic(hw) then
      ShowWindow(hw, SW_RESTORE);
    SetFocus(hw);
  end;
end;

function OKToRun(moduleID: word; allowMultiInstance: boolean): boolean;
{- return true if it is OK for the specified module ID to execute. This function
   should be called before an application executes }
var
  ret   : boolean;
  p2    : array[0..50] of char;
  p1    : array[0..200] of char;
begin
  ret:= true;   {- assume it ok }

  if allowMultiInstance or (hPrevInst = 0) then
  begin
    { MOD_WA is out of case statement range }
    if moduleID = MOD_WA then
      ret:= not (ModuleActive(MOD_FILMAIN , false) or
                 ModuleActive(MOD_EDSCHEMA, false) or
                 ModuleActive(MOD_CUSTINTR, false))
    else
    begin
      case moduleID of
        MOD_DMSMAIN  : ret:= not (ModuleActive(MOD_FILMAIN,  false) or
                                  ModuleActive(MOD_EDSCHEMA, false) or
                                  ModuleActive(MOD_CONVERT,  false) or
                                  ModuleActive(MOD_CUSTINTR, false));

        MOD_RPTGEN   : ret:= not (ModuleActive(MOD_FILMAIN,  false) or
                                  ModuleActive(MOD_CONVERT,  false) or
                                  ModuleActive(MOD_EDSCHEMA, false));

        MOD_TWOWAY   : ret:= not (ModuleActive(MOD_FILMAIN,  false) or
                                  ModuleActive(MOD_CONVERT,  false) or
                                  ModuleActive(MOD_EDSCHEMA, false) or
                                  ModuleActive(MOD_CUSTINTR, false));

        MOD_PRTMAIN  : ret:= not (ModuleActive(MOD_FILMAIN , false) or
                                  ModuleActive(MOD_CONVERT,  false) or
                                  ModuleActive(MOD_EDSCHEMA, false) or
                                  ModuleActive(MOD_CUSTINTR, false));

        MOD_FILMAIN  : ret:= not (ModuleActive(MOD_DMSMAIN,  false) or
                                  ModuleActive(MOD_CONVERT,  false) or
                                  ModuleActive(MOD_EDSCHEMA, false) or
                                  ModuleActive(MOD_CUSTINTR, false) or
                                  ModuleActive(MOD_RPTGEN,   false) or
                                  ModuleActive(MOD_WA,       false) or
                                  ModuleActive(MOD_TWOWAY,   false) or
                                  ModuleActive(MOD_PRTMAIN,  false));

        MOD_EDSCHEMA : ret:= not (ModuleActive(MOD_DMSMAIN,  false) or
                                  ModuleActive(MOD_CONVERT,  false) or
                                  ModuleActive(MOD_RPTGEN,   false) or
                                  ModuleActive(MOD_WA,       false) or
                                  ModuleActive(MOD_CUSTINTR, false) or
                                  ModuleActive(MOD_TWOWAY,   false) or
                                  ModuleActive(MOD_FILMAIN,  false) or
                                  ModuleActive(MOD_PRTMAIN,  false));

        MOD_CONVERT  : ret:= not (ModuleActive(MOD_DMSMAIN,  false) or
                                  ModuleActive(MOD_RPTGEN,   false) or
                                  ModuleActive(MOD_EDSCHEMA, false) or
                                  ModuleActive(MOD_WA,       false) or
                                  ModuleActive(MOD_CUSTINTR, false) or
                                  ModuleActive(MOD_TWOWAY,   false) or
                                  ModuleActive(MOD_FILMAIN,  false) or
                                  ModuleActive(MOD_PRTMAIN,  false));

        MOD_CUSTINTR : ret:= not (ModuleActive(MOD_PRTMAIN,  false) or
                                  ModuleActive(MOD_CONVERT,  false) or
                                  ModuleActive(MOD_DMSMAIN,  false) or
                                  ModuleActive(MOD_EDSCHEMA, false) or
                                  ModuleActive(MOD_WA,       false) or
                                  ModuleActive(MOD_FILMAIN,  false) or
                                  ModuleActive(MOD_TWOWAY,   false));
      end;
    end;
    if not ret then
    begin
      MessageBox(0, SR(IDS_CONFLICTS, p1, sizeof(p1)-1),
                    SR(IDS_ERROR, p2, sizeof(p2)-1),
                    MB_SYSTEMMODAL or MB_ICONEXCLAMATION);

    end;
  end
  else
  begin
      MessageBox(0, SR(IDS_ALREADYRUNNING, p1, sizeof(p1)-1),
                    SR(IDS_ERROR, p2, sizeof(p2)-1),
                    MB_SYSTEMMODAL or MB_ICONEXCLAMATION);
    ret:= false;
    FocusLastInstance(moduleID);
  end;

  OKtoRun:= ret;
end;

function IsDMSProgRunning: boolean;
{- this function returns true if any DMS programs are running }
begin
  IsDMSProgRunning:= ModuleActive(MOD_DMSMAIN, false) or
                     ModuleActive(MOD_RPTGEN, false) or
                     ModuleActive(MOD_TWOWAY, false) or
                     ModuleActive(MOD_PRTMAIN, false) or
                     ModuleActive(MOD_FILMAIN, false) or
                     ModuleActive(MOD_EDSCHEMA, false) or
                     ModuleActive(MOD_CUSTINTR, false) or
                     ModuleActive(MOD_CONVERT, false) or
                     ModuleActive(MOD_WA, false);
end;

END.

{- rcf }
