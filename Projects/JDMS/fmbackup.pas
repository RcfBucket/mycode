{----------------------------------------------------------------------------}
{  Module Name  : FMBACKUP.PAS                                               }
{  Programmer   : DWC                                                        }
{  Date Created : 05/11/95                                                   }
{                                                                            }
{  Purpose -                                                                 }
{  This module allows the user to backup data from the Nihongo databases.    }
{                                                                            }
{  Assumptions -                                                             }
{  None.                                                                     }
{                                                                            }
{  Initialization -                                                          }
{  None.                                                                     }
{                                                                            }
{  Revision History -                                                        }
{                                                                            }
{  Version  Date      Who    Did What                                        }
{  -------  --------  ------ ----------------------------------------------- }
{  1.00     05/11/95  DWC     Initial release.                               }
{                                                                            }
{----------------------------------------------------------------------------}

unit FMBACKUP;

INTERFACE

uses
  OWindows;

procedure FMFullBackup(aParent: PWindowsObject);
procedure FMIncrBackup(aParent: PWindowsObject);
procedure FMReportBackup(aParent: PWindowsObject);

IMPLEMENTATION

uses
  MScan,
  Objects,
  WinDos,
  WinTypes,
  WinProcs,
  ApiTools,
  DBTypes,
  DBLib,
  DBFile,
  DlgLib,
  Strings,
  fmconst,
  fmtypes,
  fmerror,
  userMsgs,
  DMString,
  INILib,
  fmglobal;

type

PDlgFBackup = ^TDlgFBackup;
TDlgFBackup = object(TDlgFMStatus)
  {}
  procedure   ProcessDlg(var msg: TMessage); virtual WM_FIRST + WM_FMPROCESS;
end;

PDlgIBackup = ^TDlgIBackup;
TDlgIBackup = object(TDlgFMStatus)
  {}
  procedure   ProcessDlg(var msg: TMessage); virtual WM_FIRST + WM_FMPROCESS;
end;

PDlgRBackup = ^TDlgRBackup;
TDlgRBackup = object(TDlgFMStatus)
  {}
  procedure   ProcessDlg(var msg: TMessage); virtual WM_FIRST + WM_FMPROCESS;
end;

{----------------------[CompareSchemas]-------------------------------------}
{---------------------------------------------------------------------------}
{                                                                           }
{---------------------------------------------------------------------------}
function CompareSchemas(StatDlg  : PDlgFMStatus;
                        fNList   : PCollection;
                        alocDir,
                        aRemDir,
                        anExt   : PChar): integer;
var
  retVal    : integer;
  dbLocal   : PDBReadDesc;
  dbRemote  : PDBReadDesc;
  i         : integer;
  fileENtry : PArchFile;
  oldMode   : Word;

begin
  oldMode := SetErrorMode(SEM_FAILCRITICALERRORS);
  retVal := continueJob;
  i := 0;

  statDlg^.Update;
  while (i <= fNList^.count -1) and
        (retVal = continueJob) and StatDlg^.CanContinue do
  begin
    fileEntry := fNList^.At(i);
    dbLocal:= New(PDBReadDesc, Init(fileEntry^.fileName, alocDir));
    if (dbLocal = nil) then
    begin
      retVal := fmErr_OpeningFile;
      StrCopy(fmErr_Msg,alocDir);
      StrCat(fmErr_Msg,fileEntry^.fileName);
    end;
    if retval = continueJob then
    begin
      statDlg^.Update;
      dbRemote:= New(PDBReadDesc, Init(fileEntry^.fileName,aRemDir));
      if (dbRemote = nil) then
      begin
        retVal := fmErr_OpeningFile;
        StrCopy(fmErr_Msg,aRemDir);
        StrCat(fmErr_Msg,fileEntry^.fileName);
      end;
      if retVal = continueJob then
        if not dbLocal^.CompareRecStruct(dbRemote) then
        begin
          retVal := fmErr_NoMatchSchema;
          StrCopy(fmErr_Msg,fileEntry^.fileName);
        end;
    end;

    MSDisposeObj(dbLocal);
    MSDisposeObj(dbRemote);
    inc(i);
    statDlg^.Update;
  end;

  SetErrorMode(oldMode);
  CompareSchemas := retVal;
end;

{----------------------[ClearArchiveFlags]----------------------------------}
{---------------------------------------------------------------------------}
{                                                                           }
{---------------------------------------------------------------------------}
procedure ClearArchiveFlags(dlg :PDlgFMStatus; fNList :PCollection; aDir :PChar);
var
  db          : PDBCopyFile;
  i           : integer;
  j           : longint;
  fileEntry   : PArchfile;
  errorNumber : integer;

begin
  dlg^.Update;
  i := 0;
  j := 0;
  dlg^.ResetPct(1);

  while (i <= fNList^.count -1) and dlg^.CanContinue do
  begin
    fileEntry := fNList^.At(i);
    db:= New(PDBCopyFile, Init(fileEntry^.fileName, aDir, DBOpenExclusive));
    if db^.dbc^.GetFirst(db^.dbr) then
      repeat
        if db^.dbr^.GetFlag(db_ArchivePos) then
        begin
          db^.dbr^.SetFlag(db_ArchivePos,false);
          if not db^.dbc^.UpdateRec(db^.dbr) then
            errorNumber := db^.dbc^.dbErrorNum;
        end;
        dlg^.Update;
        dlg^.CompPctLevel(j,totalRecords,1);
        inc(j);
      until (not db^.dbc^.GetNext(db^.dbr)) or not dlg^.CanContinue;
    MSDisposeObj(db);
    inc(i);
  end;

  dlg^.SetText('  ',IDC_PB1);
end;

{----------------------[TransferFiles]--------------------------------------}
{---------------------------------------------------------------------------}
{                                                                           }
{---------------------------------------------------------------------------}
function TransferFiles(dlg           : PDlgFMStatus;
                       fNList        : PCollection;
                       aSrcDir       : PChar;
                       var diskNum   : integer;
                       backupRec     : TBackupRec;
                       fullBackup    : boolean;
                       aTitle        : PChar) : integer;
var
  aDestDir : array[0..255] of char;
  pStr     : array[0..50] of char;
  retVal   : integer;

begin
  retVal := continueJob;

  dlg^.update;
  if dlg^.CanContinue then
  begin
    retVal := InsertDisk(dlg, diskNum, backupRec, aTitle);
    if (retVal = continueJob) and dlg^.CanContinue  then
    begin
      dlg^.update;

      dlg^.SetText(SR(IDS_PERDISK,  pStr, 50),IDC_PB1_TEXT);
      dlg^.SetText(SR(IDS_PERTOTAL, pStr, 50),IDC_PB2_TEXT);
      dlg^.ResetPct(1);
      dlg^.ResetPct(2);
      totalCt := 0;
      totalSpace := backupRec.numBytes;
      diskSizeCt := 0;

      StrCopy(aDestDir,'A:');
      if fullBackup and dlg^.CanContinue then
        retval := CopyFiles(dlg, fNList, aSrcDir, aDestDir, fileDescExt,
                            diskNum, backupRec, true, false, true, aTitle);
      if (retVal = continueJob) and dlg^.CanContinue then
        retVal := CopyFiles(dlg, fNList, aSrcDir, aDestDir, datFileExt,
                            diskNum, backupRec, true, false, true, aTitle);
    end;
  end;
  dlg^.update;

  TransferFiles := retVal;
end;

{---------------------------[CheckForIncremental]---------------------------}
{---------------------------------------------------------------------------}
{                                                                           }
{---------------------------------------------------------------------------}
function CheckForIncremental(dlg: PDlgFMStatus; fNList: PCollection; dbDir: PChar): integer;
var
  db            : PDBCopyFile;
  i             : integer;
  retVal        : integer;
  fileEntry     : PArchFile;

begin
  retVal := NoIncremental;
  i := 0;

  dlg^.Update;
  {- loop through each database file -}
  while ((i <= fNList^.count -1) and
         (retVal = NoIncremental) and dlg^.CanContinue) do
  begin
    fileEntry := fNList^.At(i);
    db:= New(PDBCopyFile, Init(fileEntry^.fileName, dbDir, DBOpenExclusive));
    if db^.dbc^.GetFirst(db^.dbr) then
      {- loop through each database record -}
      repeat
        dlg^.Update;
        if db^.dbr^.GetFlag(db_ArchivePos) then
        begin
          retVal := continueJob;
        end;
      until not((db^.dbc^.GetNext(db^.dbr)) and
                (retVal = NoIncremental) and dlg^.CanContinue);
    MSDisposeObj(db);
    inc(i);
    dlg^.Update;
  end;

  CheckForIncremental := retVal;
end;

{----------------------[BuildIncrementalDatabase]---------------------------}
{---------------------------------------------------------------------------}
{                                                                           }
{---------------------------------------------------------------------------}
function BuildIncrementalDatabase(dlg     :PDlgFMStatus;
                                  fNList  :PCollection;
                                  dbDir,
                                  workDir :PChar): integer;
var
  theCreator    : TDBCreator;
  db            : PDBCopyFile;
  arcDB         : PDBCopyFile;
  newDBD        : PDBReadWriteDesc;
  i             : integer;
  retVal        : integer;
  createNewFile : boolean;
  j             : longInt;
  fileEntry     : PArchFile;

begin
  retVal := continueJob;
  i := 0;
  j := 0;

  dlg^.ResetPct(1);
  dlg^.Update;
  {- loop through each database file -}
  while ((i <= fNList^.count -1) and
         (retVal = continueJob) and dlg^.CanContinue) do
  begin
    fileEntry := fNList^.At(i);
    createNewFile := true;
    arcDB := nil;
    db:= New(PDBCopyFile, Init(fileEntry^.fileName, dbDir, DBOpenExclusive));
    if db^.dbc^.GetFirst(db^.dbr) then
      {- loop through each database record -}
      repeat
        dlg^.Update;
        if db^.dbr^.GetFlag(db_ArchivePos) then
        begin
          if createNewFile then
          begin
            createNewFile := false;
            newDBD:= New(PDBReadWriteDesc, Init(fileEntry^.fileName,workDir));
            newDBD^.CopyDesc(db^.dbd);
            dlg^.Update;
            if not theCreator.CreateTable(newDBD) then
            begin
              retVal := fmErr_CreatingFile;
              StrCopy(fmErr_Msg,workDir);
              StrCat(fmErr_Msg,fileEntry^.fileName);
            end;
            arcDB:= New(PDBCopyFile, Init(fileEntry^.fileName, workDir, DBOpenExclusive));
            if (arcDB = nil) then
            begin
              retVal := fmErr_OpeningFile;
              StrCopy(fmErr_Msg,workDir);
              StrCat(fmErr_Msg,fileEntry^.fileName);
            end;
            MSDisposeObj(newDBD);
          end;
          (* Do not clear archive bits. *)
          (*db^.dbr^.SetFlag(db_ArchivePos,false);*)
          (*db^.dbc^.UpdateRec(db^.dbr);*)
          dlg^.Update;
          arcDB^.dbr^.CopyRecord(db^.dbr);
          arcDB^.dbr^.SetFlag(db_ArchivePos,false);
          arcDB^.dbc^.InsertRec(arcDB^.dbr);
        end;
        dlg^.CompPctLevel(j,totalRecords,1);
        inc(j);
      until not((db^.dbc^.GetNext(db^.dbr)) and (retVal = continueJob) and dlg^.CanContinue);
    MSDisposeObj(db);
    MSDisposeObj(arcDB);
    inc(i);
    dlg^.Update;
  end;

  dlg^.SetText('    ',IDC_PB1);
  BuildIncrementalDatabase := retVal;
end;

{----------------------[ComputeReportBytes]---------------------------------}
{---------------------------------------------------------------------------}
{                                                                           }
{---------------------------------------------------------------------------}
function ComputeReportBytes(dlg : PDlgFMStatus; fNList : PCollection; aDir :PChar; var estDiskNum : integer): longint;
var
  filePath   : array[0..maxPathLen] of char;
  dirInfo    : TSearchRec;
  temp       : longint;
  i          : integer;
  fileEntry  : PArchFile;
  totalBytes : longint;

begin
  dlg^.Update;

  totalBytes := 0;

  for i := 0 to fNList^.count-1 do
  begin
    fileEntry := fNList^.At(i);
    StrCopy(filePath, aDir);
    StrCat(filePath, fileEntry^.fileName);
    StrCat(filePath, '.DB*');
    FindFirst(filePath, faAnyFile - faDirectory - faVolumeID, dirInfo);
    while DosError = 0 do
    begin
      totalBytes := totalBytes + dirInfo.size;
      FindNext(dirInfo);
      dlg^.Update;
    end;
  end;

  if totalBytes <> 0 then
  begin
    estDiskNum := totalBytes div floppyBytes;
    temp := totalBytes mod floppyBytes;
    if temp > 0 then
      estDiskNum := estDiskNum + 1;
  end;

  ComputeReportBytes := totalBytes;
end;

{----------------------[TDlgFBackup.ProcessDlg]-----------------------------}
{---------------------------------------------------------------------------}
{                                                                           }
{---------------------------------------------------------------------------}
procedure TDlgFBackup.ProcessDlg(var msg: TMessage);
var
  diskNum      : integer;
  estDiskNum   : integer;
  dbDir        : array[0..maxPathLen] of char; { directory location of database files }
  backupRec    : TBackupRec;
  updateMsg    : array [0..255] of char;
  confirmMsg   : array [0..255] of char;
  aTitle       : array [0..255] of char;
  msgStr       : array [0..255] of char;
  retVal       : integer;
  totalBytes   : longint;
  fileNameList : PCollection;
  drive        : byte;
  verifyBackup : Boolean;
  vSet         : Boolean;

begin
  verifyBackup := INIVerifyBackup;
  diskNum := 0;
  estDiskNum := 0;
  retVal := continueJob;
  StrCopy(fmErr_Msg,'');

  SetText('    ',0);
  SetText('    ',IDC_TEXT1);
  SetText('    ',IDC_TEXT2);
  SetText('    ',IDC_PB1_TEXT);
  SetText('    ',IDC_PB1);
  SetText('    ',IDC_PB2_TEXT);
  SetText('    ',IDC_PB2);

  SetText(SR(IDS_BACKUPMAINMSG, updateMsg, 255),0);
  SetText(SR(IDS_BACKUPSET, updateMsg, 255),IDC_TEXT2);
  GetCurrentDir(dbDir, drive);

  fileNameList := New(PCollection, Init(20, 5));

  retVal := BuildArchiveFiles(@Self, fileNameList, dbDir, fileDescExt, backupAllFiles);
  if (retVal = continueJob) and CanContinue then
  begin
    SetText(SR(IDS_CLEARINGFLAGS, updateMsg, 255), IDC_TEXT2);
    ClearArchiveFlags(@Self, fileNameList, dbDir);
    totalBytes := ComputeTotalBytes(@Self, dbDir, estDiskNum);
    retVal := MakeCatalog(@Self, backupRec, totalBytes, estDiskNum, FullCatalog);
    SetText(SR(IDS_BACKUP, updateMsg, 255), IDC_TEXT1);
    SR(IDS_FMFB, aTitle, 255);
    if VerifyBackup then
    begin
      GetVerify(vSet);
      SetVerify(True);
    end;
    retVal := TransferFiles(@Self, fileNameList, dbDir, diskNum,
                            backupRec, true, aTitle);
    if  (retVal = continueJob) and canContinue then
    begin
      SetText('    ',IDC_TEXT1);
      SetText(SR(IDS_UPDATECATALOG, msgStr, 255),IDC_TEXT2);
      SetText('    ',IDC_PB1_TEXT);
      SetText('    ',IDC_PB1);
      SetText('    ',IDC_PB2_TEXT);
      SetText('    ',IDC_PB2);
      SR(IDS_FULLBACKUPCOMPLETE, msgStr, 255);
      UpdateDisk1(@Self, diskNum, backupRec, msgStr);
    end;
    if VerifyBackup then
      SetVerify(vSet);
  end;

  if (retVal <> continueJob) and (retVal <> cancelJob) then
  begin
    SR(IDS_FULLBACKUPTITLE, updateMsg, 255);
    FMShowError(@Self, updateMsg, retVal, fmErr_Msg);
  end;

  MSDisposeObj(fileNameList);
  EndDlg(IDCancel);
end;

{----------------------[FMFullBackup]---------------------------------------}
{---------------------------------------------------------------------------}
{                                                                           }
{---------------------------------------------------------------------------}
procedure FMFullBackup(aParent: PWindowsObject);
var
  dlgBackup    : PDlgFBackup;

begin

  dlgBackup:= New(PDlgFBackup, Init(aParent,true));
  application^.ExecDialog(dlgBackup);

end;

{----------------------[TDlgIBackup.ProcessDlg]-----------------------------}
{---------------------------------------------------------------------------}
{                                                                           }
{---------------------------------------------------------------------------}
procedure TDlgIBackup.ProcessDlg(var msg: TMessage);
var
  retVal       : integer;
  dbDir        : array[0..maxPathLen] of char; { directory location of database files }
  workDir      : array[0..maxPathLen] of char; { directory temporary database files }
  fullRec      : TBackupRec;
  backupRec    : TBackupRec;
  updateMsg    : array [0..255] of char;
  aTitle       : array [0..255] of char;
  msgStr       : array [0..255] of char;
  diskNum      : integer;
  estDiskNum   : integer;
  totalBytes   : longint;
  fileNameList : PCollection;
  freeBytes    : longint;
  neededBytes  : longint;
  drive        : byte;
  verifyBackup : Boolean;
  vSet         : Boolean;

begin
  verifyBackup := INIVerifyBackup;
  diskNum := 0;
  estDiskNum := 0;
  retVal := continueJob;
  StrCopy(fmErr_Msg,'');

  GetCurrentDir(dbDir, drive);

  GetWorkingDir(dbDir, workDir, backupDir);

  Update;
  SetText('    ',0);
  SetText('    ',IDC_TEXT1);
  SetText('    ',IDC_TEXT2);
  SetText('    ',IDC_PB1_TEXT);
  SetText('    ',IDC_PB1);
  SetText('    ',IDC_PB2_TEXT);
  SetText('    ',IDC_PB2);

  SetText(SR(IDS_BACKUPMAINMSG, updateMsg, 255),0);
  SetText(SR(IDS_BACKUPSET, updateMsg, 255),IDC_TEXT2);

  fileNameList := New(PCollection, Init(20, 5));
  retVal := BuildArchiveFiles(@Self, fileNameList, dbDir,fileDescExt,backupAllFiles);
  {Make sure that an incremental backup is necessary. }
  retVal := CheckForIncremental(@Self, fileNameList, dbDir);
  if (retVal = continueJob) and CanContinue then
  begin
    {Check the drive to make sure there is enough free disk space to perform }
    {this operation.                                                         }
    freeBytes := DiskFree(drive);
    neededBytes := ComputeTotalBytes(@Self, dbDir, estDiskNum);
    if neededBytes < freeBytes then
    begin
      retVal := GetFirstFullBackupDisk(@Self, fullRec);
      if (retVal = continueJob) and CanContinue then
      begin
        Update;
        SetText(SR(IDS_CHECKSCHEMA, updateMsg, 255), IDC_TEXT2);
        retVal := CompareSchemas(@Self, fileNameList, dbDir, 'A:',fileDescExt);
        if (retVal = continueJob) and CanContinue then
        begin
          Update;
          SetText(SR(IDS_CREATINGFILES, updateMsg, 255),IDC_TEXT2);
          CleanDir(workDir,'*.*');
          retVal := BuildIncrementalDatabase(@Self, fileNameList, dbDir, workDir);
          if (retVal = continueJob) and CanContinue then
          begin
            Update;
            fileNameList^.DeleteAll;
            retVal := BuildArchiveFiles(@Self, fileNameList, workDir, fileDescExt, backupAllFiles);
            if (retVal = continueJob) and CanContinue then
            begin
              CleanDir(workDir,'*.DBI');
              totalBytes := ComputeTotalBytes(@Self, workDir, estDiskNum);
              retVal := MakeCatalog(@Self, backupRec, totalBytes, estDiskNum, IncrCatalog);
              SetText(SR(IDS_BACKUP, updateMsg, 255),IDC_TEXT1);
              SR(IDS_FMIB, aTitle, 255);
              if VerifyBackup then
              begin
                GetVerify(vSet);
                SetVerify(True);
              end;
              retVal := TransferFiles(@Self,  fileNameList, workDir,
                                      diskNum, backupRec, false, aTitle);
              CleanDir(workDir,'*.*');
              if  (retVal = continueJob) and canContinue then
              begin
                SetText('    ',IDC_TEXT1);
                SetText(SR(IDS_UPDATECATALOG, msgStr, 255),IDC_TEXT2);
                SetText('    ',IDC_PB1_TEXT);
                SetText('    ',IDC_PB1);
                SetText('    ',IDC_PB2_TEXT);
                SetText('    ',IDC_PB2);
                SR(IDS_INCRBACKUPCOMPLETE, msgStr, 255);
                UpdateDisk1(@Self, diskNum, backupRec, msgStr);
                UpdateBackupSet(@Self, backupRec, fullRec);
              end;
              if VerifyBackup then
                SetVerify(VSet);
            end;
          end;
        end;
      end;
    end
    else
    begin
      {-  insufficient disk space to perform this operation. -}
      SR(IDS_INFORMATION, aTitle, 255);
      SR(IDS_INSUFFDISKSPACE, updateMsg, 255);
      InfoMsg(self.HWindow, aTitle, updateMsg);
    end;
  end
  else
  begin
    if (retVal = NoIncremental) and canContinue then
    begin
      SR(IDS_INFORMATION, aTitle, 255);
      SR(IDS_NOINCREMENTAL, updateMsg, 255);
      InfoMsg(self.HWindow, aTitle, updateMsg);
    end;
  end;
  if (retVal <> continueJob) and (retVal <> cancelJob) and (retVal <> NoIncremental) then
  begin
    SR(IDS_INCRBACKUPTITLE, updateMsg, 255);
    FMShowError(@Self, updateMsg, retVal, fmErr_Msg);
  end;
  MSDisposeObj(fileNameList);
  EndDlg(IDCancel);
end;

{----------------------[FMIncrBackup]---------------------------------------}
{---------------------------------------------------------------------------}
{                                                                           }
{---------------------------------------------------------------------------}
procedure FMIncrBackup(aParent: PWindowsObject);
var
  dlgBackup : PDlgIBackup;

begin
  dlgBackup:= New(PDlgIBackup, Init(aParent,true));
  application^.ExecDialog(dlgBackup);
end;


{----------------------[TDlgRBackup.ProcessDlg]-----------------------------}
{---------------------------------------------------------------------------}
{                                                                           }
{---------------------------------------------------------------------------}
procedure TDlgRBackup.ProcessDlg(var msg: TMessage);
var
  diskNum      : integer;
  estDiskNum   : integer;
  dbDir        : array[0..maxPathLen] of char; { directory location of database files }
  backupRec    : TBackupRec;
  confirmMsg   : array [0..255] of char;
  updateMsg    : array [0..255] of char;
  msgStr       : array [0..255] of char;
  retVal       : integer;
  totalBytes   : longint;
  fileNameList : PCollection;
  aTitle       : array [0..255] of char;
  drive        : byte;
  verifyBackup : Boolean;
  vSet         : Boolean;

begin
  verifyBackup := INIVerifyBackup;
  diskNum := 0;
  estDiskNum := 0;
  retVal := continueJob;
  StrCopy(fmErr_Msg,'');

  SetText('    ',0);
  SetText('    ',IDC_TEXT1);
  SetText('    ',IDC_TEXT2);
  SetText('    ',IDC_PB1_TEXT);
  SetText('    ',IDC_PB1);
  SetText('    ',IDC_PB2_TEXT);
  SetText('    ',IDC_PB2);

  SetText(SR(IDS_BACKUPMAINMSG, updateMsg, 255),0);
  SetText(SR(IDS_REPORTBACKUP, updateMsg, 255),IDC_TEXT2);

  GetCurrentDir(dbDir, drive);
  fileNameList := New(PCollection, Init(20, 5));

  retVal := BuildArchiveFiles(@self, fileNameList, dbDir, fileDescExt, backupReportFiles);
  if (retVal = continueJob) and CanContinue then
  begin
    totalBytes := ComputeReportBytes(@self, fileNameList, dbDir, estDiskNum);
    retVal := MakeCatalog(@self, backupRec, totalBytes, estDiskNum, ReportCatalog);
    SetText(SR(IDS_BACKUP, updateMsg, 255), IDC_TEXT1);
    SR(IDS_FMRB, aTitle, 255);
    if VerifyBackup then
    begin
      GetVerify(vSet);
      SetVerify(True);
    end;
    retVal := TransferFiles(@Self, fileNameList, dbDir, diskNum, backupRec,true, aTitle);
    if  (retVal = continueJob) and canContinue then
    begin
      SetText('    ',IDC_TEXT1);
      SetText(SR(IDS_UPDATECATALOG, msgStr, 255),IDC_TEXT2);
      SetText('    ',IDC_PB1_TEXT);
      SetText('    ',IDC_PB1);
      SetText('    ',IDC_PB2_TEXT);
      SetText('    ',IDC_PB2);
      SR(IDS_REPORTCOMPLETE, msgStr, 255);
      UpdateDisk1(@Self, diskNum, backupRec, msgStr);
    end;
    if VerifyBackup then
      SetVerify(vSet);
  end;

  if (retVal <> continueJob) and (retVal <> cancelJob) then
  begin
    SR(IDS_FULLBACKUPTITLE, updateMsg, 255);
    FMShowError(@Self, updateMsg, retVal, fmErr_Msg);
  end;

  MSDisposeObj(fileNameList);
  EndDlg(IDCancel);
end;

{----------------------[FMREPORTBackup]-------------------------------------}
{---------------------------------------------------------------------------}
{                                                                           }
{---------------------------------------------------------------------------}
procedure FMReportBackup(aParent: PWindowsObject);
var
  dlgBackup : PDlgRBackup;

begin

  dlgBackup:= New(PDlgRBackup, Init(aParent,true));
  application^.ExecDialog(dlgBackup);

end;

END.
{- DWC }

