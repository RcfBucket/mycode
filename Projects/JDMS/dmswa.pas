{----------------------------------------------------------------------------}
{  Module Name  : DMSWA.PAS                                                  }
{  Programmer   : EJ                                                         }
{  Date Created : 05/04/95                                                   }
{                                                                            }
{  Purpose -                                                                 }
{  This module is the main application for the DMS WA interface.             }
{                                                                            }
{  Assumptions -                                                             }
{  None.                                                                     }
{                                                                            }
{  Revision History -                                                        }
{                                                                            }
{  Version  Date      Who    Did What                                        }
{  -------  --------  ------ ----------------------------------------------- }
{  1.00     05/04/95  EJ     Initial release.                                }
{                                                                            }
{----------------------------------------------------------------------------}

program DMSWA;

uses
  WAMain,
  DMSDebug;

BEGIN
  WAMain.Start;
END.
