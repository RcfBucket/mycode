UNIT WLdmstrg;  { Strings Unit }

INTERFACE

USES	Strings, winprocs, WinTypes;
{$R WLDMStrg.RES}

FUNCTION GetStr(strNum: INTEGER; Strng: PCHAR; maxChars: Word):INTEGER;
FUNCTION GetPStr(strNum: INTEGER; VAR Strng: STRING): INTEGER;

IMPLEMENTATION

FUNCTION GetStr(strNum: INTEGER; Strng: PCHAR; maxChars: Word):INTEGER;
VAR
	rtnval: INTEGER;
	buffer: PChar;
BEGIN
	GetMem(buffer, maxChars+1);
	rtnval:= LoadString(HInstance, strNum, buffer, maxChars);

	IF rtnval > 0 THEN
		strCopy(strng, buffer);

	FreeMem(buffer, maxChars+1);
	GetStr := rtnval;
END;

FUNCTION GetPStr(strNum: INTEGER; VAR Strng: STRING): INTEGER;
VAR
	rtnval: INTEGER;
	buffer: array[0..255] of CHAR;

BEGIN
  rtnval:= LoadString(HInstance, strNum, buffer, 255);
  IF rtnval >0 THEN
  BEGIN
	 strng:= StrPas(buffer);
  END;
  GetPStr := rtnval;
END;

END.
