(****************************************************************************


cvt20.inc

produced by Borland Resource Workshop


*****************************************************************************)

const
  DLG_CVT20         = 17976;
  DLG_V20MAP        = 17977;
  DLG_CVT20Convert  = 17978;
  DLG_BATMAP        = 17979;
  DLG_EDITBATORD    = 17980;

  IDS_RESTOREFLOPPY = 17977;
  IDS_PREPCOMPLETE  = 17979;
  IDS_INSERTDISK    = 17980;

  LBL_MSG           = 204;

  IDC_STATLIST      = 207;

  IDC_BATID         = 205;
  IDC_CLEARMAP      = 103;
  IDC_DONE          = 102;
  IDC_EDIT2         = 202;
  IDC_EDIT3         = 203;
  IDC_EDIT4         = 204;
  IDC_EDITMAP       = 101;
  IDC_FINISH        = 101;
  IDC_GRID          = 202;
  IDC_LIST          = 202;
  IDC_LOG           = 103;
  IDC_MAP           = 102;
  IDC_NEXT          = 101;
  IDC_STOP          = 102;
  IDC_TEXT0         = 208;
  IDS_INVALIDDISK   = 17976;
  IDS_RESTORECOMP   = 17981;
  IDS_TITLE         = 17978;
	DLG_GETORD	=	17981;
	IDC_BATTERY	=	201;
	LBL_BATTERY	=	202;
	DLG_LISTDATA	=	17982;
	IDC_GROUP	=	203;
	LBL_GRPID	=	210;
	IDC_ORDDESC	=	301;
	IDC_GRPDESC	=	300;
	IDC_EXIT	=	2;
	IDC_EDIT1	=	201;
