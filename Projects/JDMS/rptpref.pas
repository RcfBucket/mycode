Unit RptPref;

INTERFACE

uses
  DMSDebug,
  Strings,
  ApiTools,
  ctllib,
  RptData,
  DlgLib,
  WinProcs,
  WinTypes,
  ODialogs,
  OWindows;

{$I RPTGEN.INC}

type
  {- TPrefDlg modifies the preferences structure. All preferences are set if
     the user selects OK. Preferences are stored in DMS.INI }
  PPrefDlg = ^TPrefDlg;
  TPrefDlg = object(TCenterDlg)
    constructor Init(AParent: PWindowsObject);
    procedure SetupWindow; virtual;
    procedure OK(var msg: TMessage); virtual id_first + ID_OK;
  end;

IMPLEMENTATION

constructor TPrefDlg.Init(AParent: PWindowsObject);
var
  c  : PControl;
begin
  inherited Init(AParent, MAKEINTRESOURCE(DLG_PREFERENCE));
  c:= New(PLRadioButton, InitResource(@self, IDC_INCH, false));
  c:= New(PLRadioButton, InitResource(@self, IDC_MM, false));
  c:= New(PLRadioButton, InitResource(@self, IDC_CM, false));
end;

procedure TPrefDlg.SetupWindow;
begin
  inherited SetupWindow;
  SendDlgItemMsg(IDC_INCH, BM_SETCHECK, BF_UNCHECKED, 0);
  SendDlgItemMsg(IDC_MM, BM_SETCHECK, BF_UNCHECKED, 0);
  SendDlgItemMsg(IDC_CM, BM_SETCHECK, BF_UNCHECKED, 0);
  if preferences.units = rptInch then
    SendDlgItemMsg(IDC_INCH, BM_SETCHECK, BF_CHECKED, 0)
  else if preferences.units = rptMM then
    SendDlgItemMsg(IDC_MM, BM_SETCHECK, BF_CHECKED, 0)
  else
    SendDlgItemMsg(IDC_CM, BM_SETCHECK, BF_CHECKED, 0);
end;

procedure TPrefDlg.OK(var msg: TMessage);
var
  pstr   : array[0..150] of char;
begin
  if SendDlgItemMsg(IDC_INCH, BM_GETCHECK, 0,0) = BF_CHECKED then
    preferences.units:= rptInch
  else if SendDlgItemMsg(IDC_MM, BM_GETCHECK, 0, 0) = BF_CHECKED then
    preferences.units:= rptMM
  else
    preferences.units:= rptCM;

  case preferences.units of
    rptInch : strcopy(pstr, 'IN');  {- do not translate these strings }
    rptMM   : strcopy(pstr, 'MM');
    rptCM   : strcopy(pstr, 'CM');
  end;
  WritePrivateProfileString('Report Generator', 'Units', pstr, 'DMS.INI');

  inherited OK(msg);
end;

END.

{- rcf }
