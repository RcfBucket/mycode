{$F+}   {!!!!!!!}

unit WLWaitLb;

INTERFACE

Uses
   WinDos;

var
  WaitOneMS : word;
  LoopsPerTick : longint;

procedure MSWait(ms : word);
Procedure SetTO(SecCnt: Word);
Function TimeOut: Boolean;

IMPLEMENTATION

{$L WAITWLDR.OBJ}

procedure MSWait(ms : word); external;
procedure WaitInit; external;


Var
   tdrop        : Boolean;
   clockTo      : LongInt;

Procedure SetTO(SecCnt: Word);
{- This procedure calculates the "real time" TimeOut time, from the
   number of seconds passed to it and sets a global variable for the
   TimeOut function }
Var
   Hour,Min,Sec,Sec100  : Word;
Begin
   GetTime(hour, min, sec, sec100);
   If (Hour = 24) and (Min = 59) and (SecCnt > (60 - Sec)) then
    ClockTO:= SecCnt - (60 - Sec)
  Else
    ClockTO:= (Hour*60+Min) * 60 + Sec + SecCnt;
End;


Function TimeOut: Boolean;
{- This function checks the clock for a TimeOut condition }
Var
  TempClk      : LongInt;
  Hour,Min,Sec : Word;
  sec100       : Word;
Begin
   TimeOut:= True;
   If not TDrop then Begin
      GetTime(hour, min, sec, sec100);
      TempClk:= (Hour*60+Min) * 60 + sec;
      If TempClk > ClockTO Then
         TDrop:= True
      Else
         TimeOut:= False;
   End;
End;


BEGIN
   clockTo:= 0;
   tdrop:= False;
   WaitInit;
END.


