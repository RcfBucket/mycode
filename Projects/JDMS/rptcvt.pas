program RptCvt;

{- report converter }

uses
  CommDlg,
  DBIds,
  Strings,
  ApiTools,
  MScan,
  RptXFer,
  Ctl3d,
  IniLib,
  DlgLib,
  DBFile,
  DBLib,
  DBTypes,
  OWindows,
  ODialogs,
  WinTypes,
  WinProcs;

{$R RPTCVT.RES}
{$I RPTCVT.INC}

type
  TCharArray = array[0..100] of char;

  TCvtApp = object(TApplication)
    procedure InitMainWindow; virtual;
  end;

  PCvtDlg = ^TCvtDlg;
  TCvtDlg = object(TCenterDlgWindow)
    list    : PListBox;
    constructor Init;
    procedure SetupWindow; virtual;
    function  GetClassName: PChar; virtual;
    procedure GetWindowClass(var aWndClass: TWndClass); virtual;
    procedure IDExport(var msg: TMessage); virtual ID_FIRST + IDC_EXPORT;
    procedure IDImport(var msg: TMessage); virtual ID_FIRST + IDC_IMPORT;
    procedure IDDelete(var msg: TMessage); virtual ID_FIRST + IDC_DELETE;
    procedure FillList;
  end;

constructor TCvtDlg.Init;
begin
  inherited Init(nil, MakeIntResource(DLG_MAIN));
  list:= New(PListBox, InitResource(@self, IDC_LIST));
end;

procedure TCvtDlg.SetupWindow;
begin
  inherited SetupWindow;
  FillList;
end;

function TCvtDlg.GetClassName: PChar;
begin
  GetClassName:= 'RptCvt';
end;

procedure TCvtDlg.GetWindowClass(var aWndClass: TWndClass);
begin
  inherited GetWindowClass(aWndClass);
  aWndClass.HIcon:= LoadIcon(HInstance, MakeIntResource(ICON_1));
end;

procedure TCvtDlg.FillList;
var
  pstr    : array[0..100] of char;
  db      : PDBFile;
  idx     : integer;
begin
  list^.ClearList;
  db:= New(PDBFile, Init(DBRPTFmtsFile, '', dbOpenNormal));
  if db^.dbc^.GetFirst(db^.dbr) then
  begin
    repeat
      db^.dbr^.GetFieldAsStr(DBRPTFmtName, pstr, sizeof(pstr)-1);
      idx:= list^.AddString(pstr);
      SendDlgItemMsg(IDC_LIST, LB_SETITEMDATA, idx, db^.dbr^.GetSeqValue);
    until not db^.dbc^.GetNext(db^.dbr);
  end;
  MSDisposeObj(db);
end;

procedure TCvtDlg.IDDelete(var msg: TMessage);
var
  k   : integer;
  seq : TSeqNum;
begin
  k:= list^.GetSelIndex;
  if k >= 0 then
  begin
    if YesNoMsg(hWindow, 'Confirm', 'Delete selected report?') then
    begin
      seq:= SendDlgItemMsg(IDC_LIST, LB_GETITEMDATA, k, 0);
      DeleteReport(seq);
      FillList;
    end;
  end;
end;

function GetFileName(HWindow: HWnd; forceExist: Boolean;
          filename, pathname: PChar; srchFilterTxt, srchFilter: PChar): Boolean;
{- srchFilterTxt should be description of extension,
   srchFilter should be the extension (including the dot, ie, '.TXT'.
   On entrance, fileName should have dialog box caption }
var
  tfile   : TOpenFileName;
  filter  : array[0..80] of char;
  fname   : array[0..80] of char;
  fullFile: array[0..80] of char;
  defExt  : array[0..10] of char;
  k       : Integer;
begin
  FillChar(tFile, SizeOf(tFile), 0);
  FillChar(filter, sizeof(filter), #0);
  FillChar(fullFile, sizeof(fullFile), #0);

  StrCopy(filter, srchFilterTxt);
  strCat(filter, ' (*');
  strCat(filter, srchFilter);
  strCat(filter, ')');

  k:= Strlen(filter);
  StrCopy(fname, '*'); StrCat(fname, srchFilter);
  StrCopy(@filter[k+1], fname);

  Inc(k, strlen(srchFilter)+2);

  StrCopy(@filter[k+1], 'All Files (*.*)');
  Inc(k, strlen('All Files (*.*)')+1);
  StrCopy(@filter[k+1], '*.*');

  StrCopy(defExt, srchFilter);
  Move(defExt[1], defExt[0], 4);

  tFile.lStructSize:= sizeOf(tFile);
  tFile.hWndOwner:= HWindow;
  tFile.hInstance:= hinstance;
  tFile.lpstrFilter:= filter;
  tFile.nFilterIndex:= 0;
  tFile.lpstrFile:= fullFile;
  tFile.nMaxFile:= sizeof(fullFile);
  tFile.lpstrFileTitle:= fName;
  tFile.nMaxFileTitle:= sizeof(fname);
  StrCopy(fname, fileName);
  tFile.lpstrTitle:= fname;
  tFile.lpstrDefExt:= defExt;
  if forceExist then
    tFile.flags:= OFN_FILEMUSTEXIST;
  StrCopy(fileName, '');
  if GetOpenFileName(tFile) then
  begin
    { parse file name and extension }
    StrCopy(pathName, '');
    if tFile.nFileOffset > 0 then
    begin
      Move(tFile.lpstrFile[0], pathName[0], tFile.nFileOffset);
      pathName[tFile.nFileOffset]:= #0;
    end;
{    k:= tFile.nFileExtension - tFile.nFileOffset - 1;}
    Move(tFile.lpstrFileTitle[0], fileName[0], k);
{    fileName[k]:= #0;}
    GetFileName:= True;
  end
  else
    GetFileName:= False;
end;

function GetSaveName(HWindow: HWnd; forceExist: Boolean;
          filename, pathname: PChar; srchFilterTxt, srchFilter: PChar): Boolean;
{- srchFilterTxt should be description of extension,
   srchFilter should be the extension (including the dot, ie, '.TXT'.
   On entrance, fileName should have dialog box caption }
var
  tfile   : TOpenFileName;
  filter  : array[0..80] of char;
  fname   : array[0..80] of char;
  fullFile: array[0..80] of char;
  defExt  : array[0..10] of char;
  k       : Integer;
begin
  FillChar(tFile, SizeOf(tFile), 0);
  FillChar(filter, sizeof(filter), #0);
  FillChar(fullFile, sizeof(fullFile), #0);

  StrCopy(filter, srchFilterTxt);
  strCat(filter, ' (*');
  strCat(filter, srchFilter);
  strCat(filter, ')');

  k:= Strlen(filter);
  StrCopy(fname, '*'); StrCat(fname, srchFilter);
  StrCopy(@filter[k+1], fname);

  Inc(k, strlen(srchFilter)+2);

  StrCopy(@filter[k+1], 'All Files (*.*)');
  Inc(k, strlen('All Files (*.*)')+1);
  StrCopy(@filter[k+1], '*.*');

  StrCopy(defExt, srchFilter);
  Move(defExt[1], defExt[0], 4);

  tFile.lStructSize:= sizeOf(tFile);
  tFile.hWndOwner:= HWindow;
  tFile.hInstance:= hinstance;
  tFile.lpstrFilter:= filter;
  tFile.nFilterIndex:= 0;
  tFile.lpstrFile:= fullFile;
  tFile.nMaxFile:= sizeof(fullFile);
  tFile.lpstrFileTitle:= fName;
  tFile.nMaxFileTitle:= sizeof(fname);
  StrCopy(fname, fileName);
  tFile.lpstrTitle:= fname;
  tFile.lpstrDefExt:= defExt;
  if forceExist then
    tFile.flags:= OFN_FILEMUSTEXIST;
  StrCopy(fileName, '');
  if GetSaveFileName(tFile) then
  begin
    { parse file name and extension }
    StrCopy(pathName, '');
    if tFile.nFileOffset > 0 then
    begin
      Move(tFile.lpstrFile[0], pathName[0], tFile.nFileOffset);
      pathName[tFile.nFileOffset]:= #0;
    end;
{    k:= tFile.nFileExtension - tFile.nFileOffset - 1;}
    Move(tFile.lpstrFileTitle[0], fileName[0], k);
{    fileName[k]:= #0;}
    GetSaveName:= True;
  end
  else
    GetSaveName:= False;
end;

procedure TCvtDlg.IDImport(var msg: TMessage);
var
  fName, path : array[0..80] of char;
begin
  StrCopy(fname, 'Import Report');
  if GetFileName(hWindow, true, fName, path, 'Text Files', '.TXT') then
  begin
    StrCat(path, fname);
    ImportReport(path);
    FillList;
  end;
end;

procedure TCvtDlg.IDExport(var msg: TMessage);
var
  k   : integer;
  seq : TSeqNum;
  fName, path : array[0..80] of char;
begin
  k:= list^.GetSelIndex;
  if k >= 0 then
  begin
    StrCopy(fname, 'Export Report');
    if GetSaveName(hWindow, true, fName, path, 'Text Files', '.TXT') then
    begin
      StrCat(path, fname);
      seq:= SendDlgItemMsg(IDC_LIST, LB_GETITEMDATA, k, 0);
      ExportReport(seq, path);
    end;
  end
  else
    MessageBeep(0);
end;

procedure TCvtApp.InitMainWindow;
begin
  if INIUse3D then
    Register3dApp(true, true, true);
  mainWindow:= New(PCvtDlg, Init);
end;

var
  app : TCvtApp;

BEGIN
  app.Init('RptCvt');
  app.Run;
  app.Done;
END.
