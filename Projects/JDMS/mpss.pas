{----------------------------------------------------------------------------}
{  Module Name  : MPSS.PAS                                                   }
{  Programmer   : EJ                                                         }
{  Date Created : 04/04/95                                                   }
{                                                                            }
{  Purpose -                                                                 }
{  This module is used to create objects to search the Nihongo DMS           }
{  database files with stored and dynamic rules.                             }
{                                                                            }
{  Assumptions -                                                             }
{  None.                                                                     }
{                                                                            }
{  Revision History -                                                        }
{                                                                            }
{  Version  Date      Who    Did What                                        }
{  -------  --------  ------ ----------------------------------------------- }
{  1.00     04/04/95  EJ     Initial release                                 }
{                                                                            }
{----------------------------------------------------------------------------}

unit MPSS;

INTERFACE

uses
  Objects,
  OWindows,
  WinTypes,
  DBLib,
  DBFile,
  ResLib,
  ListLib,
  PrnPrev,
  MPSSObjs;

Const
  SkipUndefDates    = True;
  IncludeUndefDates = False;

type
  PMPSS = ^TMPSS;
  TMPSS = object(TObject)
    TotalRecords    : longint;
    UpdateFrequency : word;
    mRules          : PRules;
    mFiles          : PFiles;
    mFields         : PFields;
    mFileCount      : word;
    mFieldCount     : word;
    mRuleLoaded     : boolean;
    mCurrentRecord  : longint;
    mValidRecord    : longint;
    IgnoreUndefDates: Boolean;
    {}
    constructor Init(aParent: PWindowsObject; drivingFH: PDBFile; xIgnoreUndefDates: Boolean);
    destructor Done; virtual;
    function  MPSSLoadRules(aParent: PWindowsObject; aTitle: PChar): boolean;
    function  MPSSGetFirst(drivingFH: PDBFile): boolean;
    function  MPSSGetNext(drivingFH: PDBFile): boolean;
    function  MPSSGetPercent: integer;
    function  MPSSValidRecord(drivingFH: PDBFile): boolean;
    procedure PrintRules(printInfo: PPrintInfo);
    {}
    private
    { The following fields are used only for result comparisons
      for ISOLATE driving files}
    resFileIdx      : word;
    resList         : PResults;
    mResRec         : PResRecObj;
    mnmList         : PListObject;
    mnmListRec      : PMnemListRec;
    {}
    function  ValidRecord: boolean;
    procedure FreshenRecord(FileIdx: word);
    procedure SetUpdateFreq;
    function  CreateFileEntry(fileName: PChar): word;
    function  AddFileAssoc(fileIdx: word; assocIdx: word): word;
  end;

IMPLEMENTATION

uses
  WinProcs,
  Strings,
  MScan,
  APITools,
  DBIDs,
  DBTypes,
  MPSSDlg,
  DMSErr,
  DMSDebug;

const
  greaterThan         = $40;
  lessThan            = $20;


{----------------------------------[ TMPSS ]---------------------------------}
{----------------------------------------------------------------------------}
{  Initialize an MPSS object.                                                }
{----------------------------------------------------------------------------}
constructor TMPSS.Init(aParent: PWindowsObject; drivingFH: PDBFile; xIgnoreUndefDates: Boolean);
var
  j, k         : word;
(*WaitCurs     : PWaitCursor;  *)
  fi           : TFldInfo;
  fileName     : FNString;
  tempStr      : PChar;
  testFH       : PDBFile;
begin
  resList := nil;
  mFields := nil;
  mFiles := nil;
  mRules := nil;
  mnmListRec := nil;
  mnmList := nil;
  resFileIdx := 0;
  TotalRecords := 0;
  UpdateFrequency := 0;
  mCurrentRecord := 0;
  mValidRecord := 0;
  mRuleLoaded := FALSE;

  if not inherited Init then
    Fail;

(*WaitCurs  :=  New(PWaitCursor, Init); *)
  IgnoreUndefDates:=xIgnoreUndefDates;
  mnmList := New(PListObject, Init);
  mnmListRec := New(PMnemListRec, Init);
  if (mnmList = nil) or (mnmListRec = nil) then
  begin
    ShowError(aParent, IDS_INITERR1, nil, 0, MOD_MPSS, 1);
    Done;
  end;

  { Setup rules }
  GetMem(mRules, SizeOf(TRules));
  if mRules = nil then
  begin
    ShowError(aParent, IDS_INITERR1, nil, 0, MOD_MPSS, 2);
    Halt;
  end;
  mRules^[0].FH := nil;

  { Setup files }
  GetMem(mFiles, SizeOf(TFiles));
  if mFiles = nil then
  begin
    ShowError(aParent, IDS_INITERR1, nil, 0, MOD_MPSS, 3);
    Halt;
  end;
  mFileCount := 1;
  drivingFH^.dbd^.FileName(fileName, 8);
  StrLCopy(mFiles^[1].FileName, fileName, 8);
  mFiles^[1].FH := drivingFH;
  mFiles^[1].FH^.dbd^.FileDesc(mFiles^[1].FileDesc, SizeOf(mFiles^[1].FileDesc)-1);
  mFiles^[1].Fresh := TRUE;
  mFiles^[1].LinkedBy := 0;
  FillChar(mFiles^[1].LinkAssoc, sizeof(mFiles^[1].LinkAssoc), 0);

  { Link other files }
  if StrIComp(fileName, DBIsoFile) = 0 then
  begin
    AddFileAssoc(mFileCount, DBISOLATE_SPECIMEN_ASSOC);
    AddFileAssoc(mFileCount, DBSPECIMEN_PATIENT_ASSOC);
    CreateFileEntry(DBTSTSUBFile);
    resFileIdx := mFileCount;
    resList := New(PResults, Init);
    if resList = nil then
    begin
      ShowError(aParent, IDS_INITERR1, nil, 0, MOD_MPSS, 4);
      Done;
      Fail;
    end;
  end
  else if StrIComp(fileName, DBSpecFile) = 0 then
  begin
    AddFileAssoc(mFileCount, DBSPECIMEN_PATIENT_ASSOC);
  end
  else if StrIComp(fileName, DBPatFile) = 0 then
  begin
  end
  else
  begin
    Done;
    Fail;
  end;
  mFiles^[mFileCount + 1].FH := nil;

  { Setup fields }
  GetMem(mFields, SizeOf(TFields));
  if mFields = nil then
  begin
    ShowError(aParent, IDS_INITERR1, nil, 0, MOD_MPSS, 5);
    Halt;
  end;
  mFieldCount := 0;
  for j := 1 to mFileCount do
  begin
    if j = resFileIdx then
      Continue;
    for k := 1 to mFiles^[j].FH^.dbd^.NumFields do
    begin
      mFiles^[j].FH^.dbd^.FieldInfo(k, fi);
      if (fi.userFlag and db_Hidden) <> 0 then
        Continue;
      Inc(mFieldCount);
      mFields^[mFieldCount].FH := mFiles^[j].FH;
      StrLCopy(mFields^[mFieldCount].FileDesc, mFiles^[j].FileDesc, SizeOf(mFields^[mFieldCount].FileDesc));
      StrLCopy(mFields^[mFieldCount].FieldName, fi.fldName, SizeOf(mFields^[mFieldCount].FieldName));
      mFields^[mFieldCount].FieldNo := k;
      mFields^[mFieldCount].FieldType := fi.fldType;
      mFields^[mFieldCount].TestRef := -1;    { not a test result type }
      if fi.fldType = dbSeqRef then
        mFields^[mFieldCount].OpsType := 1
      else
        mFields^[mFieldCount].OpsType := 0;
    end;
  end;
  if StrIComp(fileName, DBIsoFile) = 0 then
  begin
    j := resFileIdx;
    GetMem(tempStr, 101);
    testFH := New(PDBFile, Init(DBTSTFile, '', dbOpenNormal));
    if testFH = nil then
    begin
      ShowError(aParent, IDS_INITERR1, nil, 0, MOD_MPSS, 6);
      Done;
      Fail;
    end;

    if testFH^.dbc^.GetFirst(testFH^.dbr) then
      repeat
        Inc(mFieldCount);
        mFields^[mFieldCount].FH := mFiles^[j].FH;
        StrLCopy(mFields^[mFieldCount].FileDesc, mFiles^[j].FileDesc, SizeOf(mFields^[mFieldCount].FileDesc));
        mFields^[mFieldCount].TestRef := testFH^.dbr^.GetSeqValue;
        if StrComp(testFH^.dbr^.GetFieldAsStr(DBTSTTstCat, tempStr, 10), 'NCCLS') = 0 then
        begin
          testFH^.dbr^.GetFieldAsStr(DBTSTID, mFields^[mFieldCount].FieldName, SizeOf(mFields^[mFieldCount].FieldName));
          StrLCat(mFields^[mFieldCount].FieldName, ' ', SizeOf(mFields^[mFieldCount].FieldName));
          StrLCat(mFields^[mFieldCount].FieldName, tempStr, SizeOf(mFields^[mFieldCount].FieldName));
          mFields^[mFieldCount].FieldNo := DBTSTSUBNCCLS;
          mFields^[mFieldCount].FieldType := dbSeqRef;
          mFields^[mFieldCount].OpsType := 1;
        end
        else if StrComp(testFH^.dbr^.GetFieldAsStr(DBTSTTstCat, tempStr, 10), 'MIC') = 0 then
        begin
          testFH^.dbr^.GetFieldAsStr(DBTSTID, mFields^[mFieldCount].FieldName, SizeOf(mFields^[mFieldCount].FieldName));
          StrLCat(mFields^[mFieldCount].FieldName, ' ', SizeOf(mFields^[mFieldCount].FieldName));
          StrLCat(mFields^[mFieldCount].FieldName, tempStr, SizeOf(mFields^[mFieldCount].FieldName));
          mFields^[mFieldCount].FieldNo := DBTSTSUBMIC;
          mFields^[mFieldCount].FieldType := dbSeqRef;
          mFields^[mFieldCount].OpsType := 2;
        end
        else if StrComp(testFH^.dbr^.GetFieldAsStr(DBTSTTstCat, tempStr, 10), 'BIO') = 0 then
        begin
          testFH^.dbr^.GetFieldAsStr(DBTSTName, mFields^[mFieldCount].FieldName, SizeOf(mFields^[mFieldCount].FieldName));
          StrLCat(mFields^[mFieldCount].FieldName, ' ', SizeOf(mFields^[mFieldCount].FieldName));
          StrLCat(mFields^[mFieldCount].FieldName, tempStr, SizeOf(mFields^[mFieldCount].FieldName));
          mFields^[mFieldCount].FieldNo := DBTSTSUBBiotype;
          mFields^[mFieldCount].FieldType := dbZString;
          mFields^[mFieldCount].OpsType := 3;
        end
        else
        begin
          testFH^.dbr^.GetFieldAsStr(DBTSTName, mFields^[mFieldCount].FieldName, SizeOf(mFields^[mFieldCount].FieldName));
          StrLCat(mFields^[mFieldCount].FieldName, ' ', SizeOf(mFields^[mFieldCount].FieldName));
          StrLCat(mFields^[mFieldCount].FieldName, tempStr, SizeOf(mFields^[mFieldCount].FieldName));
          testFH^.dbr^.GetFieldAsStr(DBTSTName, tempStr, 100);
          mFields^[mFieldCount].FieldNo := DBTSTSUBTest;
          mFields^[mFieldCount].FieldType := dbZString;
          mFields^[mFieldCount].OpsType := 3;
        end;
      until not testFH^.dbc^.GetNext(testFH^.dbr);

    MSDisposeObj(testFH);
    MSFreeMem(tempStr, 101);
  end;
  mFields^[mFieldCount + 1].FH := nil;
  mFields^[mFieldCount + 1].FieldNo := word(-1);

  SetUpdateFreq;
(*MSDisposeObj(WaitCurs); *)
end;

destructor TMPSS.Done;
var
  k : word;
begin
  MSDisposeObj(resList);
  MSFreeMem(mFields, SizeOf(TFields));
  if mFiles <> nil then
  begin
    k := 2;
    while mFiles^[k].FH <> nil do
    begin
      MSDisposeObj(mFiles^[k].FH);
      Inc(k);
    end;
  end;
  MSFreeMem(mFiles, SizeOf(TFiles));
  MSFreeMem(mRules, SizeOf(TRules));
  MSDisposeObj(mnmListRec);
  MSDisposeObj(mnmList);
  inherited Done;
end;

function  TMPSS.MPSSLoadRules(aParent: PWindowsObject; aTitle: PChar): boolean;
var
  MPSSDlg      : PMPSSMainDialog;
begin
  { Get rules from user }
  MPSSDlg := New(PMPSSMainDialog, Init(aParent, mRules, mFiles, mFields,
                                       mFiles^[1].FileName, aTitle));
  mRuleLoaded := (Application^.ExecDialog(MPSSDlg) = IDOK);
  MPSSLoadRules := mRuleLoaded;
end;

function  TMPSS.MPSSGetFirst(drivingFH: PDBFile): boolean;
var
  valid : boolean;
begin
  MPSSGetFirst := FALSE;
  mCurrentRecord := 0;
  mValidRecord := 0;
  if drivingFH <> mFiles^[1].FH then
    Exit;

  { Setup TotalRecords }
  TotalRecords := mFiles^[1].FH^.dbc^.NumRecords;

  if drivingFH^.dbc^.GetFirst(drivingFH^.dbr) then
  begin
    Inc(mCurrentRecord);
    valid := ValidRecord;
    while not valid do
    begin
      if not drivingFH^.dbc^.GetNext(drivingFH^.dbr) then
        Break;
      Inc(mCurrentRecord);
      valid := ValidRecord;
    end;
    if valid then
      mValidRecord := 1;
    MPSSGetFirst := valid;
  end;
end;

function  TMPSS.MPSSGetNext(drivingFH: PDBFile): boolean;
var
  valid : boolean;
begin
  MPSSGetNext := FALSE;
  if drivingFH <> mFiles^[1].FH then
    Exit;

  if drivingFH^.dbc^.GetNext(drivingFH^.dbr) then
  begin
    Inc(mCurrentRecord);
    valid := ValidRecord;
    while not valid do
    begin
      if not drivingFH^.dbc^.GetNext(drivingFH^.dbr) then
        Break;
      Inc(mCurrentRecord);
      valid := ValidRecord;
    end;
    if valid then
      Inc(mValidRecord);
    MPSSGetNext := valid;
  end;
end;

function  TMPSS.MPSSGetPercent: integer;
begin
  if TotalRecords > 0 then
    MPSSGetPercent := ((mCurrentRecord*100) div TotalRecords)
  else
    MPSSGetPercent := 100;
end;

function  TMPSS.MPSSValidRecord(drivingFH: PDBFile): boolean;
begin
  if drivingFH <> mFiles^[1].FH then
    MPSSValidRecord := FALSE
  else
    MPSSValidRecord := ValidRecord;
end;

procedure TMPSS.PrintRules(printInfo: PPrintInfo);
{- print a page of defined rules }
var
  linestr  : PChar;
  ruleCnt  : integer;
  andOrStr : array [0..10] of char;
  opStr    : array [0..10] of char;
begin
  if not mRuleLoaded then
    Exit;

  GetMem(lineStr, 256);

  SR(IDS_MPSS_RPT_HDR, lineStr, 80);  {- user defined search criteria}
  printInfo^.body^.AddRow(r_Normal, 0);
  printInfo^.body^.AddRow(r_CenterRow, 0);
  printInfo^.body^.AddCell(lineStr, LT100, c_Stretch or c_Bold or c_BorderBottom, 0);
  printInfo^.body^.AddRow(r_Normal, 0);
  printInfo^.body^.AddRow(r_Normal, 0);

  ruleCnt := 1;
  while mRules^[ruleCnt].FH <> nil  do
  begin
    if ruleCnt = 0 then
      StrCopy(andOrStr, '')
    else if mRules^[ruleCnt].AndOr = MPSSAnd then
      SR(IDS_AND, andOrStr, 10)
    else
      SR(IDS_OR, andOrStr, 10);
    printInfo^.body^.AddCell(andOrStr, LT50, c_Normal, 0);

    case mRules^[ruleCnt].Operator of
      MPSSEQ : StrCopy(opStr, '=');
      MPSSNE : StrCopy(opStr, '<>');
      MPSSLT : StrCopy(opStr, '<');
      MPSSGT : StrCopy(opStr, '>');
      MPSSLE : StrCopy(opStr, '<=');
      MPSSGE : StrCopy(opStr, '>=');
      MPSSIN : StrCopy(opStr, '@');
    end;
    if mRules^[ruleCnt].LParen then
      StrCopy(lineStr, '(')
    else
      StrCopy(lineStr, '');
    printInfo^.body^.AddCell(lineStr, LT25, c_Normal, 0);

    StrCopy(lineStr, mRules^[ruleCnt].FileDesc);
    StrCat(lineStr, '.');
    StrCat(lineStr, mRules^[ruleCnt].FieldName);
    printInfo^.body^.AddCell(lineStr, LT200, c_Normal, 0);
    printInfo^.body^.AddCell(opStr, LT25, c_Normal, 0);
    printInfo^.body^.AddCell(mRules^[ruleCnt].Value, LT200, c_Normal, 0);

    if mRules^[ruleCnt].RParen then
      StrCopy(lineStr, ')')
    else
      StrCopy(lineStr, '');
    printInfo^.body^.AddCell(lineStr, LT25, c_Normal, 0);
  end;
  MSFreeMem(linestr, 256);
  printInfo^.body^.AddRow(r_PageBreak, 0);
end;

function  TMPSS.ValidRecord: boolean;
var
  fileCounter       : word;
  ruleCounter       : word;
  resCounter        : word;
  RunningResult     : boolean;
  ParenResult       : boolean;
  Result            : boolean;
  Comparison        : integer;
  temp              : array [0..257] of char;
  temp1             : longint;
  temp2             : single;
  temp3             : double;
  inParen           : boolean;
  reservedAndOr     : TMPSSRel;
  code              : integer;
  seq               : TSeqNum;
  aVal, bVal        : byte;
  resListLoaded     : boolean;
begin
  if not mRuleLoaded then
  begin
    ValidRecord := TRUE;
    Exit;
  end;

  RunningResult := TRUE;
  ParenResult := TRUE;
  Result := FALSE;
  inParen := FALSE;
  ruleCounter := 0;
  resListLoaded := FALSE;
  while mRules^[ruleCounter].FH <> nil do
  begin
    if mRules^[ruleCounter].LParen then
      inParen := TRUE;

    if (resFileIdx <> 0) and
       (mRules^[ruleCounter].FH = mFiles^[resFileIdx].FH) then
    begin
      { compare the results }
      if not resListLoaded then
      begin
        resListLoaded := TRUE;
        resList^.LoadResults(mFiles^[1].FH^.dbr);
      end;
      resCounter := 0;
      while (resCounter < resList^.Count) do
      begin
        mResRec := resList^.At(resCounter);
        if mResRec^.testSeq = mRules^[ruleCounter].TestRef then
          Break;
        Inc(resCounter);
      end;
      if (resCounter < resList^.Count) then
      begin
        if mRules^[ruleCounter].FieldNo = DBTSTSUBMIC then
        begin
          resList^.MICToByte(mResRec^.Result, aVal);
          seq := mRules^[ruleCounter].lValue;
          if mnmList^.FindMnemSeq(DBMICFile, mnmListRec, seq) then
          begin
            Comparison := 1;
            Val(mnmListRec^.ID, bVal, code);
            if aVal <> $FF then
              case bVal of
                greaterThan:
                  if (bVal and aVal) = bVal then
                    Comparison := 0;
                lessThan:
                  if (bVal and aVal) = bVal then
                    Comparison := 0;
                else
                  begin
                    if bVal = (aVal and not lessThan) then
                      Comparison := 0;
                  end;
              end;

            case mRules^[ruleCounter].Operator of
              MPSSEQ : Result := Comparison =  0;
              MPSSNE : Result := Comparison <> 0;
              else
                Result := FALSE;
            end;
          end
          else
            Result := ((aVal = $FF) and (mRules^[ruleCounter].Operator = MPSSEQ)) or
                      ((aVal <> $FF) and (mRules^[ruleCounter].Operator = MPSSNE));
        end
        else
        begin
          Comparison := StrComp(mResRec^.Result,
                                mRules^[ruleCounter].Value);
          case mRules^[ruleCounter].Operator of
            MPSSEQ : Result := Comparison =  0;
            MPSSNE : Result := Comparison <> 0;
            else
              Result := FALSE;
          end;
        end;
      end
      else
      begin
        Result := FALSE;
      end;
    end
    else
    begin
      { turn all fresh flags off }
      fileCounter :=  2;
      while mFiles^[fileCounter].FH <> nil do
      begin
        mFiles^[fileCounter].Fresh := FALSE;
        Inc(fileCounter)
      end;

      { find the file that needs freshening }
      fileCounter :=  1;
      while (mFiles^[fileCounter].FH <> mRules^[ruleCounter].FH) and
            (mFiles^[fileCounter].FH <> nil) do
        Inc(fileCounter);
      if mFiles^[fileCounter].FH <> nil then
      begin
        { Refresh the record of the field in the rule }
        FreshenRecord(fileCounter);

        mFiles^[fileCounter].FH^.dbr^.GetFieldAsStr(
                            mRules^[ruleCounter].FieldNo,
                            temp, 257);
        case mRules^[ruleCounter].FieldType of
          dbInteger, dbLongInt, dbWord, dbBoolean, dbByte, dbShortInt,
          dbChar, dbDate, dbTime, dbSeqRef, dbAutoInc :
          begin
            temp1 := 0;
            mFiles^[fileCounter].FH^.dbr^.GetField(
                                mRules^[ruleCounter].FieldNo,
                                @temp1, SizeOf(temp1));

            if (mRules^[ruleCounter].FieldType=dbDate) and
               (temp1=0) and (IgnoreUndefDates) then
               Comparison:=1
            else
            if temp1 = mRules^[ruleCounter].lValue then
              Comparison := 0
            else if temp1 > mRules^[ruleCounter].lValue then
              Comparison := 1
            else
              Comparison := -1;
          end;
          dbSingle :
          begin
            temp2 := 0.0;
            mFiles^[fileCounter].FH^.dbr^.GetField(
                                mRules^[ruleCounter].FieldNo,
                                @temp2, SizeOf(temp2));
            if temp2 = mRules^[ruleCounter].sValue then
              Comparison := 0
            else if temp2 > mRules^[ruleCounter].sValue then
              Comparison := 1
            else
              Comparison := -1;
          end;
          dbDouble :
          begin
            temp3 := 0.0;
            mFiles^[fileCounter].FH^.dbr^.GetField(
                                mRules^[ruleCounter].FieldNo,
                                @temp3, SizeOf(temp3));
            if temp3 = mRules^[ruleCounter].dValue then
              Comparison := 0
            else if temp3 > mRules^[ruleCounter].dValue then
              Comparison := 1
            else
              Comparison := -1;
          end;
          else    {- dbString, dbZString, dbBlock, dbCode, dbZCode }
          begin
            Comparison := StrLIComp(temp, mRules^[ruleCounter].Value, SizeOf(temp));
          end;
        end;
        case mRules^[ruleCounter].Operator of
          MPSSEQ : Result := Comparison  =   0;
          MPSSNE : Result := Comparison  <>  0;
          MPSSGT : Result := Comparison  >   0;
          MPSSLT : Result := Comparison  <   0;
          MPSSGE : Result := Comparison  >=  0;
          MPSSLE : Result := Comparison  <=  0;
          MPSSIN : Result := StrPos(temp, mRules^[ruleCounter].Value) <> nil;
        end;
      end
      else
      begin
        if mRules^[ruleCounter].LParen or
           (ruleCounter = 0) or
           (mRules^[ruleCounter].AndOr = MPSSAnd) then
          Result := FALSE
        else
          Result := TRUE;
      end;
    end;

    if inParen then
    begin
      if mRules^[ruleCounter].LParen then
      begin
        if ruleCounter = 0 then
          reservedAndOr := MPSSAndOrSpecial
        else
          reservedAndOr := mRules^[ruleCounter].AndOr;
        ParenResult := Result
      end
      else
        case mRules^[ruleCounter].AndOr of
          MPSSAnd : ParenResult := ParenResult and Result;
          MPSSOr  : ParenResult := ParenResult or  Result;
        end;

      { short circuit parenthetical condition if possible }
      if not mRules^[ruleCounter].RParen then
      begin
        if ((mRules^[ruleCounter + 1].AndOr = MPSSAnd) and
            (ParenResult = FALSE)) or
           ((mRules^[ruleCounter + 1].AndOr = MPSSOr) and
            (ParenResult = TRUE)) then
        begin
          repeat
            Inc(ruleCounter);
          until mRules^[ruleCounter].RParen;
        end;
      end;

      if mRules^[ruleCounter].RParen then
      begin
        inParen := FALSE;
        case reservedAndOr of
          MPSSAndOrSpecial : RunningResult := ParenResult;
          MPSSAnd          : RunningResult := RunningResult and ParenResult;
          MPSSOr           : RunningResult := RunningResult or  ParenResult;
        end;
      end;
    end
    else
    begin
      if ruleCounter = 0 then
        RunningResult := Result
      else
        case mRules^[ruleCounter].AndOr of
          MPSSAnd : RunningResult := RunningResult and Result;
          MPSSOr  : RunningResult := RunningResult or  Result;
        end;
    end;

    Inc(ruleCounter);

    if not inParen then
    begin
      { short circuit if possible }
      if mRules^[ruleCounter].FH <> nil then
      begin
        if ((mRules^[ruleCounter].AndOr = MPSSAnd) and
            (RunningResult = FALSE)) or
           ((mRules^[ruleCounter].AndOr = MPSSOr) and
            (RunningResult = TRUE)) then
        begin
          Break;
        end;
      end;
    end;
  end;
  ValidRecord :=  RunningResult;
end;

procedure TMPSS.FreshenRecord(fileIdx: word);
var
  linkIdx       : word;
  ai            : TAssocInfo;
  seqNum        : TSeqNum;
  fieldCounter  : word;
begin
  if mFiles^[fileIdx].Fresh then
    Exit;

  linkIdx := mFiles^[fileIdx].LinkedBy;
  ai := mFiles^[fileIdx].LinkAssoc;

  if linkIdx = 0 then
  begin
    ShowError(nil, IDS_INITERR1, nil, 0, MOD_MPSS, 7);
    Halt;
  end;

  if ai.keyNum <> 0 then
  begin
    ShowError(nil, IDS_INITERR1, nil, 0, MOD_MPSS, 8);
    Halt;
  end;

  FreshenRecord(linkIdx); {  Oooo!  recursive!!!}

  mFiles^[linkIdx].FH^.dbr^.GetField(ai.fldNum[1], @seqNum, SizeOf(seqNum));
  if not mFiles^[fileIdx].FH^.dbc^.GetSeq(mFiles^[fileIdx].FH^.dbr, seqNum) then
    mFiles^[fileIdx].FH^.dbr^.ClearRecord;

  mFiles^[fileIdx].Fresh := TRUE;
end;

{ Set UpdateFrequency }
procedure TMPSS.SetUpdateFreq;
begin
  TotalRecords := mFiles^[1].FH^.dbc^.NumRecords;
  UpdateFrequency := 1;
  if TotalRecords >  1000 then
    UpdateFrequency := 10;
  if TotalRecords >  10000 then
    UpdateFrequency := 100;
  if TotalRecords >  100000 then
    UpdateFrequency := 1000;
  if TotalRecords >  1000000 then
    UpdateFrequency := 10000;
end;

function TMPSS.CreateFileEntry(fileName: PChar): word;
begin
  Inc(mFileCount);
  StrLCopy(mFiles^[mFileCount].FileName, fileName, 8);
  mFiles^[mFileCount].FH := New(PDBFile, Init(fileName, '', dbOpenNormal));
  if mFiles^[mFileCount].FH = nil then
  begin
    ShowError(nil, IDS_INITERR1, nil, dbLastOpenError, MOD_MPSS, 9);
    Halt;
  end;
  mFiles^[mFileCount].FH^.dbd^.FileDesc(mFiles^[mFileCount].FileDesc, SizeOf(mFiles^[1].FileDesc)-1);
  mFiles^[mFileCount].Fresh := FALSE;
  mFiles^[mFileCount].LinkedBy := 0;
  FillChar(mFiles^[mFileCount].LinkAssoc, sizeof(mFiles^[mFileCount].LinkAssoc), 0);
  CreateFileEntry := mFileCount;
end;

function TMPSS.AddFileAssoc(fileIdx: word; assocIdx: word): word;
var
  temp : word;
  ai   : TAssocInfo;
begin
  mFiles^[fileIdx].FH^.dbd^.AssocInfo(assocIdx, ai);
  temp := CreateFileEntry(ai.fName);
  mFiles^[temp].LinkAssoc := ai;
  mFiles^[temp].LinkedBy := fileIdx;
  AddFileAssoc := temp;
end;


END.
