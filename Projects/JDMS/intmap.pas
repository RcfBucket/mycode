Unit IntMap;

Interface

Uses
  DBFile,
  DBLib,
  DBTypes,
  WinTypes,
  Objects,
  ODialogs,
  OWindows,
  Strings,
  WinCrt,
  IntGlob,
  BtrAPIW,
  FileIO,
	WinProcs;

const
  id_UserList      = 101;
  id_IntList       = 102;
  id_UserDisp      = 103;
  id_IntDisp       = 104;
  id_PatientFile   = 105;
  id_SpecimenFile  = 106;
  id_IsolateFile   = 107;
	id_SetupInt      = 111;
	id_Restart       = 112;
	id_ShowMap       = 113;
	id_DMSToMain     = 114;
  id_DuplicateMap  = 115;
	id_ConfirmMap    = 116;
  id_MainToDMS     = 117;
	id_Title         = 118;
	id_MapDir        = 119;
  id_MapDefaults   = 120;
	id_V17MapDefaults= 121;

  id_SetupField    = 101;
  id_SetupComp     = 102;
  id_SetupRep      = 103;
  id_SetupName     = 104;
	id_SetupDone     = 106;
  id_FieldOK       = 107;
  id_SetupList     = 108;
  id_SetupDel      = 109;
	id_SetDefaults   = 110;
	id_SetPatientRec = 111;
	id_SetTestOrderRec= 112;
	id_SetResultRec  = 113;

  id_MapList       = 101;
	id_MapTitle      = 102;
	id_MapDelete     = 104;

	id_RestartText   = 101;
  id_RestartAll    = 102;


	NoOfIntRecords   = 2;
	SetupNoLen       = 2;
	Mapped           = 2;
	NotMapped        = 1;


  DrivingFiles: array[0..2] of PChar = ('PATIENT', 'SPECIMEN', 'ISOLATE');
  SetupFiles: array[0..NoOfIntRecords-1] of PChar = ('PSETUP.DAT', 'SSETUP.DAT');

  V17SetupFiles: array[0..2] of PChar = ('P17SET.DAT', 'B17SET.DAT', 'R17SET.DAT');

Type
	PMapDlg = ^TMapDlg;
  TMapDlg = Object(TDialog)
    IntRecNo, DMSFileNo: integer;
    UseVer17Flag: boolean;
		RecordDiv: array[0..2] of integer;
		constructor Init(ParentWnd: PWindowsObject; AppName: PChar);
    destructor Done;virtual;
		procedure SetupWindow;virtual;
		procedure FillLists;virtual;
    procedure FillIntList;virtual;
    procedure WMCommand(var Msg: TMessage);virtual wm_First + wm_Command;
		procedure HandleUserList(var Msg: TMessage); virtual id_First + id_UserList;
    procedure HandleIntList(var Msg: TMessage); virtual id_First + id_IntList;
    procedure GetSelection(Index, id_List, id_Disp: integer);virtual;
    procedure MapDMSToMain(var Msg: TMessage); virtual id_First + id_DMSToMain;
    procedure MapMainToDMS(var Msg: TMessage); virtual id_First + id_MainToDMS;
    procedure ConfirmMap(var Msg: TMessage);virtual id_First + id_ConfirmMap;
    procedure SetConfirmFlags;virtual;
    procedure ShowMap(var Msg: TMessage);virtual id_First + id_ShowMap;
    procedure RestartMap(var Msg: TMessage); virtual id_First + id_Restart;
    procedure SetupIntFields(var Msg: TMessage); virtual id_First + id_SetupInt;
		procedure SetConfirmOK;virtual;
		procedure SetDMSFileName;virtual;
		procedure FillUserList;virtual;
  	procedure DuplicateMap(var Msg: TMessage);virtual id_First + id_DuplicateMap;
		procedure UseMapDefaults(var Msg: TMessage);virtual id_First + id_MapDefaults;
		procedure Use17MapDefaults(var Msg: TMessage);virtual id_First + id_V17MapDefaults;
    procedure DeleteMapFile(ToDMS, Both: Boolean);virtual;
	end;

  PShowDlg = ^TShowDlg;
  TShowDlg = object(TDialog)
		AnyItemDeleted: Boolean;
    SecondDiv, ThirdDiv: word;
		procedure SetupWindow;virtual;
    procedure AddToShowList(aDB: PDBFile;DMSIdx, IntIdx, DivNo: integer);virtual;
  	procedure DeleteMapItem(var Msg: TMessage);virtual id_First + id_MapDelete;
		function GetRecNo(Index: Integer): integer;virtual;
  end;

 PReStartDlg = ^TReStartDlg;
 TRestartDlg = object(TDialog)
		procedure SetupWindow;virtual;
		procedure All(var Msg: TMessage);virtual id_First + id_RestartAll;
		procedure Yes(var Msg: TMessage);virtual id_First + id_Yes;
    procedure No(var Msg: TMessage);virtual id_First + id_No;
 end;

 PSetupIntDlg = ^TSetupIntDlg;
 TSetupIntDlg = Object(TDialog)
 	CurrSetupFileNo: integer;
   FieldNoS, CompNoS, RepNoS: array[0..3] of char;
   FieldName: array[0..SetupNameLen] of char;
	 procedure WMCommand(var Msg: TMessage);virtual wm_First + wm_Command;
	 procedure UseDefaults(var Msg: TMessage);virtual id_First + id_SetDefaults;
   procedure FieldOK(var Msg: TMessage);virtual id_First + id_FieldOK;
   procedure SetupDone(var Msg: TMessage);virtual id_First + id_SetupDone;
   procedure SetupDelete(var Msg: TMessage);virtual id_First + id_SetupDel;
   procedure HandleList(var Msg: TMessage); virtual id_First + id_SetupList;
   procedure SetupWindow;virtual;
   procedure SeperateDataItem(Index: integer);virtual;
	 procedure GetSetupFileData;
   procedure PutSetupFileData;
   function CombineDataItem(FieldNo, CompNo, RepNo: integer): longint;virtual;
   function CheckForBlankFields: Boolean;virtual;
   procedure GetFields;virtual;
	 procedure AssignSetupFile;virtual;
   procedure DisplayFieldData;virtual;
 end;

var
 	SetupFile: Text;
  ToDMSSelected: Boolean;
  MapDlg: PMapDlg;
  FileName: string[8];

procedure IntMap_ShowMapFields(ParentWnd: HWnd; ParentWndObj: PWindowsObject);


Implementation

uses
  APITools,
  DBIDs,
  DMString,
  MScan;

procedure InitializeSetupFiles(SetupFileExt: PChar);
var
  DefaultFile: Text;
  DefaultName: array[0..12] of char;
  FieldNo, CompNo, RepNo, i: integer;
  TFieldName: array[0..SetupNameLen] of char;
  BlankChar: char;

begin
	for i := 0 to NoOfIntRecords-1 do
  begin
    Assign(SetupFile, SetupFiles[i]);
    Rewrite(SetupFile);
    StrLCopy(DefaultName, SetupFiles[i], 6);
    StrCat(DefaultName, SetupFileExt);
    Assign(DefaultFile, DefaultName);
    Reset(DefaultFile);
    while not Eof(DefaultFile) do
    begin
      readln(DefaultFile, FieldNo, CompNo, RepNo, BlankChar, TFieldName);
      writeln(SetupFile, FieldNo, ' ', CompNo, ' ', RepNo, ' ', TFieldName);
		end;
    Close(SetupFile);
    Close(DefaultFile);
  end;
end;

{
******************
SetupIntDlg Object
******************
}

procedure TSetupIntDlg.SetupWindow;
begin
  inherited SetupWindow;
  SendDlgItemMsg(id_SetupField, em_LimitText, word(SetupNoLen), 0);
  SendDlgItemMsg(id_SetupComp, em_LimitText, word(SetupNoLen), 0);
  SendDlgItemMsg(id_SetupRep, em_LimitText, word(SetupNoLen), 0);
  SendDlgItemMsg(id_SetupName, em_LimitText, word(SetupNameLen), 0);
  CurrSetupFileNo := id_SetPatientRec;
  Assign(SetupFile, SetupFiles[0]);
  SendDlgItemMsg(id_SetPatientRec, bm_SetCheck, word(1), 0);
  Reset(SetupFile);
  GetSetupFileData;
	Close(SetupFile);
end;

procedure TSetupIntDlg.DisplayFieldData;
var
	EnableField: word;

begin
  EnableField := 1;
  SendDlgItemMsg(id_SetupName, wm_SetText, 0, longint(@FieldName));
  SendDlgItemMsg(id_SetupField, wm_SetText, 0, longint(@FieldNoS));
  SendDlgItemMsg(id_SetupComp, wm_SetText, 0, longint(@CompNoS));
  SendDlgItemMsg(id_SetupRep, wm_SetText, 0, longint(@RepNoS));
  SendDlgItemMsg(id_SetupField, wm_Enable, EnableField, 0);
  SendDlgItemMsg(id_SetupComp, wm_Enable, EnableField, 0);
  SendDlgItemMsg(id_SetupRep, wm_Enable, EnableField, 0);
end;

procedure TSetupIntDlg.GetSetupFileData;
var
  FieldNo, CompNo, RepNo, Index: integer;
  BlankChar: char;

begin
  SendDlgItemMsg(id_SetupList, wm_SetRedraw, 0, 0);
  SendDlgItemMsg(id_SetupList, lb_ResetContent, 0, 0);
	while not Eof(SetupFile) do
  begin
    Readln(SetupFile, FieldNo, CompNo, RepNo, BlankChar, FieldName);
    Index := SendDlgItemMsg(id_SetupList, lb_AddString, 0, longint(@FieldName));
    SendDlgItemMsg(id_SetupList, lb_SetItemData, Index, CombineDataItem(FieldNo, CompNo, RepNo));
  end;
  SeperateDataItem(0);
  SendDlgItemMsg(id_SetupList, wm_SetRedraw, word(1), 0);
  InvalidateRect(GetItemHandle(id_SetupList), Nil, True);
	DisplayFieldData;
end;

procedure TSetupIntDlg.PutSetupFileData;
var
  FieldNo, CompNo, RepNo, Result, Count, i : integer;

begin
  Rewrite(SetupFile);
  Count := SendDlgItemMsg(id_SetupList, lb_GetCount, 0, 0);
  for i := 0 to Count-1 do
  begin
    SeperateDataItem(i);
    Val(FieldNoS, FieldNo, Result);
    Val(CompNoS, CompNo, Result);
    Val(RepNoS, RepNo, Result);
    writeln(SetupFile, FieldNo, ' ', CompNo, ' ', RepNo, ' ', FieldName);
  end;
	Close(SetupFile);
end;

procedure TSetupIntDlg.UseDefaults(var Msg: TMessage);
var
	MessageText: array[0..120] of char;
  TempMsg: array[0..50] of char;
  TempVerMsg: array[0..30] of char;
  LastCursor: HCursor;

begin
  LoadString(HInstance, str_IntBase+str_DelFieldDef1, TempMsg, SizeOf(TempMsg));
  StrCopy(MessageText, TempMsg);
  StrCat(MessageText, #13#10);

  LoadString(HInstance, str_IntBase+str_DelFieldDef2, TempMsg, SizeOf(TempMsg));
  StrCat(MessageText, TempMsg);
  StrCat(MessageText, #13#10);

  LoadString(HInstance, str_IntBase+str_Continue, TempMsg, SizeOf(TempMsg));
  StrCat(MessageText, TempMsg);

  LoadString(HInstance, str_IntBase+str_IntMsg, TempVerMsg, SizeOf(TempVerMsg));
  if MessageBox(HWindow, MessageText, TempVerMsg, mb_YesNo) = id_Yes then
  begin
    LastCursor := SetCursor(LoadCursor(0, idc_Wait));
		InitializeSetupFiles('.DEF');
		AssignSetupFile;
		Reset(SetupFile);
		GetSetupFileData;
    Close(SetupFile);
    SetCursor(LastCursor);
  end;
end;

function TSetupIntDlg.CombineDataItem(FieldNo, CompNo, RepNo: integer): longint;
begin
  CombineDataItem := (longint(FieldNo) * longint(10000)) + (longint(CompNo) * longint(100)) + longint(RepNo);
end;

procedure TSetupIntDlg.SeperateDataItem(Index: integer);
var
  TempL: longint;
  TempS: integer;

begin
  SendDlgItemMsg(id_SetupList, lb_GetText, Index, longint(@FieldName));
  TempL := SendDlgItemMsg(id_SetupList, lb_GetItemData, Index, 0);
  TempS := integer(TempL div longint(10000));
  Str(TempS, FieldNoS);
  TempL := TempL - (longint(TempS) * longint(10000));
  TempS := integer(TempL div longint(100));
  Str(TempS, CompNoS);
  TempL := TempL - (longint(TempS) * longint(100));
  Str(integer(TempL), RepNoS);
end;

function TSetupIntDlg.CheckForBlankFields: Boolean;
var
   TempVerMsg: array[0..30] of char;
   TempMsg: array[0..40] of char;

begin
  CheckForBlankFields := True;
  GetFields;
  if (StrComp(FieldNoS, '') = 0) or (StrComp(CompNoS, '') = 0) or
     (StrComp(RepNoS, '') = 0) or (StrComp(FieldName, '') = 0) then
  begin
    LoadString(HInstance, str_IntBase+str_BlankField, TempMsg, SizeOf(TempMsg));
    LoadString(HInstance, str_IntBase+str_IntMsg, TempVerMsg, SizeOf(TempVerMsg));
    MessageBox(HWindow, TempMsg, TempVerMsg, mb_ok);
  end
  else
    CheckForBlankFields := False;
end;

procedure TSetupIntDlg.GetFields;
begin
  SendDlgItemMsg(id_SetupField, wm_GetText, SetupNoLen+1, longint(@FieldNoS));
  SendDlgItemMsg(id_SetupComp, wm_GetText, SetupNoLen+1, longint(@CompNoS));
  SendDlgItemMsg(id_SetupRep, wm_GetText, SetupNoLen+1, longint(@RepNoS));
  SendDlgItemMsg(id_SetupName, wm_GetText, SetupNameLen+1, longint(@FieldName));
end;

procedure TSetupIntDlg.FieldOK(var Msg: TMessage);
var
  FieldNo, CompNo, RepNo, Index, Count, i: integer;
  TempDataItem, TempL: longint;
  TempVerMsg: array[0..30] of char;
  TempMsg: array[0..50] of char;

begin
  if not CheckForBlankFields then
  begin
    Val(FieldNoS, FieldNo, i);
    Val(CompNoS, CompNo, i);
    Val(RepNoS, RepNo, i);
    if (FieldNo <> 0) and (CompNo <> 0) and (RepNo <> 0) then
    begin
      Count := SendDlgItemMsg(id_SetupList, lb_GetCount, 0, 0);
      TempDataItem := CombineDataItem(FieldNo, CompNo, RepNo);
      i := 0;
      TempL := SendDlgItemMsg(id_SetupList, lb_GetItemData, word(i), 0);
      while (i <= Count-1) and (TempL < TempDataItem) and (Count <> 0) do
      begin
        i := i + 1;
        TempL := SendDlgItemMsg(id_SetupList, lb_GetItemData, word(i), 0);
      end;
      Index := i;
      if (SendDlgItemMsg(id_SetupList, lb_GetItemData, i, 0) = TempDataItem) and
         (Count <> 0) then
        SendDlgItemMsg(id_SetupList, lb_DeleteString, i, 0);
      SendDlgItemMsg(id_SetupList, lb_InsertString, Index, longint(@FieldName));
      SendDlgItemMsg(id_SetupList, lb_SetItemData, Index, CombineDataItem(FieldNo, CompNo, RepNo));
    end
    else
    begin
      LoadString(HInstance, str_IntBase+str_BadNum, TempMsg, SizeOf(TempMsg));
      LoadString(HInstance, str_IntBase+str_IntMsg, TempVerMsg, SizeOf(TempVerMsg));
      MessageBox(HWindow, TempMsg, TempVerMsg, mb_ok);
    end;
  end;
end;

procedure TSetupIntDlg.SetupDone(var Msg: TMessage);
begin
  PutSetupFileData;
  EndDlg(0);
end;

procedure TSetupIntDlg.SetupDelete(var Msg: TMessage);
const
  BlankStr: string[2] = '';

var
  Index: integer;
	OkToDelete: boolean;
  TempMsg: array[0..35] of char;
  TempVerMsg: array[0..30] of char;

begin
  Index := SendDlgItemMsg(id_SetupList, lb_GetCurSel, 0, 0);
  if Index  = lb_Err then
  begin
    LoadString(HInstance, str_IntBase+str_SelItem, TempMsg, SizeOf(TempMsg));
    LoadString(HInstance, str_IntBase+str_IntMsg, TempVerMsg, SizeOf(TempVerMsg));
    MessageBox(HWindow, TempMsg, TempVerMsg, mb_OK);
  end
  else
	begin
		SeperateDataItem(Index);
    OkToDelete := True;
    if (CurrSetupFileNo = id_SetPatientRec) and (StrComp(FieldNoS, '3') = 0) then
			OkToDelete := False;
    if (CurrSetupFileNo = id_SetTestOrderRec) and (StrComp(FieldNoS, '3') = 0) then
      if (StrComp(CompNoS, '1') = 0) or (StrComp(CompNoS, '2') = 0) then
				OkToDelete := False;
    if (CurrSetupFileNo = id_SetTestOrderRec) and (StrComp(FieldNoS, '5') = 0) and
       (StrComp(CompNoS, '4') = 0) then
				OkToDelete := False;
    if (CurrSetupFileNo = id_SetTestOrderRec) and (StrComp(FieldNoS, '8') = 0) then
			OkToDelete := False;
		if OkToDelete then
    begin
      SendDlgItemMsg(id_SetupList, lb_DeleteString, Index, 0);
      SendDlgItemMsg(id_SetupField, wm_SetText, 0, longint(@BlankStr));
      SendDlgItemMsg(id_SetupComp, wm_SetText, 0, longint(@BlankStr));
      SendDlgItemMsg(id_SetupRep, wm_SetText, 0, longint(@BlankStr));
      SendDlgItemMsg(id_SetupName, wm_SetText, 0, longint(@BlankStr));
		end
		else
    begin
      LoadString(HInstance, str_IntBase+str_NoDel, TempMsg, SizeOf(TempMsg));
      LoadString(HInstance, str_IntBase+str_IntMsg, TempVerMsg, SizeOf(TempVerMsg));
      MessageBox(HWindow, TempMsg, TempVerMsg, mb_OK);
    end;
  end;
end;

procedure TSetupIntDlg.AssignSetupFile;
begin
	case CurrSetupFileNo of
    id_SetPatientRec: Assign(SetupFile, SetupFiles[0]);
    id_SetTestOrderRec: Assign(SetupFile, SetupFiles[1]);
	end;
end;

procedure TSetupIntDlg.WMCommand(var Msg: TMessage);
begin
  if (Msg.wParam = id_SetPatientRec) or
     (Msg.wParam = id_SetTestOrderRec) then
	begin
    PutSetupFileData;
    CurrSetupFileNo := Msg.wParam;
    AssignSetupFile;
		Reset(SetupFile);
		GetSetupFileData;
		Close(SetupFile);
  end;
  inherited WMCommand(Msg);
end;

procedure TSetupIntDlg.HandleList(var Msg: TMessage);
var
  SelIndex: integer;
  SelectText: array[0..SetupNameLen] of char;
begin
  case Msg.lParamHi of
    lbn_SelChange:
    begin
      SelIndex := SendDlgItemMsg(id_SetupList, lb_GetCurSel, 0, 0);
      if (SelIndex > -1) then
      begin
        SendDlgItemMsg(id_SetupList, lb_GetText, SelIndex, longint(@FieldName));
        SeperateDataItem(SelIndex);
        SendDlgItemMsg(id_SetupField, wm_SetText, 0, longint(@FieldNoS));
        SendDlgItemMsg(id_SetupComp, wm_SetText, 0, longint(@CompNoS));
        SendDlgItemMsg(id_SetupRep, wm_SetText, 0, longint(@RepNoS));
        SendDlgItemMsg(id_SetupName, wm_SetText, 0, longint(@FieldName));
      end;
    end;
  end;
end;

{
*****************
ReStartDlg Object
*****************
}

procedure TReStartDlg.SetupWindow;
var
	MessageText: array[0..10] of char;
  TempMsg: array[0..20] of char;

begin
  inherited SetupWindow;
	if ToDMSSelected then
    LoadString(HInstance, str_IntBase+str_DMS, TempMsg, SizeOf(TempMsg))
  else
    LoadString(HInstance, str_IntBase+str_Int, TempMsg, SizeOf(TempMsg));
  StrCopy(MessageText, TempMsg);
  StrCat(MessageText, '.');
  SendDlgItemMsg(id_RestartText, wm_SetText, 0, longint(@MessageText));
end;

procedure TReStartDlg.Yes(var Msg: TMessage);
begin
	EndDlg(id_Yes);
end;

procedure TReStartDlg.No(var Msg: TMessage);
begin
	EndDlg(id_No);
end;

procedure TReStartDlg.All(var Msg: TMessage);
begin
  EndDlg(id_ReStartAll);
end;

{**************
ShowDlg Object
**************
}

function TShowDlg.GetRecNo(Index: integer): integer;
var
	RecNo: integer;

begin
	RecNo := 0;
	if Index > SecondDiv then
		RecNo := 1;
	if (Index > ThirdDiv) and (MapDlg^.UseVer17Flag) then
		RecNo := 2;
	GetRecNo := RecNo;
end;

procedure TShowDlg.DeleteMapItem(var Msg: TMessage);
type
	TMapIdx = record
		case integer of
			0: (
				LParam: longint);
			1: (
				LParamLo: word;
				LParamHi: word);
		end;

var
  Index, DMSIdx, IntIdx: integer;
	ToDMSMapRec: TToDMSMapRec;
	ToIntMapRec: TToIntMapRec;
	ToDMSMapKey: TToDMSMapKey;
  ToIntMapKey: TToIntMapKey;
	MapIdx: TMapIdx;
  TempMsg: array[0..35] of char;
  TempVerMsg: array[0..30] of char;

begin
  Index := SendDlgItemMsg(id_MapList, lb_GetCurSel, 0, 0);
  if  (Index <> lb_Err) then
  begin
    if (Index <> 0) and (Index <> SecondDiv) and (Index <> ThirdDiv) then
    begin
      AnyItemDeleted := True;
      MapIdx.LParam := SendDlgItemMsg(id_MapList, lb_GetItemData, word(Index), 0);
      DMSIdx := integer(MapIdx.LParamHi);
      IntIdx := MapIdx.LParamLo;
      if ToDMSSelected then
      begin
        ToDMSMapKey.DMSFileNo := MapDlg^.DMSFileNo;
        ToDMSMapKey.IntRecNo := GetRecNo(Index);
        ToDMSMapKey.DMSIdxNo := DMSIdx;
        ToDMSMapKey.IntIdxNo := IntIdx;
        BtrvStat := CallDB(B_Get_EQU, ToDMSMapFile, ToDMSMapRec.Start, ToDMSMapKey.Start, 0);
        BtrvStat := CallDB(B_Delete, ToDMSMapFile, ToDMSMapRec.Start, ToDMSMapKey.Start, 0);
      end
      else
      begin
        ToIntMapKey.DMSFileNo := MapDlg^.DMSFileNo;
        ToIntMapKey.IntRecNo := GetRecNo(Index);
        ToIntMapKey.DMSIdxNo := DMSIdx;
        ToIntMapKey.IntIdxNo := IntIdx;
        BtrvStat := CallDB(B_Get_EQU, ToIntMapFile, ToIntMapRec.Start, ToIntMapkey.Start, 0);
        BtrvStat := CallDB(B_Delete, ToIntMapFile, ToIntMapRec.Start, ToIntMapkey.Start, 0);
      end;
      SendDlgItemMsg(id_MapList, lb_DeleteString, Index, 0);
      if (Index < ThirdDiv) then
        Dec(ThirdDiv);
      if (Index < SecondDiv) then
        Dec(SecondDiv);
    end;
	end
	else
  begin
    LoadString(HInstance, str_IntBase+str_SelItem, TempMsg, SizeOf(TempMsg));
    LoadString(HInstance, str_IntBase+str_IntMsg, TempVerMsg, SizeOf(TempVerMsg));
    MessageBox(HWindow, TempMsg, TempVerMsg, mb_OK);
  end;
end;

procedure TShowDlg.AddToShowList(aDB: PDBFile;DMSIdx, IntIdx, DivNo: integer);
type
	TMapIdx = record
		case integer of
			0: (
				LParam: longint);
			1: (
				LParamLo: word;
				LParamHi: word);
	end;


var
  aField: TFldInfo;
  ListText: array[0..50] of char;
  i, FieldNo, CompNo, RepNo, Index, DivIdx: integer;
  FieldName: array[0..SetupNameLen] of char;
  BlankChar: char;
  MapIdx: TMapIdx;

begin
	case DivNo of
		0: begin
         DivIdx := SecondDiv;
				 SecondDiv := SecondDiv + 1;
				 ThirdDiv := ThirdDiv + 1;
			 end;
		1: begin
				 DivIdx := ThirdDiv;
				 ThirdDiv := ThirdDiv + 1;
			 end;
		2: begin
				 DivIdx := -1;
			 end;
	end;
  StrCopy(ListText, '');
  if (DMSIdx > 0) then
  begin
    if not aDB^.dbd^.FieldInfo(DMSIdx, aField) then
      Exit;
    StrCopy(ListText, aField.fldName);
(*  end*)
(*  else*)
(*  begin*)
(*    case DivNo of*)
(*      0:  if (DMSIdx = -1) then*)
(*            StrCopy(ListText, 'Patient ID');*)
(*      1:  if (DMSIdx = -1) then*)
(*            StrCopy(ListText, 'Specimen ID')*)
(*          else if (DMSIdx = -2) then*)
(*            StrCopy(ListText, 'Collect Date');*)
(*    end;*)
  end;
  for i := StrLen(ListText) to SetupNameLen do
    ListText[i] := ' ';
  ListText[i+1] := #0;
	if MapDlg^.UseVer17Flag then
    Assign(SetupFile, V17SetupFiles[DivNo])
	else
		if DivNo <= 1 then
      Assign(SetupFile, SetupFiles[DivNo]);
	Reset(SetupFile);
  for i := 0 to IntIdx do
    Readln(SetupFile, FieldNo, CompNo, RepNo, BlankChar, FieldName);
  Close(SetupFile);
  StrCat(ListText, FieldName);
  Index := SendDlgItemMsg(id_MapList, lb_InsertString, word(DivIdx), longint(@ListText));
  MapIdx.LParamHi := word(DmsIdx);
	MapIdx.LParamLo := IntIdx;
  SendDlgItemMsg(id_MapList, lb_SetItemData, Index, MapIdx.LParam);
end;

procedure TShowDlg.SetupWindow;
var
  DMSIdx, IntIdx, i, j: integer;
  aDB: PDBFile;
  MapTitle: array[0..40] of char;
	ListFont: HFont;
  Index: word;
	ListText: array[0..50] of char;
	ToDMSMapRec: TToDMSMapRec;
	ToIntMapRec: TToIntMapRec;
	DMSKey: TDMSKey;
	IntKey: TIntKey;
  ToDMSMapKey: TToDMSMapKey;
	NoOfRecs: integer;
  TempMsg: array[0..40] of char;
  pstr: array[0..100] of char;

begin
  inherited SetupWindow;
  AnyItemDeleted := False;
	SecondDiv := 1;
	ThirdDiv := 2;
	ListFont := GetStockObject(SYSTEM_FIXED_FONT);
  SendDlgItemMsg(id_MapList, wm_SetFont, ListFont, 0);
  aDB:= New(PDBFile, Init(StrPCopy(pstr, FileName), '', dbOpenNormal));
	if MapDlg^.UseVer17Flag then
		NoOfRecs := 3
	else
  	NoOfRecs := NoOfIntRecords;
	for i := 0 to NoOfRecs-1 do
	begin
		for j := 0 to SetupNameLen do
  	  ListText[j] := ' ';
		ListText[j+1] := #0;
		if MapDlg^.UseVer17Flag then
    begin
      J := i-1;
      if i = 0 then
         j := -2;
      LoadString(HInstance, str_IntBase+str_V17Files+j, TempMsg, SizeOf(TempMsg));
      StrCat(ListText, TempMsg);
    end
    else
    begin
      LoadString(HInstance, str_IntBase+str_Files+i, TempMsg, SizeOf(TempMsg));
      StrCat(ListText, TempMsg);
    end;
    Index := SendDlgItemMsg(id_MapList, lb_InsertString, i, longint(@ListText));
    SendDlgItemMsg(id_MapList, lb_SetItemData, Index, -1);
	end;
	if ToDMSSelected then
  begin
    LoadString(HInstance, str_IntBase+str_IntToDMS, TempMsg, SizeOf(TempMsg));
    StrCopy(MapTitle, TempMsg);
    BtrvStat := CallDB(B_Get_Low, ToDMSMapFile, ToDMSMapRec.Start, IntKey.Start, 1);
		while BtrvStat = 0 do
   	begin
     if ToDMSMapRec.DMSFileNo = MapDlg^.DMSFileNo then
        AddToShowList(aDB, ToDMSMapRec.DMSIdxNo, ToDMSMapRec.IntIdxNo, ToDMSMapRec.IntRecNo);
     BtrvStat := CallDB(B_Next, ToDMSMapFile, ToDMSMapRec.Start, IntKey.Start, 1);
		end;
	end
  else
  begin
    LoadString(HInstance, str_IntBase+str_DMSToInt, TempMsg, SizeOf(TempMsg));
    StrCopy(MapTitle, TempMsg);
    BtrvStat := CallDB(B_Get_Low, ToIntMapFile, ToIntMapRec.Start, DMSKey.Start, 1);
		while BtrvStat = 0 do
   	begin
      if ToIntMapRec.DMSFileNo = MapDlg^.DMSFileNo then
        AddToShowList(aDB, ToIntMapRec.DMSIdxNo, ToIntMapRec.IntIdxNo, ToIntMapRec.IntRecNo);
      BtrvStat := CallDB(B_Next, ToIntMapFile, ToIntMapRec.Start, DMSKey.Start, 1);
		end;
	end;
  SendDlgItemMsg(id_MapTitle, wm_SetText, 0, longint(@MapTitle));
  MSDisposeObj(aDB);
end;

{
*************
MapDlg Object
*************
}

constructor TMapDlg.Init(ParentWnd: PWindowsObject; AppName: PChar);
var
	tstr: array[0..3] of char;
	i: integer;
  buttonPtr: PButton;

begin
  inherited Init(ParentWnd, AppName);
	IntRecNo := 0;
  DMSFileNo := 0;
	ToDMSSelected := True;
	BtrvData[ToDMSMapFile].Size.Len       := SizeOf(TToDMSMapRec);
  BtrvData[ToDMSMapFile].FileName.Name  := 'TODMSMAP.DAT  ';
	BtrvData[ToIntMapFile].Size.Len       := SizeOf(TToIntMapRec);
  BtrvData[ToIntMapFile].FileName.Name  := 'TOINTMAP.DAT  ';
  OpenCloseFiles(HWindow, B_Open, [ToDMSMapFile, ToIntMapFile]);
	{Make Sure Setupfiles exist}
	for i := 0 to NoOfIntRecords-1 do
	begin
    Assign(SetupFile, SetupFiles[i]);
		{$I-}Reset(SetupFile);{$I+}
		if IOResult <> 0 then
			Rewrite(SetupFile);
		Close(SetupFile);
  end;
  New(buttonPtr, InitResource(@Self, id_V17MapDefaults));
{Removed v17 (v18) support - kmr 10/95}
(*  GetPrivateProfileString('IntCstm', 'UseVer17', '', tstr, SizeOf(tstr), 'IntCstm.ini');*)
(*  if StrComp(tstr, 'Yes') = 0 then*)
(*    UseVer17Flag := True*)
(*  else*)
    UseVer17Flag := False;
end;

destructor TMapDlg.Done;
var
	tstr: array[0..3] of char;

begin
  OpenCloseFiles(HWindow, B_Close, [ToDMSMapFile, ToIntMapFile]);
	if UseVer17Flag then
    StrCopy(tstr, 'Yes')
	else
    StrCopy(tstr, 'No');
  WritePrivateProfileString('IntCstm', 'UseVer17', tstr, 'IntCstm.ini');
  inherited Done;
end;

procedure TMapDlg.SetupWindow;
const
  StaticText: array[0..3] of char = '<-';

var
  MapTitle: array[0..16] of char;
  TempMsg: array[0..30] of char;

begin
  inherited SetupWindow;
  SendDlgItemMsg(id_PatientFile, bm_SetCheck, word(1), 0);
  SendDlgItemMsg(id_MapDir, wm_SetText, 0, longint(@StaticText));
  EnableWindow(GetDlgItem(HWindow, id_ConfirmMap), False);
  LoadString(HInstance, str_IntBase+str_IntToDMS, TempMsg, SizeOf(TempMsg));
  StrCopy(MapTitle, TempMsg);
  SendDlgItemMsg(id_MainToDMS, bm_SetCheck, 1, 0);
  SendDlgItemMsg(id_DMSToMain, bm_SetCheck, 0, 0);
  SendDlgItemMsg(id_Title, wm_SetText, 0, longint(@MapTitle));
  {Removed v17 (v18) support - kmr 10/95}
  EnableWindow(GetDlgItem(HWindow, id_V17MapDefaults), False);
  FillLists;
end;

procedure TMapDlg.FillIntList;
var
  FieldNo, CompNo, RepNo, Index, i, j, NoOfRecs: integer;
  BlankChar: char;
  FieldName: array[0..SetupNameLen] of char;
  TempMsg: array[0..40] of char;

begin
  SendDlgItemMsg(id_IntList, wm_SetRedraw, 0, 0);
  SendDlgItemMsg(id_IntList, lb_ResetContent, 0, 0);
	if UseVer17Flag then
		NoOfRecs := 3
	else
		NoOfRecs := NoOfIntRecords;
	for i := 0 to NoOfRecs-1 do
  begin
		if UseVer17Flag then
		begin
      Assign(SetupFile, V17SetupFiles[i]);
			Reset(SetupFile);
      j := i-1;
      if i = 0 then
        j := -2;
      LoadString(HInstance, str_IntBase+str_V17Files+j, TempMsg, SizeOf(TempMsg));
      Index := SendDlgItemMsg(id_IntList, lb_AddString, 0, longint(@TempMsg));
		end
		else
    begin
      Assign(SetupFile, SetupFiles[i]);
			Reset(SetupFile);
      LoadString(HInstance, str_IntBase+str_Files+i, TempMsg, SizeOf(TempMsg));
      Index := SendDlgItemMsg(id_IntList, lb_AddString, 0, longint(@TempMsg));
		end;
    SendDlgItemMsg(id_IntList, lb_SetItemData, Index, MakeLong(Index, word(-1)));
    RecordDiv[i] := Index;
  	while not Eof(SetupFile) do
 		begin
      readln(SetupFile, FieldNo, CompNo, RepNo, BlankChar, FieldName);
      Index := SendDlgItemMsg(id_IntList, lb_AddString, 0, longint(@FieldName));
      SendDlgItemMsg(id_IntList, lb_SetItemData, Index, MakeLong(Index, NotMapped));
  	end;
  	Close(SetupFile);
  end;
  SendDlgItemMsg(id_IntList, wm_SetRedraw, word(1), 0);
  SendDlgItemMsg(id_IntList, lb_SetCurSel, 0, 0);
  InvalidateRect(GetItemHandle(id_IntList), Nil, True);
end;

procedure TMapDlg.FillUserList;
var
  aDB: PDBFile;
  aField: TFldInfo;
  FieldNo: Word;
  Index: integer;
  pstr: array[0..100] of char;

begin
  SetDMSFileName;
  SendDlgItemMsg(id_UserList, wm_SetRedraw, 0, 0);
  SendDlgItemMsg(id_UserList, lb_ResetContent, 0, 0);
  aDB:= New(PDBFile, Init(StrPCopy(pstr, FileName), '', dbOpenNormal));
(*  if (StrIComp(StrPCopy(pstr, FileName), DBSPECFile) = 0) then*)
(*  begin*)
(*    StrCopy(pstr, 'Patient ID');*)
(*    Index := SendDlgItemMsg(id_UserList, lb_InsertString, word(-1), longint(@pstr));*)
(*    SendDlgItemMsg(id_UserList, lb_SetItemData, Index, MakeLong(word(-1), NotMapped));*)
(*  end*)
(*  else if (StrIComp(StrPCopy(pstr, FileName), DBISOFile) = 0) then*)
(*  begin*)
(*    StrCopy(pstr, 'Specimen ID');*)
(*    Index := SendDlgItemMsg(id_UserList, lb_InsertString, word(-1), longint(@pstr));*)
(*    SendDlgItemMsg(id_UserList, lb_SetItemData, Index, MakeLong(word(-1), NotMapped));*)
(*    StrCopy(pstr, 'Collect Date');*)
(*    Index := SendDlgItemMsg(id_UserList, lb_InsertString, word(-1), longint(@pstr));*)
(*    SendDlgItemMsg(id_UserList, lb_SetItemData, Index, MakeLong(word(-2), NotMapped));*)
(*  end;*)
  for FieldNo := 1 to aDB^.dbd^.NumFields do
  begin
    aDB^.dbd^.FieldInfo(FieldNo, aField);
    if (aField.userFlag and db_Hidden <> db_Hidden) then
    begin
      Index := SendDlgItemMsg(id_UserList, lb_InsertString, word(-1), longint(@aField.fldName));
      SendDlgItemMsg(id_UserList, lb_SetItemData, Index, MakeLong(FieldNo, NotMapped));
    end;
  end;
  SendDlgItemMsg(id_UserList, wm_SetRedraw, word(1), 0);
  SendDlgItemMsg(id_UserList, lb_SetCurSel, 0, 0);
  InvalidateRect(GetItemHandle(id_UserList), Nil, True);
  MSDisposeObj(aDB);
end;

procedure TMapDlg.FillLists;
const
	BlankStr: string[2] = '';

begin
	FillIntList;
  FillUserList;
  SetConfirmFlags;
  SendDlgItemMsg(id_UserDisp, wm_SetText, 0, longint(@BlankStr));
  SendDlgItemMsg(id_IntDisp, wm_SetText, 0, longint(@BlankStr));
  EnableWindow(GetDlgItem(HWindow, id_ConfirmMap), False);
end;

procedure TMapDlg.Use17MapDefaults(var Msg: TMessage);
var
	Prompt: array[0..92] of char;
  MapDefFile: Text;
	ToDMSMapRec: TToDMSMapRec;
	ToDMSMapKey: TToDMSMapKey;
  TempMsg: array[0..75] of char;
  TempVerMsg: array[0..30] of char;
  LastCursor: HCursor;

begin
{Removed v17 (v18) support - kmr 10/95}
(*  UseVer17Flag:= True;*)
(*  LoadString(HInstance, str_IntBase+str_V17DelMap1, TempMsg, SizeOf(TempMsg));*)
(*  StrCopy(Prompt, TempMsg);*)
(*  StrCat(Prompt, #13#10);*)
(*  LoadString(HInstance, str_IntBase+str_V17DelMap2, TempMsg, SizeOf(TempMsg));*)
(*  StrCat(Prompt, TempMsg);*)
(*  StrCat(Prompt, #13#10);*)
(*  LoadString(HInstance, str_IntBase+str_Continue, TempMsg, SizeOf(TempMsg));*)
(*  StrCat(Prompt, TempMsg);*)
(*  LoadString(HInstance, str_IntBase+str_IntMsg, TempVerMsg, SizeOf(TempVerMsg));*)
(*  if MessageBox(HWindow, Prompt, TempVerMsg, mb_YesNo) = id_Yes then*)
(*  begin*)
(*    LastCursor := SetCursor(LoadCursor(0, idc_Wait));*)
(*    DeleteMapFile(True, True);*)
(*    Assign(MapDefFile, 'V17MAP.DEF');*)
(*    Reset(MapDefFile);*)
(*    while not Eof(MapDefFile) do*)
(*    begin*)
(*      with ToDMSMapRec do*)
(*      begin*)
(*        Readln(MapDefFile, IntRecNo, IntIdxNo, DMSFileNo, DMSIdxNo);*)
(*        ToDMSMapKey.IntRecNo := IntRecNo;*)
(*        ToDMSMapKey.IntIdxNo := IntIdxNo;*)
(*        ToDMSMapKey.DMSFileNo := DMSFileNo;*)
(*        ToDMSMapKey.DMSIdxNo := DMSIdxNo;*)
(*      end;*)
(*      BtrvStat := CallDB(B_Insert, ToDMSMapFile, ToDMSMapRec.Start, ToDMSMapKey.Start, 0);*)
(*    end;*)
(*    Close(MapDefFile);*)
(*    FillLists;*)
(*    SetCursor(LastCursor);*)
(*  end;*)
end;

procedure TMapDlg.UseMapDefaults(var Msg: TMessage);
var
	Prompt: array[0..92] of char;
  MapDefFile: Text;
	ToDMSMapRec: TToDMSMapRec;
	ToIntMapRec: TToIntMapRec;
	ToIntMapKey: TToIntMapKey;
	ToDMSMapKey: TToDMSMapKey;
  TempMsg: array[0..60] of char;
  TempVerMsg: array[0..30] of char;
  LastCursor: HCursor;

begin
  LoadString(HInstance, str_IntBase+str_DelMap1, TempMsg, SizeOf(TempMsg));
  StrCopy(Prompt, TempMsg);
  StrCat(Prompt, #13#10);
  LoadString(HInstance, str_IntBase+str_DelMap2, TempMsg, SizeOf(TempMsg));
  StrCat(Prompt, TempMsg);
  StrCat(Prompt, #13#10);
  LoadString(HInstance, str_IntBase+str_Continue, TempMsg, SizeOf(TempMsg));
  StrCat(Prompt, TempMsg);
  LoadString(HInstance, str_IntBase+str_IntMsg, TempVerMsg, SizeOf(TempVerMsg));
  if MessageBox(HWindow, Prompt, TempVerMsg, mb_YesNo) = id_Yes then
	begin
    LastCursor := SetCursor(LoadCursor(0, idc_Wait));
    DeleteMapFile(True, True);

    Assign(MapDefFile, 'TODMSMAP.DEF');
		Reset(MapDefFile);
		while not Eof(MapDefFile) do
		begin
			with ToDMSMapRec do
      begin
        Readln(MapDefFile, IntRecNo, IntIdxNo, DMSFileNo, DMSIdxNo);
				ToDMSMapKey.IntRecNo := IntRecNo;
				ToDMSMapKey.IntIdxNo := IntIdxNo;
				ToDMSMapKey.DMSFileNo := DMSFileNo;
    		ToDMSMapKey.DMSIdxNo := DMSIdxNo;
      end;
      BtrvStat := CallDB(B_Insert, ToDMSMapFile, ToDMSMapRec.Start, ToDMSMapKey.Start, 0);
		end;
		Close(MapDefFile);

    Assign(MapDefFile, 'TOINTMAP.DEF');
		Reset(MapDefFile);
		while not Eof(MapDefFile) do
		begin
			with ToIntMapRec do
      begin
        Readln(MapDefFile, DMSFileNo, DMSIdxNo, IntRecNo, IntIdxNo);
				ToIntMapKey.IntRecNo := IntRecNo;
				ToIntMapKey.IntIdxNo := IntIdxNo;
				ToIntMapKey.DMSFileNo := DMSFileNo;
    		ToIntMapKey.DMSIdxNo := DMSIdxNo;
      end;
      BtrvStat := CallDB(B_Insert, ToIntMapFile, ToIntMapRec.Start, ToIntMapKey.Start, 0);
		end;
		Close(MapDefFile);

		UseVer17Flag := False;
		InitializeSetupFiles('.DMS');
    FillLists;
    SetCursor(LastCursor);
	end;
end;

procedure TMapDlg.SetDMSFileName;
var
	MapFileNo: integer;

begin
  if SendDlgItemMsg(id_PatientFile, bm_GetCheck, 0, 0) > 0 then
  begin
		FileName := 'PATIENT';
    DMSFileNo := 0;
  end
	else
    if SendDlgItemMsg(id_SpecimenFile, bm_GetCheck, 0, 0) > 0 then
    begin
			FileName := 'SPECIMEN';
      DMSFileNo := 1;
    end
		else
    begin
      FileName := 'ISOLATE';
			DMSFileNo := 2;
    end;
end;

procedure TMapDlg.SetConfirmFlags;
var
  DMSIdx, IntIdx: integer;
	ToDMSMapRec: TToDMSMapRec;
  ToIntMapRec: TToIntMapRec;
	IntKey: TIntKey;
  DMSKey: TDMSKey;
  Index: Word;
  NumRows: Word;
  found: boolean;

begin
  BtrvStat := CallDB(B_Get_Low, ToDMSMapFile, ToDMSMapRec.Start, IntKey.Start, 1);
	while BtrvStat = 0 do
  begin
    if ToDMSMapRec.DMSFileNo = DMSFileNo then
    begin
      NumRows:= SendDlgItemMsg(id_UserList, lb_GetCount, 0, 0);
      Index:= 0;
      found:= false;
      repeat
        if (LoWord(SendDlgItemMsg(id_UserList, lb_GetItemData, Index, 0)) = word(ToDMSMapRec.DMSIdxNo)) then
          found:= true
        else
          Inc(Index);
      until found or (Index >= NumRows);
      if found then
        SendDlgItemMsg(id_UserList, lb_SetItemData, Index, MakeLong(word(ToDMSMapRec.DMSIdxNo), Mapped));
    end;
    BtrvStat := CallDB(B_Next, ToDMSMapFile, ToDMSMapRec.Start, IntKey.Start, 1);
  end;
  BtrvStat := CallDB(B_Get_Low, ToIntMapFile, ToIntMapRec.Start, DMSKey.Start, 1);
  while BtrvStat = 0 do
	begin
    { skip over label rows }
    Index:= ToIntMapRec.IntIdxNo;
    Inc(Index, RecordDiv[ToIntMapRec.IntRecNo]+1);
    SendDlgItemMsg(id_IntList, lb_SetItemData, Index, MakeLong(Index, Mapped));
    BtrvStat := CallDB(B_Next, ToIntMapFile, ToIntMapRec.Start, DMSKey.Start, 1);
  end;
end;

procedure TMapDlg.WMCommand(var Msg: TMessage);
begin
  if (Msg.wParam = id_PatientFile) or
     (Msg.wParam = id_SpecimenFile) or
     (Msg.wParam = id_IsolateFile) then
			 FillLists;
  inherited WMCommand(Msg);
end;

procedure TMapDlg.DeleteMapFile(ToDMS, Both: Boolean);
var
	ToDMSMapRec: TToDMSMapRec;
	ToIntMapRec: TToIntMapRec;
	IntKey: TIntKey;
	DMSKey: TDMSkey;
  LastCursor: HCursor;

begin
  LastCursor := SetCursor(LoadCursor(0, idc_Wait));
	if (ToDMS) or (Both) then
  begin
    BtrvStat := CallDB(B_Get_Low, ToDMSMapFile, ToDMSMapRec.Start, IntKey.Start, 1);
		while BtrvStat = 0 do
		begin
      CallDB(B_Delete, ToDMSMapFile, ToDMSMapRec.Start, IntKey.Start, 1);
      BtrvStat := CallDB(B_Next, ToDMSMapFile, ToDMSMapRec.Start, IntKey.Start, 1);
		end;
	end;
	if (not ToDMS) or (Both) then
  begin
    BtrvStat := CallDB(B_Get_Low, ToIntMapFile, ToIntMapRec.Start, DMSKey.Start, 1);
		while BtrvStat = 0 do
 		begin
      CallDB(B_Delete, ToIntMapFile, ToIntMapRec.Start, DMSKey.Start, 1);
      BtrvStat := CallDB(B_Next, ToIntMapFile, ToIntMapRec.Start, DMSKey.Start, 1);
 		end;
	end;
  SetCursor(LastCursor);
end;

procedure TMapDlg.RestartMap(var Msg: TMessage);
var
	ReStartDlg: PReStartDlg;
  Response, i: integer;
  Both: boolean;
	ToDMSMapRec: TToDMSMapRec;
  ToIntMapRec: TToIntMapRec;
	IntKey: TIntKey;
  DMSKey: TDMSKey;

begin
  ReStartDlg := New(PReStartDlg, Init(@Self, 'INT_RESTART'));
  Response := Application^.ExecDialog(ReStartDlg);
  Both := False;
	if Response <> id_No then
  begin
		if Response = id_RestartAll then
			Both := True;
    DeleteMapFile(ToDMSSelected, Both);
    SendDlgItemMsg(id_UserList, lb_ResetContent, 0, 0);
    SendDlgItemMsg(id_IntList, lb_ResetContent, 0, 0);
    EnableWindow(GetDlgItem(HWindow, id_ConfirmMap), False);
    FillLists;
	end;
end;

procedure TMapDlg.HandleUserList(var Msg: TMessage);
var
  SelIndex: integer;
  TempMsg: array[0..35] of char;
  TempVerMsg: array[0..30] of char;

begin
  case Msg.lParamHi of
    lbn_SelChange:
    begin
      SelIndex := SendDlgItemMsg(id_UserList, lb_GetCurSel, 0, 0);
(*      if (not ToDMSSelected) and (integer(LoWord(SendDlgItemMsg(id_UserList, lb_GetItemData, SelIndex, 0))) < 0) then*)
(*      begin*)
(*        LoadString(HInstance, str_IntBase+str_IntMsg, TempVerMsg, SizeOf(TempVerMsg));*)
(*        MessageBox(HWindow, 'This field cannot be mapped in this direction.', TempVerMsg, mb_OK);*)
(*      end*)
(*      else*)
(*      begin*)
        GetSelection(SelIndex, id_UserList, id_UserDisp);
        SetConfirmOk;
(*      end;*)
    end;
  end;
end;

procedure TMapDlg.HandleIntList(var Msg: TMessage);
var
  SelIndex: integer;
  TempMsg: array[0..35] of char;
  TempVerMsg: array[0..30] of char;

begin
  case Msg.lParamHi of
		lbn_SelChange:
    begin
      SelIndex := SendDlgItemMsg(id_IntList, lb_GetCurSel, 0, 0);
      if (HiWord(SendDlgItemMsg(id_IntList, lb_GetItemData, SelIndex, 0)) = word(-1)) then
      begin
        LoadString(HInstance, str_IntBase+str_NoMap, TempMsg, SizeOf(TempMsg));
        LoadString(HInstance, str_IntBase+str_IntMsg, TempVerMsg, SizeOf(TempVerMsg));
        MessageBox(HWindow, TempMsg, TempVerMsg, mb_OK);
      end
			else
      begin
        GetSelection(SelIndex, id_IntList, id_IntDisp);
				SetConfirmOk;
      end;
    end;
  end;
end;

procedure TMapDlg.GetSelection(Index, id_List, id_Disp: integer);
var
  SelectText: array[0..SetupNameLen] of char;
  TempSelected: Boolean;
  TempMsg: array[0..40] of char;
  TempVerMsg: array[0..30] of char;

begin
  TempSelected := ToDMSSelected;
  if id_List = id_IntList then
    if TempSelected then
      TempSelected := False
    else
      TempSelected := True;
  if (HiWord(SendDlgItemMsg(id_List, lb_GetItemData, Index, 0)) = Mapped) and
		 (TempSelected) then
  begin
    LoadString(HInstance, str_IntBase+str_MapAlready, TempMsg, SizeOf(TempMsg));
    LoadString(HInstance, str_IntBase+str_IntMsg, TempVerMsg, SizeOf(TempVerMsg));
    MessageBox(HWindow, TempMsg, TempVerMsg, mb_OK);
  end
  else
  begin
    SendDlgItemMsg(id_List, lb_GetText, Index, longint(@SelectText));
    SendDlgItemMsg(id_Disp, wm_SetText, 0, longint(@SelectText));
  end;
end;

procedure TMapDlg.MapDMSToMain(var Msg: TMessage);
const
  StaticText: array[0..3] of char = '->';
  BlankStr: string[2] = '';

var
  MapTitle: array[0..16] of char;
  TempMsg: array[0..25] of char;

begin
  SendDlgItemMsg(id_MapDir, wm_SetText, 0, longint(@StaticText));
  ToDMSSelected := False;
  LoadString(HInstance, str_IntBase+str_DMSToInt, TempMsg, SizeOf(TempMsg));
  StrCopy(MapTitle, TempMsg);
  SendDlgItemMsg(id_Title, wm_SetText, 0, longint(@MapTitle));
  SendDlgItemMsg(id_IntDisp, wm_SetText, 0, longint(@BlankStr));
  SendDlgItemMsg(id_UserDisp, wm_SetText, 0, longint(@BlankStr));
  EnableWindow(GetDlgItem(HWindow, id_ConfirmMap), False);
end;

procedure TMapDlg.MapMainToDMS(var Msg: TMessage);
const
	StaticText: array[0..3] of char = '<-';
  BlankStr: string[2] = '';

var
  MapTitle: array[0..16] of char;
  TempMsg: array[0..25] of char;

begin
  SendDlgItemMsg(id_MapDir, wm_SetText, 0, longint(@StaticText));
  ToDMSSelected := True;
  LoadString(HInstance, str_IntBase+str_IntToDMS, TempMsg, SizeOf(TempMsg));
  StrCopy(MapTitle, TempMsg);
  SendDlgItemMsg(id_Title, wm_SetText, 0, longint(@MapTitle));
  SendDlgItemMsg(id_IntDisp, wm_SetText, 0, longint(@BlankStr));
  SendDlgItemMsg(id_UserDisp, wm_SetText, 0, longint(@BlankStr));
  EnableWindow(GetDlgItem(HWindow, id_ConfirmMap), False);
end;

procedure TMapDlg.SetConfirmOk;
var
  UserText, ListText: array[0..SetupNameLen] of char;

begin
  SendDlgItemMsg(id_UserDisp, wm_GetText, SetupNameLen+1, longint(@UserText));
  SendDlgItemMsg(id_IntDisp, wm_GetText, SetupNameLen+1, longint(@ListText));
  if not (StrComp(UserText, '') = 0) and not (StrComp(ListText, '') = 0) then
    EnableWindow(GetDlgItem(HWindow, id_ConfirmMap), True);
end;

procedure TMapDlg.ConfirmMap(var Msg: TMessage);
const
  BlankStr: string[2] = '';

var
  UserSelIndex, IntSelIndex, TempIndex: integer;
	ToDMSMapRec: TToDMSMapRec;
	ToIntMapRec: TToIntMapRec;
	ToDMSMapKey: TToDMSMapKey;
  ToIntMapKey: TToIntMapKey;
  DMSFieldNum: integer;

begin
  UserSelIndex := SendDlgItemMsg(id_UserList, lb_GetCurSel, 0, 0);
  TempIndex := SendDlgItemMsg(id_IntList, lb_GetCurSel, 0, 0);
	{Set index based on field entry in setup file not list box}
	IntSelIndex := TempIndex - 1;
  IntRecNo := 0;
	if TempIndex > RecordDiv[1] then
  begin
		IntSelIndex := TempIndex - (RecordDiv[1] + 1);
    IntRecNo := 1;
  end;
	if (TempIndex > RecordDiv[2]) and (UseVer17Flag) then
  begin
		IntSelIndex := TempIndex - (RecordDiv[2] + 1);
		IntRecNo := 2;
  end;
  DMSFieldNum:= integer(LoWord(SendDlgItemMsg(id_UserList, lb_GetItemData, UserSelIndex, 0)));
  if ToDMSSelected then
  begin
    SendDlgItemMsg(id_UserList, lb_SetItemData, UserSelIndex, MakeLong(word(DMSFieldNum), Mapped));
		ToDMSMapKey.IntRecNo := IntRecNo;
		ToDMSMapKey.IntIdxNo := IntSelIndex;
		ToDMSMapKey.DMSFileNo := DMSFileNo;
    ToDMSMapKey.DMSIdxNo := DMSFieldNum;
		ToDMSMapRec.IntRecNo := IntRecNo;
		ToDMSMapRec.IntIdxNo := IntSelIndex;
		ToDMSMapRec.DMSFileNo := DMSFileNo;
    ToDMSMapRec.DMSIdxNo := DMSFieldNum;
    BtrvStat := CallDB(B_Insert, ToDMSMapFile, ToDMSMapRec.Start, ToDMSMapKey.Start, 0);
	end
  else
  begin
    SendDlgItemMsg(id_IntList, lb_SetItemData, TempIndex, MakeLong(word(TempIndex), Mapped));
		ToIntMapKey.IntRecNo := IntRecNo;
		ToIntMapKey.IntIdxNo := IntSelIndex;
		ToIntMapKey.DMSFileNo := DMSFileNo;
    ToIntMapKey.DMSIdxNo := DMSFieldNum;
		ToIntMapRec.IntRecNo := IntRecNo;
		ToIntMapRec.IntIdxNo := IntSelIndex;
		ToIntMapRec.DMSFileNo := DMSFileNo;
    ToIntMapRec.DMSIdxNo := DMSFieldNum;
    BtrvStat := CallDB(B_Insert, ToIntMapFile, ToIntMapRec.Start, ToIntMapKey.Start, 0);
	end;
  SendDlgItemMsg(id_UserDisp, wm_SetText, 0, longint(@BlankStr));
  SendDlgItemMsg(id_IntDisp, wm_SetText, 0, longint(@BlankStr));
  EnableWindow(GetDlgItem(HWindow, id_ConfirmMap), False);
end;

procedure TMapDlg.SetupIntFields(var Msg: TMessage);
var
  SetupIntDlg: PSetupIntDlg;
  pstr20: array[0..20] of char;
  pstr255: array[0..255] of char;

begin
  if YesNoMsg(HWindow, SR(IDS_WARNING, pstr20, SizeOf(pstr20)-1),
              SR(str_IntBase+str_MapsWillBeCleared, pstr255, SizeOf(pstr255)-1)) then
  begin
    SetupIntDlg := New(PSetupIntDlg, Init(@Self, 'INT_SETUP'));
    Application^.ExecDialog(SetupIntDlg);
    UseVer17Flag := False;
    DeleteMapFile(True, True);
    {Re-Initialize list box to accomidate any changes}
    FillLists;
  end;
end;

procedure TMapDlg.DuplicateMap(var Msg: TMessage);
const
  NotFound = 4;

var
	MessageText: array[0..135] of char;
  OldDirText, NewDirText: array[0..20] of char;
	FileText: array[0..15] of char;
  DMSIdx, IntIdx: integer;
	ToDMSMapRec: TToDMSMapRec;
	ToIntMapRec: TToIntMapRec;
	ToDMSMapKey: TToDMSMapKey;
	ToIntMapKey: TToIntMapKey;
	DMSKey: TDMSKey;
  IntKey: TIntKey;
  TempMsg: array[0..20] of char;
  TempVerMsg: array[0..30] of char;
  LastCursor: HCursor;

begin
	if ToDMSSelected then
    LoadString(HInstance, str_IntBase+str_ToIntMsg, MessageText, SizeOf(MessageText))
  else
    LoadString(HInstance, str_IntBase+str_ToDMSMsg, MessageText, SizeOf(MessageText));

  StrCat(MessageText, #13#10#13#10);

  LoadString(HInstance, str_IntBase+str_Continue, TempMsg, SizeOf(TempMsg));
  StrCat(MessageText, TempMsg);

  LoadString(HInstance, str_IntBase+str_IntMsg, TempVerMsg, SizeOf(TempVerMsg));
  if MessageBox(HWindow, MessageText, TempVerMsg, mb_YesNo) = id_Yes then
	begin
    LastCursor := SetCursor(LoadCursor(0, idc_Wait));
    DeleteMapFile(not ToDMSSelected, False);
		if ToDMSSelected then
    begin
      BtrvStat := CallDB(B_Get_Low, ToDMSMapFile, ToDMSMapRec.Start, ToDMSMapKey.Start, 0);
			while BtrvStat = 0 do
			begin
				IntKey.IntRecNo := ToDMSMapRec.IntRecNo;
				IntKey.IntIdxNo := ToDMSMapRec.IntIdxNo;
				ToIntMapKey.DMSFileNo := ToDMSMapRec.DMSFileNo;
				ToIntMapKey.DMSIdxNo := ToDMSMapRec.DMSIdxNo;
				ToIntMapKey.IntRecNo := ToDMSMapRec.IntRecNo;
				ToIntMapKey.IntIdxNo := ToDMSMapRec.IntIdxNo;
				ToIntMapRec.DMSFileNo := ToDMSMapRec.DMSFileNo;
				ToIntMapRec.DMSIdxNo := ToDMSMapRec.DMSIdxNo;
				ToIntMapRec.IntRecNo := ToDMSMapRec.IntRecNo;
				ToIntMapRec.IntIdxNo := ToDMSMapRec.IntIdxNo;
        if CallDB(B_Get_EQU, ToIntMapFile, ToIntMapRec.Start, IntKey.Start, 2) = NotFound then
          BtrvStat := CallDB(B_Insert, ToIntMapFile, ToIntMapRec.Start, ToIntMapKey.Start, 0);
        BtrvStat := CallDB(B_Next, ToDMSMapFile, ToDMSMapRec.Start, ToDMSMapKey.Start, 0);
			end;
		end
		else
    begin
      BtrvStat := CallDB(B_Get_Low, ToIntMapFile, ToIntMapRec.Start, ToIntMapKey.Start, 0);
			while BtrvStat = 0 do
			begin
				DMSKey.DMSFileNo := ToIntMapRec.DMSFileNo;
				DMSKey.DMSIdxNo := ToIntMapRec.DMSIdxNo;
				ToDMSMapKey.DMSFileNo := ToIntMapRec.DMSFileNo;
				ToDMSMapKey.DMSIdxNo := ToIntMapRec.DMSIdxNo;
				ToDMSMapKey.IntRecNo := ToIntMapRec.IntRecNo;
				ToDMSMapKey.IntIdxNo := ToIntMapRec.IntIdxNo;
				ToDMSMapRec.DMSFileNo := ToIntMapRec.DMSFileNo;
				ToDMSMapRec.DMSIdxNo := ToIntMapRec.DMSIdxNo;
				ToDMSMapRec.IntRecNo := ToIntMapRec.IntRecNo;
				ToDMSMapRec.IntIdxNo := ToIntMapRec.IntIdxNo;
        if CallDB(B_Get_EQU, ToDMSMapFile, ToDMSMapRec.Start, IntKey.Start, 2) = NotFound then
          BtrvStat := CallDB(B_Insert, ToDMSMapFile, ToDMSMapRec.Start, ToDMSMapKey.Start, 0);
        BtrvStat := CallDB(B_Next, ToIntMapFile, ToIntMapRec.Start, ToIntMapKey.Start, 0);
			end;
		end;
		FillLists;
    SetCursor(LastCursor);
	end;
end;

procedure TMapDlg.ShowMap(var Msg: TMessage);
var
  ShowDlg: PShowDlg;

begin
  ShowDlg := New(PShowDlg, Init(@Self, 'SHOW_MAP'));
  Application^.ExecDialog(ShowDlg);
  FillLists;
end;

{
********************
Non-Object Functions
********************
}

procedure IntMap_ShowMapFields(ParentWnd: HWnd; ParentWndObj: PWindowsObject);
begin
  MapDlg := New(PMapDlg, Init(ParentWndObj, 'INT_MAP_FIELDS'));
  Application^.ExecDialog(MapDlg);
end;

Begin
End.
