{$F+}
unit SectIsum;

{- isolate summary section driver }

INTERFACE

{- this module interfaces via SECTREG }

IMPLEMENTATION

uses
  DMSDebug,
  DBLib,
  MScan,
  DMSErr,
  DBErrors,
  DBTypes,
  DBFile,
  ctllib,
  StrsW,
  SectReg,
  IntlLib,
  gridlib,
  DlgLib,
  RptUtil,
  APITools,
  RptShare,
  DMString,
  RptData,
  BoxWin,
  OWindows,
  ODialogs,
  WinProcs,
  WinTypes,
  Strings,
  DBIDS;

{$I RPTGEN.INC}

type
  PSectIsoSumOpts = ^TSectIsoSumOpts;
  TSectIsoSumOpts = record
    separators      : integer;  {- border style -1=none, or PS_xxx constant }
    headers         : boolean;  {- column headers on/off }
    underlineHeader : boolean;  {- underline column headers }
    isoFlds         : TRptFldList; {- selected iso field numbers (index 0 is field count) }
    fldWidth        : array[1..MaxFldsPerRecord] of longint; {- column widths in LTwips }
    options         : TSectOpt; {- section options/font }
    font            : TRptFont;
  end;

  PSectIsoSum = ^TSectIsoSum;
  TSectIsoSum = object(TBoxWin)
    isoCfg   : TSectIsoSumOpts;
    isoDB    : PDBFile;
    constructor Init(AParent: PWindowsObject; ATitle: PChar;
                     rect: TRect; AnOrigin: TPoint; AZoom: Real;
                     APage: TRptPage; AFont: TRptFont);
    procedure DefineBox(aParent: PWindowsObject); virtual;
    procedure ResetDefaults; virtual;
    function GetBoxType: integer; virtual;

    function SavePrep: boolean; virtual;
    function SaveData(var dataSeq: longint): boolean; virtual;
    procedure SaveComplete; virtual;
    function LoadData(aBoxSeq: longint): boolean; virtual;
  end;

  PIsoSumEdit = ^TIsoSumEdit;
  TIsoSumEdit = object(TCenterDlg)
    colSep    : PComboBox;
    isoList   : PGrid;
    selList   : PGrid;
    cfg       : PSectIsoSumOpts;
    tcfg      : TSectIsoSumOpts;
    sectWidth : longint;
    db        : PDBReadDesc;
    {}
    constructor Init(AParent: PWindowsObject; var ACfg: TSectIsoSumOpts; aSectWidth: longint);
    destructor Done; virtual;
    procedure SetupWindow; virtual;
    procedure FillGrids;
    procedure Options(var msg: TMessage);       virtual ID_FIRST + IDC_OPTIONS;
    procedure Add(var msg: TMessage);           virtual ID_FIRST + IDC_ADD;
    procedure Remove(var msg: TMessage);        virtual ID_FIRST + IDC_REMOVE;
    procedure Up(var msg: TMessage);            virtual ID_FIRST + IDC_UP;
    procedure Down(var msg: TMessage);          virtual ID_FIRST + IDC_DOWN;
    procedure IDFont(var msg: TMessage);        virtual ID_FIRST + IDC_FONT;
    procedure IDWidth(var msg: TMessage);       virtual ID_FIRST + IDC_WIDTH;
    procedure WMDrawItem(var msg: TMessage);    virtual WM_FIRST + WM_DRAWITEM;
    procedure WMMeasureItem(var msg: TMessage); virtual WM_FIRST + WM_MEASUREITEM;
    procedure WMCharToItem(var msg: TMessage);  virtual WM_FIRST + WM_CHARTOITEM;
    procedure WMCommand(var msg: TMessage);     virtual WM_FIRST + WM_COMMAND;
    function CanClose: Boolean; virtual;
  end;

var
  sectionName : array[0..30] of char;


{---------------------------------[ TSectIsoSum ]--}

constructor TSectIsoSum.Init(AParent: PWindowsObject; ATitle: PChar;
                          rect: TRect; AnOrigin: TPoint; AZoom: Real;
                          APage: TRptPage; AFont: TRptFont);
begin
  isoDB:= nil;
  inherited Init(AParent, ATitle, rect, AnOrigin, AZoom, APage, AFont);
end;

procedure TSectIsoSum.ResetDefaults;
begin
  FillChar(isoCfg, sizeof(isoCfg), 0);
  with isoCfg do
  begin
    MakeDefaultRFont(font);
    separators:= PS_SOLID;
    headers:= true;
    underlineHeader:= true;
    FillChar(isoFlds, sizeof(isoFlds), 0);

    {- select default isolate fields for isolate summary }
    isoFlds[0]:= 1;  {- set count to one }
    isoFlds[1]:= DBISOIso;  {- isolate number }
    fldWidth[1]:= 1440 div 2; {- 0.5 inch}

    with options do
    begin
      bkMode:= OPAQUE;
      border:= -1;  {- none }
    end;
  end;
end;

procedure TSectIsoSum.DefineBox;
var
  p  : PIsoSumEdit;
  sz : TLRect;
begin
  GetSize(sz);
  p:= New(PIsoSumEdit, Init(GetParentObj, isoCfg, sz.right));
  if application^.ExecDialog(p) = IDOK then
  begin
    SetDisplayFont(isoCfg.Font);
  end;
end;

function TSectIsoSum.GetBoxType: integer;
begin
  GetBoxType:= SectIsoSumID;
end;

function TSectIsoSum.SavePrep: boolean;
{- open database files and prepare to save }
var
  pstr  : array[0..200] of char;
begin
  isoDB:= New(PDBFile, Init(DBRptIsoSumFile, '', dbOpenNormal));
  if isoDB = nil then
  begin
    ShowError(GetParentObj, IDS_CANTOPENISUM, nil, dbLastOpenError, MOD_RPTGEN, 1);
  end;
  SavePrep:= isoDB <> nil;
end;

function TSectIsoSum.SaveData(var dataSeq: longint): boolean;
var
  pstr: array[0..200] of char;
  ret   : boolean;
  sz    : TLRect;
begin
  ret:= true;
  if isoDB <> nil then
  begin
    GetSize(sz);

    with isoDB^, dbr^ do
    begin
      PutField(DBRptIsoSumSeparators, @isoCfg.separators);
      PutField(DBRptIsoSumHeaders, @isoCfg.headers);
      PutField(DBRptIsoSumULHeader, @isoCfg.underlineHeader);
      PutField(DBRptIsoSumIsoFlds, @isoCfg.isoFlds);
      PutField(DBRptIsoSumFldWidth, @isoCfg.fldWidth);
      PutField(DBRptIsoSumFont, @isoCfg.font);
      PutField(DBRptIsoSumOptions, @isoCfg.options);
      PutField(DBRptIsoSumSize, @sz);
      dbc^.InsertRec(dbr);
      ret:= dbc^.dbErrorNum = 0;
      if not ret then
      begin
        ShowError(GetParentObj, IDS_CANTINSERTISUM, nil, dbc^.dbErrorNum, MOD_RPTGEN, 0);
        dataSeq:= -1;
      end
      else
        dataSeq:= GetSeqValue;
    end;
  end
  else
    ret:= false;
  SaveData:= ret;
end;

procedure TSectIsoSum.SaveComplete;
begin
  MSDisposeObj(isoDB);
end;

function TSectIsoSum.LoadData(aBoxSeq: longint): boolean;
var
  db    : PDBFile;
  ret   : boolean;
  pstr  : array[0..200] of char;
  sz    : TLRect;
begin
  db:= New(PDBFile, Init(DBRptIsoSumFile, '', dbOpenNormal));
  if db = nil then
  begin
    ret:= false;
    ShowError(GetParentObj, IDS_CANTOPENISUM, nil, dbLastOpenError, MOD_RPTGEN, 2);
  end
  else
  begin
    with db^, dbr^ do
    begin
      boxSeq:= aBoxSeq;
      dbc^.GetSeq(dbr, boxSeq);
      if dbc^.dbErrorNum = 0 then
      begin
        GetField(DBRptIsoSumSeparators, @isoCfg.separators, sizeOf(isoCfg.separators));
        GetField(DBRptIsoSumHeaders, @isoCfg.headers, sizeof(isoCfg.headers));
        GetField(DBRptIsoSumULHeader, @isoCfg.underlineHeader, sizeof(isoCfg.underlineHeader));
        GetField(DBRptIsoSumIsoFlds, @isoCfg.isoFlds, sizeof(isoCfg.isoFlds));
        GetField(DBRptIsoSumFldWidth, @isoCfg.fldWidth, sizeof(isoCfg.fldWidth));
        GetField(DBRptIsoSumFont, @isoCfg.font, sizeof(isoCfg.font));
        GetField(DBRptIsoSumOptions, @isoCfg.options, sizeof(isoCfg.options));
        GetField(DBRptIsoSumSize, @sz, sizeof(sz));

        SetSize(sz);
      end
      else
      begin
        ShowError(GetParentObj, IDS_CANTLOCSECTRECT, nil, dbc^.dbErrorNum, MOD_RPTGEN, 2);
        CloseWindow;  {- delete the section from the report }
        boxSeq:= -1;
        ret:= false;
      end;
    end;
    MSDisposeObj(db);
  end;
  LoadData:= ret;
end;


{-------------------------[ TIsoSumEdit ]--}

constructor TIsoSumEdit.Init(AParent: PWindowsObject; var ACfg: TSectIsoSumOpts; aSectWidth: longint);
var
  c     : PControl;
  pstr  : array[0..100] of char;
  subst : PErrSubst;
begin
  inherited Init(AParent, MakeIntResource(DLG_ISOSUMMARY));

  db:= New(PDBReadDesc, Init(DBISOFile, ''));
  if db = nil then
  begin
    subst:= New(PErrSubst, Init(DBIsoFile));
    ShowError(@self, IDS_CANTACCESSDEF, subst, dbLastOpenError, MOD_RPTGEN, 2);
    MSDisposeObj(subst);
    Halt;
  end;

  c:= New(PLStatic, InitResource(@self, 400, 0, false));
  c:= New(PLCheckBox, InitResource(@self, IDC_HEADERS, false));
  c:= New(PLCheckBox, InitResource(@self, IDC_UNDERLINEHEADER, false));

  colSep:= New(PComboBox, InitResource(@self, IDC_COLUMNSEP, 0));
  isoList:= New(PGrid, InitResource(@self, IDC_ISOFLDS, 1, false));
  selList:= New(PGrid, InitResource(@self, IDC_SELFLDS, 2, false));
  sectWidth:= aSectWidth;
  cfg:= @ACfg;
  tcfg:= cfg^;
end;

destructor TIsoSumEdit.Done;
begin
  inherited Done;
  MSDisposeObj(db);
end;

procedure TIsoSumEdit.SetupWindow;
var
  k   : integer;
  j   : longint;
  pstr: array[0..100] of char;
  p2  : array[0..100] of char;
begin
  inherited SetupWindow;

  {- set values for controls }
  FillLineTypes(HWindow, colSep);
  colSep^.SetSelIndex(0);

  for k:= 0 to colSep^.GetCount - 1 do
  begin
    j:= SendDlgItemMsg(IDC_COLUMNSEP, CB_GETITEMDATA, k, 0);
    if Integer(j) = tcfg.separators then
      colSep^.SetSelIndex(k);
  end;

  if tcfg.headers then
    SendDlgItemMsg(IDC_HEADERS, BM_SETCHECK, bf_Checked, 0)
  else
    SendDlgItemMsg(IDC_HEADERS, BM_SETCHECK, bf_Unchecked, 0);
  if tcfg.underlineHeader then
    SendDlgItemMsg(IDC_UNDERLINEHEADER, BM_SETCHECK, bf_Checked, 0)
  else
    SendDlgItemMsg(IDC_UNDERLINEHEADER, BM_SETCHECK, bf_Unchecked, 0);


  SR(IDS_WIDTH, pstr, sizeof(pstr)-1);
  UnitsStr(true, p2, 100);
  StrCat(pstr, ' (');
  StrCat(pstr, p2);
  StrCat(pstr, ')');

  selList^.SetHeader(0, SR(IDS_SELECTEDFLDS, p2, sizeof(p2)-1));
  selList^.SetHeader(1, pstr);

  isoList^.SetHeader(0, SR(IDS_ISOFLDS, p2, sizeof(p2)-1));
  isoList^.StretchColumn(0);

  selList^.SetColumnWidth(0, isoList^.GetColumnWidth(0));
  selList^.StretchColumn(1);

  selList^.SetAlignment(1, grid_Right);

  FillGrids;
end;

procedure TIsoSumEdit.wmDrawItem(var msg: TMessage);
begin
  isoList^.DrawItem(msg);
  selList^.DrawItem(msg);
end;

procedure TIsoSumEdit.WMCharToItem(var msg: TMessage);
begin
  isoList^.CharToItem(msg);
  selList^.CharToItem(msg);
end;

procedure TIsoSumEdit.wmMeasureItem(var msg: TMessage);
begin
  isoList^.MeasureItem(msg);
  selList^.MeasureItem(msg);
end;

procedure TIsoSumEdit.WMCommand(var msg: TMessage);
begin
  inherited wmCommand(msg);
  if msg.lParamLo <> 0 then  {- is message from control? }
  begin
    if msg.lParamHi = LBN_DBLCLK then
    begin
      if msg.wParam = IDC_ISOFLDS then
        Add(msg)
      else if msg.wParam = IDC_SELFLDS then
        IDWidth(msg);
    end;
  end;
end;

procedure TIsoSumEdit.FillGrids;
var
  k   : integer;
  fld : longint;
  num : real;
  pstr: array[0..100] of char;
begin
  FillIsoFlds(db, isoList, selList, tcfg.isoFlds);
  for k:= 0 to selList^.GetRowCount - 1 do  {- iterate thru selected fields }
  begin
    LTwipsToUnits(tcfg.fldWidth[k+1], num);
    IntlRealToStr(num, pstr, 100);
    selList^.SetData(1, k, pstr);
  end;
end;

procedure TIsoSumEdit.Options(var msg: TMessage);
begin
  GetSectOpt(@self, tCfg.options);
end;

procedure TIsoSumEdit.Down(var msg: TMessage);
var
  row  : integer;
begin
  row:= selList^.GetSelIndex;
  if (row <> -1) and (row < selList^.GetRowCount-1) then
  begin
    selList^.SwapRows(row, row+1);
    selList^.SetSelIndex(row+1);
  end;
end;

procedure TIsoSumEdit.Up(var msg: TMessage);
var
  row  : integer;
begin
  row:= selList^.GetSelIndex;
  if row > 0 then
  begin
    selList^.SwapRows(row, row-1);
    selList^.SetSelIndex(row-1);
  end;
end;

procedure TIsoSumEdit.IDWidth(var msg: TMessage);
var
  pstr  : array[0..100] of char;
  p2    : array[0..100] of char;
  k     : integer;
  w     : longint;
  num   : real;
  code  : integer;
begin
  k:= selList^.GetSelIndex;
  if k <> -1 then
  begin
    selList^.GetData(1, k, pstr, 100);
    Val(pstr, num, code);
    UnitsToLTwips(num, w);

    selList^.GetData(0, k, pstr, 100);
    StrCat(pstr, ' ');
    StrCat(pstr, SR(IDS_COLUMN, p2, sizeof(p2)-1));

    if GetWidth(@self, pstr, w, sectWidth) then
    begin
      LTwipsToUnits(w, num);
      IntlRealToStr(num, pstr, 100);
      selList^.SetData(1, k, pstr);
    end;
  end
  else
    MessageBeep(0);
end;

procedure TIsoSumEdit.Remove(var msg: TMessage);
var
  pstr  : array[0..100] of char;
  k     : integer;
  row   : integer;
  id    : Longint;
begin
  row:= selList^.GetSelIndex;
  if row = -1 then
    MessageBeep(0)
  else
  begin
    id:= selList^.GetItemData(row);

    pstr[0]:= chr(id + ord('@'));
    pstr[1]:= #0;
    k:= isoList^.AddString(pstr);

    selList^.GetData(0, row, pstr, 100);
    isoList^.SetData(0, k, pstr);
    isoList^.SetItemData(k, id);

    selList^.DeleteString(row);
  end;
end;

procedure TIsoSumEdit.Add(var msg: TMessage);
var
  pstr  : array[0..100] of char;
  k     : integer;
  row   : integer;
  units : Real;
  id    : Longint;
  w     : integer;
begin
  row:= isoList^.GetSelIndex;
  if row = -1 then
    MessageBeep(0)
  else
  begin
    isoList^.GetData(0, row, pstr, 100);  {- get field name }
    id:= isoList^.GetItemData(row);  {- get field number }

    k:= selList^.AddString(pstr);
    selList^.SetData(0, k, pstr);
    selList^.SetItemData(k, id);

    w:= integer(db^.FieldSizeForStr(id));
    if (db^.dbErrorNum <> 0) or (w < 0) then
      w:= 0;

    {w:= CalcDefaultWidth(tcfg.font, w);}
    w:= 1440;
    LTwipsToUnits(w, units);
    IntlRealToStr(units, pstr, 100);

    selList^.SetData(1, k, pstr);

    isoList^.DeleteString(row);
  end;
end;

procedure TIsoSumEdit.IDFont(var msg: TMessage);
begin
  GetAFont(HWindow, tcfg.font);
end;

function TIsoSumEdit.CanClose: Boolean;
var
  j     : integer;
  ret   : boolean;
  tot   : longint;
  p1    : array[0..100] of char;
  p2    : array[0..200] of char;

  procedure ExtractWidthFromGrid(row: integer; var aVal: longint);
  var
    p1    : array[0..50] of char;
    num   : real;
    err   : boolean;
  begin
    selList^.GetData(1, row, p1, sizeof(p1)-1);
    num:= IntlStrToReal(p1, err);
    UnitsToLTwips(num, aVal);
  end;

begin
  ret:= true;
  if selList^.GetRowCount = 0 then
  begin
    ShowError(@self, IDS_ISOFLDMUSTBESEL, nil, 0, MOD_RPTGEN, 0);
    ret:= false;
  end
  else
  begin
    tot:= 0;
    fillchar(tcfg.fldWidth, sizeof(tcfg.fldWidth), 0);
    fillchar(tcfg.isoFlds, sizeof(tcfg.isoFlds), 0);
    for j:= 0 to selList^.GetRowCount - 1 do
    begin
      Inc(tCfg.isoFlds[0]);
      tcfg.isoFlds[j+1]:= integer(selList^.GetItemData(j));
      ExtractWidthFromGrid(j, tcfg.fldWidth[j+1]);
      Inc(tot, tcfg.fldWidth[j+1]);
    end;
    if tot > sectWidth then
    begin
      ret:= YesNoMsg(HWindow, SR(IDS_WARNING, p1, sizeof(p1)-1),
                              SR(IDS_ISUMCOLWIDTHERR, p2, sizeof(p2)-1));
    end;
  end;

  if ret then
  begin
    j:= Integer(SendDlgItemMsg(IDC_COLUMNSEP, CB_GETITEMDATA, colSep^.GetSelIndex, 0));
    tcfg.separators:= j;
    tcfg.headers:= SendDlgItemMsg(IDC_HEADERS, BM_GETCHECK, 0,0) = bf_Checked;
    tCfg.underlineHeader:= SendDlgItemMsg(IDC_UNDERLINEHEADER, BM_GETCHECK, 0,0) = bf_Checked;
    cfg^:= tcfg;
  end;
  CanClose:= ret;
end;

function SectIsoDelete(action: integer; aRptSeq, aSectSeq: longint): boolean;
const
  db  : PDBFile = nil;
var
  ret   : boolean;
begin
  ret:= true;
  if (action and ACTION_OPEN <> 0) then
  begin
    db:= New(PDBFile, Init(DBRptIsoSumFile, '', dbOpenNormal));
    if db = nil then
    begin
      ShowError(nil, IDS_CANTOPENISUM, nil, dbLastOpenError, MOD_RPTGEN, 3);
      ret:= false;
    end;
  end;

  if ret and (action and ACTION_DELETE <> 0) and (db <> nil) then
  begin
    with db^ do
    begin
      dbc^.GetSeq(dbr, aSectSeq);
      if dbc^.dbErrorNum = 0 then
        dbc^.DeleteRec(dbr);
    end;
  end;

  if ret and (action and ACTION_CLOSE <> 0) then
  begin
    MSDisposeObj(db);
  end;

  SectIsoDelete:= ret;
end;

{----------------------------[ Regsitered initialization function ]--}

function SectIsoSumInit(aParent: PWindowsObject; r: TRect; origin: TPoint;
                      aZoom: real; page: TRptPage; aFont: TRptFont): PBoxWin;
begin
  SectIsoSumInit:= New(PSectIsoSum, Init(aParent, sectionName, r, origin, azoom, page, aFont));
end;


BEGIN
  sections^.RegisterSection(SectIsoSumInit, SectIsoDelete, SR(IDS_ISOSummary,
                            sectionName, sizeof(sectionName)), SectIsoSumID);
END.

{- rcf }
