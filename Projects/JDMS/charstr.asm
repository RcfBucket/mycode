.MODEL large, Pascal

PUBLIC        CharStr

.code

;========================================================== CharStr
;
; Function CharStr(ch: Char; pstr: PChar; len: Word): PChar;
;
; return a string of ch's with length len
;----------------------------------------------------------------------
CharStr         PROC FAR
; Parameters
char            equ byte ptr  ss:[bp+12]
pstr            equ dword ptr ss:[bp+8]
len             equ word ptr  ss:[bp+6]
pSize           equ 8

        push    bp
        mov     bp,sp
        push    ds

        cld                     ; go foreward ->
        mov     ax,len          ; get length
        les     di,pstr         ; ES:DI -> result string

        mov     cx,ax           ; put lenght in CL
        jcxz    done

        mov     al,char         ; get character to fill
        rep     stosb           ; store bytes in return string
done:
        mov     al,0            ; store terminating null
        stosb

        mov     ax, word ptr pstr       ; setup function return
        mov     dx, word ptr pstr+2

        pop     ds
        pop     bp
        ret     pSize

Charstr ENDP

        END
