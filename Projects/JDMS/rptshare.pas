unit RptShare;

{- this unit is provided as a place to put shared types and constants between
   the report generator and the print engine }

INTERFACE

uses
  DBTypes,
  DlgLib,
  ODialogs,
  OWindows,
  WinTypes;

const
  RptGenVersion         = '01.20';

const
  maxRptNameLen         = 50;
  maxRptsPerPage        = 5;
  maxRptFreeText        = 255;  {- maximum # of chars in report free text field }
  MaxIsoPerSect         = 20;   {- maximum number of isolates per section }

  {- section id constants }
  SectDemoFreeID        = 1;
  SectIsoSumID          = 2;
  SectIsoCompID         = 3;
  SectGSID              = 4;
  SectIsoFullID         = 5;

type
  {- array to hold field numbers. element 0 hold number of elements }
  PRptFldList = ^TRptFldList;
  TRptFldList = array[0..MaxFldsPerRecord] of integer;

  PFldOpts = ^TFldOpts;
  TFldOpts = record
    just      : integer;  {- DT_LEFT, DT_CENTER or DT_RIGHT (see DrawText API call)}
    bold      : boolean;
    underline : boolean;
  end;

  TLPoint = record
    x  : longint;
    y  : longint;
  end;

  TLRect = record
    left, top, right, bottom : longint;
  end;

  {- section options }
  PSectOpt = ^TSectOpt;
  TSectOpt = record
    bkMode   : integer;  {- background mode for section (OPAQUE or TRANSPARENT }
    border   : integer;  {- border style -1=none, or PS_xxx constant }
  end;

  {- report section font information }
  PRptFont = ^TRptFont;
  TRptFont = record
    lf     : TLogFont;  {- logical font structure }
    ptSize : integer;   {- ten time point size }
    color  : TColorRef;
  end;

  {- record to describe the a report's attributes }
  PRptPage = ^TRptPage;
  TRptPage = record
    size        : TPoint;  {- size of page, in LTwips }
    margin      : TRect;   {- page margins }
    rptsPerPage : integer; {- reports per physical page }
    orient      : integer; {- orientation (DMORIENT_PORTRAIT or DMORIENT_LANDSCAPE) }
    paperIdx    : integer; {- DMPAPER... index of selected paper size }
    spacing     : integer;
  end;

  PZoomDlg = ^TZoomDlg;
  TZoomDlg = object(TCenterDlg)
    zoom  : ^Real;
    lb    : PListBox;
    constructor Init(AParent: PWindowsObject; aName: PChar; listID: integer;
                     var aZoom: Real);
    procedure SetupWindow; virtual;
    procedure WMCommand(var msg: TMessage); virtual WM_FIRST + WM_COMMAND;
    procedure OK(var msg: TMessage); virtual id_First + id_OK;
  end;

  {- following is used by the full isolate detail sections }
  TFullDetailRec = record
    detailType      : integer;  { 0-drug abbreviation, 1-Drug name, 2-Alternate drug abbrev,
                                  3-MIC, 4- Interpretation }
    visible         : boolean;  {- sub column visible if true }
    width           : integer;  {- width in LTwips of sub-column }
  end;
  TDetailList = array[1..5] of TFullDetailRec;

IMPLEMENTATION

constructor TZoomDlg.Init(AParent: PWindowsObject; aName: PChar; listID: integer;
                          var aZoom: Real);
begin
  zoom:= @aZoom;
  inherited Init(AParent, aName);
  lb:= New(PListBox, InitResource(@self, listID));
end;

procedure TZoomDlg.SetupWindow;

begin
  inherited SetupWindow;
  lb^.AddString('25%');
  lb^.AddString('50%');
  lb^.AddString('75%');
  lb^.AddString('100%');
  lb^.AddString('125%');
  lb^.AddString('150%');
  lb^.AddString('175%');
  lb^.AddString('200%');
  lb^.SetSelIndex(Trunc(4.0 * zoom^ - 1.0));
end;

procedure TZoomDlg.WMCommand(var msg: TMessage);
begin
  inherited WMCommand(msg);
  if (msg.lParamLo <> 0) and (msg.wParam = lb^.GetID) then
  begin
    case msg.lParamHi of  {- lParamHi = LBN_xxx }
      LBN_DBLCLK:
        OK(msg);
    end;
  end;
end;

procedure TZoomDlg.OK(var msg: TMessage);
var
  li  : integer;
begin
  li:= lb^.GetSelIndex;
  if li >= 0 then
  begin
    zoom^:= ((li + 1) * 25.0) / 100.0;
    inherited OK(msg);
  end;
end;


END.

{- rcf }
