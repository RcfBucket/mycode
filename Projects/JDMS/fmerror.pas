{----------------------------------------------------------------------------}
{  Module Name  : FMError.PAS                                                }
{  Programmer   : DWC                                                        }
{  Date Created : 07/11/95                                                   }
{                                                                            }
{  Purpose -                                                                 }
{                                                                            }
{  Assumptions -                                                             }
{  None.                                                                     }
{                                                                            }
{  Initialization -                                                          }
{  None.                                                                     }
{                                                                            }
{  Revision History -                                                        }
{                                                                            }
{  Version  Date      Who    Did What                                        }
{  -------  --------  ------ ----------------------------------------------- }
{  1.00     05/11/95  DWC     Initial release.                               }
{                                                                            }
{----------------------------------------------------------------------------}

unit FMERROR;

INTERFACE

uses
  DlgLib;

procedure FMShowError(aDlg: PCenterDlg; title : PChar; errNum : integer; fmErrStr : PChar);

IMPLEMENTATION

uses
  ApiTools,
  Strings,
  FMConst;

{------------------------[FMShowError]--------------------------------------}
{---------------------------------------------------------------------------}
{                                                                           }
{---------------------------------------------------------------------------}
procedure FMShowError(aDlg     : PCenterDlg;
                      title    : PChar;
                      errNum   : integer;
                      fmErrStr : PChar);
var
  errStr  : array[0..255] of char;

begin

  StrCopy(errStr,'');

  case errNum of
    5:
      begin
        SR(IDS_DISKWRITE, errStr, 255);
        StrCat(errStr,fmErrStr);
      end;
    fmErr_NoMatchSchema:
      begin
        SR(IDS_NOMATCHSCHEMA, errStr, 255);
        StrCat(errStr,fmErrStr);
      end;
    fmErr_OpeningFile:
      begin
        SR(IDS_OPENINGFILE, errStr, 255);
        StrCat(errStr,fmErrStr);
      end;
    fmErr_CreatingFile:
      begin
        SR(IDS_CREATINGFILE, errStr, 255);
        StrCat(errStr,fmErrStr);
      end;
    fmErr_OpeningFileExclus:
      begin
        SR(IDS_OPENINGFILEEXCLUS, errStr, 255);
        StrCat(errStr,fmErrStr);
      end;
    fmErr_DiskWrite:
      begin
        SR(IDS_DISKWRITE, errStr, 255);
      end;
    fmErr_HardDiskFull:
      begin
        SR(IDS_HARDDISKFULL, errStr, 255);
      end;
    fmErr_MPSSFailed:
      begin
        SR(IDS_MPSSFAILED, errStr, 255);
      end;
    fmErr_CreatingFileDesc:
      begin
        SR(IDS_CREATINGFILEDESC, errStr, 255);
        StrCat(errStr,fmErrStr);
      end;
    fmErr_DeletingFile:
      begin
        SR(IDS_DELETINGFILE, errStr, 255);
        StrCat(errStr,fmErrStr);
      end;
    fmErr_RenamingFile:
      begin
        SR(IDS_RENAMINGFILE, errStr, 255);
        StrCat(errStr,fmErrStr);
      end;
    fmErr_InsertingRecord:
      begin
        SR(IDS_INSERTINGRECORD, errStr, 255);
        StrCat(errStr,fmErrStr);
      end;
else
   StrCopy(errStr,SR(IDS_FAILED, errStr, 255));
  end;

  ErrorMsg(aDlg^.HWindow,title,errStr);

end;

End.
{- DWC }

