{$R-}
{----------------------------------------------------------------------------}
{  Module Name  : BIOTYPE.PAS                                                }
{  Programmer   : EJ                                                         }
{  Date Created : 02/10/95                                                   }
{                                                                            }
{  Purpose -                                                                 }
{  This module provides the objects required to compute and manipulate       }
{  biotype numbers.                                                          }
{                                                                            }
{  Assumptions -                                                             }
{  None.                                                                     }
{                                                                            }
{  Revision History -                                                        }
{                                                                            }
{  Version  Date      Who    Did What                                        }
{  -------  --------  ------ ----------------------------------------------- }
{  1.00     02/10/94  EJ     Initial release.                                }
{                                                                            }
{----------------------------------------------------------------------------}

unit Biotype;

INTERFACE

uses
  OWindows,
  Objects,
  ResLib,
  DLGLib,
  Trays,
  DMSDebug,
  DBFile,
  IDLib;

const
  BioNameLen     = 4;
  MaxBioChems    = 47;  { Max biochems in biotype }
  GluWellNum     = 24;
  SucWellNum     = 25;
  SorWellNum     = 26;
  LysWellNum     = 56;
  ArgWellNum     = 57;
  OrnWellNum     = 58;

type
  BioBit=RECORD     { part of a bdesc.jen record }
    group : byte;   { kind of biotype bit }
    index : byte;   { well number of biochem }
    Name  : array[0..BioNameLen] of char;
  end;

  { Parsed BDESC record }
  TBDesc = array[0..MaxBioChems, 1..2] of BioBit;

  { Unparsed BDESC record }
  TBDescRec = array[-2..MaxBioChems, 1..48] of char;

  PBiotypeObject = ^TBiotypeObject;
  TBiotypeObject = object(TObject)
    Biotype       : string[80];
    BioMask       : string[80];
    Identify      : PIdentify;
    MaxBitNum     : integer;
    tfsRec        : PTFSpecRec;
    Class, bDescClass       : integer;
    PosFlags,
    ChkFlags,
    virtualTests  : PPanelBitmap;
    neededResults : string[80];
    indoleBitNum  : integer;
    oxidaseBitNum : integer;
    oxiRes, indRes,
    betaHemRes    : integer;
    MICData       : PMICData;
    BDesc         : TBDesc;
    BDescRec      : TBDescRec;
    BDescFile     : file of TBDescRec;
    IndWasDispd   : boolean;  {Indicate that RNID3 panels dispensed indole}
    {}
    constructor Init(atfsRec: PTFSpecRec);
    destructor Done; virtual;
    procedure Abort(ErrNum: integer);
    procedure ScanBDesc(RecNum: integer);
    function  ComputeBiotype(AClass       : integer;
                             APosFlags,
                             AChkFlags    : PPanelBitmap;
                             AMICData     : PMICData;
                             oxiResult,
                             indResult,
                             betaHemResult   : integer;
                             AVirtualTests: PPanelBitmap) : boolean;
    function  RecomputeBiotype: boolean;
    procedure SetBioClass(newClass: integer);
    procedure SetBiotype(AClass  : integer;
                         ABiotype: PChar);
    function  BitInfo(BitNum: integer;
                  var Value,
                      wellNum,
                      group : integer;
                      Desc  : PChar): boolean; virtual;
    procedure CheckOxidaseORIndole(bitnum: Integer;
                               var needed: Boolean);
    function  GetTopID(var orgNum : integer;
                           probTxt: PChar;
                           Footnotes: PChar;
                           SpecialChars: Pchar): boolean;
    function  DrugUsed(drugRDN: integer): boolean;
    procedure PatchBDESC; virtual;
  end;

  PWABiotypeObject = ^TWABiotypeObject;
  TWABiotypeObject = object(TBiotypeObject)
    procedure PatchBDESC; virtual;
  end;

IMPLEMENTATION

uses
  ODialogs,
  WinTypes,
  WinProcs,
  APITools,
  MScan,
  OrgGroup,
  Strings,
  DMString,
  DBIds,
  DMSErr,
  Interps,
  Bits;

{----------------------------------------------------[ TBiotypeObject ]--}

constructor TBiotypeObject.Init(atfsRec: PTFSpecRec);
begin
  inherited Init;
  biotype := '';
  biomask := '';
  MaxBitNum:=-1;
  IndWasDispd := False;  {Assume that indole was not dispensed}

  {$IFOPT I+}
    {$DEFINE chki}
  {$ENDIF}
  {$I-}

  Assign(BDescFile, 'BDESC.JEN');
  Reset(BDescFile);
  if IoResult <> 0 then
    Abort(3);

  {$IFDEF chki}
    {$I+}
    {$UNDEF chki}
  {$ENDIF}

  Identify := nil;
  Identify:= New(PIdentify, Init('ORGPRB.TXT', '', TRUE, FALSE));
  if identify = nil then
  begin
    Done;
    Fail;
  end;

  tfsRec := aTfsRec;
  Class:= -1;
  bDescClass := -1;
end;

destructor TBiotypeObject.Done;
begin
  inherited Done;
  MSDisposeObj(Identify);

  {$IFOPT I+}
    {$DEFINE chki}
  {$ENDIF}
  {$I-}

  Close(BDescFile);
  if IoResult <> 0 then {clear IORESULT };

  {$IFDEF chki}
    {$I+}
    {$UNDEF chki}
  {$ENDIF}
end;

procedure TBioTypeObject.SetBioClass(newClass: integer);
{- sets a new class }
begin
  Class:= newClass;
  bDescClass := newClass;
end;

{-----------------------------------------------------------------------}
{ purpose :  error out when some condition has been met.                }
{ input   :  errnum  - The error message to be displayed.               }
{-----------------------------------------------------------------------}
procedure TBiotypeObject.Abort(ErrNum: integer);
var
  pstr  : array [0..60] of char;
begin
  SR(IDS_PROGHALT, pstr, sizeof(pStr)-1);
  ShowErrorStr(0, pstr, ErrNum + MOD_BIOTYPE, MOD_BIOTYPE, 0, sizeof(pStr)-1);
  Halt;
end;

procedure TBiotypeObject.ScanBDesc(RecNum: integer);
var
  bit, cl : integer;
  s       : PChar;
  tmpstr  : string;
  Code    : integer;

  procedure IncS;
  begin
    Inc(s);
    if (s^ = #13) OR (s^ = #10) then
      Abort(1);
  end;

  function NextItem: string;
  begin
    tmpstr:= '';
    while s^ = ' ' do IncS;
    while s^ <> ' ' do
    begin
      tmpstr:= tmpstr + s^;
      IncS;
    end;
    NextItem:= tmpstr;
  end;

begin
  Seek(BDescFile, RecNum);
  Read(BDescFile, BDescRec);
  for bit:= 0 to MaxBiochems do
  begin
    if BDescRec[bit][48] <> #10 then
      Abort(2);
    s:= @BDescRec[bit][1];
    for cl:= 1 to 2 do
    begin
      Val(NextItem, BDesc[bit][cl].group, Code);
      Val(NextItem, BDesc[bit][cl].index, Code);
      IncS;
      if s^ = ' ' then
        tmpstr:= ''
      else
        NextItem;
      if Length(tmpstr) > BioNameLen then
        tmpstr[0]:= CHR(BioNameLen);
      StrPCopy(BDesc[bit][cl].Name, tmpstr);
    end;
  end;
end;

function TBiotypeObject.RecomputeBiotype: boolean;
{- recomputes the biotype based on the parameters passed to ComputeBiotype.
  ComputeBiotype MUST MUST MUST have been called before this routine }
begin
  ReComputeBiotype:= ComputeBiotype(Class, PosFlags,
                                    ChkFlags, MICData, oxires, indRes,
                                    betaHemres, virtualTests);
end;

procedure TBiotypeObject.PatchBDESC;
begin
  { if anaerobe panel and not walkaway then read INDole  }
  if tfsRec^.PSet = 3 then
  begin
    BDesc[1,1].Group:= 0; { Define as a normal bio bit. }
    BDesc[1,1].Index:= 57;  { INDole well # }
  end;
end;


procedure TBiotypeObject.CheckOxidaseORIndole(bitnum: Integer;
                                          var needed: Boolean);
var
  orgGrpObj: TOrgGrpObj;
begin
  needed:= FALSE;
  if BitNum >= 0 then
  begin
    if not Identify^.Identify(Biotype, BioMask, tfsRec^.PSet*10+Class) then
      exit;
    if (bitNum = oxidaseBitNum) then
      { result is needed if mixture of pos and neg, prompt }
      needed := (TestBioBit(Identify^.TestPos, BitNum)) and
                (TestBioBit(Identify^.TestNeg, BitNum));
    if bitNum = IndoleBitNum then
    begin
      orgGrpObj.Init;
      needed := ((Identify^.IDList.Probs.P^[1] < 91.0) and  { top org in list is < 91% }
                 TestBioBit(Identify^.TestPos, BitNum) and  { org prob >= 50% exists }
                 TestBioBit(Identify^.TestNeg, BitNum))     { org prob < 50% exists }
                or
                ((OrgGrpObj.OrgInGroup(Identify^.IDList.ReportOrgs.P^[1], gSalmo)) AND { top org in list is Salmonella }
                 (Identify^.IDList.Probs.P^[1] > 85.0)); { top org in list is > 85% }
      orgGrpObj.Done;
    end;
  end;
end;

function TBiotypeObject.ComputeBiotype( AClass       : integer;
                                        APosFlags,
                                        AChkFlags    : PPanelBitmap;
                                        AMICData     : PMICData;
                                        oxiResult,
                                        indResult,
                                        betaHemResult: integer;
                                        AVirtualTests: PPanelBitmap) : boolean;
{- Purpose : Compute the BIOTYPE from tray data. }
Const
  RNID3_IDRec  = 20;        {Identifies BDesc record number for RNID3 panels}
var
  BitValue    : boolean;
  BitNum      : integer;
  i,ret       : integer;
  BiotypeLen  : integer;
  resultsNeeded,
  isNeeded : boolean;
  RNID3IndoleValid : boolean;        {For RNID3, indicates indole was dispensed by WA}
begin
  ret:= 0;
  ComputeBiotype:= false;
  resultsNeeded := false;
  PosFlags:= APosFlags;
  ChkFlags:= AChkFlags;
  MICData:= AMICData;
  virtualTests:= AVirtualTests;
  indoleBitNum := -1;
  oxidasebitNum := -1;
  oxiRes := oxiResult;
  indRes := indResult;
  betaHemRes := betaHemResult;
  RNID3IndoleValid := False;         {RNID3 indole only}

  {- only get class from parameter when it is undefined (-1) }
  if Class = -1 then
  begin
    if AClass = 0 then
      Class:= 1
    else
      Class:= AClass;
  end;
  if tfsRec^.pSet = 3 then
    bDescClass := 1
  else
    bDescClass := Class;

  ScanBDesc(tfsRec^.IDRec);
  for BitNum:= 0 to MaxBioChems do
    if BDesc[BitNum, bDescClass].group <> 254 then
      MaxBitNum:= BitNum;
  BiotypeLen:= (MaxBitNum + 2) DIV 3;

  FillChar(bioType[1], BioTypeLen+1, '0');
  biotype[0]:= Chr(biotypelen);
  FillChar(bioMask[1], BioTypeLen+1, '0');
  biomask[0]:= Chr(biotypelen);
  FillChar(neededResults[1], BioTypeLen+1, '0');
  neededResults[0]:= Chr(biotypelen);

  PatchBDESC;

  for BitNum:= 0 to MaxBioChems do { for each bit of biotype }
  begin
    BitValue:= FALSE;
    with BDesc[BitNum, bDescClass] do
    begin
      case Group of
        { -5 means this bit is "indole valid" bit }
        251:  Begin                                    {IndoleValid must precede Indole in BDesc rec}
                If (tfsRec^.IDRec = RNID3_IDRec) then  {Retrieve bit from WA data for RNID3}
                Begin
                  If IndRes = 0 then                   {Result has not been determined manually}
                    RNID3IndoleValid := IndWasDispd    {Did indole dispense occur?}
                  Else                                 {Manual result exists}
                    RNID3IndoleValid := True;          {Assume manual results are valid}
                  BitValue := RNID3IndoleValid;
                End
                Else                                   {RNID2 does not support IndoleValid}
                  BitValue:= TRUE;                     {init bit to 'not tested'}
              End;

        { -4 tells us where indole bit is }
        252:  Begin                                    {IndoleValid must precede Indole in BDesc rec}
                If (tfsRec^.IDRec = RNID3_IDRec) then  {Retrieve bit from WA data for RNID3}
                Begin
                  If IndRes = 0 then                   {Result has not been determined manually}
                  Begin
                    If RNID3IndoleValid then           {Indole result depends on valid indole dispense}
                      BitValue:= TestBit(PosFlags^, index)  {Dispense occurred so result depends on growth}
                    Else
                    Begin                              {No dispense so have user call result}
                      BitValue:= false;
                      indoleBitNum := bitNum;
                      SetBioBit(bioMask,BitNum);       {Mask indole for now}
                      resultsNeeded := true;           {Indole on rapid panel can be read manually}
                    End;
                  End
                  Else                                 {Manual result exists}
                    BitValue:= (IndRes = 3);           {Get it from IndRes}
                End
                Else                                   {RNID2 does not support Indole}
                  BitValue:= FALSE;                    {init bit to neg result}
              End;

        { virtual test results - fluoro panels only }
        253:  bitValue:= TestBit(virtualTests^,index);

        254:  {UnusedBit:= TRUE};

        { Oxidase, INDole(rapid anaerobe) or Beta Hem }
        255:  begin
              case index of
                0  :  begin
                      oxidaseBitNum := bitNum;
                      if oxiRes = 0 then
                        begin
                          BitValue:= FALSE;
                          SetBioBit(bioMask,BitNum);
                          resultsNeeded := true;
                        end
                        else
                          BitValue:= oxiRes=3;
                      end;
                1  :  begin
                      if betaHemRes = 0 then
                        BitValue:= FALSE
                      else
                        BitValue:= betaHemRes = 3;
                    end;
                57 :  begin
                      indoleBitNum := bitNum;
                      if indRes = 0 then
                        begin
                          BitValue:= FALSE;
                          SetBioBit(bioMask,BitNum);
                          resultsNeeded := true;
                        end
                        else
                          BitValue:= indRes = 3;
                      end;
              end;
            end;

        { biochemical result }
        0 : begin
              if (ChkFlags <> nil) and (TestBit(ChkFlags^, index)) then
              begin
                SetBioBit(neededResults, BitNum);
                BitValue:= false;
              end
              else
                BitValue:= TestBit(PosFlags^, index);
            end;

        { anything else is MIC result }
      else
        begin
          { true if mic is above given threshold }
          BitValue:= (MICData^[Group] >= Index);
        end;
      end; { case }
    end; { with }

    if (BitValue) then
      SetBioBit(Biotype, BitNum);

  end; {for each biochem }

  { Remove leading zeroes }
  while Length(Biotype) > BiotypeLen do
  begin
    Delete(Biotype, 1, 1);
    Delete(BioMask, 1, 1);
  end;

  { See if need to fill in oxidase or indole }
  if resultsNeeded then
  begin
    if oxidasebitNum <> -1 then
    begin
      isNeeded := false;
      CheckOxidaseORIndole(oxidaseBitNum, isNeeded);
      if isNeeded then
        SetBioBit(neededResults, oxidaseBitNum)
    end
    else if indoleBitNum <> -1 then
    begin
      isNeeded := false;
      CheckOxidaseORIndole(indoleBitNum, isNeeded);
      if isNeeded then
        SetBioBit(neededResults, indoleBitNum);
    end;
  end;

  {- now check the neededResults array to see if any results are needed. If so
     return false }
  resultsNeeded:= false;
  for i:= 1 to Length(neededResults) do
  begin
    if neededResults[i] <> '0' then
    begin
      resultsNeeded:= true;
      break;
    end;
  end;

  ComputeBiotype:= not resultsNeeded;
end;

function TBiotypeObject.BitInfo(BitNum: integer;
                            var Value,
                                wellnum,
                                group: integer;
                                Desc: PChar): boolean;
{- NOTE: ComputeBiotype or SetBiotype must be called first. }
var
  Used  : boolean;
begin
  if TestBioBit(Biotype, BitNum) then
    Value:= 1
  else
    Value:= 0;
  Used:= BDesc[BitNum][bDescClass].Group <> 254;  { -2 }
  wellNum := BDesc[BitNum][bDescClass].index;
  group := BDesc[BitNum][bDescClass].group;
  if (BDesc[BitNum][bDescClass].group = 255) then {oxidase or indole or hem}
    if (tfsRec^.pSet = 3) or (tfsRec^.pset = 1) then { if a neg or anearobe }
      value := -1;
  if Used then
    StrCopy(Desc, BDesc[BitNum][bDescClass].Name);
  BitInfo:= Used;
end;

procedure TBiotypeObject.SetBiotype(AClass: integer; ABiotype: PChar);
{- Initialize biotype for BitInfo method. }
var
  BitValue  : boolean;
  BitNum    : integer;
  BiotypeLen: integer;
  i         : integer;
begin
  {- only get class from parameter when it is undefined (-1) }
  if Class = -1 then
  begin
    if AClass = 0 then
      Class:= 1
    else
      Class:= AClass;
  end;
  if tfsRec^.pSet = 3 then
    bDescClass := 1
  else
    bDescClass := Class;

  ScanBDesc(tfsRec^.IDRec);
  BiotypeLen:= (MaxBioChems + 2) DIV 3;
  Biotype:= StrPas(ABiotype);
  while Length(Biotype) < BiotypeLen do
    Biotype:= '0' + Biotype;

  { if anaerobe panel and not walkaway then read INDole  }
  if tfsRec^.PSet = 3 then
  begin
    BDesc[1,1].Group:= 0; { Define as a normal bio bit. }
    BDesc[1,1].Index:= 57;  { INDole well # }
  end;
  for BitNum:= 0 to MaxBioChems do
    if BDesc[BitNum, bDescClass].Group <> 254 then
      MaxBitNum:= BitNum;
  for BitNum:= 0 to MaxBioChems do  { for each bit of biotype }
  begin
    BitValue:= TestBioBit(Biotype, BitNum);
    with BDesc[BitNum, bDescClass] do
    begin
      case Group of
        { -5 means this bit is "indole valid" bit }
        251:  {indTestBit:= i};

        { -4 tells us where indole bit is }
        252:  begin
            end;

        { virtual test results - fluoro panels only }
        253: ;
        254: {UnusedBit:= TRUE};

        { Oxidase, INDole(rapid anaerobe) or Beta Hem }
        255:  begin
            end;

        { biochemical result }
        0:    begin
            end;

        { anything else is MIC result }
        else
        begin
        end;
      end; { case }
    end; { with }
  end; {for each biochem }

  { Remove leading zeroes }
  BiotypeLen:= (MaxBitNum + 2) DIV 3;
  while Length(Biotype) > BiotypeLen do
    Delete(Biotype, 1, 1);
end;

function TBiotypeObject.GetTopID(
  var orgNum   : integer;
  probTxt      : PChar;
  Footnotes    : PChar;
  SpecialChars : PChar)
  : boolean;
{- return the top organism. Returns FALSE if invalid biotype, otherwise
  returns TRUE and id will contain the organism number (0 for very rare
  biotype) }
var
  i               : integer;
  j               : integer;
  setClass        : integer;
  tstr            : string[80];
  OrgList         : TOrgList;
  TheFootnotes    : String;
  TheSpecialChars : String;
begin
  GetTopID:= false;
  orgNum := 0;
  if probTxt <> nil then
    StrCopy(probTxt, '');

  setClass:= tfsRec^.PSet * 10 + Class;

  if setClass = 0 then
    setClass:= 201;

  if (setClass MOD 10) = 0 then
    setClass:= SetClass + 1;

  if not Identify^.Identify(biotype, bioMask, setClass) then
    Exit;

  if not Identify^.InvalidBiotype then
  begin
    if Identify^.IDList.NumOrgs > 0 then
    begin
      orgNum := Identify^.IDList.ReportOrgs.P^[1];
      if probTxt <> nil then
      begin
        Str(identify^.IDList.PROBS.P^[1]:4:1, tstr);
        if tStr = '100.0' then
          tStr := '99.9';
        tstr:= tstr + '%';
        StrPCopy(probTxt, tstr);
      end;
      For j := 1 To Identify^.IDList.NumOrgs Do
        OrgList[j] := Identify^.IDList.ReportOrgs.P^[j];
      If Footnotes <> Nil then
      Begin
        OrgObject.ReportFootNotes(Identify^.IDList.NumOrgs,1,OrgList,BioType,TheFootnotes);
        StrCopy(Footnotes,Addr(TheFootnotes));
      End;
      If SpecialChars <> Nil then
      Begin
        OrgObject.ReportSpclChars(Identify^.IDList.ReportOrgs.P^[1],30,TheSpecialChars);
        StrCopy(SpecialChars,Addr(TheSpecialChars));
      End;
      GetTopID := true;
    end
    else
    begin
      {a 0 orgNum (VRB) is returned which is not invalid}
      GetTopID := true;
    end;
  end;
end;

function TBiotypeObject.DrugUsed(drugRDN: integer): boolean;
var
  BitNum    : integer;
begin
  DrugUsed := FALSE;
  for BitNum:= 0 to MaxBitNum do { for each bit of biotype }
  begin
    with BDesc[BitNum, bDescClass] do
    begin
      case Group of
        { -5 means this bit is "indole valid" bit }
        251:  ;
        { -4 tells us where indole bit is }
        252:  ;
        { -3 means virtual test results - fluoro panels only }
        253:  ;
        { -2 means not used }
        254:  ;
        { -1 means Oxidase, INDole(rapid anaerobe) or Beta Hem }
        255:  ;
        { biochemical result }
        0 :   ;

        { anything else is MIC result }
        else
        begin
          if drugRDN = Group then
          begin
            DrugUsed := TRUE;
            Break;
          end;
        end;
      end; { case }
    end; { with }
  end; {for each biochem }
end;

procedure TWABiotypeObject.PatchBDESC;
begin
  { if anaerobe panel and walkaway then dont read INDole  }
  if tfsRec^.PSet = 3 then
  begin
    BDesc[1,1].Index:= 57;  { INDole well # }
  end;
end;

END.

