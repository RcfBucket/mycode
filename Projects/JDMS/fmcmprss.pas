{----------------------------------------------------------------------------}
{  Module Name  : FMCMPRSS.PAS                                               }
{  Programmer   : DWC                                                        }
{  Date Created : 05/11/95                                                   }
{                                                                            }
{  Purpose -                                                                 }
{  This module allows the user to compress the Nihongo database.             }
{                                                                            }
{  Assumptions -                                                             }
{  None.                                                                     }
{                                                                            }
{  Initialization -                                                          }
{  None.                                                                     }
{                                                                            }
{  Revision History -                                                        }
{                                                                            }
{  Version  Date      Who    Did What                                        }
{  -------  --------  ------ ----------------------------------------------- }
{  1.00     05/11/95  DWC     Initial release.                               }
{                                                                            }
{----------------------------------------------------------------------------}

unit FMCMPRSS;

INTERFACE

uses
  OWindows;


procedure FMCompress(aParent: PWindowsObject);

IMPLEMENTATION

uses
  mscan,
  Objects,
  fmconst,
  fmtypes,
  fmglobal,
  fmerror,
  status,
  ApiTools,
  WinTypes,
  WinProcs,
  WinDOS,
  DBFile,
  DBLib,
  DBTypes,
  DMString,
  Strings;

{$I STATUS.INC}

const
  TempDBFileName = 'TEMP';

type
  PCompressDlg = ^TCompressDlg;
  TCompressDlg = object(TPctStatDlg)
    {}
    procedure  Cancel(var msg: TMessage); virtual ID_FIRST + ID_CANCEL;
  end;


procedure TCompressDlg.Cancel(var Msg: TMessage);
var
  cancelMsg  : array [0..255] of char;
  confirmMsg : array [0..255] of char;

begin
  MessageBeep(MB_ICONQUESTION);
  SR(IDS_CONFIRM, confirmMsg,255);
  SR(IDS_CANCELMSG, cancelMsg, 255);
  if YesNoMsg(hwindow, confirmMsg, cancelMsg) then
    SetContinue(false);
end;

{-----------------------------------------------------------------------------}
{                File Compression Support Routines                            }
{-----------------------------------------------------------------------------}
{-----------------------------------------------------------------------------}
{                 Routine                                                     }
{-----------------------------------------------------------------------------}
function FileCountLock(aDlg              :PPctStatDlg;
                        fList            :PCollection;
                        aDir             :PChar;
                        anExt            :PChar;
                        var totalRecords : longint): integer;

var
  retVal  : integer;
  filePath: array[0..maxPathLen] of char;
  fileDir : array[0..maxPathLen] of char;
  fileName: array[0..maxFileNameLen] of char;
  fileExt : array[0..fsExtension] of char;
  dirInfo : TSearchRec;
  pStr    : array[0..100] of char;
  db      : PDBReadWriteFile;
  numRec  : longint;

begin
  retVal := continueJob;

  StrCopy(filePath, aDir);
  StrCat(filePath, '*');
  StrCat(filePath, anExt);
  FindFirst(filePath, faAnyFile - faDirectory - faVolumeID, dirInfo);
  while (DosError = 0) and (retVal = continueJob) and (aDlg^.CanContinue) do
  begin
    FileSplit(dirInfo.Name, fileDir, fileName, fileExt);
    db:= New(PDBReadWriteFile, Init(fileName, aDir, DBOpenExclusive));
    if (db = nil) then
    begin
      retVal := fmErr_OpeningFileExclus;
      StrCopy(fmErr_msg, aDir);
      StrCat(fmErr_msg,fileName);
    end
    else
    begin
      numRec := db^.dbc^.NumRecords;
      Dispose(db, Done);
      totalRecords := totalRecords + numRec;
      fList^.Insert(New(PCompressFileEntry, Init(fileName,numRec)));
    end;
    FindNext(dirInfo);
    aDlg^.Update;
  end;
  FileCountLock := retVal;
end;

{-----------------------------------------------------------------------------}
{                 Routine                                                     }
{-----------------------------------------------------------------------------}
procedure SetFileName(StatDlg:PPctStatDlg; desc:PChar);
begin
  StatDlg^.SetText(desc, IDC_TEXT1);
end;

{-----------------------------------------------------------------------------}
{                 Routine                                                     }
{-----------------------------------------------------------------------------}
procedure SetFileNumber(StatDlg:PPctStatDlg; numFiles:integer);
var
  pStr    : array[0..100] of char;
  pNStr    : array[0..5] of char;
  srStr   : array[0..30] of char;

begin
  StrCopy(pStr,SR(IDS_FMREMAINMSG, srstr, 30));
  str(numFiles,pNStr);
  StrCat(pStr,' ');
  StrCat(pStr,pNStr);
  StatDlg^.SetText(pStr,IDC_TEXT3);
end;

{-----------------------------------------------------------------------------}
{                 Routine                                                     }
{-----------------------------------------------------------------------------}
function  ReadWrite(aDlg         : PPctStatDlg;
                    db           : PDBReadWriteFile;
                    tempdb       : PDBCopyFile;
                    numRec       : longint;
                    totalRecords : longint;
                    recCount     : longint;
                    fileName     : PChar): integer;

var
  i       :longint;
  retVal  : integer;

begin
  retVal := continueJob;
  i := 0;

  if db^.dbc^.GetFirst(db^.dbr) then
    repeat
      tempdb^.dbr^.CopyRecord(db^.dbr);
      if not tempdb^.dbc^.InsertRec(tempdb^.dbr) then
      begin
        retVal := fmErr_InsertingRecord;
        StrCopy(fmErr_Msg,fileName);
      end;
      inc(i);
      inc(recCount);
      aDlg^.compPctLevel(i,numRec);
      aDlg^.Update;
    until not db^.dbc^.GetNext(db^.dbr);

  ReadWrite := retVal;
end;

{-----------------------------------------------------------------------------}
{                 Routine                                                     }
{-----------------------------------------------------------------------------}
function ProcessRecords(aDlg         :PPctStatDlg;
                        db           :PDBReadWriteFile;
                        numRecords   : longInt;
                        totalRecords : longInt;
                        var recCount : longInt): integer;
var
  theCreator : TDBCreator;
  newDBD     :PDBReadWriteDesc;
  pathstr    : array[0..255] of char;
  pstr       : array[0..255] of char;
  pStr2      : array[0..255] of char;
  filestr    : array[0..255] of char;
  retCode    : integer;
  tempdb     : PDBCopyFile;
  tempDBDesc : PDBReadWriteDesc;
  info       : TFileInfo;
  fil        : file;
  retVal     : integer;

begin
  retVal := continueJob;

  aDlg^.ResetPct;
  db^.dbd^.FileInfo(info);
  db^.dbd^.FilePath(pathstr, maxPathLen);
  newDBD:= New(PDBReadWriteDesc, Init(TempDBFileName, pathstr));
  newDBD^.CopyDesc(db^.dbd);

  {$I-}
  StrCopy(pStr, pathstr);
  StrCat(pStr, TempDBFileName);
  StrCat(pStr, fileDescExt);
  Assign(fil, pStr);
  Erase(fil);
  retCode:= IOResult;
  if ((retCode <> 0) and (retCode <> 2)) then
  begin
    retVal := fmErr_DeletingFile;
    StrCopy(fmErr_Msg,pStr)
  end;

  aDlg^.Update;
  StrCopy(pStr, pathstr);
  StrCat(pStr, TempDBFileName);
  StrCat(pStr, datFileExt);
  Assign(fil, pStr);
  Erase(fil);
  retCode:= IOResult;
  if ((retCode <> 0) and (retCode <> 2)) then
  begin
    retVal := fmErr_DeletingFile;
    StrCopy(fmErr_Msg,pStr)
  end;
  {$I+}

  if not theCreator.CreateTable(newDBD) then
  begin
    retVal := fmErr_CreatingFile;
    StrCopy(fmErr_Msg,pathstr);
    StrCat(fmErr_Msg,TempDBFileName);
  end;

  tempdb:= New(PDBCopyFile, Init(TempDBFileName, pathstr, DBOpenExclusive));
  if (tempdb = nil) then
  begin
    retVal := fmErr_OpeningFileExclus;
    StrCopy(fmErr_msg, pathstr);
    StrCat(fmErr_msg,TempDBFileName);
  end;

  retVal := ReadWrite(aDlg, db, tempdb, numRecords,totalRecords,recCount, TempDBFileName);
  aDlg^.Update;

  Dispose(db, Done);
  Dispose(tempdb, Done);
  tempDBDesc:= new(PDBReadWriteDesc, Init(info.fName, pathstr));
  retCode:= DBDeleteDatabase(tempDBDesc);
  if (retCode <> 0) then
  begin
    retVal := fmErr_DeletingFile;
    StrCopy(fmErr_Msg,pathstr);
    StrCat(fmErr_Msg,info.fName);
  end;
  if (not newDBD^.RenameDatabase(info.fName)) then
  begin
    retVal := fmErr_RenamingFile;
    StrCopy(fmErr_Msg,info.fName);
  end;

  Dispose(newDBD, Done);

  ProcessRecords := retVal;
end;

{-----------------------------------------------------------------------------}
{                 Routine                                                     }
{-----------------------------------------------------------------------------}
function FileCompress(aDlg             :PPctStatDlg;
                      fList            :PCollection;
                      aDir             :PChar;
                      var totalRecords :longint ): integer;
var
  fileName : array[0..maxFileNameLen] of char;
  db       : PDBReadWriteFile;
  dbInfo   : TFileInfo;
  pStr     : array[0..100] of char;
  retVal   : integer;
  j        : integer;
  aEntry   : PCompressFileEntry;
  numFiles : integer;
  recCount : longint;

begin
  retVal := continueJob;
  recCount := 0;

  j := 0;
  numFiles := fList^.count;
  while (j <= fList^.count-1) and (retVal = continueJob) and (aDlg^.CanContinue) do
  begin
    aEntry := fList^.At(j);
    db:= New(PDBReadWriteFile, Init(aEntry^.fileName, aDir, DBOpenExclusive));
    if (db = nil) then
    begin
      retVal := fmErr_OpeningFileExclus;
      StrCopy(fmErr_msg, aDir);
      StrCat(fmErr_msg,aEntry^.fileName);
    end
    else
    begin
      db^.dbd^.FileInfo(dbInfo);
      SetFileName(aDlg, dbInfo.desc);
      SetFileNumber(aDlg, numFiles);
      retVal := ProcessRecords(aDlg,db,aEntry^.numRecords,totalRecords,recCount);
      numFiles := numFiles - 1;
    end;
    inc(j);
  end;
  FileCompress := retVal;
end;

{-----------------------------------------------------------------------------}
{                 Routine                                                     }
{-----------------------------------------------------------------------------}
procedure FMCompress(aParent: PWindowsObject);
var
  dlg          : PCompressDlg;
  dbDir        : array[0..maxPathLen] of char; { directory location of database files }
  totalRecords : longint;
  compressMsg  : array [0..255] of char;
  updateMsg    : array [0..255] of char;
  fList        : PCollection;
  retVal       : integer;
  srstr        : array[0..25] of char;
  drive        : byte;

begin
  retVal := continueJob;
  totalRecords := 0;
  fList := New(PCollection, Init(20, 5));
  StrCopy(fmErr_Msg,'');

  GetCurrentDir(dbDir, drive);
  dlg:= New(PCompressDlg,Init(aParent, MakeIntResource(DLG_STATUS), true, IDC_PERCENTBAR));
  application^.MakeWindow(Dlg);
  dlg^.SetText(' ',IDC_TEXT1);
  dlg^.SetText(' ',IDC_TEXT2);
  dlg^.SetText(' ',IDC_TEXT3);
  dlg^.SetText(SR(IDS_FMCDF, srstr, 25),0);
  dlg^.Update;

  retVal := FileCountLock(dlg,fList,dbDir,fileDescExt,totalRecords);

  dlg^.Update;
  if (dlg^.CanContinue) and (retVal = continueJob) then
    retVal := FileCompress(dlg,fList,dbDir,totalRecords);

  if (retVal <> continueJob) and (retVal <> cancelJob) then
  begin
    SR(IDS_COMPRESSTITLE, updateMsg, 255);
    FMShowError(dlg, updateMsg, retVal, fmErr_Msg);
  end;

  Dispose(dlg, Done);
end;

END.
{- DWC }
