{***** PRNOBJ resource constants *****

  Module ID = $70F0 = 28912

******}

const
  CM_25             = 201;
  CM_50             = 202;
  CM_75             = 203;
  CM_100            = 204;
  CM_125            = 205;
  CM_150            = 206;
  CM_175            = 207;
  CM_200            = 208;
  CM_PREVIOUS       = 209;
  CM_PRINT          = 210;
  CM_SETUP          = 211;
  CM_CLOSEWIN       = 212;
  CM_NEXT           = 213;
  CM_TOP            = 214;
  CM_BOTTOM         = 215;
  CM_GOTO           = 216;
  CM_DEBUG          = 217;

  DLG_GOTO          = 28912;
  DLG_PRINTPREV     = 28913;
  DLG_PERCENT       = 28914;

  IDC_PERCENT       = 200;

  MENU_MAIN         = 28912;

  IDC_PRINT         = 101;
  IDC_SETUP         = 102;
  IDC_PREVIEW       = 103;
  IDC_PNAME         = 200;
  IDC_GOTONUM       = 201;
  IDC_TITLE         = 203;

  IDS_ERROR         = 28912;
  IDS_INVALIDDEVICE = 28913;
  IDS_NOPRINTER     = 28914;
  IDS_OF            = 28915;
  IDS_SCANNING      = 28916;
