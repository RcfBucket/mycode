Unit WLWAGPIB;

Interface

Uses WObjects, WinTypes, WinProcs, WLDMSErr, WLmScan, WLTrays, wlHWConf, Strings, WLDMStrg;

CONST
  MaxGPIBBytes  = 4160;
  WAStrBase     = 6100;  { base in DMString }

  {**  W/A Commands Constants **}

  ABFcmd        = $0801; {** ABORT VOLUME FORMAT **}
  AIRcmd        = $3004; {** REPORT 620 AIR READ FROM LAST CALIBRATE **}
  BOOTcmd       = $0001; {** BOOT OS **}
  BSIcmd        = $2002; {** BARCODE SCAN INQUIRY COMMAND **}
  CAFcmd        = $200B; {** CLOSE APP DATA FILES **}
  CARcmd        = $2103; {** CANCEL ACCESS REQUEST **}
  CLOSEcmd      = $0805; {** CLOSE THE CURRENTLY OPENED FILE **}
  CLTcmd        = $3001; {** CALIBRATION DATE TIME INQUIRY **}
  CREATEcmd     = $0807; {** CREATE A NEW FILE **}
  CSTcmd        = $2202; {** CANCEL STERILIZATION **}
  DACcmd        = $3002; {** WASUP DAC SETTING INQUIRY. **}
  DFMTcmd       = $0003; {** LOW-LEVEL DISK FORMAT **}
  DVIcmd        = $2005; {** DATA AVAILABLE INQUIRY **}
  ECHOcmd       = $001F; {** ECHO DATA COMMAND (COMM. TEST) **}
  EDLcmd        = $0005; {** ENTER DEFECT LIST **}
  ERAcmd        = $080D; {** ERASE A FILE **}
  EXcmd         = $0006; {** EXECUTE AT ADDRESS **}
  FASTcmd       = $0810; {** TURN W/A HARD DISK IMMEDIATE UPDATE OFF **}
  GETWATYPEcmd  = $1256; {** RETURN THE TYPE AND SIZE OF THE W/A **}
  GSIZEcmd      = $001C; {** GPIB BUFFER SIZE **}
  HOMcmd        = $2204; {** HOME ALL AXES **}
  HSIcmd        = $000C; {** HARDWARE STATUS INQUIRY **}
  HugeECHOcmd   = $001F; {** ECHO DATA COMMAND (COMM. TEST) **}
  IDIcmd        = $0007; {** IDENTIFY CODE LEVEL **}
  IDPHOTOcmd    = $3000; {** IDENTIFY ACTIVE PHOTOSYSTEM **}
  INTcmd        = $3003; {** WASUP INTEGRATE TIME INQUIRY. **}
  JRADSYScmd    = $3106; {** REPORT A/D SYSTEM CALIBRATION PARAMETERS. **}
  JRAIRcmd      = $3102; {** REPORT FLUOROMETER STATUS. **}
  JRENVSTATcmd  = $3107; {** REPORT VARIOUS ENVIRONMENTAL PARAMETERS. **}
  JRFLUORcmd    = $3105; {** REPORT FLUOROMETER STATUS. **}
  JRGAINScmd    = $3100; {** REPORT GAINS USED FOR EACH FILTER BY DIODE. **}
  KILLCmd       = $2108; {** Terminate Panel Processing **}
  LARcmd        = $2101; {** LOAD ACCESS **}
  MARcmd        = $2102; {** MAINTENANCE ACCESS **}
  MPPURGEcmd    = $210A; {** MAINTENANCE POSITION FOR PURGE **}
  MPREFcmd      = $210C; {** MAINTENANCE POSITION FOR CLEANING REFERENCE DISK **}
  MPSHIELDcmd   = $210D; {** MAINTENANCE POSITION FOR CLEANING SHIELD **}
  OPENcmd       = $0814; {** OPEN A FILE **}
  PDIcmd        = $2008; {** PROCESSED DATA INQUIRY **}
  PEIcmd        = $2009; {** VIRTUAL TESTS INQUIRY **}
  PFIcmd        = $0010; {** POWER FAIL INQUIRY **}
  PMIcmd        = $2007; {** MIC INQUIRY **}
  PRIcmd        = $2006; {** PANEL RESULTS INQUIRY **}
  PSIcmd        = $2004; {** PANEL STATUS INQUIRY **}
  PSTcmd        = $2203; {** PREP FOR STERILIZE **}
  PURcmd        = $2107; {** PURGE REAGENT **}
  QSTcmd        = $2205; {** QUICKY SELF TEST **}
  RDIcmd        = $200A; {** RAW DATA INQUIRY **}
  READcmd       = $0816; {** READ SOME BYTES FROM THE CURRENTLY OPEN FILE **}
  READREGcmd    = $001B; {** READ CMOS REGISTER **}
  RESETcmd      = $0013; {** RESET SYSTEM (REBOOT) **}
  ROIcmd        = $200F; {** GET RAW DATA OFFSET **}
  ROMCMDcmd     = $0024; {** ROMCMD CALL **}
  RSTcmd        = $000B; {** RESET SYSTEM TIME.  SETS INTERNAL  **}
  RSTPURcmd     = $2109; {** RESET THE LAST PURGE TIME TO NOW **}
  SAFEcmd       = $0811; {** TURN W/A HARD DISK IMMEDIATE UPDATE ON. **}
  SAVEcmd       = $081A; {** SAVE THE OPERATING SYSTEM ON THE DISK **}
  SCIcmd        = $2003; {** SLOT CONTENT INQUIRY **}
  SETREGcmd     = $001A; {** SET CMOS REGISTER **}
  SSCcmd        = $2104; {** SET SLOT CONTENTS **}
  SSIcmd        = $2001; {** SLOT STATUS INQUIRY COMMAND **}
  SSTcmd        = $000F; {** SET SYSTEM TIME **}
  STIcmd        = $081D; {** STATUS INQUIRY **}
  STOcmd        = $0008; {** STORE MEMORY IMAGE **}
  STRcmd        = $2201; {** STERILIZE **}
  UNMPPURGEcmd  = $210B; {** RETURN FROM MAINTENANCE PURGE POSITION **}
  USEDOFScmd    = $3103; {** REPORT OFFSETS ACTUALLY USED. **}
  USEDVOLTScmd  = $310D; {** REPORT USED DAC VOLTAGES **}
  VFMTcmd       = $081F; {** VOLUME FORMAT **}
  WATYPEcmd     = $210E; {** RETURN W/A TYPE AND HEIGHT **}
  WRITEcmd      = $0820; {** WRITE A RECORD TO DISK **}
  SetPDLCmd     = $2106; { Send PDL file to W/A }

  MaxDefects    = 30;    { For EDL command }
  MaxSectors    = 32;    { For block transfer commands }

{** Message Lengths **}
  SSTLen       = 7;
  SCILen       = 1;
  PSILen       = 2;
  PDILen       = 3;
  RDILen       = 4;
  PURLen       = 1;
  LARLen       = 1;
  SSCLen       = 5;
  CDILen       = 1;
  ECHOLen      = 257;
  ROMCMDLen    = 4;
  ROILen       = 2;
  RRegLen      = 1;
  SRegLen      = 5;
  RESETLen     = 1;

{** Panel Status Codes **}
  SlotEmpty         = 0;
  NoDataYet         = 1;
  MICsAvailable     = 2;
  ReagentsAdded     = 3;
  ProcessComplete   = 4;
  PanelUnrecognized = 5;
  AbortedPanel      = 6;
  PanelMissing      = 7;
  DuplicatePanelId  = 8;
  LampFailure       = 9;

{** Max # of MICs + extra tests back from PMI command = maxmics + 4 **}
  MaxPmi       = 48;

{ return values for ReadyForWAMessage }
  AOK        = 0;
  TryAgain   = 1;
  QuitTrying = 2;

{ WA TYPES }
  ClassicWA = 0;
  WA40      = 1;
  WA96      = 2;

TYPE

  PPanelFlags = ^TPanelFlags;
  TPanelFlags = OBJECT(TObject)
    flagVal        : INTEGER;
    PowerFailure   : BOOLEAN;
    ISFGrowth      : BOOLEAN;
    BadSterileWell : BOOLEAN;
    SkippedWells   : BOOLEAN;
    BadDCB         : BOOLEAN;
    BadOFB         : BOOLEAN;
    VerifyPanelId  : BOOLEAN;
    PanelInWA      : BOOLEAN;
    PossibleMRSASE : BOOLEAN;
    SlowGrower     : BOOLEAN;
    VirtualTests   : BOOLEAN;
    DelayedRead    : BOOLEAN;
    InvSterileWell : BOOLEAN;
    IDAvailOnRapid : BOOLEAN;
    NoDispense     : BOOLEAN;
    BadNPC         : BOOLEAN;
    BadBNAC        : BOOLEAN;
    CONSTRUCTOR Init(flags : INTEGER);
  END;

  TimeDate  = RECORD
                sec  : BYTE;
                min  : BYTE;
                hour : BYTE;
                day  : BYTE;
                month: BYTE;
                year : INTEGER;
              END;

  PfiRec    = RECORD
                pfiOff   : TimeDate;
                pfiOn    : TimeDate;
                pfiLen   : LONGINT;
              END;

  HugeArray   = ARRAY[1..MaxGpibBytes] Of Byte;

  SendBlockType = Record
                Class: Byte;    { 0 = data, 1 = start addr }
                LoadOffset: Integer;
                LoadSegment: Integer;
                SendData: HugeArray;
              End;

  Sector = Array [0..127] Of Byte;

  XferBlockType = Record
                Offset: Longint;
                Size: Integer;
                Data: Array [1..MaxSectors] Of Sector;
              End;

  WaDefectType = Record
      Cylinder: Integer;
      Track   : Byte;
  End;
  PDefectType = ^WaDefectType;

  StiArray    = ARRAY[1..12] OF BYTE;
  BsiArray    = ARRAY[1..96] OF BYTE;
  PriArray    = ARRAY[1..12] OF BYTE;
  PmiArray    = ARRAY[1..MaxPmi] OF BYTE;
  PdiArray    = ARRAY[1..12] OF LONGINT;
  CdiArray    = ARRAY[1..12] OF WORD;
  IntArray    = ARRAY[1..8] OF WORD;
  DacArray    = ARRAY[1..16] OF BYTE;
  RdiArray    = ARRAY[1..12] OF INTEGER;
  EchoArray   = ARRAY[1..255] OF BYTE;
  PDefectArray= ^DefectArray;
  DefectArray = ARRAY[1..MaxDefects] Of WaDefectType;

  JrGnArray   = ARRAY[1..72] OF BYTE;          { gain values for WA-40 color optics }
	JrAirArray  = ARRAY[1..72] OF WORD;          { Air reference reading by diode/filter combination }
	UsdOArray   = ARRAY[1..72] OF WORD;          { actual offset values used, by diode/filter combinations }
  JrDACArray  = ARRAY[1..72] of BYTE;          { actual values used in reading... }
	JrMscArray  = ARRAY[1..5] OF INTEGER;            { used to hold calibration values }

  PSendMsgRec = ^TSendMsgRec;
  TSendMsgRec = RECORD
                cmd    : INTEGER;
                msgLen : INTEGER;
                CASE INTEGER OF
                  1: (sstTime   : TimeDate);              {** SST **}
                  2: (sciSlot   : BYTE);                  {** SCI **}
                  3: (psiPanId  : INTEGER);               {** PSI,PRI,PMI,PEI **}
                  4: (pdiPanId  : INTEGER;                {** PDI **}
                      pdiRow    : BYTE);
                  5: (rdiPanId  : INTEGER;                {** RDI **}
                      rdiRow    : BYTE;
                      rdiFiltr  : BYTE);
                  6: (purReag   : WORD);                  {** PUR **}
                  7: (arMins    : BYTE);                  {** LAR,MAR **}
                  8: (sscSlot   : BYTE;                   {** SSC **}
                      sscType   : BYTE;
                      sscClass  : BYTE;
                      sscPanId  : INTEGER);
                  9: (cdiRow    : BYTE);                  {** CDI **}
                 10: (rmCmdNum  : INTEGER;                {** ROM **}
                      rmCmdAX   : INTEGER);
                 11: (echoMsg   : EchoArray;              {** ECHO,IDI **}
                      crcBytes  : INTEGER);
                 12: (RoiPanId  : integer);
                 13: (RegNum    : byte;                   {** ReadReg,SetReg **}
                      RegValue  : longint);
                 14: (ResetType : Byte);                  { ** RESET ** }
                 15: (DefectList: DefectArray);
                 16: (Passes    : Integer);               { ** VFMT ** }
                 17: (FileName  : String);                { ** CREATE, OPEN ** }
                 18: (Header    : Sector);                { ** Save OS ** }
              END;

  PRecvMsgRec = ^TRecvMsgRec;
  TRecvMsgRec = RECORD
                cmd : INTEGER;
                msgLen : INTEGER;
                status : INTEGER;
                CASE INTEGER OF
                  1: (stiBits  : StiArray);              {** STI,ERI **}
                  2: (pfiData  : PfiRec);                {** PFI **}
                  3: (bsiInfo  : BsiArray);              {** SSI,BSI **}
                  4: (psiPid   : INTEGER;                {** SCI,PSI,DVI **}
                      psiSlot  : BYTE;
                      psiStat  : BYTE;
                      psiFlags : INTEGER;
                      psiPtyp  : BYTE;
                      psiLastRd: TimeDate);
                  5: (priPid   : INTEGER;                {** PRI,PEI **}
                      priSlot  : BYTE;
                      priClass : BYTE;
                      priBits  : PriArray);
                  6: (pmiPid   : INTEGER;                {** PMI **}
                      pmiSlot  : BYTE;
                      pmiCount : BYTE;
                      pmiMICs  : PmiArray);
                  7: (pdiPid   : INTEGER;                {** PDI **}
                      pdiSlot  : BYTE;
                      pdiRow   : BYTE;
                      pdiData  : PdiArray);
                  8: (cdiInfo  : CdiArray);              {** CDI **}
                  9: (rdiPid   : INTEGER;                {** RDI **}
                      rdiSlot  : BYTE;
                      rdiFiltr : BYTE;
                      rdiRow   : BYTE;
                      rdiData  : RdiArray);
                 10: (arDelay  : BYTE;                   {** LAR,MAR **}
                      arDuratn : BYTE);
                 11: (qstStat  : INTEGER);               {** QST **}
                 12: (calDate  : TimeDate);
                 13: (intInfo  : IntArray);              {** INT **}
                 14: (dacInfo  : DacArray);              {** DAC **}
                 15: (errPid   : INTEGER;                {** ERROR **}
                      MWErrNum : WORD);
                 16: (jrGains  : JrGnArray);             {** JRGAINS **}
                 17: (jrOffsts : UsdOArray);             {** USED OFFSETS **}
                 18: (jrAirs   : JrAirArray);            {** JRAIRS **}
                 19: (jrMisc   : JrMscArray);            {** JRFLUOR etc **}
                 20: (jrDACs   : JrDACArray);
                 21: (echoMsg  : EchoArray;              {** ECHO,IDI **}
                      crcBytes : INTEGER);
                 22: (RoiPanID : integer;
                      RoiSlot  : byte;
                      RoiOffset: RdiArray);
                 23: (RegNum   : byte;                   {** ReadReg **}
                      RegValue : longint);
              END;

  PHugeSendMsgRec = ^THugeSendMsgRec;
  THugeSendMsgRec = Record
                cmd : INTEGER;
                msgLen : INTEGER;
                Case Integer Of
                  1:  (Data      : HugeArray);
                  2:  (SendBlock : SendBlockType);
                  3:  (XferBlock : XferBlockType);
  End;

  PHugeRecvMsgRec = ^THugeRecvMsgRec;
  THugeRecvMsgRec = Record
                cmd : INTEGER;
                msgLen : INTEGER;
                status : INTEGER;
                Case Integer Of
                  1:  (Data: HugeArray);
                  2:  (XferBlock: XferBlockType);
  End;

  PBaseWACommand = ^TBaseWACommand;
  TBaseWACommand = OBJECT (TObject)
    WAID : INTEGER;
    WAStatNum : INTEGER;
    WAErrNum : INTEGER;
    WAGpibErrNum: INTEGER;
    ErrorMode: INTEGER;

    CONSTRUCTOR Init(id : INTEGER);
    FUNCTION WAStatus(statNum : INTEGER) : BOOLEAN; VIRTUAL;
    FUNCTION WAError(errNum : INTEGER) : BOOLEAN; VIRTUAL;
    FUNCTION GPIBError(errNum : INTEGER) : BOOLEAN; VIRTUAL;
    PROCEDURE SetErrorMode (NewErrorMode: INTEGER); VIRTUAL; { 0 = Call SetError on GPIB errors }
                                                          { 1 = Don't call SetError on GPIB errors }
  END;

  PWACommand = ^TWACommand;
  TWACommand = OBJECT(TBaseWACommand)
    sendMsg : PSendMsgRec;
    recvMsg : PRecvMsgRec;
    CONSTRUCTOR Init(id : INTEGER);
    DESTRUCTOR Done; VIRTUAL;
    FUNCTION SendWAMessage : BOOLEAN; VIRTUAL;
    { private method }
    PROCEDURE MakeSendMessage; VIRTUAL;
    FUNCTION WAReadyForMessage : INTEGER; VIRTUAL;
  END;

  PHugeWACommand = ^TWACommand;
  THugeWACommand = OBJECT(TBaseWACommand)
    sendMsg : PHugeSendMsgRec;
    recvMsg : PHugeRecvMsgRec;
    CONSTRUCTOR Init(id : INTEGER);
    DESTRUCTOR Done; VIRTUAL;
    FUNCTION SendWAMessage : BOOLEAN; VIRTUAL;
    { private method }
    PROCEDURE MakeSendMessage; VIRTUAL;
  END;

  PWAABFcmd = ^TWAABFCmd;
  TWAABFCmd = OBJECT(TWACommand)
    PROCEDURE MakeSendMessage; VIRTUAL;
  END;

  PWAAIRcmd = ^TWAAIRCmd;
  TWAAIRCmd = OBJECT(TWACommand)
    PROCEDURE MakeSendMessage; VIRTUAL;
    PROCEDURE SetParams(row : BYTE); VIRTUAL;
  END;

  PWABOOTcmd = ^TWABOOTCmd;
  TWABOOTCmd = OBJECT(TWACommand)
    PROCEDURE MakeSendMessage; VIRTUAL;
  END;

  PWABSIcmd = ^TWABSICmd;
  TWABSICmd = OBJECT(TWACommand)
    PROCEDURE MakeSendMessage; VIRTUAL;
    PROCEDURE GetParams; VIRTUAL;
  END;

  PWACAFcmd = ^TWACAFCmd;
  TWACAFCmd = OBJECT(TWACommand)
    PROCEDURE MakeSendMessage; VIRTUAL;
  END;

  PWACARCmd = ^TWACARCmd;
  TWACARCmd = OBJECT(TWACommand)
    PROCEDURE MakeSendMessage; VIRTUAL;
  END;

  PWACLOSEcmd = ^TWACLOSECmd;
  TWACLOSECmd = OBJECT(TWACommand)
    PROCEDURE MakeSendMessage; VIRTUAL;
  END;

  PWACLTcmd = ^TWACLTCmd;
  TWACLTCmd = OBJECT(TWACommand)
    PROCEDURE MakeSendMessage; VIRTUAL;
  END;

  PWACREATEcmd = ^TWACREATECmd;
  TWACREATECmd = OBJECT(TWACommand)
    PROCEDURE MakeSendMessage; VIRTUAL;
    Procedure SetParams (Name: String); VIRTUAL;
  END;

  PWACSTcmd = ^TWACSTCmd;
  TWACSTCmd = OBJECT(TWACommand)
    PROCEDURE MakeSendMessage; VIRTUAL;
  END;

  PWADACcmd = ^TWADACCmd;
  TWADACCmd = OBJECT(TWACommand)
    PROCEDURE MakeSendMessage; VIRTUAL;
    PROCEDURE GetParams; VIRTUAL;
  END;

  PWADFMTcmd = ^TWADFMTCmd;
  TWADFMTCmd = OBJECT(TWACommand)
    PROCEDURE MakeSendMessage; VIRTUAL;
  END;

  PWADVIcmd = ^TWADVICmd;
  TWADVICmd = OBJECT(TWACommand)
    PROCEDURE MakeSendMessage; VIRTUAL;
    PROCEDURE GetParams(VAR panelId : INTEGER;
                        VAR slot,stat,panelType : BYTE;
                        VAR flags : INTEGER); VIRTUAL;
  END;

  PWAECHOcmd = ^TWAECHOCmd;
  TWAECHOCmd = OBJECT(TWACommand)
    PROCEDURE MakeSendMessage; VIRTUAL;
    PROCEDURE GetParams; VIRTUAL;
    PROCEDURE SetParams; VIRTUAL;
  END;

  PWAEDLcmd = ^TWAEDLCmd;
  TWAEDLCmd = OBJECT(TWACommand)
    PROCEDURE MakeSendMessage; VIRTUAL;
    Procedure SetParams (Entries: Integer; DArray: PDefectArray); VIRTUAL;
  END;

  PWAERAcmd = ^TWAERACmd;
  TWAERACmd = OBJECT(TWACommand)
    PROCEDURE MakeSendMessage; VIRTUAL;
    Procedure SetParams (FileName: String); VIRTUAL;
  END;

  PWAEXcmd = ^TWAEXCmd;
  TWAEXCmd = OBJECT(TWACommand)
    PROCEDURE MakeSendMessage; VIRTUAL;
  END;

  PWAFASTcmd = ^TWAFASTCmd;
  TWAFASTCmd = OBJECT(TWACommand)
    PROCEDURE MakeSendMessage; VIRTUAL;
  END;

  PWAGETWATYPEcmd = ^TWAGETWATYPECmd;
  TWAGETWATYPECmd = OBJECT(TWACommand)
    PROCEDURE MakeSendMessage; VIRTUAL;
    Procedure GetParams (Var WaType: Integer); VIRTUAL;
  END;

  PWAGSIZEcmd = ^TWAGSIZECmd;
  TWAGSIZECmd = OBJECT(TWACommand)
    PROCEDURE MakeSendMessage; VIRTUAL;
    Procedure GetParams (Var rxSize, txSize: Integer);  VIRTUAL;{ Sizes of WA send and receive buffers }
  END;

  PWAHOMcmd = ^TWAHOMCmd;
  TWAHOMCmd = OBJECT(TWACommand)
    PROCEDURE MakeSendMessage; VIRTUAL;
  END;

  PWAHSIcmd = ^TWAHSICmd;
  TWAHSICmd = OBJECT(TWACommand)
    PROCEDURE MakeSendMessage; VIRTUAL;
  END;

  PWAIDICmd = ^TWAIDICmd;
  TWAIDICmd = OBJECT(TWACommand)
    PROCEDURE MakeSendMessage; VIRTUAL;
    PROCEDURE GetParams (Var Len: Integer; Var IdiData: EchoArray); VIRTUAL;
    PROCEDURE SetParams;
  END;

  PWAIDPHOTOcmd = ^TWAIDPHOTOCmd;
  TWAIDPHOTOCmd = OBJECT(TWACommand)
    PROCEDURE MakeSendMessage; VIRTUAL;
  END;

  PWAINTcmd = ^TWAINTCmd;
  TWAINTCmd = OBJECT(TWACommand)
    PROCEDURE MakeSendMessage; VIRTUAL;
    PROCEDURE GetParams; VIRTUAL;
  END;

  PWAJRADSYScmd = ^TWAJRADSYSCmd;
  TWAJRADSYSCmd = OBJECT(TWACommand)
    PROCEDURE MakeSendMessage; VIRTUAL;
  END;

  PWAJRAIRcmd = ^TWAJRAIRCmd;
  TWAJRAIRCmd = OBJECT(TWACommand)
    PROCEDURE MakeSendMessage; VIRTUAL;
    PROCEDURE GetParams; VIRTUAL;
  END;

  PWAJRENVSTATcmd = ^TWAJRENVSTATCmd;
  TWAJRENVSTATCmd = OBJECT(TWACommand)
    PROCEDURE MakeSendMessage; VIRTUAL;
  END;

  PWAJRFLUORcmd = ^TWAJRFLUORCmd;
  TWAJRFLUORCmd = OBJECT(TWACommand)
    PROCEDURE MakeSendMessage; VIRTUAL;
    PROCEDURE GetParams; VIRTUAL;
  END;

  PWAJRGAINScmd = ^TWAJRGAINSCmd;
  TWAJRGAINSCmd = OBJECT(TWACommand)
    PROCEDURE MakeSendMessage; VIRTUAL;
    PROCEDURE GetParams; VIRTUAL;
  END;

  PWAKILLcmd = ^TWAKILLCmd;
  TWAKILLCmd = OBJECT(TWACommand)
    PROCEDURE MakeSendMessage; VIRTUAL;
    PROCEDURE SetParams(pID : INTEGER);
  END;

  PWALARCmd = ^TWALARCmd;
  TWALARCmd = OBJECT(TWACommand)
    PROCEDURE MakeSendMessage; VIRTUAL;
    PROCEDURE GetParams; VIRTUAL;
    PROCEDURE SetParams(mins : BYTE); VIRTUAL;
  END;

  PWAMARcmd = ^TWAMARCmd;
  TWAMARCmd = OBJECT(TWACommand)
    PROCEDURE MakeSendMessage; VIRTUAL;
    PROCEDURE GetParams; VIRTUAL;
    PROCEDURE SetParams(mins : BYTE); VIRTUAL;
  END;

  PWAMPPURGEcmd = ^TWAMPPURGECmd;
  TWAMPPURGECmd = OBJECT(TWACommand)
    PROCEDURE MakeSendMessage; VIRTUAL;
  END;

  PWAMPREFcmd = ^TWAMPREFCmd;
  TWAMPREFCmd = OBJECT(TWACommand)
    PROCEDURE MakeSendMessage; VIRTUAL;
  END;

  PWAMPSHIELDcmd = ^TWAMPSHIELDCmd;
  TWAMPSHIELDCmd = OBJECT(TWACommand)
    PROCEDURE MakeSendMessage; VIRTUAL;
  END;

  PWAOPENcmd = ^TWAOPENCmd;
  TWAOPENCmd = OBJECT(TWACommand)
    PROCEDURE MakeSendMessage; VIRTUAL;
    Procedure SetParams (FileName: String); VIRTUAL;
  END;

  PWAPDIcmd = ^TWAPDICmd;
  TWAPDICmd = OBJECT(TWACommand)
    PROCEDURE MakeSendMessage; VIRTUAL;
    PROCEDURE GetParams; VIRTUAL;
    PROCEDURE SetParams(panelID : INTEGER; row : BYTE); VIRTUAL;
  END;

  PWAPEIcmd = ^TWAPEICmd;
  TWAPEICmd = OBJECT(TWACommand)
    PROCEDURE MakeSendMessage; VIRTUAL;
    PROCEDURE GetParams(VAR panelId : INTEGER;
                        VAR slot, class : BYTE;
                        VAR bits : PriArray); VIRTUAL;
    PROCEDURE SetParams(pId : INTEGER); VIRTUAL;
  END;

  PWAPFIcmd = ^TWAPFICmd;
  TWAPFICmd = OBJECT(TWACommand)
    PROCEDURE MakeSendMessage; VIRTUAL;
    PROCEDURE GetParams; VIRTUAL;
  END;

  PWAPMIcmd = ^TWAPMICmd;
  TWAPMICmd = OBJECT(TWACommand)
    PROCEDURE MakeSendMessage; VIRTUAL;
    PROCEDURE GetParams(VAR panelId : INTEGER;
                        VAR slot, count : BYTE;
                        VAR MICs : PMIArray); VIRTUAL;
    PROCEDURE SetParams(pId : INTEGER); VIRTUAL;
  END;

  PWAPRIcmd = ^TWAPRICmd;
  TWAPRICmd = OBJECT(TWACommand)
    PROCEDURE MakeSendMessage; VIRTUAL;
    PROCEDURE GetParams(VAR panelId : INTEGER;
                        VAR slot, class : BYTE;
                        VAR bits : PriArray); VIRTUAL;
    PROCEDURE SetParams(pId : INTEGER); VIRTUAL;
  END;

  PWAPSIcmd = ^TWAPSICmd;
  TWAPSICmd = OBJECT(TWACommand)
    PROCEDURE MakeSendMessage; VIRTUAL;
    PROCEDURE GetParams(VAR panelId : INTEGER;
                        VAR slot,stat,panelType : BYTE;
                        VAR flags : INTEGER;
                        VAR lastRead : TimeDate); VIRTUAL;
    PROCEDURE SetParams(pId : INTEGER); VIRTUAL;
  END;

  PWAPSTcmd = ^TWAPSTCmd;
  TWAPSTCmd = OBJECT(TWACommand)
    PROCEDURE MakeSendMessage; VIRTUAL;
  END;

  PWAPURcmd = ^TWAPURCmd;
  TWAPURCmd = OBJECT(TWACommand)
    PROCEDURE MakeSendMessage; VIRTUAL;
    PROCEDURE SetParams(reagent : WORD); VIRTUAL;
  END;

  PWAQSTcmd = ^TWAQSTCmd;
  TWAQSTCmd = OBJECT(TWACommand)
    PROCEDURE MakeSendMessage; VIRTUAL;
    PROCEDURE GetParams; VIRTUAL;
  END;

  PWARDIcmd = ^TWARDICmd;
  TWARDICmd = OBJECT(TWACommand)
    PROCEDURE MakeSendMessage; VIRTUAL;
    PROCEDURE GetParams; VIRTUAL;
    PROCEDURE SetParams(panId : INTEGER; row, filter : BYTE); VIRTUAL;
  END;

  PWAREADREGcmd = ^TWAREADREGCmd;
  TWAREADREGCmd = OBJECT(TWACommand)
    PROCEDURE MakeSendMessage; VIRTUAL;
    PROCEDURE GetParams; VIRTUAL;
    PROCEDURE SetParams; VIRTUAL;
  END;

  PWARESETcmd = ^TWARESETCmd;
  TWARESETCmd = OBJECT(TWACommand)
    PROCEDURE MakeSendMessage; VIRTUAL;
    Procedure SetParams (RType: Byte); VIRTUAL;
  END;

  PWAROIcmd = ^TWAROICmd;
  TWAROICmd = OBJECT(TWACommand)
    PROCEDURE MakeSendMessage; VIRTUAL;
    PROCEDURE SetParams(pId : INTEGER); VIRTUAL;
  END;

  PWAROMCMDcmd = ^TWAROMCMDCmd;
  TWAROMCMDCmd = OBJECT(TWACommand)
    PROCEDURE MakeSendMessage; VIRTUAL;
    PROCEDURE SetParams (CmdNum, AXValue: Integer); VIRTUAL;
  END;

  PWARSTcmd = ^TWARSTCmd;
  TWARSTCmd = OBJECT(TWACommand)
    PROCEDURE MakeSendMessage; VIRTUAL;
  END;

  PWARSTPURcmd = ^TWARSTPURCmd;
  TWARSTPURCmd = OBJECT(TWACommand)
    PROCEDURE MakeSendMessage; VIRTUAL;
  END;

  PWASAFEcmd = ^TWASAFECmd;
  TWASAFECmd = OBJECT(TWACommand)
    PROCEDURE MakeSendMessage; VIRTUAL;
  END;

  PWASAVEcmd = ^TWASAVECmd;
  TWASAVECmd = OBJECT(TWACommand)
    PROCEDURE MakeSendMessage; VIRTUAL;
  END;

  PWAV2SAVEcmd = ^TWAV2SAVECmd;
  TWAV2SAVECmd = OBJECT(TWACommand)
    PROCEDURE MakeSendMessage; VIRTUAL;
    PROCEDURE SetParams(OpSysHeader : Sector); VIRTUAL;
  END;

  PWASCIcmd = ^TWASCICmd;
  TWASCICmd = OBJECT(TWACommand)
    PROCEDURE MakeSendMessage; VIRTUAL;
    PROCEDURE GetParams(VAR panelId : INTEGER;
                        VAR slot,stat,paneltype : BYTE;
                        VAR flags : INTEGER;
                        VAR NextRead : TimeDate); VIRTUAL;
    PROCEDURE SetParams(slot : BYTE); VIRTUAL;
  END;

  PWASETREGcmd = ^TWASETREGCmd;
  TWASETREGCmd = OBJECT(TWACommand)
    PROCEDURE MakeSendMessage; VIRTUAL;
    PROCEDURE SetParams(regNum : BYTE; regVal : LONGINT); VIRTUAL;
  END;

  PWASSCcmd = ^TWASSCCmd;
  TWASSCCmd = OBJECT(TWACommand)
    PROCEDURE MakeSendMessage; VIRTUAL;
    PROCEDURE SetParams(slot, panelType, family : BYTE; panelID : INTEGER); VIRTUAL;
  END;

  PWASSIcmd = ^TWASSICmd;
  TWASSICmd = OBJECT(TWACommand)
    PROCEDURE MakeSendMessage; VIRTUAL;
    PROCEDURE GetParams; VIRTUAL;
  END;

  PWASSTcmd = ^TWASSTCmd;
  TWASSTCmd = OBJECT(TWACommand)
    PROCEDURE MakeSendMessage; VIRTUAL;
    PROCEDURE SetParams(WATime : TimeDate); VIRTUAL;
  END;

  PWASTIcmd = ^TWASTICmd;
  TWASTICmd = OBJECT(TWACommand)
    PROCEDURE MakeSendMessage; VIRTUAL;
    PROCEDURE GetParams(VAR stiStat : STIArray); VIRTUAL;
  END;

  PWASTRcmd = ^TWASTRCmd;
  TWASTRCmd = OBJECT(TWACommand)
    PROCEDURE MakeSendMessage; VIRTUAL;
  END;

  PWAUNMPPURGEcmd = ^TWAUNMPPURGECmd;
  TWAUNMPPURGECmd = OBJECT(TWACommand)
    PROCEDURE MakeSendMessage; VIRTUAL;
  END;

  PWAUSEDOFScmd = ^TWAUSEDOFSCmd;
  TWAUSEDOFSCmd = OBJECT(TWACommand)
    PROCEDURE MakeSendMessage; VIRTUAL;
    PROCEDURE GetParams; VIRTUAL;
  END;

  PWAUSEDVOLTScmd = ^TWAUSEDVOLTSCmd;
  TWAUSEDVOLTSCmd = OBJECT(TWACommand)
    PROCEDURE MakeSendMessage; VIRTUAL;
  END;

  PWAVFMTcmd = ^TWAVFMTCmd;
  TWAVFMTCmd = OBJECT(TWACommand)
    PROCEDURE MakeSendMessage; VIRTUAL;
    Procedure SetParams (NumPasses: Integer); VIRTUAL;
  END;

  PWAWATYPEcmd = ^TWAWATYPECmd;
  TWAWATYPECmd = OBJECT(TWACommand)
    PROCEDURE MakeSendMessage; VIRTUAL;
  END;

  { Descendants of THugeWACommand }

  PWAREADcmd = ^TWAREADCmd;
  TWAREADCmd = OBJECT(THugeWACommand)
    PROCEDURE MakeSendMessage; VIRTUAL;
    Procedure SetParams (Offset: LongInt; Size: Word); VIRTUAL;
  END;

  PWASTOcmd = ^TWASTOCmd;
  TWASTOCmd = OBJECT(THugeWACommand)
    PROCEDURE MakeSendMessage; VIRTUAL;
    Procedure SetParams (Class: Byte; LoadOffset, LoadSegment, Len: Word;
                         DataPtr: Pointer); VIRTUAL;
  END;

  PWAWRITEcmd = ^TWAWRITECmd;
  TWAWRITECmd = OBJECT(THugeWACommand)
    PROCEDURE MakeSendMessage; VIRTUAL;
    Procedure SetParams (Offset: LongInt; Size: Word; DataPtr: Pointer); VIRTUAL;
  END;

  PWASetPDLcmd = ^TWASetPDLCmd;
  TWASetPDLCmd = OBJECT(TWACommand)
    PROCEDURE MakeSendMessage; VIRTUAL;
    PROCEDURE SetParams (pdlFileName : PChar); VIRTUAL;
  END;

  PWAHugeEchoCmd = ^TWAHugeEchoCmd;
  TWAHugeEchoCmd = OBJECT (THugeWaCommand)
    Procedure MakeSendMessage; Virtual;
    Procedure SetParams (Size: Word; DataPtr: Pointer); Virtual;
    Procedure GetParams (Size: Word; DataPtr: Pointer); Virtual;
  End;

FUNCTION InitWA(WaNum : INTEGER) : INTEGER;
FUNCTION WAComm(waID : INTEGER;
                transmitMsg : PSendMsgRec;
                receiveMsg  : PRecvMsgRec ) : INTEGER;

VAR WAID : INTEGER;
    slotsPerTower : INTEGER;
    SlotTotal : INTEGER;
		WAType : INTEGER;
    CommErrorMsgUp : BOOLEAN;

Implementation

Uses WLJamChk;

FUNCTION InitWA;        EXTERNAL 'WAGPIB' name 'INITWA';
FUNCTION WAComm;        EXTERNAL 'WAGPIB' name 'WACOMM';

{ Methods for TPanelFlags }

CONSTRUCTOR TPanelFlags.Init(flags : INTEGER);
  BEGIN
    TObject.Init;
    flagVal := flags;
    PowerFailure := ((flags AND $0001) <> 0);
    ISFGrowth := ((flags AND $0002) <> 0);
    BadSterileWell := ((flags AND $0004) <> 0);
    SkippedWells := ((flags AND $0008) <> 0);
    BadDCB := ((flags AND $0010) <> 0);
    BadOFB := ((flags AND $0020) <> 0);
    VerifyPanelId := ((flags AND $0040) <> 0);
    PanelInWA := ((flags AND $0080) <> 0);
    PossibleMRSASE := ((flags AND $0100) <> 0);
    SlowGrower := ((flags AND $0200) <> 0);
    VirtualTests := ((flags AND $0400) <> 0);
    DelayedRead := ((flags AND $0800) <> 0);
    InvSterileWell := ((flags AND $1000) <> 0);
    IDAvailOnRapid := ((flags AND $2000) <> 0);
    NoDispense     := ((flags AND $8000) <> 0);
    BadNPC := FALSE;
    BadBNAC := FALSE;
  END;

{ Methods for TBaseWACommand }

CONSTRUCTOR TBaseWACommand.Init (id: Integer);
Begin
   TObject.Init;
   WAID := Id;
   ErrorMode := 0;
End;

FUNCTION TBaseWACommand.WAError(errNum : INTEGER) : BOOLEAN;
BEGIN
  WAError := FALSE;
  IF (errNum = 0) THEN EXIT;
  IF (errNum = $468) OR (errNum = $469) OR (errNum = $46A) THEN EXIT;
  WAError := TRUE;
  WAErrNum := err_WA+errNum;
END;

FUNCTION TBaseWACommand.WAStatus(statNum : INTEGER) : BOOLEAN;
BEGIN
  WAStatus := FALSE;
  IF statNum = 0 THEN EXIT;
  IF (statNum = $468) OR (statNum = $469) OR (statNum = $46A) THEN
    BEGIN
      WAStatus := TRUE;
      WAStatNum := statNum;
    END;
END;

FUNCTION TBaseWACommand.GPIBError(errNum : INTEGER) : BOOLEAN;
BEGIN
  WAGpibErrNum := errNum;
  GPIBError := FALSE;
  IF errNum = 0 THEN EXIT;
  GPIBError := TRUE;
  If ErrorMode = 0 Then
      SetError(err_GPIB+errNum);
END;

PROCEDURE TBaseWACommand.SetErrorMode (NewErrorMode: Integer);
BEGIN
  ErrorMode := NewErrorMode;
END;

{ Methods for TWACommand }

CONSTRUCTOR TWACommand.Init(Id : INTEGER);
BEGIN
  TBaseWACommand.Init (Id);
  GetMem(sendMsg,SIZEOF(TSendMsgRec));
  GetMem(recvMsg,SIZEOF(TRecvMsgRec));
  MakeSendMessage;
END;

DESTRUCTOR TWACommand.Done;
BEGIN
  FreeMem(sendMsg,SIZEOF(TSendMsgRec));
  FreeMem(recvMsg,SIZEOF(TREcvMsgRec));
  TObject.Done;
END;

FUNCTION TWACommand.WAReadyForMessage : INTEGER;
VAR currentWAStat : LONGINT;
    focusWndw : HWnd;
    tempStr : ARRAY[0..64] OF Char;
BEGIN
  WAReadyForMessage := 0;
  If SendMsg^.Cmd = STICmd Then EXIT;
  If SendMsg^.Cmd = GetWATypeCmd Then EXIT;
  WAReadyForMessage := 1;
  currentWAStat := WLJamChk.GetWAStatus;

  IF ((currentWAStat AND WASystemNotReady) = WASystemNotReady) OR
     (((currentWAStat AND WABCSInProgress) = WABCSInProgress) AND
     ((currentWAStat AND WAJam) <> WAJam)) THEN EXIT;
  IF ((currentWAStat AND WACantTalk) = WACantTalk) THEN { bc 1/21/92 }
		BEGIN
			IF NOT commErrorMsgUp THEN
				BEGIN
          commErrorMsgUp := TRUE;
          focusWndw := GetFocus;
          GetStr(6124,tempStr,64); {Select 'Yes' to re-establish W/A communications.}
					IF MessageBox(Application^.MainWindow^.HWindow,tempStr,'',mb_okCancel) = idOK THEN
						BEGIN
							If TheJamChecker <> NIL Then
                BEGIN
  	              TheJamChecker^.WaStatusWindow^.ClearCommError;
                  GetWAStatus;
                END;
              SetFocus(focusWndw);
						END
          ELSE WAReadyForMessage := 2;
					commErrorMsgUp := FALSE;
				END
      ELSE
				WAReadyForMessage := 2;
			EXIT;
		END;
  IF ((currentWAStat AND WASterilizationInProgress) = WASterilizationInProgress) THEN
    IF sendMsg^.cmd <> CSTCmd THEN EXIT;
	WAReadyForMessage := 0;
END;

FUNCTION TWACommand.SendWAMessage : BOOLEAN;
VAR ret : INTEGER;
BEGIN
  SendWAMessage := FALSE;
  WAStatNum := 0;
  WAErrNum := 0;
  REPEAT
    ret := WAReadyForMessage;
    MSDelay(50);
  UNTIL (ret = 0) OR (ret = 2);
  IF ret = 2 THEN EXIT;
  IF GPIBERROR(WLWAGPIB.WaComm(WAID,sendMsg,recvMsg)) THEN
    EXIT;
  IF integer(sendMsg^.cmd or $8000) <> recvMsg^.cmd THEN
    BEGIN
      IF errorMode = 0 THEN
        SetError(err_WA + recvMsg^.cmd);
      EXIT;
    END;
  IF WAError(recvMsg^.status) THEN
    BEGIN
      IF errorMode = 0 THEN
        SetError(WAErrNum);
      EXIT;
    END;
  WAStatus(recvMsg^.status);
  SendWAMessage := TRUE;
END;

PROCEDURE TWACommand.MakeSendMessage;
BEGIN
  FillChar(sendMsg^,SIZEOF(TSendMsgRec),CHR(0));
END;

{ Methods for THugeWACommand }

CONSTRUCTOR THugeWACommand.Init(Id : INTEGER);
BEGIN
  TBaseWACommand.Init (Id);
  GetMem(sendMsg,SIZEOF(THugeSendMsgRec));
  GetMem(recvMsg,SIZEOF(THugeRecvMsgRec));
  MakeSendMessage;
END;

DESTRUCTOR THugeWACommand.Done;
BEGIN
  FreeMem(sendMsg,SIZEOF(THugeSendMsgRec));
  FreeMem(recvMsg,SIZEOF(THugeRecvMsgRec));
  TObject.Done;
END;

FUNCTION THugeWACommand.SendWAMessage : BOOLEAN;
BEGIN
  SendWAMessage := FALSE;
  WAStatNum := 0;
  WAErrNum := 0;
  IF GPIBERROR(WLWAGPIB.WaComm(WAID,PSendMsgRec (sendMsg),
                                  PRecvMsgRec (recvMsg))) THEN EXIT;
  IF WAError(recvMsg^.status) THEN
    BEGIN
      SetError(recvMsg^.status);
      EXIT;
    END;
  WAStatus(recvMsg^.status);
  SendWAMessage := TRUE;
END;

PROCEDURE THugeWACommand.MakeSendMessage;
BEGIN
  FillChar(sendMsg^,SIZEOF(THugeSendMsgRec),CHR(0));
END;


{**********************************************************************}
{**            Methods for the descendents of TWACommand             **}
{**********************************************************************}

PROCEDURE TWAABFCmd.MakeSendMessage;
BEGIN
  TWACommand.MakeSendMessage;
  sendMsg^.cmd    := ABFCmd;
END;

PROCEDURE TWAAIRCmd.MakeSendMessage;
BEGIN
  TWACommand.MakeSendMessage;
  sendMsg^.cmd    := AIRCmd;
  sendMsg^.msgLen := CDILen;
END;

PROCEDURE TWAAIRCmd.SetParams(row : BYTE);
BEGIN
  sendMsg^.CDIRow := row;
END;

PROCEDURE TWABOOTCmd.MakeSendMessage;
BEGIN
  TWACommand.MakeSendMessage;
  sendMsg^.cmd    := BOOTCmd;
END;

PROCEDURE TWABSICmd.MakeSendMessage;
BEGIN
  TWACommand.MakeSendMessage;
  sendMsg^.cmd    := BSICmd;
END;

PROCEDURE TWABSICmd.GetParams;
BEGIN
END;

PROCEDURE TWACAFCmd.MakeSendMessage;
BEGIN
  TWACommand.MakeSendMessage;
  sendMsg^.cmd    := CAFCmd;
END;

PROCEDURE TWACARCmd.MakeSendMessage;
BEGIN
  TWACommand.MakeSendMessage;
  sendMsg^.cmd    := CARCmd;
END;

PROCEDURE TWACLOSECmd.MakeSendMessage;
BEGIN
  TWACommand.MakeSendMessage;
  sendMsg^.cmd    := CLOSECmd;
END;

PROCEDURE TWACLTCmd.MakeSendMessage;
BEGIN
  TWACommand.MakeSendMessage;
  sendMsg^.cmd    := CLTCmd;
END;

PROCEDURE TWACREATECmd.MakeSendMessage;
BEGIN
  TWACommand.MakeSendMessage;
  sendMsg^.cmd    := CREATECmd;
END;

Procedure TWACREATECmd.SetParams (Name: String);
Begin
  SendMsg^.MsgLen := Length (Name);
  Move (Name[1], SendMsg^.FileName, Length (Name));
End;

PROCEDURE TWACSTCmd.MakeSendMessage;
BEGIN
  TWACommand.MakeSendMessage;
  sendMsg^.cmd    := CSTCmd;
END;

PROCEDURE TWADACCmd.MakeSendMessage;
BEGIN
  TWACommand.MakeSendMessage;
  sendMsg^.cmd    := DACCmd;
END;

PROCEDURE TWADACCmd.GetParams;
BEGIN
END;

PROCEDURE TWADFMTCmd.MakeSendMessage;
BEGIN
  TWACommand.MakeSendMessage;
  sendMsg^.cmd    := DFMTCmd;
END;

PROCEDURE TWADVICmd.MakeSendMessage;
BEGIN
  TWACommand.MakeSendMessage;
  sendMsg^.cmd    := DVICmd;
  FillChar(recvMsg^,SIZEOF(TRecvMsgRec),CHR(0));
END;

PROCEDURE TWADVICmd.GetParams(VAR panelId : INTEGER;
                              VAR slot,stat,paneltype : BYTE;
                              VAR flags : INTEGER);
BEGIN
  panelId := recvMsg^.psiPId;
  slot := recvMsg^.psiSlot;
  stat := recvMsg^.psiStat;
  panelType := recvMsg^.psiPTyp;
  flags := recvMsg^.psiFlags;
END;

PROCEDURE TWAECHOCmd.MakeSendMessage;
VAR i : INTEGER;
BEGIN
  TWACommand.MakeSendMessage;
  sendMsg^.cmd    := ECHOCmd;
  sendMsg^.msgLen := ECHOLen-2; { according to 18.20 WASUP code }
  FOR i := 1 TO ECHOLen-2 DO sendMsg^.echoMsg[i] := i;
END;

PROCEDURE TWAECHOCmd.GetParams;
BEGIN
END;

PROCEDURE TWAECHOCmd.SetParams;
BEGIN
END;

PROCEDURE TWAEDLCmd.MakeSendMessage;
BEGIN
  TWACommand.MakeSendMessage;
  sendMsg^.cmd    := EDLCmd;
END;

Procedure TWAEDLCmd.SetParams (Entries: Integer; DArray: PDefectArray);
Begin
  SendMsg^.MsgLen := Entries * SizeOf (WaDefectType);
  Move (DArray^, SendMsg^.DefectList, SendMsg^.MsgLen);
End;

PROCEDURE TWAERACmd.MakeSendMessage;
BEGIN
  TWACommand.MakeSendMessage;
  sendMsg^.cmd    := ERACmd;
END;

Procedure TWAERACmd.SetParams (FileName: String);
Begin
  SendMsg^.MsgLen := Length (FileName);
	Move (FileName[1], SendMsg^.FileName, Length (FileName));
End;

PROCEDURE TWAEXCmd.MakeSendMessage;
BEGIN
  TWACommand.MakeSendMessage;
  sendMsg^.cmd    := EXCmd;
END;

PROCEDURE TWAFASTCmd.MakeSendMessage;
BEGIN
  TWACommand.MakeSendMessage;
  sendMsg^.cmd    := FASTCmd;
END;

PROCEDURE TWAGETWATYPECmd.MakeSendMessage;
BEGIN
  TWACommand.MakeSendMessage;
  sendMsg^.cmd    := GETWATYPECmd;
END;

Procedure TWAGETWATYPECmd.GetParams (Var WaType: Integer);
Begin
  WaType := -1;         { Unknown }
  If RecvMsg^.Status <> 0 Then Exit;
  If RecvMsg^.EchoMsg[3] = 0 Then { Classic }
    WaType := 0
  Else
    WaType := 1;
End;

PROCEDURE TWAGSIZECmd.MakeSendMessage;
BEGIN
  TWACommand.MakeSendMessage;
  sendMsg^.cmd    := GSIZECmd;
END;

Procedure TWAGSIZECmd.GetParams (Var rxSize, txSize: Integer); { Sizes of WA send and receive buffers }
Begin
   Move (RecvMsg^.EchoMsg[1], rxSize, SizeOf (rxSize));
   Move (RecvMsg^.EchoMsg[3], txSize, SizeOf (txSize));
End;

PROCEDURE TWAHOMCmd.MakeSendMessage;
BEGIN
  TWACommand.MakeSendMessage;
  sendMsg^.cmd    := HOMCmd;
END;

PROCEDURE TWAHSICmd.MakeSendMessage;
BEGIN
  TWACommand.MakeSendMessage;
  sendMsg^.cmd    := HSICmd;
END;

PROCEDURE TWAIDICmd.MakeSendMessage;
BEGIN
  TWACommand.MakeSendMessage;
  sendMsg^.cmd    := IDICmd;
	sendMsg^.msgLen := 0;
END;

PROCEDURE TWAIDICmd.GetParams (Var Len: Integer; Var IdiData: EchoArray);
BEGIN
    Len := RecvMsg^.MsgLen;
    IdiData := RecvMsg^.EchoMsg;
END;

PROCEDURE TWAIDICmd.SetParams;
BEGIN
END;

PROCEDURE TWAIDPHOTOCmd.MakeSendMessage;
BEGIN
  TWACommand.MakeSendMessage;
  sendMsg^.cmd    := IDPHOTOCmd;
END;

PROCEDURE TWAINTCmd.MakeSendMessage;
BEGIN
  TWACommand.MakeSendMessage;
  sendMsg^.cmd    := INTCmd;
END;

PROCEDURE TWAINTCmd.GetParams;
BEGIN
END;

PROCEDURE TWAJRADSYSCmd.MakeSendMessage;
BEGIN
  TWACommand.MakeSendMessage;
  sendMsg^.cmd    := JRADSYSCmd;
END;

PROCEDURE TWAJRAIRCmd.MakeSendMessage;
BEGIN
  TWACommand.MakeSendMessage;
  sendMsg^.cmd    := JRAIRCmd;
END;

PROCEDURE TWAJRAIRCmd.GetParams;
BEGIN
END;

PROCEDURE TWAJRENVSTATCmd.MakeSendMessage;
BEGIN
  TWACommand.MakeSendMessage;
  sendMsg^.cmd    := JRENVSTATCmd;
END;

PROCEDURE TWAJRFLUORCmd.MakeSendMessage;
BEGIN
  TWACommand.MakeSendMessage;
  sendMsg^.cmd    := JRFLUORCmd;
END;

PROCEDURE TWAJRFLUORCmd.GetParams;
BEGIN
END;

PROCEDURE TWAJRGAINSCmd.MakeSendMessage;
BEGIN
  TWACommand.MakeSendMessage;
  sendMsg^.cmd    := JRGAINSCmd;
END;

PROCEDURE TWAJRGAINSCmd.GetParams;
BEGIN
END;

PROCEDURE TWAKILLCmd.MakeSendMessage;
BEGIN
  TWACommand.MakeSendMessage;
  sendMsg^.cmd    := KILLCmd;
  sendMsg^.msgLen := PSILen;
END;

PROCEDURE TWAKILLCmd.SetParams(pId : INTEGER);
BEGIN
  sendMsg^.psiPanId := PId;
END;

PROCEDURE TWALARCmd.MakeSendMessage;
BEGIN
  TWACommand.MakeSendMessage;
  sendMsg^.cmd    := LARCmd;
  sendMsg^.msgLen := LARLen;
END;

PROCEDURE TWALARCmd.GetParams;
BEGIN
END;

PROCEDURE TWALARCmd.SetParams(mins : BYTE);
BEGIN
  sendMsg^.arMins := mins;
END;

PROCEDURE TWAMARCmd.MakeSendMessage;
BEGIN
  TWACommand.MakeSendMessage;
  sendMsg^.cmd    := MARCmd;
  sendMsg^.msgLen := LARLen;
END;

PROCEDURE TWAMARCmd.GetParams;
BEGIN
END;

PROCEDURE TWAMARCmd.SetParams(mins : BYTE);
BEGIN
  sendMsg^.arMins := mins;
END;

PROCEDURE TWAMPPURGECmd.MakeSendMessage;
BEGIN
  TWACommand.MakeSendMessage;
  sendMsg^.cmd    := MPPURGECmd;
END;

PROCEDURE TWAMPREFCmd.MakeSendMessage;
BEGIN
  TWACommand.MakeSendMessage;
  sendMsg^.cmd    := MPREFCmd;
END;

PROCEDURE TWAMPSHIELDCmd.MakeSendMessage;
BEGIN
  TWACommand.MakeSendMessage;
  sendMsg^.cmd    := MPSHIELDCmd;
END;

PROCEDURE TWAOPENCmd.MakeSendMessage;
BEGIN
  TWACommand.MakeSendMessage;
  sendMsg^.cmd    := OPENCmd;
END;

Procedure TWAOPENCmd.SetParams (FileName: String);
Begin
  SendMsg^.MsgLen := Length (FileName);
	Move (FileName[1], SendMsg^.FileName, Length (FileName));
End;

PROCEDURE TWAPDICmd.MakeSendMessage;
BEGIN
  TWACommand.MakeSendMessage;
  sendMsg^.cmd    := PDICmd;
  sendMsg^.msgLen := PDILen;
END;

PROCEDURE TWAPDICmd.GetParams;
BEGIN
END;

PROCEDURE TWAPDICmd.SetParams(panelID : INTEGER; row : BYTE);
BEGIN
  sendMsg^.PDIPanID := panelID;
  sendMsg^.PDIRow := row;
END;

PROCEDURE TWAPEICmd.MakeSendMessage;
BEGIN
  TWACommand.MakeSendMessage;
  sendMsg^.cmd    := PEICmd;
  sendMsg^.msgLen := PSILen;
  FillChar(recvMsg^,SIZEOF(TRecvMsgRec),CHR(0));
END;

PROCEDURE TWAPEICmd.GetParams(VAR panelId : INTEGER;
                              VAR slot, class : BYTE;
                              VAR bits : PriArray);
BEGIN
  panelId := recvMsg^.priPid;
  slot := recvMsg^.priSlot;
  class := recvMsg^.priClass;
  MOVE(recvMsg^.priBits,bits,SIZEOF(PRIArray));
END;

PROCEDURE TWAPEICmd.SetParams(pId : INTEGER);
BEGIN
  sendMsg^.psiPanId := pId;
END;

PROCEDURE TWAPFICmd.MakeSendMessage;
BEGIN
  TWACommand.MakeSendMessage;
  sendMsg^.cmd    := PFICmd;
END;

PROCEDURE TWAPFICmd.GetParams;
BEGIN
END;

PROCEDURE TWAPMICmd.MakeSendMessage;
BEGIN
  TWACommand.MakeSendMessage;
  sendMsg^.cmd    := PMICmd;
  sendMsg^.msgLen := PSILen;
  FillChar(recvMsg^,SIZEOF(TRecvMsgRec),CHR(0));
END;

PROCEDURE TWAPMICmd.GetParams(VAR panelId : INTEGER;
                              VAR slot, count : BYTE;
                              VAR MICs : PMIArray);
BEGIN
  panelId := recvMsg^.pmiPId;
  slot := recvMsg^.pmiSlot;
  count := recvMsg^.pmiCount;
  MOVE(recvMsg^.pmiMICs,MICs,SIZEOF(PMIArray));
END;

PROCEDURE TWAPMICmd.SetParams(pId : INTEGER);
BEGIN
  sendMsg^.psiPanId := pId;
END;

PROCEDURE TWAPRICmd.MakeSendMessage;
BEGIN
  TWACommand.MakeSendMessage;
  sendMsg^.cmd    := PRICmd;
  sendMsg^.msgLen := PSILen;
  FillChar(recvMsg^,SIZEOF(TRecvMsgRec),CHR(0));
END;

PROCEDURE TWAPRICmd.GetParams(VAR panelId : INTEGER;
                              VAR slot, class : BYTE;
                              VAR bits : PriArray);
BEGIN
  panelId := recvMsg^.priPId;
  slot := recvMsg^.priSlot;
  class := recvMsg^.priClass;
  MOVE(recvMsg^.priBits,bits,SIZEOF(PRIArray));
END;

PROCEDURE TWAPRICmd.SetParams(pId : INTEGER);
BEGIN
  sendMsg^.psiPanId := pId;
END;

PROCEDURE TWAPSICmd.MakeSendMessage;
BEGIN
  TWACommand.MakeSendMessage;
  sendMsg^.cmd    := PSICmd;
  sendMsg^.msgLen := PSILen;
  FillChar(recvMsg^,SIZEOF(TRecvMsgRec),CHR(0));
END;

PROCEDURE TWAPSICmd.GetParams(VAR panelId : INTEGER;
                              VAR slot,stat,paneltype : BYTE;
                              VAR flags : INTEGER;
                              VAR lastRead : TimeDate);
BEGIN
  panelId := recvMsg^.psiPId;
  slot := recvMsg^.psiSlot;
  stat := recvMsg^.psiStat;
  panelType := recvMsg^.psiPTyp;
  flags := recvMsg^.psiFlags;
  MOVE(recvMsg^.psiLastRd,lastRead,SIZEOF(TimeDate));
END;

PROCEDURE TWAPSICmd.SetParams(pId : INTEGER);
BEGIN
  sendMsg^.psiPanId := PId;
END;

PROCEDURE TWAPSTCmd.MakeSendMessage;
BEGIN
  TWACommand.MakeSendMessage;
  sendMsg^.cmd    := PSTCmd;
END;

PROCEDURE TWAPURCmd.MakeSendMessage;
BEGIN
  TWACommand.MakeSendMessage;
  sendMsg^.cmd    := PURCmd;
  sendMsg^.msgLen := PURLen;
END;

PROCEDURE TWAPURCmd.SetParams(reagent : WORD);
BEGIN
  sendMsg^.purReag := reagent;
END;

PROCEDURE TWAQSTCmd.MakeSendMessage;
BEGIN
  TWACommand.MakeSendMessage;
  sendMsg^.cmd    := QSTCmd;
END;

PROCEDURE TWAQSTCmd.GetParams;
BEGIN
END;

PROCEDURE TWARDICmd.MakeSendMessage;
BEGIN
  TWACommand.MakeSendMessage;
  sendMsg^.cmd    := RDICmd;
  sendMsg^.msgLen := RDILen;
END;

PROCEDURE TWARDICmd.GetParams;
BEGIN
END;

PROCEDURE TWARDICmd.SetParams(panId : INTEGER; row, filter : BYTE);
BEGIN
  sendMsg^.rdiPanId := panID;
  sendMsg^.rdiRow := row;
  sendMsg^.rdiFiltr := filter;
END;

PROCEDURE TWAREADREGCmd.MakeSendMessage;
BEGIN
  TWACommand.MakeSendMessage;
  sendMsg^.cmd    := READREGCmd;
  sendMsg^.msgLen := RREGLen;
END;

PROCEDURE TWAREADREGCmd.GetParams;
BEGIN
END;

PROCEDURE TWAREADREGCmd.SetParams;
BEGIN
END;

PROCEDURE TWARESETCmd.MakeSendMessage;
BEGIN
  TWACommand.MakeSendMessage;
  SendMsg^.MsgLen := RESETLen;
  sendMsg^.cmd    := RESETCmd;
END;

Procedure TWARESETCmd.SetParams (RType: Byte);
Begin
  SendMsg^.ResetType := RType;
End;

PROCEDURE TWAROICmd.MakeSendMessage;
BEGIN
  TWACommand.MakeSendMessage;
  SendMsg^.MsgLen := ROILen;
  sendMsg^.cmd    := ROICmd;
END;

PROCEDURE TWAROICmd.SetParams(pId : INTEGER);
BEGIN
  sendMsg^.ROIPanId := PId;
END;

PROCEDURE TWAROMCMDCmd.MakeSendMessage;
BEGIN
  TWACommand.MakeSendMessage;
  sendMsg^.cmd    := ROMCMDCmd;
  sendMsg^.msgLen := ROMCMDLen;
END;

PROCEDURE TWARSTCmd.MakeSendMessage;
BEGIN
  TWACommand.MakeSendMessage;
  sendMsg^.cmd    := RSTCmd;
END;

PROCEDURE TWAROMCMDCmd.SetParams (CmdNum, AXValue: Integer);
BEGIN
  SendMsg^.RmCmdNum := CmdNum;
  SendMsg^.RmCmdAX := AXValue;
END;

PROCEDURE TWARSTPURCmd.MakeSendMessage;
BEGIN
  TWACommand.MakeSendMessage;
  sendMsg^.cmd    := RSTPURCmd;
END;

PROCEDURE TWASAFECmd.MakeSendMessage;
BEGIN
  TWACommand.MakeSendMessage;
  sendMsg^.cmd    := SAFECmd;
END;

PROCEDURE TWASAVECmd.MakeSendMessage;
BEGIN
  TWACommand.MakeSendMessage;
  sendMsg^.cmd    := SAVECmd;
END;

PROCEDURE TWAV2SAVECmd.MakeSendMessage;
BEGIN
  TWACommand.MakeSendMessage;
  sendMsg^.cmd    := SAVECmd;
END;

PROCEDURE TWAV2SAVECmd.SetParams(OpSysHeader : Sector);
Begin
  SendMsg^.MsgLen := SizeOf (OpSysHeader);
  SendMsg^.Header := OpSysHeader;
End;

PROCEDURE TWASCICmd.MakeSendMessage;
BEGIN
  TWACommand.MakeSendMessage;
  sendMsg^.cmd    := SCICmd;
  sendMsg^.msgLen := SCILen;
  FillChar(recvMsg^,SIZEOF(TRecvMsgRec),CHR(0));
END;

PROCEDURE TWASCICmd.GetParams(VAR panelId : INTEGER;
                        VAR slot,stat,panelType : BYTE;
                        VAR flags : INTEGER;
                        VAR nextRead : TimeDate);
BEGIN
  panelId := recvMsg^.psiPId;
  slot := recvMsg^.psiSlot;
  stat := recvMsg^.psiStat;
  panelType := recvMsg^.psiPTyp;
  flags := recvMsg^.psiFlags;
  MOVE(recvMsg^.psiLastRd,nextRead,SIZEOF(TimeDate));
END;

PROCEDURE TWASCICmd.SetParams(slot : BYTE);
BEGIN
  sendMsg^.sciSlot := slot;
END;

PROCEDURE TWASETREGCmd.MakeSendMessage;
BEGIN
  TWACommand.MakeSendMessage;
  sendMsg^.cmd    := SETREGCmd;
  sendMsg^.msgLen := SREGLen;
END;

PROCEDURE TWASETREGCmd.SetParams(regNum : BYTE; regVal : LONGINT);
BEGIN
  sendMsg^.regNum := regNum;
  sendMsg^.regValue := regVal;
END;

PROCEDURE TWASSCCmd.MakeSendMessage;
BEGIN
  TWACommand.MakeSendMessage;
  sendMsg^.cmd    := SSCCmd;
  sendMsg^.msgLen := SSCLen;
END;

PROCEDURE TWASSCCmd.SetParams(slot, panelType, family : BYTE; panelID : INTEGER);
BEGIN
  sendMsg^.sscSlot := slot;
  sendMsg^.sscType := panelType;
  sendMsg^.sscClass := family;
  sendMsg^.sscPanID := panelID;
END;

PROCEDURE TWASSICmd.MakeSendMessage;
BEGIN
  TWACommand.MakeSendMessage;
  sendMsg^.cmd    := SSICmd;
END;

PROCEDURE TWASSICmd.GetParams;
BEGIN
END;

PROCEDURE TWASSTCmd.MakeSendMessage;
BEGIN
  TWACommand.MakeSendMessage;
  sendMsg^.cmd    := SSTCmd;
  sendMsg^.msgLen := SSTLen;
END;

PROCEDURE TWASSTCmd.SetParams(WATime:TimeDate);
BEGIN
  MOVE(WATime,sendMsg^.sstTime,SIZEOF(TimeDate));
END;

PROCEDURE TWASTICmd.MakeSendMessage;
BEGIN
  TWACommand.MakeSendMessage;
  sendMsg^.cmd    := STICmd;
END;

PROCEDURE TWASTICmd.GetParams(VAR stiStat : STIArray);
BEGIN
  MOVE(recvMsg^.stiBits[1],stiStat[1],SIZEOF(STIArray));
END;

PROCEDURE TWASTRCmd.MakeSendMessage;
BEGIN
  TWACommand.MakeSendMessage;
  sendMsg^.cmd    := STRCmd;
END;

PROCEDURE TWAUNMPPURGECmd.MakeSendMessage;
BEGIN
  TWACommand.MakeSendMessage;
  sendMsg^.cmd    := UNMPPURGECmd;
END;

PROCEDURE TWAUSEDOFSCmd.MakeSendMessage;
BEGIN
  TWACommand.MakeSendMessage;
  sendMsg^.cmd    := USEDOFSCmd;
END;

PROCEDURE TWAUSEDOFSCmd.GetParams;
BEGIN
END;

PROCEDURE TWAUSEDVOLTSCmd.MakeSendMessage;
BEGIN
  TWACommand.MakeSendMessage;
  sendMsg^.cmd    := USEDVOLTSCmd;
END;

PROCEDURE TWAVFMTCmd.MakeSendMessage;
BEGIN
  TWACommand.MakeSendMessage;
  sendMsg^.cmd    := VFMTCmd;
END;

Procedure TWAVFMTCmd.SetParams (NumPasses: Integer);
Begin
  SendMsg^.MsgLen := 2;
  SendMsg^.Passes := NumPasses;
End;

PROCEDURE TWAWATYPECmd.MakeSendMessage;
BEGIN
  TWACommand.MakeSendMessage;
  sendMsg^.cmd    := WATYPECmd;
END;

{ Methods for Huge wa command objects }

PROCEDURE TWAREADCmd.MakeSendMessage;
BEGIN
  THugeWACommand.MakeSendMessage;
  sendMsg^.cmd    := READCmd;
END;

Procedure TWAREADCmd.SetParams (Offset: LongInt; Size: Word);
Begin
  SendMsg^.MsgLen := 6;
  SendMsg^.XferBlock.Offset := Offset;
  SendMsg^.XferBlock.Size := Size;
End;

PROCEDURE TWASTOCmd.MakeSendMessage;
BEGIN
  THugeWACommand.MakeSendMessage;
  sendMsg^.cmd    := STOCmd;
END;

Procedure TWASTOCmd.SetParams (Class: Byte; LoadOffset, LoadSegment,
                               Len: Word; DataPtr: Pointer);
Begin
  SendMsg^.MsgLen := Len + SizeOf (Class) + SizeOf (LoadOffset) + SizeOf (LoadSegment);
  SendMsg^.SendBlock.Class := Class;
  SendMsg^.SendBlock.LoadOffset := LoadOffset;
  SendMsg^.SendBlock.LoadSegment := LoadSegment;
  If DataPtr <> NIL Then
      Move (DataPtr^, SendMsg^.SendBlock.SendData, Len);
End;

Procedure TWAHugeEchoCmd.MakeSendMessage;
Begin
  THugeWACommand.MakeSendMessage;
  sendMsg^.cmd := EchoCmd;
End;

Procedure TWaHugeEchoCmd.SetParams (Size: Word; DataPtr: Pointer);
Begin
  SendMsg^.MsgLen := Size;
  If DataPtr <> NIL Then
    Move (DataPtr^, SendMsg^.Data, Size);
End;

Procedure TWaHugeEchoCmd.GetParams (Size: Word; DataPtr: Pointer);
Begin
  If DataPtr <> NIL Then
    Move (RecvMsg^.Data, DataPtr^, Size);
End;

PROCEDURE TWAWRITECmd.MakeSendMessage;
BEGIN
  THugeWACommand.MakeSendMessage;
  sendMsg^.cmd    := WRITECmd;
END;

Procedure TWAWRITECmd.SetParams (Offset: LongInt; Size: Word; DataPtr: Pointer);
Begin
  SendMsg^.MsgLen := Size + SizeOf (Offset) + SizeOf (Size);
  SendMsg^.XferBlock.Offset := Offset;
  SendMsg^.XferBlock.Size := Size;
  If DataPtr <> NIL Then
			Move (DataPtr^, SendMsg^.XferBlock.Data, Size);
End;

PROCEDURE TWASetPDLCmd.MakeSendMessage;
BEGIN
  TWACommand.MakeSendMessage;
  sendMsg^.cmd := SetPDLCmd;
END;

PROCEDURE TWASetPDLCmd.SetParams(PdlFileName : PChar);
BEGIN
  SendMsg^.MsgLen := StrLen(pdlFileName);
	MOVE(pdlFIleName[0],sendMsg^.FileName[0],sendMsg^.msgLen);
END;

{ Unit initialization }

BEGIN
  WAID := WLWAGPIB.InitWA(1);     { returns -1 if no WA attached }
  commErrorMsgUp := FALSE;
END.
