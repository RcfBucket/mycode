{----------------------------------------------------------------------------}
{  Module Name  : FMGLOBAL.PAS                                               }
{  Programmer   : DWC                                                        }
{  Date Created : 05/11/95                                                   }
{                                                                            }
{  Purpose -                                                                 }
{  This module contains procedure functions and procedure used throughout    }
{  file maintenance.                                                         }
{                                                                            }
{  Assumptions -                                                             }
{  None.                                                                     }
{                                                                            }
{  Initialization -                                                          }
{  None.                                                                     }
{                                                                            }
{  Revision History -                                                        }
{                                                                            }
{  Version  Date      Who    Did What                                        }
{  -------  --------  ------ ----------------------------------------------- }
{  1.00     05/11/95  DWC     Initial release.                               }
{                                                                            }
{----------------------------------------------------------------------------}
{+$I-,R-,S-}
{$R-,S-}

unit FMGLOBAL;

INTERFACE

uses
  Objects,
  fmconst,
  fmtypes;

function InsertDisk(aParent     : PDlgFMStatus;
                    var diskNum : integer;
                    rec         : TBackupRec;
                    aTitle      : PChar): integer;

function GetFirstFullBackupDisk(dlg         : PDlgFMStatus;
                                var fullRec : TBackupRec): integer;

procedure UpdateDisk1(dlg     : PDlgFMStatus;
                      diskNum : integer;
                      rec     : TBackupRec;
                      msgStr  : PChar);

procedure UpdateBackupSet(dlg        : PDlgFMStatus;
                          incrRec,
                          fullRec    : TBackupRec);

procedure CleanDir(adir, aFileId : PChar);

procedure WipeDisk(aDisk : PChar);

function InsertRestoreDisk(aParent    : PDlgFMStatus;
                           diskNum,
                           totalDisks : integer;
                           BackupType : char;
                           backupId   : integer;
                           msgStr,
                           aTitle     : PChar): integer;

function getSetInfo(aParent    : PDlgFMStatus;
                    var setRec : TBackupRec;
                    msgStr,
                    aTitle     : PChar;
                    catalogType: Char;
                    backupId   : integer): integer;

Function CopyFile(dlg                : PDlgFMStatus;
                  source,
                  destination        : PChar;
                  var dn             : integer;
                  rec                : TBackupRec;
                  floppyCopy,
                  appendFile,
                  displayPercentBar  : boolean;
                  aTitle             : PChar): Integer;

Function MoveFile(dlg               : PDlgFMStatus;
                  source,
                  destination       : PChar;
                  var dn            : integer;
                  rec               : TBackupRec;
                  floppyCopy,
                  appendFile,
                  displayPercentBar : boolean;
                  aTitle            : PChar): Integer;

function  SearchForFile(fNList: PCollection; dbFile: PChar): PFileNameEntry;

function  restoreFiles(dlg: PDlgFMStatus; dbDir, lastFileName: PChar): integer;

function  BuildArchiveFiles(statDlg   : PDlgFMStatus;
                            fNList    : PCollection;
                            aDir      : PChar;
                            anExt     : PChar;
                            buildType : integer): integer;

function ComputeTotalBytes(dlg            : PDlgFMStatus;
                           aDir           : PChar;
                           var estDiskNum : integer): longint;

function  CopyFiles(dlg               : PDlgFMStatus;
                    fNList            : PCollection;
                    aSrcDir,
                    aDestDir,
                    anExt             : PChar;
                    var dn            : integer;
                    rec               : TBackupRec;
                    floppyCopy,
                    appendFile,
                    displayPercentBar : boolean;
                    aTitle            : PChar): integer;

function RestoreData(dlg:PDlgFMStatus; dbDir, workDir: PChar): integer;

procedure GetCurrentDir(adir : PChar; var drive : byte);

procedure GetWorkingDir(dbDir, workDir, backupDir : PChar);

function MakeCatalog(aParent    : PDlgFMStatus;
                     var rec    : TBackupRec;
                     totalBytes : longint;
                     numDisks   : integer;
                     catalogType: char): integer;

IMPLEMENTATION

uses
  OWindows,
  fmerror,
  ApiTools,
  DBTypes,
  DBLib,
  DBFile,
  WinDos,
  WinProcs,
  WinTypes,
  DlgLib,
  strsw,
  inilib,
  DMString,
  Strings;

Type
  PByteArray = ^byteArray;
  byteArray  = Array[0..0] of byte;

  ListPtr = ^ListRec;
	ListRec = Record			{- linked list of buffers }
    buf   : ^byteArray;
		size	: Word;
		next	: ListPtr;
	End;

PDlgRestoreDisk = ^TDlgRestoreDisk;
TDlgRestoreDisk = object(TCenterDlg)
  disk   : array[0..10] of char;
  aMsg   : array[0..50] of char;
  dlgTitle : array[0..255] of char;
  {}
  constructor Init(AParent: PWindowsObject; diskStr, msgStr, aTitle : PChar);
  destructor Done; virtual;
  procedure SetupWindow; virtual;
  procedure Cancel(var Msg: TMessage); virtual ID_FIRST + ID_CANCEL;
end;


PDlgDiskNum = ^TDlgDiskNum;
TDlgDiskNum = object(TCenterDlg)
  disk     : array[0..10] of char;
  dlgTitle : array[0..255] of char;
  {}
  constructor Init(AParent: PWindowsObject; diskStr, aTitle : PChar);
  destructor Done; virtual;
  procedure SetupWindow; virtual;
  procedure Cancel(var Msg: TMessage); virtual ID_FIRST + ID_CANCEL;
end;

PDlgOverWrite = ^TDlgOverWrite;
TDlgOverWrite = object(TCenterDlg)
  {}
  constructor Init(AParent: PWindowsObject);
  destructor Done; virtual;
  procedure SetupWindow; virtual;
end;

Var
	copyInit		: Boolean;

Procedure AddToList(buf: PByteArray; Var cbuf: ListPtr; bytes: Word);
{- add buf to tail of cbuf }
Var
	temp	: ListPtr;
  aux   : ListPtr;
Begin
	GetMem(aux, SizeOf(listrec));			{- allocate memory for record}
	With aux^ Do Begin
		next:= Nil;
		buf:= Nil;
		size:= Bytes;
	End;

	If cbuf = Nil Then Begin				{- no buffer list exists yet}
		cbuf:= aux;
	End
	Else Begin									{- find last buffer in list }
		temp:= cbuf;
		While temp^.next <> Nil Do
			temp:= temp^.next;
		temp^.next:= aux;
	End;

	GetMem(aux^.buf, bytes);
	Move(buf^, aux^.buf^, bytes);
End;

Procedure FreeList(Var list: ListPtr);
{- free memory taken by copybuf}
Var
	temp		: ListPtr;
Begin
	While list <> Nil Do Begin
		temp:= list;
		list:= list^.next;
		FreeMem(temp^.buf, temp^.size);
		FreeMem(temp, SizeOf(listrec));
	End;
	List:= Nil;
End;

{-------------------------------[CopyFile]----------------------------------}
{---------------------------------------------------------------------------}
{ copy [src] to [dest], return 0 if no error                                }
{---------------------------------------------------------------------------}
Function CopyFile(dlg               : PDlgFMStatus;
                  source,
                  destination       : PChar;
                  var dn            : integer;
                  rec               : TBackupRec;
                  floppyCopy,
                  appendFile,
                  displayPercentBar : boolean;
                  aTitle            : PChar): Integer;
Var
  retval     : Integer;
  src        : File;
  dest       : File;
  tlist      : ListPtr;
  copylist   : ListPtr;      {- copy list buffer }
  buf        : PByteArray;      {- io buffer }
  numwritten : Word;
  numread    : Word;
  bufsize    : Word;
  temp       : LongInt;
  offset     : Word;
  size       : longint;
  oldMode     : Word;

  Procedure OpenFiles;
  Begin
    {$I-}
    Assign(src, source);

    Assign(dest, destination);
    Reset(src, 1);            {- open files }
    retval:= IoResult;
    If retval <> 0 Then
      Exit;
    if appendFile then
    begin
      Reset(dest,1);
      size := FileSize(dest);
      seek(dest,size);
    end
    else
      Rewrite(dest, 1);
    retval:= IoResult;
    {$I+}
  End;

	Procedure CloseFiles;
	Begin
    {$I-}
		Close(src);
		If IoResult <> 0 Then ;
		Close(dest);
		If IoResult <> 0 Then ;
    {$I+}
	End;

Begin
  oldMode := SetErrorMode(SEM_FAILCRITICALERRORS);
  OpenFiles;

  If retval = 0 Then Begin
    If MaxAvail > 128000 + $400 Then
      bufsize:= 64000
    Else Begin
      temp:= (LongInt(MaxAvail) Shr 1) - $400;
      If temp > 0 Then
        bufSize:= temp
      Else
        retVal:= 8;   {- not enough memory }
    End;
    {- NOTE: requires at least (bufsize*2) bytes available }
    {-    leave at least 1k ($400 bytes) free }
  End;

  If retval = 0 Then Begin

    GetMem(buf, bufsize);     {- allocate io buffer }
    copylist:= Nil;

    While not Eof(src) and (retval = 0) Do
    Begin
      {$I-}
      BlockRead(src, buf^, bufsize, numread);
			retVal:= IoResult;
      {$I+}
      If retVal = 0 Then
      Begin

        AddToList(buf, copylist, numread);    {- add chunk to list }

        If (MemAvail < (bufsize+$100)) or Eof(src) Then
        Begin
					tlist:= copylist;
					Repeat
						numread:= tlist^.size;
            {$I-}
						BlockWrite(dest, tlist^.buf^, numread, numwritten);
						If IoResult <> 0 Then
							retVal:= IoResult
            {$I+}
						Else
            begin
              if displayPercentBar then
              begin
                diskSizeCt := diskSizeCt + numwritten;
                totalCt := totalCt + numWritten;
                dlg^.CompPctLevel(diskSizeCt,DiskSpace,1);
                dlg^.CompPctLevel(totalCt,totalSpace,2);
              end;
              If numwritten < numread  Then  {- disk is full }
              begin
                if floppyCopy then
                begin
                  dlg^.CompPctLevel(DiskSpace,DiskSpace,1);
                  diskSizeCt := 0;
                  offset := numwritten;
                  dlg^.ResetPct(1);
                  Close(dest);
                  retVal := InsertDisk(dlg,dn,rec,aTitle);
                  if retVal = continueJob then
                  begin
                    {$I-}
                    Assign(dest, destination);
                    Rewrite(dest, 1);
                    retval:= IoResult;
                    {$I+}
                    If retval = 0 Then
                    begin
                      numread := numread-offset;
                      {$I-}
                      BlockWrite(dest,tlist^.buf^[offset],numread,numwritten);
                      retval:= IoResult;
                      {$I+}
                    end;
                    diskSizeCt := diskSizeCt + numwritten;
                    totalCt := totalCt + numwritten;
                    dlg^.CompPctLevel(diskSizeCt,DiskSpace,1);
                    dlg^.CompPctLevel(totalCt,totalSpace,2);
                  end;
                end
                else
                  retVal := fmErr_HardDiskFull;
              end;
            end;
            tlist:= tlist^.next;
          Until (tlist = Nil) or (numwritten <> numread) or (retval <> continueJob);
					FreeList(copylist);
				End;
			End;
		End;

    FreeMem(buf, bufsize);    {- free buffer space }

    If retVal = 0 Then Begin
      {$I-}
      GetFTime(src, temp);      {- equate file dates/times }
      SetFTime(dest, temp);
      {$I+}
    End;
  End;

  CloseFiles;

  If retVal <> 0 Then Begin   {- delete dest file when error occurs }
    {$I-}
    Erase(dest);
    If IoResult <> 0 Then ; {- clear IoResult }
    {$I+}
  End;

  SetErrorMode(oldMode);
  CopyFile:= retval;
End;

{-------------------------------[MoveFile]----------------------------------}
{---------------------------------------------------------------------------}
{ move a file from source to destination                                    }
{ If destination is on another drive then copy the source and delete it     }
{---------------------------------------------------------------------------}
Function MoveFile(dlg :PDlgFMStatus; source, destination: PChar;
                  var dn :integer; rec : TBackupRec;
                  floppyCopy, appendFile, displayPercentBar : boolean;
                  aTitle: PChar): Integer;
Var
	s, d	: File;
	k		: Integer;
Begin
	Assign(s, source);
	Rename(s, destination);
	k:= IoResult;
	If k = 17 Then Begin				{- attempting to rename across drives }
    k:= CopyFile(dlg, source, destination, dn, rec, floppyCopy,
                 appendFile, displayPercentBar, aTitle);
		If k = 0 Then Begin
      {$I-}
      Erase(s);
			k:= IoResult;
      {$I+}
		End;
	End;
	MoveFile:= k;
End;

{----------------------[SearchForFile]--------------------------------------}
{---------------------------------------------------------------------------}
{                                                                           }
{---------------------------------------------------------------------------}
function SearchForFile(fNList : PCollection; dbFile: PChar): PFileNameEntry;

  function Matches(aFileEntry : PFileNameEntry) : boolean; far;
  begin
    Matches := (StrIComp(aFileEntry^.fileName, dbFile) = 0);
  end;

var
  aFileEntry : PFileNameEntry;

begin
  SearchForFile := fNList^.FirstThat(@Matches);
end;

{---------------------------[restoreFiles]----------------------------------}
{---------------------------------------------------------------------------}
{                                                                           }
{---------------------------------------------------------------------------}
function restoreFiles(dlg: PDlgFMStatus; dbDir, lastFileName: PChar): integer;
var
  retVal     : integer;
  filePath   : array[0..maxPathLen] of char;
  srcFile    : array[0..255] of char;
  destFile   : array[0..255] of char;
  dirInfo    : TSearchRec;
  dn         : integer;
  rec        : TBackupRec;
  appendFile : boolean;
  oldMode    : word;

begin
  oldMode := SetErrorMode(SEM_FAILCRITICALERRORS);
  retVal := continueJob;
  dlg^.Update;

  StrCopy(filePath, 'A:*.db*');
  FindFirst(filePath, faAnyFile - faDirectory - faVolumeID, dirInfo);
  if DosError = 0 then
  begin
    while (DosError = 0) and (retVal = continueJob) and (dlg^.CanContinue)  do
    begin
      if strComp(lastFileName, dirInfo.Name) = 0 then
        appendFile := true
      else
        appendFile := false;
      strCopy(lastFileName,dirInfo.Name);

      StrCopy(srcFile, 'A:');
      StrCat(srcFile, dirInfo.Name);

      StrCopy(destFile, dbDir);
      StrCat(destFile, dirInfo.Name);

      dlg^.Update;
      retVal := CopyFile(dlg, srcFile, destFile, dn, rec, false, appendFile, true, nil);

      FindNext(dirInfo);
    end;
  end
  else
    retVal := DosError;

  SetErrorMode(oldMode);
  restoreFiles := retVal;
end;

{----------------------[FindArchiveField]-----------------------------------}
{---------------------------------------------------------------------------}
{                                                                           }
{---------------------------------------------------------------------------}
function FindArchiveField(db : PDBReadWriteFile): integer;
var
  fInfo   : TFldInfo;
  tFields : integer;
  temp    : integer;
  field   : integer;

begin
  temp := 0;
  field := 1;

  tFields := db^.dbd^.NumFields;
  while (field <= tFields) do
  begin
     db^.dbd^.FieldInfo(field,fInfo);
     if (fInfo.userFlag and db_Flags) <> 0 then
     begin
       temp := field;
       field := tFields + 1;
     end;
     inc(field);
  end;
  FindArchiveField := temp;
end;

{----------------------[BuildArchiveFiles]----------------------------------}
{---------------------------------------------------------------------------}
{                                                                           }
{---------------------------------------------------------------------------}
function BuildArchiveFiles(statDlg: PDlgFMStatus;
                           fNList : PCollection;
                           aDir   : PChar;
                           anExt  : PChar;
                           buildType : integer): integer;
var
  retVal      : integer;
  filePath    : array[0..maxPathLen] of char;
  fileDir     : array[0..maxPathLen] of char;
  fileName    : array[0..maxFileNameLen] of char;
  fileExt     : array[0..fsExtension] of char;
  dirInfo     : TSearchRec;
  pStr        : array[0..100] of char;
  db          : PDBReadWriteFile;
  fieldNumber : integer;
  fileInfo    : TFileInfo;

begin
  totalRecords := 0;
  retVal := continueJob;
  db := nil;

  statDlg^.Update;
  StrCopy(filePath, aDir);
  StrCat(filePath, '*');
  StrCat(filePath, anExt);
  FindFirst(filePath, faAnyFile - faDirectory - faVolumeID, dirInfo);
  while (DosError = 0) and (retVal = continueJob) and (statDlg^.CanContinue)  do
  begin
    FileSplit(dirInfo.Name, fileDir, fileName, fileExt);
    db:= New(PDBReadWriteFile, Init(fileName, aDir, DBOpenExclusive));
    if (db = nil) then
    begin
      retVal := fmErr_OpeningFileExclus;
      StrCopy(fmErr_Msg,aDir);
      StrCat(fmErr_Msg,fileName);
    end
    else
    begin
      db^.dbd^.FileInfo(fileInfo);
      fieldNumber := FindArchiveField(db);
      totalRecords := totalRecords + db^.dbc^.NumRecords;
      if buildType = backupAllFiles then
        fNList^.Insert(New(PArchFile, Init(fileName, db^.dbc^.NumRecords, fieldNumber)))
      else if (buildType = backupReportFiles) and
              ((fileInfo.userFlag and db_ReportFile) = db_ReportFile) then
        fNList^.Insert(New(PArchFile, Init(fileName, db^.dbc^.NumRecords, fieldNumber)));
      Dispose(db, Done);
      FindNext(dirInfo);
    end;
    StatDlg^.Update;
  end;
  BuildArchiveFiles := retVal;
end;

{----------------------[ComputeTotalBytes]----------------------------------}
{---------------------------------------------------------------------------}
{                                                                           }
{---------------------------------------------------------------------------}
function ComputeTotalBytes(dlg            : PDlgFMStatus;
                           aDir           : PChar;
                           var estDiskNum : integer): longint;
var
  filePath   : array[0..maxPathLen] of char;
  dirInfo    : TSearchRec;
  temp       : longint;
  totalBytes : longint;

begin
  dlg^.Update;
  totalBytes := 0;
  StrCopy(filePath, aDir);
  StrCat(filePath, '*.DB*');
  FindFirst(filePath, faAnyFile - faDirectory - faVolumeID, dirInfo);
  while DosError = 0 do
  begin
    totalBytes := totalBytes + dirInfo.size;
    FindNext(dirInfo);
    dlg^.Update;
  end;

  if totalBytes <> 0 then
  begin
    estDiskNum := totalBytes div floppyBytes;
    temp := totalBytes mod floppyBytes;
    if temp > 0 then
      estDiskNum := estDiskNum + 1;
  end;

  ComputeTotalBytes := totalBytes;
end;

{----------------------[CopyFiles]------------------------------------------}
{---------------------------------------------------------------------------}
{                                                                           }
{---------------------------------------------------------------------------}
function CopyFiles(dlg :PDlgFMStatus; fNList : PCollection;
                    aSrcDir, aDestDir, anExt :PChar;
                    var dn :integer; rec : TBackupRec;
                    floppyCopy, appendFile, displayPercentBar: boolean;
                    aTitle: PChar): integer;
var
  srcFile   : array[0..255] of char;
  destFile  : array[0..255] of char;
  i         : integer;
  retVal    : integer;
  fileEntry : PArchFile;

begin
  retVal := continueJob;
  i := 0;

  dlg^.Update;
  while (i <=  FNList^.count-1) and
        (retVal = continueJob) and dlg^.CanContinue do
  begin
    fileEntry := FNList^.At(i);
    StrCopy(srcFile, aSrcDir);
    StrCat(srcFile, fileEntry^.fileName);
    StrCat(srcFile, anExt);

    StrCopy(destFile, aDestDir);
    StrCat(destFile, fileEntry^.fileName);
    StrCat(destFile, anExt);

    dlg^.Update;
    retVal := CopyFile(dlg, srcFile, destFile, dn, rec, floppyCopy,
                       appendFile, displayPercentBar, aTitle);
    inc(i);
  end;

  CopyFiles := retVal;
end;

{-----------------------------[RestoreData]---------------------------------}
{---------------------------------------------------------------------------}
{                                                                           }
{---------------------------------------------------------------------------}
function RestoreData(dlg:PDlgFMStatus; dbDir, workDir: PChar): integer;
var
  retVal    : integer;
  filePath  : array[0..maxPathLen] of char;
  fileDir   : array[0..maxPathLen] of char;
  fileName  : array[0..maxFileNameLen] of char;
  fileExt   : array[0..fsExtension] of char;
  dirInfo   : TSearchRec;
  srcFile   : array[0..255] of char;
  destFile  : array[0..255] of char;
  pStr      : array[0..100] of char;
  db        : PDBReadWriteFile;
  dn        : integer;
  rec       : TBackupRec;
  srcDB     : PDBCopyFile;
  destDB    : PDBCopyFile;
  seqRef    : TSeqNum;
  errorFlag : boolean;

begin
  dlg^.SetText('  ', IDC_PB1_TEXT);
  ShowWindow(GetDlgItem(dlg^.HWindow, IDC_PB1), SW_HIDE);
  ShowWindow(GetDlgItem(dlg^.HWindow, IDC_PB2_TEXT), SW_HIDE);
  ShowWindow(GetDlgItem(dlg^.HWindow, IDC_PB2), SW_HIDE);

  retVal := continueJob;

  StrCopy(filePath, workDir);
  StrCat(filePath, '*.DBD');
  FindFirst(filePath, faAnyFile - faDirectory - faVolumeID, dirInfo);
  while (DosError = 0) and (retVal = continueJob) and (dlg^.CanContinue)  do
  begin
    FileSplit(dirInfo.Name, fileDir, fileName, fileExt);
    StrCopy(srcFile, dbDir);
    StrCat(srcFile, fileName);
    StrCat(srcFile, '.DBI');

    StrCopy(destFile, workDir);
    StrCat(destFile, fileName);
    StrCat(destFile, '.DBI');

    dlg^.Update;
    retVal := CopyFile(dlg, srcFile, destFile, dn, rec, false, false, false, nil);
    FindNext(dirInfo);
  end;

  {- Needs better error checking. -}
  StrCopy(filePath, workDir);
  StrCat(filePath, '*.DBI');
  FindFirst(filePath, faAnyFile - faDirectory - faVolumeID, dirInfo);
  while (DosError = 0) and (retVal = continueJob) and (dlg^.CanContinue)  do
  begin
    FileSplit(dirInfo.Name, fileDir, fileName, fileExt);

    srcDB := New(PDBCopyFile, Init(fileName, workDir, DBOpenExclusive));
    if (srcDB = nil) then
    begin
      retVal := fmErr_OpeningFile;
      StrCopy(fmErr_Msg,workDir);
      StrCat(fmErr_Msg,fileName);
    end;

    destDB:= New(PDBCopyFile, Init(fileName, dbDir, DBOpenExclusive));
    if (destDB = nil) then
    begin
      retVal := fmErr_OpeningFile;
      StrCopy(fmErr_Msg,dbDir);
      StrCat(fmErr_Msg,fileName);
    end;

    if srcDB^.dbc^.GetFirst(srcDB^.dbr) then
      repeat

        seqRef := srcDB^.dbr^.getSeqValue;

        errorFlag := destDB^.dbc^.GetSeq(destDB^.dbr, seqRef);

        destDB^.dbr^.CopyRecord(srcDB^.dbr);
        if errorFlag then
          destDB^.dbc^.UpdateRec(destDB^.dbr)
        else
          destDB^.dbc^.InsertRec(destDB^.dbr);
      until not srcDB^.dbc^.GetNext(srcDB^.dbr);

    if (srcDB <> nil) then
      Dispose(srcDB, Done);
    if (destDB <> nil) then
      Dispose(destDB, Done);

    FindNext(dirInfo);
  end;

  restoreData := retVal;
end;

{----------------------[GetFullBackupCatalog]-------------------------------}
{---------------------------------------------------------------------------}
{                                                                           }
{  Possible errors                                                          }
{  - No catalog on disk 'Disk.XXX' DosError = 18                            }
{  - Not the first disk of the Full Backup                                  }
{  - Not the a Full backup disk                                             }
{  - Hardware Disk Error   any DosError other than 18                       }
{---------------------------------------------------------------------------}
function GetFullBackupCatalog(aParent   : PDlgFMStatus;
                              catalogId : integer;
                              var rec   : TBackupRec): integer;
var
  filePath   : array[0..maxPathLen] of char;
  fileDir    : array[0..maxPathLen] of char;
  fileName   : array[0..maxFileNameLen] of char;
  fileExt    : array[0..fsExtension] of char;
  dirInfo    : TSearchRec;
  aFile      : file;
  numread    : word;
  warnMsg    : array [0..255] of char;
  errorMsg   : array [0..255] of char;
  retVal     : integer;
  diskId     : integer;
  code       : integer;

Begin
  retVal := continueJob;

  repeat
    FindFirst('A:DISK.*', faAnyFile - faDirectory - faVolumeID, dirInfo);
    if DosError = 0 then
    begin
      FileSplit(dirInfo.Name, fileDir, fileName, fileExt);
      fileExt[0] := '0';
      Val(fileExt,diskId,code);
      if diskId = 1 then
      begin
        StrCopy(filePath, 'A:');
        StrCat(filePath, dirInfo.name);
        Assign(aFile, filePath);
        Reset(aFile);
        retVal := IOResult;
        if retVal <> continueJob then
        begin
          GetFullBackupCatalog := retVal;
          exit;
        end;
        BlockRead(aFile, rec, sizeOF(TBackupRec), numread);
        retVal := IOResult;
        if retVal <> continueJob then
        begin
          GetFullBackupCatalog := retVal;
          exit;
        end;
        close(aFile);
        retVal := IOResult;
        if retVal <> continueJob then
        begin
          GetFullBackupCatalog := retVal;
          exit;
        end;
        if ((rec.BType = 'F') and (rec.BIdent = catalogID)) then
          retVal := continueJob
        else
        begin
          SR(IDS_WARNING, warnMsg, 255);
          SR(IDS_INVALIDDISKMSG, errorMsg, 255);
          if RetryCancelMsg(aParent^.hwindow, warnMsg, errorMsg) then
            retVal := tryNewDisk
          else
            retVal := cancelJob;
        end;
      end
      else
      begin
          {Disk in drive is not disk 1. Display message}
          SR(IDS_WARNING, warnMsg, 255);
          SR(IDS_NOTDISK1MSG, errorMsg, 255);
          if RetryCancelMsg(aParent^.hwindow, warnMsg, errorMsg) then
            retVal := tryNewDisk
          else
            retVal := cancelJob;
      end;
    end
    else
    begin {- DosError  -}
      if DosError = noMoreFiles then    {- DosError 18 -}
      begin
        SR(IDS_WARNING, warnMsg, 255);
        SR(IDS_CATALOGNOTFOUNDMSG, errorMsg, 255);
        if RetryCancelMsg(aParent^.hwindow, warnMsg, errorMsg) then
          retVal := tryNewDisk
        else
          retVal := cancelJob;
      end
      else {- all other DosErrors  -}
      begin
        SR(IDS_WARNING, warnMsg, 255);
        SR(IDS_DISKERRORMSG, errorMsg, 255);
        if RetryCancelMsg(aParent^.hwindow, warnMsg, errorMsg) then
          retVal := tryNewDisk
        else
          retVal := cancelJob;
      end;
    end;
  until retVal <> tryNewDisk;
  GetFullBackupCatalog := retVal;
end;

{----------------------[SetFullBackupCatalog]-------------------------------}
{---------------------------------------------------------------------------}
{                                                                           }
{---------------------------------------------------------------------------}
function SetFullBackupCatalog(var rec    : TBackupRec;
                              totalBytes : longint;
                              dt         : TDateTime): integer;
var
  filePath   : array[0..maxPathLen] of char;
  aFile      : file;
  numwritten : word;
  retVal     : integer;

begin
  retVal := continueJob;

  rec.incrDt := dt;
  rec.numIncr := 1;
  rec.numIncrBytes := totalBytes;
  {$I-}
  Assign(aFile, 'A:DISK.001');
  Rewrite(aFile,1);
  retVal := IOResult;
  if retVal = continueJob then
  begin
    BlockWrite(aFile, rec, sizeOF(TBackupRec), numwritten);
    retVal := IOResult;
    if  (retVal = continueJob) and (numwritten <> sizeOf(TBackupRec)) then
      retVal := fmErr_DiskWrite;
  end;
  close(aFile);
  {$I+}
  SetFullBackupCatalog := retVal;
end;

{--------------------------[MakeCatalog]------------------------------------}
{---------------------------------------------------------------------------}
{                                                                           }
{---------------------------------------------------------------------------}
function MakeCatalog(aParent    : PDlgFMStatus;
                     var rec    : TBackupRec;
                     totalBytes : longint;
                     numDisks   : integer;
                     catalogType: char): integer;
var
  y,m,d,dow   : word;
  h,mn,s,hund : word;
  idStr       : array [0..100] of char;
  id          : integer;
  code        : integer;
  temp        : TBackupRec;
  retVal      : integer;
  oldMode     : Word;

begin
  retVal := continueJob;

  GetDate(y,m,d,dow);
  GetTime(h,mn,s,hund);

  rec.BType        := catalogType;
  rec.Status       := BackupInComplete;
  rec.dt.year      := y;
  rec.dt.month     := m;
  rec.dt.day       := d;
  rec.dt.hour      := h;
  rec.dt.min       := mn;
  rec.dt.sec       := s;
  rec.numDisks     := numDisks;
  rec.numBytes     := totalBytes;
  rec.incrDt.year  := 0;
  rec.incrDt.month := 0;
  rec.incrDt.day   := 0;
  rec.incrDt.hour  := 0;
  rec.incrDt.min   := 0;
  rec.incrDt.sec   := 0;
  rec.numIncr      := 0;
  rec.numIncrBytes := 0;

  case catalogType of
    FullCatalog   :
      begin
        INIGetBackupID(idStr);
        Val(idStr,id,code);
        id := id + 1;
        rec.BIdent := id;
        Str(id, idStr);
        INISetBackupID(idStr);
      end;
    IncrCatalog   :
      begin
        oldMode := SetErrorMode(SEM_FAILCRITICALERRORS);
        {- Make sure the Full backup is the most recent full backup. -}
        INIGetBackupID(idStr);
        Val(idStr,id,code);
        (*retVal := GetFullBackupCatalog(aParent,id,temp);*)
        (*if retVal = continueJob then*)
        (*begin*)
        rec.BIdent := id;
        (*retVal := SetFullBackupCatalog(temp, totalBytes, rec.dt);*)
        (*end;*)
        SetErrorMode(oldMode);
      end;
    ExtractCatalog:
      begin
        INIGetExtractID(idStr);
        Val(idStr,id,code);
        id := id + 1;
        rec.BIdent := id;
        Str(id, idStr);
        INISetExtractID(idStr);
      end;
    ReportCatalog :
      rec.BIdent := random(100);
  end;

  MakeCatalog := retVal;
end;

{----------------------[GetCurrentDir]--------------------------------------}
{---------------------------------------------------------------------------}
{                                                                           }
{---------------------------------------------------------------------------}
procedure GetCurrentDir(aDir : PChar; var drive : byte);
var
  filePath : array[0..maxPathLen] of char;
  fileName: array[0..maxFileNameLen] of char;
  fileExt : array[0..fsExtension] of char;

begin
  if (ParamCount > 0) then
  begin
    StrPCopy(aDir, ParamStr(1));
    if (aDir[StrLen(aDir)] <> ':') and (aDir[StrLen(aDir)] <> '\') then
      StrCat(aDir, '\');
  end
  else
  begin
    StrPCopy(filePath, ParamStr(0));
    FileSplit(filePath, aDir, fileName, fileExt);
  end;

  {default drive - It may have to be changed later.}
  drive := 0;
end;

{----------------------[GetWorkingDir]--------------------------------------}
{---------------------------------------------------------------------------}
{                                                                           }
{---------------------------------------------------------------------------}
procedure GetWorkingDir(dbDir, workDir, backupDir : PChar);
var
  filePath: array[0..maxPathLen] of char;
  dirInfo : TSearchRec;
  aFile   : file;
  dirFound : boolean;
Begin
  StrCopy(workDir, dbDir);
  if (workDir[StrLen(workDir)-1] <> ':') and (workDir[StrLen(workDir)-1] <> '\') then
    StrCat(workDir, '\');
  StrCat(workDir, backupDir);

  dirFound := false;
  StrCopy(filePath, dbDir);
  StrCat(filePath, '*.');
  FindFirst(filePath, faDirectory, dirInfo);
  while (DosError = 0) and (not dirFound) do
  begin
    if StrComp(backupdir,dirInfo.Name) = 0 then
      dirFound := true
    else
      FindNext(dirInfo);
  end;
  if NOT dirFound then
    CreateDir(workDir);

  if (workDir[StrLen(workDir)] <> ':') and (workDir[StrLen(workDir)] <> '\') then
    StrCat(workDir, '\');

end;


{-------------------------[KillDir]-----------------------------------------}
{---------------------------------------------------------------------------}
{Recursive directory killer!                                                }
{---------------------------------------------------------------------------}
Procedure KillDir(fDir: PChar);
var
  diskPath : array[0..255] of char;
  filePath : array[0..255] of char;
  newpath  : array[0..255] of char;
  aFileRec : TSearchRec;
  attr     : Word;
  ff       : File;
  retVal   : integer;

begin
  StrCopy(diskPath, fDir);
  if (diskPath[StrLen(diskPath)-1] <> ':') and
     (diskPath[StrLen(diskPath)-1] <> '\') then
    StrCat(diskPath, '\');

  StrCopy(filePath,diskPath);
  StrCat(filePath, '*.*');

  FindFirst(filePath, faAnyFile, aFileRec);
  if (DosError <> NoMoreFiles) then
  begin
    repeat
      { Ignore the two default entries in each directory. }
      if (StrComp(aFileRec.Name,'.') <> 0) and
         (StrComp(aFileRec.Name,'..') <> 0) then
      begin
        { If the search item we are currently working with is a }
        { directory, the Attribute word will have bit $10 set.  }
        if ((aFileRec.Attr and faDirectory) <> 0) then
        begin
          { Make the newly found directory the current default directory. }
          ChDir(aFileRec.Name);
          { Recursive call to kill the newly found subdirectory's contents. }
          StrCopy(newPath,diskPath);
          StrCat(newPath,aFileRec.Name);
          KillDir(newPath);
          {$I-}            { Turn off i/o checking so we don't error out  }
          ChDir('..');     { if they try to delete directory they are in. }
          { Remove the directory we just backed out of. }
          RemoveDir(newPath);
          {$I+}
        end
      else
        begin
          { It is not a directory, so erase it. }
          StrCopy(newPath,diskPath);
          StrCat(newPath,aFileRec.Name);
          {$I-}
          assign(ff, newPath);
          SetFAttr(ff,$00);
          retVal := IOResult;
          reset(ff);
          close(ff);
          erase(ff);
          retVal := IOResult;
          {$I+}
        end;
      end;
      FindNext(aFileRec);
    until (DosError = NoMoreFiles);
  end;
end;

{------------------------[WipeDisk]-----------------------------------------}
{---------------------------------------------------------------------------}
{ BE CAREFULL                                                               }
{---------------------------------------------------------------------------}
procedure WipeDisk(aDisk: PChar);
var
  oldPath  : array[0..255] of char;
  diskPath : array[0..255] of char;
  filePath : array[0..255] of char;
  newpath  : array[0..255] of char;
  aFileRec : TSearchRec;
  attr     : Word;
  ff       : File;
  retVal   : integer;

Begin
  retVal := continueJob;

  StrCopy(diskPath, aDisk);
  if (diskPath[StrLen(diskPath)-1] <> '\') then
    StrCat(diskPath, '\');

  StrCopy(filePath,diskPath);
  StrCat(filePath, '*.*');

  {I-}
  GetCurDir(oldPath,0);
  SetCurdir(diskPath);
  retVal := IOResult;
  {I+}

  if retVal = continueJob then
  begin
    FindFirst(filePath, faAnyFile, aFileRec);
    if (DosError <> NoMoreFiles) then
    begin
      repeat
        { Ignore the two default entries in each directory. }
        if (StrComp(aFileRec.Name,'.') <> 0) and
           (StrComp(aFileRec.Name,'..') <> 0) then
        begin
          { If the search item we are currently working with is a }
          { directory, the Attribute word will have bit $10 set.  }
          if ((aFileRec.Attr and faDirectory) <> 0) then
          begin
            { Make the newly found directory the current default directory. }
            ChDir(StrPas(aFileRec.Name));
            StrCopy(newPath,diskPath);
            StrCat(newPath,aFileRec.Name);
            KillDir(newPath);
            {$I-}            { Turn off i/o checking so we don't error out  }
            ChDir('..');     { if they try to delete directory they are in. }
            { Remove the directory we just backed out of. }
            RemoveDir(newPath);
            {$I+}
          end
          else
          begin
            { It is not a directory, so erase it. }
            StrCopy(newPath,diskPath);
            StrCat(newPath,aFileRec.Name);
            assign(ff, newPath);
            SetFAttr(ff,$00);
            retVal := DosError;
            {$I-}            { Turn off i/o checking so we don't error out  }
            reset(ff);
            close(ff);
            erase(ff);
            retVal := IOResult;
            {$I+}            { Turn off i/o checking so we don't error out  }
          end;
        end;
        FindNext(aFileRec);
      until (DosError = NoMoreFiles);
    end;
  end;

  {I-}
  SetCurdir(oldPath);
  retVal := IOResult;
  {I+}

end;

{----------------------[CleanDir]-------------------------------------------}
{---------------------------------------------------------------------------}
{                                                                           }
{---------------------------------------------------------------------------}
procedure CleanDir(adir, aFileId : PChar);
var
  allFiles: array[0..maxPathLen] of char;
  filePath: array[0..maxPathLen] of char;
  dirInfo : TSearchRec;
  aFile   : file;
Begin
  StrCopy(allFiles, adir);
  StrCat(allFiles, aFileId);
  FindFirst(allFiles, faAnyFile - faDirectory - faVolumeID, dirInfo);
  while DosError = 0 do
  begin
    StrCopy(filePath, adir);
    StrCat(filePath, dirInfo.name);
    {$I-}
    Assign(aFile, filePath);
    SetFAttr(aFile,0);
    Erase(afile);
    {$I+}
    FindNext(dirInfo);
  end;

end;

{----------------------[WriteProtected]-------------------------------------}
{---------------------------------------------------------------------------}
{                                                                           }
{                                                                           }
{---------------------------------------------------------------------------}
Function WriteProtected(dlg : PDlgFMStatus; dirStr : PChar): integer;
var
  retVal     : integer;
  aFile      : File;
  numwritten : Word;
  dummyData  : integer;
  fileName   : array [0..20] of char;
  p1Str       : array [0..255] of char;
  p2Str       : array [0..255] of char;

begin
  retVal := continueJob;
  StrCopy(FileName, dirStr);
  StrCat(FileName, 'DWC.XYZ');

  {$I-}
  Assign(aFile, fileName);
  Rewrite(aFile, 1);
  if IOResult = 5 then
  begin
    Close(aFile);
    retVal := IOResult;
    if RetryCancelMsg(dlg^.hwindow, SR(IDS_WARNING, p1Str, 255),
                      SR(IDS_DISKWRITEERRORMSG, p2str, 255)) then
      retVal := tryNewDisk
    else
      retVal := cancelJob;
  end
  else
  begin
    dummyData := 100;
    BlockWrite(aFile, dummyData, 4, numwritten);
    retVal:= IOResult;
    Close(aFile);
    retVal:= IOResult;
    Erase(aFile);
    retVal:= IOResult;
  end;
  {$I+}
  WriteProtected := retVal;
end;


{----------------------[CheckDisk]------------------------------------------}
{---------------------------------------------------------------------------}
{                                                                           }
{ Make sure the disk does not already belong to the backup set.             }
{ Make sure the disk is not write protected.                                }
{ Delete any data that is already on the disk.                              }
{                                                                           }
{---------------------------------------------------------------------------}
function CheckDisk(aParent: PDlgFMStatus; rec : TBackupRec): integer;
var
  dlgOverWrite : PDlgOverwrite;
  retVal       : integer;
  testVal      : integer;
  filePath     : array[0..maxPathLen] of char;
  dirInfo      : TSearchRec;
  aFile        : file;
  temp         : TBackupRec;
  numread      : word;
  numwritten   : word;
  errorMsg     : array [0..255] of char;
  warnMsg      : array [0..255] of char;
  dSize        : longint;
  dFree        : longint;
  pStr         : array [0..50] of char;

Begin
  retVal := continueJob;

  {$I-}
  repeat
    FindFirst('A:DISK.*', faAnyFile - faDirectory - faVolumeID, dirInfo);
    if DosError = 0 then
    begin
      StrCopy(filePath, 'A:');
      StrCat(filePath, dirInfo.name);
      Assign(aFile, filePath);
      testVal := IOResult;
      Reset(aFile);
      testVal := IOResult;
      BlockRead(aFile, temp, sizeOf(TBackupRec), numread);
      testVal := IOResult;
      close(aFile);
      testVal := IOResult;
      if (temp.BIdent = rec.BIdent) and (temp.BType <> 'I')   then
      begin
        SR(IDS_WARNING, warnMsg, 255);
        SR(IDS_USEDDISKMSG, errorMsg, 255);
        if RetryCancelMsg(aParent^.hwindow, warnMsg, errorMsg) then
          retVal := tryNewDisk
        else
          retVal := cancelJob;
      end
      else
        retval := continueJob;
    end
    else
    begin {- DosError 18 -}
      if DosError = noMoreFiles then
        retVal := continueJob
      else
      begin {- Any other DosError -}
        SR(IDS_WARNING, warnMsg, 255);
        SR(IDS_DISKERRORMSG, errorMsg, 255);
        if RetryCancelMsg(aParent^.hwindow, warnMsg, errorMsg) then
          retVal := tryNewDisk
        else
          retVal := cancelJob;
      end;
    end;
    if (retVal = continueJob) then
      retVal := WriteProtected(aParent, 'A:');
    if (retVal = continueJob) then
    begin
      dSize := DiskSize(1);
      dFree := DiskFree(1);
      if DiskSize(1) <> DiskFree(1) then
      begin
        dlgOverWrite:= New(PDlgOverWrite, Init(aParent));
        if application^.ExecDialog(dlgOverWrite) = IDOK then
        begin
          if retVal = continueJob then
          begin
            aParent^.SetText(SR(IDS_FLOPPYDELETE,  pStr, 50),IDC_PB1_TEXT);
            ShowWindow(GetDlgItem(aParent^.HWindow, IDC_PB1), SW_HIDE);
            ShowWindow(GetDlgItem(aParent^.HWindow, IDCANCEL), SW_HIDE);
            WipeDisk('A:');
            aParent^.SetText(SR(IDS_PERDISK,  pStr, 50),IDC_PB1_TEXT);
            ShowWindow(GetDlgItem(aParent^.HWindow, IDCANCEL), SW_SHOW);
            ShowWindow(GetDlgItem(aParent^.HWindow, IDC_PB1), SW_SHOW);
          end;
        end
        else
          retVal := tryNewDisk;
      end;
    end;
  until retVal <> tryNewDisk;
  {$I+}

  CheckDisk := retVal;
end;

{----------------------[WriteDiskId]----------------------------------------}
{---------------------------------------------------------------------------}
{                                                                           }
{---------------------------------------------------------------------------}
function WriteDiskId(diskNum : integer; rec : TBackupRec): integer;
var
  f   : File;
  numwritten : Word;
  numread    : Word;
  fPChar     : array [0..255] of char;
  pDiskNum   : array [0..255] of char;
  fext       : array [0..3] of char;
  retVal     : integer;

Begin
  retVal := continueJob;
  Str(diskNum,pDiskNum);
  LeftPad(fext,pDiskNum,'0',3);

  StrCopy(fPchar,'A:DISK.');
  StrCat(fPchar,fext);
  {$I-}
  Assign(f, fPchar);
  Rewrite(f, 1);
  retval:= IOResult;
  If retval = continueJob Then
  begin
     BlockWrite(f, rec, sizeOf(TBackupRec), numwritten);
     retVal := IOResult;
     if (retVal = continueJob) and  (numwritten <> sizeOf(TBackupRec)) then
        retVal := fmErr_DiskWrite;
  end;

  close(f);
  {$I+}
  WriteDiskId := retVal;
end;

{----------------------[GetFirstFullBackupDisk]-----------------------------}
{---------------------------------------------------------------------------}
{                                                                           }
{---------------------------------------------------------------------------}
function GetFirstFullBackupDisk(dlg         : PDlgFMStatus;
                                var fullRec : TBackupRec): integer;
var
  dlgIncr  : PDlgDiskComp;
  retVal   : integer;
  testVal  : integer;
  oldMode  : Word;
  dirInfo  : TSearchRec;
  aFile    : file;
  numread  : word;
  errorMsg : array [0..255] of char;
  warnMsg  : array [0..255] of char;
  idStr    : array [0..10] of char;
  id       : integer;
  code     : integer;

begin
  retVal := continueJob;
  oldMode := SetErrorMode(SEM_FAILCRITICALERRORS);
  INIGetBackupID(idStr);
  Val(idStr, id, code);

  dlgIncr:= New(PDlgDiskComp, Init(dlg, MakeIntResource(DLG_INCR)));
  if application^.ExecDialog(dlgIncr) = IDOK then
  begin
    repeat
      FindFirst('A:DISK.001', faAnyFile - faDirectory - faVolumeID, dirInfo);
      if DosError = 0 then
      begin
        Assign(aFile, 'A:DISK.001');
        testVal := IOResult;
        Reset(aFile);
        testVal := IOResult;
        BlockRead(aFile, fullRec, sizeOf(TBackupRec), numread);
        testVal := IOResult;
        close(aFile);
        testVal := IOResult;
        if (fullRec.BIdent = id) and (fullRec.BType = 'F')   then
          retval := continueJob
        else
        begin
          SR(IDS_WARNING, warnMsg, 255);
          SR(IDS_NOTFIRSTDISK, errorMsg, 255);
          if RetryCancelMsg(dlg^.hwindow, warnMsg, errorMsg) then
            retVal := tryNewDisk
          else
            retVal := cancelJob;
        end;
      end
      else
      begin {- DosError 18 -}
        if DosError = noMoreFiles then
        begin
          SR(IDS_WARNING, warnMsg, 255);
          SR(IDS_NOTFIRSTDISK, errorMsg, 255);
          if RetryCancelMsg(dlg^.hwindow, warnMsg, errorMsg) then
            retVal := tryNewDisk
          else
            retVal := cancelJob;
        end
        else
        begin {- Any other DosError -}
          SR(IDS_WARNING, warnMsg, 255);
          SR(IDS_DISKERRORMSG, errorMsg, 255);
          if RetryCancelMsg(dlg^.hwindow, warnMsg, errorMsg) then
            retVal := tryNewDisk
          else
            retVal := cancelJob;
        end;
      end;
(*      if (retVal = continueJob) then*)
(*        retVal := WriteProtected(dlg, 'A:');*)
    until retVal <> tryNewDisk;
  end
  else
    retVal := cancelJob;

  SetErrorMode(oldMode);
  GetFirstFullBackupDisk := retVal;
end;

{----------------------[InsertDisk]-----------------------------------------}
{---------------------------------------------------------------------------}
{                                                                           }
{---------------------------------------------------------------------------}
function InsertDisk(aParent     : PDlgFMStatus;
                    var diskNum : integer;
                    rec         : TBackupRec;
                    aTitle      : PChar) : integer;
var
  dlgDisk     : PDlgDiskNum;
  diskDisplay : array[0..20] of char;
  diskStr     : array [0..10] of char;
  retVal      : integer;
  oldMode     : Word;
  errorMsg    : array [0..255] of char;
  warnMsg     : array [0..255] of char;
  diskMsg     : array [0..255] of char;

begin
  retVal := continueJob;

  inc(diskNum);
  Str(diskNum, diskStr);
  SR(IDS_FMDISK, DiskMsg, 255);

  strCopy(diskDisplay, DiskMsg);
  StrCat(diskDisplay,' ');
  StrCat(diskDisplay,diskStr);
  oldMode := SetErrorMode(SEM_FAILCRITICALERRORS);

  dlgDisk:= New(PDlgDiskNum, Init(aParent,diskStr,aTitle));
  if application^.ExecDialog(dlgDisk) = IDOK then
  begin
    aParent^.SetText(diskDisplay, IDC_TEXT2);
    repeat
      aParent^.Update;
      retVal := CheckDisk(aParent,rec);
      if retVal = continueJob then
      begin
        diskSpace := DiskFree(1);
        retval := WriteDiskId(diskNum,rec);
        aParent^.Update;
        if retVal = fmErr_DiskWrite then
        begin
          SR(IDS_WARNING, warnMsg, 255);
          SR(IDS_CATALOGERRORMSG, errorMsg, 255);
          if RetryCancelMsg(aParent^.hwindow, warnMsg, errorMsg) then
            retVal := tryNewDisk
          else
            retVal := cancelJob;
        end;
      end;
    until retval <> tryNewDisk;
  end
  else
    retVal := cancelJob;

  aParent^.Update;
  SetErrorMode(oldMode);
  InsertDisk := retVal;
end;

{-------------------------[InsertRestoreDisk]-------------------------------}
{---------------------------------------------------------------------------}
{                                                                           }
{---------------------------------------------------------------------------}
function InsertRestoreDisk(aParent    : PDlgFMStatus;
                           diskNum,
                           totalDisks : integer;
                           BackupType : char;
                           backupId   : integer;
                           msgStr,
                           aTitle     : PChar): integer;
var
  filePath    : array[0..maxPathLen] of char;
  fileDir     : array[0..maxPathLen] of char;
  fileName    : array[0..maxFileNameLen] of char;
  fileExt     : array[0..fsExtension] of char;
  dlgDisk     : PDlgRestoreDisk;
  numread     : word;
  retVal      : integer;
  diskId      : integer;
  code        : integer;
  oldMode     : Word;
  errorMsg    : array [0..255] of char;
  warnMsg     : array [0..255] of char;
  dirInfo     : TSearchRec;
  aFile       : file;
  diskStr     : array [0..10] of char;
  diskDisplay : array[0..25] of char;
  tempPChar   : array[0..10] of char;
  temp        : TBackupRec;

begin
  retVal := continueJob;
  Str(diskNum, diskStr);

  oldMode := SetErrorMode(SEM_FAILCRITICALERRORS);

  diskSpace := floppyBytes;
  dlgDisk:= New(PDlgRestoreDisk, Init(aParent,diskStr,msgStr, aTitle));
  if application^.ExecDialog(dlgDisk) = IDOK then
  begin
    aParent^.Update;
    StrCopy(diskDisplay,'Disk ');
    Str(diskNum, tempPChar);
    StrCat(diskDisplay,tempPChar);
    StrCat(diskDisplay,' of ');
    str(totalDisks,tempPChar);
    StrCat(diskDisplay,tempPChar);
    aParent^.SetText(diskDisplay, IDC_TEXT2);
    repeat
      FindFirst('A:DISK.*', faAnyFile - faDirectory - faVolumeID, dirInfo);
      if DosError = 0 then
      begin
        FileSplit(dirInfo.Name, fileDir, fileName, fileExt);
        fileExt[0] := '0';
        Val(fileExt,diskId,code);
        if diskId = diskNum then
        begin
          StrCopy(filePath, 'A:');
          StrCat(filePath, dirInfo.name);
          Assign(aFile, filePath);
          Reset(aFile);
          BlockRead(aFile, temp, sizeOF(TBackupRec), numread);
          close(aFile);
          if (backupType = temp.BType)and(backupId = temp.BIdent) then
            retval := continueJob
          else
          begin
            SR(IDS_WARNING, warnMsg, 255);
            SR(IDS_WRONGBACKUPSET, errorMsg, 255);
            if RetryCancelMsg(aParent^.hwindow, warnMsg, errorMsg) then
              retVal := tryNewDisk
            else
              retVal := cancelJob;
          end;
        end
        else
        begin
          SR(IDS_WARNING, warnMsg, 255);
          SR(IDS_NOTCORRECTDISKMSG, errorMsg, 255);
          if RetryCancelMsg(aParent^.hwindow, warnMsg, errorMsg) then
            retVal := tryNewDisk
          else
            retVal := cancelJob;
        end;
      end
      else
      begin {- DosError 18 -}
        if DosError = noMoreFiles then
        begin
          {- no catalog on disk -}
          SR(IDS_WARNING, warnMsg, 255);
          SR(IDS_CATALOGNOTFOUNDMSG, errorMsg, 255);
          if RetryCancelMsg(aParent^.hwindow, warnMsg, errorMsg) then
            retVal := tryNewDisk
          else
            retVal := cancelJob;
        end
        else
        begin {- Any other DosError -}
          SR(IDS_WARNING, warnMsg, 255);
          SR(IDS_DISKERRORMSG, errorMsg, 255);
          if RetryCancelMsg(aParent^.hwindow, warnMsg, errorMsg) then
            retVal := tryNewDisk
          else
            retVal := cancelJob;
        end;
      end;
    until retVal <> tryNewDisk;
  end
  else
    retVal := cancelJob;

  aParent^.Update;
  SetErrorMode(oldMode);
  InsertRestoreDisk := retVal;
end;

{--------------------------------[getSetInfo]-------------------------------}
{---------------------------------------------------------------------------}
{                                                                           }
{---------------------------------------------------------------------------}
function getSetInfo(aParent    : PDlgFMStatus;
                    var setRec : TBackupRec;
                    msgStr,
                    aTitle     : PChar;
                    catalogType: Char;
                    backupId   : integer): integer;
var
  filePath    : array[0..maxPathLen] of char;
  fileDir     : array[0..maxPathLen] of char;
  fileName    : array[0..maxFileNameLen] of char;
  fileExt     : array[0..fsExtension] of char;
  dlgDisk     : PDlgRestoreDisk;
  numread     : word;
  retVal      : integer;
  diskId      : integer;
  code        : integer;
  oldMode     : Word;
  errorMsg    : array [0..255] of char;
  warnMsg     : array [0..255] of char;
  dirInfo     : TSearchRec;
  aFile       : file;
  catalogCond : boolean;

begin
  retVal := continueJob;

  oldMode := SetErrorMode(SEM_FAILCRITICALERRORS);

  diskSpace := floppyBytes;
  dlgDisk:= New(PDlgRestoreDisk, Init(aParent,'1',msgStr, aTitle));
  if application^.ExecDialog(dlgDisk) = IDOK then
  begin
    aParent^.Update;
    repeat
      FindFirst('A:DISK.*', faAnyFile - faDirectory - faVolumeID, dirInfo);
      if DosError = 0 then
      begin
        FileSplit(dirInfo.Name, fileDir, fileName, fileExt);
        fileExt[0] := '0';
        Val(fileExt,diskId,code);
        if diskId = 1 then
        begin
          StrCopy(filePath, 'A:');
          StrCat(filePath, dirInfo.name);
          Assign(aFile, filePath);
          Reset(aFile);
          BlockRead(aFile, setRec, sizeOF(TBackupRec), numread);
          close(aFile);

          catalogCond := false;
          case catalogType of
            FullCatalog:
              if (setRec.BType = catalogType) and (setRec.status = BackupComplete) then
                catalogCond := true;
            IncrCatalog:
              if (setRec.BType = catalogType) and (setRec.BIdent = backupId) and
                 (setRec.status = BackupComplete) then
                catalogCond := true;
            ExtractCatalog:
              if ((setRec.BType = catalogType) or (setRec.BType = FullCatalog)) and
                  (setRec.status = BackupComplete) then
                catalogCond := true;
            ReportCatalog:
              if (setRec.BType = catalogType) and (setRec.status = BackupComplete) then
                catalogCond := true;
          end;

          if catalogCond then
            retval := continueJob
          else
          begin
            SR(IDS_WARNING, warnMsg, 255);
            if catalogType = IncrCatalog then
              SR(IDS_NOTINCRBACKUPSET, errorMsg, 255)
            else
              SR(IDS_NOTFULLBACKUPSET, errorMsg, 255);

            if (setRec.status = BackupInComplete) and (setRec.BType = FullCatalog)  then
              SR(IDS_INVALIDBACKUP, errorMsg, 255);

            if (setRec.status = BackupInComplete) and (setRec.BType = ExtractCatalog)   then
              SR(IDS_INVALIDEXTRACT, errorMsg, 255);

            if RetryCancelMsg(aParent^.hwindow, warnMsg, errorMsg) then
              retVal := tryNewDisk
            else
              retVal := cancelJob;
          end;
        end
        else
        begin
          SR(IDS_WARNING, warnMsg, 255);
          SR(IDS_NOTDISK1MSG, errorMsg, 255);
          if RetryCancelMsg(aParent^.hwindow, warnMsg, errorMsg) then
            retVal := tryNewDisk
          else
            retVal := cancelJob;
        end;
      end
      else
      begin {- DosError 18 -}
        if DosError = noMoreFiles then
        begin
          {- no catalog on disk -}
          SR(IDS_WARNING, warnMsg, 255);
          SR(IDS_CATALOGNOTFOUNDMSG, errorMsg, 255);
          if RetryCancelMsg(aParent^.hwindow, warnMsg, errorMsg) then
            retVal := tryNewDisk
          else
            retVal := cancelJob;
        end
        else
        begin {- Any other DosError -}
          SR(IDS_WARNING, warnMsg, 255);
          SR(IDS_DISKERRORMSG, errorMsg, 255);
          if RetryCancelMsg(aParent^.hwindow, warnMsg, errorMsg) then
            retVal := tryNewDisk
          else
            retVal := cancelJob;
        end;
      end;
    until retVal <> tryNewDisk;
  end
  else
    retVal := cancelJob;

  aParent^.Update;
  SetErrorMode(oldMode);
  getSetInfo := retVal;
end;

{----------------------[UpdateBackupSet]------------------------------------}
{---------------------------------------------------------------------------}
{                                                                           }
{---------------------------------------------------------------------------}
procedure UpdateBackupSet(dlg        : PDlgFMStatus;
                          incrRec,
                          fullRec    : TBackupRec);
var
  dirInfo    : TSearchRec;
  aFile      : file;
  numread    : word;
  numwritten : word;
  msgStr     : array [0..255] of char;
  errorMsg   : array [0..255] of char;
  confirmMsg : array [0..255] of char;
  warnMsg    : array [0..255] of char;
  retVal     : integer;
  testVal    : integer;
  oldMode    : Word;
  diskId     : integer;
  code       : integer;
  temp       : TBackupRec;

Begin
  retVal := continueJob;
  oldMode := SetErrorMode(SEM_FAILCRITICALERRORS);
  SR(IDS_CONFIRM, confirmMsg, 255);
  SR(IDS_WARNING, warnMsg, 255);
  SR(IDS_FULLBACKUPCOMPLETE, msgStr, 255);

  InfoMsg(dlg^.hwindow, confirmMsg, msgStr);

  repeat
    FindFirst('A:DISK.001', faAnyFile - faDirectory - faVolumeID, dirInfo);
    if DosError = 0 then
    begin
      {$I-}
      Assign(aFile, 'A:DISK.001');
      testVal := IOResult;
      Reset(aFile, 1);
      BlockRead(aFile, temp, sizeOf(TBackupRec), numread);
      testVal := IOResult;
      if (temp.BType = fullRec.BTYPE) and (temp.BIdent = fullRec.BIdent) then
      begin
        temp.IncrDT  := incrRec.dt;
        temp.numIncr := 1;
        temp.numIncrBytes := incrRec.numBytes;
        Rewrite(aFile, 1);
        if IOResult = 5 then
        begin
          Close(aFile);
          testVal := IOResult;
          InfoMsg(dlg^.hwindow, warnMsg, SR(IDS_DISKWRITEERRORMSG, errorMsg, 255));
          retVal := tryNewDisk;
        end
        else
        begin
          BlockWrite(aFile, temp, sizeOf(TBackupRec), numwritten);
          testVal := IOResult;
          Close(aFile);
          testVal:= IOResult;
          retVal := continueJob;
        end;
      end
      else
      begin
        InfoMsg(dlg^.hwindow, warnMsg, SR(IDS_CATALOGERRORMSG, errorMsg, 255));
        retVal := tryNewDisk
      end;
      {$I+}
    end
    else
    begin
      InfoMsg(dlg^.hwindow, warnMsg, SR(IDS_INVALIDDISK, errorMsg, 255));
      retVal := tryNewDisk;
    end;
  until retVal <> tryNewDisk;

  SetErrorMode(oldMode);
end;


{----------------------[UpdateDisk1]----------------------------------------}
{---------------------------------------------------------------------------}
{                                                                           }
{---------------------------------------------------------------------------}
procedure UpdateDisk1(dlg     : PDlgFMStatus;
                      diskNum : integer;
                      rec     : TBackupRec;
                      msgStr  : PChar);
var
  dirInfo    : TSearchRec;
  aFile      : file;
  numread    : word;
  numwritten : word;
  errorMsg   : array [0..255] of char;
  confirmMsg : array [0..255] of char;
  warnMsg    : array [0..255] of char;
  retVal     : integer;
  testVal    : integer;
  oldMode    : Word;
  diskId     : integer;
  code       : integer;
  temp       : TBackupRec;

Begin
  retVal := continueJob;
  oldMode := SetErrorMode(SEM_FAILCRITICALERRORS);
  SR(IDS_CONFIRM, confirmMsg, 255);
  SR(IDS_WARNING, warnMsg, 255);

  if diskNum <> 1 then
    InfoMsg(dlg^.hwindow, confirmMsg, msgStr);

  repeat
    FindFirst('A:DISK.001', faAnyFile - faDirectory - faVolumeID, dirInfo);
    if DosError = 0 then
    begin
      {$I-}
      Assign(aFile, 'A:DISK.001');
      testVal := IOResult;
      Reset(aFile, 1);
      BlockRead(aFile, temp, sizeOf(TBackupRec), numread);
      testVal := IOResult;
      if (temp.BType = rec.BTYPE) and (temp.BIdent = rec.BIdent) then
      begin
        temp.numDisks := diskNum;
        temp.Status := BackupComplete;
        Rewrite(aFile, 1);
        if IOResult = 5 then
        begin
          Close(aFile);
          testVal := IOResult;
          InfoMsg(dlg^.hwindow, warnMsg, SR(IDS_DISKWRITEERRORMSG, errorMsg, 255));
          retVal := tryNewDisk;
        end
        else
        begin
          BlockWrite(aFile, temp, sizeOf(TBackupRec), numwritten);
          testVal := IOResult;
          Close(aFile);
          testVal:= IOResult;
          retVal := continueJob;
        end;
      end
      else
      begin
        InfoMsg(dlg^.hwindow, warnMsg, SR(IDS_CATALOGERRORMSG, errorMsg, 255));
        retVal := tryNewDisk
      end;
      {$I+}
    end
    else
    begin
      InfoMsg(dlg^.hwindow, warnMsg, SR(IDS_INVALIDDISK, errorMsg, 255));
      retVal := tryNewDisk;
    end;
  until retVal <> tryNewDisk;

  SetErrorMode(oldMode);
end;

{----------------------[TDlgDiskNum]----------------------------------------}
{---------------------------------------------------------------------------}
{                                                                           }
{---------------------------------------------------------------------------}
constructor TDlgDiskNum.Init(AParent: PWindowsObject; diskStr, aTitle : PChar);
begin
  inherited Init(AParent, MakeIntResource(DLG_DISKNUMBER));
  StrCopy(disk,diskStr);
  StrCopy(dlgTitle,aTitle);
end;

destructor TDlgDiskNum.Done;
begin
  inherited Done;
end;

procedure TDlgDiskNum.SetupWindow;
begin
  inherited SetupWindow;
  SendDlgItemMsg(IDC_TEXT1, WM_SETTEXT, 0, longint(@disk));
  SendMessage(HWindow, WM_SETTEXT, 0, longint(@dlgTitle));
end;

procedure TDlgDiskNum.Cancel(var Msg: TMessage);
var
  confirmMsg : array [0..255] of char;
  cancelMsg  : array [0..255] of char;
begin
  SR(IDS_CANCELMSG, cancelMsg, 255);
  SR(IDS_CONFIRM, confirmMsg, 255);
  if  YesNoMsg(hwindow, confirmMsg, cancelMsg) then
    inherited Cancel(Msg);
end;

{----------------------[TDlgOverWrite]----------------------------------------}
{---------------------------------------------------------------------------}
{                                                                           }
{---------------------------------------------------------------------------}
constructor TDlgOverWrite.Init(AParent: PWindowsObject);
begin
  inherited Init(AParent, MakeIntResource(DLG_OVERWRITE));
end;

destructor TDlgOverWrite.Done;
begin
  inherited Done;
end;

procedure TDlgOverWrite.SetupWindow;
begin
  inherited SetupWindow;
end;

{----------------------[TDlgRestoreDisk]------------------------------------}
{---------------------------------------------------------------------------}
{                                                                           }
{---------------------------------------------------------------------------}
constructor TDlgRestoreDisk.Init(AParent: PWindowsObject; diskStr, msgStr, aTitle : PChar);
begin
  inherited Init(AParent, MakeIntResource(DLG_DISKRESTORE));
  StrCopy(disk,diskStr);
  StrCopy(aMsg,msgStr);
  StrCopy(dlgTitle, aTitle);
end;

destructor TDlgRestoreDisk.Done;
begin
  inherited Done;
end;

procedure TDlgRestoreDisk.SetupWindow;
begin
  inherited SetupWindow;
  SendMessage(HWindow, WM_SETTEXT, 0, longint(@dlgTitle));
  SendDlgItemMsg(IDC_TEXT1, WM_SETTEXT, 0, longint(@disk));
  SendDlgItemMsg(IDC_TEXT2, WM_SETTEXT, 0, longint(@aMsg));
end;

procedure TDlgRestoreDisk.Cancel(var Msg: TMessage);
var
  cancelMsg  : array [0..255] of char;
  confirmMsg : array [0..255] of char;
begin
  SR(IDS_CONFIRM, confirmMsg, 255);
  SR(IDS_CANCELMSG, cancelMsg, 255);
  if  YesNoMsg(hwindow, confirmMsg, cancelMsg) then
    inherited Cancel(Msg);
end;


End.
