
unit CvtMpFld;

{- Map field contents used by the convert program (CONVERT.PAS }

INTERFACE

uses
  OWindows,
  CvtMisc;


function  GetMapping(aParent: PWindowsObject; aMaps: PMapCollection; aRow: word): boolean;


IMPLEMENTATION

uses
  ODialogs,
  WinTypes,
  WinProcs,
  DlgLib,
  CtlLib,
  DBTypes,
  DBFile,
  DBLib,
  APITools,
  MScan;

{$I CONVERT.INC}

type
  PCvtMapFldDlg = ^TCvtMapFldDlg;
  TCvtMapFldDlg = object(TCenterDlg)
    constructor Init(aParent: PWindowsObject; aMaps: PMapCollection; aRow: word);
    destructor Done; virtual;
    procedure SetupWindow; virtual;
    function  CanClose: boolean; virtual;
    procedure EnableButtons; virtual;
    procedure FldList(var msg: TMessage); virtual ID_FIRST + IDC_FLD_LIST;
    {}
    private
    maps      : PMapCollection;
    row       : word;
    mapObj    : PMapObj;
    list      : PListBox;
    db        : PDBFile;
  end;


constructor TCvtMapFldDlg.Init(aParent: PWindowsObject; aMaps: PMapCollection; aRow: word);
var
  c   : PControl;
begin
  inherited Init(aParent, MakeIntResource(DLG_MAPFIELD));
  maps := aMaps;
  row := aRow;
  mapObj := maps^.At(row);
  c := New(PLStatic, InitResource(@self, IDC_LBL1, 0, FALSE));
  c := New(PLStatic, InitResource(@self, IDC_LBL2, 0, FALSE));
  list:= New(PListBox, InitResource(@self, IDC_FLD_LIST));
  db := OpenV30File(mapObj^.fileName);
  if db = nil then
  begin
    LogStr('Cannot open file: ');
    LogStr(mapObj^.fileName);
    LogStr('  Error: ');
    LogHex(dbLastOpenError);
    LogLn;
    CriticalError('Could not open version 3 database file.');
    Fail;
  end;
end;

destructor TCvtMapFldDlg.Done;
begin
  MSDisposeObj(db);
  inherited Done;
end;

procedure TCvtMapFldDlg.SetupWindow;
var
  i, j  : integer;
  fi    : TFldInfo;
begin
  inherited SetupWindow;
  SetDlgItemText(hWindow, IDC_FILE, mapObj^.fileName);
  SetDlgItemText(hWindow, IDC_FIELD, mapObj^.fldOldName);
  for i := 1 to db^.dbd^.NumFields do
  begin
    if not db^.dbd^.FieldInfo(i, fi) then
    begin
      LogStr('Cannot get info for field: ');
      LogInt(i);
      LogLn;
      Continue;
    end;
    if (fi.userFlag and db_UD) <> db_UD then
      Continue;
    if fi.fldType <> mapObj^.fldType then
      Continue;
    if ((fi.fldType = dbString) or
        (fi.fldType = dbZString) or
        (fi.fldType = dbCode) or
        (fi.fldType = dbZCode)) and
       (fi.fldSize < mapObj^.size) then
      Continue;
    if maps^.V3FldMapped(mapObj^.fileName, i) >= 0 then
      Continue;

    j := list^.AddString(fi.fldName);
    SendMessage(list^.HWindow, LB_SETITEMDATA, j, i);
  end;
  if list^.GetCount <= 0 then
  begin
    InfoMsg(hWindow, 'Warning', 'There are no user defined fields available to be mapped.');
    EndDlg(IDCANCEL);
  end
  else
    list^.SetSelIndex(0);
end;

function TCvtMapFldDlg.CanClose: boolean;
var
  num        : integer;
  aFld3Name  : array[0..maxFldNameLen] of char; {- name of field }
begin
  CanClose:= FALSE;
  num := list^.GetSelIndex;
  if num = -1 then
    Exit;

  list^.GetSelString(aFld3Name, maxFldNameLen);
  num := SendMessage(list^.HWindow, LB_GETITEMDATA, num, 0);
  maps^.SetV3Fld(row, num, aFld3Name);
  CanClose:= TRUE;
end;

procedure TCvtMapFldDlg.EnableButtons;
begin
  EnableWindow(GetItemHandle(ID_OK), list^.GetSelIndex <> -1);
end;

procedure TCvtMapFldDlg.FldList(var msg: TMessage);
begin
  if msg.lParamHi = LBN_SELCHANGE then
    EnableButtons
  else if msg.lParamHI = LBN_DBLCLK then
  begin
    if CanClose then
      EndDlg(IDOK);
  end
  else
    DefWndProc(msg);
end;



function GetMapping(aParent: PWindowsObject; aMaps: PMapCollection; aRow: word): boolean;
{-   }
begin
  GetMapping :=
    application^.ExecDialog(New(PCvtMapFldDlg, Init(aParent, aMaps, aRow))) = IDOK;
end;


END.

