Unit LogEvent;

Interface

Uses
  WinDos,
  IntGlob,
  WinProcs,
  WinTypes,
	Strings;


const
  lgevStart       = 10;
  lgevClose       = 11;
  lgevRcvPat      = 12;
  lgevRcvSpc      = 13;
  lgevRcvIso      = 14;
  lgevSendPat     = 15;
  lgevSendSpc     = 16;
  lgevSendIso     = 17;
  lgevSendReq     = 18;
  lgevRcvReq      = 19;
  lgevPrintLog    = 20;
  lgevEraseLog    = 21;
  lgevCustomEdit  = 22;
  lgevRcvOrd      = 23;
	lgevRcvColDate  = 24;
  lgevSendOrd     = 25;
	lgevSendColDate = 26;
	lgevRcvField    = 27;
	lgevSentField   = 28;
	lgevEndRcv      = 29;


function LogEvent_GetEventDescription(EventNum: integer): PChar;

Implementation

function LogEvent_GetEventDescription(EventNum: integer): PChar;
var
   LogNum: integer;
   TempMsg: array[0..255] of char;

begin
  if (EventNum >= 10) and (EventNum <= 29) then
  begin
    LogNum := EventNum + str_IntBase + 200 - 10;
		if EventNum > 15 then
			if EventNum = 16 then
        LogNum := str_IntBase + 237
      else
				Dec(LogNum);
    LoadString(HInstance, LogNum, TempMsg, SizeOf(TempMsg));
  end
  else
      if (EventNum >= 102) and (EventNum <= 118) then
      begin
        LogNum := EventNum + str_IntBase + 200 - 83;
        LoadString(HInstance, LogNum, TempMsg, SizeOf(TempMsg));
      end
      else
        StrCopy(TempMsg, '');

  LogEvent_GetEventDescription := TempMsg;
	case EventNum of
		1: LogEvent_GetEventDescription := 'Test 1';
		2: LogEvent_GetEventDescription := 'Test 2';
		3: LogEvent_GetEventDescription := 'Test 3';
		4: LogEvent_GetEventDescription := 'Test 4';
		5: LogEvent_GetEventDescription := 'Test 5';
		6: LogEvent_GetEventDescription := 'Test 6';
		7: LogEvent_GetEventDescription := 'Test 7';
		8: LogEvent_GetEventDescription := 'Test 8';
		9: LogEvent_GetEventDescription := 'Test 9';
  end;


end;


Begin
End.
