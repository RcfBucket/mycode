{----------------------------------------------------------------------------}
{  Module Name  : FMEXTRCT.PAS                                               }
{  Programmer   : DWC                                                        }
{  Date Created : 05/11/95                                                   }
{                                                                            }
{  Purpose -                                                                 }
{  This module allows the user to extract data from the Nihongo databases.   }
{                                                                            }
{  Assumptions -                                                             }
{  None.                                                                     }
{                                                                            }
{  Initialization -                                                          }
{  None.                                                                     }
{                                                                            }
{  Revision History -                                                        }
{                                                                            }
{  Version  Date      Who    Did What                                        }
{  -------  --------  ------ ----------------------------------------------- }
{  1.00     05/11/95  DWC     Initial release.                               }
{                                                                            }
{----------------------------------------------------------------------------}

unit FMEXTRCT;

INTERFACE

uses
  OWindows;

procedure FMExtract(aParent: PWindowsObject);

IMPLEMENTATION

uses
  mscan,
  Objects,
  SPECMPSS,
  fmconst,
  fmtypes,
  fmglobal,
  fmerror,
  ApiTools,
  DBIDS,
  DBTypes,
  DBLib,
  DBFile,
  WinDOS,
  WinTypes,
  userMsgs,
  DMString,
  INILib,
  Strings;

type

PDlgExtract = ^TDlgExtract;
TDlgExtract = object(TDlgFMStatus)
  {}
  procedure   ProcessDlg(var msg: TMessage); virtual WM_FIRST + WM_FMPROCESS;
end;

{--------------------------[NextSeqList]------------------------------------}
{---------------------------------------------------------------------------}
{                                                                           }
{---------------------------------------------------------------------------}
function NextSeqList(aDlg: PDlgFMStatus; collFileName, dbFileName : PChar;
                         fNList : PCollection): integer;
var
  retVal       : integer;
  driveFile    : PDBFile;
  seqRef       : TSeqNum;
  seq          : TSeqNum;
  aFileEntry1  : PFileNameEntry;
  aFileEntry2  : PFileNameEntry;
  aFileEntry3  : PFileNameEntry;
  assocList    : PCollection;
  recMap       : array[0..maxFldsPerRecord] of xref;
  recCnt       : integer;
  j            : integer;
  k            : integer;
  indx         : integer;
  anAssocEntry : PAssocEntry;
  aSeqNumEntry : PSeqNumEntry;

begin
  retVal := continueJob;
  recCnt := 0;

  aDlg^.Update;
  if (StrIComp(collFileName, dbFileName) = 0) then
  begin
    aFileEntry1 := SearchForFile(fNList, collFileName);
    for j := 0 to aFileEntry1^.assocList^.count-1 do
    Begin
      anAssocEntry := aFileEntry1^.assocList^.At(j);
      aFileEntry2 := SearchForFile(fNList, anAssocEntry^.FileName);
      recMap[recCnt].fieldNum := anAssocEntry^.fieldNum;
      recMap[recCnt].seqCollection := aFileEntry2^.seqNumList;
      inc(recCnt);
    end;
  end
  else
  begin
    aFileEntry1 := SearchForFile(fNList, collFileName);
    aFileEntry3 := SearchForFile(fNList, dbFileName);
    for j := 0 to aFileEntry3^.assocList^.count-1 do
    Begin
      anAssocEntry := aFileEntry3^.assocList^.At(j);
      aFileEntry2 := SearchForFile(fNList, anAssocEntry^.FileName);
      recMap[recCnt].fieldNum := anAssocEntry^.fieldNum;
      recMap[recCnt].seqCollection := aFileEntry2^.seqNumList;
      inc(recCnt);
    end;
  end;

  driveFile := New(PDBFile, Init(dbFileName, '', dbOpenNormal));
  if (driveFile = nil) then
  begin
    retVal := fmErr_OpeningFile;
    StrCopy(fmErr_Msg,dbFileName);
  end;

  j := 0;
  while (j <= aFileEntry1^.seqNumList^.count-1) and
        (retVal = continueJob) and aDlg^.CanContinue  do
  Begin
    aDlg^.Update;
    aSeqNumEntry := aFileEntry1^.seqNumList^.At(j);
    driveFile^.dbc^.GetSeq(driveFile^.dbr, aSeqNumEntry^.oldSeqNum);
    for k := 0 to recCnt -1 do
    begin
      seq := 0;
      indx := recMap[k].fieldNum;
      driveFile^.dbr^.GetField(indx, @seq, sizeOf(seq));
      if seq > 0 then
        recMap[k].seqCollection^.Insert(New(PSeqNumEntry, Init(seq)));
    end;
    inc(j);
    aDlg^.Update;
  end;
  MSDisposeObj(driveFile);

  NextSeqList := retval;
end;

{----------------------[NextSeqListWKey]------------------------------------}
{---------------------------------------------------------------------------}
{                                                                           }
{---------------------------------------------------------------------------}
function NextSeqListWKey(adlg: PDlgFMStatus; collFileName, dbFileName : PChar;
                         fNList : PCollection; pKeyField, key  : integer): integer;
var
  retVal       : integer;
  driveFile    : PDBFile;
  seqRef       : TSeqNum;
  seq          : TSeqNum;
  aFileEntry1  : PFileNameEntry;
  aFileEntry2  : PFileNameEntry;
  aFileEntry3  : PFileNameEntry;
  assocList    : PCollection;
  recMap       : array[0..maxFldsPerRecord] of xref;
  recCnt       : integer;
  j            : integer;
  k            : integer;
  indx         : integer;
  anAssocEntry : PAssocEntry;
  aSeqNumEntry : PSeqNumEntry;

begin
  retVal := continueJob;
  recCnt := 0;

  aDlg^.Update;
  aFileEntry1 := SearchForFile(fNList, collFileName);
  aFileEntry3 := SearchForFile(fNList, dbFileName);
  for j := 0 to aFileEntry3^.assocList^.count-1 do
  Begin
    anAssocEntry := aFileEntry3^.assocList^.At(j);
    aFileEntry2 := SearchForFile(fNList, anAssocEntry^.FileName);
    recMap[recCnt].fieldNum := anAssocEntry^.fieldNum;
    recMap[recCnt].seqCollection := aFileEntry2^.seqNumList;
    inc(recCnt);
  end;

  driveFile := New(PDBFile, Init(dbFileName, '', dbOpenNormal));
  if (driveFile = nil) then
  begin
    retVal := fmErr_OpeningFile;
    StrCopy(fmErr_Msg, dbFileName);
  end;

  driveFile^.dbc^.SetCurKeyNum(key);

  j := 0;
  while (j <= aFileEntry1^.seqNumList^.count-1) and
        (retVal = continueJob) and aDlg^.CanContinue  do
  Begin
    aDlg^.Update;
    aSeqNumEntry := aFileEntry1^.seqNumList^.At(j);
    driveFile^.dbr^.ClearRecord;
    driveFile^.dbr^.PutField(pKeyField,@aSeqNumEntry^.oldSeqNum);
    if driveFile^.dbc^.GetFirstContains(driveFile^.dbr) then
      repeat
        aDlg^.Update;
        seq := driveFile^.dbr^.GetSeqValue;
        aFileEntry3^.seqNumList^.Insert(New(PSeqNumEntry, Init(seq)));
        for k := 0 to recCnt -1 do
        begin
          aDlg^.Update;
          seq := 0;
          indx := recMap[k].fieldNum;
          driveFile^.dbr^.GetField(indx, @seq, sizeOf(seq));
          if seq > 0 then
            recMap[k].seqCollection^.Insert(New(PSeqNumEntry, Init(seq)));
        end;
        aDlg^.Update;
      until (not driveFile^.dbc^.GetNextContains(driveFile^.dbr)) or not aDlg^.CanContinue;
    inc(j);
    aDlg^.Update;
  end;
  MSDisposeOBj(driveFile);

  NextSeqListWKey := retval;
end;

{----------------------[BuildSeqNumList]------------------------------------}
{---------------------------------------------------------------------------}
{                                                                           }
{---------------------------------------------------------------------------}
function BuildSeqNumList(aParent: PDlgFMStatus; fNList : PCollection): integer;

var
  retVal       : integer;
  driveFile    : PDBFile;
  mpssObj      : PSpecMPSS;
  seqRef       : TSeqNum;
  seq          : TSeqNum;
  aFileEntry1  : PFileNameEntry;
  aFileEntry2  : PFileNameEntry;
  assocList    : PCollection;
  recMap       : array[0..maxFldsPerRecord] of xref;
  recCnt       : integer;
  j            : integer;
  k            : integer;
  indx         : integer;
  anAssocEntry : PAssocEntry;
  pstr         : array[0..25] of char;

begin
  retVal := continueJob;

  aParent^.Update;
  {- Find Specimen SeqNumber and all other seqNumber in each record -}
  recCnt := 0;
  aFileEntry1 := SearchForFile(fNList, DBSpecFile);
  for j := 0 to aFileEntry1^.assocList^.count-1 do
  Begin
    anAssocEntry := aFileEntry1^.assocList^.At(j);
    aFileEntry2 := SearchForFile(fNList, anAssocEntry^.FileName);
    recMap[recCnt].fieldNum := anAssocEntry^.fieldNum;
    recMap[recCnt].seqCollection := aFileEntry2^.seqNumList;
    inc(recCnt);
  end;

  aParent^.Update;
  driveFile := New(PDBFile, Init(DBSpecFile, '', dbOpenNormal));
  mpssObj := New(PSpecMPSS, Init(aParent, driveFile));
  if (mpssObj <> nil) then
  begin
    if mpssObj^.LoadRules(aParent, SR(IDS_FMEXTRACT, pstr, 25)) then
    begin
      if mpssObj^.GetFirstSpecimen then
        repeat
          aParent^.Update;
          seqRef := driveFile^.dbr^.getSeqValue;
          aFileEntry1^.seqNumList^.Insert(New(PSeqNumEntry, Init(seqRef)));
          for k := 0 to recCnt -1 do
          begin
            seq := 0;
            indx := recMap[k].fieldNum;
            driveFile^.dbr^.GetField(indx, @seq, sizeOf(seq));
            if seq > 0 then
              recMap[k].seqCollection^.Insert(New(PSeqNumEntry, Init(seq)));;
          end;
        until not (mpssObj^.GetNextSpecimen) (*and aParent^.CanContinue) *);
    end
    else
      retVal := cancelJob;
  end
  else
    retVal := fmErr_MPSSFailed;
  MSDisposeObj(mpssObj);
  MSDisposeObj(driveFile);

  {- Specimen with Partial patient key-}
  (*if (retVal = continueJob) and aParent^.CanContinue then*)
  (*  retVal := NextSeqListWKey(aParent, DBPATFile, DBSPECFile, fNList, 3, 3);*)

  {- Isolate with Partial specimen key-}
  if (retVal = continueJob) and aParent^.CanContinue then
    retVal := NextSeqListWKey(aParent, DBSPECFile, DBIsoFile, fNList, 2, 1);

  {- Gram Stain with Partial specimen key-}
  if (retVal = continueJob) and aParent^.CanContinue then
    retVal := NextSeqListWKey(aParent, DBSPECFile, DBGSFile, fNList, 1, 1);

  {- Patient Seq-}
  if (retVal = continueJob) and aParent^.CanContinue then
    retVal := NextSeqList(aParent, DBPATFile, DBPATFile, fNList);

  {- Isorder with Partial isolate key-}
  if (retVal = continueJob) and aParent^.CanContinue then
    retVal := NextSeqListWKey(aParent, DBISOFile, DBISOORDFile, fNList, 1, 1);

  {- Results with Partial isorder key-}
  if (retVal = continueJob) and aParent^.CanContinue then
    retVal := NextSeqListWKey(aParent, DBISOORDFile, DBRESFile, fNList,1, 1);

  {- Order with Partial isolate key-}
  if (retVal = continueJob) and aParent^.CanContinue then
    retVal := NextSeqListWKey(aParent, DBISOFile, DBORDFile, fNList, 1, 1);

  {- Orderxrf with Partial order key-}
  if (retVal = continueJob) and aParent^.CanContinue then
    retVal := NextSeqListWKey(aParent, DBORDFile, DBORDXRFFile, fNList, 1, 1);

  {- tstgpxrf with Partial testgrp key -}
  if (retVal = continueJob) and aParent^.CanContinue then
    retVal := NextSeqListWKey(aParent, DBTSTGRPFile, DBTSTGRPXRFFile, fNList, 1, 1);

  {- test -}
  if (retVal = continueJob) and aParent^.CanContinue then
    retVal := NextSeqList(aParent, DBTSTFile, DBTSTFile, fNList);

  BuildSeqNumList := retval;
end;

{-------------------------[BuildDBFiles]------------------------------------}
{---------------------------------------------------------------------------}
{                                                                           }
{---------------------------------------------------------------------------}
function BuildDBFiles(aDlg: PDlgFMStatus; fNList : PCollection;
                      dbDir, tmpDir: PChar): integer;
var
  retVal     : integer;
  i          : integer;
  j          : integer;
  fEntry     : PFileNameEntry;
  aSeqEntry  : PSeqNumEntry;
  seqList    : PSeqNumCollection;
  fileName   : array[0..maxFileNameLen] of char;
  seqNum     : TSeqNum;
  actDB      : PDBReadWriteFile;
  tmpDB      : PDBCopyFile;
  theCreator : TDBCreator;
  newDBD     : PDBReadWriteDesc;
  tmpDBDesc  : PDBReadWriteDesc;

begin
  retVal := continueJob;

  aDlg^.Update;
  i := 0;
  while (i <= fNList^.count-1) and (retVal = continueJob) and aDlg^.CanContinue do
  begin
    aDlg^.Update;
    fEntry := fNList^.At(i);
    StrCopy(fileName,fEntry^.fileName);

    actDB := New(PDBReadWriteFile, Init(fileName, dbDir, DBOpenNormal));
    if (actDB = nil) then
    begin
      retVal := fmErr_OpeningFile;
      StrCopy(fmErr_Msg,dbDir);
      StrCat(fmErr_Msg,fileName);
    end
    else
    begin
      newDBD:= New(PDBReadWriteDesc, Init(fileName, tmpDir));
      if (newDBD = nil) then
      begin
        retVal := fmErr_CreatingFileDesc;
        StrCopy(fmErr_Msg,tmpDir);
        StrCat(fmErr_Msg,fileName);
      end
      else
      begin
        newDBD^.CopyDesc(actDB^.dbd);
        if not theCreator.CreateTable(newDBD) then
        begin
          retVal := fmErr_CreatingFile;
          StrCopy(fmErr_Msg,tmpDir);
          StrCat(fmErr_Msg,fileName);
        end
        else
        begin
          tmpDB:= New(PDBCopyFile, Init(fileName, tmpDir, DBOpenNormal));
          if (tmpDB = nil) then
          begin
            retVal := fmErr_OpeningFile;
            StrCopy(fmErr_Msg,tmpDir);
            StrCat(fmErr_Msg, fileName);
          end;
        end;
        MSDisposeObj(newDBD);
      end;
    end;

    seqList := fEntry^.seqNumList;
    j := 0;
    while (j <= seqList^.count-1) and (retVal = continueJob) and aDlg^.CanContinue do
    Begin
      aSeqEntry := seqList^.At(j);
      actDB^.dbc^.GetSeq(actDB^.dbr,aSeqEntry^.oldSeqNum);
      tmpDB^.dbr^.CopyRecord(actDB^.dbr);
      tmpDB^.dbc^.InsertRec(tmpDB^.dbr);
      inc(j);
      aDlg^.Update;
    end;

    MSDisposeObj(actDB);
    MSDisposeObj(tmpDB);

    aDlg^.Update;

    inc(i);
  end;

  BuildDBFiles := retVal;
end;

{---------------------------[BuildExtractFiles]-----------------------------}
{---------------------------------------------------------------------------}
{                                                                           }
{---------------------------------------------------------------------------}
function BuildExtractFiles(aDlg: PDlgFMStatus; fNList : PCollection;
                           aDir, anExt : PChar):integer;
var
  filePath    : array[0..maxPathLen] of char;
  fileDir     : array[0..maxPathLen] of char;
  fileName    : array[0..maxFileNameLen] of char;
  fileExt     : array[0..fsExtension] of char;
  dirInfo     : TSearchRec;
  dbFile      : PDBFile;
  fileInfo    : TFileInfo;
  assocInfo   : TAssocInfo;
  fldInfo     : TFldInfo;
  i           : integer;
  assocFName  : array[0..maxFileNameLen] of char;
  retVal      : integer;
  assocList   : PCollection;
  seqNumList  : PSeqNumCollection;

begin
  retVal := continueJob;

  aDlg^.Update;
  StrCopy(filePath, aDir);
  StrCat(filePath, '*');
  StrCat(filePath, anExt);
  FindFirst(filePath, faAnyFile - faDirectory - faVolumeID, dirInfo);
  while ((DosError = 0) and (retVal = continueJob) and (aDlg^.CanContinue))  do
  begin
    FileSplit(dirInfo.Name, fileDir, fileName, fileExt);
    dbFile := New(PDBFile, Init(fileName, '', dbOpenNormal));
    if (dbFile = nil) then
    begin
      retVal := fmErr_OpeningFile;
      StrCopy(fmErr_Msg,fileName);
    end
    else
    begin
      aDlg^.Update;
      dbFile^.dbd^.FileInfo(fileInfo);
      if ((fileInfo.userFlag and db_ExtractFile) = db_ExtractFile) or
         ((fileInfo.userFlag and db_UDFile) = db_UDFile) then
      begin
        assocList := New(PCollection, Init(5,2));
        seqNumList := New(PSeqNumCollection, Init(100,20));
        for i := 1 to dbFile^.dbd^.NumFields do
        begin
          aDlg^.Update;
          dbFile^.dbd^.FieldInfo(i, fldInfo);
          if fldInfo.fldType = dbSeqRef then
          begin
            if fldInfo.fldAssoc > 0 then
            begin
              dbFile^.dbd^.AssocInfo(fldInfo.fldAssoc, assocInfo);
              assocList^.Insert(New(PAssocEntry, Init(i,assocInfo.fname)));
            end
            else
            begin
            {- Error cannot continue -}
            end;
          end;
        end;
        fNList^.Insert(New(PFileNameEntry, Init(fileName,assocList, seqNumList)));
      end;
      FindNext(dirInfo);
      MSDisposeObj(dbFile);
    end;
  end;
  aDlg^.Update;
  BuildExtractFiles := retVal;
end;


{---------------------[TransferExtractFiles]--------------------------------}
{---------------------------------------------------------------------------}
{                                                                           }
{---------------------------------------------------------------------------}
function TransferExtractFiles(dlg      : PDlgFMStatus;
                              fNList   : PCollection;
                              aSrcDir,
                              aDestDir : PChar;
                              var dn   : integer;
                              rec      : TBackupRec ) : integer;
var
  retVal     : integer;
  i          : integer;
  fEntry     : PFileNameEntry;
  fileName   : array[0..maxFileNameLen] of char;
  srcFile    : array[0..255] of char;
  destFile   : array[0..255] of char;
  aTitle     : array[0..255] of char;
  pStr       : array[0..50] of char;

begin
  retVal := continueJob;

  dlg^.SetText(SR(IDS_PERDISK,  pStr, 50),IDC_PB1_TEXT);
  dlg^.SetText(SR(IDS_PERTOTAL, pStr, 50),IDC_PB2_TEXT);
  dlg^.ResetPct(1);
  dlg^.ResetPct(2);
  totalCt := 0;
  totalSpace := rec.numBytes;
  diskSizeCt := 0;

  dlg^.update;
  SR(IDS_FMEXTRACT, aTitle, 255);
  retVal := InsertDisk(dlg,dn,rec, aTitle);
  if (retVal = continueJob) and dlg^.CanContinue  then
  begin
    i := 0;
    while (i <= fNList^.count-1) and (retVal = continueJob) and dlg^.CanContinue do
    begin
      fEntry := fNList^.At(i);
      StrCopy(fileName,fEntry^.fileName);

      StrCopy(srcFile, aSrcDir);
      StrCat(srcFile, fileName);
      StrCat(srcFile, fileDescExt);

      StrCopy(destFile, aDestDir);
      StrCat(destFile, fileName);
      StrCat(destFile, fileDescExt);

      retVal := CopyFile(dlg,srcFile,destFile,dn,rec,true,false,true, aTitle);
      inc(i);
    end;

    i := 0;
    while (i <= fNList^.count-1) and (retVal = continueJob) and dlg^.CanContinue do
    begin
      fEntry := fNList^.At(i);
      StrCopy(fileName,fEntry^.fileName);

      StrCopy(srcFile, aSrcDir);
      StrCat(srcFile, fileName);
      StrCat(srcFile, datFileExt);

      StrCopy(destFile, aDestDir);
      StrCat(destFile, fileName);
      StrCat(destFile, datFileExt);

      retVal := CopyFile(dlg,srcFile,destFile,dn,rec,true,false,true, aTitle);
      inc(i);
    end;
  end;

  TransferExtractFiles := retVal;
end;

{----------------------[TDlgExtract.ProcessDlg]-----------------------------}
{---------------------------------------------------------------------------}
{                                                                           }
{---------------------------------------------------------------------------}
procedure TDlgExtract.ProcessDlg(var msg: TMessage);
var
  retVal       : integer;
  extractRec   : TBackupRec;
  updateMsg    : array [0..255] of char;
  diskNum      : integer;
  estDiskNum   : integer;
  totalBytes   : longint;
  dbDir        : array[0..maxPathLen] of char;
  workDir      : array[0..maxPathLen] of char;
  fileNameList : PCollection;
  aTitle       : array [0..255] of char;
  msgStr       : array [0..255] of char;
  freeBytes    : longint;
  neededBytes  : longint;
  drive        : byte;
  verifyExtract: Boolean;
  vSet         : Boolean;

begin
  verifyExtract := INIVerifyBackup;
  diskSizeCt := 0;
  diskNum := 0;
  estDiskNum := 0;
  retVal := continueJob;
  StrCopy(fmErr_Msg,'');

  Update;
  SetText('    ',0);
  SetText('    ',IDC_TEXT1);
  SetText('    ',IDC_TEXT2);
  SetText('    ',IDC_PB1_TEXT);
  SetText('    ',IDC_PB1);
  SetText('    ',IDC_PB2_TEXT);
  SetText('    ',IDC_PB2);
  SR(IDS_EXTRACTMAINMSG, updateMsg, 255);
  SetText(updateMsg,0);
  SR(IDS_EXTRACTSET, updateMsg, 255);
  SetText(updateMsg,IDC_TEXT1);

  fileNameList := New(PCollection, Init(20, 5));

  if (CanContinue) then
    GetCurrentDir(dbDir, drive);

  if (CanContinue) then
    GetWorkingDir(dbDir, workDir, backupDir);

  freeBytes := DiskFree(drive);
  neededBytes := ComputeTotalBytes(@Self, dbDir, estDiskNum);
  if neededBytes < freeBytes then
  begin
    if (CanContinue) then
    begin
      SR(IDS_EXTRACTBUILD, updateMsg, 255);
      SetText(updateMsg,IDC_TEXT1);
      BuildExtractFiles(@Self, fileNameList, dbDir, fileDescExt);
    end;

    if (retVal = continueJob) and (CanContinue) then
    begin
      Update;
      retVal := BuildSeqNumList(@Self, fileNameList);

      if (retVal = continueJob) and (CanContinue) then
      begin
        CleanDir(workDir,'*.*');

        if (retVal = continueJob) and (CanContinue) then
        begin
          SR(IDS_EXTRACTDATABASE, updateMsg, 255);
          SetText(updateMsg,IDC_TEXT1);
          retVal := BuildDBFiles(@Self, fileNameList, dbDir, workDir);
          totalBytes := ComputeTotalBytes(@Self, workDir, estDiskNum);
          retVal := MakeCatalog(@Self, extractRec, totalBytes, estDiskNum, ExtractCatalog);

          if (retVal = continueJob) and (CanContinue) then
          begin
            SR(IDS_EXTRACTBACKUP, updateMsg, 255);
            SetText(updateMsg,IDC_TEXT1);
            ResetPct(1);
            if VerifyExtract then
            begin
              GetVerify(vSet);
              SetVerify(True);
            end;
            retVal := TransferExtractFiles(@Self, fileNameList, workDir, 'A:',
                     diskNum, ExtractRec);
            if VerifyExtract then
              SetVerify(vSet);
            SetText('    ',IDC_TEXT1);
            SetText(SR(IDS_CLEANDIR, msgStr, 255),IDC_TEXT2);
            SetText('    ',IDC_PB1_TEXT);
            SetText('    ',IDC_PB1);
            SetText('    ',IDC_PB2_TEXT);
            SetText('    ',IDC_PB2);
            CleanDir(workDir,'*.*');
            if  (retVal = continueJob) and canContinue then
            begin
              SetText(SR(IDS_UPDATECATALOG, msgStr, 255),IDC_TEXT2);
              SR(IDS_EXTRACTCOMPLETE, msgStr, 255);
              UpdateDisk1(@Self, diskNum, extractRec, msgStr);
            end;

          end;
        end;
      end;
    end;

    if (retVal <> continueJob) and (retVal <> cancelJob) then
    begin
      SR(IDS_ExtractTITLE, updateMsg, 255);
      FMShowError(@Self, updateMsg, retVal, fmErr_Msg);
    end;
  end
  else
  begin
    {-  insufficient disk space to perform this operation. -}
    SR(IDS_INFORMATION, aTitle, 255);
    SR(IDS_INSUFFDISKSPACE, updateMsg, 255);
    InfoMsg(self.HWindow, aTitle, updateMsg);
  end;

  MSDisposeObj(fileNameList);
  EndDlg(IDCancel);
end;

{----------------------[FMExtract]------------------------------------------}
{---------------------------------------------------------------------------}
{                                                                           }
{---------------------------------------------------------------------------}
procedure FMExtract(aParent: PWindowsObject);
var
  dlgExtract   : PDlgExtract;

begin

  dlgExtract:= New(PDlgExtract, Init(aParent,true));
  application^.ExecDialog(dlgExtract);
end;

End.
