unit EpiFiles;

INTERFACE

const
  DBEPI1File                         = 'EPITMP1';
  DBEPI1Org                          = 1;
  DBEPI1Drug                         = 2;
  DBEPI1MICIdx                       = 3;
  DBEPI1Count                        = 4;

  DBEPI1_ID_KEY                      = 1;

  DBEPI2File                         = 'EPITMP2';
  DBEPI2Org                          = 1;
  DBEPI2Drug                         = 2;
  DBEPI2Count                        = 3;
  DBEPI2GTIdx                        = 4;

  DBEPI2_ID_KEY                      = 1;

  DBEPI3File                         = 'EPITMP3';
  DBEPI3Org                          = 1;
  DBEPI3Count                        = 2;

  DBEPI3_ID_KEY                      = 1;

  DBEPI4File                         = 'EPITMP4';
  DBEPI4Org                          = 1;
  DBEPI4Drug                         = 2;
  DBEPI4Count                        = 3;
  DBEPI4SusCnt                       = 4;
  DBEPI4IntCnt                       = 5;       {SM}
  DBEPI4ResCnt                       = 6;       {SM}

  DBEPI4_ID_KEY                      = 1;

  DBEPI5File                         = 'EPITMP5';
  DBEPI5Item                         = 1;
  DBEPI5Group                        = 2;
  DBEPI5Org                          = 3;
  DBEPI5Count                        = 4;

  DBEPI5_ID_KEY                      = 1;

  DBEPI6File                         = 'EPITMP6';
  DBEPI6Item                         = 1;
  DBEPI6Group                        = 2;
  DBEPI6Count                        = 3;

  DBEPI6_ID_KEY                      = 1;

  DBEPI7File                         = 'EPITMP7';
  DBEPI7Item                         = 1;

  DBEPI7_ID_KEY                      = 1;

function CreateEPITMP1: boolean;
function CreateEPITMP2: boolean;
function CreateEPITMP3: boolean;
function CreateEPITMP4: boolean;
function CreateEPITMP5: boolean;
function CreateEPITMP6: boolean;
function CreateEPITMP7: boolean;

IMPLEMENTATION

uses
  Strings,
  DBLib,
  DBTypes;

function DoAddField(aDB: PDBReadWriteDesc; aName: PChar;
                    aSize: integer; aType: TDBFldTypes): boolean;
var
  fld   : TFldInfo;
begin
  FillChar(fld, SizeOf(fld), 0);
  StrCopy(fld.fldName, aName);
  fld.fldType:= aType;
  fld.userFlag:= db_Hidden;
  fld.fldSize:= aSize;

  DoAddField:= (aDB^.AddField(fld) > 0);
end;

function CreateEPITMP1: boolean;
{- Create the EPI temp file. Return true if successful.
   Note: the existing epi file will be deleted }
var
  db      : PDBReadWriteDesc;
  ret     : boolean;
  code    : integer;
  key     : TKeyInfo;
  keySeg  : TSegmentInfo;
  dbc     : PDBCreator;
begin
  ret:= false;
  db:= New(PDBReadWriteDesc, Init(DBEPI1File, ''));
  if (db <> nil) then
  begin
    code:= DBDeleteDatabase(db);   {- delete existing database }
    if code = 0 then
    begin
      db:= New(PDBReadWriteDesc, Init(DBEPI1File, ''));
      if db <> nil then
      begin
        with db^ do {- set up the schema for the database }
        begin
          SetFileFlag(db_ReadOnly);
          SetFileDesc('EPI Temporary File (1)');
          {- add field info }
          ret:= DoAddField(db, 'Org', 0, dbSeqRef);
          if ret then
            ret:= DoAddField(db, 'Drug', 0, dbSeqRef);
          if ret then
            ret:= DoAddField(db, 'MIC Index', 0, dbInteger);
          if ret then
            ret:= DoAddField(db, 'Count', 0, dbLongint);

          {- add key info }
          if ret then
          begin
            FillChar(key, SizeOf(key), 0);
            StrCopy(key.keyName, 'ID');
            key.unique:= true;
            ret:= AddKey(key) > 0;
            if ret then
            begin
              keySeg.descend:= false;

              keySeg.fldNum:= 1;
              ret:= AddKeySeg(1, keySeg);

              if ret then
              begin
                keySeg.fldNum:= 2;
                ret:= AddKeySeg(1, keySeg);
              end;

              if ret then
              begin
                keySeg.fldNum:= 3;
                ret:= AddKeySeg(1, keySeg);
              end;
            end;
          end;

          {- now create database }
          dbc:= New(PDBCreator, Init);
          ret:= dbc^.CreateTable(db);
          Dispose(dbc, Done);
        end;
        Dispose(db, Done);
      end;
    end;
  end;
  CreateEPITMP1:= ret;
end;

function CreateEPITMP2: boolean;
{- Create the EPI temp file. Return true if successful.
   Note: the existing epi file will be deleted }
var
  db      : PDBReadWriteDesc;
  ret     : boolean;
  code    : integer;
  key     : TKeyInfo;
  keySeg  : TSegmentInfo;
  dbc     : PDBCreator;
begin
  ret:= false;
  db:= New(PDBReadWriteDesc, Init(DBEPI2File, ''));
  if (db <> nil) then
  begin
    code:= DBDeleteDatabase(db);   {- delete existing database }
    if code = 0 then
    begin
      db:= New(PDBReadWriteDesc, Init(DBEPI2File, ''));
      if db <> nil then
      begin
        with db^ do {- set up the schema for the database }
        begin
          SetFileFlag(db_ReadOnly);
          SetFileDesc('EPI Temporary File (2)');
          {- add field info }
          ret:= DoAddField(db, 'Org', 0, dbSeqRef);
          if ret then
            ret:= DoAddField(db, 'Drug', 0, dbSeqRef);
          if ret then
            ret:= DoAddField(db, 'Count', 0, dbLongint);
          if ret then
            ret:= DoAddField(db, 'GT Index', 0, dbInteger);

          {- add key info }
          if ret then
          begin
            FillChar(key, SizeOf(key), 0);
            StrCopy(key.keyName, 'ID');
            key.unique:= true;
            ret:= AddKey(key) > 0;
            if ret then
            begin
              keySeg.descend:= false;

              keySeg.fldNum:= 1;
              ret:= AddKeySeg(1, keySeg);

              if ret then
              begin
                keySeg.fldNum:= 2;
                ret:= AddKeySeg(1, keySeg);
              end;
            end;
          end;

          {- now create database }
          dbc:= New(PDBCreator, Init);
          ret:= dbc^.CreateTable(db);
          Dispose(dbc, Done);
        end;
        Dispose(db, Done);
      end;
    end;
  end;
  CreateEPITMP2:= ret;
end;

function CreateEPITMP3: boolean;
{- Create the EPI temp file. Return true if successful.
   Note: the existing epi file will be deleted }
var
  db      : PDBReadWriteDesc;
  ret     : boolean;
  code    : integer;
  key     : TKeyInfo;
  keySeg  : TSegmentInfo;
  dbc     : PDBCreator;
begin
  ret:= false;
  db:= New(PDBReadWriteDesc, Init(DBEPI3File, ''));
  if (db <> nil) then
  begin
    code:= DBDeleteDatabase(db);   {- delete existing database }
    if code = 0 then
    begin
      db:= New(PDBReadWriteDesc, Init(DBEPI3File, ''));
      if db <> nil then
      begin
        with db^ do {- set up the schema for the database }
        begin
          SetFileFlag(db_ReadOnly);
          SetFileDesc('EPI Temporary File (3)');
          {- add field info }
          ret:= DoAddField(db, 'Org', 0, dbSeqRef);
          if ret then
            ret:= DoAddField(db, 'Count', 0, dbLongint);

          {- add key info }
          if ret then
          begin
            FillChar(key, SizeOf(key), 0);
            StrCopy(key.keyName, 'ID');
            key.unique:= true;
            ret:= AddKey(key) > 0;
            if ret then
            begin
              keySeg.descend:= false;
              keySeg.fldNum:= 1;
              ret:= AddKeySeg(1, keySeg);
            end;
          end;

          {- now create database }
          dbc:= New(PDBCreator, Init);
          ret:= dbc^.CreateTable(db);
          Dispose(dbc, Done);
        end;
        Dispose(db, Done);
      end;
    end;
  end;
  CreateEPITMP3:= ret;
end;

function CreateEPITMP4: boolean;
{- Create the EPI temp file. Return true if successful.
   Note: the existing epi file will be deleted }
var
  db      : PDBReadWriteDesc;
  ret     : boolean;
  code    : integer;
  key     : TKeyInfo;
  keySeg  : TSegmentInfo;
  dbc     : PDBCreator;
begin
  ret:= false;
  db:= New(PDBReadWriteDesc, Init(DBEPI4File, ''));
  if (db <> nil) then
  begin
    code:= DBDeleteDatabase(db);   {- delete existing database }
    if code = 0 then
    begin
      db:= New(PDBReadWriteDesc, Init(DBEPI4File, ''));
      if db <> nil then
      begin
        with db^ do {- set up the schema for the database }
        begin
          SetFileFlag(db_ReadOnly);
          SetFileDesc('EPI Temporary File (4)');
          {- add field info }
          ret:= DoAddField(db, 'Org', 0, dbSeqRef);
          if ret then
            ret:= DoAddField(db, 'Drug', 0, dbSeqRef);
          if ret then
            ret:= DoAddField(db, 'Total Count', 0, dbLongint);
          if ret then
            ret:= DoAddField(db, 'Susceptible Count', 0, dbLongint);
          if ret then
            ret:= DoAddField(db, 'I Count', 0, dbLongint); {SM}
          if ret then
            ret:= DoAddField(db, 'R Count', 0, dbLongint); {SM}

          {- add key info }
          if ret then
          begin
            FillChar(key, SizeOf(key), 0);
            StrCopy(key.keyName, 'ID');
            key.unique:= true;
            ret:= AddKey(key) > 0;
            if ret then
            begin
              keySeg.descend:= false;

              keySeg.fldNum:= 1;
              ret:= AddKeySeg(1, keySeg);

              if ret then
              begin
                keySeg.fldNum:= 2;
                ret:= AddKeySeg(1, keySeg);
              end;
            end;
          end;

          {- now create database }
          dbc:= New(PDBCreator, Init);
          ret:= dbc^.CreateTable(db);
          Dispose(dbc, Done);
        end;
        Dispose(db, Done);
      end;
    end;
  end;
  CreateEPITMP4:= ret;
end;

function CreateEPITMP5: boolean;
{- Create the EPI temp file. Return true if successful.
   Note: the existing epi file will be deleted }
var
  db      : PDBReadWriteDesc;
  ret     : boolean;
  code    : integer;
  key     : TKeyInfo;
  keySeg  : TSegmentInfo;
  dbc     : PDBCreator;
begin
  ret:= false;
  db:= New(PDBReadWriteDesc, Init(DBEPI5File, ''));
  if (db <> nil) then
  begin
    code:= DBDeleteDatabase(db);   {- delete existing database }
    if code = 0 then
    begin
      db:= New(PDBReadWriteDesc, Init(DBEPI5File, ''));
      if db <> nil then
      begin
        with db^ do {- set up the schema for the database }
        begin
          SetFileFlag(db_ReadOnly);
          SetFileDesc('EPI Temporary File (5)');
          {- add field info }
          ret:= DoAddField(db, 'Item', 0, dbSeqRef);
          if ret then
            ret:= DoAddField(db, 'Group', 0, dbInteger);
          if ret then
            ret:= DoAddField(db, 'Org', 0, dbSeqRef);
          if ret then
            ret:= DoAddField(db, 'Count', 0, dbLongint);

          {- add key info }
          if ret then
          begin
            FillChar(key, SizeOf(key), 0);
            StrCopy(key.keyName, 'ID');
            key.unique:= true;
            ret:= AddKey(key) > 0;
            if ret then
            begin
              keySeg.descend:= false;

              keySeg.fldNum:= 1;
              ret:= AddKeySeg(1, keySeg);

              if ret then
              begin
                keySeg.fldNum:= 2;
                ret:= AddKeySeg(1, keySeg);
              end;
              if ret then
              begin
                keySeg.fldNum:= 3;
                ret:= AddKeySeg(1, keySeg);
              end;
            end;
          end;

          {- now create database }
          dbc:= New(PDBCreator, Init);
          ret:= dbc^.CreateTable(db);
          Dispose(dbc, Done);
        end;
        Dispose(db, Done);
      end;
    end;
  end;
  CreateEPITMP5:= ret;
end;

function CreateEPITMP6: boolean;
{- Create the EPI temp file. Return true if successful.
   Note: the existing epi file will be deleted }
var
  db      : PDBReadWriteDesc;
  ret     : boolean;
  code    : integer;
  key     : TKeyInfo;
  keySeg  : TSegmentInfo;
  dbc     : PDBCreator;
begin
  ret:= false;
  db:= New(PDBReadWriteDesc, Init(DBEPI6File, ''));
  if (db <> nil) then
  begin
    code:= DBDeleteDatabase(db);   {- delete existing database }
    if code = 0 then
    begin
      db:= New(PDBReadWriteDesc, Init(DBEPI6File, ''));
      if db <> nil then
      begin
        with db^ do {- set up the schema for the database }
        begin
          SetFileFlag(db_ReadOnly);
          SetFileDesc('EPI Temporary File (6)');
          {- add field info }
          ret:= DoAddField(db, 'Item', 0, dbSeqRef);
          if ret then
            ret:= DoAddField(db, 'Group', 0, dbInteger);
          if ret then
            ret:= DoAddField(db, 'Count', 0, dbLongint);

          {- add key info }
          if ret then
          begin
            FillChar(key, SizeOf(key), 0);
            StrCopy(key.keyName, 'ID');
            key.unique:= true;
            ret:= AddKey(key) > 0;
            if ret then
            begin
              keySeg.descend:= false;

              keySeg.fldNum:= 1;
              ret:= AddKeySeg(1, keySeg);

              if ret then
              begin
                keySeg.fldNum:= 2;
                ret:= AddKeySeg(1, keySeg);
              end;
            end;
          end;

          {- now create database }
          dbc:= New(PDBCreator, Init);
          ret:= dbc^.CreateTable(db);
          Dispose(dbc, Done);
        end;
        Dispose(db, Done);
      end;
    end;
  end;
  CreateEPITMP6:= ret;
end;

function CreateEPITMP7: boolean;
{- Create the EPI temp file. Return true if successful.
   Note: the existing epi file will be deleted }
var
  db      : PDBReadWriteDesc;
  ret     : boolean;
  code    : integer;
  key     : TKeyInfo;
  keySeg  : TSegmentInfo;
  dbc     : PDBCreator;
begin
  ret:= false;
  db:= New(PDBReadWriteDesc, Init(DBEPI7File, ''));
  if (db <> nil) then
  begin
    code:= DBDeleteDatabase(db);   {- delete existing database }
    if code = 0 then
    begin
      db:= New(PDBReadWriteDesc, Init(DBEPI7File, ''));
      if db <> nil then
      begin
        with db^ do {- set up the schema for the database }
        begin
          SetFileFlag(db_ReadOnly);
          SetFileDesc('EPI Temporary File (7)');
          SetAutoSeq(true);
          {- add field info }
          ret:= DoAddField(db, 'Item', 33, dbZString);

          {- add key info }
          if ret then
          begin
            FillChar(key, SizeOf(key), 0);
            StrCopy(key.keyName, 'ID');
            key.unique:= true;
            ret:= AddKey(key) > 0;
            if ret then
            begin
              keySeg.descend:= false;

              keySeg.fldNum:= 1;
              ret:= AddKeySeg(1, keySeg);
            end;
          end;

          {- now create database }
          dbc:= New(PDBCreator, Init);
          ret:= dbc^.CreateTable(db);
          Dispose(dbc, Done);
        end;
        Dispose(db, Done);
      end;
    end;
  end;
  CreateEPITMP7:= ret;
end;

END.

{- rcf}
