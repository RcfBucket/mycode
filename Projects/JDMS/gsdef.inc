{***
  GSDEF lib resource ids
  Module ID = $0400 = 1024
****}
const

  DLG_GSDEF         = 1024;
  DLG_GSEDIT        = 1025;

  IDC_GRID          = 201;
  IDC_ABBREV        = 202;
  IDC_NAME          = 203;
  IDC_FREETEXT      = 204;
	IDC_EDIT	=	105;
  IDC_STANDARD      = 206;

  IDC_ADD           = 100;
  IDC_DELETE        = 101;
  IDC_UP            = 102;
  IDC_DOWN          = 103;
  IDC_DEFINE        = 104;

  IDS_ABBREV        = 1024;
  IDS_ADDMODE       = 1025;
  IDS_CANTDEL       = 1026;
  IDS_CANTINS       = 1027;
  IDS_CANTMOD       = 1028;
  IDS_CANTOPENGS    = 1029;
  IDS_CANTOPENRES   = 1030;
  IDS_CONFIRM       = 1031;
  IDS_CONFIRMDEL    = 1032;
  IDS_CONFIRMPURGE  = 1033;
  IDS_CONFIRMSAVE   = 1034;
  IDS_EDITMODE      = 1035;
  IDS_ERROR         = 1036;
  IDS_FATAL         = 1037;
  IDS_FREETEXT      = 1038;
  IDS_INFO          = 1039;
  IDS_MAXSLOTS      = 1040;
  IDS_NAME          = 1041;
  IDS_NORES         = 1042;
  IDS_NOSEL         = 1043;
  IDS_REQUIRED      = 1044;
  IDS_RESTYPE       = 1045;
  IDS_STANDARD      = 1046;
	IDC_LBLABBR	=	300;
	IDC_LBLNAME	=	301;
	IDS_CANTUPDATESLOT	=	1047;
	IDS_ROWALLOCERR	=	1048;
	IDC_MODE	=	310;
	IDS_CANTALLOCTEMP	=	1049;
