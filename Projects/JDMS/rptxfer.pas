unit RptXFer;

INTERFACE

uses
  DBTypes;

procedure ExportReport(rptSeq: TSeqNum; outFilName: PChar);
procedure ImportReport(inFilName: PChar);
procedure DeleteReport(rptSeq: TSeqNum);

IMPLEMENTATION

uses
  RptShare,
  DBIDs,
  MScan,
  Strings,
  StrsW,
  DBFile,
  ApiTools,
  DBLib;

var
  pw    : PWaitCursor;

function OpenDB(name: PChar; isFatal: boolean): PDBFile;
var
  pstr  : array[0..100] of char;
  db    : PDBFile;
begin
  db:= New(PDBFile, Init(name, '', dbOpenNormal));
  if db = nil then
  begin
    StrCopy(pstr, 'Cannot open file : ');
    StrCat(pstr, name);
    FatalError('Error', pstr);
    if isFatal then
      halt;
  end;
  OpenDB:= db;
end;

function GetSubField(fld: integer; dest, src: PChar; maxlen: integer): PChar;
var
  k   : integer;
  pc  : PChar;
begin
  GetSubField:= dest;
  StrCopy(dest, '');
  if fld <= 0 then exit;
  k:= 1;
  pc:= src;
  while (pc <> nil) and (k < fld) do
  begin
    pc:= StrScan(pc, '^');
    if pc <> nil then
      pc:= (pc + 1);
    Inc(k);
  end;
  if pc <> nil then
  begin
    StrLCopy(dest, pc, maxLen);
    pc:= StrScan(dest, '^');
    if pc <> nil then
      pc^:= #0;
  end;
end;

procedure ExportReport(rptSeq: TSeqNum; outFilName: PChar);
var
  k       : integer;
  fil     : Text;
  db      : PDBFile;
  xref    : PDBFile;
  sectID  : integer;
  sectSeq : TSeqNum;

  procedure DumpIt(rec: PDBRec);
  var
    dbd   : PDBReadDesc;
    fi    : Text;
    pstr  : PChar;
    size  : integer;
    k     : integer;
  begin
    dbd:= rec^.Desc;
    for k:= 1 to dbd^.NumFields do
    begin
      size:= dbd^.FieldSizeForStr(k);
      GetMem(pstr, size);
      db^.dbr^.GetFieldAsStr(k, pstr, size-1);
      write(fil, pstr);
      if k < dbd^.NumFields then
        write(fil, '^');
    end;
    writeln(fil);
  end;

  procedure DumpDemo(aSeq: TSeqNum);
  begin
    db:= OpenDB(DBRPTDemoSectsFile, true);
    if db^.dbc^.GetSeq(db^.dbr, aSeq) then
    begin
      write(fil, '-', SectDemoFreeID, '^Demographics^');
      DumpIt(db^.dbr);
      MSDisposeObj(db);

      db:= OpenDB(DBRPTDemoFldsFile, true);
      db^.dbr^.PutField(DBRptDemoFldsSectRef, @aSeq);
      if db^.dbc^.GetFirstContains(db^.dbr) then
      begin
        repeat
          write(fil, '   ');
          DumpIt(db^.dbr);
        until not db^.dbc^.GetNextContains(db^.dbr);
      end;
      MSDisposeObj(db);
    end
    else
      MSDisposeObj(db);
  end;

  procedure DumpGS(aSeq: TSeqNum);
  begin
    db:= OpenDB(DBRPTGSSectsFile, true);
    if db^.dbc^.GetSeq(db^.dbr, aSeq) then
    begin
      write(fil, '-', SectGSID, '^Gram Stain^');
      DumpIt(db^.dbr);
      MSDisposeObj(db);
      db:= OpenDB(DBRPTGSFldsFile, true);
      db^.dbr^.PutField(DBRptGSFldsSectRef, @aSeq);
      if db^.dbc^.GetFirstContains(db^.dbr) then
      begin
        repeat
          write(fil, '   ');
          DumpIt(db^.dbr);
        until not db^.dbc^.GetNextContains(db^.dbr);
      end;
      MSDisposeObj(db);
    end
    else
      MSDisposeObj(db);
  end;

  procedure DumpIsoSum(aSeq: TSeqNum);
  begin
    db:= OpenDB(DBRptIsoSumFile, true);
    if db^.dbc^.GetSeq(db^.dbr, aSeq) then
    begin
      write(fil, '-', SectIsoSumID, '^Isolate Summary^');
      DumpIt(db^.dbr);
    end;
    MSDisposeObj(db);
  end;

  procedure DumpIsoComp(aSeq: TSeqNum);
  begin
    db:= OpenDB(DBRptIsoCmpFile, true);
    if db^.dbc^.GetSeq(db^.dbr, aSeq) then
    begin
      write(fil, '-', SectIsoCompID, '^Isolate Compressed^');
      DumpIt(db^.dbr);
    end;
    MSDisposeObj(db);
  end;

  procedure DumpIsoFull(aSeq: TSeqNum);
  begin
    db:= OpenDB(DBRptIsoFulFile, true);
    if db^.dbc^.GetSeq(db^.dbr, aSeq) then
    begin
      write(fil, '-', SectIsoFullID, '^Isolate Full^');
      DumpIt(db^.dbr);
    end;
    MSDisposeObj(db);
  end;

begin
  pw:= New(PWaitCursor, Init);
  Assign(fil, outFilName);
  Rewrite(fil);

  db:= OpenDB(DBRPTFmtsFile, true);
  if db = nil then exit;
  if db^.dbc^.GetSeq(db^.dbr, rptSeq) then
  begin
    write(fil, '>');
    DumpIt(db^.dbr);
  end;
  MSDisposeObj(db);

  {- go through the cross reference file and print the records for each
      of the sections in the report format }
  xref:= New(PDBFile, Init(DBRPTSectsFile, '', dbOpenNormal));
  xref^.dbr^.PutField(DBRptSectReportRef, @rptSeq);
  if xref^.dbc^.GetFirstContains(xref^.dbr) then
  begin
    repeat
      xref^.dbr^.GetField(DBRptSectSectID, @sectID, sizeof(sectID));
      xref^.dbr^.GetField(DBRptSectSectRef, @sectSeq, sizeof(sectSeq));
      case sectID of
        SectDemoFreeID:
          DumpDemo(sectSeq);
        SectIsoSumID:
          DumpIsoSum(sectSeq);
        SectIsoCompID:
          DumpIsoComp(sectSeq);
        SectGSID:
          DumpGS(sectSeq);
        SectIsoFullID:
          DumpIsoFull(sectSeq);
      end;
    until not xref^.dbc^.GetNextContains(xref^.dbr);
  end;
  MSDisposeObj(xref);

  Close(fil);
  MSDisposeObj(pw);
  pw:= nil;
end;

procedure ImportReport(inFilName: PChar);
var
  txtLin, buf : PChar;
  fil         : Text;
  rptSeq      : TSeqNum;
  db          : PDBFile;
  bufSize     : integer;
  okToRead    : boolean;
  sectID      : integer;
  code        : integer;

  procedure XFerFld(rec: PDBRec; fld, sFld: integer);
  begin
    GetSubField(sFld, buf, txtLin, bufSize-1);
    rec^.PutFieldAsStr(fld, buf);
  end;

  procedure StartIt;
  var
    k   : integer;
  begin
    {- at this point, the [p1] contains the data for the report format record }
    {- Add the report record to the database and collect the new seq number }
    rptSeq:= 0;
    db:= OpenDB(DBRPTFmtsFile, true);
    StripRange(txtLin, txtLin, bufSize-1, '>', '>');
    for k:= 1 to db^.dbd^.NumFields do
      XFerFld(db^.dbr, k, k);
    if not db^.dbc^.InsertRec(db^.dbr) then
      FatalError('Error', 'Cannot insert report format record')
    else
      rptSeq:= db^.dbr^.GetSeqValue;
    MSDisposeObj(db);
  end;

  procedure GetLine;
  var
    ch    : char;
    k     : integer;
  begin
    k:= 0;
    while not Eoln(fil) do
    begin
      Read(fil, ch);
      (txtLin + k)^:= ch;
      Inc(k);
    end;
    (txtLin + k)^:= #0;
    Read(fil, ch, ch);  {- clear EOL marker }
  end;

  procedure AddToXRef(rpt, sSeq: TSeqNum; sID: integer);
  var
    xref  : PDBFile;
  begin
    xref:= OpenDB(DBRPTSectsFile, true);
    xref^.dbr^.PutField(DBRptSectReportRef, @rpt);
    xref^.dbr^.PutField(DBRptSectSectRef, @sSeq);
    xref^.dbr^.PutField(DBRptSectSectID, @sID);
    if not xref^.dbc^.InsertRec(xref^.dbr) then
    begin
      FatalError('Error', 'Cannot insert xref record');
      halt;
    end;
    MSDisposeObj(xref);
  end;

  procedure DoDemographics;
  var
    flds    : PDBFile;
    sectSeq : TSeqNum;
    k       : integer;
    allDone : boolean;
  begin
    {- the demographic section record is in txtLin at this point }
    db:= OpenDB(DBRPTDemoSectsFile, true);
    flds:= OpenDB(DBRPTDemoFldsFile, true);
    for k:= 1 to db^.dbd^.NumFields do
      XFerFld(db^.dbr, k, k+2);
    if not db^.dbc^.InsertRec(db^.dbr) then    {- add section record }
    begin
      FatalError('Error', 'Cannot insert demographic section record');
      halt;
    end;
    sectSeq:= db^.dbr^.GetSeqValue;

    {- now read all demographic section fields and insert them }
    allDone:= false;
    repeat
      GetLine;
      if (strlen(txtLin) > 0) then
      begin
        if (txtLin^ = ' ') then
        begin
          flds^.dbr^.PutField(DBRptDemoFldsSectRef, @sectSeq);
          for k:= 2 to flds^.dbd^.NumFields do
            XFerFld(flds^.dbr, k, k);
          if not flds^.dbc^.InsertRec(flds^.dbr) then
          begin
            FatalError('Error', 'Cannot insert demo field record');
            halt;
          end;
        end
        else
          allDone:= true;
      end;
    until Eof(fil) or allDone;

    {- tell main loop not to read line next pass through main loop}
    okToRead:= not allDone;

    AddToXRef(rptSeq, sectSeq, sectID);
    MSDisposeObj(flds);
    MSDisposeObj(db);
  end;

  procedure DoGS;
  var
    flds    : PDBFile;
    sectSeq : TSeqNum;
    k       : integer;
    allDone : boolean;
  begin
    {- the gs section record is in txtLin at this point }
    db:= OpenDB(DBRPTGSSectsFile, true);
    flds:= OpenDB(DBRPTGSFldsFile, true);
    for k:= 1 to db^.dbd^.NumFields do
      XFerFld(db^.dbr, k, k+2);
    if not db^.dbc^.InsertRec(db^.dbr) then    {- add section record }
    begin
      FatalError('Error', 'Cannot insert gram stain section record');
      halt;
    end;
    sectSeq:= db^.dbr^.GetSeqValue;

    {- now read all gs section fields and insert them }
    allDone:= false;
    repeat
      GetLine;
      if (strlen(txtLin) > 0) then
      begin
        if (txtLin^ = ' ') then
        begin
          flds^.dbr^.PutField(DBRptGSFldsSectRef, @sectSeq);
          for k:= 2 to flds^.dbd^.NumFields do
            XFerFld(flds^.dbr, k, k);
          if not flds^.dbc^.InsertRec(flds^.dbr) then
          begin
            FatalError('Error', 'Cannot insert gs field record');
            halt;
          end;
        end
        else
          allDone:= true;
      end;
    until Eof(fil) or allDone;

    {- tell main loop not to read line next pass through main loop}
    okToRead:= not allDone;

    AddToXRef(rptSeq, sectSeq, sectID);
    MSDisposeObj(flds);
    MSDisposeObj(db);
  end;

  procedure DoIsoSum;
  var
    sectSeq : TSeqNum;
    k       : integer;
  begin
    db:= OpenDB(DBRptIsoSumFile, true);
    for k:= 1 to db^.dbd^.NumFields do
      XFerFld(db^.dbr, k, k+2);
    if not db^.dbc^.InsertRec(db^.dbr) then    {- add section record }
    begin
      FatalError('Error', 'Cannot insert isolate summary section record');
      halt;
    end;
    sectSeq:= db^.dbr^.GetSeqValue;
    AddToXRef(rptSeq, sectSeq, sectID);
    MSDisposeObj(db);
  end;

  procedure DoIsoComp;
  var
    sectSeq : TSeqNum;
    k       : integer;
  begin
    db:= OpenDB(DBRptIsoCmpFile, true);
    for k:= 1 to db^.dbd^.NumFields do
      XFerFld(db^.dbr, k, k+2);
    if not db^.dbc^.InsertRec(db^.dbr) then    {- add section record }
    begin
      FatalError('Error', 'Cannot insert isolate compressed section record');
      halt;
    end;
    sectSeq:= db^.dbr^.GetSeqValue;
    AddToXRef(rptSeq, sectSeq, sectID);
    MSDisposeObj(db);
  end;

  procedure DoIsoFull;
  var
    sectSeq : TSeqNum;
    k       : integer;
  begin
    db:= OpenDB(DBRptIsoFulFile, true);
    for k:= 1 to db^.dbd^.NumFields do
      XFerFld(db^.dbr, k, k+2);
    if not db^.dbc^.InsertRec(db^.dbr) then    {- add section record }
    begin
      FatalError('Error', 'Cannot insert isolate full section record');
      halt;
    end;
    sectSeq:= db^.dbr^.GetSeqValue;
    AddToXRef(rptSeq, sectSeq, sectID);
    MSDisposeObj(db);
  end;

begin
  pw:= New(PWaitCursor, Init);
  Assign(fil, inFilName);
  Reset(fil);
  if IoResult <> 0 then
  begin
    FatalError('Error', 'Cannot open input file');
    Halt;
  end;
  bufSize:= 1025;
  GetMem(buf, bufSize);
  GetMem(txtLin, bufSize);

  rptSeq:= 0;
  okToRead:= true;
  while not Eof(fil) do
  begin
    if okToRead then
      GetLine;
    okToRead:= true;
    if StrLen(txtLin) > 0 then
    begin
      if txtLin^ = '>' then   {- start of report description }
        StartIt
      else if rptSeq > 0 then {- if report started }
      begin
        if txtLin^ = '-' then
        begin
          GetSubField(1, buf, txtLin, bufSize-1);
          Val(buf, sectID, code);
          sectID:= Abs(sectID);
          case sectID of
            SectDemoFreeID:
              DoDemographics;
            SectIsoSumID:
              DoIsoSum;
            SectIsoCompID:
              DoIsoComp;
            SectGSID:
              DoGS;
            SectIsoFullID:
              DoIsoFull;
          end;
        end;
      end;
    end;
  end;
  MSFreeMem(buf, bufSize);
  MSFreeMem(txtLin, bufSize);
  MSDisposeObj(pw);
  pw:= nil;
end;

procedure DeleteReport(rptSeq: TSeqNum);
var
  db      : PDBFile;
  xref    : PDBFile;
  sectID  : integer;
  sectSeq : TSeqNum;
begin
  pw:= New(PWaitCursor, Init);

  db:= OpenDB(DBRPTFmtsFile, true);
  if db = nil then exit;
  if db^.dbc^.GetSeq(db^.dbr, rptSeq) then
  begin
    db^.dbc^.DeleteRec(db^.dbr);
    MSDisposeObj(db);

    xref:= OpenDB(DBRPTSectsFile, true);
    xref^.dbr^.PutField(DBRptSectReportRef, @rptSeq);
    if xref^.dbc^.GetFirstContains(xref^.dbr) then
    begin
      repeat
        xref^.dbr^.GetField(DBRptSectSectRef, @sectSeq, sizeof(sectSeq));
        xref^.dbr^.GetField(DBRptSectSectID, @sectID, sizeof(sectID));
        case sectID of
          SectDemoFreeID:
            begin
              db:= OpenDB(DBRPTDemoSectsFile, true);
              if db^.dbc^.GetSeq(db^.dbr, sectSeq) then
                db^.dbc^.DeleteRec(db^.dbr);
              MSDisposeObj(db);
              db:= OpenDB(DBRPTDemoFldsFile, true);
              db^.dbr^.PutField(DBRptDemoFldsSectRef, @sectSeq);
              if db^.dbc^.GetFirstContains(db^.dbr) then
              begin
                repeat
                  db^.dbc^.DeleteRec(db^.dbr);
                until not db^.dbc^.GetNextContains(db^.dbr);
              end;
              MSDisposeObj(db);
            end;
          SectIsoSumID:
            begin
              db:= OpenDB(DBRptIsoSumFile, true);
              if db^.dbc^.GetSeq(db^.dbr, sectSeq) then
                db^.dbc^.DeleteRec(db^.dbr);
              MSDisposeObj(db);
            end;
          SectIsoCompID:
            begin
              db:= OpenDB(DBRptIsoCmpFile, true);
              if db^.dbc^.GetSeq(db^.dbr, sectSeq) then
                db^.dbc^.DeleteRec(db^.dbr);
              MSDisposeObj(db);
            end;
          SectGSID:
            begin
              db:= OpenDB(DBRPTGSSectsFile, true);
              if db^.dbc^.GetSeq(db^.dbr, sectSeq) then
                db^.dbc^.DeleteRec(db^.dbr);
              MSDisposeObj(db);
              db:= OpenDB(DBRPTGSFldsFile, true);
              db^.dbr^.PutField(DBRptGSFldsSectRef, @sectSeq);
              if db^.dbc^.GetFirstContains(db^.dbr) then
              begin
                repeat
                  db^.dbc^.DeleteRec(db^.dbr);
                until not db^.dbc^.GetNextContains(db^.dbr);
              end;
              MSDisposeObj(db);
            end;
          SectIsoFullID:
            begin
              db:= OpenDB(DBRptIsoFulFile, true);
              if db^.dbc^.GetSeq(db^.dbr, sectSeq) then
                db^.dbc^.DeleteRec(db^.dbr);
              MSDisposeObj(db);
            end;
        end;  {- case }
        xref^.dbc^.DeleteRec(xref^.dbr);
      until not xref^.dbc^.GetNextContains(xref^.dbr);
    end;
    MSDisposeObj(xref);
  end;

  MSDisposeObj(pw);
  pw:= nil;
end;

var
  exitSave  : pointer;

procedure ExitRoutine; far;
begin
  MSDisposeObj(pw);
  exitProc:= exitSave;
end;

BEGIN
  pw:= nil;
  exitSave:= exitProc;
  exitProc:= @ExitRoutine;
END.

{- rcf }
