unit GSDef;

{- Gram Stain definition module.

  This module provides a dialog object that can be created to modify the gram
  stain defintion.  The object assumes that the gram stain definition file as
  well as the gram stain result file are in the current directory.
}
INTERFACE

uses
  OWindows;

const
  MaxGSAbbrLen    = 12;  {- values from DBMake.TXT for gram stain defintion }
  MaxGSNameLen    = 40;
  MaxGSFreeText   = 60;

  MaxGSSlots      = 12;  {- maximum number of slots in the definition }

  GSFreeText        = 'F'; {- grid result type flags }
  GSStandard        = 'S';

procedure DefineGSDef(aParent: PWindowsObject);

{$R GSDEF.RES}

IMPLEMENTATION

uses
  DMSDebug,
  DMSErr,
  MScan,
  ListLib,
  Ctl3d,
  StrsW,
  DBLib,
  DBErrors,
  DBTypes,
  DBFile,
  CtlLib,
  ApiTools,
  UIFlds,
  dbids,
  GridLib,
  DlgLib,
  Strings,
  WinProcs,
  WinTypes,
  Win31,
  Objects,
  ODialogs;

{$I GSDEF.INC}

type
  {- slot defintion record - useful only to the GSDef object for the interface}
  PSlotDef = ^TSlotDef;
  TSlotDef = record
    abbr    : array[0..MaxGSAbbrLen] of char;
    name    : array[0..MaxGSNameLen] of char;
    resType : char;
    seq     : TSeqNum;
  end;

  {- Gram Stain Definition Dialog Box }
  PGSDef = ^TGSDef;
  TGSDef = object(TCenterDlg)
    constructor Init(aParent: PWindowsObject);
    destructor Done; virtual;
    procedure SetupWindow; virtual;
    procedure WMDrawItem(var msg: TMessage); virtual WM_FIRST + WM_DRAWITEM;
    procedure WMMeasureItem(var msg: Tmessage); virtual WM_FIRST + WM_MEASUREITEM;
    procedure WMCharToItem(var msg: Tmessage); virtual WM_FIRST + WM_CHARTOITEM;
    procedure IDAdd(var msg: TMessage); virtual ID_FIRST + IDC_ADD;
    procedure IDEdit(var msg: TMessage); virtual ID_FIRST + IDC_EDIT;
    procedure IDDelete(var msg: TMessage); virtual ID_FIRST + IDC_DELETE;
    procedure IDDefineVals(var msg: TMessage); virtual ID_FIRST + IDC_DEFINE;
    procedure IDUp(var msg: TMessage); virtual ID_FIRST + IDC_UP;
    procedure IDDown(var msg: TMessage); virtual ID_FIRST + IDC_DOWN;
    procedure IDGrid(var msg: TMessage); virtual ID_FIRST + IDC_GRID;
    function CanClose: boolean; virtual;
    procedure Cancel(var msg: TMessage); virtual ID_FIRST + ID_CANCEL;
    procedure EnableButtons; virtual;
    private
    grid    : PGrid;
    slot    : array[0..MaxGSSlots - 1] of TSlotDef;
    db      : PDBFile;
    curs    : HCursor;
    procedure CheckUpDown;
    procedure SetRowData(aSlot: TSlotDef; aRow: integer);
    function SlotInUse(aSlotRef: TSeqNum): boolean;
  end;

const
  Grid_Abbr         = 0;   {- grid column numbers }
  Grid_Name         = 1;
  Grid_Type         = 2;
  Grid_Seq          = 3;

type
  PGSEdit = ^TGSEdit;
  TGSEdit = object(TCenterDlg)
    constructor Init(aParent: PWindowsObject; adb: PDBFile; var aSlotDef: TSlotDef; editMode: boolean);
    destructor Done; virtual;
    procedure SetupWindow; virtual;
    function CanClose: boolean; virtual;
    procedure WMCtlColor(var msg: TMessage); virtual WM_FIRST + WM_CTLCOLOR;
    private
    slot    : PSlotDef;
    db      : PDBFile;
    abr     : PUIFld;
    name    : PUIFld;
    trec    : PDBRec;
    edMode  : boolean;  {- true = edit mode, false = add mode }
  end;

constructor TGSDef.Init(aParent: PWindowsObject);
var
  k       : integer;
  pw      : TWaitCursor;
begin
  pw.Init;
  db:= nil;

  {- clear internal slot list }
  FillChar(slot, sizeof(slot), 0);
  for k:= 0 to MaxGSSlots - 1 do
    slot[k].resType:= GSStandard;

  {- open the GS definition file }
  db:= New(PDBFile, Init(DBGSDefFile, '', DBOpenExclusive));
  if (db = nil) then
  begin
    ShowError(nil, IDS_CANTOPENGS, nil, dbLastOpenError, MOD_GSDEF, 0);
    pw.Done;
    Fail;
  end;

  inherited Init(aParent, MakeIntResource(DLG_GSDEF));
  grid:= New(PGrid, InitResource(@self, IDC_GRID, 4, false));
  pw.Done;
end;

destructor TGSDef.Done;
begin
  inherited Done;
  if db <> nil then
    Dispose(db, Done);
end;

procedure TGSDef.SetupWindow;
var
  dbOK  : boolean;
  p1,p2 : array[0..100] of char;
  idx   : integer;
  k     : integer;

begin
  inherited SetupWindow;

  {- setup grid }
  grid^.SetHeader(Grid_Abbr, SR(IDS_ABBREV, p1, 100));
  grid^.SetHeader(Grid_Name, SR(IDS_NAME, p1, 100));
  grid^.SetHeader(Grid_Type, SR(IDS_RESTYPE, p1, 100));

  k:= StrLen(SR(IDS_FREETEXT, p1, sizeof(p1)-1));
  if k < StrLen(SR(IDS_STANDARD, p1, sizeof(p1)-1)) then
    k:= StrLen(SR(IDS_STANDARD, p1, sizeof(p1)-1));
  grid^.SetAvgCharWidth(Grid_Abbr, db^.dbd^.FieldSizeForStr(DBGSDefAbbr));
  grid^.SetAvgCharWidth(Grid_Type, k);
  grid^.StretchColumn(grid_Name);

  {- read the definition file and initialize internal slot list }
  with db^ do
  begin
    dbOK:= dbc^.GetFirst(dbr);
    idx:= 0;
    while dbOK do
    begin
      dbr^.GetField(DBGSDefAbbr, @slot[idx].abbr, sizeof(slot[idx].abbr) - 1);
      dbr^.GetField(DBGSDefDesc, @slot[idx].name, sizeof(slot[idx].name) - 1);
      dbr^.GetField(DBGSDefResType, @slot[idx].resType, 1);
      slot[idx].seq:= dbr^.GetSeqValue;

      {- add to grid }
      k:= grid^.AddString('');
      if k < 0 then
      begin
        ShowError(nil, IDS_ROWALLOCERR, nil, 0, MOD_GSDEF, 0);
        halt;
      end;
      SetRowData(slot[idx], k);

      if SlotInUse(dbr^.GetSeqValue) then
        grid^.EnableRow(idx, false);
      Inc(idx);
      dbOK:= dbc^.GetNext(dbr);
    end;
  end;
  grid^.SetSelIndex(0);
  EnableButtons;  {- enable/disable buttons }
  SetCursor(curs);
end;

procedure TGSDef.WMDrawItem(Var msg: TMessage);
begin
  grid^.DrawItem(msg);
end;

procedure TGSDef.WMMeasureItem(var msg: Tmessage);
begin
  grid^.MeasureItem(msg);
end;

procedure TGSDef.WMCharToItem(var msg: Tmessage);
begin
  grid^.CharToItem(msg);
end;

procedure TGSDef.CheckUpDown;
{- check the up/down buttons. Disable or enable as necessary }
begin
  if grid^.GetRowCount > 0 then
  begin
    EnableWindow(GetItemHandle(IDC_UP), grid^.GetSelIndex > 0);
    EnableWindow(GetItemHandle(IDC_DOWN), grid^.GetSelIndex < grid^.GetRowCount - 1);
  end
  else
  begin
    EnableWindow(GetItemHandle(IDC_UP), false);
    EnableWindow(GetItemHandle(IDC_DOWN), false);
  end;
end;

procedure TGSDef.EnableButtons;
{- check the current status and enable/disable the appropriate buttons }
var
  enabl   : boolean;
begin
  enabl:= grid^.GetRowCount > 0;
  EnableWindow(GetItemHandle(IDC_DEFINE), enabl);
  EnableWindow(GetItemHandle(IDC_EDIT), enabl);
  EnableWindow(GetItemHandle(IDC_DELETE), enabl);
  EnableWindow(GetItemHandle(IDC_ADD), grid^.GetRowCount < 12);
  CheckUpDown;
end;

procedure TGSDef.SetRowData(aSlot: TSlotDef; aRow: integer);
{- set the data in the specified row for the slot definition }
{- aSlot - a slot defintion
   aRow  - The row of the grid to set (aRow must be an existing row) }
var
  p1    : array[0..100] of char;
begin
  grid^.SetData(Grid_Abbr, aRow, aSlot.abbr);
  grid^.SetData(Grid_Name, aRow, aSlot.name);
  if aSlot.resType = GSFreeText then
    grid^.SetData(Grid_Type, aRow, SR(IDS_FREETEXT, p1, sizeof(p1)-1))
  else
    grid^.SetData(Grid_Type, aRow, SR(IDS_STANDARD, p1, sizeof(p1)-1));
end;

procedure TGSDef.IDGrid(var msg: TMessage);
{- respond to Grid messages }
begin
  if msg.lParamHi = LBN_SELCHANGE then
    CheckUpDown
  else if msg.lParamHi = LBN_DBLCLK then
    IDEdit(msg);
  DefWndProc(msg);
end;

procedure TGSDef.IDAdd(var msg: TMessage);
{- add a slot to the defintion }
var
  idx, k  : integer;
  subst   : PErrSubst;
  p1      : array[0..100] of char;
begin
  {- are the max number of slots defined ? }
  if (grid^.GetRowCount >= MaxGSSlots) then
  begin
    Str(MaxGSSlots, p1);
    subst:= New(PErrSubst, Init(p1));
    ShowError(@self, IDS_MAXSLOTS, subst, 0, MOD_GSDEF, 0);
    Dispose(subst, Done);
  end
  else
  begin
    idx:= integer(grid^.GetRowCount);
    {- execute the slot editor dialog }
    if application^.ExecDialog(New(PGSEdit, Init(@self, db, slot[idx], false))) = IDOK then
    begin
      k:= grid^.AddString('');  {- add the slot to the grid }
      if k >= 0 then
      begin
        SetRowData(slot[idx], k);
        grid^.SetSelIndex(k);
        EnableButtons;
        if not IsWindowEnabled(GetItemHandle(IDC_ADD)) then
          FocusCtl(HWindow, IDC_GRID)
        else
          FocusCtl(HWindow, IDC_ADD);
      end
      else
      begin
        ShowError(nil, IDS_ROWALLOCERR, nil, 0, MOD_GSDEF, 0);
        halt;
      end;
    end;
  end;
end;

procedure TGSDef.IDEdit(var msg: TMessage);
{- edit the currently selected slot }
var
  p1, p2  : array[0..100] of char;
  args    : array[0..3] of PChar;
  idx     : integer;
begin
  idx:= grid^.GetSelIndex;
  if idx >= 0 then
  begin
    {- don't allow editing of disabled rows }
    if not grid^.GetRowEnabled(idx) then
      ErrorMsg(HWindow, SR(IDS_ERROR, p1, 100), SR(IDS_CANTMOD, p2, 100))
    else
      if application^.ExecDialog(New(PGSEdit, Init(@self, db, slot[idx], true))) = IDOK then
        SetRowData(slot[idx], idx);
  end
  else
    ErrorMsg(HWindow, SR(IDS_ERROR, p1, 100), SR(IDS_NOSEL, p2, 100));
end;

procedure TGSDef.IDDelete(var msg: TMessage);
var
  p1, p2  : array[0..100] of char;
  k, j    : integer;
begin
  k:= grid^.GetSelIndex;
  if k >= 0 then
  begin
    if not grid^.GetRowEnabled(k) then
      ErrorMsg(HWindow, SR(IDS_ERROR, p1, 100), SR(IDS_CANTMOD, p2, 100))
    else
      if YesNoMsg(HWindow, SR(IDS_CONFIRM, p1, 100), SR(IDS_CONFIRMDEL, p2, 100)) then
      begin
        grid^.DeleteString(k);
        for j:= k + 1 to MaxGSSlots - 1 do
          slot[j - 1]:= slot[j];
        FillChar(slot[MaxGSSlots - 1], sizeof(slot[MaxGSSLots - 1]), 0);
        if k >= grid^.GetRowCount then
          Dec(k);
        grid^.SetSelIndex(k);
        EnableButtons;
      end;
  end;
end;

procedure TGSDef.IDDefineVals(var msg: TMessage);
{- call the gram stain mnemonic file editor }
var
  listObj   : PListObject;
begin
  listObj:= New(PListObject, Init);
  listObj^.EditableMnemList(@self, DBGSMnemFile, '');
  Dispose(listObj, Done);
end;

procedure TGSDef.IDUp(var msg: TMessage);
{- move the current slot up one }
var
  k     : integer;
  temp  : TSlotDef;
begin
  k:= grid^.GetSelIndex;
  if k > 0 then
  begin
    grid^.SwapRows(k, k - 1);
    grid^.SetSelIndex(k - 1);
    temp:= slot[k];
    slot[k]:= slot[k - 1];
    slot[k - 1]:= temp;
    CheckUpDown;
    FocusCtl(HWindow, IDC_GRID);
  end;
end;

procedure TGSDef.IDDown(var msg: TMessage);
{- move the current slot down one }
var
  k     : integer;
  temp  : TSlotDef;
begin
  k:= grid^.GetSelIndex;
  if k < grid^.GetRowCount - 1 then
  begin
    grid^.SwapRows(k, k + 1);
    grid^.SetSelIndex(k + 1);
    temp:= slot[k];
    slot[k]:= slot[k + 1];
    slot[k + 1]:= temp;
    CheckUpDown;
    FocusCtl(HWindow, IDC_GRID);
  end;
end;

procedure TGSDef.Cancel(var msg: TMessage);
var
  k  : integer;
begin
  if CanClose then
    inherited Cancel(msg);
end;

function TGSDef.CanClose: boolean;
var
  p1, p2  : array[0..100] of char;
  k       : integer;
  c       : integer;
  isOK    : boolean;
  seq     : TSeqNum;
  pw      : TWaitCursor;
begin
  c:= YesNoCancelMsg(HWindow, SR(IDS_CONFIRM, p1, 100), SR(IDS_CONFIRMSAVE, p2, 100));
  if c = IDYES then
  begin
    pw.Init;
    with db^ do
    begin
      {- first delete all unused slots from database }
      if dbc^.GetFirst(dbr) then
      begin
        repeat
          if not SlotInUse(dbr^.GetSeqValue) then
            dbc^.DeleteRec(dbr);
          dbc^.GetNext(dbr);
        until dbc^.dbErrorNum <> 0;
      end;

      {- next renumber the slot numbers of the referenced slots. This procedure
         sets the slot number to currentSlot + MaxSlots. This is to move them
         temporarily so when the new slots are added to the database there
         wont be any duplicate index problems. After new slots are added, the
         referenced slots number will be reset to its proper value }
      for k:= 0 to grid^.GetRowCount - 1 do
      begin
        if not grid^.GetRowEnabled(k) then  {- slot in use }
        begin
          if dbc^.GetSeq(dbr, slot[k].seq) then
          begin
            c:= k + MaxGSSlots;
            dbr^.PutField(DBGSDefSlotNum, @c);
            dbc^.UpdateRec(dbr);
            if (dbc^.dbErrorNum <> 0) then
              ShowError(@self, IDS_CANTUPDATESLOT, nil, dbc^.dbErrorNum, MOD_GSDEF, 0);
          end;
        end;
      end;

      {- now add all newly defined slots. No need to worry about duplicate
         index errors since existing referenced slots have been moved up (see above)}
      for k:= 0 to grid^.GetRowCount - 1 do
      begin
        if grid^.GetRowEnabled(k) then
        with slot[k] do
        begin
          dbr^.PutField(DBGSDefSlotNum, @k);
          dbr^.PutField(DBGSDefAbbr, @abbr);
          dbr^.PutField(DBGSDefDesc, @name);
          dbr^.PutField(DBGSDefResType, @resType);
          dbc^.InsertRec(dbr);
          if (dbc^.dbErrorNum <> 0) then
            ShowError(@self, IDS_CANTINS, nil, dbc^.dbErrorNum, MOD_GSDEF, 0);
        end;
      end;

      {- now renumber the referenced slots to their proper slot number }
      if dbc^.GetFirst(dbr) then
      begin
        repeat
          dbr^.GetField(DBGSDefSlotNum, @k, sizeof(k));
          if k >= MaxGSSlots then
          begin
            Dec(k, MaxGSSlots);
            dbr^.PutField(DBGSDefSlotNum, @k);
            dbc^.UpdateRec(dbr);
            if (dbc^.dbErrorNum <> 0) then
              ShowError(@self, IDS_CANTUPDATESLOT, nil, dbc^.dbErrorNum, MOD_GSDEF, 0);
          end;
        until not dbc^.GetNext(dbr);
      end;
    end;
    pw.Done;
  end;
  CanClose:= c <> IDCANCEL;
end;

function TGSDef.SlotInUse(aSlotRef: TSeqNum): boolean;
{- return true if the specified slot is in use in the result file }
var
  rdb   : PDBFile;
  ret   : boolean;
k : longint;
begin
  ret:= false;

  rdb:= New(PDBFile, Init(DBGSFile, '', DBOpenExclusive));
  if rdb <> nil then
  begin
    with rdb^ do
    begin
      dbc^.SetCurKeyNum(2);  {- set key to SlotRef key }
      dbr^.PutField(DBGSGSTstRef, @aSlotRef);
      ret:= dbc^.GetEQ(dbr);
    end;
  end;

  if rdb <> nil then
    Dispose(rdb, Done);

  SlotInUse:= ret;
end;

{--------------------------------------------------------------[ TGSEdit ]--}

constructor TGSEdit.Init(aParent: PWindowsObject; aDb: PDBFile; var aSlotDef: TSlotDef; editMode: boolean);
var
  c   : PControl;
  fi  : TFldInfo;
begin
  inherited Init(aParent, MakeIntResource(DLG_GSEDIT));

  slot:= @aSlotDef;
  db:= aDb;

  tRec:= New(PDBRec, Init(db^.dbc));
  if trec = nil then
  begin
    ShowError(nil, IDS_CANTALLOCTEMP, nil, 0, MOD_GSDEF, 0);
    Fail;
  end;

  {- setup un-bolded labels }
  c:= New(PLStatic, InitResource(@self, IDC_LBLABBR, 0, false));
  c:= New(PLStatic, InitResource(@self, IDC_LBLNAME, 0, false));

  c:= New(PLRadioButton, InitResource(@self, IDC_STANDARD, false));
  c:= New(PLRadioButton, InitResource(@self, IDC_FREETEXT, false));

  abr:= New(PUIFld, Init(@self, db^.dbd, nil, true, DBGSDefAbbr, IDC_ABBREV, IDC_LBLABBR));
  name:= New(PUIFld, Init(@self, db^.dbd, nil, true, DBGSDefDesc, IDC_NAME, IDC_LBLNAME));
  edMode:= editMode;
end;

destructor TGSEdit.Done;
begin
  inherited Done;
  Dispose(trec, Done);
end;

procedure TGSEdit.SetupWindow;
var
  pstr: array[0..100] of char;
begin
  inherited SetupWindow;

  trec^.PutField(DBGSDefAbbr, @slot^.abbr);
  trec^.PutField(DBGSDefDesc, @slot^.name);

  name^.XFerRecToCtl(trec);
  abr^.XFerRecToCtl(trec);

  SendDlgItemMsg(IDC_STANDARD, BM_SETCHECK, BF_UNCHECKED, 0);
                    SendDlgItemMsg(IDC_FREETEXT, BM_SETCHECK, BF_UNCHECKED, 0);
  if slot^.resType = GSFreeText then
    SendDlgItemMsg(IDC_FREETEXT, BM_SETCHECK, BF_CHECKED, 0)
  else
    SendDlgItemMsg(IDC_STANDARD, BM_SETCHECK, BF_CHECKED, 0);

  if edMode then
    SR(IDS_EDITMODE, pstr, sizeof(pstr)-1)
  else
    SR(IDS_ADDMODE, pstr, sizeof(pstr)-1);
  SetDlgItemText(HWindow, IDC_MODE, pstr);
end;

function TGSEdit.CanClose: boolean;
var
  ret   : boolean;
  err   : integer;
begin
  ret:= true;
  err:= abr^.IsValid;
  if err <> 0 then
    ShowUIFldError(@self, err, abr)
  else
  begin
    err:= name^.IsValid;
    if err <> 0 then
      ShowUIFldError(@self, err, name);
  end;
  ret:= err = 0;

  if ret then
  begin
    abr^.XferCtlToRec(trec);
    name^.XferCtlToRec(trec);

    trec^.GetField(DBGSDefAbbr, @slot^.abbr, sizeof(slot^.abbr)-1);
    trec^.GetField(DBGSDefDesc, @slot^.name, sizeof(slot^.name)-1);

    if SendDlgItemMsg(IDC_STANDARD, BM_GETCHECK, 0,0) = BF_CHECKED then
      slot^.resType:= GSStandard
    else
      slot^.resType:= GSFreeText;
  end;

  CanClose:= ret;
end;

procedure TGSEdit.WMCtlColor(var msg: TMessage);
begin
  if msg.lParamLo = GetItemHandle(IDC_MODE) then
  begin
    SetTextColor(msg.wParam, RGB($FF,0,0));
    msg.result:= GetStockObject(WHITE_BRUSH);
    SetWindowLong(HWindow, DWL_MSGRESULT, msg.result);  {- set message result (for dialogs) }
  end
  else
    DefWndProc(msg);
end;

procedure DefineGSDef(aParent: PWindowsObject);
var
  d   : PGSDef;
begin
  d:= New(PGSDef, Init(aParent));
  application^.ExecDialog(d);
end;

END.

{- rcf }
