unit RptZoom;

{- this unit provides a dialog box to retrieve a zoom factor }

INTERFACE

uses
  DMSDebug,
  DlgLib,
  WinProcs,
  WinTypes,
  ODialogs,
  OWindows,
  Strings;

{$I RPTGEN.INC}

type
  PZoomDlg = ^TZoomDlg;
  TZoomDlg = object(TCenterDlg)
    zoom  : ^Real;
    lb    : PListBox;
    constructor Init(AParent: PWindowsObject; var aZoom: Real);
    procedure SetupWindow; virtual;
    procedure idList(var msg: TMessage); virtual id_First + 101;
    procedure OK(var msg: TMessage); virtual id_First + id_OK;
  end;

IMPLEMENTATION

constructor TZoomDlg.Init(AParent: PWindowsObject; var aZoom: Real);
begin
  zoom:= @aZoom;
  inherited Init(AParent, MAKEINTRESOURCE(DLG_ZOOM));
  lb:= New(PListBox, InitResource(@self, 101));
end;

procedure TZoomDlg.SetupWindow;

begin
  inherited SetupWindow;
  lb^.AddString('25%');
  lb^.AddString('50%');
  lb^.AddString('75%');
  lb^.AddString('100%');
  lb^.AddString('125%');
  lb^.AddString('150%');
  lb^.AddString('175%');
  lb^.AddString('200%');
  lb^.SetSelIndex(Trunc(4.0 * zoom^ - 1.0));
end;

procedure TZoomDlg.idList(var msg: TMessage);
begin
  DefWndProc(msg);
  if (msg.wParam = 101) then
  begin
    case msg.lParamHi of  {- lParamHi = LBN_xxx }
      LBN_DBLCLK:
        OK(msg);
    end;
  end;
end;

procedure TZoomDlg.OK(var msg: TMessage);
var
  li  : Integer;
begin
  li:= lb^.GetSelIndex;
  if li <> -1 then
  begin
    zoom^:= ((li + 1) * 25.0) / 100.0;
    inherited OK(msg);
  end;
end;

END.

{- rcf }
