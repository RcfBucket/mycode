unit PDLObj;
{----------------------------------------------------------------------------}
{  Module Name  : PDLObj.PAS                                               }
{  Programmer   : MJ                                                         }
{  Date Created : 05/95                                                      }
{                                                                            }
{  Purpose -            }
{                                                                            }
{  Assumptions -                                                             }
{              None.                                                                     }
{                                                                            }
{  Revision History -                                                        }
{                                                                            }
{  Version  Date      Who    Did What                                        }
{  -------  --------  ------ ----------------------------------------------- }
{  1.00     05/95     MJ     Initial release.                                }
{                                                                            }
{----------------------------------------------------------------------------}

{$R-}
{$Q-}

INTERFACE

uses
  Objects,
  Bits,
  DMString,
  IntlLib,
  APITools,
  Strings,
  DMSErr,
  MSCAN,
  Trays,
  WinTypes,
  WinProcs,
  WinDos;

const
  MaxDesc              = 25;    { Offset to end of panel description }
  MAXTRWell            = 95;    { Max well number }
  MaxData              = 47;    { Highest PDL data address   }
  MAXFilter            = 7;     { Extended to 7 from 6 to support waterblank averages }
  WaterBlankFilter     = 1;     { Compensates for extended filter (TEMPORARY) }
  run                  = 0;     { Index to runvalues part of rawdata array}
  cal                  = 1;     { Index to calibrate values part of rawdata array}
  MaxPDLBuff           = 63;    { Number of cached pages (must be power of 2 minus 1) }
  OffMask              = $FF;   { Mask for offset within page }
  PageMask             = $7F;   { Mask for page portion of code address }
  PageShift            = 8;     { Low bit position of page in address }
  PageSectors          = 2;     { Number of sectors in a page ( (OffMask+1)/128 ) }
  MaxStack             = 63;    { Stack size }
  MaxArray             = 95;    { Offset to end of panelarray }
  SectorSize           = 128;
  FirstSector          = 1;     { First sector of code in PDL file }
  MinInt               = -32768;
  MaxCode              = 32767; { Highest allowed PDL code address }
  PDLTrue              = 3;     { Certain/TRUE }
  PDLFalse             = 0;     { Certain/FALSE }

type
  PreDef      = (loccheck, locval, thresh, avgng, upperng,
                 lowerng, contam, insuff, class, ofbad,
                 dcbad, callrange, endpoint, micsize, skips,
                 needsreag, savemics, growtime, reacttime,
                 maxovertime, micssaved, reagdispd, nextflags,
                 possmrsa, slowgrow, trayedited, badnpc, badbnac);

  Wellnum     = 0..MaxTRWell;

  TPanelList  = array[0..MaxTray] of integer;
  DescType    = record case integer of
    0:( name     : string[15];
        entry    : integer;
        disp     : integer;
        micAddr  : integer;
        flags    : integer;
        extNum   : integer);
   1:( mem      : array[0..MaxDesc] of byte);
    end;

   SourceRng   = Run..Cal;
   Wavelength  = 0..MaxFilter;
   PRawData    = ^TRawData;
   TRawData    = array[sourceRng, wavelength, wellnum] of integer;
   Page        = array[0..OffMask] of byte;

   PPDLObject = ^TPDLObject;
   TPDLObject = object(TObject)
      PDLErrorWndw: HWnd;
      panelNum    : integer;
      extNum      : integer;
      panelFlags  : integer;
      panelList   : TPanelList;
      objectFile  : FILE;
      formData    : array[wellnum] of integer;
      pDLDATA     : array[0..MaxData] of integer;
      rawData     : PRawData;
      posFlags    : TPanelBitmap;
      chkFlags    : TPanelBitmap;
      MICData     : TMICData;
      skipDrug    : array[1..MAXMicLen] of integer;
      pageTable   : array[1..MaxPDLBuff] of page;
      pageMap     : array[0..PageMask] of 0..MaxPDLBuff;
      codeMap     : array[1..MaxPDLBuff] of 0..PageMask;
      killBuff    : 0..MaxPdlBuff;
      thisPage    : integer;
      thisBuff    : integer;
      killPage    : integer;
      { private }
      curMICAddr  : integer;
      panelArray  : array[0..MaxArray] of integer;
      panelDesc   : DescType;
      stack       : array[0..MaxStack] of integer;
      stackPtr    : integer;
      IsWaterReady : Boolean;
      {}
      constructor Init(aRawData: PRawData);
      Procedure ResetAS4PDO(aRawData: PRawData; PanelNumber: Integer);
      procedure SetHWindow(wndw : HWnd);
      procedure PDLAbort(errID : integer);
      procedure LoadBuff(pageNum: integer; adr: pointer);
      function ObjByte(adr: integer): byte;
      function ObjWord(adr: integer): integer;
      function Min(a,b: integer): integer;
      function Max(a,b: integer): integer;
      function Not4(a: integer): integer;
      function Xor4(a,b: integer): integer;
      function Abs(n: longint): longint;
      function Cbi(data: byte): integer;
      function FormResult(wellnum, formAddress: integer): integer;
      procedure Push(value: integer);
      function Pop: integer;
      function Tos: integer;
      procedure Swap;
      procedure Dup;
      procedure ComputeAverage;
      procedure EvalWells;
      procedure EvalMICs;
      function  Quad(posResult, uncertain: boolean): integer;
      procedure RunProgram(address, curWell: integer);
      procedure FindPanel(trayNum: integer);
      procedure RedoEval;
      procedure EvalPanel(ARawData: PRawData; trayNum: integer);
      Function  GetAS4PDOName(PanelNumber: Integer; Var AS4PDOName: String): Boolean;
      destructor Done; virtual;
    end;

IMPLEMENTATION

uses
  DateLib;

{$I AUTOSCAN.INC }
{$I AS4RDR.INC }

Function TPDLObject.GetAS4PDOName
{===================================================================}
{  Assemble a PDO file name based panel number, the basic names,    }
{  whether an AS4AVG.TBL (waterblank average) file is present,      }
{  and if the panel supports waterblank.                            }
{===================================================================}
  (PanelNumber    : Integer;     {Which panel to get}
   Var AS4PDOName : String)      {Returns PDO name}
  : Boolean;                     {True=sucess;  False=failure}
Const
  pkWaterMapA   = 'AS4GRPA'; {Feature name}
  pkWaterMapB   = 'AS4GRPB'; {Feature name}
  pkWaterMapC   = 'AS4GRPC'; {Feature name}
  pkWaterMapD   = 'AS4GRPD'; {Feature name}
var
  WaterBlankStatus    : array[0..20] of char; {Feature status}
  IsWaterBlankEnabled : Boolean;              {Feature status}
  PanelOffset         : Integer;              {Index into waterblank map}
begin
  {Set default return value (failed)}
  GetAS4PDOName := False;
  FillChar(AS4PDOName,SizeOf(AS4PDOName),chr(0));
  FillChar(WaterBlankStatus,SizeOf(WaterBlankStatus),'0');

  {Get base PDO name, waterblank status for panels and index into waterblank status string}
  If (PanelNumber >=   1) and (PanelNumber <=  20) Then
  Begin
    PanelOffset := PanelNumber-1;
    AS4PDOName := pkWaterMapA;
    GetPrivateProfileString(appName, pkWaterMapA, 'NONE', WaterBlankStatus, sizeof(WaterBlankStatus), profFile);
  End;
  If (PanelNumber >=  21) and (PanelNumber <=  40) Then
  Begin
    PanelOffset := PanelNumber - 20-1;
    AS4PDOName := pkWaterMapB;
    GetPrivateProfileString(appName, pkWaterMapB, 'NONE', WaterBlankStatus, sizeof(WaterBlankStatus), profFile);
  End;
  If (PanelNumber >=  41) and (PanelNumber <=  60) Then
  Begin
    PanelOffset := PanelNumber - 40-1;
    AS4PDOName := pkWaterMapC;
    GetPrivateProfileString(appName, pkWaterMapC, 'NONE', WaterBlankStatus, sizeof(WaterBlankStatus), profFile);
  End;
  If (PanelNumber >=  61) and (PanelNumber <=  80) Then
  Begin
    PanelOffset := PanelNumber - 60-1;
    AS4PDOName := pkWaterMapD;
    GetPrivateProfileString(appName, pkWaterMapD, 'NONE', WaterBlankStatus, sizeof(WaterBlankStatus), profFile);
  End;

  If ( Strcomp(Addr(AS4PDOName),'')<>0 ) Then
  Begin
    If ( IsWaterReady ) Then
    Begin
      If ( StrComp(WaterBlankStatus,'NONE') <> 0 ) Then
      Begin
        IsWaterBlankEnabled := ((WaterBlankStatus[PanelOffset]='W') or (WaterBlankStatus[PanelOffset]='w'));
        {If this panel has waterblank support, get the right PDO file}
        If IsWaterBlankEnabled Then
          AS4PDOName := Concat(AS4PDOName,'W');
      End;
    End;
    AS4PDOName := Concat(AS4PDOName,'.PDO');
    {Set return value (success)}
    GetAS4PDOName := True;
  End;
end;

procedure TPDLObject.PDLAbort(errID : integer);
{--------------------------------------------------------------}
{ purpose :   Error out when some condition has been met.      }
{ input   :   errnum   - The error message to be displayed.    }
{      x   - Some Integer to be printed.                       }
{--------------------------------------------------------------}
var
  errMsg : array[0..63] of char;
begin
  ErrorMsg(PDLErrorWndw, SR(IDS_AS4TITLE, errMsg, 63), SR(errID, errMsg, 63));
  Halt;
end;

procedure TPDLObject.SetHWindow(wndw: HWnd);
begin
  PDLErrorWndw := wndw;
end;

procedure TPDLObject.LoadBuff(pageNum: integer; adr: pointer);
{---------------------------------------}
{ Load page from file For ObjByte       }
{---------------------------------------}
var
  res	: word;
begin
  Seek(ObjectFile, (pagenum * PageSectors) + FirstSector);
  BlockRead(ObjectFile, adr^, PageSectors, res);
  if (IoResult <> 0) or (res = 0) then
    PdlAbort(IDS_PDLPAGELOAD);
end;

function TPDLObject.ObjByte(adr: integer): byte;
{---------------------------------------}
{ Fetch byte from given PDL address     }
{---------------------------------------}
begin
  thispage:= adr shr PageShift;
  thisbuff:= PageMap[ thispage ];
  if thisbuff = 0 then
  begin
    killpage:= CodeMap[ killBuff ];  { Get victim ptr }
    if killpage <> $FF then
      PageMap[ killpage ]:= 0;      { flag that its been swapped out }
    thisbuff:= killBuff;
    PageMap[thispage]:= thisbuff;    { select victim to overwrite }
    CodeMap[thisbuff]:= thispage;    { remember who owns the buffer }
    LoadBuff(thispage, @PageTable[thisbuff]);
    killBuff:= (killBuff+1) and MaxPdlBuff;
    if KillBuff=0 then
      KillBuff:= KillBuff + 1;
  end;
  ObjByte:= PageTable[ thisbuff, adr and OffMask ]
end;

function TPDLObject.ObjWord(adr: integer): integer;
{---------------------------------------}
{ Fetch word from given PDL address     }
{---------------------------------------}
var
  lowbyte  : integer;
begin
  lowbyte:= ObjByte(adr);
  ObjWord:= lowbyte OR (ObjByte(adr+1) shl 8);
end;

function TPDLObject.Min( a,b : integer ) : integer;
{---------------------------------------}
{ Return the smaller of two Integers    }
{---------------------------------------}
begin
  if a < b then
    Min:= a
  else
    Min:= b
end;

function TPDLObject.Max( a,b : integer ) : integer;
{---------------------------------------}
{ Return the larger of two Integers     }
{---------------------------------------}
begin
  if a > b then
    Max:= a
  else
    Max:= b
end;

function TPDLObject.NOT4(a: integer): integer;
{---------------------------------------}
{ return negation of quaternary value   }
{---------------------------------------}
begin
  not4:= 3-a
end;

function TPDLObject.XOR4(a,b: integer): integer;
{---------------------------------------}
{ XOR two quaternary Booleans           }
{---------------------------------------}
begin   { ( A and NOT B ) or ( B and NOT A ) }
  xor4:= Max(Min(a, Not4(b)), Min(b, Not4(a)));
end;

function TPDLObject.Abs(n: longint): longint;
{--------------------------------}
{ Return ABS of an Integer.      }
{ Needed to fix compiler bug!    }
{--------------------------------}
begin
  if n > 0 then
    Abs:= n
  else
    Abs:= -n
end;

function TPDLObject.Cbi( data : byte ) : integer;
{-----------------------------------------}
{ Sign-extend a byte into an Integer.     }
{-----------------------------------------}
begin
  if data>127 then
    cbi:= data - 256
  else
    cbi:= data;
end;

function TPDLObject.FormResult(wellnum, formaddress: integer): integer;
{-------------------------------------------------------------------}
{ Return the result of the formula at FORMADDRESS evaluated using   }
{ data from WELLNUM.                                                }
{-------------------------------------------------------------------}
var
  temp   : integer;
  scale  : integer;
  filter : integer;
begin
  temp:= 0;
  if (wellnum>=0) and (wellnum<=95) then
  begin
    repeat
      scale:= cbi(ObjByte(formaddress));
      if scale<>-128 then
      begin
        filter:= ObjByte(formaddress+1);
        formaddress:= formaddress+2;
        temp:= temp+scale*rawdata^[filter div 16,filter mod 16,wellnum]
      end;
    until scale = -128;
  end;
  formresult:= temp;
end;

procedure TPDLObject.Push(value : integer);
{-----------------------------------------------------}
{ Push a number onto the evaluation stack.            }
{-----------------------------------------------------}
begin
  if stackptr<=maxstack then
     stack[stackptr]:= value;
  stackptr:= stackptr+1
end;

function  TPDLObject.Pop : integer;
{---------------------------------------------------}
{ Pop a value from the evaluation stack.            }
{---------------------------------------------------}
begin
  if stackptr<>0 then
     stackptr:= stackptr - 1;
  pop:= stack[stackptr]
end;

function TPDLObject.Tos : integer;
{----------------------------------------------}
{ Return TOS without popping.                  }
{----------------------------------------------}
begin
  tos:= stack[stackptr-1]
end;

procedure TPDLObject.Swap;
{---------------------------------------------------}
{ Swap the top two items on the stack               }
{---------------------------------------------------}
var
  xtos, xnos  : integer;
begin
  xtos:= pop;
  xnos:= pop;
  push(xtos);
  push(xnos)
end;

procedure TPDLObject.Dup;
{----------------------------------------------}
{ Duplicate the item on the top of the stack.  }
{----------------------------------------------}
var
  xtos   : integer;
begin
  xtos:= pop;
  push(xtos);
  push(xtos)
end;

procedure TPDLObject.ComputeAverage;
{---------------------------------------}
{ Compute the average no-growth         }
{---------------------------------------}
var
  accumulator : longint;
  wellcount   : longint;
  curwelladdr : integer;
  curwellnum  : integer;
  flags       : integer;
  tempstack   : integer;
  wellvalue   : integer;
begin
  accumulator:= 0;
  wellcount := 0;
  curwelladdr:= PanelDesc.MicAddr;
  tempstack:= stackptr;
  while ObjByte(curwelladdr)<>255 do
  begin
    flags:= ObjByte(curwelladdr+5);
    if not testbit(flags, 0) then   { Don't average this drug   }
      curwelladdr:= curwelladdr + ObjByte(curwelladdr) + 9
    else
    begin
      curwelladdr:= curwelladdr + 8;   { skip MIC header   }
      curwellnum:= cbi(ObjByte(curwelladdr));
      while curwellnum <> -1 do
      begin
        if curwellnum >= 0 then
        begin { negative well means exclude }
          runprogram(panelArray[curwellnum],curwellnum);
          stackptr:= tempstack;
          wellvalue:= formdata[curwellnum];
          if (wellvalue<=PDLDATA[ord(upperng)]) and
             (wellvalue>=PDLDATA[ord(lowerng)]) then
          begin
            accumulator:= accumulator+wellvalue;
            wellcount:= wellcount+1;
          end;
        end;
        curwelladdr:= curwelladdr+1;
        curwellnum:= cbi(ObjByte(curwelladdr));
      end;
      curwelladdr:= curwelladdr+1   { skip MIC trailer   }
    end;
  end;
  if wellcount<>0 then
    PDLDATA[ord(avgng)]:= accumulator Div wellcount
  else
    { DMS 15.00 - used to be minint+1000. Now AVGNG will be changed later}
    PDLDATA[ord(avgng)]:= minint
end;

procedure TPDLObject.EvalWells;
{---------------------------------------}
{ Determine pos/neg For all wells.      }
{---------------------------------------}
var
  wellnum  : integer;
  temp     : integer;
begin
  for wellnum:= 0 to 95 do
  begin
    runprogram(panelArray[wellnum],wellnum);
    temp:= pop;
    if temp>1 then
      SetBit(posflags, wellnum)
    else
      Clearbit(posflags, wellnum);
    if (temp=1) or (temp=2) then
      SetBit(chkflags, wellnum)
    else
      clearbit(chkflags, wellnum)
  end;
end;

procedure TPDLObject.EvalMICs;
{---------------------------------}
{ Find all MIC endpoints.         }
{---------------------------------}
var
  skipcount   : integer;
  codeaddr    : integer;
  micnum      : integer;
begin
  micnum:= 1;
  skipcount:= 0;
  curmicaddr:= PanelDesc.MicAddr;
  while ObjByte(curmicaddr)<>255 do
  begin
    PDLDATA[ord(endpoint)]:= maxint;
    PDLDATA[ord(micsize)] := ObjByte(curmicaddr);
    PDLDATA[ord(skips)]   := 0;
    codeaddr:= ObjWord(curmicaddr+6);
    runprogram(codeaddr,maxint);
    skipcount:= skipcount+PDLDATA[ord(skips)];
    skipDrug[micnum]:= PDLDATA[ord(skips)];
    micdata[micnum]:= PDLDATA[ord(endpoint)];
    micnum:= micnum+1;
    curmicaddr:= curmicaddr + ObjByte(curmicaddr) + 9
  end;
  PDLDATA[ord(skips)]:= skipcount
end;

function  TPDLObject.QUAD(posresult, uncertain: boolean): integer;
{-----------------------------------------------}
{ Return a quaternary logical result given      }
{ pos and uncertain.                            }
{-----------------------------------------------}
begin
  if posresult then
    if uncertain then
      quad:= 2
    else
      quad:= 3
  else
    if uncertain then
      quad:= 1
    else
      quad:= 0
end;

procedure TPDLObject.RunProgram(address, curwell: integer);
{--------------------------------------------}
{ Main simulator procedure.                  }
{--------------------------------------------}
var
  opcode   : integer;
  scratch  : integer;
  nxtwell  : integer;

  function NXTBYTE : integer;
  {-----------------------------------------------}
  { Return a byte operand.                        }
  {-----------------------------------------------}
  begin
    nxtbyte:= ObjByte(address);
    address:= address+1
  end;

  function NxtWord : integer;
  {-----------------------------------------------}
  { Return a word operand.                        }
  {-----------------------------------------------}
  var
    lowbyte: integer;
  begin
    lowbyte:= nxtbyte;
    nxtword:= lowbyte+(nxtbyte shl 8)
  end;

procedure ReLop(opCode: integer);
{-----------------------------------------------}
{ Do a relational operation.                    }
{-----------------------------------------------}
var
  xtos        : integer;
  xnos        : integer;
  posresult   : boolean;
  uncertain   : boolean;
  range       : integer;
begin
  if opcode > 47 then
  begin  { call range specified   }
    range := nxtbyte;
    opcode:= opcode-48
  end
  else
  begin
    range:= PDLDATA[ord(callrange)];   { default range   }
    opcode:= opcode-32
  end;
  if opcode<6 then
  begin  { legal opcode   }
    xtos:= pop;
    xnos:= pop;
    Case opcode of
      0 : posresult:= xnos >  xtos;
      1 : posresult:= xnos <  xtos;
      2 : posresult:= xnos >= xtos;
      3 : posresult:= xnos <= xtos;
      4 : posresult:= xnos =  xtos;
      5 : posresult:= xnos <> xtos
    end;
    uncertain:= abs(xtos-xnos) < range;
    push(quad(posresult,uncertain))
  end;
end;

begin   (* START of RUNPROGRAM *)
  if (address>maxcode) or (address<0) then
    exit;
  nxtwell:= curwell;
  repeat
    opcode:= nxtbyte;
    if (opcode >= 32) and (opcode <= 53) then   { relational   }
      relop(opcode)
    else
      case opcode of
        1 : push(pop+pop);
        2 : begin
              scratch:= pop;
              push(-scratch+pop);
            end;
        3 : push(pop*pop);
        4 : begin
              swap;
              scratch:= pop;
              push(scratch div pop);
            end;
        5 : begin
              swap;
              scratch:= pop;
              push(scratch mod pop)
            end;
        6 : push(-pop);
        8 : push(min(pop, pop));   { and4(pop,pop));   }
        9 : push(max(pop, pop));   { or4(pop,pop));   }
       10 : push(xor4(pop,pop));
       11 : push(not4(pop));       { not4(pop));      }
       16 : dup;                  { dup }
       17 : scratch:= pop;       { pop }
       18 : swap;                 { swap }
       64 : Begin
              scratch:= nxtwell;
              push(formresult(scratch, nxtword));   { eval   }
              nxtwell:= curwell
            end;
       66 : begin            { val   }
              scratch:= pop;
              if (scratch<0) or (scratch>95) then
                push(minint)
              else
                push(formdata[scratch])
            end;
       67 : begin            { pos   }
              scratch:= pop;
              if (scratch<0) or (scratch>95) then
                push(0)
              else
                push(quad(testbit(posflags, scratch),
              testbit(chkflags, scratch)))
            end;
       80 : address:= nxtword+address;   { jmp   }
       81 : if pop < 2 then      { jf   }
              address:= nxtword+address
            else
              scratch:= nxtword;
       82 : begin            { call   }
              scratch:= nxtword;
              runprogram(scratch, nxtwell);
              nxtwell:= curwell;
            end;
    83,84 : exit;         { ret, exit }
       85 : begin
              nxtwell:= pop;
              runprogram(panelArray[nxtwell],nxtwell);   { dcall   }
              nxtwell:= curwell;
            end;
       86 : push(micdata[pop]);   { micval   }
       87 : micdata[pop]:= 0;   { clrmic   }
   96,105 : push(nxtword);   { lit, litw   }
       97 : push(PDLDATA[nxtbyte]);   { lod   }
       98 : PDLDATA[nxtbyte]:= pop;   { sto   }
   99,104 : push(nxtbyte);   { ref, litb   }
      100 : begin            { well   }
              scratch:= pop;
              if (scratch<0) or (scratch>=ObjByte(curmicaddr)) then
                 push(maxint)
              else
                 push(abs(cbi(ObjByte(curmicaddr+scratch+8))))
            end;
      101 : formdata[nxtwell]:= tos; { save }
      102 : push(nxtwell);    { curr   }
(* Remove For AS4 version
      106 : push(nxtlong);    { litd   }
*)
      112 : computeaverage;   { avg   }
      113 : nxtwell:= pop;    { set   }
      128 : evalwells;        { evrx   }
      129 : evalmics;         { evmi   }
    end;  {case}
  until false;
end;

procedure TPDLObject.FindPanel(traynum : integer);
{---------------------------------------------------------}
{ See If the panel type read is indeed the panel expected }
{---------------------------------------------------------}
var
  i,
  testpanel,
  paneladdr,
  entryaddr  : integer;
begin
  testpanel:= 0;
  panelnum:= -1;
  extnum:= 0;
  while (extnum <> traynum) and (panellist[testpanel] <> 0) do
  begin
    paneladdr:= panellist[testpanel];
    for i:= 0 to maxdesc do
       panelDesc.Mem[i]:= ObjByte(paneladdr+i);
    extnum:= panelDesc.ExtNum + 1;
    testpanel:= testpanel + 1;
  end;
  if (extnum <> traynum) then   { in TRAYREAD, If extnum <> traynum, }
    exit;                      { but extnum > 0, Then ... }
  { at this point, assume matching panel has been found }
  PDLDATA[ord(loccheck)]:= PdlTRUE;
  PDLDATA[ord(locval)]  := -2;      { initialize locator as bad }
  entryaddr:= ObjWord(paneladdr+16);
  for i:= 0 to maxArray do
    panelArray[i]:= ObjWord(paneladdr+26+2*i);
  runprogram(entryaddr,maxint);

  if PDLDATA[ord(locval)] = -2 then
    extnum:= 0   { this tells trayread that loc well looks bad. }
  else
    panelnum:= testpanel;
  PDLDATA[ord(loccheck)]:= PdlFALSE;
end;

procedure TPDLObject.RedoEval;
{----------------------------------------------------------}
{ Execute the panel-dependent PDL code with the TRAYEDITED }
{ flag enabled.                                            }
{----------------------------------------------------------}
begin
  PDLDATA[ord(trayedited)]:= PdlTRUE;
  runprogram(PanelDesc.Entry,maxint);
  PDLDATA[ord(trayedited)]:= PdlFALSE;
end;

procedure TPDLObject.EvalPanel
  (ARawData:PRawData;
   traynum : integer);
{-----------------------------------------------------------------------}
{ Evaluate run data.  Determine panel type, MIC's, biochemicals         }
{ Traynum is the number of the DMS tray expected.                       }
{-----------------------------------------------------------------------}
begin
  {Open the appropriate PDO for this panel}
  ResetAS4PDO(ARawData,TrayNum);
  stackptr:= 0;
  findpanel(traynum);
  if panelnum >= 0 then
  begin
    fillchar(formdata,sizeof(formdata),chr($80));
    PDLDATA[ ORD(loccheck) ]:= PdlFALSE;
    PDLDATA[ ORD(trayedited) ]:= PdlFALSE;
    PDLDATA[ ORD(badbnac) ]:= PdlFALSE;
    PDLDATA[ ORD(badnpc) ]:= PdlFALSE;
    runprogram(PanelDesc.Entry,maxint)
  end;
end;

Procedure TPDLObject.ResetAS4PDO
  (aRawData    : PRawData;
   PanelNumber : Integer);
var
  PDLFileSize : integer;
  i           : integer;
  AS4PDOName  : String;
begin
  rawdata:= aRawData;
  Fillchar(AS4PDOName,SizeOf(AS4PDOName),Chr(0));
  If GetAS4PDOName(PanelNumber,AS4PDOName) Then
    Assign(ObjectFile, AS4PDOName);
  if IoResult <> 0 then
    PDLAbort(IDS_NOPDLASSIGN);
  Reset(ObjectFile, 1);
  if IoResult <> 0 then
    PDLAbort(IDS_NOPDLRESET);
  Seek(ObjectFile, 0);
  if IoResult <> 0 then
    PDLAbort(IDS_NOPDLSEEK);
  BlockRead(ObjectFile, PanelList, SIZEOF(TPanelList));
  if IoResult <> 0 then
    PDLAbort(IDS_NOPDLBLKREAD);
  i:= 0;
  while (i<=MaxTray) and (PanelList[i]<>0) do
    Inc(i);
  if i = 0 then
    PDLAbort(IDS_NOPANELSINPDL);         { no panels }
  PDLFileSize:= PanelList[i-1]+212;
  if PDLFileSize > MaxCode then                   { too big   }
    PDLAbort(IDS_PDLFILETOOBIG);
  Reset(ObjectFile, SectorSize);
  if IoResult <> 0 then
    PDLAbort(IDS_NOPDLSECTOR);
  Fillchar(PageMap, sizeof(PageMap), chr(0));
  Fillchar(CodeMap, sizeof(CodeMap), chr($FF));
  killBuff:= 1;
end;

constructor TPDLObject.Init(aRawData: PRawData);
Const
  pkWaterAvgFile  = 'WaterAvgFile'; {Feature name}
  pkIsWaterReady  = 'IsWaterReady'; {Feature name}
Var
  AverageFilePtr  : File of Integer;      {Waterblank average values}
  WellCount       : Integer;              {Well position}
  AvgFileName     : array[0..80] of char; {Default average file name}
begin
  inherited Init;
  {$IFOPT I+}
    {$DEFINE chki}
  {$ENDIF}
  {$I-}

  {PDO open routine moved to subroutine so it can be called from EvalPanel also}
  ResetAS4PDO(ARawData,1);

  IsWaterReady := False;
  GetPrivateProfileString(appName, pkWaterAvgFile, 'NONE', AvgFileName, sizeof(AvgFileName), profFile);
  For WellCount := 0 To MaxTRWell Do
  Begin
    ARawData^[Run,MaxFilter,WellCount] := 0;
    ARawData^[Cal,MaxFilter,WellCount] := 0;
  End;
  If ( Strcomp(AvgFileName,'NONE')<>0 ) Then Begin
    Assign(AverageFilePtr, AvgFileName);
    If IoResult = 0 then
    Begin
      Reset(AverageFilePtr);
      If IoResult = 0 then
      Begin
        For WellCount := 0 To MaxTRWell Do
          Read(AverageFilePtr,ARawData^[0,MaxFilter,WellCount]);
        IsWaterReady := True;
        Close(AverageFilePtr);
        WritePrivateProfileString(appName, pkIsWaterReady, 'Enabled', profFile);
      End;
    End;
  End;

  {$IFDEF chki}
    {$I+}
    {$UNDEF chki}
  {$ENDIF}
end;

destructor TPDLObject.Done;
begin
  inherited Done;
  Close(ObjectFile);
  if IoResult = 0 then {- clear IoResult }
end;

END.
