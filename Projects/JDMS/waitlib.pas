{$F+}   {!!!!!!!}

unit WaitLib;

INTERFACE

uses
   WinDos;

var
  WaitOneMS     : word;
  LoopsPerTick  : longint;

procedure MSWait(ms : word);
procedure SetTO(SecCnt: Word);
function TimeOut: boolean;

IMPLEMENTATION

{$L WAIT.OBJ}

procedure MSWait(ms : word); external;
procedure WaitInit; external;

var
  tDrop    : boolean;
  clockTo  : longInt;

procedure SetTO(secCnt: word);
{- This procedure calculates the "real time" TimeOut time, from the
   number of seconds passed to it and sets a global variable for the
   TimeOut function }
var
   Hour,Min,Sec,Sec100  : word;
begin
  tDrop := FALSE;
  GetTime(hour, min, sec, sec100);
  if (hour = 24) and (min = 59) and (secCnt > (60 - sec)) then
    clockTo := SecCnt - (60 - Sec)
  else
    clockTo := (longint(Hour) * 60 + Min) * 60 + sec + secCnt;
End;


function TimeOut: boolean;
{- This function checks the clock for a TimeOut condition }
var
  tempClk      : longint;
  hour,min,sec : word;
  sec100       : word;
begin
  TimeOut := TRUE;
  if not tDrop then
  begin
    GetTime(hour, min, sec, sec100);
    tempClk := (longint(hour) * 60 + min) * 60 + sec;
    if tempClk > clockTo then
      tDrop := TRUE
    else
      TimeOut := FALSE;
  end;
end;


BEGIN
  clockTo := 0;
  tDrop := FALSE;
  WaitInit;
END.

{- rcf }
