Unit IntCstm;

Interface

Uses
  WinTypes,
  Objects,
  ODialogs,
  OWindows,
  WinProcs,
  IntGlob,
	Strings;

const
	id_IntPort          = 101;

{ id's 102 to 105 are for Cstm paramaters }

  id_UseXonXoff       = 106;
  id_LogFile          = 107;
  id_FileName         = 108;
  id_Timeout	        = 109;
  id_ModemInstalled   = 110;
  id_CfgStrings       = 111;
  id_SaveCfg	        = 112;
	id_AbortCfg         = 113;
  id_DBCSMode         = 114;
  id_ConvMode         = 115;
	id_UseLF            = 116;
	id_UseMicroScan     = 117;
	id_UseAstm          = 118;

  id_InitStr          = 101;
  id_DialStr          = 102;
  id_HangupStr        = 103;
  id_BreakStr	        = 104;
	id_Save	            = 105;

	id_InputFile        = 101;
  id_OutputFile       = 102;
  id_SaveNames        = 103;
  id_FilterMulti = 140;


Type
{Data transfer records for dialog controls.}
  ComboBoxType    = Record
                      ComboStrings: PStrCollection;
											ComboSelection: array[0..MaxComboBoxLen] of Char;
                    end;

  LogComboType    = Record
                      ComboStrings: PStrCollection;
                      ComboSelection: array[0..MaxLogLen] of Char;
                    end;

  HPSCfgBuffType  = Record
                      CstmParmsCfg: array[0..NoOfCstmParms-1] of ComboBoxType;
                      LogFileCfg: LogComboType;
											TimeoutCfg: array[0..MaxTimeoutLen] of char;
											ConvModeCfg: ComboBoxType;
                      DBCSModeCfg: ComboBoxType;
                      ModemInstalledChkCfg: Word;
                      IntFileNameCfg: array[0..79] of char;
                      XRefChksCfg: array[0..13] of word;
                    	LFChkCfg: Word;
                    	MicroScanChkCfg: Word;
                      AstmChkCfg: word;
											XonXoffChkCfg: word;
                      FilterMultiChkCfg: word;
                    end;

  StringsCfgBuffType  = Record
                          InitStrCfg: array[0..InitStrLen] of Char;
                          DialStrCfg: array[0..DialStrLen] of Char;
                          HangupStrCfg: array[0..InitStrLen] of Char;
                          BreakStrCfg: array[0..BreakStrLen] of Char;
                        end;

  PCustomCfg = ^TCustomCfg;
  TCustomCfg = Object(TObject)
    CustomCfgWnd: HWnd;
    Procedure WriteCustomCfgBuff;virtual;
    Procedure InitComboBoxLists;virtual;
    Procedure Run;virtual;
    Constructor Init(TempWnd: HWnd; TempSelf: PWindowsObject);
   end;

  PCustomCfgDlg = ^TCustomCfgDlg;
  TCustomCfgDlg = Object(TDialog)
    Procedure SetupWindow;virtual;
    Procedure SaveCustomCfg(var Msg: TMessage);virtual id_First + id_SaveCfg;
    Procedure AbortCustomCfg(var Msg: TMessage);virtual id_First + id_AbortCfg;
    procedure PortParamSel(var Msg: TMessage);virtual id_First + id_IntPort;
    Procedure EditModemStrings(var Msg: TMessage);virtual id_First +
      id_CfgStrings;
    Procedure ToggleChkStrings(var Msg: TMessage);virtual id_First +
      id_ModemInstalled;
    Procedure DisposeComboBoxCollections;virtual;
  end;

  PStringsCfgDlg = ^TStringsCfgDlg;
  TStringsCfgDlg = Object(TDialog)
    Procedure SaveStrings(var Msg: TMessage);virtual id_First + id_Save;
    Procedure WriteStringsBuff;virtual;
  end;


	PIOFileNamesDlg = ^TIOFileNamesDlg;
  TIOFileNamesDlg = Object(TDialog)
    procedure SaveNames(var Msg: TMessage);virtual id_First + id_SaveNames;
    procedure SetupWindow;virtual;
  end;

var
  CustomCfgBuff: HPSCfgBuffType;
  StringsCfgBuff: StringsCfgBuffType;

  Procedure IntCstm_InitCustomCfgBuff;
  Procedure IntCstm_InitStringsBuff;
  Procedure IntCstm_ExecIntCustomDlg(CurrentWnd: HWnd; PWindowObjPtr:
    PWindowsObject);

Implementation

Uses
  IntASTM,
  LogEvent,
  Log,
  MScan;

var
  CustomCfg: PCustomCfg;
  CustomCfgDlg: PCustomCfgDlg;
  SelfPtr: PWindowsObject;
  MainHWnd: HWnd;

{
****************
CustomCfg Object
****************
}

Constructor TCustomCfg.Init(TempWnd: HWnd; TempSelf: PWindowsObject);
begin
  inherited Init;
  SelfPtr := TempSelf;
  IntCstm_InitCustomCfgBuff;
  InitComboBoxLists;
end;

Procedure TCustomCfg.Run;
var
  CheckPtr: PCheckBox;
  ComboPtr: PComboBox;
  EditPtr: PEdit;
  i, W, H: integer;
  Metrics: TTextMetric;
  DC: HDC;





begin
  CustomCfgDlg := New(PCustomCfgDlg, Init(SelfPtr, 'INT_CUSTOM'));
  DC := GetDC(CustomCfgDlg^.HWindow);
  GetTextMetrics(DC, Metrics);
  W := Metrics.tmMaxCharWidth;
  H := Metrics.tmHeight;
  ReleaseDC(CustomCfgDlg^.HWindow, DC);
  CustomCfgDlg^.EnableTransfer;
  for i := 0 to NoOfCstmParms-1 do
    New(ComboPtr, InitResource(CustomCfgDlg, id_IntPort+i, MaxComboBoxLen + 1));
  New(ComboPtr, InitResource(CustomCfgDlg, id_LogFile, MaxLogLen + 1));
  New(EditPtr, InitResource(CustomCfgDlg, id_Timeout, MaxTimeoutLen + 1));
  New(ComboPtr, InitResource(CustomCfgDlg, id_ConvMode, MaxComboBoxLen + 1));
  New(ComboPtr, InitResource(CustomCfgDlg, id_DBCSMode, MaxComboBoxLen + 1));
  New(CheckPtr, InitResource(CustomCfgDlg, id_ModemInstalled));
  New(EditPtr, InitResource(CustomCfgDlg, id_FileName, 80));
  for i := 0 to 13 do
    New(CheckPtr, InitResource(CustomCfgDlg, 120+i));
  New(CheckPtr, InitResource(CustomCfgDlg, id_UseLF));
  New(CheckPtr, InitResource(CustomCfgDlg, id_UseMicroScan));
  New(CheckPtr, InitResource(CustomCfgDlg, id_UseAstm));
  New(CheckPtr, InitResource(CustomCfgDlg, id_UseXonXoff));
  New(CheckPtr, InitResource(CustomCfgDlg, id_FilterMulti));
	CustomCfgDlg^.TransferBuffer := @CustomCfgBuff;
  Application^.ExecDialog(CustomCfgDlg);
end;

Procedure TCustomCfg.InitComboBoxLists;
{Initialize Combobox lists}
var
  i: Integer;
  TempMsg: array[0..30] of char;

begin
  with CustomCfgBuff do
  begin
    CustomCfgBuff.CstmParmsCfg[0].ComboStrings := New(PStrCollection, Init(NoOfPortParms+1, 1));
    for i := 0 to NoOfPortParms do
      CstmParmsCfg[0].ComboStrings^.AtInsert(i, StrNew(PortArray[i]));
    LoadString(HInstance, str_IntBase+str_File, TempMsg, SizeOf(TempMsg));
    CstmParmsCfg[0].ComboStrings^.AtInsert(i+1, StrNew(TempMsg));

    CstmParmsCfg[1].ComboStrings := New(PStrCollection, Init(NoOfBaudParms+1, 1));
    for i := 0 to NoOfBaudParms do
      CstmParmsCfg[1].ComboStrings^.AtInsert(i, StrNew(BaudArray[i]));

    CstmParmsCfg[2].ComboStrings := New(PStrCollection,
      Init(NoOfParityParms+1, 1));
    for i := 0 to NoOfParityParms do
    begin
      LoadString(HInstance, str_IntBase+str_Parity+i, TempMsg, SizeOf(TempMsg));
      CstmParmsCfg[2].ComboStrings^.AtInsert(i, StrNew(TempMsg));
    end;

    CstmParmsCfg[3].ComboStrings := New(PStrCollection,
      Init(NoOfStopBitParms+1, 1));
    for i := 0 to NoOfStopBitParms do
      CstmParmsCfg[3].ComboStrings^.AtInsert(i, StrNew(StopBitArray[i]));

    CstmParmsCfg[4].ComboStrings := New(PStrCollection,
      Init(NoOfDataBitParms+1, 1));
    for i := 0 to NoOfDataBitParms do
      CstmParmsCfg[4].ComboStrings^.AtInsert(i, StrNew(DataBitArray[i]));

    DBCSModeCfg.ComboStrings := New(PStrCollection, Init(3, 1));
    LoadString(HInstance, str_IntBase+str_None, TempMsg, SizeOf(TempMsg));
    DBCSModeCfg.ComboStrings^.AtInsert(0, StrNew(TempMsg));
    LoadString(HInstance, str_IntBase+str_JIS, TempMsg, SizeOf(TempMsg));
    DBCSModeCfg.ComboStrings^.AtInsert(1, StrNew(TempMsg));
    LoadString(HInstance, str_IntBase+str_ShiftJIS, TempMsg, SizeOf(TempMsg));
    DBCSModeCfg.ComboStrings^.AtInsert(2, StrNew(TempMsg));

    ConvModeCfg.ComboStrings := New(PStrCollection, Init(2, 1));
    LoadString(HInstance, str_IntBase+str_None, TempMsg, SizeOf(TempMsg));
    ConvModeCfg.ComboStrings^.AtInsert(0, StrNew(TempMsg));
    LoadString(HInstance, str_IntBase+str_SISO, TempMsg, SizeOf(TempMsg));
    ConvModeCfg.ComboStrings^.AtInsert(1, StrNew(TempMsg));

    LogFileCfg.ComboStrings := New(PStrCollection, Init(NoOfLogFileParms+1, 1));
    for i := 0 to NoOfLogFileParms do
    begin
      LoadString(HInstance, str_IntBase+str_LevName+i, TempMsg, SizeOf(TempMsg));
      LogFileCfg.ComboStrings^.AtInsert(i, StrNew(TempMsg));
    end;
	end;
end;

Procedure TCustomCfg.WriteCustomCfgBuff;
var
  LastCursor: HCursor;
  TStr: array [0..1] of Char;
  Index, i, Result: integer;
  AStr, XStr: array[0..3] of Char;

begin
  LastCursor := SetCursor(LoadCursor(0, idc_Wait));
  with CustomCfgBuff do
  begin
    {Save parameter values in .ini file}
    for i := 0 to NoOfCstmParms-1 do
      WritePrivateProfileString('IntCstm', CstmParmNames[i],
        CstmParmsCfg[i].ComboSelection, 'IntCstm.ini');
		{Save log/timout values in .ini file}
    WritePrivateProfileString('IntCstm', 'LogFile', LogFileCfg.ComboSelection,
      'IntCstm.ini');
    Index := SendDlgItemMessage(CustomCfgDlg^.HWindow, id_LogFile, cb_FindString,
      word(-1), longint(@LogFileCfg.ComboSelection));
    WritePrivateProfileString('IntCstm', 'LogLevel', LogLevelArray[Index],
      'IntCstm.ini');
    StrCopy(LogFile^.LogRecordingLevel, LogLevelArray[Index]);
    Val(LogFile^.LogRecordingLevel, LogFile^.CurrLogLevel, Result);
    WritePrivateProfileString('IntCstm', 'Timeout', TimeoutCfg, 'IntCstm.ini');

    WritePrivateProfileString('IntCstm', 'Conv', ConvModeCfg.ComboSelection,
			'IntCstm.ini');
    WritePrivateProfileString('IntCstm', 'DBCS', DBCSModeCfg.ComboSelection, 'IntCstm.ini');

    {Save checkbox settings in .ini file, Y = checked, N = unchecked}
    for i := 0 to 13 do
    begin
      if XRefChksCfg[i] = bf_Checked then
        StrCopy(TStr, 'Y')
      else
        StrCopy(TStr, 'N');
      WritePrivateProfileString('IntCstm', XRefTableNames[i], TStr, 'IntCstm.ini');
    end;
    if ModemInstalledChkCfg = bf_Checked then
      StrCopy(TStr, 'Y')
    else
      StrCopy(TStr, 'N');
    WritePrivateProfileString('IntCstm', 'ModemInstalled', TStr, 'IntCstm.ini');
    WritePrivateProfileString('IntCstm', 'InterfaceFile', IntFileNameCfg,
      'IntCstm.ini');
		if MicroScanChkCfg = bf_Checked then
      StrCopy(XStr, 'Yes')
    else
      StrCopy(XStr, 'No');
    WritePrivateProfileString('IntCstm', 'IntMicroScan', XStr, 'IntCstm.ini');
		if AstmChkCfg = bf_Checked then
      StrCopy(XStr, 'Yes')
    else
      StrCopy(XStr, 'No');
    WritePrivateProfileString('IntCstm', 'IntAstm', XStr, 'IntCstm.ini');
    if AstmChkCfg = bf_UnChecked then
      XonXoffChkCfg := bf_Checked
    else
      XonXoffChkCfg := bf_UnChecked;
		if XonXoffChkCfg = bf_Checked then
      StrCopy(XStr, 'Yes')
    else
      StrCopy(XStr, 'No');
    WritePrivateProfileString('IntCstm', 'IntXON', XStr, 'IntCstm.ini');
		if LFChkCfg = bf_Checked then
    begin
      StrCopy(XStr, '10');
      StrCopy(AStr, '10');
    end
    else
    begin
      StrCopy(XStr, '13');
      if AstmChkCfg = bf_Checked then
        StrCopy(AStr, '10')
      else
        StrCopy(AStr, '13');
    end;
    WritePrivateProfileString('IntCstm', 'EoRec', XStr, 'IntCstm.ini');
    WritePrivateProfileString('IntCstm', 'EoFrame', AStr, 'IntCstm.int');

    if FilterMultiChkCfg = bf_Checked
       then  StrCopy(XStr, 'Yes')
       else  StrCopy(XStr, 'No');
    WritePrivateProfileString('IntCstm', 'FilterMultiDrugs', XStr, 'IntCstm.ini');

	end;
  SetCursor(LastCursor);
end;

{
*******************
CustomCfgDlg Object
*******************
}

Procedure TCustomCfgDlg.SetupWindow;
var
  i: integer;
  ModemCheck: word;
  pstr: array[0..3] of char;

begin
  inherited SetupWindow;
	ModemCheck := CustomCfgBuff.ModemInstalledChkCfg;
	{Disable Modem for now}
	ModemCheck := bf_Unchecked;
  if ModemCheck = bf_Unchecked then
    EnableWindow(GetDlgItem(HWindow, id_CfgStrings), False);
  EnableWindow(GetDlgItem(hWindow, id_ModemInstalled), False);
  {Removed v17 (v18) support - kmr 10/95}
  EnableWindow(GetDlgItem(HWindow, id_UseMicroScan), False);
  GetPrivateProfileString('IntCstm', 'EnableUnusedXRefs', 'NO', pstr, SizeOf(pstr), 'IntCstm.ini');
  if (StrIComp(pstr, 'YES') <> 0) then
  begin
    {Disable Comment cross-reference. It is not used.}
    EnableWindow(GetDlgItem(hWindow, 126), False);
    {Disable User-Defined cross-reference. It is not used.}
    EnableWindow(GetDlgItem(hWindow, 133), False);
  end;
end;

Procedure TCustomCfgDlg.ToggleChkStrings(var Msg: TMessage);
begin
  if CustomCfgBuff.ModemInstalledChkCfg = bf_Checked then
  begin
    CustomCfgBuff.ModemInstalledChkCfg := bf_Unchecked;
    EnableWindow(GetDlgItem(HWindow, id_CfgStrings), False);
  end
  else
  begin
    CustomCfgBuff.ModemInstalledChkCfg := bf_Checked;
    EnableWindow(GetDlgItem(HWindow, id_CfgStrings), True);
  end;
end;


procedure TCustomCfgDlg.PortParamSel(var Msg: TMessage);
var
	ParamVal: array[0..10] of char;
  IOFileNamesDlg : PIOFileNamesDlg;
  TempMsg: array[0..10] of char;

begin
	if Msg.lParamHi = cbn_SelChange then
  begin
    SendDlgItemMsg(id_IntPort, wm_GetText, 10, longint(@ParamVal));
    LoadString(HInstance, str_IntBase+str_File, TempMsg, SizeOf(TempMsg));
    if StrComp(ParamVal, TempMsg) = 0 then
		begin
      IOFileNamesDlg := New(PIOFileNamesDlg, Init(@Self, 'IO_FILE_NAMES'));
    	Application^.ExecDialog(IOFileNamesDlg);
		end;
  end;
end;

{Dialog box used for modems}
Procedure TCustomCfgDlg.EditModemStrings(var Msg: TMessage);
var
  EditPtr: PEdit;
  StringsCfgDlg: PStringsCfgDlg;

begin
  StringsCfgDlg := New(PStringsCfgDlg, Init(@Self, 'MODEM_STR_CFG'));
  New(EditPtr, InitResource(StringsCfgDlg, id_InitStr, InitStrLen + 1));
  New(EditPtr, InitResource(StringsCfgDlg, id_DialStr, DialStrLen + 1));
  New(EditPtr, InitResource(StringsCfgDlg, id_HangupStr, InitStrLen + 1));
  New(EditPtr, InitResource(StringsCfgDlg, id_BreakStr, BreakStrLen + 1));
  IntCstm_InitstringsBuff;
  StringsCfgDlg^.TransferBuffer := @StringsCfgBuff;
  Application^.ExecDialog(StringsCfgDlg);
end;

Procedure TCustomCfgDlg.SaveCustomCfg(var Msg: TMessage);
var
 i: integer;

begin
  TransferData(tf_GetData);
  CustomCfg^.WriteCustomCfgBuff;
  DisposeComboBoxCollections;
	for i := 0 to 13 do
		if CustomCfgBuff.XRefChksCfg[i] = bf_UnChecked then
      EnableMenuItem(GetMenu(MainHWnd), i+120, mf_ByCommand or mf_Grayed)
		else
      EnableMenuItem(GetMenu(MainHWnd), i+120, mf_ByCommand or mf_Enabled);
	EndDlg(0);
  LogFile^.LogEvent(0, SendBit, lgevCustomEdit, '', -1);
	Restart;
end;

Procedure TCustomCfgDlg.AbortCustomCfg(var Msg: TMessage);
begin
  DisposeComboBoxCollections;
  EndDlg(0);
end;

Procedure TCustomCfgDlg.DisposeComboBoxCollections;
{Make sure all allocated collections for transfering data have been
 de-allocated}

var
  i: integer;

begin
  for i := 0 to NoOfCstmParms-1 do
    MSDisposeObj(CustomCfgBuff.CstmParmsCfg[i].ComboStrings);
  MSDisposeObj(CustomCfgBuff.LogFileCfg.ComboStrings);
  MSDisposeObj(CustomCfgBuff.ConvModeCfg.ComboStrings);
  MSDisposeObj(CustomCfgBuff.DBCSModeCfg.ComboStrings);
end;


{
*********************
IOFileNamesDlg Object
*********************
}

procedure TIOFileNamesDlg.SetupWindow;
var
	IOFileName: array[0..14] of char;

begin
  GetPrivateProfileString('IntCstm', 'IntInFile', '', IOFileName,
    SizeOf(IOFileName), 'IntCstm.ini');
  SendDlgItemMsg(id_InputFile, wm_SetText, 0, longint(@IOFileName));
  GetPrivateProfileString('IntCstm', 'IntOutFile', '', IOFileName,
    SizeOf(IOFileName), 'IntCstm.ini');
  SendDlgItemMsg(id_OutputFile, wm_SetText, 0, longint(@IOFileName));
end;

procedure TIOFileNamesDlg.SaveNames(var Msg: TMessage);
var
	IOFileName: array[0..14] of char;

begin
  SendDlgItemMsg(id_InputFile, wm_GetText, 14, longint(@IOFileName));
  WritePrivateProfileString('IntCstm', 'IntInFile', IOFileName, 'IntCstm.ini');
  SendDlgItemMsg(id_OutputFile, wm_GetText, 14, longint(@IOFileName));
  WritePrivateProfileString('IntCstm', 'IntOutFile', IOFileName, 'IntCstm.ini');
	EndDlg(0);
end;


{
********************
StringsCfgDlg Object
********************
}

Procedure TStringsCfgDlg.WritestringsBuff;
var
  LastCursor: HCursor;
  TStr: array[0..1] of Char;

begin
  LastCursor := SetCursor(LoadCursor(0, idc_Wait));
  with StringsCfgBuff do
  begin
    {Save strings in .ini file}
    WritePrivateProfileString('ModemStrings', 'InitStr', InitStrCfg, 'IntCstm.ini');
    WritePrivateProfileString('ModemStrings', 'DialStr', DialStrCfg, 'IntCstm.ini');
    WritePrivateProfileString('ModemStrings', 'HangupStr', HangupStrCfg,
      'IntCstm.ini');
    WritePrivateProfileString('ModemStrings', 'BreakStr', BreakStrCfg,
      'IntCstm.ini');
  end;
  SetCursor(LastCursor);
end;

Procedure TStringsCfgDlg.SaveStrings(var Msg: TMessage);
begin
  TransferData(tf_GetData);
  WriteStringsBuff;
  EndDlg(0);
end;

{
********************
Non-Object Functions
********************
}

Procedure IntCstm_InitStringsBuff;
begin
  {Get modem control strings from .ini file}
  FillChar(StringsCfgBuff, SizeOf(StringsCfgBuff), 0);
  with StringsCfgBuff do
  begin
    GetPrivateProfileString('ModemStrings', 'InitStr', '', InitStrCfg,
      SizeOf(InitStrCfg), 'IntCstm.ini');
    GetPrivateProfileString('ModemStrings', 'DialStr', '', DialStrCfg,
      SizeOf(DialStrCfg), 'IntCstm.ini');
    GetPrivateProfileString('ModemStrings', 'HangupStr', '', HangupStrCfg,
      SizeOf(HangupStrCfg), 'IntCstm.ini');
    GetPrivateProfileString('ModemStrings', 'BreakStr', '', BreakStrCfg,
      SizeOf(BreakStrCfg), 'IntCstm.ini');
  end;
end;

Procedure IntCstm_InitCustomCfgBuff;
var
  TStr: array[0..1] of Char;
  i, j, Result: integer;
	XStr: array[0..3] of Char;
  tempStr: array[0..50] of char;
  found: boolean;

begin
  FillChar(CustomCfgBuff, SizeOf(CustomCfgBuff), 0);
  with CustomCfgBuff do
  begin
    {Get parameters from .ini file}
    for i := 0 to NoOfCstmParms-1 do
    begin
      GetPrivateProfileString('IntCstm', CstmParmNames[i], '', CstmParmsCfg[i].
        ComboSelection, SizeOf(CstmParmsCfg[i].ComboSelection), 'IntCstm.ini');
      { The following code was put here to eliminate the need to translate the .INI file.   }
      { It defaults the parity string to the translated version of 'None' if a valid string }
      { is not found.                                                                       }
      if (i = 2) then { parity }
      begin
        found:= False;
        for j:= 0 to NoOfParityParms do
        begin
          LoadString(HInstance, str_IntBase+str_Parity+j, tempStr, SizeOf(tempStr));
          if (StrComp(tempStr, CstmParmsCfg[i].ComboSelection) = 0) then
            found:= True;
        end;
        if not found then
        begin
          LoadString( HInstance, str_IntBase+str_Parity, CstmParmsCfg[i].ComboSelection,
                      SizeOf(CstmParmsCfg[i].ComboSelection));
          WritePrivateProfileString('IntCstm', CstmParmNames[i],
                                    CstmParmsCfg[i].ComboSelection, 'IntCstm.ini');
        end;
      end;
    end;

    {Get Log/Timeout values from .ini file}
    GetPrivateProfileString('IntCstm', 'LogFile', '', LogFileCfg.ComboSelection,
      SizeOf(LogFileCfg.ComboSelection), 'IntCstm.ini');
    { The following code was put here to eliminate the need to translate the .INI file.   }
    { It defaults the log level string to the translated version of 'None' if a valid     }
    { string is not found.                                                                }
    found:= False;
    for j := 0 to NoOfLogFileParms do
    begin
      LoadString(HInstance, str_IntBase+str_LevName+j, tempStr, SizeOf(tempStr));
      if (StrComp(tempStr, LogFileCfg.ComboSelection) = 0) then
        found:= True;
    end;
    if not found then
    begin
      LoadString( HInstance, str_IntBase+str_LevName, LogFileCfg.ComboSelection,
                  SizeOf(LogFileCfg.ComboSelection));
      WritePrivateProfileString('IntCstm', 'LogFile', LogFileCfg.ComboSelection, 'IntCstm.ini');
      WritePrivateProfileString('IntCstm', 'LogLevel', LogLevelArray[0], 'IntCstm.ini');
    end;

    GetPrivateProfileString('IntCstm', 'Timeout', '', TimeoutCfg,
      SizeOf(TimeoutCfg), 'IntCstm.ini');

    GetPrivateProfileString('IntCstm', 'Conv', '', ConvModeCfg.ComboSelection,
                            SizeOf(ConvModeCfg.ComboSelection), 'IntCstm.ini');
    { The following code was put here to eliminate the need to translate the .INI file.   }
    { It defaults the double-byte conversion string to the translated version of 'None'   }
    { if a valid string is not found.                                                     }
    found:= false;
    LoadString(HInstance, str_IntBase+str_None, tempStr, SizeOf(tempStr));
    found:= found or (StrComp(tempStr, ConvModeCfg.ComboSelection) = 0);
    LoadString(HInstance, str_IntBase+str_SISO, tempStr, SizeOf(tempStr));
    found:= found or (StrComp(tempStr, ConvModeCfg.ComboSelection) = 0);
    if not found then
    begin
      LoadString( HInstance, str_IntBase+str_None, ConvModeCfg.ComboSelection,
                  SizeOf(ConvModeCfg.ComboSelection));
      WritePrivateProfileString('IntCstm', 'Conv', ConvModeCfg.ComboSelection, 'IntCstm.ini');
    end;

    GetPrivateProfileString('IntCstm', 'DBCS', '', DBCSModeCfg.ComboSelection,
                            SizeOf(DBCSModeCfg.ComboSelection), 'IntCstm.ini');
    { The following code was put here to eliminate the need to translate the .INI file.   }
    { It defaults the double-byte character code string to the translated version of      }
    { 'None' if a valid string is not found.                                              }
    found:= false;
    LoadString(HInstance, str_IntBase+str_None, tempStr, SizeOf(tempStr));
    found:= found or (StrComp(tempStr, DBCSModeCfg.ComboSelection) = 0);
    LoadString(HInstance, str_IntBase+str_JIS, tempStr, SizeOf(tempStr));
    found:= found or (StrComp(tempStr, DBCSModeCfg.ComboSelection) = 0);
    LoadString(HInstance, str_IntBase+str_ShiftJIS, tempStr, SizeOf(tempStr));
    found:= found or (StrComp(tempStr, DBCSModeCfg.ComboSelection) = 0);
    if not found then
    begin
      LoadString( HInstance, str_IntBase+str_None, DBCSModeCfg.ComboSelection,
                  SizeOf(DBCSModeCfg.ComboSelection));
      WritePrivateProfileString('IntCstm', 'DBCS', DBCSModeCfg.ComboSelection, 'IntCstm.ini');
    end;

    {Get Checkbox stati from .ini file, the values are stored as Y = checked
     and N = unchecked, once read do appropriate setting of checkbox}
		for i := 0 to 13 do
    begin
      GetPrivateProfileString('IntCstm', XRefTableNames[i], '', TStr,
        SizeOf(TStr), 'IntCstm.ini');
      if TStr[0] = 'Y' then
        XRefChksCfg[i] := bf_Checked
      else
        XRefChksCfg[i] := bf_UnChecked;
    end;
    GetPrivateProfileString('IntCstm', 'ModemInstalled', '', TStr, SizeOf(TStr),
      'IntCstm.ini');
    if TStr[0] = 'Y' then
      ModemInstalledChkCfg := bf_Checked
    else
      ModemInstalledChkCfg := bf_UnChecked;
    GetPrivateProfileString('IntCstm', 'InterfaceFile', '', IntFileNameCfg,
      SizeOf(IntFileNameCfg), 'IntCstm.ini');
    GetPrivateProfileString('IntCstm', 'EoRec', '', XStr, SizeOf(XStr),
      'IntCstm.ini');
    if StrComp(XStr, '10') = 0 then
      LFChkCfg := bf_Checked
    else
			LFChkCfg := bf_UnChecked;
    GetPrivateProfileString('IntCstm', 'IntMicroScan', '', XStr, SizeOf(XStr),
      'IntCstm.ini');
    if StrComp(XStr, 'Yes') = 0 then
      MicroScanChkCfg := bf_Checked
    else
			MicroScanChkCfg := bf_UnChecked;
    GetPrivateProfileString('IntCstm', 'IntAstm', '', XStr, SizeOf(XStr),
      'IntCstm.ini');
    if StrComp(XStr, 'Yes') = 0 then
      AstmChkCfg := bf_Checked
    else
			AstmChkCfg := bf_UnChecked;
    GetPrivateProfileString('IntCstm', 'IntXON', '', XStr, SizeOf(XStr),
      'IntCstm.ini');
    if StrComp(XStr, 'Yes') = 0 then
      XonXoffChkCfg := bf_Checked
    else
			XonXoffChkCfg := bf_UnChecked;

    GetPrivateProfileString
      ('IntCstm', 'FilterMultiDrugs', '', XStr, SizeOf(XStr), 'IntCstm.ini');
    if StrComp(XStr, 'Yes') = 0
       then FilterMultiChkCfg := bf_Checked
       else FilterMultiChkCfg := bf_UnChecked;

	end;
end;

Procedure IntCstm_ExecIntCustomDlg(CurrentWnd: HWnd; PWindowObjPtr:
   PWindowsObject);

begin
  MainHWnd := CurrentWnd;
  CustomCfg := New(PCustomCfg, Init(CurrentWnd, PWindowObjPtr));
  CustomCfg^.Run;
  MSDisposeObj(CustomCfg);
end;


Begin
End.



