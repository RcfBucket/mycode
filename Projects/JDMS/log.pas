Unit Log;

{$T-}

Interface

Uses
  WinTypes,
  Objects,
  ODialogs,
  OWindows,
  WinProcs,
  WinDos,
  PrnPrev,
  LogEvent,
  DebugDat,
  IntAstm,
  IntGlob,
	Strings;

const
  cm_PrtTodays    = 101;
  cm_PrtAll       = 102;
  cm_EraseAll     = 103;
  cm_EraseOut     = 104;
	cm_DateRange    = 106;
	cm_EraseSize    = 105;

	cm_ExitLogWnd   = 110;

  id_BegDate      = 101;
  id_EndDate      = 102;
  id_RangeOk      = 103;

  id_SizeEdit     = 101;
	id_SizeOk       = 102;

	id_LogList      = 300;

	ToDisplay       = 1;
	ToPrinter       = 2;

	ListDisplayLen  = 100;

	RecvColor       = '1';
	SendColor       = '2';
  ErrorColor      = '3';
	SendBit = 0;
	RecvBit = $80;
	ErrorBit = $40;

	{Log Error Codes}
	HPS_LogFileErr  = 1;
	HPS_TextFileErr = 2;
  HPS_DbgFileErr  = 3;
	HPS_LogTempErr  = 4;
  HPS_ListFileErr = 5;

Type
	LogFileRecType   = record
										   DateTime: longint;
										   EventType: byte;
 										   EventNum: integer;
										   TextFilePos: longint;
										 	 TextRecLen: word;
										   DebugFilePos: longint;
									   end;

	RangeCfgBuffType = record
                       BegDateCfg:  array[0..8] of Char;
                       EndDateCfg:  array[0..8] of Char;
                     end;

 	SizeCfgBuffType  = record
                       SizeCfg: array[0..4] of Char;
									   end;

	TLogDisplayLine  = record
										  	Line: array[0..ListDisplayLen+1] of char;
									   end;


{	PTempLogFiles = ^TTempLogFiles;
	TTempLogFiles = object(TObject)
    TmpFile, TmpDbgFile, TmpTxtFile: File;
		TmpBuf: array[0..255] of char;
    TxtFileUsed, DbgFileUsed: boolean;
    BegDbgPos, EndDbgPos: longint;
		constructor Init;
		function OpenTempFiles: boolean;
    procedure CloseTempFiles;
		function WriteToTmpDbgFile(var LogFileRec: LogFileRecType): boolean;
		function WriteToTmpTxtFile(var LogFileRec: LogFileRecType): boolean;
	end;
}

	PLogFileWnd = ^TLogFileWnd;
  TLogFileWnd = object(TWindow)
    LogPrint: PMFIPrintInfo;
		DisplayBufHnd: THandle;
    ColorTextType: char;
    FileEntries, DescCol: integer;
		ListFileError: boolean;
    CurrWndObj: PWindowsObject;
    WndIdxPtr, FileIdxPtr, LineHeight: longint;
 		constructor Init(ParentWndObj:PWindowsObject; WndTitle: PChar);
		procedure SetupWindow;virtual;
		procedure SetColorText(EventType: byte);virtual;
		procedure CreateListFile;virtual;
		procedure ShowBuffer(PaintDC: HDC);virtual;
		procedure Paint(PaintDC: HDC; var PaintInfo: TPaintStruct);virtual;
    procedure WMSize(var Msg: TMessage);virtual wm_First + wm_Size;
		procedure FillBuffer;virtual;
		procedure PrintTodays(var Msg: TMessage);virtual cm_First + cm_PrtTodays;
    procedure PrintAll(var Msg: TMessage);virtual cm_First + cm_PrtAll;
    procedure EraseAll(var Msg: TMessage);virtual cm_First + cm_EraseAll;
    procedure EraseOutsideRange(var Msg: TMessage);virtual cm_First + cm_EraseOut;
    procedure MaxFileEntries(var Msg: TMessage);virtual cm_First + cm_EraseSize;
    procedure EditDateRange(var Msg: TMessage);virtual cm_First + cm_DateRange;
    procedure PrintLogItems(AllItems: Boolean);virtual;
		procedure MultiLineDisplay(Line: PChar; Device: integer);virtual;
		procedure Display(Text: PChar; Device: integer);virtual;
		procedure DisplayLine(LineSeg: PChar; Device: integer);virtual;
		function  PlaceDateTimeStr(DateTimeRec: TDateTime):PChar;virtual;
    function  CompDate(CurrentDate, LogDate: PChar): Integer;virtual;
    procedure ExitLogWnd(var Msg: TMessage); virtual cm_First + cm_ExitLogWnd;
		procedure DisplayEntries(PaintDC: HDC);virtual;
		procedure DisplayEntry(LineNo: integer;TextLine: PChar;PaintDC: HDC);virtual;
		destructor Done;virtual;
  end;

  PLogRangeDlg = ^TLogRangeDlg;
  TLogRangeDlg = Object(TDialog)
    Procedure SaveRanges(var Msg: TMessage);virtual id_First + id_RangeOk;
  end;

  PLogSizeDlg = ^TLogSizeDlg;
  TLogSizeDlg = object(TDialog)
    procedure SaveSize(var Msg: TMessage);virtual id_First + id_SizeOk;
  end;

	PLogFile = ^TLogFile;
  TLogFile = object(TObject)
		LogRecordingLevel: array[0..1] of char;
		LgFileHnd: File;
		LgTextHnd: File;
		Entries: integer;
		MaxEntries: integer;
    LogActive: boolean;
		LogFileName: array[0..14] of char;
		LogTextFileName: array[0..14] of char;
		CurrLogLevel: integer;
    LogFileError, TextFileError, DbgFileError: boolean;
		constructor Init(UseLog: boolean);
		destructor Done;virtual;
		procedure ExecLogFileWnd(CurrentWnd: HWnd; WndObjPtr: PWindowsObject);virtual;
		function LogEvent(OptDateTime:longint;EventType:byte;EventNum:integer;TextFileData:PChar;
													DebugFilePos:longint): integer;virtual;
		function GetCurrDate: PChar;virtual;
    procedure ClearOldRecs; virtual;

    private
		function GetCurrTime: PChar;virtual;
    procedure DisplayLogError(ErrorNum, IOErrNum: integer);
	end;

var
	LogFile: PLogFile;

Implementation

uses
  FileIO,
  MScan;


{used for segment math for block memory}
procedure AHIncr; far; external 'KERNEL' index 114;

var
  RangeCfgBuff: RangeCfgBuffType;
  SizeCfgBuff: SizeCfgBuffType;
	LogFileWnd: PLogFileWnd;
	CurrIndex: integer;
	LogDisplayLine: TLogDisplayLine;
	LgListFile: file;
	DbgFileName: array[0..12] of char;
    TmpFile, TmpDbgFile, TmpTxtFile: File;
		TmpBuf: array[0..255] of char;
    TxtFileUsed, DbgFileUsed: boolean;
    BegDbgPos, EndDbgPos: longint;


{
*******************
TempLogFiles Object
*******************
}

procedure TempLogFiles_Init;
begin
	TxtFileUsed := False;
	DbgFileUsed := False;
	BegDbgPos := -1;
  EndDbgPos := -1;
end;

function TempLogFiles_OpenTempFiles: boolean;
var
	OpenError: boolean;
	IOErr: integer;

begin
  OpenError := False;

	{Open temp files for log entries to save}
  Assign(TmpFile, 'TEMPLOG.FIL');
  {$I-} Rewrite(TmpFile, 1); {$I+}
  IOErr := IOResult;
	if IOErr <> 0 then
		OpenError := True;
  Assign(TmpDbgFile, 'TEMPDBG.FIL');
  {$I-}Rewrite(TmpDbgFile, 1);{$I+}
  IOErr := IOResult;
  if IOErr <> 0 then
		OpenError := True;
  Assign(TmpTxtFile, 'TEMPTXT.FIL');
  {$I-}Rewrite(TmpTxtFile, 1);{$I+}
  IOErr := IOResult;
  if IOErr <> 0 then
		OpenError := True;

  TempLogFiles_OpenTempFiles := not OpenError;
end;

procedure TempLogFiles_CloseTempFiles;
begin
	Close(TmpDbgFile);
	if DbgFileUsed then
  begin
		Erase(LogDbgFile);
    Rename(TmpDbgFile, DbgFileName);
    Assign(LogDbgFile, DbgFileName);
  end;

	Close(TmpTxtFile);
	if TxtFileUsed then
  begin
   	Erase(LogFile^.LgTextHnd);
    Rename(TmpTxtFile, LogFile^.LogTextFileName);
    Assign(LogFile^.LgTextHnd, LogFile^.LogTextFileName);
  end;
end;

function TempLogFiles_WriteToTmpDbgFile(var LogFileRec: LogFileRecType): boolean;
var
  j, CharsToRead, IOErr, CharCount: longint;
  FileError: boolean;

begin
	DbgFileUsed := True;
  FileError := False;
	if BegDbgPos = -1 then
  begin
		BegDbgPos := LogFileRec.DebugFilePos;  {Save current file position and store new debug file pos = 0}
		LogFileRec.DebugFilePos := 0;
  end
	else
	begin
   	EndDbgPos := LogFileRec.DebugFilePos;  {This file pos is start of next debug block and end of previous one}
    Seek(LogDbgFile, BegDbgPos);
    CharsToRead := EndDbgPos - BegDbgPos;

    while CharsToRead > 0 do
    begin
      CharCount := CharsToRead;
      if CharCount > 255 then
        CharCount := 255;
      j := 0;
      while j <= CharCount-1 do
      begin
        {$I-}Read(LogDbgFile, TmpBuf[j]){$I+};
			  IOErr := IOResult;
			  if IOErr <> 0 then
				  FileError := True;
        Inc(j);
			end;
      {$I-}BlockWrite(TmpDbgFile, TmpBuf[0], CharCount);{$I+}  {save in temp file to save}
		  IOErr := IOResult;
		  if IOErr <> 0 then
			  FileError := True;
      CharsToRead := CharsToRead - CharCount;
    end;

		LogFileRec.DebugFilePos := FilePos(TmpDbgFile);
		BegDbgPos := EndDbgPos;  {Set next beggining block}
	end;
	TempLogFiles_WriteToTmpDbgFile := not FileError;
end;

function TempLogFiles_WriteToTmpTxtFile(var LogFileRec: LogFileRecType): boolean;
var
	FileError: boolean;
  IOErr: integer;

begin
	TxtFileUsed := True;
  FileError := False;
  Seek(LogFile^.LgTextHnd, LogFileRec.TextFilePos);
  {$I-}BlockRead(LogFile^.LgTextHnd, TmpBuf[0], LogFileRec.TextRecLen+1);{$I+}
	IOErr := IOResult;
	if IOErr <> 0 then
		FileError := True;
	LogFileRec.TextFilePos := FilePos(TmpTxtFile);
  {$I-}BlockWrite(TmpTxtFile, TmpBuf[0], LogFileRec.TextRecLen+1);{$I+}
	IOErr := IOResult;
	if IOErr <> 0 then
		FileError := True;
  TempLogFiles_WriteToTmpTxtFile := not FileError;
end;


{
**************
LogFile Object
**************
}

procedure TLogFile.DisplayLogError(ErrorNum, IOErrNum: integer);
var
	ErrorStr: array[0..3] of char;
  ErrorMsg: array[0..25] of char;
  TempMsg: array[0..30] of char;
  TempVerMsg: array[0..25] of char;

begin
  Str(ErrorNum, ErrorStr);
  LoadString(HInstance, str_IntBase+str_LogError, TempMsg, SizeOf(TempMsg));
  StrCopy(ErrorMsg, TempMsg);
  StrCat(ErrorMsg, ErrorStr);
	if IOErrNum <> 0 then
  begin
    StrCat(ErrorMsg, ', ');
    Str(IOErrNum, ErrorStr);
    StrCat(ErrorMsg, ErrorStr);
  end;
  LoadString(HInstance, str_IntBase+str_IntMsg, TempVerMsg, SizeOf(TempVerMsg));
  MessageBox(GetFocus, ErrorMsg, TempVerMsg, mb_OK);
end;

function TLogFile.GetCurrTime: PChar;
var
  H, M, S, S100: word;
  Temp: String[4];
  TStr: array[0..11] of Char;

begin
  StrCopy(TStr, '');
  GetTime(H, M, S, S100);
  Str(H, Temp);
  if H < 10 then
    Temp := '0' + Temp;
  TStr[0] := Temp[1];
  TStr[1] := Temp[2];
  TStr[2] := ':';
  Str(M, Temp);
  if M < 10 then
    Temp := '0' + Temp;
  TStr[3] := Temp[1];
  TStr[4] := Temp[2];
  TStr[5] := ':';
  Str(S, Temp);
  if S < 10 then
    Temp := '0' + Temp;
  TStr[6] := Temp[1];
  TStr[7] := Temp[2];
  TStr[8] := ':';
  Str(S100, Temp);
  if S100 < 10 then
    Temp := '0' + Temp;
  TStr[9] := Temp[1];
  TStr[10] := Temp[2];
  TStr[11] := #0;
  GetCurrTime := @TStr;
end;

function TLogFile.GetCurrDate: PChar;
var
  y, m, d, dw: word;
  Temp: String[4];
  TStr: array[0..8] of Char;

begin
  StrCopy(TStr, '');
  GetDate(y, m, d, dw);
  Str(m, Temp);
  if m < 10 then
    Temp := '0' + Temp;
  TStr[0] := Temp[1];
  TStr[1] := Temp[2];
  TStr[2] := '/';
  Str(d, Temp);
  if d < 10 then
    Temp := '0' + Temp;
  TStr[3] := Temp[1];
  TStr[4] := Temp[2];
  TStr[5] := '/';
  Str(y, Temp);
  TStr[6] := Temp[3];
  TStr[7] := Temp[4];
  TStr[8] := #0;
  GetCurrDate := TStr;
end;

constructor TLogFile.Init(UseLog: boolean);
var
 	tstr: array[0..4] of Char;
  Result, IOErr: integer;

begin
  LogActive := UseLog; {Disable logging functionality}

  GetPrivateProfileString('IntLog', 'MaxEntries', '1600', tstr, SizeOf(tstr), 'INTCSTM.INI');
  Val(tstr, MaxEntries, Result);
  GetPrivateProfileString('IntLog', 'Entries', '0', tstr, SizeOf(tstr), 'INTCSTM.INI');
  Val(tstr, Entries, Result);

  GetPrivateProfileString('IntCstm', 'LogLevel', '0', LogRecordingLevel, SizeOf(tstr), 'INTCSTM.INI');
  Val(LogRecordingLevel, CurrLogLevel, Result);

  GetPrivateProfileString('IntCstm', 'InterfaceFile', 'LOGINT.FIL', LogFileName, SizeOf(LogFileName), 'INTCSTM.INI');
  LogFileError := False;
  Assign(LgFileHnd, LogFileName);
  {$I-}Reset(LgFileHnd, 1);{$I+}
	if IOResult <> 0 then
    {$I-}Rewrite(LgFileHnd, 1);{$I+}
  IOErr := IOResult;
	if IOErr <> 0 then
  begin
		LogFileError := True;
    DisplayLogError(HPS_LogFileErr, IOErr);
  end
  else
    Seek(LgFileHnd, FileSize(LgFileHnd));

  GetPrivateProfileString('IntCstm', 'LogTextFile', 'LOGTEXT.FIL', LogTextFileName, SizeOf(LogTextFileName), 'INTCSTM.INI');
	TextFileError := False;
  Assign(LgTextHnd, LogTextFileName);
  {$I-}Reset(LgTextHnd, 1);{$I+}
	if IOResult <> 0 then
    {$I-}Rewrite(LgTextHnd, 1);{$I+}
  IOErr := IOResult;
	if IOErr <> 0 then
  begin
		TextFileError := True;
    DisplayLogError(HPS_TextFileErr, IOErr);
  end
  else
    Seek(LgTextHnd, FileSize(LgTextHnd));

  GetPrivateProfileString('IntCstm', 'LogDbgFile', 'INTDBG.FIL', DbgFileName, SizeOf(DbgFileName), 'INTCSTM.INI');
	DbgFileError := False;
  Assign(LogDbgFile, DbgFileName);
	{$I-}Reset(LogDbgFile);{$I+}
	if IOResult <> 0 then
		{$I-}Rewrite(LogDbgFile);{$I+}
  IOErr := IOResult;
	if IOErr <> 0 then
  begin
		DbgFileError := True;
    DisplayLogError(HPS_DbgFileErr, IOErr);
  end
	else
  begin
  	Debugfptr := FileSize(LogDbgFile);
    Seek(LogDbgFile, Debugfptr);
  end;
end;

procedure TLogFile.ClearOldRecs;
var
  i, IOErr: integer;
	LogFileRec: LogFileRecType;
  EntriesToClear: integer;

begin
	if LogFileError then
  	Exit;

  EntriesToClear:= 0;
  if Entries >= MaxEntries then
    EntriesToClear:= (Entries - (MaxEntries div 2))
  else
    Exit;

  Reset(LgFileHnd, 1);

	i := 1;

	TempLogFiles_Init;
	if not TempLogFiles_OpenTempFiles then
  	Exit;


  {Seek(LgFileHnd, (SizeOfLogFileRec*EntriesToClear));} {Should do same as while loop}

  while (i <= EntriesToClear) and (not Eof(LgFileHnd)) do
	begin
    Inc(i);
    {$I-}BlockRead(LgFileHnd, LogFileRec, SizeOf(LogFileRec));{$I+}
		IOErr := IOResult;
		if IOErr <> 0 then
    begin
      LogFileError := True;
			Exit;
  	end;
	end;


	Entries := 0;

	while not Eof(LgFileHnd) do
  begin
    {$I-}BlockRead(LgFileHnd, LogFileRec, SizeOf(LogFileRec));{$I+}  {Read log entry to save}
		IOErr := IOResult;
		if IOErr <> 0 then
		begin
			LogFileError := True;
			Exit;
    end;


    if LogFileRec.DebugFilePos <> -1 then   {Check if it has debug info}
			if not TempLogFiles_WriteToTmpDbgFile(LogFileRec) then
				Exit;


    if LogFileRec.TextFilePos <> -1 then  {if text info, read from text file, store in temp file and save new file pos}
			if not TempLogFiles_WriteToTmpTxtFile(LogFileRec) then
      	Exit;


    {$I-}BlockWrite(TmpFile, LogFileRec, SizeOf(LogFileRec));{$I+}  {Save new log file entry, with new}
																																								{text and dbg file pos}
		IOErr := IOResult;
		if IOErr <> 0 then
			Exit;
		Entries := Entries + 1;

	end;


	if (BegDbgPos <> FileSize(LogDbgFile)) and (DbgFileUsed) then {save last debug file info}
	begin
		LogFileRec.DebugFilePos := FileSize(LogDbgFile);
		if not TempLogFiles_WriteToTmpDbgFile(LogFileRec) then
			Exit;
  end;


	{Close files and rename temp stored data to working log file}
	Close(LgFileHnd);
	Close(TmpFile);
  Erase(LgFileHnd);
  Rename(TmpFile, LogFileName);
  Assign(LgFileHnd, LogFileName);
  Reset(LgFileHnd, 1);
  Seek(LgFileHnd, FileSize(LgFileHnd));


	Close(LogDbgFile);
	Close(LgTextHnd);

	TempLogFiles_CloseTempFiles;

	Reset(LogDbgFile);
  Seek(LogDbgFile, FileSize(LogDbgFile));

  Reset(LgTextHnd, 1);
  Seek(LgTextHnd, FileSize(LgTextHnd));

end;

function TLogFile.LogEvent(OptDateTime:longint;EventType:byte;EventNum:integer;TextFileData:PChar;
													DebugFilePos:longint): integer;
var
	DateTimeRec: TDateTime;
  DateTime: longint;
  S100, DOW: word;
	LogFileRec: LogFileRecType;
  IOErr: integer;

begin
  if not LogActive then
    Exit;
  Entries := Entries + 1;
	DateTime := OptDateTime;
	if DateTime = 0 then
	begin
		with DateTimeRec do
    begin
      GetTime(Hour, Min, Sec, S100);
      GetDate(Year, Month, Day, DOW);
		end;
    PackTime(DateTimeRec, DateTime);
  end;
	LogFileRec.DateTime := DateTime;
	LogFileRec.EventNum := EventNum;
	LogFileRec.EventType := EventType;
	if TextFileData = nil then
  begin
    LogFileRec.TextFilePos := -1;
    LogFileRec.TextRecLen := 0;
  end
	else
  begin
		if not TextFileError then
    begin
      Seek(LgTextHnd, FileSize(LgTextHnd));
			LogFileRec.TextFilePos := FilePos(LgTextHnd);
			LogFileRec.TextRecLen := StrLen(TextFileData);
      {$I-}BlockWrite(LgTextHnd, TextFileData[0], LogFileRec.TextRecLen+1);{$I+}
			IOErr := IOResult;
			if IOErr <> 0 then
			begin
				TextFileError := True;
			end;
		end;
  end;
	if not LogFileError then
  begin
    LogFileRec.DebugFilePos := DebugFilePos;
    {$I-}BlockWrite(LgFileHnd, LogFileRec, SizeOf(LogFileRec));{$I+}
    IOErr := IOResult;
    if IOErr <> 0 then
    begin
      LogFileError := True;
    end;
	end;
	LogEvent := IOErr;
end;

procedure TLogFile.ExecLogFileWnd(CurrentWnd: HWnd; WndObjPtr: PWindowsObject);
var
	peekMsg: TMsg;
  TempMsg: array[0..20] of char;

begin
  LoadString(HInstance, str_IntBase+str_LogTitle, TempMsg, SizeOf(TempMsg));
  LogFileWnd := New(PLogFileWnd, Init(WndObjPtr, TempMsg));
	Application^.MakeWindow(LogFileWnd);
	while LogFileWnd^.HWindow <> 0 do
	begin
    while PeekMessage(peekMsg, 0, 0, 0, pm_Remove) do
		begin
			TranslateMessage(peekMsg);
			DispatchMessage(peekMsg);
		end;
	end;
end;

destructor TLogFile.Done;
var
	tstr: array[0..4] of char;

begin
  Str(MaxEntries, tstr);
  WritePrivateProfileString('IntLog', 'MaxEntries', tstr, 'INTCSTM.INI');
  Str(Entries, tstr);
  WritePrivateProfileString('IntLog', 'Entries', tstr, 'INTCSTM.INI');
	Close(LgFileHnd);
	Close(LgTextHnd);
	Reset(LogDbgFile);{Make sure debug file is open before close}
	Close(LogDbgFile);
end;

{
*****************
LogFileWnd Object
*****************
}

constructor TLogFileWnd.Init(ParentWndObj:PWindowsObject; WndTitle: PChar);
var
  Pen, OldPen: HPen;
	Metrics: TTextMetric;
  LogDC: HDC;

begin
	CurrWndObj := ParentWndObj;
  inherited Init(ParentWndObj, WndTitle);
	with Attr do
  begin
    Menu := LoadMenu(HInstance, 'INT_LOG_MENU');
		Style := ws_Popup or ws_Visible or ws_OverlappedWindow or ws_Maximize
             or ws_VScroll or ws_HScroll;
    X := 80; Y := 75; W := 500; H := 300;
	end;
  Scroller := New(PScroller, Init(@Self, 1, 1, 200, 200));

  LogDC := GetDC(HWindow);
  GetTextMetrics(LogDC, Metrics);
	with Metrics do
  begin
		LineHeight := tmHeight + tmInternalLeading + tmExternalLeading;
		DescCol := tmAveCharWidth * 19;
	end;
  ReleaseDC(HWindow, LogDC);
	Scroller^.YLine := LineHeight;
	Scroller^.TrackMode := False;
	ListFileError := False;

  WndIdxPtr := 0;
  FileIdxPtr := 0;
end;

procedure TLogFileWnd.ExitLogWnd(var Msg: TMessage);
begin
  CLoseWindow;
end;

destructor TLogFileWnd.Done;
begin
  inherited Done;
  Seek(LogDbgFile, FileSize(LogDbgFile));
{	GlobalFree(DisplayBufHnd);
}
end;

procedure TLogFileWnd.WMSize(var Msg: TMessage);
var
	ClientRect: TRect;

begin
  inherited WMSize(Msg);
	if not (Msg.WParam = sizeIconic) then
	begin
    GetClientRect(HWindow, ClientRect);
		with ClientRect do
      Scroller^.SetRange((Right - Left), (LineHeight * FileEntries) - (Bottom - Top));
    InvalidateRect(HWindow, nil, True);
	end;
end;

procedure TLogFileWnd.Paint(PaintDC: HDC; var PaintInfo: TPaintStruct);
begin
  inherited Paint(PaintDC, PaintInfo);
  DisplayEntries(PaintDC);
{	ShowBuffer(PaintDC);
}
end;

procedure TLogFileWnd.DisplayEntries(PaintDC: HDC);
const
	MaxDispLines = 75;
	PreWndLines = 25;

var
  EntriesToDisplay, LineNo, i, IOErr: integer;
	TextLine: array[0..ListDisplayLen+1] of char;
  tFilePtr: longint;
	tYPos: longint;

begin
	tYPos := Scroller^.YPos;
	if tYPos = 0 then
		WndIdxPtr := 0
  else
		WndIdxPtr := tYPos div LineHeight;

	if WndIdxPtr <= PreWndLines then
		WndIdxPtr := 0
	else
		WndIdxPtr := WndIdxPtr - PreWndLines;


	FileIdxPtr := WndIdxPtr*(ListDisplayLen+2);



  EntriesToDisplay := FileEntries - WndIdxPtr;

	if EntriesToDisplay > MaxDispLines then
		EntriesToDisplay := MaxDispLines;

	if ListFileError then
  	Exit;
  {$I-}Reset(LgListFile, 1);{$I+}
	IOErr := IOResult;
	if IOErr <> 0 then
  begin
		ListFileError := True;
    LogFile^.DisplayLogError(HPS_ListFileErr, IOErr);
	end;

  LineNo := WndIdxPtr;
	tFilePtr := FileIdxPtr;
	for i := 0 to EntriesToDisplay-1 do
  begin
    Seek(LgListFile, tFilePtr);
    {$I-}BlockRead(LgListFile, TextLine, ListDisplayLen+1);{$I+}
		TextLine[ListDisplayLen+1] := #0;
		IOErr := IOResult;
		if IOErr <> 0 then
  	begin
			ListFileError := True;
      LogFile^.DisplayLogError(HPS_ListFileErr, IOErr);
			Exit;
		end;
    DisplayEntry(LineNo, TextLine, PaintDC);
		tFilePtr := tFilePtr + ListDisplayLen + 2;
    Inc(LineNo);
  end;
	Close(LgListFile);
end;

procedure TLogFileWnd.DisplayEntry(LineNo: integer;TextLine: PChar;PaintDC: HDC);
var
	DispLine: array[0..(ListDisplayLen-18)] of char;
	DateStr: array[0..18] of char;
  Y, j, k: integer;

begin
	Y := (LineNo * LineHeight) + 4;
  SetTextColor(PaintDC, GetSysColor(color_GrayText));
  StrLCopy(DateStr, TextLine, 18);
  TextOut(PaintDC, 0, Y, DateStr, 18);
	case TextLine[ListDisplayLen] of
    RecvColor: SetTextColor(PaintDC, GetSysColor(color_WindowText));
    SendColor: SetTextColor(PaintDC, GetSysColor(color_GrayText));
    ErrorColor: SetTextColor(PaintDC, RGB(127, 0, 0));
  end;
	j := 0;
	for k := 18 to ListDisplayLen-1 do
 	begin
		DispLine[j] := TextLine[k];
		Inc(j);
	end;
	DispLine[j] := #0;
  TextOut(PaintDC, DescCol, Y, DispLine, StrLen(DispLine));
end;

procedure TLogFileWnd.ShowBuffer(PaintDC: HDC);
{No longer used}
type
	LongType = record
		case word of
			0: (Ptr: Pointer);
			1: (Long: longint);
			2: (Lo: word;
					Hi: word);
		end;

var
  i, j, k, Y: integer;
	DispLine: array[0..(ListDisplayLen-18)] of char;
	DateStr: array[0..18] of char;
  Start, FromAddr, Log: LongType;

begin
	Y := 4;
  i := 1;
  Start.Long := 0;
  Log.Ptr := GlobalLock(DisplayBufHnd);
	if Log.Ptr <> nil then
  begin
		while i <= (FileEntries) do
		begin
			FromAddr.Hi := Log.Hi + (Start.Hi * Ofs(AHIncr));
      FromAddr.Lo := Start.Lo;
      SetTextColor(PaintDC, RGB(65, 37, 31){GetSysColor(color_GrayText)});
      StrLCopy(DateStr, PChar(FromAddr.Ptr), 18);
      TextOut(PaintDC, 0, Y, DateStr, 18);
			case PChar(FromAddr.Ptr)[ListDisplayLen] of
        RecvColor: SetTextColor(PaintDC, GetSysColor(color_WindowText));
        SendColor: SetTextColor(PaintDC, RGB(65, 37, 31){GetSysColor(color_GrayText)});
        ErrorColor: SetTextColor(PaintDC, RGB(127, 0, 0));
    	end;
			j := 0;
			for k := 18 to ListDisplayLen-1 do
    	begin
				DispLine[j] := PChar(FromAddr.Ptr)[k];
				Inc(j);
			end;
			DispLine[j] := #0;
      TextOut(PaintDC, DescCol, Y, DispLine, StrLen(DispLine));
			Y := Y + LineHeight;
			Start.Long := Start.Long + ListDisplayLen + 2;
			Inc(i);
		end;
		GlobalUnlock(DisplayBufHnd);
	end;
end;

procedure TLogFileWnd.FillBuffer;
{No longer used}
type
	LongType = record
		case word of
			0: (Ptr: Pointer);
			1: (Long: longint);
			2: (Lo: word;
					Hi: word);
		end;

var
  FileByteSize, Count: longint;
  Start, ToAddr, Log: LongType;
  IOErr: integer;
  TempMsg: array[0..55] of char;
  TempVerMsg: array[0..25] of char;

begin
	if ListFileError then
  	Exit;
  {$I-}Reset(LgListFile, 1);{$I+}
	IOErr := IOResult;
	if IOErr <> 0 then
  begin
		ListFileError := True;
    LogFile^.DisplayLogError(HPS_ListFileErr, IOErr);
	end;
  Seek(LgListFile, 0);
	FileByteSize := (FileEntries)*(ListDisplayLen+2);
  DisplayBufHnd := GlobalAlloc(gmem_Moveable or gmem_Zeroinit, FileByteSize);
	if DisplayBufHnd <> 0 then
	begin
		Start.Long := 0;
		Count := FileByteSize;
		Log.Ptr := GlobalLock(DisplayBufHnd);
		if Log.Ptr <> nil then
		begin
			while Count > 0 do
			begin
				ToAddr.Hi := Log.Hi + (Start.Hi * Ofs(AHIncr));
				ToAddr.Lo := Start.Lo;
				if Count > $4000 then
					Count := $4000;
        {$I-}BlockRead(LgListFile, ToAddr.Ptr^, Count);{$I+}
        IOErr := IOResult;
				if IOErr <> 0 then
        begin
					ListFileError := True;
          LogFile^.DisplayLogError(HPS_ListFileErr, IOErr);
					Exit;
    		end;
				Start.Long := Start.Long + Count;
				Count := FileByteSize - Start.Long;
			end;
      GlobalUnlock(DisplayBufHnd);
    end;
  end
	else
  begin
    LoadString(HInstance, str_IntBase+str_LogMem, TempMsg, SizeOf(TempMsg));
    LoadString(HInstance, str_IntBase+str_IntMsg, TempVerMsg, SizeOf(TempVerMsg));
    MessageBox(HWindow, TempMsg, TempVerMsg, mb_OK);
  end;
	Close(LgListFile);
end;

procedure TLogFileWnd.SetupWindow;
var
  LastCursor: HCursor;
begin
  inherited SetupWindow;
  LastCursor := SetCursor(LoadCursor(0, idc_Wait));
  LogFile^.ClearOldRecs;
  CreateListFile;
  SetCursor(LastCursor);
{  FillBuffer;
}
end;

procedure TLogFileWnd.DisplayLine(LineSeg: PChar; Device: integer);
var
  Result, TempDispType, i, j, TextLen, IOErr: integer;
	PrintLine: array[0..ListDisplayLen] of char;

begin
	if Device = ToDisplay then
  begin
    StrCopy(LogDisplayLine.Line, LineSeg);
    TextLen := StrLen(LogDisplayLine.Line);
		for i := 0 to (ListDisplayLen-TextLen) do
			LogDisplayLine.Line[TextLen+i] := ' ';
		LogDisplayLine.Line[ListDisplayLen] := ColorTextType;
		LogDisplayLine.Line[ListDisplayLen+1] := #0;
    Inc(FileEntries);
    {$I-}BlockWrite(LgListFile, LogDisplayLine.Line, SizeOf(LogDisplayLine));{$I+}
		IOErr := IOResult;
		if IOErr <> 0 then
		begin
			ListFileError := True;
      LogFile^.DisplayLogError(HPS_ListFileErr, IOErr);
    end;
	end
	else
  begin
		if LineSeg[ListDisplayLen] = RecvColor then
		begin
			LineSeg[17] := '*';
    end;
    StrLCopy(PrintLine, LineSeg, ListDisplayLen);
    LogPrint^.ListLn(PrintLine);
  end;
end;

procedure TLogFileWnd.MultiLineDisplay(Line: PChar; Device: integer);
var
  FirstSeg, SecondSeg, ThirdSeg: array[0..ListDisplayLen] of char;
  FourthSeg: array[0..30] of char;
  LineLen, SegLen: integer;
  i, j: integer;

begin
  StrCopy(FirstSeg, '');
  StrCopy(SecondSeg, '');
  StrCopy(ThirdSeg, '');
  StrCopy(FourthSeg, '');
	LineLen := StrLen(Line);
	j := 0;
	for i := 0 to ListDisplayLen-1 do
 	begin
		FirstSeg[j] := Line[i];
   	Inc(j);
	end;
	FirstSeg[j] := #0;
	j := 0;
	if LineLen-ListDisplayLen > ListDisplayLen then
		SegLen := ListDisplayLen
	else
		SegLen := LineLen-ListDisplayLen;
	for i := ListDisplayLen to ListDisplayLen+SegLen do
	begin
		SecondSeg[j] := Line[i];
    Inc(j);
  end;
	if SegLen > ListDisplayLen then
  begin
		j := 0;
		if LineLen-(ListDisplayLen*2) > ListDisplayLen then
			SegLen := ListDisplayLen
		else
			SegLen := LineLen-(ListDisplayLen);
		for i := (ListDisplayLen*2) to (ListDisplayLen*2)+SegLen do
		begin
			ThirdSeg[j] := Line[i];
    	Inc(j);
		end;
		if SegLen > ListDisplayLen then
    begin
			j := 0;
			for i := (ListDisplayLen*2)+SegLen to LineLen do
			begin
				FourthSeg[j] := Line[i];
    		Inc(j);
			end;
  	end;
  end;
  if StrComp(FourthSeg, '') <> 0 then
    DisplayLine(FourthSeg, Device);
  if StrComp(ThirdSeg, '') <> 0 then
    DisplayLine(ThirdSeg, Device);
  DisplayLine(SecondSeg, Device);
  DisplayLine(FirstSeg, Device);
end;

procedure TLogFileWnd.Display(Text: PChar; Device: integer);
begin
	if StrLen(Text) > ListDisplayLen then
    MultiLineDisplay(Text, Device)
  else
    DisplayLine(Text, Device);
end;

function TLogFileWnd.PlaceDateTimeStr(DateTimeRec: TDateTime):PChar;
var
  MonthS, DayS, SYearS, HourS, MinS, SecS: array[0..2] of char;
	LYearS: array[0..4] of char;
  DTStr: array[0..17] of char;

begin
	with DateTimeRec do
  begin
    StrCopy(DTStr, '');
    Str(Month, MonthS);
		if Month < 10 then
      StrCat(DTStr, '0');
    StrCat(DTStr, MonthS);
    StrCat(DTStr, '/');
    Str(Day, DayS);
		if Day < 10 then
      StrCat(DTStr, '0');
    StrCat(DTStr, DayS);
    StrCat(DTStr, '/');
    Str(Year, LYearS);
		SYearS[0] := LYearS[2];
		SYearS[1] := LYearS[3];
    SYearS[2] := #0;
    StrCat(DTStr, SYearS);
    StrCat(DTStr, ' ');
    Str(Hour, HourS);
		if Hour < 10 then
      StrCat(DTStr, '0');
    StrCat(DTStr, HourS);
    StrCat(DTStr, ':');
    Str(Min, MinS);
		if Min < 10 then
      StrCat(DTStr, '0');
    StrCat(DTStr, MinS);
    StrCat(DTStr, ':');
    Str(Sec, SecS);
		if Sec < 10 then
      StrCat(DTStr, '0');
    StrCat(DTStr, SecS);
	end;
	PlaceDateTimeStr := DTStr;
end;

procedure TLogFileWnd.SetColorText(EventType: byte);
begin
	case EventType of
		SendBit: ColorTextType := SendColor;
		RecvBit: ColorTextType := RecvColor;
	 	ErrorBit: ColorTextType := ErrorColor;
  end;
end;

procedure TLogFileWnd.CreateListFile;
var
  Line, TextData: array[0..255] of char;
  FilePtr: longint;
	LastCursor: HCursor;
	LogFileRec: LogFileRecType;
	DateTimeRec: TDateTime;
  CharsToGet, IOErr: integer;
  CharsRead, TempLines, LinesToDisplay: longint;

begin
	if LogFile^.LogFileError then
  	Exit;
	Debugfptr := FileSize(LogDbgFile);
  Assign(LgListFile, 'LOGLIST.FIL');
  {$I-}Rewrite(LgListFile, 1);{$I+}
	IOErr := IOResult;
	if IOErr <> 0 then
  begin
		ListFileError := True;
    LogFile^.DisplayLogError(HPS_ListFileErr, IOErr);
    Exit;
	end;
	FileEntries := 0;
  LastCursor := SetCursor(LoadCursor(0, idc_Wait));
  {$I-}Reset(LogFile^.LgFileHnd, 1);{$I+}
	IOErr := IOResult;
	if IOErr <> 0 then
	begin
		LogFile^.LogFileError := True;
    LogFile^.DisplayLogError(HPS_LogFileErr, IOErr);
  	Exit;
  end;
	FilePtr := FileSize(LogFile^.LgFileHnd)-longint(SizeOf(LogFileRec));
	while FilePtr >= longint(0) do
  begin
    Seek(LogFile^.LgFileHnd, FilePtr);
    {$I-}BlockRead(LogFile^.LgFileHnd, LogFileRec, SizeOf(LogFileRec));{$I+}
		IOErr := IOResult;
		if IOErr <> 0 then
		begin
			LogFile^.LogFileError := True;
      LogFile^.DisplayLogError(HPS_LogFileErr, IOErr);
  		Exit;
  	end;
		SetColorText(LogFileRec.EventType);
    UnPackTime(LogFileRec.DateTime, DateTimeRec);
    StrCopy(Line, PlaceDateTimeStr(DateTimeRec));
    StrCat(Line, ' ');
    StrCat(Line, LogEvent_GetEventDescription(LogFileRec.EventNum));
    if LogFileRec.DebugFilePos <> -1 then
    begin
      CharsRead := Debugfptr - LogFileRec.DebugFilePos;
			TempLines := CharsRead div (ListDisplayLen-20);
			if CharsRead = ListDisplayLen-20 then
				Dec(TempLines);
      LinesToDisplay := TempLines;
			if CharsRead mod (ListDisplayLen-20) > 0 then
      	Inc(LinesToDisplay);
			CharsToGet := CharsRead - (TempLines * (ListDisplayLen-20));
			while CharsRead > 0 do
    	begin
				Debugfptr :=	Debugfptr - CharsToGet;
        Seek(LogDbgFile, Debugfptr);
        StrCopy(Line, PlaceDateTimeStr(DateTimeRec));
        StrCat(Line, ' ');
        StrCat(Line, DebugDat_GetDebugData(CharsToGet));
        DisplayLine(Line, ToDisplay);
        Dec(LinesToDisplay);
				CharsRead := CharsRead - CharsToGet;
				CharsToGet := (ListDisplayLen-20);
			end;
      if (StrLen(LogEvent_GetEventDescription(LogFileRec.EventNum)) > 0) then
      begin
        StrCopy(Line, PlaceDateTimeStr(DateTimeRec));
        StrCat(Line, ' ');
        StrCat(Line, LogEvent_GetEventDescription(LogFileRec.EventNum));
        Display(Line, ToDisplay);
      end;
    end
		else
		begin
			if LogFileRec.TextFilePos <> -1 then
      begin
        Seek(LogFile^.LgTextHnd, LogFileRec.TextFilePos);
        {$I-}BlockRead(LogFile^.LgTextHnd, TextData[0], LogFileRec.TextRecLen+1);{$I+}
				IOErr := IOResult;
				if IOErr <> 0 then
				begin
					LogFile^.TextFileError := True;
          LogFile^.DisplayLogError(HPS_TextFileErr, IOErr);
  				Exit;
  			end;
        StrCat(Line, TextData);
      end;
      Display(Line, ToDisplay);
    end;
    FilePtr := FilePtr - longint(SizeOf(LogFileRec));
  end;
  Seek(LogFile^.LgFileHnd, FileSize(LogFile^.LgFileHnd));
	SetCursor(LastCursor);
	Close(LgListFile);
end;

function TLogFileWnd.CompDate(CurrentDate, LogDate: PChar): Integer;
{Compare two dates and return 0 if equal, -1 if less than and 1 if
 greater than.
}
var
  Val1, Val2: array[0..2] of char;
  i, j, Result, Stat: integer;

begin
  Stat := 0;
  {Get year}
  j := 6;
  for i := 0 to 1 do
  begin
    Val1[i] := CurrentDate[j];
    Val2[i] := LogDate[j];
    j := j + 1;
  end;
  Val1[2] := #0;
  Val2[2] := #0;
  Val(Val1, i, Result);
  Val(Val2, j, Result);
  if i < j then Stat := -1;
  if i > j then Stat := 1;
  {Get month}
  if Stat = 0 then
  begin
    for i := 0 to 1 do
    begin
      Val1[i] := CurrentDate[i];
      Val2[i] := LogDate[i];
    end;
    Val1[2] := #0;
    Val2[2] := #0;
    Val(Val1, i, Result);
    Val(Val2, j, Result);
    if i < j then Stat := -1;
    if i > j then Stat := 1;
  end;
  {Get day}
  if Stat = 0 then
  begin
    j := 3;
    for i := 0 to 1 do
    begin
      Val1[i] := CurrentDate[j];
      Val2[i] := LogDate[j];
      j := j + 1;
    end;
    Val1[2] := #0;
    Val2[2] := #0;
    Val(Val1, i, Result);
    Val(Val2, j, Result);
    if i < j then Stat := -1;
    if i > j then Stat := 1;
  end;
  CompDate := Stat;
end;

procedure TLogFileWnd.PrintLogItems(AllItems: Boolean);
var
	Line: array[0..ListDisplayLen+1] of char;
  LogDate, CurrentDate: array[0..MaxDateLen] of char;
  FilePtr: longint;
  DateFound, IOErr: integer;
	LogFileRec: LogFileRecType;
  DateTimeRec: TDateTime;
  pd: PPrnPrevDlg;
  title: array[0..50] of char;

begin
  LoadString(HInstance, str_IntBase+str_LogTitle, title, SizeOf(title));
  LogPrint := New(PMFIPrintInfo, Init(title));
  LogFile^.LogEvent(0, SendBit, lgevPrintLog, nil, -1);
  DateFound := 0;
  StrCopy(CurrentDate, LogFile^.GetCurrDate);
  {$I-}Reset(LgListFile, 1);{$I+}
	IOErr := IOResult;
	if IOErr <> 0 then
	begin
		ListFileError := True;
    LogFile^.DisplayLogError(HPS_ListFileErr, IOErr);
 		Exit;
 	end;
	while (not Eof(LgListFile)) and (DateFound <> 1) do
  begin
		if not ListFileError then
    begin
      {$I-}BlockRead(LgListFile, Line, ListDisplayLen+2);{$I+}
			IOErr := IOResult;
			if IOErr <> 0 then
			begin
				ListFileError := True;
        LogFile^.DisplayLogError(HPS_ListFileErr, IOErr);
			end;
      StrLCopy(LogDate, Line, 8);
			if not AllItems then
        DateFound := CompDate(CurrentDate, LogDate);
			if DateFound = 0 then
        DisplayLine(Line, ToPrinter);
  	end;
	end;
  Close(LgListFile);
  pd:= New(PPrnPrevDlg, Init(@self, LogPrint, title));
  application^.ExecDialog(pd);
  MSDisposeObj(LogPrint);
end;

procedure TLogFileWnd.PrintTodays(var Msg: TMessage);
begin
  PrintLogItems(False);
end;

procedure TLogFileWnd.PrintAll(var Msg: TMessage);
begin
  PrintLogItems(True);
end;

procedure TLogFileWnd.EraseAll(var Msg: TMessage);
var
	LogFileRec: LogFileRecType;
  IOErr: integer;

begin
	LogFile^.Entries := 0;
	Close(LogFile^.LgFileHnd);
  {$I-}Rewrite(LogFile^.LgFileHnd, 1);{$I+}
	IOErr := IOResult;
	if IOErr <> 0 then
	begin
		LogFile^.LogFileError := True;
    LogFile^.DisplayLogError(HPS_LogFileErr, IOErr);
	end;
	Close(LogFile^.LgTextHnd);
  {$I-}Rewrite(LogFile^.LgTextHnd, 1);{$I+}
	IOErr := IOResult;
	if IOErr <> 0 then
	begin
		LogFile^.TextFileError := True;
    LogFile^.DisplayLogError(HPS_TextFileErr, IOErr);
	end;
	Close(LogDbgFile);
	{$I-}Rewrite(LogDbgFile);{$I+}
	IOErr := IOResult;
	if IOErr <> 0 then
	begin
		LogFile^.DbgFileError := True;
    LogFile^.DisplayLogError(HPS_DbgFileErr, IOErr);
	end;
  LogFile^.LogEvent(0, SendBit, lgevEraseLog, nil, -1);
	CreateListFile;
	FillBuffer;
  InvalidateRect(HWindow, nil, True);
end;

procedure TLogFileWnd.EraseOutsideRange(var Msg: TMessage);
var
  DateTimeStr: array[0..17] of char;
  DateFound, i, IOErr: integer;
  LogDate, CurrentDate: array[0..MaxDateLen] of char;
	LogFileRec: LogFileRecType;
	DateTimeRec: TDateTime;
	RangeStr: array[0..22] of char;
  NoMatch: boolean;

begin
	if not LogFile^.LogFileError then
		Exit;
  Reset(LogFile^.LgFileHnd, 1);

  TempLogFiles_Init;
	if not TempLogFiles_OpenTempFiles then
  	Exit;

	LogFile^.Entries := 0;

  NoMatch := True;
  DateFound := 1;

  GetPrivateProfileString('IntLog', 'BegDate', '', CurrentDate, SizeOf(CurrentDate), 'INTCSTM.INI');
  StrCopy(RangeStr, CurrentDate);
  StrCat(RangeStr, '-');
	while (not Eof(LogFile^.LgFileHnd)) and (DateFound = 1) do
  begin
    {$I-}BlockRead(LogFile^.LgFileHnd, LogFileRec, SizeOf(LogFileRec));{$I+}
		IOErr := IOResult;
		if IOErr <> 0 then
  	begin
			LogFile^.LogFileError := True;
      LogFile^.DisplayLogError(HPS_LogFileErr, IOErr);
			Exit;
  	end;
    UnPackTime(LogFileRec.DateTime, DateTimeRec);
    StrCopy(DateTimeStr, PlaceDateTimeStr(DateTimeRec));
    StrLCopy(LogDate, DateTimeStr, 8);
    DateFound := CompDate(LogDate, CurrentDate);
	end;

  GetPrivateProfileString('IntLog', 'EndDate', '', CurrentDate, SizeOf(CurrentDate), 'INTCSTM.INI');
  StrCat(RangeStr, CurrentDate);
  StrCat(RangeStr, '.');
  DateFound := CompDate(CurrentDate, LogDate);


	while (not Eof(LogFile^.LgFileHnd)) and (not DateFound = 1) do
  begin

 		if LogFileRec.DebugFilePos <> -1 then   {Check if it has debug info}
			if not TempLogFiles_WriteToTmpDbgFile(LogFileRec) then
				Exit;


    if LogFileRec.TextFilePos <> -1 then  {if text info, read from text file, store in temp file and save new file pos}
			if not TempLogFiles_WriteToTmpTxtFile(LogFileRec) then
      	Exit;


    {$I-}BlockWrite(TmpFile, LogFileRec, SizeOf(LogFileRec));{$I+}  {Save new log file entry, with new}
																																								{text and dbg file pos}
		IOErr := IOResult;
		if IOErr <> 0 then
			Exit;


    {$I-}BlockRead(LogFile^.LgFileHnd, LogFileRec, SizeOf(LogFileRec));{$I+}
		IOErr := IOResult;
		if IOErr <> 0 then
  	begin
			LogFile^.LogFileError := True;
      LogFile^.DisplayLogError(HPS_LogFileErr, IOErr);
			Exit;
  	end;

    UnPackTime(LogFileRec.DateTime, DateTimeRec);
    StrCopy(DateTimeStr, PlaceDateTimeStr(DateTimeRec));
    StrLCopy(LogDate, DateTimeStr, 8);
    DateFound := CompDate(CurrentDate, LogDate);
		Inc(LogFile^.Entries);
    NoMatch := False;
	end;


	if (Eof(LogFile^.LgFileHnd)) and (not DateFound = 1) and (not NoMatch) then
  begin
		if LogFileRec.DebugFilePos <> -1 then   {Check if it has debug info}
			if not TempLogFiles_WriteToTmpDbgFile(LogFileRec) then
				Exit;

    if LogFileRec.TextFilePos <> -1 then  {if text info, read from text file, store in temp file and save new file pos}
			if not TempLogFiles_WriteToTmpTxtFile(LogFileRec) then
      	Exit;
	end;



	if (BegDbgPos <> FileSize(LogDbgFile)) and (DbgFileUsed) then {save last debug file info}
	begin
		LogFileRec.DebugFilePos := FileSize(LogDbgFile);
		if not TempLogFiles_WriteToTmpDbgFile(LogFileRec) then
			Exit;
	end;

	{Close files and rename temp stored data to working log file}
	Close(LogFile^.LgFileHnd);
	Close(TmpFile);
	Erase(LogFile^.LgFileHnd);
  Rename(TmpFile, LogFile^.LogFileName);
  Assign(LogFile^.LgFileHnd, LogFile^.LogFileName);
  Reset(LogFile^.LgFileHnd, 1);
  Seek(LogFile^.LgFileHnd, FileSize(LogFile^.LgFileHnd));


	Close(LogDbgFile);
	Close(LogFile^.LgTextHnd);

  TempLogFiles_CloseTempFiles;


	Reset(LogDbgFile);
  Seek(LogDbgFile, FileSize(LogDbgFile));

  Reset(LogFile^.LgTextHnd, 1);
  Seek(LogFile^.LgTextHnd, FileSize(LogFile^.LgTextHnd));

  LogFile^.LogEvent(0, SendBit, lgevEraseLog, RangeStr, -1);
	CreateListFile;
  InvalidateRect(HWindow, nil, True);
end;

procedure TLogFileWnd.MaxFileEntries(var Msg: TMessage);
var
  EditPtr: PEdit;
	LogSizeDlg: PLogSizeDlg;
  Result: integer;

begin
  LogSizeDlg := New(PLogSizeDlg, Init(@Self, 'INT_LOG_SIZE'));
  New(EditPtr, InitResource(LogSizeDlg, id_SizeEdit, 4+1));
  FillChar(SizeCfgBuff, SizeOf(SizeCfgBuff), 0);
  Str(LogFile^.MaxEntries, SizeCfgBuff.SizeCfg);
	LogSizeDlg^.TransferBuffer := @SizeCfgBuff;
	Result := Application^.ExecDialog(LogSizeDlg);
	if Result <> idCancel then
		LogFile^.MaxEntries := Result;
end;

procedure TLogFileWnd.EditDateRange(var Msg: TMessage);
var
  EditPtr: PEdit;
  LogRangeDlg: PLogRangeDlg;

begin
  LogRangeDlg := New(PLogRangeDlg, Init(@Self, 'INT_LOG_RANGE'));
  New(EditPtr, InitResource(LogRangeDlg, id_BegDate, 8+1));
  New(EditPtr, InitResource(LogRangeDlg, id_EndDate, 8+1));
  FillChar(RangeCfgBuff, SizeOf(RangeCfgBuff), 0);
  with RangeCfgBuff do
  begin
    GetPrivateProfileString('IntLog', 'BegDate', '', BegDateCfg, SizeOf(BegDateCfg), 'INTCSTM.INI');
    GetPrivateProfileString('IntLog', 'EndDate', '', EndDateCfg, SizeOf(EndDateCfg), 'INTCSTM.INI');
  end;
  LogRangeDlg^.TransferBuffer := @RangeCfgBuff;
  Application^.ExecDialog(LogRangeDlg);
end;

{
*****************
LogSizeDlg Object
*****************
}

procedure TLogSizeDlg.SaveSize(var Msg: TMessage);
var
	Result: integer;
  MaxEntries: integer;

begin
	TransferData(tf_GetData);
  Val(SizeCfgBuff.SizeCfg, MaxEntries, Result);
	EndDlg(MaxEntries);
end;

{
******************
LogRangeDlg Object
******************
}

procedure TLogRangeDlg.SaveRanges(var Msg: TMessage);
begin
  TransferData(tf_GetData);
  with RangeCfgBuff do
  begin
    WritePrivateProfileString('IntLog', 'BegDate', BegDateCfg, 'INTCSTM.INI');
    WritePrivateProfileString('IntLog', 'EndDate', EndDateCfg, 'INTCSTM.INI');
	end;
  EndDlg(0);
end;


Begin
End.

