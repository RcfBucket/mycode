unit UISpec;

INTERFACE

uses
  DBLib,
  DBTypes,
  OWindows;

{- call specimen entry. aRec is a record to preload }
procedure UISpecimenEntry(aParent: PWindowsObject; aSeq: TSeqNum);

IMPLEMENTATION

uses
  UICommon,
  UIPat,
  UIGSRes,
  APITools,
  CtlLib,
  UserMsgs,
  DBFile,
  DBIDs,
  PatReps,
  DMSErr,
  DMString,
  DlgLib,
  ListLib,
  INILib,
  MScan,
  ODialogs,
  Objects,
  Strings,
  UIFlds,
  Win31,
  WinProcs,
  WinTypes;

{$R UISPEC.RES}
{$I UISPEC.INC}

type
  PUISpecimen = ^TUISpecimen;
  TUISpecimen = object(TUICommonDlg)
    dbSpec    : PDBFile;  {- the record in the dbSpec is always pointing to the
                             currently loaded record. If any }
    dbPat     : PDBFile;
    origSpec  : PDBRec;   {- original record of a rec being edited }
    origPat   : PDBRec;
    tempSpec  : PDBRec;
    ClearSpec : Boolean;
    {}
    constructor Init(aParent: PWindowsObject; aSeq: TSeqNum);
    destructor Done; virtual;
    procedure SetupWindow; virtual;

    procedure WMLostFocus(var msg: TMessage);   virtual WM_FIRST + WM_LOSTFOCUS;
    procedure WMFldList(var msg: TMessage);     virtual WM_FIRST + WM_FLDLIST;
    {- field exit methods }
    procedure DoPatIDExit;
    procedure DoSpecCollExit(newFocID: word);

    {- misc stuff }
    procedure ClearFields; virtual;
    procedure EnableButtons; virtual;
    function FillScreen: boolean;
    procedure FillPatient;
    procedure ClearPatient;

    {- overwritten methods}
    procedure SetModuleID; virtual;
    function DataChanged: boolean; virtual;
    function CheckSave(clearAfter: boolean): boolean; virtual;
    function SaveData(clearAfter: boolean): boolean; virtual;
    procedure NextPrev(doNext: boolean); virtual;
    procedure DeleteData; virtual;

    procedure IDPatient(var msg: TMessage); virtual ID_FIRST + IDC_PATIENT;
    procedure IDGramStain(var msg: TMessage); virtual ID_FIRST + IDC_GS;
    procedure IDReview(var msg: TMessage); virtual ID_FIRST + IDC_REVIEW;
    procedure IDNext(var msg: TMessage);   virtual ID_FIRST + IDC_UINEXT;
    private
    last      : PStatic;  {- last name static text }
    first     : PStatic;  {- first name static text }
    sFld      : PUIFld;   {- commonly used fields }
    cFld      : PUIFld;
    pFld      : PUIFld;
  end;

constructor TUISpecimen.Init(aParent: PWindowsObject; aSeq: TSeqNum);
var
  f   : PUIFld;
  k   : integer;
  fNum: integer;
  c   : PControl;
  fi  : TFldInfo;
  p1,p2 : array[0..100] of char;
  subst : PErrSubst;
begin
  inherited Init(aParent, MakeIntResource(DLG_UISPECIMEN), aSeq);
  ClearSpec:=True;
  {- open all files being modified }
  dbSpec:= New(PDBFile, Init(DBSpecFile, '', dbOpenNormal));
  if dbSpec = nil then
  begin
    subst:= New(PErrSubst, Init(DBSPECFile));
    DoFail(IDS_ERROROPEN, DBLastOpenError, subst);   {- cant open specimen file }
    MSDisposeObj(subst);
    fail;
  end;

  {- get spec file in proper sort order }
  if INISpecimenListSort = 2 then
    dbSpec^.dbc^.SetCurKeyNum(DBSPEC_DATE_KEY)
  else
    dbSpec^.dbc^.SetCurKeyNum(DBSPEC_ID_KEY);
  dbSpec^.dbc^.GetFirst(dbSpec^.dbr);

  dbPat:= New(PDBFile, Init(dbPatFile, '', dbOpenNormal));
  if dbPat = nil then         {- cant open patient }
  begin
    subst:= New(PErrSubst, Init(DBSPECFile));
    DoFail(IDS_ERROROPEN, DBLastOpenError, subst);
    MSDisposeObj(subst);
    fail;
  end;

  origPat:= New(PDBRec, Init(dbPat^.dbc));
  if origPat = nil then
  begin
    DoFail(IDS_CANTALLOCREC, 0, nil);
    Fail;
  end;

  origSpec:= New(PDBRec, Init(dbSpec^.dbc));
  if origSpec = nil then {- cant allocate record buffer }
  begin
    DoFail(IDS_CANTALLOCREC, 0, nil);
    Fail;
  end;

  tempSpec:= New(PDBRec, Init(dbSpec^.dbc));
  if tempSpec = nil then
  begin
    DoFail(IDS_CANTALLOCREC, 0, nil);
    Fail;
  end;

  c:= New(PLStatic, InitResource(@self, IDC_LBLLAST, 0, false));
  c:= New(PLStatic, InitResource(@self, IDC_LBLFIRST, 0, false));
  InitModeText(IDC_MODE);
  last:= New(PStatic, InitResource(@self, IDC_TXTLAST, 0));
  first:= New(PStatic, InitResource(@self, IDC_TXTFIRST, 0));

  sFld:= LinkUIFld(DBSpecSpecID,      true,  false, 0, IDC_EDSPECID,   IDC_LBLSPECID,   dbSpec^.dbd);
  cFld:= LinkUIFld(DBSpecCollectDate, true,  false, 0, IDC_EDCOLLECT,  IDC_LBLCOLLECT,  dbSpec^.dbd);
         LinkUIFld(DBSpecSrc,         false, false, 0, IDC_EDSOURCE,   IDC_LBLSOURCE,   dbSpec^.dbd);
         LinkUIFld(DBSpecWard,        false, false, 0, IDC_EDWARD,     IDC_LBLWARD,     dbSpec^.dbd);
         LinkUIFld(DBSpecFreeText,    false, false, 0, IDC_EDFREETEXT, IDC_LBLFREETEXT, dbSpec^.dbd);
  pFld:= LinkUIFld(DBPatPatID,        false, false, 0, IDC_EDPATID,    IDC_LBLPATID,    dbPat^.dbd);
         LinkUIFld(DBSPECStat,        false, false, 0, IDC_EDSTATUS,   IDC_LBLSTATUS,   dbSpec^.dbd);

  {- now link all user defined fields }
  k:= IDC_EDUSER1;
  for fNum:= DBSpecUDBase to dbSpec^.dbd^.NumFields do
  begin
    dbSpec^.dbd^.FieldInfo(fNum, fi);
    if ((fi.userFlag and DB_UD) <> 0) then
    begin
      LinkUIFld(fNum, false, false, 0, k, k+100, dbSpec^.dbd);
      Inc(k);
    end;
  end;
end;

destructor TUISpecimen.Done;
begin
  inherited Done;
  MSDisposeObj(origSpec);
  MSDisposeObj(origPat);
  MSDisposeObj(tempSpec);
  MSDisposeObj(dbSpec);
  MSDisposeObj(dbPat);
end;

procedure TUISpecimen.SetupWindow;
begin
  inherited SetupWindow;

  if preLoad <> 0 then
  begin
    {- need to load a specimen record }
    if dbSpec^.dbc^.GetSeq(tempSpec, preLoad) then
    begin
      dbSpec^.dbr^.CopyRecord(tempSpec);
      if FillScreen then
        SetMode(MODE_EDIT);
    end;
  end;
end;

procedure TUISpecimen.SetModuleID;
begin
  modID:= MOD_UISPECIMEN;
end;

procedure TUISpecimen.ClearPatient;
{- clear patient ID/name information }
begin
  first^.SetText(nil);
  last^.SetText(nil);
  dbPat^.dbr^.ClearRecord;
  origPat^.ClearRecord;
  EnableButtons;
end;

procedure TUISpecimen.ClearFields;
{- clear all fields on screen }
var
  fld       : PUIFld;
  listRec   : PMnemListRec;
begin
  inherited ClearFields;
  ClearPatient;
  sFld^.FocusFld;   {- focus specimen ID field }
  origSpec^.ClearRecord;
  dbSpec^.dbr^.ClearRecord;

  {- set default "Preliminary" status }
  listRec:= New(PMnemListRec, Init);
  if (listRec <> nil) and listObj^.FindMnemID(@self, DBStatFile, '1', listRec, true) then
  begin
    fld:= FindField(IDC_EDSTATUS);
    if fld <> nil then
    begin
      dbSpec^.dbr^.PutField(DBSpecStat, @listRec^.seq);
      fld^.XFerRecToCtl(dbSpec^.dbr);
      origSpec^.CopyRecord(dbSpec^.dbr);
    end;
    MSDisposeObj(listRec);
  end;
end;

procedure TUISpecimen.EnableButtons;
begin
  EnableWindow(GetItemHandle(IDC_UIPREV),    (mode = MODE_EDIT) and (preLoad = 0));
  EnableWindow(GetItemHandle(IDC_UINEXT),     preLoad = 0);
  EnableWindow(GetItemHandle(IDC_UICLEAR),    preLoad = 0);
  EnableWindow(GetItemHandle(IDC_UIDELETE),  (mode = MODE_EDIT) and (preLoad = 0));
  EnableWindow(GetItemHandle(IDC_GS),         mode = MODE_EDIT);
  EnableWindow(GetItemHandle(IDC_REVIEW),     mode = MODE_EDIT);
  EnableWindow(GetItemHandle(IDC_UISAVE),     mode > MODE_NONE);
  EnableWindow(GetItemHandle(IDC_PATIENT),    origPat^.GetSeqValue > 0);
end;

function TUISpecimen.FillScreen: boolean;
{- called when dbSpec has been loaded. Fills all controls and loads any relevant
   patient data. Assumes the data to be filled is in dbSpec^.dbr. Also, sets the
   orig recs to the value of the loaded record }

  procedure GetData(aFld: PUIFld); far;
  begin
    if aFld^.GetDBD = dbSpec^.dbd then
      aFld^.XferRecToCtl(dbSpec^.dbr)
    else
      aFld^.XFerRecToCtl(dbPat^.dbr);
  end;

var
  seq   : TSeqNum;
begin
  if not origSpec^.CopyRecord(dbSpec^.dbr) then
  begin
    ShowError(@self, IDS_CANTCOPYREC, nil, origSpec^.dbErrorNum, modID, 0);
    FillScreen:= false;
  end
  else
  begin
    {- get the associated patient record }
    dbSpec^.dbr^.GetField(DBSpecPatRef, @seq, sizeof(seq));
    if dbPat^.dbc^.GetSeq(dbPat^.dbr, seq) then
      FillPatient
    else
      ClearPatient;

    flds^.ForEach(@GetData);
    FillScreen:= true;
  end;
end;

procedure TUISpecimen.FillPatient;
{- from dbPat, fill the patient related portion of the screen. Assumes that
   patient data is in dbPat^.dbr }
var
  pstr  : array[0..200] of char;
begin
  dbPat^.dbr^.GetField(DBPatLName, @pstr, 100);
  last^.SetText(pstr);
  dbPat^.dbr^.GetField(DBPatFName, @pstr, 100);
  first^.SetText(pstr);
  origPat^.CopyRecord(dbPat^.dbr);
  pFld^.XFerRecToCtl(dbPat^.dbr);   {- get patient ID from record}
  EnableButtons;
end;

procedure TUISpecimen.IDNext(var msg: TMessage);
begin
  ClearSpec:=False;
  NextPrev(True);
  ClearSpec:=True;
end;

procedure TUISpecimen.NextPrev(doNext: boolean);
var
  fld   : PUIFld;
  isOK  : boolean;
begin
  if not CheckSave(ClearSpec) then exit;

  if (mode <> MODE_EDIT) or (origSpec^.GetSeqValue = 0) then
  begin
    if dbSpec^.dbc^.GetFirst(tempSpec) then
    begin
      dbSpec^.dbr^.CopyRecord(tempSpec);
      if FillScreen then
        SetMode(MODE_EDIT);
    end
    else
      ShowError(@self, IDS_NOSPECS, nil, dbSpec^.dbc^.dbErrorNum, modID, 0);
  end
  else
  begin
    tempSpec^.CopyRecord(origSpec);
    if doNext then
      isOK:= dbSpec^.dbc^.GetNext(tempSpec)
    else
      isOK:= dbSpec^.dbc^.GetPrev(tempSpec);

    if not isOK then
    begin
      if dbSpec^.dbc^.dbErrorNum = MOD_BTRV + 9 then
        ShowError(@self, IDS_DBNOMOREDATA, nil, dbSpec^.dbc^.dbErrorNum, modID, 0)
      else
        ShowError(@self, IDS_DBERROR, nil, dbSpec^.dbc^.dbErrorNum, modID, 1);
    end
    else
    begin
      dbSpec^.dbr^.CopyRecord(tempSpec);
      if FillScreen then
        SetMode(MODE_EDIT);
    end;
  end;

  sfld^.FocusFld;
end;

procedure TUISpecimen.IDPatient(var msg: TMessage);
var
  seq   : TSeqNum;
begin
  DoPatIDExit; {- make sure patient ID is loaded if its been changed }
  seq:= origPat^.GetSeqValue;
  if seq > 0 then
  begin
    UIPatientEntry(@self, seq);
    {- reload patient record }
    if dbPat^.dbc^.GetSeq(dbPat^.dbr, seq) then
      FillPatient
    else
    begin
      ShowError(@self, IDS_ERRLODPAT, nil, dbPat^.dbc^.dbErrorNum, modID, 0);
      ClearPatient;
    end;
    sFld^.FocusFld;
  end;
end;

procedure TUISpecimen.IDGramStain(var msg: TMessage);
begin
  if mode = MODE_EDIT then
    UIGSResults(@self, origSpec^.GetSeqValue);
end;

procedure TUISpecimen.IDReview(var msg: TMessage);
var
  p1  : array[0..50] of char;
  cd  : longint;
begin
  sFld^.XFerCtlToRec(tempSpec);
  cFld^.XFerCtlToRec(tempSpec);
  tempSpec^.GetFieldAsStr(DBSpecSpecID, p1, sizeof(p1)-1);
  cd := 0;
  tempSpec^.GetField(DBSPECCollectDate, @cd, sizeof(cd));
  SpecimenReview(@self, p1, cd);
end;

procedure TUISpecimen.DeleteData;
var
  p1, p2  : array[0..100] of char;
  err     : integer;
begin
  if mode = MODE_EDIT then
  begin
    if YesNoMsg(HWindow, SR(IDS_CONFIRM, p1, 100), SR(IDS_CONFIRMDELSPEC, p2, 100)) then
    begin
      err:= 0;
      WaitCursor(true);
      if dbSpec^.dbc^.GetSeq(dbSpec^.dbr, origSpec^.GetSeqValue) then
      begin
        if not dbSpec^.dbc^.DeleteRec(dbSpec^.dbr) then
          err:= dbSpec^.dbc^.dbErrorNum
        else
          ClearFields;
      end
      else
        err:= dbSpec^.dbc^.dbErrorNum;

      if err <> 0 then
        ShowError(@self, IDS_CANTDEL, nil, err, modID, 0);
      WaitCursor(false);
    end;
  end;
end;

function TUISpecimen.DataChanged: boolean;
{- compare the current data with the originally loaded data.  If any data has
   changed, then return true }

  function IsModified(aFld: PUIFld): boolean; far;
  begin
    if aFld^.GetDBD = dbSpec^.dbd then
      IsModified:= aFld^.IsModified(origSpec)
    else
      IsModified:= aFld^.IsModified(origPat);
  end;

begin
  DataChanged:= flds^.FirstThat(@IsModified) <> nil;
end;

function TUISpecimen.SaveData(clearAfter: boolean): boolean;
{- save the data for the specimen. return true if the data was saved. false otherwise}
var
  ret     : boolean;
  iso     : PDBFile;

  function ValidateScreen: boolean;
  {- check screen for validity }
  var
    sret    : boolean;
    fld     : PUIFld;
    errNum  : integer;

    function IsInvalid(aFld: PUIFld): boolean; far;
    begin
      errNum:= aFld^.Format;
      IsInvalid:= errNum <> 0;
    end;

  begin
    sret:= true;
    fld:= flds^.FirstThat(@IsInvalid);
    if (fld <> nil) then
    begin {- some other error }
      ShowUIFldError(@self, errNum, fld);
      sRet:= false;
    end;
    ValidateScreen:= sret;
  end;

  function StartTransaction: boolean;
  var
    errNum  : integer;
  begin
    errNum:= DBBeginTransaction;
    if errNum <> 0 then
    begin
      ShowError(@self, IDS_TRANSERROR, nil, errNum, modID, 0);
      StartTransaction:= false;
    end
    else
      StartTransaction:= true;
  end;

  function LockRecord: boolean;
  {- lock spec records and place current contents of records in tempPat and tempSpec
     if the records are not found then the user is prompted to recreate in which case
     the appropriate mode (mode, patMode) is modified accordingly }
  begin
    {- no need to lock if adding a new specimen }
    if mode = MODE_EDIT then
    begin
      tempSpec^.CopyRecord(origSpec);
      LockRecord:= LockIt(dbSpec, tempSpec, IDS_SPECLOCKED, IDS_CANTLOCATEORIGSPEC, mode);
    end
    else
      LockRecord:= true;
  end;

  function CheckModified: boolean;
  {- at this point the original record is locked. Now see if someone else has
      modified it since we originally loaded it. if so prompt for overwrite. Assumes
      the current contents of the record are placed in tempSpec/tempPat }
  var
    sret    : boolean;
    p1, p2  : array[0..200] of char;
  begin
    sret:= true;
    if mode = MODE_EDIT then    {- only valid if editing a specimen }
    begin
      if not origSpec^.CompareRecord(tempSpec) then
        sret:= YesNoMsg(HWindow, SR(IDS_WARNING, p1, 200), SR(IDS_SPECMODIFIED, p2, 200));
    end;
    CheckModified:= sRet;
  end;

  function SaveIt: boolean;
  var
    sRet  : boolean;
    eNum  : integer;
    seq   : TSeqNum;

    procedure XFerToRec(aFld: PUIFld); far;
    begin
      if aFld^.GetDBD = dbSpec^.dbd then  {- skip over patient id field }
        aFld^.XFerCtlToRec(dbSpec^.dbr)
      else
        aFld^.XFerCtlToRec(dbPat^.dbr);
    end;

  begin
    sRet:= true;

    DoPatIDExit; {- make sure patient ID is loaded if its been changed }

    {- now transfer all fields to the record buffer }
    flds^.ForEach(@XFerToRec);  {- get specimen fields }

    {- handle patient ID }
    if not pFld^.IsEmpty and (origPat^.GetSeqValue = 0) then
    begin
(*      dbPat^.dbr^.ClearRecord;*)
(*      pFld^.XFerCtlToRec(dbPat^.dbr);*)
      if not dbPat^.dbc^.InsertRec(dbPat^.dbr) then
      begin
        ShowError(@self, IDS_PATSAVERROR, nil, dbPat^.dbc^.dbErrorNum,
                  modID, 0);
        sret:= false;
      end;
      dbPat^.dbc^.InsertRec(dbPat^.dbr);
    end;

    if sRet then  {- now for specimen }
    begin
      seq:= dbPat^.dbr^.GetSeqValue;  {- get patient seq for specimen reference }
      dbSpec^.dbr^.PutField(DBSpecPatRef, @seq);

      if mode = MODE_NEW then
        dbSpec^.dbc^.InsertRec(dbSpec^.dbr)
      else
        dbSpec^.dbc^.UpdateRec(dbSpec^.dbr);
      eNum:= dbSpec^.dbc^.dbErrorNum;

      if eNum <> 0 then
      begin
        {- special error handling for common errors }
        case eNum of
          MOD_BTRV + 5: eNum:= IDS_DUPSPEC;
          else
            eNum:= IDS_CANNOTSAVE;
        end;
        ShowError(@self, eNum, nil, dbSpec^.dbc^.dbErrorNum, modID, 0);
        sFld^.FocusFld;
        sret:= false;
      end
      else if mode = MODE_EDIT then
      begin   {- update any isolate records that may be affected }
        eNum:= UpdateSpecIsos(iso, nil, dbSpec^.dbr);
        if eNum <> 0 then
        begin
          ShowError(@self, IDS_ERRUPDISOS, nil, eNum, modID, 0);
          sRet:= false;
        end;
      end;
    end;
    SaveIt:= sret;
  end;


  procedure UnlockRecords;
  begin
    if mode = MODE_EDIT then
      dbSpec^.dbc^.UnlockRec(tempSpec);
  end;

var
  subst   : PErrSubst;
begin
  WaitCursor(true);
  ret:= true;

  iso:= nil;
  if mode = MODE_EDIT then
  begin
    iso:= New(PDBFile, Init(DBIsoFile, '', dbOpenNormal));
    if iso = nil then
    begin
      subst:= New(PErrSubst, Init(DBIsoFile));
      ShowError(@self, IDS_ERROROPEN, subst, dbLastOpenError, modID, 0);
      MSDisposeObj(subst);
      ret:= false;
    end;
  end;

  if ret then ret:= ValidateScreen;
  if ret then ret:= StartTransaction;
  if ret then ret:= LockRecord;
  if ret then ret:= CheckModified;
  if ret then ret:= SaveIt;
  UnlockRecords;
  if ret then
    DBEndTransaction
  else
    DBAbortTransaction;

  {- and finally, clear fields }
  if ret and clearAfter then
    ClearFields;

  MSDisposeObj(iso);

  SaveData:= ret;
  WaitCursor(false);
end;

function TUISpecimen.CheckSave(clearAfter: boolean): boolean;
var
  ret     : boolean;
  pstr    : array[0..200] of char;
  p2      : array[0..50] of char;
  k       : integer;
begin
  ret:= true;
  if mode <> MODE_NONE then
  begin
    {- first see if any data has changed }
    if DataChanged then
    begin
      k:= YesNoCancelMsg(HWindow, SR(IDS_CONFIRM, p2, 50), SR(IDS_CONFIRMSAVE, pstr, 200));
      if k = IDYES then
        ret:= SaveData(clearAfter)
      else
        ret:= k <> IDCANCEL;
    end;
  end;
  CheckSave:= ret;
end;

{---- field exit routines }

procedure TUISpecimen.DoSpecCollExit(newFocID: word);
var
  id  : word;
begin
  {- if a mode has been established then no need for this processing }
  if (mode = MODE_NONE) and (newFocID <> IDC_EDSPECID) and (newFocID <> IDC_EDCOLLECT) then
  begin
    WaitCursor(true);
    {- if one or the other fields are empty then no processing can occur. We need
      both fields to perform the load }
    if not (sFld^.IsEmpty or cFld^.IsEmpty) then
    begin
      {- attempt to find the spec/collect date, if not found then NEW mode
          assumed, otherwise EDIT mode is assumed }

      sFld^.XferCtlToRec(tempSpec);
      cFld^.XferCtlToRec(tempSpec);

      if listObj^.FindKey(@self, tempSpec, true) then
      begin
        {- relocate the specimen cursor to the proper record (so next/prev still work) }
        if not dbSpec^.dbc^.GotoRecord(tempSpec) then
        begin
          ShowError(@self, IDS_CANNOTPOS, nil, dbSpec^.dbc^.dbErrorNum, modID, 0);
          SetMode(MODE_NEW);
          origSpec^.ClearRecord;
        end
        else
        begin
          dbSpec^.dbr^.CopyRecord(tempSpec);
          if FillScreen then
            SetMode(MODE_EDIT);
        end;
      end
      else
      begin
        if listObj^.errorNum <> 0 then
          ShowError(@self, IDS_DBERROR, nil, listObj^.errorNum, modID, 2);
        SetMode(MODE_NEW);
        origSpec^.ClearRecord;
      end;
    end;
    WaitCursor(false);
  end;
end;

procedure TUISpecimen.DoPatIDExit;
{- called when patien id loses focus }
var
  pstr  : array[0..200] of char;
begin
  if pFld^.IsEmpty then
    ClearPatient
  else
  begin
    if pFld^.IsModified(dbPat^.dbr) then
    begin
      pFld^.XFerCtlToRec(dbPat^.dbr);
      if listObj^.FindKey(@self, dbPat^.dbr, true) then
        FillPatient
      else
      begin
        if listObj^.errorNum <> 0 then
          ShowError(@self, IDS_DBERROR, nil, listObj^.errorNum, modID, 3);
        ClearPatient;
      end;
    end;
  end;
end;

procedure TUISpecimen.WMFldList(var msg: TMessage);
{- called when user selects LIST in a field }
var
  id    : word;
  pstr  : array[0..100] of char;
  p2    : array[0..100] of char;
  ret   : longint;    {- return value to the UIFld that created this message }
  cont  : boolean;
begin
  ret:= -1;   {- tell UI field passing this message NOT to list (non-zero) }

  uiFldsProcessFocus:= false;   {- turn off field focus processing }

  id:= msg.wParam;
  if (id = IDC_EDSPECID) or (id = IDC_EDCOLLECT) then
  begin
    cont:= true;
    {- first see if any data has been entered. If so, prompt to save so the user
       wont lose data if they select from a list }
    if mode <> MODE_NONE then
      cont:= CheckSave(false);
    if cont then
    begin
      sFld^.XferCtlToRec(tempSpec);
      cFld^.XferCtlToRec(tempSpec);
      if listObj^.SelectList(@self, tempSpec) then
      begin
        dbSpec^.dbc^.GotoRecord(tempSpec); {- relocate for next/prev }
        dbSpec^.dbr^.CopyRecord(tempSpec);
        if FillScreen then
          SetMode(MODE_EDIT);
      end
      else
      begin
        if listObj^.errorNum <> 0 then
        begin
          if listObj^.errorNum = LISTERR_PARTIALKEYNOTFOUND then
            ShowError(@self, IDS_CANNOTLISTNOMATCH, nil, listObj^.errorNum, modID, 0)
          else
            ShowError(@self, IDS_CANNOTLISTBAD, nil, listObj^.errorNum, modID, 0);
        end;
      end;
    end;
  end
  else if id = IDC_EDPATID then
  begin
    pFld^.XFerCtlToRec(dbPat^.dbr);
    if listObj^.SelectList(@self, dbPat^.dbr) then
      FillPatient
    else if listObj^.errorNum <> 0 then
    begin
      if listObj^.errorNum = LISTERR_PARTIALKEYNOTFOUND then
        ShowError(@self, IDS_CANNOTLISTNOMATCH, nil, listObj^.errorNum, modID, 0)
      else
        ShowError(@self, IDS_CANNOTLISTBAD, nil, listObj^.errorNum, modID, 0);
    end;
  end
  else
    ret:= 0;    {- this routine is not handling the list. Let the UIfld handle it }

  uiFldsProcessFocus:= true;  {- turn focus processing back on }

  SetWindowLong(HWindow, DWL_MSGRESULT, ret);  {- set message result (for dialogs) }
end;

procedure TUISpecimen.WMLostFocus(var msg: TMessage);
var
  fld   : PUIFld;
  err   : integer;
begin
  fld:= PUIFld(msg.lParam); {- get field pointer to fld losing focus }
  {- first see if the field is valid }
  err:= fld^.IsValid;
  if (err <> 0) and (err <> UIErrRequired) then   {- ignore required fields on field exit }
    ShowUIFldError(@self, err, fld)
  else
  begin
    {- full key possibly entered, check for auto loading of data }
    case fld^.GetID of
      IDC_EDSPECID, IDC_EDCOLLECT:
        DoSpecCollExit(msg.wParam);
      IDC_EDPATID:
        DoPatIDExit;
    end;
  end;
  SetWindowLong(HWindow, DWL_MSGRESULT, 0);  {- set message result (for dialogs) }
end;

{------[ Interfaced Procedures ]--}

procedure UISpecimenEntry(aParent: PWindowsObject; aSeq: TSeqNum);
var
  d   : PDialog;
begin
  d:= New(PUISpecimen, Init(aParent, aSeq));
  application^.ExecDialog(d);
end;


END.


{- rcf}
