unit Cvt18Map;

INTERFACE

uses
  OWindows;

procedure MapV18Fields(aParent: PWindowsObject);

IMPLEMENTATION

uses
  Strings,
  ApiTools,
  IntlLib,
  DBTypes,
  ODialogs,
  WinProcs,
  DateLib,
  WinTypes,
  Cvt18Cvt,
  CvtMisc,
  DlgLib,
  GridLib;

{$I CVT18.INC}

type
  PMap18Dlg = ^TMap18Dlg;
  TMap18Dlg = object(TCenterDlg)
    collDateFld : PEdit;
    collDateStr : array[0..25] of char;
    collDate    : TIntlDate;
    grid        : PMapGrid;
    maps        : PMapCollection;
    constructor Init(aParent: PWindowsObject);
    procedure SetupWindow; virtual;
    destructor Done; virtual;
    procedure WMCharToItem(var msg: TMessage);  virtual WM_FIRST + WM_CHARTOITEM;
    procedure WMDrawItem(var msg: TMessage);    virtual WM_FIRST + WM_DRAWITEM;
    procedure WMMeasureItem(var msg: TMessage); virtual WM_FIRST + WM_MEASUREITEM;
    procedure IDMap(var msg: TMessage); virtual ID_FIRST + IDC_MAP;
    procedure ClearMap(var msg: TMessage); virtual ID_FIRST + IDC_CLEARMAP;
    procedure IDNext(var msg: TMessage); virtual ID_FIRST + IDC_NEXT;
    function CanClose: boolean; virtual;
    procedure Cancel(var msg: TMessage); virtual ID_FIRST + ID_CANCEL;
    procedure MapAField;
    procedure WMCommand(var msg: TMessage); virtual WM_FIRST + WM_COMMAND;
  end;

constructor TMap18Dlg.Init(aParent: PWindowsObject);
begin
  inherited Init(aParent, MakeIntResource(DLG_CVT18MAPPING));

  collDateFld:= New(PEdit, InitResource(@Self, IDC_CURRDATE, sizeof(collDateStr)));
  IntlDateStr(IntlPackDate(1911, 11, 11), collDateStr, sizeof(collDateStr)-1); {- default collect date }

  maps:= New(PMapCollection, Init(Version18));
  maps^.AddField( 1, 0, 'PATIENT', 'Date of Birth', dbDate);
  maps^.AddField( 2,11, 'PATIENT', 'Gender', dbZString);
  maps^.AddField( 3,11, 'PATIENT', 'Race', dbZString);
  maps^.AddField( 4, 0, 'PATIENT', 'Date In', dbDate);
  maps^.AddField( 5, 0, 'PATIENT', 'Date Out', dbDate);
  maps^.AddField( 6, 0, 'PATIENT', 'Physician', dbSeqRef);
  maps^.AddField( 7, 0, 'PATIENT', 'Patient Ward', dbSeqRef);
  maps^.AddField( 8,11, 'PATIENT', 'Room Number', dbZString);
  maps^.AddField( 9,11, 'PATIENT', 'In/Out Patient Status', dbZString);
  maps^.AddField(10, 0, 'PATIENT', 'Institute', dbSeqRef);
  maps^.AddField(11, 0, 'PATIENT', 'Service', dbSeqRef);

  maps^.AddField( 1, 0, 'SPECIMEN', 'Collection Time', dbTime);
  maps^.AddField( 2, 0, 'SPECIMEN', 'Order Date', dbDate);
  maps^.AddField( 3, 0, 'SPECIMEN', 'Order Physician', dbSeqRef);
  maps^.AddField( 4, 0, 'SPECIMEN', 'Receive Date', dbDate);
  maps^.AddField( 5, 0, 'SPECIMEN', 'Receive Time', dbTime);
  maps^.AddField( 6, 0, 'SPECIMEN', 'Tech Id', dbSeqRef);

  grid:= New(PMapGrid, InitResource(@self, IDC_V18GRID, maps));
end;

procedure TMap18Dlg.SetupWindow;
begin
  inherited SetupWindow;
  collDateFld^.SetText(collDateStr);
end;

destructor TMap18Dlg.Done;
begin
  Dispose(maps, Done);
  inherited Done;
end;

procedure TMap18Dlg.WMCharToItem(var msg: TMessage);
begin
  grid^.CharToItem(msg);
end;

procedure TMap18Dlg.WMDrawItem(var msg: TMessage);
begin
  grid^.DrawItem(msg);
end;

procedure TMap18Dlg.WMMeasureItem(var msg: TMessage);
begin
  grid^.MeasureItem(msg);
end;

procedure TMap18Dlg.IDMap(var msg: TMessage);
begin
  MapAField;
end;

procedure TMap18Dlg.ClearMap(var msg: TMessage);
begin
  grid^.ClearFieldMap(grid^.GetSelIndex);
end;

function TMap18Dlg.CanClose: boolean;
begin
  CanClose:= ConfirmExit(hWindow);
end;

procedure TMap18Dlg.Cancel(var msg: TMessage);
begin
  if CanClose then
    inherited Cancel(msg);
end;

procedure TMap18Dlg.IDNext(var msg: TMessage);
var
  err : boolean;
begin
  collDateFld^.GetText(collDateStr, sizeof(collDateStr)-1);
  if strlen(collDateStr) = 0 then
  begin
    ErrorMsg(hWindow, 'Error', 'Collect date is required.');
    FocusCtl(hWindow, collDateFld^.GetID);
    exit;
  end;

  collDate:= IntlStrToDate(collDateStr, err);
  if err or not ValidDate(collDate) then
  begin
    ErrorMsg(hWindow, 'Error', 'Invalid Collect Date entered.');
    FocusCtl(hWindow, collDateFld^.GetID);
    exit;
  end;

  ShowWindow(hWindow, SW_HIDE);
  Cvt18ConvertData(@self, maps, collDate);
  EndDlg(IDOK);
end;

procedure TMap18Dlg.WMCommand(var msg: TMessage);
begin
  inherited WMCommand(msg);
  if (msg.lParamLo <> 0) then   {- message is from a control }
  begin
    if msg.lParamHI = LBN_DBLCLK then
      MapAField;
  end;
end;

procedure TMap18Dlg.MapAField;
var
  k   : integer;
begin
  k:= grid^.GetSelIndex;
  if k >= 0 then
    grid^.MapAField(k);
end;

procedure MapV18Fields(aParent: PWindowsObject);
begin
  application^.ExecDialog(New(PMap18Dlg, Init(aParent)));
end;

END.

