unit Sessions;

INTERFACE

uses
  OWindows,
  DBTypes;

function GetCurrentSession(name  : PChar;
                           maxLen: integer): TSeqNum;
procedure SetCurrentSession(aSession: TSeqNum);
function GetSessionInfo(seq   : TSeqNum;
                        id,
                        desc  : PChar;
                        maxLen: integer): boolean;
procedure DefineSessions(aParent: PWindowsObject);

IMPLEMENTATION

uses
  ApiTools,
  DBIDS,
  DMSDebug,
  DMString,
  ListLib,
  MScan,
  Strings,
  DBFile,
  INILib,
  WinProcs;

var
  curSes  : TSeqNum;
  curName : array[0..50] of char;

function GetSessionInfo(seq: TSeqNum; id, desc: PChar; maxLen: integer): boolean;
{- this function is used to access session information based on a session seq.
   If seq is zero, then empty session information is returned, otherwise the
   specified session is retrieved and the information returned. If there is an
   error, then the function returns false}
var
  db    : PDBFile;
  ret   : boolean;
begin
  ret:= false;

  db:= New(PDBFile, Init(DBSessFile, '', dbOpenNormal));
  if db <> nil then
  begin
    if seq = 0 then { Sessions disabled }
      ret:= True    { Just return empty record }
    else
      ret:= db^.dbc^.GetSeq(db^.dbr, seq);
  end;

  if ret then
  begin
    db^.dbr^.GetFieldAsStr(DBSessID, id, maxLen);
    db^.dbr^.GetFieldAsStr(DBSessDesc, desc, maxLen);
  end;

  MSDisposeObj(db);

  GetSessionInfo:= ret;
end;

function GetCurrentSession(name: PChar; maxLen: integer): TSeqNum;
{- returns the currently selected session sequence number. A zero is returned if
   the sessions are disabled. The name of the current session is returned
   in name }
var
  pstr  : array[0..50] of char;
  curID : array[0..50] of char;
  db    : PDBFile;
begin
  {- if first time function has been called, get last session used from INI file }
  if curSes = -1 then
  begin
    curSes:= 0;
    StrCopy(curName, '');
    INIGetLastSession(curID, 50);  {- get ID of last session }
    if (StrLen(curID) > 0) then
    begin
      { Build the string "<ERROR>", or its translated version }
      StrLCopy(curName, '<', sizeof(curName)-1);
      StrLCat(curName, AnsiUpper(SR(IDS_ERROR, pstr, 50)), sizeof(curName)-1);
      StrLCat(curName, '>', sizeof(curName)-1);
      db:= New(PDBFile, Init(DBSessFile, '', dbOpenNormal));
      if db <> nil then
      begin
        db^.dbr^.PutField(DBSessID, @curID);
        if db^.dbc^.GetEQ(db^.dbr) then
        begin
          curSes:= db^.dbr^.GetSeqValue;
          db^.dbr^.GetFieldAsStr(DBSessID, curID, sizeof(curID)-1);
          db^.dbr^.GetFieldAsStr(DBSessDesc, curName, sizeof(curName)-1);
        end;
        MSDisposeObj(db);
      end;
    end;
  end;
  StrLCopy(name, curName, maxLen);
  GetCurrentSession:= curSes;
end;

procedure SetCurrentSession(aSession: TSeqNum);
{- sets the current session to the session specified. Setting the current
   session to 0 (zero) means sessions are disabled }
var
  db    : PDBFile;
  curID : array[0..50] of char;
  isOK  : boolean;
begin
  isOK:= True;
  if (aSession = 0) then
  begin
    StrCopy(curID, '');
    StrCopy(curName, '');
    curSes:= aSession;
  end
  else
  begin
    db:= New(PDBFile, Init(DBSessFile, '', dbOpenNormal));
    if db <> nil then
    begin
      isOK:= db^.dbc^.GetSeq(db^.dbr, aSession);
      if isOK then
      begin
        db^.dbr^.GetFieldAsStr(DBSessID, curID, 50);
        db^.dbr^.GetFieldAsStr(DBSessDesc, curName, sizeof(curName)-1);
        curSes:= aSession;
      end;
      MSDisposeObj(db);
    end;
  end;
  {- now write session info to INI file }
  INISetLastSession(curID);
  {- now tell the Main DMS screen to update the session name in the  }
  {  title bar                                                       }
  SendChangeTitleMsg;
end;

procedure DefineSessions(aParent: PWindowsObject);
var
  sessList : PListObject;
begin
  sessList:= New(PListObject, Init);
  if sessList <> nil then
  begin
    sessList^.EditableMnemList(aParent, DBSessFile, '');
    MSDisposeObj(sessList);
  end;
end;

BEGIN
  curSes:= -1;
END.

{- rcf }
