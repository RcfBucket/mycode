{----------------------------------------------------------------------------}
{  Module Name  : EDSCHEMA.PAS                                               }
{  Programmer   : Kevin Riley                                                }
{  Date Created : January 1995                                               }
{                                                                            }
{  Purpose -                                                                 }
{  This program allows the end-user to modify the schema for the three       }
{  core database files (PATIENT, SPECIMEN, ISOLATE).  Fields can be added,   }
{  and user-defined fields can be deleted.  If a mnemonic field is added,    }
{  the user can associate the field with an existing mnemonic file, or       }
{  create a new mnemonic file with which to associate it.  If the database   }
{  being modified contains any records, the data will be transferred to      }
{  the modified database file with added fields filled with empty values     }
{  and deleted fields values lost.                                           }
{                                                                            }
{  Assumptions -                                                             }
{  None.                                                                     }
{                                                                            }
{  Initialization -                                                          }
{  None.                                                                     }
{                                                                            }
{  Revision History -                                                        }
{                                                                            }
{  Version  Date      Who    Did What                                        }
{  -------  --------  ------ ----------------------------------------------- }
{  1.00     02/13/94  KMR    Initial release.                                }
{                                                                            }
{----------------------------------------------------------------------------}

program EdSchema;

uses
  OWindows,
  ODialogs,
  Objects,
  WinTypes,
  UserMsgs,
  WinProcs,
  WinDOS,
  Strings,
  StrsW,
  DMString,
  DMSErr,
  MScan,
  IniLib,
  ApiTools,
  Ctl3D,
  DlgLib,
  GridLib,
  CtlLib,
  SpinEdit,
  ListLib,
  Status,
  DBFile,
  DBLib,
  DBErrors,
  DBTypes,
  MTASK,
  Win31,
  DMSDebug;


{$R EDSCHEMA.RES}
{$I EDSCHEMA.INC}

const
    NumColsFile       = 5;      { number of columns in the files grid }
    ColFileDesc       = 0;      { column number of FILE DESCRIPTION }
    ColFileName       = 1;      { column number of FILE NAME }
    ColFileAutoSeq    = 2;      { column number of FILE AUTO SEQ flag }
    ColFileUserDef    = 3;      { column number of USER DEFINED FILE flag }
    ColFileNumRecs    = 4;

    NumColsField      = 4;      { number of columns in the fields grid }
    ColFieldName      = 0;      { column number of FIELD NAME }
    ColFieldType      = 1;      { column number of FIELD TYPE }
    ColFieldUserDef   = 2;      { column number of USER DEFINED FIELD flag }
    ColFieldFlags     = 3;      { column number of FIELD FLAGS }

    NumColsMnemFile   = 2;      { number of columns in the mnemonic files grid }
    ColMnemFileDesc   = 0;      { column number of FILE DESCRIPTION }
    ColMnemFileName   = 1;      { column number of FILE NAME }

    NumColsMnemData   = 2;      { number of columns in the mnemonic data grid }
    ColMnemDataID     = 0;      { column number of mnemonic ID }
    ColMnemDataDesc   = 1;      { column number of mnemonic DESCRIPTION }

    MaxStringFieldLen = 1024;   { maximum length of a database STRING field }

type
  PAssocInfo = ^TAssocInfo;

  PDlgAddMnemFile = ^TDlgAddMnemFile;
  TDlgAddMnemFile = object(TCenterDlg)
    edtDesc   : PEdit;
    edtName   : PEdit;
    dbDir     : PChar;    { directory location of database files }
    fName     : PChar;
    {}
    constructor Init(AParent: PWindowsObject; aDir, aName: PChar);
    procedure SetupWindow; virtual;
    function CanClose: boolean; virtual;
  end;

  PDlgMnemFields = ^TDlgMnemFields;
  TDlgMnemFields = object(TCenterDlg)
    grid   : PGrid;
    dbDir  : PChar;           { directory location of database files }
    fName  : array [0..maxFileNameLen] of char;
    pAssoc : PAssocInfo;
    {}
    constructor Init(AParent: PWindowsObject; aDir: PChar; aPAssoc: PAssocInfo);
    procedure SetupWindow; virtual;
    procedure FillGrid;
    procedure DrawItem (var msg: TMessage); virtual WM_FIRST + WM_DRAWITEM;
    procedure MeasureItem (var msg: TMessage); virtual WM_FIRST + WM_MEASUREITEM;
    procedure CharToItem (var msg: TMessage); virtual WM_FIRST + WM_CHARTOITEM;
    procedure BtnSelect(var msg: TMessage); virtual ID_FIRST + IDC_BTN_SELECT;
    procedure BtnCreate(var msg: TMessage); virtual ID_FIRST + IDC_BTN_CREATE;
    procedure BtnView(var msg: TMessage); virtual ID_FIRST + IDC_BTN_VIEW;
    procedure GrdMnemFiles(var msg: TMessage); virtual ID_FIRST + IDC_GRD_FILE;
  end;

  PFieldRec = ^TFieldRec;
  TFieldRec = object(TObject)
    fi   : TFldInfo;
    ai   : TAssocInfo;
    ud   : boolean;
    add  : boolean;
    del  : boolean;
    {}
    constructor Init;
  end;

  PDlgAddField = ^TDlgAddField;
  TDlgAddField = object(TCenterDlg)
    edtName   : PEdit;
    cmbType   : PComboBox;
    lblLength : PLStatic;
    spnLength : PSpinInt;
    dbDir     : PChar;        { directory location of database files }
    fldRec    : PFieldRec;
    {}
    constructor Init(AParent: PWindowsObject; aDir: PChar; aFld: PFieldRec);
    procedure SetupWindow; virtual;
    function  CanClose: boolean; virtual;
    procedure CmbTypeNotify(var msg: TMessage); virtual ID_FIRST + IDC_CMB_FIELD_TYPES;
  end;

  PDlgFields = ^TDlgFields;
  TDlgFields = object(TCenterDlg)
    grid        : PGrid;
    dbDir       : PChar;              { directory location of database files }
    db          : PDBReadWriteFile;   { database file being modified }
    bEmpty      : boolean;            { database file is empty }
    fieldColl   : PCollection;        { collection of field rec objects }
    cntUD       : integer;
    {}
    constructor Init(AParent: PWindowsObject; aDir: PChar;
                     aDB: PDBReadWriteFile; aEmpty: boolean);
    destructor Done; virtual;
    procedure SetupWindow; virtual;
    procedure FillGrid;
    procedure EnableButtons; virtual;
    procedure DrawItem (var msg: TMessage); virtual WM_FIRST + WM_DRAWITEM;
    procedure MeasureItem (var msg: TMessage); virtual WM_FIRST + WM_MEASUREITEM;
    procedure CharToItem (var msg: TMessage); virtual WM_FIRST + WM_CHARTOITEM;
    procedure BtnAdd(var msg: TMessage); virtual ID_FIRST + IDC_BTN_ADD;
    procedure BtnDelete(var msg: TMessage); virtual ID_FIRST + IDC_BTN_DELETE;
    procedure BtnSave(var msg: TMessage); virtual ID_FIRST + IDC_BTN_SAVE;
    procedure Cancel(var msg: TMessage); virtual ID_FIRST + IDCANCEL;
    procedure GrdField(var msg: TMessage); virtual ID_FIRST + IDC_GRD_FIELD;
    function  ModifiedFields: boolean;
    procedure CopyData(fileName, filePath: PChar);
  end;

  PDlgMain = ^TDlgMain;
  TDlgMain = object(TCenterDlgWindow)
    grid  : PGrid;
    dbDir : array [0..maxPathLen] of char; { directory location of database files }
    {}
    constructor Init(AParent: PWindowsObject);
    destructor Done; virtual;
    procedure SetupWindow; virtual;
    procedure FillGrid;
    procedure ClearGrid;
    procedure EnableButtons; virtual;
    procedure DrawItem (var msg: TMessage); virtual WM_FIRST + WM_DRAWITEM;
    procedure MeasureItem (var msg: TMessage); virtual WM_FIRST + WM_MEASUREITEM;
    procedure CharToItem (var msg: TMessage); virtual WM_FIRST + WM_CHARTOITEM;
    procedure BtnEdit(var msg: TMessage); virtual ID_FIRST + IDC_BTN_EDIT;
    procedure GrdFile(var msg: TMessage); virtual ID_FIRST + IDC_GRD_FILE;
    function  GetClassName: PChar; virtual;
    procedure GetWindowClass(var aWndClass: TWndClass); virtual;
    procedure WMDMSQuery(var msg: TMessage); virtual WM_FIRST + WM_MAINPROGQUERY;
  end;

  TMe = object(T3dApplication)
    procedure InitMainWindow; virtual;
  end;

var
  me    : TMe;
  err   : PErrSubst;
  title : array [0..40] of char;

{----------------------------------------------------------------------------}
{                       Miscellaneous Support Routines                       }
{----------------------------------------------------------------------------}

{----------------------------------------------------------------------------}
{  Displays an error message along with a database error number and string.  }
{  NOTE: This procedure was blatantly stolen from EditDB.                    }
{                                                                            }
{  Returns : None.                                                           }
{  InParms : HWindow - Window handle of the caller.                          }
{            msg     - Error message to be displayed.                        }
{            errNum  - Database error number to be displayed.                }
{  OutParms: None.                                                           }
{----------------------------------------------------------------------------}
procedure ShowDBError(aParent: PWindowsObject; msg: PChar; errNum: integer);
var
  pstr  : array[0..256] of char;
begin
  StrLCopy(pstr, msg, sizeof(pstr)-1);
  ShowErrorStr(aParent^.hWindow, msg, errNum, MOD_EDSCHEMA, 0, sizeof(pstr)-1);
end;

{----------------------------------------------------------------------------}
{                               TDlgAddMnemFile                              }
{----------------------------------------------------------------------------}
{----------------------------------------------------------------------------}
{  Prompts the user for a new mnemonic file name and description, and if     }
{  the user selects OK and the file does not already exist, creates a        }
{  mnemonic file with an ID field and a description field.                   }
{----------------------------------------------------------------------------}

constructor TDlgAddMnemFile.Init(AParent: PWindowsObject; aDir, aName: PChar);
const
  fNameChars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
begin
  inherited Init(AParent, MakeIntResource(DLG_ADD_MNEM_FILE));
  edtDesc := New(PEdit, InitResource(@Self, IDC_EDT_FILE_DESC, maxFileDescLen+1));
  edtName := New(PFilterEdit, InitResource(@Self, IDC_EDT_FILE_NAME, maxFileNameLen+1, fNameChars));
  dbDir := aDir;
  fName := aName;
end;

procedure TDlgAddMnemFile.SetupWindow;
begin
  inherited SetupWindow;
end;

function TDlgAddMnemFile.CanClose: boolean;
var
  dbDesc     : PDBReadWriteDesc;
  fDesc      : array [0..maxFileDescLen] of char;
  pStr       : array [0..255] of char;
  aField     : TFldInfo;
  aKey       : TKeyInfo;
  aKeySeg    : TSegmentInfo;
  theCreator : TDBCreator;
begin
  CanClose := FALSE;
  if not inherited CanClose then
    Exit;

  edtDesc^.GetText(fDesc, maxFileDescLen+1);
  edtName^.GetText(fName, maxFileNameLen+1);

  if (StrLen(fDesc) = 0) then
  begin
    ShowError(@Self, IDS_FILEDESC_REQ, nil, 0, 0, 0);
    FocusCtl(HWindow, IDC_EDT_FILE_DESC);
    Exit;
  end;

  if (StrLen(fName) = 0) then
  begin
    ShowError(@Self, IDS_FILENAME_REQ, nil, 0, 0, 0);
    FocusCtl(HWindow, IDC_EDT_FILE_NAME);
    Exit;
  end;

  StrLCopy(pStr, dbDir, 255);
  StrLCat(pStr, fName, 255);
  StrLCat(pStr, fileDescExt, 255);
  if FileExists(pStr) then
  begin
    err := New(PErrSubst, Init(fName));
    ShowError(@Self, IDS_NAME_EXISTS, err, 0, 0, 0);
    MSDisposeObj(err);
    FocusCtl(HWindow, IDC_EDT_FILE_NAME);
    Exit;
  end;

  if not YesNoMsg(HWindow, title, SR(IDS_CREATE_FILE, pStr, 100)) then
    Exit;

  dbDesc := New(PDBReadWriteDesc, Init(fName, dbDir));

  dbDesc^.SetFileDesc(fDesc);
  dbDesc^.SetFileFlag(db_UDFile or db_MnemonicFile);
  dbDesc^.SetAutoSeq(TRUE);
  dbDesc^.SetDataType(0);

  StrCopy(aField.fldName, SR(IDS_FLD_ID, pStr, maxFldNameLen));
  aField.fldSize := 13;
  aField.fldPos := 1;
  aField.fldType := dbZString;
  aField.fldAssoc := 0;
  aField.userFlag := db_MNMID;
  aField.sysFlag := 0;
  dbDesc^.AddField(aField);

  StrCopy(aField.fldName, SR(IDS_FLD_DESC, pStr, maxFldNameLen));
  aField.fldSize := 41;
  aField.fldPos := 2;
  aField.fldType := dbZString;
  aField.fldAssoc := 0;
  aField.userFlag := db_MNMDescr;
  aField.sysFlag := 0;
  dbDesc^.AddField(aField);

  StrCopy(aKey.keyName, SR(IDS_FLD_ID, pStr, maxFldNameLen));
  aKey.keyFlag := 0;
  aKey.unique := TRUE;
  aKey.numSegs := 1;
  dbDesc^.AddKey(aKey);

  aKeySeg.fldnum := 1;
  aKeySeg.descend := FALSE;
  dbDesc^.AddKeySeg(1, aKeySeg);

  if not theCreator.CreateTable(dbDesc) then
  begin
    ShowDBError(@Self, SR(IDS_CREATE_ERR, pStr, 100), dbLastOpenError);
    FocusCtl(HWindow, IDC_EDT_FILE_NAME);
    MSDisposeObj(dbDesc);
    Exit;
  end;
  MSDisposeObj(dbDesc);
  CanClose := TRUE;
end;


{----------------------------------------------------------------------------}
{                               TDlgMnemFields                               }
{----------------------------------------------------------------------------}
{----------------------------------------------------------------------------}
{  Allows the user to select a mnemonic file to associate with a             }
{  mnemonic field, to create a new mnemonic file, or to view the             }
{  contents of a mnemonic file.                                              }
{----------------------------------------------------------------------------}

constructor TDlgMnemFields.Init(AParent: PWindowsObject; aDir: PChar;
                                aPAssoc: PAssocInfo);
begin
  inherited Init(AParent, MakeIntResource(DLG_ADD_MNEM_FIELD));
  grid := New(PGrid, InitResource(@Self, IDC_GRD_FILE, NumColsMnemFile, FALSE));
  dbDir := aDir;
  StrCopy(fName, '');
  pAssoc := aPAssoc;
end;

procedure TDlgMnemFields.SetupWindow;
var
  pStr    : array [0..40] of char;
begin
  inherited SetupWindow;
  grid^.SetHeader(ColMnemFileDesc, SR(IDS_FILEDESC, pStr, 40));
  grid^.SetHeader(ColMnemFileName, SR(IDS_FILENAME, pStr, 40));
  FillGrid;
  grid^.OptColumnWidth(ColMnemFileName);
  grid^.StretchColumn(ColMnemFileDesc);
  grid^.SetSearchColumn(ColMnemFileName);
end;

procedure TDlgMnemFields.FillGrid;
var
  filePath : array [0..maxPathLen] of char;
  fileDir  : array [0..maxPathLen] of char;
  fileName : array [0..maxFileNameLen] of char;
  fileExt  : array [0..fsExtension] of char;
  fileDesc : array [0..maxFileDescLen] of char;
  dirInfo  : TSearchRec;
  db       : PDBReadWriteFile;
  dbInfo   : TFileInfo;
  aRow     : longint;
  sel      : integer;
begin
  with grid^ do
  begin
    SetRedraw(FALSE);
    sel := GetSelIndex;
    if sel = -1 then
      sel := 0;
    ClearList;
    StrLCopy(filePath, dbDir, maxPathLen);
    StrLCat(filePath, '*', maxPathLen);
    StrLCat(filePath, fileDescExt, maxPathLen);
    FindFirst(filePath, faAnyFile - faDirectory - faVolumeID, dirInfo);
    while DosError = 0 do
    begin
      FileSplit(dirInfo.Name, fileDir, fileName, fileExt);
      { Do not want temporary file coming up in grid! }
      if (StrIComp(fileName, tempDBFileName) <> 0) then
      begin
        db := New(PDBReadWriteFile, Init(fileName, dbDir, DBOpenExclusive));
        if (db <> nil) then
        begin
          db^.dbd^.FileInfo(dbInfo);
          if (dbInfo.userFlag and db_MnemonicFile) = db_MnemonicFile then
          begin
            StrLCopy(fileDesc, dbInfo.desc, maxFileDescLen);
            AnsiUpper(fileDesc);
            aRow := AddString(fileDesc);
            SetData(ColMnemFileDesc, aRow, dbInfo.desc);
            SetData(ColMnemFileName, aRow, dbInfo.fName);
          end;
          MSDisposeObj(db);
        end;
      end;
      FindNext(dirInfo);
    end;
    if grid^.SetSelString(fName, -1) = lb_Err then
      SetSelIndex(sel);
    SetRedraw(TRUE);
  end;
end;

procedure TDlgMnemFields.DrawItem(var msg: TMessage);
begin
  grid^.DrawItem(msg);
end;

procedure TDlgMnemFields.MeasureItem(var msg: TMessage);
begin
  grid^.MeasureItem(msg);
end;

procedure TDlgMnemFields.CharToItem(var msg: TMessage);
begin
  grid^.CharToItem(msg);
end;

procedure TDlgMnemFields.BtnSelect(var msg: TMessage);
begin
  if (grid^.GetSelIndex >= 0) then
  begin
    grid^.GetData(ColMnemFileName, grid^.GetSelIndex, pAssoc^.fName, maxFileNameLen);
    pAssoc^.keyNum := 0;
    pAssoc^.numFields := 1;
    EndDlg(IDC_BTN_SELECT);
  end;
end;

procedure TDlgMnemFields.BtnCreate(var msg: TMessage);
begin
  StrCopy(fName, '');
  if (application^.ExecDialog(New(PDlgAddMnemFile, Init(@Self, dbDir, fName))) <> IDCANCEL) then
    FillGrid;
end;

procedure TDlgMnemFields.BtnView(var msg: TMessage);
var
  lo    : PListObject;
begin
  if (grid^.GetSelIndex >= 0) then
  begin
    grid^.GetData(ColMnemFileName, grid^.GetSelIndex, fName, maxFileNameLen);
    lo := New(PListObject, Init);
    lo^.ViewOnlyMnemList(@Self, fName, '');
    MSDisposeObj(lo);
  end;
end;

procedure TDlgMnemFields.GrdMnemFiles(var msg: TMessage);
begin
  if msg.LParamHI = LBN_DBLCLK then
    BtnSelect(msg)
  else
    DefWndProc(msg);
end;


{----------------------------------------------------------------------------}
{                                TFieldRec                                   }
{----------------------------------------------------------------------------}

constructor TFieldRec.Init;
begin
  inherited Init;
  FillChar(fi, SizeOf(fi), 0);
  FillChar(ai, SizeOf(ai), 0);
  ud := FALSE;
  add := FALSE;
  del := FALSE;
end;


{----------------------------------------------------------------------------}
{                                TDlgAddField                                }
{----------------------------------------------------------------------------}
{----------------------------------------------------------------------------}
{  Prompts the user for a new field name, field type, and, if string         }
{  type is specified, field length.  If the user specifies a mnemonic        }
{  field type, a mnemonic file list dialog is displayed to get the           }
{  associated file name.                                                     }
{----------------------------------------------------------------------------}

constructor TDlgAddField.Init(AParent: PWindowsObject; aDir: PChar; aFld: PFieldRec);
var
  c       : PControl;
begin
  inherited Init(AParent, MakeIntResource(DLG_ADD_FIELD));
  dbDir := aDir;
  fldRec := aFld;
  edtName := New(PEdit, InitResource(@Self, IDC_EDT_FIELD_NAME, maxFldNameLen+1));
  cmbType := New(PComboBox, InitResource(@Self, IDC_CMB_FIELD_TYPES, 25));
  c := New(PLStatic, InitResource(@Self, IDC_LBL1, 0, FALSE));
  c := New(PLStatic, InitResource(@Self, IDC_LBL2, 0, FALSE));
  lblLength := New(PLStatic, InitResource(@Self, IDC_LBL_LENGTH, 25, FALSE));
  spnLength := New(PSpinInt, InitResource(@Self, IDC_EDT_FIELD_SIZE, 5, 10, 1, MaxStringFieldLen, 1));
end;

procedure TDlgAddField.SetupWindow;
var
  pStr  : array [0..20] of char;
begin
  inherited SetupWindow;
  cmbType^.AddString(SR(IDS_FT_STR, pStr, 20));
  cmbType^.AddString(SR(IDS_FT_MNEM, pStr, 20));
  cmbType^.AddString(SR(IDS_FT_DATE, pStr, 20));
  cmbType^.AddString(SR(IDS_FT_TIME, pStr, 20));
  cmbType^.AddString(SR(IDS_FT_INT, pStr, 20));
  cmbType^.AddString(SR(IDS_FT_NUM, pStr, 20));
  cmbType^.SetSelIndex(-1);
end;

function TDlgAddField.CanClose: boolean;
begin
  CanClose := FALSE;
  if not inherited CanClose then
    Exit;

  edtName^.GetText(fldRec^.fi.fldName, maxFldNameLen+1);
  if (StrLen(fldRec^.fi.fldName) = 0) then
  begin
    ShowError(@Self, IDS_NAME_REQ, nil, 0, 0, 0);
    FocusCtl(HWindow, IDC_EDT_FIELD_NAME);
    Exit;
  end;
  fldRec^.fi.fldType := GetDBTypeFromUserType(cmbType^.GetSelIndex);
  if (fldRec^.fi.fldType = dbInvalid) then
  begin
    ShowError(@Self, IDS_TYPE_REQ, nil, 0, 0, 0);
    FocusCtl(HWindow, IDC_CMB_FIELD_TYPES);
    Exit;
  end;
  if (fldRec^.fi.fldType = dbZString) then
  begin
    if spnLength^.GetTextLen = 0 then
    begin
      ShowError(@Self, IDS_LENGTH_REQ, nil, 0, 0, 0);
      FocusCtl(HWindow, IDC_EDT_FIELD_SIZE);
      Exit;
    end;
    if not spnLength^.NumInRange then
    begin
      ShowError(@Self, IDS_LENGTH_ERR, nil, 0, 0, 0);
      FocusCtl(HWindow, IDC_EDT_FIELD_SIZE);
      Exit;
    end;
    fldRec^.fi.fldSize := spnLength^.GetLong + 1;
    fldRec^.fi.userFlag := fldRec^.fi.userFlag or db_FreeText;
  end
  else if (fldRec^.fi.fldType = dbSeqRef) then
  begin
    if application^.ExecDialog(New(PDlgMnemFields,
                        Init(@Self, dbDir, @fldRec^.ai))) = IDCANCEL then
      Exit;

    fldRec^.fi.userFlag := fldRec^.fi.userFlag or db_Mnemonic;
  end;
  CanClose := TRUE;
end;

procedure TDlgAddField.CmbTypeNotify(var msg: TMessage);
begin
  if (msg.LParamHI = CBN_SELCHANGE) then
  begin
    EnableWindow(lblLength^.HWindow, (cmbType^.GetSelIndex = 0));
    EnableWindow(spnLength^.HWindow, (cmbType^.GetSelIndex = 0));
    if (cmbType^.GetSelIndex <> 0) then
      spnLength^.Clear;
  end;
end;


{----------------------------------------------------------------------------}
{                                 TDlgFields                                 }
{----------------------------------------------------------------------------}
{----------------------------------------------------------------------------}
{  Displays the database fields.  Allows the user to select a user-          }
{  defined fields to delete from the database, to add user-defined           }
{  fields to the database, and to save a modified database with its          }
{  data.                                                                     }
{----------------------------------------------------------------------------}

constructor TDlgFields.Init(AParent: PWindowsObject; aDir: PChar;
                            aDB: PDBReadWriteFile; aEmpty: boolean);
var
  c       : PControl;
begin
  inherited Init(AParent, MakeIntResource(DLG_FIELDS));
  dbDir := aDir;
  db := aDB;
  bEmpty := aEmpty;
  cntUD := 0;
  fieldColl := nil;
  grid := New(PGrid, InitResource(@Self, IDC_GRD_FIELD, NumColsField, FALSE));
  c := New(PLStatic, InitResource(@Self, IDC_LBL1, 0, FALSE));
end;

destructor TDlgFields.Done;
begin
  inherited Done;
  MSDisposeObj(fieldColl);
end;

procedure TDlgFields.SetupWindow;
var
  dbInfo  : TFileInfo;
  pStr    : array [0..100] of char;
  j, k    : integer;
  fldRec  : PFieldRec;
begin
  inherited SetupWindow;
  k := db^.dbd^.NumFields;
  fieldColl := New(PCollection, Init(k, maxUDFlds));
  for j := 1 to k do
  begin
    fldRec := New(PFieldRec, Init);
    db^.dbd^.FieldInfo(j, fldRec^.fi);
    if (fldRec^.fi.fldAssoc > 0) then
      db^.dbd^.AssocInfo(fldRec^.fi.fldAssoc, fldRec^.ai);
    fldRec^.ud := (fldRec^.fi.userFlag and db_UD) <> 0;
    if fldRec^.ud then
      Inc(cntUD);
    fieldColl^.Insert(fldRec);
  end;
  db^.dbd^.FileInfo(dbInfo);
  SetDlgItemText(HWindow, IDC_TXT_FILE_DESC, dbInfo.desc);
  with grid^ do
  begin
    SetHeader(ColFieldName, SR(IDS_FLDNAME, pStr, 100));
    SetHeader(ColFieldType, SR(IDS_FLDTYPE, pStr, 100));
    SetHeader(ColFieldUserDef, SR(IDS_FLD_UD, pStr, 100));
    SetHeader(ColFieldFlags, 'Flags');         {- not to be seen by user }
    SetAlignment(ColFieldUserDef, GRID_CENTER);
    SetAlignment(ColFieldFlags, GRID_RIGHT);
    FillGrid;
    OptColumnWidth(ColFieldName);
    OptColumnWidth(ColFieldType);
    OptColumnWidth(ColFieldUserDef);
    SetColumnWidth(ColFieldFlags, 0);          {- not to be seen by user }
    StretchColumn(ColFieldName);
  end;
end;

{----------------------------------------------------------------------------}
{  Fills the grid with field data, enabling the row if the field is          }
{  user-defined.  Field data items that are flaged as deleted are not        }
{  added to the grid.                                                        }
{----------------------------------------------------------------------------}
procedure TDlgFields.FillGrid;
var
  i, j  : integer;
  sel   : integer;

  procedure AddToGrid(fldRec: PFieldRec); Far;
  var
    aRow : longint;
    pStr : array [0..25] of char;
  begin
    if ((fldRec^.fi.userFlag and db_Hidden) = 0) and
        not fldRec^.del then
    with grid^ do
    begin
      aRow := AddString('');
      SetData(ColFieldName, aRow, fldRec^.fi.fldName);
      DBUserTypeStr(fldRec^.fi.fldType, fldRec^.fi.fldSize,
              (fldRec^.fi.userFlag and db_Mnemonic) = db_Mnemonic, fldRec^.ai.fName, pStr, 20);
      SetData(ColFieldType, aRow, pstr);
      BoolToStr((fldRec^.fi.userFlag and db_UD) = db_UD, pStr, SizeOf(pStr)-1);
      SetData(ColFieldUserDef, aRow, pStr);
      HexStr(fldRec^.fi.userFlag, SizeOf(fldRec^.fi.userFlag), pStr, 10);
      SetData(ColFieldFlags, aRow, pStr);
      SetItemData(aRow, fieldColl^.IndexOf(fldRec));
      EnableRow(aRow, fldRec^.ud);
    end;
  end;

begin
  with grid^ do
  begin
    sel := GetSelIndex;
    if sel = -1 then
      sel := 0;
    SetRedraw(FALSE);
    ClearList;
    fieldColl^.ForEach(@AddToGrid);
    i := GetRowCount;
    if (sel >= i) then
      sel := i - 1;
    SetSelIndex(sel);
    SetRedraw(TRUE);
  end;
  EnableButtons;
end;

procedure TDlgFields.EnableButtons;
var
  bEnable        : boolean;
begin
  bEnable := ModifiedFields;
  EnableWindow(GetItemHandle(IDC_BTN_SAVE), bEnable);
  bEnable := cntUD < maxUDFlds;
  EnableWindow(GetItemHandle(IDC_BTN_ADD), bEnable);
  bEnable := grid^.GetRowEnabled(grid^.GetSelIndex);
  EnableWindow(GetItemHandle(IDC_BTN_DELETE), bEnable);
end;

procedure TDlgFields.DrawItem(var msg: TMessage);
begin
  grid^.DrawItem(msg);
end;

procedure TDlgFields.MeasureItem(var msg: TMessage);
begin
  grid^.MeasureItem(msg);
end;

procedure TDlgFields.CharToItem(var msg: TMessage);
begin
  grid^.CharToItem(msg);
end;

{----------------------------------------------------------------------------}
{  Adds a new user-defined field item to the collection, if the field        }
{  name does not already exist.                                              }
{----------------------------------------------------------------------------}
procedure TDlgFields.BtnAdd(var msg: TMessage);
var
  dlg     : PDlgAddField;
  fldRec  : PFieldRec;

  function NameExists(fRec: PFieldRec): boolean; Far;
  begin
    NameExists := not fRec^.del and
                  (StrIComp(fldRec^.fi.fldName, fRec^.fi.fldName) = 0);
  end;

begin
  if cntUD >= maxUDFlds then
  begin
    ShowError(@Self, IDS_MAX_FLDS, err, 0, 0, 0);
    Exit;
  end;

  fldRec := New(PFieldRec, Init);
  fldRec^.fi.userFlag := db_UD;
  fldRec^.ud := TRUE;
  fldRec^.add := TRUE;
  dlg := New(PDlgAddField, Init(@Self, dbDir, fldRec));
  if (application^.ExecDialog(dlg) <> IDCANCEL) then
  begin
    if fieldColl^.FirstThat(@NameExists) = nil then
    begin
      Inc(cntUD);
      fieldColl^.Insert(fldRec);
      FillGrid;
      grid^.SetSelIndex(grid^.GetRowCount - 1);
      EnableButtons;
    end
    else
    begin
      err := New(PErrSubst, Init(fldRec^.fi.fldName));
      ShowError(@Self, IDS_NAME_DUP, err, 0, 0, 0);
      MSDisposeObj(err);
      MSDisposeObj(fldRec);
    end;
  end
  else
    MSDisposeObj(fldRec);
end;

{----------------------------------------------------------------------------}
{  Deletes a new user-defined field or flags an old user-defined field       }
{  as deleted.                                                               }
{----------------------------------------------------------------------------}
procedure TDlgFields.BtnDelete(var msg: TMessage);
var
  sel     : integer;
  fldRec  : PFieldRec;
  pStr    : array [0..255] of char;
  fldName : array [0..maxFldNameLen] of char;
begin
  sel := grid^.GetSelIndex;
  if sel >= 0 then
  begin
    fldRec := fieldColl^.At(grid^.GetItemData(sel));
    if fldRec^.ud then
    begin
      grid^.GetData(ColFieldName, sel, fldName, maxFldNameLen);
      if fldRec^.add or bEmpty then
        SR(IDS_DELETE_ADDED_FLD, pStr, 255)
      else
        SR(IDS_DELETE_FLD, pStr, 255);
      StrLCat(pStr, fldName, 255);
      if YesNoMsg(HWindow, title, pStr) then
      begin
        Dec(cntUD);
        if fldRec^.add then
          fieldColl^.Free(fldRec)
        else
          fldRec^.del := TRUE;
        FillGrid;
      end;
    end
    else
    begin
      ShowError(@Self, IDS_DELETE_FLD_ERR, nil, 0, 0, 0);
    end;
  end;
end;

{----------------------------------------------------------------------------}
{  If the user confirms the save, renames the old database, creates a        }
{  new copy of the database, deletes and adds the user-defined fields,       }
{  and transfers the data from the old database to the new one.              }
{----------------------------------------------------------------------------}
procedure TDlgFields.BtnSave(var msg: TMessage);
var
  theCreator : TDBCreator;
  info       : TFileInfo;
  tempDBD    : PDBReadWriteDesc;
  filePath   : array [0..maxPathLen] of char;
  pStr       : array [0..100] of char;
  fldError   : boolean;
  retCode    : integer;
  j          : integer;
  srcFile    : PDBFile;
  fil        : File;

  procedure DeleteFld(fldRec: PFieldRec); Far;
  begin
    if fldRec^.del then
    begin
      if not tempDBD^.DeleteField(fieldColl^.IndexOf(fldRec) + 1) then
      begin
        fldError := TRUE;
        ShowDBError(@Self, SR(IDS_FLD_DEL_ERR, pStr, 100), tempDBD^.dbErrorNum);
      end;
    end;
  end;

  procedure AddFld(fldRec: PFieldRec); Far;
  var
    fldNum   : integer;
    desc     : PDBReadWriteDesc;
    di       : TDelAssocInfo;
  begin
    if fldRec^.add then
    begin
      fldNum := tempDBD^.AddField(fldRec^.fi);
      if (fldNum = 0) or not tempDBD^.StoreDef then
      begin
        fldError := TRUE;
        ShowDBError(@Self, SR(IDS_FLD_ADD_ERR, pStr, 100), tempDBD^.dbErrorNum);
        Exit;
      end;

      if (fldRec^.ai.numFields > 0) then
      begin
        fldRec^.ai.fldNum[1]:= fldNum;

        if (tempDBD^.AddAssoc(fldRec^.ai) = 0) then
        begin
          fldError := TRUE;
          ShowDBError(@Self, SR(IDS_ASSOC_ADD_ERR, pStr, 100), dbLastOpenError);
          tempDBD^.DeleteField(fldNum);
          Exit;
        end;

        desc := New(PDBReadWriteDesc,
                    Init(fldRec^.ai.fName, tempDBD^.FilePath(pStr, 255)));
        if desc = nil then
        begin
          fldError := TRUE;
          ShowDBError(@Self, SR(IDS_ASSOC_ERR, pStr, 100), dbLastOpenError);
          tempDBD^.DeleteField(fldNum);
          Exit;
        end;

        FillChar(di, SizeOf(di), 0);
        StrLCopy(di.fName, info.fName, maxFileNameLen);
        di.delAction := DISALLOW_DELETE;
        di.bDownline := FALSE;
        StrCopy(di.assocFldName, fldRec^.fi.fldName);
        if (desc^.AddDelAssoc(di) = 0) or not desc^.StoreDef then
        begin
          fldError := TRUE;
          ShowDBError(@Self, SR(IDS_DELASSOC_ERR, pStr, 100), desc^.dbErrorNum);
          tempDBD^.DeleteField(fldNum);
        end;
        MSDisposeObj(desc);
      end;
    end;
  end;

begin
  if not ModifiedFields then
  begin
    ShowError(@Self, IDS_NO_CHANGE, nil, 0, 0, 0);
    Exit;
  end;

  if not YesNoMsg(HWindow, title, SR(IDS_SAVE_CHANGES, pStr, 100)) then
    Exit;

  db^.dbd^.FileInfo(info);
  db^.dbd^.FilePath(filePath, maxPathLen);

  tempDBD := New(PDBReadWriteDesc, Init(tempDBFileName, filePath));
  if tempDBD = nil then
  begin
    {- no database exists. Make sure there is not another file using
       the temp.dbi/dbd file names. If so, delete them (if possible) }
    {$I-}
    Assign(fil, tempDBFileName + datFileExt);
    Erase(fil);
    j:= IOResult;
    if j <> 0 then
    begin
      ShowDBError(@Self, SR(IDS_DELETE_ERR, pStr, 100), j);
      exit;
    end;
    Assign(fil, tempDBFileName + fileDescExt);
    Erase(fil);
    j:= IOResult;
    if j <> 0 then
    begin
      ShowDBError(@Self, SR(IDS_DELETE_ERR, pStr, 100), j);
      exit;
    end;
  end
  else
  begin
    retCode := DBDeleteDatabase(tempDBD);
    if retCode <> 0 then
    begin
      ShowDBError(@Self, SR(IDS_DELETE_ERR, pStr, 100), retCode);
      Exit;
    end;
  end;

  {- rename origial file to TEMP }
  if not db^.dbd^.RenameDatabase(tempDBFileName) then
  begin
    ShowDBError(@Self, SR(IDS_RENAME_ERR, pStr, 100), db^.dbd^.dbErrorNum);
    db^.dbd^.RenameDatabase(info.fName);
    Exit;
  end;

  tempDBD := New(PDBReadWriteDesc, Init(info.fName, filePath));
  tempDBD^.CopyDesc(db^.dbd);

  fldError := FALSE;

  for j := fieldColl^.Count - 1 downto 0 do
    DeleteFld(fieldColl^.At(j));

  fieldColl^.ForEach(@AddFld);

  if fldError then
  begin
    retCode := DBDeleteDatabase(tempDBD);
    if not db^.dbd^.RenameDatabase(info.fName) then
      ShowDBError(@Self, SR(IDS_RENAME_ERR, pStr, 100), db^.dbd^.dbErrorNum);
    Exit;
  end;

  if not theCreator.CreateTable(tempDBD) then
  begin
    ShowDBError(@Self, SR(IDS_CREATE_ERR, pStr, 100), dbLastOpenError);
    retCode := DBDeleteDatabase(tempDBD);
    if not db^.dbd^.RenameDatabase(info.fName) then
      ShowDBError(@Self, SR(IDS_RENAME_ERR, pStr, 100), db^.dbd^.dbErrorNum);
    Exit;
  end;

  MSDisposeObj(tempDBD);
  CopyData(info.fName, filePath);

  retCode := DBDeleteDatabase(db^.dbd);
  EndDlg(IDC_BTN_SAVE);
end;

procedure TDlgFields.Cancel(var msg: TMessage);
var
  pStr     : array [0..100] of char;
begin
  if ModifiedFields then
    if not YesNoMsg(HWindow, title, SR(IDS_ABANDON_CHANGES, pStr, 100)) then
      Exit;

  inherited Cancel(msg);
end;

procedure TDlgFields.GrdField(var msg: TMessage);
begin
  if msg.LParamHI = LBN_SELCHANGE then
    EnableButtons
  else
    DefWndProc(msg);
end;

function  TDlgFields.ModifiedFields: boolean;

  function IsModified(fldRec: PFieldRec): boolean; Far;
  begin
    IsModified := fldRec^.add or fldRec^.del;
  end;

begin
  ModifiedFields := (fieldColl^.FirstThat(@IsModified) <> nil);
end;

{----------------------------------------------------------------------------}
{  Copies data from the old database file to the new modified database       }
{  file.  A status dialog is displayed to show progress.                     }
{----------------------------------------------------------------------------}
procedure TDlgFields.CopyData(fileName, filePath: PChar);
var
  srcFile    : PDBFile;
  tempdb     : PDBCopyFile;
  pStr       : array [0..100] of char;
  numRec     : longint;
  i          : longint;
  statDlg    : PPctStatDlg;
begin
  srcFile := New(PDBFile, Init(tempDBFileName, filePath, DBOpenExclusive));
  if (srcFile = nil) then
  begin
    SR(IDS_CANNOT_OPEN, pStr, 100);
    StrLCat(pStr, tempDBFileName, 100);
    ShowDBError(@Self, pStr, dbLastOpenError);
    Exit;
  end;

  numRec := srcFile^.dbc^.NumRecords;
  if numRec <> 0 then
  begin
    tempdb := New(PDBCopyFile, Init(fileName, filePath, DBOpenExclusive));
    if (tempdb = nil) then
    begin
      SR(IDS_CANNOT_OPEN, pStr, 100);
      StrLCat(pStr, fileName, 100);
      ShowDBError(@Self, pStr, dbLastOpenError);
      Exit;
    end;

    statDlg := New(PPctStatDlg, Init(@Self, MakeIntResource(DLG_STATUS), TRUE, IDC_PERCENTBAR));
    Application^.MakeWindow(statDlg);
    statDlg^.SetText(SR(IDS_PERCENT_COMPLETE, pStr, 100), IDC_LBL1);

    i := 0;
    if srcFile^.dbc^.GetFirst(srcFile^.dbr) then
      repeat
        tempdb^.dbr^.ClearRecord;
        tempdb^.dbr^.CopyFields(srcFile^.dbr);
        if not tempdb^.dbc^.InsertRec(tempdb^.dbr) then
        begin
          ShowDBError(@Self, SR(IDS_INSERT_ERR, pStr, 100), tempdb^.dbr^.dbErrorNum);
        end;
        Inc(i);
        statDlg^.CompPctLevel(i, numRec);
        statDlg^.Update;
      until not srcFile^.dbc^.GetNext(srcFile^.dbr);

    MSDisposeObj(statDlg);
    MSDisposeObj(tempdb);
  end;
  MSDisposeObj(srcFile);
end;


{----------------------------------------------------------------------------}
{                                  TDlgMain                                  }
{----------------------------------------------------------------------------}
{----------------------------------------------------------------------------}
{  Displays the user-editable database files, enabling the row if the        }
{  file contains no data.  Allows the user to select a file with which       }
{  to add and delete fields.                                                 }
{----------------------------------------------------------------------------}

constructor TDlgMain.Init(AParent: PWindowsObject);
begin
  inherited Init(AParent, MakeIntResource(DLG_MAIN));
  grid := New(PGrid, InitResource(@Self, IDC_GRD_FILE, NumColsFile, FALSE));
end;

destructor TDlgMain.Done;
begin
  ClearGrid;
  inherited Done;
end;

procedure TDlgMain.SetupWindow;
var
  filePath : array [0..maxPathLen] of char;
  fileName : array [0..maxFileNameLen] of char;
  fileExt  : array [0..fsExtension] of char;
  pStr     : array [0..100] of char;
  tw       : TWaitCursor;
begin
  inherited SetupWindow;
  tw.Init;
  grid^.SetHeader(ColFileDesc, SR(IDS_FILEDESC, pStr, 100));
  grid^.SetHeader(ColFileName, SR(IDS_FILENAME, pStr, 100));
  grid^.SetHeader(ColFileAutoSeq, 'Auto Seq'); {- not to be seen by user }
  grid^.SetHeader(ColFileUserDef, SR(IDS_FLD_UD, pStr, 100));
  grid^.SetAlignment(ColFileAutoSeq, GRID_CENTER);
  grid^.SetAlignment(ColFileUserDef, GRID_CENTER);
  grid^.SetColumnWidth(ColFileName, 0);
  grid^.SetColumnWidth(ColFileAutoSeq, 0);     {- not to be seen by user }
  grid^.SetColumnWidth(ColFileUserDef, 0);
  grid^.StretchColumn(ColFileDesc);

  if (ParamCount > 0) then
  begin
    StrPCopy(dbDir, ParamStr(1));
    if (dbDir[StrLen(dbDir)] <> ':') and (dbDir[StrLen(dbDir)] <> '\') then
      StrLCat(dbDir, '\', maxPathLen);
  end
  else
  begin
    StrPCopy(filePath, ParamStr(0));
    FileSplit(filePath, dbDir, fileName, fileExt);
  end;
  tw.Done;
  FillGrid;
end;

procedure TDlgMain.FillGrid;
var
  filePath : array [0..maxPathLen] of char;
  fileDir  : array [0..maxPathLen] of char;
  fileName : array [0..maxFileNameLen] of char;
  fileExt  : array [0..fsExtension] of char;
  dirInfo  : TSearchRec;
  db       : PDBReadWriteFile;
  dbInfo   : TFileInfo;
  pStr     : array [0..100] of char;
  aRow     : longint;
  sel      : integer;
  tw       : TWaitCursor;
begin
  tw.Init;
  with grid^ do
  begin
    sel := GetSelIndex;
    if sel = -1 then
      sel := 0;
    SetRedraw(FALSE);
    ClearGrid;
    StrLCopy(filePath, dbDir, maxPathLen - fsExtension);
    StrLCat(filePath, '*', maxPathLen - fsExtension);
    StrLCat(filePath, fileDescExt, maxPathLen);
    FindFirst(filePath, faAnyFile - faDirectory - faVolumeID, dirInfo);
    while DosError = 0 do
    begin
      FileSplit(dirInfo.Name, fileDir, fileName, fileExt);
      { Do not want temporary file coming up in grid! }
      if (StrComp(fileName, tempDBFileName) <> 0) then
      begin
        db := New(PDBReadWriteFile, Init(fileName, dbDir, DBOpenExclusive));
        if (db = nil) then
        begin
          SR(IDS_CANNOT_OPEN, pStr, 100);
          StrLCat(pStr, dbDir, 100);
          StrLCat(pStr, dirInfo.Name, 100);
          ShowDBError(@Self, pStr, dbLastOpenError);
          ClearGrid;
          tw.Done;
          Exit;
        end;

        db^.dbd^.FileInfo(dbInfo);
        if (dbInfo.userFlag and db_UserEdit) = db_UserEdit then
        begin
          StrLCopy(pStr, dbInfo.desc, 100);
          AnsiUpper(pStr);
          aRow := AddString(pStr);
          SetData(ColFileDesc, aRow, dbInfo.desc);
          SetData(ColFileName, aRow, dbInfo.fName);
          BoolToStr(dbInfo.autoSeq, pStr, SizeOf(pStr)-1);
          SetData(ColFileAutoSeq, aRow, pStr);
          BoolToStr((dbInfo.userFlag and db_UDFile) = db_UDFile, pStr, SizeOf(pStr)-1);
          SetData(ColFileUserDef, aRow, pStr);
          SetItemData(aRow, longint(db));
          Str(db^.dbc^.NumRecords, pstr);
          SetData(ColFileNumRecs, aRow, pstr);
(*          EnableRow(aRow, (db^.dbc^.NumRecords = 0));*)
          MSDisposeObj(db^.dbr);
          db^.dbr := nil;
          MSDisposeObj(db^.dbc);
          db^.dbc := nil;
        end
        else
          MSDisposeObj(db);
      end;
      FindNext(dirInfo);
    end;
    SetSelIndex(sel);
    SetRedraw(TRUE);
  end;
  tw.Done;
end;

procedure TDlgMain.ClearGrid;
var
  db     : PDBReadWriteFile;
  maxRow : longint;
  aRow   : longint;
begin
  with grid^ do
  begin
    maxRow := GetRowCount - 1;
    for aRow := 0 to maxRow do
    begin
      db := pointer(GetItemData(aRow));
      MSDisposeObj(db);
    end;
    ClearList;
  end;
end;

procedure TDlgMain.EnableButtons;
var
  bEnable    : boolean;
  sel        : integer;
begin
  bEnable := FALSE;
  sel := grid^.GetSelIndex;
  if sel <> lb_Err then
    bEnable := TRUE;

  EnableWindow(GetItemHandle(IDC_BTN_EDIT), bEnable);
end;

procedure TDlgMain.DrawItem(var msg: TMessage);
begin
  grid^.DrawItem(msg);
end;

procedure TDlgMain.MeasureItem(var msg: TMessage);
begin
  grid^.MeasureItem(msg);
end;

procedure TDlgMain.CharToItem(var msg: TMessage);
begin
  grid^.CharToItem(msg);
end;

procedure TDlgMain.BtnEdit(var msg: TMessage);
var
  db   : PDBReadWriteFile;
  dlg  : PDlgFields;
  sel  : integer;
  enab : boolean;
  pStr : array [0..100] of char;
  k    : longint;
  code : integer;
begin
  sel := grid^.GetSelIndex;
  if sel <> lb_Err then
  begin
    grid^.GetData(ColFileNumRecs, sel, pstr, sizeof(pstr)-1);
    Val(pstr, k, code);
    enab:= (k = 0); {grid^.GetRowEnabled(sel);}
    if not (enab or NoYesMsg(HWindow, title, SR(IDS_CONTAINS_DATA, pStr, 100))) then
      Exit;
    db := pointer(grid^.GetItemData(sel));
    dlg := New(PDlgFields, Init(@Self, dbDir, db, enab));
    if (application^.ExecDialog(dlg) = IDC_BTN_SAVE) then
      FillGrid;
  end;
end;

procedure TDlgMain.GrdFile(var msg: TMessage);
begin
  if msg.LParamHI = LBN_SELCHANGE then
    EnableButtons
  else if msg.LParamHI = LBN_DBLCLK then
    BtnEdit(msg)
  else
    DefWndProc(msg);
end;

function TDlgMain.GetClassName: PChar;
begin
  GetClassName:= GetModuleName(MOD_EDSCHEMA);
end;

procedure TDlgMain.GetWindowClass(var aWndClass: TWndClass);
begin
  inherited GetWindowClass(aWndClass);
  aWndClass.HIcon:= LoadIcon(HInstance, MakeIntResource(ICON_1));
end;

procedure TDlgMain.WMDMSQuery(var msg: TMessage);
{- respond to the task module and tell it who I am }
begin
  StrLCopy(PChar(msg.lParam), application^.Name, msg.wParam);
  msg.result:= DMSMainProgRetCode;
  SetWindowLong(HWindow, DWL_MSGRESULT, msg.result);  {- set message result (for dialogs) }
end;


{----------------------------------------------------------------------------}
{                                     TMe                                    }
{----------------------------------------------------------------------------}

procedure TMe.InitMainWindow;
begin
  if INIUse3D then
    Register3dApp(TRUE, TRUE, TRUE);
  MainWindow := New(PDlgMain, Init(nil));
end;


BEGIN
  if OKToRun(MOD_EDSCHEMA, false) then
  begin
    RegisterErrorTitle(SR(IDS_TITLE, title, 40));
    me.Init(GetModuleName(MOD_EDSCHEMA));
    me.Run;
    me.Done;
  end;
END.
