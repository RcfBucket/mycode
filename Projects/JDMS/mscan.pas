unit MScan;
{----------------------------------------------------------------------------}
{  Module Name  : MSCAN.PAS                                                  }
{  Programmer   : everybody                                                  }
{  Date Created : 02/95                                                      }
{                                                                            }
{  Purpose -                                                                 }
{  This module is the global module for the DMS.  Miscellaneous global       }
{  functions and constants are defined here. Nothing in this module should   }
{  be here if it is only used by one module.                                 }
{                                                                            }
{  Assumptions -                                                             }
{  None                                                                      }
{                                                                            }
{  Initialization -                                                          }
{  An application should call the 'RegisterErrorTitle' procedure contained   }
{  herein to set the title that will be used in the error message box. This  }
{  title is used to identify what application is creating the error.         }
{                                                                            }
{  Revision History -                                                        }
{                                                                            }
{  Version  Date      Who    Did What                                        }
{  -------  --------  ------ ----------------------------------------------- }
{  1.00     02/95     rcf    Modified the original MScan from 2.0            }
{                                                                            }
{----------------------------------------------------------------------------}

INTERFACE

uses
  Objects,
  WinProcs,
  WinTypes;

type
  PWaitCursor = ^TWaitCursor;
  TWaitCursor = object(TObject)
    oldCursor  : HCursor;
    {}
    constructor Init;
    destructor Done; virtual;
  end;

const
  profFile       = 'DMS.INI';   {- DMS profile string file }
  appName        = 'DMS';       {- application name for WriteProfilestring }

  {- Module ID numbers.  These numbers are allocated in blocks of 256. }
  MOD_DMSTRING    = $0000;  { General purpose global strings }
  MOD_FILMAIN     = $0100;  {- File Maintenance }
  MOD_RPTGEN      = $0300;  {- report editor }
  MOD_GSDEF       = $0400;  {- gram stain definition }
  MOD_CUSTINTR    = $0500;  {- interpretation editor }
  MOD_AS4QC       = $0600;  {- AS4QC module }
  MOD_EDSCHEMA    = $0700;  {- Database Schema Editor }
  MOD_MPSS        = $0800;  {- Database MPSS }
  MOD_GPIB        = $0900;  {- GPIB }
  MOD_AUTOSCAN    = $0A00;  {- autoscan module }
  MOD_PANDUMP     = $0B00;  {- Panel Data Report }
  MOD_FINAL       = $0C00;  {- Finalize specimens module }
  MOD_MOVEISO     = $0C30;  {- Main menu Move isolates module }
  MOD_PATREPS     = $0D00;  {- Patient reports module }
  MOD_TWOWAY      = $0E00;  {:)Mainframe Interface }
{ MOD_next        = $0F00;} {- ????? }

  {- ui is hogging up the entire $2000 range for now}
  MOD_UIFLDS      = $2000;  {- user interface fields }
  MOD_UICOMMON    = $2030;  {- UI Data entry general }
  MOD_UISPECIMEN  = $2100;  {- Specimen data entry }
  MOD_UIPATIENT   = $2130;  {- Patient data entry }
  MOD_UISPECLOG   = $2160;  {- specimen login }
  MOD_UIGSRESULTS = $2190;  {- gram stain results }
  MOD_UIISO       = $21C0;  {- isolate entry }
  MOD_UIQEORD     = $21F0;  {- quick order entry }
  MOD_UIIDMIC     = $2220;  {- ID MIC entry }
  MOD_UIRESULTS   = $2250;  {- UI results entry stuff }
  MOD_CTLLIB      = $2800;  {- list edit control stuff (ctllib) }
  MOD_LISTLIB     = $2900;  {  List Library (includes UI) }

  {- wamain is hogging up the entire $3000 range for now}
  MOD_WAMAIN      = $3000;  {- WAMain App }
  MOD_WAPANEL     = $3A00;  {- WA Panel module }
  MOD_WAREADER    = $3B00;  {- WA Panel Reader module }
  MOD_JAMCHECK    = $3C00;  {- WA Jam Checker module }
  MOD_WAMAINT     = $3D00;  {- WA Maint module }
  MOD_WAQC        = $3E00;  {- WA QC module }
  MOD_WACPSR      = $3F00;  {- WA CPSR module }

  MOD_PRTMAIN     = $4000;  {- report modules }
  MOD_PRTLPR      = $4050;  {- long patient reports module }
  MOD_PRTEPI      = $4060;  {- EPI main reports module }
  MOD_EPIMIC      = $4070;  {- EPIMIC reports module }
  MOD_EPISUS      = $4080;  {- EPISUS reports module }
  MOD_EPIUSER     = $4090;  {- EPIUSER reports module }
  MOD_ENGPRINT    = $4100;  {- report engine (ENGPRINT) }
  MOD_PRTMNEM     = $4120;  {- Mnemonic print lib (PRTMNEM) }
  MOD_RPTENG      = $4500;  {- Report section engine module (ENGINE) }
  MOD_CONVERT     = $4600;  {- used by the convert program $4600-$46FF }
{  MOD_AVAIL       = $4700;}

  {NOTE: errors in the ranges 1-199, 1000-1999 and 2000-2099 ($6000) are
         BTRIEVE errors }
  MOD_BTRV        = $6000;   {- reserving $6000 - $6900 }
  MOD_DB          = MOD_BTRV + $100;

  {- $7000-$7100 has been broken down for smaller lib that dont need 256 ids }
  MOD_TRAYLIB     = $7000;  {- tray library }
  MOD_IDLIB       = $7020;  {- ID library }
  MOD_EDITDB      = $7030;  {- EDITDB.EXE }
  MOD_RANDDATA    = $7040;  {- RANDDATA.EXE }
  MOD_JNDATA      = $7050;  {- JNDATA.EXE }
  MOD_INTERP      = $7060;  {- Interps lib }
  MOD_ORGGROUP    = $7070;  {- ORGGROUP lib }
  MOD_BIOTYPE     = $7080;  {- BIOTypes lib }
  MOD_PURGE       = $7090;  {- Purge module }
  MOD_BARCODES    = $70B0;  {- Barcodes module }
  MOD_INTRPMAP    = $70C0;  {- Therapy interpretation mapping library }
  MOD_SPINEDIT    = $70D0;  {- SpinEdit library }
  MOD_GRIDLIB     = $70E0;  {- Grid library }
  MOD_PRINTPREV   = $70F0;  {- Print library (printprev) }
  MOD_PRNOBJ      = $7100;  {- Print Object library }
  MOD_AS4RDR      = $7120;  {- AS4 UI code }
  MOD_DMSMAIN     = $7130;  {- DMSMain App }
  MOD_THRESHLD    = $7140;  {- Database Threshold options }
  MOD_CHKSPACE    = $7150;  {- Disk space checking for DB thresholds feature }
  MOD_HWCONFIG    = $7160;  {- Hardware configuration }
  MOD_CUSTOM      = $7170;  {- Customize screen implementation }
  MOD_STATUS      = $7180;  {- Status library }
  MOD_RESLIB      = $7190;  {- result library }
  MOD_PNLVIEW     = $71A0;  {- Panel Reader Screen Lib }
  MOD_REPORT      = $71B0;  {- Report module }
  MOD_DRUGSEL     = $71C0;  {- Drug Selection module }
  MOD_TASKS       = $71D0;  {- Task manager module }

  MOD_WA          = $8000;  {- All numbers above $8000 are reserved for the WA }


function FileExists(fileName: PChar): Boolean;

{- functions to manipulate set/Family fields }
function MakeSetFamily(aSet, aFamily: byte): word;
function ExtractFamily(setFamily: word): byte;
function ExtractSet(setFamily: word): byte;
function GetSetAsStr(aSet: byte; buf: PChar; maxLen: integer): PChar;
function ExtractSetAsStr(aSetFamily: word; buf: PChar; maxLen: integer): PChar;
function GetFamilyAsStr(aSet, aFamily: byte; buf: PChar; maxLen: integer): PChar;
function ExtractFamilyAsStr(aSetFamily: word; buf: PChar; maxLen: integer): PChar;
procedure SendChangeTitleMsg;
procedure MSStrDispose(var s: PChar);
procedure MSFreeMem(var p; size: word);
procedure MSDisposeObj(var obj);

IMPLEMENTATION

uses
  OWindows,
  Strings,
  UserMsgs,
  DMSDebug,
  APITools,
  DMString;

function FileExists(fileName: PChar): Boolean;
{ Boolean function that returns True if the file exists;otherwise,
  it returns False. Closes the file if it exists. }
var
  f       : file;
  curMode : integer;
begin
  curMode:= FileMode;
  {$I-}
  Assign(f, fileName);
  FileMode := 0;  { Set file access to read only }
  Reset(f);
  Close(f);
  {$I+}
  FileMode:= curMode;
  FileExists := (IOResult = 0);
end;  { FileExists }

constructor TWaitCursor.Init;
begin
  inherited Init;
  oldCursor:= SetCursor(LoadCursor(0, IDC_WAIT));
  {- if no mouse is present the show the hour glass in the center of the screen }
  if GetSystemMetrics(SM_MOUSEPRESENT) = 0 then
  begin
    SetCursorPos( GetSystemMetrics(SM_CXSCREEN) div 2,
                  GetSystemMetrics(SM_CYSCREEN) div 2);
    ShowCursor(true);
  end;
end;

destructor TWaitCursor.Done;
begin
  if GetSystemMetrics(SM_MOUSEPRESENT) = 0 then
    ShowCursor(false);
  SetCursor(oldCursor);
  inherited Done;
end;


function ExtractSet(setFamily: word): byte;
{- extracts the set from a set-Family field.
   setFamily is the setFamily field to extract from }
begin
  ExtractSet:= (setFamily shr 8);
end;

function ExtractFamily(setFamily: word): byte;
{- extracts the Family from a set-Family field.
   setFamily is the setFamily field to extract from }
begin
  ExtractFamily:= (setFamily and $FF);
end;

function MakeSetFamily(aSet, aFamily: byte): word;
{- create a set-Family field }
begin
  MakeSetFamily:= (word(aSet) shl 8) or aFamily;
end;

function GetSetAsStr(aSet: byte; buf: PChar; maxLen: integer): PChar;
{ return a string representation of a Panel Set byte value }
begin
  case aSet of
    1, 20:      SR(IDS_GRAMNEGATIVE, buf, maxLen);
    2, 5, 21:   SR(IDS_GRAMPOSITIVE, buf, maxLen);
    3:          SR(IDS_ANAEROBE, buf, maxLen);
    4:          SR(IDS_HNID, buf, maxLen);
    6:          SR(IDS_YEAST, buf, maxLen);
    else        SR(IDS_UNKNOWN, buf, maxLen);
  end;
  GetSetAsStr:= buf;
end;

function ExtractSetAsStr(aSetFamily: word; buf: PChar; maxLen: integer): PChar;
{- extract the set from the set Family and return a string representation for it.
   Ie,  set  1 = Gram negative, 2 = Gram Positive 3 = Anaerobe etc }
begin
  ExtractSetAsStr:= GetSetAsStr(ExtractSet(aSetFamily), buf, maxLen);
end;

function GetFamilyAsStr(aSet, aFamily: byte; buf: PChar; maxLen: integer): PChar;
{ return a string representation of an Organism Family byte value }
begin
  StrCopy(buf, '');
  case aSet of
    1, 20:
      begin
        case aFamily of
          1:  SR(IDS_FERMENTER, buf, maxLen);
          2:  SR(IDS_NONFERMENTER, buf, maxLen);
        end;
      end;
    2, 5, 21:
      begin
        case aFamily of
          1:  SR(IDS_STREP, buf, maxLen);
          2:  SR(IDS_MICROLIST, buf, maxLen);
        end;
      end;
    3:
      begin
        case aFamily of
          1:  SR(IDS_GRAMNEGBACILLI, buf, maxLen);
          2:  SR(IDS_GRAMPOSBACILLI, buf, maxLen);
          3:  SR(IDS_COCCI, buf, maxLen);
          4:  SR(IDS_CLOSTRIDIA, buf, maxLen);
        end;
      end;
  end;
  GetFamilyAsStr:= buf;
end;

function ExtractFamilyAsStr(aSetFamily: word; buf: PChar; maxLen: integer): PChar;
{- extract the Family (family) from the set-Family and return a string
   representation for it. Ie,  Fermenter, Gram Neg Baccili }
begin
  ExtractFamilyAsStr:= GetFamilyAsStr(ExtractSet(aSetFamily), ExtractFamily(aSetFamily), buf, maxLen);
end;

procedure SendChangeTitleMsg;
begin
  SendMessage(application^.mainWindow^.HWindow, WM_CHANGETITLE, 0, 0);
end;

procedure MSDisposeObj(var obj);
{- used to detroy objects. This procedure will destroy the object and then set
   the pointer value to nil }
begin
  if PObject(obj) <> nil then
  begin
    Dispose(PObject(obj), Done);
    PObject(obj):= nil;
  end;
end;

procedure MSFreeMem(var p; size: word);
{- used to free up memory. This procedure will free the memory allocated to [p]
   and then set the pointer value to nil.
   P can be any pointer type that was allocated using GetMem. }
begin
  if pointer(p) <> nil then
  begin
    FreeMem(pointer(p), size);
    pointer(p):= nil;
  end;
end;

procedure MSStrDispose(var s: PChar);
{- used to free strings allocated by StrNew and then set the pointer to nil. }
begin
  if s <> nil then
  begin
    StrDispose(s);
    s:= nil;
  end;
end;

END.

