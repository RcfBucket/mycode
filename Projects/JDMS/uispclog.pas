unit UISpcLog;

{- Specimen Login }

INTERFACE

uses
  DBLib,
  DBTypes,
  OWindows;

{- call Specimen Login. aSpecSeq is a specimen record to preload }
procedure UISpecimenLogin(aParent: PWindowsObject; aSpecSeq: TSeqNum);

IMPLEMENTATION

uses
  INILib,
  UICommon,
  UIPat,
  UIGSRes,
  UISpec,
  UIIso,
  APITools,
  CtlLib,
  UserMsgs,
  DBFile,
  DBIDs,
  DMSErr,
  DMString,
  DlgLib,
  ListLib,
  MScan,
  ODialogs,
  Objects,
  Strings,
  UIFlds,
  Win31,
  WinProcs,
  WinTypes;

{$R UISPCLOG.RES}
{$I UISPCLOG.INC}

type
  PUISpecLog = ^TUISpecLog;
  TUISpecLog = object(TUICommonDlg)
    dbSpec    : PDBFile;  {- the record in the dbSpec is always pointing to the
                             currently loaded record. If any }
    dbPat     : PDBFile;
    origSpec  : PDBRec;   {- original record of a rec being edited }
    origPat   : PDBRec;
    tempSpec  : PDBRec;
    tempPat   : PDBRec;
    patModeTxt: PStatic;
    patMode   : integer;
    ClearSpec : boolean;
    {}
    constructor Init(aParent: PWindowsObject; aSeq: TSeqNum);
    destructor Done; virtual;
    procedure SetupWindow; virtual;

    procedure WMLostFocus(var msg: TMessage);   virtual WM_FIRST + WM_LOSTFOCUS;
    procedure WMFldList(var msg: TMessage);     virtual WM_FIRST + WM_FLDLIST;
    procedure WMCtlColor(var msg: TMessage);    virtual WM_FIRST + WM_CTLCOLOR;
    procedure IDClear(var msg: TMessage);       virtual ID_FIRST + IDC_UICLEAR;

    {- field exit methods }
    procedure DoPatIDExit;
    procedure DoSpecCollExit(newFocID: word);

    {- misc stuff }
    procedure SetMode(aMode: integer); virtual;
    procedure SetPatMode(aMode: integer);
    procedure ClearFields; virtual;
    procedure EnableButtons; virtual;
    function FillScreen: boolean;
    procedure FillPatient;
    procedure ClearPatient;
    procedure RecallLastSpecimen; virtual;
    procedure SaveLastSpecimen;
    procedure AutoSaveNew(NextMode: Integer);

    {- overwritten methods}
    procedure SetModuleID; virtual;
    function DataChanged: boolean; virtual;
    function CheckSave(clearAfter: boolean): boolean; virtual;
    function SaveData(clearAfter: boolean): boolean; virtual;
    procedure NextPrev(doNext: boolean); virtual;
    procedure DeleteData; virtual;

    procedure IDPatient(var msg: TMessage); virtual ID_FIRST + IDC_PATIENT;
    procedure IDSpecimen(var msg: TMessage); virtual ID_FIRST + IDC_SPECIMEN;
    procedure IDGramStain(var msg: TMessage); virtual ID_FIRST + IDC_GS;
    procedure IDIsolate(var msg: TMessage); virtual ID_FIRST + IDC_ISOLATE;
    procedure IDNext(var msg: TMessage);   virtual ID_FIRST + IDC_UINEXT;
    private
    sFld      : PUIFld;   {- commonly used fields }
    cFld      : PUIFld;
    pFld      : PUIFld;
  end;

constructor TUISpecLog.Init(aParent: PWindowsObject; aSeq: TSeqNum);
var
  f   : PUIFld;
  k   : integer;
  fNum: integer;
  c   : PControl;
  fi  : TFldInfo;
  subst : PErrSubst;
begin
  inherited Init(aParent, MakeIntResource(DLG_UISPECLOG), aSeq);
  ClearSpec:=True;

  {- open all files being modified }
  dbSpec:= New(PDBFile, Init(DBSPECFile, '', dbOpenNormal));
  if dbSpec = nil then
  begin
    subst:= New(PErrSubst, Init(DBSpecFile));
    DoFail(IDS_ERROROPEN, dbLastOpenError, subst);   {- cant open specimen file }
    MSDisposeObj(subst);
    Fail;
  end;

  {- get spec file in proper sort order }
  if INISpecimenListSort = 2 then
    dbSpec^.dbc^.SetCurKeyNum(DBSPEC_DATE_KEY)
  else
    dbSpec^.dbc^.SetCurKeyNum(DBSPEC_ID_KEY);
  dbSpec^.dbc^.GetFirst(dbSpec^.dbr);

  dbPat:= New(PDBFile, Init(DBPatFile, '', dbOpenNormal));
  if dbPat = nil then         {- cant open patient }
  begin
    subst:= New(PErrSubst, Init(DBPatFile));
    DoFail(IDS_ERROROPEN, dbLastOpenError, subst);
    MSDisposeObj(subst);
    Fail;
  end;

  origPat:= New(PDBRec, Init(dbPat^.dbc));
  if origPat = nil then
  begin
    DoFail(IDS_CANTALLOCREC, 0, nil);
    fail;
  end;

  origSpec:= New(PDBRec, Init(dbSpec^.dbc));
  if origSpec = nil then {- cant allocate record buffer }
  begin
    DoFail(IDS_CANTALLOCREC, 0, nil);
    fail;
  end;

  tempSpec:= New(PDBRec, Init(dbSpec^.dbc));
  if tempSpec = nil then
  begin
    DoFail(IDS_CANTALLOCREC, 0, nil);
    fail;
  end;

  tempPat:= New(PDBRec, Init(dbPat^.dbc));
  if tempPat = nil then
  begin
    DoFail(IDS_CANTALLOCREC, 0, nil);
    fail;
  end;

  InitModeText(IDC_MODESPEC);
  patModeTxt:= New(PStatic, InitResource(@self, IDC_MODEPAT, 0));
  c:= New(PLStatic, InitResource(@self, IDC_SPECFREE, 0, false));

  sFld:= LinkUIFld(DBSpecSpecID,      true,  false, 0, IDC_EDSPECID,   IDC_LBLSPECID,   dbSpec^.dbd);
  cFld:= LinkUIFld(DBSpecCollectDate, true,  false, 0, IDC_EDCOLLECT,  IDC_LBLCOLLECT,  dbSpec^.dbd);
         LinkUIFld(DBSpecSrc,         false, false, 0, IDC_EDSOURCE,   IDC_LBLSOURCE,   dbSpec^.dbd);
         LinkUIFld(DBSpecWard,        false, false, 0, IDC_EDWARD,     IDC_LBLWARD,     dbSpec^.dbd);
         LinkUIFld(DBSpecFreeText,    false, false, 0, IDC_EDFREETEXT, 0,               dbSpec^.dbd);
  pFld:= LinkUIFld(DBPatPatID,        false, false, 0, IDC_EDPATID,    IDC_LBLPATID,    dbPat^.dbd);
         LinkUIFld(DBPatLName,        false, false, 0, IDC_EDLAST,     IDC_LBLLAST,     dbPat^.dbd);
         LinkUIFld(DBPatFName,        false, false, 0, IDC_EDFIRST,    IDC_LBLFIRST,    dbPat^.dbd);

  {- now link all user defined fields }
  k:= IDC_EDSPECUSER1;
  for fNum:= DBSpecUDBase to DBSpecUDBase + 3 do
  begin
    dbSpec^.dbd^.FieldInfo(fNum, fi);
    if (dbSpec^.dbd^.dbErrorNum = 0) and ((fi.userFlag and DB_UD) <> 0) then
    begin
      LinkUIFld(fNum, false, false, 0, k, k+100, dbSpec^.dbd);
      Inc(k);
    end;
  end;

  k:= IDC_EDPATUSER1;
  for fNum:= DBPatUDBase to DBPatUDBase + 3 do
  begin
    dbPat^.dbd^.FieldInfo(fNum, fi);
    if (dbPat^.dbd^.dbErrorNum = 0) and ((fi.userFlag and DB_UD) <> 0) then
    begin
      LinkUIFld(fNum, false, false, 0, k, k+100, dbPat^.dbd);
      Inc(k);
    end;
  end;
end;

destructor TUISpecLog.Done;
begin
  inherited Done;
  MSDisposeObj(origSpec);
  MSDisposeObj(origPat);
  MSDisposeObj(tempSpec);
  MSDisposeObj(tempPat);
  MSDisposeObj(dbSpec);
  MSDisposeObj(dbPat);
end;

procedure TUISpecLog.SetupWindow;
begin
  inherited SetupWindow;

  SetPatMode(MODE_NONE);

  if preLoad <> 0 then
  begin
    {- need to load a specimen record }
    if dbSpec^.dbc^.GetSeq(tempSpec, preLoad) then
    begin
      dbSpec^.dbr^.CopyRecord(tempSpec);
      if FillScreen then
        SetMode(MODE_EDIT);
    end;
  end;
end;

procedure TUISpecLog.SetModuleID;
begin
  modID:= MOD_UISPECLOG;
end;

procedure TUISpecLog.SetMode(aMode: integer);
{- set the current mode. If a mode is negative, the mode will not be changed,
   but the mode text will be updated }
var
  pstr  : array[0..50] of char;
begin
  if aMode >= MODE_NONE then
    mode:= aMode;
  if modeTxt <> nil then
    case mode of
      MODE_NEW  : modeTxt^.SetText(SR(IDS_NEWSPEC, pstr, 50));
      MODE_EDIT : modeTxt^.SetText(SR(IDS_EDITSPEC, pstr, 50));
      else
      begin
        modeTxt^.SetText('');
      end;
    end;
  EnableButtons;
end;

procedure TUISpecLog.SetPatMode(aMode: integer);
var
  pstr  : array[0..100] of char;
begin
  if aMode >= MODE_NONE then
    patmode:= aMode;
  case patMode of
    MODE_NEW  : patModeTxt^.SetText(SR(IDS_NEWPAT, pstr, 50));
    MODE_EDIT : patModeTxt^.SetText(SR(IDS_EDITPAT, pstr, 50));
    else
      patModeTxt^.SetText(nil);
  end; {case}
end;

procedure TUISpecLog.ClearPatient;
{- clear patient ID/name information }

  procedure ClearIt(aFld: PUIFld); far;
  begin
    if aFld^.GetDBD = dbPat^.dbd then
      aFld^.Clear;
  end;

begin
  SetPatMode(MODE_NONE);
  flds^.ForEach(@ClearIt);
  dbPat^.dbr^.ClearRecord;
  origPat^.ClearRecord;
  EnableButtons;
end;

procedure TUISpecLog.RecallLastSpecimen;
var
  p1, p2    : array[0..50] of char;
begin
  if (mode = MODE_NONE) and (StrLen(lastSpec) > 0) and (StrLen(lastCD) > 0) then
  begin
    ClearFields;
    tempSpec^.PutFieldAsStr(DBSpecSpecID, lastSpec);
    tempSpec^.PutFieldAsStr(DBSpecCollectDate, lastCD);
    sFld^.XFerRecToCtl(tempSpec);
    cFld^.XFerRecToCtl(tempSpec);
    DoSpecCollExit(0);
    sFld^.FocusFld;
  end
  else
    MessageBeep(0);
end;

type
  PClearDlg = ^TClearDlg;
  TClearDlg = object(TCenterDlg)
    procedure IDAll(var msg: TMessage); virtual ID_FIRST + IDC_CLEARALL;
    procedure IDClearPat(var msg: TMessage); virtual ID_FIRST + IDC_CLEARPAT;
  end;

procedure TClearDlg.IDAll(var msg: TMessage);
begin
  EndDlg(IDC_CLEARALL);
end;

procedure TClearDlg.IDClearPat(var msg: TMessage);
begin
  EndDlg(IDC_CLEARPAT);
end;

procedure TUISpecLog.IDClear(var msg: TMessage);
var
  k   : integer;
begin
  if CheckSave(false) then
  begin
    k:= application^.ExecDialog(New(PClearDlg, Init(@self, MakeIntResource(DLG_CLEAR))));
    if k = IDC_CLEARPAT then
    begin
      ClearPatient;
      pFld^.FocusFld;
    end
    else if k = IDC_CLEARALL then
    begin
      ClearFields;
      sFld^.FocusFld;
    end;
  end;
end;

procedure TUISpecLog.ClearFields;
{- clear all fields on screen }
var
  listRec   : PMnemListRec;
begin
  inherited ClearFields;
  ClearPatient;
  sFld^.FocusFld;   {- focus specimen ID field }
  origSpec^.ClearRecord;
  dbSpec^.dbr^.ClearRecord;

  EnableButtons;

  {- set default "Preliminary" status }
  listRec:= New(PMnemListRec, Init);
  if (listRec <> nil) and listObj^.FindMnemID(@self, DBStatFile, '1', listRec, true) then
  begin
    dbSpec^.dbr^.PutField(DBSpecStat, @listRec^.seq);
  end;
  origSpec^.CopyRecord(dbSpec^.dbr);
end;

procedure TUISpecLog.EnableButtons;
begin
  EnableWindow(GetItemHandle(IDC_UIPREV),    (mode = MODE_EDIT) and (preLoad = 0));
  EnableWindow(GetItemHandle(IDC_UINEXT),     preLoad = 0);
  EnableWindow(GetItemHandle(IDC_UICLEAR),    preLoad = 0);
  EnableWindow(GetItemHandle(IDC_UIDELETE),  (mode = MODE_EDIT) and (preLoad = 0));

  EnableWindow(GetItemHandle(IDC_ISOLATE),    mode = MODE_EDIT);
  EnableWindow(GetItemHandle(IDC_UISAVE),     mode > MODE_NONE);


  EnableWindow(GetItemHandle(IDC_GS),        (mode = MODE_EDIT) or
                                             (not cfld^.IsEmpty));
  EnableWindow(GetItemHandle(IDC_PATIENT),   (origPat^.GetSeqValue > 0) or
                                             (not pfld^.IsEmpty));
  EnableWindow(GetItemHandle(IDC_SPECIMEN),  (origSpec^.GetSeqValue > 0) or
                                             (not cfld^.IsEmpty));
end;

function TUISpecLog.FillScreen: boolean;
{- called when dbSpec has been loaded. Fills all controls and loads any relevant
   patient data. Assumes the data to be filled is in dbSpec^.dbr. Also, sets the
   orig recs to the value of the loaded record }

  procedure GetData(aFld: PUIFld); far;
  begin
    if aFld^.GetDBD = dbSpec^.dbd then
      aFld^.XferRecToCtl(dbSpec^.dbr);
  end;

var
  pstr  : array[0..50] of char;
  seq   : TSeqNum;
begin
  if not origSpec^.CopyRecord(dbSpec^.dbr) then
  begin
    ShowError(@self, IDS_CANTCOPYREC, nil, origSpec^.dbErrorNum, modID, 0);
    FillScreen:= false;
  end
  else
  begin
    {- get the associated patient record }
    dbSpec^.dbr^.GetField(DBSpecPatRef, @seq, sizeof(seq));
    if dbPat^.dbc^.GetSeq(dbPat^.dbr, seq) then
      FillPatient
    else
      ClearPatient;

    flds^.ForEach(@GetData);
    FillScreen:= true;
    SaveLastSpecimen;
  end;
end;

procedure TUISpecLog.FillPatient;
{- from dbPat, fill the patient related portion of the screen. Assumes that
   patient data is in dbPat^.dbr }

  procedure GetData(aFld: PUIFld); far;
  begin
    if aFld^.GetDBD = dbPat^.dbd then
      aFld^.XFerRecToCtl(dbPat^.dbr);
  end;

begin
  SetPatMode(MODE_EDIT);
  origPat^.CopyRecord(dbPat^.dbr);
  flds^.ForEach(@GetData);
  EnableButtons;
end;

procedure TUISpecLog.IDNext(var msg: TMessage);
begin
  ClearSpec:=False;
  NextPrev(True);
  ClearSpec:=True;
end;

procedure TUISpecLog.NextPrev(doNext: boolean);
var
  fld   : PUIFld;
  isOK  : boolean;
begin
  if not CheckSave(ClearSpec) then exit;

  if (mode <> MODE_EDIT) or (origSpec^.GetSeqValue = 0) then
  begin
    if dbSpec^.dbc^.GetFirst(tempSpec) then
    begin
      dbSpec^.dbr^.CopyRecord(tempSpec);
      if FillScreen then
        SetMode(MODE_EDIT);
    end
    else
      ShowError(@self, IDS_NOSPECS, nil, dbSpec^.dbc^.dbErrorNum, modID, 0);
  end
  else
  begin
    tempSpec^.CopyRecord(origSpec);
    if doNext then
      isOK:= dbSpec^.dbc^.GetNext(tempSpec)
    else
      isOK:= dbSpec^.dbc^.GetPrev(tempSpec);

    if not isOK then
    begin
      if dbSpec^.dbc^.dbErrorNum = MOD_BTRV + 9 then
        ShowError(@self, IDS_DBNOMOREDATA, nil, dbSpec^.dbc^.dbErrorNum, modID, 0)
      else
        ShowError(@self, IDS_DBERROR, nil, dbSpec^.dbc^.dbErrorNum, modID, 1);
    end
    else
    begin
      dbSpec^.dbr^.CopyRecord(tempSpec);
      if FillScreen then
        SetMode(MODE_EDIT);
    end;
  end;

  sfld^.FocusFld;
end;

procedure TUISpecLog.IDSpecimen(var msg: TMessage);
var
  seq   : TSeqNum;
begin
  AutoSaveNew(Mode_Edit);

  seq:= origSpec^.GetSeqValue;
  if (seq > 0) then
  begin
    if CheckSave(false) then
    begin
      UISpecimenEntry(@self, seq);
      {- reload patient record }
      if dbSpec^.dbc^.GetSeq(dbSpec^.dbr, seq) then
        FillScreen
      else
      begin
        ShowError(@self, IDS_ERRLODSPEC, nil, dbSpec^.dbc^.dbErrorNum, modID, 0);
        ClearFields;
      end;
      sFld^.FocusFld;
    end;
  end;
end;

procedure TUISpecLog.IDPatient(var msg: TMessage);
var
  seq   : TSeqNum;
begin
  AutoSaveNew(Mode_None);

  seq:= origPat^.GetSeqValue;
  if seq > 0 then
  begin
    if CheckSave(false) then
    begin
      UIPatientEntry(@self, seq);
      {- reload patient record }
      if dbPat^.dbc^.GetSeq(dbPat^.dbr, seq) then
        FillPatient
      else
      begin
        ShowError(@self, IDS_ERRLODPAT, nil, dbPat^.dbc^.dbErrorNum, modID, 0);
        ClearPatient;
      end;
      pFld^.FocusFld;
    end;
  end;
end;

procedure TUISpecLog.IDGramStain(var msg: TMessage);
begin
  AutoSaveNew(Mode_Edit);

  if mode = MODE_EDIT then
    UIGSResults(@self, origSpec^.GetSeqValue);
end;

procedure TUISpecLog.IDIsolate(var msg: TMessage);
begin
  if mode = MODE_EDIT then
    UIIsoEntry(@self, origSpec^.GetSeqValue, 0);
end;

procedure TUISpecLog.DeleteData;
var
  p1, p2  : array[0..100] of char;
  err     : integer;
begin
  if mode = MODE_EDIT then
  begin
    if YesNoMsg(HWindow, SR(IDS_CONFIRM, p1, 100), SR(IDS_CONFIRMDELSPEC, p2, 100)) then
    begin
      err:= 0;
      WaitCursor(true);
      if dbSpec^.dbc^.GetSeq(dbSpec^.dbr, origSpec^.GetSeqValue) then
      begin
        if not dbSpec^.dbc^.DeleteRec(dbSpec^.dbr) then
          err:= dbSpec^.dbc^.dbErrorNum
        else
          ClearFields;
      end
      else
        err:= dbSpec^.dbc^.dbErrorNum;

      if err <> 0 then
        ShowError(@self, IDS_CANTDEL, nil, err, modID, 0);
      WaitCursor(false);
    end;
  end;
end;

function TUISpecLog.DataChanged: boolean;
{- compare the current data with the originally loaded data.  If any data has
   changed, then return true }

  function IsModified(aFld: PUIFld): boolean; far;
  begin
    if aFld^.GetDBD = dbSpec^.dbd then
      IsModified:= aFld^.IsModified(origSpec)
    else
      IsModified:= aFld^.IsModified(origPat);
  end;

begin
  DataChanged:= flds^.FirstThat(@IsModified) <> nil;
end;

procedure TUISpecLog.AutoSaveNew(NextMode: Integer);
{- Compulsory Save for New Data }
begin
  if Mode=Mode_New then
    If SaveData(False) then
    begin
      SetMode(NextMode);
      FillScreen;
    end;
end;

procedure TUISpecLog.SaveLastSpecimen;
{- save the just added/edited specimen to the lastSpec/CD fields }
var
  p1, p2  : array[0..50] of char;
begin
  dbSpec^.dbr^.GetFieldAsStr(DBSpecSpecID, p1, sizeof(p1)-1);
  dbSpec^.dbr^.GetFieldAsStr(DBSpecCollectDate, p2, sizeof(p2)-1);
  SetLastSpecimen(p1, p2);
end;

function TUISpecLog.SaveData(clearAfter: boolean): boolean;
var
  ret   : boolean;
  iso   : PDBFile;

  function ValidateScreen: boolean;
  {- check screen for validity }
  var
    sret    : boolean;
    fld     : PUIFld;
    errNum  : integer;

    function IsInvalid(aFld: PUIFld): boolean; far;
    begin
      errNum:= aFld^.Format;
      IsInvalid:= errNum <> 0;
    end;

  begin
    sret:= true;
    fld:= flds^.FirstThat(@IsInvalid);
    if (fld = nil) and (patMode <> MODE_NONE) and (pFld^.IsEmpty) then
    begin
      ShowUIFldError(@self, UIErrRequired, pFld);
      sRet:= false;
    end
    else if (fld <> nil) then
    begin {- some other error }
      ShowUIFldError(@self, errNum, fld);
      sRet:= false;
    end;
    ValidateScreen:= sret;
  end;

  function StartTransaction: boolean;
  var
    errNum  : integer;
  begin
    errNum:= DBBeginTransaction;
    if errNum <> 0 then
    begin
      ShowError(@self, IDS_TRANSERROR, nil, errNum, modID, 0);
      StartTransaction:= false;
    end
    else
      StartTransaction:= true;
  end;

  function LockRecord: boolean;
  {- lock pat/spec records and place current contents of records in tempPat and tempSpec
     if the records are not found then the user is prompted to recreate in which case
     the appropriate mode (mode, patMode) is modified accordingly }
  var
    sret  : boolean;
  begin
    sret:= true;
    {- no need to lock if adding a new specimen }
    if mode = MODE_EDIT then
    begin
      tempSpec^.CopyRecord(origSpec);
      sret:= LockIt(dbSpec, tempSpec, IDS_SPECLOCKED, IDS_CANTLOCATEORIGSPEC, mode);
    end;

    {- no need to lock if adding a new patient }
    if sRet and (patMode = MODE_EDIT) then
    begin
      tempPat^.CopyRecord(origPat);
      sret:= LockIt(dbPat, tempPat, IDS_PATLOCKED, IDS_CANTLOCATEORIGPAT, patMode);
    end;

    LockRecord:= sRet;
  end;

  function CheckModified: boolean;
  {- at this point the original record is locked. Now see if someone else has
      modified it since we originally loaded it. if so prompt for overwrite. Assumes
      the current contents of the record are placed in tempSpec/tempPat }
  var
    sret    : boolean;
    p1, p2  : array[0..200] of char;
  begin
    sret:= true;
    if mode = MODE_EDIT then    {- only valid if editing a specimen }
    begin
      if not origSpec^.CompareRecord(tempSpec) then
        sret:= YesNoMsg(HWindow, SR(IDS_WARNING, p1, 200), SR(IDS_SPECMODIFIED, p2, 200));
    end;

    if sret and (patMode = MODE_EDIT) then
      if not origPat^.CompareRecord(tempPat) then
        sret:= YesNoMsg(HWindow, SR(IDS_WARNING, p1, 200), SR(IDS_PATMODIFIED, p2, 200));
    CheckModified:= sRet;
  end;

  function SaveIt: boolean;
  var
    sRet  : boolean;
    eNum  : integer;
    seq   : TSeqNum;

    procedure XFerToRec(aFld: PUIFld); far;
    begin
      if aFld^.GetDBD = dbSpec^.dbd then  {- skip over patient id field }
        aFld^.XFerCtlToRec(dbSpec^.dbr)
      else
        aFld^.XFerCtlToRec(dbPat^.dbr);
    end;

  begin
    sRet:= true;

    DoPatIDExit; {- make sure patient ID is loaded if its been changed }

    {- now transfer all fields to the record buffer }
    flds^.ForEach(@XFerToRec);  {- get specimen fields }

    if patMode <> MODE_NONE then
    begin
      if patMode = MODE_EDIT then
        dbPat^.dbc^.UpdateRec(dbPat^.dbr)
      else
        dbPat^.dbc^.InsertRec(dbPat^.dbr);

      eNum:= 0;
      case dbPat^.dbc^.dbErrorNum of
        MOD_BTRV + 5: eNum:= IDS_DUPPAT;
        else
          eNum:= IDS_PATSAVERROR;
      end;
      if dbPat^.dbc^.dbErrorNum <> 0 then
      begin
        ShowError(@self, eNum, nil, dbPat^.dbc^.dbErrorNum, modID, 0);
        sret:= false;
      end;
    end;

    if sRet then  {- now for specimen }
    begin
      seq:= dbPat^.dbr^.GetSeqValue;  {- get patient seq for specimen reference }
      dbSpec^.dbr^.PutField(DBSpecPatRef, @seq);

      if mode = MODE_NEW then
        dbSpec^.dbc^.InsertRec(dbSpec^.dbr)
      else
        dbSpec^.dbc^.UpdateRec(dbSpec^.dbr);
      eNum:= dbSpec^.dbc^.dbErrorNum;

      if eNum <> 0 then
      begin
        {- special error handling for common errors }
        case eNum of
          MOD_BTRV + 5: eNum:= IDS_DUPSPEC;
          else
            eNum:= IDS_CANNOTSAVE;
        end;
        ShowError(@self, eNum, nil, dbSpec^.dbc^.dbErrorNum, modID, 0);
        sFld^.FocusFld;
        sret:= false;
      end
      else if mode = MODE_EDIT then
      begin   {- update any isolate records that may be affected }
        eNum:= UpdateSpecIsos(iso, nil, dbSpec^.dbr);
        if eNum <> 0 then
        begin
          ShowError(@self, IDS_ERRUPDISOS, nil, eNum, modID, 0);
          sRet:= false;
        end;
      end;
    end;
    SaveIt:= sret;
  end;


  procedure UnlockRecords;
  begin
    if mode = MODE_EDIT then
      dbSpec^.dbc^.UnlockRec(tempSpec);
    if patMode = MODE_EDIT then
      dbPat^.dbc^.UnlockRec(tempPat);
  end;

var
  subst   : PErrSubst;
begin
  WaitCursor(true);
  ret:= true;
  SetLastSpecimen(nil, nil);  {- clear last specimen }

  iso:= nil;
  if mode = MODE_EDIT then
  begin
    iso:= New(PDBFile, Init(DBIsoFile, '', dbOpenNormal));
    if iso = nil then
    begin
      subst:= New(PErrSubst, Init(DBIsoFile));
      ShowError(@self, IDS_ERROROPEN, subst, dbLastOpenError, modID, 0);
      MSDisposeObj(subst);
      ret:= false;
    end;
  end;
  if ret then ret:= ValidateScreen;
  if ret then ret:= StartTransaction;
  if ret then ret:= LockRecord;
  if ret then ret:= CheckModified;
  if ret then ret:= SaveIt;
  UnlockRecords;
  if ret then
    DBEndTransaction
  else
    DBAbortTransaction;

  if ret then
    SaveLastSpecimen;

  {- and finally, clear fields }
  if ret and clearAfter then
    ClearFields;

  MSDisposeObj(iso);

  SaveData:= ret;
  WaitCursor(false);
end;

function TUISpecLog.CheckSave(clearAfter: boolean): boolean;
var
  ret     : boolean;
  pstr    : array[0..200] of char;
  p2      : array[0..50] of char;
  k       : integer;
begin
  ret:= true;
  if mode <> MODE_NONE then
  begin
    {- first see if any data has changed }
    if DataChanged then
    begin
      k:= YesNoCancelMsg(HWindow, SR(IDS_CONFIRM, p2, 50), SR(IDS_CONFIRMSAVE, pstr, 200));
      if k = IDYES then
        ret:= SaveData(clearAfter)
      else
        ret:= k <> IDCANCEL;
    end;
  end;
  CheckSave:= ret;
end;

{---- field exit routines }

procedure TUISpecLog.DoSpecCollExit(newFocID: word);
var
  id  : word;
begin
  {- if a mode has been established then no need for this processing }
  if (mode = MODE_NONE) and (newFocID <> IDC_EDSPECID) and (newFocID <> IDC_EDCOLLECT) then
  begin
    WaitCursor(true);
    {- if one or the other fields are empty then no processing can occur. We need
      both fields to perform the load }
    if not (sFld^.IsEmpty or cFld^.IsEmpty) then
    begin
      {- attempt to find the spec/collect date, if not found then NEW mode
          assumed, otherwise EDIT mode is assumed }

      sFld^.XferCtlToRec(tempSpec);
      cFld^.XferCtlToRec(tempSpec);

      if listObj^.FindKey(@self, tempSpec, true) then
      begin
        {- relocate the specimen cursor to the proper record (so next/prev still work) }
        if not dbSpec^.dbc^.GotoRecord(tempSpec) then
        begin
          ShowError(@self, IDS_CANNOTPOS, nil, dbSpec^.dbc^.dbErrorNum, modID, 0);
          SetMode(MODE_NEW);
          origSpec^.ClearRecord;
        end
        else
        begin
          dbSpec^.dbr^.CopyRecord(tempSpec);
          if FillScreen then
            SetMode(MODE_EDIT);
        end;
      end
      else
      begin
        if listObj^.errorNum <> 0 then
          ShowError(@self, IDS_DBERROR, nil, listObj^.errorNum, modID, 2);
        SetMode(MODE_NEW);
        origSpec^.ClearRecord;
      end;
    end;
    WaitCursor(false);
  end;
end;

procedure TUISpecLog.DoPatIDExit;
{- called when patien id loses focus }
var
  pstr  : array[0..200] of char;
begin
  if patMode <> MODE_NONE then
    exit;
  if pFld^.IsEmpty then
    ClearPatient
  else
  begin
    if pFld^.IsModified(origPat) then
    begin
      pFld^.XFerCtlToRec(dbPat^.dbr);
      if listObj^.FindKey(@self, dbPat^.dbr, true) then
        FillPatient
      else
      begin
        if listObj^.errorNum <> 0 then
          ShowError(@self, IDS_DBERROR, nil, listObj^.errorNum, modID, 3);
        pFld^.GetCtlAsStr(pstr, 50);
        ClearPatient;
        dbPat^.dbr^.PutField(DBPatPatID, @pstr);
        pFld^.XFerRecToCtl(dbPat^.dbr);
        SetPatMode(MODE_NEW);
      end;
    end;
  end;
end;

procedure TUISpecLog.WMCtlColor(var msg: TMessage);
begin
  if (msg.lParamLo = patModeTxt^.HWindow) then
  begin
    SetTextColor(msg.wParam, RGB($FF,0,0));
    msg.result:= GetStockObject(WHITE_BRUSH);
    SetWindowLong(HWindow, DWL_MSGRESULT, msg.result);  {- set message result (for dialogs) }
  end
  else
    inherited WMCtlColor(msg);
end;

procedure TUISpecLog.WMFldList(var msg: TMessage);
{- called when user selects LIST in a field }
var
  id    : word;
  pstr  : array[0..100] of char;
  p2    : array[0..100] of char;
  ret   : longint;    {- return value to the UIFld that created this message }
  cont  : boolean;
begin
  ret:= -1;   {- tell UI field passing this message NOT to list (non-zero) }

  uiFldsProcessFocus:= false;   {- turn off field focus processing }

  id:= msg.wParam;
  if (id = IDC_EDSPECID) or (id = IDC_EDCOLLECT) then
  begin
    cont:= true;
    {- first see if any data has been entered. If so, prompt to save so the user
       wont lose data if they select from a list }
    if mode <> MODE_NONE then
      cont:= CheckSave(false);
    if cont then
    begin
      sFld^.XferCtlToRec(tempSpec);
      cFld^.XferCtlToRec(tempSpec);
      if listObj^.SelectList(@self, tempSpec) then
      begin
        dbSpec^.dbc^.GotoRecord(tempSpec); {- relocate for next/prev }
        dbSpec^.dbr^.CopyRecord(tempSpec);
        if FillScreen then
          SetMode(MODE_EDIT);
      end
      else if listObj^.errorNum <> 0 then
      begin
        if listObj^.errorNum = LISTERR_PARTIALKEYNOTFOUND then
          ShowError(@self, IDS_CANNOTLISTNOMATCH, nil, listObj^.errorNum, modID, 0)
        else
          ShowError(@self, IDS_CANNOTLISTBAD, nil, listObj^.errorNum, modID, 0);
      end;
    end;
  end
  else if id = IDC_EDPATID then
  begin
    pFld^.XFerCtlToRec(dbPat^.dbr);
    if listObj^.SelectList(@self, dbPat^.dbr) then
      FillPatient
    else if listObj^.errorNum <> 0 then
    begin
      if listObj^.errorNum = LISTERR_PARTIALKEYNOTFOUND then
        ShowError(@self, IDS_CANNOTLISTNOMATCH, nil, listObj^.errorNum, modID, 0)
      else
        ShowError(@self, IDS_CANNOTLISTBAD, nil, listObj^.errorNum, modID, 0);
    end;
  end
  else
    ret:= 0;    {- this routine is not handling the list. Let the UIfld handle it }

  uiFldsProcessFocus:= true;  {- turn focus processing back on }

  SetWindowLong(HWindow, DWL_MSGRESULT, ret);  {- set message result (for dialogs) }
end;

procedure TUISpecLog.WMLostFocus(var msg: TMessage);
var
  fld   : PUIFld;
  err   : integer;
begin
  fld:= PUIFld(msg.lParam); {- get field pointer to fld losing focus }
  {- first see if the field is valid }
  err:= fld^.IsValid;
  if (err <> 0) and (err <> UIErrRequired) then   {- ignore required fields on field exit }
    ShowUIFldError(@self, err, fld)
  else
  begin
    {- full key possibly entered, check for auto loading of data }
    case fld^.GetID of
      IDC_EDSPECID, IDC_EDCOLLECT:
        DoSpecCollExit(msg.wParam);
      IDC_EDPATID:
        DoPatIDExit;
    end;
    EnableButtons;
  end;
  SetWindowLong(HWindow, DWL_MSGRESULT, 0);  {- set message result (for dialogs) }
end;

{------[ Interfaced Procedures ]--}

procedure UISpecimenLogin(aParent: PWindowsObject; aSpecSeq: TSeqNum);
var
  d   : PDialog;
begin
  d:= New(PUISpecLog, Init(aParent, aSpecSeq));
  application^.ExecDialog(d);
end;


END.

{- rcf}
