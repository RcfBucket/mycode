{----------------------------------------------------------------------------}
{  Module Name  : DBLIB.PAS                                                  }
{  Programmer   : EJ                                                         }
{  Date Created : 10/18/94                                                   }
{                                                                            }
{  Purpose -                                                                 }
{  This module provides the objects required to Create and Manipulate        }
{  a DB definition file and a BTRIEVE database.                              }
{                                                                            }
{  Assumptions -                                                             }
{  None.                                                                     }
{                                                                            }
{  Initialization -                                                          }
{  Substitutes DBExit as the exitProc.                                       }
{                                                                            }
{  Revision History -                                                        }
{                                                                            }
{  Version  Date      Who    Did What                                        }
{  -------  --------  ------ ----------------------------------------------- }
{  1.00     12/01/94  EJ     Initial release.                                }
{                                                                            }
{----------------------------------------------------------------------------}

unit DBLib;

INTERFACE

{.$DEFINE DBLibDEBUG}    {- remove before release }

uses
  Objects,
  DBTypes;

type
  {- Base database object }
  TDBObject = object(TObject)
    dbErrorNum  : integer;     {- last error number : Read/Only! }
    {}
    {- methods }
    constructor Init;
  end;

  {- Base database information object }
  PDBReadDesc = ^TDBReadDesc;
  TDBReadDesc = object(TDBObject)
    constructor Init(aFileName, aPath: PChar);
    procedure FileInfo(var afile: TFileInfo);
    function  FileName(pStr: PChar; maxLen: integer): PChar;
    function  FilePath(pStr: PChar; maxLen: integer): PChar;
    function  FileDesc(pStr: PChar; maxLen: integer): PChar;
    function  FieldInfo(fieldNum: integer; var afield: TFldInfo): boolean;
    function  FieldName(fieldNum: integer; pStr: PChar; maxLen: integer): PChar;
    function  KeyInfo(keyNum: integer; var akey: TKeyInfo): boolean;
    function  AssocInfo(assocNum: integer;
                 var aassoc: TAssocInfo): boolean;
    function  DelAssocInfo(delAssocNum: integer;
                 var adel: TDelAssocInfo): boolean;
    function  NumFields: integer;
    function  NumKeys: integer;
    function  NumAssocs: integer;
    function  NumDelAssocs: integer;
    function  RecordSize: integer; virtual;
    function  IsAutoSeq: boolean;
    function  CompareRecStruct(aDBDesc: PDBReadDesc): boolean; virtual;
    function  DataType: integer;
    function  KeyLength(keyNum: integer): integer;
    function  FieldNumber(fldName: PChar): integer;
    function  FieldSizeForStr(fieldNum: integer): word;
    {}
    private
    mFilePath     : array[0..maxPathLen] of char; {- path for file }
    mFileInfo     : TFileInfo;  {- file information }
    mDataType     : integer;    {- data type number of the file }
    mBase         : integer;    {- base number of the file }
    mFields       : integer;    {- number of fields defined in file }
    mKeys         : integer;    {- number of keys defined for file }
    mAssocs       : integer;    {- number of assocs defined in file }
    mDelAssocs    : integer;    {- number of del assocs def'd in file }
    mFieldList    : TFldList;   {- field list }
    mKeyList      : TKeyList;   {- key list }
    mAssocList    : TAssocList; {- assoc list }
    mDelAssocList : TDelAssocList;  {- delassoc list }
    {}
    procedure PInit(aFileName, aPath: PChar);
    function  PKeyLength(keyNum: integer): integer;
    function  KeyLengthAsStr(keyNum: integer): word;
    function  GetMaxKeyLengthAsStr: word;
    function  LoadDef(aFileName: PChar): boolean;
    function  CheckFieldNum(var fieldNum: integer): boolean;
    function  CheckKeyNum(var keyNum: integer): boolean;
    function  CheckAssocNum(var assocNum: integer): boolean;
    function  CheckDelAssocNum(var delAssocNum: integer): boolean;
  end;

  {- Read/Write database information object }
  PDBReadWriteDesc = ^TDBReadWriteDesc;
  TDBReadWriteDesc = object(TDBReadDesc)
    constructor Init(aFileName, aPath: PChar);
    function  SetFileDesc(aDesc: PChar): boolean;
    function  SetFileFlag(flag: word): boolean;
    function  SetAutoSeq(seqFlag: boolean): boolean;
    function  SetDataType(aType: integer): boolean;
    function  AddField(afield: TFldInfo): integer;
    function  DeleteField(fieldNum: integer): boolean;
    function  AddKey(akey: TKeyInfo): integer;
    function  AddKeySeg(keyNum: integer; akeySeg: TSegmentInfo): boolean;
    function  AddAssoc(var aassoc: TAssocInfo): integer;
    function  AddDelAssoc(var adel: TDelAssocInfo): integer;
    function  StoreDef: boolean;
    function  CopyDesc(src: PDBReadDesc): boolean;
    function  RenameDatabase(newName: PChar): boolean;
    {}
    private
    function  DeleteFldAssoc(fieldNum: integer): boolean;
    function  IsKeyField(fieldNum: integer): boolean;
    function  ValidToCreate: boolean;
  end;

  {- object used to create database files }
  PDBCreator = ^TDBCreator;
  TDBCreator = object(TDBObject)
    function  CreateTable(pDesc: PDBReadWriteDesc): boolean;
  end;

  {- Base database cursor object }
  PDBCursor = ^TDBCursor;

  {- Base database record object }
  PDBRec = ^TDBRec;
  TDBRec = object(TDBObject)
    constructor Init(pCursor: PDBCursor);
    destructor  Done; virtual;
    function  ClearField(fieldNum: integer): boolean;
    function  ClearRecord: boolean;
    function  ClearData: boolean;
    function  FieldIsEmpty(fieldNum: integer): boolean;
    function  GetField(fieldNum: integer; aVal: pointer; maxSize: integer): boolean;
    function  GetFieldAsStr(fieldNum: integer; aStr: PChar; maxLen: integer): PChar;
    function  PutField(fieldNum: integer; aVal: pointer): boolean;
    function  PutFieldAsStr(fieldNum: integer; aStr: PChar): boolean;
    function  CopyRecord(pRec: PDBRec): boolean;
    function  CopyData(pRec: PDBRec): boolean;
    function  CompareRecord(pRec: PDBRec): boolean;
    function  CompareData(pRec: PDBRec): boolean;
    function  GetSeqValue: TSeqNum;
    function  IsReferenced: boolean;     (* remove later *)
    function  OkayToMessWith: boolean;
    function  GetFlag(flagPos: byte): boolean;
    procedure SetFlag(flagPos: byte; aVal: boolean);
    function  Desc: PDBReadDesc;
    {}
    private
    mDesc         : PDBReadDesc;
    mDataType     : integer;    {- data type number of the file }
    mCurPosition  : longint;    {-           default=0 }
    mCurSeq       : ^TSeqNum;   {- will be nil if auto seq = false }
    mFlags        : ^byte;      {- will be nil if there is no flags field }
    mBufSize      : integer;
    mBuffer       : PDataArray;
    mTBuf         : PDataArray;
  end;

  {- Base database cursor object }
  TDBCursor = object(TDBObject)
    constructor Init(pDesc: PDBReadDesc; openMode: integer);
    destructor  Done; virtual;
    function  GetCurKeyNum: integer;
    function  SetCurKeyNum(keyNum: integer): boolean; virtual;
    function  SetSeqKeyNum: boolean; virtual;
    function  GetPosition: longint;
    function  GotoPosition(pRec: PDBRec; pos: longint): boolean; virtual;
    function  GotoRecord(pRec: PDBRec): boolean;
    function  DeleteRec(pRec: PDBRec): boolean;
    function  GetLock(pRec: PDBRec): boolean;
    function  UnlockRec(pRec: PDBRec): boolean;
    function  GetSeq(pRec: PDBRec; seqNum: TSeqNum): boolean; virtual;
    function  GetEQ(pRec: PDBRec): boolean; virtual;
    function  GetGE(pRec: PDBRec): boolean; virtual;
    function  GetFirst(pRec: PDBRec): boolean; virtual;
    function  GetLast(pRec: PDBRec): boolean; virtual;
    function  GetNext(pRec: PDBRec): boolean; virtual;
    function  GetPrev(pRec: PDBRec): boolean; virtual;
    function  GetFirstContains(pRec: PDBRec): boolean; virtual;
    function  GetNextContains(pRec: PDBRec): boolean; virtual;
    function  GetPrevContains(pRec: PDBRec): boolean; virtual;
    function  InsertRec(pRec: PDBRec): boolean; virtual;
    function  UpdateRec(pRec: PDBRec): boolean; virtual;
    function  NumRecords: longint; virtual;
    function  FileVersion(vstr: PChar; maxLen: integer): PChar;
    function  Desc: PDBReadDesc;
(*
    {- record buffer manipulation routines }
    procedure XFerToUser(Var userRec; AMaxSize: Word); virtual;
    procedure XFerFromUser(Var userRec); virtual;
    {- BTRIEVE operations }
    {- step function  - these functions walk thru the database in physical
                        order. Logical currency is lost after calling one
                        of these functions.}
    procedure StepFirst; virtual;
    procedure StepLast; virtual;
    procedure StepNext; virtual;
    procedure StepPrev; virtual;
*)
    {}
    private
    mDesc         : PDBReadDesc;
    mDataType     : integer;    {- data type number of the file }
    mKeyBuf       : TKeyBuf;    {- key buffer for BTRV calls }
    mKeyStr       : PChar;      {  key str used for "contains" searches }
    mKeyStrSize   : word;       {  size of mKeyStr }
    mPosBlk       : TPosBlk;    {- position block for BTRV calls }
    mCurKey       : integer;    {- current key for BTRV calls, default=1 }
    mBufSize      : integer;
    {}
    function  CheckRec(pRec: PDBRec): boolean;
    procedure CheckGet(pRec: PDBRec);
    procedure BuildKey(pRec: PDBRec);
    procedure BuildKeyAsStr(pRec: PDBRec; aKeyStr: PChar; aKeyStrSize: word);
    function  PDelete(pRec: PDBRec): boolean;
    function  PDeleteRec(pRec: PDBRec): boolean;
  end;

  {- Database association cursor object }
  PDBAssocCursor = ^TDBAssocCursor;
  TDBAssocCursor = object(TDBCursor)
    constructor Init(fromRec: PDBRec; assocNum: integer;
                     var extValList: TAssocParamList; openMode: integer);
    destructor  Done; virtual;
    function  SetCurKeyNum(keyNum: integer): boolean; virtual;
    function  SetSeqKeyNum: boolean; virtual;
    function  GotoPosition(pRec: PDBRec; pos: longint): boolean; virtual;
    function  GetSeq(pRec: PDBRec; seqNum: TSeqNum): boolean; virtual;
    function  GetEQ(pRec: PDBRec): boolean; virtual;
    function  GetGE(pRec: PDBRec): boolean; virtual;
    function  GetFirst(pRec: PDBRec): boolean; virtual;
    function  GetLast(pRec: PDBRec): boolean; virtual;
    function  GetNext(pRec: PDBRec): boolean; virtual;
    function  GetPrev(pRec: PDBRec): boolean; virtual;
    function  NumRecords: longint; virtual;
    {}
    private
    mFilterBuf    : TKeyBuf;    {- filter buffer for checking results }
    mFilterLen    : integer;
    mUsingSeqNum  : boolean;    {- position block for BTRV calls }
  end;

  {- Database copy cursor object }
  PDBCopyCursor = ^TDBCopyCursor;
  TDBCopyCursor = object(TDBCursor)
    function  InsertRec(pRec: PDBRec): boolean; virtual;
    function  UpdateRec(pRec: PDBRec): boolean; virtual;
  end;

  {- Database copy rec object }
  PDBCopyRec = ^TDBCopyRec;
  TDBCopyRec = object(TDBRec)
    function  CopyFields(pRec: PDBRec): boolean; virtual;
  end;

function  ValidFileName(aname: PChar): boolean;
function  DBDeleteDatabase(var aDBDesc: PDBReadWriteDesc): integer;
function  BTVersion(var vMajor, vMinor: word; vstr: PChar; maxLen: integer): PChar;
function  DBBeginTransaction: integer;
function  DBEndTransaction: integer;
function  DBAbortTransaction: integer;

var
  dbLastOpenError : integer;

IMPLEMENTATION

uses
  WinProcs,        (* remove before general release *)
  WinTypes,        (* remove before general release *)
  Strings,
  StrsW,
  btrapiw,
  btrconst,
  INILib,
  IntlLib,
  DateLib,
  BlockCvt,
  DMString,
  MScan,
  DBErrors,
  DBFile,
  DMSErr,
  DMSDebug;

{$L compblok}
function CompareBlock(Ptr1, Ptr2: pointer; CSize: word): boolean; external;

type
  PSeqNum = ^longint;

var
  inTransaction : boolean;
  delColl       : PCollection;

{$IFDEF DBLibDEBUG}
  openFiles     : PCollection;

procedure DebugFileOpened(dbc: PDBCursor);
begin
  openFiles^.Insert(dbc);
end;

procedure DebugFileClosed(aDbc: PDBCursor);
var
  d   : PDBCursor;

  function Matches(dbc: PDBCursor): boolean; far;
  begin
    Matches:= dbc = aDbc;
  end;

begin
  d:= openFiles^.FirstThat(@Matches);
  if d <> nil then
    openFiles^.AtDelete(openFiles^.IndexOf(d));
end;
{$ENDIF}

{--------------------------------[ TDBObject ]-------------------------------}
{----------------------------------------------------------------------------}
{  Initialize a database object.                                             }
{----------------------------------------------------------------------------}
constructor TDBObject.Init;
begin
  inherited Init;
  dbErrorNum := 0;
  dbLastOpenError := 0;
end;


{--------------------------------[ TDBReadDesc ]-----------------------------}
{----------------------------------------------------------------------------}
{  Initialize a database read description object.                            }
{                                                                            }
{  InParms : aFileName - a valid file name                                   }
{            aPath - an existing file path                                   }
{----------------------------------------------------------------------------}
constructor TDBReadDesc.Init(aFileName, aPath: PChar);
begin
  PInit(aFileName, aPath);

  if dbLastOpenError <> 0 then
    Fail
  else if dbErrorNum <> 0 then
  begin
    dbLastOpenError := dbErrorNum;
    Fail;
  end;
end;

{----------------------------------------------------------------------------}
{  Return file information.                                                  }
{                                                                            }
{  OutParms: afile - file information struct                                 }
{----------------------------------------------------------------------------}
procedure TDBReadDesc.FileInfo(var afile: TFileInfo);
begin
  dbErrorNum := 0;
  Move(mFileInfo, afile, SizeOf(mFileInfo));
end;

{----------------------------------------------------------------------------}
{  Return file name information.  The file name will be returned in          }
{  pStr and will be returned as a function result.                           }
{                                                                            }
{  Returns : a pointer to pStr.                                              }
{  InParms : maxLen is the max length of the string pStr.                    }
{  OutParms: pStr contains the file name.                                    }
{----------------------------------------------------------------------------}
function TDBReadDesc.FileName(pStr: PChar; maxLen: integer): PChar;
begin
  dbErrorNum := 0;
  StrLCopy(pStr, mFileInfo.fName, maxLen);
  FileName := pStr;
end;

{----------------------------------------------------------------------------}
{  Return file path information.  Path information will be returned in       }
{  pStr and will be returned as a function result.                           }
{                                                                            }
{  Returns : a pointer to pStr.                                              }
{  InParms : maxLen is the max length of the string pStr.                    }
{  OutParms: pStr contains the file path.                                    }
{----------------------------------------------------------------------------}
function TDBReadDesc.FilePath(pStr: PChar; maxLen: integer): PChar;
begin
  dbErrorNum := 0;
  StrLCopy(pStr, mFilePath, maxLen);
  FilePath := pStr;
end;

{----------------------------------------------------------------------------}
{  Return file description string.  Information will be returned in          }
{  pStr and will be returned as a function result.                           }
{                                                                            }
{  Returns : a pointer to pStr.                                              }
{  InParms : maxLen is the max length of the string pStr.                    }
{  OutParms: pStr contains the file description.                             }
{----------------------------------------------------------------------------}
function TDBReadDesc.FileDesc(pStr: PChar; maxLen: integer): PChar;
begin
  dbErrorNum := 0;
  StrLCopy(pStr, mFileInfo.desc, maxLen);
  FileDesc := pStr;
end;

{----------------------------------------------------------------------------}
{  Return information for a specified field.  All field information          }
{  will be returned in afield, if the field number is valid.                 }
{                                                                            }
{  Returns : TRUE if function successful.                                    }
{  InParms : fieldNum is the field number for info request.                  }
{  OutParms: afield contains all field information.                          }
{----------------------------------------------------------------------------}
function TDBReadDesc.FieldInfo(fieldNum: integer; var afield: TFldInfo): boolean;
begin
  dbErrorNum := 0;
  if CheckFieldNum(fieldNum) then
  begin
    afield := mFieldList[fieldNum];
  end;
  FieldInfo := dbErrorNum = 0;
end;

{----------------------------------------------------------------------------}
{  Return name of a specified field.                                         }
{                                                                            }
{  Returns : TRUE if function successful.                                    }
{  InParms : fieldNum is the field number for info request.                  }
{            maxLen is the max length of the string pStr.                    }
{  OutParms: pStr contains the field name.                                   }
{----------------------------------------------------------------------------}
function TDBReadDesc.FieldName(fieldNum: integer; pStr: PChar; maxLen: integer): PChar;
begin
  dbErrorNum := 0;
  if CheckFieldNum(fieldNum) then
    StrLCopy(pStr, mFieldList[fieldNum].fldName, maxLen)
  else
    pStr^ := #0;
  FieldName := pStr;
end;

{----------------------------------------------------------------------------}
{  Return information for a specified key.  All key information will         }
{  be returned in akey, if the key number is valid.                          }
{                                                                            }
{  Returns : TRUE if function successful.                                    }
{  InParms : keyNum is the key number for info request.                      }
{  OutParms: akey contains all key information.                              }
{----------------------------------------------------------------------------}
function TDBReadDesc.KeyInfo(keyNum: integer; var akey: TKeyInfo): boolean;
begin
  dbErrorNum := 0;
  if CheckKeyNum(keyNum) then
  begin
    akey := mKeyList[keyNum];
  end;
  KeyInfo := dbErrorNum = 0;
end;

{----------------------------------------------------------------------------}
{  Return information for a specified association.  All association          }
{  information will be returned in aassoc, if the assoc number is valid.     }
{                                                                            }
{  Returns : TRUE if function successful.                                    }
{  InParms : assocNum is the association number for info request.            }
{  OutParms: aassoc contains all association information.                    }
{----------------------------------------------------------------------------}
function TDBReadDesc.AssocInfo(assocNum: integer;
            var aassoc: TAssocInfo): boolean;
begin
  dbErrorNum := 0;
  if CheckAssocNum(assocNum) then
  begin
    aassoc := mAssocList[assocNum];
  end;
  AssocInfo := dbErrorNum = 0;
end;

{----------------------------------------------------------------------------}
{  Return information for a specified delAssoc. All delAssoc information     }
{  will be returned in adel, if the delAssoc number is valid.                }
{                                                                            }
{  Returns : TRUE if function successful.                                    }
{  InParms : delAssocNum is the delete association number for info request.  }
{  OutParms: adel contains all delete association information.               }
{----------------------------------------------------------------------------}
function TDBReadDesc.DelAssocInfo(delAssocNum: integer;
            var adel: TDelAssocInfo): boolean;
begin
  dbErrorNum := 0;
  if CheckDelAssocNum(delAssocNum) then
  begin
    adel := mDelAssocList[delAssocNum];
  end;
  DelAssocInfo := dbErrorNum = 0;
end;

{----------------------------------------------------------------------------}
{  Return number of fields defined for this object.                          }
{                                                                            }
{  Returns : the number of fields defined for this object.                   }
{----------------------------------------------------------------------------}
function TDBReadDesc.NumFields: integer;
begin
  dbErrorNum := 0;
  NumFields := mFields;
end;

{----------------------------------------------------------------------------}
{  Return number of keys defined for this object.                            }
{                                                                            }
{  Returns : the number of keys defined for this object.                     }
{----------------------------------------------------------------------------}
function TDBReadDesc.NumKeys: integer;
begin
  dbErrorNum := 0;
  NumKeys := mKeys;
end;

{----------------------------------------------------------------------------}
{  Return number of associations defined for this object.                    }
{                                                                            }
{  Returns : the number of associations defined for this object.             }
{----------------------------------------------------------------------------}
function TDBReadDesc.NumAssocs: integer;
begin
  dbErrorNum := 0;
  NumAssocs := mAssocs;
end;

{----------------------------------------------------------------------------}
{  Return number of delete associations defined for this object.             }
{                                                                            }
{  Returns : the number of associations defined for this object.             }
{----------------------------------------------------------------------------}
function TDBReadDesc.NumDelAssocs: integer;
begin
  dbErrorNum := 0;
  NumDelAssocs := mDelAssocs;
end;

{----------------------------------------------------------------------------}
{  Returns the record size for this object.                                  }
{                                                                            }
{  Returns : the record size for this object.                                }
{----------------------------------------------------------------------------}
function TDBReadDesc.RecordSize: integer;
var
  k  : integer;
  c  : integer;
begin
  dbErrorNum := 0;
  c := 0;
  for k := 1 to mFields do
    Inc(c, mFieldList[k].fldSize);

  RecordSize := c;
end;

{----------------------------------------------------------------------------}
{  Returns the auto sequence number flag value.                              }
{                                                                            }
{  Returns : TRUE if auto sequence numbers are being used.                   }
{----------------------------------------------------------------------------}
function TDBReadDesc.IsAutoSeq: boolean;
begin
  dbErrorNum := 0;
  IsAutoSeq := mFileInfo.autoSeq;
end;

{----------------------------------------------------------------------------}
{  Compare record structures of objects, return TRUE if equivelent.          }
{                                                                            }
{  Assumptions                                                               }
{  Equivelent here, means same number of fields, same record size and        }
{  related fields are of same name, size, type, and position in record.      }
{                                                                            }
{  Errors returned                                                           }
{    dbErr_IncompatibleRecStruct                                             }
{                                                                            }
{  Returns : TRUE if record structures are compatible.                       }
{  InParms : aDBDesc is the object to use to compare with.                   }
{----------------------------------------------------------------------------}
function TDBReadDesc.CompareRecStruct(aDBDesc: PDBReadDesc): boolean;
var
  k, j  : integer;
begin
  dbErrorNum := 0;
  if mFields <> aDBDesc^.mFields then
    dbErrorNum := dbErr_IncompatibleRecStruct
  else if mKeys <> aDBDesc^.mKeys then
    dbErrorNum := dbErr_IncompatibleRecStruct
  else
  begin
    k := 1;
    while (k <= mFields) and (dbErrorNum = 0) do
    begin
      if (StrIComp(mFieldList[k].fldName, aDBDesc^.mFieldList[k].fldName) <> 0) or
         (mFieldList[k].fldSize <> aDBDesc^.mFieldList[k].fldSize) or
         (mFieldList[k].fldType <> aDBDesc^.mFieldList[k].fldType) or
         ((((mFieldList[k].userFlag and db_Mnemonic) <> 0) or
           ((aDBDesc^.mFieldList[k].userFlag and db_Mnemonic) <> 0)) and
          (((mFieldList[k].userFlag and db_Mnemonic) <>
            (aDBDesc^.mFieldList[k].userFlag and db_Mnemonic)) or
           (mFieldList[k].fldAssoc = 0) or
           (aDBDesc^.mFieldList[k].fldAssoc = 0) or
           (StrLIComp(mAssocList[mFieldList[k].fldAssoc].fName,
             aDBDesc^.mAssocList[aDBDesc^.mFieldList[k].fldAssoc].fName, 9) <> 0))) or
         (mFieldList[k].fldPos <> aDBDesc^.mFieldList[k].fldPos) then {<- this check is kinda redundant!}
        dbErrorNum := dbErr_IncompatibleRecStruct;
      inc(k);
    end;
    k := 1;
    while (k <= mKeys) and (dbErrorNum = 0) do
    begin
      if (StrIComp(mKeyList[k].keyName, aDBDesc^.mKeyList[k].keyName) <> 0) or
         (mKeyList[k].unique <> aDBDesc^.mKeyList[k].unique) or
         (mKeyList[k].numSegs <> aDBDesc^.mKeyList[k].numSegs) then
        dbErrorNum := dbErr_IncompatibleRecStruct
      else
      begin
        j := 1;
        while (j <= mKeyList[k].numSegs) and (dbErrorNum = 0) do
        begin
          if mKeyList[k].segList[j].fldNum <>
             aDBDesc^.mKeyList[k].segList[j].fldNum then
          begin
            dbErrorNum := dbErr_IncompatibleRecStruct
          end;
          inc(j);
        end;
      end;
      inc(k);
    end;
  end;
  CompareRecStruct := dbErrorNum = 0;
end;

{----------------------------------------------------------------------------}
{  Returns the data type value for this object.                              }
{                                                                            }
{  Returns : the data type value.                                            }
{----------------------------------------------------------------------------}
function TDBReadDesc.DataType: integer;
begin
  dbErrorNum := 0;
  DataType := mDataType;
end;

{----------------------------------------------------------------------------}
{  Returns the combined length of the fields used to define the              }
{  specified key.                                                            }
{                                                                            }
{  Returns : the key length for the keyNum.                                  }
{  InParms : keyNum is the key number for info request.                      }
{----------------------------------------------------------------------------}
function TDBReadDesc.KeyLength(keyNum: integer): integer;
begin
  KeyLength := 0;
  if CheckKeyNum(keyNum) then
    KeyLength := PKeyLength(keyNum);
end;

{----------------------------------------------------------------------------}
{  Return the field number for the specified field name.                     }
{                                                                            }
{  Returns : the field number if fldName is found, else 0.                   }
{  InParms : fldName is the field name for info request.                     }
{----------------------------------------------------------------------------}
function TDBReadDesc.FieldNumber(fldName: PChar): integer;
var
  k     : integer;
begin
  dbErrorNum := 0;
  FieldNumber := 0;
  for k := 1 to mFields do
    if StrIComp(mFieldList[k].fldName, fldName) = 0 then
    begin
      FieldNumber := k + mBase;
      break;
    end;
end;

{----------------------------------------------------------------------------}
{  Return maximum length of the string representation of a field.            }
{  This function also returns 1 extra byte for the length byte or            }
{  terminating null.                                                         }
{                                                                            }
{  Errors returned                                                           }
{    dbErr_InvalidType                                                       }
{                                                                            }
{  Returns : maximum length of a string required to represent a field.       }
{  InParms : fieldNum is the field number for the info request.              }
{----------------------------------------------------------------------------}
function TDBReadDesc.FieldSizeForStr(fieldNum: integer): word;
var
  len   : integer;
begin
  dbErrorNum := 0;
  len := 0;
  if CheckFieldNum(fieldNum) then
  begin
    len := DBStrSizeForType(mFieldList[fieldNum].fldType,
                            mFieldList[fieldNum].fldSize);
    if len = 0 then
      dbErrorNum := dbErr_InvalidType;
  end;
  FieldSizeForStr := len;
end;

{----------------------------------------------------------------------------}
{  Initialize a database read description object.                            }
{                                                                            }
{  Assumptions                                                               }
{  If description file name is invalid then init will fail.                  }
{                                                                            }
{  Errors returned                                                           }
{    dbErr_InvalidFileName                                                   }
{                                                                            }
{  InParms : aFileName - a valid file name                                   }
{            aPath - an existing file path                                   }
{----------------------------------------------------------------------------}
procedure TDBReadDesc.PInit(aFileName, aPath: PChar);
var
  k    : integer;
begin
  inherited Init;

  StrLCopy(mFilePath, aPath, SizeOf(mFilePath)-1);
  if StrLen(mFilePath) > 0 then
    if mFilePath[StrLen(mFilePath)-1] <> '\' then
      StrLCat(mFilePath, '\', SizeOf(mFilePath)-1);
  FillChar(mFileInfo, SizeOf(mFileInfo), 0);
  StrCopy(mFileInfo.fName, aFileName);

{$I-}
  if IOResult <> 0 then {clear IOResult };
{$I+}

  if not ValidFileName(mFileInfo.fName) then
  begin
    dbLastOpenError := dbErr_InvalidFileName;
  end
  else
  begin
    LoadDef(mFileInfo.fName);
    if StrIComp(mFileInfo.fName, aFileName) <> 0 then
      dbLastOpenError := dbErr_FileNameMismatch;
  end;
end;

{----------------------------------------------------------------------------}
{  Returns the combined length of the fields used to define the              }
{  specified key.                                                            }
{                                                                            }
{  Returns : the key length for the keyNum.                                  }
{  InParms : keyNum is the key number for info request.                      }
{----------------------------------------------------------------------------}
function TDBReadDesc.PKeyLength(keyNum: integer): integer;
var
  k, c   : integer;
  segFld : integer;
begin
  dbErrorNum := 0;
  c := 0;
  for k := 1 to mKeyList[keyNum].numSegs do
  begin
    segFld := mKeyList[keyNum].segList[k].fldNum;
    Inc(c, mFieldList[segFld].fldSize);
  end;
  PKeyLength := c;
end;

{----------------------------------------------------------------------------}
{  Returns the combined length of the fields (in string form) used to        }
{  define the specified key.                                                 }
{                                                                            }
{  Returns : the key string length for the keyNum.                           }
{  InParms : keyNum is the key number for info request.                      }
{----------------------------------------------------------------------------}
function TDBReadDesc.KeyLengthAsStr(keyNum: integer): word;
var
  k      : integer;
  c      : word;
  segFld : integer;
begin
  dbErrorNum := 0;
  c := 0;
  for k := 1 to mKeyList[keyNum].numSegs do
  begin
    segFld := mKeyList[keyNum].segList[k].fldNum;
    Inc(c, FieldSizeForStr(segFld)-1);
  end;
  KeyLengthAsStr := c;
end;

{----------------------------------------------------------------------------}
{  Returns the string length of the key that is largest in string form.      }
{                                                                            }
{  Returns : the key string length for the largest key.                      }
{----------------------------------------------------------------------------}
function  TDBReadDesc.GetMaxKeyLengthAsStr: word;
var
  k      : integer;
  c      : word;
  retVal : word;
begin
  dbErrorNum:= 0;
  retVal:= 0;
  for k:= 1 to mKeys do
  begin
    c:= KeyLengthAsStr(k);
    if (c > retVal) then
      retVal:= c;
  end;
  GetMaxKeyLengthAsStr:= retVal;
end;

{----------------------------------------------------------------------------}
{  Load definition file, return TRUE if ok.                                  }
{                                                                            }
{  Assumptions                                                               }
{  The definition file has the following format -                            }
{    Header-Version                                                          }
{    <CR/LF>                                                                 }
{    FileDescription                                                         }
{    ^Z                                                                      }
{    FileInformation                                                         }
{    DataType                                                                }
{    Base                                                                    }
{    num Fields                                                              }
{    num Keys                                                                }
{    num Assocs                                                              }
{    num DelAssocs                                                           }
{    FieldList                                                               }
{    KeyList                                                                 }
{    AssocList                                                               }
{    DelAssocList                                                            }
{                                                                            }
{  Errors returned                                                           }
{    dbErr_CannotOpenDefFile                                                 }
{    dbErr_ReadingDefFile                                                    }
{    dbErr_InvalidDefFile                                                    }
{    dbErr_InvalidDefVersion                                                 }
{                                                                            }
{  Returns : TRUE if function successful.                                    }
{  InParms : aFileName - a valid file name                                   }
{----------------------------------------------------------------------------}
function TDBReadDesc.LoadDef(aFileName: PChar): boolean;
var
  fil   : file;
  tstr  : array[0..255] of char;
begin
  dbErrorNum := 0;
  StrCopy(tstr, mFilePath);
  StrCat(tstr, aFileName);
  StrCat(tstr, fileDescExt);

{$I-}
  if IOResult <> 0 then ;

  Assign(fil, tstr);
  Reset(fil, 1);

  repeat
    if IOResult <> 0 then
    begin
      dbErrorNum := dbErr_CannotOpenDefFile;
      break;
    end;

    {- check header }
    BlockRead(fil, tstr, Length(dbHeader));
    if IOResult <> 0 then
    begin
      dbErrorNum := dbErr_ReadingDefFile;
      break;
    end;

    tstr[Length(dbHeader)] := #0;
    if StrComp(dbHeader, tstr) <> 0 then
    begin
      dbErrorNum := dbErr_InvalidDefFile;
      break;
    end;

    BlockRead(fil, tstr, 1);  { read '-' }
    if IOResult <> 0 then
    begin
      dbErrorNum := dbErr_ReadingDefFile;
      break;
    end;

    BlockRead(fil, tstr, Length(dbVersion));
    if IOResult <> 0 then
    begin
      dbErrorNum := dbErr_ReadingDefFile;
      break;
    end;

    tstr[Length(dbVersion)] := #0;
    if StrComp(tstr, dbVersion) <> 0 then
    begin
      dbErrorNum := dbErr_InvalidDefVersion;
      break;
    end;

    {- read the rest of the file }
    BlockRead(fil, tstr, 2);  { read CR/LF }
    if IOResult <> 0 then
    begin
      dbErrorNum := dbErr_ReadingDefFile;
      break;
    end;

    BlockRead(fil, tstr, SizeOf(mFileInfo.desc));
    if IOResult <> 0 then
    begin
      dbErrorNum := dbErr_ReadingDefFile;
      break;
    end;

    BlockRead(fil, tstr, 1);  { read ^Z }
    if IOResult <> 0 then
    begin
      dbErrorNum := dbErr_ReadingDefFile;
      break;
    end;

    BlockRead(fil, mFileInfo, SizeOf(mFileInfo));
    if IOResult <> 0 then
    begin
      dbErrorNum := dbErr_ReadingDefFile;
      break;
    end;

    BlockRead(fil, mDataType, SizeOf(mDataType));
    if IOResult <> 0 then
    begin
      dbErrorNum := dbErr_ReadingDefFile;
      break;
    end;

    BlockRead(fil, mBase, SizeOf(mBase));
    if IOResult <> 0 then
    begin
      dbErrorNum := dbErr_ReadingDefFile;
      break;
    end;

    BlockRead(fil, mFields, SizeOf(mFields));
    if IOResult <> 0 then
    begin
      dbErrorNum := dbErr_ReadingDefFile;
      break;
    end;

    BlockRead(fil, mKeys, SizeOf(mKeys));
    if IOResult <> 0 then
    begin
      dbErrorNum := dbErr_ReadingDefFile;
      break;
    end;

    BlockRead(fil, mAssocs, SizeOf(mAssocs));
    if IOResult <> 0 then
    begin
      dbErrorNum := dbErr_ReadingDefFile;
      break;
    end;

    BlockRead(fil, mDelAssocs, SizeOf(mDelAssocs));
    if IOResult <> 0 then
    begin
      dbErrorNum := dbErr_ReadingDefFile;
      break;
    end;

    BlockRead(fil, mFieldList, SizeOf(mFieldList));
    if IOResult <> 0 then
    begin
      dbErrorNum := dbErr_ReadingDefFile;
      break;
    end;

    BlockRead(fil, mKeyList, SizeOf(mKeyList));
    if IOResult <> 0 then
    begin
      dbErrorNum := dbErr_ReadingDefFile;
      break;
    end;

    BlockRead(fil, mAssocList, SizeOf(mAssocList));
    if IOResult <> 0 then
    begin
      dbErrorNum := dbErr_ReadingDefFile;
      break;
    end;

    BlockRead(fil, mDelAssocList, SizeOf(mDelAssocList));
    if IOResult <> 0 then
    begin
      dbErrorNum := dbErr_ReadingDefFile;
      break;
    end;
  until TRUE;

  Close(fil);
  if IOResult <> 0 then {nothing};

  LoadDef := dbErrorNum = 0;
{$I+}
end;

{----------------------------------------------------------------------------}
{  Check field number, set dbErrorNumber accordingly.                        }
{                                                                            }
{  Errors returned                                                           }
{    dbErr_InvalidFieldNumber                                                }
{                                                                            }
{  Returns : TRUE if field number is valid.                                  }
{  InParms : fieldNum is the field number to validate.                       }
{----------------------------------------------------------------------------}
function TDBReadDesc.CheckFieldNum(var fieldNum: integer): boolean;
begin
  dbErrorNum := 0;
  if (fieldNum <= mBase) or (fieldNum > mFields + mBase) then
    dbErrorNum := dbErr_InvalidFieldNumber
  else if mBase <> 0 then
    fieldNum := fieldNum MOD mBase;
  CheckFieldNum := dbErrorNum = 0;
end;

{----------------------------------------------------------------------------}
{  Check key number, set dbErrorNumber accordingly.                          }
{                                                                            }
{  Errors returned                                                           }
{    dbErr_InvalidKeyNumber                                                  }
{                                                                            }
{  Returns : TRUE if key number is valid.                                    }
{  InParms : keyNum is the key number to validate.                           }
{----------------------------------------------------------------------------}
function TDBReadDesc.CheckKeyNum(var keyNum: integer): boolean;
begin
  dbErrorNum := 0;
  if (keyNum <= mBase) or (keyNum > mKeys + mBase) then
    dbErrorNum := dbErr_InvalidKeyNumber
  else if mBase <> 0 then
    keyNum := keyNum MOD mBase;
  CheckKeyNum := dbErrorNum = 0;
end;

{----------------------------------------------------------------------------}
{  Check assoc number, set dbErrorNumber accordingly.                        }
{                                                                            }
{  Errors returned                                                           }
{    dbErr_InvalidAssocNumber                                                }
{                                                                            }
{  Returns : TRUE if assoc number is valid.                                  }
{  InParms : assocNum is the assoc number to validate.                       }
{----------------------------------------------------------------------------}
function TDBReadDesc.CheckAssocNum(var assocNum: integer): boolean;
begin
  dbErrorNum := 0;
  if (assocNum <= mBase) or (assocNum > mAssocs + mBase) then
    dbErrorNum := dbErr_InvalidAssocNumber
  else if mBase <> 0 then
    assocNum := assocNum MOD mBase;
  CheckAssocNum := dbErrorNum = 0;
end;

{----------------------------------------------------------------------------}
{  Check delAssoc number, set dbErrorNumber accordingly.                     }
{                                                                            }
{  Errors returned                                                           }
{    dbErr_InvalidDelAssocNumber                                             }
{                                                                            }
{  Returns : TRUE if delAssoc number is valid.                               }
{  InParms : delAssocNum is the delAssoc number to validate.                 }
{----------------------------------------------------------------------------}
function TDBReadDesc.CheckDelAssocNum(var delAssocNum: integer): boolean;
begin
  dbErrorNum := 0;
  if (delAssocNum <= mBase) or (delAssocNum > mDelAssocs + mBase) then
    dbErrorNum := dbErr_InvalidDelAssocNumber
  else if mBase <> 0 then
    delAssocNum := delAssocNum MOD mBase;
  CheckDelAssocNum := dbErrorNum = 0;
end;

{-----------------------------[ TDBReadWriteDesc ]---------------------------}
{----------------------------------------------------------------------------}
{  Initialize a database read/write description object.                      }
{                                                                            }
{  InParms : aFileName - a valid file name                                   }
{            aPath - an existing file path                                   }
{----------------------------------------------------------------------------}
constructor TDBReadWriteDesc.Init(aFileName, aPath: PChar);
begin
  TDBReadDesc.PInit(aFileName, aPath);

  if dbLastOpenError <> 0 then
    Fail
  else if dbErrorNum <> 0 then
  begin
    mDataType := 0;
    mBase := 0;
    mFields := 0;
    mKeys := 0;
    mAssocs := 0;
    mDelAssocs := 0;
    FillChar(mFieldList, SizeOf(mFieldList), 0);
    FillChar(mKeyList, SizeOf(mKeyList), 0);
    FillChar(mAssocList, SizeOf(mAssocList), 0);
    FillChar(mDelAssocList, SizeOf(mDelAssocList), 0);
  end;
end;

{----------------------------------------------------------------------------}
{  Set the description of the file.                                          }
{                                                                            }
{  Returns : TRUE if no error.                                               }
{  InParms : aDesc is the description string.                                }
{----------------------------------------------------------------------------}
function TDBReadWriteDesc.SetFileDesc(aDesc: PChar): boolean;
begin
  dbErrorNum := 0;
  {- wipe out any garbage info (so its not stored to disk) }
  FillChar(mFileInfo.desc, SizeOf(mFileInfo.desc), 0);
  StrLCopy(mFileInfo.desc, aDesc, SizeOf(mFileInfo.desc)-1);
  SetFileDesc := dbErrorNum = 0;
end;

{----------------------------------------------------------------------------}
{  Set the user file flag value.                                             }
{                                                                            }
{  Returns : TRUE if no error.                                               }
{  InParms : flag is the user flag value to be stored.                       }
{----------------------------------------------------------------------------}
function TDBReadWriteDesc.SetFileFlag(flag: word): boolean;
begin
  dbErrorNum := 0;
  mFileInfo.userFlag := flag;
  SetFileFlag := dbErrorNum = 0;
end;

{----------------------------------------------------------------------------}
{  Set the auto sequence number flag value.                                  }
{                                                                            }
{  Returns : TRUE if no error.                                               }
{  InParms : seqFlag is the auto sequence number flag value.                 }
{----------------------------------------------------------------------------}
function TDBReadWriteDesc.SetAutoSeq(seqFlag: boolean): boolean;
begin
  dbErrorNum := 0;
  mFileInfo.AutoSeq := seqFlag;
  SetAutoSeq := dbErrorNum = 0;
end;

{----------------------------------------------------------------------------}
{  Set the data type value and compute the base value.                       }
{                                                                            }
{  Returns : TRUE if no error.                                               }
{  InParms : aType is the data type value.                                   }
{----------------------------------------------------------------------------}
function TDBReadWriteDesc.SetDataType(aType: integer): boolean;
begin
  dbErrorNum := 0;
  mDataType := aType;
(*mBase := aType * 100;*)
  SetDataType := dbErrorNum = 0;
end;

{----------------------------------------------------------------------------}
{  Add a field to the database description.                                  }
{                                                                            }
{  Errors returned                                                           }
{    dbErr_InvalidFieldName                                                  }
{    dbErr_FieldNameExists                                                   }
{    dbErr_InvalidFieldSize                                                  }
{    dbErr_MaxFieldsDefined                                                  }
{    dbErr_InvalidType                                                       }
{    dbErr_MaxRecSizeExceeded                                                }
{                                                                            }
{  Returns : field number if added successfully else 0                       }
{  InParms : afield - a field info struct                                    }
{----------------------------------------------------------------------------}
function TDBReadWriteDesc.AddField(afield: TFldInfo): integer;
var
  k, j  : integer;
begin
  dbErrorNum := 0;
  AddField := 0;
  if StrLen(afield.fldName) = 0 then
    dbErrorNum := dbErr_InvalidFieldName
  else if FieldNumber(afield.fldName) <> 0 then
    dbErrorNum := dbErr_FieldNameExists
  else if ((afield.fldType = dbString) or (afield.fldType = dbZString) or
           (afield.fldType = dbZCode) or (afield.fldType = dbCode) or
           (afield.fldType = dbBlock)) and (afield.fldSize = 0) then
    dbErrorNum := dbErr_InvalidFieldSize
  else if mFields = maxFldsPerRecord then
    dbErrorNum := dbErr_MaxFieldsDefined
  else if afield.fldType = dbInvalid then
    dbErrorNum := dbErr_InvalidType
  else
  begin
    k := RecordSize;
    j := DBSizeFromType(afield.fldType, afield.fldSize);
    if k + j > maxRecordSize then
      dbErrorNum := dbErr_MaxRecSizeExceeded
    else
    begin
      Inc(mFields);
      StrLCopy(mFieldList[mFields].fldName, afield.fldName, maxFldNameLen);
      mFieldList[mFields].fldSize := j;
      mFieldList[mFields].fldPos := k;
      mFieldList[mFields].fldType := afield.fldType;
      mFieldList[mFields].userFlag := afield.userFlag;
      mFieldList[mFields].sysFlag := 0;
      AddField := mFields + mBase;
    end;
  end;
end;

{----------------------------------------------------------------------------}
{  Delete a field.  No field with keys can be deleted.  Delete any           }
{  associations that reference the deleted field.  Repack trailing           }
{  fields.                                                                   }
{                                                                            }
{  Errors returned                                                           }
{    dbErr_FieldHasKeys                                                      }
{                                                                            }
{  Returns : TRUE if no error.                                               }
{  InParms : fieldNum is the field number to delete.                         }
{----------------------------------------------------------------------------}
function TDBReadWriteDesc.DeleteField(fieldNum: integer): boolean;
var
  k     : integer;
  s     : integer;
  fSize : integer;
begin
  dbErrorNum := 0;
  if CheckFieldNum(fieldNum) then
  begin
    if IsKeyField(fieldNum) then
    begin
      dbErrorNum := dbErr_FieldHasKeys;
    end
    else if DeleteFldAssoc(fieldNum) then
    begin
      {- first, pack all fields adjusting fld positions as neccessary }
      fSize := mFieldList[fieldNum].fldSize;
      for k := fieldNum+1 to mFields do
      begin
        Dec(mFieldList[k].fldPos, fSize);
        mFieldList[k-1] := mFieldList[k];
      end;
      {- clear last field }
      FillChar(mFieldList[mFields], SizeOf(mFieldList[mFields]), 0);
      Dec(mFields);

      {- now, for every segment that referenced a field number greater
         than the one deleted, decrement its field reference by 1 }
      for k := 1 to mKeys do
        for s := 1 to mKeyList[k].numSegs do
          if mKeyList[k].segList[s].fldNum > fieldNum then
            Dec(mKeyList[k].segList[s].fldNum);

      {- now, for every association that referenced a field number
         greater than the one deleted, decrement its reference by 1 }
      for k := 1 to mAssocs do
        for s := 1 to mAssocList[k].numFields do
          if mAssocList[k].fldNum[s] > fieldNum then
            Dec(mAssocList[k].fldNum[s]);
    end;
  end;
  DeleteField := dbErrorNum = 0;
end;

{----------------------------------------------------------------------------}
{  Add a key to the database description.                                    }
{                                                                            }
{  Errors returned                                                           }
{    dbErr_MaxKeysDefined                                                    }
{                                                                            }
{  Returns : a key number if added successfully else 0                       }
{  InParms : akey - a key info struct                                        }
{----------------------------------------------------------------------------}
function TDBReadWriteDesc.AddKey(akey: TKeyInfo): integer;
begin
  dbErrorNum := 0;
  AddKey := 0;
  if mKeys = maxKeys then
  begin
    dbErrorNum := dbErr_MaxKeysDefined;
  end
  else
  begin
    Inc(mKeys);
    mKeyList[mKeys] := akey;
    with mKeyList[mKeys] do
    begin
      numSegs := 0;
      FillChar(segList, SizeOf(segList), 0);
    end;
    AddKey := mKeys + mBase;
  end;
end;

{----------------------------------------------------------------------------}
{  Add a key segment to a key.                                               }
{                                                                            }
{  Errors returned                                                           }
{    dbErr_MaxSegsDefined                                                    }
{    dbErr_MaxKeyLenExceeded                                                 }
{                                                                            }
{  Returns : TRUE if no error.                                               }
{  InParms : keyNum is the key number to add the key segment to.             }
{            akeySeg contains the key segment information.                   }
{----------------------------------------------------------------------------}
function TDBReadWriteDesc.AddKeySeg(keyNum: integer; akeySeg: TSegmentInfo): boolean;
var
  segIdx : integer;
  k, c   : integer;
begin
  dbErrorNum := 0;
  if CheckKeyNum(keyNum) and CheckFieldNum(akeySeg.fldNum) then
  begin
    if mKeyList[keyNum].numSegs = maxSegs then
      dbErrorNum := dbErr_MaxSegsDefined
    else if (PKeyLength(keyNum) + mFieldList[akeySeg.fldNum].fldSize) >
            maxKeyLen then
      dbErrorNum := dbErr_MaxKeyLenExceeded
    else
    begin
      Inc(mKeyList[keyNum].numSegs);
      mKeyList[keyNum].segList[mKeyList[keyNum].numSegs] := akeySeg;
    end;
  end;
  AddKeySeg := dbErrorNum = 0;
end;

{----------------------------------------------------------------------------}
{  Add an association to the database description.  If there is only         }
{  one referenced dbSeqRef field, then assign the assoc number to the        }
{  field info struct.                                                        }
{                                                                            }
{  Errors returned                                                           }
{    dbErr_MaxAssocsDefined                                                  }
{                                                                            }
{  Returns : an association number if added successfully else 0              }
{  InParms : aassoc - an association info struct                             }
{----------------------------------------------------------------------------}
function TDBReadWriteDesc.AddAssoc(var aassoc: TAssocInfo): integer;
var
  desc : PDBReadDesc;
  i    : integer;
begin
  dbErrorNum := 0;
  AddAssoc := 0;
  desc := New(PDBReadDesc, Init(aassoc.fName, mFilePath));
  if mAssocs = maxAssocs then
  begin
    dbErrorNum := dbErr_MaxAssocsDefined;
  end
  else if desc = nil then
  begin
    dbErrorNum := dbLastOpenError;
  end
  else if desc^.dbErrorNum <> 0 then
  begin
    dbErrorNum := desc^.dbErrorNum;
    desc^.dbErrorNum := 0;
  end
  else if (aassoc.keyNum <> 0) and
          (not desc^.CheckKeyNum(aassoc.keyNum)) then
  begin
    dbErrorNum := desc^.dbErrorNum;
    desc^.dbErrorNum := 0;
  end
  else
  begin
    for i := 1 to aassoc.numFields do
      if aassoc.fldNum[i] <> 0 then
        if not CheckFieldNum(aassoc.fldNum[i]) then
          break;
  end;

  if dbErrorNum = 0 then
  begin
    Inc(mAssocs);
    mAssocList[mAssocs] := aassoc;
    if (aassoc.numFields = 1) and (aassoc.fldNum[1] <> 0) then
      if mFieldList[aassoc.fldNum[1]].fldType = dbSeqRef then
        mFieldList[aassoc.fldNum[1]].fldAssoc := mAssocs;
    AddAssoc := mAssocs + mBase;
  end;
  MSDisposeObj(desc);
end;

{----------------------------------------------------------------------------}
{  Add a delete association to the database description.                     }
{                                                                            }
{  Assumptions                                                               }
{  Only the following delete combinations are supported -                    }
{    Downline and DISALLOW_DELETE                                            }
{    Downline and DELETE_ASSOCIATED                                          }
{    Upline   and DISALLOW_DELETE                                            }
{                                                                            }
{  Errors returned                                                           }
{    dbErr_MaxDelAssocsDefined                                               }
{    dbErr_InvalidDelAssoc                                                   }
{                                                                            }
{  Returns : a delete association number if added successfully else 0        }
{  InParms : adel - a delete association info struct                         }
{----------------------------------------------------------------------------}
function TDBReadWriteDesc.AddDelAssoc(var adel: TDelAssocInfo): integer;
var
  desc : PDBReadDesc;
begin
  dbErrorNum := 0;
  AddDelAssoc := 0;

  desc := New(PDBReadDesc, Init(adel.fName, mFilePath));
  if mDelAssocs = maxDelAssocs then
  begin
    dbErrorNum := dbErr_MaxDelAssocsDefined;
  end
  else if desc = nil then
  begin
    dbErrorNum := dbLastOpenError;
  end
  else if desc^.dbErrorNum <> 0 then
  begin
    dbErrorNum := desc^.dbErrorNum;
    desc^.dbErrorNum := 0;
  end
  else if not(((adel.delAction = DELETE_ASSOCIATED) and
                adel.bDownline) or
              (adel.delAction = DISALLOW_DELETE)) then
  begin
    dbErrorNum := dbErr_InvalidDelAssoc;
  end
  else if (not adel.bDownline) and
          (adel.delAction = DISALLOW_DELETE) and
          (StrLen(adel.assocFldName) <> 0) and
          (desc^.FieldNumber(adel.assocFldName) = 0) then
  begin
    dbErrorNum := dbErr_InvalidFieldName;
  end
  else
  begin
    Inc(mDelAssocs);
    mDelAssocList[mDelAssocs] := adel;
    AddDelAssoc := mDelAssocs + mBase;
  end;
  MSDisposeObj(desc);
end;

{----------------------------------------------------------------------------}
{  Store definition file to disk, return TRUE if ok.                         }
{                                                                            }
{  Assumptions                                                               }
{  The definition file has the following format -                            }
{    Header-Version                                                          }
{    <CR/LF>                                                                 }
{    FileDescription                                                         }
{    ^Z                                                                      }
{    FileInformation                                                         }
{    DataType                                                                }
{    Base                                                                    }
{    num Fields                                                              }
{    num Keys                                                                }
{    num Assocs                                                              }
{    num DelAssocs                                                           }
{    FieldList                                                               }
{    KeyList                                                                 }
{    AssocList                                                               }
{    DelAssocList                                                            }
{                                                                            }
{  Errors returned                                                           }
{    dbErr_CreatingDefFile                                                   }
{    dbErr_WritingDefFile                                                    }
{                                                                            }
{  Returns : TRUE if function successful.                                    }
{----------------------------------------------------------------------------}
function TDBReadWriteDesc.StoreDef: boolean;
var
  fil   : file;
  tstr  : array[0..255] of char;
begin
{$I-}
  if IOResult <> 0 then ;
  dbErrorNum := 0;
  StrCopy(tstr, mFilePath);
  StrCat(tstr, mFileInfo.fName);
  StrCat(tstr, fileDescExt);

  Assign(fil, tstr);
  Rewrite(fil, 1);

  repeat
    if IOResult <> 0 then
    begin
      dbErrorNum := dbErr_CreatingDefFile;
      break;
    end;

    {- write header }
    StrCopy(tstr, dbHeader);
    BlockWrite(fil, tstr, StrLen(tstr));
    if IOResult <> 0 then
    begin
      dbErrorNum := dbErr_WritingDefFile;
      break;
    end;

    tstr[0] := '-';
    BlockWrite(fil, tstr[0], 1);
    if IOResult <> 0 then
    begin
      dbErrorNum := dbErr_WritingDefFile;
      break;
    end;

    StrCopy(tstr, dbVersion);
    BlockWrite(fil, tstr, StrLen(tstr));
    if IOResult <> 0 then
    begin
      dbErrorNum := dbErr_WritingDefFile;
      break;
    end;

    { write CR/LF }
    StrCopy(tstr, #13#10);
    BlockWrite(fil, tstr, 2);
    if IOResult <> 0 then
    begin
      dbErrorNum := dbErr_WritingDefFile;
      break;
    end;

    BlockWrite(fil, mFileInfo.desc, SizeOf(mFileInfo.desc));
    if IOResult <> 0 then
    begin
      dbErrorNum := dbErr_WritingDefFile;
      break;
    end;

    { write ^Z }
    tstr[0] := ^Z;
    BlockWrite(fil, tstr, 1);
    if IOResult <> 0 then
    begin
      dbErrorNum := dbErr_WritingDefFile;
      break;
    end;

    BlockWrite(fil, mFileInfo, SizeOf(mFileInfo));
    if IOResult <> 0 then
    begin
      dbErrorNum := dbErr_WritingDefFile;
      break;
    end;

    BlockWrite(fil, mDataType, SizeOf(mDataType));
    if IOResult <> 0 then
    begin
      dbErrorNum := dbErr_WritingDefFile;
      break;
    end;

    BlockWrite(fil, mBase, SizeOf(mBase));
    if IOResult <> 0 then
    begin
      dbErrorNum := dbErr_WritingDefFile;
      break;
    end;

    BlockWrite(fil, mFields, SizeOf(mFields));
    if IOResult <> 0 then
    begin
      dbErrorNum := dbErr_WritingDefFile;
      break;
    end;

    BlockWrite(fil, mKeys, SizeOf(mKeys));
    if IOResult <> 0 then
    begin
      dbErrorNum := dbErr_WritingDefFile;
      break;
    end;

    BlockWrite(fil, mAssocs, SizeOf(mAssocs));
    if IOResult <> 0 then
    begin
      dbErrorNum := dbErr_WritingDefFile;
      break;
    end;

    BlockWrite(fil, mDelAssocs, SizeOf(mDelAssocs));
    if IOResult <> 0 then
    begin
      dbErrorNum := dbErr_WritingDefFile;
      break;
    end;

    BlockWrite(fil, mFieldList, SizeOf(mFieldList));
    if IOResult <> 0 then
    begin
      dbErrorNum := dbErr_WritingDefFile;
      break;
    end;

    BlockWrite(fil, mKeyList, SizeOf(mKeyList));
    if IOResult <> 0 then
    begin
      dbErrorNum := dbErr_WritingDefFile;
      break;
    end;

    BlockWrite(fil, mAssocList, SizeOf(mAssocList));
    if IOResult <> 0 then
    begin
      dbErrorNum := dbErr_WritingDefFile;
      break;
    end;

    BlockWrite(fil, mDelAssocList, SizeOf(mDelAssocList));
    if IOResult <> 0 then
    begin
      dbErrorNum := dbErr_WritingDefFile;
      break;
    end;
  until TRUE;

  Close(fil);
  if IOResult <> 0 then {nothing};

  StoreDef := dbErrorNum = 0;
{$I+}
end;

{----------------------------------------------------------------------------}
{  This function will copy a database description object.                    }
{                                                                            }
{  NOTE:                                                                     }
{  Private data members of TDBReadDesc are accessed directly.                }
{                                                                            }
{  InParms : src is the file definition for the database information         }
{            file to be copied from.                                         }
{----------------------------------------------------------------------------}
function TDBReadWriteDesc.CopyDesc(src: PDBReadDesc): boolean;
begin
  dbErrorNum := 0;
  StrCopy(mFileInfo.desc, src^.mFileInfo.desc);
  mFileInfo.pageSize:= src^.mFileInfo.pageSize;
  mFileInfo.autoSeq:= src^.mFileInfo.autoSeq;
  mFileInfo.userFlag:= src^.mFileInfo.userFlag;
  mDataType:= src^.mDataType;
  mBase:= src^.mBase;
  mFields:= src^.mFields;
  mKeys:= src^.mKeys;
  mAssocs:= src^.mAssocs;
  mDelAssocs:= src^.mDelAssocs;
  Move(src^.mFieldList, mFieldList, SizeOf(mFieldList));
  Move(src^.mKeyList, mKeyList, SizeOf(mKeyList));
  Move(src^.mAssocList, mAssocList, SizeOf(mAssocList));
  Move(src^.mDelAssocList, mDelAssocList, SizeOf(mDelAssocList));
  CopyDesc := dbErrorNum = 0;
end;

{----------------------------------------------------------------------------}
{  This function will rename a database and its associated files.            }
{                                                                            }
{  Errors returned                                                           }
{    dbErr_CantRenameDataFile                                                }
{    dbErr_CantRenameDefFile                                                 }
{                                                                            }
{  Returns : TRUE if no error was generated.                                 }
{  InParms : newName is the name that the files will be renamed to.          }
{----------------------------------------------------------------------------}
function TDBReadWriteDesc.RenameDatabase(newName: PChar): boolean;
var
  oldStr : array[0..maxPathLen] of char;
  newStr : array[0..maxPathLen] of char;
  oldStr2: array[0..maxPathLen] of char;
  newStr2: array[0..maxPathLen] of char;
  fil    : file;
begin
  dbErrorNum := 0;
{$I-}
  if IOResult <> 0 then ;

  StrCopy(oldStr, mFilePath);
  StrCopy(newStr, mFilePath);
  StrCat(oldStr, mFileInfo.fName);
  StrCat(newStr, newName);

  StrCopy(oldStr2, oldStr);
  StrCat(oldStr2, fileDescExt);
  StrCopy(newStr2, newStr);
  StrCat(newStr2, fileDescExt);
  Assign(fil, oldStr2);
  Rename(fil, newStr2);
  if IOResult <> 0 then
    dbErrorNum := dbErr_CantRenameDefFile;

  StrCopy(oldStr2, oldStr);
  StrCat(oldStr2, datFileExt);
  StrCopy(newStr2, newStr);
  StrCat(newStr2, datFileExt);
  Assign(fil, oldStr2);
  Rename(fil, newStr2);
  if IOResult <> 0 then
    if dbErrorNum = 0 then
      dbErrorNum := dbErr_CantRenameDataFile;

  if dbErrorNum = 0 then
  begin
    StrCopy(mFileInfo.fName, newName);
    StoreDef;
  end;

{$I+}
  RenameDatabase := dbErrorNum = 0;
end;

{----------------------------------------------------------------------------}
{  Determine if the specified field number is used as part of a key.         }
{                                                                            }
{  Returns : TRUE if the field number is used as part of any key.            }
{  InParms : fieldNum is the field number to check.                          }
{----------------------------------------------------------------------------}
function TDBReadWriteDesc.IsKeyField(fieldNum: integer): boolean;
var
  k, s : integer;
  kf   : boolean;
begin
  dbErrorNum := 0;
  kf := FALSE;
  for k := 1 to mKeys do
  begin
    for s := 1 to mKeyList[k].numSegs do
      if mKeyList[k].segList[s].fldNum = fieldNum then
      begin
        kf := TRUE;
        break;
      end;
    if kf = TRUE then
      break;
  end;
  IsKeyField := kf;
end;

{----------------------------------------------------------------------------}
{  Delete any association for the specified field number as well as          }
{  any related delassoc in the associated file.                              }
{                                                                            }
{  Returns : TRUE if no error was generated.                                 }
{  InParms : fieldNum is the field number to check for association.          }
{----------------------------------------------------------------------------}
function TDBReadWriteDesc.DeleteFldAssoc(fieldNum: integer): boolean;
var
  j, k  : integer;
  desc  : PDBReadWriteDesc;
  pack  : boolean;
begin
  dbErrorNum := 0;
  {- first, delete any associations }
  j := mFieldList[fieldNum].fldAssoc;
  if j <> 0 then
  begin
    desc := New(PDBReadWriteDesc, Init(mAssocList[j].fName, mFilePath));
    if desc = nil then
    begin
      dbErrorNum := dbLastOpenError;
    end
    else if desc^.dbErrorNum <> 0 then
    begin
      dbErrorNum := desc^.dbErrorNum;
      desc^.dbErrorNum := 0;
    end
    else
    begin
      for k := j+1 to mAssocs do
        mAssocList[k-1] := mAssocList[k];
      {- clear last assoc }
      FillChar(mAssocList[mAssocs], SizeOf(mAssocList[mAssocs]), 0);
      Dec(mAssocs);
      { remove association pointer from field record }
      mFieldList[fieldNum].fldAssoc:= 0;
      { now decrement all fldAssoc fields above the one deleted }
      for k:= 1 to mFields do
      begin
        if (mFieldList[k].fldAssoc > j) then
          Dec(mFieldList[k].fldAssoc);
      end;

      pack := FALSE;
      for k := 1 to desc^.mDelAssocs do
        if StrIComp(desc^.mDelAssocList[k].fName, mFileInfo.fName) = 0 then
        begin
          pack := TRUE;
          break;
        end;
      if pack then
      begin
        for k := k+1 to desc^.mDelAssocs do
          desc^.mDelAssocList[k-1] := desc^.mDelAssocList[k];
        FillChar(desc^.mDelAssocList[desc^.mDelAssocs],
                 SizeOf(desc^.mDelAssocList[desc^.mDelAssocs]), 0);
        Dec(desc^.mDelAssocs);
        desc^.StoreDef;
      end;
    end;
    MSDisposeObj(desc);
  end;

  DeleteFldAssoc := dbErrorNum = 0;
end;

{----------------------------------------------------------------------------}
{  Verify that the file definition meets the criteria for creation.          }
{                                                                            }
{  Errors returned                                                           }
{    dbErr_NoFields                                                          }
{    dbErr_RecordTooSmall                                                    }
{    dbErr_NoKeys                                                            }
{    dbErr_NoKeySegments                                                     }
{                                                                            }
{  Returns : TRUE if the file definition is valid to create.                 }
{----------------------------------------------------------------------------}
function TDBReadWriteDesc.ValidToCreate: boolean;
var
  k  : integer;
begin
  dbErrorNum := 0;

  if mFields = 0 then
    dbErrorNum := dbErr_NoFields
  else if RecordSize < minRecordSize then
    dbErrorNum := dbErr_RecordTooSmall
  else if (mKeys = 0) and not mFileInfo.autoSeq then
    dbErrorNum := dbErr_NoKeys
  else
  begin
    k := 1;
    while (dbErrorNum = 0) and (k <= mKeys) do
    begin
      if mKeyList[k].numSegs = 0 then
        dbErrorNum := dbErr_NoKeySegments;
      Inc(k);
    end;
  end;
  ValidToCreate := dbErrorNum = 0;
end;

{------------------------------[ TDBCreator ]--------------------------------}
{----------------------------------------------------------------------------}
{  Create the BTRIEVE and database information files using the database      }
{  description object.                                                       }
{                                                                            }
{  Returns : TRUE if function successful.                                    }
{  InParms : pDesc is file definition to use to create the BTRIEVE           }
{            file and the information file.                                  }
{----------------------------------------------------------------------------}
function TDBCreator.CreateTable(pDesc: PDBReadWriteDesc): boolean;
var
   keyBuf : TKeyBuf;        {- key buffer }
   fBuf   : TBTFileInfo;    {- btrieve file info buffer }
   datLen : word;
   i      : integer;
   k, j   : integer;
   segFld : integer;
   keyNum : integer;
   posBlk : TPosBlk;        {- position block }
begin
  dbErrorNum := 0;
  pDesc^.mFileInfo.pageSize := maxPageSize;
  {- need to store the definition to disk }
  if pDesc^.ValidToCreate and pDesc^.StoreDef then
  begin
    StrCopy(keyBuf, pDesc^.mFilePath);
    StrCat(keyBuf, pDesc^.mFileInfo.fName);
    StrCat(keyBuf, datFileExt);

    { fill fSpec buffer }
    FillChar(fBuf, SizeOf(fBuf), 0);
    with fBuf.fSpec do
    begin
      recordLen := pDesc^.RecordSize;
      if pDesc^.mFileInfo.autoSeq then
        Inc(recordLen, SizeOf(TSeqNum));
      pageSize := pDesc^.mFileInfo.pageSize;
      totalKeys := pDesc^.mKeys;
      if pDesc^.mFileInfo.autoSeq then
        Inc(totalKeys);    { add one for auto seq key }
      notUsed := 0;
      cfileFlags := 0;
      creserved := 0;
      allocation := 0;
    end;
    datlen := SizeOf(TBTFileSpec);

    { now fill keySpec records }
    i := 1;
    for k := 1 to pDesc^.mKeys do
    begin
      for j := 1 to pDesc^.mKeyList[k].numSegs do
      begin
        with fBuf.kSpec[i] do
        begin
          { get the fld that the current segment refers to }
          segFld := pDesc^.mKeyList[k].segList[j].fldNum;
          {- key positions are 1 based }
          keyPos := pDesc^.mFieldList[segFld].fldPos + 1;
          keyLength := pDesc^.mFieldList[segFld].fldSize;
          notUsed := 0;
          cExtKeyType := BTExtKeyType(pDesc^.mFieldList[segFld].fldType);
          cNullValue := 0;
          cReserved := 0;

          {- set keyflags }
          keyFlags := EXTTYPE_KEY; {default}

          {- auto increment keys are not modifiable }
          {  all other keys are }
          if pDesc^.mFieldList[segFld].fldType <> dbAutoInc then
            keyFlags := keyFlags or MODX;
          {- for dbCode types, set case insensitive key }
          if (pDesc^.mFieldList[segFld].fldType = dbCode) or
             (pDesc^.mFieldList[segFld].fldType = dbZCode) then
            keyFlags := keyFlags or NOCASE_KEY;
          if not pDesc^.mKeyList[k].unique then
            keyflags := keyflags or DUP;
          if pDesc^.mKeyList[k].segList[j].descend then
            keyFlags := keyFlags or DESC_KEY;
          {- if this is not the last segment in key indicate so}
          if j < pDesc^.mKeyList[k].numSegs then
            keyFlags := keyFlags or SEG;

          {- increment data length by one key spec record }
          Inc(datLen, SizeOf(TBTKeySpec));
          Inc(i);  {- increment keyspeclist index }
        end;
      end;
    end;

    {- if auto sequence number specified then add it to
       key specification }
    if pDesc^.mFileInfo.autoSeq then
    begin
      {- i is already set to the right index from above}
      with fBuf.kSpec[i] do
      begin
        keyPos := pDesc^.RecordSize + 1; { make 1 based }
        keyLength := SizeOf(TSeqNum);
        keyFlags := EXTTYPE_KEY;
        notUsed := 0;
        cExtKeyType := BTExtKeyType(dbAutoInc);
        cNullValue := 0;
        cReserved := 0;
        Inc(datLen, SizeOf(TBTKeySpec));
      end;
    end;

    if dbErrorNum = 0 then
    begin
      keyNum := 0;
      dbErrorNum := BTRV(B_CREATE, posBlk, fBuf, datlen, keyBuf, keyNum);
      if dbErrorNum > 0 then
        dbErrorNum := dbErrorNum + MOD_BTRV;
    end;
  end
  else
  begin
    dbErrorNum := pDesc^.dbErrorNum;
    pDesc^.dbErrorNum := 0;
  end;
  CreateTable := dbErrorNum = 0;
end;


{-------------------------------[ TDBRec ]-----------------------------------}
{----------------------------------------------------------------------------}
{  Initialize a database record object.                                      }
{                                                                            }
{  Assumptions                                                               }
{  If memory allocation fails then init will fail.                           }
{                                                                            }
{  Errors returned                                                           }
{    dbErr_CannotAllocateRecBuf                                              }
{                                                                            }
{  InParms : pCursor - a valid cursor object                                 }
{----------------------------------------------------------------------------}
constructor TDBRec.Init(pCursor: PDBCursor);
var
  c  : integer;
  k  : integer;
begin
  inherited Init;
  mDesc := pCursor^.mDesc;
  mDataType := pCursor^.mDataType;
  mCurPosition := 0;
  mCurSeq := nil;
  mFlags := nil;
  mBufSize := pCursor^.mBufSize;

  GetMem(mBuffer, mBufSize);
  GetMem(mTBuf, mBufSize);
  if (mBuffer = nil) or (mTBuf = nil) then
  begin
    dbLastOpenError := dbErr_CannotAllocateRecBuf;
    Done;
    Fail;
  end;

  FillChar(mBuffer^, mBufSize, 0);
  FillChar(mTBuf^, mBufSize, 0);
  mCurSeq := Addr(mBuffer^[mDesc^.RecordSize]);
  c := 0;
  for k := 1 to mDesc^.mFields do
  begin
    if (mDesc^.mFieldList[k].userFlag and db_Flags) <> 0 then
    begin
      mFlags := Addr(mBuffer^[c]);
      Break;
    end;
    Inc(c, mDesc^.mFieldList[k].fldSize);
  end;
end;

{----------------------------------------------------------------------------}
{  Frees memory allocated for the record object.                             }
{----------------------------------------------------------------------------}
destructor TDBRec.Done;
begin
  MSFreeMem(mTBuf, mBufSize);
  MSFreeMem(mBuffer, mBufSize);
  inherited Done;
end;

{----------------------------------------------------------------------------}
{  Clears the specified field and sets the field flag to empty.              }
{                                                                            }
{  Returns : TRUE if no error.                                               }
{  InParms : fieldNum is the field number for the clear request.             }
{----------------------------------------------------------------------------}
function TDBRec.ClearField(fieldNum: integer): boolean;
begin
  dbErrorNum := 0;
  if mDesc^.CheckFieldNum(fieldNum) then
  begin
    FillChar(mBuffer^[mDesc^.mFieldList[fieldNum].fldPos],
             mDesc^.mFieldList[fieldNum].fldSize, dbEmptyVal);
  end
  else
  begin
    dbErrorNum := mDesc^.dbErrorNum;
    mDesc^.dbErrorNum := 0;
  end;
  ClearField := dbErrorNum = 0;
end;

{----------------------------------------------------------------------------}
{  Clears all of the fields in the record and the seqnum.                    }
{                                                                            }
{  Returns : TRUE if no error.                                               }
{----------------------------------------------------------------------------}
function TDBRec.ClearRecord: boolean;
begin
  dbErrorNum := 0;
  FillChar(mBuffer^, mBufSize, dbEmptyVal);
  mCurPosition := 0;
  ClearRecord := dbErrorNum = 0;
end;

{----------------------------------------------------------------------------}
{  Clears all of the fields in the record.                                   }
{                                                                            }
{  Returns : TRUE if no error.                                               }
{----------------------------------------------------------------------------}
function TDBRec.ClearData: boolean;
begin
  dbErrorNum := 0;
  FillChar(mBuffer^, mDesc^.RecordSize, dbEmptyVal);
  ClearData := dbErrorNum = 0;
end;

{----------------------------------------------------------------------------}
{  Returns : TRUE if the specified field is empty                            }
{  InParms : fieldNum is the field number for the request.                   }
{----------------------------------------------------------------------------}
function TDBRec.FieldIsEmpty(fieldNum: integer): boolean;
var
  tstr  : array[0..25] of char;
  s     : single;
  d     : double;
begin
  dbErrorNum := 0;
  if mDesc^.CheckFieldNum(fieldNum) then
  begin
    if (mDesc^.mFieldList[fieldNum].fldType = dbSeqRef) or
       (mDesc^.mFieldList[fieldNum].fldType = dbAutoInc) then
      FieldIsEmpty := (StrComp(GetFieldAsStr(fieldNum, tstr, 25), '0') = 0)
    else if (mDesc^.mFieldList[fieldNum].fldType = dbWord) or
            (mDesc^.mFieldList[fieldNum].fldType = dbByte) or
            (mDesc^.mFieldList[fieldNum].fldType = dbInteger) or
            (mDesc^.mFieldList[fieldNum].fldType = dbLongint) or
            (mDesc^.mFieldList[fieldNum].fldType = dbShortInt) then
      FieldIsEmpty := FALSE
    else
      FieldIsEmpty := (StrComp(GetFieldAsStr(fieldNum, tstr, 25), '') = 0);
  end
  else
  begin
    FieldIsEmpty := TRUE;
    dbErrorNum := mDesc^.dbErrorNum;
    mDesc^.dbErrorNum := 0;
  end;
end;

{----------------------------------------------------------------------------}
{  Extract a field from the record buffer.  Up to maxSize bytes will         }
{  be moved into the caller's variable.                                      }
{                                                                            }
{  Assumptions                                                               }
{  That aVal is of the appropriate type for the data retrieved.              }
{                                                                            }
{  Returns : TRUE if no error.                                               }
{  InParms : fieldNum is the field number for the request.                   }
{            maxSize is the maximum number of bytes to copy into aVal.       }
{  OutParms: aVal is the caller's variable.                                  }
{----------------------------------------------------------------------------}
function TDBRec.GetField(fieldNum: integer; aVal: pointer; maxSize: integer): boolean;
begin
  dbErrorNum := 0;
  if mDesc^.CheckFieldNum(fieldNum) then
  begin
    if mDesc^.mFieldList[fieldNum].fldSize < maxSize then
      maxSize := mDesc^.mFieldList[fieldNum].fldSize;
    Move(mBuffer^[mDesc^.mFieldList[fieldNum].fldPos], aVal^, maxSize);
  end
  else
  begin
    dbErrorNum := mDesc^.dbErrorNum;
    mDesc^.dbErrorNum := 0;
  end;
  GetField := dbErrorNum = 0;
end;

{----------------------------------------------------------------------------}
{  Extract a field value from the record buffer and convert the value        }
{  into a string representation.  Up to maxLen chars will be moved           }
{  into the caller's string variable.                                        }
{                                                                            }
{  Errors returned                                                           }
{    dbErr_InvalidType                                                       }
{    dbErr_CannotAllocateRecBuf                                              }
{                                                                            }
{  Returns : TRUE if no error.                                               }
{  InParms : fieldNum is the field number for the request.                   }
{            maxLen is the maximum size of the string variable aStr.         }
{  OutParms: aStr is the caller's string variable.                           }
{----------------------------------------------------------------------------}
function TDBRec.GetFieldAsStr(fieldNum: integer; aStr: PChar; maxLen: integer): PChar;
var
  tstr  : array[0..255] of char;
  lstr  : string;
  pstr  : PChar;
  li    : longint;
  i     : integer;
  w     : word;
  b     : boolean;
  bt    : byte;
  typ   : TDBFldTypes;
  s     : single;
  d     : double;
  si    : shortint;
begin
  dbErrorNum := 0;
  aStr[0] := #0;
  if mDesc^.CheckFieldNum(fieldNum) then
  begin
    typ := mDesc^.mFieldList[fieldNum].fldType;
    case typ of
      dbInteger:
        begin
          GetField(fieldNum + mDesc^.mBase, @i, SizeOf(i));
          IntlIntToStr(i, tstr, 255);
        end;
      dbWord:
        begin
          GetField(fieldNum + mDesc^.mBase, @w, SizeOf(w));
          IntlIntToStr(w, tstr, 255);
        end;
      dbDate:
        begin
          GetField(fieldNum + mDesc^.mBase, @li, SizeOf(longint));
          IntlDateStr(li, tstr, 255);
        end;
      dbTime:
        begin
          GetField(fieldNum + mDesc^.mBase, @li, SizeOf(longint));
          IntlTimeStr(li, INIShowSeconds, tstr, 255);
        end;
      dbLongint, dbAutoInc, dbSeqRef:
        begin
          GetField(fieldNum + mDesc^.mBase, @li, SizeOf(li));
          IntlIntToStr(li, tstr, 255);
        end;
      dbByte, dbShortInt, dbChar, dbBoolean:
        begin
          GetField(fieldNum + mDesc^.mBase, @bt, SizeOf(bt));
          if typ = dbChar then
          begin
            tstr[0] := Char(bt);
            tstr[1] := #0;
          end
          else if typ = dbByte then
            Str(bt, tstr)
          else if typ = dbShortInt then
          begin
            si := ShortInt(bt);
            Str(si, tstr);
          end
          else
            BoolToStr(Boolean(bt), tstr, 255);
        end;
      dbSingle:
        begin
          GetField(fieldNum + mDesc^.mBase, @s, SizeOf(s));
          IntlRealToStr(s, aStr, maxLen);
        end;
      dbDouble:
        begin
          GetField(fieldNum + mDesc^.mBase, @d, SizeOf(d));
          IntlRealToStr(d, aStr, maxLen);
        end;
      dbZString, dbZCode:
        begin
          i := mDesc^.mFieldList[fieldNum].fldSize;
          GetMem(pstr, i);
          if pstr = nil then
            dbErrorNum := dbErr_CannotAllocateRecBuf
          else
          begin
            GetField(fieldNum + mDesc^.mBase, pstr, i);
            StrLCopy(aStr, pstr, maxLen);
            MSFreeMem(pstr, i);
          end;
        end;
      dbBlock:
        begin
          i := mDesc^.mFieldList[fieldNum].fldSize;
          GetMem(pstr, i);
          if pstr = nil then
            dbErrorNum := dbErr_CannotAllocateRecBuf
          else
          begin
            GetField(fieldNum + mDesc^.mBase, pstr, i);
            CvtBlockToStr(aStr, pstr, i, maxLen);
            MSFreeMem(pstr, i);
          end;
        end;
      dbString, dbCode:
        begin
          GetField(fieldNum + mDesc^.mBase, @lstr, SizeOf(tstr)-1);
          StrPCopy(tstr, lstr);
        end;
      else
        dbErrorNum := dbErr_InvalidType;
    end;
    if (typ <> dbZString) and (typ <> dbZCode) and (typ <> dbBlock) and
       (typ <> dbSingle) and (typ <> dbDouble) then
      StrLCopy(aStr, tstr, maxLen);
  end
  else
  begin
    dbErrorNum := mDesc^.dbErrorNum;
    mDesc^.dbErrorNum := 0;
  end;
  GetFieldAsStr := aStr;
end;

{----------------------------------------------------------------------------}
{  Put value into proper field.                                              }
{                                                                            }
{  Assumptions                                                               }
{  Be sure to pass a dereferenced variable to this function.                 }
{  If, if using a ZString and you would like to put a variable you           }
{  declared as (pstr :PChar) then pass in value as pstr^. If you are         }
{  passing a pointer of any type, pass it as p^.                             }
{                                                                            }
{  Returns : TRUE if no error.                                               }
{  InParms : fieldNum is the field number for the request.                   }
{            aVal is the caller's variable.                                  }
{----------------------------------------------------------------------------}
function TDBRec.PutField(fieldNum: integer; aVal: pointer): boolean;
var
  fPos   : integer;
  len    : integer;
  fSize  : word;
begin
  dbErrorNum := 0;
  if mDesc^.CheckFieldNum(fieldNum) then
  begin
    fSize := mDesc^.mFieldList[fieldNum].fldSize;
    fPos := mDesc^.mFieldList[fieldNum].fldPos;
    if (mDesc^.mFieldList[fieldNum].fldType = dbString) or
       (mDesc^.mFieldList[fieldNum].fldType = dbCode) then
    begin
       if Byte(aVal^) > (fSize - 1) then
         Byte(aVal^) := fSize - 1
       else { zero out slack data at end of string (if any) }
         FillChar(mBuffer^[fPos + Byte(aVal^)+1], fSize-Byte(aVal^)-1, dbEmptyVal);
       { now copy string }
       Move(aVal^, mBuffer^[fPos], Byte(aVal^)+1);
    end
    else
    begin
      if (mDesc^.mFieldList[fieldNum].fldType = dbZString) or
         (mDesc^.mFieldList[fieldNum].fldType = dbZCode) then
      begin
        len := StrLen(PChar(aVal));
        if len > fSize - 1 then
        begin
          mBuffer^[fPos + fSize] := dbEmptyVal;
          Dec(fSize);
        end
        else
        begin
          FillChar(mBuffer^[fPos + len + 1], fSize - len - 1, dbEmptyVal);
          { set fsize to length of string + null byte }
          fSize := len + 1;
        end;
      end;
      Move(aVal^, mBuffer^[fPos], fSize);
    end;
  end
  else
  begin
    dbErrorNum := mDesc^.dbErrorNum;
    mDesc^.dbErrorNum := 0;
  end;
  PutField := dbErrorNum = 0;
end;

{----------------------------------------------------------------------------}
{  Convert string value and put value into proper field.                     }
{                                                                            }
{  Errors returned                                                           }
{    dbErr_InvalidParameter                                                  }
{    dbErr_InvalidFieldValue                                                 }
{    dbErr_InvalidType                                                       }
{    dbErr_InvalidFieldValue                                                 }
{                                                                            }
{  Returns : TRUE if no error.                                               }
{  InParms : fieldNum is the field number for the request.                   }
{            aStr is the caller's string variable.                           }
{----------------------------------------------------------------------------}
function TDBRec.PutFieldAsStr(fieldNum: integer; aStr: PChar): boolean;
var
  fPos        : integer;
  fSize       : word;
  typ         : TDBFldTypes;
  code        : integer;
  berr        : boolean;
  fieldBuf    :
    record
      case integer of
        1 : (asInteger : integer);
        2 : (asLongInt : longint);
        3 : (asWord    : word);
        4 : (asShort   : shortint);
        5 : (asByte    : byte);
        6 : (asChar    : char);
        7 : (asString  : string);
        8 : (asSingle  : single);
        9 : (asDouble  : double);
        10: (asDate    : TIntlDate);
        11: (AsTime    : TIntlTime);
    end;
begin
  dbErrorNum := 0;
  PutFieldAsStr := FALSE;
  if aStr = nil then
  begin
    dbErrorNum := dbErr_InvalidParameter;
    exit;
  end;
  if mDesc^.CheckFieldNum(fieldNum) then
  begin
    fSize := mDesc^.mFieldList[fieldNum].fldSize;
    fPos := mDesc^.mFieldList[fieldNum].fldPos;
    typ := mDesc^.mFieldList[fieldNum].fldType;
    code := 0;
    case typ of
      dbInteger:
        begin
          if aStr[0] = #0 then
            StrCopy(aStr, '0');
          fieldBuf.asInteger := IntlStrToInt(aStr, Boolean(code));
        end;
      dbLongint, dbAutoInc, dbSeqRef:
        begin
          if aStr[0] = #0 then
            StrCopy(aStr, '0');
          fieldBuf.asLongInt := IntlStrToInt(aStr, Boolean(code));
        end;
      dbWord:
        begin
          if aStr[0] = #0 then
            StrCopy(aStr, '0');
          fieldBuf.asWord := IntlStrToInt(aStr, Boolean(code));
        end;
      dbByte:
        begin
          if aStr[0] = #0 then
            StrCopy(aStr, '0');
          fieldBuf.asByte := IntlStrToInt(aStr, Boolean(code));
        end;
      dbShortInt:
        begin
          if aStr[0] = #0 then
            StrCopy(aStr, '0');
          fieldBuf.asShort := IntlStrToInt(aStr, Boolean(code));
        end;
      dbChar:
        begin
          fieldBuf.asChar := aStr[0];
        end;
      dbBoolean:
        begin
          fieldBuf.asByte := Byte(StrToBool(aStr, code));
        end;
      dbSingle:
        begin
          if aStr[0] = #0 then
            StrCopy(aStr, '0');
          fieldBuf.asSingle := IntlStrToReal(aStr, Boolean(code));
        end;
      dbDouble:
        begin
          if aStr[0] = #0 then
            StrCopy(aStr, '0.0');
          fieldBuf.asDouble := IntlStrToReal(aStr, Boolean(code));
        end;
      dbZString, dbZCode:
        begin
          if StrLen(aStr) >= fSize then
          begin
            dbErrorNum := dbErr_InvalidFieldValue;
            exit;
          end;
        end;
      dbBlock:
        begin
          code := CvtStrToBlock(aStr, aStr, fSize);
        end;
      dbString, dbCode:
        begin
          if StrLen(aStr) >= fSize then
          begin
            dbErrorNum := dbErr_InvalidFieldValue;
            exit;
          end;
          FieldBuf.AsString := StrPas(aStr);
        end;
      dbDate:
        begin
          fieldBuf.asDate := IntlStrToDate(aStr, berr);
          if berr or not ValidDate(fieldBuf.asDate) then
          begin
            dbErrorNum := dbErr_InvalidFieldValue;
            exit;
          end;
        end;
      dbTime:
        begin
          fieldBuf.asTime := IntlStrToTime(aStr, berr);
          if berr or not ValidTime(fieldBuf.asTime) then
          begin
            dbErrorNum := dbErr_InvalidFieldValue;
            exit;
          end;
        end;
      else
        begin
          dbErrorNum := dbErr_InvalidType;
          exit;
        end;
    end;

    if code <> 0 then
    begin
      dbErrorNum := dbErr_InvalidFieldValue;
      exit;
    end;

    if (typ = dbZString) or (typ = dbZCode) or (typ = dbBlock) then
      Move(aStr^, mBuffer^[fPos], fSize)
    else
      Move(fieldBuf, mBuffer^[fPos], fSize);
  end
  else
  begin
    dbErrorNum := mDesc^.dbErrorNum;
    mDesc^.dbErrorNum := 0;
  end;
  PutFieldAsStr := dbErrorNum = 0;
end;

{----------------------------------------------------------------------------}
{  Copy a record buffer from another file object. Record structures          }
{  must be equivelent.                                                       }
{                                                                            }
{  Returns : TRUE if no error.                                               }
{  InParms : pRec is the source of record buffer.                            }
{----------------------------------------------------------------------------}
function TDBRec.CopyRecord(pRec: PDBRec): boolean;
begin
  dbErrorNum := 0;
  if @self = pRec then
    dbErrorNum := dbErr_CopyToFromSelf
  else if mDesc^.CompareRecStruct(pRec^.mDesc) then
  begin
    {- at this point the record structures are deemed equivelent }
    Move(pRec^.mBuffer^, mBuffer^, mBufSize);
    mCurPosition := pRec^.mCurPosition;
  end;
  CopyRecord := dbErrorNum = 0;
end;

{----------------------------------------------------------------------------}
{  Copy a record buffer (less SeqNum) from another file object.              }
{  Record structures must be equivelent.                                     }
{                                                                            }
{  Returns : TRUE if no error.                                               }
{  InParms : pRec is the source of record buffer.                            }
{----------------------------------------------------------------------------}
function TDBRec.CopyData(pRec: PDBRec): boolean;
begin
  dbErrorNum := 0;
  if @self = pRec then
    dbErrorNum := dbErr_CopyToFromSelf
  else if mDesc^.CompareRecStruct(pRec^.mDesc) then
  begin
    {- at this point the record structures are deemed equivelent }
    Move(pRec^.mBuffer^, mBuffer^, mDesc^.RecordSize);
  end;
  CopyData := dbErrorNum = 0;
end;

{----------------------------------------------------------------------------}
{  Compare record structures and data of objects, including seqnum.          }
{                                                                            }
{  Assumptions                                                               }
{  Equivelent here, means same number of fields, same record size,           }
{  related fields are of same size and type, and data is identical.          }
{                                                                            }
{  Errors returned                                                           }
{    dbErr_InvalidRecBuf                                                     }
{    dbErr_IncompatibleRecStruct                                             }
{                                                                            }
{  Returns : TRUE if equivelent and no error.                                }
{  InParms : pRec is object to compare against.                              }
{----------------------------------------------------------------------------}
function TDBRec.CompareRecord(pRec: PDBRec): boolean;
var
  k  : integer;
  tf : boolean;
begin
  dbErrorNum := 0;
  tf := TRUE;
  if (pRec^.mBuffer = nil) or (mBuffer = nil) then
    dbErrorNum := dbErr_InvalidRecBuf
  else if not mDesc^.CompareRecStruct(pRec^.mDesc) then
    dbErrorNum := dbErr_IncompatibleRecStruct
  else
    tf := CompareBlock(pRec^.mBuffer, mBuffer, mBufSize);
(*  for k := 0 to mBufSize-1 do
      if pRec^.mBuffer^[k] <> mBuffer^[k] then
      begin
        tf := FALSE;
        break;
      end;                                     *)
  CompareRecord := tf and (dbErrorNum = 0);
end;

{----------------------------------------------------------------------------}
{  Compare record structures and data of objects, not includung seqnum.      }
{                                                                            }
{  Assumptions                                                               }
{  Equivelent here, means same number of fields, same record size,           }
{  related fields are of same size and type, and data is identical.          }
{                                                                            }
{  Errors returned                                                           }
{    dbErr_InvalidRecBuf                                                     }
{    dbErr_IncompatibleRecStruct                                             }
{                                                                            }
{  Returns : TRUE if equivelent and no error.                                }
{  InParms : pRec is object to compare against.                              }
{----------------------------------------------------------------------------}
function TDBRec.CompareData(pRec: PDBRec): boolean;
var
  k  : integer;
  tf : boolean;
begin
  dbErrorNum := 0;
  tf := TRUE;
  if (pRec^.mBuffer = nil) or (mBuffer = nil) then
    dbErrorNum := dbErr_InvalidRecBuf
  else if not mDesc^.CompareRecStruct(pRec^.mDesc) then
    dbErrorNum := dbErr_IncompatibleRecStruct
  else
    for k := 0 to mDesc^.RecordSize-1 do
      if pRec^.mBuffer^[k] <> mBuffer^[k] then
      begin
        tf := FALSE;
        break;
      end;
  CompareData := tf and (dbErrorNum = 0);
end;

{----------------------------------------------------------------------------}
{  Return the sequence number for the current record.  Return -1 if          }
{  sequence numbers are not allowed for file.                                }
{                                                                            }
{  Errors returned                                                           }
{    dbErr_AutoSeqNotAllowed                                                 }
{----------------------------------------------------------------------------}
function TDBRec.GetSeqValue: TSeqNum;
begin
  dbErrorNum := 0;
  GetSeqValue := -1;
  if mDesc^.mFileInfo.autoSeq then
    GetSeqValue := mCurSeq^
  else
    dbErrorNum := dbErr_AutoSeqNotAllowed;
end;

{----------------------------------------------------------------------------}
{  Check the DelAssoc chain to see if the record is referenced.              }
{                                                                            }
{  Returns : TRUE if record is referenced and no error.                      }
{----------------------------------------------------------------------------}
function TDBRec.IsReferenced: boolean;
var
  tf    : boolean;
  i, j  : integer;
  assoc : PDBAssocFile;
  afile : PDBFile;
  fPos  : integer;
begin
  dbErrorNum := 0;
  tf := FALSE;

  with mDesc^ do
  begin
    for i := 1 to mDelAssocs do
    begin
      if mDelAssocList[i].bDownline then
      begin
        for j := 1 to mAssocs do
          if StrIComp(mDelAssocList[i].fName, mAssocList[j].fName) = 0 then
            break;

        assoc := New(PDBAssocFile, Init(@Self, j, dbOpenNormal));
        if assoc = nil then
        begin
          Self.dbErrorNum := dbLastOpenError;
          break;
        end;

        if assoc^.dbc^.GetFirst(assoc^.dbr) then
          tf := TRUE;

        MSDisposeObj(assoc);
      end
      else
      begin
        { table scan for SeqNum in specified field name }
        afile := New(PDBFile, Init(mDelAssocList[i].fName, mFilePath, dbOpenNormal));
        if afile = nil then
        begin
          Self.dbErrorNum := dbLastOpenError;
          break;
        end;

        j := afile^.dbd^.FieldNumber(mDelAssocList[i].assocFldName);
        fPos := afile^.dbd^.mFieldList[j].fldPos;
        afile^.dbc^.GetFirst(afile^.dbr);
        repeat
          if mCurSeq^ = PSeqNum(@afile^.dbr^.mBuffer^[fPos])^ then
          begin
            tf := TRUE;
            break;
          end;
        until not afile^.dbc^.GetNext(afile^.dbr);
        MSDisposeObj(afile);
      end;
      if tf = TRUE then
        break;
    end;
  end;

  IsReferenced := tf and (dbErrorNum = 0);
end;

{----------------------------------------------------------------------------}
{  Check the DelAssoc chain to see if this record is referenced by           }
{  anyone who would mind if this record were deleted.                        }
{                                                                            }
{  Returns : TRUE if record is deleteable and no error.                      }
{----------------------------------------------------------------------------}
function TDBRec.OkayToMessWith: boolean;
var
  flg   : boolean;
  i, j  : integer;
  assoc : PDBAssocFile;
  afile : PDBFile;
  fPos  : integer;
begin
  dbErrorNum := 0;
  flg := TRUE;

  with mDesc^ do
  begin
    for i := 1 to mDelAssocs do
    begin
      if mDelAssocList[i].bDownline then
      begin
        for j := 1 to mAssocs do
          if StrIComp(mDelAssocList[i].fName, mAssocList[j].fName) = 0 then
            break;

        assoc := New(PDBAssocFile, Init(@Self, j, dbOpenNormal));
        if assoc = nil then
        begin
          Self.dbErrorNum := dbLastOpenError;
          flg := FALSE;
          break;
        end;

        if mDelAssocList[i].delAction = DISALLOW_DELETE then
          if assoc^.dbc^.GetFirst(assoc^.dbr) then
            flg := FALSE;

        MSDisposeObj(assoc);
      end
      else
      begin
        if mDelAssocList[i].delAction = DISALLOW_DELETE then
        begin
          { table scan for SegNum in specified field name }
          afile := New(PDBFile, Init(mDelAssocList[i].fName, mFilePath, dbOpenNormal));
          if afile = nil then
          begin
            Self.dbErrorNum := dbLastOpenError;
            break;
          end;
          j := afile^.dbd^.FieldNumber(mDelAssocList[i].assocFldName);
          fPos := afile^.dbd^.mFieldList[j].fldPos;
          afile^.dbc^.GetFirst(afile^.dbr);
          repeat
            if mCurSeq^ = PSeqNum(@afile^.dbr^.mBuffer^[fPos])^ then
            begin
              flg := FALSE;
              break;
            end;
          until not afile^.dbc^.GetNext(afile^.dbr);
          MSDisposeObj(afile);
        end;
      end;
      if flg = FALSE then
        break;
    end;
  end;

  OkayToMessWith := flg and (dbErrorNum = 0);
end;

function TDBRec.GetFlag(flagPos: byte): boolean;
begin
  if mflags = nil then
    GetFlag := FALSE
  else
    GetFlag := (mFlags^ and (1 shl (flagPos mod 8))) <> 0;
end;

procedure TDBRec.SetFlag(flagPos: byte; aVal: boolean);
begin
  if mflags <> nil then
  begin
    if aVal then
      mFlags^ := mFlags^ or (1 shl (flagPos mod 8))
    else
      mFlags^ := mFlags^ and not (1 shl (flagPos mod 8));
  end;
end;

{----------------------------------------------------------------------------}
{  Returns a pointer to the DBReadDesc object.                               }
{----------------------------------------------------------------------------}
function TDBRec.Desc: PDBReadDesc;
begin
  dbErrorNum := 0;
  Desc := mDesc;
end;

{------------------------------[ TDBCursor ]---------------------------------}
{----------------------------------------------------------------------------}
{  Initialize and open a BTRIEVE file.  If the constructor fails             }
{  (returns nil), then the error code will be in dbLastOpenError             }
{                                                                            }
{  InParms : pDesc - a database description object                           }
{            openMode - BTRV file open mode                                  }
{----------------------------------------------------------------------------}
constructor TDBCursor.Init(pDesc: PDBReadDesc; openMode: integer);
var
  k       : integer;
  datLen  : word;
  dataBuf : array[0..10] of byte;
begin
  inherited Init;
  mDesc := pDesc;
  mDataType := pDesc^.mDataType;
  FillChar(mKeyBuf, SizeOf(mKeyBuf), 0);
  mKeyStr:= nil;
  mKeyStrSize:= 0;
  FillChar(mPosBlk, SizeOf(mPosBlk), 0);
  mCurKey := 1;
  mBufSize := mDesc^.RecordSize;
  if mDesc^.mFileInfo.autoSeq then
    Inc(mBufSize, SizeOf(TSeqNum));

  StrCopy(mKeyBuf, pDesc^.mFilePath);
  StrCat(mKeybuf, pDesc^.mFileInfo.fName);
  StrCat(mKeyBuf, datFileExt);
  datLen := 0;
  FillChar(dataBuf, SizeOf(dataBuf), 0);

  mKeyStrSize:= mDesc^.GetMaxKeyLengthAsStr+1;
  GetMem(mKeyStr, mKeyStrSize);
  if (mKeyStr = nil) then
  begin
    dbLastOpenError := dbErr_CannotAllocateRecBuf;
    Done;
    Fail;
  end;

  dbLastOpenError := BTRV(B_OPEN, mPosBlk, dataBuf, datLen, mKeyBuf, openMode);
  if dbLastOpenError <> 0 then
  begin
    dbLastOpenError := dbLastOpenError + MOD_BTRV;
    Done;
    Fail;
  end;

  FillChar(mKeyBuf, SizeOf(mKeyBuf), 0);
  dbErrorNum := BTRV(B_GET_FIRST+KEY_BIAS, mPosBlk, dataBuf, datLen, mKeyBuf, mCurKey-1);
  if dbErrorNum > 0 then
    dbErrorNum := dbErrorNum + MOD_BTRV;
{$IFDEF DBLibDEBUG}
  DebugFileOpened(@self);
{$ENDIF}
end;

{----------------------------------------------------------------------------}
{  Closes the BTRIEVE file that the DBCursor references.                     }
{----------------------------------------------------------------------------}
destructor TDBCursor.Done;
var
 dlen : word;
 dBuf : longint;  { not used by btrieve }
begin
{$IFDEF DBLibDEBUG}
  DebugFileClosed(@self);
{$ENDIF}
  BTRV(B_CLOSE, mPosBlk, dBuf, dlen, mKeyBuf, mCurKey-1);
  MSFreeMem(mKeyStr, mKeyStrSize);
  inherited Done;
end;

{----------------------------------------------------------------------------}
{  Gets the key used for indexed operations into the BTRIEVE file.           }
{                                                                            }
{  Returns : the key number used for indexing into the file.                 }
{----------------------------------------------------------------------------}
function TDBCursor.GetCurKeyNum: integer;
begin
  GetCurKeyNum := mCurKey;
end;

{----------------------------------------------------------------------------}
{  Sets the key used for indexed operations into the BTRIEVE file.           }
{                                                                            }
{  Assumptions                                                               }
{  The unit does not need to test that the user is setting the key           }
{  with the current key value.                                               }
{                                                                            }
{  Returns : TRUE if no error.                                               }
{  InParms : keyNum is the key number to use for indexing into the file.     }
{----------------------------------------------------------------------------}
function TDBCursor.SetCurKeyNum(keyNum: integer): boolean;
begin
  dbErrorNum := 0;
  if mDesc^.CheckKeyNum(keyNum) then
    mCurKey := keyNum;  {- switch current key num }
  SetCurKeyNum := dbErrorNum = 0;
end;

{----------------------------------------------------------------------------}
{  Sets the key used for indexed operations into the BTRIEVE file to the     }
{  sequence number key.                                                      }
{                                                                            }
{  Returns : TRUE if no error.                                               }
{----------------------------------------------------------------------------}
function TDBCursor.SetSeqKeyNum: boolean;
begin
  dbErrorNum := 0;
  if mDesc^.mFileInfo.autoSeq then
    mCurKey := desc^.mKeys + 1  {- switch current key num }
  else
    dbErrorNum := dbErr_AutoSeqNotAllowed;
  SetSeqKeyNum := dbErrorNum = 0;
end;

{----------------------------------------------------------------------------}
{  Return position of current record or -1 if there is an error.             }
{----------------------------------------------------------------------------}
function TDBCursor.GetPosition: longint;
var
  dLen  : word;
  li    : longint;
begin
  dLen := 4;
  dbErrorNum := BTRV(B_GET_POSITION, mPosBlk, li, dLen, mKeyBuf, 0);
  if dbErrorNum <> 0 then
  begin
    dbErrorNum := dbErrorNum + MOD_BTRV;
    GetPosition := -1;
  end
  else
    GetPosition := li;
end;

{----------------------------------------------------------------------------}
{  Go to a specific record specified by pos(ition).                          }
{                                                                            }
{  Assumptions                                                               }
{  Position must have been retrieved by GetPosition.                         }
{                                                                            }
{  Returns : TRUE if no error.                                               }
{  InParms : pRec is the object used to store the record at the position.    }
{            pos is the position to use to find the record.                  }
{----------------------------------------------------------------------------}
function TDBCursor.GotoPosition(pRec: PDBRec; pos: longint): boolean;
var
  dLen  : word;
begin
  if CheckRec(pRec) then
  begin
    dLen := pRec^.mBufSize;
    Move(pos, pRec^.mTBuf^, SizeOf(pos));
    dbErrorNum := BTRV(B_GET_DIRECT, mPosBlk, pRec^.mTBuf^,
                       dLen, mKeyBuf, mCurKey-1);
    if dbErrorNum <> 0 then
    begin
      dbErrorNum := dbErrorNum + MOD_BTRV;
    end
    else
    begin
      Move(pRec^.mTBuf^, pRec^.mBuffer^, pRec^.mBufSize);
      CheckGet(pRec);
    end;
  end;
  GotoPosition := dbErrorNum = 0;
end;

{----------------------------------------------------------------------------}
{  Set the cursors position specified by the position of the record.         }
{  The record contents will NOT be retrieved.                                }
{                                                                            }
{  Assumptions                                                               }
{  The record must have a valid position.                                    }
{                                                                            }
{  Returns : TRUE if no error.                                               }
{  InParms : pRec is the object used to find the position.                   }
{----------------------------------------------------------------------------}
function TDBCursor.GotoRecord(pRec: PDBRec): boolean;
var
  dLen  : word;
begin
  if CheckRec(pRec) then
  begin
    dLen := pRec^.mBufSize;
    Move(pRec^.mCurPosition, pRec^.mTBuf^, SizeOf(pRec^.mCurPosition));
    dbErrorNum := BTRV(B_GET_DIRECT, mPosBlk, pRec^.mTBuf^,
                       dLen, mKeyBuf, mCurKey-1);
    if dbErrorNum <> 0 then
    begin
      dbErrorNum := dbErrorNum + MOD_BTRV;
    end;
  end;
  GotoRecord := dbErrorNum = 0;
end;

{----------------------------------------------------------------------------}
{  Validates the record object before calling a private function to          }
{  perform the delete operation.                                             }
{                                                                            }
{  Assumptions                                                               }
{  Delete reestablishes positioning to the NEXT logical record!  If          }
{  last record is deleted in the logical order, then current position        }
{  will be undefined.                                                        }
{                                                                            }
{  Returns : TRUE if no error.                                               }
{  InParms : pRec is the record object used to validate and find the         }
{            record to delete.                                               }
{----------------------------------------------------------------------------}
function TDBCursor.DeleteRec(pRec: PDBRec): boolean;
begin
  if CheckRec(pRec) then
    PDelete(pRec);
  DeleteRec := dbErrorNum = 0;
end;

{----------------------------------------------------------------------------}
{  This function attempts to lock the specified record and re-gets           }
{  the record.                                                               }
{                                                                            }
{  Returns : TRUE if no error.                                               }
{  InParms : pRec is the record object used to find, lock, and store         }
{            the record.                                                     }
{----------------------------------------------------------------------------}
function TDBCursor.GetLock(pRec: PDBRec): boolean;
var
  dLen  : word;
begin
  if CheckRec(pRec) then
  begin
    dLen := pRec^.mBufSize;
    Move(pRec^.mCurPosition, pRec^.mTBuf^, SizeOf(pRec^.mCurPosition));
    dbErrorNum := BTRV(B_GET_DIRECT+S_WAIT_LOCK, mPosBlk, pRec^.mTBuf^,
                       dLen, mKeyBuf, mCurKey-1);
    if dbErrorNum <> 0 then
    begin
      dbErrorNum := dbErrorNum + MOD_BTRV;
    end
    else
    begin
      Move(pRec^.mTBuf^, pRec^.mBuffer^, pRec^.mBufSize);
      CheckGet(pRec);
    end;
  end;
  GetLock := dbErrorNum = 0;
end;

{----------------------------------------------------------------------------}
{  Releases any record locks.                                                }
{                                                                            }
{  Returns : TRUE if no error.                                               }
{  InParms : pRec is not really used.                                        }
{----------------------------------------------------------------------------}
function TDBCursor.UnlockRec(pRec: PDBRec): boolean;
var
  dLen  : word;
begin
  if CheckRec(pRec) then
  begin
    dLen := 0;
    dbErrorNum := BTRV(B_UNLOCK, mPosBlk, pRec^.mTBuf^, dLen, mKeybuf, 0);
    if dbErrorNum <> 0 then
      dbErrorNum := dbErrorNum + MOD_BTRV;
  end;
  UnlockRec := dbErrorNum = 0;
end;

{----------------------------------------------------------------------------}
{  Find record with specified sequence number.                               }
{                                                                            }
{  Errors returned                                                           }
{    dbErr_AutoSeqNotAllowed                                                 }
{                                                                            }
{  Returns : TRUE if no error.                                               }
{  InParms : pRec is the object used to store the record at the position.    }
{            seqNum is the sequence number to use to find the record.        }
{----------------------------------------------------------------------------}
function TDBCursor.GetSeq(pRec: PDBRec; seqNum: TSeqNum): boolean;
var
  k    : integer;
  dLen : word;
begin
  if CheckRec(pRec) then
  begin
    if mDesc^.mFileInfo.autoSeq then
    begin
      dLen := pRec^.mBufSize;
      Move(seqNum, mKeyBuf, SizeOf(seqNum));
      {- search for sequence number using the seqKeyNum - Dont overwrite
         current RecBuf or current key }
      dbErrorNum := BTRV(B_GET_EQUAL, mPosBlk, pRec^.mBuffer^,
                         dLen, mKeyBuf, mDesc^.mKeys);
      if dbErrorNum <> 0 then
        dbErrorNum := dbErrorNum + MOD_BTRV;
      CheckGet(pRec);
      if mCurKey <> (mDesc^.mKeys + 1) then
      begin
        {- found the seq num, now must change key num back to original
           key num to reset index path }
        k := dbErrorNum; { save errornum so function does not overwrite}
        GotoRecord(pRec);
        dbErrorNum := k;
      end;
    end
    else
      dbErrorNum := dbErr_AutoSeqNotAllowed;
  end;
  GetSeq := dbErrorNum = 0;
end;

{----------------------------------------------------------------------------}
{  Get a record whose current key value is equal to the key value            }
{  created from the key fields of pRec.                                      }
{                                                                            }
{  Assumptions                                                               }
{  dbErrorNum will equal (4) when the record is not found.                   }
{                                                                            }
{  Returns : TRUE if no error.                                               }
{  InParms : pRec is the record object used to find and store the record.    }
{----------------------------------------------------------------------------}
function TDBCursor.GetEQ(pRec: PDBRec): boolean;
var
  k    : integer;
  dLen : word;
begin
  if CheckRec(pRec) then
  begin
    BuildKey(pRec);   {- build the key to search on }
    dLen := pRec^.mBufSize;
    dbErrorNum := BTRV(B_GET_EQUAL, mPosBlk, pRec^.mBuffer^,
                       dLen, mKeyBuf, mCurKey-1);
    if dbErrorNum <> 0 then
      dbErrorNum := dbErrorNum + MOD_BTRV;
    CheckGet(pRec);
  end;
  GetEQ := dbErrorNum = 0;
end;

{----------------------------------------------------------------------------}
{  Get a record whose current key value is greater than or equal to          }
{  the key value created from the key fields of pRec.                        }
{                                                                            }
{  Assumptions                                                               }
{  dbErrorNum will equal (4) when the record is not found.                   }
{                                                                            }
{  Returns : TRUE if no error.                                               }
{  InParms : pRec is the record object used to find and store the record.    }
{----------------------------------------------------------------------------}
function TDBCursor.GetGE(pRec: PDBRec): boolean;
var
  k    : integer;
  dLen : word;
begin
  if CheckRec(pRec) then
  begin
    BuildKey(pRec);   {- build the key to search on }
    dLen := pRec^.mBufSize;
    dbErrorNum := BTRV(B_GET_GE, mPosBlk, pRec^.mBuffer^,
                       dLen, mKeyBuf, mCurKey-1);
    if dbErrorNum <> 0 then
      dbErrorNum := dbErrorNum + MOD_BTRV;
    CheckGet(pRec);
  end;
  GetGE := dbErrorNum = 0;
end;

{----------------------------------------------------------------------------}
{  Go to first record for the current key.                                   }
{                                                                            }
{  Returns : TRUE if no error.                                               }
{  InParms : pRec is the record object used to store the record.             }
{----------------------------------------------------------------------------}
function TDBCursor.GetFirst(pRec: PDBRec): boolean;
var
  dLen  : word;
begin
  if CheckRec(pRec) then
  begin
    {- no need to check cache here, just insure that last Id used is correct }
    dLen := pRec^.mBufSize;
    dbErrorNum := BTRV(B_GET_FIRST, mPosBlk, pRec^.mBuffer^,
                       dLen, mKeyBuf, mCurKey-1);
    if dbErrorNum <> 0 then
      dbErrorNum := dbErrorNum + MOD_BTRV;
    CheckGet(pRec);
  end;
  GetFirst := dbErrorNum = 0;
end;

{----------------------------------------------------------------------------}
{  Go to the last record for the current key.                                }
{                                                                            }
{  Returns : TRUE if no error.                                               }
{  InParms : pRec is the record object used to store the record.             }
{----------------------------------------------------------------------------}
function TDBCursor.GetLast(pRec: PDBRec): boolean;
var
  dLen  : word;
begin
  if CheckRec(pRec) then
  begin
    {- no need to check cache here, just insure that last Id used is correct }
    dLen := pRec^.mBufSize;
    dbErrorNum := BTRV(B_GET_LAST, mPosBlk, pRec^.mBuffer^,
                       dLen, mKeyBuf, mCurKey-1);
    if dbErrorNum <> 0 then
      dbErrorNum := dbErrorNum + MOD_BTRV;
    CheckGet(pRec);
  end;
  GetLast := dbErrorNum = 0;
end;

{----------------------------------------------------------------------------}
{  Go to the next record for the current key.                                }
{                                                                            }
{  Returns : TRUE if no error.                                               }
{  InParms : pRec is the record object used to store the record.             }
{----------------------------------------------------------------------------}
function TDBCursor.GetNext(pRec: PDBRec): boolean;
var
  dLen  : word;
begin
  if CheckRec(pRec) then
  begin
    {- no need to check cache here, just insure that last Id used is correct }
    dLen := pRec^.mBufSize;
    dbErrorNum := BTRV(B_GET_NEXT, mPosBlk, pRec^.mBuffer^,
                       dLen, mKeyBuf, mCurKey-1);
    if dbErrorNum <> 0 then
      dbErrorNum := dbErrorNum + MOD_BTRV;
    CheckGet(pRec);
  end;
  GetNext := dbErrorNum = 0;
end;

{----------------------------------------------------------------------------}
{  Go to the previous record for the current key.                            }
{                                                                            }
{  Returns : TRUE if no error.                                               }
{  InParms : pRec is the record object used to store the record.             }
{----------------------------------------------------------------------------}
function TDBCursor.GetPrev(pRec: PDBRec): boolean;
var
  dLen  : word;
begin
  if CheckRec(pRec) then
  begin
    {- no need to check cache here, just insure that last Id used is correct }
    dLen := pRec^.mBufSize;
    dbErrorNum := BTRV(B_GET_PREVIOUS, mPosBlk, pRec^.mBuffer^,
                       dLen, mKeyBuf, mCurKey-1);
    if dbErrorNum <> 0 then
      dbErrorNum := dbErrorNum + MOD_BTRV;
    CheckGet(pRec);
  end;
  GetPrev := dbErrorNum = 0;
end;

{----------------------------------------------------------------------------}
{  Get the first record whose current key string "contains" the key string   }
{  created from the key fields of pRec (i.e. Partial key).                   }
{                                                                            }
{  Assumptions                                                               }
{  dbErrorNum will equal (B_KEY_VALUE_NOT_FOUND + MOD_BTRV) when the record  }
{  is not found.                                                             }
{                                                                            }
{  Returns : TRUE if no error.                                               }
{  InParms : pRec is the record object used to find and store the record.    }
{----------------------------------------------------------------------------}
function TDBCursor.GetFirstContains(pRec: PDBRec): boolean;
var
  pos       : longint;
  compStr   : PChar;
begin
  BuildKeyAsStr(pRec, mKeyStr, mKeyStrSize);
  pos := pRec^.mCurPosition;
  if GetGE(pRec) then
  begin
    GetMem(compStr, mKeyStrSize);
    if (compStr = nil) then
      dbErrorNum := dbErr_CannotAllocateRecBuf
    else
    begin
      BuildKeyAsStr(pRec, compStr, mKeyStrSize);
      if (StrLComp(mKeyStr, compStr, StrLen(mKeyStr)) <> 0) then
      begin
        TDBCursor.GotoPosition(pRec, pos);
        dbErrorNum := B_KEY_VALUE_NOT_FOUND + MOD_BTRV;
      end;
      MSFreeMem(compStr, mKeyStrSize);
    end;
  end;
  GetFirstContains:= dbErrorNum = 0;
end;

{----------------------------------------------------------------------------}
{  Get the next record whose current key string "contains" the key string    }
{  created from the key fields of pRec (i.e. Partial key).                   }
{                                                                            }
{  Assumptions                                                               }
{  dbErrorNum will equal (B_END_OF_FILE + MOD_BTRV) when a matching record   }
{  is not found.                                                             }
{                                                                            }
{  Returns : TRUE if no error.                                               }
{  InParms : pRec is the record object used to find and store the record.    }
{----------------------------------------------------------------------------}
function  TDBCursor.GetNextContains(pRec: PDBRec): boolean;
var
  pos       : longint;
  compStr   : PChar;
begin
  pos := pRec^.mCurPosition;
  if GetNext(pRec) then
  begin
    GetMem(compStr, mKeyStrSize);
    if (compStr = nil) then
      dbErrorNum := dbErr_CannotAllocateRecBuf
    else
    begin
      BuildKeyAsStr(pRec, compStr, mKeyStrSize);
      if (StrLComp(mKeyStr, compStr, StrLen(mKeyStr)) <> 0) then
      begin
        TDBCursor.GotoPosition(pRec, pos);
        dbErrorNum := B_END_OF_FILE + MOD_BTRV;
      end;
      MSFreeMem(compStr, mKeyStrSize);
    end;
  end;
  GetNextContains:= dbErrorNum = 0;
end;

{----------------------------------------------------------------------------}
{  Get the previous record whose current key string "contains" the           }
{  key string created from the key fields of pRec (i.e. Partial key).        }
{                                                                            }
{  Assumptions                                                               }
{  dbErrorNum will equal (B_END_OF_FILE + MOD_BTRV) when a matching record   }
{  is not found.                                                             }
{                                                                            }
{  Returns : TRUE if no error.                                               }
{  InParms : pRec is the record object used to find and store the record.    }
{----------------------------------------------------------------------------}
function  TDBCursor.GetPrevContains(pRec: PDBRec): boolean;
var
  pos       : longint;
  compStr   : PChar;
begin
  pos := pRec^.mCurPosition;
  if GetPrev(pRec) then
  begin
    GetMem(compStr, mKeyStrSize);
    if (compStr = nil) then
      dbErrorNum := dbErr_CannotAllocateRecBuf
    else
    begin
      BuildKeyAsStr(pRec, compStr, mKeyStrSize);
      if (StrLComp(mKeyStr, compStr, StrLen(mKeyStr)) <> 0) then
      begin
        TDBCursor.GotoPosition(pRec, pos);
        dbErrorNum := B_END_OF_FILE + MOD_BTRV;
      end;
      MSFreeMem(compStr, mKeyStrSize);
    end;
  end;
  GetPrevContains:= dbErrorNum = 0;
end;

{----------------------------------------------------------------------------}
{  Insert the record in the database file.                                   }
{                                                                            }
{  Returns : TRUE if no error.                                               }
{  InParms : pRec is the record object used to insert the record.            }
{----------------------------------------------------------------------------}
function TDBCursor.InsertRec(pRec: PDBRec): boolean;
var
  dLen  : word;
  k     : integer;
  tSeq  : TSeqNum;
begin
  if CheckRec(pRec) then
  begin
    {- clear current sequence number so it will get a new one on insert}
    if pRec^.mDesc^.mFileInfo.autoSeq then
    begin
      tSeq := pRec^.mCurSeq^;
      pRec^.mCurSeq^ := 0;
    end;
    pRec^.SetFlag(db_ArchivePos, TRUE);

    dLen := pRec^.mBufSize;
    dbErrorNum := BTRV(B_INSERT, mPosBlk, pRec^.mBuffer^,
                       dLen, mKeyBuf, mCurKey-1);
    if dbErrorNum <> 0 then
      dbErrorNum := dbErrorNum + MOD_BTRV;

    {- after insert, mBuffer^ gets updated with any new autoInc keys }

    {- restore sequence number if there is an error }
    if (dbErrorNum <> 0) and pRec^.mDesc^.mFileInfo.autoSeq then
      pRec^.mCurSeq^ := tSeq;  {- restore original seq number }
  end;
  InsertRec := dbErrorNum = 0;
end;

{----------------------------------------------------------------------------}
{  Update the current record in the file.                                    }
{                                                                            }
{  Returns : TRUE if no error.                                               }
{  InParms : pRec is the record object used to update the record.            }
{----------------------------------------------------------------------------}
function TDBCursor.UpdateRec(pRec: PDBRec): boolean;
var
  dLen  : word;
  k     : integer;
  tSeq  : TSeqNum;
begin
  if CheckRec(pRec) then
  begin
    {- do NOT clear sequence number }
    pRec^.SetFlag(db_ArchivePos, TRUE);
    dLen := pRec^.mBufSize;
    dbErrorNum := BTRV(B_UPDATE, mPosBlk, pRec^.mBuffer^,
                       dLen, mKeyBuf, mCurKey-1);
    if dbErrorNum <> 0 then
      dbErrorNum := dbErrorNum + MOD_BTRV;
  end;
  UpdateRec := dbErrorNum = 0;
end;

{----------------------------------------------------------------------------}
{  Returns the number of records in the BTRIEVE file.                        }
{----------------------------------------------------------------------------}
function TDBCursor.NumRecords: longint;
var
  fBuf  : TBTFileInfo;
  dLen  : Word;
  keyBuf: TKeyBuf;
begin
  dLen := SizeOf(fBuf);
  dbErrorNum := BTRV(B_STAT, mPosBlk, fBuf, dlen, keyBuf, 0);
  if dbErrorNum = 0 then
    NumRecords := fBuf.fSpec.numRecords
  else
    NumRecords := 0;
end;

{----------------------------------------------------------------------------}
{  Returns the BTRIEVE file version.                                         }
{----------------------------------------------------------------------------}
function TDBCursor.FileVersion(vstr: PChar; maxLen: integer): PChar;
const
  hStr  : array[0..15] of char = '0123456789ABCDEF';
var
  fBuf  : TBTFileInfo;
  dLen  : word;
  keyBuf: TKeyBuf;
  b     : byte;
  ps    : array[0..3] of char;
begin
  dLen := SizeOf(fBuf);
  dbErrorNum := BTRV(B_STAT, mPosBlk, fBuf, dLen, keyBuf, -1);
  {- the version byte is at offset 05 of the extended stat buffer }
  if dbErrorNum = 0 then
  begin
    b := PDataArray(@fBuf)^[5];
    ps[0] := hStr[b shr 4];
    ps[1] := '.';
    ps[2] := hStr[b and $F];
    ps[3] := #0;
    StrLCopy(vstr, ps, maxLen);
  end
  else
    StrLCopy(vstr, 'x.x', maxLen);
  FileVersion := vstr;
end;

{----------------------------------------------------------------------------}
{  Return a pointer to the object's descriptor.                              }
{----------------------------------------------------------------------------}
function TDBCursor.Desc: PDBReadDesc;
begin
  dbErrorNum := 0;
  Desc := mDesc;
end;

{----------------------------------------------------------------------------}
{  Verifies that the descriptor of the record is the same as the             }
{  descriptor of the cursor object.                                          }
{                                                                            }
{  Assumptions                                                               }
{  There is no need to have records and cursors for the same BTRIEVE         }
{  file have different descriptor objects.                                   }
{                                                                            }
{  Errors returned                                                           }
{    dbErr_IncompatibleObjects                                               }
{                                                                            }
{  Returns : TRUE if pointers to descriptors are identical.                  }
{  InParms : pRec is the record object used to compare with the cursor.      }
{----------------------------------------------------------------------------}
function TDBCursor.CheckRec(pRec: PDBRec): boolean;
begin
  dbErrorNum := 0;
  if pRec^.mDesc <> mDesc then
    dbErrorNum := dbErr_IncompatibleObjects;
  CheckRec := dbErrorNum = 0;
end;

{----------------------------------------------------------------------------}
{  This function is called after get functions, it retrieves the             }
{  current record address.                                                   }
{                                                                            }
{  InParms : pRec is the record object used to check get operation.          }
{----------------------------------------------------------------------------}
procedure TDBCursor.CheckGet(pRec: PDBRec);
begin
  if dbErrorNum = 0 then
    pRec^.mCurPosition := GetPosition;
end;

{----------------------------------------------------------------------------}
{  Build a key from the current record using the current key definition.     }
{                                                                            }
{  InParms : pRec is the record object used to build the key value.          }
{----------------------------------------------------------------------------}
procedure TDBCursor.BuildKey(pRec: PDBRec);
var
  k      : integer;
  segFld : integer;
  c      : integer;
begin
  c := 0;
  FillChar(mKeyBuf, SizeOf(mKeyBuf), dbEmptyVal);
  with mDesc^.mKeyList[mCurKey] do
  begin
    for k := 1 to numSegs do
    begin
      segFld := segList[k].fldNum;
      if segList[k].descend and pRec^.FieldIsEmpty(segFld) then
        FillChar(mKeyBuf[c], mDesc^.mFieldList[segFld].fldSize, $FF)
      else
        Move(pRec^.mBuffer^[mDesc^.mFieldList[segFld].fldPos],
             mKeyBuf[c],
             mDesc^.mFieldList[segFld].fldSize);
      Inc(c, mDesc^.mFieldList[segFld].fldSize);
    end;
  end;
end;

{----------------------------------------------------------------------------}
{  Build a key string from the current record using the current key          }
{  definition.                                                               }
{                                                                            }
{  InParms : pRec is the record object used to build the key string.         }
{  OutParms: aKeyStr is the key string being built.                          }
{            aKeyStrSize is the maximum size of the key string.              }
{----------------------------------------------------------------------------}
procedure TDBCursor.BuildKeyAsStr(pRec: PDBRec; aKeyStr: PChar; aKeyStrSize: word);
var
  k      : integer;
  segFld : integer;
  fSize  : word;
  fStr   : PChar;
  pstr   : PChar;
begin
  StrCopy(aKeyStr, '');
  with mDesc^.mKeyList[mCurKey] do
  begin
    for k := 1 to numSegs do
    begin
      segFld := segList[k].fldNum;
      fSize:= mDesc^.FieldSizeForStr(segFld);
      GetMem(fStr, fSize);
      if (fStr = nil) then
        dbErrorNum := dbErr_CannotAllocateRecBuf
      else
      begin
        pRec^.GetFieldAsStr(segFld, fStr, fSize-1);
        if (mDesc^.mFieldList[segFld].fldType = dbSeqRef) and (fStr[0] = '0') then
          fStr[0] := #0;

        {- force left padding for numeric key segments so partial key searches
           work properly }
        if (mDesc^.mFieldList[segFld].fldType in
              [dbInteger, dbLongInt, dbWord, dbByte, dbShortInt,
               dbSingle,dbDouble,dbSeqRef,dbAutoInc]) then
        begin
          GetMem(pstr, fSize);
          LeftPad(pstr, fstr, ' ', fSize - 1);
          StrCopy(fstr, pstr);
          FreeMem(pstr, fSize);
        end
        else
          Pad(fStr, fStr, #32, fSize-1);

        StrLCat(aKeyStr, fStr, aKeyStrSize-1);
        MSFreeMem(fStr, fSize);
      end;
    end;
    TrimTrail(aKeyStr, aKeyStr, #32, aKeyStrSize-1);
  end;
end;

{----------------------------------------------------------------------------}
{  Checks for any delete associations and if no problems, delete the         }
{  current record.                                                           }
{                                                                            }
{  Assumptions                                                               }
{  The first association that matches the delete association filename        }
{  is used to create the DBAssocCursor, when needed.                         }
{                                                                            }
{  The application does not have an associated file open in exclusive        }
{  mode.                                                                     }
{                                                                            }
{  Delete reestablishes positioning to the NEXT logical record!  If          }
{  last record is deleted in the logical order, then current position        }
{  will be undefined.                                                        }
{                                                                            }
{  Errors returned                                                           }
{    dbErr_InvalidDelAssoc                                                   }
{    dbErr_CantDeleteRecord                                                  }
{                                                                            }
{  Returns : TRUE if no error.                                               }
{  InParms : pRec is the record object used to validate and find the         }
{            record to delete.                                               }
{----------------------------------------------------------------------------}
function TDBCursor.PDelete(pRec: PDBRec): boolean;
var
  i, j  : integer;
  flg   : boolean;
  assoc : PDBAssocFile;
  afile : PDBFile;
  fPos  : integer;
begin
  flg   := TRUE;
  with mDesc^ do
  begin
    for i := 1 to mDelAssocs do
    begin
      if mDelAssocList[i].bDownline then
      begin
        for j := 1 to mAssocs do
          if StrIComp(mDelAssocList[i].fName, mAssocList[j].fName) = 0 then
            break;

        assoc := New(PDBAssocFile, Init(pRec, j, dbOpenNormal));
        if assoc = nil then
        begin
          Self.dbErrorNum := dbLastOpenError;
          break;
        end;

        if mDelAssocList[i].delAction = DELETE_ASSOCIATED then
        begin
          while assoc^.dbc^.GetFirst(assoc^.dbr) and flg do
            flg := assoc^.dbc^.PDelete(assoc^.dbr);
        end
        else  { DISALLOW_DELETE }
        begin
          if assoc^.dbc^.GetFirst(assoc^.dbr) then
            flg := FALSE;
        end;
        if inTransaction then
          delColl^.Insert(assoc)
        else
          MSDisposeObj(assoc);
      end
      else
      begin
        if mDelAssocList[i].delAction = DISALLOW_DELETE then
        begin
          { table scan for SegNum in specified field name }
          afile := New(PDBFile, Init(mDelAssocList[i].fName, mFilePath, dbOpenNormal));
          if afile = nil then
          begin
            Self.dbErrorNum := dbLastOpenError;
            break;
          end;
          j := afile^.dbd^.FieldNumber(mDelAssocList[i].assocFldName);
          fPos := afile^.dbd^.mFieldList[j].fldPos;
          afile^.dbc^.GetFirst(afile^.dbr);
          repeat
            if pRec^.mCurSeq^ = PSeqNum(@afile^.dbr^.mBuffer^[fPos])^ then
            begin
              flg := FALSE;
              break;
            end;
          until not afile^.dbc^.GetNext(afile^.dbr);
          MSDisposeObj(afile);
        end
        else
        begin
          Self.dbErrorNum := dbErr_InvalidDelAssoc;
          break;
        end;
      end;
      if flg = FALSE then
        break;
    end;
  end;
  if Self.dbErrorNum = 0 then
  begin
    if flg then
      PDeleteRec(pRec)
    else
      dbErrorNum := dbErr_CantDeleteRecord;
  end;
  PDelete := dbErrorNum = 0;
end;

{----------------------------------------------------------------------------}
{  Positions the cursor and performs the delete operation.                   }
{                                                                            }
{  Assumptions                                                               }
{  Delete reestablishes positioning to the NEXT logical record!  If          }
{  last record is deleted in the logical order, then current position        }
{  will be undefined.                                                        }
{                                                                            }
{  Returns : TRUE if no error.                                               }
{  InParms : pRec is the record object used to find the record to delete.    }
{----------------------------------------------------------------------------}
function TDBCursor.PDeleteRec(pRec: PDBRec): boolean;
var
  dLen  : word;
begin
  dLen := pRec^.mBufSize;
  Move(pRec^.mCurPosition, pRec^.mTBuf^, SizeOf(pRec^.mCurPosition));
  dbErrorNum := BTRV(B_GET_DIRECT, mPosBlk, pRec^.mTBuf^,
                     dLen, mKeyBuf, mCurKey-1);
  if dbErrorNum = 0 then
  begin
    dLen := 0;
    dbErrorNum := BTRV(B_DELETE, mPosBlk, pRec^.mBuffer^,
                       dLen, mKeyBuf, mCurKey-1);
  end;
  if dbErrorNum <> 0 then
    dbErrorNum := dbErrorNum + MOD_BTRV;
  PDeleteRec := dbErrorNum = 0;
end;

{------------------------------[ TDBAssocCursor ]----------------------------}
{----------------------------------------------------------------------------}
{                                                                            }
{  Initialize and open a BTRIEVE file.  Build a filter buffer based          }
{  on the association to be used for subsequent get operations.  If          }
{  the constructor fails (returns nil), then the error code will be          }
{  in dbLastOpenError                                                        }
{                                                                            }
{  Assumptions                                                               }
{  If mUsingSeqNum is TRUE then mCurSeq is not nil.                          }
{  The extValList is valid for the association specified by assocNum.        }
{                                                                            }
{  Errors returned                                                           }
{    dbErr_MaxKeyLenExceeded                                                 }
{    dbErr_InvalidAssoc                                                      }
{                                                                            }
{  InParms : pDesc - a database description object                           }
{            openMode - BTRV file open mode                                  }
{----------------------------------------------------------------------------}
constructor TDBAssocCursor.Init(fromRec: PDBRec; assocNum: integer;
                     var extValList: TAssocParamList; openMode: integer);
var
  ai      : TAssocInfo;
  i, j, k : integer;
begin
  if not fromRec^.mDesc^.CheckAssocNum(assocNum) then
  begin
    dbLastOpenError := fromRec^.mDesc^.dbErrorNum;
    fromRec^.mDesc^.dbErrorNum := 0;
    Done;
    Fail;
  end;

  ai := fromRec^.mDesc^.mAssocList[assocNum];
  mDesc := New(PDBReadDesc, Init(ai.fName, fromRec^.mDesc^.mFilePath));
  if mDesc = nil then
  begin
    Done;
    Fail
  end
  else if not inherited Init(mDesc, openMode) then
  begin
    Done;
    Fail;
  end;

  FillChar(mFilterBuf, SizeOf(mFilterBuf), 0);
  mFilterLen := 0;

  mUsingSeqNum := ai.keyNum = 0;
  if mUsingSeqNum then
  begin
    mCurKey := mDesc^.mKeys + 1;
    mFilterLen := SizeOf(TSeqNum);
    if not fromRec^.GetField(ai.fldNum[1], @mFilterBuf, mFilterLen) then
    begin
      dbLastOpenError := fromRec^.dbErrorNum;
      fromRec^.dbErrorNum := 0;
    end;
  end
  else
  begin
    mCurKey := ai.keyNum;
    for k := 1 to ai.numFields do
    begin
      i := mDesc^.mFieldList[mDesc^.mKeyList[mCurKey].segList[k].fldNum].fldSize;
      if (i + mFilterLen) > SizeOf(mFilterBuf) then
      begin
        dbLastOpenError := dbErr_MaxKeyLenExceeded;
        break;
      end;
      case ai.fldNum[k] of
        -5:
          Move(ai.constFldValue, mFilterBuf[mFilterLen], i);
        -maxAssocFlds..-1:
          Move(extValList[-ai.fldNum[k]]^, mFilterBuf[mFilterLen], i);
        0:
          Move(fromRec^.mCurSeq^, mFilterBuf[mFilterLen], i);
        1..maxAssocFlds:
          begin
            if not fromRec^.GetField(ai.fldNum[k], @mFilterBuf[mFilterLen], i) then
            begin
              dbLastOpenError := fromRec^.dbErrorNum;
              fromRec^.dbErrorNum := 0;
              break;
            end;
          end;
        else
          begin
            dbLastOpenError := dbErr_InvalidAssoc;
            break;
          end;
      end;
      Inc(mFilterLen, i);
    end;
  end;
  if dbLastOpenError <> 0 then
  begin
    Done;
    Fail;
  end;
end;

{----------------------------------------------------------------------------}
{  Frees the allocated descriptor and calls the inherited method.            }
{----------------------------------------------------------------------------}
destructor TDBAssocCursor.Done;
begin
  MSDisposeObj(mDesc);
  inherited Done;
end;

{----------------------------------------------------------------------------}
{  This method is not allowed for this object.                               }
{                                                                            }
{  Errors returned                                                           }
{    dbErr_InvalidOperation                                                  }
{----------------------------------------------------------------------------}
function TDBAssocCursor.SetCurKeyNum(keyNum: integer): boolean;
begin
  dbErrorNum := dbErr_InvalidOperation;
  SetCurKeyNum := dbErrorNum = 0;
  Abstract;
end;

{----------------------------------------------------------------------------}
{  This method is not allowed for this object.                               }
{                                                                            }
{  Errors returned                                                           }
{    dbErr_InvalidOperation                                                  }
{----------------------------------------------------------------------------}
function TDBAssocCursor.SetSeqKeyNum: boolean;
begin
  dbErrorNum := dbErr_InvalidOperation;
  SetSeqKeyNum := dbErrorNum = 0;
  Abstract;
end;

{----------------------------------------------------------------------------}
{  Go to a specific record specified by pos(ition).  Generates an            }
{  error if the record does not match the filter.                            }
{                                                                            }
{  Assumptions                                                               }
{  Position must have been retrieved by GetPosition.                         }
{                                                                            }
{  Errors returned                                                           }
{    B_INVALID_RECORD_ADDRESS                                                }
{                                                                            }
{  Returns : TRUE if no error.                                               }
{  InParms : pRec is the object used to store the record at the position.    }
{            pos is the position to use to find the record.                  }
{----------------------------------------------------------------------------}
function TDBAssocCursor.GotoPosition(pRec: PDBRec; pos: longint): boolean;
var
  i    : integer;
begin
  if TDBCursor.GotoPosition(pRec, pos) then
    for i := 0 to mFilterLen-1 do
      if mFilterBuf[i] <> mKeyBuf[i] then
      begin
        dbErrorNum := B_INVALID_RECORD_ADDRESS + MOD_BTRV;
        break;
      end;
  GotoPosition := dbErrorNum = 0;
end;

{----------------------------------------------------------------------------}
{  Find record with specified sequence number.  Generates an error if        }
{  the record does not match the filter.                                     }
{                                                                            }
{  Errors returned                                                           }
{    B_KEY_VALUE_NOT_FOUND                                                   }
{                                                                            }
{  Returns : TRUE if no error.                                               }
{  InParms : pRec is the object used to store the record at the position.    }
{            seqNum is the sequence number to use to find the record.        }
{----------------------------------------------------------------------------}
function TDBAssocCursor.GetSeq(pRec: PDBRec; seqNum: TSeqNum): boolean;
var
  i    : integer;
  pos  : longint;
begin
  pos := pRec^.mCurPosition;
  if TDBCursor.GetSeq(pRec, seqNum) then
    for i := 0 to mFilterLen-1 do
      if mFilterBuf[i] <> mKeyBuf[i] then
      begin
        TDBCursor.GotoPosition(pRec, pos);
        dbErrorNum := B_KEY_VALUE_NOT_FOUND + MOD_BTRV;
        break;
      end;
  GetSeq := dbErrorNum = 0;
end;

{----------------------------------------------------------------------------}
{  Get a record whose current key value is equal to the key value            }
{  created from the key fields of pRec.  Generates an error if the           }
{  record does not match the filter.                                         }
{                                                                            }
{  Assumptions                                                               }
{  dbErrorNum will equal (4) when the record is not found.                   }
{                                                                            }
{  Errors returned                                                           }
{    B_KEY_VALUE_NOT_FOUND                                                   }
{                                                                            }
{  Returns : TRUE if no error.                                               }
{  InParms : pRec is the record object used to find and store the record.    }
{----------------------------------------------------------------------------}
function TDBAssocCursor.GetEQ(pRec: PDBRec): boolean;
var
  i    : integer;
  pos  : longint;
begin
  pos := pRec^.mCurPosition;
  if TDBCursor.GetEQ(pRec) then
    for i := 0 to mFilterLen-1 do
      if mFilterBuf[i] <> mKeyBuf[i] then
      begin
        TDBCursor.GotoPosition(pRec, pos);
        dbErrorNum := B_KEY_VALUE_NOT_FOUND + MOD_BTRV;
        break;
      end;
  GetEQ := dbErrorNum = 0;
end;

{----------------------------------------------------------------------------}
{  Get a record whose current key value is greater than or equal to          }
{  the key value created from the key fields of pRec.  Generates an          }
{  error if the record does not match the filter.                            }
{                                                                            }
{  Assumptions                                                               }
{  dbErrorNum will equal (4) when the record is not found.                   }
{                                                                            }
{  Errors returned                                                           }
{    B_KEY_VALUE_NOT_FOUND                                                   }
{                                                                            }
{  Returns : TRUE if no error.                                               }
{  InParms : pRec is the record object used to find and store the record.    }
{----------------------------------------------------------------------------}
function TDBAssocCursor.GetGE(pRec: PDBRec): boolean;
var
  i    : integer;
  pos  : longint;
begin
  pos := pRec^.mCurPosition;
  if TDBCursor.GetGE(pRec) then
    for i := 0 to mFilterLen-1 do
      if mFilterBuf[i] <> mKeyBuf[i] then
      begin
        TDBCursor.GotoPosition(pRec, pos);
        dbErrorNum := B_KEY_VALUE_NOT_FOUND + MOD_BTRV;
        break;
      end;
  GetGE := dbErrorNum = 0;
end;

{----------------------------------------------------------------------------}
{  Go to first record for the current key that mathes the filter buffer.     }
{                                                                            }
{  Errors returned                                                           }
{    B_END_OF_FILE                                                           }
{                                                                            }
{  Returns : TRUE if no error.                                               }
{  InParms : pRec is the record object used to store the record.             }
{----------------------------------------------------------------------------}
function TDBAssocCursor.GetFirst(pRec: PDBRec): boolean;
var
  i    : integer;
  dLen : word;
  pos  : longint;
begin
  if CheckRec(pRec) then
  begin
    pos := pRec^.mCurPosition;
    FillChar(mKeyBuf, SizeOf(mKeyBuf), 0);
    Move(mFilterBuf, mKeyBuf, mFilterLen);
    dLen := pRec^.mBufSize;
    dbErrorNum := BTRV(B_GET_GE, mPosBlk, pRec^.mBuffer^,
                       dLen, mKeyBuf, mCurKey-1);
    if dbErrorNum = B_KEY_VALUE_NOT_FOUND then
      dbErrorNum := B_END_OF_FILE + MOD_BTRV
    else if dbErrorNum <> 0 then
      dbErrorNum := dbErrorNum + MOD_BTRV
    else
      for i := 0 to mFilterLen-1 do
        if mFilterBuf[i] <> mKeyBuf[i] then
        begin
          TDBCursor.GotoPosition(pRec, pos);
          dbErrorNum := B_END_OF_FILE + MOD_BTRV;
          break;
        end;
    CheckGet(pRec);
  end;
  GetFirst := dbErrorNum = 0;
end;

{----------------------------------------------------------------------------}
{  Go to the last record for the current key that matches the filter         }
{  buffer.                                                                   }
{                                                                            }
{  Returns : TRUE if no error.                                               }
{  InParms : pRec is the record object used to store the record.             }
{----------------------------------------------------------------------------}
function TDBAssocCursor.GetLast(pRec: PDBRec): boolean;
var
  pos  : longint;
begin
  if GetFirst(pRec) then
  begin
    pos := pRec^.mCurPosition;
    while GetNext(pRec) do
      pos := pRec^.mCurPosition;
    TDBCursor.GotoPosition(pRec, pos);
  end;
  GetLast := dbErrorNum = 0;
end;

{----------------------------------------------------------------------------}
{  Go to the next record for the current key that matches the filter         }
{  buffer.                                                                   }
{                                                                            }
{  Errors returned                                                           }
{    B_END_OF_FILE                                                           }
{                                                                            }
{  Returns : TRUE if no error.                                               }
{  InParms : pRec is the record object used to store the record.             }
{----------------------------------------------------------------------------}
function TDBAssocCursor.GetNext(pRec: PDBRec): boolean;
var
  i    : integer;
  pos  : longint;
begin
  pos := pRec^.mCurPosition;
  if TDBCursor.GetNext(pRec) then
    for i := 0 to mFilterLen-1 do
      if mFilterBuf[i] <> mKeyBuf[i] then
      begin
        TDBCursor.GotoPosition(pRec, pos);
        dbErrorNum := B_END_OF_FILE + MOD_BTRV;
        break;
      end;
  GetNext := dbErrorNum = 0;
end;

{----------------------------------------------------------------------------}
{  Go to the previous record for the current key that matches the            }
{  filter buffer.                                                            }
{                                                                            }
{  Errors returned                                                           }
{    B_END_OF_FILE                                                           }
{                                                                            }
{  Returns : TRUE if no error.                                               }
{  InParms : pRec is the record object used to store the record.             }
{----------------------------------------------------------------------------}
function TDBAssocCursor.GetPrev(pRec: PDBRec): boolean;
var
  i    : integer;
  pos  : longint;
begin
  pos := pRec^.mCurPosition;
  if TDBCursor.GetPrev(pRec) then
    for i := 0 to mFilterLen-1 do
      if mFilterBuf[i] <> mKeyBuf[i] then
      begin
        TDBCursor.GotoPosition(pRec, pos);
        dbErrorNum := B_END_OF_FILE + MOD_BTRV;
        break;
      end;
  GetPrev := dbErrorNum = 0;
end;

{----------------------------------------------------------------------------}
{  Returns the number of records that match the filter buffer.               }
{----------------------------------------------------------------------------}
function TDBAssocCursor.NumRecords: longint;
var
  li   : longint;
  pRec : PDBRec;
begin
  dbErrorNum := 0;
  li := 0;
  pRec := New(PDBRec, Init(@self));
  if pRec = nil then
    dbErrorNum := dbErr_CannotAllocateRecBuf
  else
  begin
    if GetFirst(pRec) then
    begin
      li := 1;
      while GetNext(pRec) do
        Inc(li);
    end;
    MSDisposeObj(pRec);
  end;
  NumRecords := li;
end;


{-----------------------------[ TDBCopyCursor ]------------------------------}
{----------------------------------------------------------------------------}
{  Insert the record in the database file without clearing any seqnum.       }
{                                                                            }
{  Returns : TRUE if no error.                                               }
{  InParms : pRec is the record object used to insert the record.            }
{----------------------------------------------------------------------------}
function TDBCopyCursor.InsertRec(pRec: PDBRec): boolean;
var
  dLen  : word;
begin
  if CheckRec(pRec) then
  begin
    dLen := pRec^.mBufSize;
    dbErrorNum := BTRV(B_INSERT, mPosBlk, pRec^.mBuffer^,
                       dLen, mKeyBuf, mCurKey-1);
    if dbErrorNum <> 0 then
      dbErrorNum := dbErrorNum + MOD_BTRV;
  end;
  InsertRec := dbErrorNum = 0;
end;

{----------------------------------------------------------------------------}
{  Update the current record in the file.                                    }
{                                                                            }
{  Returns : TRUE if no error.                                               }
{  InParms : pRec is the record object used to update the record.            }
{----------------------------------------------------------------------------}
function TDBCopyCursor.UpdateRec(pRec: PDBRec): boolean;
var
  dLen  : word;
  k     : integer;
  tSeq  : TSeqNum;
begin
  if CheckRec(pRec) then
  begin
    {- do NOT clear sequence number }
    dLen := pRec^.mBufSize;
    dbErrorNum := BTRV(B_UPDATE, mPosBlk, pRec^.mBuffer^,
                       dLen, mKeyBuf, mCurKey-1);
    if dbErrorNum <> 0 then
      dbErrorNum := dbErrorNum + MOD_BTRV;
  end;
  UpdateRec := dbErrorNum = 0;
end;


{-----------------------------[ TDBCopyRec ]---------------------------------}
{----------------------------------------------------------------------------}
{  Copy a record buffer from another record object fields. Record            }
{  structures need not be equivelent.                                        }
{                                                                            }
{  Returns : TRUE if no error.                                               }
{  InParms : pRec is the source of record buffer.                            }
{----------------------------------------------------------------------------}
function TDBCopyRec.CopyFields(pRec: PDBRec): boolean;
var
  j      : integer;
  fNum   : integer;
begin
  dbErrorNum := 0;
  if PDBRec(@self) = pRec then
    dbErrorNum := dbErr_CopyToFromSelf
  else
  begin
    for j := 1 to mDesc^.mFields do
    begin
      fNum := pRec^.mDesc^.FieldNumber(mDesc^.mFieldList[j].fldName);
      if fNum = 0 then
        Continue;
      if mDesc^.mFieldList[j].fldType <> pRec^.mDesc^.mFieldList[fNum].fldType then
        Continue;
      if (mDesc^.mFieldList[j].fldType <> dbString) and
         (mDesc^.mFieldList[j].fldType <> dbZString) and
         (mDesc^.mFieldList[j].fldType <> dbCode) and
         (mDesc^.mFieldList[j].fldType <> dbZCode) and
         (mDesc^.mFieldList[j].fldSize <> pRec^.mDesc^.mFieldList[fNum].fldSize) then
        Continue;

      PutField(j, @pRec^.mBuffer^[pRec^.mDesc^.mFieldList[fNum].fldPos]);
      if dbErrorNum <> 0 then
        break;
    end;

    if mDesc^.mFileInfo.autoSeq then
      if pRec^.mDesc^.mFileInfo.autoSeq then
        mCurSeq^ := pRec^.mCurSeq^
      else
        mCurSeq^ := 0;
  end;

  CopyFields := dbErrorNum = 0;
end;


{---------------------[ Misc interface routines ]----------------------------}

{----------------------------------------------------------------------------}
{  Determines if the file name is valid.                                     }
{                                                                            }
{  Returns : TRUE if the file name is valid.                                 }
{  InParms : aname is the file name to validate.                             }
{----------------------------------------------------------------------------}
function ValidFileName(aname: PChar): boolean;
const
  InvalidChars = ':\><*?%^()+=|][;"'',/';
var
  k   : integer;
  bad : boolean;
  c   : array[0..1] of char;
begin
  bad := StrLen(aname) = 0;

  k := 1;
  c[1] := #0;
  while not bad and (k <= StrLen(aname)) do
  begin
    c[0] := aname[k];
    bad := StrPos(InvalidChars, c) <> nil;
    Inc(k);
  end;

  ValidFileName := not bad;
end;

{----------------------------------------------------------------------------}
{  This function will remove a database from the disk.  The object           }
{  will also be disposed of and set to nil.  No subsequent operations        }
{  will be valid for the object.                                             }
{                                                                            }
{  Errors returned                                                           }
{    dbErr_CantDeleteDataFile                                                }
{    dbErr_CantDeleteDefFile                                                 }
{                                                                            }
{  Returns : error code if any.                                              }
{  InParms : aDBDesc is the file definition to use to delete the             }
{            BTRIEVE and database information files.                         }
{  OutParms: aDBDesc is set to nil;                                          }
{----------------------------------------------------------------------------}
function DBDeleteDatabase(var aDBDesc: PDBReadWriteDesc): integer;
var
  tstr  : array[0..150] of char;
  tstr2 : array[0..100] of char;
  info  : TFileInfo;
  fil   : file;
  ret   : integer;
  tmp   : integer;
begin
{$I-}
  if IOResult <> 0 then ;
  ret := 0;
  if aDBDesc = nil then
  begin
    DBDeleteDatabase := ret;
    exit;
  end;

  aDBDesc^.FileInfo(info);
  aDBDesc^.FilePath(tstr, 150);
  StrCat(tstr, info.fName);

  StrCopy(tstr2, tstr);
  StrCat(tstr2, fileDescExt);
  Assign(fil, tstr2);
  Erase(fil);
  tmp:= IOResult;
  if (tmp <> 0) and (tmp <> 2) then {- ignore file not found }
    ret := dbErr_CantDeleteDefFile;

  StrCopy(tstr2, tstr);
  StrCat(tstr2, datFileExt);
  Assign(fil, tstr2);
  Erase(fil);
  tmp:= IOResult;
  if (tmp <> 0) and (tmp <> 2) and (ret = 0) then
    ret := dbErr_CantDeleteDataFile;

  MSDisposeObj(aDBDesc);
  DBDeleteDatabase := ret;
{$I+}
end;

{----------------------------------------------------------------------------}
{  Returns the BTRIEVE version.                                              }
{                                                                            }
{  Returns : a pointer to vstr.                                              }
{  InParms : vstr is the string to place the version information into.       }
{            maxLen is the maximum string length of vstr.                    }
{----------------------------------------------------------------------------}
function BTVersion(var vMajor, vMinor: word; vstr: PChar; maxLen: integer): PChar;
var
  data  : array[0..15] of byte;
  posB  : TPosBlk;
  kbuf  : array[0..100] of char;
  tstr  : array[0..10] of char;
  dlen  : word;
  k     : integer;
begin
  dlen := SizeOf(data);
  FillChar(data, SizeOf(data), 0);
  FillChar(posB, SizeOf(posB), 0);
  FillChar(kBuf, SizeOf(kbuf), 0);
  k := BTRV(B_VERSION, posB, data, dlen, kbuf, 0);
  if k = 0 then
  begin
    vMajor := word(data[0]);
    vMinor := word(data[2]);
    StrCopy(kBuf, '');
    Str(Word(data[0]), tstr);
    StrCat(kBuf, tstr);
    StrCat(kBuf, '.');
    Str(Word(data[2]), tstr);
    StrCat(kBuf, tstr);
    tstr[0] := Char(data[4]);
    tstr[1] := #0;
    StrCat(kBuf, tstr);
  end
  else
  begin
    vMajor := 0;
    vMinor := 0;
    k := k + MOD_BTRV;
    StrCopy(kBuf, '<error ');
    Str(k, tstr);
    StrCat(kbuf, tstr);
    StrCat(kbuf, '>');
  end;
  StrLCopy(vstr, kBuf, maxLen);
  BTVersion := vstr;
end;

{----------------------------------------------------------------------------}
{  Start a BTRIEVE transaction.                                              }
{                                                                            }
{  Returns : a non-zero BTRIEVE error code if failed                         }
{----------------------------------------------------------------------------}
function DBBeginTransaction: integer;
var
  posBlk, d, dlen, k, kb: word;
begin
  posBlk := 0; d:= 0; dlen:= 0;
  k := 0; kb:= 0;
  k := BTRV(B_BEGIN_TRAN+CCURR_T_BIAS, posBlk, d, dLen, k, kb);
  if k > 0 then
    k := k + MOD_BTRV
  else
    inTransaction := TRUE;
  dbBeginTransaction := k;
end;

{----------------------------------------------------------------------------}
{  End a BTRIEVE transaction.                                                }
{                                                                            }
{  Returns : a non-zero BTRIEVE error code if failed                         }
{----------------------------------------------------------------------------}
function DBEndTransaction: integer;
var
  posBlk, d, dlen, k, kb: word;
begin
  posBlk := 0; d:= 0; dlen:= 0;
  k := 0; kb:= 0;
  k := BTRV(B_END_TRAN, posBlk, d, dLen, k, kb);
  if k > 0 then
    k := k + MOD_BTRV;
  inTransaction := FALSE;
  delColl^.FreeAll;
  dbEndTransaction := k;
end;

{----------------------------------------------------------------------------}
{  Abort a BTRIEVE transaction.                                              }
{                                                                            }
{  Returns : a non-zero BTRIEVE error code if failed                         }
{----------------------------------------------------------------------------}
function DBAbortTransaction: integer;
var
  posBlk, d, dlen, k, kb: word;
begin
  posBlk := 0; d:= 0; dlen:= 0;
  k := 0; kb:= 0;
  k := BTRV(B_ABORT_TRAN, posBlk, d, dLen, k, kb);
  if k > 0 then
    k := k + MOD_BTRV;
  inTransaction := FALSE;
  delColl^.FreeAll;
  dbAbortTransaction := k;
end;

{----------------------------------[ Exit Routine Stuff ]--------------------}

var
  saveExit  : Pointer;

{----------------------------------------------------------------------------}
{  Performs exit functions for the module and replaces the exitProc with the }
{  saved exit procedure.                                                     }
{----------------------------------------------------------------------------}
procedure DBExit; far;
var
  d, p, dl, k  : word;
{$IFDEF DBLibDEBUG}
  pstr         : PChar;
  tstr         : array[0..40] of char;
  dbc          : PDBCursor;
{$ENDIF}
begin
  MSDisposeObj(delColl);

{$IFDEF DBLibDEBUG}
  if openFiles^.count > 0 then     (* remove before general release *)
  begin
    GetMem(pstr, 201);
    StrCopy(pstr, 'File remaining open (');
    Str(openFiles^.count, tstr);
    StrLCat(pstr, tstr, 200);
    StrLCat(pstr, ')'#10#10, 200);
    for k:= 0 to openFiles^.count - 1 do
    begin
      dbc:= openFiles^.At(k);
      dbc^.mDesc^.FileName(tstr, sizeof(tstr)-1);
      StrLCat(pstr, tstr, 200);
      if k <> openFiles^.count - 1 then
        StrLCat(pstr, ', ', 200);
    end;
    MessageBox(0, pstr, 'DBLib Development Error', MB_APPLMODAL or MB_ICONINFORMATION);
    MSFreeMem(pstr, 201);
  end;                       (* remove before general release *)
  openFiles^.DeleteAll;
  MSDisposeObj(openFiles);
{$ENDIF}

  BTRV(B_STOP, p, d, dl, k, 0);  {- close btrieve. parameters are not used}
  exitProc := saveExit;  {- restore old exit procedure }
end;

var
  v1, v2 : word;
  vstr   : array[0..15] of char;
  perr   : PErrSubst;

BEGIN
{$IFDEF DBLibDEBUG}
  openFiles:= New(PCollection, Init(25, 25));
{$ENDIF}
  inTransaction := FALSE;
  delColl  := New(PCollection, Init(25, 25));
  saveExit := exitProc;
  exitProc := @DBExit;
  BTVersion(v1, v2, vstr, 15);
  if v1 < 6 then
  begin
    perr := New(PErrSubst, Init(vstr));
    ShowError(nil, IDS_OLD_BTRV_VERSION, perr, dbErr_OldBtrieveVersion,
              MOD_BTRV, 0);
    MSDisposeObj(perr);
    Halt;
  end;
  SetHandleCount(255);
END.
