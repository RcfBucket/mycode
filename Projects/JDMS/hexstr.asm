.MODEL large, Pascal

PUBLIC HexStr

.code

;========================================================== HexStr
;
; function HexStr(Var num; bytecount: Byte; pstr: PChar; maxLen: Integer): PChar;
;
; return a hex string representing num. byte count = SizeOf(num)
; string will be empty in case of an error.
;
;----------------------------------------------------------------------

HexStr          PROC FAR
; Parameters
num             equ     dword ptr ss:[bp+14]
byteCount       equ     byte ptr  ss:[bp+12]
pstr            equ     dword ptr ss:[bp+8]
maxLen          equ     word ptr  ss:[bp+6]
pSize           equ     12

        push    bp
        mov     bp, sp                  ; get pointer into stack
        push    ds

        mov     ax, word ptr ss:[bp+8]  ; check for nil
        or      ax, word ptr ss:[bp+10]
        jz      error

        cmp     maxLen,0
        je      error

        les     di,pstr                 ; get address of function result
        lds     si,num                  ; get number address
        mov     al,byteCount            ; how many bytes
        xor     ah,ah                   ; make a word
        mov     cx,ax                   ; keep track of bytes in CX
        add     si,ax                   ; start from most significant byte of number
        dec     si
        shl     ax,1                    ; how many hex digits? (2 per byte)
        mov     bx, 0

HexLoop:
        std                             ; scan number from MSB to LSB
        lodsb                           ; get next byte
        mov     ah,al                   ; save it
        shr     al,1                    ; extract high nibble
        shr     al,1
        shr     al,1
        shr     al,1
        add     al,90h                  ; special hex conversion sequence
        daa                             ; using add's and daa's
        adc     al,40h
        daa                             ; nibble now converted to ASCII
        cld                             ; store ASCII going up
        cmp     bx, maxLen
        jge     premature
        inc     bx
        stosb
        mov     al,ah                   ; repeat conversion for low nibble
        and     al,0fh
        add     al,90h
        daa
        adc     al,40h
        daa
        cmp     bx, maxLen
        jge     premature
        inc     bx
        stosb
        loop    HexLoop
premature:
        mov     al, 0
        stosb
error:
        mov     ax, word ptr pstr
        mov     dx, word ptr pstr+2

        pop     ds              ; restore DS
        mov     sp,bp           ; restore stack
        pop     bp
        ret     pSize
HexStr  ENDP

        END
