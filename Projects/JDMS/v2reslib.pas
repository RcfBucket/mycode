unit V2ResLib;

{- Results library }

{
This module is used to retrieve results stored in RESULTS.DAT for
v3.00 data conversion.

Each test procedure defined in a battery will have ONE result record that
will contain all test results for the procedure.

Since all test results are packed into one record for each procedure, this
library must be used to unpack the results into a standard table format
that is compatible with all of the other DBIF files.

RESULT.DAT format on disk ---

  SPECIMEN--COLLECTDATE--ISOLATE--BATTERY--PROCEDURE--[ RESULTS ]

This result record expands into many in memory records.
Format of memory records (using LoadResults) are as follows:
(results are loaded based on key of SPECIMEN-COLLECTDATE-ISOLATE-BATTERY)

  PROCEDURE--TESTID--TESTCAT--TESTDRUG--TESTNAME--RESULT
  ..
  ..

The result list in memory is sorted by PROCID-TESTID.
}

{- Error codes that cause program termination (BadError)

  err_ResLib +                (err_ResLib is defined in MSCAN [= 2400])

  1 - Cannot open BATDEF.DAT
  2 - Cannot allocate a record buffer.
  3 - Battery ID not found in battery definition file (BATDEF).
  4 - Cannot open TEST.DAT
  5 - Cannot open RESULT.DAT

}

INTERFACE

uses
  Objects,
  OWindows,
  CvtMisc,
  DB2DBIF,
  V3ResLib;

type
  PV2ResultList = ^TV2ResultList;
  TV2ResultList = object(TSortedCollection)
    function Compare(key1, key2: Pointer): integer; virtual;
  end;

  PV2Results = ^TV2Results;
  TV2Results = object(TObject)
    results       : PV2ResultList;
    {- the following fields are the keys used to load data from RESULT.DAT}
    spc           : array[0..8] of char;
    spcCD         : array[0..25] of char;
    iso           : array[0..3] of char;
    batID         : array[0..5] of char;
    resultsLoaded : boolean;
    resFieldSize  : word;
    resBuf        : PChar;    {- buffer to hold packed results }
    {}
    constructor Init;
    destructor Done; virtual;
    procedure LoadResults(batOrdRec: PDB2Record);
    function  XferResults(v3: PV3Results; ATestProc: PChar): boolean;
    function  NumResults: integer;
    function  ResultsExist(ATestProc: PChar): boolean;
    procedure NumTestProcs(ATestProcID: PChar; var first, count: integer);
    procedure FirstTestProc(AProc: PChar);
    function  NextTestProc(AProcID: PChar): boolean;
    function  GetResult(testProc, testID, res: PChar; maxChars: integer): boolean;
    function  FindResult(ATestProc, ATestID: PChar): integer;
    function  FindProc(ATestProc: PChar): integer;
    {}
    private
    procedure UnPackResults(AProc: PChar; APackedRes: PChar);
  end;


IMPLEMENTATION

uses
  WinTypes,
  Strings,
  DBIF,
  DBIFFlds,
  V2Trays,
  MScan;

const
  err_ResLib        = 2400;
  maximumStrResult  = 20;   {- maximum length of an unpacked result }
  greaterThan       = $80;
  lessThan          = $40;

type
  PV2ResRecObj = ^TV2ResRecObj;
  TV2ResRecObj = object(TObject)
    procID    : array[0..25] of char;
    testID    : array[0..25] of char;
    testName  : array[0..31] of char;
    result    : array[0..maximumStrResult] of char;
    testCat   : array[0..25] of char;
    testDrug  : array[0..25] of char;
    udFlag    : boolean;
    MaxResLen : integer;
    dupFlag   : boolean;
    {}
    constructor Init(AProc, ATestID, ATestName, ACat, ADrugID: PChar;
                     AFlag, AIsDup: boolean; AMaxResLen: integer);
    procedure ChangeResult(newResult: PChar);
  end;


procedure BadError(num: integer; abort: boolean);
{- show a critical error and possibly abort }
var
  errStr : array[0..32] of char;
begin
  Str(num + err_ResLib, errStr);
  StrCat(errStr, ' Error occurred!');
  MessageBox(0, errStr, 'DMS Error Monitor',  mb_TaskModal or mb_OK or mb_IconStop);
  if abort then
    Halt(1);
end;

procedure TrimLead(var tstr: string);
var
  k   : integer;
begin
  k:= 1;
  while (k <= Length(tstr)) and (tstr[k] = ' ') do
    Inc(k);
  tstr:= Copy(tstr, k, 255);
end;

procedure TrimTrail(var tstr: string);
{- return src with all trailing blanks removed }
var
  k   : integer;
begin
  k:= Length(tstr);
  while (k >= 1) and (tstr[k] = ' ') do
    Dec(k);
  tstr[0]:= Chr(k);
end;

procedure TrimTrailP(p: PChar);
{- return p with all trailing blanks removed }
var
  tstr  : string;
begin
  tstr:= StrPas(p);
  TrimTrail(tstr);
  StrPCopy(p, tstr);
end;

{----------------------------------------------------------------------}

constructor TV2ResRecObj.Init(AProc, ATestID, ATestName, ACat, ADrugID: PChar;
                              AFlag, AIsDup: boolean; AMaxResLen: integer);
begin
  inherited Init;

  maxResLen:= AMaxResLen;
  udFlag := AFlag;
  dupFlag:= aIsDup;
  StrCopy(result, '');      {- result is initially blank }
  StrCopy(testID, ATestID);
  StrCopy(testName, ATestName);
  StrCopy(testCat, ACat);
  StrCopy(testDrug, ADrugID);
  StrCopy(procID, AProc);
end;

procedure TV2ResRecObj.ChangeResult(newResult: PChar);
{- change the result value }
begin
  StrLCopy(result, newResult, maximumStrResult);
end;


{------------------------------------------------------[ TV2ResultList ]--}

function TV2ResultList.Compare(key1, key2: Pointer): integer;
{- sort results list by : PROCID-TESTID }
var
  k1, k2  : array[0..80] of char;
begin
  StrCopy(k1, PV2ResRecObj(key1)^.procID);
  StrCat(k1, PV2ResRecObj(key1)^.testID);

  StrCopy(k2, PV2ResRecObj(key2)^.procID);
  StrCat(k2, PV2ResRecObj(key2)^.testID);

  Compare:= StrComp(k1, k2);
end;

{-----------------------------------------------------------[ TV2Results ]--}

constructor TV2Results.Init;
var
  fh    : DBIFFileHandle;
begin
  inherited Init;
  results:= New(PV2ResultList, Init(60, 60));
  results^.duplicates:= True;

  resultsLoaded:= False;

  ChDir(dbifDir);
  fh:= DBIFOpenFile('RESULT', DBIFShared);
  if DBIFGetErrorCode <> 0 then BadError(5, True);
  resFieldSize:= DBIFGetFieldSize(fh, DBIFFldResult);
  DBIFCloseFile(fh);
  GetMem(resBuf, resFieldSize);
  ChDir(workDir);
end;

destructor TV2Results.Done;
begin
  MSFreeMem(resBuf, resFieldSize);
  Dispose(results, Done);
  inherited Done;
end;

procedure TV2Results.LoadResults(batOrdRec: PDB2Record);

  function Matches(rh: DBIFRecordHandle; fld: PChar; fldNum: integer): boolean;
  {- test value in a DBIF record with a value in a PDB2Field. Return True if
    values are equal. rh specifies a DBIF record handle, fld is a value
    to test against and fldNum is the field in rh to test against fld }
  var
    tstr  : string[132];
  begin
    tstr := StrPas(fld);
    Matches:= (tstr = DBIFGetFieldVal(rh, fldNum));
  end;

var
  fld       : PDB2Field;
  batDefFH  : DBIFFileHandle;
  batDefRH  : DBIFRecordHandle;
  tstDefFH  : DBIFFileHandle;
  tstDefRH  : DBIFRecordHandle;
  resFH     : DBIFFileHandle;
  resRH     : DBIFRecordHandle;
  tstr      : string;
  isOK      : boolean;
  k, c      : integer;
  hour      : PWaitCursor;
  exists    : boolean;
  pTestID   : array[0..25] of char;
  pDrug     : array[0..25] of char;
  pProc     : array[0..25] of char;
  pCat      : array[0..25] of char;
  pTestName : array[0..31] of char;
  flag      : boolean;
  resultLen : integer;
  dupped    : boolean;

  function IsDup(item: PV2ResRecObj): boolean; far;
  begin
    IsDup:= (StrComp(item^.testDrug, pDrug) = 0) and
            (StrComp(item^.testCat, pCat) = 0) and
            (StrComp(item^.procID, pProc) = 0);
  end;

begin
  results^.FreeAll; {- clear any pending results in memory }

  if batOrdRec = nil then
    Exit;

  hour:= New(PWaitCursor, Init);
  ChDir(dbifDir);

                  {- assign battery order key values }
  fld:= batOrdRec^.GetField(DbifFldBatteryOrdSpecimenID);
  StrCopy(spc, fld^.value);
  fld:= batOrdRec^.GetField(DbifFldBatteryOrdCollectDate);
  StrPCopy(spcCD, fld^.orgValue);
  fld:= batOrdRec^.GetField(DbifFldBatteryOrdIsolateID);
  StrCopy(iso, fld^.value);
  fld:= batOrdRec^.GetField(DbifFldBatteryOrdDefID);
  StrCopy(batID, fld^.value);

  {- Open BATTERY definition file }
  batDefFH:= DBIFOpenFile('BATDEF', DBIFShared);
  if DBIFGetErrorCode <> 0 then BadError(1, True);
  batDefRH:= DBIFNewRecord(batDefFH);
  if DBIFGetErrorCode <> 0 then BadError(2, True);

  resFH:= DBIFOpenFile('RESULT', DBIFShared);
  if DBIFGetErrorCode <> 0 then BadError(5, True);
  resRH:= DBIFNewRecord(resFH);
  if DBIFGetErrorCode <> 0 then BadError(2, True);

  exists:= False;

  {- Open TEST definition file }
  tstDefFH:= DBIFOpenFile('TEST', DBIFShared);
  if DBIFGetErrorCode <> 0 then BadError(4, True);
  tstDefRH:= DBIFNewRecord(tstDefFH);
  if DBIFGetErrorCode <> 0 then BadError(2, True);

  {- find battery ID in battery definition file }
  tstr := StrPas(batID);
  DBIFPutFieldVal(batDefRH, DBIFFldBatteryDefID, tstr);
  DBIFSearchRecordGE(batDefRH);
  if DBIFGetErrorCode = 0 then begin
    {- if no definition exists for the battery then abort }
    if not Matches(batDefRH, batID, DBIFFldBatteryDefID) then begin
      BadError(3, True);
    end
    else
    begin
      {- at this point if exists is true I have found the first battery
        definition for the battery. Now load all TestID, TestProc,
          TestName into memory. if exists is false then the battery isnt
          defined so call baderror.
      }

      {- loop through the battery definition and load the PROCCODE and
        TESTID, then from then using the test id, load the TESTNAME and
        TESTCAT, TESTDRUG and RESULTLEN from test.dat.
      }
      isOK:= True;
      while isOK do begin
        isOK:= Matches(batDefRH, batID, DBIFFldBatteryDefID);
        if isOK then begin
          {- get testProc ID }
          tstr:= DBIFGetFieldVal(batDefRH, DBIFFldBatteryDefTestProc);
          StrPCopy(pProc, tstr);

          {- get test ID }
          tstr:= DBIFGetFieldVal(batDefRH, DBIFFldBatteryDefTestID);
          StrPCopy(pTestID, tstr);

          {- get test name (description) and category and drug id and result
            length for test }
          DBIFPutFieldVal(tstDefRH, DBIFFldTestID, tstr);
          DBIFGetRecord(tstDefRH);

          StrCopy(pTestName, ''); {- set defaults first }
          StrCopy(pCat, '');
          StrCopy(pDrug, '');
          resultLen:= 0;

          if DBIFGetErrorCode = 0 then begin  {- test found ! }
            {- Get Full test name }
            tstr:= DBIFGetFieldVal(tstDefRH, DBIFFldTestName);
            StrPCopy(pTestName, tstr);

            {- Get test category }
            tstr:= DBIFGetFieldVal(tstDefRH, DBIFFldTestCategory);
            StrPCopy(pCat, tstr);

            {- Get drug defined for test }
            tstr:= DBIFGetFieldVal(tstDefRH, DBIFFldTestDrug);
            StrPCopy(pDrug, tstr);

            {- Get test UD flag }
            tstr:= DBIFGetFieldVal(tstDefRH, DbifFldTestUDFlag);
            flag := (tstr = 'Y');

            {- Get length of result field defined for this test }
            tstr:= DBIFGetFieldVal(tstDefRH, DBIFFldTestResultLen);
            Val(tstr, k, c);
            if (c = 0) then
              resultLen:= k;
          end;

          {- see if the PROC/Drug/Category already exists }
          dupped:= results^.FirstThat(@IsDup) <> nil;

          if StrLen(pCat) = 0 then
          begin
            StrCopy(pCat, 'BIO');
            StrCopy(pDrug, '');
          end;

          {- add procedure/test information to result collection }
          results^.Insert(New(PV2ResRecObj, Init(
             pProc, pTestID, pTestName, pCat, pDrug, flag, dupped, resultLen)));
        end;
        if isOK then begin
          DBIFNextRecord(batDefRH);
          isok:= DBIFGetErrorCode = 0;
        end;
      end;
    end;
  end;

  {- Now that the entire battery definition is loaded into memory, see
    if any results exist in RESULT.DAT. if so, read each the result
    record for each procCode in the definition.
    Each result record read will have to be unpacked and each individual
    test must be put into the "in memory" list, matching it with it's
    appropriate test id (see unpackresult)
  }

  {- find all result records }
  tstr := StrPas(spc);
  DBIFPutFieldVal(resRH, DBIFFldResultSpecimenID, tstr);
  tstr := StrPas(spcCD);
  DBIFPutFieldVal(resRH, DBIFFldResultCollectDate, tstr);
  tstr := StrPas(iso);
  DBIFPutFieldVal(resRH, DBIFFldResultIsolateID, tstr);
  tstr := StrPas(batID);
  DBIFPutFieldVal(resRH, DBIFFldResultDefID, tstr);

  DBIFSearchRecordGE(resRH);
  if DBIFGetErrorCode = 0 then begin
    isOK:= True;
    while isOK and
      Matches(resRH, spc, DBIFFldResultSpecimenID) and
      Matches(resRH, spcCD, DBIFFldResultCollectDate) and
      Matches(resRH, iso, DBIFFldResultIsolateID) and
      Matches(resRH, batID, DBIFFldResultDefID) do
    begin
      DBIFGetField(resRH, DBIFFldResultTestProcID, pProc);
      DBIFGetField(resRH, DBIFFldResult, resBuf);
      UnPackResults(pProc, resBuf);
      DBIFNextRecord(resRH);
      isOK:= DBIFGetErrorCode = 0;
    end;
  end;

  DBIFCloseRecord(tstDefRH);
  DBIFCloseRecord(batDefRH);
  DBIFCloseRecord(resRH);
  DBIFCloseFile(tstDefFH);
  DBIFCloseFile(batDefFH);
  DBIFCloseFile(resFH);

  resultsLoaded:= True;

  ChDir(workDir);
  Dispose(hour, Done);
end;

function TV2Results.XferResults(v3: PV3Results; ATestProc: PChar): boolean;
var
  item         : PV2ResRecObj;
  i            : integer;
  p            : PChar;
  first, count : integer;
begin
  XferResults := TRUE;
  if (ATestProc = nil) or (StrLen(ATestProc) = 0) then
  begin
    count := NumResults;
    if count > 0 then
      first := 0
    else
      first := -1
  end
  else
    NumTestProcs(ATestProc, first, count);

  if first = -1 then
  begin
    XferResults := FALSE;
    LogStr('Cannot convert result: ');
    LogStr(' Spec: ');
    LogStr(spc);
    LogStr(' - ');
    LogStr(spcCD);
    LogStr(', Iso: ');
    LogStr(iso);
    LogStr(', BatID: ');
    LogStr(batID);
    if (ATestProc <> nil) and (StrLen(ATestProc) <> 0) then
    begin
      LogStr(', Proc: ');
      LogStr(ATestProc);
    end;
    LogLn;
    Exit;
  end;

  for i := first to (count + first - 1)  do
  begin
    item := results^.At(i);
    if item^.dupFlag then
      continue;

    if ((StrComp(item^.testCat, 'NCCLS') = 0) or
        (StrComp(item^.testCat, 'MIC') = 0)) and
       (not item^.udFlag) then
    begin
      {- Strip 0 or 1 from testID }
      p := StrEnd(item^.testID);
      Dec(p);
      if (p^ = '1') or (p^ = '0') then
        p^ := #0;
      TrimTrailP(item^.testID);
    end;

    {- check for POS ID BLac test. Since BL was removed from v3.0 PID panel this
       check had to be made to avoid an error }
    if (StrComp(item^.procID, 'PID') = 0) and (StrComp(item^.testID, '.BL') = 0) then
      continue;

    if not v3^.SetResult(item^.testID, item^.testCat, item^.result, FALSE) then
    begin
      LogStr('Cannot convert result. ');
      LogStr(' Spec: ');
      LogStr(spc);
      LogStr(' - ');
      LogStr(spcCD);
      LogStr(', Iso: ');
      LogStr(iso);
      LogStr(', BatID: ');
      LogStr(batID);
      LogStr(', Proc: ');
      LogStr(item^.procID);
      LogStr(', Test: ');
      LogStr(item^.testID);
      LogStr(', Cat: ');
      LogStr(item^.testCat);
      LogStr(', Result: ');
      LogStr(item^.result);
      LogLn;
      XferResults := FALSE;
    end;
  end;

  if not v3^.SaveResults then
  begin
    LogHex(v3^.dbErrorNum);
    LogStr(' - Cannot save results. ');
    LogStr(' Spec: ');
    LogStr(spc);
    LogStr(' - ');
    LogStr(spcCD);
    LogStr(', Iso: ');
    LogStr(iso);
    LogStr(', BatID: ');
    LogStr(batID);
    LogLn;
    XferResults := FALSE;
  end;
end;

procedure ByteToMIC(mic: PChar; bVal: byte; tObj: PTraysObject);
{- convert byte value to MIC string }
var
  pstr  : array[0..50] of char;
  tstr  : string[40];
begin
  StrCopy(mic, '');
  if bVal = $FF then EXIT;
  if bVal and greaterThan <> 0 then
    StrCat(mic, '>')
  else if bVal and LessThan <> 0 then
    StrCat(mic, '<')
  else
    StrCat(mic, ' ');
  {- get MIC index into mater dil array. Mask off greater/less than bits
    using $3F }
  tstr:= tobj^.gmda[bVal and $3F];
  TrimTrail(tstr);
  TrimLead(tstr);
  StrPCopy(pstr, tstr);
  StrCat(mic, pstr);
end;

Procedure NCTherapy(InterpCode: Integer; Var InterpText: String);
Begin
   Case InterpCode Of
        0   :   InterpText:= 'N/A';
        1   :   InterpText:= 'TFG';
        2   :   InterpText:= 'MS';
        3   :   InterpText:= 'S';
        4   :   InterpText:= 'R';
        5   :   InterpText:= 'N/R';
        6   :   InterpText:= '';
        7   :   InterpText:= 'BLac';
        8   :   InterpText:= 'I';
        9   :   InterpText:= '';
       10   :   InterpText:= '';
       11   :   InterpText:= '';
       12   :   InterpText:= '';
       13   :   InterpText:= 'IB';
       14   :   InterpText:= '';
       15   :   InterpText:= '';
        Else   InterpText:= '****';   { Impossible! }
   End; {CASE}
End;

procedure ByteToNCCLS(nccls: PChar; bVal: byte);
{- convert a byte value to a NCCLS value }
var
  tstr  : string[30];
begin
  NCTherapy(bVal, tstr);
  StrPCopy(nccls, tstr);
end;

function TV2Results.NumResults: integer;
begin
  NumResults:= results^.count;
end;

function TV2Results.ResultsExist(ATestProc: PChar): boolean;
{- return true if ANY results exist for the given testProc }
var
  res : PV2ResRecObj;
  k   : integer;
  found : boolean;
begin
  ResultsExist:= False;
  if numResults <= 0 then Exit;
  k:= 0;
  found:= False;
  while (k < numResults) and not found do begin
    res:= results^.At(k);
    if StrComp(res^.procID, ATestProc) = 0 then
      found:= StrLen(res^.result) > 0;
    Inc(k);
  end;
  ResultsExist:= found;
end;

procedure TV2Results.NumTestProcs(ATestProcID: PChar; var first, count: integer);
{- calculate the number of results associated with a test procedure. Return
  the index of the first occurance and the count. First will equal -1 if
  the test proc is not found }
var
  k   : integer;
  found : boolean;
  res : PV2ResRecObj;
begin
  count:= 0;
  first:= -1;
  if NumResults <= 0 then Exit;
  k:= 0;
  while k < NumResults do begin
    res:= results^.At(k);
    if StrComp(ATestProcID, res^.procID) = 0 then begin
      if first = -1 then
        first:= k;
      Inc(count);
    end;
    Inc(k);
  end;
end;

procedure TV2Results.FirstTestProc(AProc: PChar);
{- return the first test procedure code in the result block }
var
  p   : PV2ResRecObj;
begin
  StrCopy(AProc, '');
  if NumResults = 0 then Exit;
  p:= results^.At(0);
  StrCopy(AProc, p^.procId);
end;

function TV2Results.NextTestProc(AProcID: PChar): boolean;
{- return the next procedure code in result block. AProcID must contain
  the current procedure code (if AProcID = '' then the first procedure
  code will be returned). if no procedure codes exist after AProcID then
  FALSE will be returned. }
var
  k   : integer;
  p   : PV2ResRecObj;
  found : boolean;
begin
  NextTestProc:= False;
  if NumResults = 0 then Exit;
  k:= 0;
  found:= False;
  k:= FindProc(AProcID);  {- find current procedure in list }
  if k <> -1 then begin
    while (k < NumResults) and not found do begin
      p:= results^.At(k);
      found:= StrComp(p^.ProcID, AProcID) <> 0;
      Inc(k);
    end;
    if found then begin
      StrCopy(AProcID, p^.ProcID);
      NextTestProc:= True;
    end;
  end;
end;

function TV2Results.GetResult(testProc, testID: PChar; res: PChar; maxChars: integer): boolean;
{- return a result for the particular test ID }

  function IsMatch(item: PV2ResRecObj): boolean; Far;
  begin
    IsMatch:= (StrComp(item^.testID, testID) = 0) and
              (StrComp(item^.procID, testProc) = 0);
  end;
var
  found : PV2ResRecObj;
begin
  GetResult:= False;
  res[0]:= #0;
  found:= results^.FirstThat(@IsMatch);
  if found <> nil then begin
    if maxChars > maximumStrResult then
      maxChars:= maximumStrResult;
    StrMove(res, found^.result, maxChars);
    res[maxChars]:= #0;
    GetResult:= True;
  end;
end;

function TV2Results.FindResult(ATestProc, ATestID: PChar): integer;
{- return index of result matching atestId. Return -1 if error }

  function IsMatch(item: PV2ResRecObj): boolean; Far;
  begin
    IsMatch:= (StrComp(ATestID, item^.testID) = 0) AND
              (StrComp(item^.procID, AtestProc) = 0);
  end;

var
  res : PV2ResRecObj;
begin
  res:= results^.FirstThat(@IsMatch);
  if res <> nil then
    FindResult:= results^.IndexOf(res)
  else
    FindResult:= -1;
end;

function TV2Results.FindProc(ATestProc: PChar): integer;
{- return index of first result in specified procedure block.
  Return -1 if error }

  function Matches(item: PV2ResRecObj): boolean; Far;
  begin
    Matches:= (StrComp(item^.procID, ATestProc) = 0);
  end;

var
  res : PV2ResRecObj;
begin
  res:= results^.FirstThat(@Matches);
  if res <> nil then
    FindProc:= results^.IndexOf(res)
  else
    FindProc:= -1;
end;

procedure TV2Results.UnPackResults(AProc: PChar; APackedRes: PChar);
{- this routine is used internally only! The packed result field passed
  in will be unpacked and put into the result list in memory for the
  specified procedure }
var
  k     : integer;
  pstr    : PChar;
  pstr2   : PChar;
  finished  : boolean;
  p     : PV2ResRecObj;
  idx   : integer;
  num   : byte;
  tobj    : PTraysObject;
  tstr    : string[25];
begin
  k:= FindProc(AProc);
  if k <> -1 then begin
    tObj:= New(PTraysObject, Init);
    GetMem(pstr, 255);
    GetMem(pstr2, 255);
    finished:= False;
    idx:= 0;
    while (k < NumResults) and not finished do begin
      p:= results^.At(k);
      finished:= StrComp(p^.procid, AProc) <> 0;
      if not finished then begin
        FillChar(pstr[0], maximumStrResult, ' ');
        pstr[maximumStrResult]:= #0;
        Move(APackedRes[idx], pstr[0], p^.maxResLen);
        Inc(idx, p^.maxResLen);
        TrimTrailP(pstr);
        if StrComp(p^.testCat, 'MIC') = 0 then begin
          Move(pstr[0], num, 1);
          ByteToMIC(pstr, num, tObj);
        end
        else if StrComp(p^.testCat, 'NCCLS') = 0 then begin
          Move(pstr[0], num, 1);
          StrCopy(pstr, '');
          ByteToNCCLS(pstr, num);
        end;

        StrCopy(p^.result, pstr);
      end;
      Inc(k);
    end;
    Dispose(tobj, Done);
    MSFreeMem(pstr, 255);
    MSFreeMem(pstr2, 255);
  end;
end;


END.

