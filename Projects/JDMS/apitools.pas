unit APITools;

{- This unit provides some common API helper functions. }

INTERFACE

uses
  Strings,
  WinTypes,
  WinProcs;

function DoEvent(forHWnd: HWnd): boolean;
procedure HandleEvents(forHWnd: HWnd);
procedure SetLogicalTwips(dc: HDC; zoom: real);
procedure LTwipsPerPixel(dc: HDC; var LTpPX, LTpPY: integer);
function PointsToLTwips(aPt: integer): integer;

procedure FocusCtl(HWindow: HWnd; id: word);

procedure ErrorMsg(HWindow: HWnd; title, msg: PChar);
procedure FatalError(title, msg: PChar);
function  YesNoMsg(HWindow: HWnd; title, msg: PChar): boolean;
function  RetryCancelMsg(HWindow: HWnd; title, msg: PChar): boolean;
function  YesNoCancelMsg(HWindow: HWnd; title, msg: PChar): integer;
function  NoYesMsg(HWindow: HWnd; title, msg: PChar): boolean;
function  NoYesCancelMsg(HWindow: HWnd; title, msg: PChar): integer;
procedure InfoMsg(HWindow: HWnd; title, msg: PChar);
function OkCancelMsg(HWindow: HWnd; title, msg: PChar): boolean;

procedure MakeScreenFont(var lf: TLogFont; bold, fixedPoint: boolean);

function SR(idx: integer; pstr: PChar; maxLen: word): PChar;

IMPLEMENTATION

uses
  IniLib,
  DMSDebug;

function DoEvent(forHWnd: HWnd): boolean;
{-----------------------------------------------------------------------------}
{ Processes an events for a specified window                                  }
{                                                                             }
{ InParms : forHWnd - the window to process events for.                       }
{-----------------------------------------------------------------------------}
var
  msg  : TMsg;
begin
  if PeekMessage(msg, forHWnd, 0, 0, PM_REMOVE) then
  begin
    if not IsDialogMessage(forHWnd, msg) then
    begin
      TranslateMessage(msg);
      DispatchMessage(msg);
    end;
    DoEvent:= true;
  end
  else
    DoEvent:= false;
end;

{-----------------------------------------------------------------------------}
{ Processes all events for a specified window until the queue is purged.      }
{                                                                             }
{ InParms : forHWnd - the window to process events for.                       }
{-----------------------------------------------------------------------------}
procedure HandleEvents(forHWnd: HWnd);
begin
  while DoEvent(forHWnd) do ;
end;

{-----------------------------------------------------------------------------}
{ Convert the specified number of 'points' to logical twips. This function    }
{ uses the fact that there are 20 Ltwips per point in the ltwips mapping mode.}
{ There are (1440 LT/in.) and (72 pt/in.) so 1440 / 72 = 20 LT/pt             }
{                                                                             }
{ InParms : aPt - number of points to convert.                                }
{ Returns : The function returns the number of logical twips for the number   }
{           of points specified.                                              }
{-----------------------------------------------------------------------------}
function PointsToLTwips(aPt: integer): integer;
begin
  PointsToLTwips:= aPt * 20;
end;

{-----------------------------------------------------------------------------}
{ Return number of LTwips per pixel for the device. accounts for any zoom     }
{ factor.                                                                     }
{                                                                             }
{ InParms : dc    - the (valid) device context in which a logical twips       }
{                   mapping mode has been set.                                }
{ OutParms: LTpPX - logical twips per pixel (horizontal)                      }
{           LTpPY - logical twips per pixel (vertical)                        }
{-----------------------------------------------------------------------------}
procedure LTwipsPerPixel(dc: HDC; var LTpPX, LTpPY: integer);
var
  ext  : Longint;
begin
  ext:= GetWindowExt(dc);
  LTpPX:= Round((ext and $FFFF) / GetDeviceCaps(dc, LOGPIXELSX));
  LTpPY:= Round((ext shr 16) / GetDeviceCaps(dc, LOGPIXELSY));
end;

{-----------------------------------------------------------------------------}
{ Sets a logical twips mapping mode for a DC.                                 }
{                                                                             }
{ InParms : dc   - the (valid) device context in which to set the mapping mode}
{           zoom - the zoom factor (1.0 = 100%, 0.25 = 25%, 2.0 = 200% etc)   }
{ OutParms: none                                                              }
{-----------------------------------------------------------------------------}
procedure SetLogicalTwips(dc: HDC; zoom: Real);
var
  x  : integer;
  y  : integer;
begin
  x:= Trunc(1440.0 / zoom);
  y:= Trunc(1440.0 / zoom);
  SetMapMode(dc, MM_ANISOTROPIC);
  SetWindowExt(dc, x, y);
  SetViewportExt(dc, GetDeviceCaps(dc, LOGPIXELSX),
                     GetDeviceCaps(dc, LOGPIXELSY));
end;

function MsgPrim(HWindow: HWnd; title, msg: PChar; msgFlag: word): integer;
{- Message primitive routine. Saves and restores the mouse cursor }
var
  curs  : HCursor;
begin
  curs:= SetCursor(LoadCursor(0, IDC_ARROW));
  MsgPrim:= MessageBox(HWindow, msg, title, msgFlag);
  SetCursor(curs);
end;

{-----------------------------------------------------------------------------}
{  Display an information message box.                                        }
{                                                                             }
{  InParms : HWindow - an hwindow value for the parent window of the message  }
{                      box.                                                   }
{            title   - title of the information window                        }
{            msg     - the information text to display.                       }
{  OutParms: none                                                             }
{-----------------------------------------------------------------------------}
procedure InfoMsg(HWindow: HWnd; title, msg: PChar);
begin
  MsgPrim(HWindow, title, msg, MB_APPLMODAL or MB_ICONINFORMATION);
end;

{-----------------------------------------------------------------------------}
{  Display a message box and get a Yes/No answer from the user with the NO    }
{  button being the default (initial focus) button.                           }
{                                                                             }
{  Returns : True if Yes is selected, False if no is selected.                }
{  InParms : HWindow - an hwindow value for the parent window of the message  }
{                      box.                                                   }
{            title   - title of the information window                        }
{            msg     - the information text to display.                       }
{  OutParms: none                                                             }
{-----------------------------------------------------------------------------}
function NoYesMsg(HWindow: HWnd; title, msg: PChar): boolean;
begin
  NoYesMsg:= MsgPrim(HWindow, title, msg, MB_APPLMODAL or MB_ICONQUESTION or
                     MB_DEFBUTTON2 or MB_YESNO) = IDYES;
end;

function YesNoMsg(HWindow: HWnd; title, msg: PChar): boolean;
{-----------------------------------------------------------------------------}
{  Display a message box and get a Yes/No answer from the user with the YES   }
{  button being the default (initial focus) button.                           }
{                                                                             }
{  Returns : True if Yes is selected, False if no is selected.                }
{  InParms : HWindow - an hwindow value for the parent window of the message  }
{                      box.                                                   }
{            title   - title of the information window                        }
{            msg     - the information text to display.                       }
{  OutParms: none                                                             }
{-----------------------------------------------------------------------------}
begin
  YesNoMsg:= MsgPrim(HWindow, title, msg,
               MB_APPLMODAL or MB_ICONQUESTION or MB_YESNO) = IDYES;
end;

function RetryCancelMsg(HWindow: HWnd; title, msg: PChar): boolean;
begin
  RetryCancelMsg:= MsgPrim(HWindow, title, msg,
               MB_APPLMODAL or MB_ICONQUESTION or MB_RETRYCANCEL) = IDRETRY;
end;

function YesNoCancelMsg(HWindow: HWnd; title, msg: PChar): integer;
{-----------------------------------------------------------------------------}
{ Display a message box and get a Yes/No/Cancel answer from the user with the }
{ YES button being the default (initial focus) button.                        }
{                                                                             }
{ Returns : IDYES or IDNO or IDCANCEL depending on users selection.           }
{ InParms : HWindow - an hwindow value for the parent window of the message   }
{                     box.                                                    }
{           title   - title of the information window                         }
{           msg     - the information text to display.                        }
{-----------------------------------------------------------------------------}
begin
  YesNoCancelMsg:= MsgPrim(HWindow, title, msg,
                           MB_APPLMODAL or MB_ICONQUESTION or MB_YESNOCANCEL);
end;

{-----------------------------------------------------------------------------}
{ Display a message box and get a Yes/No/Cancel answer from the user with the }
{ NO button being the default (initial focus) button.                         }
{                                                                             }
{ Returns : IDYES or IDNO or IDCANCEL depending on users selection.           }
{ InParms : HWindow - an hwindow value for the parent window of the message   }
{                     box.                                                    }
{           title   - title of the information window                         }
{           msg     - the information text to display.                        }
{-----------------------------------------------------------------------------}
function NoYesCancelMsg(HWindow: HWnd; title, msg: PChar): integer;
begin
  NoYesCancelMsg:= MsgPrim(HWindow, title, msg,
            MB_APPLMODAL or MB_ICONQUESTION or MB_DEFBUTTON2 or MB_YESNOCANCEL);
end;

{-----------------------------------------------------------------------------}
{ Display a message box for a fatal error. This procedure uses the system     }
{ modal message box.                                                          }
{                                                                             }
{ InParms : title  - title of the information window                          }
{           msg    - the information text to display.                         }
{-----------------------------------------------------------------------------}
procedure FatalError(title, msg: PChar);
begin
  MsgPrim(0, title, msg, MB_SYSTEMMODAL);
end;

{-----------------------------------------------------------------------------}
{ Display a message box for an error message.                                 }
{                                                                             }
{ InParms : HWindow - an hwindow value for the parent window of the message   }
{                     box.                                                    }
{           title  - title of the information window                          }
{           msg    - the information text to display.                         }
{-----------------------------------------------------------------------------}
procedure ErrorMsg(HWindow: HWnd; title, msg: PChar);
begin
  MessageBeep(MB_ICONEXCLAMATION);
  MsgPrim(HWindow, title, msg, MB_APPLMODAL or MB_ICONEXCLAMATION);
end;

{-----------------------------------------------------------------------------}
{-----------------------------------------------------------------------------}
function OkCancelMsg(HWindow: HWnd; title, msg: PChar): boolean;
begin
  OkCancelMsg:= MsgPrim(HWindow, title, msg, MB_APPLMODAL or MB_OKCANCEL) = IDOK;
end;

{-----------------------------------------------------------------------------}
{ Set focus to a control in a dialog box. This procedure is preferred to      }
{ the SetFocus API call because it modifies the border around the default     }
{ button.                                                                     }
{ InParms : HWindow - The HWindow of the dialog that contains the control.    }
{           id      - The ID of the control to receive focus.                 }
{-----------------------------------------------------------------------------}
procedure FocusCtl(HWindow: HWnd; id: word);
begin
  PostMessage(HWindow, WM_NEXTDLGCTL, GetDlgItem(HWindow, id), 1);
end;

{$IFDEF KANJI}
{-----------------------------------------------------------------------------}
{ Create a logical (scaleable) font structure. This function will return an   }
{ ARIAL font in ENGLISH and the default Kanji Shift JIS font under JWindows.  }
{ This font can be used in grids or controls where it is desired to "unbold"  }
{ the font.                                                                   }
{ OutParms: lf  - The logical font structure. The lhHeight field will already }
{                 be set. Just pass the log font along to CreateFontIndirect  }
{           bold- If true, then a bold font will be returned, otherwise a     }
{                 thin font will be returned.
{-----------------------------------------------------------------------------}
procedure MakeScreenFont(var lf: TLogFont; bold, fixedPoint: boolean);
{- JWindows MakeScreenFont }
var
  dc : HDC;
  f  : HFont;
  k  : integer;
begin
  FillChar(lf, sizeof(lf), 0);

  if bold then
    lf.lfWeight:= FW_BOLD
  else
    lf.lfWeight:= FW_NORMAL;
  lf.lfPitchAndFamily:= FIXED_PITCH;
  lf.lfcharSet:= SHIFTJIS_CHARSET;

  {- get the face name windows assigns to the font }
  dc:= GetDC(0);
  k:= INIScreenPointSize;
  lf.lfHeight:= -MulDiv(k, GetDeviceCaps(dc, LOGPIXELSY), 72);
  f:= CreateFontIndirect(lf);
  SelectObject(dc, f);
  GetTextFace(dc, lf_FaceSize, lf.lfFaceName);
  ReleaseDC(0, dc);
  DeleteObject(f);
end;

{$ELSE}

procedure MakeScreenFont(var lf: TLogFont; bold, fixedPoint: boolean);
{- English MakeScreenFont }
{  in  : bold       - true if bold font desired, false for thin font }
{        fixedPoint - If true, a fixed point font will be returned }
{  out : lf         - log font structure for use by caller }
var
  k  : integer;
  dc : HDC;
  f  : HFont;
begin
  FillChar(lf, sizeof(lf), 0);
  lf.lfPitchAndFamily:= FF_SWISS;
  if bold then
    lf.lfWeight:= FW_BOLD
  else
    lf.lfWeight:= FW_NORMAL;
  if fixedPoint then
  begin
    lf.lfPitchAndFamily:= FF_MODERN;
    strcopy(lf.lfFaceName, 'Courier New');
  end
  else
    strcopy(lf.lfFaceName, 'Arial');

  {- get the face name windows assigns to the font }
  dc:= GetDC(0);
  k:= INIScreenPointSize;
  lf.lfHeight:= -MulDiv(k, GetDeviceCaps(dc, LOGPIXELSY), 72);
  f:= CreateFontIndirect(lf);
  SelectObject(dc, f);
  GetTextFace(dc, lf_FaceSize, lf.lfFaceName);
  ReleaseDC(0, dc);
  DeleteObject(f);
end;

{$ENDIF}

{-----------------------------------------------------------------------------}
{ Extract a string resource. Assumes a valid string index is passed.          }
{                                                                             }
{ InParms : idx    - The index of the desired string.                         }
{           pstr   - A buffer to hold the returned string.                    }
{           maxLen - The maximum number of characters to return               }
{ Returns : This function returns a pointer to the returned buffer (pstr)     }
{-----------------------------------------------------------------------------}
function SR(idx: integer; pstr: PChar; maxLen: word): PChar;
begin
  if LoadString(HInstance, idx, pstr, maxLen) = 0 then
    pstr^:= #0;
  SR:= pstr;
end;

END.

{- rcf }
