(****************************************************************************


randdata.inc

produced by Borland Resource Workshop


*****************************************************************************)

const

	DLG_MAIN	=	$7041;
	DLG_STATUS	=	$7042;

	ICON_1	=	$7041;

	IDC_PROCESS	=	100;

	IDC_SOURCE	=	201;
	IDC_SESSION	=	202;
	IDC_PATIENT	=	203;
	IDC_SPECIMEN	=	204;
	IDC_ISOLATE	=	205;
	IDC_DATEVAR	=	206;

	IDC_LBL1	=	501;
	IDC_LBL2	=	502;
	IDC_LBL3	=	503;
	IDC_LBL4	=	504;
	IDC_LBL5	=	505;
	IDC_LBL6	=	506;
