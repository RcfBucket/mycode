(****************************************************************************


uiidmic.inc

produced by Borland Resource Workshop


*****************************************************************************)

const

  DLG_IDMIC             = 8736;
  IDC_ISOLATE           = 101;
  IDC_SPECIMEN          = 102;
  IDC_PATIENT           = 103;
  IDC_RECALC            = 104;
  IDC_SESSION           = 105;
  IDC_EDITTEST          = 106;

  IDC_NONMICGRID        = 500;
  IDC_MICGRID           = 501;

  IDC_EDSPECID          = 200;
  IDC_EDCOLLECT         = 201;
  IDC_EDPATID           = 202;
  IDC_EDORDER           = 203;
  IDC_EDISO             = 204;
  IDC_EDORG             = 205;
  IDC_EDWARD            = 206;
  IDC_EDSOURCE          = 207;
  IDC_EDLAST            = 208;
  IDC_EDFIRST           = 209;

  IDC_LBLSPECID         = 300;
  IDC_LBLCOLLECT        = 301;
  IDC_LBLPATID          = 302;
  IDC_LBLISO            = 303;
  IDC_LBLORDER          = 304;
  IDC_LBLORG            = 305;
  IDC_LBLWARD           = 306;
  IDC_LBLSOURCE         = 307;
  IDC_LBLLAST           = 308;
  IDC_LBLFIRST          = 309;

  DLG_CLEAR             = 8737;
  IDC_CLEARALL          = 101;
  IDC_CLEARRESULTS      = 102;
  IDC_CLEARISO          = 103;
	IDS_NORESSELECTED	=	8736;
	IDS_DORECALC	=	8737;
	IDS_NOORG	=	8738;
	IDS_TEST	=	8739;
	IDS_RESULT	=	8740;
	IDS_DRUG	=	8741;
	IDS_MIC	=	8742;
	IDS_INTERPRETATION	=	8743;
	IDS_ISONOTFOUND	=	8744;
	IDS_CANTLODSPEC	=	8745;
	IDS_ISONOTINSESS	=	8746;
	IDS_ISOMOVED	=	8747;
	IDC_LBLFAM	=	310;
	IDC_ADDTEST	=	107;
	DIALOG_1	=	1;
	DLG_ADDDRUGS	=	8738;
	IDC_ADDDRUGLIST	=	101;
	IDS_ADDDESC	=	8748;
	IDS_ADDFAILED	=	8749;
	IDC_Family	=	108;
