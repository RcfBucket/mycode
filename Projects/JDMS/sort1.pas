{----------------------------------------------------------------------------}
{  Module Name  : Sort1.PAS                                                  }
{  Programmer   : Steven Mercier                                             }
{  Date Created : July, 21, 1998                                             }
{  Requirements : ..., S3.28 [ref. a]                                        }
{                                                                            }
{  Purpose - This unit provides a group 1 sort engine. It is used with the   }
{    following standard DMS reports: Specimen Log, Isolate Log, Short        }
{    Format Report, and the Long Format report.                              }
{                                                                            }
{  Assumptions -                                                             }
{                                                                            }
{  Referenced Documents                                                      }
{    a. S3.28 Design Document, Revision 3                                    }
{    b. Database Design Document, Revision 0                                 }
{    c. Software Specification for Nihongo Data Management System v4.00      }
{                                                                            }
{  Revision History - This project is under version control, use it to view  }
{    revision history.                                                       }
{                                                                            }
{----------------------------------------------------------------------------}

unit Sort1;

INTERFACE

uses
  DBLib,
  DBTypes,
  DBFile,
  OWindows,
  Sort_Dlg,
  Sort_Eng,
  Sort_Par;

type
  {--------------------------------------------------------------------------}
  { This object provides a sort engine for a specimen data file. See base
  { class description. Thic class defines a number of specimen keys and
  { methods to access them
  {--------------------------------------------------------------------------}
  PSpecimenSortEngine = ^SpecimenSortEngine;
  SpecimenSortEngine = object( SortEngine )

    pSpecimenRecord,
    pPatientRecord,
    pSourceRecord,
    pWardRecord: PDBRec; { pointers to external record objects }
    constructor Init( input_Parameters: SortEngineParameters );
    destructor Done;
    procedure Input( SeqNum: TSeqNum );
    function GetItem( KeyNumber: integer; pItem: Pointer;
      MaxSize: Word ): boolean; virtual;
    function DataTypeOf( KeyNumber: integer ): SEKeyType; virtual;

    private
    dbSpec,
    dbPat,
    srcFile,
    wardFile: PDBFile;
  end;

  {--------------------------------------------------------------------------}
  { This object provides names for sort keys in group 1.
  {--------------------------------------------------------------------------}
  PGroup1KeyInformation = ^TGroup1KeyInformation;
  TGroup1KeyInformation = object( TKeyInformation )
    function GetKeyName( KeyNumber: integer; KeyName: PChar; Size: integer ):
      PChar; virtual;
    function NumberOfKeys: integer; virtual;
  end;

procedure GetGroup1SortParameters( var Parameters: SortEngineParameters );
procedure SetGroup1SortParameters( NewParameters: SortEngineParameters );


IMPLEMENTATION


uses
  APITools,
  DBIds,
  DMString,
  Gather,
  IntlLib,
  INILib,
  ListLib,
  MScan,
  MPSSObjs,
  Objects,
  PrnObj,
  PrnPrev,
  ResLib,
  SpecMPSS,
  Status,
  Strings,
  StrSW,
  WinTypes,
  WinProcs;

{$I PATREPS.INC}



                        {----------------------}
                        {  Helper Functions    }
                        {----------------------}



{--------------------------------------------------------------------------}
{ This procedure handles assertions for this unit. If an assertion is
{ false, it halts the program. Otherwise, it does nothing.
{--------------------------------------------------------------------------}

procedure Assert( flag: boolean );
begin
  if not flag then
  begin
    MessageBox( 0, 'SORT1.PAS', 'Error', MB_OK );
    Halt;
  end;
end;



                        {----------------------}
                        {    KeyInformation    }
                        {----------------------}



type
  {--------------------------------------------------------------------------}
  { This type enumerates the set of keys that can be used with the specimen
  { sort engine. If this set is changed in any way, the code in this file
  { must be updated.
  {--------------------------------------------------------------------------}
  KeyNumbers =
  (
    KN_SPECIMEN_ID,
    KN_COLLECT_DATE,
    KN_PATIENT_ID,
    KN_SOURCE_ID,
    KN_WARD_ID
  );


const
  {--------------------------------------------------------------------------}
  { The following constants specifies the numner of keys in the set above.
  { These values must be updated if the set size changes.
  {--------------------------------------------------------------------------}
  NUMBER_OF_KEYS = 5;
  MAXIMUM_KEY_NUMBER = Ord( KN_WARD_ID );


{----------------------------------------------------------------------------}
{ This function gets the name of a key given its number.
{----------------------------------------------------------------------------}

function TGroup1KeyInformation.GetKeyName( KeyNumber: integer;
  KeyName: PChar; Size: integer ): PChar;
begin
  case KeyNumbers( KeyNumber ) of
    KN_COLLECT_DATE :
      GetKeyName := SR( IDS_COLLECT_DATE, KeyName, Size );
    KN_SPECIMEN_ID :
      GetKeyName := SR( IDS_SPECIMEN_ID, KeyName, Size );
    KN_PATIENT_ID :
      GetKeyName := SR( IDS_PATIENT_ID, KeyName, Size );
    KN_SOURCE_ID :
      GetKeyName := SR( IDS_SOURCE_ID, KeyName, Size );
    KN_WARD_ID :
      GetKeyName := SR( IDS_WARD_ID, KeyName, Size );
  else
    Assert( false );
  end;
end;



{----------------------------------------------------------------------------}
{ This function returns the number of keys in group 1.
{----------------------------------------------------------------------------}

function TGroup1KeyInformation.NumberOfKeys: integer;
begin
  NumberOfKeys := NUMBER_OF_KEYS;
end;



                        {----------------------}
                        {  SpecimenSortEngine  }
                        {----------------------}



{----------------------------------------------------------------------------}
{ This constructor initializes the base class (see SortEngine) and clears
{ the record pointers to prevent misuse.
{----------------------------------------------------------------------------}

constructor SpecimenSortEngine.Init( input_Parameters: SortEngineParameters );
const
  {--------------------------------------------------------------------------}
  { This constans sets the buffer size used by the sort engine. Looking at
  { ref. a, the longest expected field is FreeText at 121 bytes, thus, the
  { following buffer size should hold any data set.
  {--------------------------------------------------------------------------}
  BUFFER_SIZE = SE_MAXLEVELS * 121;

begin
  inherited Init( input_Parameters, BUFFER_SIZE );
  pSpecimenRecord := nil;
  pPatientRecord := nil;
  pSourceRecord := nil;
  pWardRecord := nil;

  dbSpec := New( PDBFile, Init( DBSpecFile, '', dbOpenNormal ) );
  Assert( dbSpec <> nil );
  dbPat := New( PDBFile, Init( DBPatFile, '', dbOpenNormal ) );
  Assert( dbPat <> nil );
  srcFile:= New( PDBFile, Init( DBSRCFile, '', dbopenNormal ) );
  Assert( srcFile <> nil );
  wardFile := New( PDBFile, Init( DBWARDFile, '', dbopenNormal ) );
  Assert( wardFile <> nil );
end;



{----------------------------------------------------------------------------}
{ This constructor initializes the base class (see SortEngine) and clears
{ the record pointers to prevent misuse.
{----------------------------------------------------------------------------}

destructor SpecimenSortEngine.Done;
begin
  inherited Done;
  Dispose( dbSpec, Done );
  Dispose( dbPat, Done );
  Dispose( srcFile, Done );
  Dispose( wardFile );
end;



{----------------------------------------------------------------------------}
{ This function indicates the data type for each defined key.
{----------------------------------------------------------------------------}

function SpecimenSortEngine.DataTypeOf( KeyNumber: integer ): SEKeyType;
begin
  case KeyNumbers( KeyNumber ) of
    KN_COLLECT_DATE :
      DataTypeOf := SEKT_INTL_DATE;
    KN_SPECIMEN_ID,
    KN_PATIENT_ID,
    KN_SOURCE_ID,
    KN_WARD_ID :
      DataTypeOf := SEKT_ZSTRING;
  else
    Assert( false );
  end;
end;



{----------------------------------------------------------------------------}
{ See base class description. This override sets up record pointers
{ needed by GetItem and uses the specimen record's sequence number for the
{ sort index.
{----------------------------------------------------------------------------}

procedure SpecimenSortEngine.Input( SeqNum: TSeqNum );

  {--------------------------------------------------------------------------}
  { This function accesses a record referenced in a specimen record.
  {--------------------------------------------------------------------------}
  function pRecord( pSpecimenRecord: PDBRec; FieldNum: integer;
    db: PDBFile ): PDBRec;
  var
    SeqNum: TSeqNum;
  begin
    pSpecimenRecord^.GetField( FieldNum, @SeqNum, sizeof( TSeqNum ) );
    if SeqNum <> 0 then
    begin
      db^.dbc^.GetSeq( db^.dbr, SeqNum );
      pRecord := db^.dbr;
    end
    else
      pRecord := nil;
  end;

begin

  dbSpec^.dbc^.GetSeq( dbSpec^.dbr, SeqNum );
  pSpecimenRecord := dbSpec^.dbr;
  pPatientRecord := pRecord( pSpecimenRecord, DBSpecPatRef, dbPat );
  pSourceRecord := pRecord( pSpecimenRecord, DBSpecSrc, srcFile );
  pWardRecord := pRecord( pSpecimenRecord, DBSpecWard, wardFile );

  inherited Input( SeqNum );

  pSpecimenRecord := nil;
  pPatientRecord := nil;
  pSourceRecord := nil;
  pWardRecord := nil;
end;



{----------------------------------------------------------------------------}
{ See base class description. This override provides access to key
{ fields associated with a specimen record.
{----------------------------------------------------------------------------}

function SpecimenSortEngine.GetItem( KeyNumber: integer; pItem: Pointer;
  MaxSize: Word ): boolean;
var
  SeqNum: TSeqNum;
begin

  { Assert preconditions }
  Assert( TypeOf( pSpecimenRecord^ ) = TypeOf( TDBRec ) );
  Assert( ( pPatientRecord = nil ) or
    ( TypeOf( pPatientRecord^ ) = TypeOf( TDBRec ) ) );
  Assert( ( pSourceRecord = nil ) or
    ( TypeOf( pSourceRecord^ ) = TypeOf( TDBRec ) ) );
  Assert( ( pWardRecord = nil ) or
    ( TypeOf( pWardRecord^ ) = TypeOf( TDBRec ) ) );
  Assert( MaxSize > 0 );

  { To simplify the case statements, set a default return value. }
  GetItem := true;

  { Get specified item, adjusting return value if necessary. }
  case KeyNumbers( KeyNumber ) of

    KN_COLLECT_DATE :
      GetItem := pSpecimenRecord^.GetField( DBSpecCollectDate, pItem, MaxSize );

    KN_SPECIMEN_ID :
      GetItem := pSpecimenRecord^.GetField( DBSpecSpecID, pItem, MaxSize );

    KN_PATIENT_ID :
      if pPatientRecord <> nil then
        GetItem := pPatientRecord^.GetField( DBPATPatID, pItem, MaxSize )
      else
        PChar( pItem )^ := #0;

    KN_SOURCE_ID :
      if pSourceRecord <> nil then
        GetItem := pSourceRecord^.GetField( DBSrcID, pItem, MaxSize )
      else
        PChar( pItem )^ := #0;

    KN_WARD_ID :
      if pWardRecord <> nil then
        GetItem := pWardRecord^.GetField( DBWardID, pItem, MaxSize )
      else
        PChar( pItem )^ := #0;
  else
    GetItem := false;
  end;

end;



                        {----------------------}
                        {      Unit Init       }
                        {----------------------}



const
  {--------------------------------------------------------------------------}
  { This constant defines the INI file section name for group 1 sort
  { parameters.
  {--------------------------------------------------------------------------}
  SectionName = 'Reports - G1 Sort Parameters';


var
  {--------------------------------------------------------------------------}
  { This variable is the default parameters. It's only accessed directly
  { during unit initialization and in the two procedures below.
  {--------------------------------------------------------------------------}
  MasterParameters: SortEngineParameters;



{----------------------------------------------------------------------------}
{ This function gets a copy of the master sort parameters for group 1
{ reports.
{----------------------------------------------------------------------------}

procedure GetGroup1SortParameters( var Parameters: SortEngineParameters );
begin
  Parameters := MasterParameters;
end;



{----------------------------------------------------------------------------}
{ This procedure updates the master paramerters for group 1 reports. This
{ procedure replaces the master with a new one and writes it back to the
{ INI file.
{----------------------------------------------------------------------------}

procedure SetGroup1SortParameters( NewParameters: SortEngineParameters );
begin
  MasterParameters := NewParameters;
  MasterParameters.StoreToINI( SectionName );
end;



{----------------------------------------------------------------------------}
{ Unit initialization. This block loads the sort parameters from the INI
{ file at program start up. Although the command handlers could load them on
{ demand, this will make the commands handlers more responsive, especially
{ when the INI file is not currently cached.
{----------------------------------------------------------------------------}

begin
  MasterParameters.LoadFromINI( SectionName, MAXIMUM_KEY_NUMBER );
end.