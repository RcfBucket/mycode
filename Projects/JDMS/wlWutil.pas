Unit WLWUtil;

{$DEFINE WACONNECT}

Interface

{$IFDEF WACONNECT }
Uses
  WinTypes,
  WinProcs,
  WObjects,
  Strings,
  WLWAGpib,
  WLDMStrg;

{$ELSE}
Uses
  WinTypes,
  WinProcs,
  WObjects,
  Strings,
  WLDMStrg;

{$ENDIF}

Type
    WaModes   = (ROM, LOADER, APP);
		FNameType = String[64];

Const
    wm_BlockCount = wm_User + 500;

    ROMWaClassic = 1;
    ROMWaXX			 = 2;
    ROMWaUnknown = -1;


Function SendAFile (LocalName, RemoteName: FNameType; Wind: HWnd): Boolean;
function FileExists(FileName: String): Boolean;
Function GetBufferSize (Var MaxXfer: Integer): Boolean;
Function GetWaMode (Wind: HWnd; Var Mode: WAModes): Boolean;
Function SetWaMode (Wind: HWnd; NewMode: WAModes): Boolean;
Procedure WDelay (Wind: HWnd; Time: Word);
Function GetNextLine (Var f: Text; SlotsPerTower: Integer;
                      Var LocalName, RemoteName: String): Boolean;
Procedure FatalWAError (Wind: HWnd);
Function GetWaTypeFromROM: Integer;

Implementation

Const
    MaxBytes = 4096;
    WaBase   = 6100;

Var
    Buffer: Array [1..MaxBytes] Of Byte;
    RomWaType: Integer;


Procedure FatalWAError (Wind: HWnd);
Var
    PStr, PStr2: Array [0..50] Of Char;

Begin
    GetStr (WaBase + 02, PStr, 50);     { Walkaway communications error }
    GetStr (WaBase + 01, PStr2, 50);    { Walkaway Loader }
    MessageBox (Wind, PStr, PStr2, mb_OK or mb_IconExclamation);
    Application^.MainWindow^.CloseWindow;
End;


Function GetBufferSize (Var MaxXfer: Integer): Boolean;
{---------------------------------------}
{ Get the buffer sizes from the W/A.	}
{---------------------------------------}
Var
{$IFDEF WACONNECT}
    GSizeCmd: PWaGSizeCmd;
{$ENDIF}
    rxSize, txSize: Integer;
    AOK: Boolean;

BEGIN
    AOK := False;
{$IFDEF WACONNECT}
    GSizeCmd := New (PWaGSizeCmd, Init (WAID));
    GSizeCmd^.MakeSendMessage;
    GSizeCmd^.SendWAMessage;
    GSizeCmd^.GetParams (rxSize, txSize);
    AOK := (GSizeCmd^.RecvMsg^.Status = 0);
    Dispose (GSizeCmd, Done);
{$ELSE}
    AOK := True;
    rxSize := MaxBytes;
    txSize := MaxBytes;
{$ENDIF}

    maxXfer := rxSize;
    IF txSize < rxSize THEN maxXfer := txSize;
    IF maxXfer > MaxBytes THEN maxXfer := MaxBytes;
    GetBufferSize := AOK;
END;

function FileExists(FileName: String): Boolean;
{ Boolean function that returns True if the file exists; otherwise,
   it returns False. Closes the file if it exists. }
var
  F: file;
begin
  {$I-}
  Assign(F, FileName);
  Reset(F);
  Close(F);
  {$I+}
  FileExists := (IOResult = 0) and (FileName <> '');
end;  { FileExists }

Function GetNextLine (Var f: Text; SlotsPerTower: Integer;
                      Var LocalName, RemoteName: String): Boolean;
Var
    FileLine: String;
    x       : Integer;

Begin
    GetNextLine := Not (Eof (f));
    If Not Eof (f) Then
    Begin
        Repeat
            ReadLn (f, FileLine);
        Until (Eof (f)) Or (FileLine[1] <> '*');
    End;
    x := Pos (',', FileLine);
    RemoteName := Copy (FileLine, 1, x-1);
    Delete (FileLine, 1, x);
    x := Pos (',', FileLine);
    If GetWaTypeFromROM = ROMWaClassic Then
        Delete (FileLine, x, Length (FileLine) - x + 1)
    Else                     	  { Assumes WAXX }
        Delete (FileLine, 1, x);
    LocalName := FileLine;
End;

Function SendAFile (LocalName, RemoteName: FNameType; Wind: HWnd): Boolean;
Var
    f: File;
    FSize: LongInt;
    ReadAddress: LongInt;
    NumToRead: Word;
    NumRead: Word;
    BlockSize: Integer;
    BlockCount: Word;
    SourceOpen: Boolean;
    WaFileOpen: Boolean;
    AOK: Boolean;
    TStr: String;
    CStr: Array [0..80] Of Char;
    PStr: Array [0..50] Of Char;

{$IFDEF WACONNECT}
    CreateCmd: PWACREATECmd;
    WriteCmd: PWAWRITECmd;
    CloseCmd: PWACLOSECmd;
{$ENDIF}

    Procedure CleanupAfterSend;
    Begin
        If SourceOpen Then
            Close (f);
{$IFDEF WACONNECT}
        If WaFileOpen Then
        Begin
            CloseCmd := New (PWACLOSECmd, Init (WAID));
            CloseCmd^.MakeSendMessage;
            CloseCmd^.SendWAMessage;
            Dispose (CloseCmd, Done);
        End;
{$ENDIF}
    End;

Begin
    SendAFile := True;       { Returns false only on serious errors,
                               e.g. WA communications errors }
    SourceOpen := False;
    WaFileOpen := False;
    BlockCount := 0;
    If Not FileExists (LocalName) Then
    Begin
        GetPStr (WaBase + 44, TStr);    { File not found: }
        TStr := TStr + LocalName;
        StrPCopy (CStr, TStr);
        GetStr (WaBase + 01, PStr, 50); { Walkaway Loader }
        MessageBox(GetFocus, CStr, PStr, mb_OK);
        SetFocus (Wind);
        Exit;
    End;

    If Not GetBufferSize (BlockSize) Then
    Begin
        FatalWAError (Wind);
        SendAFile := False;
        Exit;
    End;
    Assign (f, LocalName);
    Reset (f, 1);
    SourceOpen := True;

{$IFDEF WACONNECT}
    CreateCmd := New (PWaCreateCmd, Init (WAID));
    CreateCmd^.MakeSendMessage;
    CreateCmd^.SetParams (RemoteName);
    CreateCmd^.SendWAMessage;
    AOK := (CreateCmd^.RecvMsg^.Status = 0);
    Dispose (CreateCmd, Done);
    If Not AOK Then
    Begin
        FatalWAError (Wind);
        CleanupAfterSend;
        SendAFile := False;
        Exit;
    End;
    WaFileOpen := True;
{$ENDIF}

    FSize := FileSize (f);
    ReadAddress := 0;
    While (ReadAddress < FSize) Do
    Begin
        If (FSize - ReadAddress) > BlockSize Then
            NumToRead := BlockSize
        Else
            NumToRead := FSize - ReadAddress;

        BlockRead (f, Buffer, NumToRead, NumRead);
        If NumToRead <> NumRead Then
        Begin
            GetPStr (WaBase + 45, TStr);        { Error reading: }
            TStr := TStr + LocalName;
            StrPCopy (CStr, TStr);
            GetStr (WaBase + 01, PStr, 50);     { Walkaway Loader }
            MessageBox(GetFocus, CStr, PStr, mb_OK);
            CleanupAfterSend;
            Exit;
        End;

{$IFDEF WACONNECT}
        WriteCmd := New (PWAWRITECmd, Init (WAID));
        WriteCmd^.MakeSendMessage;
        WriteCmd^.SetParams (ReadAddress, NumRead, Addr (Buffer));
        WriteCmd^.SendWAMessage;
        AOK := (WriteCmd^.RecvMsg^.Status = 0);
        Dispose (WriteCmd, Done);
        If Not AOK Then
        Begin
            FatalWAError (Wind);
            CleanupAfterSend;
            SendAFile := False;
            Exit;
        End;
{$ENDIF}

        Inc (BlockCount);
        SendMessage (Wind, wm_BlockCount, BlockCount, 0);
        ReadAddress := ReadAddress + NumToRead;
    End;
    CleanupAfterSend;
End;

Function GetWaMode (Wind: HWnd; Var Mode: WAModes): Boolean;
Var
{$IFDEF WACONNECT}
    IdiCmd : PWAIDICmd;
    IdiData: EchoArray;
{$ENDIF}
    Len    : Integer;
    AOK    : Boolean;
    Index  : Integer;

Begin
{$IFDEF WACONNECT}
    GetWaMode := False;
    IdiCmd := New (PWAIDICmd, Init (WAID));
    IdiCmd^.MakeSendMessage;
    IdiCmd^.SendWAMessage;
    AOK := (IdiCmd^.RecvMsg^.Status = 0);
    IdiCmd^.GetParams (Len, IdiData);
    Dispose (IdiCmd, Done);

    If Not AOK Then Exit;
    GetWaMode := True;
    Mode := Rom;
    Index := 1;
    While Index < Len Do
    Begin
        If (IdiData[Index] = 1) And (Mode = Rom) Then
            Mode := Loader;
        If IdiData[Index] in [2..5] Then
            Mode := App;
        Index := Index + IdiData[Index+1] + 2;
    End;
{$ELSE}
    Mode := Loader;
    GetWaMode := True;
{$ENDIF}
End;


Function SetWaMode (Wind: HWnd; NewMode: WAModes): Boolean;
var
    CurrentMode: WAModes;
{$IFDEF WACONNECT}
    CafCmd     : PWACAFCmd;
    ResetCmd   : PWARESETCmd;
    BootCmd    : PWABOOTCmd;
    ExCmd      : PWAEXCmd;
{$ENDIF}
    ResetMode  : Byte;
    AOK        : Boolean;

Begin
{$IFDEF WACONNECT}
    SetWaMode := False;
    If Not GetWaMode (Wind, CurrentMode) Then Exit;

    If CurrentMode <> NewMode Then
    Begin
        If CurrentMode = App Then
        Begin
            CafCmd := New (PWACAFCmd, Init (WAID));
            CafCmd^.MakeSendMessage;
            CafCmd^.SendWAMessage;
            Dispose (CafCmd, Done);
        End;

        If CurrentMode = Rom Then
            ResetMode := 4    { Reset to ROM }
        Else
            ResetMode := 5;   { Reset to OS }
        ResetCmd := New (PWARESETCmd, Init (WAID));
        ResetCmd^.MakeSendMessage;
        ResetCmd^.SetParams (ResetMode);
        ResetCmd^.SendWaMessage;
        Dispose (ResetCmd, Done);
        WDelay (Wind, 25000);          { Wait for WA to reset }

        If Not GetWaMode (Wind, CurrentMode) Then Exit;
        If CurrentMode <> NewMode Then
        Begin
            If (CurrentMode = Rom) And (NewMode = Loader) Then
            Begin
                BootCmd := New (PWABOOTCmd, Init (WAID));
                BootCmd^.MakeSendMessage;
                BootCmd^.SendWAMessage;
                AOK := (BootCmd^.RecvMsg^.Status = 0);
                Dispose (BootCmd, Done);
                If Not AOK Then Exit;

                ExCmd := New (PWAEXCmd, Init (WAID));
                ExCmd^.MakeSendMessage;
                ExCmd^.SendWAMessage;
                AOK := (ExCmd^.RecvMsg^.Status = 0);
                Dispose (ExCmd, Done);
                If Not AOK Then Exit;
                WDelay (Wind, 5000);
            End
            Else
                Exit;           { Didn't get requested mode }
        End;
    End;
{$ENDIF}
    SetWaMode := True;
End;


Procedure WDelay (Wind: HWnd; Time: Word);
var
    M      : TMsg;
    MsgRcvd: Boolean;

Begin
    SetTimer (Wind, 0, Time, Nil);
    MsgRcvd := False;
    Repeat
        GetMessage (M, Wind, 0, 0);
        If M.Message = wm_Timer Then
            MsgRcvd := True
        Else
        Begin
            TranslateMessage (M);
            DispatchMessage (M);
        End;
    Until MsgRcvd;
    KillTimer (Wind, 0);
End;

Function GetWaTypeFromROM: Integer;
Var
		Index  : Integer;
    AllDone: Boolean;
    IdiCmd : PWAIDICmd;
    IdiData: EchoArray;
    Len    : Integer;
    AOK    : Boolean;

Begin
		If ROMWaType = ROMWaUnknown Then
    Begin
		  IdiCmd := New (PWAIDICmd, Init (WAID));
    	IdiCmd^.MakeSendMessage;
		  IdiCmd^.SendWAMessage;
    	AOK := (IdiCmd^.RecvMsg^.Status = 0);
		  IdiCmd^.GetParams (Len, IdiData);
    	Dispose (IdiCmd, Done);

		  If Not AOK Then Exit;

	    Index := 1;
      AllDone := False;
  	  While (Not AllDone) And (Index < Len) Do
    	Begin
      	  If (IdiData[Index] = 0) Then
          Begin
        	    ROMWaType := ROMWaClassic;
              AllDone := True;
	        End
          Else If (IdiData[Index] = 6) Then
          Begin
          		ROMWaType := ROMWaXX;
              AllDone := True;
          End;
    	    Index := Index + IdiData[Index+1] + 2;
    	End;
    End;
    GetWaTypeFromROM := ROMWaType;
End;


Begin
		ROMWaType := ROMWaUnknown;
End.

