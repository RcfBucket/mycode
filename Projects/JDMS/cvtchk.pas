unit CvtChk;

INTERFACE


procedure CheckDMSFiles;


IMPLEMENTATION

uses
  CvtMisc,
  ApiTools,
  Strings,
  MScan,
  DMString,
  DMSErr,
  DBIDs,
  DBLib,
  DBTypes,
  DBFile;


procedure BadError(num, loc: integer; abort: boolean);
{- show a critical error and possibly abort }
var
  pstr : array[0..100] of char;
  p2   : array[0..20] of char;
begin
  Str(num, p2);
  StrCopy(pstr, 'Database is incomplete. Error: ');
  StrCat(pstr, p2);
  Str(loc, p2);
  StrCat(pstr, ' Loc: ');
  StrCat(pstr, p2);
  CriticalError(pstr);
end;


procedure CheckDMSFiles;
var
  db    : PDBFile;
begin
  db := New(PDBFile, Init(DBPATFile, '', dbOpenNormal));
  if db = nil then BadError(dbLastOpenError, 1, TRUE);
  MSDisposeObj(db);
  db := New(PDBFile, Init(DBSPECFile, '', dbOpenNormal));
  if db = nil then BadError(dbLastOpenError, 2, TRUE);
  MSDisposeObj(db);
  db := New(PDBFile, Init(DBISOFile, '', dbOpenNormal));
  if db = nil then BadError(dbLastOpenError, 3, TRUE);
  MSDisposeObj(db);
  db := New(PDBFile, Init(DBGSFile, '', dbOpenNormal));
  if db = nil then BadError(dbLastOpenError, 4, TRUE);
  MSDisposeObj(db);
  db := New(PDBFile, Init(DBISOORDFile, '', dbOpenNormal));
  if db = nil then BadError(dbLastOpenError, 5, TRUE);
  MSDisposeObj(db);
  db := New(PDBFile, Init(DBRESFile, '', dbOpenNormal));
  if db = nil then BadError(dbLastOpenError, 6, TRUE);
  MSDisposeObj(db);
  db := New(PDBFile, Init(DBSRCFile, '', dbOpenNormal));
  if db = nil then BadError(dbLastOpenError, 7, TRUE);
  MSDisposeObj(db);
  db := New(PDBFile, Init(DBWARDFile, '', dbOpenNormal));
  if db = nil then BadError(dbLastOpenError, 8, TRUE);
  MSDisposeObj(db);
  db := New(PDBFile, Init(DBSTATFile, '', dbOpenNormal));
  if db = nil then BadError(dbLastOpenError, 9, TRUE);
  MSDisposeObj(db);
  db := New(PDBFile, Init(DBSESSFile, '', dbOpenNormal));
  if db = nil then BadError(dbLastOpenError, 10, TRUE);
  MSDisposeObj(db);
  db := New(PDBFile, Init(DBORGFile, '', dbOpenNormal));
  if db = nil then BadError(dbLastOpenError, 11, TRUE);
  MSDisposeObj(db);
  db := New(PDBFile, Init(DBTSTGRPFile, '', dbOpenNormal));
  if db = nil then BadError(dbLastOpenError, 12, TRUE);
  MSDisposeObj(db);
  db := New(PDBFile, Init(DBTSTGRPXRFFile, '', dbOpenNormal));
  if db = nil then BadError(dbLastOpenError, 13, TRUE);
  MSDisposeObj(db);
  db := New(PDBFile, Init(DBTSTFile, '', dbOpenNormal));
  if db = nil then BadError(dbLastOpenError, 14, TRUE);
  MSDisposeObj(db);
  db := New(PDBFile, Init(DBTSTCATFile, '', dbOpenNormal));
  if db = nil then BadError(dbLastOpenError, 15, TRUE);
  MSDisposeObj(db);
  db := New(PDBFile, Init(DBDRUGFile, '', dbOpenNormal));
  if db = nil then BadError(dbLastOpenError, 16, TRUE);
  MSDisposeObj(db);
  db := New(PDBFile, Init(DBGSDEFFile, '', dbOpenNormal));
  if db = nil then BadError(dbLastOpenError, 17, TRUE);
  MSDisposeObj(db);
  db := New(PDBFile, Init(DBGSMNEMFile, '', dbOpenNormal));
  if db = nil then BadError(dbLastOpenError, 18, TRUE);
  MSDisposeObj(db);
  db := New(PDBFile, Init(DBORDFile, '', dbOpenNormal));
  if db = nil then BadError(dbLastOpenError, 19, TRUE);
  MSDisposeObj(db);
  db := New(PDBFile, Init(DBORDXRFFile, '', dbOpenNormal));
  if db = nil then BadError(dbLastOpenError, 20, TRUE);
  MSDisposeObj(db);
end;


END.
