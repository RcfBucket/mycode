Unit RptAlign;

INTERFACE

uses
  RptShare,
  oWindows;

function GetAlignment(AParent: PWindowsObject; title, prompt, lbl: PChar; var r: TLRect): boolean;

{$I RPTGEN.INC}

IMPLEMENTATION

uses
  DMSDebug,
  DMSErr,
  ctllib,
  ApiTools,
  Strings,
  SpinEdit,
  DlgLib,
  RptUtil,
  oDialogs,
  objects,
  MScan,
  WinProcs,
  WinTypes;

type
  PAlignDlg = ^TAlignDlg;
  TAlignDlg = object(TCenterDlg)
    alignSize: ^TLRect;
    title         : PChar;
    prompt        : PChar;
    lbl           : PChar;
    left, top     : PSpinReal;
    width, height : PSpinReal;
    constructor Init(aParent: PWindowsObject; aTitle, aPrompt, aLbl: PChar; var ASize: TLRect);
    procedure SetupWindow; virtual;
    function CanClose: boolean; virtual;
  end;

function GetAlignment(AParent: PWindowsObject;
              title, prompt, lbl: PChar; var r: TLRect): Boolean;
{- return alignment parameters. Return true if OK false if canceled.
   TRect returned will have left/top, width/height values in LTwips if
   OK selected }
var
  p  : PAlignDlg;
begin
  p:= New(PAlignDlg, Init(AParent, title, prompt, lbl, r));
  GetAlignment:= application^.ExecDialog(p) = IDOK;
end;

{-----------------------------------[ TAlignDlg ]--}

constructor TAlignDlg.Init(AParent: PWindowsObject; aTitle, aPrompt, aLbl: PChar; var ASize: TLRect);
var
  c  : PControl;
  k  : integer;
  max: real;
begin
  inherited Init(AParent, MAKEINTRESOURCE(DLG_ALIGN));

  max:= 1440 * 14;
  LTwipsToUnits(1440 * 14, max);

  left:= New(PSpinReal, InitResource(@self, IDC_LEFT, 25, 0, 0,max, 0.1));
  top:= New(PSpinReal, InitResource(@self, IDC_TOP, 25, 0, 0,max, 0.1));
  width:= New(PSpinReal, InitResource(@self, IDC_WIDTH, 25, 0, 0,max, 0.1));
  height:= New(PSpinReal, InitResource(@self, IDC_HEIGHT, 25, 0, 0,max, 0.1));
  for k:= 300 to 303 do
  begin
    c:= New(PLStatic, InitResource(@self, k, 0, false));
    c:= New(PLStatic, InitResource(@self, k+100, 0, false));
  end;
  title:= atitle;
  prompt:= aprompt;
  lbl:= aLbl;
  alignSize:= @ASize;
end;

procedure TAlignDlg.SetupWindow;
var
  pstr  : array[0..100] of char;
  k     : integer;
  num   : Real;
begin
  inherited SetupWindow;

  SetWindowText(HWindow, title);
  SendDlgItemMsg(IDC_PROMPT, WM_SETTEXT, 0, longint(prompt));
  SendDlgItemMsg(IDC_LABEL, WM_SETTEXT, 0, longint(lbl));

  UnitsStr(true, pstr, 100);
  for k:= 300 to 303 do
    SendDlgItemMsg(k, WM_SETTEXT, 0, longint(@pstr));

  LTwipsToUnits(alignSize^.left, num);
  left^.SetNum(num);

  LTwipsToUnits(alignSize^.top, num);
  top^.SetNum(num);

  LTwipsToUnits(alignSize^.right, num);
  width^.SetNum(num);

  LTwipsToUnits(alignSize^.bottom, num);
  height^.SetNum(num);
end;

function TAlignDlg.CanClose: Boolean;
var
  ret   : boolean;
  k     : integer;

  procedure GetANum(ed: PSpinReal; var aVal: longint);
  var
    num   : Real;
  begin
    if ret then
    begin
      if ed^.FldIsBlank or not ed^.NumIsValid then
      begin
        ShowError(@self, IDS_INVNUMFORMAT, nil, 0, MOD_RPTGEN, 3);
        FocusCtl(HWindow, ed^.GetID);
        ret:= false;
      end
      else
      begin
        num:= ed^.GetDouble;
        UnitsToLTwips(num, aVal);
      end;
    end;
  end;

begin
  ret:= true;

  GetANum(left, alignSize^.left);
  GetANum(top, alignSize^.top);
  GetANum(width, alignSize^.right);
  GetANum(height, alignSize^.bottom);

  CanClose:= ret;
end;

END.

{- rcf}
