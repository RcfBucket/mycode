unit ListLib;

INTERFACE

uses
  DBFile,
  DBLib,
  DBTypes,
  MScan,
  Objects,
  OWindows;

const
  { error values }
  listErr_CantAllocateMemory          = MOD_LISTLIB +  0;
  listErr_CantFindMnemIDField         = MOD_LISTLIB +  1;
  listErr_CantFindMnemDescField       = MOD_LISTLIB +  2;
  listErr_PartialKeyNotFound          = MOD_LISTLIB +  3;

type
  PMnemListRec = ^TMnemListRec;
  TMnemListRec = object(TObject)
    errorNum  : integer;{ Read-only }
    ID        : PChar;  { Read-only }
    desc      : PChar;  { Read-only }
    seq       : TSeqNum;{ Read-only }
    constructor Init;
    destructor Done; virtual;
    function Clear: boolean;
  private
    IDLen,
    descLen : word;
    procedure ClearIDAndDesc;
    function SetIDAndDesc(aID, aDesc: PChar; aIDLen, aDescLen: word): boolean;
  end;

  PListObject = ^TListObject;
  TListObject = object(TObject)
    errorNum  : integer;{ Read-only }
    constructor Init;
    destructor Done; virtual;
    function SelectList(aParent: PWindowsObject; aDBRec: PDBRec): boolean;
    function ViewOnlyList(aParent: PWindowsObject; aDBRec: PDBRec): boolean;
    function FindKey(aParent: PWindowsObject; aDBRec: PDBRec; doExactMatch: boolean): boolean;
    function FindSeq(aDBRec: PDBRec; aSeq: TSeqNum): boolean;
    function PeekSeq(aFileName: PChar; aSeq: TSeqNum): PDBRec; { Read-only return value }
    function SelectMnemList(aParent: PWindowsObject; aFileName, aID: PChar; aListRec: PMnemListRec): boolean;
    function ViewOnlyMnemList(aParent: PWindowsObject; aFileName, aID: PChar): boolean;
    function EditableMnemList(aParent: PWindowsObject; aFileName, aID: PChar): boolean;
    function FindMnemID(aParent: PWindowsObject;
                        aFileName, aID: PChar;
                        aListRec: PMnemListRec;
                        doExactMatch: boolean): boolean;
    function FindMnemSeq( aFileName: PChar;
                          aListRec: PMnemListRec;
                          aSeq: TSeqNum): boolean;
    function GetMnemIDLen(aFileName: PChar): word;
    function GetMnemDescLen(aFileName: PChar): word;
  private
    listOnPartialID : boolean;
    dbFileColl      : PCollection;
    function GetDB(aFileName: PChar): PDBFile;
    function SetKey(aDB: PDBFile): boolean;
    function GetIDFieldNum(aDBD: PDBReadDesc): integer;
    function GetDescFieldNum(aDBD: PDBReadDesc): integer;
    function GetUDFlagFieldNum(aDBD: PDBReadDesc): integer;
    function GetMnemInfo( aFileName: PChar;
                          var aDB: PDBFile;
                          var idFld: integer;
                          var idSize: word;
                          var descFld: integer;
                          var descSize: word;
                          var UDFlagFld: integer): boolean;
    function UpdateListRec( aListRec: PMnemListRec;
                            aDB: PDBFile;
                            idFld: integer;
                            idSize: word;
                            descFld: integer;
                            descSize: word): boolean;
    function SetKeyFields(aDB: PDBFile; aKeyStr: PChar): boolean;
  end;

IMPLEMENTATION

uses
  APITools,
  BTRConst,
  CtlLib,
  DBErrors,
  DBIds,
  DlgLib,
  DMSDebug,
  DMSErr,
  DMString,
  GridLib,
  INILib,
  ODialogs,
  PrnPrev,
  Sessions,
  Status,
  Strings,
  StrsW,
  UIFlds,
  UserMsgs,
  WinProcs,
  WinTypes,
  Win31;

{$R LISTLIB.RES}
{$I LISTLIB.INC}

const
  { button modes }
  modeViewOnly    = 0;
  modeEditOnly    = 1;
  modeSelectOnly  = 2;
  modeSelectEdit  = 3;

  maxResBlockLen  = 200;
  idPanel         = 'I';
  micPanel        = 'M';
  comboPanel      = 'C';
  SeqStrLen       = 10;

type
  PDBGrid = ^TDBGrid;
  TDBGrid = object(TGrid)
    function BuildKeyStr(aCol: integer; aRow: longint; keyBuf: PChar; maxLen: integer; numeric: boolean): PChar; virtual;
    function Sort(aCol, maxLen: integer; numeric: boolean): boolean; virtual;
  end;

  PDlgListBase = ^TDlgListBase;
  TDlgListBase = object(TCenterDlg)
    constructor Init( aParent: PWindowsObject;
                      dlgResID : integer;
                      aDB: PDBFile;
                      calledFromMnem: boolean;
                      aListObject: PListObject;
                      idFieldNumber: integer;
                      idFieldSize: word;
                      descFieldNumber: integer;
                      descFieldSize: word;
                      UDFlagFieldNumber: integer);
    procedure SetupWindow; virtual;
    procedure DrawItem (var msg: TMessage); virtual WM_FIRST + WM_DRAWITEM;
    procedure MeasureItem (var msg: TMessage); virtual WM_FIRST + WM_MEASUREITEM;
    procedure CharToItem (var msg: TMessage); virtual WM_FIRST + WM_CHARTOITEM;
    function  SetupGrid: boolean; virtual;
    function  FillGrid(setPosition: boolean; optimize: boolean; UDOnly: boolean): boolean; virtual;
    function  FillGridRow(aRow: longint): boolean; virtual;
    procedure WMFillGrid(var msg: TMessage); virtual WM_FIRST + WM_FILLMYGRID;
  private
    grid          : PDBGrid;
    closeButton   : PButton;
    db            : PDBFile;
    mnemMode      : boolean;
    listObject    : PListObject;
    idFld         : integer;
    idSize        : word;
    descFld       : integer;
    descSize      : word;
    UDFlagFld     : integer;
    lastVisibleCol: integer;
  end;

  PDlgList = ^TDlgList;
  TDlgList = object(TDlgListBase)
    constructor Init( aParent: PWindowsObject;
                      dlgResID : integer;
                      aDB: PDBFile;
                      calledFromMnem: boolean;
                      aListObject: PListObject;
                      buttonMode: byte;
                      idFieldNumber: integer;
                      idFieldSize: word;
                      descFieldNumber: integer;
                      descFieldSize: word;
                      UDFlagFieldNumber: integer);
    function  CanClose: boolean; virtual;
    procedure EnableButtons; virtual;
    procedure EditData; virtual;
    procedure DeleteData; virtual;
    procedure BtnDelete(var msg: TMessage); virtual ID_FIRST + BTN_DELETE;
    procedure BtnAdd(var msg: TMessage); virtual ID_FIRST + BTN_ADD;
    procedure BtnEdit(var msg: TMessage); virtual ID_FIRST + BTN_EDIT;
    procedure BtnPrint(var msg: TMessage); virtual ID_FIRST + BTN_PRINT;
    procedure GrdList(var msg: TMessage); virtual ID_FIRST + GRD_LIST;
  private
    addButton     : PButton;
    editButton    : PButton;
    deleteButton  : PButton;
    printButton   : PButton;
    selectButton  : PButton;
    showAdd       : boolean;
    showEdit      : boolean;
    showDelete    : boolean;
    showPrint     : boolean;
    showSelect    : boolean;
  end;

  PDlgListSessions = ^TDlgListSessions;
  TDlgListSessions = object(TDlgList)
    constructor Init( aParent: PWindowsObject;
                      dlgResID: integer;
                      aDB: PDBFile;
                      calledFromMnem: boolean;
                      aListObject: PListObject;
                      buttonMode: byte;
                      idFieldNumber: integer;
                      idFieldSize: word;
                      descFieldNumber: integer;
                      descFieldSize: word;
                      UDFlagFieldNumber: integer);
    procedure SetupWindow; virtual;
    procedure EnableButtons; virtual;
    procedure EditData; virtual;
    procedure DeleteData; virtual;
    procedure SetCurSessionText;
    procedure BtnSelectCurSession(var msg: TMessage); virtual ID_FIRST + BTN_SELCUR;
    procedure BtnDisableCurSession(var msg: TMessage); virtual ID_FIRST + BTN_DISABLE;
  private
    selectCurSessButton   : PButton;
    disableCurSessButton  : PButton;
    lblCurSession         : PLStatic;
    txtCurSession         : PLStatic;
    showSelectCurSession  : boolean;
    showDisableCurSession : boolean;
  end;

  PDlgListDrugs = ^TDlgListDrugs;
  TDlgListDrugs = object(TDlgList)
    constructor Init( aParent: PWindowsObject;
                      dlgResID: integer;
                      aDB: PDBFile;
                      calledFromMnem: boolean;
                      aListObject: PListObject;
                      buttonMode: byte;
                      idFieldNumber: integer;
                      idFieldSize: word;
                      descFieldNumber: integer;
                      descFieldSize: word;
                      UDFlagFieldNumber: integer);
    procedure EnableButtons; virtual;
    procedure BtnOptions(var msg: TMessage); virtual ID_FIRST + BTN_OPTIONS;
  private
    optionsButton : PButton;
    showOptions   : boolean;
  end;

  PDlgDrugOptions = ^TDlgDrugOptions;
  TDlgDrugOptions = object(TDlgListBase)
    constructor Init( aParent: PWindowsObject;
                      aDB: PDBFile;
                      aListObject: PListObject;
                      idFieldNumber: integer;
                      idFieldSize: word;
                      descFieldNumber: integer;
                      descFieldSize: word;
                      UDFlagFieldNumber: integer);
    procedure SetupWindow; virtual;
    function  CanClose: boolean; virtual;
    procedure EnableButtons; virtual;
    procedure SuppAll(supp: boolean);
    procedure SuppToggle(aRow: longint);
    procedure BtnSuppAll(var msg: TMessage); virtual ID_FIRST + BTN_SUPPALL;
    procedure BtnSuppNone(var msg: TMessage); virtual ID_FIRST + BTN_SUPPNONE;
    procedure BtnSuppToggle(var msg: TMessage); virtual ID_FIRST + BTN_SUPPTOGGLE;
    procedure BtnSuppInvert(var msg: TMessage); virtual ID_FIRST + BTN_SUPPINVERT;
    procedure SwapRows(row1, row2: longint);
    procedure BtnSortUp(var msg: TMessage); virtual ID_FIRST + BTN_SORTUP;
    procedure BtnSortDown(var msg: TMessage); virtual ID_FIRST + BTN_SORTDOWN;
    procedure BtnSortReset(var msg: TMessage); virtual ID_FIRST + BTN_SORTRESET;
  private
    suppAllButton    : PButton;
    suppNoneButton   : PButton;
    suppToggleButton : PButton;
    suppInvertButton : PButton;
    sortUpButton     : PButton;
    sortDownButton   : PButton;
    sortResetButton  : PButton;
  end;

  PDlgListOrganisms = ^TDlgListOrganisms;
  TDlgListOrganisms = object(TDlgList)
    constructor Init( aParent: PWindowsObject;
                      dlgResID: integer;
                      aDB: PDBFile;
                      calledFromMnem: boolean;
                      aListObject: PListObject;
                      buttonMode: byte;
                      idFieldNumber: integer;
                      idFieldSize: word;
                      descFieldNumber: integer;
                      descFieldSize: word;
                      UDFlagFieldNumber: integer);
    destructor Done; virtual;
    procedure SetupWindow; virtual;
    function  SetupGrid: boolean; virtual;
    function  FillGrid(setPosition: boolean; optimize: boolean; UDOnly: boolean): boolean; virtual;
    function  FillGridRow(aRow: longint): boolean; virtual;
    procedure WMFillGrid(var msg: TMessage); virtual WM_FIRST + WM_FILLMYGRID;
    procedure SortGrid;
    procedure EditData; virtual;
    procedure ComboSets(var msg: TMessage); virtual ID_FIRST + CMB_SETS;
    procedure CheckUDOnly(var msg: TMessage); virtual ID_FIRST + CHK_UDONLY;
    procedure RadioUnsorted(var msg: TMessage); virtual ID_FIRST + RDO_UNSORTED;
    procedure RadioID(var msg: TMessage); virtual ID_FIRST + RDO_ID;
    procedure RadioName(var msg: TMessage); virtual ID_FIRST + RDO_NAME;
    procedure RadioSet(var msg: TMessage); virtual ID_FIRST + RDO_SET;
    procedure RadioShortName(var msg: TMessage); virtual ID_FIRST + RDO_SHORTNAME;
    procedure RadioLongName(var msg: TMessage); virtual ID_FIRST + RDO_LONGNAME;
  private
    lblSets               : PLStatic;
    cmbSets               : PComboBox;
    chkUDOnly             : PLCheckBox;
    rdoSortBy             : PRadioGroup;
    rdoNameDisplayed      : PRadioGroup;
  end;

  PDlgEditMnem = ^TDlgEditMnem;
  TDlgEditMnem = object(TCenterDlg)
    constructor Init( aParent: PWindowsObject;
                      dlgResID: integer;
                      editMode: boolean;
                      aDB: PDBFile;
                      aListObj: PListObject;
                      idFieldNumber: integer;
                      idFieldSize: word;
                      descFieldNumber: integer;
                      descFieldSize: word;
                      UDFlagFieldNumber: integer);
    destructor Done; virtual;
    procedure SetupWindow; virtual;
    procedure Cancel(var msg: TMessage); virtual ID_FIRST + ID_CANCEL;
    function  DataChanged: boolean; virtual;
    function  CheckSave: boolean; virtual;
    function  LinkUIFld(aFldNum: integer; isReq, isCheckFld: boolean;
                        aResID, aLblID: word; dbd: PDBReadDesc): PUIFld; virtual;
    function  SaveData(clearRecord: boolean): boolean; virtual;
    procedure BtnSave(var msg: TMessage); virtual ID_FIRST + BTN_SAVE;
    procedure ClearFields; virtual;
    procedure WMCtlColor(var msg: TMessage); virtual WM_FIRST + WM_CTLCOLOR;
  private
    flds      : PCollection;  { collection of all user interface fields }
    listObj   : PListObject;
    txtMode   : PStatic;      { current mode text }
    edtID     : PUIFld;
    edtDesc   : PUIFld;
    saveButton: PButton;
    editing   : boolean;
    db        : PDBFile;
    idFld     : integer;
    idSize    : word;
    descFld   : integer;
    descSize  : word;
    UDFlagFld : integer;
    dataSaved : boolean;
  end;

  PDlgEditSource = ^TDlgEditSource;
  TDlgEditSource = object(TDlgEditMnem)
    constructor Init( aParent: PWindowsObject;
                      dlgResID: integer;
                      editMode: boolean;
                      aDB: PDBFile;
                      aListObj: PListObject;
                      idFieldNumber: integer;
                      idFieldSize: word;
                      descFieldNumber: integer;
                      descFieldSize: word;
                      UDFlagFieldNumber: integer);
  private
    chkUrnSrc : PUIFld;
  end;

  PDlgEditDrug = ^TDlgEditDrug;
  TDlgEditDrug = object(TDlgEditMnem)
    constructor Init( aParent: PWindowsObject;
                      dlgResID: integer;
                      editMode: boolean;
                      aDB: PDBFile;
                      aListObj: PListObject;
                      idFieldNumber: integer;
                      idFieldSize: word;
                      descFieldNumber: integer;
                      descFieldSize: word;
                      UDFlagFieldNumber: integer;
                      altOnly: boolean);
    procedure SetupWindow; virtual;
    function  SaveData(clearRecord: boolean): boolean; virtual;
    function  AssignHiddenFields: boolean;
    function  CalcSortNums(var sortNum, sortNumDflt: longint): boolean;
    procedure WMLostFocus(var msg: TMessage);   virtual WM_FIRST + WM_LOSTFOCUS;
    function  TransferIDToAltAbbr: boolean;
  private
    edtDesc2 : PUIFld;
    limited  : boolean;
  end;

  PDlgEditOrganism = ^TDlgEditOrganism;
  TDlgEditOrganism = object(TDlgEditMnem)
    constructor Init( aParent: PWindowsObject;
                      dlgResID: integer;
                      editMode: boolean;
                      aDB: PDBFile;
                      aListObj: PListObject;
                      idFieldNumber: integer;
                      idFieldSize: word;
                      descFieldNumber: integer;
                      descFieldSize: word;
                      UDFlagFieldNumber: integer);
    procedure SetupWindow; virtual;
    function  DataChanged: boolean; virtual;
    function  SaveData(clearRecord: boolean): boolean; virtual;
    procedure ComboSet(var msg: TMessage); virtual ID_FIRST + CMB_SET;
    procedure SelectSet(aSet: byte);
    procedure SelectFamily(aFamily: byte);
  private
    edtDesc2  : PUIFld;
    lblSet    : PLStatic;
    cmbSet    : PComboBox;
    lblFamily : PLStatic;
    cmbFamily : PComboBox;
  end;

  PDlgEditTest = ^TDlgEditTest;
  TDlgEditTest = object(TDlgEditMnem)
    constructor Init( aParent: PWindowsObject;
                      dlgResID: integer;
                      editMode: boolean;
                      aDB: PDBFile;
                      aListObj: PListObject;
                      idFieldNumber: integer;
                      idFieldSize: word;
                      descFieldNumber: integer;
                      descFieldSize: word;
                      UDFlagFieldNumber: integer);
    procedure SetupWindow; virtual;
    procedure LostFocus(var msg: TMessage); virtual WM_FIRST + WM_LOSTFOCUS;
    function  DataChanged: boolean; virtual;
    function  SaveData(clearRecord: boolean): boolean; virtual;
  private
    edtCategory : PUIFld;
    edtDrug     : PUIFld;
    procedure EnableDrug(on: boolean);
    procedure DoTestCatExit(fldResID: integer);
  end;

  PDlgEditTestGroup = ^TDlgEditTestGroup;
  TDlgEditTestGroup = object(TDlgEditMnem)
    constructor Init( aParent: PWindowsObject;
                      dlgResID: integer;
                      editMode: boolean;
                      aDB: PDBFile;
                      aListObj: PListObject;
                      idFieldNumber: integer;
                      idFieldSize: word;
                      descFieldNumber: integer;
                      descFieldSize: word;
                      UDFlagFieldNumber: integer);
    destructor Done; virtual;
    procedure SetupWindow; virtual;
    procedure DrawItem (var msg: TMessage); virtual WM_FIRST + WM_DRAWITEM;
    procedure MeasureItem (var msg: TMessage); virtual WM_FIRST + WM_MEASUREITEM;
    procedure CharToItem (var msg: TMessage); virtual WM_FIRST + WM_CHARTOITEM;
    procedure EnableButtons; virtual;
    function  FillGrid(optimize: boolean): boolean;
    function  AddGridRow(seq: TSeqNum): longint;
    function  DataChanged: boolean; virtual;
    function  SaveData(clearRecord: boolean): boolean; virtual;
    function  SaveXRef: boolean;
    function  DuplicateTest(seq: TSeqNum): boolean;
    procedure BtnAddTest(var msg: TMessage); virtual ID_FIRST + BTN_ADD;
    procedure BtnRemoveTest(var msg: TMessage); virtual ID_FIRST + BTN_DELETE;
    function  GetResLen(aTestRec: PDBRec; var resLen: integer): boolean;
  private
    grid              : PDBGrid;
    lblNumTests       : PLStatic;
    txtNumTests       : PStatic;
    addTestButton     : PButton;
    removeTestButton  : PButton;
    lastVisibleCol    : integer;
    testDB            : PDBFile;
    xrefDB            : PDBFile;
    testCatDB         : PDBFile;
    resBlockLen       : integer;
    gridChanged       : boolean;
  end;

  PDlgEditOrder = ^TDlgEditOrder;
  TDlgEditOrder = object(TDlgEditMnem)
    constructor Init( aParent: PWindowsObject;
                      dlgResID: integer;
                      editMode: boolean;
                      aDB: PDBFile;
                      aListObj: PListObject;
                      idFieldNumber: integer;
                      idFieldSize: word;
                      descFieldNumber: integer;
                      descFieldSize: word;
                      UDFlagFieldNumber: integer);
    destructor Done; virtual;
    procedure SetupWindow; virtual;
    procedure DrawItem (var msg: TMessage); virtual WM_FIRST + WM_DRAWITEM;
    procedure MeasureItem (var msg: TMessage); virtual WM_FIRST + WM_MEASUREITEM;
    procedure CharToItem (var msg: TMessage); virtual WM_FIRST + WM_CHARTOITEM;
    procedure EnableButtons; virtual;
    function  FillGrid(optimize: boolean): boolean;
    function  AddGridRow(seq: TSeqNum): longint;
    function  DataChanged: boolean; virtual;
    function  SaveData(clearRecord: boolean): boolean; virtual;
    function  SaveXRef: boolean;
    function  OKToAddTestGroup(seq: TSeqNum): boolean;
    function  DuplicateTestGroup(seq : TSeqNum): boolean;
    function  DuplicateTest(seq : TSeqNum): boolean;
    function  IDMicConflict(aDBRec: PDBRec): boolean;
    function  GetCurOrderSet: byte;
    procedure BtnAddTestGroup(var msg: TMessage); virtual ID_FIRST + BTN_ADD;
    procedure BtnRemoveTestGroup(var msg: TMessage); virtual ID_FIRST + BTN_DELETE;
  private
    grid                  : PDBGrid;
    lblNumTestGroups      : PLStatic;
    txtNumTestGroups      : PStatic;
    addTestGroupButton    : PButton;
    removeTestGroupButton : PButton;
    lastVisibleCol        : integer;
    testGroupDB           : PDBFile;
    xrefDB                : PDBFile;
    testXrefDB            : PDBFile;
    testDB                : PDBFile;
    gridChanged           : boolean;
  end;

procedure LLShowError(aParent : PWindowsObject; { parent window object (or nil)}
                      msgID   : word;           { string resource ID for message}
                      substStr: PChar;          { substitution string for message}
                      errNum  : longint);       { module ID (need to account for words and negatives) }
var
  errSubst : PErrSubst;
begin
  errSubst:= nil;
  if (substStr <> nil) then
    errSubst:= New(PErrSubst, Init(substStr));
  ShowError(aParent, msgID, errSubst, errNum, MOD_LISTLIB, 0);
  if (errSubst <> nil) then
    Dispose(errSubst, Done);
end;

procedure ShowButton(Wnd: HWnd; visible, enabled: boolean);
begin
  if visible then
    ShowWindow(Wnd, SW_SHOW)
  else
    ShowWindow(Wnd, SW_HIDE);
  EnableWindow(Wnd, (visible and enabled));
end;

function SetupDBGrid(aParent: PWindowsObject; aGrid: PDBGrid; aDB: PDBFile;
                     aListObject: PListObject; var lastVisibleCol: integer): boolean;
var
  retVal  : boolean;
  fNum    : integer;
  cWidth  : integer;
  fInfo   : TFldInfo;
  aInfo   : TAssocInfo;
  fLen    : word;
  descFld : integer;
begin
  retVal:= True;
  descFld:= 0;
  with aGrid^ do
  begin
    for fNum:= 1 to aDB^.dbd^.NumFields do
    begin
      if not aDB^.dbd^.FieldInfo(fNum, fInfo) then
      begin
        retVal:= False;
        LLShowError(aParent, IDS_DBOPFAILED, 'FieldInfo', aDB^.dbd^.dbErrorNum);
        break;
      end
      else if (fInfo.userFlag and db_Hidden <> db_Hidden) then
      begin
        if ((fInfo.userFlag and db_MNMDescr) = db_MNMDescr) then
          descFld:= fNum;
        fLen:= 0;
        SetHeader(fNum, fInfo.fldName);
        if (fInfo.userFlag and db_Mnemonic = db_Mnemonic) then
        begin
          if (fInfo.fldAssoc < 1) then
          begin
            retVal:= False;
            LLShowError(aParent, IDS_MNEMNOASSOC, nil, 0);
            break;
          end
          else
          begin
            if not aDB^.dbd^.AssocInfo(fInfo.fldAssoc, aInfo) then
            begin
              retVal:= False;
              LLShowError(aParent, IDS_DBOPFAILED, 'AssocInfo', aDB^.dbd^.dbErrorNum);
              break;
            end
            else
            begin
              fLen:= aListObject^.GetMnemDescLen(aInfo.fName);
              if (fLen = 0) then
              begin
                retVal:= False;
                LLShowError(aParent, IDS_LISTOPFAILED, 'GetMnemDescLen', aListObject^.ErrorNum);
                break;
              end;
            end;
          end;
        end
        else
        begin
          fLen:= aDB^.dbd^.FieldSizeForStr(fNum)-1;
          if (aDB^.dbd^.dbErrorNum <> 0) then
          begin
            retVal:= False;
            LLShowError(aParent, IDS_DBOPFAILED, 'FieldSizeForStr', aDB^.dbd^.dbErrorNum);
            break;
          end;
        end;
        { set default width to average character width of 1/2 filled field, but }
        { add buffer to field length so small fields are made wide enough       }
        SetAvgCharWidth(fNum, ((fLen + 6) div 2));
        lastVisibleCol:= fNum;
        if (GetSearchColumn = 0) then
          SetSearchColumn(fNum);
      end;
    end;
    if retVal then
    begin
      if (descFld > 0) then
        StretchColumn(descFld)
      else
        StretchColumn(lastVisibleCol);
    end;
    SetupDBGrid:= retVal;
  end;
end;

function FillDBGridRow( aParent: PWindowsObject; aGrid: PDBGrid; aDB: PDBFile;
                        aListObject: PListObject; aRow: longint): boolean;
var
  retVal  : boolean;
  seqStr  : array[0..SeqStrLen] of char;
  fStr    : PChar;
  fSize   : word;
  aCol    : integer;
  seq     : TSeqNum;
  fInfo   : TFldInfo;
  aInfo   : TAssocInfo;
  listRec : PMnemListRec;
begin
  retVal:= True;
  listRec:= New(PMnemListRec, Init);
  if (listRec = nil) then
  begin
    retVal:= False;
    LLShowError(aParent, IDS_MEMFAILURE, nil, 0);
  end
  else
  begin
    with aGrid^ do
    begin
      seq:= aDB^.dbr^.GetSeqValue;
      if (seq < 0) then
      begin
        retVal:= False;
        LLShowError(aParent, IDS_DBOPFAILED, 'GetSeqValue', aDB^.dbr^.dbErrorNum);
      end
      else
      begin
        SetItemData(aRow, seq);
        Str(seq, seqStr);
        SetData(0, aRow, seqStr);
        for aCol:= 1 to aDB^.dbd^.NumFields do
        begin
          if not aDB^.dbd^.FieldInfo(aCol, fInfo) then
          begin
            retVal:= False;
            LLShowError(aParent, IDS_DBOPFAILED, 'FieldInfo', aDB^.dbd^.dbErrorNum);
            break;
          end
          else
          begin
            if (fInfo.userFlag and db_Mnemonic = db_Mnemonic) then
            begin
              if (fInfo.fldAssoc < 1) then
              begin
                retVal:= False;
                LLShowError(aParent, IDS_MNEMNOASSOC, nil, 0);
                break;
              end
              else
              begin
                if not aDB^.dbd^.AssocInfo(fInfo.fldAssoc, aInfo) then
                begin
                  retVal:= False;
                  LLShowError(aParent, IDS_DBOPFAILED, 'AssocInfo', aDB^.dbd^.dbErrorNum);
                  break;
                end
                else
                begin
                  fSize:= aListObject^.GetMnemDescLen(aInfo.fName)+1;
                  if (fSize = 1) then
                  begin
                    retVal:= False;
                    LLShowError(aParent, IDS_LISTOPFAILED, 'GetMnemDescLen', aListObject^.ErrorNum);
                    break;
                  end
                  else
                  begin
                    GetMem(fStr, fSize);
                    if (fStr = nil) then
                    begin
                      retVal:= False;
                      LLShowError(aParent, IDS_MEMFAILURE, nil, 0);
                      break;
                    end
                    else
                    begin
                      if not aDB^.dbr^.GetField(aCol, @seq, SizeOf(seq)) then
                      begin
                        retVal:= False;
                        LLShowError(aParent, IDS_DBOPFAILED, 'GetField', aDB^.dbr^.dbErrorNum);
                        FreeMem(fStr, fSize);
                        break;
                      end
                      else
                      begin
                        if (seq = 0) then
                          StrCopy(fStr, '')
                        else if not aListObject^.FindMnemSeq(aInfo.fName, listRec, seq) then
                        begin
                          retVal:= False;
                          LLShowError(aParent, IDS_LISTOPFAILED, 'FindMnemSeq', aListObject^.ErrorNum);
                          FreeMem(fStr, fSize);
                          break;
                        end
                        else
                          StrLCopy(fStr, listRec^.desc, fSize-1);
                      end;
                    end;
                  end;
                end;
              end;
            end
            else
            begin
              fSize:= aDB^.dbd^.FieldSizeForStr(aCol);
              GetMem(fStr, fSize);
              if (fStr = nil) then
              begin
                retVal:= False;
                LLShowError(aParent, IDS_MEMFAILURE, nil, 0);
                break;
              end
              else
              begin
                aDB^.dbr^.GetFieldAsStr(aCol, fStr, fSize-1);
                if (aDB^.dbr^.dbErrorNum <> 0) then
                begin
                  retVal:= False;
                  LLShowError(aParent, IDS_DBOPFAILED, 'GetFieldAsStr', aDB^.dbr^.dbErrorNum);
                  FreeMem(fStr, fSize);
                  break;
                end;
              end;
            end;
            if retVal then
            begin
              SetData(aCol, aRow, fStr);
              FreeMem(fStr, fSize);
            end;
          end;
        end;
      end;
    end;
    Dispose(listRec, Done);
  end;
  FillDBGridRow:= retVal;
end;

procedure SortDBGrid(aGrid: PDBGrid; aCol, maxLen: integer; numeric: boolean);
var
  selSeq : tSeqNum;
  curRow : longint;
  i1, i2 : integer;
  redraw : boolean;
begin
  redraw:= aGrid^.GetRedraw;
  aGrid^.SetRedraw(False);
  curRow:= aGrid^.GetSelIndex;
  if (curRow >= 0) then
  begin
    selSeq:= aGrid^.GetItemData(curRow);
    aGrid^.Sort(aCol, maxLen, numeric);
    curRow:= 0;
    while (selSeq <> aGrid^.GetItemData(curRow)) do
      Inc(curRow);
    aGrid^.SetSelIndex(aGrid^.GetRowCount-1);
    aGrid^.SetSelIndex(curRow);
  end;
  aGrid^.SetRedraw(redraw);
end;

function DoListDialog(aParent: PWindowsObject;
                      aDB: PDBFile;
                      calledFromMnem: boolean;
                      aListObject: PListObject;
                      buttonMode: byte;
                      idFieldNumber: integer;
                      idFieldSize: word;
                      descFieldNumber: integer;
                      descFieldSize: word;
                      UDFlagFieldNumber: integer): integer;
var
  retVal  : integer;
  fName   : array[0..maxFileNameLen] of char;
  curKey  : integer;
begin
  retVal:= IDCANCEL;
  if (StrIComp(aDB^.dbd^.FileName(fName, maxFileNameLen), DBDRUGFile) = 0) then
  begin
    curKey:= aDB^.dbc^.GetCurKeyNum;
    if not aDB^.dbc^.SetCurKeyNum(DBDRUG_SORT_KEY) then
      LLShowError(aParent, IDS_DBOPFAILED, 'SetCurKeyNum', aDB^.dbc^.dbErrorNum)
    else
    begin
      retVal:= application^.ExecDialog(New(PDlgListDrugs, Init( aParent,
                                                                DLG_LIST,
                                                                aDB,
                                                                calledFromMnem,
                                                                aListObject,
                                                                buttonMode,
                                                                idFieldNumber,
                                                                idFieldSize,
                                                                descFieldNumber,
                                                                descFieldSize,
                                                                UDFlagFieldNumber)));
      if not aDB^.dbc^.SetCurKeyNum(curKey) then
        LLShowError(aParent, IDS_DBOPFAILED, 'SetCurKeyNum', aDB^.dbc^.dbErrorNum);
    end;
  end
  else if (StrIComp(aDB^.dbd^.FileName(fName, maxFileNameLen), DBSESSFile) = 0) then
  begin
    retVal:= application^.ExecDialog(New(PDlgListSessions, Init(aParent,
                                                                DLG_LISTSESSIONS,
                                                                aDB,
                                                                calledFromMnem,
                                                                aListObject,
                                                                buttonMode,
                                                                idFieldNumber,
                                                                idFieldSize,
                                                                descFieldNumber,
                                                                descFieldSize,
                                                                UDFlagFieldNumber)));
  end
  else if (StrIComp(aDB^.dbd^.FileName(fName, maxFileNameLen), DBORGFile) = 0) then
  begin
    retVal:= application^.ExecDialog(New(PDlgListOrganisms, Init( aParent,
                                                                  DLG_LISTORGANISMS,
                                                                  aDB,
                                                                  calledFromMnem,
                                                                  aListObject,
                                                                  buttonMode,
                                                                  idFieldNumber,
                                                                  idFieldSize,
                                                                  descFieldNumber,
                                                                  descFieldSize,
                                                                  UDFlagFieldNumber)));
  end
  else
    retVal:= application^.ExecDialog(New(PDlgList, Init(aParent,
                                                        DLG_LIST,
                                                        aDB,
                                                        calledFromMnem,
                                                        aListObject,
                                                        buttonMode,
                                                        idFieldNumber,
                                                        idFieldSize,
                                                        descFieldNumber,
                                                        descFieldSize,
                                                        UDFlagFieldNumber)));
  DoListDialog:= retVal;
end;

function DoEditDialog(aParent: PWindowsObject;
                      aDB: PDBFile;
                      aListObject: PListObject;
                      idFieldNumber: integer;
                      idFieldSize: word;
                      descFieldNumber: integer;
                      descFieldSize: word;
                      UDFlagFieldNumber: integer;
                      editMode: boolean): integer;
var
  retVal        : integer;
  fName         : array[0..maxFileNameLen] of char;
  pstr1         : array[0..100] of char;
  pstr2         : array[0..100] of char;
  userDef       : boolean;
  isReferenced  : boolean;
  proceed       : boolean;
begin
  retVal:= IDCANCEL;
  isReferenced:= False;
  userDef:= True;
  proceed:= True;

  if editMode then
  begin
    isReferenced:= not aDB^.dbr^.OkayToMessWith;
    if (aDB^.dbr^.dbErrorNum <> 0) then
    begin
      proceed:= False;
      LLShowError(aParent, IDS_DBOPFAILED, 'OkayToMessWith', aDB^.dbr^.dbErrorNum);
    end;
    if proceed and (UDFlagFieldNumber > 0) then
    begin
      if not aDB^.dbr^.GetField(UDFlagFieldNumber, @userDef, SizeOf(userDef)) then
      begin
        proceed:= False;
        LLShowError(aParent, IDS_DBOPFAILED, 'GetField', aDB^.dbr^.dbErrorNum);
      end;
    end;
  end;

  if proceed then
  begin
    if (StrIComp(aDB^.dbd^.FileName(fName, maxFileNameLen), DBDRUGFile) = 0) then
    begin
      retVal:= application^.ExecDialog(New(PDlgEditDrug, Init(aParent,
                                                              DLG_EDIT_DRUG,
                                                              editMode,
                                                              aDB,
                                                              aListObject,
                                                              idFieldNumber,
                                                              idFieldSize,
                                                              descFieldNumber,
                                                              descFieldSize,
                                                              UDFlagFieldNumber,
                                                              editMode and (isReferenced or (not userDef)))));
    end
    else
    begin
      if editMode and (isReferenced or (not userDef)) then
        InfoMsg(aParent^.HWindow, SR(IDS_WARNING, pstr1, SizeOf(pstr1)-1), SR(IDS_CANTEDITREC, pstr2, SizeOf(pstr2)-1))
      else
      begin
        if (StrIComp(aDB^.dbd^.FileName(fName, maxFileNameLen), DBORGFile) = 0) then
          retVal:= application^.ExecDialog(New(PDlgEditOrganism, Init(aParent,
                                                                      DLG_EDITORGANISM,
                                                                      editMode,
                                                                      aDB,
                                                                      aListObject,
                                                                      idFieldNumber,
                                                                      idFieldSize,
                                                                      descFieldNumber,
                                                                      descFieldSize,
                                                                      UDFlagFieldNumber)))
        else if (StrIComp(aDB^.dbd^.FileName(fName, maxFileNameLen), DBORDFile) = 0) then
          retVal:= application^.ExecDialog(New(PDlgEditOrder, Init( aParent,
                                                                    DLG_EDITORDER,
                                                                    editMode,
                                                                    aDB,
                                                                    aListObject,
                                                                    idFieldNumber,
                                                                    idFieldSize,
                                                                    descFieldNumber,
                                                                    descFieldSize,
                                                                    UDFlagFieldNumber)))
        else if (StrIComp(aDB^.dbd^.FileName(fName, maxFileNameLen), DBTSTGRPFile) = 0) then
          retVal:= application^.ExecDialog(New(PDlgEditTestGroup, Init( aParent,
                                                                        DLG_EDITTESTGROUP,
                                                                        editMode,
                                                                        aDB,
                                                                        aListObject,
                                                                        idFieldNumber,
                                                                        idFieldSize,
                                                                        descFieldNumber,
                                                                        descFieldSize,
                                                                        UDFlagFieldNumber)))
        else if (StrIComp(aDB^.dbd^.FileName(fName, maxFileNameLen), DBTSTFile) = 0) then
          retVal:= application^.ExecDialog(New(PDlgEditTest, Init(aParent,
                                                                  DLG_EDIT_TEST,
                                                                  editMode,
                                                                  aDB,
                                                                  aListObject,
                                                                  idFieldNumber,
                                                                  idFieldSize,
                                                                  descFieldNumber,
                                                                  descFieldSize,
                                                                  UDFlagFieldNumber)))
        else if (StrIComp(aDB^.dbd^.FileName(fName, maxFileNameLen), DBSRCFile) = 0) then
          retVal:= application^.ExecDialog(New(PDlgEditSource, Init(aParent,
                                                                    DLG_EDIT_MNEM,
                                                                    editMode,
                                                                    aDB,
                                                                    aListObject,
                                                                    idFieldNumber,
                                                                    idFieldSize,
                                                                    descFieldNumber,
                                                                    descFieldSize,
                                                                    UDFlagFieldNumber)))
        else
          retVal:= application^.ExecDialog(New(PDlgEditMnem, Init(aParent,
                                                                  DLG_EDIT_MNEM,
                                                                  editMode,
                                                                  aDB,
                                                                  aListObject,
                                                                  idFieldNumber,
                                                                  idFieldSize,
                                                                  descFieldNumber,
                                                                  descFieldSize,
                                                                  UDFlagFieldNumber)));
      end;
    end;
  end;
  DoEditDialog:= retVal;
end;

function TDBGrid.BuildKeyStr(aCol: integer; aRow: longint; keyBuf: PChar; maxLen: integer; numeric: boolean): PChar;
var
  tBuf: PChar;
  seqBuf1,seqBuf2: array[0..SeqStrLen] of char;
begin
  GetMem(tBuf, maxLen+1);
  GetData(aCol, aRow, tBuf, maxLen);
  { Pad it, leaving room for the sequence number to be appended }
  if numeric then
    LeftPad(keyBuf, tBuf, ' ', maxLen-SeqStrLen)
  else
    Pad(keyBuf, tBuf, ' ', maxLen-SeqStrLen);
  GetData(0, aRow, seqBuf1, SeqStrLen);
  LeftPad(seqBuf2, seqBuf1, ' ', SeqStrLen);
  StrLCat(keyBuf, seqBuf2, maxLen);
  FreeMem(tBuf, maxLen+1);
  BuildKeyStr:= keyBuf;
end;

function TDBGrid.Sort(aCol, maxLen: integer; numeric: boolean): boolean;
{ need to allow for extra characters when the sequence number string is appended in BuildKeyStr }
begin
  Sort:= inherited Sort(aCol, maxLen+SeqStrLen, numeric);
end;


constructor TDlgListBase.Init(aParent: PWindowsObject;
                              dlgResID: integer;
                              aDB: PDBFile;
                              calledFromMnem: boolean;
                              aListObject: PListObject;
                              idFieldNumber: integer;
                              idFieldSize: word;
                              descFieldNumber: integer;
                              descFieldSize: word;
                              UDFlagFieldNumber: integer);
begin
  inherited Init(aParent, MakeIntResource(dlgResID));
  db:= aDB;
  mnemMode:= calledFromMnem;
  listObject:= aListObject;
  idFld:= idFieldNumber;
  idSize:= idFieldSize;
  descFld:= descFieldNumber;
  descSize:= descFieldSize;
  UDFlagFld:= UDFlagFieldNumber;
  lastVisibleCol:= 0;
  grid:= New(PDBGrid, InitResource(@Self, GRD_LIST, MaxGridColumns, false));
  closeButton:= New(PButton, InitResource(@Self, IDCANCEL));
end;

procedure TDlgListBase.SetupWindow;
var
  pstr20: array[0..20] of char;
  title : array[0..maxFileDescLen+20] of char;
begin
  inherited SetupWindow;
  db^.dbd^.FileDesc(title, SizeOf(title)-1);
  StrLCat(title, ' ', SizeOf(title)-1);
  StrLCat(title, SR(IDS_LIST, pstr20, SizeOf(pstr20)-1), SizeOf(title)-1);
  SetWindowText(HWindow, title);
  { hide the grid while setting up and filling it, to avoid flickering effect }
  ShowWindow(grid^.HWindow, SW_HIDE);
  if not SetupGrid then
    EndDlg(IDCANCEL);
  PostMessage(HWindow, WM_FILLMYGRID, 0, 0);
  PostMessage(HWindow, WM_NEXTDLGCTL, grid^.HWindow, 1);
end;

procedure TDlgListBase.DrawItem(var msg: TMessage);
begin
  grid^.DrawItem(msg);
end;

procedure TDlgListBase.MeasureItem(var msg: TMessage);
begin
  grid^.MeasureItem(msg);
end;

procedure TDlgListBase.CharToItem(var msg: TMessage);
begin
  grid^.CharToItem(msg);
end;

function  TDlgListBase.SetupGrid: boolean;
begin
  SetupGrid:= SetupDBGrid(@Self, grid, db, listObject, lastVisibleCol);
end;

function TDlgListBase.FillGrid(setPosition: boolean; optimize: boolean; UDOnly: boolean): boolean;
var
  retVal              : boolean;
  aRow                : longint;
  id1                 : PChar;
  id2                 : PChar;
  pstr20              : array[0..20] of char;
  pstr80              : array[0..80] of char;
  stat                : PPctStatDlg;
  numRecs             : longint;
  curRec              : longint;
  cont                : boolean;
  userDef             : boolean;
  redraw              : boolean;
  gridStatusCutoff    : longint;
  gridStatusBlockSize : longint;
  hourGlass           : PWaitCursor;
begin
  retVal:= True;
  hourGlass:= new(PWaitCursor, Init);
  stat:= nil;
  cont:= True;
  gridStatusCutoff:= INIGridStatusCutoff;
  gridStatusBlockSize:= INIGridStatusBlockSize;
  with grid^ do
  begin
    ShowWindow(grid^.HWindow, SW_HIDE);
    redraw:= GetRedraw;
    SetRedraw(False);
    ClearList;
    numRecs:= db^.dbc^.NumRecords;
    if (numRecs > 0) then
    begin
      if (numRecs > gridStatusCutoff) then
      begin
        stat:= New(PPctStatDlg, Init(@Self, MakeIntResource(DLG_STATUS), True, BAR_PROGRESS));
        application^.MakeWindow(stat);
      end;
      if mnemMode then
      begin
        GetMem(id1, idSize);
        GetMem(id2, idSize);
        if (id1 = nil) or (id2 = nil) then
        begin
          retVal:= False;
          LLShowError(@Self, IDS_MEMFAILURE, nil, 0);
        end
        else
        begin
          db^.dbr^.GetFieldAsStr(idFld, id1, idSize-1);
          if (db^.dbr^.dbErrorNum <> 0) then
          begin
            retVal:= False;
            LLShowError(@Self, IDS_DBOPFAILED, 'GetFieldAsStr', db^.dbr^.dbErrorNum);
          end
          else
          begin
            curRec:= 0;
            if db^.dbc^.GetFirst(db^.dbr) then
            repeat
              userDef:= False;
              if UDOnly and (UDFlagFld > 0) then
              begin
                if not db^.dbr^.GetField(UDFlagFld, @userDef, SizeOf(userDef)) then
                begin
                  retVal:= False;
                  LLShowError(@Self, IDS_DBOPFAILED, 'GetField', db^.dbr^.dbErrorNum);
                  break;
                end;
              end;
              if retVal and ((not UDOnly) or userDef) then
              begin
                aRow:= AddString('');
                if (aRow < 0) then
                begin
                  InfoMsg(HWindow, SR(IDS_WARNING, pstr20, SizeOf(pstr20)-1), SR(IDS_TOOMANYRECS, pstr80, SizeOf(pstr80)-1));
                  break;
                end
                else
                begin
                  if not FillGridRow(aRow) then
                  begin
                    retVal:= False;
                    break;
                  end
                  else
                  begin
                    db^.dbr^.GetFieldAsStr(idFld, id2, idSize-1);
                    if (db^.dbr^.dbErrorNum <> 0) then
                    begin
                      retVal:= False;
                      LLShowError(@Self, IDS_DBOPFAILED, 'GetFieldAsStr', db^.dbr^.dbErrorNum);
                      break;
                    end
                    else
                    begin
                      if setPosition and (GetSelIndex < 0) and
                        (StrLIComp(id1, id2, StrLen(id1)) = 0) then
                        SetSelIndex(aRow);
                    end;
                  end;
                end;
                if (stat <> nil) and ((curRec mod gridStatusBlockSize = 0) or (curRec = numRecs)) then
                begin
                  stat^.CompPctLevel(curRec, numRecs);
                  if not stat^.CanContinue and
                    not YesNoMsg(stat^.HWindow, SR(IDS_CONFIRM, pstr20, SizeOf(pstr20)-1),
                                  SR(IDS_CANCELFILL, pstr80, SizeOf(pstr80)-1)) then
                    stat^.SetContinue(True);
                  cont:= stat^.CanContinue;
                end;
              end;
              Inc(curRec);
            until not (db^.dbc^.GetNext(db^.dbr) and (cont));
          end;
          FreeMem(id1, idSize);
          FreeMem(id2, idSize);
        end;
      end
      else
      begin
        curRec:= 0;
        if db^.dbc^.GetFirstContains(db^.dbr) then
        repeat
          aRow:= AddString('');
          if (aRow < 0) then
          begin
            InfoMsg(HWindow, SR(IDS_WARNING, pstr20, SizeOf(pstr20)-1), SR(IDS_TOOMANYRECS, pstr80, SizeOf(pstr80)-1));
            break;
          end
          else
          begin
            if not FillGridRow(aRow) then
            begin
              retVal:= False;
              break;
            end;
            if (stat <> nil) and ((curRec mod gridStatusBlockSize = 0) or (curRec = numRecs)) then
            begin
              stat^.CompPctLevel(curRec, numRecs);
              if not stat^.CanContinue and
                not YesNoMsg(stat^.HWindow, SR(IDS_CONFIRM, pstr20, SizeOf(pstr20)-1),
                              SR(IDS_CANCELFILL, pstr80, SizeOf(pstr80)-1)) then
                stat^.SetContinue(True);
              cont:= stat^.CanContinue;
            end;
          end;
          Inc(curRec);
        until not (db^.dbc^.GetNextContains(db^.dbr) and (cont));
      end;
      if (stat <> nil) then
        Dispose(stat, Done);
    end;
    if retVal then
    begin
      if optimize then
      begin
        if mnemMode and (GetColumnWidth(descFld) > 0) then
          grid^.OptimizeGrid(descFld)
        else
          grid^.OptimizeGrid(lastVisibleCol);
      end;
      if (GetSelIndex < 0) then
        SetSelIndex(0);
      SetRedraw(redraw);
      ShowWindow(grid^.HWindow, SW_SHOW);
    end;
  end;
  if (hourGlass <> nil) then
    Dispose(hourGlass, Done);
  FillGrid:= retVal;
end;

function  TDlgListBase.FillGridRow(aRow: longint): boolean;
begin
  FillGridRow:= FillDBGridRow(@Self, grid, db, listObject, aRow)
end;

procedure TDlgListBase.WMFillGrid(var msg: TMessage);
begin
  if not FillGrid(True, True, False) then
    EndDlg(IDCANCEL)
  else
    EnableButtons;
end;

constructor TDlgList.Init(  aParent: PWindowsObject;
                            dlgResID: integer;
                            aDB: PDBFile;
                            calledFromMnem: boolean;
                            aListObject: PListObject;
                            buttonMode: byte;
                            idFieldNumber: integer;
                            idFieldSize: word;
                            descFieldNumber: integer;
                            descFieldSize: word;
                            UDFlagFieldNumber: integer);
var
  fi : tFileInfo;
begin
  inherited Init( aParent, dlgResID, aDB, calledFromMnem, aListObject, idFieldNumber,
                  idFieldSize, descFieldNumber, descFieldSize, UDFlagFieldNumber);
  db^.dbd^.FileInfo(fi);
  showAdd:= ((buttonMode = modeEditOnly) or (buttonMode = modeSelectEdit)) and
            (fi.userFlag and db_ReadOnly <> db_ReadOnly);
  showEdit:= showAdd;
  showDelete:= showAdd;
  showPrint:= showAdd;
  showSelect:= (buttonMode = modeSelectOnly) or (buttonMode = modeSelectEdit);
  addButton:= New(PButton, InitResource(@Self, BTN_ADD));
  editButton:= New(PButton, InitResource(@Self, BTN_EDIT));
  deleteButton:= New(PButton, InitResource(@Self, BTN_DELETE));
  printButton:= New(PButton, InitResource(@Self, BTN_PRINT));
  selectButton:= New(PButton, InitResource(@Self, IDOK));
end;

function TDlgList.CanClose: boolean;
var
  retVal    : boolean;
begin
  retVal:= inherited CanClose;

  retVal:= retVal and (grid^.GetSelIndex >= 0);
  if retVal then
  begin
    if not db^.dbc^.GetSeq(db^.dbr, grid^.GetItemData(grid^.GetSelIndex)) then
    begin
      retVal:= False;
      LLShowError(@Self, IDS_DBOPFAILED, 'GetSeq', db^.dbc^.dbErrorNum);
    end;
  end;
  CanClose:= retVal;
end;

procedure TDlgList.EnableButtons;
var
  pstr  : array[0..50] of char;
begin
  if not showSelect then
    closeButton^.SetCaption(SR(IDS_CLOSE, pstr, SizeOf(pstr)-1));
  ShowButton(addButton^.HWindow, showAdd, True);
  ShowButton(editButton^.HWindow, showEdit, (grid^.GetSelIndex >= 0));
  ShowButton(deleteButton^.HWindow, showDelete, (grid^.GetSelIndex >= 0));
  ShowButton(printButton^.HWindow, showPrint, (grid^.GetRowCount > 0));
  ShowButton(selectButton^.HWindow, showSelect, (grid^.GetSelIndex >= 0));
end;

procedure TDlgList.EditData;
var
  pos : longint;
begin
  pos:= grid^.GetSelIndex;
  if (pos >= 0) then
  begin
    if not db^.dbc^.GetSeq(db^.dbr, grid^.GetItemData(pos)) then
      LLShowError(@Self, IDS_DBOPFAILED, 'GetSeq', db^.dbc^.dbErrorNum)
    else if (DoEditDialog(@Self, db, listObject, idFld, idSize, descFld, descSize, UDFlagFld, True) = IDOK) then
    begin
      FillGridRow(pos);
      EnableButtons;
    end;
  end;
end;

procedure TDlgList.DeleteData;
var
  pos           : longint;
  p1,p2         : array[0..100] of char;
  userDef       : boolean;
  isReferenced  : boolean;
  proceed       : boolean;
begin
  isReferenced:= False;
  userDef:= True;
  proceed:= True;
  pos:= grid^.GetSelIndex;
  if (pos >= 0) then
  begin
    if not db^.dbc^.GetSeq(db^.dbr, grid^.GetItemData(pos)) then
      LLShowError(@Self, IDS_DBOPFAILED, 'GetSeq', db^.dbc^.dbErrorNum)
    else
    begin
      isReferenced:= not db^.dbr^.OkayToMessWith;
      if (db^.dbr^.dbErrorNum <> 0) then
      begin
        proceed:= False;
        LLShowError(@Self, IDS_DBOPFAILED, 'OkayToMessWith', db^.dbr^.dbErrorNum);
      end;
      if proceed and (UDFlagFld > 0) then
      begin
        if not db^.dbr^.GetField(UDFlagFld, @userDef, SizeOf(userDef)) then
        begin
          proceed:= False;
          LLShowError(@Self, IDS_DBOPFAILED, 'GetField', db^.dbr^.dbErrorNum);
        end;
      end;
      if (proceed and (isReferenced or (not userDef))) then
      begin
        proceed:= False;
        InfoMsg(HWindow, SR(IDS_WARNING, p1, SizeOf(p1)-1), SR(IDS_CANTDELETEREC, p2, SizeOf(p2)-1));
      end;
      if proceed and YesNoMsg(HWindow, SR(IDS_CONFIRM, p1, SizeOf(p1)-1), SR(IDS_DEL_CUR_REC, p2, SizeOf(p2)-1)) then
      begin
        if not db^.dbc^.DeleteRec(db^.dbr) then
        begin
          if (db^.dbc^.dbErrorNum = dbErr_CantDeleteRecord) then
            InfoMsg(HWindow, SR(IDS_WARNING, p1, SizeOf(p1)-1), SR(IDS_CANTDELETEREC, p2, SizeOf(p2)-1))
          else
            LLShowError(@Self, IDS_DBOPFAILED, 'DeleteRec', db^.dbc^.dbErrorNum);
        end
        else
        begin
          grid^.DeleteString(pos);
          if (pos >= grid^.GetRowCount) then
            pos:= grid^.GetRowCount-1;
          grid^.SetSelIndex(pos);
          EnableButtons;
        end;
      end;
    end;
  end;
end;

procedure TDlgList.GrdList(var msg: TMessage);
begin
  if showSelect and (msg.lParamHI = LBN_DBLCLK) then
    OK(msg);
end;

procedure TDlgList.BtnDelete(var msg: TMessage);
begin
  DeleteData;
end;

procedure TDlgList.BtnAdd(var msg: TMessage);
begin
  if (DoEditDialog(@Self, db, listObject, idFld, idSize, descFld, descSize, UDFlagFld, False) = IDOK) then
  begin
    FillGrid(False, False, False);
    EnableButtons;
  end;
end;

procedure TDlgList.BtnEdit(var msg: TMessage);
begin
  EditData;
end;

procedure TDlgList.BtnPrint(var msg: TMessage);
var
  title     : array[0..50] of char;
  pstr      : array[0..200] of char;
  pd        : PPrnPrevDlg;
  pInfo     : PPrintInfo;
  lf        : TLogFont;
  aRow      : longint;
  aCol      : integer;
begin
  MakeScreenFont(lf, false, false);
  pInfo:= New(PPrintInfo, Init(lf, IniDefPointSize));

  with pInfo^ do
  begin
    header^.AddRow(r_BorderTop, 0);
    GetWindowText(HWindow, title, SizeOf(title)-1);
    header^.AddCell(title, 0, c_Stretch or c_Bold, -2);
    header^.AddCell(SR(IDS_PRINTEDON, pstr, SizeOf(pstr)-1), LT200, c_PageRight or c_Right or c_Date, 0);
    header^.AddRow(r_Normal, 0);

    footer^.AddRow(r_CenterRow or r_BorderTop, 0);
    footer^.AddCell(SR(IDS_PAGEPOFN, pstr, SizeOf(pstr)-1), LT300, c_Center or c_PageNum or c_NumPages, 0);

    grid^.GetPrintHeader(header, True);
    grid^.GetPrintBody(pInfo, False);

    pd:= New(PPrnPrevDlg, Init(@self, pInfo, title));
    application^.ExecDialog(pd);
  end;
  Dispose(pInfo, Done);
end;

constructor TDlgListSessions.Init(aParent: PWindowsObject;
                                  dlgResID: integer;
                                  aDB: PDBFile;
                                  calledFromMnem: boolean;
                                  aListObject: PListObject;
                                  buttonMode: byte;
                                  idFieldNumber: integer;
                                  idFieldSize: word;
                                  descFieldNumber: integer;
                                  descFieldSize: word;
                                  UDFlagFieldNumber: integer);
begin
  inherited Init( aParent, dlgResID, aDB, calledFromMnem, aListObject, buttonMode,
                  idFieldNumber, idFieldSize, descFieldNumber, descFieldSize, UDFlagFieldNumber);
  showSelectCurSession:= (buttonMode = modeEditOnly);
  showDisableCurSession:= showSelectCurSession;
  selectCurSessButton:= New(PButton, InitResource(@Self, BTN_SELCUR));
  disableCurSessButton:= New(PButton, InitResource(@Self, BTN_DISABLE));
  lblCurSession:= New(PLStatic, InitResource(@Self, LBL_CURSESSION, 0, False));
  txtCurSession:= New(PLStatic, InitResource(@Self, TXT_CURSESSION, 0, True));
end;

procedure TDlgListSessions.SetupWindow;
begin
  inherited SetupWindow;
  SetCurSessionText;
end;

procedure TDlgListSessions.EnableButtons;
begin
  inherited EnableButtons;
  ShowButton(selectCurSessButton^.HWindow, showSelectCurSession, (grid^.GetRowCount > 0));
  ShowButton(disableCurSessButton^.HWindow, showDisableCurSession, (grid^.GetRowCount > 0));
end;

procedure TDlgListSessions.EditData;
var
  pos       : longint;
  seq       : TSeqNum;
  pstr      : array[0..100] of char;
  isCurrent : boolean;
begin
  pos:= grid^.GetSelIndex;
  if (pos >= 0) then
  begin
    { isCurrent MUST be set before the inherited call }
    isCurrent:= (grid^.GetItemData(pos) = GetCurrentSession(pstr, SizeOf(pstr)-1));
    inherited EditData;
    if isCurrent then
    begin
      SetCurrentSession(grid^.GetItemData(pos));
      SetCurSessionText;
    end;
  end;
end;

procedure TDlgListSessions.DeleteData;
var
  pos       : longint;
  pstr      : array[0..100] of char;
  pstr20    : array[0..20] of char;
  pstr80    : array[0..80] of char;
begin
  pos:= grid^.GetSelIndex;
  if (pos >= 0) then
  begin
    if (grid^.GetItemData(pos) = GetCurrentSession(pstr, SizeOf(pstr)-1)) then
      InfoMsg(HWindow, SR(IDS_WARNING, pstr20, SizeOf(pstr20)-1), SR(IDS_CANTDELCURSESS, pstr80, SizeOf(pstr80)-1))
    else
      inherited DeleteData;
  end;
end;

procedure TDlgListSessions.SetCurSessionText;
var
  pstr  : array[0..100] of char;
begin
  if (GetCurrentSession(pstr, SizeOf(pstr)-1) = 0) then
    SR(IDS_SESSIONSDISABLED, pstr, SizeOf(pstr)-1);
  txtCurSession^.SetText(pstr);
end;

procedure TDlgListSessions.BtnSelectCurSession(var msg: TMessage);
var
  pos : longint;
begin
  pos:= grid^.GetSelIndex;
  if (pos >= 0) then
  begin
    SetCurrentSession(grid^.GetItemData(pos));
    SetCurSessionText;
  end;
end;

procedure TDlgListSessions.BtnDisableCurSession(var msg: TMessage);
begin
  SetCurrentSession(0);
  SetCurSessionText;
end;

constructor TDlgListDrugs.Init( aParent: PWindowsObject;
                                dlgResID: integer;
                                aDB: PDBFile;
                                calledFromMnem: boolean;
                                aListObject: PListObject;
                                buttonMode: byte;
                                idFieldNumber: integer;
                                idFieldSize: word;
                                descFieldNumber: integer;
                                descFieldSize: word;
                                UDFlagFieldNumber: integer);
begin
  inherited Init( aParent, dlgResID, aDB, calledFromMnem, aListObject, buttonMode,
                  idFieldNumber, idFieldSize, descFieldNumber, descFieldSize, UDFlagFieldNumber);
  showOptions:= showAdd;
  optionsButton:= New(PButton, InitResource(@Self, BTN_OPTIONS));
end;

procedure TDlgListDrugs.EnableButtons;
begin
  inherited EnableButtons;
  ShowButton(optionsButton^.HWindow, showOptions, (grid^.GetRowCount > 0));
end;

procedure TDlgListDrugs.BtnOptions(var msg: TMessage);
var
  id : PChar;
  pos: longint;
begin
  GetMem(id, idSize);
  if (id = nil) then
    LLShowError(@Self, IDS_MEMFAILURE, nil, 0)
  else
  begin
    StrCopy(id, '');
    pos:= grid^.GetSelIndex;
    if (pos >= 0) then
      grid^.GetData(idFld, grid^.GetSelIndex, id, idSize-1);
    db^.dbr^.ClearRecord;
    if (application^.ExecDialog(New(PDlgDrugOptions, Init(@Self,
                                                          db,
                                                          listObject,
                                                          idFld,
                                                          idSize,
                                                          descFld,
                                                          descSize,
                                                          UDFlagFld))) = IDOK) then
    begin
      if not db^.dbr^.PutFieldAsStr(idFld, id) then
        LLShowError(@Self, IDS_DBOPFAILED, 'PutFieldAsStr', db^.dbr^.dbErrorNum)
      else
      begin
        FillGrid(True, False, False);
        EnableButtons;
      end;
    end;
    FreeMem(id, idSize);
  end;
end;

constructor TDlgDrugOptions.Init( aParent: PWindowsObject;
                                  aDB: PDBFile;
                                  aListObject: PListObject;
                                  idFieldNumber: integer;
                                  idFieldSize: word;
                                  descFieldNumber: integer;
                                  descFieldSize: word;
                                  UDFlagFieldNumber: integer);
begin
  inherited Init( aParent, DLG_DRUGOPTIONS, aDB, True, aListObject,
                  idFieldNumber, idFieldSize, descFieldNumber, descFieldSize, UDFlagFieldNumber);
  suppAllButton:= New(PButton, InitResource(@Self, BTN_SUPPALL));
  suppNoneButton:= New(PButton, InitResource(@Self, BTN_SUPPNONE));
  suppToggleButton:= New(PButton, InitResource(@Self, BTN_SUPPTOGGLE));
  suppInvertButton:= New(PButton, InitResource(@Self, BTN_SUPPINVERT));
  sortUpButton:= New(PButton, InitResource(@Self, BTN_SORTUP));
  sortDownButton:= New(PButton, InitResource(@Self, BTN_SORTDOWN));
  sortResetButton:= New(PButton, InitResource(@Self, BTN_SORTRESET));
end;

procedure TDlgDrugOptions.SetupWindow;
var
  pstr : array[0..80] of char;
begin
  inherited SetupWindow;
  SetWindowText(HWindow, SR(IDS_DRUGOPTIONS, pstr, SizeOf(pstr)-1));
end;

function TDlgDrugOptions.CanClose: boolean;
var
  retVal  : boolean;
  aRow    : longint;
  pstr1   : array[0..20] of char;
  pstr2   : array[0..20] of char;
  seq     : TSeqNum;
  changed : boolean;
begin
  retVal:= inherited CanClose;
  if retVal then
  begin
    for aRow:= 0 to grid^.GetRowCount-1 do
    begin
      changed:= False;
      seq:= grid^.GetItemData(aRow);
      if not db^.dbc^.GetSeq(db^.dbr, seq) then
      begin
        LLShowError(@Self, IDS_DBOPFAILED, 'GetSeq', db^.dbc^.dbErrorNum);
        retVal:= False;
        break;
      end;
      grid^.GetData(DBDRUGSortNum, aRow, pstr1, SizeOf(pstr1)-1);
      db^.dbr^.GetFieldAsStr(DBDRUGSortNum, pstr2, SizeOf(pstr2)-1);
      if (db^.dbr^.dbErrorNum <> 0) then
      begin
        LLShowError(@Self, IDS_DBOPFAILED, 'GetFieldAsStr', db^.dbr^.dbErrorNum);
        retVal:= False;
        break;
      end;
      if (StrComp(pstr1, pstr2) <> 0) then
      begin
        changed:= True;
        if not db^.dbr^.PutFieldAsStr(DBDRUGSortNum, pstr1) then
        begin
          LLShowError(@Self, IDS_DBOPFAILED, 'PutFieldAsStr', db^.dbr^.dbErrorNum);
          retVal:= False;
          break;
        end;
      end;
      grid^.GetData(DBDRUGSupp, aRow, pstr1, SizeOf(pstr1)-1);
      db^.dbr^.GetFieldAsStr(DBDRUGSupp, pstr2, SizeOf(pstr2)-1);
      if (db^.dbr^.dbErrorNum <> 0) then
      begin
        LLShowError(@Self, IDS_DBOPFAILED, 'GetFieldAsStr', db^.dbr^.dbErrorNum);
        retVal:= False;
        break;
      end;
      if (StrComp(pstr1, pstr2) <> 0) then
      begin
        changed:= True;
        if not db^.dbr^.PutFieldAsStr(DBDRUGSupp, pstr1) then
        begin
          LLShowError(@Self, IDS_DBOPFAILED, 'PutFieldAsStr', db^.dbr^.dbErrorNum);
          retVal:= False;
          break;
        end;
      end;
      if changed then
      begin
        if not db^.dbc^.UpdateRec(db^.dbr) then
        begin
          LLShowError(@Self, IDS_DBOPFAILED, 'UpdateRec', db^.dbc^.dbErrorNum);
          retVal:= False;
          break;
        end;
      end;
    end;
  end;
  CanClose:= retVal;
end;

procedure TDlgDrugOptions.EnableButtons;
begin
  inherited EnableButtons;
  ShowButton(suppAllButton^.HWindow, True, (grid^.GetRowCount > 0));
  ShowButton(suppNoneButton^.HWindow, True, (grid^.GetRowCount > 0));
  ShowButton(suppToggleButton^.HWindow, True, (grid^.GetSelIndex >= 0));
  ShowButton(suppInvertButton^.HWindow, True, (grid^.GetRowCount > 0));
  ShowButton(sortUpButton^.HWindow, True, (grid^.GetSelIndex >= 0));
  ShowButton(sortDownButton^.HWindow, True, (grid^.GetSelIndex >= 0));
  ShowButton(sortResetButton^.HWindow, True, (grid^.GetRowCount > 0));
end;

procedure TDlgDrugOptions.SuppAll(supp: boolean);
var
  aRow : longint;
  pstr : array[0..20] of char;
begin
  for aRow:= 0 to grid^.GetRowCount-1 do
  begin
    grid^.SetData(DBDRUGSupp, aRow, BoolToStr(supp, pstr, SizeOf(pstr)-1));
  end;
end;

procedure TDlgDrugOptions.SuppToggle(aRow: longint);
var
  pstr : array[0..20] of char;
  err  : integer;
begin
  grid^.GetData(DBDRUGSupp, aRow, pstr, SizeOf(pstr)-1);
  grid^.SetData(DBDRUGSupp, aRow, BoolToStr((not StrToBool(pstr, err)), pstr, SizeOf(pstr)-1));
end;

procedure TDlgDrugOptions.BtnSuppAll(var msg: TMessage);
begin
  SuppAll(True);
end;

procedure TDlgDrugOptions.BtnSuppNone(var msg: TMessage);
begin
  SuppAll(False);
end;

procedure TDlgDrugOptions.BtnSuppToggle(var msg: TMessage);
begin
  SuppToggle(grid^.GetSelIndex);
end;

procedure TDlgDrugOptions.BtnSuppInvert(var msg: TMessage);
var
  aRow : longint;
begin
  for aRow:= 0 to grid^.GetRowCount-1 do
    SuppToggle(aRow);
end;

procedure TDlgDrugOptions.SwapRows(row1, row2: longint);
var
  pstr1 : array[0..20] of char;
  pstr2 : array[0..20] of char;
begin
  grid^.SwapRows(row1, row2);
  grid^.GetData(DBDRUGSortNum, row1, pstr1, SizeOf(pstr1)-1);
  grid^.GetData(DBDRUGSortNum, row2, pstr2, SizeOf(pstr2)-1);
  grid^.SetData(DBDRUGSortNum, row1, pstr2);
  grid^.SetData(DBDRUGSortNum, row2, pstr1);
end;

procedure TDlgDrugOptions.BtnSortUp(var msg: TMessage);
var
  aRow  : longint;
begin
  aRow:= grid^.GetSelIndex;
  if (aRow > 0) then
  begin
    SwapRows(aRow, aRow-1);
    grid^.SetSelIndex(aRow-1);
  end;
end;

procedure TDlgDrugOptions.BtnSortDown(var msg: TMessage);
var
  aRow : longint;
begin
  aRow:= grid^.GetSelIndex;
  if (aRow < grid^.GetRowCount-1) then
  begin
    SwapRows(aRow, aRow+1);
    grid^.SetSelIndex(aRow+1);
  end;
end;

procedure TDlgDrugOptions.BtnSortReset(var msg: TMessage);
var
  aRow : longint;
  pstr : array[0..20] of char;
begin
  for aRow:= 0 to grid^.GetRowCount-1 do
  begin
    grid^.GetData(DBDRUGDfltSortNum, aRow, pstr, SizeOf(pstr)-1);
    grid^.SetData(DBDRUGSortNum, aRow, pstr);
  end;
  SortDBGrid(grid, DBDRUGSortNum, 20, True);
end;

constructor TDlgListOrganisms.Init( aParent: PWindowsObject;
                                    dlgResID: integer;
                                    aDB: PDBFile;
                                    calledFromMnem: boolean;
                                    aListObject: PListObject;
                                    buttonMode: byte;
                                    idFieldNumber: integer;
                                    idFieldSize: word;
                                    descFieldNumber: integer;
                                    descFieldSize: word;
                                    UDFlagFieldNumber: integer);
begin
  inherited Init( aParent, dlgResID, aDB, calledFromMnem, aListObject, buttonMode,
                  idFieldNumber, idFieldSize, descFieldNumber, descFieldSize, UDFlagFieldNumber);
  lblSets:= New(PLStatic, InitResource(@Self, LBL_SETS, 0, False));
  cmbSets:= New(PComboBox, InitResource(@Self, CMB_SETS, 0));
  chkUDOnly:= New(PLCheckBox, InitResource(@Self, CHK_UDONLY, False));
  rdoSortBy:= New(PRadioGroup, Init(@Self, GRP_SORTBY, 0, False));
  rdoSortBy^.AddToGroup(RDO_UNSORTED, RDO_UNSORTED);
  rdoSortBy^.AddToGroup(RDO_ID, RDO_ID);
  rdoSortBy^.AddToGroup(RDO_NAME, RDO_NAME);
  rdoSortBy^.AddToGroup(RDO_SET, RDO_SET);
  rdoNameDisplayed:= New(PRadioGroup, Init(@Self, GRP_NAMEDISPLAYED, 0, False));
  rdoNameDisplayed^.AddToGroup(RDO_SHORTNAME, RDO_SHORTNAME);
  rdoNameDisplayed^.AddToGroup(RDO_LONGNAME, RDO_LONGNAME);
end;

destructor TDlgListOrganisms.Done;
begin
  if (rdoSortBy <> nil) then
    Dispose(rdoSortBy, Done);
  if (rdoNameDisplayed <> nil) then
    Dispose(rdoNameDisplayed, Done);

  inherited Done;
end;

procedure TDlgListOrganisms.SetupWindow;
var
  pstr : array[0..80] of char;
begin
  inherited SetupWindow;
  cmbSets^.AddString(SR(IDS_ALL, pstr, SizeOf(pstr)-1));
  cmbSets^.AddString(GetSetAsStr(1, pstr, SizeOf(pstr)-1));
  cmbSets^.AddString(GetSetAsStr(2, pstr, SizeOf(pstr)-1));
  cmbSets^.AddString(GetSetAsStr(3, pstr, SizeOf(pstr)-1));
  cmbSets^.AddString(GetSetAsStr(4, pstr, SizeOf(pstr)-1));
  cmbSets^.AddString(GetSetAsStr(6, pstr, SizeOf(pstr)-1));
  cmbSets^.SetSelIndex(0);
  rdoSortBy^.SetSelected(RDO_UNSORTED);
  rdoNameDisplayed^.SetSelected(RDO_SHORTNAME);
end;

function  TDlgListOrganisms.SetupGrid: boolean;
var
  retVal  : boolean;
  pstr    : array[0..50] of char;
begin
  retVal:= inherited SetupGrid;
  if retVal then
  begin
    grid^.SetHeader(db^.dbd^.NumFields+1, SR(IDS_FAMILY, pstr, SizeOf(pstr)-1));
    grid^.SetColumnWidth(db^.dbd^.NumFields+1, 100);
    grid^.SetColumnWidth(DBORGLName, 0);
    if (lastVisibleCol = DBORGLName) then
      lastVisibleCol:= DBORGSName;
    grid^.StretchColumn(DBORGSName);
  end;
  SetupGrid:= retVal;
end;

function TDlgListOrganisms.FillGrid(setPosition: boolean; optimize: boolean; UDOnly: boolean): boolean;
var
  retVal  : boolean;
  redraw  : boolean;
  hourGlass: PWaitCursor;
  id: PChar;
begin
  hourGlass:= new(PWaitCursor, Init);
  redraw:= grid^.GetRedraw;
  grid^.SetRedraw(False);

  GetMem(id, idSize);
  if (id = nil) then
  begin
    retVal:= False;
    LLShowError(@Self, IDS_MEMFAILURE, nil, 0);
  end
  else
  begin
    db^.dbr^.GetFieldAsStr(idFld, id, idSize-1);
    if (db^.dbr^.dbErrorNum <> 0) then
    begin
      retVal:= False;
      LLShowError(@Self, IDS_DBOPFAILED, 'GetFieldAsStr', db^.dbr^.dbErrorNum);
    end
    else
    begin
      retVal:= inherited FillGrid(setPosition, optimize, (chkUDOnly^.GetCheck = bf_Checked));
      if retVal then
      begin
        SortGrid;
        if (not setPosition) or (StrLen(id) = 0) then
          grid^.SetSelIndex(0);
      end;
    end;
    FreeMem(id, idSize);
  end;

  grid^.SetRedraw(redraw);
  if (hourGlass <> nil) then
    Dispose(hourGlass, Done);
  FillGrid:= retVal;
end;

function  TDlgListOrganisms.FillGridRow(aRow: longint): boolean;
var
  retVal    : boolean;
  pstr1     : array[0..50] of char;
  pstr2     : array[0..50] of char;
  orgSet    : byte;
begin
  retVal:= inherited FillGridRow(aRow);
  if retVal and not db^.dbr^.GetField(DBORGSet, @orgSet, SizeOf(orgSet)) then
  begin
    retVal:= False;
    LLShowError(@Self, IDS_DBOPFAILED, 'GetField', db^.dbr^.dbErrorNum);
  end;
  if retVal then
  begin
    grid^.SetData(db^.dbd^.NumFields+1, aRow, GetSetAsStr(orgSet, pstr1, SizeOf(pstr1)-1));
    if (cmbSets^.GetSelIndex > 0) then
    begin
      cmbSets^.GetText(pstr2, SizeOf(pstr2)-1);
      if (StrComp(pstr1, pstr2) <> 0) then
        grid^.DeleteString(aRow);
    end;
  end;
  FillGridRow:= retVal;
end;

procedure TDlgListOrganisms.WMFillGrid(var msg: TMessage);
var
  redraw: boolean;
begin
  rdoSortBy^.SetSelected(RDO_NAME);
  inherited WMFillGrid(msg);
end;

procedure TDlgListOrganisms.SortGrid;
var
  hourGlass: PWaitCursor;
begin
  hourGlass:= new(PWaitCursor, Init);
  case rdoSortBy^.SelectedVal of
    RDO_UNSORTED:
      begin
        SortDBGrid(grid, 0, SeqStrLen, True);
        grid^.SetSearchColumn(0);
      end;
    RDO_ID:
      begin
        SortDBGrid(grid, DBORGOrg, db^.dbd^.FieldSizeForStr(DBORGOrg), True);
        grid^.SetSearchColumn(DBORGOrg);
      end;
    RDO_NAME:
      if (rdoNameDisplayed^.SelectedVal = RDO_SHORTNAME) then
      begin
        SortDBGrid(grid, DBORGSName, db^.dbd^.FieldSizeForStr(DBORGSName), False);
        grid^.SetSearchColumn(DBORGSName);
      end
      else
      begin
        SortDBGrid(grid, DBORGLName, db^.dbd^.FieldSizeForStr(DBORGLName), False);
        grid^.SetSearchColumn(DBORGLName);
      end;
    RDO_SET:
      begin
        SortDBGrid(grid, db^.dbd^.NumFields+1, 50, False);
        grid^.SetSearchColumn(db^.dbd^.NumFields+1);
      end;
  end;
  if (hourGlass <> nil) then
    Dispose(hourGlass, Done);
end;

procedure TDlgListOrganisms.EditData;
begin
  inherited EditData;
  if (grid^.GetSelIndex = -1) and (grid^.GetRowCount > 0) then
    grid^.SetSelIndex(grid^.GetRowCount-1);
  EnableButtons;
end;

procedure TDlgListOrganisms.ComboSets(var msg: TMessage);
begin
  if (msg.lParamHI = CBN_SELCHANGE) then
  begin
    FillGrid(False, False, (chkUDOnly^.GetCheck = bf_Checked));
    EnableButtons;
  end;
end;

procedure TDlgListOrganisms.CheckUDOnly(var msg: TMessage);
begin
  if (msg.lParamHI = BN_CLICKED) then
  begin
    FillGrid(False, False, (chkUDOnly^.GetCheck = bf_Checked));
    EnableButtons;
  end;
end;

procedure TDlgListOrganisms.RadioUnsorted(var msg: TMessage);
begin
  if (msg.lParamHI = BN_CLICKED) then
    SortGrid;
end;

procedure TDlgListOrganisms.RadioID(var msg: TMessage);
begin
  if (msg.lParamHI = BN_CLICKED) then
    SortGrid;
end;

procedure TDlgListOrganisms.RadioName(var msg: TMessage);
begin
  if (msg.lParamHI = BN_CLICKED) then
    SortGrid;
end;

procedure TDlgListOrganisms.RadioSet(var msg: TMessage);
begin
  if (msg.lParamHI = BN_CLICKED) then
    SortGrid;
end;

procedure TDlgListOrganisms.RadioShortName(var msg: TMessage);
begin
  if (msg.lParamHI = BN_CLICKED) then
    if (grid^.GetColumnWidth(DBORGSName) = 0) then
    begin
      grid^.SetColumnWidth(DBORGSName, grid^.GetColumnWidth(DBORGLName));
      grid^.SetColumnWidth(DBORGLName, 0);
      if (lastVisibleCol = DBORGLName) then
        lastVisibleCol:= DBORGSName;
      if (rdoSortBy^.SelectedVal = RDO_NAME) then
        SortGrid;
    end;
end;

procedure TDlgListOrganisms.RadioLongName(var msg: TMessage);
begin
  if (msg.lParamHI = BN_CLICKED) then
    if (grid^.GetColumnWidth(DBORGLName) = 0) then
    begin
      grid^.SetColumnWidth(DBORGLName, grid^.GetColumnWidth(DBORGSName));
      grid^.SetColumnWidth(DBORGSName, 0);
      if (lastVisibleCol = DBORGSName) then
        lastVisibleCol:= DBORGLName;
      if (rdoSortBy^.SelectedVal = RDO_NAME) then
        SortGrid;
    end;
end;

constructor TDlgEditMnem.Init(aParent: PWindowsObject;
                              dlgResID: integer;
                              editMode: boolean;
                              aDB: PDBFile;
                              aListObj: PListObject;
                              idFieldNumber: integer;
                              idFieldSize: word;
                              descFieldNumber: integer;
                              descFieldSize: word;
                              UDFlagFieldNumber: integer);
begin
  inherited Init(aParent, MakeIntResource(dlgResID));

  editing:= editMode;
  db:= aDB;
  listObj:= aListObj;
  idFld:= idFieldNumber;
  idSize:= idFieldSize;
  descFld:= descFieldNumber;
  descSize:= descFieldSize;
  UDFlagFld:= UDFlagFieldNumber;
  dataSaved:= false;
  if not editing then
    db^.dbr^.ClearRecord;

  txtMode:= New(PStatic, InitResource(@Self, TXT_MODE, 0));
  saveButton:= New(PButton, InitResource(@Self, BTN_SAVE));
  flds:= New(PCollection, Init(25, 10));
  if (flds = nil) then
  begin
    Done;
    Fail;
  end;

  edtID:= LinkUIFld(idFld, True, False, EDT_ID, TXT_ID, db^.dbd);
  edtDesc:= LinkUIFld(descFld, True, False, EDT_DESC, TXT_DESC, db^.dbd);
end;

destructor TDlgEditMnem.Done;
begin
  if (flds <> nil) then
    Dispose(flds, Done);

  inherited Done;
end;

procedure TDlgEditMnem.SetupWindow;
var
  pstr  : array[0..50] of char;
  fDesc : array[0..maxFileDescLen] of char;

    procedure ShowIt(aFld: PUIFld); far;
    begin
      aFld^.Show(True);
    end;

  procedure GetData(aFld: PUIFld); far;
  begin
      aFld^.XferRecToCtl(db^.dbr)
  end;

begin
  inherited SetupWindow;

  SetWindowText(HWindow, db^.dbd^.FileDesc(fDesc, maxFileDescLen));
  if editing then
  begin
    txtMode^.SetText(SR(IDS_EDIT, pstr, SizeOf(pstr)-1));
    flds^.ForEach(@GetData);  { get data and put into fields }
  end
  else
  begin
    txtMode^.SetText(SR(IDS_New, pstr, SizeOf(pstr)-1));
  end;
  flds^.ForEach(@ShowIt);  { show all fields }
end;

procedure TDlgEditMnem.Cancel(var msg: TMessage);
begin
  if CheckSave then
  begin
    { if data was saved then then we want to let the parent know, send IDOK back }
    if dataSaved then
      EndDlg(IDOK)
    else
      inherited Cancel(msg);
  end;
end;

function TDlgEditMnem.DataChanged: boolean;
{- compare the current data with the originally loaded data.  If any data has
   changed, then return true }

  function IsModified(aFld: PUIFld): boolean; far;
  begin
    IsModified:= aFld^.IsModified(db^.dbr)
  end;

begin
  DataChanged:= (flds^.FirstThat(@IsModified) <> nil);
end;

function TDlgEditMnem.CheckSave: boolean;
var
  ret     : boolean;
  pstr    : array[0..200] of char;
  p2      : array[0..50] of char;
  k       : integer;
begin
  ret:= True;
  {- first see if any data has changed }
  if DataChanged then
  begin
    k:= YesNoCancelMsg(HWindow, SR(IDS_CONFIRM, p2, 50), SR(IDS_CONFIRMSAVE, pstr, 200));
    if k = IDYES then
      ret:= SaveData(True)
    else
      ret:= k <> IDCANCEL;
  end;
  CheckSave:= ret;
end;

function TDlgEditMnem.LinkUIFld(aFldNum: integer; isReq, isCheckFld: boolean;
                                aResID, aLblID: word; dbd: PDBReadDesc): PUIFld;
var
  fld : PUIFld;
begin
  if isCheckFld then
    fld:= New(PUICheckFld, Init(@Self, dbd, aFldNum, aResID, True))
  else
    fld:= New(PUIFld, Init(@Self, dbd, listObj, isReq, aFldNum, aResID, aLblID));
  if (fld = nil) then
  begin
    LLShowError(@Self, IDS_UIFLDINITERR, nil, 0);
    Halt;
  end;
  flds^.Insert(fld); { add field to field list collection }
  LinkUIFld:= fld;
end;

function TDlgEditMnem.SaveData(clearRecord: boolean): boolean;
var
  ret     : boolean;

  function ValidateScreen: boolean;
  {- check screen for validity }
  var
    sret    : boolean;
    fld     : PUIFld;
    errNum  : integer;

    function IsInvalid(aFld: PUIFld): boolean; far;
    begin
      errNum:= aFld^.Format;
      IsInvalid:= errNum <> 0;
    end;

  begin
    sret:= true;
    fld:= flds^.FirstThat(@IsInvalid);
    if (fld <> nil) then
    begin {- some other error }
      ShowUIFldError(@self, errNum, fld);
      sRet:= false;
    end;
    ValidateScreen:= sret;
  end;

  function SaveIt: boolean;
  var
    sRet  : boolean;
    tBool : boolean;
    eNum  : integer;
    seq   : TSeqNum;

    procedure XFerToRec(aFld: PUIFld); far;
    begin
      aFld^.XFerCtlToRec(db^.dbr);
    end;

  begin
    sRet:= true;

    {- now transfer all fields to the record buffer }
    flds^.ForEach(@XFerToRec);  {- get specimen fields }

    if editing then
      db^.dbc^.UpdateRec(db^.dbr)
    else
    begin
      if (UDFlagFld > 0) then
      begin
        tBool:= True;
        if not db^.dbr^.PutField(UDFlagFld, @tBool) then
        begin
          LLShowError(@Self, IDS_DBOPFAILED, 'PutField', db^.dbr^.dbErrorNum);
          sRet:= False;
        end;
      end;
      if sRet then
        db^.dbc^.InsertRec(db^.dbr);
    end;

    if sRet then
    begin
      eNum:= 0;
      {- special error handling for common errors }
      case db^.dbc^.dbErrorNum of
        MOD_BTRV + 5: eNum:= IDS_DUPREC;
        else
          eNum:= IDS_CANNOTSAVE;
      end;

      if db^.dbc^.dbErrorNum <> 0 then
      begin
        LLShowError(@Self, eNum, nil, db^.dbc^.dbErrorNum);
        edtID^.FocusFld;
        sret:= false;
      end;
    end;

    SaveIt:= sret;
  end;


begin
  ret:= True;
  if ret then
    ret:= ValidateScreen;
  if ret then
    ret:= SaveIt;
  if ret then
  begin
    ClearFields;
    if ((not editing) and clearRecord) then
      db^.dbr^.ClearRecord;
  end;
  dataSaved:= dataSaved or ret;

  SaveData:= ret;
end;

procedure TDlgEditMnem.BtnSave(var msg: TMessage);
begin
  if SaveData(True) and editing then
    EndDlg(IDOK);
end;

procedure TDlgEditMnem.ClearFields;
{- clear all fields on screen }

  procedure ClearIt(aFld: PUIFld); far;
  begin
    aFld^.Clear;
  end;

begin
  flds^.ForEach(@ClearIt);
  edtID^.FocusFld; { focus on the ID field }
end;

procedure TDlgEditMnem.WMCtlColor(var msg: TMessage);
begin
  if (txtMode <> nil) and (msg.lParamLo = txtMode^.HWindow) then
  begin
    SetTextColor(msg.wParam, RGB($FF,0,0));
    msg.result:= GetStockObject(WHITE_BRUSH);
    SetWindowLong(HWindow, DWL_MSGRESULT, msg.result);  {- set message result (for dialogs) }
  end
  else
    DefWndProc(msg);
end;

constructor TDlgEditSource.Init(aParent: PWindowsObject;
                              dlgResID: integer;
                              editMode: boolean;
                              aDB: PDBFile;
                              aListObj: PListObject;
                              idFieldNumber: integer;
                              idFieldSize: word;
                              descFieldNumber: integer;
                              descFieldSize: word;
                              UDFlagFieldNumber: integer);
begin
  inherited Init( aParent, dlgResID, editMode, aDB, aListObj, idFieldNumber,
                  idFieldSize, descFieldNumber, descFieldSize, UDFlagFieldNumber);
  chkUrnSrc:= LinkUIFld(DBSRCUrine, False, True, CHK_URINE_SOURCE, 0, db^.dbd);
end;

constructor TDlgEditDrug.Init(aParent: PWindowsObject;
                              dlgResID: integer;
                              editMode: boolean;
                              aDB: PDBFile;
                              aListObj: PListObject;
                              idFieldNumber: integer;
                              idFieldSize: word;
                              descFieldNumber: integer;
                              descFieldSize: word;
                              UDFlagFieldNumber: integer;
                              altOnly: boolean);
begin
  inherited Init( aParent, dlgResID, editMode, aDB, aListObj, idFieldNumber,
                  idFieldSize, descFieldNumber, descFieldSize, UDFlagFieldNumber);
  edtID^.UseUpperOnly(false);
  edtDesc2:= LinkUIFld(DBDRUGAltAbbr, True, False, EDT_DESC2, TXT_DESC2, db^.dbd);
  limited:= altOnly;
end;

procedure TDlgEditDrug.SetupWindow;
begin
  inherited SetupWindow;
  if limited then
  begin
    edtID^.Enable(False);
    edtDesc^.Enable(False);
  end;
end;

function TDlgEditDrug.SaveData(clearRecord: boolean): boolean;
var
  retVal : boolean;
begin
  retVal:= TransferIDToAltAbbr;
  if retVal and (not editing) then
    retVal:= AssignHiddenFields;
  if retVal then
    retVal:= inherited SaveData(clearRecord);
  SaveData:= retVal;
end;

function TDlgEditDrug.AssignHiddenFields: boolean;
var
  retVal      : boolean;
  sortNum     : longint;
  sortNumDflt : longint;
  tBool       : boolean;
begin
  retVal:= CalcSortNums(sortNum, sortNumDflt);
  if retVal then
  begin
    if not db^.dbr^.PutField(DBDRUGSortNum, @sortNum) then
    begin
      LLShowError(@Self, IDS_DBOPFAILED, 'PutField', db^.dbr^.dbErrorNum);
      retVal:= False;
    end;
  end;
  if retVal then
  begin
    if not db^.dbr^.PutField(DBDRUGDfltSortNum, @sortNumDflt) then
    begin
      LLShowError(@Self, IDS_DBOPFAILED, 'PutField', db^.dbr^.dbErrorNum);
      retVal:= False;
    end;
  end;
  if retVal then
  begin
    tBool:= False;
    if not db^.dbr^.PutField(DBDRUGSupp, @tBool) then
    begin
      LLShowError(@Self, IDS_DBOPFAILED, 'PutField', db^.dbr^.dbErrorNum);
      retVal:= False;
    end;
  end;
  AssignHiddenFields:= retVal;
end;

function  TDlgEditDrug.CalcSortNums(var sortNum, sortNumDflt: longint): boolean;
var
  retVal  : boolean;
  curKey  : integer;
begin
  retVal:= True;
  sortNum:= 1;
  sortNumDflt:= 1;
  if (db^.dbc^.NumRecords > 0) then
  begin
    curKey:= db^.dbc^.GetCurKeyNum;
    if not db^.dbc^.SetCurKeyNum(DBDRUG_DFLTSORT_KEY) then
    begin
      LLShowError(@Self, IDS_DBOPFAILED, 'SetCurKeyNum', db^.dbc^.dbErrorNum);
      retVal:= False;
    end;
    if retVal then
    begin
      if db^.dbc^.GetLast(db^.dbr) then
        if not db^.dbr^.GetField(DBDRUGDfltSortNum, @sortNumDflt, SizeOf(sortNumDflt)) then
        begin
          LLShowError(@Self, IDS_DBOPFAILED, 'GetField', db^.dbr^.dbErrorNum);
          retVal:= False;
        end
        else
          Inc(sortNumDflt);
    end;
    if retVal then
    begin
      if not db^.dbc^.SetCurKeyNum(DBDRUG_SORT_KEY) then
      begin
        LLShowError(@Self, IDS_DBOPFAILED, 'SetCurKeyNum', db^.dbc^.dbErrorNum);
        retVal:= False;
      end;
      if retVal then
      begin
        if db^.dbc^.GetLast(db^.dbr) then
          if not db^.dbr^.GetField(DBDRUGSortNum, @sortNum, SizeOf(sortNum)) then
          begin
            LLShowError(@Self, IDS_DBOPFAILED, 'GetField', db^.dbr^.dbErrorNum);
            retVal:= False;
          end
          else
            Inc(sortNum);
      end;
    end;
    if not db^.dbc^.SetCurKeyNum(curKey) then
    begin
      LLShowError(@Self, IDS_DBOPFAILED, 'SetCurKeyNum', db^.dbc^.dbErrorNum);
      retVal:= False;
    end;
  end;
  CalcSortNums:= retVal;
end;

procedure TDlgEditDrug.WMLostFocus(var msg: TMessage);
var
  fld  : PUIFld;
begin
  fld:= PUIFld(msg.lParam); { get field pointer to fld losing focus }
  if (fld^.GetID = EDT_ID) and (not TransferIDToAltAbbr) then
    fld^.FocusFld;
  SetWindowLong(HWindow, DWL_MSGRESULT, 0);  {- set message result (for dialogs) }
end;

function TDlgEditDrug.TransferIDToAltAbbr: boolean;
var
  retVal: boolean;
  fld1  : PUIFld;
  fld2  : PUIFld;
  pstr  : array[0..100] of char;

  function IsAbbrFld(aFld: PUIFld): boolean; far;
  begin
    IsAbbrFld:= (aFld^.GetID = EDT_ID);
  end;

  function IsAltAbbrFld(aFld: PUIFld): boolean; far;
  begin
    IsAltAbbrFld:= (aFld^.GetID = EDT_DESC2);
  end;

begin
  retVal:= True;
  fld1:= flds^.FirstThat(@IsAbbrFld);
  fld2:= flds^.FirstThat(@IsAltAbbrFld);
  if (fld1 <> nil) and (not fld1^.IsEmpty) and (fld2 <> nil) and (fld2^.IsEmpty) then
  begin
    fld1^.XFerCtlToRec(db^.dbr);
    db^.dbr^.GetFieldAsStr(fld1^.FldNumber, pstr, SizeOf(pstr)-1);
    if (db^.dbr^.dbErrorNum <> 0) then
    begin
      LLShowError(@Self, IDS_DBOPFAILED, 'GetFieldAsStr', db^.dbr^.dbErrorNum);
      retVal:= False;
    end;
    if retVal and (not db^.dbr^.PutFieldAsStr(fld2^.FldNumber, pstr)) then
    begin
      LLShowError(@Self, IDS_DBOPFAILED, 'PutFieldAsStr', db^.dbr^.dbErrorNum);
      retVal:= False;
    end;
    if retVal then
      fld2^.XferRecToCtl(db^.dbr);
  end;
  TransferIDToAltAbbr:= retVal;
end;

constructor TDlgEditOrganism.Init(aParent: PWindowsObject;
                                  dlgResID: integer;
                                  editMode: boolean;
                                  aDB: PDBFile;
                                  aListObj: PListObject;
                                  idFieldNumber: integer;
                                  idFieldSize: word;
                                  descFieldNumber: integer;
                                  descFieldSize: word;
                                  UDFlagFieldNumber: integer);
begin
  inherited Init( aParent, dlgResID, editMode, aDB, aListObj, idFieldNumber,
                  idFieldSize, descFieldNumber, descFieldSize, UDFlagFieldNumber);
  edtDesc2:= LinkUIFld(DBORGLName, True, False, EDT_DESC2, TXT_DESC2, db^.dbd);
  lblSet:= New(PLStatic, InitResource(@Self, TXT_SET, 0, False));
  lblFamily:= New(PLStatic, InitResource(@Self, TXT_FAMILY, 0, False));
  cmbSet:= New(PComboBox, InitResource(@Self, CMB_SET, 0));
  cmbFamily:= New(PComboBox, InitResource(@Self, CMB_FAMILY, 0));
end;

procedure TDlgEditOrganism.SetupWindow;
var
  pstr    : array[0..80] of char;
  tSet    : byte;
  tFamily : byte;
begin
  inherited SetupWindow;
  cmbSet^.AddString(GetSetAsStr(1, pstr, SizeOf(pstr)-1));
  cmbSet^.AddString(GetSetAsStr(2, pstr, SizeOf(pstr)-1));
  cmbSet^.AddString(GetSetAsStr(3, pstr, SizeOf(pstr)-1));
  cmbSet^.AddString(GetSetAsStr(4, pstr, SizeOf(pstr)-1));
  cmbSet^.AddString(GetSetAsStr(6, pstr, SizeOf(pstr)-1));
  if editing then
  begin
    if not db^.dbr^.GetField(DBORGSet, @tSet, SizeOf(tSet)) then
    begin
      LLShowError(@Self, IDS_DBOPFAILED, 'GetField', db^.dbr^.dbErrorNum);
      EndDlg(IDCANCEL);
    end;
    if not db^.dbr^.GetField(DBORGFamily, @tFamily, SizeOf(tFamily)) then
    begin
      LLShowError(@Self, IDS_DBOPFAILED, 'GetField', db^.dbr^.dbErrorNum);
      EndDlg(IDCANCEL);
    end;
  end
  else
  begin
    tSet:= 1;
    tFamily:= 1;
  end;
  SelectSet(tSet);
  SelectFamily(tFamily);
end;

function TDlgEditOrganism.DataChanged: boolean;
{- compare the current data with the originally loaded data.  If any data has
   changed, then return true }
var
  retVal  : boolean;
  tSet1   : byte;
  tFamily1: byte;
  tSet2   : byte;
  tFamily2: byte;
begin
  retVal:= inherited DataChanged;
  if not retVal then
  begin
    case cmbSet^.GetSelIndex of
      0:  tSet1:= 1;
      1:  tSet1:= 2;
      2:  tSet1:= 3;
      3:  tSet1:= 4;
      4:  tSet1:= 6;
    end;
    tFamily1:= cmbFamily^.GetSelIndex+1;

    if (not retVal) then
    begin
      if not db^.dbr^.GetField(DBORGSet, @tSet2, SizeOf(tSet2)) then
        LLShowError(@Self, IDS_DBOPFAILED, 'GetField', db^.dbr^.dbErrorNum)
      else
        retVal:= (editing and (tSet1 <> tSet2)) or ((not editing) and (tSet1 <> 1));
    end;

    if (not retVal) then
    begin
      if not db^.dbr^.GetField(DBORGFamily, @tFamily2, SizeOf(tFamily2)) then
        LLShowError(@Self, IDS_DBOPFAILED, 'PutField', db^.dbr^.dbErrorNum)
      else
        retVal:= (editing and (tFamily1 <> tFamily2)) or ((not editing) and (tFamily1 <> 1));
    end;
  end;
  DataChanged:= retVal;
end;

function TDlgEditOrganism.SaveData(clearRecord: boolean): boolean;
var
  retVal : boolean;
  pstr   : array[0..80] of char;
  tSet   : byte;
  tFamily: byte;
  tOrgNum: longint;
  err    : integer;
begin
  retVal:= True;
  err:= edtID^.GetCtlAsStr(pstr, SizeOf(pstr)-1);
  if (err <> 0) then
  begin
    ShowUIFldError(@Self, err, edtID);
    retVal:= False;
  end;
  if retVal then
  begin
    Val(pstr, tOrgNum, err);
    if (err <> 0) or (tOrgNum < 600) or ((tOrgNum > 699) and (tOrgNum < 1000)) then
    begin
      LLShowError(@Self, IDS_INVALIDORGNUM, nil, 0);
      edtID^.FocusFld;
      retVal:= False;
    end;
  end;
  if retVal then
  begin
    Case cmbSet^.GetSelIndex of
      0:  tSet:= 1;
      1:  tSet:= 2;
      2:  tSet:= 3;
      3:  tSet:= 4;
      4:  tSet:= 6;
    end;
    tFamily:= cmbFamily^.GetSelIndex+1;
  end;
  if retVal and not db^.dbr^.PutField(DBORGSet, @tSet) then
  begin
    LLShowError(@Self, IDS_DBOPFAILED, 'PutField', db^.dbr^.dbErrorNum);
    retVal:= False;
  end;
  if retVal and not db^.dbr^.PutField(DBORGFamily, @tFamily) then
  begin
    LLShowError(@Self, IDS_DBOPFAILED, 'PutField', db^.dbr^.dbErrorNum);
    retVal:= False;
  end;
  if retVal then
  Begin
    retVal:= inherited SaveData(clearRecord);
    SelectSet(1);
    SelectFamily(1);
  End;
  SaveData:= retVal;
end;

procedure TDlgEditOrganism.ComboSet(var msg: TMessage);
begin
  if (msg.lParamHI = CBN_SELCHANGE) then
    SelectFamily(1);
end;

procedure TDlgEditOrganism.SelectSet(aSet: byte);
begin
  Case aSet of
    1:  cmbSet^.SetSelIndex(0);
    2:  cmbSet^.SetSelIndex(1);
    3:  cmbSet^.SetSelIndex(2);
    4:  cmbSet^.SetSelIndex(3);
    6:  cmbSet^.SetSelIndex(4);
  end;
end;

procedure TDlgEditOrganism.SelectFamily(aFamily: byte);
var
  pstr  : array[0..80] of char;
begin
  cmbFamily^.ClearList;
  Case cmbSet^.GetSelIndex of
    0:  begin
          cmbFamily^.AddString(SR(IDS_FERMENTER, pstr, SizeOf(pstr)-1));
          cmbFamily^.AddString(SR(IDS_NONFERMENTER, pstr, SizeOf(pstr)-1));
        end;
    1:  begin
          cmbFamily^.AddString(SR(IDS_STREP, pstr, SizeOf(pstr)-1));
          cmbFamily^.AddString(SR(IDS_MICROLIST, pstr, SizeOf(pstr)-1));
        end;
    2:  begin
          cmbFamily^.AddString(SR(IDS_GRAMNEGBACILLI, pstr, SizeOf(pstr)-1));
          cmbFamily^.AddString(SR(IDS_GRAMPOSBACILLI, pstr, SizeOf(pstr)-1));
          cmbFamily^.AddString(SR(IDS_COCCI, pstr, SizeOf(pstr)-1));
          cmbFamily^.AddString(SR(IDS_CLOSTRIDIA, pstr, SizeOf(pstr)-1));
        end;
    3,
    4:  begin
          cmbSet^.GetSelString(pstr, SizeOf(pstr)-1);
          cmbFamily^.AddString(pstr);
        end;
  end;
  cmbFamily^.SetSelIndex(aFamily-1);
end;

constructor TDlgEditTest.Init(aParent: PWindowsObject;
                              dlgResID: integer;
                              editMode: boolean;
                              aDB: PDBFile;
                              aListObj: PListObject;
                              idFieldNumber: integer;
                              idFieldSize: word;
                              descFieldNumber: integer;
                              descFieldSize: word;
                              UDFlagFieldNumber: integer);
begin
  inherited Init( aParent, dlgResID, editMode, aDB, aListObj, idFieldNumber,
                  idFieldSize, descFieldNumber, descFieldSize, UDFlagFieldNumber);
  edtCategory:= LinkUIFld(DBTSTTstCatRef, True, False, EDT_CATEGORY, TXT_CATEGORY, db^.dbd);
  edtDrug:= LinkUIFld(DBTSTDrugRef, False, False, EDT_DRUG, TXT_DRUG, db^.dbd);
end;

procedure TDlgEditTest.SetupWindow;
begin
  inherited SetupWindow;
  DoTestCatExit(0);
end;

procedure TDlgEditTest.LostFocus(var msg: TMessage);
var
  pstr    : array[0..100] of char;
  fld     : PUIFld;
  err     : integer;
begin
  fld:= PUIFld(msg.lParam); {- get field pointer to fld losing focus }
  if (fld^.GetID = EDT_CATEGORY) or (fld^.GetID = EDT_DRUG) then
  begin
    err:= fld^.Format;
    if (err <> 0) and (err <> UIErrRequired) then   {- ignore required fields on field exit }
      ShowUIFldError(@self, err, fld)
    else if (fld^.GetID = EDT_CATEGORY) then
      DoTestCatExit(msg.wParam);
  end;
  SetWindowLong(HWindow, DWL_MSGRESULT, 0);  {- set message result (for dialogs) }
end;

function TDlgEditTest.DataChanged: boolean;
{- compare the current data with the originally loaded data.  If any data has
   changed, then return true }
var
  retVal  : boolean;
  err     : integer;
begin
  retVal:= inherited DataChanged;

  if not retVal then
  begin
    err:= edtCategory^.Format;
    retVal:= (err <> 0) and (err <> UIErrRequired); {- ignore required fields }
  end;

  if not retVal then
  begin
    err:= edtDrug^.Format;
    retVal:= (err <> 0) and (err <> UIErrRequired); {- ignore required fields }
  end;

  DataChanged:= retVal;
end;

function TDlgEditTest.SaveData(clearRecord: boolean): boolean;
var
  retVal  : boolean;
  pstr    : array[0..100] of char;
begin
  retVal:= True;
  DoTestCatExit(0);
  if not db^.dbr^.PutFieldAsStr(DBTSTTstCat, edtCategory^.listRec^.ID) then
  begin
    retVal:= False;
    LLShowError(@Self, IDS_DBOPFAILED, 'PutFieldAsStr', db^.dbr^.dbErrorNum);
  end;
  if retVal then
    retVal:= inherited SaveData(clearRecord);
  if retVal then
    EnableDrug(True);
  SaveData:= retVal;
end;

procedure TDlgEditTest.EnableDrug(on: boolean);
begin
  edtDrug^.Enable(on);
  edtDrug^.SetRequired(on);
  if not on then
    edtDrug^.Clear;
end;

procedure TDlgEditTest.DoTestCatExit(fldResID: integer);
var
  pstr : array[0..100] of char;
begin
  edtCategory^.GetCtlAsStr(pstr, SizeOf(pstr)-1);
  if (StrIComp(pstr, 'MIC') = 0) or (StrIComp(pstr, 'NCCLS') = 0) or edtCategory^.IsEmpty then
  begin
    EnableDrug(True);
    if (fldResID = EDT_ID) then
      edtDrug^.FocusFld;
  end
  else
  begin
    if (fldResID = EDT_DRUG) then
      edtID^.FocusFld;
    EnableDrug(False);
  end;
end;

constructor TDlgEditTestGroup.Init( aParent: PWindowsObject;
                                    dlgResID: integer;
                                    editMode: boolean;
                                    aDB: PDBFile;
                                    aListObj: PListObject;
                                    idFieldNumber: integer;
                                    idFieldSize: word;
                                    descFieldNumber: integer;
                                    descFieldSize: word;
                                    UDFlagFieldNumber: integer);
begin
  inherited Init( aParent, dlgResID, editMode, aDB, aListObj, idFieldNumber,
                  idFieldSize, descFieldNumber, descFieldSize, UDFlagFieldNumber);
  testDB:= nil;
  xrefDB:= nil;
  testCatDB:= nil;
  lastVisibleCol:= 0;
  resBlockLen:= 0;
  testDB:= New(PDBFile, Init(DBTSTFile, '', dbOpenNormal));
  xrefDB:= New(PDBFile, Init(DBTSTGRPXRFFile, '', dbOpenNormal));
  testCatDB:= New(PDBFile, Init(DBTSTCATFile, '', dbOpenNormal));
  grid:= New(PDBGrid, InitResource(@Self, GRD_LIST, testDB^.dbd^.NumFields+1, False));
  lblNumTests:= New(PLStatic, InitResource(@Self, LBL_NUMTESTS, 0, False));
  txtNumTests:= New(PStatic, InitResource(@Self, TXT_NUMTESTS, 0));
  addTestButton:= New(PButton, InitResource(@Self, BTN_ADD));
  removeTestButton:= New(PButton, InitResource(@Self, BTN_DELETE));
  gridChanged:= False;
end;

destructor TDlgEditTestGroup.Done;
begin
  if (testDB <> nil) then
    Dispose(testDB, Done);
  if (xrefDB <> nil) then
    Dispose(xrefDB, Done);
  if (testCatDB <> nil) then
    Dispose(testCatDB, Done);

  inherited Done;
end;

procedure TDlgEditTestGroup.SetupWindow;
begin
  inherited SetupWindow;
  if not SetupDBGrid(@Self, grid, testDB, listObj, lastVisibleCol) then
    EndDlg(IDCANCEL)
  else if editing and not FillGrid(False) then
    EndDlg(IDCANCEL)
  else
    EnableButtons;
end;

procedure TDlgEditTestGroup.DrawItem(var msg: TMessage);
begin
  grid^.DrawItem(msg);
end;

procedure TDlgEditTestGroup.MeasureItem(var msg: TMessage);
begin
  grid^.MeasureItem(msg);
end;

procedure TDlgEditTestGroup.CharToItem(var msg: TMessage);
begin
  grid^.CharToItem(msg);
end;

procedure TDlgEditTestGroup.EnableButtons;
var
  pstr : array[0..20] of char;
begin
  ShowButton(addTestButton^.HWindow, True, True);
  ShowButton(removeTestButton^.HWindow, True, (grid^.GetSelIndex >= 0));
  Str(grid^.GetRowCount, pstr);
  txtNumTests^.SetText(pstr);
end;

function TDlgEditTestGroup.FillGrid(optimize: boolean): boolean;
var
  retVal  : boolean;
  seq     : TSeqNum;
  redraw  : boolean;
begin
  retVal:= True;
  with grid^ do
  begin
    redraw:= GetRedraw;
    SetRedraw(False);
    ClearList;
    if (xrefDB^.dbc^.NumRecords > 0) then
    begin
      seq:= db^.dbr^.GetSeqValue;
      if (db^.dbr^.dbErrorNum <> 0) then
      begin
        retVal:= False;
        LLShowError(@Self, IDS_DBOPFAILED, 'GetSeqValue', db^.dbr^.dbErrorNum);
      end
      else
      begin
        if not xrefDB^.dbr^.PutField(DBTSTGRPXRFTstGrpRef, @seq) then
        begin
          retVal:= False;
          LLShowError(@Self, IDS_DBOPFAILED, 'PutField', xrefDB^.dbr^.dbErrorNum);
        end
        else
        begin
          if xrefDB^.dbc^.GetFirstContains(xrefDB^.dbr) then
          repeat
            if not xrefDB^.dbr^.GetField(DBTSTGRPXRFTstRef, @seq, SizeOf(seq)) then
            begin
              retVal:= False;
              LLShowError(@Self, IDS_DBOPFAILED, 'GetField', xrefDB^.dbr^.dbErrorNum);
              break;
            end;
            if (AddGridRow(seq) < 0) then
              break;
          until not xrefDB^.dbc^.GetNextContains(xrefDB^.dbr);
        end;
      end;
    end;
    if retVal then
    begin
      if optimize then
      begin
        if (descFld > 0) and (GetColumnWidth(descFld) > 0) then
          grid^.OptimizeGrid(descFld)
        else
          grid^.OptimizeGrid(lastVisibleCol);
      end;
      SetSelIndex(0);
      SetRedraw(redraw);
    end;
  end;
  FillGrid:= retVal;
end;

function TDlgEditTestGroup.AddGridRow(seq: TSeqNum): longint;
var
  aRow    : longint;
  pstr20  : array[0..20] of char;
  pstr80  : array[0..80] of char;
  resLen  : integer;
begin
  aRow:= -1;
  if not testDB^.dbc^.GetSeq(testDB^.dbr, seq) then
    LLShowError(@Self, IDS_DBOPFAILED, 'GetSeq', testDB^.dbc^.dbErrorNum)
  else
  begin
    if GetResLen(testDB^.dbr, resLen) then
    begin
      if (resBlockLen + resLen > maxResBlockLen) then
        LLShowError(@Self, IDS_TESTGROUPTOOBIG, nil, 0)
      else
      begin
        aRow:= grid^.AddString('');
        if (aRow < 0) then
          InfoMsg(HWindow, SR(IDS_WARNING, pstr20, SizeOf(pstr20)-1), SR(IDS_TOOMANYRECS, pstr80, SizeOf(pstr80)-1))
        else
        begin
          if not FillDBGridRow(@Self, grid, testDB, listObj, aRow) then
          begin
            grid^.DeleteString(aRow);
            aRow:= -1;
          end
          else
            Inc(resBlockLen, resLen);
        end;
      end;
    end;
  end;
  AddGridRow:= aRow;
end;

function TDlgEditTestGroup.DataChanged: boolean;
{- compare the current data with the originally loaded data.  If any data has
   changed, then return true }
begin
  DataChanged:= ((inherited DataChanged) or gridChanged);
end;

function TDlgEditTestGroup.SaveData(clearRecord: boolean): boolean;
var
  retVal  : boolean;
begin
  retVal:= True;
  if (grid^.GetRowCount = 0) then
  begin
    retVal:= False;
    LLShowError(@Self, IDS_TESTSREQUIRED, nil, 0)
  end;
  if retVal and (not db^.dbr^.PutFieldAsStr(DBTSTGRPSet, '8')) then
  begin
    LLShowError(@Self, IDS_DBOPFAILED, 'PutFieldAsStr', db^.dbr^.dbErrorNum);
    retVal:= False;
  end;
  {- layout should always be two chars (user defined get XX) }
  if retVal and (not db^.dbr^.PutFieldAsStr(DBTSTGRPLayout, 'XX')) then
  begin
    LLShowError(@Self, IDS_DBOPFAILED, 'PutFieldAsStr', db^.dbr^.dbErrorNum);
    retVal:= False;
  end;
  if retVal and (not db^.dbr^.PutField(DBTSTGRPLen, @resBlockLen)) then
  begin
    LLShowError(@Self, IDS_DBOPFAILED, 'PutField', db^.dbr^.dbErrorNum);
    retVal:= False;
  end;
  if retVal then
    retVal:= inherited SaveData(False);
  if retVal then
    retVal:= SaveXRef;
  if retVal then
  begin
    grid^.ClearList;
    gridChanged:= False;
  end;
  if ((not editing) and clearRecord) then
    db^.dbr^.ClearRecord;
  EnableButtons;
  SaveData:= retVal;
end;

function TDlgEditTestGroup.SaveXRef: boolean;
var
  retVal  : boolean;
  aRow    : longint;
  grpSeq  : TSeqNum;
  tstSeq  : TSeqNum;
  resLen  : integer;
  resPos  : integer;
begin
  retVal:= True;

  grpSeq:= db^.dbr^.GetSeqValue;
  if (db^.dbr^.dbErrorNum <> 0) then
  begin
    retVal:= False;
    LLShowError(@Self, IDS_DBOPFAILED, 'GetSeqValue', db^.dbr^.dbErrorNum);
  end
  else
  begin
    if editing and (grpSeq > 0) then
    begin
      { remove old xref records }
      xrefDB^.dbr^.ClearRecord;
      if not xrefDB^.dbr^.PutField(DBTSTGRPXRFTstGrpRef, @grpSeq) then
      begin
        retVal:= False;
        LLShowError(@Self, IDS_DBOPFAILED, 'PutField', xrefDB^.dbr^.dbErrorNum);
      end
      else
      begin
        if xrefDB^.dbc^.GetFirstContains(xrefDB^.dbr) then
        repeat
          if not xRefDB^.dbc^.DeleteRec(xrefDB^.dbr) then
          begin
            retVal:= False;
            if (xrefDB^.dbc^.dbErrorNum = dbErr_CantDeleteRecord) then
              LLShowError(@Self, IDS_CANTDELETEREC, nil, 0)
            else
              LLShowError(@Self, IDS_DBOPFAILED, 'DeleteRec', xrefDB^.dbc^.dbErrorNum);
          end;
          if not retVal then
            break;
        until not xrefDB^.dbc^.GetNextContains(xrefDB^.dbr);
      end;
    end;
    { save New xref records }
    if retVal then
    begin
      resPos:= 0;
      for aRow:= 0 To grid^.GetRowCount-1 do
      begin
        if not xrefDB^.dbr^.PutField(DBTSTGRPXRFTstGrpRef, @grpSeq) then
        begin
          retVal:= False;
          LLShowError(@Self, IDS_DBOPFAILED, 'PutField', xrefDB^.dbr^.dbErrorNum);
        end;
        if retVal then
        begin
          tstSeq:= grid^.GetItemData(aRow);
          if not xrefDB^.dbr^.PutField(DBTSTGRPXRFTstRef, @tstSeq) then
          begin
            retVal:= False;
            LLShowError(@Self, IDS_DBOPFAILED, 'PutField', xrefDB^.dbr^.dbErrorNum);
          end;
        end;
        if retVal and not xrefDB^.dbr^.PutField(DBTSTGRPXRFResBlockPos, @resPos) then
        begin
          retVal:= False;
          LLShowError(@Self, IDS_DBOPFAILED, 'PutField', xrefDB^.dbr^.dbErrorNum);
        end;
        if retVal and not testDB^.dbc^.GetSeq(testDB^.dbr, tstSeq) then
        begin
          retVal:= False;
          LLShowError(@Self, IDS_DBOPFAILED, 'GetSeq', testDB^.dbc^.dbErrorNum);
        end
        else
        begin
          retVal:= GetResLen(testDB^.dbr, resLen);
        end;
        if retVal then
        begin
          Inc(resPos, resLen); {increment for next loop iteration}
          xrefDB^.dbc^.InsertRec(xrefDB^.dbr);
          if (xrefDB^.dbr^.dbErrorNum <> 0) then
          begin
            retVal:= False;
            LLShowError(@Self, IDS_DBOPFAILED, 'InsertRec', xrefDB^.dbr^.dbErrorNum);
          end;
        end;
        if not retVal then
          break;
      end;
    end;
  end;
  SaveXRef:= retVal;
end;

function TDlgEditTestGroup.DuplicateTest(seq: TSeqNum): boolean;
var
  retVal  : boolean;
  aRow    : longint;
begin
  retVal:= False;
  if (grid^.GetRowCount > 0) then
  begin
    aRow:= 0;
    repeat
      retVal:= (seq = grid^.GetItemData(aRow));
      if not retVal then
        Inc(aRow);
    until retVal or (aRow = grid^.GetRowCount);
  end;
  DuplicateTest:= retVal;
end;

procedure TDlgEditTestGroup.BtnAddTest(var msg: TMessage);
var
  listRec : PMnemListRec;
  aRow    : longint;
begin
  listRec:= New(PMnemListRec, Init);
  if (listRec = nil) then
    LLShowError(@Self, IDS_LISTRECFAILED, nil, 0)
  else
  begin
    if listObj^.SelectMnemList(@Self, DBTSTFile, '', listRec) then
    begin
      if DuplicateTest(listRec^.seq) then
        LLShowError(@Self, IDS_DUPLICATETESTINGROUP, nil, 0)
      else
      begin
        aRow:= AddGridRow(listRec^.seq);
        if (aRow >= 0) then
        begin
          grid^.SetSelIndex(aRow);
          gridChanged:= True;
          EnableButtons;
        end;
      end;
    end;
    Dispose(listRec, Done);
  end;
end;

procedure TDlgEditTestGroup.BtnRemoveTest(var msg: TMessage);
var
  pos     : longint;
  resLen  : integer;
begin
  pos:= grid^.GetSelIndex;
  if (pos >= 0) then
  begin
    if not testDB^.dbc^.GetSeq(testDB^.dbr, grid^.GetItemData(pos)) then
      LLShowError(@Self, IDS_DBOPFAILED, 'GetSeq', testDB^.dbc^.dbErrorNum)
    else
    begin
      if GetResLen(testDB^.dbr, resLen) then
      begin
        Dec(resBlockLen, resLen);
        grid^.DeleteString(pos);
        if (pos >= grid^.GetRowCount) then
          pos:= grid^.GetRowCount-1;
        grid^.SetSelIndex(pos);
        gridChanged:= True;
        EnableButtons;
      end;
    end;
  end;
end;

function TDlgEditTestGroup.GetResLen(aTestRec: PDBRec; var resLen: integer): boolean;
var
  retVal  : boolean;
  seq     : TSeqNum;
begin
  retVal:= True;
  resLen:= 0;
  if not aTestRec^.GetField(DBTSTTstCatRef, @seq, SizeOf(seq)) then
  begin
    retVal:= False;
    LLShowError(@Self, IDS_DBOPFAILED, 'GetField', aTestRec^.dbErrorNum);
  end;
  if retVal and not testCatDB^.dbc^.GetSeq(testCatDB^.dbr, seq) then
  begin
    retVal:= False;
    LLShowError(@Self, IDS_DBOPFAILED, 'GetSeq', testCatDB^.dbc^.dbErrorNum);
  end;
  if retVal and not testCatDB^.dbr^.GetField(DBTSTCATResLen, @resLen, SizeOf(resLen)) then
  begin
    retVal:= False;
    LLShowError(@Self, IDS_DBOPFAILED, 'GetField', testCatDB^.dbr^.dbErrorNum);
  end;
  GetResLen:= retVal;
end;

constructor TDlgEditOrder.Init( aParent: PWindowsObject;
                                dlgResID: integer;
                                editMode: boolean;
                                aDB: PDBFile;
                                aListObj: PListObject;
                                idFieldNumber: integer;
                                idFieldSize: word;
                                descFieldNumber: integer;
                                descFieldSize: word;
                                UDFlagFieldNumber: integer);
begin
  inherited Init( aParent, dlgResID, editMode, aDB, aListObj, idFieldNumber,
                  idFieldSize, descFieldNumber, descFieldSize, UDFlagFieldNumber);
  testGroupDB:= nil;
  xrefDB:= nil;
  lastVisibleCol:= 0;
  testGroupDB:= New(PDBFile, Init(DBTSTGRPFile, '', dbOpenNormal));
  xrefDB:= New(PDBFile, Init(DBORDXRFFile, '', dbOpenNormal));
  testXrefDB:= New(PDBFile, Init(DBTSTGRPXRFFile, '', dbOpenNormal));
  testDB:= New(PDBFile, Init(DBTSTFile, '', dbOpenNormal));
  grid:= New(PDBGrid, InitResource(@Self, GRD_LIST, testGroupDB^.dbd^.NumFields+1, False));
  lblNumTestGroups:= New(PLStatic, InitResource(@Self, LBL_NUMTESTS, 0, False));
  txtNumTestGroups:= New(PStatic, InitResource(@Self, TXT_NUMTESTS, 0));
  addTestGroupButton:= New(PButton, InitResource(@Self, BTN_ADD));
  removeTestGroupButton:= New(PButton, InitResource(@Self, BTN_DELETE));
  gridChanged:= False;
end;

destructor TDlgEditOrder.Done;
begin
  if (testGroupDB <> nil) then
    Dispose(testGroupDB, Done);
  if (xrefDB <> nil) then
    Dispose(xrefDB, Done);
  if (testXrefDB <> nil) then
    Dispose(testXrefDB, Done);
  if (testDB <> nil) then
    Dispose(testDB, Done);

  inherited Done;
end;

procedure TDlgEditOrder.SetupWindow;
begin
  inherited SetupWindow;
  if not SetupDBGrid(@Self, grid, testGroupDB, listObj, lastVisibleCol) then
    EndDlg(IDCANCEL)
  else if editing and not FillGrid(False) then
    EndDlg(IDCANCEL)
  else
    EnableButtons;
end;

procedure TDlgEditOrder.DrawItem(var msg: TMessage);
begin
  grid^.DrawItem(msg);
end;

procedure TDlgEditOrder.MeasureItem(var msg: TMessage);
begin
  grid^.MeasureItem(msg);
end;

procedure TDlgEditOrder.CharToItem(var msg: TMessage);
begin
  grid^.CharToItem(msg);
end;

procedure TDlgEditOrder.EnableButtons;
var
  pstr : array[0..20] of char;
begin
  ShowButton(addTestGroupButton^.HWindow, True, True);
  ShowButton(removeTestGroupButton^.HWindow, True, (grid^.GetSelIndex >= 0));
  Str(grid^.GetRowCount, pstr);
  txtNumTestGroups^.SetText(pstr);
end;

function TDlgEditOrder.FillGrid(optimize: boolean): boolean;
var
  retVal  : boolean;
  seq     : TSeqNum;
  redraw  : boolean;
begin
  retVal:= True;
  with grid^ do
  begin
    redraw:= GetRedraw;
    SetRedraw(False);
    ClearList;
    if (xrefDB^.dbc^.NumRecords > 0) then
    begin
      seq:= db^.dbr^.GetSeqValue;
      if (db^.dbr^.dbErrorNum <> 0) then
      begin
        retVal:= False;
        LLShowError(@Self, IDS_DBOPFAILED, 'GetSeqValue', db^.dbr^.dbErrorNum);
      end
      else
      begin
        if not xrefDB^.dbr^.PutField(DBORDXRFOrdRef, @seq) then
        begin
          retVal:= False;
          LLShowError(@Self, IDS_DBOPFAILED, 'PutField', xrefDB^.dbr^.dbErrorNum);
        end
        else
        begin
          if xrefDB^.dbc^.GetFirstContains(xrefDB^.dbr) then
          repeat
            if not xrefDB^.dbr^.GetField(DBORDXRFTstGrpRef, @seq, SizeOf(seq)) then
            begin
              retVal:= False;
              LLShowError(@Self, IDS_DBOPFAILED, 'GetField', xrefDB^.dbr^.dbErrorNum);
              break;
            end;
            if (AddGridRow(seq) < 0) then
              break;
          until not xrefDB^.dbc^.GetNextContains(xrefDB^.dbr);
        end;
      end;
    end;
    if retVal then
    begin
      if optimize then
      begin
        if (descFld > 0) and (GetColumnWidth(descFld) > 0) then
          grid^.OptimizeGrid(descFld)
        else
          grid^.OptimizeGrid(lastVisibleCol);
      end;
      SetSelIndex(0);
      SetRedraw(redraw);
    end;
  end;
  FillGrid:= retVal;
end;

function TDlgEditOrder.AddGridRow(seq: TSeqNum): longint;
var
  aRow    : longint;
  pstr20  : array[0..20] of char;
  pstr80  : array[0..80] of char;
begin
  aRow:= -1;
  if not testGroupDB^.dbc^.GetSeq(testGroupDB^.dbr, seq) then
    LLShowError(@Self, IDS_DBOPFAILED, 'GetSeq', testGroupDB^.dbc^.dbErrorNum)
  else
  begin
    aRow:= grid^.AddString('');
    if (aRow < 0) then
      InfoMsg(HWindow, SR(IDS_WARNING, pstr20, SizeOf(pstr20)-1), SR(IDS_TOOMANYRECS, pstr80, SizeOf(pstr80)-1))
    else
    begin
      if not FillDBGridRow(@Self, grid, testGroupDB, listObj, aRow) then
      begin
        grid^.DeleteString(aRow);
        aRow:= -1;
      end;
    end;
  end;
  AddGridRow:= aRow;
end;

function TDlgEditOrder.DataChanged: boolean;
{- compare the current data with the originally loaded data.  If any data has
   changed, then return true }
begin
  DataChanged:= ((inherited DataChanged) or gridChanged);
end;

function TDlgEditOrder.SaveData(clearRecord: boolean): boolean;
var
  retVal    : boolean;
  orderSet  : byte;
begin
  retVal:= True;
  if (grid^.GetRowCount = 0) then
  begin
    retVal:= False;
    LLShowError(@Self, IDS_TESTGROUPSREQUIRED, nil, 0)
  end;
  if retVal then
  begin
    orderSet:= GetCurOrderSet;
    if not db^.dbr^.PutField(DBORDSet, @orderSet) then
    begin
      retVal:= False;
      LLShowError(@Self, IDS_DBOPFAILED, 'PutField', db^.dbr^.dbErrorNum);
    end;
  end;
  if retVal then
    retVal:= inherited SaveData(False);
  if retVal then
    retVal:= SaveXRef;
  if retVal then
  begin
    grid^.ClearList;
    gridChanged:= False;
  end;
  if ((not editing) and clearRecord) then
    db^.dbr^.ClearRecord;
  EnableButtons;
  SaveData:= retVal;
end;

function TDlgEditOrder.SaveXRef: boolean;
var
  retVal  : boolean;
  aRow    : longint;
  grpSeq  : TSeqNum;
  ordSeq  : TSeqNum;
begin
  retVal:= True;

  ordSeq:= db^.dbr^.GetSeqValue;
  if (db^.dbr^.dbErrorNum <> 0) then
  begin
    retVal:= False;
    LLShowError(@Self, IDS_DBOPFAILED, 'GetSeqValue', db^.dbr^.dbErrorNum);
  end
  else
  begin
    if editing and (ordSeq > 0) then
    begin
      { remove old xref records }
      xrefDB^.dbr^.ClearRecord;
      if not xrefDB^.dbr^.PutField(DBORDXRFOrdRef, @ordSeq) then
      begin
        retVal:= False;
        LLShowError(@Self, IDS_DBOPFAILED, 'PutField', xrefDB^.dbr^.dbErrorNum);
      end
      else
      begin
        if xrefDB^.dbc^.GetFirstContains(xrefDB^.dbr) then
        repeat
          if not xRefDB^.dbc^.DeleteRec(xrefDB^.dbr) then
          begin
            retVal:= False;
            if (xrefDB^.dbc^.dbErrorNum = dbErr_CantDeleteRecord) then
              LLShowError(@Self, IDS_CANTDELETEREC, nil, 0)
            else
              LLShowError(@Self, IDS_DBOPFAILED, 'DeleteRec', xrefDB^.dbc^.dbErrorNum);
          end;
          if not retVal then
            break;
        until not xrefDB^.dbc^.GetNextContains(xrefDB^.dbr);
      end;
    end;
    { save New xref records }
    if retVal then
    begin
      for aRow:= 0 To grid^.GetRowCount-1 do
      begin
        if not xrefDB^.dbr^.PutField(DBORDXRFOrdRef, @ordSeq) then
        begin
          retVal:= False;
          LLShowError(@Self, IDS_DBOPFAILED, 'PutField', xrefDB^.dbr^.dbErrorNum);
        end;
        if retVal then
        begin
          grpSeq:= grid^.GetItemData(aRow);
          if not xrefDB^.dbr^.PutField(DBORDXRFTstGrpRef, @grpSeq) then
          begin
            retVal:= False;
            LLShowError(@Self, IDS_DBOPFAILED, 'PutField', xrefDB^.dbr^.dbErrorNum);
          end;
          xrefDB^.dbc^.InsertRec(xrefDB^.dbr);
          if (xrefDB^.dbr^.dbErrorNum <> 0) then
          begin
            retVal:= False;
            LLShowError(@Self, IDS_DBOPFAILED, 'InsertRec', xrefDB^.dbr^.dbErrorNum);
          end;
        end;
        if not retVal then
          break;
      end;
    end;
  end;
  SaveXRef:= retVal;
end;

function TDlgEditOrder.OKToAddTestGroup(seq: TSeqNum): boolean;
var
  retVal    : boolean;
  aDBRec    : PDBRec;
  tstGrpSet : byte;
  orderSet  : byte;
begin
  retVal:= True;
  if (grid^.GetRowCount > 0) then
  begin
    aDBRec:= listObj^.PeekSeq(DBTSTGRPFile, seq);
    if (aDBRec = nil) then
    begin
      retVal:= False;
      LLShowError(@Self, IDS_DBOPFAILED, 'PeekSeq', aDBRec^.dbErrorNum);
    end;
    if retVal then
    begin
      aDBRec^.GetField(DBTSTGRPSet, @tstGrpSet, SizeOf(tstGrpSet));
      if (aDBRec^.dbErrorNum <> 0) then
      begin
        retVal:= False;
        LLShowError(@Self, IDS_DBOPFAILED, 'GetField', aDBRec^.dbErrorNum);
      end;
    end;
    if retVal then
    begin
      orderSet:= GetCurOrderSet;
      { make sure panel set is compatible within the order }
      if  (orderSet <> 8) and (tstGrpSet <> 8) and (tstGrpSet <> orderSet) then
      begin
        retVal:= False;
        LLShowError(@Self, IDS_INCOMPATIBLETESTGROUP, nil, 0)
      end;
    end;
    if retVal and DuplicateTestGroup(seq) then
    begin
      retVal:= False;
      LLShowError(@Self, IDS_DUPLICATETESTGROUPINORDER, nil, 0)
    end;
    if retVal and IDMICConflict(aDBRec) then
    begin
      retVal:= False;
      LLShowError(@Self, IDS_IDMICCONFLICT, nil, 0)
    end;
    if retVal and DuplicateTest(seq) then
    begin
      retVal:= False;
      LLShowError(@Self, IDS_DUPLICATETESTINORDER, nil, 0)
    end;
  end;
  OKToAddTestGroup:= retVal;
end;

function TDlgEditOrder.DuplicateTestGroup(seq : TSeqNum): boolean;
var
  retVal  : boolean;
  aRow    : longint;
begin
  retVal:= False;
  if (grid^.GetRowCount > 0) then
  begin
    aRow:= 0;
    repeat
      retVal:= (seq = grid^.GetItemData(aRow));
      if not retVal then
        Inc(aRow);
    until retVal or (aRow = grid^.GetRowCount);
  end;
  DuplicateTestGroup:= retVal;
end;

function  TDlgEditOrder.IDMicConflict(aDBRec: PDBRec): boolean;
var
  retVal  : boolean;
  aRow    : longint;
  layout  : array[0..10] of char;
  pstr10  : array[0..10] of char;
begin
  retVal:= False;
  if (grid^.GetRowCount > 0) then
  begin
    aDBRec^.GetFieldAsStr(DBTSTGRPLayout, layout, SizeOf(layout)-1);
    if (aDBRec^.dbErrorNum <> 0) then
    begin
      retVal:= True;
      LLShowError(@Self, IDS_DBOPFAILED, 'GetFieldAsStr', aDBRec^.dbErrorNum);
    end
    else
    begin
      aRow:= 0;
      repeat
        grid^.GetData(DBTSTGRPLayout, aRow, pstr10, SizeOf(pstr10)-1);
        { don't combine ID panel with ID or Combo panel }
        if layout[2] = idpanel then
          if (pstr10[2] = idpanel) or (pstr10[2] = comboPanel) then
            retVal:= True;
        { don't combine MIC panel with MIC or Combo panel }
        if (layout[2] = micPanel) then
          if (pstr10[2] = micPanel) or (pstr10[2] = comboPanel) then
            retVal:= True;
        { don't combine Combo panel with ANY other panel }
        if (layout[2] = combopanel) then
          if (pstr10[2] = comboPanel) or
             (pstr10[2] = idPanel) or
             (pstr10[2] = micPanel) then
            retVal:= True;
        if not retVal then
          Inc(aRow);
      until retVal or (aRow = grid^.GetRowCount);
    end;
  end;
  IDMicConflict:= retVal;
end;

function TDlgEditOrder.DuplicateTest(seq : TSeqNum): boolean;

  function Matches(aDBRec: PDBRec): boolean; far;
  var
    retVal     : boolean;
    testCatSeq1: TSeqNum;
    drugSeq1   : TSeqNum;
    testCatSeq2: TSeqNum;
    drugSeq2   : TSeqNum;
  begin
    retVal:= False;
    if not aDBRec^.GetField(DBTSTTstCatRef, @testCatSeq1, SizeOf(testCatSeq1)) then
    begin
      retVal:= True;
      LLShowError(@Self, IDS_DBOPFAILED, 'GetField', aDBRec^.dbErrorNum);
    end
    else if not aDBRec^.GetField(DBTSTDrugRef, @drugSeq1, SizeOf(drugSeq1)) then
    begin
      retVal:= True;
      LLShowError(@Self, IDS_DBOPFAILED, 'GetField', aDBRec^.dbErrorNum);
    end
    else if not testDB^.dbr^.GetField(DBTSTTstCatRef, @testCatSeq2, SizeOf(testCatSeq2)) then
    begin
      retVal:= True;
      LLShowError(@Self, IDS_DBOPFAILED, 'GetField', testDB^.dbr^.dbErrorNum);
    end
    else if not testDB^.dbr^.GetField(DBTSTDrugRef, @drugSeq2, SizeOf(drugSeq2)) then
    begin
      retVal:= True;
      LLShowError(@Self, IDS_DBOPFAILED, 'GetField', testDB^.dbr^.dbErrorNum);
    end
    else
    begin
      retVal:= (testCatSeq1 = testCatSeq2) and (drugSeq1 = drugSeq2);
    end;
    Matches:= retVal;
  end;

var
  retVal  : boolean;
  aRow    : longint;
  testColl: PCollection;
  testSeq : TSeqNum;
  aDBRec  : PDBRec;
begin
  retVal:= False;
  if (grid^.GetRowCount > 0) then
  begin
    { build collection of tests for the test group being added }
    testColl:= New(PCollection, Init(10, 5));
    if (testColl = nil) then
    begin
      retVal:= True;
      LLShowError(@Self, IDS_MEMFAILURE, nil, 0);
    end
    else
    begin
      testXrefDB^.dbr^.ClearRecord;
      if not testXrefDB^.dbr^.PutField(DBTSTGRPXRFTstGrpRef, @seq) then
      begin
        retVal:= True;
        LLShowError(@Self, IDS_DBOPFAILED, 'PutField', testXrefDB^.dbr^.dbErrorNum);
      end
      else
      begin
        if testXrefDB^.dbc^.GetFirstContains(testXrefDB^.dbr) then
        repeat
          if not testXrefDB^.dbr^.GetField(DBTSTGRPXRFTstRef, @testSeq, SizeOf(testSeq)) then
          begin
            retVal:= True;
            LLShowError(@Self, IDS_DBOPFAILED, 'GetField', testXrefDB^.dbr^.dbErrorNum);
          end
          else
          begin
            aDBRec:= New(PDBRec, Init(testDB^.dbc));
            if not testDB^.dbc^.GetSeq(aDBRec, testSeq) then
            begin
              retVal:= True;
              LLShowError(@Self, IDS_DBOPFAILED, 'GetSeq', testDB^.dbc^.dbErrorNum);
            end
            else
              testColl^.Insert(aDBRec);
          end;
        until retVal or (not testXrefDB^.dbc^.GetNextContains(testXrefDB^.dbr));
      end;
      { now check each test already selected against those in collection }
      if not retVal then
      begin
        aRow:= 0;
        repeat
          seq:= grid^.GetItemData(aRow);
          testXrefDB^.dbr^.ClearRecord;
          if not testXrefDB^.dbr^.PutField(DBTSTGRPXRFTstGrpRef, @seq) then
          begin
            retVal:= True;
            LLShowError(@Self, IDS_DBOPFAILED, 'PutField', testXrefDB^.dbr^.dbErrorNum);
          end
          else
          begin
            if testXrefDB^.dbc^.GetFirstContains(testXrefDB^.dbr) then
            repeat
              if not testXrefDB^.dbr^.GetField(DBTSTGRPXRFTstRef, @testSeq, SizeOf(testSeq)) then
              begin
                retVal:= True;
                LLShowError(@Self, IDS_DBOPFAILED, 'GetField', testXrefDB^.dbr^.dbErrorNum);
              end
              else
              begin
                if not testDB^.dbc^.GetSeq(testDB^.dbr, testSeq) then
                begin
                  retVal:= True;
                  LLShowError(@Self, IDS_DBOPFAILED, 'GetSeq', testDB^.dbc^.dbErrorNum);
                end
                else
                begin
                  retVal:= (testColl^.FirstThat(@Matches) <> nil);
                end;
              end;
            until retVal or (not testXrefDB^.dbc^.GetNextContains(testXrefDB^.dbr));
          end;
          if not retVal then
            Inc(aRow);
        until retVal or (aRow = grid^.GetRowCount);
      end;
      Dispose(testColl, Done);
    end;
  end;
  DuplicateTest:= retVal;
end;

function  TDlgEditOrder.GetCurOrderSet: byte;
var
  tSet    : byte;
  aRow    : longint;
  pstr10  : array[0..10] of char;
  errCode : integer;
begin
  tSet:= 8; {default to user defined}
  if (grid^.GetRowCount > 0) then
  begin
    aRow:= 0;
    repeat
      grid^.GetData(DBORDSet, aRow, pstr10, SizeOf(pstr10)-1);
      Val(pstr10, tSet, errCode);
      Inc(aRow);
    until (tSet <> 8) or (aRow = grid^.GetRowCount);
  end;
  GetCurOrderSet:= tSet;
end;

procedure TDlgEditOrder.BtnAddTestGroup(var msg: TMessage);
var
  listRec : PMnemListRec;
  aRow    : longint;
begin
  listRec:= New(PMnemListRec, Init);
  if (listRec = nil) then
    LLShowError(@Self, IDS_LISTRECFAILED, nil, 0)
  else
  begin
    if listObj^.SelectMnemList(@Self, DBTSTGRPFile, '', listRec) then
    begin
      if OKToAddTestGroup(listRec^.seq) then
      begin
        aRow:= AddGridRow(listRec^.seq);
        if (aRow >= 0) then
        begin
          grid^.SetSelIndex(aRow);
          gridChanged:= True;
          EnableButtons;
        end;
      end;
    end;
    Dispose(listRec, Done);
  end;
end;

procedure TDlgEditOrder.BtnRemoveTestGroup(var msg: TMessage);
var
  pos       : longint;
begin
  pos:= grid^.GetSelIndex;
  if (pos >= 0) then
  begin
    grid^.DeleteString(pos);
    if (pos >= grid^.GetRowCount) then
      pos:= grid^.GetRowCount-1;
    grid^.SetSelIndex(pos);
    gridChanged:= True;
    EnableButtons;
  end;
end;

constructor TMnemListRec.Init;
begin
  inherited Init;
  ID:= nil;
  desc:= nil;
  if not Clear then
  begin
    Done;
    Fail;
  end;
end;

destructor TMnemListRec.Done;
begin
  ClearIDAndDesc;
  inherited Done;
end;

function TMnemListRec.Clear: boolean;
begin
  errorNum:= 0;
  if SetIDAndDesc('', '', 0, 0) then
    seq:= 0;
  Clear:= (errorNum = 0);
end;

procedure TMnemListRec.ClearIDAndDesc;
begin
  if (ID <> nil) then
    FreeMem(ID, IDLen+1);
  if (desc <> nil) then
    FreeMem(desc, DescLen+1);
  IDLen:= 0;
  descLen:= 0;
end;

function TMnemListRec.SetIDAndDesc(aID, aDesc: PChar; aIDLen, aDescLen: word): boolean;
begin
  errorNum:= 0;
  ClearIDAndDesc;
  GetMem(ID, aIDLen+1);
  GetMem(desc, aDescLen+1);
  if (ID = nil) or (desc = nil) then
    errorNum:= listErr_CantAllocateMemory
  else
  begin
    StrLCopy(ID, aID, aIDLen);
    StrLCopy(desc, aDesc, aDescLen);
    IDLen:= aIDLen;
    descLen:= aDescLen;
  end;
  SetIDAndDesc:= (errorNum = 0);
end;

constructor TListObject.Init;
begin
  inherited Init;
  listOnPartialID:= INIListOnPartialID;
  dbFileColl:= New(PCollection, Init(5, 5));
  if (dbFileColl = nil) then
  begin
    Done;
    Fail;
  end;
end;

destructor TListObject.Done;
begin
  if (dbFileColl <> nil) then
    Dispose(dbFileColl, Done);
  inherited Done;
end;

{ False if error or if selection list was cancelled }
{ if return is False, errorNum = listErr_PartialKeyNotFound if no match found }
function TListObject.SelectList(aParent: PWindowsObject; aDBRec: PDBRec): boolean;
var
  retVal  : boolean;
  db      : PDBFile;
  fName   : array[0..maxFileNameLen] of char;
begin
  retVal:= False;
  errorNum:= 0;
  db:= GetDB(aDBRec^.Desc^.FileName(fName, maxFileNameLen));
  if (db <> nil) then
  begin
    if not db^.dbr^.CopyRecord(aDBRec) then
      errorNum:= db^.dbr^.dbErrorNum
    else if SetKey(db) then
    begin
      if not db^.dbc^.GetFirstContains(db^.dbr) then
      begin
        if  (db^.dbc^.dbErrorNum <> B_KEY_VALUE_NOT_FOUND + MOD_BTRV) and
            (db^.dbc^.dbErrorNum <> B_END_OF_FILE + MOD_BTRV)then
          errorNum:= db^.dbc^.dbErrorNum
        else
          errorNum:= listErr_PartialKeyNotFound
      end
      else if not db^.dbr^.CopyRecord(aDBRec) then
        errorNum:= db^.dbr^.dbErrorNum
      else if (DoListDialog(aParent, db, False, @Self, modeSelectOnly, 0, 0, 0, 0, 0) = IDOK) then
      begin
        if not aDBRec^.CopyRecord(db^.dbr) then
          errorNum:= aDBRec^.dbErrorNum
        else
          retVal:= True;
      end;
    end;
  end;
  SelectList:= retVal;
end;

{ False if error }
function TListObject.ViewOnlyList(aParent: PWindowsObject; aDBRec: PDBRec): boolean;
var
  db      : PDBFile;
  fName   : array[0..maxFileNameLen] of char;
begin
  errorNum:= 0;
  db:= GetDB(aDBRec^.Desc^.FileName(fName, maxFileNameLen));
  if (db <> nil) then
  begin
    if not db^.dbr^.CopyRecord(aDBRec) then
      errorNum:= db^.dbr^.dbErrorNum
    else if SetKey(db) then
    begin
      DoListDialog(aParent, db, False, @Self, modeViewOnly, 0, 0, 0, 0, 0);
    end;
  end;
  ViewOnlyList:= (errorNum = 0);
end;

{ False if error or if selection list was cancelled }
function TListObject.FindKey(aParent: PWindowsObject; aDBRec: PDBRec; doExactMatch: boolean): boolean;
var
  retVal  : boolean;
  db      : PDBFile;
  fName   : array[0..maxFileNameLen] of char;
begin
  retVal:= False;
  errorNum:= 0;
  db:= GetDB(aDBRec^.Desc^.FileName(fName, maxFileNameLen));
  if (db <> nil) then
  begin
    if not db^.dbr^.CopyRecord(aDBRec) then
      errorNum:= db^.dbr^.dbErrorNum
    else if SetKey(db) then
    begin
      if db^.dbc^.GetEQ(db^.dbr) then
      begin
        if not aDBRec^.CopyRecord(db^.dbr) then
          errorNum:= aDBRec^.dbErrorNum
        else
          retVal:= True;
      end
      else if (db^.dbc^.dbErrorNum <> B_KEY_VALUE_NOT_FOUND + MOD_BTRV) then
        errorNum:= db^.dbc^.dbErrorNum
      else if (not doExactMatch) and listOnPartialID then
      begin
        if (DoListDialog(aParent, db, False, @Self, modeSelectOnly, 0, 0, 0, 0, 0) = IDOK) then
        begin
          if not aDBRec^.CopyRecord(db^.dbr) then
            errorNum:= aDBRec^.dbErrorNum
          else
            retVal:= True;
        end;
      end;
    end;
  end;
  FindKey:= retVal;
end;

{ False if error or if not found }
function TListObject.FindSeq(aDBRec: PDBRec; aSeq: TSeqNum): boolean;
var
  retVal  : boolean;
  db      : PDBFile;
  fName   : array[0..maxFileNameLen] of char;
begin
  retVal:= False;
  errorNum:= 0;
  db:= GetDB(aDBRec^.Desc^.FileName(fName, maxFileNameLen));
  if (db <> nil) then
  begin
    if not db^.dbr^.CopyRecord(aDBRec) then
      errorNum:= db^.dbr^.dbErrorNum
    else if not db^.dbc^.GetSeq(db^.dbr, aSeq) then
      errorNum:= db^.dbc^.dbErrorNum
    else if not aDBRec^.CopyRecord(db^.dbr) then
      errorNum:= aDBRec^.dbErrorNum
    else
      retVal:= True;
  end;
  FindSeq:= retVal;
end;

{ Returns nil if not found, otherwise returns a pointer to my internal record }
function TListObject.PeekSeq(aFileName: PChar; aSeq: TSeqNum): PDBRec; { Read-only return value }
var
  retVal  : PDBRec;
  db      : PDBFile;
begin
  retVal:= nil;
  errorNum:= 0;
  db:= GetDB(aFileName);
  if (db <> nil) then
  begin
    if not db^.dbc^.GetSeq(db^.dbr, aSeq) then
      errorNum:= db^.dbc^.dbErrorNum
    else
      retVal:= db^.dbr;
  end;
  PeekSeq:= retVal;
end;

{ False if error or if selection cancelled }
function TListObject.SelectMnemList(aParent: PWindowsObject;
                                     aFileName, aID: PChar;
                                     aListRec: PMnemListRec): boolean;
var
  retVal    : boolean;
  db        : PDBFile;
  idFld     : integer;
  descFld   : integer;
  idSize    : word;
  descSize  : word;
  UDFlagFld : integer;
begin
  retVal:= False;
  errorNum:= 0;
  if GetMnemInfo(aFileName, db, idFld, idSize, descFld, descSize, UDFlagFld) then
  begin
    if SetKeyFields(db, aID) then
    begin
      if (DoListDialog(aParent, db, True, @Self, modeSelectEdit, idFld, idSize, descFld, descSize, UDFlagFld) = IDOK) then
      begin
        retVal:= UpdateListRec(aListRec, db, idFld, idSize, descFld, descSize);
      end;
    end;
  end;
  SelectMnemList:= retVal;
end;

{ False if error }
function TListObject.ViewOnlyMnemList(aParent: PWindowsObject; aFileName, aID: PChar): boolean;
var
  db        : PDBFile;
  idFld     : integer;
  descFld   : integer;
  idSize    : word;
  descSize  : word;
  UDFlagFld : integer;
begin
  errorNum:= 0;
  if GetMnemInfo(aFileName, db, idFld, idSize, descFld, descSize, UDFlagFld) then
  begin
    if SetKeyFields(db, aID) then
    begin
      DoListDialog(aParent, db, True, @Self, modeViewOnly, idFld, idSize, descFld, descSize, UDFlagFld);
    end;
  end;
  ViewOnlyMnemList:= (errorNum = 0);
end;

{ False if error }
function TListObject.EditableMnemList(aParent: PWindowsObject; aFileName, aID: PChar): boolean;
var
  db        : PDBFile;
  idFld     : integer;
  descFld   : integer;
  idSize    : word;
  descSize  : word;
  UDFlagFld : integer;
begin
  errorNum:= 0;
  if GetMnemInfo(aFileName, db, idFld, idSize, descFld, descSize, UDFlagFld) then
  begin
    if SetKeyFields(db, aID) then
    begin
      DoListDialog(aParent, db, True, @Self, modeEditOnly, idFld, idSize, descFld, descSize, UDFlagFld);
    end;
  end;
  EditableMnemList:= (errorNum = 0);
end;

{ GUARANTEE : When doExactMatch is true, a selection list will NEVER be displayed, or your money back!!! }
{ False if error or if selection cancelled }
function TListObject.FindMnemID(aParent: PWindowsObject;
                                aFileName, aID: PChar;
                                aListRec: PMnemListRec;
                                doExactMatch: boolean): boolean;
var
  retVal    : boolean;
  db        : PDBFile;
  idFld     : integer;
  descFld   : integer;
  idSize    : word;
  descSize  : word;
  UDFlagFld : integer;
begin
  retVal:= False;
  errorNum:= 0;
  if GetMnemInfo(aFileName, db, idFld, idSize, descFld, descSize, UDFlagFld) then
  begin
    if SetKeyFields(db, aID) then
    begin
      if db^.dbc^.GetEQ(db^.dbr) then
      begin
        retVal:= UpdateListRec(aListRec, db, idFld, idSize, descFld, descSize);
      end
      else if (db^.dbc^.dbErrorNum <> B_KEY_VALUE_NOT_FOUND + MOD_BTRV) then
        errorNum:= db^.dbc^.dbErrorNum
      else
      begin
        if (not doExactMatch) and listOnPartialID then
        begin
          if (DoListDialog(aParent, db, True, @Self, modeSelectEdit, idFld, idSize, descFld, descSize, UDFlagFld) = IDOK) then
          begin
            retVal:= UpdateListRec(aListRec, db, idFld, idSize, descFld, descSize);
          end;
        end;
      end;
    end;
  end;
  FindMnemID:= retVal;
end;

{ False if error }
function TListObject.FindMnemSeq( aFileName: PChar;
                                  aListRec: PMnemListRec;
                                  aSeq: TSeqNum): boolean;
var
  retVal    : boolean;
  db        : PDBFile;
  idFld     : integer;
  descFld   : integer;
  idSize    : word;
  descSize  : word;
  UDFlagFld : integer;
begin
  retVal:= False;
  errorNum:= 0;
  if GetMnemInfo(aFileName, db, idFld, idSize, descFld, descSize, UDFlagFld) then
  begin
    if db^.dbc^.GetSeq(db^.dbr, aSeq) then
    begin
      retVal:= UpdateListRec(aListRec, db, idFld, idSize, descFld, descSize);
    end;
  end;
  FindMnemSeq:= retVal;
end;

{ Zero if error }
function TListObject.GetMnemIDLen(aFileName: PChar): word;
var
  db    : PDBFile;
  idFld : integer;
  idLen : word;
begin
  idLen:= 0;
  errorNum:= 0;
  db:= GetDB(aFileName);
  if (db <> nil) then
  begin
    idFld:= GetIDFieldNum(db^.dbd);
    if (idFld > 0) then
    begin
      idLen:= db^.dbd^.FieldSizeForStr(idFld)-1;
      errorNum:= db^.dbd^.dbErrorNum;
    end;
  end;
  GetMnemIDLen:= idLen;
end;

{ Zero if error }
function TListObject.GetMnemDescLen(aFileName: PChar): word;
var
  db      : PDBFile;
  descFld : integer;
  descLen : word;
begin
  descLen:= 0;
  errorNum:= 0;
  db:= GetDB(aFileName);
  if (db <> nil) then
  begin
    descFld:= GetDescFieldNum(db^.dbd);
    if (descFld > 0) then
    begin
      descLen:= db^.dbd^.FieldSizeForStr(descFld)-1;
      errorNum:= db^.dbd^.dbErrorNum;
    end;
  end;
  GetMnemDescLen:= descLen;
end;

{ returns nil on error }
function TListObject.GetDB(aFileName: PChar): PDBFile;

  function Matches(aDB: PDBFile): boolean; far;
  var
    fi : TFileInfo;
  begin
    aDB^.dbd^.FileInfo(fi);
    Matches:= (StrIComp(aFileName, fi.fname) = 0);
  end;

var
  db : PDBFile;
begin
  errorNum:= 0;
  db:= dbFileColl^.FirstThat(@Matches);
  if (db = nil) then
  begin
    db:= New(PDBFile, Init(aFileName, '', dbOpenNormal));
    if (db = nil) then
      errorNum:= dbLastOpenError
    else
      dbFileColl^.Insert(db);
  end;
  if (db <> nil) then
    db^.dbr^.ClearRecord;
  GetDB:= db;
end;

function TListObject.SetKey(aDB: PDBFile): boolean;
var
  keyNum : integer;
  fName  : array[0..maxFileNameLen] of char;
begin
  errorNum:= 0;
  keyNum:= 1;
  if (StrIComp(aDB^.dbd^.FileName(fName, maxFileNameLen), dbSPECFile) = 0) then
  begin
    if aDB^.dbr^.FieldIsEmpty(dbSPECCollectDate) then
      keyNum:= DBSPEC_ID_KEY
    else
      keyNum:= DBSPEC_DATE_KEY;
  end;
  if not aDB^.dbc^.SetCurKeyNum(keyNum) then
    errorNum:= aDB^.dbc^.dbErrorNum;
  SetKey:= (errorNum = 0);
end;

{ returns 0 on error }
function TListObject.GetIDFieldNum(aDBD: PDBReadDesc): integer;
var
  fi  : TFldInfo;
  i   : integer;
begin
  errorNum:= 0;
  for i:= 1 to aDBD^.NumFields do
  begin
    if not aDBD^.FieldInfo(i, fi) then
    begin
      errorNum:= aDBD^.dbErrorNum;
      i:= 0;
      break;
    end;
    if ((fi.userFlag and db_MNMID) = db_MNMID) then
      break;
  end;
  if (i > 0) and ((fi.userFlag and db_MNMID) <> db_MNMID) then
  begin
    errorNum:= listErr_CantFindMnemIDField;
    i:= 0;
  end;
  GetIDFieldNum:= i;
end;

{ returns 0 on error }
function TListObject.GetDescFieldNum(aDBD: PDBReadDesc): integer;
var
  fi  : TFldInfo;
  i   : integer;
begin
  errorNum:= 0;
  for i:= 1 to aDBD^.NumFields do
  begin
    if not aDBD^.FieldInfo(i, fi) then
    begin
      errorNum:= aDBD^.dbErrorNum;
      i:= 0;
      break;
    end;
    if ((fi.userFlag and db_MNMDescr) = db_MNMDescr) then
      break;
  end;
  if (i > 0) and ((fi.userFlag and db_MNMDescr) <> db_MNMDescr) then
  begin
    errorNum:= listErr_CantFindMnemDescField;
    i:= 0;
  end;
  GetDescFieldNum:= i;
end;

{ returns 0 on error or if no UDFlag field found }
function TListObject.GetUDFlagFieldNum(aDBD: PDBReadDesc): integer;
var
  fi  : TFldInfo;
  i   : integer;
begin
  errorNum:= 0;
  for i:= 1 to aDBD^.NumFields do
  begin
    if not aDBD^.FieldInfo(i, fi) then
    begin
      errorNum:= aDBD^.dbErrorNum;
      i:= 0;
      break;
    end;
    if ((fi.userFlag and db_UDRec) = db_UDRec) then
      break;
  end;
  if (i > 0) and ((fi.userFlag and db_UDRec) <> db_UDRec) then
    i:= 0;
  GetUDFlagFieldNum:= i;
end;

function TListObject.GetMnemInfo( aFileName: PChar;
                                  var aDB: PDBFile;
                                  var idFld: integer;
                                  var idSize: word;
                                  var descFld: integer;
                                  var descSize: word;
                                  var UDFlagFld: integer): boolean;
begin
  errorNum:= 0;
  aDB:= GetDB(aFileName);
  if (aDB <> nil) then
  begin
    idFld:= GetIDFieldNum(aDB^.dbd);
    if (idFld > 0) then
    begin
      descFld:= GetDescFieldNum(aDB^.dbd);
      if (descFld > 0) then
      begin
        UDFlagFld:= GetUDFlagFieldNum(aDB^.dbd);
        if (errorNum = 0) then
        begin
          idSize:= aDB^.dbd^.FieldSizeForStr(idFld);
          errorNum:= aDB^.dbd^.dbErrorNum;
          if (errorNum = 0) then
          begin
            descSize:= aDB^.dbd^.FieldSizeForStr(descFld);
            errorNum:= aDB^.dbd^.dbErrorNum;
          end;
        end;
      end;
    end;
  end;
  GetMnemInfo:= (errorNum = 0);
end;

function TListObject.UpdateListRec( aListRec: PMnemListRec;
                                    aDB: PDBFile;
                                    idFld: integer;
                                    idSize: word;
                                    descFld: integer;
                                    descSize: word): boolean;
begin
  errorNum:= 0;
  if not aListRec^.SetIDAndDesc('', '', idSize, descSize) then
    errorNum:= aListRec^.errorNum
  else
  begin
    aDB^.dbr^.GetFieldAsStr(idFld, aListRec^.ID, idSize-1);
    errorNum:= aDB^.dbr^.dbErrorNum;
    if (errorNum = 0) then
    begin
      aDB^.dbr^.GetFieldAsStr(descFld, aListRec^.desc, descSize-1);
      errorNum:= aDB^.dbr^.dbErrorNum;
      if (errorNum = 0) then
      begin
        aListRec^.seq:= aDB^.dbr^.GetSeqValue;
        errorNum:= aDB^.dbr^.dbErrorNum;
      end;
    end;
  end;
  UpdateListRec:= (errorNum = 0);
end;

function TListObject.SetKeyFields(aDB: PDBFile; aKeyStr: PChar): boolean;
var
  keyInfo : TKeyInfo;
  segNum  : integer;
begin
  errorNum:= 0;
  if not aDB^.dbd^.KeyInfo(aDB^.dbc^.GetCurKeyNum, keyInfo) then
    errorNum:= aDB^.dbd^.dbErrorNum
  else
  begin
    for segNum:= 1 to keyInfo.numSegs do
    begin
      if not aDB^.dbr^.PutFieldAsStr(keyInfo.segList[segNum].fldNum, aKeyStr) then
      begin
        errorNum:= aDB^.dbr^.dbErrorNum;
        break;
      end;
    end;
  end;
  SetKeyFields:= (errorNum = 0);
end;

END.
