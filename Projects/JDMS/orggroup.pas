unit OrgGroup;

INTERFACE

uses
  WinDOS,
  DBFile,
  DBIds,
  Bits,
  Objects;

const
  OrgRecSize = 16;
  gStaph     =  1;
  gStrep     =  2;
  gEntro     =  3;
  gStPnm     =  4;
  gNFerm     =  5;
  gPsudo     =  6;
  gLMono     =  7;
  gPsAer     =  8;
  gCitro     =  9;
  gPAndP     =  10;
  gAAnit     =  11;
  gMmPm      =  12;
  gSalSg     =  13;
  gVStrp     =  14;
  gBStrp     =  15;
  gDAmal     =  16;
  gSerta     =  17;
  gCns       =  18;
  gBLacI     =  19;
  gSalmo     =  20;
  gXanth     =  21;
  gMiscO     =  32;

type
  OrgBitMapTyp = array[1..16] of byte;

  TOrgBuffer = array [1..OrgRecSize] of byte;

  POrgGrpObj = ^TOrgGrpObj;
  TOrgGrpObj = object(TObject)
    orgGrpFile : file of TOrgBuffer;
    bitmap     : TOrgBuffer;
    {}
    constructor Init;
    function OrgGReadRec(orgNum: integer): boolean;
    function OrgGWriteRec(orgNum: integer; var abitMap: OrgBitMapTyp): boolean;
    function OrgInGroup(orgNum: integer; oGroupNum: integer): boolean;
    procedure OrgGroupAbort(errNum: integer);
    destructor Done; virtual;
  end;

IMPLEMENTATION

uses
  DMString,
  MScan,
  DMSErr;

{$IFOPT I+}
  {$DEFINE chki}
{$ENDIF}
{$I-}

constructor TOrgGrpObj.Init;
{-----------------------------------------------------------------------}
{ Open the organism group data file.                                    }
{-----------------------------------------------------------------------}
begin
  if inherited Init then
  begin
    Assign(orgGrpFile, 'ORGGROUP.DAT');
    Reset(orgGrpFile);
    if IOResult <> 0 then
      OrgGroupAbort(1);
  end;
end;

destructor TOrgGrpObj.Done;
{-----------------------------------------------------------------------}
{ Close the organism group data file.                                   }
{-----------------------------------------------------------------------}
begin
  Close(orgGrpFile);
  if IOResult <> 0 then;
  inherited Done;
end;

function TOrgGrpObj.OrgGReadRec(orgNum: integer): boolean;
{-----------------------------------------------------------------------}
{ Read in an organism group bit map.                                    }
{-----------------------------------------------------------------------}
begin
  OrgGReadRec := false;
  Seek(orgGrpFile, orgNum);
  if eof(orgGrpFile) then
    Exit;
  Read(orgGrpFile,bitmap);
  OrgGReadRec := (IOResult = 0);
end;

function TOrgGrpObj.OrgInGroup(orgNum: integer; oGroupNum: integer): boolean;
{-----------------------------------------------------------------------}
{ OrgInGroup takes an org number and a group number constant and returns}
{ a TRUE if that organism is in that group.  A FALSE is returned if     }
{ there are any problems.                                               }
{-----------------------------------------------------------------------}
begin
  OrgInGroup := false;
  if (orgNum < 1) or (orgNum > 997) then
    Exit;
  if not OrgGReadRec(orgNum) then
    Exit;
  OrgInGroup := TestBit(bitMap, oGroupNum);
end;

procedure TOrgGrpObj.OrgGroupAbort(errNum: integer);
{-----------------------------------------------------------------------}
{ purpose :  Error out when some condition has been met.                }
{ input   :  errnum  - The error message to be displayed.               }
{-----------------------------------------------------------------------}
begin
  ShowError(nil, IDS_PROGHALT, nil, errNum, MOD_ORGGROUP, 0);
  Halt;
end;

function TOrgGrpObj.OrgGWriteRec(orgNum: integer; var abitMap: OrgBitMapTyp): boolean;
{-----------------------------------------------------------------------}
{ Write out an organism group bit map.                                  }
{-----------------------------------------------------------------------}
begin
  Seek(orgGrpFile, orgNum);
  Read(orgGrpFile, bitmap);
  OrgGWriteRec:= (IOResult = 0);
end;

{$IFDEF chki}
  {$I+}
  {$UNDEF chki}
{$ENDIF}


END.

