unit Cvt18;

INTERFACE

uses
  OWindows;

procedure Get18Data(aParent: PWindowsObject);

IMPLEMENTATION

uses
  StrSW,
  ODialogs,
  Mscan,
  Objects,
  Strings,
  CRCLib,
  Cvt18Map,
  CvtMisc,
  Cvt18Msc,
  ApiTools,
  WaitLib,
  userMsgs,
  WinDos,
  WinTypes,
  WinProcs,
  DlgLib;

{$R CVT18.RES}
{$I CVT18.INC}

type
  PCvt18RestoreDlg = ^TCvt18RestoreDlg;
  TCvt18RestoreDlg = object(TCenterDlg)
    diskId        : integer;
    indx          : integer;
    lastFile      : array[0..20] of char;
    appndFile     : array[0..20] of char;
    appndFlag     : boolean;
    dataSetId     : array[0..100] of char;
    mergeDir      : array[0..100] of char;
    restoreDir    : array[0..100] of char;
    listFN        : PListBox;
    custList      : PCollection;
    dataList      : PCollection;
    working       : boolean;
    {}
    constructor Init(aParent: PWindowsObject);
    procedure SetupWindow; virtual;
    procedure SetText(text: PChar; id: integer);
    procedure IDNext(var msg: TMessage); virtual ID_FIRST + IDC_NEXT;
    procedure IDExit(var msg: TMessage); virtual ID_FIRST + IDC_EXIT;
    procedure RestoreData(var msg: TMessage); virtual WM_FIRST + WM_FMPROCESS;
    function  Copy18Custom : integer;
    function  Copy18DataSet : integer;
    function  VerifyCustDisk: integer;
    function  VerifyDataSetDisk: integer;
    function  CopyV18CustomFiles : integer;
    function  CopyV18DataSetFiles : integer;
    procedure BuildRestoreLists;
    function  CreateV18IndexFiles : integer;
    procedure EnableButtons; virtual;
    function CanClose: boolean; virtual;
    procedure Cancel(var msg: TMessage); virtual ID_FIRST + ID_CANCEL;
  end;

{-----{ Misc functions ] -----}
function GetDiskNumber(diskStr : pChar): integer;
var
  tempStr : array[0..2] of char;
  tempInt : integer;
  code    : integer;

begin
  tempStr[0] := #0;
  tempStr[1] := #0;
  tempStr[2] := #0;
  tempInt := -1;
  if diskStr[0] <> '/' then
    tempStr[0] := diskStr[0];
  if diskStr[1] <> '/' then
    tempStr[1] := diskStr[1];
  val(tempStr,tempInt, code);

  GetDiskNumber := tempInt;
end;


{-----{ TCvt18RestoreDlg ] -----}
constructor TCvt18RestoreDlg.Init(aParent: PWindowsObject);
begin
  inherited Init(aParent, MakeIntResource(DLG_CVT18RESTORE));
  working:= false;
  listFN := new(PListBox, InitResource(@self,IDC_STATLIST));
  StrCopy(restoreDir, curDMSDir);
  StrCat(restoreDir, OldDataDir);
  StrCopy(mergeDir, curDMSDir);
  StrCat(mergeDir, MergeDataDir);
  custList   := new(PCollection, Init(20,5));
  dataList   := new(PCollection, Init(20,5));
end;

procedure TCvt18RestoreDlg.SetupWindow;
begin
  inherited SetupWindow;
  EnableWindow(GetItemHandle(IDC_NEXT), false);
  EnableWindow(GetItemHandle(IDC_EXIT), false);
  PostMessage(HWindow, WM_FMPROCESS, 0, 0);
end;

procedure TCvt18RestoreDlg.SetText(text: PChar; id: integer);
begin
  if id <> 0 then {- set text of control }
    SendDlgItemMsg(id, WM_SETTEXT, 0, longint(text))
  else {- set caption of dialog }
    SendMessage(HWindow, WM_SETTEXT, 0, longint(text));
end;


procedure TCvt18RestoreDlg.IDNext(var msg: TMessage);
begin
  ShowWindow(hWindow, SW_HIDE);
  MapV18Fields(@Self);
  EndDlg(IDOk);
end;

procedure TCvt18RestoreDlg.IDExit(var msg: TMessage);
begin
  if ConfirmExit(hWindow) then
    EndDlg(IDOk);
end;

procedure TCvt18RestoreDlg.RestoreData(var msg: TMessage);
var
  pStr       : array[0..100] of char;
  confirmStr : array[0..100] of char;
  CRC        : word;
  retVal     : integer;

begin
  working:= true;
  retVal := continueJob;
  EnableButtons;
  CRC := 0;

  SetText(SR(IDS_PREP_DISK,  pStr, 100),LBL_MSG);

  BuildRestoreLists;

  ClearCRC(Version18);

  if PrepareMergeDir <> 0 then
    retVal:= cancelJob;

  if not CleanDir(restoreDir, '*.*') then
    retVal := cancelJob;


(*  if retVal = continueJob then*)
(*    if not CleanDir(MergeDir, '*.*') then*)
(*      retVal := cancelJob;*)
(*  if retVal = continueJob then*)
(*    retVal := CopyFiles(curDMSDir, mergeDir, '*.db*');*)

  SetText(SR(IDS_PREP_COMPLETE, pStr, 100), LBL_MSG);

  { Restore Custom files    }
  { Setup restore variables }
  if retVal = continueJob then
  begin
    diskId := 1;
    StrCopy(lastFile,'');
    StrCopy(appndFile, '');
    appndFlag := false;
    StrCopy(dataSetID, customDisk);
    retVal := Copy18Custom;
  end;

  { Restore DataSet files    }
  { Setup restore variables }
  if retVal = continueJob then
  begin
    diskId := 1;
    StrCopy(lastFile,'');
    StrCopy(appndFile, '');
    appndFlag := false;
    StrCopy(dataSetID, '');
    retVal :=  Copy18DataSet;
  end;

  SetText(SR(IDS_CREATEINDEX, pStr, 100), LBL_MSG);
  if retVal = continueJob then
    retVal := CreateV18IndexFiles;

  if retVal = continueJob then
  begin
    if not GetDirCRC(restoreDir, CRC) then
      retVal := cancelJob
    else
      SetLastCRC(Version18, CRC);
  end;

  working:= false;
  EnableButtons;
  if (retVal = continueJob)  then
  begin
    SetText(SR(IDS_V18RESTORECOMP, pStr, sizeof(pstr)-1), LBL_MSG);
    MessageBeep(0);
  end
  else
  begin
    ErrorMsg(hWindow, 'Error', 'Error preparing hard disk');
    EndDlg(IDOk);
  end;
  FocusCtl(hWindow, IDC_NEXT);
end;

function TCvt18RestoreDlg.Copy18Custom: integer;
var
  pStr        : array[0..100] of char;
  confirmStr  : array[0..100] of char;
  retVal      : integer;

begin
  retVal := continueJob;
  indx   := 0;
  AddNumToString(IDS_INSERTCUSTDISK, diskId, pStr, 100);
  if PromptForDisk(hwindow, SR(IDS_V1810RESTORE, confirmStr, 100), pStr, false) then
  begin
    repeat
      AddNumToString(IDS_RESTORE_CUSTOM, diskId, pStr, 100);
      SetText(pStr, LBL_MSG);
      retVal := VerifyCustDisk;
      if retVal = continueJob then
        retVal := CopyV18CustomFiles;
      if (retVal = continueJob) then
      begin
        Inc(diskId);
        AddNumToString(IDS_INSERTCUSTDISK, diskId, pStr, 100);
        if not PromptForDisk(hwindow, SR(IDS_V1810RESTORE, confirmStr, 100), pStr, false) then
          retVal := cancelJob;
      end;
    until retVal <> continueJob;
  end
  else
    retVal := cancelJob;

  if retVal = restoreComplete then
    retVal := continueJob;

  Copy18Custom := retVal;
end;

function TCvt18RestoreDlg.VerifyCustDisk: integer;
{- Verifies that the correct disk was inserted into the floppy drive.
   if the correct disk is in the floppy disk retrieve the dataSet id,
   disk number, file count, the last file on the disk and check to see
   if the file spans to the next disk. }
var
  oldMode   : Word;
  retVal    : integer;
  dirInfo   : TSearchRec;
  pStr      : array [0..100] of char;
  errorMsg  : array [0..100] of char;
  bId       : array[0..25] of char;
  bDiskId   : array[0..25] of char;
  bdummy    : array[0..25] of char;
  f         : Text;
  i         : integer;
  newDiskId : integer;
begin
  retVal := continueJob;
  oldMode := SetErrorMode(SEM_FAILCRITICALERRORS);
  repeat
    FindFirst('A:DATAID.TXT', faReadOnly or faArchive, dirInfo);
    if DosError = 0 then
    begin
      retVal := continueJob;
      Assign(f, 'A:DATAID.TXT');
      Reset(f);
      readln(f,bId);       { DataSet Name        }
      readln(f,bdummy);    { DataSet Backup date }
      readln(f,bdummy);    { DataSet DMS Version }
      readln(f,bDiskId);   { DataSet Disk Number }
      readln(f,bdummy);    { DataSet # of Sectors}
      while (not Eof(f)) do
        readln(f,bdummy);  { DataSet files on disk }
      close(f);
      if(bdummy[0] = 'Z') then
      begin
        appndFlag := true;
        TrimLead(lastFile, bdummy, 'Z', 25);
      end
      else
      begin
        appndFlag := false;
        TrimLead(lastFile, bdummy, ' ', 25);
      end;

      newDiskId := getDiskNumber(bDiskId);
      if (StrComp(bId, dataSetId) <> 0) or (newDiskId <> diskId) then
      begin
        AddNumToString(IDS_INSERTCUSTDISK, diskId, pStr, 100);
        if PromptForDisk(hwindow, 'Error', pStr, true) then   {- retry }
          retVal := tryNewDisk
        else
          retVal := cancelJob;
      end;
    end
    else
    begin {- DosError  -}
      AddNumToString(IDS_INSERTCUSTDISK, diskId, pStr, 100);
      if PromptForDisk(hwindow, 'Error', pStr, true) then
        retVal := tryNewDisk
      else
        retVal := cancelJob;
    end;
  until retVal <> tryNewDisk;

  SetErrorMode(oldMode);
  VerifyCustDisk := retVal;
end;

function TCvt18RestoreDlg.copyV18CustomFiles: integer;
var
  src         : array[0..100] of char;
  dest        : array[0..100] of char;
  pfn         : pChar;
  moreFiles   : boolean;
  retVal      : integer;
  appndToEnd  : boolean;

begin
  retVal := continueJob;
  moreFiles := true;
  while (moreFiles) and (indx < custList^.Count) do
  begin
    pfn := custList^.At(indx);
    appndToEnd := false;
    if (StrComp(appndFile, pfn) = 0) then
      appndToEnd := true;
    StrCopy(src, 'A:');
    StrCat(src, pfn);
    StrCopy(dest, restoreDir);
    StrCat(dest, pfn);
    AddNameToList(pfn, listFN);
    retVal := CopyFile(src, dest, appndToEnd);
    if (StrComp(lastFile, pfn) = 0) then
    begin
      if appndFlag then
        StrCopy(appndFile, lastFile)
      else if (strComp(lastFile,custList^.At(custList^.Count-1)) =0 ) then
        retVal := restoreComplete;
      moreFiles := false;
    end
    else
      inc(indx);
  end;
  copyV18CustomFiles := retVal;
end;

function TCvt18RestoreDlg.Copy18DataSet: integer;
var
  pStr        : array[0..100] of char;
  confirmStr  : array[0..100] of char;
  retVal      : integer;

begin
  retVal := continueJob;
  indx := 0;
  AddNumToString(IDS_INSERTDATADISK, diskId, pStr, 100);
  if PromptForDisk(hwindow, SR(IDS_V1810RESTORE, confirmStr, 100), pStr, false) then
  begin
    repeat
      AddNumToString(IDS_RESTORE_DATASET, diskId, pStr, 100);
      SetText(pStr,LBL_MSG);
      retVal := VerifyDataSetDisk;
      if (retVal = continueJob) then
        retVal := CopyV18DataSetFiles;
      if (retVal = continueJob) then
      begin
        inc(diskId);
        AddNumToString(IDS_INSERTDATADISK, diskId, pStr, 100);
        if not PromptForDisk(hwindow, SR(IDS_V1810RESTORE, confirmStr, 100), pStr, false) then
          retVal := cancelJob;
      end;
    until retVal <> continueJob;
  end
  else
    retVal := cancelJob;

  if retVal = restoreComplete then
    retVal := continueJob;

  Copy18DataSet := retVal;
end;

{- Verifies that the correct disk was insert into the floppy drive.
   if the correct disk is in the floppy disk retrieve the dataSet id,
   disk number, file count, the last file on the disk and check to see
   if the file spans to the next disk. }
function TCvt18RestoreDlg.VerifyDataSetDisk: integer;
var
  oldMode   : Word;
  retVal    : integer;
  dirInfo   : TSearchRec;
  pStr      : array [0..100] of char;
  errorMsg  : array [0..100] of char;
  bId       : array[0..25] of char;
  bDiskId   : array[0..25] of char;
  bdummy    : array[0..25] of char;
  f         : Text;
  i         : integer;
  newDiskId : integer;

begin
  retVal := continueJob;
  oldMode := SetErrorMode(SEM_FAILCRITICALERRORS);
  repeat
    FindFirst('A:DATAID.TXT', faReadOnly or faArchive, dirInfo);
    if DosError = 0 then
    begin
      retVal := continueJob;
      Assign(f, 'A:DATAID.TXT');
      Reset(f);
      readln(f,bId);       { DataSet Name        }
      readln(f,bdummy);    { DataSet Backup date }
      readln(f,bdummy);    { DataSet DMS Version }
      readln(f,bDiskId);   { DataSet Disk Number }
      readln(f,bdummy);    { DataSet # of Sectors}
      while (not Eof(f)) do
        readln(f,bdummy);  { DataSet files on disk }
      close(f);
      if(bdummy[0] = 'Z') then
      begin
        appndFlag := true;
        TrimLead(lastFile, bdummy, 'Z', 25);
      end
      else
      begin
        appndFlag := false;
        TrimLead(lastFile, bdummy, ' ', 25);
      end;

      if diskId = 1 then
        StrCopy(dataSetId, bId);

      newDiskId := getDiskNumber(bDiskId);

      if (StrComp(bid, customDisk) = 0) or (StrComp(bId, dataSetId) <> 0) or (newDiskId <> diskId) then
      begin
        AddNumToString(IDS_INSERTDATADISK, diskId, pStr, 100);
        if PromptForDisk(hwindow, 'Error', pStr, true) then
          retVal := tryNewDisk
        else
          retVal := cancelJob;
      end;
    end
    else
    begin {- DosError  -}
      AddNumToString(IDS_INSERTDATADISK, diskId, pStr, 100);
      if PromptForDisk(hwindow, 'Error', pStr, true) then
        retVal := tryNewDisk
      else
        retVal := cancelJob;
    end;
  until retVal <> tryNewDisk;

  SetErrorMode(oldMode);
  VerifyDataSetDisk := retVal;
end;

function TCvt18RestoreDlg.copyV18DataSetFiles: integer;
var
  src         : array[0..100] of char;
  dest        : array[0..100] of char;
  pfn         : pChar;
  moreFiles   : boolean;
  retVal      : integer;
  appndToEnd  : boolean;

begin
  retVal := continueJob;
  moreFiles := true;
  while (moreFiles) and (indx < dataList^.Count) do
  begin
    pfn := dataList^.At(indx);
    appndToEnd := false;
    if (StrComp(appndFile, pfn) = 0) then
      appndToEnd := true;
    StrCopy(src, 'A:');
    StrCat(src, pfn);
    StrCopy(dest, restoreDir);
    StrCat(dest, pfn);
    AddNameToList(pfn, listFN);
    retVal := CopyFile(src, dest, appndToEnd);
    if (StrComp(lastFile, pfn) = 0) then
    begin
      if appndFlag then
        StrCopy(appndFile, lastFile)
      else
        if (strComp(lastFile,dataList^.At(dataList^.Count-1)) =0 ) then
          retVal := restoreComplete;
      moreFiles := false;
    end
    else
      inc(indx);
  end;
  copyV18DataSetFiles := retVal;
end;

function TCvt18RestoreDlg.CreateV18IndexFiles: integer;
var
  retVal         : integer;

begin
  retVal := continueJob;

  { Create the BTrieve DBD and DBI files }
  retVal := CreateIndexFiles(restoreDir);

  if retVal = continueJob then
    retVal := LoadIndexFiles(restoreDir);

  CreateV18IndexFiles := retVal;
end;

procedure TCvt18RestoreDlg.EnableButtons;
begin
  EnableWindow(GetItemHandle(IDC_NEXT), not working);
  EnableWindow(GetItemHandle(IDC_EXIT), not working);
end;

function TCvt18RestoreDlg.CanClose: boolean;
begin
  if not working then
    CanClose:= ConfirmExit(hWindow)
  else
    CanClose:= false;
end;

procedure TCvt18RestoreDlg.Cancel(var msg: TMessage);
begin
  if CanClose then
    inherited Cancel(msg);
end;

procedure TCvt18RestoreDlg.BuildRestoreLists;
begin
  { - Custom Files - }
  custList^.Insert(strNew('HOSPITAL.NAM'));
  custList^.Insert(strNew('ORG.NAM'));
  custList^.Insert(strNew('DRUGSUPP.IDX'));
  custList^.Insert(strNew('LONGNAME.TBL'));
  custList^.Insert(strNew('SGCONFIG.TBL'));
  custList^.Insert(strNew('DOSAGE.DAT'));
  custList^.Insert(strNew('THERAPY.XRF'));
  custList^.Insert(strNew('OFFLINE.FIL'));
  custList^.Insert(strNew('DEE.BIN'));
  custList^.Insert(strNew('CUSTOM.FIL'));
  custList^.Insert(strNew('CUSTOM.IDX'));
  custList^.Insert(strNew('MPFORMAT.FIL'));
  custList^.Insert(strNew('ORGGROUP.DAT'));
  custList^.Insert(strNew('AUTOPRN.DAT'));
  custList^.Insert(strNew('AUTOPRN.CST'));
  custList^.Insert(strNew('AUTOPRN.QP'));
  custList^.Insert(strNew('TMPMISC.FIL'));

  { - DataSet Files - }
  dataList^.Insert(strNew('DM$DATA.DAT'));
  dataList^.Insert(strNew('DM$PATS.IDX'));
  dataList^.Insert(strNew('DM$SPEC.IDX'));
  dataList^.Insert(strNew('DM$FREE.DAT'));
end;

procedure Get18Data(aParent: PWindowsObject);
begin
  application^.ExecDialog(New(PCvt18RestoreDlg, Init(aParent)));
end;

END.

