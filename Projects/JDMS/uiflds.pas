unit UIFlds;

{- User Interface field objects }

INTERFACE

uses
  MScan,
  DBTypes,
  DBLib,
  ListLib,
  Ctl3D,
  Objects,
  ODialogs,
  OWindows,
  WinTypes;

const
  UIListKey   = VK_F3;
  UIListKey2  = VK_PRIOR;

  {- error codes returned from IsValid }
  UIErrRequired       = MOD_UIFLDS + 0;   {- field is required }
  UIErrInvalid        = MOD_UIFLDS + 1;   {- invalid field format }
  UIErrInvalidMnem    = MOD_UIFLDS + 2;   {- invalid mnem (ID not found) }
  UIErrInvalidNumber  = MOD_UIFLDS + 3;   {- invalid numeric format }
  UIErrOutOfRange     = MOD_UIFLDS + 4;   {- number out of range }
  UIErrMemAlloc       = MOD_UIFLDS + 5;   {- cannot allocate memory }

  {- Field type constants. These constants can be used to represent the field
     types defined below.  If a new field type is added, then a corresponding
     constant should be added }
  UIEditType         = 1;  {- generic edit field }
  UIMnemType         = 2;  {- mnemonic edit field }
  UIUpcaseType       = 3;  {- uppercase field type }
  UIFreeTextType     = 4;  {- free text field type }
  UICheckType        = 5;  {- check box field (for boolean fields) }
  UIExpandIDType     = 6;  {- expanded ID type (specimen/patient ID) }
  UIRealType         = 7;
  UIIntType          = 8;  {- integer type field (shortint through longint) }
  UIDateType         = 9;
  UITimeType         = 10;
  UIBoolType         = 12;
  UIBlockType        = 13;
  UIListEditType     = 14; {- list edit field }

type
  {- TUIFld is the base type for all UI fields. }
  PUIFld = ^TUIFld;
  TUIFld = object(TObject)
    ctl         : PControl;      {- the control itself }
    listRec     : PMnemListRec;  {- this field contains mnem info ONLY when the
                                    field is tied to a mnem seq ref field.
                                    THIS IS READ ONLY }
    {- construction / destruction }
    constructor Init(
      aParent : PWindowsObject;  {- parent window object }
      aDBd    : PDBReadDesc;     {- db descriptor (required) }
      aListObj: PListObject;     {- list object }
      isReqd  : boolean;         {- true if field is required }
      aFldNum : word;            {- fld # in DB record }
      aCtlID  : word;            {- res ID for control }
      aLblID  : word);           {- res ID for a TStatic label for the fld (0=no label) }
    constructor PostInit(
      aParent : PWindowsObject;
      aDBD    : PDBReadDesc;
      aListObj: PListObject;
      isReqd  : boolean;
      aFldNum : word;
      aCtlID  : word;
      aLblID  : word;
      aCtlTemplate: word;        {- id of template resource for edit control }
      aLblTemplate: word);       {- id of template resource for label }

    destructor Done; virtual;

    {- virtual methods }
    procedure XferRecToCtl(aDBR: PDBRec); virtual;
    function XFerCtlToRec(aDBR: PDBRec): boolean; virtual;

    procedure Clear; virtual;
    procedure Enable(on: boolean); virtual;
    procedure Show(on: boolean);
    procedure SetRequired(on: boolean);
    procedure FocusFld;
    procedure SetNewLabel(newLbl: PChar); virtual;
    procedure ListData; virtual;

    function FldNumber: integer;  {- return the field number for field }
    function GetID: word;         {- return the resource ID for control }
    function GetCtlType: integer; {- returns the current control type }
    function GetDBD: PDBReadDesc; {- return a pointer to the flds DB descriptor READ/ONLY }
    function GetCtlAsStr(aStr: PChar; maxLen: word): integer; virtual;
    function SetCtlAsStr(aStr: PChar): integer;

    function Format: integer; virtual;

    procedure UseUpperOnly(on: boolean);
    function IsUD: boolean; virtual;
    function IsEnabled: boolean;
    function IsValid: integer; virtual;
    function IsEmpty: boolean; virtual;
    function IsModified(aDBR: PDBRec): boolean; virtual;
    {}
    private
    lbl         : PStatic;       {- label of control (if any) }
    parent      : PWindowsObject;{- the parent dialog owning the field }
    fldNum      : integer;       {- the field number for the field }
    dbd         : PDBReadDesc;   {- a db description object }
    fldStrLen   : word;          {- the length of the fld returned as str from DB }
    fldStr      : PChar;         {- maximum length of the field (as string) }
    fldStr2     : PChar;         {- another buffer }
    fldInfo     : TFldInfo;      {- the database field info record for the db fld }
    ctlCreated  : boolean;       {- true=control created, false=control not created}
    hidden      : boolean;
    postCreate  : boolean;       {- true if control is post-created }
    ctlType     : integer;       {- control type index }
    assocFName  : array[0..9] of char;
    listObj     : PListObject;
    clearFlag   : integer;
    validSeq    : TSeqNum;
    isRequired  : boolean;
    {}
    function Initialize(aParent : PWindowsObject; aDBd: PDBReadDesc; aListObj: PListObject;
                        isReqd: boolean; aFldNum, aCtlID, aLblID: word): boolean; virtual;
    procedure CalcControlType; virtual;
    procedure BindToControl(aCtlID, aTemplate: word); virtual;
    procedure BindToLabel(aLblID, aTemplate: word); virtual;
    procedure ControlCreated; virtual;
    procedure CtlLostFocus(newFoc: HWnd); virtual;
    procedure CtlGotFocus(oldFoc: HWnd); virtual;
    procedure CharPressed(var vKey: word); virtual;
    procedure CheckTemplate(aTemplate: word; var font: HFont; var x,y,w,h: word);
    procedure SetupFromTemplate(aTemplate: word; aCtl: PControl; font: HFont);
    procedure LookUpData;
    procedure TrimIt; virtual;
  end;

  {- A static UI field }
  PUIStaticFld = ^TUIStaticFld;
  TUIStaticFld = object(TUIFld)
    constructor Init(
      aParent : PWindowsObject;  {- parent window object }
      aDBd    : PDBReadDesc;     {- db descriptor (required) }
      aListObj: PListObject;     {- list object }
      aFldNum : word;            {- fld # in DB record }
      aCtlID  : word;            {- res ID for control }
      aLblID  : word);           {- res ID for a TStatic label for the fld (0=no label) }
    constructor PostInit(
      aParent : PWindowsObject;
      aDBD    : PDBReadDesc;
      aListObj: PListObject;
      aFldNum : word;
      aCtlID  : word;
      aLblID  : word;
      aCtlTemplate: word;        {- id of template resource for edit control }
      aLblTemplate: word);       {- id of template resource for label }

    {- virtual methods }
    function XFerCtlToRec(aDBR: PDBRec): boolean; virtual;
    function Format: integer; virtual;
    function IsValid: integer; virtual;
    private
    procedure BindToControl(aCtlID, aTemplate: word); virtual;
  end;

  PUIListFld = ^TUIListFld;
  TUIListFld = object(TUIFld)
    constructor Init(
      aParent : PWindowsObject;
      aDBd    : PDBReadDesc;
      isReqd  : boolean;         {- true if field is required }
      aFldNum : word;            {- fld # in DB record }
      aCtlID  : word;            {- res ID for control }
      aLblID  : word;            {- res ID for a TStatic label for the fld (0=no label) }
      aTitle  : PChar;           {- title for list box }
      aHeader : PChar;           {- Header text for list box grid }
      aList   : PCollection);    {- collection of PListEditItem's (see ctllib) }
                                 {  This hold a list of valid data }
    constructor PostInit(
      aParent : PWindowsObject;
      aDBD    : PDBReadDesc;
      isReqd  : boolean;
      aFldNum : word;
      aCtlID  : word;
      aLblID  : word;
      aCtlTemplate: word;        {- id of template resource for edit control }
      aLblTemplate: word;        {- id of template resource for label }
      aTitle  : PChar;
      aHeader : PChar;
      aList   : PCollection);
    private
    list      : PCollection;
    title     : PChar;
    header    : PChar;
    procedure BindToControl(aCtlID, aTemplate: word); virtual;
    procedure CalcControlType; virtual;
  end;

  PUICheckFld = ^TUICheckFld;
  TUICheckFld = object(TUIFld)
    {- construction / destruction }
    constructor Init(
      aParent : PWindowsObject;  {- parent window object }
      aDBd    : PDBReadDesc;     {- db descriptor (required) }
      aFldNum : word;            {- fld # in DB record }
      aCtlID  : word;            {- res ID for control }
      useFieldName: boolean);      {- if true, then replace caption with fld name}
    constructor PostInit(
      aParent : PWindowsObject;
      aDBD    : PDBReadDesc;
      aFldNum : word;
      aCtlID  : word;
      aCtlTemplate: word;        {- id of template resource for edit control }
      aTitle  : PChar;
      useFieldName: boolean);

    destructor Done; virtual;
    procedure XferRecToCtl(aDBR: PDBRec); virtual;
    function XFerCtlToRec(aDBR: PDBRec): boolean; virtual;
    procedure SetNewLabel(newLbl: PChar); virtual;
    procedure Clear; virtual;
    function Format: integer; virtual;
    function IsValid: integer; virtual;
    function IsEmpty: boolean; virtual;
    function IsModified(aDBR: PDBRec): boolean; virtual;
    function GetCtlAsStr(aStr: PChar; maxLen: word): integer; virtual;
    {}
    private
    useFldName  : boolean;
    title       : PChar;
    procedure CalcControlType; virtual;
    procedure BindToControl(aCtlID, aTemplate: word); virtual;
    procedure ControlCreated; virtual;
    procedure TrimIt; virtual;
  end;

  PUIFullEditFld = ^TUIFullEditFld;
  TUIFullEditFld = object(TUIFld)
    private
    procedure CalcControlType; virtual;
  end;

var
  {- This var controls how the UI flds process the WM_KILLFOCUS message. If
     it is false, then the UI flds will not send the WM_LOSTFOCUS message to
     the parent.  This is useful when the parent wants to pop up a message box
     or another dialog but does not want to be interrupted with the lost focus
     message. Ie, parent decides a field is invalid and wants to display an
     error. If UIfldsProcessFocus is true, popping up the error message will
     generate *another* lost focus to the parent. Setting it to false will fix
     this problem }
  uiFldsProcessFocus: boolean;

procedure ShowUIFldError(aParent: PWindowsObject; anError: integer; fld: PUIFld);
procedure CheckUserKey(parent: PWindowsObject; msg: TMessage);

IMPLEMENTATION

uses
  DlgLib,
  CtlLib,
  Strings,
  StrsW,
  GridLib,
  INILib,
  ApiTools,
  SpinEdit,
  DBFile,
  UserMsgs,
  IntlLib,
  DateLib,
  DMString,
  DMSErr,
  Win31,
  WinProcs;

{$R UIFLDS.RES}
{$I UIFLDS.INC}

{- User interface controls }

{- All new controls should be created from one of the base control described
   below. If a new control is needed, the a base for that control will have
   to be created that has the same interface as the base control shown below.

   Since there is no multiple inheritence in pascal, I had to create a base
   object for each type of control that may be used by a field object. Each
   base control has common methods but unfortunately the control pointer (ctl)
   in the field object is PControl. So, when a field need to access a control
   it must typecast it to the proper type (the type it was bound as in
   BindControl
}

type
  {- BASE CONTROLS ----------------------------------------------------------}
  PUIEditCtl = ^TUIEditCtl;
  TUIEditCtl = object(TEdit)
    uiFld   : PUIFld;
    upper   : boolean;
    {}
    constructor InitResource(aParent: PWindowsObject; anID, aTextLen: word;
                             aUIFld: PUIFld; forceUpper: boolean);
    constructor Init(aParent: PWindowsObject; anID, x,y,w,h, aTextLen: word;
                     aUIFld: PUIFld; forceUpper: boolean);
    procedure SetupWindow; virtual;
    procedure WMKillFocus(var msg: TMessage); virtual WM_FIRST + WM_KILLFOCUS;
    procedure WMSetFocus(var msg: TMessage); virtual WM_FIRST + WM_SETFOCUS;
    procedure WMChar(var msg: TMessage); virtual WM_FIRST + WM_CHAR;
    procedure WMKeyDown(var msg: TMessage); virtual WM_FIRST + WM_KEYDOWN;
    procedure WMRButtonDown(var msg: TMessage); virtual WM_FIRST + WM_RBUTTONDOWN;
    procedure WMLButtonDown(var msg: TMessage); virtual WM_FIRST + WM_LBUTTONDOWN;
    {- following methods are required in all TEdit type base controls }
    function IsValid: integer; virtual;
    function Format: integer; virtual;
  end;

  PUIListEditCtl = ^TUIListEditCtl;
  TUIListEditCtl = object(TListEdit)
    uiFld   : PUIFld;
    constructor InitResource(aParent: PWindowsObject; anID: word;
                             aUIFld: PUIFld; aTitle, aListHeader: PChar; aList: PCollection);
    constructor Init(aParent: PWindowsObject; anID, x,y,w,h: word;
                            aUIFld: PUIFld; aTitle, aListHeader: PChar; aList: PCollection);
    procedure SetupWindow; virtual;
    procedure WMKillFocus(var msg: TMessage); virtual WM_FIRST + WM_KILLFOCUS;
    procedure WMSetFocus(var msg: TMessage); virtual WM_FIRST + WM_SETFOCUS;
    procedure WMKeyDown(var msg: TMessage); virtual WM_FIRST + WM_KEYDOWN;
    procedure WMRButtonDown(var msg: TMessage); virtual WM_FIRST + WM_RBUTTONDOWN;
    procedure WMLButtonDown(var msg: TMessage); virtual WM_FIRST + WM_LBUTTONDOWN;
    {- following methods are required in all TEdit type base controls }
    function IsValid: integer; virtual;
    function Format: integer; virtual;
    procedure DataSelected; virtual;
  end;

  PUINumCtl = ^TUINumCtl;
  TUINumCtl = object(TSpinReal)
    uiFld   : PUIFld;
    constructor InitResource(aParent: PWindowsObject; anID: integer; aUIFld: PUIFld;
                             aMin, aMax, aStep: double; showSpin: boolean);
    constructor Init(aParent: PWindowsObject; anID, x,y,w,h: word; aUIFld: PUIFld;
                     aMin, aMax, aStep: double; showSpin: boolean);
    procedure SetupWindow; virtual;
    procedure WMKillFocus(var msg: TMessage); virtual WM_FIRST + WM_KILLFOCUS;
    procedure WMSetFocus(var msg: TMessage); virtual WM_FIRST + WM_SETFOCUS;
    procedure WMKeyDown(var msg: TMessage); virtual WM_FIRST + WM_KEYDOWN;
    procedure WMChar(var msg: TMessage); virtual WM_FIRST + WM_CHAR;
    procedure WMLButtonDown(var msg: TMessage); virtual WM_FIRST + WM_LBUTTONDOWN;
    procedure WMRButtonDown(var msg: TMessage); virtual WM_FIRST + WM_RBUTTONDOWN;
    {- following methods are required in all TEdit type base controls }
    function IsValid: integer; virtual;
    function Format: integer; virtual;
  end;

  PUIIntCtl = ^TUIIntCtl;
  TUIIntCtl = object(TSpinInt)
    uiFld   : PUIFld;
    constructor InitResource(aParent: PWindowsObject; anID: integer; aUIFld: PUIFld;
                             aMin, aMax, aStep: longint; showSpin: boolean);
    constructor Init(aParent: PWindowsObject; anID, x,y,w,h: word; aUIFld: PUIFld;
                     aMin, aMax, aStep: longint; showSpin: boolean);
    procedure SetupWindow; virtual;
    procedure WMKillFocus(var msg: TMessage); virtual WM_FIRST + WM_KILLFOCUS;
    procedure WMSetFocus(var msg: TMessage); virtual WM_FIRST + WM_SETFOCUS;
    procedure WMKeyDown(var msg: TMessage); virtual WM_FIRST + WM_KEYDOWN;
    procedure WMChar(var msg: TMessage); virtual WM_FIRST + WM_CHAR;
    procedure WMLButtonDown(var msg: TMessage); virtual WM_FIRST + WM_LBUTTONDOWN;
    procedure WMRButtonDown(var msg: TMessage); virtual WM_FIRST + WM_RBUTTONDOWN;
    {- following methods are required in all TEdit type base controls }
    function IsValid: integer; virtual;
    function Format: integer; virtual;
  end;

  PUIDateCtl = ^TUIDateCtl;
  TUIDateCtl = object(TSpinEdit)
    uiFld   : PUIFld;
    constructor InitResource(aParent: PWindowsObject; anID: integer; aUIFld: PUIFld;
                             showSpin: boolean);
    constructor Init(aParent: PWindowsObject; anID, x,y,w,h: word; aUIFld: PUIFld;
                     showSpin: boolean);
    procedure SetupWindow; virtual;
    procedure WMKillFocus(var msg: TMessage); virtual WM_FIRST + WM_KILLFOCUS;
    procedure WMSetFocus(var msg: TMessage); virtual WM_FIRST + WM_SETFOCUS;
    procedure WMKeyDown(var msg: TMessage); virtual WM_FIRST + WM_KEYDOWN;
    procedure WMChar(var msg: TMessage); virtual WM_FIRST + WM_CHAR;
    procedure WMLButtonDown(var msg: TMessage); virtual WM_FIRST + WM_LBUTTONDOWN;
    procedure WMRButtonDown(var msg: TMessage); virtual WM_FIRST + WM_RBUTTONDOWN;
    procedure SpinUp; virtual;
    procedure SpinDown; virtual;
    function ExtractDate: TIntlDate;
    procedure DoSpin(anInc: integer);
    {- following methods are required in all TEdit type base controls }
    function IsValid: integer; virtual;
    function Format: integer; virtual;
  end;

  PUICheckCtl = ^TUICheckCtl;
  TUICheckCtl = object(TLCheckBox)
    uiFld   : PUIFld;
    constructor InitResource(aParent: PWindowsObject; anID: word; aUIFld: PUIFld);
    constructor Init(aParent: PWindowsObject; anID: word; aText: PChar;
                     x,y,w,h: word; aUIFld: PUIFld);
    procedure SetupWindow; virtual;
    procedure WMKillFocus(var msg: TMessage); virtual WM_FIRST + WM_KILLFOCUS;
    procedure WMSetFocus(var msg: TMessage); virtual WM_FIRST + WM_SETFOCUS;
    procedure WMKeyDown(var msg: TMessage); virtual WM_FIRST + WM_KEYDOWN;
    procedure WMChar(var msg: TMessage); virtual WM_FIRST + WM_CHAR;
    procedure WMLButtonDown(var msg: TMessage); virtual WM_FIRST + WM_LBUTTONDOWN;
    procedure WMRButtonDown(var msg: TMessage); virtual WM_FIRST + WM_RBUTTONDOWN;
  end;

  PUIStaticCtl = ^TUIStaticCtl;
  TUIStaticCtl = object(TLStatic)
    uiFld   : PUIFld;
    constructor InitResource(aParent: PWindowsObject; anID: word; bold: boolean;
                            aUIFld: PUIFld);
    constructor Init(aParent: PWindowsObject; anID: integer; aTitle: PChar;
                     x,y,w,h: integer; aTextLen: word; bold: boolean;
                     aUIFld: PUIFld);
    procedure SetupWindow; virtual;
  end;

  {- END OF BASE CONTROLS ----------------------------------------------------}

  {- specialized TUIEditCtl that allows entry for dbBlock field types (hex digits)}
  PUIBlockCtl = ^TUIBlockCtl;
  TUIBlockCtl = object(TUIEditCtl)
    procedure WMChar(var msg: TMessage); virtual WM_FIRST + WM_CHAR;
  end;

  {- Specialize edit control for Boolean field types. Allows toggle of value
     with the spacebar }
  PUIBoolCtl = ^TUIBoolCtl;
  TUIBoolCtl = object(TUIEditCtl)
    procedure WMChar(var msg: TMessage); virtual WM_FIRST + WM_CHAR;
    function IsValid: integer; virtual;
    function Format: integer; virtual;
  end;

  {- specialize edit control for Time fields. Provide current time when then
     list key is pressed }
  PUITimeCtl = ^TUITimeCtl;
  TUITimeCtl = object(TUIEditCtl)
    function IsValid: integer; virtual;
    function Format: integer; virtual;
  end;

  {- free text edit box used by the free text field type list function }
  PFreeTxtDlg = ^TFreeTxtDlg;
  TFreeTxtDlg = object(TCenterDlg)
    buf   : PChar;
    ed    : PEdit;
    len   : integer;
    {}
    constructor Init(aParent: PWindowsObject; aBuf: PChar; aMaxLen: integer);
    procedure SetupWindow; virtual;
    function CanClose: boolean; virtual;
    procedure EnableButtons; virtual;
    procedure IDClear(var msg: TMessage); virtual ID_FIRST + IDC_UICLEAR;
    procedure WMCommand(var msg: TMessage); virtual WM_FIRST + WM_COMMAND;
  end;

procedure CheckUserKey(parent: PWindowsObject; msg: TMessage);
{- if a function key was pressed that will not be processed by the control,
   then pass it along to the parent dialog to see if it would like to
   process it }
begin
  if (msg.wParam >= VK_F1) and (msg.wParam <= VK_F24) then
    PostMessage(parent^.HWindow, WM_USERKEY, msg.wParam, msg.lParam)
end;

procedure ShowUIFldError(aParent: PWindowsObject; anError: integer; fld: PUIFld);
var
  p1   : array[0..100] of char;
  p2   : array[0..100] of char;
begin
  if anError <> 0 then
  begin
    UIFldsProcessFocus:= false;
    case anError of
      UIErrRequired:
        strcopy(p1, SR(IDS_UIFLDSFLDREQ,p1, sizeof(p1)-1));
      UIErrInvalidMnem:
        strcopy(p1, SR(IDS_UIFLDSMNEMIDNOTFOUND,p1, sizeof(p1)-1));
      UIErrInvalidNumber:
        strcopy(p1, SR(IDS_UIFLDSINVALIDNUMBER, p1, sizeof(p1)-1));
      UIErrOutOfRange:
        strcopy(p1, SR(IDS_UIFLDSOUTOFRANGE, p1, sizeof(p1)-1));
      UIErrMemAlloc:
        strcopy(p1, SR(IDS_UIFLDSMEMALLOCERR, p1, sizeof(p1)-1));
      else
        strcopy(p1, SR(IDS_UIFLDSINVALIDFLD, p1, sizeof(p1)-1));
    end;
    StrCat(p1, #10#10);
    StrCat(p1, fld^.fldInfo.fldName);
    ErrorMsg(aParent^.HWindow, SR(IDS_UIFLDSFLDENTERR, p2, sizeof(p2)-1), p1);
    fld^.FocusFld;
    HandleEvents(aParent^.HWindow);
    UIFldsProcessFocus:= true;
  end;
end;

function IsListKey(key: word): boolean;
{- return true if the specified key is a list key }
begin
  if ((GetKeyState(VK_CONTROL) and $80) = 0) and
     ((GetKeyState(VK_SHIFT) and $80) = 0) then
    IsListKey:= (key = UIListKey) or (key = UIListKey2)
  else
    IsListKey:= false;
end;

{=============================================================================}

function TUIFld.Initialize(aParent : PWindowsObject; aDBd: PDBReadDesc;
                            aListObj: PListObject; isReqd: boolean;
                            aFldNum, aCtlID, aLblID: word): boolean;
var
  subst : PErrSubst;
  p1    : array[0..200] of char;
  p2    : array[0..25] of char;
begin
  Initialize:= false;

  {- first look at the parameters and see if everything is valid }
  if (aParent = nil) or (aCtlID <= 0) or (aDBd = nil) or
     (aFldNum < 0) or (aFldNum > aDBd^.NumFields) then
  begin
    Str(aFldNum, p1);
    subst:= New(PErrSubst, Init(p1));
    Str(aCtlID, p1);
    subst^.AddSubstStr(p1);
    ShowError(nil, IDS_UIFLDSINVINIT, subst, 0, MOD_UIFLDS, 0);
    MSDisposeObj(subst);
    exit;
  end;

  {- setup internal data members }
  dbd:= aDBd;
  fldNum:= aFldNum;
  parent:= aParent;
  ctl:= nil;
  lbl:= nil;
  ctlCreated:= false;
  hidden:= false;
  postCreate:= false;
  ctlType:= 0;
  clearFlag:= 0;
  validSeq:= 0;   {- 0 means field is empty }
  strcopy(assocFName, '');
  listRec:= nil;
  listObj:= aListObj;
  SetRequired(isReqd);

  {- get the length of the string returned as string from the database }
  fldStrLen:= dbd^.FieldSizeForStr(fldNum);
  if dbd^.dbErrorNum <> 0 then
  begin
    ShowError(nil, IDS_UIFLDSINVFLDNUM, nil, 0, MOD_UIFLDS, 0);
    Halt;
  end;
  dbd^.FieldInfo(fldNum, fldInfo);

  CalcControlType;

  GetMem(fldStr, fldStrLen+1);
  if fldStr = nil then
    exit;
  GetMem(fldStr2, fldStrLen+1);
  if fldStr2 = nil then
  begin
    MSFreeMem(fldStr, fldStrLen+1);
    exit;
  end;

  Initialize:= true;
end;

procedure TUIFld.CalcControlType;
{- figure out what type of field the UI field is going to represent. Sets the
   ctlType data member to the appropriate type }
var
  ai    : TAssocInfo;
  p1, p2: array[0..100] of char;
  subst : PErrSubst;
begin
  if fldInfo.fldType = dbSeqRef then      {- SEQREF FIELD TYPE }
  begin
    if ((fldInfo.userFlag and DB_Mnemonic) = 0) then
    begin
      {- attempt to link to a non-mnemonic seqref field type }
      strcopy(p1, 'Field ');  {- no need to translate this message, this should }
      str(fldNum, p2);        {- never happen in a released product }
      strcat(p1, p2);
      strcat(p1, ' (');
      strcat(p1, fldInfo.fldName);
      strcat(p1, ') in the ''');
      dbd^.FileName(p2, 100);
      strcat(p1, p2);
      strcat(p1, ''' file.');
      FatalError('Attempt to link edit field object to a TSeqRef field.', p1);
      Halt;
    end;
    if fldInfo.fldAssoc <= 0 then
    begin
      {- mnemonic field found with no association file name }
      subst:= New(PErrSubst, Init(fldInfo.fldName));
      ShowError(nil, IDS_UINOMNEMFILE, subst, 0, MOD_UIFLDS, 0);
      MSDisposeObj(subst);
      Halt;
    end;
    if listObj = nil then
    begin
      FatalError('Attempt to initialize a mnemonic field type', 'with a, nil PListObject.');
      Halt;
    end;

    listRec:= New(PMnemListRec, Init);
    if listRec = nil then
    begin
      ShowError(nil, IDS_CANTINITLISTREC, nil, 0, MOD_UIFLDS, 0);
      Halt;
    end;
    dbd^.AssocInfo(fldInfo.fldAssoc, ai);
    strcopy(assocFName, ai.fname);
    fldStrLen:= listObj^.GetMnemDescLen(assocFName) + 1;
    if listObj^.errorNum <> 0 then
    begin
      ShowError(nil, IDS_CANTINIT, nil, listObj^.errorNum, MOD_UIFLDS, 0);
      Halt;
    end;
    ctlType:= UIMnemType;
  end
  else if fldInfo.userFlag and DB_FREETEXT <> 0 then
    ctlType:= UIFreeTextType
  else if fldInfo.userFlag and DB_EXPANDID <> 0 then
    ctlType:= UIExpandIDType
  else
  begin
    case fldInfo.fldType of
      dbCode, dbZCode:
        ctlType:= UIUpcaseType;
      dbInteger, dbLongInt, dbWord, dbByte, dbShortInt:
        ctlType:= UIIntType;
      dbBoolean:
        ctlType:= UIBoolType;
      dbChar, dbString, dbZString:
        ctlType:= UIEditType;
      dbSingle, dbDouble:
        ctlType:= UIRealType;
      dbDate:
        ctlType:= UIDateType;
      dbTime:
        ctlType:= UITimeType;
      dbBlock:
        ctlType:= UIBlockType;
    else
      FatalError('Fatal Error', 'Attempt to bind a field object to an unsupported DB field type');
      Halt;
    end;
  end;
end;

constructor TUIFld.Init(aParent: PWindowsObject; aDBd: PDBReadDesc;
                        aListObj: PListObject; isReqd: boolean;
                        aFldNum, aCtlID, aLblID: word);
begin
  if not Initialize(aParent, aDBD, aListObj, isReqd, aFldNum, aCtlID, aLblID) then
    fail;

  inherited Init;

  {- label *must* be initialized first in order for the UI fld object to be
     able to set the label contents at window setup }
  if aLblID > 0 then
    BindToLabel(aLblID, 0);
  BindToControl(aCtlID, 0);
end;

{- postInit should be used if a field needs to be created *after* a dialog is
   already created. (after the dialog setupwindow) }
constructor TUIFld.PostInit(aParent : PWindowsObject; aDBD: PDBReadDesc;
                  aListObj: PListObject; isReqd: boolean; aFldNum, aCtlID, aLblID: word;
                  aCtlTemplate, aLblTemplate: word);
begin
  if not Initialize(aParent, aDBD, aListObj, isReqd, aFldNum, aCtlID, aLblID) then
    Fail;

  if aParent^.HWindow <= 0 then
  begin
    FatalError('Fatal Error', 'Attempt to POSTINIT on a dialog that has not been created.');
    Halt;
  end;

  postCreate:= true;

  inherited Init;

  if aLblID > 0 then
    BindToLabel(aLblID, aLblTemplate);
  BindToControl(aCtlID, aCtlTemplate);
end;

destructor TUIFld.Done;
begin
  uiFldsProcessFocus:= false;
  MSFreeMem(fldStr, fldStrLen + 1);
  MSFreeMem(fldStr2, fldStrlen + 1);
  MSDisposeObj(listRec);
  if postCreate then  {- if field was post created then destroy the controls manually }
  begin
    {- make sure that if the control currently has focus, that I move the
       focus before I destroy the control. If not things get really bad. BAM! }
    if GetFocus = ctl^.hWindow then
    begin
      PostMessage(parent^.hWindow, WM_NEXTDLGCTL, 0, 0);
      HandleEvents(parent^.hWindow);
    end;
    MSDisposeObj(lbl);
    MSDisposeObj(ctl);
  end;
  inherited Done;
  uiFldsProcessFocus:= true;
end;

procedure TUIFld.TrimIt;
{- trim all spaces from control }
begin
  SendMessage(ctl^.HWindow, WM_GETTEXT, fldStrLen, longint(fldStr));
  TrimAll(fldStr2, fldStr, ' ', fldStrLen);
  SendMessage(ctl^.HWindow, WM_SETTEXT, 0, longint(fldStr2));
end;

function TUIFld.IsValid: integer;
{- This function returns an error code to indicate whether or not a field is
   valid. see constants above for defined return values }
(* Need to put range checking in the spin edit controls!!!
   Need to test for required database fields *)
var
  err   : integer;
  num   : double;
  lNum  : longint;
  bErr  : boolean;
  dt    : TIntlDate;
begin
  err:= 0;
  TrimIt;
  if isRequired and IsEmpty then
    err:= UIErrRequired
  else
  begin
    case ctlType of
      UIUpcaseType:
        err:= PUIEditCtl(ctl)^.IsValid;
      UIRealType:
        begin
          err:= PUINumCtl(ctl)^.IsValid;
          if err = 0 then
          begin
            num:= Abs(PUINumCtl(ctl)^.GetDouble);
            if num <> 0 then
            case fldInfo.fldType of
              dbSingle:
                if ((num < 1.5e-45) or (num > 3.4e38)) then
                  err:= UIErrOutOfRange;
              dbDouble:
                if ((num < 5.0e-324) or (num > 1.7e308)) then
                  err:= UIErrOutOfRange;
            end;
          end;
        end;
      UIIntType:
        begin
          err:= PUIIntCtl(ctl)^.IsValid;
          if err = 0 then
          begin
            {- now check for number exceeding type limits }
            lNum:= PUIIntCtl(ctl)^.GetLong;
            case fldInfo.fldType of
              dbInteger:
                if (lnum < -MaxInt-1) or (lnum > MaxInt) then
                  err:= UIErrOutOfRange;
              dbLongint, dbSeqRef:
                if (lnum < -MaxLongint-1) or (lnum > MaxLongint) then
                  err:= UIErrOutOfRange;
              dbShortInt:
                if (lnum < -128) or (lnum > 127) then
                  err:= UIErrOutOfRange;
              dbByte:
                if (lnum < 0) or (lnum > 255) then
                  err:= UIErrOutOfRange;
              dbWord:
                if (lnum < 0) or (lnum > $FFFF) then
                  err:= UIErrOutOfRange;
            end;
          end;
        end;
      UIDateType:
        err:= PUIDateCtl(ctl)^.IsValid;
      UITimeType:
        err:= PUITimeCtl(ctl)^.IsValid;
      UIBoolType:
        err:= PUIBoolCtl(ctl)^.IsValid;
      UIBlockType:
        err:= PUIBlockCtl(ctl)^.IsValid;
      UIListEditType:
        err:= PUIListEditCtl(ctl)^.IsValid;
      UIMnemType:
        begin
          err:= PUIEditCtl(ctl)^.IsValid;
          if err = 0 then
          begin
            if clearFlag <> 0 then  {- user has typed something that was not expanded }
            begin
              clearFlag:= 0;
              LookUpData;
            end;
            if validSeq >= 0 then
              err:= 0
            else
              err:= UIErrInvalidMnem;
          end;
        end;
      else { case }
        err:= PUIEditCtl(ctl)^.IsValid;
    end;
  end;

  IsValid:= err;
end;

procedure TUIFld.LookUpData;
{- this method is only intended for use when the field is a Mnemonic fld }
var
  pstr  : array[0..100] of char;
begin
  if ctlType <> UIMnemType then exit;

  SendMessage(ctl^.HWindow, WM_GETTEXT, 100, longint(@pstr));
  if strlen(pstr) > 0 then
  begin
    if listObj^.FindMnemID(parent, assocFName, pstr, listRec, true) then
    begin
      SendMessage(ctl^.HWindow, WM_SETTEXT, 0, longint(listRec^.desc));
      validSeq:= listRec^.seq;
    end
    else
      validSeq:= -1;  {- flag as invalid code }
  end
  else
    Clear;
end;

function TUIFld.Format: integer;
{- format the control }
var
  left    : array[0..100] of char;
  right   : array[0..100] of char;
  p       : PChar;
  num     : double;
  code    : integer;
  ret     : integer;
begin
  ret:= IsValid;
  if ret = 0 then
  begin
    case ctlType of
      UIUpcaseType:
        ret:= PUIEditCtl(ctl)^.Format;
      UIRealType:
        ret:= PUINumCtl(ctl)^.Format;
      UIIntType:
        ret:= PUIIntCtl(ctl)^.Format;
      UIDateType:
        ret:= PUIDateCtl(ctl)^.Format;
      UITimeType:
        ret:= PUITimeCtl(ctl)^.Format;
      UIBoolType:
        ret:= PUIBoolCtl(ctl)^.Format;
      UIBlockType:
        ret:= PUIBlockCtl(ctl)^.Format;
      UIListEditType:
        ret:= PUIListEditCtl(ctl)^.Format;
      UIExpandIDType:
        begin
          ret:= PUIEditCtl(ctl)^.Format;
          if not IsEmpty and (ret = 0) then
          begin
            SendMessage(ctl^.HWindow, WM_GETTEXT, fldStrLen, longint(fldStr));
            p:= StrScan(fldStr, '.');
            if p <> nil then
            begin
              StrCopy(right, (p+1));
              p^:= #0;
              StrCopy(left, fldStr);
              GetMem(p, fldStrLen+1);
              StrCopy(p, '');
              LeftPad(fldStr, p, '0', fldStrLen - 1);
              MSFreeMem(p, fldStrLen+1);
              Move(left[0], fldStr[0], strlen(left));
              Move(right[0], fldStr[StrLen(fldStr)-StrLen(right)], StrLen(right));
              SendMessage(ctl^.HWindow, WM_SETTEXT, 0, longint(fldStr));
            end
            else
            begin
              Val(fldStr, num, code);
              if code = 0 then  {- standard number entered, left just with 0's }
              begin
                LeftPad(left, fldStr, '0', fldStrLen-1);
                SendMessage(ctl^.HWindow, WM_SETTEXT, 0, longint(@left));
              end
            end;
          end;
        end;
    else
      ret:= PUIEditCtl(ctl)^.Format;
    end;
  end;
  Format:= ret;
end;

function TUIFld.XFerCtlToRec(aDBR: PDBRec): boolean;
{- transfer the control to the associated field in the record buffer. returns
   false if unsuccessful }
begin
  XFerCtlToRec:= true;
  if (aDBR <> nil) then  {- make sure a record buffer has been specified }
  begin
    if not dbd^.CompareRecStruct(aDBR^.Desc) then
    else
    begin
      case ctlType of
        UIMnemType:
          aDBR^.PutField(fldNum, @listRec^.seq);
      else
        {- all others are edit controls }
        SendMessage(ctl^.HWindow, WM_GETTEXT, fldStrLen, longint(fldStr));
        aDBR^.PutFieldAsStr(fldNum, fldStr);
      end;
      XFerCtlToRec:= aDBR^.dbErrorNum = 0;
    end;
  end;
end;

procedure TUIFld.XferRecToCtl(aDBR: PDBRec);
{- transfer the associated field in the record buffer to the control }
var
  b   : boolean;
  seq : TSeqNum;
begin
  Clear;
  if aDBR <> nil then  {- make sure a record buffer has been specified }
  begin
    if not dbd^.CompareRecStruct(aDBR^.Desc) then
      ShowError(nil, IDS_INCOMPATIBLESTRUCT, nil, 0, MOD_UIFLDS, 0)
    else
    begin
      if ctlType = UIMnemType then
      begin
        aDBR^.GetField(fldNum, @seq, sizeof(seq));
        if (seq <> 0) and (listObj^.FindMnemSeq(assocFName, listRec, seq)) then
        begin
          SendMessage(ctl^.HWindow, WM_SETTEXT, 0, longint(listRec^.desc));
          validSeq:= listRec^.seq;
        end;
      end
      else
      begin
        {- all other fields are EDIT fields }
        aDBR^.GetFieldAsStr(fldNum, fldStr, fldStrLen);
        SendMessage(ctl^.HWindow, WM_SETTEXT, 0, longint(fldStr));
      end;
    end;
  end;
end;

procedure TUIFld.Clear;
{- clear the control }
begin
  SendMessage(ctl^.HWindow, WM_SETTEXT, 0, 0);
  if listRec <> nil then
    listRec^.Clear;
  validSeq:= 0;
  clearFlag:= 0;
end;

procedure TUIFld.Enable(on: boolean);
{- enable/disable the control }
var
  temp  : boolean;
begin
  temp:= uiFldsProcessFocus;
  uiFldsProcessFocus:= false;
  if not on and (GetFocus = ctl^.HWindow) then
    SendMessage(parent^.HWindow, WM_NEXTDLGCTL, 0, 0); {- focus next ctl }
  EnableWindow(ctl^.HWindow, on);
  if lbl <> nil then
    EnableWindow(lbl^.HWindow, on);
  uiFldsProcessFocus:= temp;
end;

procedure TUIFld.SetRequired(on: boolean);
begin
  isRequired:= on;
end;

procedure TUIFld.Show(on: boolean);
var
  mode  : integer;
begin
  if ctlCreated then
  begin
    if on then
      mode:= SW_SHOW
    else
    begin
      if (GetFocus = ctl^.HWindow) then
        SendMessage(parent^.HWindow, WM_NEXTDLGCTL, 0, 0); {- focus next ctl }
      mode:= SW_HIDE;
    end;
    ctl^.Show(mode);
    if lbl <> nil then
      lbl^.Show(mode);
  end;
  hidden:= not on;
end;

procedure TUIFld.UseUpperOnly(on: boolean);
{- tell the control to force upper case or not.  This is needed especially for
   ZCode field types (since they by default force upper case) when you specifically
   do not want to force uppercase }
begin
  if not (ctlType in [UIRealType, UIIntType, UIDateType, UIListEditType, UICheckType]) then
    PUIEditCtl(ctl)^.upper:= on;
end;

function TUIFld.IsUD: boolean;
{- return true if field is user defined }
begin
  IsUD:= fldInfo.userFlag and DB_UD <> 0;
end;

function TUIFld.IsEnabled: boolean;
{- return true if the control is enabled }
begin
  IsEnabled:= IsWindowEnabled(ctl^.HWindow);
end;

procedure TUIFld.FocusFld;
{- focus the control }
begin
  PostMessage(parent^.HWindow, WM_NEXTDLGCTL, ctl^.HWindow, 1);
end;

function TUIFld.FldNumber: integer;
{- return the field number that the field is associated with in the database }
begin
  FldNumber:= fldNum;
end;

function TUIFld.SetCtlAsStr(aStr: PChar): integer;
var
  err   : integer;
begin
  if ctlType = UICheckType then
  begin
    if StrToBool(aStr, err) then
      SendMessage(ctl^.Hwindow, BM_SETCHECK, BF_CHECKED, 0)
    else
      SendMessage(ctl^.Hwindow, BM_SETCHECK, BF_UNCHECKED, 0);
  end
  else
    SendMessage(ctl^.HWindow, WM_SETTEXT, 0, longint(aStr));
  SetCtlAsStr:= Format;
  LookUpData;
end;

function TUIFld.GetCtlAsStr(aStr: PChar; maxLen: word): integer;
{- gets control as it is represented as a string. Returns an error code (the
   same as Isvalid returns) if the content of the field is invalid. }
var
  ret : integer;
begin
  ret:= Format;
  if ret = 0 then
  begin
    SendMessage(ctl^.HWindow, WM_GETTEXT, fldStrLen, longint(fldStr));
    StrLCopy(aStr, fldStr, maxLen);
  end;
  GetCtlAsStr:= ret;
end;

function TUIFLd.GetDBD: PDBReadDesc;
begin
  GetDBD:= dbd;
end;

function TUIFld.GetCtlType: integer;
{- returns the current control type }
begin
  GetCtlType:= ctlType;
end;

function TUIFld.GetID: word;
{- return the resource ID for the control }
begin
  GetID:= word(ctl^.GetID);
end;

procedure TUIFld.SetNewLabel(newLbl: PChar);
begin
  if lbl <> nil then
    lbl^.SetText(newLbl);
end;

procedure TUIFld.ListData;
{- list data associated with the field. Owners can abort the list by returning non zero
   from the WM_FLDLIST function }
var
  d     : PFreeTxtDlg;
begin
  if SendMessage(parent^.HWindow, WM_FLDLIST, ctl^.GetID, longint(@self)) = 0 then
  begin
    uiFldsProcessFocus:= false;
    case ctlType of
      UIListEditType:
        PUIListEditCtl(ctl)^.ListData;
      UITimeType:
        begin   {- set current time }
          IntlTimeStr(IntlCurrentTime, INIShowSeconds, fldStr, fldStrLen);
          SendMessage(ctl^.HWindow, WM_SETTEXT, 0, longint(fldStr));
        end;
      UIMnemType:
        begin
          {- if the user has typed something, then use that text, otherwise just
             use the ID that was last loaded }
          if clearFlag = 1 then
            SendMessage(ctl^.HWindow, WM_GETTEXT, fldStrLen, longint(fldStr))
          else
            StrLCopy(fldStr, listRec^.ID, fldStrLen);

          if listObj^.SelectMnemList(parent, assocFName, fldStr, listRec) then
          begin
            SendMessage(ctl^.HWindow, WM_SETTEXT, 0, longint(listRec^.desc));
            validSeq:= listRec^.seq;
            clearFlag:= 0;
            PostMessage(parent^.HWindow, WM_MNEMSELECTED, GetID, longint(@self));
          end
          else
            if listObj^.errorNum <> 0 then
              ShowError(parent, IDS_MNEMLISTERR, nil, listObj^.errorNum, MOD_UIFLDS, 0);
        end;
      UIFreeTextType:
        begin
          SendMessage(ctl^.HWindow, WM_GETTEXT, fldStrLen, longint(fldStr));
          d:= New(PFreeTxtDlg, Init(parent, fldStr, fldStrLen));
          if application^.ExecDialog(d) = IDOK then
            SendMessage(ctl^.HWindow, WM_SETTEXT, 0, longint(fldStr));
        end;
    end;
    uiFldsProcessFocus:= true;
  end;
end;

function TUIFld.IsEmpty: boolean;
begin
  SendMessage(ctl^.HWindow, WM_GETTEXT, fldStrLen, longint(fldStr));
  TrimAll(fldStr2, fldStr, ' ', fldStrLen);
  IsEmpty:= StrLen(fldStr2) = 0;
end;

function TUIFld.IsModified(aDBR: PDBRec): boolean;
{- is modified is used to tell if value of the UI control is different from
   the value in the database record (for this field) }
var
  pstr  : PChar;
  seq   : TSeqNum;
begin
  if aDBR <> nil then
  begin
    if ctlType <> UIMnemType then
    begin
      SendMessage(ctl^.HWindow, WM_GETTEXT, fldStrLen, longint(fldStr));
      GetMem(pstr, fldStrLen+1);
      aDBR^.GetFieldAsStr(fldNum, pstr, fldStrLen);
      IsModified:= StrIComp(fldStr, pstr) <> 0;
      MSFreeMem(pstr, fldStrLen+1);
    end
    else
    begin
      aDBR^.GetField(fldNum, @seq, sizeof(seq));
      IsModified:= seq <> listRec^.Seq;
    end;
  end
  else
    IsModified:= false;
end;

procedure TUIFld.CheckTemplate(aTemplate: word; var font: HFont; var x,y,w,h: word);
var
  r     : TRect;
  pt    : TPoint;
  hw    : HWnd;
  pstr  : array[0..200] of char;
  p2    : array[0..20] of char;
begin
  {- get a handle to the template control }
  hw:= GetDlgItem(parent^.HWindow, aTemplate);
  if hw = 0 then
  begin
    StrCopy(pstr, 'Invalid template ID in UI Field (ID = ');
    Str(aTemplate, p2);
    StrCat(pstr, p2);
    StrCat(pstr, ')');
    FatalError('Fatal Error', pstr);
    Halt;
  end;

  {- get template font }
  font:= SendMessage(hw, WM_GETFONT, 0,0);
  {- get template size }
  GetWindowRect(hw, r);
  pt.x:= r.left;
  pt.y:= r.top;
  ScreenToClient(parent^.HWindow, pt);
  x:= pt.x; y:= pt.y;
  w:= r.right - r.left;
  h:= r.bottom - r.top;
end;

procedure TUIFld.SetupFromTemplate(aTemplate: word; aCtl: PControl; font: HFont);
var
  hw    : HWnd;
begin
  hw:= GetDlgItem(parent^.HWindow, aTemplate);
  application^.MakeWindow(aCtl);
  if font <> 0 then
    SendMessage(aCtl^.HWindow, WM_SETFONT, font, longint(true));
  SetWindowPos(aCtl^.HWindow, hw, 0,0,0,0, SWP_NOSIZE or SWP_NOMOVE);
  InvalidateRect(aCtl^.HWindow, nil, true);
end;

procedure TUIFld.BindToControl(aCtlID, aTemplate: word);
var
  p1, p2: array[0..100] of char;
  font  : HFont;
  x,y   : word;
  w,h   : word;
  len   : integer;
begin
  if aTemplate > 0 then
  begin {- late bound controls }
    CheckTemplate(aTemplate, font, x,y,w,h);
    case ctlType of
      UIUpcaseType, UIExpandIDType, UIMnemType:
        begin
          if ctlType = UIMnemType then
            len:= listObj^.GetMnemIDLen(assocFName) + 1
          else
            len:= fldStrLen;
          ctl:= New(PUIEditCtl, Init(parent, aCtlID, x,y,w,h, len, @self, true));
        end;
      UIRealType:
        ctl:= New(PUINumCtl, Init(parent, aCtlID, x,y,w,h, @self, 0, 0, 1.0, true));
      UIIntType:
        ctl:= New(PUIIntCtl, Init(parent, aCtlID, x,y,w,h, @self, 0, 0, 1, true));
      UIDateType:
        ctl:= New(PUIDateCtl, Init(parent, aCtlID, x,y,w,h, @self, true));
      UITimeType:
        ctl:= New(PUITimeCtl, Init(parent, aCtlID, x,y,w,h, fldStrLen, @self, false));
      UIBoolType:
        ctl:= New(PUIBoolCtl, Init(parent, aCtlID, x,y,w,h, fldStrLen, @self, false));
      UIBlockType:
        ctl:= New(PUIBlockCtl, Init(parent, aCtlID, x,y,w,h, fldStrLen, @self, false));
    else
      ctl:= New(PUIEditCtl, Init(parent, aCtlID, x,y,w,h, fldStrLen, @self, false));
    end;
    SetupFromTemplate(aTemplate, ctl, font);
  end
  else
  begin   {- normal init resource methods }
    case ctlType of
      UIUpcaseType, UIExpandIDType, UIMnemType:
        begin
          if ctlType = UIMnemType then
            len:= listObj^.GetMnemIDLen(assocFName) + 1
          else
            len:= fldStrLen;
          ctl:= New(PUIEditCtl, InitResource(parent, aCtlID, len, @self, true));
        end;
      UIRealType:
        ctl:= New(PUINumCtl, InitResource(parent, aCtlID, @self, 0, 0, 1.0, true));
      UIIntType:
        ctl:= New(PUIIntCtl, InitResource(parent, aCtlID, @self, 0, 0, 1, true));
      UIDateType:
        ctl:= New(PUIDateCtl, InitResource(parent, aCtlID, @self, true));
      UITimeType:
        ctl:= New(PUITimeCtl, InitResource(parent, aCtlID, fldStrLen, @self, false));
      UIBoolType:
        ctl:= New(PUIBoolCtl, InitResource(parent, aCtlID, fldStrLen, @self, false));
      UIBlockType:
        ctl:= New(PUIBlockCtl, InitResource(parent, aCtlID, fldStrLen, @self, false));
    else
      ctl:= New(PUIEditCtl, InitResource(parent, aCtlID, fldStrLen, @self, false));
    end;
  end;
end;

procedure TUIFld.BindToLabel(aLblID, aTemplate: word);
var
  font  : HFont;
  x,y,w,h: word;
begin
  if aTemplate > 0 then
  begin
    CheckTemplate(aTemplate, font, x,y,w,h);

    lbl:= New(PLStatic, Init(parent, aLblID, nil, x,y,w,h, 0, false));

    SetupFromTemplate(aTemplate, lbl, font);
  end
  else
    lbl:= New(PLStatic, InitResource(parent, aLblID, 0, false));
end;

procedure TUIFld.ControlCreated;
{- this method is called by the control the after the SETUPWINDOW of the
   control is complete. if no lbl field is defined no action occurs }
var
  style   : longint;
begin
  ctlCreated:= true;
  if lbl <> nil then
    lbl^.SetText(fldInfo.fldName);
  if hidden then
    ctl^.Show(SW_HIDE);
  if (ctlType = UIFreeTextType) and postCreate then
  begin
(*    style:= GetWindowLong(ctl^.HWindow, GWL_STYLE);*)
(*    style:= style or ES_MULTILINE or WS_VSCROLL or ES_WANTRETURN;*)
(*    SetWindowLong(ctl^.HWindow, GWL_STYLE, style);*)
(*    style:= GetWindowLong(ctl^.HWindow, GWL_STYLE);*)
(*    InvalidateRect(ctl^.HWindow, nil, true);*)
  end;
end;

procedure TUIFld.CtlLostFocus(newFoc: HWnd);
{- called when the associated control loses it's focus. The field objects must
   be sure not to envoke any windows in this routine. If a window is displayed
   in this routine the Kill/Set Focus chain will be disturbed and unpredictable
   results will occur }
var
  newFocID: integer;
begin
  {- is focus processing enabled? }
  if not uiFldsProcessFocus then
    Exit;

  Format;

  {- if losing focus to a button then ignore lost focus }
  newFocID:= GetDlgCtrlID(newFoc);
  if (newFocID < 200) then
    Exit;

(*  if ctlType = UIMnemType then*)
(*  begin*)
(*    if clearFlag <> 0 then  {- user has typed something }*)
(*    begin*)
(*      LookUpData;*)
(*      clearFlag:= 0;*)
(*    end;*)
(*  end;*)

  PostMessage(parent^.HWindow, WM_LOSTFOCUS, newFocID, longint(@self));
end;

procedure TUIFld.CtlGotFocus(oldFoc: HWnd);
{- called when the associated control gains focus }
begin
end;

procedure TUIFld.CharPressed(var vKey: word);
{- called by control when a character is pressed, but before any action is taken }
var
  pstr    : array[0..200] of char;
begin
  if ctlType = UIMnemType then
  begin
    if clearFlag = 0 then   {- first character typed }
    begin
      Clear;
      clearFlag:= 1;
      PostMessage(parent^.HWindow, WM_AUTOCLEARED, GetID, longint(@self));
    end;
  end;
end;

{-----------------------------------------------------------[ TUIStaticFld ]--}

constructor TUIStaticFld.Init(aParent: PWindowsObject; aDBd: PDBReadDesc;
                         aListObj: PListObject; aFldNum, aCtlID, aLblID: word);
begin
  inherited Init(aParent, aDBD, aListObj, false, aFldNum, aCtlID, aLblID);
end;

constructor TUIStaticFld.PostInit(aParent : PWindowsObject; aDBD: PDBReadDesc;
                  aListObj: PListObject; aFldNum, aCtlID, aLblID: word;
                  aCtlTemplate, aLblTemplate: word);
begin
  inherited PostInit(aParent, aDBD, aListObj, false, aFldNum, aCtlID, aLblID, aCtlTemplate,
                     aLblTemplate);
end;

procedure TUIStaticFld.BindToControl(aCtlID, aTemplate: word);
var
  font    : HFont;
  x,y,w,h : word;
begin
  if aTemplate > 0 then
  begin
    CheckTemplate(aTemplate, font, x,y,w,h);

    ctl:= New(PUIStaticCtl, Init(parent, aCtlID, nil, x,y,w,h, 0, true, @self));

    SetupFromTemplate(aTemplate, ctl, font);
  end
  else
    ctl:= New(PUIStaticCtl, InitResource(parent, aCtlID, true, @self));
end;

function TUIStaticFld.XFerCtlToRec(aDBR: PDBRec): boolean;
{- this function cannot be performed on static fields (yet???) }
begin
  XFerCtlToRec:= false;
end;

function TUIStaticFld.Format: integer;
begin
  Format:= 0;
end;

function TUIStaticFld.IsValid: integer;
begin
  IsValid:= 0;
end;

{-----------------------------------------------------------[ TUICheckFld ]--}

constructor TUICheckFld.Init(aParent: PWindowsObject; aDBd: PDBReadDesc;
                             aFldNum, aCtlID: word; useFieldName: boolean);
begin
  useFldName:= useFieldName;
  title:= nil;
  inherited Init(aParent, aDBD, nil, false, aFldNum, aCtlID, 0);
end;

constructor TUICheckFld.PostInit(aParent: PWindowsObject; aDBD: PDBReadDesc;
                              aFldNum, aCtlID, aCtlTemplate: word;
                              aTitle: PChar; useFieldName: boolean);
begin
  useFldName:= useFieldName;
  if (aTitle <> nil) and (strlen(aTitle) > 0) then
    title:= StrNew(aTitle)
  else
    title:= nil;
  inherited PostInit(aParent, aDBD, nil, false, aFldNum, aCtlID, 0, aCtlTemplate, 0);
end;

destructor TUICheckFld.Done;
begin
  MSStrDispose(title);
  inherited Done;
end;

procedure TUICheckFld.CalcControlType;
{- Set the ctlType data member.  CheckField types only work with boolean
   database fields }
begin
  if fldInfo.fldType = dbBoolean then
  begin
    ctlType:= UICheckType;
  end
  else
  begin
    FatalError('Fatal Error', 'Attempt to link a CheckFld to a non-boolean database field');
    Halt;
  end;
end;

function TUICheckFld.XFerCtlToRec(aDBR: PDBRec): boolean;
{- transfer the control to the associated field in the record buffer. returns
   false if unsuccessful }
var
  b     : boolean;
begin
  XFerCtlToRec:= true;
  if aDBR <> nil then  {- make sure a record buffer has been specified }
  begin
    b:= SendMessage(ctl^.HWindow, BM_GETCHECK, 0,0) = BF_CHECKED;
    aDBR^.PutField(fldNum, @b);
    XFerCtlToRec:= aDBR^.dbErrorNum = 0;
  end;
end;

procedure TUICheckFld.XferRecToCtl(aDBR: PDBRec);
{- transfer the associated field in the record buffer to the control }
var
  b   : boolean;
begin
  aDBR^.GetField(fldNum, @b, sizeof(b));
  if b then
    SendMessage(ctl^.HWindow, BM_SETCHECK, BF_CHECKED, 0)
  else
    SendMessage(ctl^.Hwindow, BM_SETCHECK, BF_UNCHECKED, 0);
end;

procedure TUICheckFld.Clear;
begin
  SendMessage(ctl^.HWindow, BM_SETCHECK, BF_UNCHECKED, 0);
end;

function TUICheckFld.Format: integer;
begin
  Format:= 0;
end;

function TUICheckFld.IsValid: integer;
begin
  IsValid:= 0;
end;

function TUICheckFld.GetCtlAsStr(aStr: PChar; maxLen: word): integer;
var
  ret : integer;
  b   : boolean;
begin
  ret:= Format;
  if ret = 0 then
  begin
    b:= SendMessage(ctl^.HWindow, BM_GETCHECK, 0,0) = BF_CHECKED;
    BoolToStr(b, aStr, maxLen);
  end;
  GetCtlAsStr:= ret;
end;

function TUICheckFld.IsEmpty: boolean;
begin
  IsEmpty:= false;
end;

function TUICheckFld.IsModified(aDBR: PDBRec): boolean;
var
  bf  : boolean;
  bc  : boolean;
begin
  if aDBR <> nil then
  begin
    aDBR^.GetField(fldNum, @bf, sizeof(bf));
    bc:= SendMessage(ctl^.HWindow, BM_GETCHECK, 0,0) = BF_CHECKED;
    IsModified:= (bf <> bc);
  end
  else
    IsModified:= false;
end;

procedure TUICheckFld.BindToControl(aCtlID, aTemplate: word);
var
  font    : HFont;
  x,y,w,h : word;
begin
  if aTemplate > 0 then
  begin
    CheckTemplate(aTemplate, font, x,y,w,h);
    ctl:= New(PUICheckCtl, Init(parent, aCtlID, title, x,y,w,h, @self));
    SetupFromTemplate(aTemplate, ctl, font);
  end
  else
    ctl:= New(PUICheckCtl, InitResource(parent, aCtlID, @self));
end;

procedure TUICheckFld.SetNewLabel(newLbl: PChar);
begin
  SendMessage(ctl^.HWindow, WM_SETTEXT, 0, longint(newLbl));
end;

procedure TUICheckFld.ControlCreated;
begin
  inherited ControlCreated;
  if useFldName then
    SetNewLabel(fldInfo.fldName);
  Clear;
end;

procedure TUICheckFld.TrimIt;
begin
  {- do nothing }
end;

{-----------------------------------------------------------[ TUIFullEdit ]--}

procedure TUIFullEditFld.CalcControlType;
begin
  if fldInfo.fldType = dbSeqRef then      {- SEQREF FIELD TYPE }
  begin
    if ((fldInfo.userFlag and DB_MNEMONIC) = 0) then
      ctlType:= UIIntType
    else
      inherited CalcControlType;
  end
  else
    inherited CalcControlType;
end;

{-----------------------------------------------------------[ TFreeTxtDlg ]--}

constructor TFreeTxtDlg.Init(aParent: PWindowsObject; aBuf: PChar; aMaxLen: integer);
begin
  inherited Init(aParent, MakeIntResource(DLG_UIFREETEXT));
  ed:= New(PEdit, InitResource(@self, IDC_FREETEXT, aMaxLen));
  buf:= aBuf;
  len:= aMaxLen;
end;

procedure TFreeTxtDlg.SetupWindow;
begin
  inherited SetupWindow;
  ed^.SetText(buf);
  PostMessage(ed^.HWindow, EM_SETSEL, 0, MakeLong(word(-1), strlen(buf)));
end;

function TFreeTxtDlg.CanClose: boolean;
begin
  ed^.GetText(buf, len);
  CanClose:= true;
end;

procedure TFreeTxtDlg.EnableButtons;
var
  pstr  : array[0..200] of char;
begin
  ed^.GetText(pstr, 200);
  EnableWindow(GetItemHandle(IDC_UICLEAR), strlen(pstr) > 0);
end;

procedure TFreeTxtDlg.WMCommand(var msg: TMessage);
begin
  if (msg.wParam = IDC_FREETEXT) and (msg.lParamHi = EN_CHANGE) then
    EnableButtons
  else
    inherited WMCommand(msg);
end;

procedure TFreeTxtDlg.IDClear(var msg: TMessage);
var
  p1    : array[0..50] of char;
  p2    : array[0..100] of char;
begin
  if YesNoMsg(HWindow, SR(IDS_CONFIRM, p1, 50), SR(IDS_UIDELFREE, p2, 100)) then
  begin
    ed^.SetText('');
    EnableButtons;
    FocusCtl(HWindow, ed^.GetID);
  end;
end;

{===============================  UI CONTROLS ===============================}
{============================================================================}

{-----------------------------------------------------------[ TUIEditCtl]--}

constructor TUIEditCtl.InitResource(aParent: PWindowsObject; anID, aTextLen: word;
                             aUIFld: PUIFld; forceUpper: boolean);
var
  len   : word;
begin
  uiFld:= aUIFld;
  upper:= forceUpper;
  inherited InitResource(aParent, anID, aTextLen);
end;

constructor TUIEditCtl.Init(aParent: PWindowsObject; anID, x,y,w,h, aTextLen: word;
                            aUIFld: PUIFld; forceUpper: boolean);
begin
  uiFld:= aUIFld;
  upper:= forceUpper;
  inherited Init(aParent, anID, nil, x,y,w,h, aTextLen, false);
end;

procedure TUIEditCtl.SetupWindow;
begin
  inherited SetupWindow;

(*  Register3DControl(HWindow);*)

  {- call the UIFld to tell it that the control is now created }
  uiFld^.ControlCreated;
end;

procedure TUIEditCtl.WMKillFocus(var msg: TMessage);
var
  btnID : integer;
begin
  inherited WMKillFocus(msg);
  uiFld^.CtlLostFocus(msg.wParam);
end;

procedure TUIEditCtl.WMSetFocus(var msg: TMessage);
begin
  DefWndProc(msg);
  uiFld^.CtlGotFocus(msg.wParam);
end;

procedure TUIEditCtl.WMChar(var msg: TMessage);
begin
  uiFld^.CharPressed(msg.wParam);
  if upper then
    msg.wParam:= word(AnsiUpper(PChar(msg.wParam and $FF)));
  if msg.wParam <> 0 then
    inherited WMChar(msg)
  else
    msg.result:= 0;
end;

procedure TUIEditCtl.WMKeyDown(var msg: TMessage);
begin
  if msg.wParam = VK_DELETE then
    uiFld^.CharPressed(msg.wParam);
  if IsListKey(msg.wParam) then
  begin
    uiFld^.ListData;
    msg.result:= 0;
  end
  else
  begin
    CheckUserKey(parent, msg);
    inherited WMKeyDown(msg);
  end;
end;

procedure TUIEditCtl.WMRButtonDown(var msg: TMessage);
begin
  DefWndProc(msg);

  if GetFocus <> hWindow then   {- focus the control }
    uiFld^.FocusFld;

  HandleEvents(parent^.HWindow);  {- let things settle down }

  if GetFocus = hWindow then  {- see if control still has focus. If not then }
    uiFld^.ListData;          {- someone else wont reliquish focus so dont list}
end;

procedure TUIEditCtl.WMLButtonDown(var msg: TMessage);
begin
  inherited WMLButtonDown(msg);
  while GetCapture > 0 do
    DoEvent(HWindow);
end;

function TUIEditCtl.IsValid: integer;
begin
  IsValid:= 0;
end;

function TUIEditCtl.Format: integer;
begin
  Format:= 0;
end;

{--------------------------------------------------------[ TUIListEditCtl ]--}

constructor TUIListEditCtl.InitResource(aParent: PWindowsObject; anID: word;
                             aUIFld: PUIFld; aTitle, aListHeader: PChar; aList: PCollection);
begin
  uiFld:= aUIFld;
  inherited InitResource(aParent, anID, aList, aListHeader, aTitle, false, UIListKey);
end;

constructor TUIListEditCtl.Init(aParent: PWindowsObject; anID, x,y,w,h: word;
                            aUIFld: PUIFld; aTitle, aListHeader: PChar; aList: PCollection);
begin
  uiFld:= aUIFld;
  inherited Init(aParent, anID, nil, x,y,w,h, aList, aListHeader, aTitle, false, UIListKey);
end;

procedure TUIListEditCtl.SetupWindow;
begin
  inherited SetupWindow;

(*  Register3DControl(HWindow);*)

  {- call the UIFld to tell it that the control is now created }
  uiFld^.ControlCreated;
end;

procedure TUIListEditCtl.WMKillFocus(var msg: TMessage);
var
  btnID : integer;
begin
  inherited WMKillFocus(msg);
  uiFld^.CtlLostFocus(msg.wParam);
end;

procedure TUIListEditCtl.WMSetFocus(var msg: TMessage);
begin
  DefWndProc(msg);
  uiFld^.CtlGotFocus(msg.wParam);
end;

procedure TUIListEditCtl.WMKeyDown(var msg: TMessage);
begin
  if msg.wParam = VK_DELETE then
    uiFld^.CharPressed(msg.wParam);
  if IsListKey(msg.wParam) then
  begin
    uiFld^.ListData;
    msg.result:= 0;
  end
  else
  begin
    CheckUserKey(parent, msg);
    inherited WMKeyDown(msg);
  end;
end;

procedure TUIListEditCtl.WMRButtonDown(var msg: TMessage);
begin
  DefWndProc(msg);

  if GetFocus <> hWindow then   {- focus the control }
    uiFld^.FocusFld;

  HandleEvents(parent^.HWindow);  {- let things settle down }

  if GetFocus = hWindow then  {- see if control still has focus. If not then }
    uiFld^.ListData;          {- someone else wont reliquish focus so dont list}
end;

procedure TUIListEditCtl.WMLButtonDown(var msg: TMessage);
begin
  inherited WMLButtonDown(msg);
  while GetCapture > 0 do
    DoEvent(HWindow);
end;

procedure TUIListEditCtl.DataSelected;
begin
  {- notify the parent that data was selected into the control }
  PostMessage(parent^.HWindow, WM_MNEMSELECTED, GetID, longint(uiFld));
end;

function TUIListEditCtl.IsValid: integer;
begin
  if inherited IsValid <> 0 then
    IsValid:= UIErrInvalid
  else
    IsValid:= 0;
end;

function TUIListEditCtl.Format: integer;
begin
  if inherited Format <> 0 then
    Format:= UIErrInvalid
  else
    Format:= 0;
end;

{-----------------------------------------------------------[ TUINumCtl ]--}

constructor TUINumCtl.InitResource(aParent: PWindowsObject; anID: integer;
                   aUIFld: PUIFld; aMin, aMax, aStep: double; showSpin: boolean);
var
  len : word;
  sw  : integer;    {- spin width }
begin
  uiFld:= aUIFld;
  len:= uiFld^.dbd^.FieldSizeForStr(uiFld^.fldNum);
  if showSpin then
    sw:= 0
  else
    sw:= -1;
  inherited InitResource(aParent, anID, len, sw, aMin, aMax, aStep);
end;

constructor TUINumCtl.Init(aParent: PWindowsObject; anID, x,y,w,h: word; aUIFld: PUIFld;
                           aMin, aMax, aStep: double; showSpin: boolean);
var
  len : word;
  sw  : integer;    {- spin width }
begin
  uiFld:= aUIFld;
  len:= uiFld^.dbd^.FieldSizeForStr(uiFld^.fldNum);
  if showSpin then
    sw:= 0
  else
    sw:= -1;
  inherited Init(aParent, anID, nil, x,y,w,h, len, false, sw, aMin, aMax, aStep);
end;

procedure TUINumCtl.SetupWindow;
begin
  inherited SetupWindow;
(*  Register3DControl(HWindow);*)
  {- call the UIFld to tell it that the control is now created }
  uiFld^.ControlCreated;
end;

procedure TUINumCtl.WMSetFocus(var msg: TMessage);
begin
  DefWndProc(msg);
  uiFld^.CtlGotFocus(msg.wParam);
end;

procedure TUINumCtl.WMKillFocus(var msg: TMessage);
var
  btnID : integer;
begin
  inherited WMKillFocus(msg);
  uiFld^.CtlLostFocus(msg.wParam);
end;

procedure TUINumCtl.WMChar(var msg: TMessage);
begin
  uiFld^.CharPressed(msg.wParam);
  if msg.wParam <> 0 then
    inherited WMChar(msg)
  else
    msg.result:= 0;
end;

procedure TUINumCtl.WMKeyDown(var msg: TMessage);
begin
  CheckUserKey(parent, msg);
  inherited WMKeyDown(msg);
  if IsListKey(msg.wParam) then
    uiFld^.ListData;
end;

procedure TUINumCtl.WMLButtonDown(var msg: TMessage);
begin
  inherited WMLButtonDown(msg);
  while GetCapture > 0 do
    DoEvent(HWindow);
end;

procedure TUINumCtl.WMRButtonDown(var msg: TMessage);
begin
  DefWndProc(msg);

  if GetFocus <> hWindow then   {- focus the control }
    uiFld^.FocusFld;

  HandleEvents(parent^.HWindow);  {- let things settle down }

  if GetFocus = hWindow then  {- see if control still has focus. If not then }
    uiFld^.ListData;          {- someone else wont reliquish focus so dont list}
end;

function TUINumCtl.IsValid: integer;
begin
  if not NumIsValid then
    IsValid:= UIErrInvalidNumber
  else if not NumInRange then
    IsValid:= UIErrOutOfRange
  else
    IsValid:= 0;
end;

function TUINumCtl.Format: integer;
var
  ret   : integer;
begin
  ret:= IsValid;
  if ret = 0 then
    SetNum(GetDouble);  {- format number }
  Format:= ret;
end;

{-----------------------------------------------------------[ TUIIntCtl ]--}

constructor TUIIntCtl.InitResource(aParent: PWindowsObject; anID: integer;
                                   aUIFld: PUIFld; aMin, aMax, aStep: longint; showSpin: boolean);
var
  len : word;
  sw  : integer;
begin
  uiFld:= aUIFld;
  len:= uiFld^.dbd^.FieldSizeForStr(uiFld^.fldNum);
  if showSpin then
    sw:= 0
  else
    sw:= -1;
  inherited InitResource(aParent, anID, len, sw, aMin, aMax, aStep);
end;

constructor TUIIntCtl.Init(aParent: PWindowsObject; anID, x,y,w,h: word; aUIFld: PUIFld;
                           aMin, aMax, aStep: longint; showSpin: boolean);
var
  len : word;
  sw  : integer;
begin
  uiFld:= aUIFld;
  len:= uiFld^.dbd^.FieldSizeForStr(uiFld^.fldNum);
  if showSpin then
    sw:= 0
  else
    sw:= -1;
  inherited Init(aParent, anID, nil, x,y,w,h, len, false, sw, aMin, aMax, aStep);
end;

procedure TUIIntCtl.SetupWindow;
begin
  inherited SetupWindow;
(*  Register3DControl(HWindow);*)
  {- call the UIFld to tell it that the control is now created }
  uiFld^.ControlCreated;
end;

procedure TUIIntCtl.WMKillFocus(var msg: TMessage);
var
  btnID : integer;
begin
  inherited WMKillFocus(msg);
  uiFld^.CtlLostFocus(msg.wParam);
end;

procedure TUIIntCtl.WMSetFocus(var msg: TMessage);
begin
  DefWndProc(msg);
  uiFld^.CtlGotFocus(msg.wParam);
end;

procedure TUIIntCtl.WMChar(var msg: TMessage);
begin
  uiFld^.CharPressed(msg.wParam);
  if msg.wParam <> 0 then
    inherited WMChar(msg)
  else
    msg.result:= 0;
end;

procedure TUIIntCtl.WMKeyDown(var msg: TMessage);
begin
  CheckUserKey(parent, msg);
  inherited WMKeyDown(msg);
  if IsListKey(msg.wParam) then
    uiFld^.ListData;
end;

procedure TUIIntCtl.WMLButtonDown(var msg: TMessage);
begin
  inherited WMLButtonDown(msg);
  while GetCapture > 0 do
    DoEvent(HWindow);
end;

procedure TUIIntCtl.WMRButtonDown(var msg: TMessage);
begin
  DefWndProc(msg);

  if GetFocus <> hWindow then   {- focus the control }
    uiFld^.FocusFld;

  HandleEvents(parent^.HWindow);  {- let things settle down }

  if GetFocus = hWindow then  {- see if control still has focus. If not then }
    uiFld^.ListData;          {- someone else wont reliquish focus so dont list}
end;

function TUIIntCtl.IsValid: integer;
begin
  if not NumIsValid then
    IsValid:= UIErrInvalidNumber
  else if not NumInRange then
    IsValid:= UIErrOutOfRange
  else
    IsValid:= 0;
end;

function TUIIntCtl.Format: integer;
var
  ret : integer;
begin
  ret:= IsValid;
  if ret = 0 then
    SetNum(GetLong);  {- format number }
  Format:= ret;
end;

{-----------------------------------------------------------[ TUIDateCtl ]--}

constructor TUIDateCtl.InitResource(aParent: PWindowsObject; anID: integer;
                                    aUIFld: PUIFld; showSpin: boolean);
var
  len : word;
  sw  : integer;
begin
  uiFld:= aUIFld;
  len:= uiFld^.dbd^.FieldSizeForStr(uiFld^.fldNum);
  if showSpin then
    sw:= 0
  else
    sw:= -1;
  inherited InitResource(aParent, anID, len, sw);
end;

constructor TUIDateCtl.Init(aParent: PWindowsObject; anID, x,y,w,h: word;
                            aUIFld: PUIFld; showSpin: boolean);
var
  len : word;
  sw  : integer;
begin
  uiFld:= aUIFld;
  len:= uiFld^.dbd^.FieldSizeForStr(uiFld^.fldNum);
  if showSpin then
    sw:= 0
  else
    sw:= -1;
  inherited Init(aParent, anID, nil, x,y,w,h, len, false, sw);
end;

procedure TUIDateCtl.SetupWindow;
begin
  inherited SetupWindow;
(*  Register3DControl(HWindow);*)
  {- call the UIFld to tell it that the control is now created }
  uiFld^.ControlCreated;
end;

procedure TUIDateCtl.WMSetFocus(var msg: TMessage);
begin
  DefWndProc(msg);
  uiFld^.CtlGotFocus(msg.wParam);
end;

procedure TUIDateCtl.WMKillFocus(var msg: TMessage);
begin
  inherited WMKillFocus(msg);
  uiFld^.CtlLostFocus(msg.wParam);
end;

procedure TUIDateCtl.WMChar(var msg: TMessage);
var
  pstr  : array[0..200] of char;
begin
  uiFld^.CharPressed(msg.wParam);
  if msg.wParam <> 0 then
  begin
    if char(msg.WParam) = ' ' then
    begin
      IntlDateStr(IntlCurrentDate, pstr, 200);
      SetText(pstr);
      msg.result:= 0;
    end
    else
      inherited WMChar(msg);
  end
  else
    msg.result:= 0;
end;

procedure TUIDateCtl.WMKeyDown(var msg: TMessage);
begin
  CheckUserKey(parent, msg);
  inherited WMKeyDown(msg);
  if IsListKey(msg.wParam) then
    uiFld^.ListData;
end;

procedure TUIDateCtl.WMLButtonDown(var msg: TMessage);
begin
  inherited WMLButtonDown(msg);
  while GetCapture > 0 do
    DoEvent(HWindow);
end;

procedure TUIDateCtl.WMRButtonDown(var msg: TMessage);
begin
  DefWndProc(msg);

  if GetFocus <> hWindow then   {- focus the control }
    uiFld^.FocusFld;

  HandleEvents(parent^.HWindow);  {- let things settle down }

  if GetFocus = hWindow then  {- see if control still has focus. If not then }
    uiFld^.ListData;          {- someone else wont reliquish focus so dont list}
end;

function TUIDateCtl.ExtractDate: TIntlDate;
{- extracts the date from the control, returns 0 if error or blank }
var
  pstr  : array[0..100] of char;
  err   : boolean;
  dt    : TIntlDate;
begin
  ExtractDate:= 0;
  GetText(pstr, 100);
  dt:= IntlStrToDate(pstr, err);
  if not err then
    if ValidDate(dt) then
      ExtractDate:= dt;
end;

procedure TUIDateCtl.DoSpin(anInc: integer);
var
  pstr  : array[0..100] of char;
  dt    : TIntlDate;
begin
  dt:= ExtractDate;
  if dt = 0 then
    dt:= IntlCurrentDate
  else
    dt:= IncDate(dt, anInc);
  SetText(IntlDateStr(dt, pstr, 100))
end;

procedure TUIDateCtl.SpinDown;
begin
  DoSpin(-1);
end;

procedure TUIDateCtl.SpinUp;
begin
  DoSpin(1);
end;

function TUIDateCtl.IsValid: integer;
var
  pstr  : array[0..100] of char;
  bErr  : boolean;
  err   : integer;
  dt    : TIntlDate;
begin
  err:= 0;
  GetText(pstr, 100);
  dt:= IntlStrToDate(pstr, bErr);
  if bErr or not ValidDate(dt) then
    err:= UIErrInvalid;
  IsValid:= err;
end;

function TUIDateCtl.Format: integer;
var
  dt  : TIntlDate;
  pstr: array[0..100] of char;
  p2  : array[0..100] of char;
  ret : integer;
begin
  ret:= IsValid;
  if ret = 0 then
  begin
    dt:= ExtractDate;
    SetText(IntlDateStr(dt, pstr, 100))
  end;
  Format:= ret;
end;

{-----------------------------------------------------------[ TUIBoolCtl ]--}

procedure TUIBoolCtl.WMChar(var msg: TMessage);
var
  pstr  : array[0..100] of char;
  err   : integer;
  b     : boolean;
begin
  uiFld^.CharPressed(msg.wParam);
  if msg.wParam <> 0 then
  begin
    if char(msg.wParam) = ' ' then  {- toggle boolean value }
    begin
      GetText(pstr, 100);
      b:= not StrToBool(pstr, err);
      if err <> 0 then
        b:= false;
      BoolToStr(b, pstr, 100);
      SetText(pstr);
      msg.result:= 0;
    end
    else
      inherited WMChar(msg);
  end
  else
    msg.result:= 0;
end;

function TUIBoolCtl.IsValid: integer;
var
  pstr  : array[0..100] of char;
  err   : integer;
begin
  IsValid:= 0;
  GetText(pstr, 100);
  if StrLen(pstr) > 0 then
  begin
    StrToBool(pstr, err);
    if err <> 0 then
      IsValid:= UIErrInvalid;
  end;
end;

function TUIBoolCtl.Format: integer;
var
  pstr  : array[0..100] of char;
  err   : integer;
  ret   : integer;
begin
  ret:= IsValid;
  if ret = 0 then
  begin
    GetText(pstr, 100);
    BoolToStr(StrToBool(pstr, err), pstr, 100);
    SetText(pstr);
  end;
  Format:= ret;
end;

{-------------------------------------------------------------[ TUITimeCtl]--}

function TUITimeCtl.IsValid: integer;
var
  pstr  : array[0..100] of char;
  bErr  : boolean;
  err   : integer;
  dt    : TIntlTime;
begin
  err:= 0;
  GetText(pstr, 100);
  dt:= IntlStrToTime(pstr, bErr);
  if bErr or not ValidTime(dt) then
    err:= UIErrInvalid;
  IsValid:= err;
end;

function TUITimeCtl.Format: integer;
var
  pstr  : array[0..100] of char;
  err   : boolean;
  dt    : TIntlTime;
  ret   : integer;
begin
  ret:= IsValid;
  if ret = 0 then
  begin
    GetText(pstr, 100);
    dt:= IntlStrToTime(pstr, err);
    SetText(IntlTimeStr(dt, INIShowSeconds, pstr, 100));
  end;
  Format:= ret;
end;

{-----------------------------------------------------------[ TUIBlockCtl ]--}

procedure TUIBlockCtl.WMChar(var msg: TMessage);
var
  ch  : char;
begin
  uiFld^.CharPressed(msg.wParam);
  if msg.wParam <> 0 then
  begin
    ch:= char(msg.wParam);
    if (ch <> #8) and (ch <> ' ') and ((ch < '0') or (ch > '9')) and
       ((Upcase(char(ch)) < 'A') or (Upcase(char(ch)) > 'F')) then
      msg.result:= 0
    else
      inherited WMChar(msg);
  end
  else
    msg.result:= 0;
end;

{-----------------------------------------------------------[ TUICheckCtl ]--}

constructor TUICheckCtl.InitResource(aParent: PWindowsObject; anID: word; aUIFld: PUIFld);
begin
  uiFld:= aUIFld;
  inherited InitResource(aParent, anID, false);
end;

constructor TUICheckCtl.Init(aParent: PWindowsObject; anID: word; aText: PChar;
                             x,y,w,h: word; aUIFld: PUIFld);
begin
  uiFld:= aUIFld;
  inherited Init(aParent, anID, aText, x,y,w,h, nil, false);
end;

procedure TUICheckCtl.SetupWindow;
begin
  inherited SetupWindow;
(*  Register3DControl(HWindow);*)
  {- call the UIFld to tell it that the control is now created }
  uiFld^.ControlCreated;
end;

procedure TUICheckCtl.WMSetFocus(var msg: TMessage);
begin
  DefWndProc(msg);
  uiFld^.CtlGotFocus(msg.wParam);
end;

procedure TUICheckCtl.WMKillFocus(var msg: TMessage);
begin
  DefWndProc(msg);
  uiFld^.CtlLostFocus(msg.wParam);
end;

procedure TUICheckCtl.WMChar(var msg: TMessage);
begin
  uiFld^.CharPressed(msg.wParam);
  if msg.wParam <> 0 then
    DefWndProc(msg)
  else
    msg.result:= 0;
end;

procedure TUICheckCtl.WMKeyDown(var msg: TMessage);
begin
  CheckUserKey(parent, msg);
  DefWndProc(msg);
  if IsListKey(msg.wParam) then
    uiFld^.ListData;
end;

procedure TUICheckCtl.WMLButtonDown(var msg: TMessage);
begin
  inherited WMLButtonDown(msg);
  {- special handling so focus processing is handled correctly }
  while GetCapture > 0 do
    DoEvent(HWindow);
end;

procedure TUICheckCtl.WMRButtonDown(var msg: TMessage);
begin
  DefWndProc(msg);

  if GetFocus <> hWindow then   {- focus the control }
    uiFld^.FocusFld;

  HandleEvents(parent^.HWindow);  {- let things settle down }

  if GetFocus = hWindow then  {- see if control still has focus. If not then }
    uiFld^.ListData;          {- someone else wont reliquish focus so dont list}
end;

{-----------------------------------------------------------[ TUICheckCtl ]--}

constructor TUIStaticCtl.InitResource(aParent: PWindowsObject; anID: word;
                                      bold: boolean; aUIFld: PUIFld);
begin
  uiFld:= aUIFld;
  inherited InitResource(aParent, anID, 0, bold);
end;

constructor TUIStaticCtl.Init(aParent: PWindowsObject; anID: integer; aTitle: PChar;
                              x,y,w,h: integer; aTextLen: word; bold: boolean;
                              aUIFld: PUIFld);
begin
  uiFld:= aUIFld;
  inherited Init(aParent, anID, nil, x,y,w,h, 0, bold);
end;

procedure TUIStaticCtl.SetupWindow;
begin
  inherited SetupWindow;
  {- call the UIFld to tell it that the control is now created }
  uiFld^.ControlCreated;
end;

{-----------------------------------------------------------[ TUIListFld ]--}

constructor TUIListFld.Init(aParent: PWindowsObject; aDBd: PDBReadDesc; isReqd: boolean;
                            aFldNum, aCtlID, aLblID: word; aTitle, aHeader: PChar;
                            aList: PCollection);
begin
  title:= aTitle;
  header:= aHeader;
  list:= aList;
  if not inherited Init(aParent, aDBD, nil, isReqd, aFldNum, aCtlID, aLblID) then
    fail;
end;


constructor TUIListFld.PostInit(aParent: PWindowsObject; aDBD: PDBReadDesc;
        isReqd: boolean; aFldNum, aCtlID, aLblID, aCtlTemplate, aLblTemplate: word;
        aTitle, aHeader: PChar; aList: PCollection);
begin
  title:= aTitle;
  header:= aHeader;
  list:= aList;
  if not inherited PostInit(aParent, aDBD, nil, isReqd, aFldNum, aCtlID, aLblID, aCtlTemplate, aLblTemplate) then
    fail;
end;

procedure TUIListFld.BindToControl(aCtlID, aTemplate: word);
var
  font  : HFont;
  x,y   : word;
  w,h   : word;
begin
  if aTemplate > 0 then
  begin {- late bound controls }
    CheckTemplate(aTemplate, font, x,y,w,h);
    ctl:= New(PUIListEditCtl, Init(parent, aCtlID, x,y,w,h, @self, title, header, list));
    SetupFromTemplate(aTemplate, ctl, font);
  end
  else
  begin   {- normal init resource methods }
    ctl:= New(PUIListEditCtl, InitResource(parent, aCtlID, @self, title, header, list));
  end;
end;

procedure TUIListFld.CalcControlType;
{- figure out what type of field the UI field is going to represent. Sets the
   ctlType data member to the appropriate type }
begin
  if fldInfo.fldType = dbSeqRef then      {- SEQREF FIELD TYPE }
  begin
    FatalError('Fatal Error', 'Cannot link a SeqRef fld to a UI List fld.');
    Halt;
  end;
  ctlType:= UIListEditType
end;

BEGIN
  uiFldsProcessFocus:= true;
END.

{- rcf}
