Unit WLJamChk;

{$R WLJam.Res}

Interface

Uses WObjects, WinTypes, WinProcs, Strings, WLWagpib,
     WLmScan, WLDMSErr, wlHWConf, WLDMStrg;

Const
    TimerInterval = 5000;      { Check for jam every 5 seconds }
    wt_Classic = 0;
    wt_WAXX = 1;
    wt_Unknown = -1;


    { Walkaway Error Constants - all in lo word }
    WaBCSError			= $00000001;
    WaJam			= $00000002;
    WaAccessTimeExpired		= $00000004;
    WaTempTooLow		= $00000008;
    WaTempTooHigh		= $00000010;
    WaDispensePressureLow	= $00000020;
    WaColorLampFailure		= $00000040;
    WaSystemNotReady		= $00000080;
    WaCantTalk                  = $00008000;

    { Walkaway Status Constants - all in hi word }
    WaBCSInProgress		= $00010000;
    WaSterilizationInProgress	= $00020000;
    WaBCSDone			= $00040000;
    WaDataAvailable		= $00080000;


Type
  PJamDialog = ^TJamDialog;
  TJamDialog = object(TDialog)
  End;

  PWaStatusWindow = ^TWaStatusWindow;
  TWaStatusWindow = Object (TWindow)
    WaStatus	    : LongInt;
    StatusShowing   : LongInt;	{ The status corresponding to the message
				  currently on the screen }
    CurrentMsg      : Array [0..128] Of Char;
    WindowForText   : HWnd;
    BuffAddr	    : PChar;
    Posted	    : Boolean;

    Constructor Init (AParent: PWindowsObject; ATitle: PChar);
    Procedure SetWaStatus (CommStat: Word; Var StiStuff: StiArray);
    Function GetWaStatus: LongInt;
    Procedure ClearCommError;
  End;


  PJamCheckerWindow = ^TJamCheckerWindow;
  TJamCheckerWindow = Object (TWindow)
    TimerSet: Boolean;
    MessageUp: Boolean;
    WaType: Integer;
    WaError: Boolean;
    WaStatusWindow: PWaStatusWindow;

    constructor Init(aParent: PWindowsObject; aTitle: PChar);
    procedure TimerTick(var Msg: TMessage);
      virtual wm_First + wm_Timer;
    Procedure SetupWindow; virtual;
    procedure WMDestroy(var Msg: TMessage); virtual wm_First + wm_Destroy;

    Function GetWaType: Integer;
    Procedure SetWaStatusWindow (Dlg: HWnd; BA: PChar);

    Procedure CheckWANow;
  End;

  Function GetWaStatus: LongInt;
  Function SetUpWaType: Boolean;

Var
    TheJamChecker: PJamCheckerWindow;

Implementation

Const					{ Bit offsets for STI command }
    StiErr		    = 64;
    BarCodeScanDone	    = 65;
    AccessGranted	    = 66;
    AccessNixed		    = 67;
    DataAvailable	    = 68;
    Sterilizing		    = 69;
    DiagResultAvailable	    = 70;
    SystemNotReady	    = 71;

    StiJam		    = 80;
    AccessTimeExpired	    = 81;
    TempTooLow		    = 82;
    TempTooHigh		    = 83;
    DispensePressureLow	    = 84;
    BarcodeScanErrors	    = 85;
    DiagOvertimeError	    = 86;
    ColorLampFailure	    = 87;

    JamBase                 = 6100;     { Base of strings in DMSTRING.RES }
    WABase                  = 6100;     { Base of strings in DMSTRING.RES }

{-----------------------------------------------------------------------}
{									}
{-----------------------------------------------------------------------}
Function GetWaStatus: LongInt;
Begin
    GetWaStatus := 0;
    If TheJamChecker <> NIL Then
    Begin
	TheJamChecker^.CheckWaNow;
	GetWaStatus := TheJamChecker^.WaStatusWindow^.GetWaStatus;
    End;
End;


{-----------------------------------------------------------------------}
{									}
{-----------------------------------------------------------------------}
Constructor TWaStatusWindow.Init (AParent: PWindowsObject; ATitle: PChar);
Begin
    TWindow.Init (AParent, ATitle);
    If (WAID = -1) Or (SlotsPerTower = 0) Then
        WaStatus := WaCantTalk
    Else
        WaStatus := 0;
    StatusShowing := Not WaStatus; { to force it to be displayed }
    CurrentMsg[0] := Chr (0);
    WindowForText := 0;
    BuffAddr := NIL;
End;


{-----------------------------------------------------------------------}
{									}
{-----------------------------------------------------------------------}
Procedure TWaStatusWindow.ClearCommError;
Begin
    If SlotsPerTower = 0 Then
      SetUpWaType;
    If SlotsPerTower <> 0 Then
      WaStatus := WaStatus And (Not WaCantTalk);
End;


{-----------------------------------------------------------------------}
{									}
{-----------------------------------------------------------------------}
Procedure TWaStatusWindow.SetWaStatus (CommStat: Word; Var StiStuff: StiArray);
Var
    TStr, TStr2: String;
    StatusString: String[128];

    Procedure PostStatus;
    Begin
        Posted := False;
	If StatusString = '' Then
      GetPStr (JamBase + 29, StatusString);   { OK }
        While Length (StatusString) < 128 Do
          StatusString := StatusString + ' ';
        IF buffAddr <> NIL THEN
          BEGIN
	    StrPCopy (BuffAddr, StatusString);
            If WindowForText <> 0 Then
            Begin
              PostMessage(WindowForText, wm_FillInWaStatus, 0, LONGINT(BuffAddr));
	      StrPCopy (CurrentMsg, StatusString);
              Posted := True;
            End;
          END;
    End;

    Procedure AddStatus (S: String);
    Begin
	StatusString := StatusString + S;
        If S <> '' Then
    	  StatusString := StatusString + #13#10;
    End;

    {-----------------------------------------------------------------------}
    Procedure CheckError (StiBit: Integer; WaStatusBit: LongInt; BadMsg, GoodMsg: String);
    Var
	NewState, OldState: Boolean;
    Begin
	NewState := TestBit (Addr (StiStuff), StiErr) And TestBit (Addr (StiStuff), StiBit);
	OldState := (WaStatus And WaStatusBit) <> 0;
	If NewState And (Not OldState) Then { Error just happened! }
	Begin
	    If (StatusShowing And WaStatusBit) = 0 Then
	    Begin
		AddStatus (BadMsg);
	    End;
	    WaStatus := WaStatus Or WaStatusBit;
	End
	Else If OldState And (Not NewState) Then { Error was just cleared }
	Begin
	    If (StatusShowing And WaStatusBit) <> 0 Then
	    Begin
		AddStatus (GoodMsg);
	    End;
	    WaStatus := WaStatus And (Not WaStatusBit);
	End;
    End;

    {-----------------------------------------------------------------------}
    Procedure CheckStatus (StiBit, StiNotBit: Integer; WaStatusBit: LongInt; Mess: String);
    Var
	NewState, OldState: Boolean;
    Begin
        If (WaStatus And WaCantTalk) <> 0 Then
        Begin
            If StatusShowing <> WaCantTalk Then
            Begin
                GetPStr (JamBase + 30, TStr);    { Unable to communicate }
                AddStatus (TStr);
            End;
            Exit;
        End;
	NewState := TestBit (Addr (StiStuff), StiBit);
	If StiNotBit <> 0 Then
	    NewState := NewState And (Not (TestBit (Addr (StiStuff), StiNotBit)));
	OldState := (WaStatus And WaStatusBit) <> 0;
	If NewState And (Not OldState) Then { Status just came on }
	Begin
	    If (StatusShowing And WaStatusBit) = 0 Then
	    Begin
		AddStatus (Mess);
	    End;
	    WaStatus := WaStatus Or WaStatusBit;
	End
	Else If OldState And (Not NewState) Then { Status just turned off }
	Begin
	    If (StatusShowing And WaStatusBit) <> 0 Then
	    Begin
		AddStatus ('');
	    End;
	    WaStatus := WaStatus And (Not WaStatusBit);
	End;
    End;


Begin
    StatusString := '';

    If CommStat <> 0 Then
        WaStatus := WaStatus Or WaCantTalk
    Else If WAID <> -1 Then
        WaStatus := WaStatus And (Not WaCantTalk);

    If (WaStatus And WaCantTalk) <> 0 Then
    Begin
        GetPStr (JamBase + 30, TStr);  { Unable to communicate }
        CheckStatus (0, 0, 0, TStr);
    End
    Else
    Begin
      GetPStr (JamBase + 31, TStr);     { System not ready }
      CheckError (SystemNotReady, WaSystemNotReady, TStr, '');
      If (WaStatus And WaSystemNotReady) = 0 Then
      Begin
        GetPStr (JamBase + 32, TStr);     { WalkAway Jam }
        GetPStr (JamBase + 33, TStr2);    { WalkAway jam cleared }
      	CheckError (StiJam, WaJam, TStr, TStr2);
        If (WaStatus And WaJam) = 0 Then
        Begin
          GetPStr (JamBase + 34, TStr);     { Colorimetric lamp failure }
      	  CheckError (ColorLampFailure, WaColorLampFailure, TStr, '');
          GetPStr (JamBase + 35, TStr);     { Dispense pressure low }
      	  CheckError (DispensePressureLow, WaDispensePressureLow, TStr, '');
          GetPStr (JamBase + 36, TStr);     { Temperature too high }
      	  CheckError (TempTooHigh, WaTempTooHigh, TStr, '');
          GetPStr (JamBase + 37, TStr);     { Temperature too low }
      	  CheckError (TempTooLow, WaTempTooLow, TStr, '');
          GetPStr (JamBase + 38, TStr);     { Access time expired }
      	  CheckError (AccessTimeExpired, WaAccessTimeExpired, TStr, '');
          GetPStr (JamBase + 39, TStr);      { Bar code scan error }
          GetPStr (JamBase + 40, TStr2);     { Bar code scan error cleared }
      	  CheckError (BarCodeScanErrors, WaBCSError, TStr, TStr2);

          GetPStr (JamBase + 41, TStr);      { Bar code scan in progress }
      	  CheckStatus (AccessNixed, BarCodeScanDone, WaBCSInProgress, TStr);
          GetPStr (JamBase + 42, TStr);      { Sterilization in progress }
      	  CheckStatus (Sterilizing, 0, WaSterilizationInProgress, TStr);
          GetPStr (JamBase + 43, TStr);      { Data available }
      	  CheckStatus (DataAvailable, 0, WaDataAvailable, TStr);
        End;
      End;
    End;

    PostStatus;
    If Posted Then
      StatusShowing := WaStatus;
End;


{-----------------------------------------------------------------------}
{									}
{-----------------------------------------------------------------------}
Function TWaStatusWindow.GetWaStatus: LongInt;
Begin
    GetWaStatus := WaStatus;
End;


{-----------------------------------------------------------------------}
{									}
{-----------------------------------------------------------------------}
Constructor TJamCheckerWindow.Init (aParent: PWindowsObject; aTitle: PChar);
Begin
    TWindow.Init (aParent, aTitle);
    SetUpWAType;
    TimerSet := False;
    MessageUp := False;
    WaError := False;
    WaType := wt_Unknown;
    WaStatusWindow := New (PWaStatusWindow, Init (@Self, ''));
    TheJamChecker := @Self;
End;


{-----------------------------------------------------------------------}
{									}
{-----------------------------------------------------------------------}
Procedure TJamCheckerWindow.SetWaStatusWindow (Dlg: HWnd; BA: PChar);
var
  DummySti: StiArray;

Begin
  WaStatusWindow^.WindowForText := Dlg;
  WaStatusWindow^.BuffAddr := BA;

  If Dlg <> 0 Then
  Begin
    	{ If the can't-talk bit is already set, then just put up
          a message saying there's a communications error.
          Otherwise check the wa normally. }
    If (WaStatusWindow^.GetWaStatus And WaCantTalk) <> 0 Then
      WaStatusWindow^.SetWaStatus ($8000, DummySti)
    Else
      CheckWaNow;
  End;
End;


{-----------------------------------------------------------------------}
{									}
{-----------------------------------------------------------------------}
Function TJamCheckerWindow.GetWAType: Integer;
Var
    GetWaTypeCmd: PWAGETWATYPECmd;

Begin
    If WaType <> wt_Unknown Then
    Begin
	GetWAType := WaType;
    End
    Else
    Begin
	GetWaTypeCmd := New (PWAGETWATYPECmd, Init (WAID));
	GetWaTypeCmd^.MakeSendMessage;
	GetWaTypeCmd^.SendWAMessage;
	GetWaTypeCmd^.GetParams (WaType);
	Dispose (GetWaTypeCmd, Done);
	GetWaType := WaType;
    End;
End;


{-----------------------------------------------------------------------}
{									}
{-----------------------------------------------------------------------}
procedure TJamCheckerWindow.WMDestroy(var Msg: TMessage);
begin
  If TimerSet Then
     KillTimer(HWindow, id_jamTimer);
  TWindow.WMDestroy(Msg);
end;


{-----------------------------------------------------------------------}
{									}
{-----------------------------------------------------------------------}
procedure TJamCheckerWindow.SetupWindow;
var
  Result: Integer;
  PStr, PStr2: Array [0..50] Of Char;

begin
    TWindow.SetupWindow;
    Result := IDRetry;
    GetStr (WABase + 14, PStr, 50);    { Could not create timer }
    GetStr (WABase + 15, PStr2, 50);   { Jam Checker }
    while (SetTimer(hWIndow, id_jamTimer, TimerInterval, nil) = 0) and (Result = IDRetry) do
	Result := MessageBox(GetFocus, PStr, PStr2, mb_RetryCancel);
    if Result = IDCancel then PostQuitMessage(0)
    Else
    Begin
	TimerSet := True;
{       ShowWindow (WaStatusWindow^.HWindow, sw_Show);  }
        CheckWaNow;
    End;
End;


{-----------------------------------------------------------------------}
{									}
{-----------------------------------------------------------------------}
procedure TJamCheckerWindow.TimerTick (Var Msg: TMessage);
Var
   Result: Integer;
   WACmd: PWASTICmd;
   CarCmd: PWaCarCmd;		{ Lock door again }
   PiezoCmd: PWAROMCMDCmd;	{ To turn off WA alarm }
   HomeCmd: PWAHomCmd;
   OriginalFocus: HWnd;
   DummySti: StiArray;

    {-----------------------------------------------------------------------}
    Procedure DoRegularDialogs;
    Begin
	Result := Application^.ExecDialog (New (PJamDialog, Init
					      (@Self, 'CLASSICJAM1')));
    End;

    {-----------------------------------------------------------------------}
    Procedure DoDispenserDialogs;
    Begin
	Result := Application^.ExecDialog (New (PJamDialog, Init
					      (@Self, 'DispenserJam')));
    End;

begin
    If MessageUp Then Exit;
    If (WaStatusWindow^.GetWaStatus And WaCantTalk) <> 0 Then
    	Exit;  { bc 1/21/92 }
    If WAID = -1 Then
        WAID := WLWAGPIB.InitWA(1);     { returns -1 if no WA attached }
    If WAID = -1 Then                 { See if we're attached now }
    Begin
        WaStatusWindow^.SetWaStatus ($8000, DummySti);
        Exit;
    End;

    If SlotsPerTower = 0 Then
    Begin
      WaStatusWindow^.SetWaStatus ($8000, DummySti);	{ Wa can't talk }
      Exit;
    End;
    WACmd := New (PWASTICmd, Init (WAID));
    WACmd^.SetErrorMode (1);      { Don't put up a DMS error box }
    WACmd^.MakeSendMessage;
    WACmd^.SendWAMessage;
    If (WACmd^.WAGpibErrNum <> 0) Or (WaCmd^.WAErrNum = -1) Then
    Begin
        WaStatusWindow^.SetWaStatus ($8000, DummySti);
        WaError := True;
    End
    Else
    Begin
        WaStatusWindow^.SetWaStatus (WACmd^.RecvMsg^.Status, WACmd^.RecvMsg^.StiBits);
        WaError := False;
    End;
    Dispose (WACmd, Done);

    If (Not WaError) And ((WaStatusWindow^.GetWaStatus And WaJam) <> 0) Then
    Begin
        MessageUp := True;
	OriginalFocus := GetFocus;

	PiezoCmd := New (PWAROMCMDCmd, Init (WAID));
	PiezoCmd^.MakeSendMessage;
	PiezoCmd^.SetParams ($31, 0);	{ Turns off alarm }
	PiezoCmd^.SendWAMessage;
	Dispose (PiezoCmd, Done);

	If WAType <> ClassicWa Then
	Begin
	    Result := Application^.ExecDialog (New (PJamDialog, Init
					      (@Self, 'DISPENSERAREA')));
	    If Result = id_OK Then
		DoDispenserDialogs
	    Else
		DoRegularDialogs;
	End
	Else
	    DoRegularDialogs;

	HomeCmd := New (PWAHomCmd, Init (WAID));
	HomeCmd^.MakeSendMessage;
	HomeCmd^.SendWAMessage;
	Dispose (HomeCmd, Done);

	Result := Application^.ExecDialog (New (PJamDialog, Init
					      (@Self, 'ClassicJamCleared')));
	If WaType <> ClassicWa Then
	    Result := Application^.ExecDialog (New (PJamDialog, Init
						  (@Self, 'WaXXGoodbye')))
	Else
	    Result := Application^.ExecDialog (New (PJamDialog, Init
						  (@Self, 'WaClassicGoodbye')));
        CarCmd := New (PWaCarCmd, Init (WaId));
        CarCmd^.MakeSendMessage;
        CarCMd^.SendWaMessage;          { Lock door }
        Dispose (CarCmd, Done);

	SetFocus (OriginalFocus);
	MessageUp := False;
    End;
end;


{-----------------------------------------------------------------------}
{									}
{-----------------------------------------------------------------------}
Procedure TJamCheckerWindow.CheckWANow;
Var
    Msg: TMessage;

Begin
    TimerTick (Msg);
End;

{-----------------------------------------------------------------------}
{									}
{-----------------------------------------------------------------------}
Function SetUpWAType: Boolean;
VAR waCmd : PWAGetWATypeCmd;
BEGIN
  SetUpWAType := False;
  waCmd := NEW(PWAGetWATypeCmd,Init(WAID));
  SlotTotal := 0;
  If waCmd^.SendWAMessage Then
  Begin
    SetUpWAType := True;
    slotsPerTower := 12;
    SlotTotal := 96;
    If wacmd^.RecvMsg^.EchoMsg[3] = 0 Then { Classic }
      WAType := ClassicWA
    Else IF waCmd^.RecvMsg^.EchoMsg[4] = 5 THEN
           WAType := WA40
         ELSE WAType := WA96;
  End;
  Dispose(waCmd,Done);
END;


Begin
    SlotsPerTower := 0;	{ It will stay 0 until we get a value from the WA }
    TheJamChecker := Nil;
End.



