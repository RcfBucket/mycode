{----------------------------------------------------------------------------}
{  Module Name  : CUSTINTR                                                   }
{  Programmer   : rcf                                                        }
{  Date Created : 11/94                                                      }
{                                                                            }
{  Purpose -                                                                 }
{  This module provides customization for the therapy.dat file.              }
{                                                                            }
{  Assumptions -                                                             }
{  Someone needs to modify the therapy.dat file :)                           }
{                                                                            }
{  Revision History -                                                        }
{                                                                            }
{  Version  Date      Who    Did What                                        }
{  -------  --------  ------ ----------------------------------------------- }
{  1.00     11/94     rcf    Created this program from the version 20        }
{                            stuff that we had.                              }
{----------------------------------------------------------------------------}

program CustIntr;

uses
  ApiTools,
  Ctl3d,
  CtlLib,
  CustUtil,
  DBFile,
  DBIds,
  DBLib,
  DBTypes,
  Win31,
  UserMsgs,
  DMSErr,
  DMString,
  DlgLib,
  GridLib,
  INILib,
  IntrpMap,
  MScan,
  MTask,
  ODialogs,
  OWindows,
  Objects,
  PrnPrev,
  Status,
  Strings,
  StrsW,
  Trays,
  WinProcs,
  WinTypes;

{$R CUSTINTR.RES}
{$I CUSTINTR.INC}

const
  TherCol       = 0;
  MICCol        = 1;
  UrineCol      = 2;
  SystemicCol   = 3;

  ERR_CANTREAD  = MOD_CUSTINTR + 1;
  ERR_CANTWRITE = MOD_CUSTINTR + 2;

type
  TMyApp = object(T3DApplication)
    procedure InitMainWindow; virtual;
  end;

  PInterpDlg = ^TInterpDlg;
  TInterpDlg = object(TCenterDlgWindow)
    grid   : PGrid;
    tray   : PTraysObject;
    drugs  : PDBFile;
    constructor Init(AParent: PWindowsObject);
    destructor Done; virtual;
    procedure SetupWindow; virtual;
    procedure WMDrawItem(var msg: TMessage);    virtual WM_FIRST + WM_DRAWITEM;
    procedure WMMeasureItem(var msg: TMessage); virtual WM_FIRST + WM_MEASUREITEM;
    procedure WMCharToItem(var msg: TMessage);  virtual WM_FIRST + WM_CHARTOITEM;
    procedure IDEdit(var msg: TMessage);        virtual ID_FIRST + IDC_EDIT;
    procedure IDSummary(var msg: TMessage);     virtual ID_FIRST + IDC_SUMMARY;
    procedure IDClose(var msg: TMessage);       virtual ID_FIRST + IDC_CLOSE;
    procedure IDList(var msg: TMessage);        virtual ID_FIRST + IDC_LIST;
    procedure FillList;
    function  GetClassName: PChar; virtual;
    procedure GetWindowClass(var aWndClass: TWndClass); virtual;
    procedure WMDMSQuery(var msg: TMessage); virtual WM_FIRST + WM_MAINPROGQUERY;
  end;

  PEditTherDlg = ^TEditTherDlg;
  TEditTherDlg = object(TCenterDlg)
    adn    : integer;
    tray   : PTraysObject;
    pnls   : PGrid;
    thers  : PGrid;
    cIntr  : PCustIntr;
    curGrp : integer;
    cursor : PWaitCursor;
    drugs  : PDBFile;
    constructor Init(AParent: PWindowsObject; anAdn: integer;
                     aDrugFile: PDBFile; ATray: PTraysObject);
    destructor Done; virtual;
    procedure SetupWindow; virtual;
    procedure WMDrawItem(var msg: TMessage); virtual WM_FIRST + WM_DRAWITEM;
    procedure WMMeasureItem(var msg: TMessage); virtual WM_FIRST + WM_MEASUREITEM;
    procedure WMCharToItem(var msg: TMessage); virtual WM_FIRST + WM_CHARTOITEM;
    procedure IDEdit(var msg: TMessage); virtual ID_FIRST + IDC_EDIT;
    procedure IDNext(var msg: TMessage); virtual ID_FIRST + IDC_NEXT;
    procedure IDPrev(var msg: TMessage); virtual ID_FIRST + IDC_PREV;
    procedure IDSave(var msg: TMessage); virtual ID_FIRST + IDC_SAVE;
    procedure SetPanelGroup(newGrp: integer);
    procedure EditTher(row: integer);
    function DoSave: boolean;
    procedure IDTherList(var msg: TMessage); virtual ID_FIRST + IDC_THERS;
    procedure Cancel(var msg: TMessage); virtual ID_FIRST + ID_CANCEL;
    procedure EnableButtons; virtual;
  end;

  PEditInterpDlg = ^TEditInterpDlg;
  TEditInterpDlg = object(TDMSDlg)
    ther   : ^byte;
    ugrid  : PGrid;
    sgrid  : PGrid;
    iVals  : PCollection;
    cIntr  : PCustIntr;
    constructor Init(AParent: PWindowsObject; var aTher: byte; aCustIntr: PCustIntr);
    destructor Done; virtual;
    procedure SetupWindow; virtual;
    procedure wmDrawItem(var msg: TMessage); virtual WM_FIRST + WM_DRAWITEM;
    procedure wmMeasureItem(var msg: TMessage); virtual WM_FIRST + WM_MEASUREITEM;
    procedure wmCharToItem(var msg: TMessage); virtual WM_FIRST + WM_CHARTOITEM;
    function CanClose: boolean; virtual;
    procedure WMCommand(var msg: TMessage); virtual WM_FIRST + WM_COMMAND;
  end;

procedure TMyApp.InitMainWindow;
begin
  if INIUse3D then
    Register3DApp(true, true, true);
  mainWindow:= New(PInterpDlg, Init(nil));
end;

procedure GetDrugName(adn: integer; trays: PTraysObject; drgFile: PDBFile;
                      name, abbr: PChar; maxLen: integer);
var
  tstr  : string[80];
  pstr  : array[0..80] of char;
  p2    : array[0..80] of char;
  p3    : array[0..80] of char;
begin
  trays^.AdnToAbbrConvert(adn, tstr);
  StrPCopy(p2, tstr);
  TrimAll(pstr, p2, ' ', 80);

  StrLCopy(abbr, pstr, maxLen);

  drgFile^.dbr^.PutField(DBDrugAbbr, @pstr);
  if drgFile^.dbc^.GetEQ(drgFile^.dbr) then
    drgFile^.dbr^.GetFieldAsStr(DBDrugName, pstr, 80)
  else
    StrCopy(pstr, '***');
  StrLCopy(name, pstr, maxLen);
end;

{---------------------------------------------[ TInterpDlg ]--}

constructor TInterpDlg.Init(AParent: PWindowsObject);
begin
  inherited Init(AParent, MakeIntResource(DLG_MAIN));
  grid:= New(PGrid, InitResource(@self, IDC_LIST, 2, false));
  tray:= New(PTraysObject, Init);
  drugs:= New(PDBFile, Init(DBDrugFile, '', dbOpenNormal));
  if drugs = nil then
  begin
    ShowError(nil, IDS_CANTOPENDRUGS, nil, dbLastOpenError, MOD_CUSTINTR, 0);
    Done;
    fail;
  end;
end;

destructor TInterpDlg.Done;
begin
  inherited Done;
  MSDisposeObj(tray);
  MSDisposeObj(drugs);
end;

procedure TInterpDlg.SetupWindow;
var
  pstr  : array[0..100] of char;
  fi    : TFldInfo;
begin
  inherited SetupWindow;

  drugs^.dbd^.FieldInfo(DBDrugAbbr, fi);
  grid^.SetHeader(0, fi.fldname);
  drugs^.dbd^.FieldInfo(DBDrugName, fi);
  grid^.SetHeader(1, fi.fldname);
  grid^.SetAvgCharWidth(0, 3);
  grid^.StretchColumn(1);
  FillList;
end;

procedure TInterpDlg.WMDrawItem(var msg: TMessage);
begin
  grid^.DrawItem(msg);
end;

procedure TInterpDlg.WMMeasureItem(var msg: TMessage);
begin
  grid^.MeasureItem(msg);
end;

procedure TInterpDlg.WMCharToItem(var msg: TMessage);
begin
  grid^.CharToItem(msg);
end;

procedure TInterpDlg.FillList;
{- fill grid list with all drugs }
var
  name  : array[0..100] of char;
  abbr  : array[0..100] of char;
  adn   : integer;
  row   : integer;
  ud    : boolean;
begin
  drugs^.dbc^.SetCurKeyNum(DBDRUG_Sort_KEY);
  if drugs^.dbc^.GetFirst(drugs^.dbr) then
  begin
    repeat
      drugs^.dbr^.GetField(DBDRUGUDFlag, @ud, sizeof(ud));
      if not UD then {- ignore all user defined drugs }
      begin
        drugs^.dbr^.GetFieldAsStr(DBDRUGAbbr, abbr, sizeof(abbr)-1);
        adn:= tray^.AbbrToAdnConvert(abbr);
        if adn = 0 then
        begin
          StrCopy(name, 'Cannot find ADN for drug : ');
          StrCat(name, abbr);
          FatalError('Fatal Error', name);
          grid^.ClearList;
          Exit;
        end;
        drugs^.dbr^.GetFieldAsStr(DBDRUGName, name, sizeof(name)-1);
        row:= grid^.AddString('');
        grid^.SetData(0, row, abbr);
        grid^.SetData(1, row, name);
        grid^.SetItemData(row, adn);
      end;
    until not drugs^.dbc^.GetNext(drugs^.dbr);
  end;
  drugs^.dbc^.SetCurKeyNum(DBDRUG_ID_KEY);
  grid^.SetSelIndex(0);
(*  for k:= 1 to tray^.lastAbr do*)
(*  begin*)
(*    GetDrugName(k, tray, drugs, name, abbr, 200);*)
(*    j:= grid^.AddString(abbr);*)
(*    grid^.SetData(0, j, abbr);*)
(*    grid^.SetData(1, j, name);*)
(*    grid^.SetItemData(j, k);*)
(*  end;*)
(*  grid^.SetSelIndex(0);*)
end;

procedure TInterpDlg.IDClose(var msg: TMessage);
begin
  Cancel(msg);
end;

procedure TInterpDlg.IDSummary(var msg: TMessage);
const
  pnlsAcross  = 5;  {- panels displayed across page }
var
  adn      : integer;
  cIntr    : PCustIntr;
  grp      : integer;
  rec      : integer;
  pstr     : array[0..100] of char;
  p2       : array[0..100] of char;
  sw       : PPctStatDlg;
  anyFound : boolean;
  k        : integer;
  pd       : PPrnPrevDlg;
  pInfo    : PPrintInfo;
  lf       : TLogFont;
  count    : integer;
  isOK     : boolean;
begin
  MakeScreenFont(lf, false, false);
  pInfo:= New(PPrintInfo, Init(lf, INIDefPointSize));

  sw:= New(PPctStatDlg, Init(application^.mainWindow, MakeIntResource(DLG_PERCENT), true, IDC_PERCENT));
  application^.MakeWindow(sw);
  sw^.CompPctLevel(0, 0);

  cIntr:= New(PCustIntr, Init);
  with pInfo^ do
  begin
    header^.AddRow(r_BorderTop, 0);
    header^.AddCell(SR(IDS_CUSTINTERPS, p2, 100), 0, c_Stretch or c_Bold, -2);
    header^.AddRow(r_BorderBottom, 0);
    header^.AddCell(SR(IDS_SUMOFCHANGE, p2, 100), LT400, c_Normal, 0);
    header^.AddCell(SR(IDS_PRINTEDON, p2, 100), LT200, c_PageRight or c_Right or c_Date, 0);
    header^.AddRow(r_Normal, 0);

    footer^.AddRow(r_CenterRow or r_BorderTop, 0);
    footer^.AddCell(SR(IDS_PAGEPOFN, p2, 100), LT300, c_Center or c_PageNum or c_NumPages, 0);

    anyFound:= false;
    with body^ do
    begin
      for adn:= 1 to tray^.lastAbr do
      begin
        sw^.CompPctLevel(adn, tray^.lastAbr);
        cIntr^.BuildPanelGroups(adn);
        for grp:= 1 to cIntr^.NumPanelGroups do
        begin
          cIntr^.LoadTherInfo(grp); { Load therapy info on demand, v4.00, ghg 8/3/98 }
          for rec:= 1 to cIntr^.grpInfo.therInfo.numRecs do
          begin
            if cIntr^.IsFileModified(grp, rec) then
            begin
              with cIntr^.grpInfo.therInfo do
              begin
                AddRow(r_StartGroup, 0);
                anyFound:= true;

                AddCell(SR(IDS_DRUG, p2, 100), LT75, c_Normal, 0);
                GetDrugName(adn, tray, drugs, pstr, p2, 100);
                AddCell(pstr, LT200, c_Bold, 0);
                LoadString(HInstance, IDS_TherDesc + therRecs[rec].txtIdx - 1, pstr, 200);
                AddCell(pstr, LT400, c_PageRight or c_Right, 0);

                AddRow(r_Normal, 0);
                with therRecs[rec] do
                begin
                  AddRow(r_Normal, 0);
                  AddCell(SR(IDS_MIC, p2, 100), LT75, c_BorderRight or c_Normal, 0);
                  for k:= therRecs[rec].numThers downto 1 do
                  begin
                    StrPCopy(pstr, cIntr^.mdaTbl[mdis[k]]);
                    AddCell(pstr, LT50, c_BorderAll or c_Center, 0);
                  end;

                  AddRow(r_Normal, 0);
                  AddCell(SR(IDS_SYSTEMIC, p2, 100), LT75, c_BorderRight or c_Normal, 0);
                  for k:= therRecs[rec].numThers downto 1 do
                  begin
                    cIntr^.InterpValStr(true, intrps[k], pstr, 200);
                    AddCell(pstr, LT50, c_BorderRight or c_Center, 0);
                  end;

                  AddRow(r_Normal, 0);
                  AddCell(SR(IDS_URINE, p2, 100), LT75, c_BorderRight or c_Normal, 0);
                  for k:= therRecs[rec].numThers downto 1 do
                  begin
                    cIntr^.InterpValStr(false, intrps[k], pstr, 200);
                    AddCell(pstr, LT50, c_BorderRight or c_BorderBottom or c_Center, 0);
                  end;
                end;
              end;

              AddRow(0, 0);
              AddRow(0, 0);
              AddCell(SR(IDS_PNLSAFFECTED, p2, 100), LT200, c_Underline, 0);
              with cIntr^.grpInfo.group[grp] do
              begin
                AddRow(r_CellWrap, 0);
                for k:= 1 to numInGroup do
                begin
                  cIntr^.PanelName(grp, k, pstr, 200);
                  AddCell(pstr, LT125, c_Normal, 0);
                end;
                AddRow(r_EndGroup or r_BorderTop, 0);
              end;
            end;
          end;
        end;
        if not sw^.CanContinue then
        begin
          if YesNoMsg(HWindow, SR(IDS_CONFIRM, pstr, sizeof(pstr)-1),
                               SR(IDS_CANCELSUM, p2, sizeof(p2)-1)) then
            break
          else
          begin
            SetFocus(sw^.HWindow);
            sw^.SetContinue(true); {- reset continue flag }
          end;
        end;
      end;
      if not anyFound then
      begin
        AddRow(r_Normal, 0);
        AddRow(r_Normal, 0);
        AddCell(SR(IDS_NOMODS, p2, 100), 0, c_Stretch or c_Bold, 0);
      end;
    end;
  end;
  isOK:= sw^.CanContinue;
  MSDisposeObj(sw);
  if isOK then
  begin
    pd:= New(PPrnPrevDlg, Init(@self, pInfo, SR(IDS_INTERPSUM, p2, 100)));
    application^.ExecDialog(pd);
  end;
  MSDisposeObj(cIntr);
  MSDisposeObj(pInfo);
end;

procedure TInterpDlg.IDEdit(var msg: TMessage);
var
  k   : integer;
  p   : PDialog;
  adn : integer;
begin
  k:= grid^.GetSelIndex;
  if k = -1 then
    MessageBeep(0)
  else
  begin
    {- pass ADN to Edit Dlg }
    adn:= grid^.GetItemData(k);
    p:= New(PEditTherDlg, Init(@self, adn, drugs, tray));
    application^.ExecDialog(p);
  end;
  FocusCtl(hWindow, IDC_LIST);
end;

procedure TInterpDlg.idList(var msg: TMessage);
begin
  if msg.lParamHi = LBN_DBLCLK then
  begin
    idEdit(msg);
    msg.result:= 0;
  end
  else
    DefWndProc(msg);
end;

function TInterpDlg.GetClassName: PChar;
begin
  GetClassName:= application^.Name;
end;

procedure TInterpDlg.GetWindowClass(var aWndClass: TWndClass);
begin
  inherited GetWindowClass(aWndClass);
  aWndClass.HIcon := LoadIcon(HInstance, MakeIntResource(ICON_1));
end;

procedure TInterpDlg.WMDMSQuery(var msg: TMessage);
{- respond to the task module and tell it who I am }
begin
  StrLCopy(PChar(msg.lParam), application^.Name, msg.wParam);
  msg.result:= DMSMainProgRetCode;
  SetWindowLong(HWindow, DWL_MSGRESULT, msg.result);  {- set message result (for dialogs) }
end;

{---------------------------------------------[ TEditTherDlg ]--}

constructor TEditTherDlg.Init(AParent: PWindowsObject; anAdn: integer;
                              aDrugFile: PDBFile; ATray: PTraysObject);
{- assumes aDrugFile has been opened and is valid }
var
  c   : PControl;
begin
  inherited Init(AParent, MakeIntResource(DLG_EDITTHERS));
  cursor:= New(PWaitCursor, Init);
  c:= New(PLStatic, InitResource(@self, IDC_DRGLBL, 0, false));
  pnls:= New(PGrid, InitResource(@self, IDC_PNLS, 1, false));
  thers:= New(PGrid, InitResource(@self, IDC_THERS, 4, false));
  adn:= anADN;
  tray:= ATray;
  cIntr:= New(PCustIntr, Init);
  cIntr^.BuildPanelGroups(adn);
  curGrp:= 1;
  drugs:= aDrugFile;
end;

procedure TEditTherDlg.SetupWindow;
var
  p2, pstr   : array[0..100] of char;
begin
  inherited SetupWindow;

  {- display selected drug name }
  GetDrugName(adn, tray, drugs, pstr, p2, 100);
  SendDlgItemMsg(IDC_DRUG, WM_SETTEXT, 0, longint(@pstr));

  pnls^.SetHeader(0, SR(IDS_PNLGRPS, pstr, 100));
  pnls^.StretchColumn(0);
  pnls^.SetColumnWidth(0, pnls^.GetColumnWidth(0)+5);
  pnls^.StretchColumn(0);

  with thers^ do
  begin
    SetHeader(therCol, SR(IDS_THERTBL, pstr, 100));
    SetHeader(MICCol, SR(IDS_MIC, pstr, 100));
    SetHeader(UrineCol, SR(IDS_URINE, pstr, 100));
    SetHeader(SystemicCol, SR(IDS_SYSTEMIC, pstr, 100));

    SetColumnWidth(MICCol, 65);
    SetColumnWidth(UrineCol, 75);
    SetColumnWidth(SystemicCol, 75);
    StretchColumn(therCol);

    SetAlignment(MICCol, grid_Center);
  end;
  SetPanelGroup(1);   {- build a panel group for ADN 1 }
  MSDisposeObj(cursor);
  PostMessage(HWindow, WM_NEXTDLGCTL, thers^.HWindow, 1);
end;

destructor TEditTherDlg.Done;
begin
  inherited Done;
  MSDisposeObj(cIntr);
end;

procedure TEditTherDlg.WMDrawItem(var msg: TMessage);
var
  p  : PDrawItemStruct;
begin
  p:= PDrawItemStruct(msg.lparam);
  case p^.ctlID of
    IDC_PNLS:   pnls^.DrawItem(msg);
    IDC_THERS:  thers^.DrawItem(msg);
  end;
end;

procedure TEditTherDlg.WMMeasureItem(var msg: TMessage);
begin
  pnls^.MeasureItem(msg);
  thers^.MeasureItem(msg);
end;

procedure TEditTherDlg.WMCharToItem(var msg: TMessage);
begin
  pnls^.CharToItem(msg);
  thers^.CharToItem(msg);
end;

procedure TEditTherDlg.idEdit(var msg: Tmessage);
var
  k  : integer;
begin
  k:= thers^.GetSelIndex;
  if k <> -1 then
    EditTher(k);
end;

procedure TEditTherDlg.idTherList(var msg: TMessage);
var
  k  : integer;
begin
  if msg.lParamHi = LBN_DBLCLK then
  begin
    k:= thers^.GetSelIndex;
    if k >= 0 then
      EditTher(k);
  end;
end;

procedure TEditTherDlg.EditTher(row: integer);
var
  pstr    : array[0..25] of char;
  id      : longint;
  therRec : integer;
  dilStep : integer;
  ther    : byte;
  therOrig: byte;
begin
  id:= thers^.GetItemData(row);

  therRec:= id and $FFFF;
  dilStep:= id shr 16;

  ther:= cIntr^.GetTherapy(curGrp, therRec, dilStep);
  if application^.ExecDialog(New(PEditInterpDlg, Init(@self, ther, cIntr))) = IDOK then
  begin
    cIntr^.SetTherapy(curGrp, therRec, dilStep, ther);
    cIntr^.InterpValStr(false, ther, pstr, 25);

    thers^.SetData(UrineCol, row, pstr);
    cIntr^.InterpValStr(true, ther, pstr, 25);

    thers^.SetData(SystemicCol, row, pstr);
  end;
end;

procedure TEditTherDlg.SetPanelGroup(newGrp: integer);
var
  k, j  : integer;
  pstr  : array[0..200] of char;
  row   : longint;
  id    : longint;
begin
  curGrp:= newGrp;
  pnls^.SetRedraw(false);
  pnls^.ClearList;

  {- display panels in current group }
  for k:= 1 to cIntr^.PanelsInGroup(curGrp) do
  begin
    cIntr^.PanelName(curGrp, k, pstr, 200);
    row:= pnls^.AddString(pstr);
    pnls^.SetData(0, row, pstr);
  end;
  pnls^.SetRedraw(true);

  {- display therapy information for current panel group }
  thers^.SetRedraw(false);
  thers^.ClearList;

  cIntr^.LoadTherInfo(curGrp); { Load therapy info on demand, v4.00, ghg 8/3/98 }
  with cIntr^, grpInfo.therInfo do
  begin

    for k:= 1 to numRecs do
    begin
      LoadString(HInstance, IDS_TherDesc + therRecs[k].txtIdx - 1, pstr, 200);
      row:= thers^.AddString(pstr);
      thers^.SetData(0, row, pstr);
      j:= 1;
      {- iterate for each dilution }
      for j:= therRecs[k].numThers downto 1 do
      begin
        StrPCopy(pstr, cIntr^.mdaTbl[therRecs[k].mdis[j]]);
        thers^.SetData(MICCol, row, pstr);

        InterpValStr(false, therRecs[k].intrps[j], pstr, 200);
        thers^.SetData(urineCol, row, pstr);

        InterpValStr(true, therRecs[k].intrps[j], pstr, 200);
        thers^.SetData(systemicCol, row, pstr);

        {- item data will contain therRec/Dilution }
        id:= MakeLong(k, j);
        thers^.SetItemData(row, id);

        if j > 1 then
          row:= thers^.AddString('');
      end;
    end;
  end;
  thers^.SetRedraw(true);
  EnableButtons;
  if curGrp = 1 then
    SetFocus(GetDlgItem(HWindow, IDC_NEXT));
  if curGrp = cIntr^.NumPanelGroups then
    SetFocus(GetDlgItem(HWindow, IDC_PREV));
  thers^.SetSelIndex(0);
end;

procedure TEditTherDlg.EnableButtons;
begin
  EnableWindow(GetDlgItem(HWindow, IDC_PREV), curGrp > 1);
  EnableWindow(GetDlgItem(HWindow, IDC_NEXT), curGrp < cIntr^.NumPanelGroups);
end;

procedure TEditTherDlg.idNext(var msg: TMessage);
Var
  k     : integer;
  err   : integer;
  pstr  : array[0..200] of char;
  p2    : array[0..100] of char;
begin
  if curGrp < cIntr^.NumPanelGroups then
  Begin
    if cIntr^.IsAnyTherModified then
    Begin
      SR(IDS_CHANGEMADE, pstr, 200);
      StrCat(pstr, #10#10);
      SR(IDS_SAVECHANGE, p2, 100);
      StrCat(pstr, p2);
      k:= YesNoCancelMsg(HWindow, SR(IDS_CONFIRM, p2, 100), pstr);
      if k = IDYES then
        if not DoSave then
          exit;

      if k <> IDCANCEL then
         SetPanelGroup(curGrp + 1);
    End
    Else
      SetPanelGroup(curGrp + 1);
  End
  else
    MessageBeep(0);
end;

procedure TEditTherDlg.idPrev(var msg: TMessage);
Var
  k     : integer;
  err   : integer;
  pstr  : array[0..200] of char;
  p2    : array[0..100] of char;
begin
  if curGrp > 1 then
  Begin
    if cIntr^.IsAnyTherModified then
    Begin
      SR(IDS_CHANGEMADE, pstr, 200);
      StrCat(pstr, #10#10);
      SR(IDS_SAVECHANGE, p2, 100);
      StrCat(pstr, p2);
      k:= YesNoCancelMsg(HWindow, SR(IDS_CONFIRM, p2, 100), pstr);
      if k = IDYES then
        if not DoSave then
          exit;

      if k <> IDCANCEL then
       SetPanelGroup(curGrp - 1);
    End
    Else
      SetPanelGroup(curGrp - 1);
  End
  else
    MessageBeep(0);
end;

function TEditTherDlg.DoSave: boolean;
var
  err  : integer;
  pstr : array[0..200] of char;
begin
  err:= cIntr^.SaveChanges;
  if err <> 0 then
  begin
    if err = 1 then
      ShowError(@self, IDS_CANTREAD, nil, ERR_CANTREAD, MOD_CUSTINTR, 0)
    else
      ShowError(@self, IDS_CANTWRITE, nil, ERR_CANTWRITE, MOD_CUSTINTR, 0);
    DoSave:= false;
  end
  else
    DoSave:= true;
end;

procedure TEditTherDlg.Cancel(var msg: TMessage);
var
  k     : integer;
  err   : integer;
  pstr  : array[0..200] of char;
  p2    : array[0..100] of char;
begin
  if cIntr^.IsAnyTherModified then
  begin
    SR(IDS_CHANGEMADE, pstr, 200);
    StrCat(pstr, #10#10);
    SR(IDS_SAVECHANGE, p2, 100);
    StrCat(pstr, p2);
    k:= YesNoCancelMsg(HWindow, SR(IDS_CONFIRM, p2, 100), pstr);
    if k = IDYES then
      if not DoSave then
        exit;

    if k <> IDCANCEL then
      inherited Cancel(msg);
  end
  else
    inherited Cancel(msg);
end;

procedure TEditTherDlg.IDSave(var msg: TMessage);
var
  k    : integer;
  pstr, p2 : array[0..100] of char;
begin
  if cIntr^.IsAnyTherModified then
  begin
    if YesNoMsg(HWindow, SR(IDS_CONFIRM, p2, 100), SR(IDS_SAVECONFIRM, pstr, 100)) then
      DoSave;
  end
  else
    InfoMsg(HWindow, SR(IDS_CUSTINTERPS, p2, 100), SR(IDS_NOCHANGES, pstr, 100));
end;


{---------------------------------------------[ TEditInterpDlg ]--}

constructor TEditInterpDlg.Init(AParent: PWindowsObject; var aTher: byte; aCustIntr: PCustIntr);
begin
  inherited Init(AParent, MakeIntResource(DLG_EDITINTERP));

  ugrid:= New(PGrid, InitResource(@self, IDC_URINELIST, 1, false));
  sgrid:= New(PGrid, InitResource(@self, IDC_BLOODLIST, 1, false));

  iVals:= New(PCollection, Init(10, 8));
  ther:= @aTher;
  cIntr:= aCustIntr;
end;

destructor TEditInterpDlg.Done;
begin
  inherited Done;
  MSDisposeObj(iVals);
end;

procedure TEditInterpDlg.SetupWindow;
var
  tstr  : string[25];
  pstr  : array[0..100] of char;
  pstr2 : array[0..25] of char;
  row   : integer;
  k     : integer;
  ufocRow, bfocRow: integer;
  bTher : byte;
  uTher : byte;
  ci    : boolean;
begin
  inherited SetupWindow;

  ugrid^.SetHeader(0, SR(IDS_URINE, pstr, 100));
  sgrid^.SetHeader(0, SR(IDS_SYSTEMIC, pstr, 100));
  uGrid^.StretchColumn(0);
(*  uGrid^.SetColumnWidth(0, ugrid^.GetColumnWidth(0)+5);*)
(*  uGrid^.StretchColumn(0);*)
  sGrid^.StretchColumn(0);
(*  sGrid^.SetColumnWidth(0, sgrid^.GetColumnWidth(0)+5);*)
(*  sGrid^.StretchColumn(0);*)

  uTher:= cIntr^.InterpVal(false, ther^);
  bTher:= cIntr^.InterpVal(true, ther^);

  ufocRow:= 0;
  bfocRow:= 0;
  for k:= 0 to 15 do
  begin
    InterpCodeToStr(k, pstr, sizeof(pstr)-1, ci);
    if (StrLen(pstr) > 0) and (pstr[0] <> '*') then
    begin
      row:= sgrid^.AddString(nil);
      sGrid^.SetData(0, row, pstr);
      row:= ugrid^.AddString(nil);
      uGrid^.SetData(0, row, pstr);
      ugrid^.SetItemData(row, k); {- itemdata = therapy byte value }
      sgrid^.SetItemData(row, k);
    end;
  end;

  StrCopy(pstr, SR(IDS_BLANK, pstr, 100));
  row:= sGrid^.AddString(nil);
  sGrid^.SetData(0, row, pstr);
  sGrid^.SetItemData(row, 6);  {- THERAPY.DAT uses 6 for blank }

  row:= uGrid^.AddString(nil);
  uGrid^.SetData(0, row, pstr);
  uGrid^.SetItemData(row, 6);

  for k:= 0 to uGrid^.GetCount - 1 do
  begin
    if uGrid^.GetItemData(k) = uTher then
      uFocRow:= k;
    if sGrid^.GetItemData(k) = bTher then
      bFocRow:= k;
  end;

  ugrid^.SetSelIndex(ufocRow);
  sgrid^.SetSelIndex(bfocRow);
end;

procedure TEditInterpDlg.WMDrawItem(var msg: TMessage);
begin
  ugrid^.DrawItem(msg);
  sgrid^.DrawItem(msg);
end;

procedure TEditInterpDlg.WMMeasureItem(var msg: TMessage);
begin
  ugrid^.MeasureItem(msg);
  sgrid^.MeasureItem(msg);
end;

procedure TEditInterpDlg.WMCharToItem(var msg: TMessage);
begin
  ugrid^.CharToItem(msg);
  sgrid^.CharToItem(msg);
end;

function TEditInterpDlg.CanClose: boolean;
var
  uSel, bSel : integer;
  uTher, bTher : byte;
begin
  CanClose:= false;
  uSel:= ugrid^.GetSelIndex;
  bSel:= sgrid^.GetSelIndex;

  if (uSel <> -1) then
  begin
    uTher:= byte(ugrid^.GetItemData(uSel));
    ther^:= cIntr^.MergeInterp(false, ther^, uTher);
  end;

  if (bSel <> -1) then
  begin
    bTher:= byte(sgrid^.GetItemData(bSel));
    ther^:= cIntr^.MergeInterp(true, ther^, bTher);
  end;

  CanClose:= true;
end;

procedure TEditInterpDlg.WMCommand(var msg: TMessage);
begin
  if msg.lParamHI = LBN_DBLCLK then
  begin
    if (msg.wParam = IDC_URINELIST) or (msg.wParam = IDC_BLOODLIST) then
    begin
      PostMessage(HWindow, WM_COMMAND, ID_OK, MakeLong(GetDlgItem(HWindow, ID_OK), BN_CLICKED));
    end
    else
      inherited WMCommand(msg);
  end
  else
    inherited WMCommand(msg);
end;


var
  myApp : TMyApp;

BEGIN
  if OKToRun(MOD_CUSTINTR, false) then
  begin
    myApp.Init(GetModuleName(MOD_CUSTINTR));
    myApp.Run;
    myApp.Done;
  end;
END.

{- rcf }
