const
	MNU_FileStatistics	=	202;
	MNU_ProgramExit	=	299;

	MNU_SpecimenLogin	=	301;
	MNU_GramStainResults	=	302;
	MNU_Isolate	=	303;
	MNU_OrderEntry	=	304;
	MNU_SpecimenEntry	=	305;
	MNU_PatientEntry	=	406;
	MNU_IDMICENTRY	=	307;


	MNU_CustMnemonics	=	501;
	MNU_CustWindows	=	502;
	MNU_CustHWConfig	=	503;
	MNU_CustDrugs	=	504;
	MNU_CUSTTESTDEF	=	505;
	MNU_CUSTTESTGROUP	=	506;
	MNU_CUSTORDERS	=	507;
	MNU_CustOrganism	=	508;
	MNU_CUSTGRAMSTAINDEF	=	509;
	MNU_CUSTGRAMSTAINVALS	=	510;
	MNU_CustIntMapping	=	511;
	MNU_CustSessions	=	512;
	MNU_CustThresholds	=	513;

	MNU_WalkAway	=	701;
	MNU_AS4QCDIAG	=	702;
	MNU_FinalSpecimens	=	703;
	MNU_MoveIsolates	=	704;
	MNU_TwoWayInt	=	705;

	MNU_HelpIndex	=	801;
	MNU_HelpUsing	=	802;
	MNU_About	=	803;

        IDS_EXITMSG             = 28977;
        IDS_TITLE               = 28978;
	ICN_DADE	=	28976;
	MNU_MAIN	=	28976;
	INV_USE_ONLY	=	2;
	MNU_REPORTS	=	407;
	IDS_ERREXEC	=	28979;
	DLG_ABOUT	=	28976;
	IDC_COPYRIGHT	=	200;
	IDC_MSCAN	=	201;
	MNU_CUS_REP_EPI	=	102;
	MNU_ESBL	=	514;
	MNU_VIE	=	518;
	MNU_VRE	=	519;
	MNU_E157	=	517;
	MNU_ESBLOrgs	=	520;
	MNU_MRS	=	516;
	MNU_IB	=	521;
	MNU_OxMultResHold	=	101;
	MNU_VaOnlyHold	=	103;
	MNU_VaStSHold	=	104;
	MNU_MultiDrugs	=	105;
	MNU_AUTORESOLVE	=	106;
	IDC_Version	=	202;
	MNU_XTRAS	=	107;
