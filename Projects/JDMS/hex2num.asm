.MODEL large, Pascal

PUBLIC HVAL

.CODE

;==========================================================
;
; function HVal(hStr: PChar; var code: integer): longint; far;
;
; Converts a hex string to a number. Will scan hStr until end of
; string or an invalid character is found. Valid characters in string
; are '0'-'9', 'A'-'F', 'a'-'f'.
;
; on return, code will be set to 0 if successful. otherwise code will be
; set to the index of the invalid character in the string and 0 will be
; returned.
;
;
;--------------------------------------------------------------------
;
;     2       4        4     4
;   | BP | RetAddr | code | hStr |	<----- Stack Usage
;   `----------------------------'
;                  ^=+6   ^=+10
;
;   Function result returned in DX:AX
;

HVal    PROC FAR

hStr    equ     dword ptr       ss:[bp+10]
code    equ     dword ptr       ss:[bp+6]
; Local vars
hiNum   equ     word ptr        ss:[bp-2]
loNum   equ     word ptr        ss:[bp-4]
pSize   equ     8

        push    bp
        mov     bp,sp
        sub     sp, 4		; Make room for local space
        push    ds

; initialize
        les     di, code        ; get pointer to code
        mov     es:[di], word ptr 0  ; set code to zero

        xor     ax,ax           ; AX = 0 (set result to 0)
        mov     loNum,ax        ; Lo(num) = 0
        mov     HiNum,ax        ; Hi(num) = 0

        cld                     ; Direction ->

        les     di, hstr        ; get length of source string
        mov     al, 0
        mov     cx, 0ffffh
        repne scasb             ; search for terminating null
        mov     ax, 0ffffh      ;
        sub     ax, cx
        dec     ax
        mov     cx,ax           ; CX = len
        jcxz    done

        lds     si,hStr
        mov     bx,cx           ; save length in BX

; While digits remain to be scanned

getdigit:
        lodsb                   ; get next digit from string

        cmp     al,'a'          ; is char ('a' thru 'f') ?
        jb      tryNumber
        cmp     al,'f'
        ja      tryNumber
        sub     al,32           ; convert to uppercase

tryNumber:
        cmp     al,'0'          ; check digit for validity
        jb      error
        cmp     al,'9'
        ja      tryletter
        sub     al,'0'          ; char is ['0'-'9'] change to binary => al
        jmp     convert
tryletter:
        cmp     al,'A'          ; is char 'A'-'F'
        jb      error
        cmp     al,'F'
        ja      error
        sub     al,37h          ; char is ['0'-'9'] change to binary => al
; AL should now have the binary value of the digit
convert:
        add     loNum,ax        ; add digit value to result num
;	adc	hiNum,0		; no need for this since adding 0-F only

        cmp     cl,1
        je      again

        shl     loNum,1		; If there is another digit available
	rcl     HiNum,1		; then multiply result number by 16
	shl     loNum,1
	rcl     HiNum,1
	shl	loNum,1
	rcl	HiNum,1
	shl	loNum,1
	rcl	HiNum,1
        jc      overflow        ; overflow, next number wont fit
again:
        loop    getDigit
        jmp     done            ; all finished

overflow:
       dec      cx              ; overflow, next number wont fit,
                                ; adjust cx to next digit
error:
       sub	bx,cx		; calculate index into string
       inc	bx
       les	di,code		; get pointer to code
       mov	es:[di], bx	; set index of invalid char
       xor	ax,ax		        ; AX = 0 (set result to 0)
       mov      loNum,ax        ; Lo(num) = 0   Clear result
       mov      HiNum,ax        ; Hi(num) = 0
done:
       mov      ax,lonum        ; set return value
       mov      dx,hinum

       pop      ds              ; restore DS
       mov      sp,bp           ; restore stack
       pop      bp
       ret      pSize

HVAL   ENDP

END