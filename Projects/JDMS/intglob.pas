Unit IntGlob;

Interface



Uses
  APITools,
  DMString,
  IniLib,
  PrnPrev,
  MScan,
  WinTypes,
  WinProcs,
  Strings,
  Objects,
  ODialogs,
  OWindows;

const
  MFIVersion = '4.00';

	CR = #13;
  NULL = #0;


{String table constants}
  str_IntBase   =  MOD_TWOWAY;
  str_EntrySel  =  0;
  str_IntMsg    =  1;
  str_OKDel     =  2;
  str_DelSingle1=  3;
  str_DelSingle2=  4;
  str_DelAll    =  5;
  str_IsBlank   =  8;
  str_WndTitle  =  9;
  str_BetaWarn1 =  10;
  str_BetaWarn2 =  11;
  str_BetaDone  =  12;
  str_TransTime =  13;
  str_MultCopy  =  14;
  str_TimerOn   =  15;
  str_TimerOff  =  16;
  str_ClosePort =  17;
  str_BuildComm =  18;
  str_OpenPort  =  19;
  str_File      =  20;
  str_PhyMnem   =  21;
  str_WardMnem  =  22;
  str_SrcMnem   =  23;
  str_AdmMnem   =  24;
  str_HspMnem   =  25;
  str_InstMnem  =  26;
  str_ComMnem   =  27;
  str_GenderMnem=  28;
  str_DosMnem   =  29;
  str_OrdIDMnem =  30;
  str_TestCatMnem= 31;
  str_TestMnem  =  32;
  str_ValueMnem =  33;
  str_UserMnem  =  34;
  str_Parity    =  35;
  str_ParityA   =  36;
  str_ParityB   =  37;
  str_LevName   =  38;
  str_LevNameA  =  39;
  str_LevNameB  =  40;
  str_JIS       =  41;
  str_ShiftJIS  =  42;
  str_None      =  43;
  str_SISO      =  44;
  str_Files     =  45;
  str_FilesA    =  46;
  str_V17Files  =  47;
  str_V17FilesA =  48;
  str_DelFieldDef1=49;
  str_DelFieldDef2=50;
  str_Continue  =  51;
  str_BlankField=  52;
  str_BadNum    =  53;
  str_SelItem   =  54;
  str_NoDel     =  55;
  str_Int       =  59;
  str_IntToDMS  =  60;
  str_DMSToInt  =  61;
  str_V17DelMap1=  62;
  str_V17DelMap2=  63;
  str_DelMap1   =  65;
  str_DelMap2   =  66;
  str_NoMap     =  67;
  str_MapAlready=  68;
  str_ToIntMsg  =  71;
  str_ToDMSMsg  =  72;
  str_ParmList  =  74;
  str_LogLev    =  75;
  str_LogName   =  76;
  str_TimeOut   =  77;
  str_ConvMode  =  78;
  str_DBCSMode  =  79;
  str_XRefOn    =  80;
  str_Modem     =  81;
  str_ModemCfg  =  82;
  str_ModemInit =  83;
  str_ModemDial =  84;
  str_ModemHangup= 85;
  str_ModemBreak=  86;
  str_XRefTitle =  87;
  str_FileTitle =  88;
  str_FileTitleA=  89;
  str_FileTitleB=  90;
  str_MapTitle  =  91;
  str_RecTitle  =  92;
  str_RecTitleA =  93;
  str_FieldTitle=  94;
  str_RecvData  =  97;
  str_BegPatID  =  98;
  str_EndPatID  =  99;
  str_BegSpcID  =  100;
  str_EndSpcID  =  101;
  str_BegColl   =  102;
  str_EndColl   =  103;
  str_BegTest   =  104;
  str_EndTest   =  105;
  str_SaveRange =  106;
  str_SendData  =  107;
  str_TransSel  =  108;
  str_LogError  =  109;
  str_LogTitle  =  110;
  str_LogMem    =  111;
  str_FieldNum  =  112;
  str_CompNum   =  113;
  str_RepeatNum =  114;
  str_FieldName =  115;
  str_DMS       =  116;
  str_ExecErr   =  117;
  str_MapsWillBeCleared = 118;
  str_NoDongle  =  236;


{XRef display constants}
  MaxListLen = 48;
  MnemCol = 0;
  AltCol = 15;

{Error Codes}
Rcv_InsSendKey    =    100;


  {1921-1934}  {also used for ini file}
  XRefTableNames: array[0..13] of PChar = ('&Physician Code', '&Ward Code', '&Source Code',
    '&Admission Code', '&Hospital Code', '&Institution Code', '&Comment Code', 'Patient &Gender Code', '&Dosage Category',
    '&Order ID', '&Test Type', '&Drug Result Type', 'Data &Value', '&User Defined Fields');


	MnemTypeOne       = 'A';
  MnemTypeTwo       = 'B';
  MnemTypeThree     = 'C';
  MnemTypeFour      = 'D';
  MnemTypeFive      = 'E';
  MnemTypeSix       = 'F';
  MnemTypeSeven     = 'G';
  MnemTypeEight     = 'H';
  MnemTypeNine      = 'I';
  MnemTypeTen       = 'J';
  MnemTypeEleven    = 'K';
  MnemTypeTwelve    = 'L';
  MnemTypeThirteen  = 'M';
  MnemTypeFourteen  = 'N';

  XRefMnemTypes: array[0..13] of char = (MnemTypeOne, MnemTypeTwo, MnemTypeThree, MnemTypeFour,
    MnemTypeFive, MnemTypeSix, MnemTypeSeven, MnemTypeEight, MnemTypeNine, MnemTypeTen, MnemTypeEleven, MnemTypeTwelve,
    MnemTypeThirteen, MnemTypeFourteen);

{These are the arrays that hold the valid selections of the ComboListBoxes
 for customization, they will appear in the order specified in the list.}

  NoOfPortParms = 3; {Actual number of parameters minus one}
  PortArray: array[0..NoOfPortParms] of PChar = ('COM1:', 'COM2:',
    'COM3:', 'COM4:');

  NoOfBaudParms = 6; {Actual number of parameters minus one}
  BaudArray: array[0..NoOfBaudParms] of PChar = ('300', '1200', '2400',
    '4800', '9600', '19200', '38400');

  NoOfParityParms = 2; {Actual number of parameters minus one}
  NoOfStopBitParms = 2; {Actual number of parameters minus one}
  StopBitArray: array[0..NoOfStopBitParms] of PChar = ('1', '1.5', '2');

  NoOfDataBitParms = 1; {Actual number of parameters minus one}
  DataBitArray: array[0..NoOfDataBitParms] of PChar = ('7', '8');

	NoOfLogFileParms = 2; {Actual number of parameters minus one}
  LogLevelArray: array[0..NoOfLogFileParms] of PChar = ('1', '2', '3');
  MenuLen        = 20;

{Names for ini file}
	NoOfCstmParms = 5;
  CstmParmNames: array[0..NoOfCstmParms-1] of PChar = ('IntPort', 'IntBaud', 'IntParity',
    'IntStop', 'IntData');

{Length of edit boxes in customization}
  MaxEditLen = 8;
  MaxLogLen = 25;
	MaxComboBoxLen = 20;
  MaxTimeoutLen = 3;
  InitStrLen = 40;
  DialStrLen = 30;
  BreakStrLen = 10;

{Field Numbers for cross reference table files}
  MTypeKey = 1;
  PrimKey = 2;
  SecKey = 3;
  IsPrimField = 4;

{for mapping/setup interface record - IntMap}
	SetupNameLen     = 25;

	id_ShowPatient   = 101;
	id_AbortTrans    = 102;
	id_ShowSpecimen  = 103;
	id_ShowIsolate   = 104;

Type
	PProcessStatusDlg = ^TProcessStatusDlg;
	TProcessStatusDlg = object(TDialog)
    Abort: Boolean;
		constructor Init(AWndObj: PWindowsObject; AName: PChar);
		procedure SetupWindow;virtual;
    procedure AbortTrans(var Msg: TMessage);virtual id_First + id_AbortTrans;
    procedure Update;virtual;
  end;

  PMFIPrintInfo = ^TMFIPrintInfo;
  TMFIPrintInfo = object(TPrintInfo)
    constructor Init(aTitle: PChar);
    procedure   ListLn(aStr: PChar);
    procedure   ListLnSpecial(aStr: PChar; cellFlags: longint);
  end;

	TRequestRanges = record
								  	 StartRange:  array[0..25] of char;
										 EndRange:    array[0..25] of char;
										 RequestType: array[0..1] of char;
                     ReTransmit: boolean;
									 end;


var
  FieldDelimiter: Char;
  RepeatDelimiter: Char;
  ComponentDelimiter: Char;
  EscapeCharDelimiter: Char;
	SingleRecord: array[0..511] of char;
	RecordID: char;
	ParentWnd: HWnd;
	LgFileHnd: File;
	Entries: integer;
	LogFileName: array[0..14] of char;
	LogTextFileName: array[0..14] of char;
	LgTextHnd: File;
  ClientReady, RcvMode: boolean;
  FirstReceive, AutoUploadOn: boolean;
	GlobHWnd: HWnd;
	GlobWndObj: PWindowsObject;
	MaxEntries: integer;
  ignoreticks: boolean;
	Ranges: TRequestRanges;
	ShowReceive: boolean;
	ProcessStatDlg : PProcessStatusDlg;

procedure FormatSpecialNumField(fldStr: PChar; fldStrLen: word);
procedure CheckDateField(AField: PChar; maxLen: integer);
procedure DisableMenu(HWindow: HWnd);
procedure EnableMenu(HWindow: HWnd);

Implementation

Uses
  IntlLib,
  StrsW;

procedure FormatSpecialNumField(fldStr: PChar; fldStrLen: word);
{ This code was blatantly stolen from TUIFld.Format. }
{ NOTE: It should be generalized later.              }
var
  left    : array[0..100] of char;
  right   : array[0..100] of char;
  p       : PChar;
  num     : double;
  code    : integer;
  ret     : integer;
begin
  AnsiUpper(fldStr);
  p:= StrScan(fldStr, '.');
  if p <> nil then
  begin
    StrCopy(right, (p+1));
    p^:= #0;
    StrCopy(left, fldStr);
    GetMem(p, fldStrLen+1);
    StrCopy(p, '');
    LeftPad(fldStr, p, '0', fldStrLen);
    MSFreeMem(p, fldStrLen+1);
    Move(left[0], fldStr[0], StrLen(left));
    Move(right[0], fldStr[StrLen(fldStr)-StrLen(right)], StrLen(right));
  end
  else
  begin
    Val(fldStr, num, code);
    if code = 0 then  {- standard number entered, left just with 0's }
    begin
      StrCopy(left, fldStr);
      LeftPad(fldStr, left, '0', fldStrLen);
    end;
  end;
end;

procedure CheckDateField(AField: PChar; maxLen: integer);

begin
  if (StrComp(AField, 't') = 0) or (StrComp(AField, 'T') = 0) then
    IntlDateStr(IntlCurrentDate, aField, maxlen);
end;

procedure DisableMenu(HWindow: HWnd);
var
  aMenu : HMenu;
  count : word;

begin
  for count:= 0 to GetMenuItemCount(GetMenu(HWindow))-1 do
  begin
    EnableMenuItem(GetMenu(HWindow), count, MF_BYPOSITION or MF_DISABLED or MF_GRAYED);
  end;
  DrawMenuBar(HWindow);
end;

procedure EnableMenu(HWindow: HWnd);
var
  aMenu : HMenu;
  count : word;

begin
  for count:= 0 to GetMenuItemCount(GetMenu(HWindow))-1 do
  begin
    EnableMenuItem(GetMenu(HWindow), count, MF_BYPOSITION or MF_ENABLED);
  end;
  DrawMenuBar(HWindow);
end;

{
*********************
ProcessRecvDlg Object
*********************
}

constructor TProcessStatusDlg.Init(AWndObj: PWindowsObject; AName: PChar);
begin
  inherited Init(AWndObj, AName);
{	if not ShowReceive then
		Attr.Style := Attr.Style or ws_Visible;
}end;

procedure TProcessStatusDlg.SetupWindow;
begin
  inherited SetupWindow;
  EnableMenuItem(GetSystemMenu(HWindow, False), 6, mf_ByPosition or mf_Grayed or mf_SysMenu);
end;

procedure TProcessStatusDlg.AbortTrans(var Msg: TMessage);
begin
  Abort := True;
end;

procedure TProcessStatusDlg.Update;
var
  Msg: TMsg;

begin
  while PeekMessage(Msg, 0, 0, 0, pm_Remove) do
    if not IsDialogMessage(HWindow, Msg) then
    begin
			TranslateMessage(Msg);
      DispatchMessage(Msg);
    end;
end;

constructor TMFIPrintInfo.Init(aTitle: PChar);
var
  fullTitle : array[0..100] of char;
  pstr      : array[0..100] of char;
  lf        : TLogFont;
begin
  {need to make font BEFORE calling the inherited constructor!}
  MakeScreenFont(lf, false, false);
  inherited Init(lf, IniDefPointSize);

  LoadString(HInstance, str_IntBase+str_IntMsg, fullTitle, SizeOf(fullTitle));
  StrCat(fullTitle, ' - ');
  StrCat(fullTitle, aTitle);
  header^.AddRow(r_BorderTop or r_BorderBottom, 0);
  header^.AddCell(fullTitle, 0, c_Stretch or c_Bold, -2);
  header^.AddCell(SR(IDS_PRINTEDON, pstr, SizeOf(pstr)-1), LT200, c_PageRight or c_Right or c_Date, 0);
  header^.AddRow(r_Normal, 0);

  footer^.AddRow(r_CenterRow or r_BorderTop, 0);
  footer^.AddCell(SR(IDS_PAGEPOFN, pstr, SizeOf(pstr)-1), LT300, c_Center or c_PageNum or c_NumPages, 0);
end;

procedure TMFIPrintInfo.ListLn(aStr: PChar);
begin
  body^.AddRow(r_Normal, 0);
  body^.AddCell(aStr, 0, c_Stretch, 0);
end;

procedure TMFIPrintInfo.ListLnSpecial(aStr: PChar; cellFlags: longint);
begin
  body^.AddRow(r_Normal, 0);
  body^.AddCell(aStr, 0, c_Stretch or cellFlags, 0);
end;

Begin
End.
