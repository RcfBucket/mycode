unit UIIDMIC;

INTERFACE

(*
  CheckIsoSession is duplicated in UIQEORD. This could be pulled together in UICOMMON
*)

uses
  OWindows,
  DBTypes;

procedure UIIDMICEntry(aParent: PWindowsObject; anIsoSeq: TSeqNum; viewOnly: boolean);

IMPLEMENTATION

uses
  APITools,
  BioType,
  DBFile,
  DBIDS,
  DBLib,
  DMSErr,
  DMString,
  DlgLib,
  ESBL,
  GridLib,
  INILib,
  ListLib,
  MScan,
  ODialogs,
  Objects,
  Gather,
  Interps,
  ResLib,
  Screens,
  Strings,
  StrSw,
  UICommon,
  UIFlds,
  UIIso,
  UISpec,
  UIPat,
  UIResult,
  UserMsgs,
  Sessions,
  Win31,
  WinProcs,
  WinTypes;

{$R UIIDMIC.RES}
{$I UICOMMON.INC}
{$I UIIDMIC.INC}

const
  {- column constants used by TUINonMICGrid }
  nonMIC_Test       = 0;    {- test name }
  nonMIC_Result     = 1;    {- Result value }

  {- column constants used by TUIMICGrid }
  MIC_Drug          = 0;    {- drug name }
  MIC_MIC           = 1;    {- MIC value }
  MIC_Interp        = 2;    {- interp value }

type
  {- UI Results grid (base) }
  PUIResGrid = ^TUIResGrid;
  TUIResGrid = object(TGrid)
    lastRow   : longint;
    {}
    procedure SetupWindow; virtual;
    procedure WMKillFocus(var msg: TMessage); virtual WM_FIRST + WM_KILLFOCUS;
    procedure WMSetFocus(var msg: TMessage); virtual WM_FIRST + WM_SETFOCUS;
  end;

  PIDMICDlg = ^TIDMICDlg;
  TIDMicDlg = object(TUICommonDlg)
    micGrid       : PUIResGrid;
    nonMICGrid    : PUIResGrid;
    dbSpec        : PDBFile;
    dbPat         : PDBFile;
    dbIso         : PDBFile;
    tempSpec      : PDBRec;
    tempIso       : PDBRec;
    origIso       : PDBRec;
    results       : PResults;
    sFld, cFld, pFld, iFld, ordr, org: PUIFld;
    uiRes         : PGatherRes;
    curSess       : TSeqNum;
    viewOnly      : boolean;
    setfamily     : word;
    fmly          : PStatic;
    IgnoreOrgFocus: boolean;

    {}
    constructor Init(aParent: PWindowsObject; anIsoSeq: TSeqNum; aViewOnly: boolean);
    destructor Done; virtual;
    procedure ShowSetFamily;
    procedure SetupWindow; virtual;
    procedure SetUpForSession;
    procedure SetModuleID; virtual;
    procedure EnableButtons; virtual;
    procedure ClearFields; virtual;
    procedure ClearIsolate;
    procedure ClearResults;
    procedure FillSpecimen;
    procedure FillIsolate;
    procedure FillResults(ClearValues: Boolean);
    procedure GetResults;
    procedure EditTest;
    procedure AddTest;
    function DataChanged: boolean; virtual;
    function CheckSave(clearAfter: boolean): boolean; virtual;
    function SaveData(clearAfter: boolean): boolean; virtual;
    procedure EnableIsoFlds(on: boolean);
    procedure DoSpecCollExit(newFocID: word);
    procedure DoIsoExit(newFocID: word);
    procedure NextPrev(doNext: boolean); virtual;
    function CheckIsoSess(anIso: PDBRec): boolean;
    procedure WMLostFocus(var msg: TMessage);    virtual WM_FIRST + WM_LOSTFOCUS;
    procedure WMFldList(var msg: TMessage);      virtual WM_FIRST + WM_FLDLIST;
    procedure IDIsolate(var msg: TMessage);      virtual ID_FIRST + IDC_ISOLATE;
    procedure IDClear(var msg: TMessage);        virtual ID_FIRST + IDC_UICLEAR;
    procedure IDSession(var msg: TMessage);      virtual ID_FIRST + IDC_SESSION;
    procedure IDSpecimen(var msg: TMessage);     virtual ID_FIRST + IDC_SPECIMEN;
    procedure IDPatient(var msg: TMessage);      virtual ID_FIRST + IDC_PATIENT;
    procedure IDEditTest(var msg: TMessage);     virtual ID_FIRST + IDC_EDITTEST;
    procedure IDAddTest(var msg: TMessage);      virtual ID_FIRST + IDC_ADDTEST;
    procedure IDRecalc(var msg: TMessage);       virtual ID_FIRST + IDC_RECALC;
    procedure WMCharToItem(var msg: TMessage);   virtual WM_FIRST + WM_CHARTOITEM;
    procedure WMDrawItem(var msg: TMessage);     virtual WM_FIRST + WM_DRAWITEM;
    procedure WMMeasureItem(var msg: TMessage);  virtual WM_FIRST + WM_MEASUREITEM;
    procedure WMGridGotFoc(var msg: TMessage);   virtual WM_FIRST + WM_RESGRIDGOTFOCUS;
    procedure WMCommand(var msg: TMessage);      virtual WM_FIRST + WM_COMMAND;

    procedure RecalcInterps;
    Procedure RecalcESBLInterps;
    function  InspectESBL: Boolean;
    function  ReplaceOrg(NewOrg: Integer): boolean;
    function  CurrentOrganism: Integer;
    function  ExitAfterSave: boolean; virtual;
  end;

  PClearDlg = ^TClearDlg;
  TClearDlg = object(TCenterDlg)
    procedure IDAll(var msg: TMessage); virtual ID_FIRST + IDC_CLEARALL;
    procedure IDClearIso(var msg: TMessage); virtual ID_FIRST + IDC_CLEARISO;
    procedure IDClearRes(var msg: TMessage); virtual ID_FIRST + IDC_CLEARRESULTS;
  end;

  PAddDrugDlg = ^TAddDrugDlg;
  TAddDrugDlg = object(TCenterDlg)
    AddDrugsGrid: PGrid;
    SelSeq      : ^TSeqNum;

    {}
    constructor Init(aParent: PWindowsObject; var xSelSeq: TSeqNum);
    destructor Done; virtual;
    procedure SetupWindow; virtual;
    procedure FillListBox; virtual;
    procedure OK(var msg: TMessage); virtual id_First + IDOK;
    procedure CANCEL(var msg: TMessage); virtual id_First + IDCANCEL;
    procedure DrawItem(var msg: TMessage); virtual wm_First + wm_DrawItem;
    procedure MeasureItem(var msg: TMessage); virtual wm_First + wm_MeasureItem;
    procedure CharToItem(var msg: TMessage); virtual wm_First + wm_CharToItem;
    procedure List(var msg: TMessage); virtual id_First + IDC_ADDDRUGLIST;
    function  GetSelSeq: TSeqNum;
  end;

procedure TClearDlg.IDAll(var msg: TMessage);
begin
  EndDlg(IDC_CLEARALL);
end;

procedure TClearDlg.IDClearIso(var msg: TMessage);
begin
  EndDlg(IDC_CLEARISO);
end;

procedure TClearDlg.IDClearRes(var msg: TMessage);
begin
  EndDlg(IDC_CLEARRESULTS);
end;

{-----------------------------------------------------------[ TUIResGrid ]--}

procedure TUIResGrid.SetupWindow;
begin
  lastRow:= -1;
  inherited SetupWindow;
end;

procedure TUIResGrid.WMKillFocus(var msg: TMessage);
{- unselect the current selection. Caret index will contain the current sel
   so it can be resored }
begin
  DefWndProc(msg);
  if GetSelIndex >= 0 then
    lastRow:= GetSelIndex
  else
    lastRow:= -1;
end;

procedure TUIResGrid.WMSetFocus(var msg: TMessage);
{- restores the selection to the last focused row }
begin
  if (parent <> nil) and (parent^.HWindow <> 0) and (uiFldsProcessFocus) then
    PostMessage(parent^.HWindow, WM_RESGRIDGOTFOCUS, word(GetID), longint(@self));
  DefWndProc(msg);
  if uiFldsProcessFocus then
    if (lastRow >= 0) and (lastRow < GetRowCount) then
      SetSelIndex(lastRow);
end;


{-------------------------------------------------------------[ TIDMICDlg ]--}
constructor TIDMicDlg.Init(aParent: PWindowsObject; anIsoSeq: TSeqNum; aViewOnly: boolean);
{- anIsoSeq is not required, if non zero then the specified iso will be preloaded}
var
  fNum    : integer;
  k       : integer;
  subst   : PErrSubst;
  fi      : TFldInfo;
  c       : PControl;
begin
  if not inherited Init(aParent, MakeIntResource(DLG_IDMIC), anIsoSeq) then
    fail;

  viewOnly:= aViewOnly;
  dbSpec:= nil;
  dbPat:= nil;
  dbIso:= nil;
  tempSpec:= nil;
  tempIso:= nil;
  origIso:= nil;
  results:= nil;
  uiRes:= nil;

  setfamily:= 0;
  IgnoreOrgFocus:=true;

  dbSpec:= New(PDBFile, Init(DBSpecFile, '', dbOpenNormal));
  if dbSpec = nil then
  begin
    subst:= New(PErrSubst, Init(DBSpecFile));
    DoFail(IDS_ERROROPEN, dbLastOpenError, subst);
    MSDisposeObj(subst);
    Fail;
  end;
  {- get spec file in proper sort order }
  if INISpecimenListSort = 2 then
    dbSpec^.dbc^.SetCurKeyNum(DBSPEC_DATE_KEY)
  else
    dbSpec^.dbc^.SetCurKeyNum(DBSPEC_ID_KEY);
  dbSpec^.dbc^.GetFirst(dbSpec^.dbr);

  dbPat:= New(PDBFile, Init(DBPatFile, '', dbOpenNormal));
  if dbPat = nil then
  begin
    subst:= New(PErrSubst, Init(DBPatFile));
    DoFail(IDS_ERROROPEN, dbLastOpenError, subst);
    MSDisposeObj(subst);
    Fail;
  end;

  dbIso:= New(PDBFile, Init(DBIsoFile, '', dbOpenNormal));
  if dbIso = nil then
  begin
    subst:= New(PErrSubst, Init(DBIsoFile));
    DoFail(IDS_ERROROPEN, dbLastOpenError, subst);
    MSDisposeObj(subst);
    Fail;
  end;

  tempSpec:= New(PDBRec, Init(dbSpec^.dbc));
  if tempSpec = nil then
  begin
    DoFail(IDS_CANTALLOCREC, 0, nil);
    Fail;
  end;
  tempIso:= New(PDBRec, Init(dbIso^.dbc));
  if tempIso = nil then
  begin
    DoFail(IDS_CANTALLOCREC, 0, nil);
    Fail;
  end;
  origIso:= New(PDBRec, Init(dbIso^.dbc));
  if origIso = nil then
  begin
    DoFail(IDS_CANTALLOCREC, 0, nil);
    Fail;
  end;

  results:= New(PResults, Init);
  if results = nil then
  begin
    DoFail(IDS_CANTINITRESULTS, 0, nil);
    Fail;
  end;
  fmly:= New(PStatic, InitResource(@self, IDC_FAMILY, 0));

  sFld:= LinkUIFld(DBSpecSpecID,      false, false, 0, IDC_EDSPECID,   IDC_LBLSPECID,   dbSpec^.dbd);
  cFld:= LinkUIFld(DBSpecCollectDate, false, false, 0, IDC_EDCOLLECT,  IDC_LBLCOLLECT,  dbSpec^.dbd);
         LinkUIFld(DBSpecSrc,         false, false, 0, IDC_EDSOURCE,   IDC_LBLSOURCE,   dbSpec^.dbd);
         LinkUIFld(DBSpecWard,        false, false, 0, IDC_EDWARD,     IDC_LBLWARD,     dbSpec^.dbd);
  pFld:= LinkUIFld(DBPatPatID,        false, false, 0, IDC_EDPATID,    IDC_LBLPATID,    dbPat^.dbd);
         LinkUIFld(DBPatLName,        false, false, 0, IDC_EDLAST,     IDC_LBLLAST,     dbPat^.dbd);
         LinkUIFld(DBPatFName,        false, false, 0, IDC_EDFIRST,    IDC_LBLFIRST,    dbPat^.dbd);

  iFld:= LinkUIFld(DBIsoIso,          false, false, 0, IDC_EDISO,      IDC_LBLISO,      dbIso^.dbd);
  ordr:= LinkUIFld(DBIsoOrdRef,       false, false, 0, IDC_EDORDER,    IDC_LBLORDER,    dbIso^.dbd);
  org:=  LinkUIFld(DBIsoOrgRef,       false, false, 0, IDC_EDORG,      IDC_LBLORG,      dbIso^.dbd);

  nonMICGrid:= New(PUIResGrid, InitResource(@self, IDC_NONMICGRID, 3, false));
  MICGrid:= New(PUIResGrid, InitResource(@self, IDC_MICGRID, 3, false));
end;

destructor TIDMICDlg.Done;
begin
  inherited Done;
  MSDisposeObj(dbSpec);
  MSDisposeObj(dbPat);
  MSDisposeObj(dbIso);
  MSDisposeObj(tempSpec);
  MSDisposeObj(tempIso);
  MSDisposeObj(origIso);
  MSDisposeObj(results);
  MSDisposeObj(uiRes);
end;

procedure TIDMicDlg.SetupWindow;
var
  k   : integer;
  row : longint;
  seq : TSeqNum;
  p1  : array[0..100] of char;

  procedure DisableIt(aFld: PUIFld); far;
  begin
    if (preLoad > 0) or ((aFld <> sFld) and (aFld <> cFld)) then
      aFld^.Enable(False);
  end;

begin
  inherited SetupWindow;

  with nonMICGrid^ do
  begin
    SetHeader(nonMIC_Test, SR(IDS_TEST, p1, sizeof(p1)-1));
    SetHeader(nonMIC_Result, SR(IDS_RESULT, p1, sizeof(p1)-1));
    SetAvgCharWidth(nonMIC_Test, 16);
    StretchColumn(nonMIC_Result);
  end;
  with MICGrid^ do
  begin
    SetHeader(MIC_Drug, SR(IDS_DRUG, p1, sizeof(p1)-1));
    SetHeader(MIC_MIC, SR(IDS_MIC, p1, sizeof(p1)-1));
    SetHeader(MIC_Interp, SR(IDS_INTERPRETATION, p1, sizeof(p1)-1));

    SetAvgCharWidth(MIC_Interp, 10);
    SetAvgCharWidth(MIC_MIC, 10);
    StretchColumn(MIC_Drug);
  end;

  flds^.ForEach(@DisableIt);
  SetupForSession;

  if preLoad <> 0 then
  begin
    if dbIso^.dbc^.GetSeq(dbIso^.dbr, preLoad) then
    begin
      dbIso^.dbr^.GetField(DBISOSpecRef, @seq, sizeof(seq));
      if dbSpec^.dbc^.GetSeq(dbSpec^.dbr, seq) then
      begin
        FillSpecimen;
        FillIsolate;
        GetResults;
        SetMode(MODE_EDIT);
        EnableButtons;
      end
      else
        ShowError(@self, IDS_ERRLODSPEC, nil, dbSpec^.dbc^.dbErrorNum, modID, 0);
    end
    else
      ShowError(@self, IDS_ERRLODISO, nil, dbSpec^.dbc^.dbErrorNum, modID, 0);
  end;
end;

procedure TIDMiCDlg.SetUpForSession;
{- set up the isolate keys based on the current session }
var
  p1, p2 : array[0..100] of char;
begin
  curSess:= GetCurrentSession(p1, sizeof(p1)-1);

  if INISpecimenListSort = 2 then
  begin
    if curSess = 0 then
      dbIso^.dbc^.SetCurKeyNum(DBISO_CDateSpecID_KEY)
    else
      dbIso^.dbc^.SetCurKeyNum(DBISO_SessionCDateSpecID_KEY);
  end
  else
  begin
    if curSess = 0 then
      dbIso^.dbc^.SetCurKeyNum(DBISO_SpecIDCDate_KEY)
    else
      dbIso^.dbc^.SetCurKeyNum(DBISO_SessionSpecIDCDate_KEY);
  end;
  if curSess <> 0 then  {- if sessions are enabled, then ensure that the "contains" }
  begin                 {- key gets setup properly in DBLib }
    tempIso^.ClearRecord;
    tempIso^.PutField(DBIsoSess, @curSess);
    dbIso^.dbc^.GetFirstContains(tempIso);
  end
  else
    dbIso^.dbc^.GetFirst(tempIso);

  if curSess = 0 then {- are sessions disabled }
    SR(IDS_SESSIONSDISABLED, p1, sizeof(p1)-1);
  if baseTitle <> nil then
  begin
    StrCopy(p2, baseTitle);
    StrCat(p2, ' --- ');
  end
  else
    StrCopy(p2, '');
  StrCat(p2, p1);
  SetWindowText(HWindow, p2);

end;

procedure TIDMicDlg.SetModuleID;
begin
  modID:= MOD_UIIDMIC;
end;

procedure TIDMICDlg.EnableButtons;
var
  specSeq   : TSeqNum;
  patSeq    : TSeqNum;
begin
  specSeq:= dbSpec^.dbr^.GetSeqValue;
  dbSpec^.dbr^.GetField(DBSpecPatRef, @patSeq, sizeof(patSeq));
  EnableWindow(GetItemHandle(IDC_UICLEAR),  not viewOnly and (preLoad = 0));
  EnableWindow(GetItemHandle(IDC_ISOLATE),  not viewOnly and (mode = MODE_EDIT) and
                                            (parent^.GetID <> MOD_UIISO));
  EnableWindow(GetItemHandle(IDC_UISAVE),   not viewOnly and (mode = MODE_EDIT));
  EnableWindow(GetItemHandle(IDC_PATIENT),  not viewOnly and (patSeq > 0));
  EnableWindow(GetItemHandle(IDC_SPECIMEN), not viewOnly and (specSeq > 0));
  EnableWindow(GetItemHandle(IDC_SESSION),  not viewOnly and (preLoad = 0));
  EnableWindow(GetItemHandle(IDC_UIPREV),   not viewOnly and (mode = MODE_EDIT) and (preLoad = 0));
  EnableWindow(GetItemHandle(IDC_UINEXT),   not viewOnly and (preLoad = 0));
  EnableWindow(GetItemHandle(IDC_EDITTEST), (not viewOnly) and (mode = MODE_EDIT));
  EnableWindow(GetItemHandle(IDC_ADDTEST), (not viewOnly) and (mode = MODE_EDIT));
  EnableWindow(GetItemHandle(IDC_RECALC),   (not viewOnly) and (mode = MODE_EDIT) and
                                            (micGrid^.GetRowCount > 0));
end;

procedure TIDMICDlg.ClearIsolate;
begin
  setFamily:= 0;
  iFld^.Clear;
  org^.Clear;
  ordr^.Clear;
  EnableIsoFlds(true);
  EnableButtons;
  fmly^.SetText('------');

  if mode = MODE_NONE then
    exit;

  dbIso^.dbr^.ClearRecord;
  origIso^.ClearRecord;
  MSDisposeObj(uiRes);
  nonMICGrid^.ClearList;
  MICGrid^.ClearList;
  results^.ClearModify;
  mode:= MODE_NONE;
end;

procedure TIDMICDlg.ClearResults;

  procedure ClearIt(res: PResRecObj); far;
  begin
    StrCopy(res^.result, '');
  end;

begin
  if mode = MODE_NONE then
    exit;

  results^.ForEach(@ClearIt);
  results^.ClearModify;
  InspectESBL;
  FillResults(True);
end;

procedure TIDMICDlg.ClearFields;
begin
  ClearIsolate;
  inherited ClearFields;
  dbSpec^.dbr^.ClearRecord;
  sFld^.Enable(true);
  cFld^.Enable(true);
  fmly^.SetText('------');
  EnableIsoFlds(false);
  sFld^.FocusFld;
  EnableButtons;
end;

function TIDMICDlg.CurrentOrganism: Integer;
var Orgnum, err: Integer;
begin
  Val(Org^.ListRec^.id, Orgnum, err);
  if err=0
     then CurrentOrganism:=Orgnum
     else CurrentOrganism:=-1;
end;

function TIDMICDlg.ReplaceOrg(NewOrg: Integer): boolean;
var NewOrgStr   : String[128];
    NewOrgPChar : array [0..128] of char;
begin
  If NewOrg<>CurrentOrganism then
  begin
    ReplaceOrg:=True;
    Str(NewOrg,NewOrgStr);
    StrPCopy(NewOrgPChar,NewOrgStr);
    Org^.SetCtlAsStr(NewOrgPChar);
  end
  else ReplaceOrg:=False;
end;

Function TIDMICDlg.InspectESBL: Boolean;
var Orgnum: integer;
    MappedOrganism: Integer;

  procedure ClearInterp(aRes: PResRecObj); far;
  var Trimmed: array [0..15] of char;
  begin
    if StrIComp(aRes^.testCatID, 'NCCLS') = 0 then
    begin
       TrimAll(Trimmed, aRes^.Result, ' ', 4);
       if (StrIComp(Trimmed,ESBLIntrStr)=0) or
          (StrIComp(Trimmed,EBLIntrStr)=0) or
          (StrIComp(Trimmed,RStarIntrStr)=0) then
          aRes^.SetResult('');
    end;
  end;

begin
  Orgnum:=CurrentOrganism;
  if Results^.ESBLChanged(Orgnum) then
  begin
    InspectESBL:=True;

    If (Orgnum<>-1)
       then RecalcESBLInterps
       else Results^.ForEach(@ClearInterp);
  end
  else InspectESBL:=False;

  if ESBLMappable(Orgnum, MappedOrganism, Results)
     then ReplaceOrg(MappedOrganism);
end;

procedure TIDMICDlg.GetResults;
begin
  WaitCursor(true);
  results^.LoadResults(dbIso^.dbr); {- load results for the isolate }
  InspectESBL;
  FillResults(True);
  WaitCursor(false);
end;

procedure TIDMICDlg.FillSpecimen;
{- assumes the specimen data is in the dbSpec^.dbr member }

  procedure GetIt(aFld: PUIFld); far;
  {- be sure to ignore isolate fields here}
  begin
    if aFld^.GetDBD = dbSpec^.dbd then
      aFld^.XferRecToCtl(dbSpec^.dbr)
    else if aFld^.GetDBD = dbPat^.dbd then
      aFld^.XFerRecToCtl(dbPat^.dbr);
  end;
var
  patSeq    : TSeqNum;
  specSeq   : TSeqNum;
begin
  WaitCursor(true);
  {- first load patient record from specimen reference }
  dbSpec^.dbr^.GetField(DBSpecPatRef, @patSeq, SizeOf(patSeq));
  if (patSeq = 0) or not dbPat^.dbc^.GetSeq(dbPat^.dbr, patSeq) then
    dbPat^.dbr^.ClearRecord;

  {- now load specimen fields }
  specSeq:= dbSpec^.dbr^.GetSeqValue;
  flds^.ForEach(@GetIt);
  sFld^.Enable(false);
  cFld^.Enable(false);
  EnableButtons;
  WaitCursor(false);
end;

procedure TIDMICDlg.FillResults(ClearValues: Boolean);
{- assumes results have been loaded in to "results" }
var
  k     : integer;
  p1, p2: array[0..100] of char;
  row   : longint;

begin
  IgnoreOrgFocus:=True;

  MSDisposeObj(uiRes);
  uiRes:= New(PGatherRes, Init(results, listObj, 0, false, true, false));

  with nonMICgrid^ do
  begin
    SetRedraw(false);
    ClearList;
    for k:= 0 to uiRes^.NumIDResults - 1 do
    begin
      uiRes^.GetIDTestName(k, p1, sizeof(p1)-1);
      row:= AddString('');
      SetData(nonMIC_Test, row, p1);
      uiRes^.GetIDResult(k, p1, sizeof(p1)-1);
      SetData(nonMIC_Result, row, p1);
    end;
  end;

  with MICgrid^ do
  begin
    SetRedraw(false);
    if ClearValues then ClearList;
    for k:= 0 to uiRes^.NumMICResults - 1 do
    begin
      if ClearValues
         then row:= AddString('')
         else row:= k;
      uiRes^.GetMICDrugName(k, p1, sizeof(p1)-1);
      SetData(MIC_Drug, row, p1);
      uiRes^.GetMICResult(k, p1, p2, sizeof(p1)-1);
      SetData(MIC_MIC, row, p1);
      SetData(MIC_Interp, row, p2);
    end;
  end;

  MICGrid^.SetRedraw(true);
  nonMICGrid^.SetRedraw(true);

  EnableButtons;

  IgnoreOrgFocus:=False;
end;

procedure TIDMICDlg.FillIsolate;
{- assumes the isolate data is in the dbIso^.dbr member }

  procedure GetIt(aFld: PUIFld); far;
  {- be sure to ignore isolate fields here}
  begin
    if aFld^.GetDBD = dbIso^.dbd then
      aFld^.XferRecToCtl(dbIso^.dbr);
  end;
var
  tmp   : word;
  fld   : PUIFld;
begin
  WaitCursor(true);
  flds^.ForEach(@GetIt);
  origIso^.CopyRecord(dbIso^.dbr);
  {- the only editable isolate field in ID/MIC is organism }
  EnableIsoFlds(false);
  if not viewOnly then
    org^.Enable(true);

  dbIso^.dbr^.GetField(DBIsoSetFamily, @setFamily, sizeof(setFamily));
  ShowSetFamily;

  EnableButtons;
  WaitCursor(false);
end;

function TIDMICDlg.DataChanged: boolean;
{- compare the current data with the originally loaded data.  If any data has
   changed, then return true }
var
  ret   : boolean;

  function IsModified(aFld: PUIFld): boolean; far;
  begin
    if aFld^.GetDBD = dbIso^.dbd then
      IsModified:= aFld^.IsModified(origIso)
    else
      IsModified:= false;
  end;

begin
  ret:= flds^.FirstThat(@IsModified) <> nil;
  if not ret then ret:= results^.IsModified;
  DataChanged:= ret;
end;

function TIDMICDlg.CheckSave(clearAfter: boolean): boolean;
var
  ret     : boolean;
  pstr    : array[0..200] of char;
  p2      : array[0..50] of char;
  k       : integer;

begin
  ret:= true;
  if mode <> MODE_NONE then
  begin
    {- first see if any data has changed }
    if DataChanged then
    begin
      k:= YesNoCancelMsg(HWindow, SR(IDS_CONFIRM, p2, 50), SR(IDS_CONFIRMSAVE, pstr, 200));
      if k = IDYES then
        ret:= SaveData(clearAfter)
      else
        ret:= k <> IDCANCEL;
    end;
  end;
  CheckSave:= ret;
end;

function TIDMICDlg.ExitAfterSave: boolean;
{- return true if dialog should exit after save button is pressed. This is
   usually true when the dialog is used as a Zoom  dialog }
begin
  ExitAfterSave:= false;
end;

function TIDMICDlg.SaveData(clearAfter: boolean): boolean;
{- save the data for the isolate and all of it's results }
var
  ret        : boolean;
  MappedOrg  : Integer;
  ScreenRes  : PScreens;
  ESBLChanged: Boolean;
  OrigESBL,
  NewESBL    : array [0..4] of char;



  function ValidateScreen: boolean;
  {- check screen for validity }
  var
    sret    : boolean;
    fld     : PUIFld;
    errNum  : integer;

    function IsInvalid(aFld: PUIFld): boolean; far;
    begin
      if aFld^.GetDBD = dbIso^.dbd then
      begin
        errNum:= aFld^.Format;
        IsInvalid:= errNum <> 0;
      end
      else
        IsInvalid:= false;
    end;

  begin
    {- first see if any fields are invalid }
    sret:= true;
    fld:= flds^.FirstThat(@IsInvalid);
    if (fld <> nil) then
    begin {- some other error }
      ShowUIFldError(@self, errNum, fld);
      sRet:= false;
    end;
    ValidateScreen:= sret;
  end;

  function StartTransaction: boolean;
  var
    errNum  : integer;
  begin
    errNum:= DBBeginTransaction;
    if errNum <> 0 then
    begin
      ShowError(@self, IDS_TRANSERROR, nil, errNum, modID, 0);
      StartTransaction:= false;
    end
    else
      StartTransaction:= true;
  end;

  function LockRecord: boolean;
  {- lock isolate record and place current content (in DB) of record in tempIso.
     if the records are not found then the user is prompted to recreate in which case
     the appropriate mode (mode, patMode) is modified accordingly }
  begin
    tempIso^.CopyRecord(origIso);
    LockRecord:= LockIt(dbIso, tempIso, IDS_ISOLOCKED, IDS_CANTLOCATEORIGISO, mode);
  end;

  function CheckModified: boolean;
  {- at this point the original record is locked. Now see if someone else has
      modified it since we originally loaded it. if so prompt for overwrite. Assumes
      the current contents of the record are placed in tempSpec/tempPat }
  var
    sret    : boolean;
    p1, p2  : array[0..200] of char;
  begin
    sret:= true;
    if not origIso^.CompareRecord(tempIso) then
      sret:= YesNoMsg(HWindow, SR(IDS_WARNING, p1, 200), SR(IDS_ISOMODIFIED, p2, 200));
    CheckModified:= sRet;
  end;

  function SaveIt: boolean;
  var
    sRet  : boolean;
    tBool : boolean;
    eNum  : integer;
    seq   : TSeqNum;
    p1    : array[0..50] of char;

    procedure XFerToRec(aFld: PUIFld); far;
    begin
      if aFld^.GetDBD = dbIso^.dbd then  {- skip over non isolate fields }
        aFld^.XFerCtlToRec(dbIso^.dbr);
    end;

  begin
    sRet:= true;

    {- first copy the original isodb contents of the record into the record that
       is going to be stored. This is because ID/MIC only allows editing of
       partial record data, therefore, I have to get the contents of the other
       (non-editable) fields into the work buffers}
    dbIso^.dbr^.CopyRecord(origIso);

    {- now transfer all (editable) fields to the record buffer }
    flds^.ForEach(@XFerToRec);

    {- update redundant specimen fields in isolate record }
    dbSpec^.dbr^.GetField(DBSpecSpecID, @p1, sizeof(p1)-1);
    dbIso^.dbr^.PutField(DBIsoSpecID, @p1);

    dbSpec^.dbr^.GetFieldAsStr(DBSpecCollectDate, p1, sizeof(p1)-1);
    dbIso^.dbr^.PutFieldAsStr(DBIsoCollectDate, p1);

    {- clear transmitted flag }
    tBool:= false;
    dbIso^.dbr^.PutField(DBISOTransFlag, @tBool);

    {- pickup the setfamily }
    dbIso^.dbr^.PutField(DBIsoSetFamily, @setFamily);


    dbIso^.dbc^.UpdateRec(dbIso^.dbr);

    if dbIso^.dbc^.dbErrorNum <> 0 then
    begin
      eNum:= 0;
      {- special error handling for common errors }
      case dbIso^.dbc^.dbErrorNum of
        MOD_BTRV + 5: eNum:= IDS_DUPISO;
        else
          eNum:= IDS_CANNOTSAVE;
      end;
      ShowError(@self, eNum, nil, dbIso^.dbc^.dbErrorNum, modID, 99);
      iFld^.FocusFld;
      sret:= false;
    end;
    SaveIt:= sret;
  end;

  procedure UnlockRecords;
  begin
    dbIso^.dbc^.UnlockRec(tempIso);
  end;

begin
  WaitCursor(true);

  {Ignore the WM_LOSTFocus for the Orgfld}
  IgnoreOrgFocus:=True;

  ret:= true;
  if ret then ret:= ValidateScreen;
  if ret then ret:= StartTransaction;
  if ret then ret:= LockRecord;
  if ret then ret:= CheckModified;

  {Remember the latest ESBLState}
  Results^.GetESBLResult(OrigESBL,4);

  {Screen this Isolate}
  ScreenRes:=New(PScreens,Init);
  ScreenIsolate(@Self, CurrentOrganism, MappedOrg,
                ExtractSet(SetFamily), Results, ScreenRes, False, True);

  {Get the Screened ESBL Result}
  Results^.GetESBLResult(NewESBL,4);
  ESBLChanged:=StrComp(NewESBL,OrigESBL)<>0;

  {Replace the Organism and recalc interps, if needed}
  if (ScreenRes^.OrganismMapped) or (ESBLChanged) then
  begin
    ReplaceOrg(MappedOrg);
    RecalcESBLInterps;
  end;

  if ret then ret:= SaveIt;
  if ret and results^.IsModified then
     results^.SaveResults;
  UnlockRecords;

  if ret then
    DBEndTransaction
  else
    DBAbortTransaction;

  {Refresh the displayed data}
  if (ScreenRes^.OrganismMapped) or (ESBLChanged) then
  begin
    FillIsolate;
    GetResults;
  end
  else
  {Just ReSynch OrigIso and TempIso}
  begin
    origIso^.CopyRecord(dbIso^.dbr);
    tempIso^.CopyRecord(origIso);
  end;
  MSDisposeObj(ScreenRes);

  {Restore the WM_LOSTFocus for the Orgfld}
  IgnoreOrgFocus:=false;

  SaveData:= ret;
  WaitCursor(false);
end;

procedure TIDMICDlg.ShowSetFamily;
var
  pstr  : array[0..100] of char;
begin
  if setFamily = 0 then
    fmly^.SetText('------')
  else
    If ExtractFamily(SetFamily)<>0 then
      fmly^.SetText(ExtractFamilyAsStr(setFamily, pstr, 100))
    Else
      fmly^.SetText(ExtractSetAsStr(setFamily, pstr, 100));
end;

procedure TIDMICDlg.EnableIsoFlds(on: boolean);

  procedure EnableIt(aFld: PUIFld); far;
  begin
    if aFld^.GetDBD = dbIso^.dbd then
    begin
      aFld^.Enable(on);
    end;
  end;

begin
  iFld^.Enable(on);
  if not viewOnly then
    org^.Enable(on)
  else
    org^.Enable(false);
  ordr^.Enable(false);
  EnableWindow(fmly^.HWindow, on);
end;

procedure TIDMICDlg.DoIsoExit(newFocID: word);
var
  specSeq   : TSeqNum;
begin
  if (mode = MODE_NONE) and not iFld^.IsEmpty then
  begin
    {- attempt to find isolate }
    specSeq:= dbSpec^.dbr^.GetSeqValue;
    tempIso^.PutField(DBIsoSpecRef, @specSeq);
    iFld^.XFerCtlToRec(tempIso);
    if listObj^.FindKey(@self, tempIso, true) then
    begin
      if CheckIsoSess(tempIso) then
      begin
        dbIso^.dbr^.CopyRecord(tempIso);
        SetMode(MODE_EDIT);
        FillIsolate;
        GetResults;
      end
      else iFld^.Clear;
      iFld^.FocusFld;
    end
    else
    begin
      ShowError(@self, IDS_ISONOTFOUND, nil, 0, modID, 0);
      iFld^.FocusFld;
    end;
  end;
end;

procedure TIDMICDlg.DoSpecCollExit(newFocID: word);
{- called when specimen/collect data lose focus }
var
  id  : word;
begin
  if (sFld^.IsEmpty or cFld^.IsEmpty) or
     (newFocID = IDC_EDSPECID) or (newFocID = IDC_EDCOLLECT) then exit;
  WaitCursor(true);
  uiFldsProcessFocus:= false;
  sFld^.XferCtlToRec(tempSpec);
  cFld^.XferCtlToRec(tempSpec);
  if listObj^.FindKey(@self, tempSpec, true) then
  begin
    {- found a specimen, load screen }
    if tempSpec^.GetSeqValue <> dbSpec^.dbr^.GetSeqValue then
    begin
      dbSpec^.dbr^.CopyRecord(tempSpec);
      FillSpecimen;
      EnableIsoFlds(true);
      iFld^.FocusFld;
    end;
  end
  else
  begin {- specimen not found (no change) }
    if listObj^.errorNum <> 0 then
      ShowError(@self, IDS_DBERROR, nil, listObj^.errorNum, modID, 0)
    else
      ShowError(@self, IDS_SPECNOTFOUND, nil, 0, modID, 0);
    sFld^.FocusFld;
  end;
  uiFldsProcessFocus:= true;
  WaitCursor(false);
end;

procedure TIDMICDlg.NextPrev(doNext: boolean);
var
  fld   : PUIFld;
  isOK  : boolean;
  seq   : TSeqNum;
begin
  if not CheckSave(true) then exit;

  if (mode <> MODE_EDIT) or (origIso^.GetSeqValue = 0) then
  begin
    tempIso^.ClearRecord;
    if curSess <> 0 then
      tempIso^.PutField(DBIsoSess, @curSess);

    if curSess <> 0 then
      isOK:= dbIso^.dbc^.GetFirstContains(tempIso)
    else
      isOK:= dbIso^.dbc^.GetFirst(tempIso);

    if isOK then
    begin
      dbIso^.dbr^.CopyRecord(tempIso);
      tempIso^.GetField(DBIsoSpecRef, @seq, sizeof(seq));
      if dbSpec^.dbc^.GetSeq(dbSpec^.dbr, seq) then
      begin
        FillSpecimen;
        dbIso^.dbr^.CopyRecord(tempIso);
        FillIsolate;
        GetResults;
        SetMode(MODE_EDIT);
      end
      else
    end
    else
      ShowError(@self, IDS_NOISOINSESS, nil, dbIso^.dbc^.dbErrorNum, modID, 0);
  end
  else
  begin
    tempIso^.CopyRecord(origIso);
    if curSess <> 0 then
    begin
      if doNext then
        isOK:= dbIso^.dbc^.GetNextContains(tempIso)
      else
        isOK:= dbIso^.dbc^.GetPrevContains(tempIso);
    end
    else
    begin
      if doNext then
        isOK:= dbIso^.dbc^.GetNext(tempIso)
      else
        isOK:= dbIso^.dbc^.GetPrev(tempIso);
    end;

    if not isOK then
    begin
      if dbIso^.dbc^.dbErrorNum = MOD_BTRV + 9 then
        ShowError(@self, IDS_DBNOMOREDATA, nil, dbIso^.dbc^.dbErrorNum, modID, 0)
      else
        ShowError(@self, IDS_DBERROR, nil, dbIso^.dbc^.dbErrorNum, modID, 1);
    end
    else
    begin
      tempIso^.GetField(DBIsoSpecRef, @seq, sizeof(seq));
      if dbSpec^.dbc^.GetSeq(dbSpec^.dbr, seq) then
      begin
        FillSpecimen;
        dbIso^.dbr^.CopyRecord(tempIso);
        FillIsolate;
        GetResults;
        SetMode(MODE_EDIT);
      end
      else
        ShowError(@self, IDS_CANTLODSPEC, nil, dbSpec^.dbc^.dbErrorNum, modID, 2);
    end;
  end;

  org^.FocusFld;
end;

function TIDMICDlg.CheckIsoSess(anIso: PDBRec): boolean;
{- given an isolate loaded into anIso, check it's session to see if it is
   in the currently loaded session, if not the prompt user to see if they wish
   to move it to the current session. If they do, then change the isolates session
   and restore it. Then return the updated record buffer.
   Return false if user does not wish to move or the move was unsuccessful,
   otherwise return true }
var
  ret   : boolean;
  seq   : TSeqNum;
  pstr  : array[0..100] of char;
  p2    : array[0..150] of char;
  row   : longint;
begin
  ret:= false;
  anIso^.GetField(DBIsoSess, @seq, sizeof(seq));
  if (curSess <> 0) and (seq <> curSess) then
  begin
    if YesNoMsg(HWindow, SR(IDS_CONFIRM, pstr, sizeof(pstr)-1), SR(IDS_ISONOTINSESS, p2, sizeof(p2)-1)) then
    begin
      seq:= anIso^.GetSeqValue;
      if dbIso^.dbc^.GetSeq(anIso, seq) then
      begin
        anIso^.PutField(DBIsoSess, @curSess); {- set the current session in the iso rec }
        if dbIso^.dbc^.UpdateRec(anIso) then
          ret:= true
        else
          ShowError(@self, IDS_DBERROR, nil, dbIso^.dbc^.dbErrorNum, modID, 0);
        if not ret then  {- at this point the isolate buffer is out of sync with the current row }
          ClearIsolate;   { so just clear everything }
      end
      else
        ShowError(@self, IDS_DBERROR, nil, dbIso^.dbc^.dbErrorNum, modID, 0);
    end;
  end
  else
    ret:= true;
  CheckIsoSess:= ret;
end;

procedure TIDMICDlg.WMFldList(var msg: TMessage);
{- called when user selects LIST in a field }
var
  id    : word;
  pstr  : array[0..100] of char;
  p2    : array[0..100] of char;
  ret   : longint;    {- return value to the UIFld that created this message }
  cont  : boolean;
  seq   : TSeqNum;
begin
  ret:= -1;   {- tell UI field passing this message NOT to list (non-zero) }

  uiFldsProcessFocus:= false;   {- turn off field focus processing }

  id:= msg.wParam;
  if (id = IDC_EDSPECID) or (id = IDC_EDCOLLECT) then
  begin
    sFld^.XferCtlToRec(tempSpec);
    cFld^.XferCtlToRec(tempSpec);
    if listObj^.SelectList(@self, tempSpec) then
    begin
      dbSpec^.dbr^.CopyRecord(tempSpec);
      FillSpecimen;
      EnableIsoFlds(true);
      iFld^.FocusFld;
    end
    else if listObj^.errorNum <> 0 then
    begin
      if listObj^.errorNum = LISTERR_PARTIALKEYNOTFOUND then
        ShowError(@self, IDS_CANNOTLISTNOMATCH, nil, listObj^.errorNum, modID, 0)
      else
        ShowError(@self, IDS_CANNOTLISTBAD, nil, listObj^.errorNum, modID, 0);
    end;
  end
  else if id = IDC_EDISO then
  begin
    if mode <> MODE_NONE then
      cont:= CheckSave(false)
    else
      cont:= true;
    if cont then
    begin
      tempIso^.ClearRecord;
      seq:= dbSpec^.dbr^.GetSeqValue;
      tempIso^.PutField(DBIsoSpecRef, @seq);
      if listObj^.SelectList(@self, tempIso) then
      begin
        if CheckIsoSess(tempIso) then
        begin
          dbIso^.dbr^.CopyRecord(tempIso);
          SetMode(MODE_EDIT);
          FillIsolate;
          GetResults;
        end;
      end
      else if listObj^.errorNum <> 0 then
      begin
        if listObj^.errorNum = LISTERR_PARTIALKEYNOTFOUND then
          ShowError(@self, IDS_CANNOTLISTNOMATCH, nil, listObj^.errorNum, modID, 0)
        else
          ShowError(@self, IDS_CANNOTLISTBAD, nil, listObj^.errorNum, modID, 0);
      end;
    end;
  end
  else
    ret:= 0;    {- this routine is not handling the list. Let the UIfld handle it }

  uiFldsProcessFocus:= true;  {- turn focus processing back on }

  SetWindowLong(HWindow, DWL_MSGRESULT, ret);  {- set message result (for dialogs) }
end;

procedure TIDMICDlg.WMLostFocus(var msg: TMessage);
var
  fld     : PUIFld;
  err     : integer;
begin
  fld:= PUIFld(msg.lParam); {- get field pointer to fld losing focus }
  {- first see if the field is valid }
  err:= fld^.IsValid;
  if (err <> 0) and (err <> UIErrRequired) then   {- ignore required fields on field exit }
    ShowUIFldError(@self, err, fld)
  else
  begin
    if (fld^.GetID = IDC_EDSPECID) or (fld^.GetID = IDC_EDCOLLECT) then
      DoSpecCollExit(msg.wParam)
    else if (fld^.GetID = IDC_EDISO) then
         DoIsoExit(msg.wParam)
         else if (fld=org) and (not IgnoreOrgFocus) then
              if InspectESBL then FillResults(False);
  end;
  SetWindowLong(HWindow, DWL_MSGRESULT, 0);  {- set message result (for dialogs) }
end;

procedure TIDMICDlg.IDIsolate(var msg: TMessage);
var
  seq   : TSeqNum;
  p1,p2 : array[0..100] of char;
  temp  : TSeqNum;
begin
  temp:= preLoad; {- save preload and then set it to zero so ID/MIC will not }
  preLoad:= 0;    {  exit after saving }
  if dbIso^.dbr^.GetSeqValue > 0 then
  begin
    if CheckSave(false) then
    begin
      UIIsoEntry(@self, 0, dbIso^.dbr^.GetSeqValue);
      dbIso^.dbc^.GetSeq(dbIso^.dbr, dbIso^.dbr^.GetSeqValue);
      dbIso^.dbr^.GetField(DBIsoSess, @seq, sizeof(seq));
      if (curSess <> 0) and (seq <> curSess) then
      begin
        InfoMsg(hWindow, SR(IDS_INFORMATION, p1, sizeof(p1)-1), SR(IDS_ISOMOVED, p2, sizeof(p2)-1));
        ClearIsolate;
        iFld^.FocusFld;
      end
      else
      begin
        dbSpec^.dbc^.GetSeq(dbSpec^.dbr, dbSpec^.dbr^.GetSeqValue);
        FillSpecimen; {- ensure any specimen changes are reload too }
        FillIsolate;
        GetResults;
      end;
    end;
  end;
  preLoad:= temp;
end;

procedure TIDMICDlg.IDClear(var msg: TMessage);
var
  k   : integer;
begin
  uiFldsProcessFocus:= false;
  k:= application^.ExecDialog(New(PClearDlg, Init(@self, MakeIntResource(DLG_CLEAR))));
  if k <> IDCANCEL then
  begin
    if CheckSave(false) then
    begin
      case k of
        IDC_CLEARISO:
          begin
            ClearIsolate;
            iFld^.FocusFld;
          end;
        IDC_CLEARALL:
          ClearFields;
        IDC_CLEARRESULTS:
          ClearResults;
      end;
    end;
  end;
  uiFldsProcessFocus:= true;
end;

procedure TIDMICDlg.IDSession(var msg: Tmessage);
var
  tSess   : TSeqNum;
  pstr    : array[0..100] of char;
begin
  if CheckSave(true) then
  begin
    tSess:= curSess;
    listObj^.EditableMnemList(@self, DBSessFile, '');
    ClearFields;
    curSess:= GetCurrentSession(pstr, sizeof(pstr)-1);
    if curSess <> tSess then
      SetupForSession;
  end;
end;

procedure TIDMICDlg.IDSpecimen(var msg: TMessage);
var
  seq   : TSeqNum;
begin
  seq:= dbSpec^.dbr^.GetSeqValue;
  if seq > 0 then
  begin
    UISpecimenEntry(@self, seq);
    {- reload record }
    if dbSpec^.dbc^.GetSeq(dbSpec^.dbr, seq) then
    begin
      FillSpecimen;
    end
    else
    begin
      ShowError(@self, IDS_ERRLODSPEC, nil, dbSpec^.dbc^.dbErrorNum, modID, 0);
      ClearFields;
    end;
  end;
end;

procedure TIDMICDlg.IDPatient(var msg: TMessage);
var
  seq   : TSeqNum;
begin
  seq:= dbPat^.dbr^.GetSeqValue;
  if seq > 0 then
  begin
    UIPatientEntry(@self, seq);
    FillSpecimen;  {- fillspecimen reloads patient as well }
  end;
end;

procedure TIDMICDlg.EditTest;
var
  k       : longint;
  OrigESBL: array [0..4] of char;
  NewESBL : array [0..4] of char;

  Procedure MonitorESBLResult;
  begin
    Results^.GetESBLResult(NewESBL,4);
    if (StrComp(OrigESBL,NewESBL)<>0) then
    begin
      TrimAll(NewESBL,NewESBL,' ',4);
      if (StrComp(NewESBL,'')=0) then
          Results^.SetESBLResult('?',True);
      if not InspectESBL then RecalcESBLInterps;
      FillResults(False);
    end;
  end;

begin
  if not IsWindowEnabled(GetItemHandle(IDC_EDITTEST)) then exit;

  uiFldsProcessFocus:= false;
  k:= MICGrid^.GetSelIndex;
  if k >= 0 then  {- edit MIC results }
  begin
    EditMICResult(@self, uiRes, k, MICGrid, MIC_MIC, MIC_Interp);

    if Results^.IsModified then
    begin
      if not InspectESBL then
         if (Results^.ESBLResultExists) and
            (Results^.GetESBLResult(NewESBL,4)) and
            (NewESBL[0] in ['+','?']) then
            RecalcESBLInterps;
      FillResults(False);
    end;

    FocusCtl(hWindow, IDC_MICGRID);
  end
  else
  begin
    k:= nonMICGrid^.GetSelIndex;
    if k >= 0 then
    begin
      Results^.GetESBLResult(OrigESBL,4);
      EditIDResult(@self, uiRes, k, nonMICGrid, nonMIC_Result);
      MonitorESBLResult;
      FocusCtl(hWindow, IDC_NONMICGRID);
    end
    else
      ShowError(@self, IDS_NORESSELECTED, nil, 0, modID, 0);
  end;
  uiFldsProcessFocus:= true;
end;

procedure TIDMICDlg.AddTest;
var AddDrugsDlg: PAddDrugDlg;
    TestSeq    : TSeqNum;
    testDB     : PDBFile;

    MICResObj,
    NCCLSResObj: PResRecObj;

    MICCat,
    CurrResStr,
    CaptionStr,
    MsgStr     : Array[0..64] of char;

begin
  AddDrugsDlg:=New(PAddDrugDlg, Init(@Self,TestSeq));
  if AddDrugsDlg<>Nil then
  if Application^.ExecDialog(AddDrugsDlg)=IDOK then
  if TestSeq>0 then
  begin
    {Make an Additional Drug NCCLS PResRecObjs}
    NCCLSResObj:=Results^.BuildAddResObj(TestSeq,dbIso^.dbr^.GetSeqValue);

    {Insert this Addtl Drug NCCLS, if not already in the results list}
    if not Results^.GetResult(NCCLSResObj^.TestID, NCCLSResObj^.TestCatID, CurrResStr,64) then
    begin
      Results^.Insert(NCCLSResObj);

      {If there's a matching MIC entry add it}
      StrCopy(MICCat,'MIC');
      testDB:= New(PDBFile, Init(DBTSTFile, '', dbOpenNormal));
      testDB^.dbr^.ClearRecord;
      testDB^.dbr^.PutField(DBTSTID, @NCCLSResObj^.TestID);
      testDB^.dbr^.PutField(DBTSTTstCat, @MICCat);
      if not Results^.GetResult(NCCLSResObj^.TestID, MICCat, CurrResStr,64) then
      if testDB^.dbc^.GetEQ(testDB^.dbr) then
      begin
        MICResObj:=Results^.BuildAddResObj(testDB^.dbr^.GetSeqValue,dbIso^.dbr^.GetSeqValue);
        Results^.Insert(MICResObj);
      end;
      Dispose(testDB,Done);

      {Refresh the grid}
      FillResults(True);
    end
    else
    begin
      {Already in the results list, display error, dispose object}
      MessageBeep(mb_iconStop);
      MessageBox(hWindow,SR(IDS_ADDFailed, MsgStr, 64),
      StrCat(StrCat(SR(IDS_Drug, CaptionStr, 64),': '),NCCLSResObj^.TestName),mb_iconStop);
      Dispose(NCCLSResObj,Done);
    end;
  end;
end;

procedure TIDMICDlg.IDEditTest(var msg: TMessage);
begin
  EditTest;
end;

procedure TIDMICDlg.IDAddTest(var msg: TMessage);
begin
  AddTest;
end;

Procedure TIDMICDlg.RecalcESBLInterps;
var TestIdx,
    OrgNum,
    Idx, RDN: Integer;
    seq      : TSeqNum;
    rec      : PDBRec;
    interp   : PInterp;
    isUrine  : Boolean;
    TestGroup: array[0..255] of char;
    Res      : PResRecObj;
    ESBLInfo : ESBLRecord;

begin
  WaitCursor(true);

  Val(org^.listRec^.id, orgNum, Idx);
  isUrine:= false;
  dbSpec^.dbr^.GetField(DBSpecSrc, @seq, sizeof(seq));
  if seq <> 0 then
  begin
    rec:= listObj^.PeekSeq(DBSRCFile, seq);
    if rec <> nil then
      rec^.GetField(DBSRCUrine, @isUrine, sizeof(isUrine));
  end;

  StrCopy(TestGroup, '');
  While Results^.NextTestGroup(TestGroup, Idx) do
  begin
    {- if the test group is not user defined nor and "additional"}
    Res:= Results^.At(Idx);
    if not ( res^.tgUD or (StrComp(res^.testgroupID,NullTestGroupID)=0)) then
    begin
      Results^.TraysObj^.LoadTray(TestGroup);  {- make sure the tray gets loaded }
      if ESBLMics(Results^.TraysObj^.tfsRec.TrayNum, ESBLInfo) then
      begin
        interp:= New(PInterp, Init(orgNum, results));
        for TestIdx:=Idx to Idx + results^.NumTests(TestGroup, Idx) - 1 do
        begin
          Res:= Results^.At(TestIdx);
          if (StrComp(res^.testCatID, 'MIC') = 0) then
              interp^.SetupInterps(res^.testID, res^.result);
        end;
        interp^.CalcESBLInterps(isUrine);
        MSDisposeObj(interp);
      end;
    end;
  end;

  WaitCursor(false);
end;

procedure TIDMICDlg.RecalcInterps;
var
  tstGrp    : array[0..100] of char;
  idx       : integer;
  count     : integer;
  interp    : PInterp;
  k         : integer;
  orgNum    : integer;
  res       : PResRecObj;
  seq       : TSeqNum;
  isUrine   : boolean;
  rec       : PDBRec;
begin
    WaitCursor(true);

    k:= org^.Format;
    if k <> 0 then
    begin
      ShowUIFldError(@self, k, org);
      exit;
    end;

    Val(org^.listRec^.id, orgNum, k);
    if (k <> 0) or (orgNum = 0) then
    begin
      ShowError(@self, IDS_NOORG, nil, 0, modID, 0);
      org^.FocusFld;
      exit;
    end;

    isUrine:= false;
    dbSpec^.dbr^.GetField(DBSpecSrc, @seq, sizeof(seq));
    if seq <> 0 then
    begin
      rec:= listObj^.PeekSeq(DBSRCFile, seq);
      if rec <> nil then
        rec^.GetField(DBSRCUrine, @isUrine, sizeof(isUrine));
    end;

    strcopy(tstGrp, '');
    while results^.NextTestGroup(tstGrp, idx) do
    begin
      {- if the test group is not user defined nor and "additional"}
      res:= results^.At(idx);
      if not ( res^.tgUD or (StrComp(res^.testgroupID,NullTestGroupID)=0)) then
      begin
        results^.traysObj^.LoadTray(tstGrp);  {- make sure the tray gets loaded }
        interp:= New(PInterp, Init(orgNum, results));
        count:= results^.NumTests(tstGrp, idx);
        for k:= idx to idx + count - 1 do
        begin
          res:= results^.At(k);
          if (StrComp(res^.testCatID, 'MIC') = 0) then
            interp^.SetupInterps(res^.testID, res^.result);
        end;
        interp^.CalcInterps(isUrine);
        MSDisposeObj(interp);
      end;
    end;
    WaitCursor(false);
end;

procedure TIDMICDlg.IDRecalc(var msg: TMessage);
var
  p1 : array[0..100] of char;
  p2 : array[0..50] of char;
begin
  if YesNoMsg(hWindow, SR(IDS_CONFIRM, p2, sizeof(p2)-1),
                       SR(IDS_DORECALC, p1, sizeof(p1)-1)) then
  begin
    RecalcInterps;
    FillResults(True);
  end;
end;

procedure TIDMICDlg.WMCharToItem(var msg: TMessage);
begin
  nonMICGrid^.CharToItem(msg);
  MICGrid^.CharToItem(msg);
end;

procedure TIDMICDlg.WMDrawItem(var msg: TMessage);
begin
  nonMICgrid^.DrawItem(msg);
  MICgrid^.DrawItem(msg);
end;

procedure TIDMICDlg.WMMeasureItem(var msg: TMessage);
begin
  nonMICgrid^.MeasureItem(msg);
  MICgrid^.MeasureItem(msg);
end;

procedure TIDMICDlg.WMGridGotFoc(var msg: TMessage);
var
  newID   : integer;
  grid    : PGrid;
begin
  grid:= PGrid(msg.lParam);
  newID:= msg.wParam;
  if (newID = IDC_MICGRID) then
  begin
    nonMICGrid^.SetSelIndex(-1);
  end
  else if newID = IDC_NONMICGRID then
  begin
    MICgrid^.SetSelIndex(-1);
  end;
end;

procedure TIDMICDlg.WMCommand(var msg: TMessage);
begin
  inherited WMCommand(msg);
  if msg.lParamLo <> 0 then   {- message is from a control }
  begin
    if msg.lParamHI = LBN_DBLCLK then
      EditTest;
  end;
end;

procedure UIIDMICEntry(aParent: PWindowsObject; anIsoSeq: TSeqNum; viewOnly: boolean);
begin
  application^.ExecDialog(New(PIDMicDlg, Init(aParent, anIsoSeq, viewOnly)));
end;

{- rcf}


{-----------------------------------------------------------[ TAddDrugDlg ]--}
constructor TAddDrugDlg.Init(aParent: PWindowsObject; var xSelSeq: TSeqNum);
begin
  inherited Init(aParent, MakeIntResource(DLG_ADDDRUGS));

  SelSeq:=@xSelSeq;

  AddDrugsGrid:=Nil;
  AddDrugsGrid := New(PGrid, InitResource(@self, IDC_ADDDRUGLIST, 2, FALSE));
end;

destructor TAddDrugDlg.Done;
begin
  MSDisposeObj(AddDrugsGrid);
  Inherited Done;
end;

procedure TAddDrugDlg.SetupWindow;
begin
  Inherited SetUpWindow;
  FillListBox;
end;

procedure TAddDrugDlg.FillListBox;
var pStr  : array[0..100] of char;
    pID,
    pCat,
    pDesc  : array[0..100] of char;
    DrugSeq: TSeqNum;
    Row    : Integer;
    testDB : PDBFile;
    drugDB : PDBFile;
begin
  AddDrugsGrid^.SetHeader(0, SR(IDS_TEST, pStr, sizeof(pStr)-1));
  AddDrugsGrid^.SetHeader(1, SR(IDS_ADDDesc, pStr, sizeof(pStr)-1));
  AddDrugsGrid^.SetAvgCharWidth(0, 12);
  AddDrugsGrid^.StretchColumn(1);

  drugDB:= New(PDBFile, Init(DBDrugFile, '', dbOpenNormal));
  testDB:=New(PDBFile, Init(DBTSTFile, '', dbOpenNormal));
  testDB^.dbr^.ClearRecord;
  if testDB^.dbc^.GetFirst(testDB^.dbr) then
  begin
    repeat
      testDB^.dbr^.GetFieldAsStr(DBTSTTstCat, pCat, 100);
      if (StrComp(pCat,'NCCLS')=0) then
      begin
        testDB^.dbr^.GetField(DBTSTDrugRef, @DrugSeq, SizeOf(DrugSeq));
        drugDB^.dbr^.ClearRecord;
        drugDB^.dbc^.GetSeq(drugDB^.dbr, DrugSeq);
        drugDB^.dbr^.GetFieldAsStr(DBDrugAltAbbr, pID, 100);
        drugDB^.dbr^.GetFieldAsStr(DBDrugName, pDesc, 64);

        Row := AddDrugsGrid^.AddString(pID);
        AddDrugsGrid^.SetData(0, Row, pID);
        AddDrugsGrid^.SetData(1, Row, pDesc);
        AddDrugsGrid^.SetItemData(Row, testDB^.dbr^.GetSeqValue);
      end;
    until not testDB^.dbc^.GetNext(testDB^.dbr);
  end;

  MSDisposeObj(testDB);
  MSDisposeObj(drugDB);

  AddDrugsGrid^.SetRedraw(TRUE);
  AddDrugsGrid^.SetSelIndex(0);
end;

function TAddDrugDlg.GetSelSeq: TSeqNum;
var Sel: Integer;
begin
  Sel := AddDrugsGrid^.GetSelIndex;
  if sel <0
     then GetSelSeq:=-1
     else GetSelSeq:=AddDrugsGrid^.GetItemData(sel);
end;

procedure TAddDrugDlg.OK(var msg: TMessage);
begin
  SelSeq^:=GetSelSeq;
  Inherited OK(msg);
end;

procedure TAddDrugDlg.CANCEL(var msg: TMessage);
begin
  SelSeq^:=-1;
  Inherited Cancel(msg);
end;

procedure TAddDrugDlg.List(var msg: TMessage);
begin
  if msg.lParamHI = LBN_DBLCLK
     then  TAddDrugDlg.OK(msg);
  DefWndProc(msg);
end;

procedure TAddDrugDlg.DrawItem(var msg: TMessage);
begin
  AddDrugsGrid^.DrawItem(msg);
end;

procedure TAddDrugDlg.MeasureItem(var msg: TMessage);
begin
  AddDrugsGrid^.MeasureItem(msg);
end;

procedure TAddDrugDlg.CharToItem(var msg: TMessage);
begin
end;


END.

