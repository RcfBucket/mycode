unit UIIso;

INTERFACE

uses
  OWindows,
  DBTypes;

procedure UIIsoEntry(aParent: PWindowsObject; aSpecSeq, aIsoSeq: TSeqNum);

{- Display a gram stain result screen.
   aSpecSeq  : 0 = generic gs results screen (user loads the specimen)
              >0 = Specimen seq number to preload }

IMPLEMENTATION

uses
  ApiTools,
  CtlLib,
  DBFile,
  DBIDS,
  DBLib,
  DMSErr,
  DMString,
  ESBL,
  BarCodes,
  Trays,
  AS4RDR,
  HWConfig,
  DlgLib,
  IniLib,
  Interps,
  ListLib,
  MScan,
  ResLib,
  ODialogs,
  Screens,
  Strings,
  StrSw,
  Sessions,
  UserMsgs,
  UICommon,
  UIFlds,
  UISpec,
  UIPat,
  UIIDMic,
  Win31,
  WinProcs,
  WinTypes;

{$I UICOMMON.INC}
{$I UIISO.INC}
{$R UIISO.RES}

type
  PUIIsoDlg = ^TUIIsoDlg;
  TUIIsoDlg = object(TUICommonDlg)
    dbSpec        : PDBFile;
    dbPat         : PDBFile;
    dbIso         : PDBFile;
    tempSpec      : PDBRec;
    tempIso       : PDBRec;
    origIso       : PDBRec;
    sFld, cFld, pFld, iFld, ordr, org: PUIFld;
    fmly          : PStatic;
    sessTxt       : PStatic;
    preLoadIso    : TSeqNum;
    specSeq       : TSeqNum;
    patSeq        : TSeqNum;
    setFamily     : word;     {- Gram Pos, Neg, Anaerobe, Strep, Staph, fermenter, Non-ferm, etc. }
    origSetFamily : word;     {- Copy of SetFamily before being modified }
    resultsExist  : boolean;
    results       : PResults;
    ManualSetFam  : boolean;
    ExtraTst      : integer;  {- indole. oxidase, betaHem (depending on family) }
    OrigExtraTst  : integer;  {- Copy of ExtraTst before being modified }
    OrigESBL      : array[0..4] of char;
    {}
    constructor Init(aParent: PWindowsObject; aSpecSeq, aIsoSeq: TSeqNum);
    destructor Done; virtual;
    procedure SetupWindow; virtual;
    procedure SetModuleID; virtual;
    procedure DoSpecCollExit(newFocID: word);
    procedure DoIsoExit(newFocID: word);
    procedure FillSpecimen;
    procedure FillIsolate;
    procedure GetResults;
    procedure ClearFields; virtual;
    function DataChanged: boolean; virtual;
    function CheckSave(clearAfter: boolean): boolean; virtual;
    function SaveData(clearAfter: boolean): boolean; virtual;
    procedure DeleteData; virtual;

    procedure ClearIsolate;
    procedure EnableIsoFlds(on: boolean);
    procedure WMLostFocus(var msg: TMessage);    virtual WM_FIRST + WM_LOSTFOCUS;
    procedure WMFldList(var msg: TMessage);      virtual WM_FIRST + WM_FLDLIST;
    procedure WMAutoClear(var msg: TMessage);    virtual WM_FIRST + WM_AUTOCLEARED;
    procedure WMMnemSelected(var msg: TMessage); virtual WM_FIRST + WM_MNEMSELECTED;
    procedure WMCtlColor(var msg: TMessage);     virtual WM_FIRST + WM_CTLCOLOR;
    procedure IDClear(var msg: TMessage);        virtual ID_FIRST + IDC_UICLEAR;
    procedure IDPatient(var msg: TMessage);      virtual ID_FIRST + IDC_PATIENT;
    procedure IDSpecimen(var msg: TMessage);     virtual ID_FIRST + IDC_SPECIMEN;
    procedure IDSession(var msg: TMessage);      virtual ID_FIRST + IDC_SESSION;
    procedure IDMove(var msg: TMessage);         virtual ID_FIRST + IDC_MOVE;
    procedure IDMIC(var msg: TMessage);          virtual ID_FIRST + IDC_IDMIC;
    procedure IDAS4(var msg: TMessage);          virtual ID_FIRST + IDC_AS4;
    procedure IDBarCodes(var msg: TMessage);     virtual ID_FIRST + IDC_BARCODES;
    procedure IDFamily;                          virtual ID_FIRST + IDC_Family;
    procedure EnableButtons; virtual;
    procedure GetSetFromOrder;
    procedure ShowSetFamily;
    procedure ShowIsoSession;
    function  GetFamily(var aTest: integer): boolean;

    procedure RecalcInterps;
    procedure RecalcESBLInterps;
    function  InspectESBL: Boolean;
    function  ReplaceOrg(NewOrg: Integer): boolean;
    function  CurrentOrganism: Integer;
  end;

  PClearDlg = ^TClearDlg;
  TClearDlg = object(TCenterDlg)
    procedure IDAll(var msg: TMessage); virtual ID_FIRST + IDC_CLEARALL;
    procedure IDClearIso(var msg: TMessage); virtual ID_FIRST + IDC_CLEARISO;
  end;

procedure TClearDlg.IDAll(var msg: TMessage);
begin
  EndDlg(IDC_CLEARALL);
end;

procedure TClearDlg.IDClearIso(var msg: TMessage);
begin
  EndDlg(IDC_CLEARISO);
end;

constructor TUIIsoDlg.Init(aParent: PWindowsObject; aSpecSeq, aIsoSeq: TSeqNum);
{- aSpecSeq is not required if aIsoSeq is specified. aIsoSeq specifies an iso
   to preload. aSpecSeq specifies a specimen to preload }
var
  fNum    : integer;
  k       : integer;
  subst   : PErrSubst;
  fi      : TFldInfo;
  c       : PControl;
begin
  if not inherited Init(aParent, MakeIntResource(DLG_UIISO), aSpecSeq) then
    fail;

  preLoadIso:= aIsoSeq;
  dbSpec:= nil;
  dbPat:= nil;
  dbIso:= nil;
  tempSpec:= nil;
  tempIso:= nil;
  origIso:= nil;
  results:= nil;
  specSeq:= 0;
  patSeq:= 0;
  setFamily:= 0;
  origSetFamily:= 0;
  StrCopy(OrigESBL,'');
  resultsExist:= false;
  ManualSetFam:= false;
  ExtraTst := 0;
  OrigExtraTst := 0;

  dbSpec:= New(PDBFile, Init(DBSpecFile, '', dbOpenNormal));
  if dbSpec = nil then
  begin
    subst:= New(PErrSubst, Init(DBSpecFile));
    DoFail(IDS_ERROROPEN, dbLastOpenError, subst);
    MSDisposeObj(subst);
    Fail;
  end;
  {- get spec file in proper sort order }
  if INISpecimenListSort = 2 then
    dbSpec^.dbc^.SetCurKeyNum(DBSPEC_DATE_KEY)
  else
    dbSpec^.dbc^.SetCurKeyNum(DBSPEC_ID_KEY);
  dbSpec^.dbc^.GetFirst(dbSpec^.dbr);

  dbPat:= New(PDBFile, Init(DBPatFile, '', dbOpenNormal));
  if dbPat = nil then
  begin
    subst:= New(PErrSubst, Init(DBPatFile));
    DoFail(IDS_ERROROPEN, dbLastOpenError, subst);
    MSDisposeObj(subst);
    Fail;
  end;

  dbIso:= New(PDBFile, Init(DBIsoFile, '', dbOpenNormal));
  if dbIso = nil then
  begin
    subst:= New(PErrSubst, Init(DBIsoFile));
    DoFail(IDS_ERROROPEN, dbLastOpenError, subst);
    MSDisposeObj(subst);
    Fail;
  end;

  tempSpec:= New(PDBRec, Init(dbSpec^.dbc));
  if tempSpec = nil then
  begin
    DoFail(IDS_CANTALLOCREC, 0, nil);
    Fail;
  end;
  tempIso:= New(PDBRec, Init(dbIso^.dbc));
  if tempIso = nil then
  begin
    DoFail(IDS_CANTALLOCREC, 0, nil);
    Fail;
  end;
  origIso:= New(PDBRec, Init(dbIso^.dbc));
  if origIso = nil then
  begin
    DoFail(IDS_CANTALLOCREC, 0, nil);
    Fail;
  end;

  results:= New(PResults, Init);
  if results = nil then
  begin
    DoFail(IDS_CANTINITRESULTS, 0, nil);
    Fail;
  end;

  InitModeText(IDC_MODE);
  sessTxt:= New(PStatic, InitResource(@self, IDC_ISOSESSION, 0));
  c:= New(PLStatic, InitResource(@self, IDC_LBLFAMILY, 0, false));
  fmly:= New(PStatic, InitResource(@self, IDC_FAMILYTXT, 0));

  sFld:= LinkUIFld(DBSpecSpecID,      false, false, 0, IDC_EDSPECID,   IDC_LBLSPECID,   dbSpec^.dbd);
  cFld:= LinkUIFld(DBSpecCollectDate, false, false, 0, IDC_EDCOLLECT,  IDC_LBLCOLLECT,  dbSpec^.dbd);
         LinkUIFld(DBSpecSrc,         false, false, 0, IDC_SOURCE,     IDC_LBLSOURCE,   dbSpec^.dbd);
         LinkUIFld(DBSpecWard,        false, false, 0, IDC_WARD,       IDC_LBLWARD,     dbSpec^.dbd);
  pFld:= LinkUIFld(DBPatPatID,        false, false, 0, IDC_PATID,      IDC_LBLPATID,    dbPat^.dbd);
         LinkUIFld(DBPatLName,        false, false, 0, IDC_LAST,       IDC_LBLLAST,     dbPat^.dbd);
         LinkUIFld(DBPatFName,        false, false, 0, IDC_FIRST,      IDC_LBLFIRST,    dbPat^.dbd);

  iFld:= LinkUIFld(DBIsoIso,          true,  false, 0, IDC_EDISO,      IDC_LBLISOID,    dbIso^.dbd);
  ordr:= LinkUIFld(DBIsoOrdRef,       false, false, 0, IDC_EDORDER,    IDC_LBLORDER,    dbIso^.dbd);
  org:=  LinkUIFld(DBIsoOrgRef,       false, false, 0, IDC_EDORG,      IDC_LBLORG,      dbIso^.dbd);
         LinkUIFld(DBIsoTstDate,      false, false, 0, IDC_EDTESTDATE, IDC_LBLTESTDATE, dbIso^.dbd);
         LinkUIFld(DBIsoTransFlag,    false, false, UICheckType, IDC_EDXMIT, 0,  dbIso^.dbd);
         LinkUIFld(DBIsoQCFlag,       false, false, UICheckType, IDC_EDQC,   0,  dbIso^.dbd);
         LinkUIFld(DBIsoFreeText,     false, false, 0, IDC_EDFREETEXT, IDC_LBLFREETEXT, dbIso^.dbd);

  {- now link all user defined fields }
  k:= IDC_EDUSER1;
  for fNum:= DBISOUDBase to dbIso^.dbd^.NumFields do
  begin
    dbIso^.dbd^.FieldInfo(fNum, fi);
    if ((fi.userFlag and DB_UD) <> 0) then
    begin
      LinkUIFld(fNum, false, false, 0, k, k+100, dbIso^.dbd);
      Inc(k);
    end;
  end;
end;

destructor TUIIsoDlg.Done;
begin
  inherited Done;
  MSDisposeObj(dbSpec);
  MSDisposeObj(dbPat);
  MSDisposeObj(dbIso);
  MSDisposeObj(tempSpec);
  MSDisposeObj(tempIso);
  MSDisposeObj(origIso);
  MSDisposeObj(results);
end;

procedure TUIIsoDlg.SetupWindow;
var
  fi    : TFldInfo;
  p1, p2: array[0..100] of char;
  seq   : TSeqNum;

  procedure DisableIt(aFld: PUIFld); far;
  begin
    if (preLoad > 0) or ((aFld <> sFld) and (aFld <> cFld)) then
      aFld^.Enable(False);
  end;

begin
  inherited SetupWindow;

  flds^.ForEach(@DisableIt);
  ShowIsoSession;

  if preLoadIso > 0 then
  begin
    if dbIso^.dbc^.GetSeq(dbIso^.dbr, preLoadIso) then
    begin
      dbIso^.dbr^.GetField(DBISOSpecRef, @preLoad, sizeof(preLoad));
      if dbSpec^.dbc^.GetSeq(dbSpec^.dbr, preload) then
      begin
        FillSpecimen;
        SetMode(MODE_EDIT);
        FillIsolate;
        GetResults;
      end
      else
        ShowError(@self, IDS_ERRLODSPEC, nil, dbSpec^.dbc^.dbErrorNum, modID, 0);
    end
    else
      ShowError(@self, IDS_ERRLODISO, nil, dbSpec^.dbc^.dbErrorNum, modID, 0);
  end
  else if preLoad > 0 then
  begin
    if dbSpec^.dbc^.GetSeq(dbSpec^.dbr, preLoad) then
      FillSpecimen
    else
      ShowError(@self, IDS_ERRLODSPEC, nil, dbSpec^.dbc^.dbErrorNum, modID, 0);
  end;
  EnableButtons;
end;

procedure TUIIsoDlg.EnableButtons;
begin
  specSeq:= dbSpec^.dbr^.GetSeqValue;
  dbSpec^.dbr^.GetField(DBSpecPatRef, @patSeq, sizeof(patSeq));
  EnableWindow(GetItemHandle(IDC_UICLEAR),  (specSeq > 0) and (preLoadIso = 0));
  EnableWindow(GetItemHandle(IDC_UIDELETE), (mode = MODE_EDIT) and (preLoadIso = 0));
  EnableWindow(GetItemHandle(IDC_IDMIC),    (mode = MODE_EDIT) and
                                            (not ordr^.IsEmpty) and
                                            (parent^.GetID <> MOD_UIIDMIC));
  EnableWindow(GetItemHandle(IDC_MOVE),     (mode = MODE_EDIT));
  EnableWindow(GetItemHandle(IDC_UISAVE),    mode > MODE_NONE);
  EnableWindow(GetItemHandle(IDC_PATIENT),   patSeq > 0);
  EnableWindow(GetItemHandle(IDC_SPECIMEN),  specSeq > 0);
  EnableWindow(GetItemHandle(IDC_SESSION),   preLoadIso = 0);
  EnableWindow(GetItemHandle(IDC_AS4),       HWExists(pkAS4));

  EnableWindow(GetItemHandle(IDC_Family),   not ((SetFamily=0) or (results^.IDResultsExist)) );
end;

procedure TUIIsoDlg.ClearIsolate;

  procedure ClearIsoFld(aFld: PUIFld); far;
  begin
    if aFld^.GetDBD = dbIso^.dbd then
      aFld^.Clear;
  end;
var
  fld   : PUIFld;
begin
  flds^.ForEach(@ClearIsoFld);
  dbIso^.dbr^.ClearRecord;
  origIso^.ClearRecord;
  iFld^.FocusFld;
  setFamily:= 0;
  origSetFamily:= 0;
  StrCopy(OrigESBL,'');
  ExtraTst:=0;
  OrigExtraTst:=0;
  fmly^.SetText('------');
  resultsExist:= false;
  ManualSetFam:= false;
  {- be sure to enable order (it may have been disabled previously) }
  ordr^.Enable(true);

  EnableButtons;
  SetMode(MODE_NONE);
  ShowIsoSession;
end;

procedure TUIIsoDlg.ClearFields;
begin
  inherited ClearFields;
  dbSpec^.dbr^.ClearRecord;
  dbIso^.dbr^.ClearRecord;
  origIso^.ClearRecord;
  specSeq:= 0;
  patSeq:= 0;
  setFamily:= 0;
  origSetFamily:= 0;
  StrCopy(OrigESBL,'');
  ExtraTst:=0;
  OrigExtraTst:=0;
  ManualSetFam:=False;
  sFld^.Enable(true);
  cFld^.Enable(true);
  fmly^.SetText('------');
  EnableIsoFlds(false);
  sFld^.FocusFld;
  EnableButtons;
  ShowIsoSession;
end;

procedure TUIIsoDlg.SetModuleID;
begin
  modID:= MOD_UIISO;
end;

procedure TUIIsoDlg.EnableIsoFlds(on: boolean);

  procedure EnableIt(aFld: PUIFld); far;
  begin
    if aFld^.GetDBD = dbIso^.dbd then
    begin
      aFld^.Enable(on);
    end;
  end;

begin
  flds^.ForEach(@EnableIt);
  EnableWindow(GetItemHandle(IDC_LBLFAMILY), on);
  EnableWindow(fmly^.HWindow, on);
end;

procedure TUIIsoDlg.FillIsolate;
{- assumes the isolate data is in the dbIso^.dbr member }

  procedure GetIt(aFld: PUIFld); far;
  {- be sure to ignore isolate fields here}
  begin
    if aFld^.GetDBD = dbIso^.dbd then
      aFld^.XferRecToCtl(dbIso^.dbr);
  end;
var
  tmp   : word;
  fld   : PUIFld;
begin
  WaitCursor(true);
  flds^.ForEach(@GetIt);
  dbIso^.dbr^.GetField(DBIsoSetFamily, @setFamily, sizeof(setFamily));
  origSetFamily:= setFamily;
  OrigExtraTst:= ExtraTst;

  ManualSetFam:=False;
  ShowSetFamily;
  origIso^.CopyRecord(dbIso^.dbr);

  ShowIsoSession;

  {- if an order has already been made on the isolate then disable the field.
     we do not allow another order to be placed if one already exists }
  ordr^.Enable(ordr^.IsEmpty);

  EnableButtons;
  WaitCursor(false);
end;

procedure TUIIsoDlg.GetResults;
begin
  WaitCursor(true);
  results^.LoadResults(dbIso^.dbr); {- load results for the isolate }
  Results^.GetESBLResult(OrigESBL,4);
  InspectESBL;
  resultsExist:= results^.AnyResultsExist;
  WaitCursor(false);
end;

procedure TUIIsoDlg.FillSpecimen;
{- assumes the specimen data is in the dbSpec^.dbr member }

  procedure GetIt(aFld: PUIFld); far;
  {- be sure to ignore isolate fields here}
  begin
    if aFld^.GetDBD = dbSpec^.dbd then
      aFld^.XferRecToCtl(dbSpec^.dbr)
    else if aFld^.GetDBD = dbPat^.dbd then
      aFld^.XFerRecToCtl(dbPat^.dbr);
  end;

begin
  WaitCursor(true);
  {- first load patient record from specimen reference }
  dbSpec^.dbr^.GetField(DBSpecPatRef, @patSeq, SizeOf(patSeq));
  if (patSeq = 0) or not dbPat^.dbc^.GetSeq(dbPat^.dbr, patSeq) then
    dbPat^.dbr^.ClearRecord;

  {- now load specimen fields }
  specSeq:= dbSpec^.dbr^.GetSeqValue;
  flds^.ForEach(@GetIt);
  sFld^.Enable(false);
  cFld^.Enable(false);
  EnableIsoFlds(true);
  iFld^.FocusFld;
  EnableButtons;
  WaitCursor(false);
end;

function TUIIsoDlg.DataChanged: boolean;
{- compare the current data with the originally loaded data.  If any data has
   changed, then return true }

  function IsModified(aFld: PUIFld): boolean; far;
  begin
    if aFld^.GetDBD = dbIso^.dbd then
      IsModified:= aFld^.IsModified(origIso)
    else
      IsModified:= false;
  end;
var
  origSess  : TSeqNum;
  curSess   : TSeqNum;
  NewESBL   : array [0..4] of char;
begin
  Results^.GetESBLResult(NewESBL,4);
  if flds^.FirstThat(@IsModified) <> nil then
    DataChanged:= true
  else if (origSetFamily <> setFamily) or
          (OrigExtraTst <> ExtraTst) or
          (StrComp(OrigESBL, NewESBL)<>0) then
    DataChanged:= true
  else
  begin
    {- has the isolate been moved }
    origIso^.GetField(DBISOSess, @origSess, sizeof(origSess));
    dbIso^.dbr^.GetField(DBISOSess, @curSess, sizeof(curSess));
    DataChanged:= curSess <> origSess;
  end;
end;

function TUIIsoDlg.CheckSave(clearAfter: boolean): boolean;
var
  ret     : boolean;
  pstr    : array[0..200] of char;
  p2      : array[0..50] of char;
  k       : integer;
begin
  ret:= true;
  if mode <> MODE_NONE then
  begin
    {- first see if any data has changed }
    if DataChanged then
    begin
      k:= YesNoCancelMsg(HWindow, SR(IDS_CONFIRM, p2, 50), SR(IDS_CONFIRMSAVE, pstr, 200));
      if k = IDYES then
        ret:= SaveData(clearAfter)
      else
        ret:= k <> IDCANCEL;
    end;
  end;
  CheckSave:= ret;
end;

procedure TUIIsoDlg.DoSpecCollExit(newFocID: word);
{- called when specimen/collect data lose focus }
var
  id  : word;
begin
  if sFld^.IsEmpty or cFld^.IsEmpty then exit;
  WaitCursor(true);
  uiFldsProcessFocus:= false;
  sFld^.XferCtlToRec(tempSpec);
  cFld^.XferCtlToRec(tempSpec);
  if listObj^.FindKey(@self, tempSpec, true) then
  begin
    {- found a specimen, load screen }
    if tempSpec^.GetSeqValue <> dbSpec^.dbr^.GetSeqValue then
    begin
      dbSpec^.dbr^.CopyRecord(tempSpec);
      FillSpecimen;
    end;
  end
  else
  begin {- specimen not found (no change) }
    if listObj^.errorNum <> 0 then
      ShowError(@self, IDS_DBERROR, nil, listObj^.errorNum, modID, 0)
    else
      ShowError(@self, IDS_SPECNOTFOUND, nil, 0, modID, 0);
  end;
  uiFldsProcessFocus:= true;
  WaitCursor(false);
end;

procedure TUIIsoDlg.DoIsoExit(newFocID: word);
begin
  if (mode = MODE_NONE) and not iFld^.IsEmpty then
  begin
    {- attempt to find isolate }
    tempIso^.PutField(DBIsoSpecRef, @specSeq);
    iFld^.XFerCtlToRec(tempIso);
    if listObj^.FindKey(@self, tempIso, true) then
    begin
      dbIso^.dbr^.CopyRecord(tempIso);
      SetMode(MODE_EDIT);
      FillIsolate;
      GetResults;
    end
    else
    begin
      if listObj^.errorNum <> 0 then
        ShowError(@self, IDS_DBERROR, nil, listObj^.errorNum, modID, 0);
      SetMode(MODE_NEW);
      origIso^.ClearRecord;
      ShowIsoSession;
    end;
  end;
end;

procedure TUIIsoDlg.WMLostFocus(var msg: TMessage);
var
  fld     : PUIFld;
  err     : integer;
  setClass: word;
begin
  fld:= PUIFld(msg.lParam); {- get field pointer to fld losing focus }
  {- first see if the field is valid }
  err:= fld^.IsValid;
  if (err <> 0) and (err <> UIErrRequired) then   {- ignore required fields on field exit }
    ShowUIFldError(@self, err, fld)
  else
    if (fld^.GetID = IDC_EDSPECID) or (fld^.GetID = IDC_EDCOLLECT) then
      DoSpecCollExit(msg.wParam)
    else if fld^.GetID = IDC_EDISO then
      DoIsoExit(msg.wParam)
    else if fld^.GetID = IDC_EDORDER then
    begin  {- check the set in the selected order }
      GetSetFromOrder;
    end
    else if (fld=org) then InspectESBL;

  SetWindowLong(HWindow, DWL_MSGRESULT, 0);  {- set message result (for dialogs) }
end;

procedure TUIIsoDlg.WMFldList(var msg: TMessage);
{- called when user selects LIST in a field }
var
  id    : word;
  pstr  : array[0..100] of char;
  p2    : array[0..100] of char;
  ret   : longint;    {- return value to the UIFld that created this message }
  cont  : boolean;
  seq   : TSeqNum;
begin
  ret:= -1;   {- tell UI field passing this message NOT to list (non-zero) }

  uiFldsProcessFocus:= false;   {- turn off field focus processing }

  id:= msg.wParam;
  if (id = IDC_EDSPECID) or (id = IDC_EDCOLLECT) then
  begin
    sFld^.XferCtlToRec(tempSpec);
    cFld^.XferCtlToRec(tempSpec);
    if listObj^.SelectList(@self, tempSpec) then
    begin
      dbSpec^.dbr^.CopyRecord(tempSpec);
      FillSpecimen;
    end
    else if listObj^.errorNum <> 0 then
    begin
      if listObj^.errorNum = LISTERR_PARTIALKEYNOTFOUND then
        ShowError(@self, IDS_CANNOTLISTNOMATCH, nil, listObj^.errorNum, modID, 0)
      else
        ShowError(@self, IDS_CANNOTLISTBAD, nil, listObj^.errorNum, modID, 0);
    end;
  end
  else if id = IDC_EDISO then
  begin
    if mode <> MODE_NONE then
      cont:= CheckSave(false)
    else
      cont:= true;
    if cont then
    begin
      tempIso^.ClearRecord;
      seq:= dbSpec^.dbr^.GetSeqValue;
      tempIso^.PutField(DBIsoSpecRef, @seq);
      if listObj^.SelectList(@self, tempIso) then
      begin
        dbIso^.dbr^.CopyRecord(tempIso);
        SetMode(MODE_EDIT);
        FillIsolate;
        GetResults;
        EnableButtons;
      end
      else if listObj^.errorNum <> 0 then
      begin
        if listObj^.errorNum = LISTERR_PARTIALKEYNOTFOUND then
          ShowError(@self, IDS_CANNOTLISTNOMATCH, nil, listObj^.errorNum, modID, 0)
        else
          ShowError(@self, IDS_CANNOTLISTBAD, nil, listObj^.errorNum, modID, 0);
      end;
    end;
  end
  else
    ret:= 0;    {- this routine is not handling the list. Let the UIfld handle it }

  uiFldsProcessFocus:= true;  {- turn focus processing back on }

  SetWindowLong(HWindow, DWL_MSGRESULT, ret);  {- set message result (for dialogs) }
end;

procedure TUIIsoDlg.WMMnemSelected(var msg: TMessage);
begin
  if msg.wParam = IDC_EDORDER then
  begin
    GetSetFromOrder;
  end;
end;

procedure TUIIsoDlg.WMAutoClear(var msg: TMessage);
begin
  if msg.wParam = IDC_EDORDER then
  begin
    setFamily:= 0;
    ManualSetFam:=False;
    ShowSetFamily;
  end;
end;

procedure TUIIsoDlg.GetSetFromOrder;
{- get the panel set from the selected order and set the setFamily member }
var
  pr      : PDBRec;
  seq     : TSeqNum;
  aSet    : byte;
begin
  aSet:= 0;
  ordr^.XFerCtlToRec(tempIso);
  tempIso^.GetField(DBIsoOrdRef, @seq, sizeof(seq));
  if seq <> 0 then
  begin
    pr:= listObj^.PeekSeq(DBORDFile, seq);
    if pr <> nil then
      pr^.GetField(DBTstGrpSet, @aSet, sizeof(aSet));
  end;
  {- if the order's set is different from the current set then wipe out the
     current set/family and test (oxi,ind,betah) info }
  if aSet <> ExtractSet(setFamily) then
  begin
    setFamily:= word(aSet) shl 8;
    ManualSetFam:=False;
  end;
  ShowSetFamily;
  EnableButtons;
end;

procedure TUIIsoDlg.ShowIsoSession;
{- assumes a mode has been set and that the isolate record is loaded (if edit mode) }
var
  p1, p2: array[0..100] of char;
  seq   : TSeqNum;
begin
  if mode = MODE_EDIT then
  begin
    dbIso^.dbr^.GetField(DBIsoSess, @seq, sizeof(seq));
    if seq = 0 then
      sessTxt^.SetText(SR(IDS_ISONOTBELONG, p1, sizeof(p1)-1))
    else if GetSessionInfo(seq, p1, p2, 100) then
      sessTxt^.SetText(p2)
    else
      sessTxt^.SetText(SR(IDS_ERROR, p1, sizeof(p1)-1));
  end
  else if mode = MODE_NEW then
  begin
    seq:= GetCurrentSession(p1, 100);
    if seq = 0 then
      sessTxt^.SetText(SR(IDS_SESSIONSDISABLED, p1, sizeof(p1)-1))
    else
      sessTxt^.SetText(p1);
  end
  else
    sessTxt^.SetText(nil);
end;

procedure TUIIsoDlg.ShowSetFamily;
var
  pstr  : array[0..100] of char;
begin
  if setFamily = 0 then
    fmly^.SetText('------')
  else
    fmly^.SetText(ExtractFamilyAsStr(setFamily, pstr, 100));
end;

function TUIIsoDlg.GetFamily(var aTest: integer): boolean;
{- prompt for family and extra test information }
var
  d     : PDialog;
  seq   : TSeqNum;
begin
  ordr^.XFerCtlToRec(tempIso);
  tempIso^.GetField(DBIsoOrdRef, @seq, sizeof(seq));
  if seq <> 0 then
  begin
    if SelectFamily(@self, seq, setFamily, aTest) then
    begin
      GetFamily:= true;
      ManualSetFam:=true;
      ShowSetFamily;
    end
    else
      GetFamily:= false;
  end
  else
  begin
    aTest:= 0;
    GetFamily:= true;
  end;
end;

procedure TUIIsoDlg.IDSpecimen(var msg: TMessage);
var
  seq   : TSeqNum;
begin
  seq:= dbSpec^.dbr^.GetSeqValue;
  if seq > 0 then
  begin
    UISpecimenEntry(@self, seq);
    {- reload patient record }
    if dbSpec^.dbc^.GetSeq(dbSpec^.dbr, seq) then
      FillSpecimen
    else
    begin
      ShowError(@self, IDS_ERRLODSPEC, nil, dbSpec^.dbc^.dbErrorNum, modID, 0);
      ClearFields;
    end;

    {- if an order has already been made on the isolate then disable the field.
       we do not allow another order to be placed if one already exists }
     ordr^.Enable(ordr^.IsEmpty);
  end;
end;

procedure TUIIsoDlg.IDSession(var msg: Tmessage);
begin
  if listObj^.EditableMnemList(@self, DBSessFile, '') then
    ShowIsoSession;
end;

procedure TUIIsoDlg.IDMove(var msg: TMessage);
begin
  if mode <> MODE_EDIT then exit;

  if application^.ExecDialog(New(PMoveIsoDlg, Init(@self, dbIso, listObj))) = IDOK then
    ShowIsoSession;
  iFld^.FocusFld;
end;

procedure TUIIsoDlg.IDMIC(var msg: TMessage);
var
  temp    : TSeqNum;
begin
  if CheckSave(false) then
  begin
    UIIDMicEntry(@self, dbIso^.dbr^.GetSeqValue, false);
    dbIso^.dbc^.GetSeq(dbIso^.dbr, dbIso^.dbr^.GetSeqValue);
    dbSpec^.dbc^.GetSeq(dbSpec^.dbr, dbSpec^.dbr^.GetSeqValue);
    FillSpecimen;
    SetMode(MODE_EDIT);
    FillIsolate;

    GetResults;
    EnableButtons;
  end;
end;

procedure TUIIsoDlg.IDAS4(var msg: TMessage);
var
  seq   : TSeqNum;
begin
  seq:= dbIso^.dbr^.GetSeqValue;
  if CheckSave(false) then
  begin
    AS4ReadSession(@self, seq);
    GetResults;
    EnableButtons;
  end;
end;

procedure TUIIsoDlg.IDBarCodes(var msg: TMessage);
var
  wap     : PDBFile;
  ses     : TSeqNum;
  tstr    : array[0..2] of char;
  tray    : PTraysObject;
begin
  if CheckSave(false) then
  begin
    ses:= GetCurrentSession(tstr, sizeof(tstr)-1);
    wap:= New(PDBFile, Init(DBWAPNLFile, '', dbOpenNormal));
    tray:= New(PTraysObject, Init);
    PrintBarcodes(@self, MakeIntResource(DLG_BARCODES), ses, dbIso^.dbr^.GetSeqValue,
                  tray, listObj, dbIso, wap);
    MSDisposeObj(tray);
    MSDisposeObj(wap);
  end;
end;

procedure TUIIsoDlg.IDPatient(var msg: TMessage);
var
  seq   : TSeqNum;
begin
  seq:= dbPat^.dbr^.GetSeqValue;
  if seq > 0 then
  begin
    UIPatientEntry(@self, seq);
    {- reload patient record }
    if dbPat^.dbc^.GetSeq(dbPat^.dbr, seq) then
      FillSpecimen
    else
    begin
      ShowError(@self, IDS_ERRLODPAT, nil, dbPat^.dbc^.dbErrorNum, modID, 0);
    end;

    {- if an order has already been made on the isolate then disable the field.
       we do not allow another order to be placed if one already exists }
     ordr^.Enable(ordr^.IsEmpty);
  end;
end;

procedure TUIIsoDlg.IDClear(var msg: TMessage);
var
  k   : integer;
begin
  k:= application^.ExecDialog(New(PClearDlg, Init(@self, MakeIntResource(DLG_CLEAR))));
  iFld^.FocusFld;
  if k <> IDCANCEL then
  begin
    if CheckSave(false) then
      if k = IDC_CLEARISO then
        ClearIsolate
      else if k = IDC_CLEARALL then
        ClearFields;
  end;
end;

procedure TUIIsoDlg.IDFamily;
{- prompt for family and extra test information on demand }
var
  aset, aFam: integer;

begin
  aSet:= ExtractSet(setFamily);
  aFam:= ExtractFamily(setFamily);
  if (aSet in [1, 2, 5, 21, 3]) then {- family and extra test info required }
  begin
    if (aSet in [2, 5, 21]) and (AFam=0) then  {- default to staph }
    begin
      aFam:= 2;
      setFamily:= MakeSetFamily(aSet, 2);
    end;
    WaitCursor(false);
    GetFamily(extraTst);
    HandleEvents(hWindow);
    WaitCursor(true);
  end;
end;

procedure TUIIsoDlg.DeleteData;
var
  p1, p2  : array[0..100] of char;
  err     : integer;
begin
  if mode = MODE_EDIT then
  begin
    if YesNoMsg(HWindow, SR(IDS_CONFIRM, p1, 100), SR(IDS_CONFIRMDELISO, p2, 100)) then
    begin
      err:= 0;
      WaitCursor(true);
      if dbIso^.dbc^.GetSeq(dbIso^.dbr, origIso^.GetSeqValue) then
      begin
        if not dbIso^.dbc^.DeleteRec(dbIso^.dbr) then
          err:= dbIso^.dbc^.dbErrorNum
        else
          ClearIsolate;
      end
      else
        err:= dbIso^.dbc^.dbErrorNum;

      if err <> 0 then
        ShowError(@self, IDS_CANTDEL, nil, err, modID, 0);
      WaitCursor(false);
    end;
  end;
end;

procedure TUIIsoDlg.RecalcInterps;
var
  tstGrp    : array[0..100] of char;
  idx       : integer;
  count     : integer;
  interp    : PInterp;
  k         : integer;
  orgNum    : integer;
  res       : PResRecObj;
  seq       : TSeqNum;
  isUrine   : boolean;
  rec       : PDBRec;
begin
    WaitCursor(true);

    Val(org^.listRec^.id, orgNum, k);

    isUrine:= false;
    dbSpec^.dbr^.GetField(DBSpecSrc, @seq, sizeof(seq));
    if seq <> 0 then
    begin
      rec:= listObj^.PeekSeq(DBSRCFile, seq);
      if rec <> nil then
        rec^.GetField(DBSRCUrine, @isUrine, sizeof(isUrine));
    end;

    strcopy(tstGrp, '');
    while results^.NextTestGroup(tstGrp, idx) do
    begin
      {- if the test group is not user defined nor and "additional"}
      res:= results^.At(idx);
      if not ( res^.tgUD or (StrComp(res^.testgroupID,NullTestGroupID)=0)) then
      begin
        results^.traysObj^.LoadTray(tstGrp);  {- make sure the tray gets loaded }
        interp:= New(PInterp, Init(orgNum, results));
        count:= results^.NumTests(tstGrp, idx);
        for k:= idx to idx + count - 1 do
        begin
          res:= results^.At(k);
          if (StrComp(res^.testCatID, 'MIC') = 0) then
            interp^.SetupInterps(res^.testID, res^.result);
        end;
        interp^.CalcInterps(isUrine);
        MSDisposeObj(interp);
      end;
    end;
    WaitCursor(false);
end;

Procedure TUIIsoDlg.RecalcESBLInterps;
var TestIdx,
    OrgNum,
    Idx, RDN: Integer;
    seq      : TSeqNum;
    rec      : PDBRec;
    interp   : PInterp;
    isUrine  : Boolean;
    TestGroup: array[0..255] of char;
    Res      : PResRecObj;
    ESBLInfo : ESBLRecord;

begin
  WaitCursor(true);

  Val(org^.listRec^.id, orgNum, Idx);

  isUrine:= false;
  dbSpec^.dbr^.GetField(DBSpecSrc, @seq, sizeof(seq));
  if seq <> 0 then
  begin
    rec:= listObj^.PeekSeq(DBSRCFile, seq);
    if rec <> nil then
      rec^.GetField(DBSRCUrine, @isUrine, sizeof(isUrine));
  end;

  StrCopy(TestGroup, '');
  While Results^.NextTestGroup(TestGroup, Idx) do
  begin
    {- if the test group is not user defined nor and "additional"}
    Res:= Results^.At(Idx);
    if not ( res^.tgUD or (StrComp(res^.testgroupID,NullTestGroupID)=0)) then
    begin
      Results^.TraysObj^.LoadTray(TestGroup);  {- make sure the tray gets loaded }
      if ESBLMics(Results^.TraysObj^.tfsRec.TrayNum, ESBLInfo) then
      begin
        interp:= New(PInterp, Init(orgNum, results));
        for TestIdx:=Idx to Idx + results^.NumTests(TestGroup, Idx) - 1 do
        begin
          Res:= Results^.At(TestIdx);
          if (StrComp(res^.testCatID, 'MIC') = 0) then
              interp^.SetupInterps(res^.testID, res^.result);
        end;
        interp^.CalcESBLInterps(isUrine);
        MSDisposeObj(interp);
      end;
    end;
  end;
  WaitCursor(false);
end;

function TUIIsoDlg.CurrentOrganism: Integer;
var Orgnum, err: Integer;
begin
  Val(Org^.ListRec^.id, Orgnum, err);
  if err=0
     then CurrentOrganism:=Orgnum
     else CurrentOrganism:=-1;
end;

function TUIIsoDlg.ReplaceOrg(NewOrg: Integer): boolean;
var NewOrgStr   : String[128];
    NewOrgPChar : array [0..128] of char;
begin
  If NewOrg<>CurrentOrganism then
  begin
    ReplaceOrg:=True;
    Str(NewOrg,NewOrgStr);
    StrPCopy(NewOrgPChar,NewOrgStr);
    Org^.SetCtlAsStr(NewOrgPChar);
  end
  else ReplaceOrg:=False;
end;

Function TUIIsoDlg.InspectESBL: Boolean;
var Orgnum: integer;
    MappedOrganism: Integer;
begin
  Orgnum:=CurrentOrganism;
  if Results^.ESBLChanged(Orgnum) then
  begin
    RecalcESBLInterps;
    InspectESBL:=True;
  end
  else InspectESBL:=false;

  if ESBLMappable(Orgnum, MappedOrganism, Results) then
     ReplaceOrg(MappedOrganism);
end;

function TUIIsoDlg.SaveData(clearAfter: boolean): boolean;
{- save the data for the isolate. After saving an isolate, order will need to
   be created (NEW mode only) and results will have to be created as well }
var
  ret           : boolean;
  isoOrd        : PDBFile;
  newOrderMade  : boolean;  {- this is set to true when a new order has been made
                               on the isolate }
  Orgnum        : integer;
  err           : integer;

  MappedOrg  : Integer;
  ScreenRes  : PScreens;
  ESBLChanged: boolean;
  NewESBL    :array[0..4] of char;

  function ValidateScreen: boolean;
  {- check screen for validity }
  var
    sret    : boolean;
    fld     : PUIFld;
    errNum  : integer;
    aSet, aFam: byte;
    aTest   : integer;

    function IsInvalid(aFld: PUIFld): boolean; far;
    begin
      if aFld^.GetDBD = dbIso^.dbd then
      begin
        errNum:= aFld^.Format;
        IsInvalid:= errNum <> 0;
      end
      else
        IsInvalid:= false;
    end;

  begin
    {- first see if any fields are invalid }
    sret:= true;
    fld:= flds^.FirstThat(@IsInvalid);
    if (fld <> nil) then
    begin {- some other error }
      ShowUIFldError(@self, errNum, fld);
      sRet:= false;
    end
    else
    begin
      {- automatic prompting for family and extra test stuff only occurs when
         a new isolate is being created }
      if newOrderMade and not ManualSetFam then
      begin
        aSet:= ExtractSet(setFamily);
        aFam:= ExtractFamily(setFamily);
        if (aSet in [1, 2, 5, 21, 3]) then {- family and extra test info required }
        begin
          if aSet in [2, 5, 21] then  {- default to staph }
          begin
            aFam:= 2;
            setFamily:= MakeSetFamily(aSet, 2);
          end;
          WaitCursor(false);
          sret:= GetFamily(extraTst);
          HandleEvents(hWindow);
          WaitCursor(true);
        end;
      end;
    end;
    ValidateScreen:= sret;
  end;

  function StartTransaction: boolean;
  var
    errNum  : integer;
  begin
    errNum:= DBBeginTransaction;
    if errNum <> 0 then
    begin
      ShowError(@self, IDS_TRANSERROR, nil, errNum, modID, 0);
      StartTransaction:= false;
    end
    else
      StartTransaction:= true;
  end;

  function LockRecord: boolean;
  {- lock isolate record and place current content (in DB) of record in tempIso.
     if the records are not found then the user is prompted to recreate in which case
     the appropriate mode (mode, patMode) is modified accordingly }
  begin
    {- no need to lock if adding a new isolate }
    if mode = MODE_EDIT then
    begin
      tempIso^.CopyRecord(origIso);
      LockRecord:= LockIt(dbIso, tempIso, IDS_ISOLOCKED, IDS_CANTLOCATEORIGISO, mode);
    end
    else
      LockRecord:= true;
  end;

  function CheckModified: boolean;
  {- at this point the original record is locked. Now see if someone else has
      modified it since we originally loaded it. if so prompt for overwrite. Assumes
      the current contents of the record are placed in tempSpec/tempPat }
  var
    sret    : boolean;
    p1, p2  : array[0..200] of char;
  begin
    sret:= true;
    if mode = MODE_EDIT then    {- only valid if editing a specimen }
    begin
      if not origIso^.CompareRecord(tempIso) then
        sret:= YesNoMsg(HWindow, SR(IDS_WARNING, p1, 200), SR(IDS_ISOMODIFIED, p2, 200));
    end;
    CheckModified:= sRet;
  end;

  function SaveIt: boolean;
  var
    sRet  : boolean;
    eNum  : integer;
    seq   : TSeqNum;
    p1    : array[0..50] of char;
    p2    : array[0..200] of char;
    tBool : boolean;

    procedure XFerToRec(aFld: PUIFld); far;
    begin
      if aFld^.GetDBD = dbIso^.dbd then  {- skip over non isolate fields }
        aFld^.XFerCtlToRec(dbIso^.dbr);
    end;

  begin
    sRet:= true;

    {- now transfer all fields to the record buffer }
    flds^.ForEach(@XFerToRec);

    seq:= dbSpec^.dbr^.GetSeqValue;  {- get specimen seq for specimen reference }
    dbIso^.dbr^.PutField(DBIsoSpecRef, @seq);
    {- update redundant specimen fields in isolate record }
    dbSpec^.dbr^.GetField(DBSpecSpecID, @p1, sizeof(p1)-1);
    dbIso^.dbr^.PutField(DBIsoSpecID, @p1);

    dbSpec^.dbr^.GetFieldAsStr(DBSpecCollectDate, p1, sizeof(p1)-1);
    dbIso^.dbr^.PutFieldAsStr(DBIsoCollectDate, p1);

    {- get current session (if new mode only) }
    if mode = MODE_NEW then
    begin
      seq:= GetCurrentSession(p1, sizeof(p1)-1);
      dbIso^.dbr^.PutField(DBIsoSess, @seq);
    end;

    {- handle transmitted flag }
    dbIso^.dbr^.GetField(DBIsoTransFlag, @tBool, sizeof(tBool));
    if tBool then
    begin
      if YesNoMsg(hWindow, SR(IDS_CONFIRM, p1, sizeof(p1)-1),
                           SR(IDS_XMITSET, p2, sizeof(p2)-1)) then
      begin
        tBool:= false;
        dbIso^.dbr^.PutField(DBIsoTransFlag, @tBool);
      end;
    end;

    {- handle set/family field }
    dbIso^.dbr^.PutField(DBIsoSetFamily, @setFamily);

    if mode = MODE_NEW then
      dbIso^.dbc^.InsertRec(dbIso^.dbr)
    else
      dbIso^.dbc^.UpdateRec(dbIso^.dbr);

    if dbIso^.dbc^.dbErrorNum <> 0 then
    begin
      eNum:= 0;
      {- special error handling for common errors }
      case dbIso^.dbc^.dbErrorNum of
        MOD_BTRV + 5: eNum:= IDS_DUPISO;
        else
          eNum:= IDS_CANNOTSAVE;
      end;
      ShowError(@self, eNum, nil, dbIso^.dbc^.dbErrorNum, modID, 99);
      iFld^.FocusFld;
      sret:= false;
    end;
    SaveIt:= sret;
  end;

  procedure UnlockRecords;
  begin
    if mode = MODE_EDIT then
      dbIso^.dbc^.UnlockRec(tempIso);
  end;

begin
  isoOrd:= nil;
  WaitCursor(true);

  {- figure out if a new order has been placed on the isolate. }
  if (ExtractSet(origSetFamily) = 0) and (ExtractSet(setFamily) <> 0) then
    newOrderMade:= true
  else
    newOrderMade:= false;

  ret:= true;
  if ret then ret:= ValidateScreen;
  if ret then ret:= StartTransaction;
  if ret then ret:= LockRecord;
  if ret then ret:= CheckModified;

  {Check ESBL in case orgmapping is needed}
  InspectESBL;
  Results^.GetESBLResult(NewESBL,4);
  ESBLChanged:=(StrComp(OrigESBL,NewESBL)<>0);

  {Provide ScreenIso Logic with the raw orgnum, so that it always prompts}
  {for ESBL/E157 values.  Also, keep track of the preScreened ESBL result}
  MappedOrg:=CurrentOrganism;
  SearchOrgMaps(ESBLSection, MappedOrg, MappedOrg, False);
  Results^.GetESBLResult(OrigESBL,4);

  {Screen this Isolate, prompt if needed}
  ScreenRes:=New(PScreens,Init);
  ScreenIsolate(@Self, MappedOrg, MappedOrg,
                ExtractSet(SetFamily), Results, ScreenRes, not ESBLChanged, not ESBLChanged);

  {Use the screened org, keep ESBL interps coherent}
  ReplaceOrg(MappedOrg);
  Results^.GetESBLResult(NewESBL,4);
  if StrComp(OrigESBL,NewESBL)<>0 then RecalcESBLInterps;

  if ret then ret:= SaveIt;
  if ret and Results^.IsModified then Results^.SaveResults;

  if ret and newOrderMade then
  begin
    ret:= CreateOrders(@self, dbIso, isoOrd, results);
    if ret then
      ret:= CreateResults(dbIso, results, setFamily, extraTst);
  end
  Else
    If Ret and Not NewOrderMade then
      ret:= CreateResults(dbIso, results, setFamily, extraTst);

  UnlockRecords;

  if ret then
  begin
    DBEndTransaction;
    origIso^.CopyRecord(dbIso^.dbr);
  end
  else
    DBAbortTransaction;

  MSDisposeObj(isoOrd);

  {- and finally, clear fields }
  if ret and clearAfter then
    ClearIsolate;

  SaveData:= ret;
  WaitCursor(false);
end;

procedure TUIIsoDlg.WMCtlColor(var msg: TMessage);
begin
  if msg.lParamLo = sessTxt^.HWindow then
  begin
    SetTextColor(msg.wParam, RGB($FF,0,0));
    msg.result:= GetStockObject(WHITE_BRUSH);
    SetWindowLong(HWindow, DWL_MSGRESULT, msg.result);  {- set message result (for dialogs) }
  end
  else
    inherited WMCtlColor(msg);
end;

{---------------------------------------------------[ Exported Function ]--}

procedure UIIsoEntry(aParent: PWindowsObject; aSpecSeq, aIsoSeq: TSeqNum);
var
  d   : PUIIsoDlg;
begin
  d:= New(PUIIsoDlg, Init(aParent, aSpecSeq, aIsoSeq));
  application^.ExecDialog(d);
end;

END.

{- rcf}
