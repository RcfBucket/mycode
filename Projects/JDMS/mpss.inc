(****************************************************************************


mpss.inc

produced by Borland Resource Workshop


*****************************************************************************)

const
	DLG_MPSSMAIN	=	$800;
	DLG_DEFRULE	=	$801;
	DLG_SAVERULE	=	$802;

	IDC_RULELIST	=	200;
	IDC_FIELDLIST	=	205;
	IDC_SAVE	=	101;
	IDC_CLEAR	=	102;
	IDC_ADD	=	103;
	IDC_EDIT	=	104;
	IDC_DELETE	=	105;
	IDC_UPDATE_LINE	=	150;
	IDC_INSERT_LINE	=	151;
	IDC_DELETE_LINE	=	152;
	IDC_AND	=	201;
	IDC_OR	=	202;
	IDC_LPAREN	=	203;
	IDC_RPAREN	=	204;
	IDC_EQ	=	210;
	IDC_NE	=	211;
	IDC_GT	=	212;
	IDC_LT	=	213;
	IDC_GE	=	214;
	IDC_LE	=	215;
	IDC_IN	=	216;
	IDC_VALUE	=	220;

	IDC_CODE	=	230;
	IDC_DESC	=	231;

	IDC_LBL1	=	500;
	IDC_LBL2	=	501;

	IDS_MPSS	=	$800;
	IDS_AND	=	$801;
	IDS_OR	=	$802;
	IDS_FIELDNAME	=	$803;
	IDS_CODE	=	$804;
	IDS_DESC	=	$805;
	IDS_ADD_RULE	=	$806;
	IDS_EDIT_RULE	=	$807;
	IDS_DELETE_RULE	=	$808;
	IDS_CANNOT_DELETE	=	$809;
	IDS_BLANK_ENTRY	=	$80A;
	IDS_IN_USE	=	$80B;
	IDS_UNMATCHED_PAREN	=	$80C;
	IDS_INVALID_RULE	=	$80D;
	IDS_INVALID_FIELD	=	$80E;
	IDS_INITERR1	=	$80F;
	IDS_BAD_FILE	=	$810;
	IDS_MPSS_RPT_HDR	=	$811;
