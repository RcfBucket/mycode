const
  IDC_PROCESSED         = 201;
  IDC_RAW               = 202;
  IDC_CALIBRATED        = 203;
  IDC_GAINS             = 204;

  IDS_AS4_TITLE         = $0B01;
  IDS_WA_TITLE          = $0B02;
  IDS_LASTREAD          = $0B03;
  IDS_PANELTYPE         = $0B04;
  IDS_ORGANISM          = $0B05;
  IDS_BIOTYPE           = $0B06;
  IDS_TESTID            = $0B07;
  IDS_MICS              = $0B08;
  IDS_INVALID_BIOTYPE   = $0B09;
  IDS_RARE_BIOTYPE      = $0B0A;
  IDS_UNDEF_BIOTYPE     = $0B0B;
  IDS_NEEDOXI           = $0B0C;
  IDS_NEEDIND           = $0B0D;
  IDS_PROC              = $0B0E;
  IDS_NOTES             = $0B0F;
  IDS_ASTERISK          = $0B10;
  IDS_CALWARNING        = $0B11;
  IDS_MICTHRESH         = $0B12;
  IDS_AVERAGEGROWTH     = $0B13;
  IDS_NGTHRESH          = $0B14;
  IDS_UNUSEDWELLS       = $0B15;
  IDS_TECHID            = $0B16;

	IDS_AS4_OutOfControl	=	2839;
