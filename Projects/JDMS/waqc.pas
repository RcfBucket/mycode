{----------------------------------------------------------------------------}
{  Module Name  : WAQC.PAS                                                   }
{  Programmer   : EJ                                                         }
{  Date Created : 05/28/95                                                   }
{                                                                            }
{  Purpose -                                                                 }
{  This module provides the user with QC functions available in              }
{  the WA INTERFACE.                                                         }
{                                                                            }
{  Assumptions -                                                             }
{  None.                                                                     }
{                                                                            }
{  Revision History -                                                        }
{                                                                            }
{  Version  Date      Who    Did What                                        }
{  -------  --------  ------ ----------------------------------------------- }
{  1.00     05/28/95  EJ     Initial release.                                }
{                                                                            }
{----------------------------------------------------------------------------}

unit WAQC;

INTERFACE

uses
  OWindows;


procedure PrintQC(aParent: PWindowsObject);

IMPLEMENTATION

uses
  Strings,
  StrsW,
  MScan,
  APITools,
  Report,
  WAMain,
  WAGPIB,
  WAMaint,
  WAUtil,
  DMSErr,
  DMSDebug;


const
  QCColOne    = 31;
  QCColTwo    = 58;
  LabelWidth  = 12;


type
  PPrintQCObj = ^TPrintQCObj;
  TPrintQCObj = object(TReport)
    hdwrObj    : PHdwrStatus;
    { Out of Control values for calssic WA }
    DiodeLo    : longint;  {-3650}
    DiodeHi    : longint;   {3250}
    ITime0Lo   : longint; {2075}
    ITime0Hi   : longint; {2975}
    ITime4Lo   : longint; {4150}
    ITime4Hi   : longint; {5950}
    ITime6Lo   : longint; {12450}
    ITime6Hi   : longint; {17850}
    ITime7Lo   : longint; {33200}
    ITime7Hi   : longint; {47600}
    CrsDacLo   : longint; {25}
    CrsDacHi   : longint; {230}
    { For WA40, Gain lower/upper limits as function of filter }
    LGnFlt1    : longint;  {4  Lower gain filter 1 }
    UGnFlt1    : longint;  {7  Upper gain filter 1 }
    LGnFlt2    : longint;  {0  Lower gain filter 2 }
    UGnFlt2    : longint;  {3  Upper gain filter 2 }
    LGnFlt3    : longint;  {0  Lower gain filter 3 }
    UGnFlt3    : longint;  {3  Upper gain filter 3 }
    LGnFlt4    : longint;  {0  Lower gain filter 4 }
    UGnFlt4    : longint;  {3  Upper gain filter 4 }
    LGnFlt5    : longint;  {1  Lower gain filter 5 }
    UGnFlt5    : longint;  {4  Upper gain filter 5 }
    LGnFlt6    : longint;  {4  Lower gain filter 6 }
    UGnFlt6    : longint;  {7  Upper gain filter 6 }
    { FOR WA40, Offset DAC upper and lower limits }
    UOffDac    : longint;  {254}
    LOffDac    : longint;  {1}
    { FOR WA40, Offsets upper limits as function of gain }
    UOffGn0    : longint;  {408   Upper offset for Gain = 0 }
    UOffGn1    : longint;  {432   Upper offset for Gain = 1 }
    UOffGn2    : longint;  {480   Upper offset for Gain = 2 }
    UOffGn3    : longint;  {576   Upper offset for Gain = 3 }
    UOffGn4    : longint;  {768   Upper offset for Gain = 4 }
    UOffGn5    : longint;  {1152  Upper offset for Gain = 5 }
    UOffGn6    : longint;  {1920  Upper offset for Gain = 6 }
    UOffGn7    : longint;  {3456  Upper offset for Gain = 7 }

    LAir       : longint;  {15839  Lower Air }
    UAir       : longint;  {33895  Upper Air }

    UDark      : longint;  {900    Upper Darkness }
    LRef       : longint;  {5000   Lower Reference Disk }
    URef       : longint;  {8000   Upper Reference Disk }
    LPmt       : longint;  {-12000 Lower PMT Voltage }
    UPmt       : longint;  {-3000  Upper PMT Voltage }
    LLamp      : longint;  {965    Lower Lamp Voltage }
    ULamp      : longint;  {985    Upper Lamp Voltage }
    LCLamp     : longint;  {1165   Lower Color Lamp Voltage }
    UCLamp     : longint;  {1185   Upper Color Lamp Voltage }

    L0VRef1    : longint;  {307    Lower 0 Voltage #1 }
    U0VRef1    : longint;  {462    Upper 0 Voltage #1 }
    L0VRef2    : longint;  {307    Lower 0 Voltage #2 }
    U0VRef2    : longint;  {462    Upper 0 Voltage #2 }
    L2VRef1    : longint;  {1504   Lower 2.5 Voltage #1 }
    U2VRef1    : longint;  {1663   Upper 2.5 Voltage #1 }
    L2VRef2    : longint;  {1504   Lower 2.5 Voltage #2 }
    U2VRef2    : longint;  {1663   Upper 2.5 Voltage #2 }

    LTemp      : longint;  {3400  Lower Temperature }
    UTemp      : longint;  {3600  Upper Temperature }
    LHmdt      : longint;  {5000  Lower Humidity }
    UHmdt      : longint;  {7000  Upper Humidity }
    UAmbTmp    : longint;  {3000  Upper Ambient Temperature }
    UIntTmp    : longint;  {4500  Upper Internal Temperature }

    dacs       : DACArray;
    gains      : INTArray;
    airs       : array[1..12] of longint;
    jrGains    : JRGnArray;
    jrOffsets  : UsdOArray;
    jrAirs     : JrAirArray;
    jrDacs     : JrDacArray;
    jrFluor,
    jrAdSys,
    jrEnvStat  : JRMscArray;
    lineNumber : integer;
    tempStr    : PChar;
    okStr,
    nokStr,
    offStr     : array[0..6] of char;
    {}
    constructor Init(aParent: PWindowsObject);
    destructor Done; virtual;
    function  ReadOutOfCtrlValues: boolean;
    procedure PrintReport;
    procedure PrintLabels;
    procedure PrintHdwrStatus;
    procedure PrintManualChecks;
    procedure PrintCalibration;
    function  PrintClassicWACalibration: boolean;
    function  PrintWA4096Calibration: boolean;
    procedure PrintTechInfo;
    function  GetGainValue(colWidth, gainIdx: integer): boolean;
    function  GetDacValue(colWidth, dacIdx: integer; CheckCourse: boolean): boolean;
    function  ValidateGainValue(stage: integer; gainVal: longint): boolean;
    function  ValidateDacValue(dacVal: longint): boolean;
    function  GetAirValue(colWidth, airIdx: integer): boolean;
    function  ValidateAirValue(airVal: longint): boolean;
    function  PrintFluorometerCalibration: boolean;
    function  GetFilterGain(filter: byte; colWidth: integer): boolean;
    function  GetFilterOffset(filter: byte; colWidth: integer): boolean;
    function  GetFilterAir(filter: byte; colWidth: integer): boolean;
    function  GetFilterDac(filter: byte; colWidth: integer): boolean;
    procedure ConcatLineChars(howMany: integer; newStr: PChar);
  end;


constructor TPrintQCObj.Init(aParent: PWindowsObject);
var
  temp  : array[0..64] of char;
begin
  tempStr := nil;
  hdwrObj := nil;
  SR(IDS_QC_HDR, temp, 64);     {Walkaway QC Instrument Diagnostics}
  if not inherited Init(aParent, temp, 121, 0) then
  begin
    Done;
    Fail;
  end;

  GetMem(tempStr, printStrLen+1);
  if tempStr = nil then
  begin
    Done;
    Fail;
  end;

  SR(IDS_OK, okStr, 6);   { OK }
  SR(IDS_NOK, nokStr, 6); { NOK }
  SR(IDS_OFF, offStr, 6); { OFF }

  hdwrObj := New(PHdwrStatus, Init);
end;

destructor TPrintQCObj.Done;
begin
  MSDisposeObj(hdwrObj);
  MSFreeMem(tempStr, printStrLen+1);
  inherited Done;
end;

function TPrintQCObj.ReadOutOfCtrlValues: boolean;
var
  f : Text;

  function ReadThisValue(var thisVal: longint): boolean;
  var
    readVal      : string;
    code         : integer;
    commentStart : integer;
  begin
    ReadThisValue := FALSE;
  {$I-}
    ReadLn(f, readVal);
    if IOResult <> 0 then Exit;
  {$I+}
    commentStart := Pos(';', readVal);
    readVal[0] := Chr(commentStart-1);
    Val(readVal, thisVal, code);
    if code <> 0 then Exit;
    ReadThisValue := TRUE;
  end;

begin
  ReadOutOfCtrlValues := FALSE;
{$I-}
  Assign(f, 'WAQCVALS.TXT');
  Reset(f);
  if IOResult = 0 then
  begin
    if not ReadThisValue(DiodeLo) then Exit;
    if not ReadThisValue(DiodeHi) then Exit;
    if not ReadThisValue(ITime0Lo) then Exit;
    if not ReadThisValue(ITime0Hi) then Exit;
    if not ReadThisValue(ITime4Lo) then Exit;
    if not ReadThisValue(ITime4Hi) then Exit;
    if not ReadThisValue(ITime6Lo) then Exit;
    if not ReadThisValue(ITime6Hi) then Exit;
    if not ReadThisValue(ITime7Lo) then Exit;
    if not ReadThisValue(ITime7Hi) then Exit;
    if not ReadThisValue(CrsDacLo) then Exit;
    if not ReadThisValue(CrsDacHi) then Exit;
    if not ReadThisValue(LGnFlt1) then Exit;
    if not ReadThisValue(UGnFlt1) then Exit;
    if not ReadThisValue(LGnFlt2) then Exit;
    if not ReadThisValue(UGnFlt2) then Exit;
    if not ReadThisValue(LGnFlt3) then Exit;
    if not ReadThisValue(UGnFlt3) then Exit;
    if not ReadThisValue(LGnFlt4) then Exit;
    if not ReadThisValue(UGnFlt4) then Exit;
    if not ReadThisValue(LGnFlt5) then Exit;
    if not ReadThisValue(UGnFlt5) then Exit;
    if not ReadThisValue(LGnFlt6) then Exit;
    if not ReadThisValue(UGnFlt6) then Exit;
    if not ReadThisValue(UOffDac) then Exit;
    if not ReadThisValue(LOffDac) then Exit;
    if not ReadThisValue(UOffGn0) then Exit;
    if not ReadThisValue(UOffGn1) then Exit;
    if not ReadThisValue(UOffGn2) then Exit;
    if not ReadThisValue(UOffGn3) then Exit;
    if not ReadThisValue(UOffGn4) then Exit;
    if not ReadThisValue(UOffGn5) then Exit;
    if not ReadThisValue(UOffGn6) then Exit;
    if not ReadThisValue(UOffGn7) then Exit;

    if not ReadThisValue(LAir) then Exit;
    if not ReadThisValue(UAir) then Exit;

    if not ReadThisValue(UDark) then Exit;
    if not ReadThisValue(LRef) then Exit;
    if not ReadThisValue(URef) then Exit;
    if not ReadThisValue(LPmt) then Exit;
    if not ReadThisValue(UPmt) then Exit;
    if not ReadThisValue(LLamp) then Exit;
    if not ReadThisValue(ULamp) then Exit;
    if not ReadThisValue(LCLamp) then Exit;
    if not ReadThisValue(UCLamp) then Exit;

    if not ReadThisValue(L0VRef1) then Exit;
    if not ReadThisValue(U0VRef1) then Exit;
    if not ReadThisValue(L0VRef2) then Exit;
    if not ReadThisValue(U0VRef2) then Exit;
    if not ReadThisValue(L2VRef1) then Exit;
    if not ReadThisValue(U2VRef1) then Exit;
    if not ReadThisValue(L2VRef2) then Exit;
    if not ReadThisValue(U2VRef2) then Exit;

    if not ReadThisValue(LTemp) then Exit;
    if not ReadThisValue(UTemp) then Exit;
    if not ReadThisValue(LHmdt) then Exit;
    if not ReadThisValue(UHmdt) then Exit;
    if not ReadThisValue(UAmbTmp) then Exit;
    if not ReadThisValue(UIntTmp) then Exit;

    Close(f);
    if IOResult <> 0 then ; { Clear it }
{$I+}
    ReadOutOfCtrlValues := TRUE;
  end;
end;

procedure TPrintQCObj.PrintReport;
begin
  PrintLabels;
  PrintHdwrStatus;
  CRLineFeed;
  PrintManualChecks;
  ConcatDashes(printStrLen, printStr);
  PrintTheLine;
  PrintCalibration;
  ConcatDashes(printStrLen, printStr);
  PrintTheLine;
  PrintTechInfo;
end;

procedure TPrintQCObj.PrintLabels;
begin
  SR(IDS_TP_LBL, printStr, printStrLen); {TEST PERFORMED}
  ConcatBlanks((QCColOne-StrLen(printStr)), printStr);
  SR(IDS_RESULT_LBL, tempStr, printStrLen); {RESULT}
  StrLCat(printStr, tempStr, printStrLen);
  ConcatBlanks((QCColTwo-StrLen(printStr)), printStr);
  SR(IDS_CM_LBL, tempStr, printStrLen); {CORRECTIVE MEASURE}
  StrLCat(printStr, tempStr, printStrLen);
  PrintTheLine;
  ConcatDashes(printStrLen, printStr);
  PrintTheLine;
end;

procedure TPrintQCObj.PrintHdwrStatus;

  procedure GetHdwrStatus(stat: integer);
  begin
    ConcatBlanks((QCColOne-StrLen(printStr)), printStr);
    if stat = 1 then
      StrLCat(printStr, okStr, printStrLen)
    else if stat = -1 then
      StrLCat(printStr, offStr, printStrLen)
    else
      StrLCat(printStr, nokStr, printStrLen);
    ConcatBlanks((QCColTwo-StrLen(printStr)), printStr);
    ConcatLineChars((printStrLen-StrLen(printStr)), printStr);
    PrintTheLine;
  end;

begin
  SR(IDS_PROCESSOR_CHECK_LBL, printStr, printStrLen);  {PROCESSOR CHECK}
  GetHdwrStatus(hdwrObj^.processor);
  SR(IDS_PROM_CHECKSUM_LBL, printStr, printStrLen);    {PROM CHECKSUM}
  GetHdwrStatus(hdwrObj^.prom);
  SR(IDS_RAM_MEMORY_LBL, printStr, printStrLen);       {RAM MEMORY CHECK}
  GetHdwrStatus(hdwrObj^.ramMem);
  SR(IDS_LIGHT_SOURCE_LBL, printStr, printStrLen);     {LIGHT SOURCE(S)}
  GetHdwrStatus(hdwrObj^.lamp);
  SR(IDS_AD_PHOTO_BOARD_LBL, printStr, printStrLen);   {A/D PHOTO BOARD}
  GetHdwrStatus(hdwrObj^.photoboard);
  SR(IDS_TEMPERATURE_LBL, printStr, printStrLen);      {TEMPERATURE}
  GetHdwrStatus(hdwrObj^.temperature);
  SR(IDS_DISPENSER_LBL, printStr, printStrLen);        {DISPENSER}
  GetHdwrStatus(hdwrObj^.dispenser);
  SR(IDS_MOTOR_LBL, printStr, printStrLen);            {MOTOR MOVEMENTS}
  GetHdwrStatus(hdwrObj^.motor);
  SR(IDS_COMMUNICATIONS_LBL, printStr, printStrLen);   {COMMUNICATIONS}
  GetHdwrStatus(hdwrObj^.comms);
  SR(IDS_FLUOR_SYSTEM_LBL, printStr, printStrLen);     {FLUOROMETRIC SYSTEM}
  GetHdwrStatus(hdwrObj^.fluor);
end;

procedure TPrintQCObj.PrintManualChecks;

  procedure GetManualCheck(strNum: integer);
  begin
    SR(strNum, printStr, printStrLen);
    ConcatBlanks((QCColOne-StrLen(printStr)), printStr);
    ConcatLineChars(6, printStr);
    ConcatBlanks((QCColTwo-StrLen(printStr)), printStr);
    ConcatLineChars((printStrLen-StrLen(printStr)), printStr);
    PrintTheLine;
  end;

begin
  SR(IDS_MANUAL_CHECK_LBL, printStr, printStrLen); {Check Manually:}
  PrintTheLine;
  GetManualCheck(IDS_MC_REAGENT_LEVEL_LBL);
  GetManualCheck(IDS_MC_WATER_LEVEL_LBL);
  GetManualCheck(IDS_MC_REAGENT_LINES_LBL);
  GetManualCheck(IDS_MC_REAGENT_WASTE_LBL);
  GetManualCheck(IDS_MC_FAN_FILTER_LBL);
  GetManualCheck(IDS_MC_FIBER_OPTICS_SHIELD_LBL);
  GetManualCheck(IDS_MC_PHOTODIODE_SHIELD_LBL);
  GetManualCheck(IDS_TEMPERATURE_LBL);
  GetManualCheck(IDS_MC_FLUOR_SHUTTER_LBL);
  GetManualCheck(IDS_MC_FLUOR_REF_DISK_LBL);
end;

procedure TPrintQCObj.PrintCalibration;
var
  printLegend : boolean;
  WACLTCmd    : PWACLTCmd;
begin
  { get the date and time of the last calibration }
  SR(IDS_CAL_TIME_LBL, printStr, printStrLen); {'CALIBRATION TIME: '}
  WACLTCmd := NEW(PWACLTCmd, Init(WAID));
  if WACLTCmd^.SendWAMessage then
  begin
    FormatReadTime(WACLTCmd^.recvMsg^.calDate, tempStr, TRUE);
    ConcatBlanks(2, printStr);
    StrLCat(printStr, tempStr, printStrLen);
  end;
  PrintTheLine;
  MSDisposeObj(WACLTCmd);
  CRLineFeed;
  if WA[WAID].waType = ClassicWA then
    printLegend := PrintClassicWACalibration
  else if (WA[WAID].waType = WA40) or (WA[WAID].waType = WA96) then
    printLegend := PrintWA4096Calibration;
  CRLineFeed;
  if printLegend then
  begin
    SR(IDS_QC_LEGEND1, printStr, printStrLen); {'Values with an asterisk are out of control}
    PrintCenter;
  end;
  SR(IDS_QC_LEGEND2, printStr, printStrLen);
  PrintCenter;
end;

function TPrintQCObj.PrintClassicWACalibration: boolean;
var
  WADACCmd : PWADACCmd;
  WAINTCmd : PWAINTCmd;
  WAAIRCmd : PWAAIRCmd;
  stageTxt : array[0..7] of char;
  i, j,
  stageNum,
  colWidth : integer;
  valStr   : array [0..30] of char;
  legend,
  okSoFar  : boolean;
begin
  PrintClassicWACalibration := FALSE;
  colWidth := 10;
  { Print the gains and dacs for each stage }
  SR(IDS_STAGE_LBL, stageTxt, 7); {STAGE}

  WADACCmd := New(PWADACCmd, Init(WAID));
  WAINTCmd := New(PWAINTCmd, Init(WAID));
  okSoFar := WADACCmd^.SendWAMessage;
  if okSoFar then
  begin
    Move(WADACCmd^.recvMsg^.dacInfo[1], dacs[1], SizeOf(DACArray));
    okSoFar := WAINTCmd^.SendWAMessage;
    if okSoFar then
      Move(WAINTCmd^.recvMsg^.intInfo[1], gains[1], SizeOf(INTArray));
    if WAINTCmd^.WAErrNum <> 0 then
      ShowError(msgParent, IDS_WA_ERR, nil, WAINTCmd^.WAErrNum, MOD_WAQC, 1);
    if WAINTCmd^.WAStatNum <> 0 then
      ShowError(msgParent, IDS_WA_ERR, nil, WAINTCmd^.WAStatNum, MOD_WAQC, 2);
  end;
  if WADACCmd^.WAErrNum <> 0 then
    ShowError(msgParent, IDS_WA_ERR, nil, WADACCmd^.WAErrNum, MOD_WAQC, 3);
  if WADACCmd^.WAStatNum <> 0 then
    ShowError(msgParent, IDS_WA_ERR, nil, WADACCmd^.WAStatNum, MOD_WAQC, 4);
  MSDisposeObj(WADACCmd);
  MSDisposeObj(WAINTCmd);
  if not okSoFar then
    Exit;

  for i := 0 to 3 do
  begin
    stageNum := i;
    { STAGE: number }
    StrCopy(printStr, stageTxt);
    ConcatBlanks(1, printStr);
    Str(stageNum, valStr);
    StrLCat(printStr, valStr, printStrLen);
    ConcatBlanks(colWidth-StrLen(printStr), printStr);

    { Column One gain and dac values }
    legend := GetGainValue(colWidth, stageNum +1);
    if legend then PrintClassicWACalibration := TRUE;
    legend := GetDacValue(colWidth,(stageNum*2)+2, FALSE);
    if legend then PrintClassicWACalibration := TRUE;
    legend := GetDacValue(colWidth,(stageNum*2)+1, TRUE);
    if legend then PrintClassicWACalibration := TRUE;

    stageNum := stageNum + 4;
    { STAGE: number }
    ConcatBlanks(3, printStr);
    StrLCat(printStr, stageTxt, printStrLen);
    ConcatBlanks(1, printStr);
    Str(stageNum, valStr);
    StrLCat(printStr, valStr, printStrLen);
    ConcatBlanks(colWidth-(StrLen(stageTxt)+StrLen(valStr)+1), printStr);

    { Column two gain and dac values }
    legend := GetGainValue(colWidth, stageNum +1);
    if legend then PrintClassicWACalibration := TRUE;
    legend := GetDacValue(colWidth, (stageNum*2)+2, FALSE);
    if legend then PrintClassicWACalibration := TRUE;
    legend := GetDacValue(colWidth, (stageNum*2)+1, TRUE);
    if legend then PrintClassicWACalibration := TRUE;

    PrintCenter;
  end;

  CRLineFeed;

  { print the 96 diode values }
  colWidth := 9;
  okSoFar := TRUE;
  WAAIRCmd := NEW(PWAAIRCmd, Init(WAID));
  for i := 0 to 7 do
  begin
    if okSoFar then
    begin
      WAAIRCmd^.SetParams(i);
      okSoFar := WAAIRCmd^.SendWaMessage;
      if okSoFar then
      begin
        for j := 1 to 12 do
        begin
          airs[j] := longint(integer(WAAIRCmd^.recvMsg^.CDIInfo[j]));
          legend := GetAirValue(colWidth, j);
          if legend then PrintClassicWACalibration := TRUE;
        end;
        PrintCenter;
      end;
      if WAAIRCmd^.WAErrNum <> 0 then
        ShowError(msgParent, IDS_WA_ERR, nil, WAAIRCmd^.WAErrNum, MOD_WAQC, 5);
      if WAAIRCmd^.WAStatNum <> 0 then
        ShowError(msgParent, IDS_WA_ERR, nil, WAAIRCmd^.WAStatNum, MOD_WAQC, 6);
    end;
  end;
  MSDisposeObj(WAAIRCmd);
  CRLineFeed;
end;

function TPrintQCObj.PrintWA4096Calibration: boolean;
var
  colWidth, i : integer;
  filter      : byte;
  valStr      : array [0..30] of char;
  legend      : boolean;

  function GetWA4096CalInfo: boolean;
  var
    WAJRGainsCmd    : PWAJRGainsCmd;
    WAUsedOfsCmd    : PWAUsedOfsCmd;
    WAJRAirCmd      : PWAJRAirCmd;
    WAUsedVoltsCmd  : PWAUsedVoltsCmd;
    WAJrFluorCmd    : PWAJRFluorCmd;
    WAJrAdSysCmd    : PWAJrAdSysCmd;
    WAJrEnvStatCmd  : PWAJrEnvStatCmd;
    waStat, waError : integer;
    loc             : integer;
  begin
    GetWA4096CalInfo := TRUE;
    waStat := 0;
    waError := 0;
    loc := 10;
    WAJrGainsCmd := New(PWAJrGainsCmd, Init(WAID));
    if WAJRGainsCmd^.SendWAMessage then
    begin
      Move(WAJrGainsCmd^.recvMsg^.jrGains[1], jrGains[1], SizeOf(JRGnArray));
      Inc(loc, 2);
      WAUsedOfsCmd := New(PWAUsedOfsCmd, Init(WAID));
      if WAUsedOfsCmd^.SendWAMessage then
      begin
        Move(WAUsedOfsCmd^.recvMsg^.jrOffsts[1], jrOffsets[1], SizeOf(UsdOArray));
        Inc(loc, 2);
        WAJRAirCmd := New(PWAJRAirCmd, Init(WAID));
        if WAJRAirCmd^.SendWAMessage then
        begin
          Move(WAJRAirCmd^.recvMsg^.jrAirs[1], jrAirs[1], SizeOf(JrAirArray));
          Inc(loc, 2);
          WAUsedVoltsCmd := New(PWAUsedVoltsCmd, Init(WAID));
          if WAUsedVoltsCmd^.SendWAMessage then
          begin
            Move(WAUsedVoltsCmd^.recvMsg^.jrDacs[1], jrDacs[1], SizeOf(JrDacArray));
            Inc(loc, 2);
            WAJrFluorCmd := New(PWAJrFluorCmd, Init(WAID));
            if WAJrFluorCmd^.SendWAMessage then
            begin
              Move(WAJrFluorCmd^.recvMsg^.jrMisc[1], jrFluor[1], SizeOf(JrMscArray));
              Inc(loc, 2);
              WAJrAdSysCmd := New(PWAJrAdSysCmd, Init(WAID));
              if WAJrAdSysCmd^.SendWAMessage then
              begin
                Move(WAJrAdSysCmd^.recvMsg^.jrMisc[1], jrAdSys[1], SizeOf(JrMscArray));
                Inc(loc, 2);
                WAJrEnvStatCmd := New(PWAJrEnvStatCmd, Init(WAID));
                if WAJrEnvStatCmd^.SendWAMessage then
                begin
                  Move(WAJrEnvStatCmd^.recvMsg^.jrMisc[1], jrEnvStat[1], SizeOf(JrMscArray));
                end
                else
                  waError := WAJrEnvStatCmd^.WAErrNum;
                if waStat = 0 then
                  waStat := WAJrEnvStatCmd^.WAStatNum;
                MSDisposeObj(WAJrEnvStatCmd);
              end
              else
                waError := WAJrAdSysCmd^.WAErrNum;
              if waStat = 0 then
                waStat := WAJrAdSysCmd^.WAStatNum;
              MSDisposeObj(WAJrAdSysCmd);
            end
            else
              waError := WAJrFluorCmd^.WAErrNum;
            if waStat = 0 then
              waStat := WAJrFluorCmd^.WAStatNum;
            MSDisposeObj(WAJrFluorCmd);
          end
          else
            waError := WAUsedVoltsCmd^.WAErrNum;
          if waStat = 0 then
            waStat := WAUsedVoltsCmd^.WAStatNum;
          MSDisposeObj(WAUsedVoltsCmd);
        end
        else
          waError := WAJRAirCmd^.WAErrNum;
        if waStat = 0 then
          waStat := WAJRAirCmd^.WAStatNum;
        MSDisposeObj(WAJRAirCmd);
      end
      else
        waError := WAUsedOfsCmd^.WAErrNum;
      if waStat = 0 then
        waStat := WAUsedOfsCmd^.WAStatNum;
      MSDisposeObj(WAUsedOfsCmd);
    end
    else
      waError := WAJrGainsCmd^.WAErrNum;
    if waStat = 0 then
      waStat := WAJrGainsCmd^.WAStatNum;
    MSDisposeObj(WAJrGainsCmd);

    if waStat <> 0 then
    begin
      ShowError(msgParent, IDS_WA_ERR, nil, waStat, MOD_WAQC, loc+1);
      GetWA4096CalInfo := FALSE;
    end;
    if waError <> 0 then
    begin
      ShowError(msgParent, IDS_WA_ERR, nil, waError, MOD_WAQC, loc);
      GetWA4096CalInfo := FALSE;
    end;
  end;

begin
  PrintWA4096Calibration := FALSE;
  if not GetWA4096CalInfo then
    Exit;
  colWidth := 6;
  ConcatBlanks(40, printStr);
  SR(IDS_COLOR_CAL_LBL, tempStr, printStrLen); {'Colormetric Calibration'}
  StrLCat(printStr, tempStr, printStrLen);
  PrintTheLine;

  SR(IDS_FILTER_DIODE_LBL, printStr, printStrLen); {'Filter  |Diode ->'}
  for i := 0 to 11 do
  begin
    Str(i, valStr);
    ConcatBlanks(LabelWidth + (colWidth*(i+1)) - StrLen(valStr) - StrLen(printStr), printStr);
    StrLCat(printStr, valStr, printStrLen);
  end;
  StrLCat(printStr, ' ', printStrLen);
  SR(IDS_FLUOR_CAL_LBL, tempStr, printStrLen);  {'Fluorometer Calibration'}
  StrLCat(printStr, tempStr, printStrLen);
  PrintTheLine;

  ConcatDashes(printStrLen, printStr);
  PrintTheLine;

  lineNumber := 1;
  for filter := 1 to 6 do
  begin
    legend := GetFilterGain(filter, colWidth);
    if legend then PrintWA4096Calibration := TRUE;
    legend := PrintFluorometerCalibration;
    if legend then PrintWA4096Calibration := TRUE;
    Inc(lineNumber);
    legend := GetFilterOffset(filter, colWidth);
    if legend then PrintWA4096Calibration := TRUE;
    legend := PrintFluorometerCalibration;
    if legend then PrintWA4096Calibration := TRUE;
    Inc(lineNumber);
    legend := GetFilterAir(filter, colWidth);
    if legend then PrintWA4096Calibration := TRUE;
    legend := PrintFluorometerCalibration;
    if legend then PrintWA4096Calibration := TRUE;
    Inc(lineNumber);
    legend := GetFilterDac(filter, colWidth);
    if legend then PrintWA4096Calibration := TRUE;
    legend := PrintFluorometerCalibration;
    if legend then PrintWA4096Calibration := TRUE;
    Inc(lineNumber);
  end;
end;

procedure TPrintQCObj.PrintTechInfo;
begin
  CRLineFeed;
  SR(IDS_TECH_LBL, printStr, printStrLen); {Tech:}
  ConcatLineChars(50, printStr);
  PrintTheLine;
end;

function TPrintQCObj.GetGainValue(colWidth, gainIdx: integer): boolean;
var
  valStr       : array [0..30] of char;
  tempLong     : longint;
  tempWord     : word;
  outOfControl : boolean;
begin
  tempWord := word(gains[gainIdx]);
  tempLong := tempWord;
  Str(tempLong, valStr);
  outOfControl := ValidateGainValue(gainIdx-1, tempLong);
  if outOfControl then
    StrCopy(tempStr, '*')
  else
    StrCopy(tempStr, ' ');
  ConcatBlanks(colWidth-StrLen(valStr)-1, tempStr);    { right justify value }
  StrCat(tempStr, valStr);
  StrLCat(printStr, tempStr, printStrLen);
  GetGainValue := outOfControl;
end;

function TPrintQCObj.GetDacValue(colWidth, dacIdx: integer; checkCourse: boolean): boolean;
var
  valStr       : array [0..30] of char;
  tempLong     : longint;
  tempWord     : word;
  outOfControl : boolean;
begin
  GetDacValue := FALSE;
  tempWord := word(dacs[dacIdx]);
  tempLong := tempWord;
  Str(tempLong, valStr);
  if checkCourse then
    outOfControl := ValidateDacValue(tempLong)
  else
    outOfControl := FALSE;
  if outOfControl then
    StrCopy(tempStr, '*')
  else
    StrCopy(tempStr, ' ');
  ConcatBlanks(colWidth-StrLen(valStr)-1, tempStr);    { right justify value }
  StrCat(tempStr, valStr);
  StrLCat(printStr, tempStr, printStrLen);
  GetDacValue := outOfControl;
end;

function TPrintQCObj.ValidateGainValue(stage: integer; gainVal: longint): boolean;
begin
  case stage of
    0, 1, 2, 3 : ValidateGainValue := (gainVal < ITime0Lo) or (gainVal > ITime0Hi);
    4, 5       : ValidateGainValue := (gainVal < ITime4Lo) or (gainVal > ITime4Hi);
    6          : ValidateGainValue := (gainVal < ITime6Lo) or (gainVal > ITime6Hi);
    7          : ValidateGainValue := (gainVal < ITime7Lo) or (gainVal > ITime7Hi);
    else
      ValidateGainValue := TRUE;  { returns out of control }
  end;
end;

function TPrintQCObj.ValidateDacValue(dacVal: longint): boolean;
begin
  ValidateDacValue := (dacVal < CrsDacLo) or (dacVal > CrsDacHi);
end;

function TPrintQCObj.GetAirValue(colWidth, airIdx: integer): boolean;
var
  valStr       : array [0..30] of char;
  outOfControl : boolean;
begin
  GetAirValue := FALSE;
  Str(airs[airIdx], valStr);
  outOfControl := ValidateAirValue(longint(airs[airIdx]));
  if outOfControl then
    StrCopy(tempStr, '*')
  else
    StrCopy(tempStr, ' ');
  ConcatBlanks(colWidth-StrLen(valStr)-1, tempStr);    { right justify value }
  StrCat(tempStr, valStr);
  StrLCat(printStr, tempStr, printStrLen);
  GetAirValue := outOfControl;
end;

function TPrintQCObj.ValidateAirValue(airVal: longint): boolean;
begin
  ValidateAirValue := (airVal < DiodeLo) or (airVal > DiodeHi);
end;

function TPrintQCObj.PrintFluorometerCalibration: boolean;
const
  TextWidth : integer = 15;
  FcolWidth : integer = 8;
var
  valStr    : array [0..30] of char;
  bad, skip : boolean;

  procedure InitLabel(id: integer);
  begin
    SR(id, tempStr, 64);
    ConcatBlanks(TextWidth-StrLen(tempStr), tempStr);
    StrLCat(printStr, tempStr, printStrLen);
  end;

begin
  bad := FALSE;
  skip := FALSE;
  valStr[0] := #0;
  case lineNumber of
    1  : begin
           InitLabel(IDS_CAL_DARK_LBL); { Dark:}
           bad := (word(jrFluor[1]) > UDark);
           Str(word(jrFluor[1]), valStr);
           if hdwrObj^.fluor = -1 then
           begin
             bad := FALSE;
             StrLCopy(valStr, offStr, SizeOf(valStr)-1);
           end;
         end;
    2  : begin
           InitLabel(IDS_CAL_REF_DISK_LBL); {Reference Disk:}
           bad := (word(jrFluor[2]) < LRef) or (word(jrFluor[2]) > URef);
           Str(word(jrFluor[2]), valStr);
           if hdwrObj^.fluor = -1 then
           begin
             bad := FALSE;
             StrLCopy(valStr, offStr, SizeOf(valStr)-1);
           end;
         end;
    3  : begin
           InitLabel(IDS_CAL_PMT_LBL); {PMT Voltage:}
           { sign extends the integer into a long integer }
           bad := (longint(jrFluor[3]) < LPmt) or (longint(jrFluor[3]) > UPmt);
           Str(jrFluor[3]:6, valStr);
           { hack to insert assumed decimal }
           StrCat(valStr, '.');
           valStr[StrLen(valStr)-1] := valStr[StrLen(valStr)-2];
           valStr[StrLen(valStr)-2] := '.';
           StrCat(valStr, 'V');
           if hdwrObj^.fluor = -1 then
           begin
             bad := FALSE;
             StrLCopy(valStr, offStr, SizeOf(valStr)-1);
           end;
         end;
    4  : begin
           InitLabel(IDS_CAL_FLUOR_LAMP_LBL); {Fluoro Lamp Voltage}
           bad := (word(jrFluor[4]) < LLamp) or (word(jrFluor[4]) > ULamp);
           Str((jrFluor[4]/100):7:2, valStr);
           StrCat(valStr, 'V');
           if hdwrObj^.fluor = -1 then
           begin
             bad := FALSE;
             StrLCopy(valStr, offStr, SizeOf(valStr)-1);
           end;
         end;
    5  : skip := TRUE;
    6  : begin
           InitLabel(IDS_COLOR_CAL_LBL); {Colormetric Calibration}
           skip := TRUE;
         end;
    7  : begin
           ConcatDashes(printStrLen-StrLen(printStr), printStr);
           skip := TRUE;
         end;
    8  : begin
           InitLabel(IDS_CAL_COLOR_LAMP_LBL); {Color Lamp Voltage:}
           bad := (word(jrFluor[5]) < LCLamp) or (word(jrFluor[5]) > UCLamp);
           Str((jrFluor[5]/100):7:2, valStr);
           StrCat(valStr, 'V');
         end;
    9  : skip := TRUE;
    10 : begin
           InitLabel(IDS_CAL_AD_SYSTEM_LBL); {A/D System Calibration}
           skip := TRUE;
         end;
    11 : begin
           ConcatDashes(printStrLen-StrLen(printStr), printStr);
           skip := TRUE;
         end;
    12 : begin
           InitLabel(IDS_CAL_0V_REF1_LBL); {OV Reference #1}
           bad := (word(jrAdSys[1]) < L0VRef1) or (word(jrAdSys[1]) > U0VRef1);
           Str(word(jrAdSys[1]), valStr);
         end;
    13 : begin
           InitLabel(IDS_CAL_25_REF1_LBL); {2.5V Reference #1}
           bad := (word(jrAdSys[2]) < L2VRef1) or (word(jrAdSys[2]) > U2VRef1);
           Str(word(jrAdSys[2]), valStr);
         end;
    14 : begin
           InitLabel(IDS_CAL_0V_REF2_LBL); {OV Reference #2}
           bad := (word(jrAdSys[3]) < L0VRef2) or (word(jrAdSys[3]) > U0VRef2);
           Str(word(jrAdSys[3]), valStr);
         end;
    15 : begin
           InitLabel(IDS_CAL_25_REF2_LBL); {2.5V Reference #2}
           bad := (word(jrAdSys[4]) < L2VRef2) or (word(jrAdSys[4]) > U2VRef2);
           Str(word(jrAdSys[4]), valStr);
         end;
    16 : skip := TRUE;
    17 : begin
           InitLabel(IDS_ENVIRON_LBL); {Environment Status}
           skip := TRUE;
         end;
    18 : begin
           ConcatDashes(printStrLen-StrLen(printStr), printStr);
           skip := TRUE;
         end;
    19 : begin
           InitLabel(IDS_ENV_TEMP_LBL); {Temperature}
           bad := (word(jrEnvStat[1]) < LTemp) or (word(jrEnvStat[1]) > UTemp);
           Str((jrEnvStat[1]/100):7:2, valStr);
           StrCat(valStr, 'C');
         end;
    20 : begin
           InitLabel(IDS_ENV_HUMIDITY_LBL); {Humidity}
           bad := (word(jrEnvStat[2]) < LHmdt) or (word(jrEnvStat[2]) > UHmdt);
           Str((jrEnvStat[2]/100):7:2, valStr);
           StrCat(valStr, 'HR');
         end;
    21 : begin
           InitLabel(IDS_ENV_AMB_TEMP_LBL); {Ambient Temp}
           bad := (word(jrEnvStat[3]) > UAmbTmp);
           Str((jrEnvStat[3]/100):7:2, valStr);
           StrCat(valStr, 'C');
         end;
    22 : begin
           InitLabel(IDS_ENV_INT_TEMP_LBL); {Internal Temp}
           bad := (word(jrEnvStat[4]) > UIntTmp);
           Str((jrEnvStat[4]/100):7:2, valStr);
           StrCat(valStr, 'C');
         end;
  end; { case }
  if not skip then
  begin
    if bad then
      StrCopy(tempStr, '*')
    else
      StrCopy(tempStr, ' ');
    ConcatBlanks(FcolWidth-StrLen(valStr)-1, tempStr);    { right justify value }
    StrCat(tempStr, valStr);
    StrLCat(printStr, tempStr, printStrLen);
  end;
  PrintTheLine;
  PrintFluorometerCalibration := bad;
end;

function TPrintQCObj.GetFilterGain(filter: byte; colWidth: integer): boolean;
var
  i        : integer;
  valStr   : array [0..30] of char;
  tempWord : word;
  tempLong : longint;
  bad      : boolean;
begin
  GetFilterGain := FALSE;
  case filter of
    1 : SR(IDS_FILTER_GAIN_1_LBL, printStr, printStrLen); {'440 nm  |Gain:    '}
    2 : SR(IDS_FILTER_GAIN_2_LBL, printStr, printStrLen); {'620 nm  |Gain:    '}
    3 : SR(IDS_FILTER_GAIN_3_LBL, printStr, printStrLen); {'590 nm  |Gain:    '}
    4 : SR(IDS_FILTER_GAIN_4_LBL, printStr, printStrLen); {'560 nm  |Gain:    '}
    5 : SR(IDS_FILTER_GAIN_5_LBL, printStr, printStrLen); {'505 nm  |Gain:    '}
    6 : SR(IDS_FILTER_GAIN_6_LBL, printStr, printStrLen); {'405 nm  |Gain:    '}
  end; { case }
  ConcatBlanks(LabelWidth-StrLen(printStr), printStr);
  for i := (((filter-1)*12)+1) to (((filter-1)*12)+12) do
  begin
    tempWord := word(jrGains[i]);
    tempLong := tempWord;
    Str(tempLong, valStr);
    case filter of
      1 : bad := (tempLong < LGnFlt1) or (tempLong > UGnFlt1);
      2 : bad := (tempLong < LGnFlt2) or (tempLong > UGnFlt2);
      3 : bad := (tempLong < LGnFlt3) or (tempLong > UGnFlt3);
      4 : bad := (tempLong < LGnFlt4) or (tempLong > UGnFlt4);
      5 : bad := (tempLong < LGnFlt5) or (tempLong > UGnFlt5);
      6 : bad := (tempLong < LGnFlt6) or (tempLong > UGnFlt6);
    end; { case }
    if bad then
    begin
      GetFilterGain := TRUE;
      StrCopy(tempStr, '*');
    end
    else
      StrCopy(tempStr, ' ');
    ConcatBlanks(colWidth-StrLen(valStr)-1, tempStr);
    StrCat(tempStr, valStr);
    StrLCat(printStr, tempStr, printStrLen);
  end;
  ConcatBlanks(1, printStr);
end;

function TPrintQCObj.GetFilterOffset(filter: byte; colWidth: integer): boolean;
var
  i        : integer;
  valStr   : array [0..30] of char;
  tempLong : longint;
  bad      : boolean;
begin
  GetFilterOffset := FALSE;
  SR(IDS_FILTER_OFFSET_LBL, printStr, printStrLen); {'        |Offset:  '}
  ConcatBlanks(LabelWidth-StrLen(printStr), printStr);
  for i := (((filter-1)*12)+1) to (((filter-1)*12)+12) do
  begin
    tempLong := longint(jrOffsets[i]);
    Str(tempLong, valStr);
    case jrGains[i] of
      0 : bad := (tempLong > UOffGn0);
      1 : bad := (tempLong > UOffGn1);
      2 : bad := (tempLong > UOffGn2);
      3 : bad := (tempLong > UOffGn3);
      4 : bad := (tempLong > UOffGn4);
      5 : bad := (tempLong > UOffGn5);
      6 : bad := (tempLong > UOffGn6);
      7 : bad := (tempLong > UOffGn7);
    end; { case }
    if bad then
    begin
      GetFilterOffset := TRUE;
      StrCopy(tempStr, '*');
    end
    else
      StrCopy(tempStr, ' ');
    ConcatBlanks(colWidth-StrLen(valStr)-1, tempStr);
    StrCat(tempStr, valStr);
    StrLCat(printStr, tempStr, printStrLen);
  end;
  ConcatBlanks(1, printStr);
end;

function TPrintQCObj.GetFilterAir(filter: byte; colWidth: integer): boolean;
var
  i        : integer;
  valStr   : array [0..30] of char;
  tempLong : longint;
  tempOfs  : longint;
begin
  GetFilterAir := FALSE;
  SR(IDS_FILTER_AIR_LBL, printStr, printStrLen); {'        |Air:  '}
  ConcatBlanks(LabelWidth-StrLen(printStr), printStr);
  for i := (((filter-1)*12)+1) to (((filter-1)*12)+12) do
  begin
    tempLong := longint(jrAirs[i]);
    tempOfs := longint(jrOffsets[i]);
    Str(tempLong, valStr);
    if ((tempLong+tempOfs) < lAir) or ((tempLong+tempOfs) > UAir) then
    begin
      GetFilterAir := TRUE;
      StrCopy(tempStr, '*');
    end
    else
      StrCopy(tempStr, ' ');
    ConcatBlanks(colWidth-StrLen(valStr)-1, tempStr);
    StrCat(tempStr, valStr);
    StrLCat(printStr, tempStr, printStrLen);
  end;
  ConcatBlanks(1, printStr);
end;

function TPrintQCObj.GetFilterDac(filter: byte; colWidth: integer): boolean;
var
  i        : integer;
  valStr   : array [0..30] of char;
  tempWord : word;
  tempLong : longint;
begin
  GetFilterDac := FALSE;
  SR(IDS_FILTER_DAC_LBL, printStr, printStrLen); {'        |DAC:  '}
  ConcatBlanks(LabelWidth-StrLen(printStr), printStr);
  for i := (((filter-1)*12)+1) to (((filter-1)*12)+12) do
  begin
    tempWord := word(jrDacs[i]);
    tempLong := tempWord;
    Str(tempLong, valStr);
    if (tempLong < LOffDAC) or (tempLong > UOffDAC) then
    begin
      GetFilterDac := TRUE;
      StrCopy(tempStr, '*');
    end
    else
      StrCopy(tempStr, ' ');
    ConcatBlanks(colWidth-StrLen(valStr)-1, tempStr);
    StrCat(tempStr, valStr);
    StrLCat(printStr, tempStr, printStrLen);
  end;
  ConcatBlanks(1, printStr);
end;

procedure TPrintQCObj.ConcatLineChars(howMany: integer; newStr: PChar);
{************************************************************************}
{*  Concats the specified number of chars onto the end of the printStr  *}
{************************************************************************}
begin
  if howMany <= 0 then
    Exit;

  Inc(howMany, StrLen(newStr));
  if howMany >= printStrLen then
    howMany := printStrLen;
  Pad(newStr, newStr, '_', howMany);
end;


{ EXPORTED PROCEDURES }

procedure PrintQC(aParent: PWindowsObject);
{**************************************************************************}
{**************************************************************************}
var
  qcObj : PPrintQCObj;
  wc    : PWaitCursor;
begin
  qcObj := New(PPrintQCObj, Init(aParent));
  if qcObj = nil then
  begin
    ShowError(aParent, IDS_WAQC_ERR, nil, MOD_WAQC+1, MOD_WAQC, 0);
    Exit;
  end;
  qcObj^.abortPrinting := TRUE;

  wc := New(PWaitCursor, Init);
  if qcObj^.ReadOutOfCtrlValues then
  begin
    qcObj^.abortPrinting := FALSE;
    qcObj^.InitHeadings;
    qcObj^.PrintReport;
  end
  else
    ShowError(aParent, IDS_WAQC_ERR, nil, MOD_WAQC+2, MOD_WAQC, 0);

  MSDisposeObj(wc);
  MSDisposeObj(qcObj);
end;


END.
