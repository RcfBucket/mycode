{----------------------------------------------------------------------------}
{  Module Name  : EDFilter.PAS                                               }
{  Programmer   : Steven Mercier                                             }
{  Date Created : May, 21, 1998                                              }
{  Requirements : S3-23 [ref. a]                                             }
{                                                                            }
{  Purpose - This module provides a filter for the elimination of duplicate  }
{    isolates in epidemiology reports. See S3-23 design document for details.}
{                                                                            }
{  Assumptions -                                                             }
{    Number of isolates into filter is limited to MaxCollectionSize.         }
{                                                                            }
{  Referenced Documents                                                      }
{    a. S3.23 Design Document, Revision 1                                    }
{                                                                            }
{  Revision History - This project is under version control, use it to view  }
{    revision history.                                                       }
{                                                                            }
{----------------------------------------------------------------------------}

unit EDFilter;

INTERFACE

uses
  DBTypes,
  Objects;

type

  {--------------------------------------------------------------------------}
  { Implementation Object - do not use externally
  {
  { This object holds a collection of unique TSeqRefObj objects. It's the
  { same as TSortedCollection except a new member added to the collection
  { replaces an existing member with the same key, which in this case it
  { the SeqNum.
  {--------------------------------------------------------------------------}
  PSeqObjList = ^TSeqObjList;
  TSeqObjList = object( TSortedCollection )
    private
    function Compare( Key1, Key2: Pointer ): integer; virtual;
  end;



  {--------------------------------------------------------------------------}
  { Implementation Object - do not use externally
  {
  { This object holds a date sorted collection of TIsoListObj objects.
  {--------------------------------------------------------------------------}
  PDatedSeqObjList = ^TDatedSeqObjList;
  TDatedSeqObjList = object( TSortedCollection )
    private
    function Compare( Key1, Key2: Pointer ): integer; virtual;
  end;



  {--------------------------------------------------------------------------}
  { This is the filter object. To use it, call the constructor, insert data
  { by calling Input for each isolate to be filtered, call Process to do the
  { filtering, then extract the output by calling Output until it returns
  { false, then call Done. Any other variations in use will raise an
  { assertion.
  {
  { Note: pOrgamisms, pPatients, and pSources are intermediate objects.
  { Because they are destroyed as soon as the data is processed, the
  { wasProcessed flag is used to indicate their existance to the destructor.
  {--------------------------------------------------------------------------}
  PEDFilterObj = ^TEDFilterObj;
  TEDFilterObj = object
    constructor Init;
    destructor Done;
    procedure Input( IsoSeq: TSeqNum );
    procedure Process;
    function Output( var IsoSeq: TSeqNum ): boolean;
    function PercentOutput: integer;
    procedure Dump( message: PChar );
    private
    pOrganisms,                { list of organisms referenced by the inputs }
    pPatients,                 { list of patients referenced by the inputs }
    pSources: PSeqObjList;     { list of sources referenced by the inputs }
    pIsos: PDatedSeqObjList;   { list of isolates, input/output }
    outputIndex: integer;      { index to element in pIsos used by Output }
    wasProcessed: boolean;     { true when inputs have been processed (see note) }
  end;



IMPLEMENTATION

uses
  DateLib,
  DBIDs,
  DBLib,
  DBFile,
  EpiCnfg,
  IntlLib,
  MScan,
  OWindows,
  Strings,
  WinTypes;

type

  {--------------------------------------------------------------------------}
  { This object holds a sequence reference as an object for use with a
  { list collection.
  {--------------------------------------------------------------------------}
  PSeqRefObj = ^TSeqRefObj;
  TSeqRefObj = object( TObject )
    SeqNum: TSeqNum;
    constructor Init( SeqRef: TSeqNum );
  end;



  {--------------------------------------------------------------------------}
  { This object holds isolate data for use with the isolate list collection.
  {--------------------------------------------------------------------------}
  PIsoListObj = ^TIsoListObj;
  TIsoListObj = object( TSeqRefObj )
    Date: TIntlDate;
    Patient,
    Source,
    Organism: TSeqNum;
    DeleteFlag: boolean;
    constructor Init( SeqRef: TSeqNum; aDate: TIntlDate; aPatient,
      aSource, aOrganism: TSeqNum );
  end;



{--------------------------------------------------------------------------}
{ This procedure handles assertions for this unit. If an assertion is
{ false, it alerts the user and halts the program. Otherwise, it does
{ nothing.
{--------------------------------------------------------------------------}

procedure Assert( flag: boolean; localErrorNum: integer );
begin
  if not flag then
  begin
    MessageBox( 0, 'EDFILTER.PAS', 'Error', MB_OK );
    Halt;
  end;
end;


{----------------------------------------------------------------------------}
{ This function programs the sort for this list. Currently, this is only
{ used to eliminate duplicate entries in the list.
{----------------------------------------------------------------------------}

function TSeqObjList.Compare( Key1, Key2: Pointer ): integer;
begin
  if PSeqRefObj( Key1 )^.SeqNum > PSeqRefObj( Key2 )^.SeqNum then
    Compare := 1
  else if PSeqRefObj( Key1 )^.SeqNum = PSeqRefObj( Key2 )^.SeqNum then
    Compare := 0
  else
    Compare := -1;
end;



{----------------------------------------------------------------------------}
{ This function programs the sort for this list. This list must be sorted by
{ date.
{
{ Assumes keys are pointers to a PIsoListObj type
{----------------------------------------------------------------------------}

function TDatedSeqObjList.Compare( Key1, Key2: Pointer ): integer;
begin
  Compare := CompareDates( PIsoListObj( Key1 )^.Date,
    PIsoListObj( Key2 )^.Date );
end;



{----------------------------------------------------------------------------}
{ This constructor initializes a sequence reference object.
{----------------------------------------------------------------------------}

constructor TSeqRefObj.Init( SeqRef: TSeqNum );
begin
  SeqNum := SeqRef;
end;



{----------------------------------------------------------------------------}
{ This constructor initializes a dated sequence reference object.
{----------------------------------------------------------------------------}

constructor TIsoListObj.Init( SeqRef: TSeqNum; aDate: TIntlDate;
  aPatient, aSource, aOrganism: TSeqNum );
begin
  inherited Init( SeqRef );
  Date := aDate;
  Patient := aPatient;
  Source := aSource;
  Organism := aOrganism;
  DeleteFlag := false;
end;



{----------------------------------------------------------------------------}
{ This constructor allocates the lists for eliminating duplicates.
{----------------------------------------------------------------------------}

constructor TEDFilterObj.Init;
begin
  pOrganisms := New( PSeqObjList, Init( 100, 100 ) );
  pPatients := New( PSeqObjList, Init( 100, 100 ) );
  pSources := New( PSeqObjList, Init( 10, 5 ) );
  pIsos := New( PDatedSeqObjList, Init( 1000, 1000 ) );
  pIsos^.Duplicates := true;
  outputIndex := 0;
  wasProcessed := false;
end;



{----------------------------------------------------------------------------}
{ This destructor deallocates the remaing objects.
{----------------------------------------------------------------------------}

destructor TEDFilterObj.Done;
begin

  {---- The intermediate objects must be disposed here if the data was not
        processed. }
  if not wasProcessed then
  begin
    Dispose( pOrganisms, Done );
    Dispose( pPatients, Done );
    Dispose( pSources, Done );
  end;

  {---- Dispose the output object }
  Dispose( pIsos, Done );
end;



{----------------------------------------------------------------------------}
{ This procedure dumps the contents of the object into a dialog box. Provided
{ for test and debug use.
{----------------------------------------------------------------------------}

procedure TEDFilterObj.Dump( message: PChar );
var
  s1, s2: array [0..80] of char;
begin
  Str( pIsos^.Count, s1 );
  StrCat( s1, ' isolates' );
  if not wasProcessed then
  begin
    StrCat( s1, ', ' );
    Str( pOrganisms^.Count, s2 );
    StrCat( s1, s2 );
    StrCat( s1, ' organisms, ' );
    Str( pPatients^.Count, s2 );
    StrCat( s1, s2 );
    StrCat( s1, ' patients, ' );
    Str( pSources^.Count, s2 );
    StrCat( s1, s2 );
    StrCat( s1, ' sources' );
  end;
  MessageBox( 0, s1, message, 0 );
end;


{----------------------------------------------------------------------------}
{ This procedure inserts an input datum into the the filter.
{----------------------------------------------------------------------------}

procedure TEDFilterObj.Input( IsoSeq: TSeqNum );

var
  pIsolate: PDBRec;

  {--------------------------------------------------------------------------}
  { This function reads the organism reference from the isolate record.
  {
  { Assert: pIsolate points to the isolate record.
  {--------------------------------------------------------------------------}
  function getOrgSeq: TSeqNum;
  var
    OrgSeq: TSeqNum;
  begin
    pIsolate^.GetField( DBISOOrgRef, @OrgSeq, SizeOf( OrgSeq ) );
    getOrgSeq := OrgSeq;
  end;

  {--------------------------------------------------------------------------}
  { This function reads the patient reference from the specimen record of
  { the isolate record.
  {
  { Assert: pIsolate points to the isolate record.
  {--------------------------------------------------------------------------}
  function getPatSeq: TSeqNum;
  var
    SpecSeq,
    PatSeq: TSeqNum;
    pSpeciment: PDBRec;
    dbSpec: PDBFile;
  begin
    dbSpec:= New( PDBFile, Init( DBSpecFile, '', dbOpenNormal ) );
    Assert( dbSpec <> nil, 1 );
    pIsolate^.GetField( DBISOSpecRef, @SpecSeq, SizeOf( SpecSeq ) );
    if dbSpec^.dbc^.GetSeq( dbSpec^.dbr, SpecSeq ) then
    begin
      dbSpec^.dbr^.GetField( DBSPECPatRef, @PatSeq, SizeOf( PatSeq ) );
      getPatSeq := PatSeq;
    end
    else
      getPatSeq := 0;
    MSDisposeObj( dbSpec );
  end;

  {--------------------------------------------------------------------------}
  { This function reads the source reference from the specimen record of
  { the isolate record.
  {
  { Assert: pIsolate points to the isolate record.
  {--------------------------------------------------------------------------}
  function getSourceSeq: TSeqNum;
  var
    SpecSeq,
    SourceSeq: TSeqNum;
    pSpeciment: PDBRec;
    dbSpec: PDBFile;
  begin
    dbSpec:= New( PDBFile, Init( DBSpecFile, '', dbOpenNormal ) );
    Assert( dbSpec <> nil, 2 );
    pIsolate^.GetField( DBISOSpecRef, @SpecSeq, SizeOf( SpecSeq ) );
    if dbSpec^.dbc^.GetSeq( dbSpec^.dbr, SpecSeq ) then
    begin
      dbSpec^.dbr^.GetField( DBSPECSrc, @SourceSeq, SizeOf( SourceSeq ) );
      getSourceSeq := SourceSeq;
    end
    else
      getSourceSeq := 0;
    MSDisposeObj( dbSpec );
  end;

  {--------------------------------------------------------------------------}
  { This function accesses the specimen collect date field in the isolate record.
  {
  { Assumption: The specimen collect date in the isolate record is the same
  { as the actual specimen collect date stored in the associated specimen
  { record.
  {--------------------------------------------------------------------------}
  function getSpecCollDate: TIntlDate;
  var
    date: TIntlDate;
  begin
    pIsolate^.GetField( DBISOCollectDate, @date, SizeOf( date ) );
    getSpecCollDate := date;
  end;

var
  PatSeq,
  SourceSeq,
  OrgSeq: TSeqNum;
  pIsoDB: PDBFile;
  getOk: boolean;

begin { Input }

  Assert( not wasProcessed, 3 );

  {---- Access the isolate record for the sub-functions. }
  pIsoDB := New( PDBFile, Init( DBISOFile, '', dbOpenNormal ) );
  Assert( pIsoDB <> nil, 4 );
  getOk := pIsoDB^.dbc^.GetSeq( pIsoDB^.dbr, IsoSeq );
  Assert( getOk, 5 );
  pIsolate := pIsoDB^.dbr;

  {---- Update organism list. Only add if new and not 0. }
  OrgSeq := getOrgSeq;
  if OrgSeq <> 0 then
    pOrganisms^.Insert( New( PSeqRefObj, Init( OrgSeq ) ) );

  {---- Update patient list. Only add if new and not 0. }
  PatSeq := getPatSeq;
  if PatSeq <> 0 then
    pPatients^.Insert( New( PSeqRefObj, Init( PatSeq ) ) );

  {---- Update source list. Only add if new. }
  SourceSeq := getSourceSeq;
  pSources^.Insert( New( PSeqRefObj, Init( SourceSeq ) ) );

  {---- Update isolate list }
  pIsos^.Insert( New( PIsoListObj, Init( IsoSeq, getSpecCollDate,
    PatSeq, SourceSeq, OrgSeq ) ) );

  {---- Update object state to indicate that it has input and clean up.  }
  MSDisposeObj( pIsoDB );

end;



{----------------------------------------------------------------------------}
{ This function extracts an element from the filter. A false return value
{ indicates that an element was not extracted because all elements have been
{ extracted.
{----------------------------------------------------------------------------}

function TEDFilterObj.Output( var IsoSeq: TSeqNum ): boolean;
var
  pIsoSeqObj: PIsoListObj;
begin
  Assert( wasProcessed, 6 );
  Assert( outputIndex >= 0, 7 );

  {---- Return next element based on the output index, return false if done. }
  if outputIndex < pIsos^.Count then
  begin
    pIsoSeqObj := pIsos^.At( outputIndex );
    IsoSeq := pIsoSeqObj^.SeqNum;
    Output := true;
    Inc( outputIndex );
  end
  else
    Output := false;
end;



{----------------------------------------------------------------------------}
{ This function reports how much of the output data has been read as a
{ percentage.
{----------------------------------------------------------------------------}

function TEDFilterObj.PercentOutput: integer;
var
  numberRead: longint;
begin
  if pIsos^.Count = 0 then
    PercentOutput := 0
  else
  begin
    numberRead := outputIndex;
    PercentOutput := numberRead * 100 div pIsos^.Count;
  end;
end;


{----------------------------------------------------------------------------}
{ This procedure processes the input data and sets up to begin data output.
{ The processing eliminates duplicate isolates.
{
{ Assert: The desired isolate records have been added to Lists using the
{ Input function.
{----------------------------------------------------------------------------}

procedure TEDFilterObj.Process;
var
  cnfg: TEpiCnfg;

  {--------------------------------------------------------------------------}
  { This function develops each patient/source combination.
  {--------------------------------------------------------------------------}
  procedure ForEachPatient( pPatient: PSeqRefObj ); far;

    {------------------------------------------------------------------------}
    { This function develops each source/organism combination.
    {------------------------------------------------------------------------}
    procedure ForEachSource( pSource: PSeqRefObj ); far;

      {----------------------------------------------------------------------}
      { This function develops each organism/isolate combination.
      {----------------------------------------------------------------------}
      procedure ForEachOrganism( pOrganism: PSeqRefObj ); far;
      var
        first: boolean;
        pPrevious: PIsoListObj;

        {--------------------------------------------------------------------}
        { This procedure processes all isolates matching the current
        { combination of patient, source, and organism. After the first
        { match, each subsequent match is range tested. If it is in range,
        { it's deleted, otherwise, it becomes the previous in the next
        { iteration.
        {--------------------------------------------------------------------}
        procedure ForEachIsolate( pIsolate: PIsoListObj ); far;

          {------------------------------------------------------------------}
          { This function returns true if the current isolate matches the
          { current patient, [source], and organism. Isolates marked for
          { deletion return false.
          {
          { Assert: cnfg is initialized
          {------------------------------------------------------------------}
          function isMatch: boolean;
          var
            source: boolean;
          begin
            if pIsolate^.DeleteFlag then
              isMatch := false
            else
            begin
              source := not cnfg.ConsiderSource
                or ( pIsolate^.Source = pSource^.SeqNum );

              isMatch := ( pIsolate^.Patient = pPatient^.SeqNum )
                and ( pIsolate^.Organism = pOrganism^.SeqNum )
                and source;
            end;
          end;

          {------------------------------------------------------------------}
          { This function will return true only if the two specified isolate
          { objects fall within the configured number of days. If either date
          { field is not an actual date (it's an invalid or null date), then
          { the dates are considered out of range.
          {
          { Assert: cnfg is initialized
          {------------------------------------------------------------------}
          function InRange( pIso1, pIso2: PIsoListObj ): boolean;
          begin
            InRange := isDate( pIso1^.Date )
              and isDate( pIso2^.Date )
              and ( DaysBetweenDates( pIso1^.Date, pIso2^.Date )
              < cnfg.SearchWindow );
          end;

        begin { ForEachIsolate }
          if isMatch then
          begin
            if first then
            begin
              pPrevious := pIsolate;
              first := false;
            end
            else
            begin
              if inRange( pIsolate, pPrevious ) then
                pIsolate^.DeleteFlag := true
              else
                pPrevious := pIsolate;
            end;
          end;
        end;

      begin { ForEachOrganism }
        first := true;
        pIsos^.ForEach( @ForEachIsolate );
      end;

    begin { ForEachSource }
      pOrganisms^.ForEach( @ForEachOrganism );
    end;

  begin { ForEachPatient }
    if cnfg.ConsiderSource then
      pSources^.ForEach( @ForEachSource )
    else
      ForEachSource( nil );
  end;

  {--------------------------------------------------------------------------}
  { This function deletes isolates tagged for deletion.
  {--------------------------------------------------------------------------}
  procedure DeleteDuplicates;

    {------------------------------------------------------------------------}
    { This function returns true if the specified isolate is tagged for
    { deletion.
    {------------------------------------------------------------------------}
    function isForDeletion( pIsolate: PIsoListObj ): boolean; far;
    begin
      isForDeletion := pIsolate^.DeleteFlag;
    end;

  var
    pIso: PIsoListObj;
  begin
    repeat
      pIso := pIsos^.FirstThat( @isForDeletion );
      if pIso <> nil then
        pIsos^.Free( pIso )
    until pIso = nil;
  end;

begin { Process }

  {---- Reprocessing is invalid. }
  Assert( not wasProcessed, 8 );

  {---- Load configuration, process the data, dispose intermediate objects,
        and update object state to reflect the fact that the data was
        processed and the intermediate objects were disposed. }
  getEpiCnfg( cnfg );
  if cnfg.OmitDuplicates then
  begin
    { Step 1: mark all duplicates for deletion. Note, it would be better if
      the duplicate could be deleted on the spot, but elements cannot be
      removed from within ForEach. }
    pPatients^.ForEach( @ForEachPatient );

    { Step 2:  delete duplicates }
    DeleteDuplicates;
  end;
  Dispose( pOrganisms, Done );
  Dispose( pPatients, Done );
  Dispose( pSources, Done );
  wasProcessed := true;

end;

END.