unit CVT20Lib;

{- Version 2.0 conversion work.
   This module provides routines for converting v2.0 data.

   Also, the SetDbifDir() function should have already been called before using
   these functions. }

INTERFACE

uses
  oDialogs,
  WinTypes,
  CvtMisc,
  OWindows;

type
  TStopType = ^boolean;

const
  WM_LISTKEY   = WM_USER + 208;
  WM_LOSTFOCUS = WM_USER + 209;

function Cvt20_Patients(aParent: PWindowsObject; doStop: TStopType; maps: PMapCollection): boolean;
function Cvt20_Specimens(aParent: PWindowsObject; doStop: TStopType; maps: PMapCollection): boolean;
function Cvt20_Isolates(aParent: PWindowsObject; doStop: TStopType; maps: PMapCollection;
                        aList: PListBox): boolean;

IMPLEMENTATION

uses
  Strings,
  StrsW,
  DBIDS,
  Objects,
  DbifFlds,
  V2Reslib,
  V3Reslib,
  IntlLib,
  GridLib,
  DlgLib,
  Cvt20Xrf,
  CvtCmp,
  APITools,
  WinProcs,
  MScan,
  DB2Dbif,
  DBFile,
  DBLib,
  DBTypes;

{$I CVT20.INC}

type
  PListDlg = ^TListDlg;
  TListDlg = object(TCenterDlg)
    grid    : PGrid;
    buf     : PChar;
    maxLen : integer;
    title  : array[0..100] of char;
    fName   : array[0..10] of char;
    constructor Init(aParent: PWindowsObject; aFile, aTitle, aBuf: PChar; aSize: integer);
    procedure SetupWindow; virtual;
    procedure WMCharToItem(var msg: TMessage);  virtual WM_FIRST + WM_CHARTOITEM;
    procedure WMDrawItem(var msg: TMessage);    virtual WM_FIRST + WM_DRAWITEM;
    procedure WMMeasureItem(var msg: TMessage); virtual WM_FIRST + WM_MEASUREITEM;
    procedure WMCommand(var msg: TMessage); virtual WM_FIRST + WM_COMMAND;
    function CanClose: boolean; virtual;
  end;

function ListOrders(aParent: PWindowsObject; buf: PChar; maxLen: integer): boolean;
var
  d   : PListDlg;
begin
  d:= New(PListDlg, Init(aParent, DBORDFile, 'V3.02 Orders', buf, maxLen));
  ListOrders:= application^.ExecDialog(d) = IDOK;
end;

function ListGroups(aParent: PWindowsObject; buf: PChar; maxLen: integer): boolean;
begin
  ListGroups:= application^.ExecDialog(
               New(PListDlg, Init(aParent, DBTSTGRPFile, 'V3.02 Groups', buf, maxLen))) = IDOK;
end;

constructor TListDlg.Init(aParent: PWindowsObject; aFile, aTitle, aBuf: PChar; aSize: integer);
begin
  inherited Init(aParent, MakeIntResource(DLG_LISTDATA));
  grid:= New(PGrid, InitResource(@self, IDC_GRID, 2, false));
  StrLCopy(fName, aFile, sizeof(fname)-1);
  StrLCopy(title, aTitle, sizeof(title)-1);
  buf:= aBuf;
  maxLen:= aSize;
end;

procedure TListDlg.SetupWindow;
var
  db    : PDBFile;
  row   : integer;
  pstr  : array[0..100] of char;
begin
  inherited SetupWindow;
  SetWindowText(hWindow, title);
  grid^.SetHeader(0, 'ID');
  grid^.SetHeader(1, 'Description');
  grid^.SetColumnWidth(0, 10);
  grid^.SetColumnWidth(1, 10);
  db:= OpenV30File(fName);
  if db <> nil then
  begin
    if db^.dbc^.GetFirst(db^.dbr) then
    begin
      repeat
        db^.dbr^.GetFieldAsStr(1, pstr, sizeof(pstr)-1);
        row:= grid^.AddString('');
        grid^.SetData(0, row, pstr);
        db^.dbr^.GetFieldAsStr(2, pstr, sizeof(pstr)-1);
        grid^.SetData(1, row, pstr);
      until not db^.dbc^.GetNext(db^.dbr);
    end;
    Dispose(db, Done);
  end;
  grid^.SetSelIndex(0);
  EnableWindow(GetItemHandle(ID_OK), grid^.GetRowCount > 0);
  grid^.OptColumnWidth(0);
  grid^.StretchColumn(1);
end;

function TListDlg.CanClose: boolean;
var
  k   : integer;
begin
  k:= grid^.GetSelIndex;
  if k >= 0 then
    grid^.GetData(0, k, buf, maxLen)
  else
    StrCopy(buf, '');
  CanClose:= true;
end;

procedure TListDlg.WMCommand;
begin
  inherited WMCommand(msg);
  if (msg.lParamLo <> 0) then   {- message is from a control }
  begin
    if msg.lParamHI = LBN_DBLCLK then
      if CanClose then
        EndDlg(IDOK);
  end;
end;

procedure TListDlg.WMCharToItem(var msg: TMessage);
begin
  grid^.CharToItem(msg);
end;

procedure TListDlg.WMDrawItem(var msg: TMessage);
begin
  grid^.DrawItem(msg);
end;

procedure TListDlg.WMMeasureItem(var msg: TMessage);
begin
  grid^.MeasureItem(msg);
end;

procedure XferFld(v2Rec: PDB2Record; v3Rec: PDBRec; v2FldNum, v3FldNum: integer);
{- transfer data from v2 record into a v3 record }
var
  fld2    : PDB2Field;
begin
  fld2:= v2Rec^.GetField(v2FldNum);
  v3Rec^.PutFieldAsStr(v3FldNum, fld2^.value);
end;

function Cvt20_Mnem(anID, aV2File, aV3File: PChar): TSeqNum;
{- convert the given ID to v3,0 }
var
  db2Rec    : PDB2Record;
  db3       : PDBFile;
  pstr      : array[0..200] of char;
  ret       : TSeqNum;
  fld       : PDB2Field;
  b         : boolean;
begin
  ret:= 0;
  if StrLen(anID) > 0 then
  begin
    StrCopy(pstr, 'Cannot open V2 mnemonic file : ');
    db2Rec:= DB2OpenFile(aV2File);
    if db2Rec = nil then
    begin
      StrCat(pstr, aV2File);
      CriticalError(pstr);
    end;

    StrCopy(pstr, 'Cannot open V3.02 mnemonic file : ');
    db3:= OpenV30File(aV3File);
    if db3 = nil then
    begin
      StrCat(pstr, aV3File);
      CriticalError(pstr);
    end;

    db3^.dbr^.PutFieldAsStr(DBMnemAbbr, anID);
    if db3^.dbc^.GetEQ(db3^.dbr) then
      ret:= db3^.dbr^.GetSeqValue
    else
    begin
      db2Rec^.ClearRec;
      db2Rec^.PutField(DbifFldMnemonicAbbr, anID);
      if DB2FindID(db2Rec) then
      begin
        XferFld(db2Rec, db3^.dbr, DbifFldMnemonicAbbr, DBMnemAbbr);
        XferFld(db2Rec, db3^.dbr, DbifFldMnemonicDesc, DBMnemDesc);
        if StrIComp(aV3File, DBSrcFile) = 0 then
        begin
          {- xfer source flag as well }
          fld:= db2Rec^.GetField(DbifFldSourceType);
          b:= (StrComp(fld^.value, 'U') = 0);
          db3^.dbr^.PutField(DBSRCUrine, @b);
        end;

        if not db3^.dbc^.InsertRec(db3^.dbr) then
        begin
          LogHex(db3^.dbc^.dbErrorNum);
          LogStr(' - Cannot convert mnemonic. File: ');
          LogStr(aV3File);
          LogStr('  ID: ');
          LogStr(anID);
          LogLn;
          CriticalError('Cannot convert mnemonic file.');
        end
        else
          ret:= db3^.dbr^.GetSeqValue;
      end;
    end;
    Dispose(db3, Done);
    DB2CloseFile(db2Rec);
  end;
  Cvt20_Mnem:= ret;
end;

function Cvt20_Org(anID: PChar): TSeqNum;
{- convert the given org ID to v3,0 and return the seq num}
var
  db2Rec    : PDB2Record;
  db3       : PDBFile;
  pstr      : array[0..200] of char;
  ret       : TSeqNum;
  fld       : PDB2Field;
  b         : boolean;
begin
  ret:= 0;
  if (StrLen(anID) > 0) and (StrComp(anID, '0') <> 0) then
  begin
    db2Rec:= DB2OpenFile('ORG');
    if db2Rec = nil then
      CriticalError('Cannot open V2 ORGANISM file');

    db3:= OpenV30File(DBORGFile);
    if db3 = nil then
      CriticalError('Cannot open V3.02 ORGANISM file');

    db3^.dbr^.PutFieldAsStr(DBORGOrg, anID);
    if db3^.dbc^.GetEQ(db3^.dbr) then
      ret:= db3^.dbr^.GetSeqValue
    else
    begin
      db2Rec^.ClearRec;
      db2Rec^.PutField(DbifFldOrganismID, anID);
      if DB2FindID(db2Rec) then
      begin
        XferFld(db2Rec, db3^.dbr, DbifFldOrganismID, DBORGOrg);
        XferFld(db2Rec, db3^.dbr, DbifFldOrganismName, DBORGSName);
        XferFld(db2Rec, db3^.dbr, DbifFldOrganismName, DBORGLName);
        XferFld(db2Rec, db3^.dbr, DbifFldOrganismSet, DBORGSet);
        XferFld(db2Rec, db3^.dbr, DbifFldOrganismName, DBORGLName);
        XferFld(db2Rec, db3^.dbr, DbifFldOrganismClass, DBORGFamily);
        b:= true;
        db3^.dbr^.PutField(DBORGUDFlag, @b);

        if not db3^.dbc^.InsertRec(db3^.dbr) then
        begin
          LogHex(db3^.dbc^.dbErrorNum);
          LogStr(' - Cannot convert Organism.  ID: ');
          LogStr(anID);
          LogLn;
          CriticalError('Cannot convert organism.');
        end
        else
          ret:= db3^.dbr^.GetSeqValue;
      end;
    end;
    Dispose(db3, Done);
    DB2CloseFile(db2Rec);
  end;
  Cvt20_Org:= ret;
end;

function Cvt20_Session(anID: PChar): TSeqNum;
{- convert the given session ID to v3.0 and return the seq num }
var
  db2   : PDB2Record;
  db3   : PDBFile;
  pstr  : array[0..200] of char;
  ret   : TSeqNum;
  fld   : PDB2Field;
begin
  ret:= 0;
  if (StrLen(anID) > 0) then
  begin
    db3:= OpenV30File(DBSESSFile);
    if db3 = nil then
      CriticalError('Cannot open V3.02 SESSION file');

    db3^.dbr^.PutFieldAsStr(DBORGOrg, anID);
    if db3^.dbc^.GetEQ(db3^.dbr) then
      ret:= db3^.dbr^.GetSeqValue
    else
    begin
      db2:= DB2OpenFile('SESSION');
      if db2 = nil then
        CriticalError('Cannot open V2 SESSION file');

      db2^.ClearRec;
      db2^.PutField(DbifFldSessionID, anID);
      if DB2FindID(db2) then
      begin
        XferFld(db2, db3^.dbr, DbifFldSessionID, DBSESSID);
        XferFld(db2, db3^.dbr, DbifFldSessionDesc, DBSESSDesc);

        if not db3^.dbc^.InsertRec(db3^.dbr) then
        begin
          LogHex(db3^.dbc^.dbErrorNum);
          LogStr(' - Cannot convert SESSION.  ID: ');
          LogStr(anID);
          LogLn;
          CriticalError('Cannot convert SESSION.');
        end
        else
          ret:= db3^.dbr^.GetSeqValue;
      end;
      DB2CloseFile(db2);
    end;
    Dispose(db3, Done);
  end;
  Cvt20_Session:= ret;
end;

function FindPatient(aPatID: PChar): TSeqNum;
{- look for a patient with the specified ID }
var
  db    : PDBFile;
  ret   : TSeqNum;
begin
  ret:= 0;
  if StrLen(aPatID) > 0 then
  begin
    db:= OpenV30File(DBPATFile);
    if db <> nil then
    begin
      db^.dbr^.PutFieldAsStr(DBPATPatID, aPatID);
      if db^.dbc^.GetEQ(db^.dbr) then
        ret:= db^.dbr^.GetSeqValue;
      Dispose(db, Done);
    end;
    if ret = 0 then
    begin
      LogStr('Patient ID not found in V3.02 database: ');
      LogStr(aPatID);
      LogLn;
    end;
  end;
  FindPatient:= ret;
end;

function CvtDrug(dID: PChar): TSeqNum;
{- convert the drug file. Only user defined drugs will be converted }
var
  db2Rec  : PDB2Record;
  db      : PDBFile;
  fld2    : PDB2Field;
  b       : boolean;
  sNum    : longint;
  tRec    : PDBRec;
  pstr    : array[0..100] of char;

  function GetNewSortNum(keyNum: integer): longint;
  var
    num   : longint;
  begin
    db^.dbc^.SetCurKeyNum(keyNum);
    if db^.dbc^.GetLast(tRec) then
      tRec^.GetField(DBDrugSortNum, @num, sizeof(num))
    else
      CriticalError('Cannot create DRUG sort key value');
    GetNewSortNum:= num + 1;
  end;

begin
  CvtDrug:= 0;
  if StrLen(dID) = 0 then
    exit;

  db:= OpenV30File(DBDRUGFile);
  if db = nil then
    CriticalError('Cannot open the V3.02 DRUG file.');

  db^.dbr^.PutFieldAsStr(DBDRUGAbbr, dID);
  if db^.dbc^.GetEQ(db^.dbr) then
    CvtDrug:= db^.dbr^.GetSeqValue
  else
  begin
    db2Rec:= DB2OpenFile('DRUG');
    if db2Rec = nil then
      CriticalError('Cannot open the V2.x DRUG file.');

    tRec:= New(PDBRec, Init(db^.dbc));

    db2Rec^.PutField(DbifFldDrugID, dID);
    if not DB2FindID(db2Rec) then
    begin
      StrCopy(pstr, 'Cannot find V2.x Drug : ');
      StrCat(pstr, dID);
      CriticalError(pstr);
    end;

    db^.dbr^.ClearRecord;
    XferFld(db2Rec, db^.dbr, DbifFldDrugID, DBDrugAbbr);
    XferFld(db2Rec, db^.dbr, DbifFldDrugName, DBDrugName);
    XferFld(db2Rec, db^.dbr, DbifFldDrugID, DBDrugAltAbbr);
    b:= false;
    db^.dbr^.PutField(DBDrugSupp, @b);
    sNum:= GetNewSortNum(DBDrug_Sort_Key);
    db^.dbr^.PutField(DBDrugSortNum, @sNum);
    sNum:= GetNewSortNum(DBDrug_DfltSort_Key);
    db^.dbr^.PutField(DBDrugDfltSortNum, @sNum);
    b:= true;
    db^.dbr^.PutField(DBDrugUDFlag, @b);

    if not db^.dbc^.InsertRec(db^.dbr) then
    begin
      {- log error to disk }
      LogHex(db^.dbc^.dbErrorNum);
      LogStr(' - Cannot create DRUG record -- ID = ');
      fld2:= db2Rec^.GetField(DbifFldDrugID);
      LogStr(fld2^.value);
      LogLn;
      StrCopy(pstr, 'Cannot create drug : ');
      StrCat(pstr, dID);
      CriticalError(pstr);
    end
    else
      CvtDrug:= db^.dbr^.GetSeqValue;
    Dispose(tRec, Done);
    DB2CloseFile(db2Rec);
  end;
  Dispose(db, Done);
end;

function CvtTest(tItem: PTestObj): TSeqNum;
{- convert a test into the 3.0 database }
var
  db    : PDBFile;
  b     : boolean;
  pstr  : array[0..100] of char;
  cat   : PDBFile;
  seq   : TSeqNum;
begin
  CvtTest:= 0;
  db:= OpenV30File(DBTSTFile);
  if db = nil then
    CriticalError('Cannot open the V3.02 TEST file.');
  cat:= OpenV30File(DBTSTCATFile);
  if cat = nil then
    CriticalError('Cannot open the V3.02 TEST Category file.');

  {- if user defined then use the V2 ID name, otherwise, use the converted V3
     ID name }
  if tItem^.testUD then
    db^.dbr^.PutFieldAsStr(DBTSTID, tItem^.testID)
  else
    db^.dbr^.PutFieldAsStr(DBTSTID, tItem^.v3TestID);

  db^.dbr^.PutFieldAsStr(DBTSTTstCat, tItem^.testCat);
  if not db^.dbc^.GetEQ(db^.dbr) then
  begin
    cat^.dbr^.PutFieldAsStr(DBTSTCATID, tItem^.testCat);
    if not cat^.dbc^.GetEQ(cat^.dbr) then
    begin
      StrCopy(pstr, 'Cannot find test category: ');
      StrCat(pstr, tItem^.testCat);
      CriticalError(pstr);
    end;
    seq:= cat^.dbr^.GetSeqValue;
    {- need to create test }
    if tItem^.testUD then
      db^.dbr^.PutFieldAsStr(DBTSTID, tItem^.testID)
    else
      db^.dbr^.PutFieldAsStr(DBTSTID, tItem^.v3testID);

    db^.dbr^.PutFieldAsStr(DBTSTName, tItem^.testName);
    db^.dbr^.PutFieldAsStr(DBTSTTstCat, tItem^.testCat);
    db^.dbr^.PutField(DBTSTTstCatRef, @seq);
    b:= true;
    db^.dbr^.PutField(DBTSTUDFlag, @b); {- user defined }

    {- only convert drugs when they are referenced by a NCCLS/MIC test. No other
       test should have drugs on them. }
    if (StrComp(tItem^.testCat, 'NCCLS') = 0) or (StrComp(tItem^.testCat, 'MIC') = 0) then
      seq:= CvtDrug(tItem^.testDrug)
    else
      seq:= 0;
    db^.dbr^.PutField(DBTSTDrugRef, @seq);
    if not db^.dbc^.InsertRec(db^.dbr) then
    begin
      LogHex(db^.dbc^.dbErrorNum);
      LogStr(' - Cannot create TEST record -- ID = ');
      LogStr(tItem^.testID);
      LogLn;
      StrCopy(pstr, 'Cannot create Test Record : ');
      StrCat(pstr, tItem^.testID);
      CriticalError(pstr);
    end;
    CvtTest:= db^.dbr^.GetSeqValue;
  end
  else
    CvtTest:= db^.dbr^.GetSeqValue;

  Dispose(cat, Done);
  Dispose(db, Done);
end;

procedure CheckDBError(db: PDBFile; keyStr: PChar);
var
  pstr    : array[0..255] of char;
  fi      : TFileInfo;
begin
  if db^.dbc^.dbErrorNum <> 0 then
  begin
    db^.dbd^.FileInfo(fi);
    LogHex(db^.dbc^.dbErrorNum);
    if db^.dbc^.dbErrorNum = $6005 then  {- check duplicate }
    begin
      StrCopy(pstr, ' - Duplicate ID in ');
      StrCat(pstr, fi.desc);
      StrCat(pstr, ' - ID: ');
      StrCat(pstr, keyStr);
      StrCat(pstr, ' - record not converted.');
      LogStr(pstr);
      LogLn;
    end
    else
    begin
      StrCopy(pstr, ' - Cannot add record to ');
      StrCat(pstr, fi.desc);
      StrCat(pstr, ' - ID: ');
      StrCat(pstr, keyStr);
      CriticalError(pstr);
    end;
  end;
end;

procedure XferMapFlds(aFile: PChar; db2: PDB2Record; db3: PDBFile; maps: PMapCollection);
var
  k     : integer;
  ai    : TAssocInfo;
  fi    : TFldInfo;
  seq   : TSeqNum;
  fld   : PDB2Field;
  fNum  : integer;
begin
  for k:= 1 to db2^.NumFields do
  begin
    fld:= db2^.GetField(k);
    if fld^.UDFlag then
    begin
      fNum:= maps^.FindMappedField(aFile, k);
      if fNum > 0 then
      begin
        if fld^.fldType = dbSeqRef then
        begin
          seq:= 0;
          db3^.dbd^.FieldInfo(fNum, fi);
          if db3^.dbd^.AssocInfo(fi.fldAssoc, ai) then
            seq:= Cvt20_Mnem(fld^.value, fld^.mnemFile, ai.fName);
          db3^.dbr^.PutField(fNum, @seq);
        end
        else
          XferFld(db2, db3^.dbr, k, fNum);
      end;
    end;
  end;
end;

function Cvt20_Patients(aParent: PWindowsObject; doStop: TStopType; maps: PMapCollection): boolean;
var
  k     : integer;
  db2   : PDB2Record;
  db3   : PDBFile;
  isOK  : boolean;
  fld   : PDB2Field;
  pstr  : array[0..100] of char;
begin
  Cvt20_Patients:= true;
  db2:= DB2OpenFile('PATIENT');
  if db2 = nil then
    CriticalError('Cannot open V2.x PATIENT file.');
  db3:= OpenV30File(dbPatFile);
  if db3 = nil then
    CriticalError('Cannot open V3.02 PATIENT file.');

  isOK:= DB2GetFirst(db2);
  while isOK and not doStop^ do
  begin
    HandleEvents(aParent^.hWindow);
    db3^.dbr^.ClearRecord;

    {- transfer system fields to version 3.0 record }
    fld:= db2^.GetField(DbifFldPatientID);  {- ensure proper pat id formatting }
    StrCopy(pstr, fld^.value);
    FormatDMSField(pstr, 12);
    db3^.dbr^.PutFieldAsStr(DBPATPatID, pstr);

    XferFld(db2, db3^.dbr, DbifFldPatientFirstName, DBPATFName);
    XferFld(db2, db3^.dbr, DbifFldPatientLastName, DBPATLName);
    XferFld(db2, db3^.dbr, DbifFldPatientFreeText, DBPATFreeText);

    {- now transfer mapped fields to v30 record }
    XferMapFlds('PATIENT', db2, db3, maps);

    {- now save the record in the v 3.0 database }
    if not db3^.dbc^.InsertRec(db3^.dbr) then
    begin
      fld:= db2^.GetField(DbifFldPatientID);
      CheckDBError(db3, fld^.value);
    end;

    isOK:= DB2GetNext(db2);
  end;

  Dispose(db3, Done);
  DB2CloseFile(db2);
end;

function Cvt20_Specimens(aParent: PWindowsObject; doStop: TStopType; maps: PMapCollection): boolean;
var
  db2   : PDB2Record;
  db3   : PDBFile;
  isOK  : boolean;
  fld   : PDB2Field;
  seq   : TSeqNum;
  pstr  : array[0..100] of char;
  fSeq, pSeq  : TSeqNum;  {- finalized and prelim seq numbers }
begin
  db3:= OpenV30File(DBSTATFile);
  if db3 = nil then
    CriticalError('Cannot open V3.02 Specimen Status file.');
  db3^.dbr^.PutFieldAsStr(DBSTATID, '1');  {- prelim }
  if db3^.dbc^.GetEQ(db3^.dbr) then
    pSeq:= db3^.dbr^.GetSeqValue
  else
    pSeq:= 0;
  db3^.dbr^.PutFieldAsStr(DBSTATID, '2');  {- final }
  if db3^.dbc^.GetEQ(db3^.dbr) then
    fSeq:= db3^.dbr^.GetSeqValue
  else
    fSeq:= 0;
  Dispose(db3, Done);

  Cvt20_Specimens:= true;
  db2:= DB2OpenFile('SPECIMEN');
  if db2 = nil then
    CriticalError('Cannot open V2.x SPECIMEN file.');
  db3:= OpenV30File(dbSpecFile);
  if db3 = nil then
    CriticalError('Cannot open V3.02 SPECIMEN file.');

  isOK:= DB2GetFirst(db2);
  while isOK and not doStop^ do
  begin
    HandleEvents(aParent^.hWindow);
    db3^.dbr^.ClearRecord;

(*DEBUG CODE
fld:= db2^.GetField(DbifFldSpecimenID);
if strcomp(fld^.value, '00000555') <> 0 then
begin
  isOK:= DB2GetNext(db2);
  continue;
end;
*)
    {- transfer system fields to version 3.0 record }
    XferFld(db2, db3^.dbr, DbifFldSpecimenID, DBSPECSpecID);
    XferFld(db2, db3^.dbr, DbifFldSpecimenCollectDate, DBSPECCollectDate);
    XferFld(db2, db3^.dbr, DbifFldSpecimenFreeText, DBSPECFreeText);
    fld:= db2^.GetField(DbifFldSpecimenWard);
    seq:= Cvt20_Mnem(fld^.value, 'WARD', DBWARDFile);
    db3^.dbr^.PutField(DBSPECWard, @seq);

    fld:= db2^.GetField(DbifFldSpecimenSource);
    seq:= Cvt20_Mnem(fld^.value, 'SOURCE', DBSRCFile);
    db3^.dbr^.PutField(DBSPECSrc, @seq);

    fld:= db2^.GetField(DbifFldSpecimenFinalizedFlag);
    if StrComp(fld^.value, 'Y') = 0 then
      seq:= fSeq
    else
      seq:= pSeq;
    db3^.dbr^.PutField(DBSPECStat, @seq);

    fld:= db2^.GetField(DbifFldSpecimenPatientID);
    seq:= FindPatient(fld^.value);
    db3^.dbr^.PutField(DBSPECPatRef, @seq);

    {- now transfer mapped fields to v30 record }
    XferMapFlds('SPECIMEN', db2, db3, maps);

    {- now save the record in the v 3.0 database }
    if not db3^.dbc^.InsertRec(db3^.dbr) then
    begin
      fld:= db2^.GetField(DBSPECSpecID);
      StrCopy(pstr, fld^.value);
      StrCat(pstr, ' ');
      fld:= db2^.GetField(DBSPECCollectDate);
      StrCat(pstr, fld^.value);
      CheckDBError(db3, pstr);
    end;

    isOK:= DB2GetNext(db2);
  end;

  Dispose(db3, Done);
  DB2CloseFile(db2);
  Cvt20_Specimens:= true;
end;

function TestGroupForProc(aProc, aGroup: PChar; maxLen: integer): TSeqNum;
{- for a given test procedure, find it's corresponding test group }
var
  db    : PDBFile;
begin
  TestGroupForProc:= 0;
  StrCopy(aGroup, '');
  db:= OpenV30File(DBTSTGRPFile);
  if db <> nil then
  begin
    db^.dbr^.PutFieldAsStr(DBTSTGRPID, aProc);
    if db^.dbc^.GetEQ(db^.dbr) then
    begin
      TestGroupForProc:= db^.dbr^.GetSeqValue;
      db^.dbr^.GetFieldAsStr(DBTSTGRPID, aGroup, maxLen);
    end;
    Dispose(db, Done);
  end;
end;

const
  g_Proc    = 0;
  g_Order   = 1;
  g_Group   = 2;
  g_Iso     = 3;

type
  PBatDlg = ^TBatDlg;
  TBatDlg = object(TCenterDlg)
    batXref : PBatID;
    grid    : PGrid;
    specSeq : TSeqNum;
    constructor Init(aParent: PWindowsObject; aSpecSeq: TSeqNum; aBatXref: PBatID);
    procedure SetupWindow; virtual;
    procedure IDDone(var msg: TMessage); virtual ID_FIRST + IDC_DONE;
    procedure IDEdit(var msg: TMessage); virtual ID_FIRST + IDC_EDITMAP;
    procedure WMCharToItem(var msg: TMessage);  virtual WM_FIRST + WM_CHARTOITEM;
    procedure WMDrawItem(var msg: TMessage);    virtual WM_FIRST + WM_DRAWITEM;
    procedure WMMeasureItem(var msg: TMessage); virtual WM_FIRST + WM_MEASUREITEM;
    procedure WMCommand(var msg: TMessage); virtual WM_FIRST + WM_COMMAND;
    procedure Cancel(var msg: TMessage); virtual ID_FIRST + ID_CANCEL;
    procedure FillGrid;
    function CanClose: boolean; virtual;
  end;

  PBatEditDlg = ^TBatEditDlg;
  TBatEditDlg = object(TCenterDlg)
    oItem     : POrder;
    proc      : PStatic;
    order, grp, iso: PEdit;
    orig      : POrder;
    batID     : array[0..10] of char;
    ordDesc, grpDesc: PEdit;
    constructor Init(aParent: PWindowsObject; aBatID: PChar; aOrdItem: POrder);
    procedure SetupWindow; virtual;
    function CanClose: boolean; virtual;
    procedure LostFocus(var msg: TMessage); virtual WM_FIRST + WM_LOSTFOCUS;
    procedure ListKey(var msg: TMessage); virtual WM_FIRST + WM_LISTKEY;
  end;

  PFocEdit = ^TFocEdit;
  TFocEdit = object(TEdit)
    procedure WMKillFocus(var msg: TMessage); virtual WM_FIRST + WM_KILLFOCUS;
    procedure WMKeyDown(var msg: TMessage); virtual WM_FIRST + WM_KEYDOWN;
    procedure WMRClick(var msg: TMessage); virtual WM_FIRST + WM_RBUTTONDOWN;
  end;

procedure TFocEdit.WMKillFocus(var msg: TMessage);
begin
  inherited WMKillFocus(msg);
  PostMessage(parent^.HWindow, WM_LOSTFOCUS, GetID, 0);
end;

procedure TFocEdit.WMKeyDown(var msg: TMessage);
begin
  inherited WMKeyDown(msg);
  if msg.wParam = VK_F3 then
    PostMessage(parent^.hWindow, WM_LISTKEY, GetID, 0);
end;

procedure TFocEdit.WMRClick(var msg: TMessage);
begin
  DefWndProc(msg);
  PostMessage(parent^.hWindow, WM_LISTKEY, GetID, 0);
end;

constructor TBatEditDlg.Init(aParent: PWindowsObject; aBatID: PChar; aOrdItem: POrder);
begin
  inherited Init(aParent, MakeIntResource(DLG_EDITBATORD));
  orig:= New(POrder, Init);
  proc:= New(PStatic, InitResource(@self, IDC_EDIT1, 0));
  order:= New(PFocEdit, InitResource(@self, IDC_EDIT2, 9));
  grp:= New(PFocEdit, InitResource(@self, IDC_EDIT3, 9));
  iso:= New(PFocEdit, InitResource(@self, IDC_EDIT4, 4));
  ordDesc:= New(PEdit, InitResource(@self, IDC_ORDDESC, 33));
  grpDesc:= New(PEdit, InitResource(@self, IDC_GRPDESC, 33));
  oItem:= aOrdItem;
  StrCopy(batID, aBatID);
  orig^.orderSeq:= oItem^.orderSeq;
  orig^.grpSeq:= oItem^.grpSeq;
  StrCopy(orig^.origProc, oItem^.origProc);
  StrCopy(orig^.isoID, oItem^.isoID);
  StrCopy(orig^.grpID, oItem^.grpID);
  StrCopy(orig^.orderID, oItem^.orderID);
  StrCopy(orig^.ordDesc, oItem^.ordDesc);
  StrCopy(orig^.grpDesc, oItem^.grpDesc);
  orig^.aSet:= oItem^.aSet;
end;

procedure TBatEditDlg.SetupWindow;
begin
  inherited SetupWindow;
  grp^.SetText(orig^.grpID);
  proc^.SetText(orig^.origProc);
  order^.SetText(orig^.orderID);
  ordDesc^.SetText(orig^.ordDesc);
  grpDesc^.SetText(orig^.grpDesc);
  iso^.SetText(orig^.isoID);
  if StrComp(orig^.origProc, 'USER') <> 0 then
  begin
    EnableWindow(grp^.hWindow, false);
    EnableWindow(GetItemHandle(LBL_GRPID), false);
    EnableWindow(GetItemHandle(IDC_GRPDESC), false);
  end;
end;

procedure TBatEditDlg.ListKey(var msg: TMessage);
var
  pstr  : array[0..100] of char;
begin
  StrCopy(pstr, 'TEST');
  case msg.wParam of
    IDC_EDIT2 :   {- order list }
      if ListOrders(@self, pstr, sizeof(pstr)-1) then
      begin
        order^.SetText(pstr);
        PostMessage(hWindow, WM_LOSTFOCUS, IDC_EDIT2, 0);
      end;
    IDC_EDIT3 :   {- group list }
      if ListGroups(@self, pstr, sizeof(pstr)-1) then
      begin
        grp^.SetText(pstr);
        PostMessage(hWindow, WM_LOSTFOCUS, IDC_EDIT3, 0);
      end;
  end;
end;

procedure TBatEditDlg.LostFocus(var msg: TMessage);
var
  pstr  : array[0..50] of char;
  desc  : array[0..50] of char;
begin
  if msg.wParam = IDC_EDIT3 then  {- group lost focus }
  begin
    grp^.GetText(pstr, sizeof(pstr)-1);
    if (StrComp(pstr, orig^.grpID) <> 0) then
    begin
      StrCopy(orig^.grpID, pstr);
      if GroupExists(pstr, desc) > 0 then
      begin
        grpDesc^.SetText(desc);
      end;
    end;
  end
  else if msg.wParam = IDC_EDIT2 then  {- order lost focus }
  begin
    order^.GetText(pstr, sizeof(pstr)-1);
    if (StrComp(pstr, orig^.orderID) <> 0) then
    begin
      StrCopy(orig^.orderID, pstr);
      if OrderExists(pstr, desc) > 0 then
      begin
        grp^.SetText('');
        grpDesc^.SetText('');
        ordDesc^.SetText(desc);
        if (GetFocus = grp^.hWindow) or (GetFocus = grpDesc^.hWindow) then
          FocusCtl(hWindow, IDC_EDIT2);
        EnableWindow(grp^.hWindow, false);
        EnableWindow(GetItemHandle(LBL_GRPID), false);
        EnableWindow(GetItemHandle(IDC_GRPDESC), false);
      end
      else
      begin
        EnableWindow(GetItemHandle(LBL_GRPID), true);
        EnableWindow(GetItemHandle(IDC_GRPDESC), true);
        EnableWindow(grp^.hWindow, true);
        if StrComp(orig^.origProc, 'USER') <> 0 then
        begin
          if (GetFocus = grp^.hWindow) or (GetFocus = grpDesc^.hWindow) then
            FocusCtl(hWindow, IDC_EDIT2);
          grp^.SetText(orig^.origProc);
          EnableWindow(grp^.hWindow, false);
          EnableWindow(GetItemHandle(LBL_GRPID), false);
          EnableWindow(GetItemHandle(IDC_GRPDESC), false);
          if GroupExists(orig^.origProc, desc) > 0 then
            grpDesc^.SetText(desc);
        end;
      end;
    end;
  end
  else if msg.WParam = IDC_EDIT4 then   {- isolate }
  begin
    iso^.GetText(pstr, 5);
    FormatDMSField(pstr, 3);
    iso^.SetText(pstr);
  end;
end;

function TBatEditDlg.CanClose: boolean;
var
  ret   : boolean;
  seq   : TSeqNum;
  tw    : TWaitCursor;
  aDesc : array[0..35] of char;

  procedure CheckLen(s, msg: PChar; id: integer);
  begin
    if ret then
      if StrLen(s) = 0 then
      begin
        ErrorMsg(hWindow, 'Error', msg);
        FocusCtl(hWindow, id);
        ret:= false;
      end;
  end;

begin
  tw.Init;

  {- force focus processing }
  PostMessage(hWindow, WM_LOSTFOCUS, IDC_EDIT2, 0);
  PostMessage(hWindow, WM_LOSTFOCUS, IDC_EDIT4, 0);

  ret:= true;
  proc^.GetText(orig^.origProc, 10);
  order^.GetText(orig^.orderID, 10);
  ordDesc^.GetText(orig^.ordDesc, 32);
  grpDesc^.GetText(orig^.grpDesc, 32);
  grp^.GetText(orig^.grpID, 10);
  iso^.GetText(orig^.isoID, 5);
  FormatDMSField(orig^.isoID, 3);
  iso^.SetText(orig^.isoID);

  CheckLen(orig^.orderID, 'Order ID is required', IDC_EDIT2);
  CheckLen(orig^.isoID, 'Isolate ID is required', IDC_EDIT4);

  if ret then
  begin
    seq:= OrderExists(orig^.orderID, aDesc);
    {- if order does not exist, then a test group is required }
    if seq = 0 then
    begin
      CheckLen(orig^.grpID, 'Test group ID is required', IDC_EDIT3);

      if ret then
      begin
        ordDesc^.GetText(orig^.ordDesc, 32);
        CheckLen(orig^.ordDesc, 'Order description is required.', IDC_ORDDESC);
      end;
    end
    else
    begin
      StrCopy(orig^.ordDesc, aDesc);
      ordDesc^.SetText(aDesc);
    end;
  end;

  {- if order exists, then make sure its compatible with then procedure }
  if ret then
  begin
    if (seq > 0) then
    begin
      ret:= CompOrderAndProc(orig^.orderID, batID, orig^.origProc);
      if not ret then
      begin
        ErrorMsg(hWindow, 'Error', 'Order is not compatible with test procedure');
        FocusCtl(hWindow, IDC_EDIT2);
      end
      else
        orig^.orderSeq:= seq;
    end;
  end;

  if ret and (orig^.orderSeq = 0) then  {- if order does not exist }
  begin
    seq:= GroupExists(orig^.grpID, aDesc);
    orig^.grpSeq:= 0;
    if seq > 0 then
    begin
      StrCopy(orig^.grpDesc, aDesc);
      grpDesc^.SetText(aDesc);
      ret:= CompGroupAndProc(orig^.grpID, batID, orig^.origProc);
      if not ret then
      begin
        ErrorMsg(hWindow, 'Error', 'Group is not compatible with test procedure.');
        FocusCtl(hWindow, IDC_EDIT3);
        ret:= false;
      end
      else
        orig^.grpSeq:= seq;
    end
    else if StrComp(orig^.origProc, 'USER') <> 0 then
    begin
      ret:= false;
      ErrorMsg(hWindow, 'Error', 'Existing Group must be specified.');
    end
    else
      CheckLen(orig^.grpDesc, 'Group description is required.', IDC_GRPDESC);
  end;

  if orig^.orderSeq > 0 then
  begin
    StrCopy(orig^.grpID, '');
    StrCopy(orig^.grpDesc, '');
    orig^.grpSeq:= 0;
  end;

  CanClose:= ret;
  if ret then
  begin
    oItem^.orderSeq:= orig^.orderSeq;
    oItem^.grpSeq:= orig^.grpSeq;
    StrCopy(oItem^.origProc, orig^.origProc);
    StrCopy(oItem^.isoID, orig^.isoID);
    StrCopy(oItem^.grpID, orig^.grpID);
    StrCopy(oItem^.orderID, orig^.orderID);
    StrCopy(oItem^.ordDesc, orig^.ordDesc);
    StrCopy(oItem^.grpDesc, orig^.grpDesc);
  end;
  tw.Done;
end;

{-----------------------------------------------------------------[ TBatDlg ]--}

type
  PXref = ^TXref;
  TXref = object(TObject)
    tstGrpRef   : TSeqNum;
    test        : TSeqNum;
    resLen      : integer;
  end;

function GetGroupSet(aGrp: PChar): integer;
{- return group set if aGrp exists, else return 0 }
var
  db  : PDBFile;
  s   : byte;
begin
  GetGroupSet:= 0;
  db:= OpenV30File(DBTSTGRPFile);
  if db <> nil then
  begin
    db^.dbr^.PutFieldAsStr(DBTSTGRPID, aGrp);
    if db^.dbc^.GetEQ(db^.dbr) then
    begin
      db^.dbr^.GetField(DBTSTGRPSet, @s, sizeof(s));
      GetGroupSet:= s;
    end;
    Dispose(db, Done);
  end;
end;

function CvtGroup(aBatID, aProcID, aGrpID, aGrpDesc: PChar): TSeqNum;
{- converts *user* proc into group }
var
  coll  : PCollection;
  xref  : PXref;
  k     : integer;
  b     : boolean;
  grpF  : PDBFile;
  db    : PDBFile;
  bt    : byte;
  tstr  : array[0..5] of char;
  t     : PTestObj;
  len   : integer;
  tSeq  : TSeqNum;
  grpSeq: TSeqNum;
  pstr  : array[0..100] of char;
begin
  grpSeq:= 0;
  grpf:= OpenV30File(DBTSTGRPFile);
  if grpf = nil then
    CriticalError('Cannot open Test Group file');

  coll:= New(PCollection, Init(25, 25));

  {- is the procedure a USER procedure ? }
  if StrComp(aProcID, 'USER') = 0 then
  begin
    {- load all tests for the group }
    BuildV2TestProcList(aBatID, 'USER', coll, true);

    {- now sum the lengths of all the tests in the list }
    len:= 0;
    for k:= 0 to coll^.count - 1 do
    begin
      t:= coll^.At(k);
      Inc(len, t^.resLen);
    end;

    {- create the group for the user proc }
    grpf^.dbr^.PutFieldAsStr(DBTSTGRPID, aGrpID);
    grpf^.dbr^.PutFieldAsStr(DBTSTGRPDesc, aGrpDesc);
    bt:= 8;
    grpf^.dbr^.PutField(DBTSTGRPSet, @bt);   {- 8 for user groups }
    StrCopy(tstr, 'XX');
    grpf^.dbr^.PutFieldAsStr(DBTSTGRPLayout, tstr);  {- XX for user groups }
    grpf^.dbr^.PutField(DBTSTGRPLen, @len);
    b:= true;
    grpf^.dbr^.PutField(DBTSTGRPUDFlag, @b);

    if not grpf^.dbc^.InsertRec(grpf^.dbr) then  {- insert the test group rec}
    begin
      StrCopy(pstr, 'Cannot create Test Group : ');
      StrCat(pstr, aGrpID);
      CriticalError(pstr);
    end;
    grpSeq:= grpf^.dbr^.GetSeqValue;  {- assign new group seq to xref}

    db:= OpenV30File(DBTSTGRPXRFFile);
    if db = nil then
      CriticalError('Cannot open Test Group Xref file');

    db^.dbr^.PutField(DBTSTGRPXRFTstGrpRef, @grpSeq);

    len:= 0;
    {- for each test in the USER procedure - ensure that is exists in V3 }
    for k:= 0 to coll^.count - 1 do
    begin
      t:= coll^.At(k);  {- get test item }

      {- if test does not exist then create it }
      tSeq:= CvtTest(t);

      {- add the test seq to the test grp xref }
      db^.dbr^.PutField(DBTSTGRPXRFTstRef, @tSeq);
      db^.dbr^.PutField(DBTSTGRPXRFResBlockPos, @len);
      Inc(len, t^.resLen);
      if not db^.dbc^.InsertRec(db^.dbr) then
      begin
        StrCopy(pstr, 'Cannot add Test Group Xref entry. Group: ');
        StrCat(pstr, aGrpID);
        CriticalError(pstr);
      end;
    end;
    Dispose(db, Done);
  end;
  Dispose(grpF, Done);
  Dispose(coll, Done);
  CvtGroup:= grpSeq;
end;

procedure XferOrder(batXref: PBatID);
{- transfer orders in the batXref record to V3.0. Ultimate results will be to
   complete the order items in the batXref structure }
var
  k     : integer;
  o     : POrder;
  db    : PDBFile;
  pstr  : array[0..100] of char;
  seq   : TSeqNum;
begin
  {- for each order in the batXref }
  for k:= 0 to batXref^.orders^.count - 1 do
  begin
    o:= batXref^.orders^.At(k);
    if o^.orderSeq = 0 then {- need to create the order }
    begin
      {- first see if the order has already been created from this batXref }
      seq:= OrderExists(o^.orderID, pstr);
      if seq > 0 then
      begin
        o^.orderSeq:= seq;
        o^.grpSeq:= 0;
      end
      else
      begin
        if o^.grpSeq = 0 then {- need to create the group (only occurs on USER procs) }
          o^.grpSeq:= CvtGroup(batXref^.batID, o^.origProc, o^.grpID, o^.grpDesc);

        {- now create an order for the user proc/group }
        db:= OpenV30File(DBORDFile);
        if db = nil then
          CriticalError('Cannot open Order file');
        db^.dbr^.PutFieldAsStr(DBORDID, o^.orderID);
        db^.dbr^.PutFieldAsStr(DBORDDesc, o^.ordDesc);
        o^.aSet:= GetGroupSet(o^.grpID);
        db^.dbr^.PutField(DBORDSet, @o^.aSet);
        if not db^.dbc^.InsertRec(db^.dbr) then
        begin
          StrCopy(pstr, 'Cannot insert order : ');
          StrCat(pstr, o^.orderID);
          CriticalError(pstr);
        end;
        o^.orderSeq:= db^.dbr^.GetSeqValue;
        Dispose(db, Done);

        {- now create the orderXref for the order/group }
        db:= OpenV30File(DBORDXRFFile);
        if db = nil then
          CriticalError('Cannot open Order Xref file');
        db^.dbr^.PutField(DBORDXRFOrdRef, @o^.orderSeq);
        db^.dbr^.PutField(DBORDXRFTstGrpRef, @o^.grpSeq);
        if not db^.dbc^.InsertRec(db^.dbr) then
        begin
          StrCopy(pstr, 'Cannot insert order xref record : ');
          StrCat(pstr, o^.orderID);
          CriticalError(pstr);
        end;
        Dispose(db, Done);
      end;
    end;
  end;

 {    if the order seq = 0 then
       create the order
       ---> if the group seq = 0 then
              create the group
              ---> for USER procs call CreateUserGroup
                   else use an existing order.
       <------grpSeq
       add order xref
       add order record.
       set the group and order seq numbers in the order item
  }
end;

constructor TBatDlg.Init(aParent: PWindowsObject; aSpecSeq: TSeqNum; aBatXref: PBatID);
begin
  inherited Init(aParent, MakeIntResource(DLG_BATMAP));
  grid:= New(PGrid, InitResource(@self, IDC_GRID, 4, false));
  batXref:= aBatXref;
  specSeq:= aSpecSeq;
end;

procedure TBatDlg.SetupWindow;
begin
  inherited SetupWindow;
  SetDlgItemText(hWindow, IDC_BATID, batXref^.batID);
  grid^.SetHeader(g_Proc, 'Procedure');
  grid^.SetHeader(g_Order, 'Order');
  grid^.SetHeader(g_Group, 'Test Group');
  grid^.SetHeader(g_Iso, 'Isolate');
  FillGrid;
  grid^.SetSelIndex(0);
end;

function TBatDlg.CanClose: boolean;
var
  ret   : boolean;
  k     : integer;
  o     : POrder;
  db    : PDBFile;
  tw    : TWaitCursor;

  function FindIso(idx: integer; iso: PChar): boolean;
  {- search the batXref for the iso, ignoring the specified index }
  var
    found : boolean;
    i     : integer;
    ordobj: POrder;
  begin
    found:= false;
    for i:= 0 to batXref^.orders^.count - 1 do
    begin
      if i <> idx then
      begin
        ordObj:= batXref^.orders^.At(i);
        found:= StrComp(iso, ordObj^.isoID) = 0;
      end;
      if found then break;
    end;
    FindIso:= found;
  end;

  function FindOrd(idx: integer; sel: POrder): boolean;
  var
    found : boolean;
    i     : integer;
    ordobj: POrder;
  begin
    found:= false;
    for i:= 0 to batXref^.orders^.count - 1 do
    begin
      if i <> idx then
      begin
        ordObj:= batXref^.orders^.At(i);
        found:= StrComp(sel^.orderID, ordObj^.orderID) = 0;
      end;
      if found then break;
    end;
    FindOrd:= found;
  end;

  function IsoExists(isoID: PChar): boolean;
  begin
    db^.dbr^.PutField(DBISOSpecRef, @specSeq);
    db^.dbr^.PutFieldAsStr(DBISOIso, isoID);
    IsoExists:= db^.dbc^.GetEQ(db^.dbr);
  end;

begin
  ret:= true;
  for k:= 0 to batXref^.orders^.count - 1 do
  begin
    o:= batXref^.orders^.At(k);
    if strlen(o^.orderID) = 0 then
    begin
      ErrorMsg(hWindow, 'Error', 'Order ID is required.');
      grid^.SetSelIndex(k);
      ret:= false;
      break;
    end
    else if (o^.orderSeq = 0) and (StrLen(o^.grpID) = 0) then
    begin
      ErrorMsg(hWindow, 'Error', 'Group ID is required');
      grid^.SetSelIndex(k);
      ret:= false;
      break;
    end
    else if StrLen(o^.isoID) = 0 then
    begin
      ErrorMsg(hWindow, 'Error', 'Isolate ID is required');
      grid^.SetSelIndex(k);
      ret:= false;
      break;
    end;
  end;

  {- now validate that all isolates are unique and do not already exist }
  if ret then
  begin
    db:= OpenV30File(DBIsoFile);
    for k:= 0 to batXref^.orders^.count - 1 do
    begin
      o:= batXref^.orders^.At(k);
      if o^.orderSeq = 0 then
      begin
        if FindOrd(k, o) then
        begin
          ErrorMsg(hWindow, 'Error', 'Duplicate order IDs are not allowed.');
          grid^.SetSelIndex(k);
          ret:= false;
          break;
        end;
      end;

      if FindIso(k, o^.isoID) then
      begin
        ErrorMsg(hWindow, 'Error', 'Duplicate isolate id entered.');
        grid^.SetSelIndex(k);
        ret:= false;
        break;
      end
      else if IsoExists(o^.isoID) then
      begin
        ErrorMsg(hWindow, 'Error', 'Isolate already exists in database.');
        grid^.SetSelIndex(k);
        ret:= false;
        break;
      end;
    end;
    Dispose(db, Done);
  end;

  {- now create orders and groups as necessary }
  if ret then
  begin
    tw.Init;
    XferOrder(batXref);
    tw.Done;
  end;

  FocusCtl(hWindow, IDC_GRID);
  CanClose:= ret;
end;

procedure TBatDlg.FillGrid;
var
  pstr  : array[0..100] of char;
  o     : POrder;
  row   : integer;
  k     : integer;
  seq   : TSeqNum;
begin
  grid^.SetRedraw(false);
  grid^.ClearList;
  for k:= 0 to batXref^.orders^.count-1 do
  begin
    o:= batXref^.orders^.At(k);
    row:= grid^.AddString('');
    grid^.SetData(g_Proc, row, o^.origProc);
    grid^.SetData(g_Group, row, o^.grpID);
    grid^.SetData(g_Iso, row, o^.isoID);
    grid^.SetData(g_Order, row, o^.orderID);
  end;
  grid^.SetColumnWidth(g_Proc, 1);
  grid^.SetColumnWidth(g_Order, 1);
  grid^.SetColumnWidth(g_Group, 1);
  grid^.SetColumnWidth(g_Iso, 1);
  grid^.OptColumnWidth(g_Proc);
  grid^.OptColumnWidth(g_Order);
  grid^.OptColumnWidth(g_Group);
  grid^.StretchColumn(g_Iso);
  grid^.SetRedraw(true);
end;

procedure TBatDlg.Cancel(var msg: TMessage);
begin
  if ConfirmExit(hWindow) then
    inherited Cancel(msg);
end;

procedure TBatDlg.WMCommand(var msg: TMessage);
begin
  inherited WMCommand(msg);
  if (msg.lParamLo <> 0) then   {- message is from a control }
  begin
    if msg.lParamHI = LBN_DBLCLK then
      IDEdit(msg);
  end;
end;

procedure TBatDlg.WMCharToItem(var msg: TMessage);
begin
  grid^.CharToItem(msg);
end;

procedure TBatDlg.WMDrawItem(var msg: TMessage);
begin
  grid^.DrawItem(msg);
end;

procedure TBatDlg.WMMeasureItem(var msg: TMessage);
begin
  grid^.MeasureItem(msg);
end;

procedure TBatDlg.IDEdit(var msg: TMessage);
var
  k     : integer;
  o     : POrder;
begin
  k:= grid^.GetSelIndex;
  if k >= 0 then
  begin
    o:= batXref^.orders^.At(k);
    application^.ExecDialog(New(PBatEditDlg, Init(@self, batXref^.batID, o)));
    FillGrid;
    grid^.SetSelIndex(k);
  end;
end;

procedure TBatDlg.IDDone(var msg: TMessage);
begin
  if CanClose then
    EndDlg(IDOK);
end;

procedure BatToOrd(aBatXref: PBatID);
{- create 1 order for the given battery }
var
  k     : integer;
  db    : PDBFile;
  xref  : PDBFile;
  sList : PStringList;
  oItem : POrder;
  aSet  : byte;
  newSet: byte;
  pstr  : array[0..100] of char;
  seq   : TSeqNum;
begin
  db:= OpenV30File(DBORDFile);
  if db = nil then
    CriticalError('Cannot open Orders file.');
  xref:= OpenV30File(DBORDXRFFile);
  if xref = nil then
    CriticalError('Cannot open Orders Xref file.');

  sList:= New(PStringList, Init);
  GetTestProcList(aBatXref^.batID, sList);
  oItem:= aBatXref^.orders^.At(0);
  oItem^.aSet:= 8;
  db^.dbr^.PutFieldAsStr(DBORDID, oItem^.orderID);
  db^.dbr^.PutFieldAsStr(DBORDDesc, oItem^.ordDesc);
  newSet:= 8;
  db^.dbr^.PutField(DBORDSet, @oItem^.aSet);

  if db^.dbc^.InsertRec(db^.dbr) then
  begin
    oItem^.orderSeq:= db^.dbr^.GetSeqValue;
    for k:= 1 to sList^.NumEntries do
    begin
      sList^.GetEntry(k, pstr, sizeof(pstr)-1); {- get a procedure name }
      if strcomp(pstr, 'USER') = 0 then
        seq:= CvtGroup(aBatXref^.batID, 'USER', oItem^.grpID, oItem^.grpDesc)
      else
      begin
        seq:= GroupExists(pstr, nil);
        if seq = 0 then
        begin
          StrCopy(pstr, 'Cannot locate test group : ');
          StrCat(pstr, pstr);
          CriticalError(pstr);
        end;
        if newSet = oItem^.aSet then
          newSet:= GetGroupSet(pstr);
      end;
      xref^.dbr^.PutField(DBORDXRFOrdRef, @oItem^.orderSeq);
      xref^.dbr^.PutField(DBORDXRFTstGrpRef, @seq);
      if not xref^.dbc^.InsertRec(xref^.dbr) then
      begin
        CriticalError('Cannot create order cross reference record.');
      end;
    end;
  end
  else
    CriticalError('Cannot create order.');

  if newSet <> aSet then  {- update the order set }
  begin
    oItem^.aSet:= newSet;
    db^.dbr^.PutField(DBORDSet, @newSet);
    db^.dbc^.UpdateRec(db^.dbr);
  end;

  Dispose(sList, Done);
  Dispose(db, Done);
  Dispose(xref, Done);
end;

type
  POrdDlg = ^TOrdDlg;
  TOrdDlg = object(TCenterDlg)
    batXref   : PBatID;
    order     : PFocEdit;
    grp       : PFocEdit;
    ordDesc   : PEdit;
    grpDesc   : PEdit;
    constructor Init(aParent: PWindowsObject; aBatXref: PBatID);
    procedure SetupWindow; virtual;
    procedure ListKey(var msg: TMessage); virtual WM_FIRST + WM_LISTKEY;
    procedure LostFocus(var msg: TMessage); virtual WM_FIRST + WM_LOSTFOCUS;
    function CanClose: boolean; virtual;
    procedure Cancel(var msg: TMessage); virtual ID_FIRST + ID_CANCEL;
  end;

constructor TOrdDlg.Init(aParent: PWindowsObject; aBatXref: PBatID);
begin
  inherited Init(aParent, MakeIntResource(DLG_GETORD));
  order:= New(PFocEdit, InitResource(@self, IDC_BATTERY, 9));
  grp:= New(PFocEdit, InitResource(@self, IDC_GROUP, 9));
  ordDesc:= New(PEdit, InitResource(@self, IDC_ORDDESC, 32));
  grpDesc:= New(PEdit, InitResource(@self, IDC_GRPDESC, 32));
  batXref:= aBatXref;
end;

procedure TOrdDlg.SetupWindow;
var
  sList : PStringList;
  k     : integer;
  pstr  : array[0..100] of char;
begin
  inherited SetupWindow;
  SetDlgItemText(hWindow, LBL_BATTERY, batXref^.batID);

  EnableWindow(grp^.hWindow, false);
  EnableWindow(GetItemHandle(LBL_GRPID), false);
  EnableWindow(GetItemHandle(IDC_GRPDESC), false);
  sList:= New(PStringList, Init);
  GetTestProcList(batXref^.batID, sList);
  for k:= 1 to sList^.NumEntries do
  begin
    sList^.GetEntry(k, pstr, sizeof(pstr)-1);
    if strcomp(pstr, 'USER') = 0 then
    begin
      EnableWindow(grp^.hWindow, true);
      EnableWindow(GetItemHandle(LBL_GRPID), true);
      EnableWindow(GetItemHandle(IDC_GRPDESC), true);
      break;
    end;
  end;
  Dispose(sList, Done);
end;

procedure TOrdDlg.LostFocus(var msg: TMessage);
var
  pstr  : array[0..50] of char;
  desc  : array[0..50] of char;
begin
  case msg.wParam of
    IDC_BATTERY:
      begin
        order^.GetText(pstr, sizeof(pstr)-1);
        if OrderExists(pstr, desc) > 0 then
          ordDesc^.SetText(desc);
      end;
    IDC_GROUP:
      begin
        grp^.GetText(pstr, sizeof(pstr)-1);
        if GroupExists(pstr, desc) > 0 then
          grpDesc^.SetText(desc);
      end;
  end;
end;

procedure TOrdDlg.ListKey;
var
  pstr  : array[0..100] of char;
begin
  case msg.wParam of
    IDC_BATTERY:
      begin
        if ListOrders(@self, pstr, sizeof(pstr)-1) then
          order^.SetText(pstr);
      end;
    IDC_GROUP:
      if IsWindowEnabled(grp^.hWindow) then
      begin
        if ListGroups(@self, pstr, sizeof(pstr)-1) then
          grp^.SetText(pstr);
        FocusCtl(hWindow, grp^.GetID);
      end;
  end;
end;

procedure TOrdDlg.Cancel(var msg: TMessage);
begin
  if ConfirmExit(hWindow) then
    inherited Cancel(msg);
end;

function TOrdDlg.CanClose: boolean;
var
  oItem : POrder;
  tw    : TWaitCursor;
  aDesc : array[0..100] of char;
begin
  CanClose:= false;
  {- force focus processing }
  PostMessage(hWindow, WM_LOSTFOCUS, IDC_BATTERY, 0);
  PostMessage(hWindow, WM_LOSTFOCUS, IDC_GROUP, 0);

  oItem:= batXref^.orders^.At(0);
  order^.GetText(oItem^.orderID, 8);
  ordDesc^.GetText(oItem^.ordDesc, 32);
  grp^.GetText(oItem^.grpID, 8);
  grpDesc^.GetText(oItem^.grpDesc, 32);

  if strlen(oItem^.orderID) = 0 then
  begin
    ErrorMsg(hWindow, 'Error', 'An Order ID is required.');
    FocusCtl(hWindow, order^.GetID);
    exit;
  end;

  oItem^.orderSeq:= OrderExists(oItem^.orderID, aDesc);
  if oItem^.orderSeq > 0 then   {- order exists ??? }
  begin
    ordDesc^.SetText(aDesc);
    ordDesc^.GetText(oItem^.ordDesc, 32);

    tw.Init;
    if not CompOrderAndBattery(oItem^.orderSeq, batXref^.batID) then
    begin
      ErrorMsg(hWindow, 'Error', 'Order is not compatible with the original battery.');
      FocusCtl(hWindow, order^.GetID);
      exit;
    end;
    tw.Done;
  end;

  if oItem^.orderSeq = 0 then
    if strlen(oItem^.ordDesc) = 0 then
    begin
      ErrorMsg(hWindow, 'Error', 'Order description is required for new orders.');
      FocusCtl(hWindow, IDC_ORDDESC);
      exit;
    end;

  if IsWindowEnabled(grp^.hWindow) then
  begin
    if StrLen(oItem^.grpID) = 0 then
    begin
      ErrorMsg(hWindow, 'Error', 'An Group ID is required.');
      FocusCtl(hWindow, grp^.GetID);
      exit;
    end;
    oItem^.grpSeq:= GroupExists(oItem^.grpID, aDesc);
    if oItem^.grpSeq > 0 then
    begin
      grpDesc^.SetText(aDesc);
      tw.Init;
      if not CompGroupAndProc(oItem^.grpID, batXref^.batID, 'USER') then
      begin
        ErrorMsg(hWindow, 'Error', 'Group is not compatible with the original USER procedure.');
        FocusCtl(hWindow, grp^.GetID);
        exit;
      end;
      tw.Done;
    end;

    grpDesc^.GetText(oItem^.grpDesc, 32);
    if strlen(oItem^.grpDesc) = 0 then
    begin
      ErrorMsg(hWindow, 'Error', 'Group description is required for new test groups.');
      FocusCtl(hWindow, IDC_GRPDESC);
      exit;
    end;
  end;

  {- at this point the order needs to be checked }
  if oItem^.orderSeq = 0 then
  begin
    tw.Init;
    BatToOrd(batXref);
    tw.Done;
  end;

  CanClose:= true;
end;

function Cvt20_Battery(aParent: PWindowsObject; var abort: boolean; specSeq: TSeqNum; aBatID: PChar): PBatID;
{- look in the battery Xref collection for the battery ID. If found then return
   the battery ID xref item. Otherwise, create new order(s) from the
   battery, add the order item info to the xref collection, and return the
   battery xref item.}

  function Matches(item: PBatID): boolean; far;
  begin
    Matches:= StrComp(item^.batID, aBatID) = 0;
  end;

var
  batXref : PBatID;
  pstr    : array[0..100] of char;
  grp     : array[0..10] of char;
  sList   : PStringList;
  k       : integer;
  seq     : TSeqNum;
  oItem   : POrder;
begin
  Cvt20_Battery:= nil;

  {- special case for normal flora / no growth batteries. }
  {- These cases allow NF/NG to be converted even though they do not have tests}
  if (StrComp(aBatID, 'NF') <> 0) and (StrComp(aBatID, 'NG') <> 0) then
    if NumProcsInBattery(aBatID) = 0 then
    begin
      LogStr('No test procedures defined for battery ');
      LogStr(aBatID);
      LogStr(' - Battery not converted.');
      LogLn;
      exit;
    end;

  abort:= false;
  sList:= New(PStringList, Init);
  {- the battery has not been seen yet. It needs to be converted to an order}
  batXref:= batIDs^.FirstThat(@Matches);
  if batXref = nil then
  begin
    if CheckDupTests(aBatID) = 0 then
    begin
      if (strcomp(aBatID, 'NF') = 0) or (strcomp(aBatID, 'NG') = 0) then
      begin
        seq:= OrderExists(aBatID, pstr);
        if seq > 0 then   {- if NG or NF already exist in the 3.x database }
        begin
          batXref:= New(PBatID, Init(aBatID));
          batXref^.AddAnOrder(seq, 0, '', '');
          batIDS^.Insert(batXref);
        end
        else
        begin
          LogStr('-- Normal Flora or No Growth battery encountered with no equivelent');
          LogStr('-- order in the v.3.02 database.');
          LogLn;
          ErrorMsg(aParent^.hWindow, 'Error',
            'A Normal Flora (NF) or No Growth (NG) battery was encountered ' +
            'with no corresponding order ID in the v.3.02 database.'#10#10 +
            'Conversion process cancelled.');
          abort:= true;
          exit;
        end;
      end
      else
      begin
        batXref:= New(PBatID, Init(aBatID));
        batXref^.AddAnOrder(0, 0, '', '');
        if application^.ExecDialog(New(POrdDlg, Init(aParent, batXref))) = IDCANCEL then
        begin
          LogStr('Convertion Aborted.');
          LogLn;
          abort:= true;
          Dispose(batXref, Done);
        end
        else
          batIDS^.Insert(batXref);
      end;
    end
    else
    begin
      GetTestProcList(aBatID, sList);
      batXref:= New(PBatID, Init(aBatID));
      for k:= 1 to sList^.NumEntries do
      begin
        sList^.GetEntry(k, pstr, sizeof(pstr)-1);
        seq:= TestGroupForProc(pstr, grp, sizeof(grp)-1);
        batXref^.AddAnOrder(0, seq, pstr, grp);
      end;
      for k:= 0 to batXref^.orders^.count - 1 do
      begin
        oItem:= batXref^.orders^.At(k);
        if GroupExists(oItem^.grpID, pstr) > 0 then
          StrCopy(oItem^.grpDesc, pstr);
      end;

      if application^.ExecDialog(New(PBatDlg, Init(aParent, specSeq, batXref))) = IDCANCEL then
      begin
        abort:= true;
        LogStr('Convertion Aborted.');
        LogLn;
        Dispose(batXref, Done);
        batXref:= nil;
      end
      else
        batIDS^.Insert(batXref);
    end;
  end
  else {- if the battery was split up, then re-prompt for isolate numbers }
  begin
    for k:= 0 to batXref^.orders^.count - 1 do
    begin
      oItem:= batxref^.orders^.At(k);
      StrCopy(oItem^.isoID, '');
    end;
    if batxref^.orders^.count > 1 then  {- battery was split }
    begin
      if application^.ExecDialog(New(PBatDlg, Init(aParent, specSeq, batXref))) = IDCANCEL then
      begin
        abort:= true;
        LogStr('Convertion Aborted.');
        LogLn;
        Dispose(batXref, Done);
        batXref:= nil;
      end
    end;
  end;

  Cvt20_Battery:= batXref;
  Dispose(sList, Done);
end;

function Cvt20_Isolates(aParent: PWindowsObject; doStop: TStopType; maps: PMapCollection;
                        aList: PListBox): boolean;
var
  db2           : PDB2Record;
  iso           : PDBFile;
  fld           : PDB2Field;
  seq           : TSeqNum;
  pstr          : array[0..100] of char;
  ret           : boolean;
  isOK          : boolean;
  spec          : PDBFile;
  b             : boolean;
  specID, cDate : array[0..20] of char;
  isoID         : array[0..10] of char;
  specSeq       : TSeqNum;
  batXref       : PBatID;
  idx           : integer;
  oItem         : POrder;
  v2Res         : PV2Results;
  v3Res         : PV3Results;
  abort         : boolean;
  setFam        : word;

  function CheckIsoExists: boolean;
  {- check to see if an isolate exists for a specimen or an error occurs then
     return false, else return true}
  begin
    CheckIsoExists:= false;

    {- specimen info is in db2 (batord) record}
    fld:= db2^.GetField(DbifFldBatteryOrdSpecimenID);
    StrCopy(specID, fld^.value);

(* DEBUG CODE*)
(*if strcomp(fld^.value, '00000555') <> 0 then*)
(*begin*)
(*  checkisoexists:= false;*)
(*  exit;*)
(*end;*)


    fld:= db2^.GetField(DbifFldBatteryOrdCollectDate);
    StrCopy(cDate, fld^.value);
    fld:= db2^.GetField(DbifFldBatteryOrdIsolateID);
    StrCopy(isoID, fld^.value);

    spec^.dbr^.ClearRecord;
    iso^.dbr^.ClearRecord;
    spec^.dbr^.PutFieldAsStr(DBSPECSpecID, specID);
    spec^.dbr^.PutFieldAsStr(DBSPECCollectDate, cDate);
    specSeq:= 0;
    if spec^.dbc^.GetEQ(spec^.dbr) then
    begin
      specSeq:= spec^.dbr^.GetSeqValue;
      iso^.dbr^.PutFieldAsStr(DBISOIso, isoID);
      iso^.dbr^.PutField(DBISOSpecRef, @specSeq);
      if not iso^.dbc^.GetEQ(iso^.dbr) then
      begin
(*        LogStr('Isolate PASSED - ');*)
(*        LogStr('Spec: '); LogStr(specID);*)
(*        LogStr(' '); LogStr(cDate);*)
(*        LogStr(' Isolate: ');*)
(*        LogStr(isoID);*)
(*        LogLn;*)
        CheckIsoExists:= true;
      end
      else
      begin
        LogStr('Isolate already exists - ');
        LogStr('Spec: '); LogStr(specID);
        LogStr(' '); LogStr(cDate);
        LogStr(' Isolate: ');
        LogStr(isoID);
        LogStr(' - Isolate not converted');
        LogLn;
      end;
    end
    else
    begin
      LogStr('Specimen not found for isolate - ');
      LogStr('Spec: '); LogStr(specID);
      LogStr(' '); LogStr(cDate);
      LogStr(' Isolate: ');
      LogStr(isoID);
      LogLn;
    end;
  end;

  procedure SaveIsolate;
  {- save the isolate record in the v 3.0 database }
  begin
    ShowCurrentIso(iso, aList);
    if not iso^.dbc^.InsertRec(iso^.dbr) then
    begin
      fld:= db2^.GetField(DBISOSpecID);
      StrCopy(pstr, fld^.value);
      StrCat(pstr, ' ');
      fld:= db2^.GetField(DBISOCollectDate);
      StrCat(pstr, fld^.value);
      fld:= db2^.GetField(DBIsoIso);
      StrCat(pstr, ' - ');
      StrCat(pstr, fld^.value);
      CheckDBError(iso, pstr);
    end;
  end;

begin
  ret:= true;
  abort:= false;
  db2:= DB2OpenFile('BATORD');
  if db2 = nil then
    CriticalError('Cannot open V2.x BATORD file.');
  iso:= OpenV30File(dbIsoFile);
  if iso = nil then
    CriticalError('Cannot open V3.02 ISOLATE file.');
  spec:= OpenV30File(dbSpecFile);
  if spec = nil then
    CriticalError('Cannot open V3.02 SPECIMEN file.');

  v2Res:= New(PV2Results, Init);
  v3Res:= New(PV3Results, Init);

  isOK:= DB2GetFirst(db2);
  while isOK and not abort and not doStop^ do
  begin
    HandleEvents(aParent^.hWindow);
    if doStop^ then
      break;
    iso^.dbr^.ClearRecord;
    {- first see that the isolate does not already exist in the 3.0 system }
    if CheckIsoExists then
    begin
      HandleEvents(aParent^.hWindow);
      if (specSeq > 0) and not doStop^  then
      begin
        iso^.dbr^.ClearRecord;
        {- transfer system fields to version 3.0 record }
        iso^.dbr^.PutField(DBISOSpecRef, @specSeq);

        fld:= db2^.GetField(DbifFldBatteryOrdIsolateID);
        StrCopy(pstr, fld^.value);
        FormatDMSField(pstr, 3);
        iso^.dbr^.PutFieldAsStr(DBISOIso, pstr);
(*        XferFld(db2, iso^.dbr, DbifFldBatteryOrdIsolateID, DBISOIso);*)

        XferFld(db2, iso^.dbr, DbifFldBatteryOrdFreeText, DBISOFreeText);
        XferFld(db2, iso^.dbr, DbifFldBatteryOrdTestDate, DBISOTstDate);

        fld:= db2^.GetField(DbifFldBatteryOrdQCFlag);
        b:= StrComp(fld^.value, 'Y') = 0;
        iso^.dbr^.PutField(DBISOQCFlag, @b);

        fld:= db2^.GetField(DbifFldBatteryOrdTransmitted);
        b:= StrComp(fld^.value, 'Y') = 0;
        iso^.dbr^.PutField(DBISOTransFlag, @b);

        iso^.dbr^.PutFieldAsStr(DBISOSpecID, specID);
        iso^.dbr^.PutFieldAsStr(DBISOCollectDate, cDate);

        fld:= db2^.GetField(DbifFldBatteryOrdOrganism);
        seq:= Cvt20_Org(fld^.value);
        iso^.dbr^.PutField(DBISOOrgRef, @seq);

        fld:= db2^.GetField(DbifFldBatteryOrdSession);
        seq:= Cvt20_Session(fld^.value);
        iso^.dbr^.PutField(DBISOSess, @seq);

        {- now transfer mapped fields to v30 record }
        XferMapFlds('ISOLATE', db2, iso, maps);

        {- now, find the battery cross reference }
        fld:= db2^.GetField(DbifFldBatteryOrdDefID);
        batXref:= Cvt20_Battery(aParent, abort, specSeq, fld^.value);
        HandleEvents(aParent^.hWindow);
        if not abort and not doStop^ then
        begin
          {- fld^.value should have the order ID in it at this point }
          if (StrComp(fld^.value, 'NF') = 0) or (StrComp(fld^.value, 'NG') = 0) then
          begin
            if batXref <> nil then
            begin
              oItem:= batXref^.orders^.At(0);
              iso^.dbr^.PutField(DBISOOrdRef, @oItem^.orderSeq);
            end;
            SaveIsolate;
            if batXref <> nil then
            begin
              abort:= not CreateIsoOrders(iso);
              if not abort then
              begin
                v3Res^.LoadResults(iso^.dbr);
                if not v3Res^.SaveResults then
                begin
                  LogHex(v3Res^.dbErrorNum);
                  LogStr(' - Error saving NF/NG results.');
                  LogLn;
                  abort:= true;
                  CriticalError('Error saving NF/NG results.');
                end;
              end
              else
                CriticalError('Cannot create isolate orders.');
            end;
          end
          else if (batXref = nil) or (batXref^.orders^.count = 0) then
            SaveIsolate {- no orders for battery }
          else
          begin
            {- Load V2 results for battery order  }
            v2Res^.LoadResults(db2);
            if batXref^.orders^.count > 1 then
            begin
              for idx:= 0 to batXref^.orders^.count - 1 do
              begin
                HandleEvents(aParent^.hWindow);
                if not doStop^ then
                begin
                  oItem:= batXref^.orders^.At(idx);
                  iso^.dbr^.PutFieldAsStr(DBISOIso, oItem^.isoID);
                  iso^.dbr^.PutField(DBISOOrdRef, @oItem^.orderSeq);
                  setFam:= MakeSetFamily(oItem^.aSet, 1);
                  iso^.dbr^.PutField(DBISOSetFamily, @setFam);
                  SaveIsolate;
                  abort:= not CreateIsoOrders(iso);
                  if not abort then
                  begin
                    v3Res^.LoadResults(iso^.dbr);
                    abort:= not v2Res^.XferResults(v3Res, oItem^.origProc);
                    if abort then
                      CriticalError('Cannot transfer results.');
                  end
                  else
                    CriticalError('Cannot create isolate orders.');
                end
                else
                  break;
              end;
            end
            else
            begin
              oItem:= batXref^.orders^.At(0);
              iso^.dbr^.PutField(DBISOOrdRef, @oItem^.orderSeq);
              setFam:= MakeSetFamily(oItem^.aSet, 1);
              iso^.dbr^.PutField(DBISOSetFamily, @setFam);
              SaveIsolate;
              abort:= not CreateIsoOrders(iso);
              if not abort then
              begin
                v3Res^.LoadResults(iso^.dbr);
                abort:= not v2Res^.XferResults(v3Res, nil);
                if abort then
                   CriticalError('Cannot transfer results.');
              end
              else
                CriticalError('Error creating isolate orders (1).');
            end;
          end;
        end;
      end;
    end;
    if not abort then
      isOK:= DB2GetNext(db2);
  end;

  Dispose(v2Res, Done);
  Dispose(v3Res, Done);
  Dispose(iso, Done);
  Dispose(spec, Done);
  DB2CloseFile(db2);
  Cvt20_Isolates:= not abort;
end;

END.

{- rcf }
