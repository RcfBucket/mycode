          MicroScan's Data Management System is the
     world's premier information storage, retrieval,
     and reporting system for the microbiology
     laboratory.  Its wealth of features and ease of
     use are available exclusively to MicroScan
     customers.
