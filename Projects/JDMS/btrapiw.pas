{*************************************************************************
**
**  Copyright 1982-1994 Btrieve Technologies, Inc. All Rights Reserved
**
*************************************************************************}
{***********************************************************************
   BTRAPIW.PAS
      This is the Pascal unit for MS Windows Btrieve to be called by
      Borland Pascal for Windows or Borland Turbo Pascal for Windows.

************************************************************************}
UNIT btrapiw;

INTERFACE
{***********************************************************************
   The following types are needed for use with 'BTRCALLBACK'.
************************************************************************}
TYPE
   SQL_YIELD_T = RECORD
      iSessionID : WORD;
   END;

   BTRV_YIELD_T = RECORD
      iOpCode           : WORD;
      bClientIDlastFour : ARRAY[ 1..4 ] OF BYTE;
   END;
     
   BTI_CB_INFO_T = RECORD
      typex : WORD;
      size  : WORD;
      case u: Boolean of
         False: ( sYield : SQL_YIELD_T );
         True:  ( bYield : BTRV_YIELD_T );
   END;

   BTI_CB_FUNC_PTR_T = FUNCTION(
                          VAR bCallbackInfo : BTI_CB_INFO_T;
                          VAR bUserData )   : WORD;

   BTI_CB_FUNC_PTR_PTR_T = ^BTI_CB_FUNC_PTR_T;
{***********************************************************************
   PLATFORM-INDEPENDENT FUNCTIONS
     BTRV and BTRVID are the same on all platforms for which they have
     an implementation.  We recommend that you use only these two
     functions with Btrieve 6.x client components, and then issue
     the B_STOP operation prior to exiting your application.
************************************************************************}
   FUNCTION BTRV(
                   operation      : WORD;
               VAR positionBlock;
               VAR dataBuffer;
               VAR dataLen        : WORD;
               VAR keyBuffer;
                   keyNumber      : INTEGER ) : INTEGER;
 
   FUNCTION BTRVID(
                   operation      : WORD;
               VAR positionBlock;
               VAR dataBuffer;
               VAR dataLen        : WORD;
               VAR keyBuffer;
                   keyNumber      : INTEGER;
               VAR clientID )     : INTEGER;

{***********************************************************************
   BTRCALLBACK - Used to register call-back function to Btrieve.
************************************************************************}
   FUNCTION BTRCALLBACK(
               iAction                   : WORD;
               iOption                   : WORD;
               fCallBackFunction         : BTI_CB_FUNC_PTR_T;
               fPreviousCallBackFunction : BTI_CB_FUNC_PTR_PTR_T;
           VAR bUserData;
           VAR bPreviousUserData         : POINTER;
           VAR bClientID )               : INTEGER;

{***********************************************************************
   HISTORICAL FUNCTIONS
      These APIs were needed prior to Btrieve 6.x client
      components.  Older applications may still call these functions,
      and the Btrieve Windows 6.x client component will work correctly.   
      New applications using the 6.x client components do NOT have to
      call these functions.
************************************************************************}
   FUNCTION BTRVINIT( VAR initializationString ) : INTEGER;
   FUNCTION BTRVSTOP : INTEGER;
   FUNCTION BRQSHELLINIT( VAR initializationString ): INTEGER;

IMPLEMENTATION

uses
  dmsdebug,
  btrconst;
{***********************************************************************
   PLATFORM-SPECIFIC FUNCTIONS
      These APIs are specific to the MS Windows platform.  With the
      exception of BTRCALLBACK, we recommend that you use either
      BTRV or BTRVID, shown above.  Slight performance gains can be
      achieved by using BTRCALL or BTRCALLID.
************************************************************************}
   FUNCTION BTRCALL(
                   operation : WORD;
               VAR posblk;
               VAR databuf;
               VAR datalen   : WORD;
               VAR keybuf;
                   keylen    : BYTE;
                   keynum    : INTEGER ) : INTEGER; FAR;
                   external 'WBTRCALL' name 'BTRCALL';

   FUNCTION BTRCALLID(
                   operation  : WORD;
               VAR posblk;
               VAR databuf;
               VAR datalen    : WORD;
               VAR keybuf;
                   keylen     : BYTE;
                   keynum     : INTEGER;
               VAR clientID ) : INTEGER; FAR;
                   external 'WBTRCALL'  name 'BTRCALLID';

   FUNCTION BTRCALLBACK(
               iAction                   : WORD;
               iOption                   : WORD;
               fCallBackFunction         : BTI_CB_FUNC_PTR_T;
               fPreviousCallBackFunction : BTI_CB_FUNC_PTR_PTR_T;
           VAR bUserData;
           VAR bPreviousUserData         : POINTER;
           VAR bClientID )               : INTEGER;
               external 'WBTRCALL'  name 'BTRCALLBACK';

   { Implementation of BTRV }
   FUNCTION BTRV(   operation      : WORD;
                VAR positionBlock;
                VAR dataBuffer;
                VAR dataLen        : WORD;
                VAR keyBuffer;
                    keyNumber      : INTEGER ): INTEGER;
   VAR
     keyLen: BYTE;
   BEGIN
    keyLen:= 255;                       {maximum key length}
    BTRV := BTRCALL(
               operation,
               positionBlock,
               dataBuffer,
               dataLen,
               keyBuffer,
               keyLen,
               keyNumber );
   END; {BTRV}


   { Implementation of BTRVID }
   FUNCTION BTRVID(
                   operation      : WORD;
               VAR positionBlock;
               VAR dataBuffer;
               VAR dataLen        : WORD;
               VAR keyBuffer;
                   keyNumber      : INTEGER;
               VAR clientID )     : INTEGER;
   VAR
     KeyLen: Byte;
   BEGIN
    KeyLen:= 255;                       {maximum key length}
    BTRVID := BTRCALLID(
                 operation,
                 positionBlock,
                 dataBuffer,
                 dataLen,
                 keyBuffer,
                 keyLen,
                 keyNumber, 
                 clientID );
   END; {BTRVID}
 
   Function WBSHELLINIT(
               VAR initializationString ): INTEGER; FAR;
                   external 'WBTRCALL'  name 'WBSHELLINIT';

   Function WBRQSHELLINIT(
               VAR initializationString ): INTEGER; FAR;
                   external 'WBTRCALL'  name 'WBRQSHELLINIT';

   FUNCTION WBTRVINIT(
               VAR intializationString): INTEGER; FAR;
                   external 'WBTRCALL'  name 'WBTRVINIT';

   FUNCTION WBTRVSTOP: INTEGER; FAR;
               external 'WBTRCALL'  name 'WBTRVSTOP';

   { Implementation of BTRVINIT }
   FUNCTION BTRVINIT( VAR initializationString ): INTEGER;
      BEGIN
         BTRVINIT := WBTRVINIT( initializationString );
      END; {End BTRVINIT}

   { Implementation of BTRVSTOP }
   FUNCTION BTRVSTOP: INTEGER;
      BEGIN
         BTRVSTOP:= WBTRVSTOP;
      END; {End BTRVSTOP}

   { Implementation of BRQSHELLINIT }
   FUNCTION BRQSHELLINIT( VAR initializationString ): INTEGER;
      BEGIN
        BRQSHELLINIT:= WBRQSHELLINIT( initializationString );
      END; {End BRQSHELLINIT}

Var
  saveExit : Pointer;

procedure ExitRoutine; Far;
Var
  d, p, k: Word;
begin
  exitproc:= saveExit;
  BTRV(B_STOP, p, d, k, k, k);
end;

BEGIN
  saveExit:= exitProc;
  ExitProc:= @ExitRoutine;
END.
