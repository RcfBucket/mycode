unit StrsW;

{--------------------------------------------------------------------------}
{ Windows string manipulation library. This unit provides additional       }
{ routines to manipultate PChar strings. The routines are written in       }
{ assembly for fast operation.                                             }
{                                                                          }
{ Date   Who   Did What                                                    }
{ ------ ----- ----------------------------------------------------------- }
{ 10/94  rcf   Initial creation of assembly modules and pascal unit.       }
{                                                                          }
{ Assumptions:                                                             }
{ All routines that manipulate zero terminated strings take a maxLen       }
{ parameter.  The actual string should be allocated with at least one more }
{ byte than specified by maxLen. This is because some windows routines     }
{ treat the maxLen parameter differently. Some include the terminating     }
{ null and some do not. So be safe with maxlen.                            }
{                                                                          }
{ An example:                                                              }
{                                                                          }
{ If you have a PChar declared as  someVar : array[0..100] of char; use    }
{ 100 as the maxLen parameter. If you allocate your string on the heap     }
{ ie, GetMem(someVar, 100) use 100-1 as the maxlen parameter.              }
{--------------------------------------------------------------------------}


INTERFACE

uses
  Strings;

function HVal(hstr: PChar; var code: integer): longint;
{- similar to Val procedure but takes a hex string rather than a decimal
   string }

function HexStr(var num; numSize: byte; hStr: PChar; maxLen: integer): PChar;
{- return a hexidecimal string representation of num }

function InsCommas(dest, src: PChar; maxLen: byte; commachar, decPt: char): PChar;
{- insert commas into a string, return result in destNumStr }

function charStr(aChar: char; pstr: PChar; ALen: Word): PChar;
{- fills a string with a specified character }

{- padding functions }
function Pad(dest, src: PChar; padWith: char; padLen: Word): PChar;
function LeftPad(dest, src: PChar; padWith: char; padLen: Word): PChar;

{- trimming functions }
function TrimTrail(dest, src: PChar; trimCh: char; maxLen: integer): PChar;
function TrimLead(dest, src: PChar; trimCh: char; maxLen: integer): PChar;

function TrimAll(dest, src: PChar; trimCh: char; maxLen: integer): PChar;

function StripRange(dest, src: PChar; maxLen: integer; low, high: Char): PChar;

{- Stripping function }
function StrStrip(dest, src : PChar; delch : char; maxLen : integer) : PChar;

IMPLEMENTATION

uses
  DMSDebug;

{$L hex2num}
{- HVal is similar to the standard Val procedure in Turbo, with the exception
   that the string passed in is a hexidecimal string. The string can contain
   0123456789 and abcdef or ABCDEF. Case does not matter }
function HVal(hstr: PChar; Var code: integer): longint;
external;

{$L hexstr}
{- HexStr converts a number to a hex string. The number must be an integer
   type (word, byte, shortInt, integer, longint). Negative values are treated as
   their unsigned equivelents.

   inParms:  num      The actual number to convert.
             numSize  Pass in sizeof(num) for this parameter. This is the
                      size in bytes of the num passed in.
             maxLen   The maximum length of the string to return.
   outParm:  hStr     The returned hex string.
   returns:  the function returns a pointer to hStr }
function HexStr(var num; numSize: byte; hStr: PChar; maxLen: integer): PChar;
external;


{$L commas}
{- InsCommas inserts commas into a numeric string.
   Assumptions: numStr is assumed to be a numeric string. The commas are
                inserted every 3 characters to the left of the decimal point
                if any.

   inParms  : numStr - A numeric string to add commas to
              maxLen - maximum length for destNumStr
              commaChar - the character to insert as the comma
              decPt - the character used as a decimal point in numstr (if any)
   outParms : destNumStr - the destination for the result
   returns  : the function returns a pointer to destNumStr }
function InsCommas(dest, src: PChar; maxLen: byte; commachar, decPt: char): PChar;
external;

{$L charstr}
{ CharStr returns a string of the specified length containing the
  specified character }
function CharStr(aChar: char; pstr: PChar; aLen: word): PChar;
external;

{$L padder}
{- The Pad functions pad a string to the right (PadCh) or to the left.
   (LeftPadCh)
   inParms : src      - string to pad
             padWith  - character to pad with
             padLen   - pad src to this length
   outParms: dest     - the destination of the pad
   returns : The function returns a pointer to dest. }
function Pad(dest, src: PChar; padWith: char; padLen: word): PChar;
external;

function LeftPad(dest, src: PChar; padWith: char; padLen: word): PChar;
external;


{$L trimmer}
{- The Trim functions trim a specified character from a string.
   Trim trims both leading and trailing characters. TrimTrail and TrimLead
   are obvious.
   inParm : src  - string to trim
            trimCh - the character to trim off
            maxLen - the maximum length of the destination string
   outParm: dest - resultant trimmed string
   returns: The function returns a pointer to the trimmed string.}
function TrimAll(dest, src: PChar; trimCh: char; maxLen: integer): PChar;
begin
  TrimLead(dest, src, trimCh, maxLen);
  TrimTrail(dest, dest, trimCh, maxLen);
  TrimAll:= dest;
end;

function TrimTrail(dest, src: PChar; trimCh: char; maxLen: integer): PChar;
external;

function TrimLead(dest, src: PChar; trimCh: char; maxLen: integer): PChar;
external;

{$L STRIPRNG}
function StripRange(dest, src: PChar; maxLen: integer; low, high: Char): PChar;
external;

function strstrip(dest, src : pchar; delch : char; maxlen : integer) : pchar;
var
  tempPChar : PChar;
  retPChar  : PChar;

begin
  retPChar := nil;
  tempPChar := StrScan(src, delch);
  if tempPChar <> nil then
  begin
    if (tempPChar-src) < maxLen then
      StrLCopy(dest,src, (tempPChar-src))
    else
      StrLCopy(dest,src, maxLen);

    StrLCopy(src, tempPChar+1, StrLen(tempPChar)-1);
    retPChar := tempPChar + 1;
  end
  else if (StrLen(src) > 0) then
  begin
    if StrLen(src) < maxLen then
      StrCopy(dest,src)
    else
      StrLCopy(dest,src, maxLen);
    StrCopy(src,'');
  end
  else
    StrCopy(dest,'');

  StrStrip := retPChar;
end;

END.
