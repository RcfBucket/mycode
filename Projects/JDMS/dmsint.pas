Program DMSInt;

Uses
  ApiTools,
  IntlLib,
  Ctl3d,
  DlgLib,
  DMSErr,
  Win31,
  WinTypes,
  WinProcs,
  Objects,
  Strings,
  MScan,
  MTask,
  UserMsgs,
  IniLib,
  IntGlob,
  IntPrint,
  IntMap,
  IntXRef,
  IntSend,
  IntRecv,
  Log,
  IntASTM,
  LogEvent,
  IntAuto,
  IntQuick,
  WinDos,
  DongChk,
  IntCstm,
  ODialogs,
  OWindows;

{$R DMSINT.RES}
{$R INTLOGO.RES}
{$I DMSLOGO.INC}

Const
  cm_RecvData  = 101;
  cm_SendData  = 102;
  cm_Custom    = 103;
  cm_LogFile   = 104;
  cm_PrnCstm   = 105;
  cm_PrnAllXRef= 106;
  cm_PrnXRef   = 107;
	cm_MapFields = 108;
	cm_PrnMapFlds= 109;
	cm_PrnAllCstm= 110;
	cm_ExitMain  = 111;
	cm_QuickSelect = 112;
	cm_RangeSelect = 113;
	cm_ReTransmit = 114;
	cm_AutoUpload = 115;
  cm_DMS        = 116;
  cm_About     = 117;
  id_VerText   = 101;

  cm_ShowReceive = 140;

Type
  TIntApp = object(T3DApplication)
		procedure InitMainWindow;virtual;
  end;

	PIntMainWnd = ^TIntMainWnd;
	TIntMainWnd = object(TWindow)
    NoDongle: Boolean;
  	CurrTransmit: word;
		TransTime : array[0..40] of char;
    dmsRunning: boolean;
		constructor Init(PWndObj: PWindowsObject; WndTitle: PChar);
    procedure ShowCstmDlg(var Msg: TMessage);virtual cm_First + cm_Custom;
    procedure GetWindowClass(var WndClass: TWndClass);virtual;
		procedure SetupWindow;virtual;
    procedure Paint(PaintDC: HDC; var PaintInfo: TPaintStruct); virtual;
    {Cross Reference tables are called through WMCommand}
		procedure WMCommand(var Msg: TMessage);virtual wm_First + wm_Command;
    procedure WMInquire(var Msg: TMessage);virtual wm_First + wm_Inquire;
    procedure SendData(var Msg: TMessage);virtual cm_First + cm_SendData;
    procedure RecvData(var Msg: TMessage);virtual cm_First + cm_RecvData;
    procedure PrintCstmParms(var Msg: TMessage);virtual cm_First + cm_PrnCstm;
    procedure PrintXRefTables(var Msg: TMessage);virtual cm_First + cm_PrnXRef;
    procedure PrintAllXRefs(var Msg: TMessage);virtual cm_First + cm_PrnAllXRef;
    procedure PrintMapFields(var Msg: TMessage);virtual cm_First + cm_PrnMapFlds;
    procedure PrintAllCustom(var Msg: TMessage);virtual cm_First + cm_PrnAllCstm;
		procedure ShowMapDlg(var Msg: TMessage);virtual cm_First + cm_MapFields;
  	procedure ShowLogFile(var Msg: TMessage);virtual cm_First + cm_LogFile;
		procedure CommTimer(var Msg:TMessage);virtual wm_First + wm_Timer;
		function  CanClose: Boolean; virtual;
		procedure ExitMain(var Msg:TMessage);virtual cm_First + cm_ExitMain;
		procedure QuickSelect(var Msg: TMessage);virtual cm_First + cm_QuickSelect;
    procedure ReTransmit(var Msg: TMessage);virtual cm_First + cm_ReTransmit;
		procedure RangeSelect(var Msg: TMessage);virtual cm_First + cm_RangeSelect;
		procedure AutoUpload(var Msg: TMessage);virtual cm_First + cm_AutoUpload;
    procedure GoToDMS(var Msg: TMessage);virtual cm_First + cm_DMS;
		procedure MsgShowReceive(var Msg: TMessage);virtual cm_First + cm_ShowReceive;
		procedure ShowAbout(var Msg: TMessage);virtual cm_First + cm_About;
    function  GetClassName: PChar; virtual;
    procedure WMDMSQuery(var msg: TMessage); virtual WM_FIRST + WM_MAINPROGQUERY;
    procedure WMWinINI(var msg: TMessage); virtual WM_FIRST + WM_WININICHANGE;
    destructor Done;virtual;
end;

  PAboutDlg = ^TAboutDlg;
  TAboutDlg = object(TCenterDlg)
    procedure SetupWindow;virtual;
  end;


{
***************
AboutDlg Object
***************
}

procedure TAboutDlg.SetupWindow;
var  tstr: array[0..255] of char;
begin
  inherited SetupWindow;
  StrLCopy(tstr, MFIVersion, SizeOf(tstr)-1);
  {$IFNDEF RELEASEVER}
  {- Internal version }
    StrCat(tstr,' Internal rev 112');
  {$ENDIF}
  SendDlgItemMsg(id_VerText, wm_SetText, 0, longint(@tstr));
end;

procedure TIntApp.InitMainWindow;
var
   TempTitle: array[0..30] of char;

begin
  if INIUse3d then
    Register3dApp(True, True, True);
  LoadString(HInstance, str_IntBase+str_WndTitle, TempTitle, SizeOf(TempTitle));
  MainWindow := New(PIntMainWnd, Init(nil, TempTitle));
end;

{Ricks Stuff}

procedure TIntMainWnd.SetupWindow;
var
  TempMessage: array[0..130] of char;
  TempMsgVer: array[0..50] of char;
  TempMsg: array[0..255] of char;

begin
  inherited SetupWindow;
  ShowWindow(hWindow, SW_SHOWMAXIMIZED);

  NoDongle := DongleOK;
  if not NoDongle then
  begin
    LoadString(HInstance, str_IntBase+str_NoDongle, TempMsg, SizeOf(TempMsg));
    LoadString(HInstance, str_IntBase+str_IntMsg, TempMsgVer, SizeOf(TempMsgVer));
    MessageBox(HWindow, TempMsg, TempMsgVer, mb_Ok);
    postquitmessage(0);
  end;

  GlobHWnd := HWindow;
	GlobWndObj := @Self;
  CurrTransmit := 0;
  Restart;

  {notify other tasks that interface is started}
  PostMessage(HWND_BROADCAST, WM_INQUIRE, ProgramStarted, MakeLong(InqMsgTwoWay, InqMsgAll));

  {- broadcast and inquire to see who is running }
  PostMessage(HWND_BROADCAST, WM_INQUIRE, ProgramInquire, MakeLong(InqMsgTwoWay, InqMsgAll));
end;

procedure TIntMainWnd.ShowAbout(var Msg: TMessage);
var
   AboutDlg: PAboutDlg;

begin
  AboutDlg := New(PAboutDlg, Init(@Self, 'INT_ABOUT'));
	Application^.ExecDialog(AboutDlg);
end;

procedure TIntMainWnd.MsgShowReceive(var Msg: TMessage);
begin
	if ShowReceive then
	begin
  	ShowReceive := False;
    CheckMenuItem(Attr.Menu, cm_ShowReceive, mf_ByCommand or mf_UnChecked);
	end
	else
	begin
  	ShowReceive := True;
    CheckMenuItem(Attr.Menu, cm_ShowReceive, mf_ByCommand or mf_Checked);
	end;
end;

procedure TIntMainWnd.GoToDMS(var Msg: TMessage);
var
  tw    : TWaitCursor;
  ret   : word;
begin
  if dmsRunning then
    PostMessage(HWND_BROADCAST, WM_INQUIRE, ProgramFocus, MakeLong(InqMsgTwoWay, InqMsgDMS))
  else
  begin
    tw.Init;
    ret:= WinExec('DMS.EXE', SW_SHOWNORMAL);
    tw.Done;
    if ret < 32 then
    begin
      ShowError(@Self, str_EXECERR, nil, ret, MOD_TWOWAY, 0);
    end;
  end;
end;

procedure TIntMainWnd.ExitMain(var Msg: TMessage);
begin
	CloseWindow;
end;

Function TIntMainWnd.CanClose: Boolean;
Begin

  canclose := false;

  if inwaitforack then  { if transmitting, don't exit }
	  exit;

  LogFile^.LogEvent(0, SendBit, lgevClose, nil, -1);
  MSDisposeObj(LogFile);

  commcleanup;
  CanClose:= True;

End;

Procedure TIntMainWnd.CommTimer(var Msg:TMessage);
{--------------------------------------------------}
{  Purpose    :                                    }
{  Responds to:                                    }
{  wParam     :                                    }
{  lParam     :                                    }
{  returns    :                                    }
{--------------------------------------------------}
Var
  Error : integer;
  loop : integer;
  xlen : integer;
  EventFlag : Integer;
  AStat      : TComStat;
  NumStr     : String;
  TmpCh      : Array[0..255] of char;
  hhour, hmin, hsec, htsec: word;
Begin
  { if here then timer has clicked }

  if autotimer then
  begin

    if deferupload and not (rcvmode or xmtmode or ackwait or inescapedelay) then
		begin
			deferupload := false;
			IntAuto_ReadAutoRanges;
      IntSend_SendRequestedData(HWindow, @Self);
		end;

		if CurrTransmit <> nexttransmit then
		begin
    	CurrTransmit := nexttransmit;
      formattime(CurrTransmit div 60, CurrTransmit mod 60, 0, 0, TransTime);
      InvalidateRect(HWindow, nil, True);
		end;


		inc(automscnt);
		if automscnt = round(tickdelay) then	{ one second passed }
		begin
		   automscnt := 0;
			inc(autoscnt);	{ increment second count }
			if autoscnt >= 60 then
			begin
          gettime(hhour, hmin, hsec, htsec);
              autoscnt := hsec;	{ align with clock }
						if (hhour*60+hmin) = nexttransmit then
				  begin
						nexttime;	{ calculate next time}
						deferupload := true; { start upload on next legal tick count }
				  end;
			end;
      end;
  end;

  inc(tickcount);

  if ignoreticks then exit;	{ prevent unintended recursion while still counting ticks }

  if InEscapeDelay Then Exit;

  ignoreticks := true;

  eventflag := 0;
  if not routetodisk then
      EventFlag := GetCommEventMask(commCID, ev_RxChar+ev_Err+ev_break+ev_Txempty); { Received char?}

  if EventFlag <> 0 then
  begin

   Error := GetCommError(CommCID, AStat);

	 if (Error <> 0) then
	 begin
			if error = $8000 then
				error := LISMode;

	 		case error of { convert getcommerror to local error codes }
				CE_Break: error := LISBreak;
				CE_Frame: error := LISFRame;
				CE_Overrun: error := LISOverrun;
				CE_Rxover: error := LISOverflow;
				CE_RXPARITY: error := LISParity;
				CE_TXFULL : error := LISXMTOverflow;
				else error := LISUnknown;
         end;

       ReportLineError(Error);
	 end;

	 if (EventFlag and ev_txempty) <> 0 then
    	txempty := true;

    if (EventFlag and ev_RxChar) <> 0 then  { if receiving Char(s) }
    Begin
         TickCount := 0;

		   if (not (useASTM or useMicroScan)) and not ackwait then  { xon/xoff }
				rcvmode := true;

			if not (rcvmode or xmtmode) then { enq? }
			begin
             if (ReadChar(TmpCh, 1) <> 0) then
             begin
                  if TmpCh[0] = chr(enq) then
                  begin
                      if ClientReady then
                      begin
                         RcvMode := true;
                         SendACK(TRUE);
                      end
                      else
                         SendACK(FALSE);
                  end;
             end;
         end
         else
         if AckWait then   { if waiting for an ack/nak }
         begin
             if (ReadChar(TmpCh, 1) <> 0) then
             begin
                  AckWait := false;
                  if TmpCh[0] = chr(ACK) then
                  begin
							 NakCount := 0;
                      AckReceived := true;
                      if EnqWait then
                      begin
                          EnqWait := false;
                          RcvMode := true;
                      end;
                  end
                  else
                  begin
                       inc(NakCount);
							  ackwait := false;
						end; { if tmpch <> ack }
				 end;      { if read char }
         end           { if ack wait }
         else          { receive and process character(s) }
         begin
              xlen := ReadChar(TmpCh, AStat.cbInQue);
				  for loop := 0 to xlen-1 do
                  AddChar(TmpCh[loop]);

         end;
    end; { if rcv char }
  end { if event flag }
  else
  if ackwait then { look for timeout conditions }
  begin
         if enqwait then
         begin
            if tickcount > timeoutsec then
				begin
					 timedout := true;
                ackwait := false;
                inc(nakcount);
				end
         end
         else  { record ack }
         begin
				  if tickcount > timeoutsec then
				  begin
              		ackwait := false;
						timedout := true;
              end;
         end;
  end { if ackwait }
  else
  if rcvmode then   { records stopped without termination of session }
	 if routetodisk then
	 begin
    ignoreticks := true;
    while ReadChar(TmpCh, 0) > 0 do
			addchar(tmpch[0]);
      rcvmode := false;
		rcvrec('');
		ignoreticks := false;
	 end
    else
		if tickcount > frametimeout then
      begin
			 reportfail(LISStopped);
          rcvmode := false;
		end;

  ignoreticks := false;

End;


{End Ricks Stuff}
constructor TIntMainWnd.Init(PWndObj: PWindowsObject; WndTitle: PChar);
var
  XRefMenu: HMenu;
  MenuStr: array[0..MenuLen] of char;
	i: integer;
	tstr: array[0..1] of char;

  Function ConvertINI : Boolean;               {Bring forward last INI file selected profiles}
  Const
    V3XXINI      = 'IntCV3XX.INI';             {Restored v3.XX INI file}
    CurrentINI   = 'IntCstm.INI';              {Installed generic INI}
    IntGeneral   = 'IntCstm';                  {General customization}
    IntModem     = 'ModemStrings';             {Modem customization, currently modem is not functional}
    IntXRef      = 'IntXRef';                  {Cross reference customization}
    IntLog       = 'IntLog';                   {Logfile customization}
    IntAuto      = 'Auto';                     {Auto transmit customization}
    IntQuick     = 'Quick';                    {Quick transmit customization}
    IntPanel     = 'PanelXRef';                {Internal panel numbers, this is NOT converted}
    NotDefined   = 'NotDefined';               {Indicates the profile is not defined}
    ConvProfile  = 'CustomConverted';          {Indicates INI has been converted}
    WasConverted = 'True';                     {Indicates INI has been converted}
  Var
    StatusConv     : Boolean;                  {Indicates status of last profile convert}
    ReOpenBuff     : TOfStruct;                {Open file structure}

    Function ConvertMiscProfile                {Bring a profile forward from last INI version}
     (Section     : PChar;                     {INI file section}
      MiscProfile : PChar;                     {Profile to convert}
      FromINI     : PChar;                     {Last INI file}
      ToINI       : PChar) : Boolean;          {New INI file}
    Var
      ProfileSetting : Array [0..100] of Char; {The old value of MiscProfile}
    Begin
      ConvertMiscProfile := False;
      {Get the profile if it is defined}
      GetPrivateProfileString(Section, MiscProfile, NotDefined, ProfileSetting, SizeOf(ProfileSetting), FromINI);
      {If it was found, bring it forward}
      If (StrComp(NotDefined,ProfileSetting)<>0) then
      Begin
        WritePrivateProfileString(Section, MiscProfile, ProfileSetting, ToINI);
        ConvertMiscProfile := True;
      End;
    End;

  Begin
    ConvertINI := False;

    {If there is an INI to convert...}
    If (OpenFile(V3XXINI, ReOpenBuff, OF_Exist) > 0) then
    Begin
      ConvertINI := True;
      StatusConv := ConvertMiscProfile(IntGeneral,'IntPort'             ,V3XXINI,CurrentINI);
      StatusConv := ConvertMiscProfile(IntGeneral,'IntBaud'             ,V3XXINI,CurrentINI);
      StatusConv := ConvertMiscProfile(IntGeneral,'IntParity'           ,V3XXINI,CurrentINI);
      StatusConv := ConvertMiscProfile(IntGeneral,'IntStop'             ,V3XXINI,CurrentINI);
      StatusConv := ConvertMiscProfile(IntGeneral,'IntData'             ,V3XXINI,CurrentINI);
      StatusConv := ConvertMiscProfile(IntGeneral,'IntAstm'             ,V3XXINI,CurrentINI);
      StatusConv := ConvertMiscProfile(IntGeneral,'IntMicroScan'        ,V3XXINI,CurrentINI);
      StatusConv := ConvertMiscProfile(IntGeneral,'IntXON'              ,V3XXINI,CurrentINI);
      StatusConv := ConvertMiscProfile(IntGeneral,'LogLevel'            ,V3XXINI,CurrentINI);
      StatusConv := ConvertMiscProfile(IntGeneral,'Conv'                ,V3XXINI,CurrentINI);
      StatusConv := ConvertMiscProfile(IntGeneral,'ChkSum'              ,V3XXINI,CurrentINI);
      StatusConv := ConvertMiscProfile(IntGeneral,'DBCS'                ,V3XXINI,CurrentINI);
      StatusConv := ConvertMiscProfile(IntGeneral,'LogDbgFile'          ,V3XXINI,CurrentINI);
      StatusConv := ConvertMiscProfile(IntGeneral,'NakCount'            ,V3XXINI,CurrentINI);
      StatusConv := ConvertMiscProfile(IntGeneral,'FrameTimeout'        ,V3XXINI,CurrentINI);
      StatusConv := ConvertMiscProfile(IntGeneral,'Timeout'             ,V3XXINI,CurrentINI);
      StatusConv := ConvertMiscProfile(IntGeneral,'IntInFile'           ,V3XXINI,CurrentINI);
      StatusConv := ConvertMiscProfile(IntGeneral,'IntOutFile'          ,V3XXINI,CurrentINI);
      StatusConv := ConvertMiscProfile(IntGeneral,'EoRec'               ,V3XXINI,CurrentINI);
      StatusConv := ConvertMiscProfile(IntGeneral,'EoFrame'             ,V3XXINI,CurrentINI);
      StatusConv := ConvertMiscProfile(IntGeneral,'&Physician Code'     ,V3XXINI,CurrentINI);
      StatusConv := ConvertMiscProfile(IntGeneral,'&Ward Code'          ,V3XXINI,CurrentINI);
      StatusConv := ConvertMiscProfile(IntGeneral,'&Source Code'        ,V3XXINI,CurrentINI);
      StatusConv := ConvertMiscProfile(IntGeneral,'&Admission Code'     ,V3XXINI,CurrentINI);
      StatusConv := ConvertMiscProfile(IntGeneral,'&Hospital Code'      ,V3XXINI,CurrentINI);
      StatusConv := ConvertMiscProfile(IntGeneral,'&Institution Code'   ,V3XXINI,CurrentINI);
      StatusConv := ConvertMiscProfile(IntGeneral,'&Comment Code'       ,V3XXINI,CurrentINI);
      StatusConv := ConvertMiscProfile(IntGeneral,'Patient &Gender Code',V3XXINI,CurrentINI);
      StatusConv := ConvertMiscProfile(IntGeneral,'&Dosage Category'    ,V3XXINI,CurrentINI);
      StatusConv := ConvertMiscProfile(IntGeneral,'&Order ID'           ,V3XXINI,CurrentINI);
      StatusConv := ConvertMiscProfile(IntGeneral,'&Test Type'          ,V3XXINI,CurrentINI);
      StatusConv := ConvertMiscProfile(IntGeneral,'&Drug Result Type'   ,V3XXINI,CurrentINI);
      StatusConv := ConvertMiscProfile(IntGeneral,'Data &Value'         ,V3XXINI,CurrentINI);
      StatusConv := ConvertMiscProfile(IntGeneral,'&User Defined Fields',V3XXINI,CurrentINI);
      StatusConv := ConvertMiscProfile(IntGeneral,'UseVer17'            ,V3XXINI,CurrentINI);
      StatusConv := ConvertMiscProfile(IntGeneral,'LogFile'             ,V3XXINI,CurrentINI);
      StatusConv := ConvertMiscProfile(IntGeneral,'ModemInstalled'      ,V3XXINI,CurrentINI);
      StatusConv := ConvertMiscProfile(IntGeneral,'InterfaceFile'       ,V3XXINI,CurrentINI);
      StatusConv := ConvertMiscProfile(IntGeneral,'WAInstalled'         ,V3XXINI,CurrentINI);
      StatusConv := ConvertMiscProfile(IntGeneral,'UseWA'               ,V3XXINI,CurrentINI);
      StatusConv := ConvertMiscProfile(IntGeneral,'ShowReceive'         ,V3XXINI,CurrentINI);
      StatusConv := ConvertMiscProfile(IntGeneral,'LogActive'           ,V3XXINI,CurrentINI);

      StatusConv := ConvertMiscProfile(IntModem  ,'InitStr'             ,V3XXINI,CurrentINI);
      StatusConv := ConvertMiscProfile(IntModem  ,'DialStr'             ,V3XXINI,CurrentINI);
      StatusConv := ConvertMiscProfile(IntModem  ,'HangupStr'           ,V3XXINI,CurrentINI);
      StatusConv := ConvertMiscProfile(IntModem  ,'BreakStr'            ,V3XXINI,CurrentINI);

      StatusConv := ConvertMiscProfile(IntXRef   ,'Physician Code'      ,V3XXINI,CurrentINI);
      StatusConv := ConvertMiscProfile(IntXRef   ,'Ward Code'           ,V3XXINI,CurrentINI);
      StatusConv := ConvertMiscProfile(IntXRef   ,'Source Code'         ,V3XXINI,CurrentINI);
      StatusConv := ConvertMiscProfile(IntXRef   ,'Admission Code'      ,V3XXINI,CurrentINI);
      StatusConv := ConvertMiscProfile(IntXRef   ,'Hospital Code'       ,V3XXINI,CurrentINI);
      StatusConv := ConvertMiscProfile(IntXRef   ,'Institution Code'    ,V3XXINI,CurrentINI);
      StatusConv := ConvertMiscProfile(IntXRef   ,'Comment Code'        ,V3XXINI,CurrentINI);
      StatusConv := ConvertMiscProfile(IntXRef   ,'Patient Sex Code'    ,V3XXINI,CurrentINI);
      StatusConv := ConvertMiscProfile(IntXRef   ,'Dosage Category'     ,V3XXINI,CurrentINI);
      StatusConv := ConvertMiscProfile(IntXRef   ,'Order ID'            ,V3XXINI,CurrentINI);
      StatusConv := ConvertMiscProfile(IntXRef   ,'Test Type'           ,V3XXINI,CurrentINI);
      StatusConv := ConvertMiscProfile(IntXRef   ,'Drug Result Type'    ,V3XXINI,CurrentINI);
      StatusConv := ConvertMiscProfile(IntXRef   ,'Data Value'          ,V3XXINI,CurrentINI);
      StatusConv := ConvertMiscProfile(IntXRef   ,'User Defined Fields' ,V3XXINI,CurrentINI);
      StatusConv := ConvertMiscProfile(IntXRef   ,'ToMScan'             ,V3XXINI,CurrentINI);

      StatusConv := ConvertMiscProfile(IntLog    ,'BegDate'             ,V3XXINI,CurrentINI);
      StatusConv := ConvertMiscProfile(IntLog    ,'EndDate'             ,V3XXINI,CurrentINI);
      StatusConv := ConvertMiscProfile(IntLog    ,'MaxEntries'          ,V3XXINI,CurrentINI);
      StatusConv := ConvertMiscProfile(IntLog    ,'Entries'             ,V3XXINI,CurrentINI);

      StatusConv := ConvertMiscProfile(IntAuto   ,'StartRange'          ,V3XXINI,CurrentINI);
      StatusConv := ConvertMiscProfile(IntAuto   ,'EndRange'            ,V3XXINI,CurrentINI);
      StatusConv := ConvertMiscProfile(IntAuto   ,'RequestType'         ,V3XXINI,CurrentINI);
      StatusConv := ConvertMiscProfile(IntAuto   ,'TransmitFlag'        ,V3XXINI,CurrentINI);
      StatusConv := ConvertMiscProfile(IntAuto   ,'TransmitTime'        ,V3XXINI,CurrentINI);
      StatusConv := ConvertMiscProfile(IntAuto   ,'Frequency'           ,V3XXINI,CurrentINI);
      StatusConv := ConvertMiscProfile(IntAuto   ,'Enabled'             ,V3XXINI,CurrentINI);

      StatusConv := ConvertMiscProfile(IntQuick  ,'StartRange'          ,V3XXINI,CurrentINI);
      StatusConv := ConvertMiscProfile(IntQuick  ,'EndRange'            ,V3XXINI,CurrentINI);
      StatusConv := ConvertMiscProfile(IntQuick  ,'RequestType'         ,V3XXINI,CurrentINI);
      StatusConv := ConvertMiscProfile(IntQuick  ,'TransmitFlag'        ,V3XXINI,CurrentINI);
      If (OpenFile(V3XXINI, ReOpenBuff, OF_Delete) > 0) then;
    End;
  End;

begin
  inherited Init(PWndObj, WndTitle);
  dmsRunning:= False;
  with Attr do
  begin
    Menu := LoadMenu(HInstance, 'INT_MAIN_MENU');
    Style:= ws_OverlappedWindow + ws_Visible;
    X:= CW_USEDEFAULT;
    Y:= CW_USEDEFAULT;
    w:= GetSystemMetrics(SM_CXFULLSCREEN);
    h:= GetSystemMetrics(SM_CYFULLSCREEN);
  end;

  If ConvertINI then;

	IntCstm_InitCustomCfgBuff; {get enabled tables and enable menus}
	for i := 0 to 13 do
		if CustomCfgBuff.XRefChksCfg[i] = bf_UnChecked then
      EnableMenuItem(Attr.Menu, i+120, mf_ByCommand or mf_Grayed)
		else
      EnableMenuItem(Attr.Menu, i+120, mf_ByCommand or mf_Enabled);
	FirstReceive := True;
  GetPrivateProfileString('Auto', 'Enabled', 'N', tstr, SizeOf(tstr), 'IntCstm.ini');
  if StrComp(tstr, 'Y') = 0 then
		AutoUploadOn := True
	else
		AutoUploadOn := False;

  GetPrivateProfileString('IntCstm', 'LogActive', 'Y', tstr, SizeOf(tstr), 'IntCstm.ini');
  if tstr[0] = 'Y' then
    UseLog := True
  else
    UseLog := False;
  LogFile := New(PLogFile, Init(UseLog));
  LogFile^.LogEvent(0, SendBit, lgevStart, nil, -1);

  GetPrivateProfileString('IntCstm', 'ShowReceive', 'Y', tstr, SizeOf(tstr), 'IntCstm.ini');
	if tstr[0] = 'Y' then
  begin
		ShowReceive := True;
    CheckMenuItem(Attr.Menu, cm_ShowReceive, mf_ByCommand or mf_Checked);
	end
	else
  begin
		ShowReceive := False;
    CheckMenuItem(Attr.Menu, cm_ShowReceive, mf_ByCommand or mf_UnChecked);
  end;
end;

destructor TIntMainWnd.Done;
begin
	{notify DMS that interface is stoped}
  PostMessage(HWND_BROADCAST, WM_INQUIRE, ProgramStopped, MakeLong(InqMsgTwoWay, InqMsgAll));
  inherited Done;
end;

procedure TIntMainWnd.Paint(PaintDC: HDC; var PaintInfo: TPaintStruct);
var
  Logo    : HBitMap;
  SaveDC  : HDC;
  BitInfo : TBitMap;
  r       : TRect;
  w       : integer;
  h       : integer;
  TimeText: array[0..30] of char;
  tstr: array[0..5] of char;
	ClientRect: TRect;
  Y: integer;
	Metrics: TTextMetric;
  TempTime: array[0..30] of char;

begin
  inherited Paint(PaintDC, PaintInfo);

  {$IFDEF RELEASEVER}  {- releasable (real) version }
  Logo:= LoadBitmap(HInstance, MAKEINTRESOURCE(BMP_DADE));
  {$ELSE}  {- for investigational use only }
  Logo:= LoadBitmap(HInstance, MAKEINTRESOURCE(BMP_INV_USE_ONLY));
  {$ENDIF}
  GetWindowRect(hWindow, r);
  w:= r.right - r.left;
  h:= r.bottom - r.top;

  SaveDC:= CreateCompatibleDC(PaintDC);
  SelectObject(SaveDC, Logo);
  GetObject(logo, SizeOf(TBitMap), @bitInfo);
  BitBlt(PaintDC, (w - bitInfo.bmWidth) div 2,(h - bitInfo.bmHeight) div 2,
	BitInfo.bmWidth, bitInfo.bmHeight, SaveDC, 0, 0, SrcCopy);
  DeleteDC(SaveDC);
  DeleteObject(logo);

  if AutoUploadOn then
	begin
    GetClientRect(HWindow, ClientRect);
    GetTextMetrics(PaintDC, Metrics);
		Y := ClientRect.bottom - Metrics.tmHeight;
    LoadString(HInstance, str_IntBase+str_TransTime, TempTime, SizeOf(TempTime));
    StrCopy(TimeText, TempTime);
		tstr[0] := TransTime[0];
    tstr[1] := TransTime[1];
		tstr[2] := TransTime[2];
		tstr[3] := TransTime[3];
		tstr[4] := TransTime[4];
		tstr[5] := #0;
    StrCat(TimeText, tstr);
    TextOut(PaintDC, 0, Y, TimeText, StrLen(TimeText));
  end;
end;

procedure TIntMainWnd.GetWindowClass(var WndClass: TWndClass);
begin
  inherited GetWindowClass(WndClass);
  WndClass.HICon := LoadIcon(HInstance, 'INT_PGRM_ICON');
end;

procedure TIntMainWnd.WMInquire(var Msg: TMessage);
begin
  if (Msg.lParamLo <> InqMsgTwoWay) and ((Msg.lParamHi = InqMsgTwoWay) or (Msg.lParamHi = InqMsgAll)) then
	begin
    case msg.wParam of
      ProgramFocus :  {- someone asked to focus the DMS }
        begin
          ShowOwnedPopups(HWindow, True);
          SetFocus(GetLastActivePopup(HWindow));
        end;
      ProgramInquire :
        begin
          {- lParamLo is the sender of this inquiry }
          PostMessage(HWND_BROADCAST, WM_INQUIRE, ProgramStarted, MakeLong(InqMsgTwoWay, msg.lParamLo));
        end;
      ProgramStopped :
        if msg.lParamLo = InqMsgDMS then
        begin
          dmsRunning:= false;
        end;
      ProgramStarted :
        if msg.lParamLo = InqMsgDMS then
        begin
          dmsRunning:= true;
        end;
    end; { case }
  end;
end;

procedure TIntMainWnd.WMCommand(var Msg: TMessage);
begin
	if Msg.lParamLo = 0 then
    if (Msg.wParam >= 120) and (Msg.wParam <= 133) then
    begin
			ClientReady := False;
      IntXRef_ExecXRefEntry(HWindow, @Self, Msg.wParam);
			ClientReady := True;
		end
    else
      inherited WMCommand(Msg)
  else
    inherited WMCommand(Msg);
end;

procedure TIntMainWnd.ShowCstmDlg(var Msg: TMessage);
begin
	ClientReady := False;
  IntCstm_ExecIntCustomDlg(HWindow, @Self);
	ClientReady := True;
end;

procedure TIntMainWnd.PrintCstmParms(var Msg: TMessage);
begin
	ClientReady := False;
  IntPrint_PrintParms(HWindow, @Self, true);
	ClientReady := True;
end;

procedure TIntMainWnd.PrintXRefTables(var Msg: TMessage);
begin
	ClientReady := False;
  IntPrint_PrintXRef(HWindow, @Self, False, true);
	ClientReady := True;
end;

procedure TIntMainWnd.PrintAllXRefs(var Msg: TMessage);
begin
	ClientReady := False;
  IntPrint_PrintXRef(HWindow, @Self, True, true);
	ClientReady := True;
end;

procedure TIntMainWnd.PrintMapFields(var Msg: TMessage);
begin
	ClientReady := False;
  IntPrint_PrintMap(HWindow, @Self, true);
	ClientReady := True;
end;

procedure TIntMainWnd.PrintAllCustom(var Msg: TMessage);
begin
	ClientReady := False;
  IntPrint_PrintAllCstm(HWindow, @Self);
	ClientReady := True;
end;

procedure TIntMainWnd.SendData(var Msg: TMessage);
begin
	ClientReady := True;
  IntSend_SendUserData(HWindow, @Self);
	ClientReady := True;
end;

procedure TIntMainWnd.RecvData(var Msg: TMessage);
begin
	ClientReady := True;
  IntRecv_RecvData(HWindow, @Self, True);
	ClientReady := True;
end;

procedure TIntMainWnd.QuickSelect(var Msg: TMessage);
var
  CurrentDate, TmpDate: array[0..10] of char;

begin
  ClientReady := True;
	IntQuick_ReadQuickRanges;
  Ranges.ReTransmit:= False;
  IntSend_SendRequestedData(HWindow, @Self);
end;

procedure TIntMainWnd.ReTransmit(var Msg: TMessage);
var
  CurrentDate, TmpDate: array[0..10] of char;

begin
  ClientReady := True;
	IntQuick_ReadQuickRanges;
  Ranges.ReTransmit:= True;
  IntSend_SendRequestedData(HWindow, @Self);
end;

procedure TIntMainWnd.RangeSelect(var Msg: TMessage);
var
	GetRangesDlg: PGetRangesDlg;

begin
	ClientReady := True;
  GetRangesDlg:= New(PGetRangesDlg, Init(@Self, 'INT_SEND_REQ'));
  if (Application^.ExecDialog(GetRangesDlg) = 0) then
    IntSend_SendRequestedData(HWindow, @Self);
end;

procedure TIntMainWnd.AutoUpload(var Msg: TMessage);
begin
  IntAuto_AutoUpload(HWindow, @Self);
end;

procedure TIntMainWnd.ShowMapDlg(var Msg: TMessage);
begin
	ClientReady := False;
  IntMap_ShowMapFields(HWindow, @Self);
	ClientReady := True;
end;

procedure TIntMainWnd.ShowLogFile(var Msg: TMessage);
begin
	ClientReady := False;
  LogFile^.ExecLogFileWnd(HWindow, @Self);
	ClientReady := True;
end;

function TIntMainWnd.GetClassName: PChar;
begin
  GetClassName:= application^.Name;
end;

procedure TIntMainWnd.WMDMSQuery(var msg: TMessage);
{- respond to the task module and tell it who I am }
begin
  StrLCopy(PChar(msg.lParam), application^.Name, msg.wParam);
  msg.result:= DMSMainProgRetCode;
end;

procedure TIntMainWnd.WMWinIni(var msg: TMessage);
{ reinitialize date/time format if Windows settings change }
begin
  IntlInitFromWinIni;
end;

var
	IntApp: TIntApp;
  tstr: array[0..1] of char;
  TempMsg: array[0..90] of char;
  TempMsgVer: array[0..30] of char;

Begin
  if OKToRun(MOD_TWOWAY, False) then
  begin
    IntApp.Init(GetModuleName(MOD_TWOWAY));
    IntApp.Run;
    IntApp.Done;
    if ShowReceive then
      StrCopy(tstr, 'Y')
    else
      StrCopy(tstr, 'N');
    WritePrivateProfileString('IntCstm', 'ShowReceive', tstr, 'IntCstm.ini');
  end;
End.
