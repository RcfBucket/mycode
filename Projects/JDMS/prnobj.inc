{***** PRNOBJ resource constants *****

  Module ID = $7100 = 28928

******}

const
  DLG_ABORT         = 28928;
  DLG_PRINT         = 28929;
  DLG_SETUP         = 28930;

  IDS_ON            = 28928;
  IDS_ABORTING      = 28929;
  IDS_PAGEOF        = 28930;
  IDS_PAGENUM       = 28931;
  IDS_ERRORTEMPLATE = 28932;
  IDS_OUTOFMEMORY   = 28933;
  IDS_OUTOFDISK     = 28934;
  IDS_PRNCANCEL     = 28935;
  IDS_PRNMGRABORT   = 28936;
  IDS_GENERROR      = 28937;
  IDS_ERRORCAPTION  = 28938;

  IDC_COMBO         = 200;
  IDC_TITLE         = 202;
  IDC_SETUP         = 203;
  IDC_DEVICE        = 204;
  IDC_PRINTERNAME   = 205;
  IDC_ALL           = 206;
  IDC_PORT          = 207;
  IDC_SELECTION     = 208;
  IDC_CURPAGE       = 209;
  IDC_PAGES         = 210;
  IDC_FROMTEXT      = 211;
  IDC_FROM          = 212;
  IDC_TOTEXT        = 213;
  IDC_TO            = 214;
  IDC_COPIES        = 215;
  IDC_COLLATE       = 216;
