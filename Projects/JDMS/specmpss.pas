unit SpecMPSS;

INTERFACE

(* need to be sure that iso file is using the correct key *)

uses
  OWindows,
  DBFile,
  DBTypes,
  MPSS,
  Objects;

type
  PSpecMPSS = ^TSpecMPSS;
  TSpecMPSS = Object(TObject)
    iMPSSObj    : PMPSS;
    constructor Init(aParent: PWindowsObject; specFile: PDBFile);
    destructor Done; virtual;
    function GetFirstSpecimen: boolean;
    function GetNextSpecimen: boolean;
    function LoadRules(aParent: PWindowsObject; MPSSTitle: PChar): boolean;
    procedure CopyRules;
    function GetCurrentSpec: longint;
    function GetTotalSpecs: longint;
    function ValidSpecimen: boolean; virtual;
    function ValidIsolate(isoSeq: TSeqNum): boolean;
    private
    dbIso,
    dbSpec    : PDBFile;
    sMPSSObj  : PMPSS;
    specInRule: boolean;
    isoInRule : boolean;
    curSpec   : longint;
    {}
    function ValidIsosOnSpec(var isosExist: boolean): boolean;
    function IsoFieldsInRule: boolean;
    function SpecFieldsInRule: boolean;
    function MatchDrivingFile(fDesc: PChar): PDBFile;
    procedure CopyRuleFiles(fIndex: integer);
  end;

IMPLEMENTATION

uses
  MScan,
  WinTypes,
  DBIDs,
  DBLib,
  APITools,
  Strings,
  MPSSObjs,
  WinProcs;

constructor TSpecMPSS.Init(aParent: PWindowsObject; specFile: PDBFile);
{************************************************************************}
{************************************************************************}
begin
  specInRule:= false;
  isoInRule:= false;
  curSpec:= 0;

  dbIso:= nil;
  iMPSSobj:= nil;
  sMPSSobj:= nil;

  inherited Init;

  if specFile = nil then
  begin
    Done;
    Fail;
  end
  else
    dbSpec:= specFile;

  dbIso:= new(PDBFile,Init(DBIsoFile, '', dbOpenNormal));
  if dbIso = nil then
  begin
    Done;
    Fail;
  end;

  iMPSSObj := new(PMPSS,Init(aParent,dbIso,IncludeUndefDates));
  if iMPSSObj = nil then
  begin
    Done;
    Fail;
  end;

  sMPSSObj := new(PMPSS,Init(aParent,dbSpec,IncludeUndefDates));
  if sMPSSObj = nil then
  begin
    Done;
    Fail;
  end;
end;

destructor TSpecMPSS.Done;
{************************************************************************}
{************************************************************************}
begin
  MSDisposeObj(dbIso);
  MSDisposeObj(iMPSSObj);
  MSDisposeObj(sMPSSObj);
  inherited Done;
end;

function TSpecMPSS.MatchDrivingFile(fDesc: PChar): PDBFile;
{************************************************************************}
{************************************************************************}
var
  i     : integer;
  found : boolean;
begin
  i := 0;
  found := false;
  MatchDrivingFile := nil;
  while (i <= 9) and (not found) do  {9 is max files in MPSSObj.pas}
  begin
    Inc(i);
    found := StrComp(sMPSSObj^.mFiles^[i].FileDesc, fDesc) = 0;
  end;
  if found then
    MatchDrivingFile := sMPSSObj^.mFiles^[i].FH;
end;

procedure TSpecMPSS.CopyRuleFiles(fIndex: integer);
{************************************************************************}
{************************************************************************}
var
  fh : PDBFile;
begin
  StrLCopy(sMPSSObj^.mRules^[fIndex].FileDesc, iMPSSObj^.mRules^[fIndex].FileDesc,
           SizeOf(sMPSSObj^.mRules^[fIndex].FileDesc)-1);
  fh := MatchDrivingFile(sMPSSObj^.mRules^[fIndex].FileDesc);
  if fh = nil then
    sMPSSObj^.mRules^[fIndex].FH := iMPSSObj^.mRules^[fIndex].FH
  else
    sMPSSObj^.mRules^[fIndex].FH := fh;
end;

procedure TSpecMPSS.CopyRules;
{************************************************************************}
{ Copy the rules from the isolate MPSS object to the rules in the        }
{  specimen MPSS object.                                                 }
{************************************************************************}
var
  i : integer;
begin
  Move(iMPSSObj^.mRules^, sMPSSObj^.mRules^, SizeOf(TRules));
  sMPSSObj^.mRuleLoaded := iMpssObj^.mRuleLoaded;
  i:= 0;
  while (i < MaxMPSSRules) and (iMPSSObj^.mRules^[i].FH <> nil) do
  begin
    CopyRuleFiles(i);
    Inc(i);
  end;
  isoInRule  := IsoFieldsInRule;
  specInRule := SpecFieldsInRule;
end;

function TSpecMPSS.LoadRules(aParent: PWIndowsObject; MPSSTitle: PChar): boolean;
{************************************************************************}
{************************************************************************}
begin
  if iMPSSObj^.MPSSLoadRules(aParent, MPSSTitle) then
  begin
    CopyRules;
    LoadRules:= true;
  end
  else
    LoadRules:= false;
end;

function TSpecMPSS.IsoFieldsInRule: boolean;
{************************************************************************}
{************************************************************************}
var
  i   : integer;
  fn  : array [0..8] of char;
begin
  IsoFieldsInRule := false;
  for i:= 0 to MaxMPSSRules - 1 do
  begin
    if sMPSSObj^.mRules^[i].FH = nil then
      Break;

    sMPSSObj^.mRules^[i].FH^.dbd^.FileName(fn, 8);
    if (StrIComp(fn, DBISOFile) = 0) or
       (StrIComp(fn, DBRESFile) = 0) then
    begin
      IsoFieldsInRule:= true;
      Break;
    end;
  end;
end;

function TSpecMPSS.SpecFieldsInRule: boolean;
{************************************************************************}
{************************************************************************}
var
  i   : integer;
  fn  : array [0..8] of char;
begin
  SpecFieldsInRule := false;
  for i:= 0 to (MaxMPSSRules - 1) do
  begin
    if sMPSSObj^.mRules^[i].FH = nil then
      Break;

    sMPSSObj^.mRules^[i].FH^.dbd^.FileName(fn, 8);
    if (StrIComp(fn, DBPATFile) = 0) or
       (StrIComp(fn, DBSPECFile) = 0) then
    begin
      SpecFieldsInRule := true;
      Break;
    end;
  end;
end;

function TSpecMPSS.ValidIsosOnSpec(var isosExist: boolean): boolean;
{- return true if one of the isolates for the current specimen match the mpssRule.
   return false when no isolate matches the rule (or no isolates exist for the
   specimen). Assumes that dbSpec is on the specimen in question.
   isosExists - Will be set to true if isos exist for specimen, false otherwise}
var
  matchFound  : boolean;
  seq         : TSeqNum;
begin
  matchFound:= false;

  {- locate the first isolate for the current specimen }
  seq:= dbSpec^.dbr^.GetSeqValue;
  dbIso^.dbr^.ClearRecord;
  dbIso^.dbr^.PutField(DBISOSpecRef, @seq);

  if dbIso^.dbc^.GetFirstContains(dbIso^.dbr) then
  begin
    isosExist:= true;
    repeat
      matchFound:= iMPSSObj^.MPSSValidRecord(dbIso);
    until matchFound or not dbIso^.dbc^.GetNextContains(dbIso^.dbr);
  end
  else
    isosExist:= false;

  ValidIsosOnSpec:= matchFound;
end;

function TSpecMPSS.ValidSpecimen: boolean;
{- this function looks at the current specimen and determines if it is valid.
   As specimen is valid when:
   1. The specimen has isolates and at least one of them pass the rule.
   2. The specimen has no isolates and the specimen record passes the specimen
      portion of the rule. }
var
  isosExist   : boolean;  {- will be set to true if isos exist on specimen }
begin
  if not sMPSSObj^.mRuleLoaded then
  begin
    ValidSpecimen := TRUE;
    Exit;
  end;

  ValidSpecimen:= false;  {- assume the worst }

  if ValidIsosOnSpec(isosExist) then
    ValidSpecimen:= true
  else
  begin
    {- at this point, no isos pass the rule. If the specimen has NO isolates,
       then the specimen record must be checked against the rule }
    if not isosExist then
    begin
      {- first see if there is even any specimen data in the rule. If not, dont
         bother wasting time in MPSS }
      if specInRule then
        ValidSpecimen:= sMPSSobj^.MPSSValidRecord(dbSpec)
      else
        ValidSpecimen:= false;
    end
    else
      ValidSpecimen:= false;
  end;
end;

function TSpecMPSS.ValidIsolate(isoSeq: TSeqNum): boolean;
{- this function looks at the current isolate and determines if it is valid.}
begin
  if not iMPSSObj^.mRuleLoaded then
  begin
    ValidIsolate := TRUE;
    Exit;
  end;

  dbIso^.dbc^.GetSeq(dbIso^.dbr, isoSeq);
  ValidIsolate:= iMPSSObj^.MPSSValidRecord(dbIso);
end;

function TSpecMPSS.GetFirstSpecimen: boolean;
{- find the first matching specimen. If this function finds the specimen, then
   dbSpec will be sitting on the valid record }
var
  matchFound  : boolean;
begin
  matchFound:= false;

  if dbSpec^.dbc^.GetFirst(dbSpec^.dbr) then
  begin
    curSpec:= 1;
    if ValidSpecimen then
      matchFound:= true
    else
      matchFound:= GetNextSpecimen;
  end;

  GetFirstSpecimen:= matchFound;
end;

function TSpecMPSS.GetNextSpecimen: boolean;
var
  matchFound  : boolean;
  isEOF       : boolean;
begin
  matchFound:= false;
  if dbSpec^.dbc^.GetNext(dbSpec^.dbr) then
  begin
    Inc(curSpec);
    isEOF:= false;
    while not matchFound and not isEOF do
    begin
      if not ValidSpecimen then
      begin
        if dbSpec^.dbc^.GetNext(dbSpec^.dbr) then
          Inc(curSpec)
        else
          isEOF:= true;
      end
      else
        matchFound:= true;
    end;
  end;

  GetNextSpecimen:= matchFound;
end;

function TSpecMPSS.GetCurrentSpec: longint;
{- return the progress through the specimen file }
begin
  GetCurrentSpec:= curSpec;
end;

function TSpecMPSS.GetTotalSpecs: longint;
{- return the total record count for specimens }
begin
  GetTotalSpecs:= sMPSSobj^.totalRecords;
end;

END.
