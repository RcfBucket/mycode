{----------------------------------------------------------------------------}
{  Module Name  : PatReps.PAS                                                }
{  Programmer   : RCF                                                        }
{  Date Created : July, 13, 1995                                             }
{  Requirements : ..., S3.28 [ref. d]                                        }
{                                                                            }
{  Purpose - This module is responsible for printing the following standard  }
{     DMS reports: Specimen Log, Isolate Log, and the following verities of  }
{     the Short Format Report (SFR): the SFR itself, Specimen Review, and    }
{     Patient Review. It also provides a Test Log that is not used.          }
{                                                                            }
{     These reports are grouped together because they share a common driving }
{     file and top-level column format. The driving file is the specimen     }
{     data file.                                                             }
{                                                                            }
{  Assumptions -                                                             }
{    a. All DMS Data files are present and valid.                            }
{    b. This code assumes the status data file [ref. a] has a fixed content  }
{       where a record having sequence number 2 indicates preliminary status }
{       and that all other sequence numbers are valid and indicate a final   }
{       status.                                                              }
{                                                                            }
{  Referenced Documents                                                      }
{    a. Database Design Document, Revision 0                                 }
{    b. S3.28 Design Document, Revision 1                                    }
{    c. ObjectWindows Programming Guide                                      }
{    d. Software Specification for Nihongo Data Management System v4.00      }
{                                                                            }
{  Revision History - This project is under version control, use it to view  }
{    revision history.                                                       }
{                                                                            }
{----------------------------------------------------------------------------}

unit PatReps;

INTERFACE

uses
  OWindows;

{ Command-level procedures for printing reports }
procedure PrintIsolateLog( aParent: PWindowsobject; useMPSS: boolean );
procedure PrintTestLog( aParent: PWindowsobject; useMPSS: boolean );
procedure PrintSFR( aParent: PWindowsobject; useMPSS: boolean );
procedure PrintSpecimenLog( aParent: PWindowsobject; useMPSS: boolean );
procedure PatientReview( aParent: PWindowsobject; patID: PChar );
procedure SpecimenReview( aParent: PWindowsobject; specID: PChar;
  collect: longint);


IMPLEMENTATION


uses
  APITools,
  DBFile,
  DBLib,
  DBIds,
  DBTypes,
  DMString,
  Gather,
  IntlLib,
  INILib,
  ListLib,
  MScan,
  MPSSObjs,
  Objects,
  PnlView,
  PrnObj,
  PrnPrev,
  ResLib,
  Screens,
  SpecMPSS,
  Status,
  Strings,
  StrSW,
  Sort_Dlg,
  Sort_Eng,
  Sort_Par,
  Sort1,
  WinTypes,
  WinProcs;

{$I PATREPS.INC}
{$R PATREPS.RES}



const
  {--------------------------------------------------------------------------}
  { This constant loosly specifies the length of printString and fieldVal
  { in TReportObj and is used throught the code except in the declaration
  { of the variables. It's used as SizeOf( printStr ). Why is it 120 and
  { not 121?
  {--------------------------------------------------------------------------}
  PrintStrLen = 120;



                        {----------------------}
                        {  Helper Functions    }
                        {----------------------}



{--------------------------------------------------------------------------}
{ This procedure handles assertions for this unit. If an assertion is
{ false, it halts the program. Otherwise, it does nothing.
{--------------------------------------------------------------------------}

procedure Assert( flag: boolean );
begin
  if not flag then
  begin
    MessageBox( 0, 'PATREPS.PAS', 'Error', MB_OK );
    Halt;
  end;
end;



{----------------------------------------------------------------------------}
{ This procedure appends the specified number of blanks onto the end of the
{ string. HowMany will be cliped to PrintStrLen.
{
{ Assert: howMany >= 0
{----------------------------------------------------------------------------}

procedure ConcatBlanks( howMany: integer; newStr: PChar );
begin
  if howMany <= 0 then
    Exit;

  Inc( howMany, StrLen( newStr ) );
  if howMany >= PrintStrLen then
    howMany:= PrintStrLen;
  Pad( newStr, newStr, ' ', howMany );
end;



{----------------------------------------------------------------------------}
{ This procedure appends the specified number of dashes onto the end of the
{ string. HowMany will be cliped to PrintStrLen.
{
{ Assert: howMany >= 0
{----------------------------------------------------------------------------}

procedure ConcatDashes( howMany: integer; newStr: PChar );
begin
  if howMany <= 0 then
    Exit;

  Inc( howMany, StrLen( newStr ) );
  if howMany >= PrintStrLen then
    howMany:= PrintStrLen;
  Pad( newStr, newStr, '-', howMany );
end;



{----------------------------------------------------------------------------}
{ This procedure inserts a colon into the string at maxSize-1 position and
{ fils any space between the colon and the end of the string with blanks. If
{ the string is too long, the operation does nothing.
{
{ Assert: 0 <= maxSize <= PrintStrLen
{----------------------------------------------------------------------------}

procedure ConcatColon( maxSize: integer; newStr: PChar );
begin
  if StrLen( newStr ) > maxSize then
    Exit;
{$IFDEF DMSDEBUG}
  if maxSize > printStrLen then
    InfoMsg( msgParent^.hWindow, 'DEBUG',
      'ConcatColon called with maxSize > printStrLen' );
{$ENDIF}
  FillChar( newStr[ StrLen( newStr ) ], maxSize - StrLen( newStr ), ' ' );
  newStr[maxSize-2] := ':';
  newStr[maxSize] := #0;
end;



                        {----------------------}
                        {      TReportObj      }
                        {----------------------}



type
  {--------------------------------------------------------------------------}
  { This abstract object forms the base for all the reports generated by
  { this module. To derive a report report, override the virtual methods as
  { needed. It's used as follows:
  {
  { 1. Construct it with a report title, MPSS mode, and any added parameters
  { 2. Call PrintLog
  { 3. Destruct it
  {
  { Oops, PrintLog is not defined in the base class. But any report should
  { know how to print itself. Whell, all derived classes will have such a
  { function even though its not designed into the base class.
  {--------------------------------------------------------------------------}
  PReportObj = ^TReportObj;
  TReportObj = object( TObject )

    { Public - the object's interface }
    constructor Init( aParent: PWindowsObject; title: PChar;
      useMPSS: boolean; SortParameters: SortEngineParameters );
    destructor Done; virtual;

    { Protected - For use in derived classes }
    private
    doMPSS       : boolean;
    sMPSS        : PSpecMPSS;
    printInfo    : PPrintInfo;
    patExists    : boolean;
    printStr     : array[0..255] of char;
    rptTitle     : PChar;
    dbIso,
    dbspec,
    dbPat,
    dbIsoOrd,
    wardFile,
    srcFile      : PDBFile;
    resList      : PResults;
    pParent      : PWindowsObject;      { read-only }
    pSorter      : PSpecimenSortEngine; { nil if sorting not requested }
    SpecimenTally: longint;    { running tally of specimens processed }
    function  GetFirstSpecimen( var SpecimenTotal : longint ): boolean;
    function  GetNextSpecimen: boolean;
    procedure SetColumnLabels; virtual;
    procedure AddColumnData;
    function  IsolatesExist: boolean;
    procedure MakeHeader; virtual;

    { Private - Not for use in derived classes }
    function  LoadPatient: boolean;

  end;



{----------------------------------------------------------------------------}
{ This constructor provides initialization of resources common to all
{ reports of this module.
{----------------------------------------------------------------------------}

constructor TReportObj.Init( aParent: PWindowsObject; title: PChar;
  useMPSS: boolean; SortParameters: SortEngineParameters );

  {--------------------------------------------------------------------------}
  { This function calls the destructor if the test value is true. It passes
  { the test value through to facilitate calling Fail.
  {--------------------------------------------------------------------------}
  function DoneOnError( test: boolean ): boolean;
  begin
    if test then
      Done;
    DoneOnError := test;
  end;

var
  Parameters: SortEngineParameters;
  printFont: TLogFont;

begin { Init }

  {---- In case consturction fails and the destructor is called, all
        pointers used in the destructor must be set to a fail-safe value. }
  dbIso := nil;
  dbSpec := nil;
  dbPat := nil;
  wardFile := nil;
  srcFile := nil;
  dbIsoOrd := nil;
  printInfo := nil;
  sMPSS := nil;
  rptTitle := nil;
  pSorter := nil;

  { Initialize the base class }
  if not inherited Init then
    Fail;

  { Store attributes }
  pParent := aParent;
  doMPSS := useMPSS;
  SpecimenTally := 0;


  { Initialize resources }
  if SortParameters.NumberOfLevels > 0 then
  begin
    pSorter := New( PSpecimenSortEngine, Init( SortParameters ) );
    if DoneOnError( pSorter = nil ) then
      Fail;
  end;
  wardFile := New( PDBFile, Init( DBWARDFile, '', dbopenNormal ) );
  if DoneOnError( wardFile = nil ) then
    Fail;
  srcFile:= New( PDBFile, Init( DBSRCFile, '', dbopenNormal ) );
  if DoneOnError( srcFile = nil ) then
    Fail;
  MakeScreenFont( printFont, false, false );
  printInfo:= New( PPrintInfo, Init( printFont, INIDefPointSize ) );
  if DoneOnError( printInfo = nil ) then
    Fail;
  dbSpec := New( PDBFile, Init( DBSpecFile, '', dbOpenNormal ) );
  if DoneOnError( dbSpec = nil ) then
    Fail;
  dbIso := New( PDBFile, Init( DBIsoFile, '', dbOpenNormal ) );
  if DoneOnError( dbIso = nil ) then
    Fail;
  dbPat := New( PDBFile, Init( DBPatFile, '', dbOpenNormal ) );
  if DoneOnError( dbPat = nil ) then
    Fail;
  dbIsoOrd := New( PDBFile, Init( DBIsoOrdFile, '', dbOpenNormal ) );
  if DoneOnError( dbIsoOrd = nil ) then
    Fail;
  sMPSS := New( PSpecMPSS, Init( aParent, dbSpec ) );
  if DoneOnError( sMPSS = nil ) then
    Fail;

  { Why get 128 bytes when the actual size can be found? }
  GetMem( rptTitle, 128 );
  StrLCopy( rptTitle, title, 127 );

  { this should go in PrintLog }
  MakeHeader;
end;



{----------------------------------------------------------------------------}
{ Thid destructor performes clean up.
{----------------------------------------------------------------------------}

destructor TReportObj.Done;
begin
  MSDisposeObj( pSorter );
  MSDisposeObj( dbIso );
  MSDisposeObj( dbSpec );
  MSDisposeObj( dbPat );
  MSDisposeObj( wardFile );
  MSDisposeObj( srcFile );
  MSDisposeObj( dbIsoOrd );
  MSDisposeObj( printInfo );
  MSDisposeObj( sMPSS );
  MSFreeMem( rptTitle, 128 );
  inherited Done;
end;



{----------------------------------------------------------------------------}
{ This procedure generate the header. Override this method if a different
{ header is needed. This procedure is called within the constructor, do not
{ call directly.
{----------------------------------------------------------------------------}

procedure TReportObj.MakeHeader;
var
  pageStr: array [0..40] of char;
begin

  { mixed levels of detail, this first block should be the same detail as
   the last block. }

  printInfo^.header^.AddRow( r_Normal, 0 );
  printInfo^.header^.AddCell( '^d  ^t', LT200, c_Date or c_Time,
    INIDefPointSize-2 );
  printInfo^.header^.AddCell( RptTitle, LT400,
    c_PageCenter or c_Center or c_Bold, 0 );
  SR( IDS_RPTPAGESTR, pageStr, 40 );
  printInfo^.header^.AddCell( pageStr, LT150,
    c_PageRight or c_Right or c_PageNum or c_NumPages, INIDefPointSize-2 );
  printInfo^.header^.AddRow( r_Normal, 0 );


  SetColumnLabels;
end;



{----------------------------------------------------------------------------}
{ This function loads the patient record referenced by the current specimen
{ record. Function returns true if successful. This function is used
{ internally, do not call directly.
{----------------------------------------------------------------------------}

function TReportObj.LoadPatient: boolean;
var
  patSeq: TSeqNum;
begin
  LoadPatient:= false;
  dbSpec^.dbr^.GetField( DBSpecPatRef, @patSeq, sizeof( TSeqNum ) );
  if patSeq <> 0 then
  begin
    dbpat^.dbc^.GetSeq( dbPat^.dbr, patSeq );
    LoadPatient:= true;
  end;
end;



{----------------------------------------------------------------------------}
{ This function returns true if the current specimen has one or more
{ isolates. This function has some side affects within the object.
{
{ Only call within a GetFirstSpecimen/GetNextSpecimen loop.
{----------------------------------------------------------------------------}

function TReportObj.IsolatesExist: boolean;
var
  specRef,
  nSeq: TSeqNum;
begin

  { Search for an isolate record containing the specimen's sequence number,
    then, as an extra test, see if the isolate record references the
    specimen. }
  IsolatesExist:= false;
  dbIso^.dbr^.ClearData;
  specRef:= dbSpec^.dbr^.GetSeqValue;
  dbIso^.dbr^.PutField( DBIsoSpecRef, @specRef );
  if dbIso^.dbc^.GetGE( dbIso^.dbr ) then
  begin
    dbIso^.dbr^.GetField( DBISOSpecRef, @nSeq, sizeof( TSeqNum ) );
    if specRef = nSeq then
      IsolatesExist:= true;
  end;
end;



{----------------------------------------------------------------------------}
{ This procedure generates default column labels and adds them to the header.
{ Called within MakeHeader, do not call directly. Override if special column
{ labels are desired.
{----------------------------------------------------------------------------}

procedure TReportObj.SetColumnLabels;
var
  pStr: array [0..PrintStrLen] of char;
begin
  printInfo^.header^.AddRow( r_BorderBottom, 0 );

  { specimen number }
  dbSpec^.dbr^.Desc^.FieldName( DBSpecSpecID, pStr, PrintStrLen-1 );
  printInfo^.header^.AddCell( pStr, LT75, c_left, INIDefPointSize-2 );
  printInfo^.header^.AddCell( ' ', LT08, c_left, INIDefPointSize-2 );

  { collect date }
  printInfo^.header^.AddCell( SR( IDS_COLLDATE, pStr, 64 ), LT75, c_left,
    INIDefPointSize-2 );
  printInfo^.header^.AddCell( ' ', LT08, c_left, INIDefPointSize-2 );

  { patient Name }
  printInfo^.header^.AddCell( SR( IDS_NAME, pStr, 64 ), LT150, c_left,
    INIDefPointSize-2 );
  printInfo^.header^.AddCell( ' ', LT08, c_left, INIDefPointSize-2 );

  { patient ID }
  dbpat^.dbr^.Desc^.FieldName( DBPATPatID, pStr, PrintStrLen-1 );
  printInfo^.header^.AddCell( pStr, LT100, c_left, INIDefPointSize-2 );
  printInfo^.header^.AddCell( ' ', LT08, c_left, INIDefPointSize-2 );

  { source }
  dbSpec^.dbr^.Desc^.FieldName( DBSpecSrc, pStr, PrintStrLen-1 );
  printInfo^.header^.AddCell( pStr, LT100, c_left, INIDefPointSize-2 );
  printInfo^.header^.AddCell( ' ', LT08, c_left, INIDefPointSize-2 );

  { Ward }
  dbSpec^.dbr^.Desc^.FieldName( DBSpecWard, pStr, PrintStrLen-1 );
  printInfo^.header^.AddCell( pStr, LT100, c_left, INIDefPointSize-2 );
  printInfo^.header^.AddCell( ' ', LT08, c_left, INIDefPointSize-2 );

 { finalyzed flag }
  dbSpec^.dbr^.Desc^.FieldName( DBSpecStat, pStr, PrintStrLen-1 );
  printInfo^.header^.AddCell( pStr, LT75, c_left, INIDefPointSize-2 );
end;



{----------------------------------------------------------------------------}
{ This procedure genarates a row of default column data and adds it to the
{ report. This data should match the columns set up in the SetColumnLabels
{ procedure. Override this method if special column data is desired.
{----------------------------------------------------------------------------}

procedure TReportObj.AddColumnData;
var
  tempLen: integer;
  seq: TSeqNum;
  pStr: array [0..PrintStrLen] of char;
begin
  printInfo^.body^.AddRow( r_startGroup, 0 );
  printStr[0]:= CHR( 0 );

  { specimen number }
  StrCopy( pStr, '' );
  dbSpec^.dbr^.GetFieldAsStr( DBSpecSpecID, pStr, PrintStrLen-1 );
  printInfo^.body^.AddCell( pStr, LT75, c_left, INIDefPointSize-2 );
  printInfo^.body^.AddCell( ' ', LT08, c_left, INIDefPointSize-2 );

  { collect date }
  StrCopy( pStr, '' );
  dbSpec^.dbr^.GetFieldAsStr( DBSpecCollectDate, pStr, PrintStrLen-1 );
  printInfo^.body^.AddCell( pStr, LT75, c_left, INIDefPointSize-2 );
  printInfo^.body^.AddCell( ' ', LT08, c_left, INIDefPointSize-2 );

  if patExists then
  begin
    StrCopy( pStr, '' );
    { patient Name }
    dbPat^.dbr^.GetFieldAsStr( DBPatLName, pStr, PrintStrLen-1 );
    StrLCopy( printStr, pStr, PrintStrLen-1 );
    dbPat^.dbr^.GetFieldAsStr( DBPatFName, pStr, PrintStrLen-1 );
    StrLCat( printStr, ' ', PrintStrLen-1 );
    StrLCat( printStr, pStr, PrintStrLen-1 );
    printInfo^.body^.AddCell( printStr, LT150, c_left, INIDefPointSize-2 );
    printInfo^.body^.AddCell( ' ', LT08, c_left, INIDefPointSize-2 );

    { patient ID }
    dbPat^.dbr^.GetFieldAsStr( DBPATPatID, pStr, PrintStrLen-1 );
    printInfo^.body^.AddCell( pStr, LT100, c_left, INIDefPointSize-2 );
    printInfo^.body^.AddCell( ' ', LT08, c_left, INIDefPointSize-2 );
  end
  else
  begin
    printInfo^.body^.AddCell( ' ', LT150, c_left, INIDefPointSize-2 );
    printInfo^.body^.AddCell( ' ', LT08, c_left, INIDefPointSize-2 );
    printInfo^.body^.AddCell( ' ', LT100, c_left, INIDefPointSize-2 );
    printInfo^.body^.AddCell( ' ', LT08, c_left, INIDefPointSize-2 );
  end;

  { source }
  dbSpec^.dbr^.GetField( DBSpecSrc, @seq, sizeof( TSeqNum ) );
  if seq <> 0 then
  begin
    srcFile^.dbc^.GetSeq( srcFile^.dbr, seq );
    srcFile^.dbr^.GetFieldAsStr( DBSRCDESC, pStr, PrintStrLen-1 );
  end
  else
    StrCopy( pStr, ' ' );
  printInfo^.body^.AddCell( pStr, LT100, c_left, INIDefPointSize-2 );
  printInfo^.body^.AddCell( ' ', LT08, c_left, INIDefPointSize-2 );

  { ward }
  dbSpec^.dbr^.GetField( DBSpecWard, @seq, sizeof( TSeqNum ) );
  if seq <> 0 then
  begin
    wardFile^.dbc^.GetSeq( wardFile^.dbr, seq );
    wardFile^.dbr^.GetFieldAsStr( DBWARDDESC, pStr, PrintStrLen-1 );
  end
  else
    StrCopy( pStr, ' ' );
  printInfo^.body^.AddCell( pStr, LT100, c_left, INIDefPointSize-2 );
  printInfo^.body^.AddCell( ' ', LT08, c_left, INIDefPointSize-2 );

  { finalized flag. Note, this code assumes the status data file has a
    a fixed content where a record having sequence number 2 indicates
    preliminary status and that all other sequence numbers are valid and
    indicate a final status. }
  StrCopy( pStr, '' );
  dbSpec^.dbr^.GetFieldAsStr( dbSpecStat, pStr, SizeOf( pStr ) );
  if StrComp( pStr, '2' ) = 0 then
    SR( IDS_FINAL, pStr, PrintStrLen-1 )
  else
    SR( IDS_PRELIM, pStr, PrintStrLen-1 );
  printInfo^.body^.AddCell( pStr, LT75, c_left, INIDefPointSize-2 );

  printInfo^.body^.AddRow( 0, 0 );
end;



{----------------------------------------------------------------------------}
{ This function loads the first specimen and its patient information if
{ available. Function returns true if specimen was loaded. If false, either
{ no specimens exist or none match the search rules. If true, GetNextSpecimen
{ may be called to get the remaining specimens.
{----------------------------------------------------------------------------}

function TReportObj.GetFirstSpecimen( var SpecimenTotal: longint ): boolean;
var
  GotOne: boolean;
  SeqNum: TSeqNum;
  pSpecimenRecord: PDBRec;

begin { GetFirstSpecimen }

  {---- Report total }
  SpecimenTotal := sMPSS^.GetTotalSpecs;

  {---- Get first specimen, method based on sort mode }
  GotOne := sMPSS^.GetFirstSpecimen;
  if GotOne and ( pSorter <> nil ) then
  begin
    repeat

      pSpecimenRecord := dbSpec^.dbr;

      {use sequence number as sort index }
      SeqNum := pSpecimenRecord^.GetSeqValue;
      Assert( SeqNum <> -1 );

      {input sort element}
      pSorter^.Input( SeqNum );

    until not sMPSS^.GetNextSpecimen;
    pSorter^.Sort;
    GotOne := pSorter^.Output( SeqNum );
    Assert( GotOne );
    dbSpec^.dbc^.GetSeq( dbSpec^.dbr, SeqNum );
  end;

  {---- Load associated information }
  if GotOne then
    patExists := LoadPatient;

  {---- Return status }
  GetFirstSpecimen := GotOne;

end;



{----------------------------------------------------------------------------}
{ This function gets the next specimen in the series started with
{ GetFirstSpecimen. It returns false if it could not get a specimen,
{ indicating the end of the series.
{----------------------------------------------------------------------------}

function TReportObj.GetNextSpecimen: boolean;

  {--------------------------------------------------------------------------}
  { This function gets the next specimen in the sorted series. It returns
  { false if it could not get a specimen, indicating the end of the series.
  {--------------------------------------------------------------------------}
  function GetSortedNext: boolean;
  var
    GotOne: boolean;
    SeqNum: TSeqNum;
  begin
    GotOne := pSorter^.Output( SeqNum );
    if GotOne then
      dbSpec^.dbc^.GetSeq( dbSpec^.dbr, SeqNum );
    GetSortedNext := GotOne;
  end;

var
  GotOne: boolean;

begin { GetNextSpecimen }

  {---- Get next specimen, method based on sort mode }
  if pSorter = nil then
    GotOne := sMPSS^.GetNextSpecimen
  else
    GotOne := GetSortedNext;

  {---- Load associated information }
  if GotOne then
    patExists := LoadPatient;

  {---- Return status }
  GetNextSpecimen := GotOne;

end;



                        {----------------------}
                        {     TTestLogObj      }
                        {----------------------}


type
  {--------------------------------------------------------------------------}
  { This object generates a Test Log and is the base class for generating
  { an Isolate Log.
  {--------------------------------------------------------------------------}
  PTestLogObj = ^TTestLogObj;
  TTestLogObj = object( TReportObj )
    constructor Init( aParent: PWindowsobject; useMPSS: boolean;
      title: PChar; SortParameters: SortEngineParameters );
    destructor Done; virtual;
    procedure PrintLog( aParent: PWindowsobject; aTitle: PChar;
      allSpecimens: boolean );

    private
    SpecimenWithIsolatesTally: { running tally of specimens having isolates }
      longint;
    resultsLoaded: boolean;
    dbOrd: PDBFile;
    dbOrg: PDBFile;
    procedure AddConclusion; virtual;
    procedure SetColumnLabels; virtual; {override not needed, remove}
    private
    function  GetFirstIsolate: boolean;
    function  GetNextIsolate: boolean;
    procedure AddSecondaryBlock( stat: PPctStatDlg ); virtual;
    procedure AddSecondaryColumnHeader; virtual;
    procedure AddSecondaryColumnData; virtual;
  end;



{----------------------------------------------------------------------------}
{ This constructor adds order and organism data file capabilities to the
{ base class capabilities. It also initializes the counts.
{----------------------------------------------------------------------------}

constructor TTestLogObj.Init( aParent: PWindowsobject; useMPSS: boolean;
  title: PChar; SortParameters: SortEngineParameters );

  {--------------------------------------------------------------------------}
  { This function calls the destructor if the test value is true. It passes
  { the test value through to facilitate calling Fail.
  {--------------------------------------------------------------------------}
  function DoneOnError( test: boolean ): boolean;
  begin
    if test then
      Done;
    DoneOnError := test;
  end;

begin

  {---- Reset counter }
  SpecimenWithIsolatesTally := 0;

  {---- The function of this variable is not ideal }
  resultsLoaded := False;

  {---- In case the consturction fails and the destructor is called, all
        pointers are set to value the destructor can handle. }
  dbOrd := nil;
  dbOrg := nil;


  { Initialize the base class }
  if not inherited Init( aParent, title, useMPSS, SortParameters ) then
    Fail;

  { Initialize resources }
  dbOrd:= New( PDBFile, Init( DBOrdFile, '', dbOpenNormal ) );
  if DoneOnError( dbOrd = nil ) then
    Fail;
  dbOrg:= New( PDBFile, Init( DBOrgFile, '', dbOpenNormal ) );
  if DoneOnError( dbOrg = nil ) then
    Fail;

end;



{----------------------------------------------------------------------------}
{ This destructor cleans itself then calls the base class destructor.
{----------------------------------------------------------------------------}

destructor TTestLogObj.Done;
begin
  MSDisposeObj( dbOrd );
  MSDisposeObj( dbOrg );
  inherited Done;
end;



{----------------------------------------------------------------------------}
{ This procedure does nothing.
{----------------------------------------------------------------------------}

procedure TTestLogObj.SetColumnLabels;
begin
  inherited SetColumnLabels;
end;



{----------------------------------------------------------------------------}
{ This procedure does nothing. Override it to append a conclusion to the
{ report. The conclustion is printed after all specimens are printed. Do not
{ call directly.
{----------------------------------------------------------------------------}

procedure TTestLogObj.AddConclusion;
begin
  { no default conclusion }
end;



{----------------------------------------------------------------------------}
{ This procedure renders a default secondary header and adds it to the
{ report. The default header contains column labels for the isolate
{ information in the AddSecondaryColumnData procedure.
{----------------------------------------------------------------------------}

procedure TTestLogObj.AddSecondaryColumnHeader;
var
  pStr: array [0..PrintStrLen] of char;
begin
  printInfo^.body^.AddRow( 0, 0 );
  printInfo^.body^.AddCell( ' ', LT50, c_left, INIDefPointSize-2 );

  { isolate id }
  dbIso^.dbr^.Desc^.FieldName( DBISOIso, pStr, PrintStrLen-1 );
  printInfo^.body^.AddCell( pStr, LT50, c_left or c_borderBottom,
    INIDefPointSize-2 );
  printInfo^.body^.AddCell( ' ', LT25, c_left, INIDefPointSize-2 );

  { Test date }
  dbIso^.dbr^.Desc^.FieldName( DBIsoTstDate, pStr, PrintStrLen-1 );
  printInfo^.body^.AddCell( pStr, LT100, c_left or c_borderBottom,
    INIDefPointSize-2 );
  printInfo^.body^.AddCell( ' ', LT25, c_left, INIDefPointSize-2 );

  { Order ID }
  dbIso^.dbr^.Desc^.FieldName( DBIsoOrdRef, pStr, PrintStrLen-1 );
  printInfo^.body^.AddCell( pStr, LT150, c_left or c_borderBottom,
    INIDefPointSize-2 );
  printInfo^.body^.AddCell( ' ', LT25, c_left, INIDefPointSize-2 );

  { organism }
  SR( IDS_ORGANISM, pStr, PrintStrLen-1 ); { Organism }
  printInfo^.body^.AddCell( pStr, LT125, c_left or c_borderBottom,
    INIDefPointSize-2 );
  printInfo^.body^.AddCell( ' ', LT25, c_left, INIDefPointSize-2 );

  { biotype }
  SR( IDS_BIOTYPE, pStr, PrintStrLen-1 ); { Biotype }
  printInfo^.body^.AddCell( pStr, LT125, c_left or c_borderBottom,
    INIDefPointSize-2 );

end;



{----------------------------------------------------------------------------}
{ This function loads the first isolate of the current specimen. Function
{ returns true if isolate was loaded. If false, no isolate exist. If true,
{ GetNextIsolate may be called to get the remaining isolates.
{
{ Only call within a GetFirstSpecimen/GetNextSpecimen loop.
{----------------------------------------------------------------------------}

function TTestLogObj.GetFirstIsolate: boolean;
var
  seq: TSeqNum;
begin
  dbIso^.dbr^.ClearRecord;
  seq:= dbSpec^.dbr^.GetSeqValue;
  dbIso^.dbr^.PutField( DBIsoSpecRef, @seq );
  GetFirstIsolate:= dbIso^.dbc^.GetFirstContains( dbIso^.dbr );
end;



{----------------------------------------------------------------------------}
{ This function gets the next isolate in the series started with
{ GetFirstIsolate. It returns false if it could not get an isolate,
{ indicating the end of the series.
{
{ Only call within a GetFirstSpecimen/GetNextSpecimen loop.
{----------------------------------------------------------------------------}

function TTestLogObj.GetNextIsolate: boolean;
var
  seq: TSeqNum;
begin
  dbIso^.dbr^.ClearData;
  seq:= dbSpec^.dbr^.GetSeqValue;
  dbIso^.dbr^.PutField( DBIsoSpecRef, @seq );
  GetNextIsolate:= dbIso^.dbc^.GetNextContains( dbIso^.dbr );
end;



{----------------------------------------------------------------------------}
{ This procedure generates default secondary coulumn data using the same
{ layout as AddSecondaryColumnHeader. The default data consists of isolate
{ information. Override this method if special secondary data is desired.
{----------------------------------------------------------------------------}

procedure TTestLogObj.AddSecondaryColumnData;
var
  pStr: array [0..PrintStrLen] of char;
  seq: TSeqNum;
begin
  printInfo^.body^.AddRow( r_startGroup, 0 );
  printInfo^.body^.AddCell( ' ', LT50, c_left, INIDefPointSize-2 );

  { isolate number }
  StrCopy( pStr, '' );
  dbIso^.dbr^.GetFieldAsStr( DBISOIso, pStr, PrintStrLen-1 );
  printInfo^.body^.AddCell( pStr, LT50, c_left, INIDefPointSize-2 );
  printInfo^.body^.AddCell( ' ', LT25, c_left, INIDefPointSize-2 );

  { Test date }
  StrCopy( pStr, '' );
  dbIso^.dbr^.GetFieldAsStr( DBIsoTstDate, pStr, PrintStrLen-1 );
  if pStr[0] <> #0 then
    printInfo^.body^.AddCell( pStr, LT100, c_left, INIDefPointSize-2 )
  else
    printInfo^.body^.AddCell( ' ', LT100, c_left, INIDefPointSize-2 );
  printInfo^.body^.AddCell( ' ', LT25, c_left, INIDefPointSize-2 );

  { Order ID }
  StrCopy( pStr, '' );
  dbIso^.dbr^.GetField( DBIsoOrdRef, @seq, sizeof( TSeqNum ) );
  if seq <> 0 then
  begin
    dbOrd^.dbc^.GetSeq( dbOrd^.dbr, seq );
    dbOrd^.dbr^.GetFieldAsStr( DBOrdDesc, pStr, PrintStrLen-1 );
  end;
  if pStr[0] <> #0 then
    printInfo^.body^.AddCell( pStr, LT150, c_left, INIDefPointSize-2 )
  else
    printInfo^.body^.AddCell( ' ', LT150, c_left, INIDefPointSize-2 );
  printInfo^.body^.AddCell( ' ', LT25, c_left, INIDefPointSize-2 );

  { organism }
  StrCopy( pStr, '' );
  dbIso^.dbr^.GetField( DBIsoOrgRef, @seq, sizeof( TSeqNum ) );
  if seq <> 0 then
  begin
    dbOrg^.dbc^.GetSeq( dbOrg^.dbr, seq );
    dbOrg^.dbr^.GetFieldAsStr( DBOrgSName, pStr, PrintStrLen-1 );
  end;
  if pStr[0] <> #0 then
    printInfo^.body^.AddCell( pStr, LT125, c_left, INIDefPointSize-2 )
  else
    printInfo^.body^.AddCell( ' ', LT125, c_left, INIDefPointSize-2 );
  printInfo^.body^.AddCell( ' ', LT25, c_left, INIDefPointSize-2 );

  { biotype }
  resultsLoaded:= resList^.LoadResults( dbIso^.dbr );
  if resultsLoaded then
  begin
    if resList^.GetResult( '.BIO', 'BIO', pStr, PrintStrLen-1 ) then
      printInfo^.body^.AddCell( pStr, LT125, c_left, INIDefPointSize-2 )
  end
end;



{----------------------------------------------------------------------------}
{ This procedure prints a block of secondary information for the current
{ specimen. The block consists of a secondary header followed by one or more
{ rows of secondary data. The default block contains a lists of the specimen's
{ isolates. Override this function if other secondary information is
{ desired. It is called within PrintLog.
{----------------------------------------------------------------------------}

procedure TTestLogObj.AddSecondaryBlock( stat: PPctStatDlg );
var
  pStr: array[0..64] of char;
  First: boolean;
begin
  {---- Add each valid isolate }
  if stat^.CanContinue and GetFirstIsolate then
  begin
    First := true;
    Inc( SpecimenWithIsolatesTally );
    repeat
      if sMPSS^.ValidIsolate( dbIso^.dbr^.GetSeqValue ) then
      begin

        { Print secondary header before the first isolate }
        if First then
        begin
          AddSecondaryColumnHeader;
          First := false;
        end;

        { print secondary information }
        AddSecondaryColumnData;

      end;
    until not stat^.CanContinue or not GetNextIsolate;
  end;

  {---- Add blank line }
  printInfo^.body^.AddRow( 0, 0 );
end;



{----------------------------------------------------------------------------}
{ This is the object's main procedure. It prompts user for MPSS rules
{ (if it was specified during construction), compiles print information for
{ the selected specimen records while updating a status dialog box, then
{ opens a print preview dialog box where the user can view and/or print the
{ report.
{
{ Inputs:
{   aParent - pointer to parent window [why is this needed?]
{   aTitle - report title
{   allSpecimens - if false, only specimens with isolate are printed.
{
{ Call this procedure directly after consturction, then call the destructor.
{
{ Note: the allSpecimens name is misleading, invert and rename it
{ omitNonIsolates
{----------------------------------------------------------------------------}

procedure TTestLogObj.PrintLog( aParent: PWindowsobject; aTitle: PChar;
  allSpecimens: boolean );
var
  printDlg: PPrnPrevDlg;
  stat: PPctStatDlg;
  isOK,
  gotOne: boolean;
  pStr: array[0..64] of char;
  SpecimenTotal: longint;
begin
  if doMPSS then
    if not sMPSS^.LoadRules( pParent, rptTitle ) then
      exit;

  stat:= New( PPctStatDlg, Init( aParent, MakeIntResource( DLG_PRTSTAT ),
    true, IDC_PERCENT ) );
  application^.MakeWindow( stat );
  SetDlgItemText( stat^.hWindow, IDC_PRTSTATTEXT, aTitle );
  stat^.CompPctLevel( 0, 0 );

  if GetFirstSpecimen( SpecimenTotal ) then
  begin
    repeat
      Inc( SpecimenTally ); {tally this specimen}
      stat^.CompPctLevel( SpecimenTally, SpecimenTotal );
      if allSpecimens or IsolatesExist then
      begin
        AddColumnData; {start group}
        if IsolatesExist then
          AddSecondaryBlock( stat );
        printInfo^.body^.AddEndGroupToLastRow;
      end;
      gotOne:= GetNextSpecimen;
    until not stat^.CanContinue or not gotOne;
    if stat^.CanContinue then
    begin
      stat^.CompPctLevel( 1, 1 );  {- force 100% }
      AddConclusion;
    end;
  end
  else
  begin
    printInfo^.body^.AddRow( 0, 0 );
    printInfo^.body^.AddCell( SR( IDS_NOSPECIMENS, pStr, 64 ), LT800,
      c_center, INIDefPointSize-2 );
    printInfo^.body^.AddRow( 0, 0 );
    stat^.CompPctLevel( 1, 1 );
  end;

  isOK:= stat^.CanContinue;
  MSDisposeObj( stat );

  if isOK then
  begin
    printDlg:= New( PPrnPrevDlg, Init( aParent, printInfo, aTitle ) );
    application^.ExecDialog( printDlg );
  end;
end;



                        {----------------------}
                        {     TIsoLogObj       }
                        {----------------------}



type
  {--------------------------------------------------------------------------}
  { This object generates the Isolate Log and is the base class for the SFR.
  {--------------------------------------------------------------------------}
  PIsoLogObj = ^TIsoLogObj;
  TIsoLogObj = object( TTestLogObj )
    constructor Init( aParent: PWindowsobject; useMPSS: boolean;
      title: PChar; SortParameters: SortEngineParameters );
    destructor Done; virtual;

    private
    listObj: PListObject;
  end;



{----------------------------------------------------------------------------}
{ This constructor adds list and result objects to the base class.
{----------------------------------------------------------------------------}

constructor TIsoLogObj.Init( aParent: PWindowsobject; useMPSS: boolean;
  title: PChar; SortParameters: SortEngineParameters );
begin

  { In case consturction fails and the destructor is called, all
    pointers used in the destructor must be set to a fail-safe value. }
  resList:= nil;
  listObj:= nil;

  { Initialize the base class }
  if not inherited Init( aParent, useMPSS, title, SortParameters ) then
    Fail;

  { Initialize lists }
  listObj:= New( PListObject, Init );
  if listObj = nil then
  begin
    Done;
    Fail;
  end;

  resList:= New( PResults, init );
  if resList = nil then
  begin
    Done;
    Fail;
  end;

end;



{----------------------------------------------------------------------------}
{ This destructor cleans up the object.
{----------------------------------------------------------------------------}

destructor TIsoLogObj.Done;
begin
  MSDisposeObj( listObj );
  MSDisposeObj( resList );
  inherited Done;
end;



                        {----------------------}
                        {       TSFRObj        }
                        {----------------------}



type
  {--------------------------------------------------------------------------}
  { This object generates a SFR.
  {--------------------------------------------------------------------------}
  PSFRObj = ^TSFRObj;
  TSFRObj = object( TIsoLogObj )
    constructor Init( aParent: PWindowsobject; useMPSS: boolean;
      SortParameters: SortEngineParameters );

    private
    procedure AddSecondaryColumnData; virtual;
    procedure AddSecondaryBlock( stat: PPctStatDlg ); virtual;
    procedure AddConclusion; virtual;
  end;


{----------------------------------------------------------------------------}
{ This constructor sets the title and then initializes the base class.
{----------------------------------------------------------------------------}

constructor TSFRObj.Init( aParent: PWindowsobject; useMPSS: boolean;
  SortParameters: SortEngineParameters );
var
  pStr: array[0..64] of char;
begin
  if not inherited Init( aParent, useMPSS, SR( IDS_SFRTITLE, pStr, 64 ),
    SortParameters ) then
    Fail;
end;

{----------------------------------------------------------------------------}
{ This override appends count information to the end of the report.
{----------------------------------------------------------------------------}

procedure TSFRObj.AddConclusion;
var
  pStr: array[0..64] of char;
begin
  printInfo^.body^.AddEndGroupToLastRow;

  { print main tally heading }
  printInfo^.body^.AddRow( r_startGroup, 0 );
  printInfo^.body^.AddRow( 0, 0 );
  printInfo^.body^.AddCell( SR( IDS_TALLYHDG, pStr, 64 ), LT800,
    c_Center or c_underline, INIDefPointSize-2 );
  printInfo^.body^.AddRow( 0, 0 );

  { print sub-headings }
  printInfo^.body^.AddRow( 0, 0 );
  printInfo^.body^.AddCell( ' ', LT100, c_Left, INIDefPointSize-2 );
  { print total sub-heading }
  printInfo^.body^.AddCell( SR( IDS_TOTALHDG, pStr, 64 ), LT150,
    c_underline or c_Center, INIDefPointSize-2 );
  printInfo^.body^.AddCell( ' ', LT25, c_Left, INIDefPointSize-2 );
  { print pos sub-heading }
  printInfo^.body^.AddCell( SR( IDS_POSHDG, pStr, 64 ), LT150,
    c_underline or c_Center, INIDefPointSize-2 );
  printInfo^.body^.AddCell( ' ', LT25, c_Left, INIDefPointSize-2 );
  { print neg sub-heading }
  printInfo^.body^.AddCell( SR( IDS_NEGHDG, pStr, 64 ), LT150,
    c_underline or c_Center, INIDefPointSize-2 );

  { print counts }
  printInfo^.body^.AddRow( 0, 0 );
  printInfo^.body^.AddCell( ' ', LT100, c_Left, INIDefPointSize-2 );
  { print total specimen count }
  Str( SpecimenTally, pStr );
  printInfo^.body^.AddCell( pStr, LT150, c_Center, INIDefPointSize-2 );
  printInfo^.body^.AddCell( '  ', LT25, c_Left, INIDefPointSize-2 );
  { print tally of specimens having isolates }
  Str( SpecimenWithIsolatesTally, pStr );
  printInfo^.body^.AddCell( pStr, LT150, c_Center, INIDefPointSize-2 );
  printInfo^.body^.AddCell( '  ', LT25, c_Left, INIDefPointSize-2 );
  { print tally of specimens not having isolates (computed) }
  Str( ( SpecimenTally - SpecimenWithIsolatesTally ), pStr );
  printInfo^.body^.AddCell( pStr, LT150, c_Center, INIDefPointSize-2 );

  printInfo^.body^.AddRow( r_endGroup, 0 );
end;



{----------------------------------------------------------------------------}
{ This override appends result information to the secondary information
{ provided by the base class.
{----------------------------------------------------------------------------}

procedure TSFRObj.AddSecondaryColumnData;

  {----------------------------------------------------------------------------}
  { This procedure renders result information for the current isolate. It will
  { either print the MIC data or a no data message if the results were not
  { loaded.
  {----------------------------------------------------------------------------}
  procedure AppendResults;

    {----------------------------------------------------------------------------}
    { This function renders the extra test information. Function returns true
    { if any information was rendered.
    {
    { May assume sortedRes^.NumIDResults > 0
    {----------------------------------------------------------------------------}
    function PrintExtraTests( sortedRes: PGatherRes ): boolean;
    const gStaph  = $1;
          gSyngy  = $16;

      function IsBioType(Res: PResRecObj): Boolean; far;
      begin
        IsBioType:=StrComp(Res^.TestCatID, 'BIO')=0;
      end;

    var
      retVal, IsHNID: boolean;
      idx,code,OrgNum: integer;
      seq: TSeqNum;
      tstr,pStr: array [0..PrintStrLen] of char;
      BioRes:PResRecObj;
    begin
      retVal:= False;

      StrCopy( printStr, '' );
      if resList^.GetResult( '.TFG', 'TFG', pStr, PrintStrLen-1 ) and
        ( StrLen( pStr ) > 0 ) then
      begin
        StrLCat( printStr, 'TFG ', PrintStrLen-1 );
        StrLCat( printStr, pStr, PrintStrLen-1 );
      end;

      { organism }
      dbIso^.dbr^.GetField( DBIsoOrgRef, @seq, sizeof( TSeqNum ) );
      if seq <> 0 then
      begin
        dbOrg^.dbc^.GetSeq( dbOrg^.dbr, seq );
        dbOrg^.dbr^.GetFieldAsStr( DBOrgOrg, pStr, PrintStrLen-1 );
        Val(pStr, orgNum, code);
      end;

      BioRes:=ResList^.FirstThat(@IsBioType);
      IsHNID:=(BioRes<>Nil) and (StrComp(BioRes^.TestGroupID,'HNID')=0);

      if CheckOrg(orgNum, GStaph) or IsHNID then
      if resList^.GetResult( '.BL', 'BL', pStr, PrintStrLen-1 ) and
        ( StrLen( pStr ) > 0 ) then
      begin
        if ( StrLen( printStr ) > 0 ) then
          StrLCat( printStr, ',  ', PrintStrLen-1 );
        StrLCat( printStr, 'BL ', PrintStrLen-1 );
        StrLCat( printStr, pStr, PrintStrLen-1 );
      end;

      if CheckOrg(orgNum, GSyngy) then
      begin
        if resList^.GetResult( '.GmS', 'GmS', pStr, PrintStrLen-1 ) and
          ( StrLen( pStr ) > 0 ) then
        begin
          if ( StrLen( printStr ) > 0 ) then
            StrLCat( printStr, ',  ', PrintStrLen-1 );
          StrLCat( printStr, 'GmS ', PrintStrLen-1 );
          StrLCat( printStr, pStr, PrintStrLen-1 );
        end;
        if resList^.GetResult( '.StS', 'StS', pStr, PrintStrLen-1 ) and
          ( StrLen( pStr ) > 0 ) then
        begin
          if ( StrLen( printStr ) > 0 ) then
            StrLCat( printStr, ',  ', PrintStrLen-1 );
          StrLCat( printStr, 'StS ', PrintStrLen-1 );
          StrLCat( printStr, pStr, PrintStrLen-1 );
        end;
      end;

      if resList^.GetResult( '.ESBL', 'ESBL', pStr, PrintStrLen-1 ) and
        ( StrLen( pStr ) > 0 ) then
      begin
        if ( StrLen( printStr ) > 0 ) then
          StrLCat( printStr, ',  ', PrintStrLen-1 );
        StrLCat( printStr, 'ESBL ', PrintStrLen-1 );
        StrLCat( printStr, pStr, PrintStrLen-1 );
      end;
      if ( StrLen( printStr ) > 0 ) then
      begin
        printInfo^.body^.AddRow( 0, 0 );
        printInfo^.body^.AddCell( '    ', LT75, c_left, INIDefPointSize-2 );
        printInfo^.body^.AddCell( printStr, LT600, c_left, INIDefPointSize-2 );
        printInfo^.body^.AddRow( 0, 0 );
        retVal:= True;
      end;
      PrintExtraTests:= retVal;
    end;

  var
    i: integer;
    sortedRes: PGatherRes;
    pStr: array[0..64] of char;
    pStr2: array[0..64] of char;

  begin { AppendResults }

    if resultsLoaded then
    begin
      printInfo^.body^.AddRow( 0, 0 );
      sortedRes:= New( PGatherRes, Init( resList, listObj, 0, false, false, false ) );
      if ( sortedRes^.NumMICResults > 0 ) or ( sortedRes^.NumIDResults > 0 )
        then
      begin
        if sortedRes^.NumMICResults > 0 then
        begin
          printInfo^.body^.AddRow( r_CellWrap, LT75 );
          printInfo^.body^.AddCell( '    ', LT75, c_left, INIDefPointSize-2 );
          for i:= 0 to sortedRes^.NumMICResults-1 do
          begin
            StrCopy( printStr, '' );
            sortedRes^.GetMICDrugAbbr( i, pstr, 64 );
            StrCat( printStr, pstr );
            StrCat( printStr, #10 );
            sortedRes^.GetMICResult( i, pstr, pstr2, 64 );
            StrCat( printStr, pstr );
            StrCat( printStr, #10 );
            StrCat( printStr, pstr2 );
            StrCat( printStr, #10 );
            printInfo^.body^.AddCell( printStr, LT50, c_left or c_WordWrap,
              INIDefPointSize-2 )
          end;
        end;
        if sortedRes^.NumIDResults > 0 then
          PrintExtraTests( sortedRes );
        printInfo^.body^.AddEndGroupToLastRow;
        MSDisposeObj( sortedRes );
        exit;
      end;
      MSDisposeObj( sortedRes );
    end;
    printInfo^.body^.AddRow( 0, 0 );
    printInfo^.body^.AddCell( SR( IDS_NOMICS, pStr, 64 ), LT800, c_Center,
      INIDefPointSize-2 );
    printInfo^.body^.AddRow( r_endGroup, 0 );
  end;

begin { AddSecondaryColumnData }

  inherited AddSecondaryColumnData;
  AppendResults;

end;



{----------------------------------------------------------------------------}
{ This override replaces the default behavior, however, it looks the same
{ except it does not print a blank line. A simpler solution might be found
{ so that the base class can be called.
{----------------------------------------------------------------------------}

procedure TSFRObj.AddSecondaryBlock( stat: PPctStatDlg );
var
  pStr: array[0..64] of char;
  First: boolean;
begin
  if stat^.CanContinue and GetFirstIsolate then
  begin
    First := true;
    Inc( SpecimenWithIsolatesTally );
    repeat
      if sMPSS^.ValidIsolate( dbIso^.dbr^.GetSeqValue ) then
      begin

        { Print secondary header before the first isolate }
        if First then
        begin
          AddSecondaryColumnHeader;
          First := false;
        end;

        { Print secondary information }
        AddSecondaryColumnData;

      end;
    until not stat^.CanContinue or not GetNextIsolate;
  end;
end;



                        {----------------------}
                        {     TSpecLogObj      }
                        {----------------------}



type
  {--------------------------------------------------------------------------}
  { This object generates a Specimen Log.
  {--------------------------------------------------------------------------}
  PSpcLogObj = ^TSpcLogObj;
  TSpcLogObj = object( TReportObj )
    constructor Init( aParent: PWindowsobject; Title: PChar; useMPSS: boolean;
      SortParameters: SortEngineParameters );
    procedure PrintLog( aParent: PWindowsobject );
  end;



{----------------------------------------------------------------------------}
{ This constructor initializes the base class with a specimen log title
{----------------------------------------------------------------------------}

constructor TSpcLogObj.Init( aParent: PWindowsobject; Title: PChar;
  useMPSS: boolean; SortParameters: SortEngineParameters );
begin
  if not inherited Init( aParent, Title, useMPSS,
    SortParameters ) then
    Fail;
end;



{----------------------------------------------------------------------------}
{ This procedure prompts user for MPSS rules (if enabled), compiles print
{ information for the selected specimen records while updating a status
{ dialog box, then opens a print preview dialog box where the user can view
{ and/or print the report.
{
{ Call this procedure directly after consturction, then call the destructor.
{----------------------------------------------------------------------------}

procedure TSpcLogObj.PrintLog( aParent: PWindowsobject );
var
  printDlg: PPrnPrevDlg;
  stat: PPctStatDlg;
  isOK: boolean;
  pStr: array[0..64] of char;
  SpecimenTotal: longint;
begin
  if doMPSS then
    if not sMPSS^.LoadRules( aParent, SR( IDS_SPECLOGTITLE, pStr, 64 ) ) then
      exit;

  if GetFirstSpecimen( SpecimenTotal ) then
  begin
    stat:= New( PPctStatDlg, Init( aParent, MakeIntResource( DLG_PRTSTAT ),
      true, IDC_PERCENT ) );
    application^.MakeWindow( stat );
    SetDlgItemText( stat^.hWindow, IDC_PRTSTATTEXT, SR( IDS_SPECLOGTITLE,
      pStr, 64 ) );
    repeat
      Inc( SpecimenTally ); {tally this specimen}
      stat^.CompPctLevel( SpecimenTally, SpecimenTotal );
      if not IsolatesExist then
      begin
        AddColumnData;
      end;
    until not stat^.CanContinue or ( not (GetNextSpecimen) );
    stat^.CompPctLevel( 1, 1 );
    isOK:= stat^.CanContinue;
    MSDisposeObj( stat );
  end;

  printDlg:= New( PPrnPrevDlg, Init( aParent, printInfo, SR( IDS_SPECLOGTITLE,
    pStr, 64 ) ) );
  application^.ExecDialog( printDlg );
end;



                        {----------------------}
                        {  EXPORTED procedures }
                        {----------------------}



{----------------------------------------------------------------------------}
{ This is the command-level procedure for printing a test log.
{
{ Inputs:
{   aParent - pointer to parent window
{   useMPSS - set true if MPSS prompting and processing is desired
{----------------------------------------------------------------------------}

procedure PrintTestLog( aParent: PWindowsobject; useMPSS: boolean );
const
  TITLE_SIZE = 64;
var
  testLog: PTestLogObj;
  SortParameters: SortEngineParameters;
  Title: PChar;
  pKeyInformation: PGroup1KeyInformation;
  Action: integer;
begin

  { Get report title }
  GetMem( Title, TITLE_SIZE );
  Title := SR( IDS_TESTLOGTITLE, Title, TITLE_SIZE );

  { Load previous and permit user to view/edit }
  GetGroup1SortParameters( SortParameters );
  pKeyInformation := New( PGroup1KeyInformation, Init );
  Action := SortParametersDialog( aParent, Title, pKeyInformation,
    false, SortParameters );

  { If user decided to continue, generate report and update default sort
    parameters. }
  if Action = id_Next then
  begin
    { Generate the report }
    testLog:= New( PTestLogObj, Init( aParent, useMPSS, Title,
      SortParameters ) );
    if testLog <> nil then
    begin
      testLog^.PrintLog( aParent, Title, False );
      MSDisposeObj( TestLog );

      { Update defaults }
      SetGroup1SortParameters( SortParameters )
    end;
  end;

  { Clean up }
  Dispose( pKeyInformation, Done );
  FreeMem( Title, TITLE_SIZE );

end;



{----------------------------------------------------------------------------}
{ This is the command-level procedure for printing an isolate log.
{
{ Inputs:
{   aParent - pointer to parent window
{   useMPSS - true if MPSS prompting and processing is desired
{----------------------------------------------------------------------------}

procedure PrintIsolateLog( aParent: PWindowsobject; useMPSS: boolean );
const
  TITLE_SIZE = 64;
var
  isoLog: PIsoLogObj;
  SortParameters: SortEngineParameters;
  Title: PChar;
  pKeyInformation: PGroup1KeyInformation;
  Action: integer;
begin

  { Get report title }
  GetMem( Title, TITLE_SIZE );
  Title := SR( IDS_ISOLATELOGTITLE, Title, TITLE_SIZE );

  { Load previous and permit user to view/edit }
  GetGroup1SortParameters( SortParameters );
  pKeyInformation := New( PGroup1KeyInformation, Init );
  Action := SortParametersDialog( aParent, Title, pKeyInformation,
    false, SortParameters );

  { If user decided to continue, generate report and update default sort
    parameters. }
  if Action = id_Next then
  begin
    { Generate the report }
    isoLog:= New( PIsoLogObj, Init( aParent, useMPSS, Title, SortParameters ) );
    if isoLog <> nil then
    begin
      isoLog^.PrintLog( aParent, Title, False );
      MSDisposeObj( isoLog );

      { Update defaults }
      SetGroup1SortParameters( SortParameters )
    end;
  end;

  { Clean up }
  Dispose( pKeyInformation, Done );
  FreeMem( Title, TITLE_SIZE );

end;



{----------------------------------------------------------------------------}
{ This is the command-level procedure for printing a SFR.
{
{ Inputs:
{   aParent - pointer to parent window
{   useMPSS - true if MPSS prompting and processing is desired
{----------------------------------------------------------------------------}

procedure PrintSFR( aParent: PWindowsobject; useMPSS: boolean );
var
  sfr: PSFRObj;
  Buffer: array[0..64] of char;
  Title: PChar;
  SortParameters: SortEngineParameters;
  pKeyInformation: PGroup1KeyInformation;
  Action: integer;
begin

  Title := SR( IDS_SFRTITLE, Buffer, SizeOf( Buffer ) );

  { Load previous and permit user to view/edit }
  GetGroup1SortParameters( SortParameters );
  pKeyInformation := New( PGroup1KeyInformation, Init );
  Action := SortParametersDialog( aParent, Title, pKeyInformation,
    false, SortParameters );

  { If user decided to continue, generate report and update default sort
    parameters. }
  if Action = id_Next then
  begin
    { Generate the report }
    sfr:= New( PSFRObj, Init( aParent, useMPSS, SortParameters ) );
    if sfr <> nil then
    begin
      sfr^.PrintLog( aParent, Title, True );
      MSDisposeObj( sfr );

      { Update defaults }
      SetGroup1SortParameters( SortParameters )
    end;
  end;

  { Clean up }
  Dispose( pKeyInformation, Done );

end;



{----------------------------------------------------------------------------}
{ This is the command-level procedure for printing a specimen log.
{
{ Inputs:
{   aParent - pointer to parent window
{   useMPSS - true if MPSS prompting and processing is desired
{----------------------------------------------------------------------------}

procedure PrintSpecimenLog( aParent: PWindowsobject; useMPSS: boolean );
const
  TITLE_SIZE = 64;
var
  spcLog: PSpcLogObj;
  SortParameters: SortEngineParameters;
  pKeyInformation: PGroup1KeyInformation;
  Action: integer;
  Title: PChar;
begin

  { Get report title }
  GetMem( Title, TITLE_SIZE );
  Title := SR( IDS_SPECLOGTITLE, Title, TITLE_SIZE );

  { Load default sort parameters and permit user to view/edit }
  GetGroup1SortParameters( SortParameters );
  pKeyInformation := New( PGroup1KeyInformation, Init );
  Action := SortParametersDialog( aParent, Title, pKeyInformation,
    false, SortParameters );

  { If user decided to continue, generate report and update default sort
    parameters. }
  if Action = id_Next then
  begin
    { Generate the report }
    spcLog := New( PSpcLogObj, Init( aParent, Title, useMPSS,
      SortParameters ) );
    if spcLog <> nil then
    begin
      spcLog^.PrintLog( aParent );
      MSDisposeObj( spcLog );

      { Update defaults }
      SetGroup1SortParameters( SortParameters )
    end;
  end;

  { Clean up }
  FreeMem( Title, TITLE_SIZE );
  Dispose( pKeyInformation, Done );

end;



{----------------------------------------------------------------------------}
{ This is the command-level procedure for printing a patient review.
{
{ Inputs:
{   aParent - pointer to parent window
{   patID - a patient ID as an xxx field in the yyy data file.
{----------------------------------------------------------------------------}

procedure PatientReview( aParent: PWindowsobject; patID: PChar );
const
  NO_MPSS = false;
var
  sfr: PSFRObj;
  pStr: array[0..64] of char;
  j: word;
  SortParameters: SortEngineParameters;
begin

  { Disable sorting }
  SortParameters.Clear;

  {}
  sfr:= New( PSFRObj, Init( aParent, NO_MPSS, SortParameters ) );
  if sfr <> nil then
  begin
    {- manually build a rule for the report object to print only for the
       specified patient }
    with sfr^.sMPSS^.iMPSSobj^ do
    begin
      j := 1;
      while StrIComp( mFiles^[j].FileName, DBPATFile ) <> 0 do
      begin
        Inc( j );
        if mFiles^[j].FH = nil then
        begin
          MSDisposeObj( sfr );
          Exit;
        end;
      end;

      mRules^[0].andOR    := MPSSAnd;
      mRules^[0].lParen   := FALSE;
      mRules^[0].rParen   := FALSE;
      mRules^[0].FH       := mFiles^[j].FH; {- patient }
      StrLCopy( mRules^[0].fileDesc, mFiles^[j].fileDesc,
        SizeOf( mRules^[0].fileDesc ) - 1 );
      StrCopy(mRules^[0].fieldName, '');
      mRules^[0].fieldNo  := DBPatPatID;
      mRules^[0].fieldType:= dbZCode;       {- type of field }
      mRules^[0].testRef  := -1;
      mRules^[0].operator := MPSSEQ;
      StrCopy( mRules^[0].value, patID );
      mRules^[0].lValue   := 0;

      mRules^[1].FH:= nil;

      mRuleLoaded:= true;
    end;
    sfr^.sMPSS^.CopyRules;

    sfr^.PrintLog( aParent, SR( IDS_SFRTITLE, pStr, 64 ), True );
    MSDisposeObj( sfr );
  end;

end;



{----------------------------------------------------------------------------}
{ This is the command-level procedure for printing the Specimen Review
{ version of the SFR.
{
{ Input:
{   aParent - pointer to parent window
{   specID - Specimen's day-wise identifier
{   collect - Specimen Collect Date
{
{ For details on specID and collect, see Specimen Record in ref. b.
{
{ Note: collect is a TIntlDate not a longint, rename it CollectDate or Date
{----------------------------------------------------------------------------}

procedure SpecimenReview( aParent: PWindowsobject; specID: PChar;
  collect: longint );
const
  NO_MPSS = false;
var
  sfr: PSFRObj;
  pStr: array[0..64] of char;
  j: word;
  SortParameters: SortEngineParameters;
begin

  { Disable sorting }
  SortParameters.Clear;

  {}
  sfr:= New( PSFRObj, Init( aParent, NO_MPSS, SortParameters ) );
  if sfr <> nil then
  begin
    {- manually build a rule for the report object to print only for the
       specified specimen }
    with sfr^.sMPSS^.iMPSSobj^ do
    begin
      j := 1;
      while StrIComp( mFiles^[j].FileName, DBSPECFile ) <> 0 do
      begin
        Inc(j);
        if mFiles^[j].FH = nil then
        begin
          MSDisposeObj( sfr );
          Exit;
        end;
      end;

      mRules^[0].andOR    := MPSSAnd;
      mRules^[0].lParen   := FALSE;
      mRules^[0].rParen   := FALSE;
      mRules^[0].FH       := mFiles^[j].FH; {- specimen }
      StrLCopy( mRules^[0].fileDesc, mFiles^[j].fileDesc,
        SizeOf( mRules^[0].fileDesc ) - 1 );
      StrCopy( mRules^[0].fieldName, '' );
      mRules^[0].fieldNo  := DBSpecSpecID;
      mRules^[0].fieldType:= dbZCode;       {- type of field }
      mRules^[0].testRef  := -1;
      mRules^[0].operator := MPSSEQ;
      StrCopy( mRules^[0].value, specID );
      mRules^[0].lValue   := 0;

      mRules^[1].andOR    := MPSSAnd;
      mRules^[1].lParen   := FALSE;
      mRules^[1].rParen   := FALSE;
      mRules^[1].FH       := mFiles^[j].FH; {- specimen }
      StrLCopy(mRules^[1].fileDesc, mFiles^[j].fileDesc,
        SizeOf(mRules^[1].fileDesc)-1);
      StrCopy(mRules^[1].fieldName, '');
      mRules^[1].fieldNo  := DBSPECCollectDate;
      mRules^[1].fieldType:= dbDate;        {- type of field }
      mRules^[1].testRef  := -1;
      mRules^[1].operator := MPSSEQ;
      StrCopy( mRules^[1].value, '' );
      mRules^[1].lValue   := collect;

      mRules^[2].FH       := nil;

      mRuleLoaded:= true;
    end;
    sfr^.sMPSS^.CopyRules;

    sfr^.PrintLog( aParent, SR( IDS_SFRTITLE, pStr, 64 ), True );
    MSDisposeObj( sfr );
  end;

end;

end.