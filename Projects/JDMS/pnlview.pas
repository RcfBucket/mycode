unit PnlView;
{----------------------------------------------------------------------------}
{  Module Name  : PNLVIEW.PAS                                                }
{  Programmer   : MJ                                                         }
{  Date Created : 05/95                                                      }
{                                                                            }
{  Purpose - This module provides common functionality for both the W/A      }
{            panel read screen and the AS4 panel read screen.  It contains   }
{            the implementation for the search dialog, editing the MIC and   }
{            biochem lists, saving the isolate tests, and the biotype editor.}
{                                                                            }
{  Assumptions -                                                             }
{  None.                                                                     }
{                                                                            }
{  Revision History -                                                        }
{                                                                            }
{  Version  Date      Who    Did What                                        }
{  -------  --------  ------ ----------------------------------------------- }
{  1.00     05/95     MJ     Initial release.                                }
{                                                                            }
{----------------------------------------------------------------------------}

INTERFACE

uses
  APITools,
  Biotype,
  DBFile,
  DBIds,
  DBLib,
  DBTypes,
  DlgLib,
  Interps,
  Objects,
  ODialogs,
  OWindows,
  DMString,
  PDLObj,
  ResLib,
  ListLib,
  Strings,
  Strsw,
  Trays,
  WinTypes,
  WinProcs,
  UserMsgs,
  UICommon,
  UIFlds;

{$I PNLVIEW.INC}
{$R PNLVIEW.RES }

const
  Fermenter   = 1;
  Nonfermenter = 2;
  Strep       = 1;
  Micro       = 2;

type
  PSelectedOrg = ^Integer;  {Org selected from biotype editor dialog}

  PSpaceList = ^TSpaceList;
  TSpaceList = object(TListBox)
    procedure wmChar(var msg: TMessage); virtual WM_FIRST + WM_CHAR;
    procedure wmKillFocus(var msg: TMessage); virtual WM_FIRST + WM_KILLFOCUS;
  end;

  PSearchDlg = ^TSearchDlg;
  TSearchDlg = object(TCenterDlg)
    constructor Init(aParent    : PWindowsobject;
                     dbSpecFile,
                     dbIsoFile,
                     tstGrpFile : PDBFile;
                 var isoOrdFile : PDBAssocFile);
    destructor Done; virtual;
    procedure SetupWindow; virtual;
    procedure DoClear(var msg: TMessage); virtual ID_FIRST + BTN_SEARCHCLEAR;
    procedure DoSearch(var msg: TMessage); virtual ID_FIRST + BTN_SEARCHOK;
    {}
    private
    dbSpec,
    dbIso,
    dbTstGrp   : PDBFile;
    dbIsoOrd   : PDBAssocFile;
    pdbIsoOrd  : ^PDBAssocFile;
    sFld,
    cFld,
    iFld       : PUIFld;
    oFld       : PUIListFld;
    tstList    : PCollection;
    pstr       : array [0..100] of char;
    listObj    : PListObject;
    specID     : array[0..16] of char;
    cDate      : array[0..16] of char;
    {}
    procedure WMLostFocus(var msg: TMessage);    virtual WM_FIRST + WM_LOSTFOCUS;
    procedure WMFldList(var msg: TMessage);      virtual WM_FIRST + WM_FLDLIST;
    procedure WMMnemSelected(var msg: TMessage); virtual WM_FIRST + WM_MNEMSELECTED;
    procedure EnableButtons; virtual;
  end;

  PEditMICDlg = ^TEditMICDlg;
  TEditMICDlg = object(TCenterDlg)
    trayObj : PTraysobject;
    drgRDN  : integer;
    dilStep : ^integer;
    list    : PListBox;
    newMIC  : PChar;
    {}
    constructor Init(aParent     : PWindowsobject;
                     aTrayObj    : PTraysobject;
                     aRDN        : integer;
                     adilStep    : pointer;
                     aNewMIC     : PChar);
    procedure SetupWindow; virtual;
    function CanClose: boolean; virtual;
  end;

  PBiochemDialog = ^TBiochemDialog;
  TBiochemDialog = OBJECT(TCenterDlg)
    BiochemName : PChar;
    posFlags    : PPanelBitmap;
    WellNum     : Integer;
    {}
    constructor Init(AParent      : PWindowsObject;
                     ABiochemName : PChar;
                     aPosFlags    : PPanelBitmap;
                     AWellNum     : Integer);
    procedure SetupWindow;  virtual;
    function CanClose: boolean; virtual;
  end;

  PXtraTestDlg = ^TXtraTestDlg;
  TXtraTestDlg = object(TCenterDlg)
    testName    : PChar;
    res         : PChar;
    {}
    constructor Init(AParent   : PWindowsObject;
                     ATestName : PChar;
                     ARes      : PChar);
    procedure SetupWindow;  virtual;
    function CanClose: boolean; virtual;
  end;

function GetETResult(testNum   : integer;
                     testID    : PChar;
                     xTraTbl   : XTraType;
                     resList   : PResults;
                     orgNum    : integer;
                 var wellFile  : text;
                     posFlags  : PPanelBitmap;
                     micData   : PMICData): integer;

function SaveMICs(resList  : PResults;
                  interp   : PInterp;
                  orgRef   : TSeqNum;
                  testFile : PDBFile;
                  micData  : PMICData;
                  overwrite: boolean): boolean;

function EditBiochem(aParent    : PWindowsobject;
                     biotypeObj : PBiotypeobject;
                     bitNum     : integer;
                     resList    : PResults;
                     newStr     : PChar;
                     posFlags   : PPanelBitmap;
                 var sugar      : boolean): boolean;

function SaveIsolateTests(aParent     : PWindowsobject;
                          overWrite,
                          prompt,
                          saveID,
                          saveMIC,
                          saveET      : boolean;
                      var canceled    : boolean;
                      var wellFile    : Text;
                          orgDB       : PDBFile;
                          newOrgRef   : TSeqNum;
                          bioStr      : PChar;
                          micData     : PMICData;
                          isoDB       : PDBFile;
                          specDB      : PDBFile;
                          resList     : PResults;
                          posFlags    : PPanelBitMap): boolean;

function BiotypeEditor(aParent    : PWindowsobject;
                       aBiotypeObj: PBiotypeobject;
                       results    : PResults;
                       anOrgID    : PDBFile;
                       aProbTxt   : PChar;
                       aPosFlags    : PPanelBitmap;
                       aSelectedOrg : PSelectedOrg): boolean;

function GetAnOrganism(aParent: PWindowsObject; var orgSeq: TSeqNum): boolean;

function GetBiotype(aParent  : PWindowsObject;
                    prompt   : boolean;
                    aFamily  : integer;
                    resList  : PResults;
                    posFlags,
                    chkFlags,
                    virtTests: PPanelBitmap;
                    MICData  : PMICData;
                    bioObj   : PBiotypeObject): boolean;

function RecomputeBiotype(aParent  : PWindowsObject;
                          resList  : PResults;
                          posFlags,
                          chkFlags : PPanelBitmap;
                          MICData  : PMICData;
                          bioObj   : PBiotypeObject): boolean;

Function PromptForBiochemResult(AParent   : PWindowsObject;
                                bioObj    : PBiotypeObject;
                                bitNum    : integer;
                                posFlags,
                                chkFlags  : PPanelBitmap;
                            var result    : boolean): BOOLEAN;

function EditMIC(aParent  : Pwindowsobject;
                 micInfo  : PMicData;
                 traysObj : PTraysobject;
                 drugRDN  : integer;  {- 0 based !!}
                 newStr   : PChar): boolean;

function AllSugarsNeg(posFlags: PPanelBitmap): boolean;

function CheckOrg(orgNum, orgGroupNum : integer) : boolean;

{------------------------------------------------------------------------------}

IMPLEMENTATION

uses
  Win31,
  IntlLib,
  MScan,
  DMSErr,
  ESBL,
  Screens,
  Bits,
  OrgGroup,
  IDLIB,
  CtlLib,
  WaitLib,
  DMSDebug;

const
  NumSC       = 6;
  SC     : array[1..NumSC] of byte =
           (11, 12, 51, 52, 21, 22);
  ID_SC  : array[1..NumSC] of word =
           (IDC_fermenter,     {- SC 11 }
            IDC_nonfermenter,  {- SC 12 }
            IDC_strep,         {- SC 51 }
            IDC_micro,         {- SC 52 }
            IDC_strep,         {- SC 21 }
            IDC_micro          {- SC 22 });
  PID1Strep   = 3;
  PID1Staph   = 4;
  PID2Strep   = 5;
  PID2Staph   = 6;

type
  PSaveDlg = ^TSaveDlg;
  TSaveDlg = object(TCenterDlg)
    saveMIC     : ^boolean;
    saveID      : ^boolean;
    idOnly      : boolean;
    MICOnly     : boolean;
    prevStor    : boolean;
    {}
    constructor Init(aParent      : PWindowsobject;
                     var aSaveID,
                     aSaveMIC     : boolean;
                     aIdOnly,
                     aMICOnly,
                     aPrevStored  : boolean);
    procedure SetupWindow; virtual;
    function CanClose: boolean; virtual;
    procedure EnableButtons; virtual;
    procedure WMCommand(var msg: TMessage); virtual WM_FIRST + WM_COMMAND;
  end;

  PBioDlg = ^TBioDlg;
  TBioDlg = object(TCenterDlg)
    orgDB       : PDBFile;
    probTxt     : PChar;
    bioObj      : PBiotypeobject;
    saveBioStr  : string;
    saveBioMask : string;
    saveClass   : integer;
    saveFlags   : TPanelBitmap;
    saveNeeded  : string;
    saveOxiRes  : array[0..20] of char;
    saveIndRes  : array[0..20] of char;
    bioChemList : PSpaceList;
    orglist     : PListBox;
    orgNums     : PListBox;
    orgProbs    : PListBox;
    AddTest1    : PListBox;  {Additional tests}
    AddTest2    : PListBox;  {Additional tests}
    AddTest3    : PListBox;  {Additional tests}
    setClass    : integer;
    SelectedOrg : PSelectedOrg;   {Org selected from orglist in biodlg}
    posFlags    : PPanelBitmap;
    resList     : PResults;
    ready       : boolean;
    {}
    constructor Init(aParent    : PWindowsobject;
                     aBiotypeObj: PBiotypeobject;
                     results    : PResults;
                     aOrg       : PDBFile;
                     aProbTxt   : PChar;
                     aPosFlags    : PPanelBitmap;
                     aSelectedOrg : PSelectedOrg);
    destructor Done; virtual;
    procedure SetupWindow; virtual;
    procedure ShowBiochems;
    procedure ShowOrgs;
    procedure EditCurrentBiochem; virtual;
    procedure HandleBioChemList(var msg: Tmessage); virtual ID_FIRST + IDC_BIOCHEMLIST;
    procedure WMCommand(var msg: TMessage); virtual WM_FIRST + WM_COMMAND;
    procedure Cancel(var msg: TMessage); virtual ID_FIRST + ID_CANCEL;
    function  CanClose: boolean; virtual;
  end;


function AllSugarsNeg(posFlags: PPanelBitmap): boolean;
begin
  AllSugarsNeg := false;
  if TestBit(posFlags^, GluWellNum) or
     TestBit(posFlags^, SucWellNum) or
     TestBit(posFlags^, SorWellNum) then
    exit;
  AllSugarsNeg := true;
end;

{ ----------------------------------------------- Private procedures ---}

{------------------------------------------------------------------------}
{                                                                        }
{------------------------------------------------------------------------}
function CheckOrg(orgNum, orgGroupNum : integer) : boolean;
var
  orgObj : POrgGrpObj;
begin
  orgObj := New(POrgGrpObj, Init);
  if orgObj <> nil then
  begin
    CheckOrg:= OrgObj^.OrgInGroup(orgNum, orgGroupNum);
    MSDisposeObj(orgObj);
  end;
end;

{------------------------------------------------------------------------}
{                                                                        }
{------------------------------------------------------------------------}
procedure GetATest(aParent : pWindowsObject;
                   aResult : PResults;
                   testID  : PChar;
                   testCat : PChar;
                   required: boolean);
var
  ok    : boolean;
  rstr  : array[0..25] of char;
  pstr  : PChar;
  d     : PXtraTestDlg;
  dlgRes: integer;
begin
  ok:= false;
  repeat
    StrCopy(rStr,'');
    d:= New(PXtraTestDlg, Init(aParent, testID, rStr));
    dlgRes:= Application^.ExecDialog(d);
    if required and ((dlgRes = idCancel) or (StrComp(rStr, '') = 0)) then
    begin
      GetMem(pStr, 101);
      SR(IDS_TEST_REQUIRED, pstr, 100);
      ShowErrorStr(aParent^.HWindow, pstr, 0, 0, 0, 100);
      MSFreeMem(pStr, 101);
    end
    else
    begin
      ok := true;
      aResult^.SetResult(testID, testCat, rStr, true);
    end;
  until ok;
end;

{------------------------------------------------------------------------}
{                                                                        }
{------------------------------------------------------------------------}
function SaveExtraTests(aParent  : PWindowsObject;
                        prompt   : boolean;
                    var wellFile : Text;
                        testFile : PDBFile;
                        orgNum   : integer;
                        resList  : PResults;
                        posFlags : PPanelBitmap;
                        MICData  : PMICData;
                        overwrite: boolean): boolean;
var
  xtraCnt     : integer;
  i, res      : integer;
  wellNum     : integer;
  testID,
  testCat     : PChar;
  xtraTbl     : XtraType;
  numXTests   : integer;
  result      : array[0..16] of char;

begin
  SaveExtraTests := false;
  {$IFOPT I+}
    {$DEFINE chki}
  {$endIF}
  {$I-}

  Reset(wellFile);
  if IoResult = 0 then
  begin
    GetMem(testID, 33);
    GetMem(testCat, 33);
    if not ((orgNum = -1){IDToFollow} or
           (resList^.traysObj^.TrayFlo)) then
    begin
      resList^.traysObj^.GetXtra(xtraTbl, numXTests);
      for i := 1 to numXTests do
      begin
        res := GetETResult(i, TestID, xTraTbl, resList, orgNum,
                           wellFile, posFlags, MICData);
        testFile^.dbr^.ClearRecord;
        testFile^.dbr^.PutFieldAsStr(DBTSTID, testID);
        if testFile^.dbc^.GetFirstContains(testFile^.dbr) then
        begin
          testFile^.dbr^.GetFieldAsStr(DBTSTTstCat, testCat, 32);
          case res  of
            0 : { dont store };
            1 : { test is negative }
                begin
                  SaveExtraTests := ResList^.SetResult(testID, testCat, '-', overWrite);
                end;
            2 : { test is positive }
                begin
                  SaveExtraTests := ResList^.SetResult(testID, testCat, '+', overWrite);
                end;
            3 : { prompt }
                if prompt then
                begin
                  GetATest(aParent, reslist, testID, testCat, True);
                  SaveExtraTests := True;
                end;
          end;
        end;
      end;  {for i := 1 to numxtests}
    end;  {if not stored results to follow}
    MSFreeMem(testID, 33);
    MSFreeMem(testCat, 33);
  end;
  {$IFDEF chki}
    {$I+}
    {$UNDEF chki}
  {$endIF}
end;

{------------------------------------------------------------------------}
{                                                                        }
{------------------------------------------------------------------------}
function SaveMICs(resList  : PResults;
                  interp   : PInterp;
                  orgRef   : TSeqNum;
                  testFile : PDBFile;
                  micData  : PMICData;
                  overwrite: boolean): boolean;
var
  i        : integer;
  dName    : array[0..63] of char;
  res      : array[0..32] of char;
  mpcOK,
  allOK,
  orgOK    : boolean;  {- If orgOK gets set to false then no
                           interpretations will be calculated. This occurs
                           when org is "VeryRare" or doesnt exist }
begin
  mpcOK := FALSE;
  allOK := TRUE;
  orgOk := (orgRef <> 0);

  for i := 1 to resList^.TraysObj^.DrugCnt do
    with resList^.traysObj^.dilList[i] do
    begin
      StrLCopy(dName, drugName, sizeof(dName)-1);
      TrimTrail(dName, dName, ' ', sizeof(dName)-1);
      if (not overWrite) and (StrComp(dName, 'Mpc') = 0) and
          resList^.GetResult(dname, 'MIC', res, 32) and (res[0] <> #0) then
      begin
        if StrComp(res, drugDils[0]) = 0 then { ' N/R' }
        begin
          mpcOK := TRUE;
          allOK := resList^.SetResult(dname, 'MIC', drugDils[MICData^[i]], TRUE) and allOK
        end
        else
          allOK := FALSE;
      end
      else
        allOK := resList^.SetResult(dname, 'MIC', drugDils[MICData^[i]], overWrite) and allOK;
      if orgOK then
      begin
        resList^.GetResult(dname, 'MIC', res, 32);
        Interp^.SetupInterps(dname, res);
      end
      else
        Interp^.SetupInterps(dname, '');
    end;
  SaveMICs := allOK or mpcOK;
end;

{-------------------------------------------------------------------------}
{-------------------------------------------------------------------------}
function GetMissingResults(aParent  : PWindowsObject;
                           resList  : PResults;
                           posFlags,
                           chkFlags : PPanelBitmap;
                           bioObj   : PBiotypeObject): boolean;
var i        : integer;
    result   : boolean;
    gotResult: boolean;
    chkOxi,
    chkInd   : boolean;
    setclass : integer;
begin
  GetMissingResults := true;
  chkOxi := false;
  chkInd := false;
  for i := 0 to bioObj^.MaxBitNum do
  begin
    if TestBioBit(bioObj^.neededResults, i) then
      if (i = bioObj^.oxidaseBitNum) then
        chkOxi := true
      else
        if (i = bioObj^.indolebitNum) then
          chkInd := true
        else
        begin
          gotResult := PromptForBiochemResult(aParent,
                                              bioObj,
                                              i,
                                              posFlags,
                                              ChkFlags,
                                              result);
          if not gotResult then
          begin
            GetMissingResults := false;
            exit;
          end;
        end;
  end;
  SetClass:= bioObj^.tfsRec^.PSet * 10 + bioObj^.Class;
  if chkOxi or chkInd then
    if bioObj^.Identify^.Identify(bioObj^.Biotype, bioObj^.BioMask, SetClass) Then
    begin
      if ChkOxi then
      begin
        gotResult := PromptForBiochemResult(aparent,
                                            bioObj,
                                            bioObj^.oxidaseBitNum,
                                            posFlags,
                                            ChkFlags,
                                            result);
        if gotResult then
          if TestBioBit(bioObj^.biotype, bioObj^.oxidaseBitNum)then
            resList^.SetResult('.OXI', 'OXI', '+', true)
          else
            resList^.SetResult('.OXI', 'OXI', '-', true);
      end;
      if ChkInd then
      begin
        gotResult := PromptForBiochemResult(aparent,
                                            bioObj,
                                            bioObj^.indoleBitNum,
                                            posFlags,
                                            ChkFlags,
                                            result);
        if gotResult then
          if TestBioBit(bioObj^.biotype, bioObj^.indoleBitNum)then
            resList^.SetResult('.IND', 'IND', '+', true)
          else
            resList^.SetResult('.IND', 'IND', '-', true);
      end;
      GetMissingResults := gotResult;
    end;
end;

{------------------------------------------------------------------------}
{                                                                        }
{------------------------------------------------------------------------}
procedure DoSetUpInterps(isoDB    : PDBFile;
                         resList  : PResults;
                         interp   : PInterp;
                         orgRef   : TSeqNum);
var
  i        : integer;
  dName    : array[0..63] of char;
  res      : array[0..32] of char;
  results  : PResults;
  orgOK    : boolean;  {- If orgOK gets set to false then no
                           interpretations will be calculated. This occurs
                           when org is "VeryRare" or doesnt exist }
begin
  results := New(PResults, Init);
  results^.traysObj^.LoadTrayByNum(resList^.traysObj^.tfsRec.traynum);
  results^.LoadResults(isoDB^.dbr);
  orgOk := (orgRef <> 0);

  for i := 1 to results^.TraysObj^.DrugCnt do
    with results^.traysObj^.dilList[i] do
    begin
      StrLCopy(dName, drugName, sizeof(dName)-1);
      TrimTrail(dName, dName, ' ', sizeof(dName)-1);
      if orgOK then
      begin
        Results^.GetResult(dname, 'MIC', res, 32);
        Interp^.SetupInterps(dname, res);
      end
      else
        Interp^.SetupInterps(dname, '');
    end;
  MSDisposeObj(results);
end;

{-------------------------------------------------------[ TBiochemDialog ]--}

constructor TBiochemDialog.Init(aParent     : PWindowsObject;
                                aBiochemName: PChar;
                                aPosFlags   : PPanelBitmap;
                                aWellNum    : integer);
begin
  if not inherited Init(AParent, MakeIntResource(DLG_MANUALCALL)) then
  begin
    Done;
    Fail;
  end;
  BiochemName:= aBiochemName;
  posFlags:= aPosFLags;
  WellNum:= aWellNum;
end;

procedure TBiochemDialog.SetupWindow;
begin
  inherited SetupWindow;
  SetDlgItemText(HWindow, IDC_PROMPTTEST, BiochemName);
  SendDlgItemMsg(IDC_NEGATIVE, BM_SETCHECK, BF_CHECKED, 0);
  SendDlgItemMsg(IDC_POSITIVE, BM_SETCHECK, BF_UNCHECKED, 0);
end;

{----------------------------------------------------------------------------}
{----------------------------------------------------------------------------}
function TBiochemDialog.CanClose: boolean;
begin
  CanClose := false;
  if inherited CanClose then
  begin
    if SendDlgItemMsg(IDC_POSITIVE, BM_GETCHECK, 0, 0) = BF_CHECKED then
      SetBit(posFlags^, WellNum)
    else
      if SendDlgItemMsg(IDC_NEGATIVE, BM_GETCHECK, 0, 0) = BF_CHECKED then
        ClearBit(posFlags^, WellNum)
      else
      begin
        CanClose := false;
        exit;
      end;
    CanClose := true;
  end;
end;

{-------------------------------------------------------[ TXtraTestDlg ]--}

constructor TXtraTestDlg.Init(aParent   : PWindowsObject;
                              aTestName : PChar;
                              aRes      : PChar);
begin
  if not inherited Init(AParent, MakeIntResource(DLG_MANUALCALL)) then
  begin
    Done;
    Fail;
  end;
  testName:= ATestName;
  res:= ARes;
end;

procedure TXtraTestDlg.SetupWindow;
begin
  inherited SetupWindow;
  SetDlgItemText(HWindow, IDC_PROMPTTEST, TestName);
  SendDlgItemMsg(IDC_NEGATIVE, BM_SETCHECK, BF_UNCHECKED, 0);
  SendDlgItemMsg(IDC_POSITIVE, BM_SETCHECK, BF_UNCHECKED, 0);
end;

{----------------------------------------------------------------------------}
{----------------------------------------------------------------------------}
function TXtraTestDlg.CanClose : boolean;
begin
  CanClose := false;
  if inherited CanClose then
  begin
    if (SendDlgItemMsg(IDC_POSITIVE, BM_GETCHECK, 0, 0) = BF_UNCHECKED) and
       (SendDlgItemMsg(IDC_NEGATIVE, BM_GETCHECK, 0, 0) = BF_UNCHECKED) then
      StrPCopy(res, '')
    else
      if SendDlgItemMsg(IDC_POSITIVE, BM_GETCHECK, 0, 0) = BF_CHECKED Then
        StrPCopy(res, '+')
      else
        StrPCopy(res, '-');
    CanClose := true;
  end;
end;

{-------------------------------------------------------[ TSpaceList ]--}

procedure TSpaceList.wmChar(var msg: TMessage);
var
  id : integer;
begin
  if msg.wparam = vk_space then
  begin
    id := GetDlgCtrlID(hWindow);
    msg.lParamHI:= LBN_DBLCLK;
    msg.lParamLO:= hWindow;
    SendMessage(GetParent(hWindow), WM_FIRST+WM_COMMAND, id, msg.lParam);
  end;
end;

procedure TSpaceList.wmKillFocus(var msg: TMessage);
begin
  SetSelIndex(-1);
  DefWndProc(msg);
end;

{----------------------------------------------------------[ TBioDlg ]--}

constructor TBioDlg.Init(aParent    : PWindowsobject;
                         aBiotypeObj: PBiotypeobject;
                         results    : PResults;
                         aOrg       : PDBFile;
                         aProbTxt   : PChar;
                         aPosFlags    : PPanelBitmap;
                         aSelectedOrg : PSelectedOrg);
begin
  if not inherited Init(aParent, MakeIntResource(DLG_BIOTYPEEDIT)) then
  begin
    Done;
    Fail;
  end;
  orgDB := aOrg;
  probTxt:= aProbTxt;
  posFlags := aPosFlags;
  bioObj:= aBiotypeObj;
  ready:= false;
  resList := results;
  saveBioStr:= bioObj^.biotype;         {- save data from biotype so it can }
  saveBioMask:= bioObj^.biomask;        {- be restored if canceled }
  saveNeeded:= bioObj^.neededresults;
  saveClass:= bioObj^.Class;
  Move(posFlags^, saveFlags, 12);
  resList^.GetResult('.OXI', 'OXI', saveOxiRes, 20);
  resList^.GetResult('.IND', 'IND', saveIndRes, 20);
  biochemList:= New(PSpaceList, InitResource(@self, IDC_BIOCHEMLIST));
  orgList:= New(PListBox, InitResource(@self, IDC_ORGLIST));
  orgNums:= New(PlistBox, InitResource(@Self, IDC_ORGNUMLIST));
  orgProbs:= New(PListBox, InitResource(@Self, IDC_ORGPROBLIST));
  AddTest1:= New(PListBox, InitResource(@Self, IDC_AddTest1));
  AddTest2:= New(PListBox, InitResource(@Self, IDC_AddTest2));
  AddTest3:= New(PListBox, InitResource(@Self, IDC_AddTest3));
  SelectedOrg:= aSelectedOrg;
  SelectedOrg^:= 0; {Point to first org in OrgList}
end;

destructor TBioDlg.Done;
begin
  inherited Done;
end;

procedure TBioDlg.SetupWindow;
var
  k     : integer;
  r     : TRect;
  font  : HFont;
  pStr  : PChar;
  negPanel : boolean;

{- CANNOT HAVE A SET/CLASS of 202 RapidNeg Non Fermenter }

begin
  inherited SetupWindow;
  for k:= 1 to numSC do
    EnableWindow(GetDlgItem(hWindow, ID_SC[k]), false);

  GetClientRect(biochemList^.hWindow, r);
  SendDlgItemMsg(IDC_BIOCHEMLIST, LB_SETCOLUMNWIDTH,
                 word((r.right - r.left) div 4), 0);
  font:= GetStockobject(SYSTEM_FIXED_FONT);
  SendDlgItemMsg(IDC_BIOCHEMLIST, WM_SETFONT, font, 0);

  SetClass:= bioObj^.tfsRec^.PSet * 10 + bioObj^.Class;

  if SetClass = 0 then
    SetClass:= 201;

  if (SetClass mod 10) = 0 then
    SetClass:= SetClass + 1;

  {- enable proper classes on screen }
  For k:= 1 To NumSC Do
  begin
    if (SC[k] div 10) = (SetClass div 10) then
    begin
      if ID_SC[k] <> IDC_NONFERMENTER then  {nonfermenter showed never be selected by the user }
        EnableWindow(GetDlgItem(hWindow, ID_SC[k]), true);
      if SetClass = SC[k] then
      begin
        SetFocus(GetDlgItem(hWindow, ID_SC[k]));
        SendDlgItemMsg(ID_SC[k], BM_SETCHECK, BF_CHECKED, 0);
      end;
    end;
  end;

  GetMem(pStr, 100);
  StrPCopy(pStr, bioObj^.biotype);
  SetDlgItemText(hWindow, IDC_BIOTYPE, pStr);
  MSFreeMem(pStr, 100);
  ready:= true;
  ShowBiochems;
  ShowOrgs;

  EnableWindow(GetDlgItem(hWindow, IDC_NONFERMENTER), false);
  if (resList^.traysObj^.tfsRec.tSet = 1) then   {neg conv panel}
  begin
    if resList^.traysObj^.TrayFlo then
      EnableWindow(GetDlgItem(hWindow, IDC_FERMENTER), false)
    else
    begin
      EnableWindow(GetDlgItem(hWindow, IDC_NONFERMENTER), true);
      EnableWindow(GetDlgItem(hWindow, IDC_FERMENTER), true);
    end;
    EnableWindow(GetDlgItem(hWindow, IDC_STREP), false);
    EnableWindow(GetDlgItem(hWindow, IDC_MICRO), false);
  end
  else if (resList^.traysObj^.tfsRec.tSet = 2) and  {pos conv panel}
          (StrComp(resList^.traysObj^.mapBat^.batIDList[resList^.traysObj^.tfsRec.trayNum],
                   'RPID') <> 0) then
  begin
    EnableWindow(GetDlgItem(hWindow, IDC_FERMENTER), false);
    EnableWindow(GetDlgItem(hWindow, IDC_STREP), true);
    EnableWindow(GetDlgItem(hWindow, IDC_MICRO), true);
  end
  else
  begin
    EnableWindow(GetDlgItem(hWindow, IDC_FERMENTER), false);
    EnableWindow(GetDlgItem(hWindow, IDC_STREP), false);
    EnableWindow(GetDlgItem(hWindow, IDC_MICRO), false);
  end;
end;

function TBioDlg.CanClose: boolean;
{-------------------------------------------------------------------------}
{ extract selected organism and return in OrgID parameter passed at INIT  }
{-------------------------------------------------------------------------}
var
  k       : integer;
  tempOrg : array[0..63] of char;
  OrgID   : array[0..63] of char;
begin
  CanClose:= false;
  if inherited CanClose then
  begin
    CanClose := true;
    k:= orgList^.GetSelIndex;
    if k >= 0 then
    begin
      if orgNums^.GetCount = 0 then
      begin
        SR(IDS_VRBIOT, orgID, sizeof(orgID)-1);   {- very rare biotype }
        StrCopy(probTxt, '');
      end
      else
      begin
        orgProbs^.GetString(probTxt, k);
        StrCat(probTxt, '%');
        orgNums^.GetString(orgID, k);
        TrimLead(tempOrg, orgID, ' ', sizeof(tempOrg)-1);
        TrimTrail(orgID, tempOrg, ' ', sizeof(orgID)-1);
        orgDB^.dbr^.PutFieldAsStr(DBORGOrg, orgID);
        orgDB^.dbc^.GetEQ(orgDB^.dbr);
        SelectedOrg^:= k;
      end;
    end;
  end;
end;

procedure TBioDlg.Cancel(var msg: TMessage);
{-------------------------------------------------------------------------}
{ restore the biotype object to its original state on Init                }
{-------------------------------------------------------------------------}
begin
  bioObj^.biotype:= saveBioStr;
  bioObj^.bioMask:= saveBioMask;
  bioObj^.Class:= saveClass;
  Move(saveFlags, posFlags^, 12);
  bioObj^.neededresults := saveNeeded;
  resList^.SetResult('.OXI', 'OXI', saveOxiRes, TRUE);
  resList^.SetResult('.IND', 'IND', saveIndRes, TRUE);
  bioObj^.RecomputeBiotype;
  inherited Cancel(msg);
end;

procedure TBioDlg.ShowBioChems;
var
  i    : integer;
  j, k,
  group: integer;
  pStr : PChar;
begin
  GetMem(pStr, 256);
  SendDlgItemMsg(IDC_BIOCHEMLIST, WM_SETREDRAW, 0, 0);
  bioChemList^.ClearList;
  for i := bioObj^.MaxBitNum downTo 0 do
  begin
    StrCopy(pStr, '***');
    if bioObj^.BitInfo(i, j, k, group, pStr) then
    begin
      Pad(pStr, pStr, ' ', 6);
      case j of
        -1 :
          begin
            if bioObj^.tfsRec^.pSet = 1 then
              resList^.GetResult('.OXI', 'OXI', @pStr[6], 20)
            else if bioObj^.tfsRec^.pSet = 3 then
              resList^.GetResult('.IND', 'IND', @pStr[6], 20)
            else
              StrCat(pStr, ' ');
          end;
         0 : StrCat(pStr, '-');
         1 : StrCat(pStr, '+');
      end;
    end;
    BiochemList^.Addstring(pStr);
  end;
  bioChemList^.SetSelIndex(0);
  SendDlgItemMsg(IDC_BIOCHEMLIST, WM_SETREDRAW, 1, 0);
  InvalidateRect(biochemList^.hWindow, nil, true);
  MSFreeMem(pStr, 256);
end;

procedure TBioDlg.HandleBioChemList(var msg: TMessage);
begin
  if msg.LParamHi = LBN_DBLCLK then
    EditCurrentBiochem;
end;

procedure TBioDlg.EditCurrentBiochem;
var
  curIdx, j: integer;
  pStr     : PChar;
  bitNum,
  id       : integer;
  sugar    : boolean;
  msg      : TMessage;
begin
  SetFocus(biochemList^.hWindow);
  if biochemList^.GetSelIndex = -1 then
  begin
    {- focus current item }
    bioChemList^.SetSelIndex(0);
    exit;
  end;
  curIdx:= biochemList^.GetSelIndex;
  if curIdx >= 0 then
  begin
    GetMem(pStr, 256);
    bitNum := BioObj^.MaxbitNum - curIdx;
    if EditBiochem(@self, bioObj, bitNum, resList, pStr, posFlags, sugar) then
    begin
      if sugar and AllSugarsNeg(posFlags) then
      begin
        msg.lParamHI:= BN_CLICKED;
        msg.lParamLO:= hWindow;
        SendDlgItemMsg(IDC_FERMENTER, BM_SETCHECK, BF_UNCHECKED, 0);
        SendDlgItemMsg(IDC_NONFERMENTER, BM_SETCHECK, BF_CHECKED, 0);
        SendMessage(hWindow, WM_FIRST+WM_COMMAND, IDC_NONFERMENTER, msg.lParam);
      end
      else
      begin
        GetBiotype(@self, TRUE, bioObj^.class, resList,
                 posFlags, bioObj^.chkflags, bioObj^.virtualTests,
                 bioObj^.micData, bioObj);
        ShowBiochems;
        StrPCopy(pStr, bioObj^.biotype);
        SetDlgItemText(hWindow, IDC_BIOTYPE, pStr);
      end;
    end;
    MSFreeMem(pStr, 256);
    biochemList^.SetSelIndex(curIdx);
  end;
  ShowOrgs;
end;

procedure TBioDlg.ShowOrgs;
var
  IDLine          : PChar;
  IDLineStr       : string;
  i               : integer;
  ADTOrgList      : TOrgList;
  AddTestList     : TAddTestList; { Additional tests for separation if identification }
  OrgCnt          : Integer;
  AddTestInfo     : string;
begin
  GetMem(IDLine, 256);

  orgList^.ClearList;
  orgNums^.ClearList;
  orgProbs^.ClearList;
  AddTest1^.ClearList;
  AddTest2^.ClearList;
  AddTest3^.ClearList;
  SetDlgItemText(hWindow, IDC_TitleAddTest1, nil);
  SetDlgItemText(hWindow, IDC_TitleAddTest2, nil);
  SetDlgItemText(hWindow, IDC_TitleAddTest3, nil);

  SendDlgItemMsg(IDC_ORGNUMLIST, WM_SETREDRAW, 0, 0);
  SendDlgItemMsg(IDC_ORGLIST, WM_SETREDRAW, 0, 0);
  SendDlgItemMsg(IDC_ORGPROBLIST, WM_SETREDRAW, 0, 0);
  SendDlgItemMsg(IDC_AddTest1, WM_SETREDRAW, 0, 0);
  SendDlgItemMsg(IDC_AddTest2, WM_SETREDRAW, 0, 0);
  SendDlgItemMsg(IDC_AddTest3, WM_SETREDRAW, 0, 0);

  For i:= 1 To NumSC Do
    if SendDlgItemMsg(ID_SC[i], BM_GETCHECK, 0, 0) <> 0 then
      Case i of
        PID1Strep : If (SetClass = SC[PID1Staph]) then SetClass := SC[PID1Strep];
        PID1Staph : If (SetClass = SC[PID1Strep]) then SetClass := SC[PID1Staph];
        PID2Strep : If (SetClass = SC[PID2Staph]) then SetClass := SC[PID2Strep];
        PID2Staph : If (SetClass = SC[PID2Strep]) then SetClass := SC[PID2Staph];
        Else
          setClass:= SC[i];
      End;
  if not bioObj^.Identify^.Identify(bioObj^.biotype, bioObj^.bioMask, SetClass) then
    SR(IDS_UNDETBIOT, IDLine, 64) {- undetermined biotype}
  else
  begin
    SR(IDS_VRBIOT, IDLine, 64);   {- very rare biotype }
    if bioObj^.Identify^.InvalidBiotype then
    begin
      bioObj^.Identify^.IDList.NumOrgs:= 0;
      SR(IDS_INVBIOT, IDLine, 64); {- invalid biotype }
    end;
  end;

  if bioObj^.Identify^.IDList.NumOrgs <= 0 then
    orgList^.Addstring(idLine)
  else
  begin
    with bioObj^.Identify^.IDList do
    begin
      for i:= 1 to NumOrgs do
      begin
        STR(ReportOrgs.P^[i]:3, IDLineStr);
        StrPCopy(IDLine, IDLineStr);
        orgNums^.Addstring(idLine);
        StrPCopy(IDLine, TOrgNames(Orgobject.OrgNames.P^[ ReportOrgs.P^[i] ]^).Name);
        orgList^.Addstring(idLine);
        STR(Probs.P^[i]:4:1, IDLineStr);
        if IDLineStr = '100.0' then
          IDLineStr:= '99.9';
        StrPCopy(IDLine, IDLineStr);
        orgProbs^.Addstring(IDLine);
        ADTOrgList[i] := ReportOrgs.P^[i];
      end;
      If (PROBS.P^[1] <= 85) then
      Begin
        OrgObject.ReportAddTests(True,NumOrgs,1,ADTOrgList,AddTestList);
        For OrgCnt := 1 to MaxOrgList do
        Begin
          AddTestInfo := Concat(AddTestList.Prob[OrgCnt,1],chr(0));
          AddTest1^.AddString(Addr(AddTestInfo[1]));
          AddTestInfo := Concat(AddTestList.Prob[OrgCnt,2],chr(0));
          AddTest2^.AddString(Addr(AddTestInfo[1]));
          AddTestInfo := Concat(AddTestList.Prob[OrgCnt,3],chr(0));
          AddTest3^.AddString(Addr(AddTestInfo[1]));
        End;
        AddTestInfo := Concat(AddTestList.Title[1],chr(0));
        SetDlgItemText(hWindow, IDC_TitleAddTest1, Addr(AddTestInfo[1]));
        AddTestInfo := Concat(AddTestList.Title[2],chr(0));
        SetDlgItemText(hWindow, IDC_TitleAddTest2, Addr(AddTestInfo[1]));
        AddTestInfo := Concat(AddTestList.Title[3],chr(0));
        SetDlgItemText(hWindow, IDC_TitleAddTest3, Addr(AddTestInfo[1]));
      End;
    end;
  end;
  SendDlgItemMsg(IDC_ORGLIST, WM_SETREDRAW, 1, 0);
  SendDlgItemMsg(IDC_ORGNUMLIST, WM_SETREDRAW, 1, 0);
  SendDlgItemMsg(IDC_ORGPROBLIST, WM_SETREDRAW, 1, 0);
  SendDlgItemMsg(IDC_AddTest1, WM_SETREDRAW, 1, 0);
  SendDlgItemMsg(IDC_AddTest2, WM_SETREDRAW, 1, 0);
  SendDlgItemMsg(IDC_AddTest3, WM_SETREDRAW, 1, 0);

  InvalidateRect(orgList^.hWindow, nil, true);
  InvalidateRect(orgNums^.hWindow, nil, true);
  InvalidateRect(orgProbs^.hWindow, nil, true);
  InvalidateRect(AddTest1^.hWindow, nil, true);
  InvalidateRect(AddTest2^.hWindow, nil, true);
  InvalidateRect(AddTest3^.hWindow, nil, true);

  orgList^.SetSelIndex(0);
  MSFreeMem(IDLine, 256);
end;

procedure TBioDlg.WMCommand(var Msg: TMessage);
var
  id   : word;
  k    : integer;
  wait : PWaitCursor;
  pStr : array[0..50] of Char;
begin
  if (msg.LParamLo <> 0) and (msg.lParamHi <> 1) then
  begin
    if (msg.lParamHi = BN_CLICKED) and ready then
    begin
      id:= msg.wparam;
      for k := 1 to numSC do
      begin
        if (id = ID_SC[k]) then
        begin
          wait:= New(PWaitCursor, Init);
          Case id of
            IDC_Strep :
              If SetClass = SC[PID1Staph] then SetClass := SC[PID1Strep]
              Else
                If SetClass = SC[PID2Staph] then SetClass := SC[PID2Strep];
            IDC_Micro :
              If SetClass = SC[PID1Strep] then SetClass := SC[PID1Staph]
              Else
                If SetClass = SC[PID2Strep] then SetClass := SC[PID2Staph];
            Else
              setClass:= SC[k];
          End;
          bioObj^.SetBioClass(setClass mod 10);
          if id = IDC_FERMENTER then
          begin
            { set Glu to Positive }
            SetBit(posFlags^, GluWellNum);
            SendDlgItemMsg(IDC_NONFERMENTER, BM_SETCHECK, BF_UNCHECKED, 0);
          end;

          GetBiotype(@self, TRUE, bioObj^.class, resList,
                     posFlags, bioObj^.chkflags, bioObj^.virtualTests,
                     bioObj^.micData, bioObj);

          StrPCopy(pStr, bioObj^.biotype);
          SetDlgItemText(hWindow, IDC_BIOTYPE, pStr);

          ShowBiochems;
          ShowOrgs;

          MSDisposeObj(wait);
        end;
      end;
    end;
  end;
  inherited WMCommand(msg);
end;


{---------------------------------------------------------[ TSaveDlg ]--}

constructor TSaveDlg.Init(aParent   : PWindowsobject;
                         var aSaveID,
                         aSaveMIC   : boolean;
                         aIDOnly,
                         aMICOnly,
                         aPrevStored: boolean);
begin
  inherited Init(aParent, MakeIntResource(DLG_AS4SAVE));
  saveMIC:= @aSaveMic;
  saveID:= @aSaveID;
  idOnly:= aIDOnly;
  MICOnly:= aMICOnly;
  prevStor:= aPrevStored;
end;

procedure TSaveDlg.SetupWindow;
var
  pStr     : PChar;
begin
  inherited SetupWindow;

  GetMem(pStr, 151);
  {- if id only panel then disable MIC checkbox, otherwise use value passed in. }
  if idOnly then
    EnableWindow(GetDlgItem(hWindow, IDC_SAVEMIC), false)
  else
    SendDlgItemMsg(IDC_SAVEMIC, BM_SETCHECK, word(saveMIC^), 0);

  SendDlgItemMsg(IDC_SAVEID, BM_SETCHECK, word(saveID^), 0);

  if prevStor then
    SR(IDS_DATASTORED, pStr, 150)
  else
    StrCopy(pStr, '');
  SetDlgItemText(hWindow, IDC_SAVE_STATUS, pStr);
  MSFreeMem(pStr, 151);
end;

procedure TSaveDlg.EnableButtons;
begin
  if (SendDlgItemMsg(IDC_SAVEMIC, BM_GETCHECK, 0, 0) = BF_CHECKED) or
     (SendDlgItemMsg(IDC_SAVEID, BM_GETCHECK, 0, 0) = BF_CHECKED) then
    EnableWindow(GetItemHandle(ID_OK), true)
  else
    EnableWindow(GetItemHandle(ID_OK), false);
end;

procedure TSaveDlg.WMCommand(var msg: TMessage);
begin
  inherited WMCommand(msg);
  EnableButtons;
end;

function TSaveDlg.CanClose: boolean;
begin
  CanClose:= false;
  if inherited CanClose then
  begin
    CanClose := true;
    saveMIC^:= SendDlgItemMsg(IDC_SAVEMIC, BM_GETCHECK, 0,0) <> 0;
    saveID^:= SendDlgItemMsg(IDC_SAVEID, BM_GETCHECK, 0,0) <> 0;
  end;
end;

{---------------------------------------------------------[ TSearchDlg ]--}

constructor TSearchDlg.Init(aParent    : PWindowsobject;
                            dbSpecFile,
                            dbIsoFile,
                            tstGrpFile : PDBFile;
                        var isoOrdFile : PDBAssocFile);
begin
  tstList := nil;
  listObj := nil;
  if not inherited Init(aParent, MakeIntResource(DLG_SEARCH)) then
  begin
    Done;
    Fail;
  end;
  dbSpec := dbSpecFile;
  dbIso := dbIsoFile;
  dbTstGrp := tstGrpFile;
  dbIsoOrd := isoOrdFile;
  pdbIsoOrd := @isoOrdFile;

  listObj := New(PListObject, Init);
  sFld := New(PUIFld, Init(@self, dbSpec^.dbd, listObj, TRUE, DBSPECSpecID, IDC_SEARCHSPECEDIT, IDC_SEARCHSPECLBL));
  cFld := New(PUIFld, Init(@self, dbSpec^.dbd, listObj, TRUE, DBSPECCollectDate, IDC_SEARCHCDEDIT, IDC_SEARCHCDLBL));
  iFld := New(PUIFld, Init(@self, dbIso^.dbd,  listObj, TRUE, DBISOIso, IDC_SEARCHISOEDIT, IDC_SEARCHISOLBL));
  tstList := New(PCollection, Init(2,2));
  tstList^.Insert(New(PListEditItem, Init('XXXXXXXX')));   (* ??? *)
  oFld := New(PUIListFld, Init(@self, dbTstGrp^.dbd, TRUE, DBTSTGRPID,
              IDC_SEARCHTGEDIT, IDC_SEARCHTGLBL, SR(IDS_TEST_GROUP, pstr,
              sizeof(pstr)-1), nil, tstList));
end;

destructor TSearchDlg.Done;
begin
  pdbIsoOrd^ := dbIsoOrd;
  MSDisposeObj(tstList);
  MSDisposeObj(listObj);
  inherited Done;
end;

procedure TSearchDlg.SetupWindow;
begin
  inherited SetupWindow;
  iFld^.Enable(TRUE);
  oFld^.Enable(TRUE);
  EnableButtons;
end;

procedure TSearchDlg.DoSearch(var msg: TMessage);
var
  seq    : TSeqNum;
  udFlag : boolean;
  tw     : TWaitCursor;
  err    : integer;
begin
  if not oFld^.IsEmpty and not iFld^.IsEmpty and
     not sFld^.IsEmpty and not cFld^.IsEmpty then
  begin
    tw.Init;
    err := sFld^.Format;
    err := cFld^.Format;
    err := iFld^.Format;
    err := oFld^.Format;
    sFld^.XferCtlToRec(dbSpec^.dbr);
    cFld^.XferCtlToRec(dbSpec^.dbr);
    iFld^.XferCtlToRec(dbIso^.dbr);
    oFld^.XferCtlToRec(dbTstGrp^.dbr);
    if dbSpec^.dbc^.GetEQ(dbSpec^.dbr) then
    begin
      seq := dbSpec^.dbr^.GetSeqValue;
      dbIso^.dbr^.PutField(DBISOSpecRef, @seq);
      dbSpec^.dbr^.GetField(DBSPECSpecID, @specID, 16);
      dbSpec^.dbr^.GetField(DBSPECCollectDate, @cDate, 16);
      dbIso^.dbr^.PutField(DBIsoSpecID, @specID);
      dbIso^.dbr^.PutField(DBIsoCollectDate, @cDate);
      if dbIso^.dbc^.GetEQ(dbIso^.dbr) then
      begin
        if dbTstGrp^.dbc^.GetEQ(dbTstGrp^.dbr) then
        begin
          dbTstGrp^.dbr^.GetField(DBTSTGRPUDFlag, @udFlag, SizeOf(boolean));
          MSDisposeObj(dbIsoOrd);
          dbIsoOrd := New(PDBAssocFile, Init(dbIso^.dbr, DBISOLATE_ISORDER_ASSOC, dbOpenNormal));
          seq := dbIso^.dbr^.GetSeqValue;
          dbIsoOrd^.dbr^.PutField(DBISOORDIsoRef, @seq);
          seq := dbTstGrp^.dbr^.GetSeqValue;
          dbIsoOrd^.dbr^.PutField(DBISOORDTstGrpRef, @seq);
          if not udFlag and dbIsoOrd^.dbc^.GetEQ(dbIsoOrd^.dbr) then
          begin
            tw.Done;
            endDlg(IDOK);
          end
          else
            InfoMsg(hWindow, '', SR(IDS_PANEL_NOT_FOUND, pstr, 100));
        end
        else
          InfoMsg(hWindow, '', SR(IDS_PANEL_NOT_FOUND, pstr, 100));
      end
      else
        InfoMsg(hWindow, '', SR(IDS_ISO_NOT_FOUND, pstr, 100));
    end
    else
      InfoMsg(hWindow, '', SR(IDS_SPEC_NOT_FOUND, pstr, 100));
    tw.Done;
  end;
end;

procedure TSearchDlg.DoClear(var msg: TMessage);
begin
  sFld^.Clear;
  cFld^.Clear;
  iFld^.Clear;
  oFld^.Clear;
  oFld^.Clear;
  EnableButtons;
end;

procedure TSearchDlg.WMLostFocus(var msg: TMessage);
var
  fld     : PUIFld;
  fldID   : integer;
  err     : integer;
  setClass: word;
  seq     : TSeqNum;
  udFlag  : boolean;

  procedure PrepIso;
  begin
    if not iFld^.IsEmpty then
    begin
      seq := dbSpec^.dbr^.GetSeqValue;
      dbIso^.dbr^.PutField(DBISOSpecRef, @seq);
      dbSpec^.dbr^.GetField(DBSPECSpecID, @specID, 16);
      dbSpec^.dbr^.GetField(DBSPECCollectDate, @cDate, 16);
      dbIso^.dbr^.PutField(DBIsoSpecID, @specID);
      dbIso^.dbr^.PutField(DBIsoCollectDate, @cDate);
      iFld^.XferCtlToRec(dbIso^.dbr);
      if dbIso^.dbc^.GetEQ(dbIso^.dbr) then
      begin
        tstList^.FreeAll;
        MSDisposeObj(dbIsoOrd);
        dbIsoOrd := New(PDBAssocFile, Init(dbIso^.dbr, DBISOLATE_ISORDER_ASSOC, dbOpenNormal));
        if (dbIsoOrd <> nil) and dbIsoOrd^.dbc^.GetFirst(dbIsoOrd^.dbr) then
        begin
          repeat
            dbIsoOrd^.dbr^.GetField(DBISOORDTstGrpRef, @seq, SizeOf(seq));
            dbTstGrp^.dbc^.GetSeq(dbTstGrp^.dbr, seq);
            dbTstGrp^.dbr^.GetField(DBTSTGRPUDFlag, @udFlag, SizeOf(boolean));
            if not udFlag then
            begin
              dbTstGrp^.dbr^.GetFieldAsStr(DBTSTGRPID, pstr, 10);
              tstList^.Insert(New(PListEditItem, Init(pstr)));
            end;
          until not dbIsoOrd^.dbc^.GetNext(dbIsoOrd^.dbr);
          if oFld^.IsValid <> 0 then
          begin
            oFld^.Clear;
            if tstList^.count = 1 then
              SetDlgItemText(hWindow, IDC_SEARCHTGEDIT, pstr);
          end;
        end;
      end
      else
      begin
        oFld^.Clear;
      end;
    end
    else
    begin
      oFld^.Clear;
    end;
  end;

begin
  uiFldsProcessFocus := FALSE;
  fld:= PUIFld(msg.lParam); {- get field pointer to fld losing focus }
  fldID := msg.wParam;      {- get field ID to fld getting focus }
  {- first see if the field is valid }
  err:= fld^.IsValid;
  if (err <> 0) and (err <> UIErrRequired) then   {- ignore required fields on field exit }
    ShowUIFldError(@self, err, fld)
  else
  begin
    if not fld^.IsEmpty then
      fld^.Format;
    if (fld = sFld) or (fld = cFld) then
    begin
      if not sFld^.IsEmpty and not cFld^.IsEmpty then
      begin
        sFld^.XferCtlToRec(dbSpec^.dbr);
        cFld^.XferCtlToRec(dbSpec^.dbr);
        if dbSpec^.dbc^.GetEQ(dbSpec^.dbr) then
        begin
          PrepIso;
        end
        else
        begin
          iFld^.Clear;
          oFld^.Clear;
          if fld = sFld then
          begin
            if fldID <> IDC_SEARCHCDEDIT then
              cFld^.FocusFld;
          end
          else
          begin
            if fldID <> IDC_SEARCHSPECEDIT then
              sFld^.FocusFld;
          end;
        end;
      end
      else
      begin
        iFld^.Clear;
        oFld^.Clear;
        if fld = sFld then
        begin
          if fldID <> IDC_SEARCHCDEDIT then
            cFld^.FocusFld;
        end
        else
        begin
          if fldID <> IDC_SEARCHSPECEDIT then
            sFld^.FocusFld;
        end;
      end;
    end
    else if fld = iFld then
      PrepIso;
  end;
  uiFldsProcessFocus := TRUE;
  EnableButtons;
end;

procedure TSearchDlg.WMFldList(var msg: TMessage);
{- called when user selects LIST in a field }
var
  id    : word;
  seq   : TSeqNum;
begin
  uiFldsProcessFocus := FALSE;
  {- this routine is handling the list by default. }
  SetWindowLong(HWindow, DWL_MSGRESULT, 1);  {- set message result (for dialogs) }
  id:= msg.wParam;
  if (id = IDC_SEARCHSPECEDIT) or (id = IDC_SEARCHCDEDIT) then
  begin
    sFld^.Format;
    cFld^.Format;
    sFld^.XferCtlToRec(dbSpec^.dbr);
    cFld^.XferCtlToRec(dbSpec^.dbr);
    if not listObj^.SelectList(@self, dbSpec^.dbr) then
    begin
      if listObj^.errorNum <> 0 then
      begin
        iFld^.Clear;
        oFld^.Clear;
        if listObj^.errorNum = LISTERR_PARTIALKEYNOTFOUND then
          ShowError(@self, IDS_CANNOTLISTNOMATCH, nil, listObj^.errorNum, MOD_PNLVIEW, 0)
        else
          ShowError(@self, IDS_CANNOTLISTBAD, nil, listObj^.errorNum, MOD_PNLVIEW, 0);
      end;
    end
    else
    begin
      uiFldsProcessFocus := FALSE;
      sFld^.XferRecToCtl(dbSpec^.dbr);
      cFld^.XferRecToCtl(dbSpec^.dbr);
      iFld^.Clear;
      oFld^.Clear;
      iFld^.FocusFld;
    end;
  end
  else if id = IDC_SEARCHISOEDIT then
  begin
    seq := dbSpec^.dbr^.GetSeqValue;
    dbIso^.dbr^.PutField(DBISOSpecRef, @seq);
    dbSpec^.dbr^.GetField(DBSPECSpecID, @specID, 16);
    dbSpec^.dbr^.GetField(DBSPECCollectDate, @cDate, 16);
    dbIso^.dbr^.PutField(DBIsoSpecID, @specID);
    dbIso^.dbr^.PutField(DBIsoCollectDate, @cDate);
    dbIso^.dbr^.ClearField(DBISOIso);
    if not listObj^.SelectList(@self, dbIso^.dbr) then
    begin
      if listObj^.errorNum <> 0 then
      begin
        oFld^.Clear;
        if listObj^.errorNum = LISTERR_PARTIALKEYNOTFOUND then
          ShowError(@self, IDS_CANNOTLISTNOMATCH, nil, listObj^.errorNum, MOD_PNLVIEW, 0)
        else
          ShowError(@self, IDS_CANNOTLISTBAD, nil, listObj^.errorNum, MOD_PNLVIEW, 0);
      end;
    end
    else
    begin
      uiFldsProcessFocus := FALSE;
      iFld^.XferRecToCtl(dbIso^.dbr);
      oFld^.Clear;
      oFld^.FocusFld;
    end;
  end
  else if id = IDC_SEARCHTGEDIT then
  begin
    oFld^.Format;
    oFld^.XferCtlToRec(dbTstGrp^.dbr);
    if tstList^.count <> 0 then
      SetWindowLong(HWindow, DWL_MSGRESULT, 0);  {- set message result (for dialogs) }
  end;
  uiFldsProcessFocus := TRUE;
end;

procedure TSearchDlg.WMMnemSelected(var msg: TMessage);
var
  fld     : PUIListFld;
  fldID   : integer;
begin
  fld := PUIListFld(msg.lParam); {- get pointer to fld }
  fldID := msg.wParam;       {- get field ID }
  if fld = oFld then
  begin
    if fld^.IsEmpty then
      SetDlgItemText(hWindow, fldID, '.');
    EnableButtons;
  end;
end;

procedure TSearchDlg.EnableButtons;
var
  bEnable  : boolean;
begin
  bEnable := not oFld^.IsEmpty and not iFld^.IsEmpty and
             not sFld^.IsEmpty and not cFld^.IsEmpty;
  EnableWindow(GetItemHandle(BTN_SEARCHOK), bEnable);
end;

{--------------------------------------------------------[ TEditMICDlg ]--}

constructor TEditMICDlg.Init(aParent : PWindowsobject;
                             aTrayObj: PTraysobject;
                             aRDN    : integer;
                             adilStep: pointer;
                             aNewMIC : PChar);
begin
  inherited Init(aParent, MakeIntResource(DLG_MICEDIT));
  trayObj:= aTrayObj;
  drgRDN:= aRDN;
  dilStep:= aDilStep;
  newMIC:= aNewMIC;
  list:= New(PListBox, InitResource(@self, IDC_Dil_List));
end;

procedure TEditMICDlg.SetupWindow;
var
  k       : integer;
begin
  inherited SetupWindow;
  with trayObj^.dilList[drgRDN] do
  begin
    SetDlgItemText(hWindow, IDC_LBL1, drugname);
    for k:= 0 to dilCnt do
      list^.Addstring(drugDils[k]);
    list^.SetSelIndex(dilStep^);
  end;
end;

function TEditMICDlg.CanClose: boolean;
begin
  CanClose := false;
  if inherited CanClose then
  begin
    list^.GetSelstring(newMIC, 20);
    dilStep^:= list^.GetSelIndex;
    CanClose:= true;
  end;
end;


{ ----------------------------------------------- Exported procedures ---}

{------------------------------------------------------------------------}
{                                                                        }
{------------------------------------------------------------------------}
function GetETResult(testNum   : integer;
                     testID    : PChar;
                     xTraTbl   : XTraType;
                     resList   : PResults;
                     orgNum    : integer;
                 var wellFile  : text;
                     posFlags  : PPanelBitmap;
                     micData   : PMICData): integer;
var
  i, xtraCnt    : integer;
  wellNum       : integer;
  found, okOrg  : boolean;
  complete      : boolean;
  xTestResult   : integer;
  xTestname     : string[25];
  xtraPos       : integer;
  numXTests     : integer;
  res,
  MICValue      : array[0..32] of char;
  drugCount     : integer;
  abbr          : string;
  pennID        : array[0..3] of char;
  tstCat        : array[0..4] of char;
begin
  GetETResult := 0;
  testID[0] := chr(0);
  case xtraTbl[testNum] of
    4,8: begin {Strepamycin Synergy or Gentamycin Synergy}
           if xtraTbl[testNum] = 4 then
           begin
             xtraPos := 1;
             StrCopy(testID, '.StS');
             StrCopy(tstCat, 'StS');
           end
           else
           begin
             xtraPos := 2;
             StrCopy(testID, '.GmS');
             StrCopy(tstCat, 'GmS');
           end;
           if CheckOrg(orgNum, GSyngy) then
           begin
             (* this code assume the extra tests are always obtained from the
                POS Flags, regardless of the instrument.  However, the W/A also
                stuffs the extra test at the end of the MIC Data.
              *)

             if resList^.traysObj^.GetXtraWellNumber(wellFile, tstCat, wellNum) then
               if TestBit(posFlags^[wellNum mod 12], (wellNum div 12)) then
                 GetETResult := 2  {Positive}
               else
                 GetETResult := 1; {Negative}
           end;
         end;
    5: begin {Oxicillin - this code should never need to be executed}
(*         xtraPos := 5;
         StrPCopy(testID, '.OXI');
         GetETResult := 3;  { always prompt }
*)       end;
    7: begin {Beta Lactamase}
         xtraPos := 4;
         StrCopy(testID, '.BL');
         StrCopy(tstCat, 'BL');
         { MJS - 10/15/93  Changed to fix problem where MICs never showing in the
           panel read window when a BLAC exception occurs and also the BLac
           exception never went away because the database was never checked first
           before looking at Pen.
         }
         { read Blac from DB first }
         if resList^.GetResult(testID, 'BL', res, 20) Then
         begin
           if StrComp(res, '+') = 0 then
           begin
             GetEtResult := 2;
             exit;
           end
           else
             if StrComp(res, '-') = 0 then
             begin
               GetEtResult := 1;
               exit;
             end;
         end;
         {end of change 10/15/93 - MJS}

         if (StrComp(resList^.traysObj^.mapBat^.batIDList[resList^.traysObj^.tfsRec.trayNum],
                     'HNID') = 0) then
         begin
           (* this code assume the extra tests are always obtained from the
              POS Flags, regardless of the instrument.  However, the W/A also
              stuffs the extra test at the end of the MIC Data.
            *)

           if resList^.traysObj^.GetXtraWellNumber(wellFile, tstCat, wellNum) then
             if TestBit(posFlags^[wellNum mod 12], (wellNum div 12)) then
               GetETResult := 2  {Positive}
             else
               GetETResult := 1; {Negative}
         end
         else
         begin
           if (StrComp(resList^.traysObj^.mapBat^.batIDList[resList^.traysObj^.tfsRec.trayNum],
                       'PID') = 0) then
             GetETResult := 0
           else { panel will be PBP1J, PBP2J, PC1B, PC1C, PC1J, PC2C, PC2J, PM1J, PM2J }
           begin
             if CheckOrg(orgNum, GStaph) then
             begin
               { check Penicillin result }
               if (StrComp(resList^.traysObj^.mapBat^.batIDList[resList^.traysObj^.tfsRec.trayNum],
                           'PBP1J') = 0) or
                  (StrComp(resList^.traysObj^.mapBat^.batIDList[resList^.traysObj^.tfsRec.trayNum],
                           'PBP2J') = 0) then
                 StrPCopy(pennID, 'PS')
               else { panel is a PC1B, PC1C, PC1J, PC2C, PC2J, PM1J, PM2J }
                 StrPCopy(pennID, 'P');
               i := 1;
               found := FALSE;
               drugCount := resList^.traysObj^.DrugCnt;
               while (i <= drugCount) and not (found) do
               begin
                 if StrComp(pennID, resList^.traysObj^.dilList[i].drugname) = 0 then
                   found := TRUE
                 else i := i +1;
               end;
               if i > drugCount then
               begin
                 GetETResult := 0;
                 exit;
               end;
               StrPCopy(MICValue, resList^.traysObj^.dilList[i].drugDils[MICData^[i]]);
               if (StrComp(MICValue, ' .03')=0) or
                  (StrComp(MICValue, '<.03')=0) or
                  (StrComp(MICValue, '<.06')=0) then
                 GetETResult := 1  {Negative}
               else if (StrComp(MICValue, '>.03')=0) or
                       (StrComp(MICValue, '>.06')=0) or
                       (StrComp(MICValue, ' .06')=0) or
                       (StrComp(MICValue, ' .12')=0) or
                       (MICValue[0] = '<') then
                 GetETResult := 3  { prompt }
               else
                 GetETResult := 2; {Positive}
             end;
           end;
         end;
       end;
    9: begin {Thymadine Free Growth}
         xtraPos := 3;
         StrCopy(testID, '.TFG');
         StrCopy(tstCat, 'TFG');
        (* this code assume the extra tests are always obtained from the
           POS Flags, regardless of the instrument.  However, the W/A also
           stuffs the extra test at the end of the MIC Data.
         *)

         if resList^.traysObj^.GetXtraWellNumber(wellFile, tstCat, wellNum) then
           if TestBit(posFlags^[wellNum mod 12], (wellNum div 12)) then
             GetETResult := 2  {Positive}
           else
             GetEtResult := 1; {Negative}
       end;
  end;
end;

{------------------------------------------------------------------------}
{                                                                        }
{------------------------------------------------------------------------}
function EditMIC(aParent  : Pwindowsobject;
                 micInfo  : PMicData;
                 traysObj : PTraysobject;
                 drugRDN  : integer;    {- 0 based !!! }
                 newStr   : PChar): boolean;

var
  dilStep : integer;
  p       : PEditMICDlg;
begin
  EditMIC:= false;
  if drugRDN >= 0 then
  begin
    {- add 1 to drugRDN to allow for zero based list box}
    Inc(drugRDN);
    dilStep:= MICInfo^[drugRDN];
    p:= New(PEditMICDlg,
           Init(aParent, traysObj, drugRDN, addr(dilStep), newStr));
    if Application^.ExecDialog(p) = idOK then
      if MICInfo^[drugRDN] <> dilStep then
      begin
        MICInfo^[drugRDN]:= dilStep;
        EditMIC:= true;
      end;
  end;
end;

{------------------------------------------------------------------------}
{                                                                        }
{------------------------------------------------------------------------}
function EditBiochem(aParent    : PWindowsobject;
                     biotypeObj : PBiotypeobject;
                     bitNum     : integer;
                     resList    : PResults;
                     newStr     : PChar;
                     posFlags   : PPanelBitmap;
                 var sugar      : boolean): boolean;
var
  value,
  wellNum,
  group   : integer;
  pstr    : PChar;
begin
  EditBiochem := true;
  BiotypeObj^.BitInfo(bitNum, value, wellNum, group, newStr);
  sugar :=(resList^.traysObj^.tfsRec.tSet = 1) {is a neg panel} AND
          (resList^.traysObj^.tfsRec.idRec <> -1) {not an MIC panel} AND
          (BiotypeObj^.class = fermenter) { is a fermentor } AND
          {well is a sugar}
          ((wellNum = GluWellNum) or
           (wellNum = SucWellNum) or
           (wellNum = SorWellNum));
  if group = 254 then {this biotype bit is unused}
  begin
    EditBiochem := false;
    exit;
  end;
  {look at ComputeBiotype for what 0, 252, 253, 254, etc. mean}
  if (group > 0 ) AND (group < 250) then {this biotype bit is derived from an MIC value and cannot be edited here.}
  begin
    GetMem(pstr, 101);
    InfoMsg(aParent^.hWindow, '', SR(IDS_CANNOT_EDIT, pstr, 100));
    MSFreeMem(pstr, 101);
    EditBiochem := false;
  end
  else
  begin
    Pad(newStr, newStr, ' ', 6);
    if TestBioBit(biotypeObj^.biotype, bitNum) then
    begin
      ClearBioBit(biotypeObj^.biotype, bitNum);
      if group = 255 then {indole, oxidase, beta hemolysis}
        {set in resList, not posFlags}
        case wellNum of
          0 : {oxidase}
              resList^.SetResult('.OXI', 'OXI', '-', true);
          1 : {beta hemolysis}
              resList^.SetResult('.HEM', 'HEM', '-', true);
          57: {indole}
              resList^.SetResult('.IND', 'IND', '-', true);
        end
      else
        {not stored in DB so set in posFlags}
        ClearBit(posFlags^, wellNum);
      StrCat(newstr, '-');
    end
    else
    begin
      SetBioBit(biotypeObj^.biotype, bitNum);
      if group = 255 then {indole, oxidase, beta hemolysis}
        {set in resList, not posFlags}
        case wellNum of
          0 : {oxidase}
              resList^.SetResult('.OXI', 'OXI', '+', true);
          1 : {beta hemolysis}
              resList^.SetResult('.HEM', 'HEM', '+', true);
          57: {indole}
              resList^.SetResult('.IND', 'IND', '+', true);
        end
      else
        {not stored in DB so set in posFlags}
        SetBit(posFlags^, wellNum);
      StrCat(newstr, '+');
    end;
  end;
end;

{------------------------------------------------------------------------}
{                                                                        }
{------------------------------------------------------------------------}
function SaveIsolateTests(aParent     : PWindowsobject;
                          overWrite,
                          prompt,
                          saveID,
                          saveMIC,
                          saveET      : boolean;
                      var canceled    : boolean;
                      var wellFile    : Text;
                          orgDB       : PDBFile;
                          newOrgRef   : TSeqNum;
                          bioStr      : PChar;
                          micData     : PMICData;
                          isoDB       : PDBFile;
                          specDB      : PDBFile;
                          resList     : PResults;
                          posFlags    : PPanelBitMap): boolean;
var
  tempStr    : PChar;
  d          : PSaveDlg;
  idOnly     : boolean;
  MICOnly    : boolean;
  idSaved,
  MICSaved,
  ETSaved,
  biotSaved,
  transactionResult: boolean;
  orgRef     : TSeqNum;
  testFile   : PDBFile;
  interp     : PInterp;
  saveKeyNum : integer;
  wait       : PWaitCursor;
  errNum,
  orgNum,
  code       : integer;
  orgPChar   : array[0..15] of char;
  srcRef     : TSeqNum;
  srcFile    : PDBFile;
  urineSrc   : boolean;
  tBool      : boolean;
  tempIso    : PDBRec;

  MappedOrg  : Integer;
  OrgSet     : Integer;
  ScreenRes  : PScreens;

  anOrg          : Integer;
  anOrgRef       : TSeqNum;
  biotypeObj     : PBiotypeObject;
  VeryRareBioType: boolean;

  procedure ClearInterp(aRes: PResRecObj); far;
  {- clear interp results }
  begin
    if StrIComp(aRes^.testCatID, 'NCCLS') = 0 then
      aRes^.SetResult('');
  end;

begin
  testFile := nil;
  interp := nil;
  tempIso := nil;
  srcFile := nil;
  SaveIsolateTests := false;
  canceled := false;
  transactionResult := false;
  IDSaved:= false;
  biotSaved:= false;
  MICSaved:= false;
  ETSaved:= false;
  testFile := New(PDBFile, Init(DBTSTFile, '', DBOpenNormal));
  saveKeyNum := testFile^.dbc^.GetCurKeyNum;
  testFile^.dbc^.SetCurKeyNum(DBTST_ID_KEY);
  {- find out if it is an id only or MIC only panel }
  MICOnly:= resList^.traysObj^.tfsRec.idRec = -1;
  IDOnly:= (resList^.traysObj^.DrugCnt = 0);

  if prompt then
  begin
    d:= New(PSaveDlg, Init(aParent, saveID, saveMIC, idOnly, MICOnly,
                        not isoDB^.dbr^.FieldIsEmpty(DBISOTstDate)));
    if Application^.ExecDialog(d) = idCancel then
    begin
      canceled:= true;
    end;
  end;

  if not canceled then
  begin
    if prompt then
       saveET:=saveMIC;

    wait:= New(PWaitCursor, Init);
    if saveID or saveMIC or saveET then
    begin
      if saveID then
      begin     {- Save ID information }
        if (overWrite) or
           ((not overWrite) and (isoDB^.dbr^.FieldIsEmpty(DBISOOrgRef))) then
        begin

          if (prompt) and (newOrgRef=0) and (not MICOnly) then
          begin
            { Extract the organism class }
            IsoDB^.dbr^.GetField(DBIsoSetFamily, @OrgSet, sizeof(OrgSet));
            OrgSet:=ExtractSet(OrgSet);

            { Build a biotype object }
            BiotypeObj:= New(PBiotypeObject, Init(@(ResList^.TraysObj^.tfsRec)));
            BioTypeObj^.SetBioClass(OrgSet);
            BioTypeObj^.SetBioType(OrgSet,BioStr);

            { Decide if this is a VRB }
            VeryRareBioType:=
              BiotypeObj^.GetTopID(anOrg, nil, nil, nil) and (anOrg=0);
            Dispose(BioTypeObj,Done);

            { If VRB, give the user a chance to enter an organism }
            if VeryRareBioType then
               if  GetAnOrganism(aParent, anOrgRef)
                   then newOrgRef:=anOrgRef;
          end;

          if newOrgRef <> 0 then
          begin
            isoDB^.dbr^.PutField(DBISOORGRef, @newOrgRef);
            IDSaved:= true;
          end;
        end;
        if not MICOnly then {if user selects to save an MIC only panel
                             the system will prompt for an org, then display
                             the save prompt.  The user selects to save both
                             ID (they selected) and the MICs.  In this case
                             there is no biotype to store.}
          { SetResult takes care of the overwrite logic }
          if resList^.SetResult('.BIO', 'BIO', bioStr, overwrite) then
            biotSaved:= true;
      end;

      {initialize the interps object with the organism found in the iso record}
      isoDB^.dbr^.GetField(DBISOOrgRef, @orgRef, sizeof(TSeqNum));
      if orgDB^.dbc^.GetSeq(orgDB^.dbr, orgRef) then
      begin
        orgDB^.dbr^.GetFieldAsStr(DBOrgOrg, orgPChar, 15);
        Val(orgPChar, orgNum, code);
      end
      else
        orgNum:= -1;
      interp := New(PInterp, Init(orgNum, resList));

      if saveET then
        if SaveExtraTests(aParent, prompt, wellFile, testFile, orgNum, resList,
                          posFlags, MICData, overwrite) then
          ETSaved:= true;

      if saveMIC then
      begin     {- save MIC only information }
        if SaveMICs(resList, interp, orgRef, testFile, micData, overwrite) then
          MICSaved:= true;
      end
      else
        { need to set up the interps objects with MICs from the database, if any}
        DoSetUpInterps(isoDB, resList, interp, orgRef);

      { if overwriting information then we must update the test date, however,
        if not overwriting information, then only update the test date if New
        information is stored }
      if IDSaved or MICSaved or ETSaved or biotSaved then
      begin
        GetMem(tempStr, 33);
        IntlDateStr(IntlCurrentDate, tempStr, 32);
        isoDB^.dbr^.PutFieldAsStr(DBISOTstDate, tempStr);
        MSFreeMem(tempStr, 33);
      end;

      if (IDSaved or MICSaved) then
      begin
        urineSrc := false;  {default if no source is entered by user}
        SpecDB^.dbr^.GetField(DBSPECSrc, @srcRef, sizeof(srcRef));
        if srcRef <> 0 then
        begin
          srcFile:= new(PDBFile, init(DBSRCFile, '', DBOpenNormal));
          if srcFile <> nil then
          begin
            srcFile^.dbc^.GetSeq(srcFile^.dbr, srcRef);
            srcFile^.dbr^.GetField(DBSRCUrine, @urineSrc, sizeof(urineSrc));
            MSDisposeObj(srcFile);
          end;
        end;

        if orgNum > 0 then
        begin
          {Calc temptative interps, VIE/VRE inspect Vcm's}
          interp^.CalcInterps(urineSrc);

          {There might be screens}
          ScreenRes:=New(PScreens,Init);
          IsoDB^.dbr^.GetField(DBIsoSetFamily, @OrgSet, sizeof(OrgSet));
          OrgSet:=ExtractSet(OrgSet);
          ScreenIsolate(aParent, Orgnum, MappedOrg, OrgSet, ResList, ScreenRes, not Prompt, False);

          {if a screen remapped the Organism, save it}
          {adjust the pInterps to use the MappedOrg}
          if ScreenRes^.ScreensFound then
          begin
            {from the Organism number, get its sequence}
            {and replace the original ISO Organism}
            Str(MappedOrg,OrgPChar);
            orgDB^.dbc^.SetCurKeyNum(DBORG_ID_KEY);
            orgDB^.dbr^.PutFieldAsStr(DBORGOrg, OrgPChar);
            orgDB^.dbc^.GetEQ(orgDB^.dbr);
            OrgRef:=orgDB^.dbr^.GetSeqValue;
            isoDB^.dbr^.PutField(DBISOORGRef, @OrgRef);

            {Throw away current interps}
            MSDisposeObj(interp);
            ResList^.ForEach(@ClearInterp);

            {ReCalc Interps if ESBL+ or EBL?}
            interp := New(PInterp, Init(MappedOrg, resList));
            if SaveMIC
               then SaveMICs(resList, interp, orgRef, testFile, micData, overwrite)
               else DoSetUpInterps(isoDB, resList, interp, orgRef);
            interp^.CalcInterps(urineSrc);
          end;

          MSDisposeObj(ScreenRes);
        end
        else reslist^.ForEach(@ClearInterp);
      end;{if IDSaved or MICSaved...}

      if IDSaved or MICSaved or ETSaved or biotSaved then
      begin
        errNum:= DBBeginTransaction;
        if errNum = 0 then
        begin
          {- handle transmit flag. (clear it) }
          tBool:= false;
          isoDB^.dbr^.PutField(DBIsoTransFlag, @tBool);

          tempIso := New(PDBRec, Init(isoDB^.dbc));
          transactionResult := (tempIso <> nil);
          if transactionResult then
          begin
            transactionResult := tempIso^.CopyRecord(isoDB^.dbr);
            if transactionResult then
              transactionResult := isoDB^.dbc^.GotoRecord(tempIso);
            MSDisposeObj(tempIso);
          end;

          if transactionResult then
            transactionResult := isoDB^.dbc^.UpdateRec(isoDB^.dbr);
          if transactionResult then
            transactionResult:= resList^.SaveResults;

          if not transactionResult then
            DBAbortTransaction
          else
            DBEndTransaction;
        end;
      end;
    end;
    MSDisposeObj(wait);
  end;

  testFile^.dbc^.SetCurKeyNum(saveKeyNum);
  MSDisposeObj(testFile);
  MSDisposeObj(interp);

  SaveIsolateTests := true;

  if saveID then
    if not IDSaved then
    begin
      SaveIsolateTests := false;
      Exit;
    end;

  if saveMIC then
    if not (MICSaved or IDOnly) then
    begin
      SaveIsolateTests := false;
      Exit;
    end;

  if not transactionResult then
  begin
    SaveIsolateTests := false;
    Exit;
  end;

end;

{---------------------------------------------------------------------------}
{ This function calls the biotype editor. It lets the user edit the biotype }
{ biochemicals, class and organism.  If the BiotypeEditor returns true,     }
{ then the selected org (number) is returned in AnOrgID with prob text in   }
{ AProb, the selected family can be obtained from the biotype object passed }
{ in (field is Class) and the New biotype can also be obtained from the     }
{ passed biotype object (field is Biotype).                                 }
{                                                                           }
{ Assumptions:                                                              }
{   Compute biotype MUST have been called BEFORE THIS ROUTINE IS CALLED     }
{---------------------------------------------------------------------------}
function BiotypeEditor(aParent    : PWindowsobject;
                       aBiotypeObj: PBiotypeobject;
                       results    : PResults;
                       anOrgID    : PDBFile;
                       aProbTxt   : PChar;
                       aPosFlags    : PPanelBitmap;
                       aSelectedOrg : PSelectedOrg): boolean;
var
  p : PBioDlg;
begin
  p:= New(PBioDlg, Init(aParent, aBiotypeObj, results, anOrgID, aProbTxt, aPosFlags, aSelectedOrg));
  BiotypeEditor:= Application^.ExecDialog(p) = idOK;
end;

{-------------------------------------------------------------------------}
Function PromptForBiochemResult(AParent   : PWindowsObject;
                                bioObj    : PBiotypeObject;
                                bitNum    : integer;
                                posFlags,
                                chkFlags  : PPanelBitmap;
                            var result    : boolean): boolean;
{- Ask the user for a manual interpretation of a reaction }
var
  d   : PBiochemDialog;
  idx : integer;
begin
  PromptForBiochemResult := false;
  with bioObj^.BDesc[BitNum][bioObj^.bDescClass] do
  begin
    idx := index;
    d:= New(PBiochemDialog, Init(AParent, name, posFlags, idx));
  end;
  if Application^.ExecDialog(d) = idOK then
  begin
    PromptForBiochemResult := true;
    ClearBioBit(bioObj^.neededResults, BitNum);
    if (bitNum = bioObj^.oxidaseBitNum) or (bitNum = bioObj^.indoleBitNum) then
      ClearBioBit(bioObj^.BioMask, BitNum)
    else if chkFlags <> nil then
      ClearBit(chkFlags^, idx);
    result := TestBit(posFlags^, idx);
    if result then
      SetBioBit(bioObj^.biotype, bitnum)
    else
      ClearBioBit(bioObj^.biotype, bitnum);
  end;
end;

{-------------------------------------------------------------------------}
{-------------------------------------------------------------------------}
function GetBiotype(aParent  : PWindowsObject;
                    prompt   : boolean;
                    aFamily  : integer;
                    resList  : PResults;
                    posFlags,
                    chkFlags,
                    virtTests: PPanelBitmap;
                    MICData  : PMICData;
                    bioObj   : PBiotypeObject): boolean;
var
  goodBiotype,
  userAbort  : boolean;
  res        : array[0..32] of char;
  oxi, ind,
  betaHem    : integer;  {00-no test, 10-neg, 11-pos}
begin
  goodBiotype := false;
  userAbort := false;
  oxi := 0;
  betaHem := 0;
  ind := 0;
  while not goodBiotype and not userAbort do
  begin
    if resList^.GetResult('.OXI', 'OXI', res, 32) then
      if StrComp(res, '') <> 0 then
      begin
        SetBit(oxi, 1); {set result exists bit}
        if StrComp(res, '+') = 0 then
          SetBit(oxi, 0); {set result bit to pos}
      end;
    if resList^.GetResult('.IND', 'IND', res, 32) then
      if StrComp(res, '') <> 0 then
      begin
        SetBit(ind, 1); {set result exists bit}
        if StrComp(res, '+') = 0 then
          SetBit(ind, 0); {set result bit to pos}
      end;
    if resList^.GetResult('.HEM', 'HEM', res, 32) then
      if StrComp(res, '') <> 0 then
      begin
        SetBit(betaHem, 1); {set result exists bit}
        if StrComp(res, '+') = 0 then
          SetBit(betaHem, 0); {set result bit to pos}
      end;

    goodBiotype := BioObj^.ComputeBiotype(aFamily,
                                          posFlags, ChkFLags,
                                          MICData, oxi, ind, betaHem,
                                          virtTests);
    if not goodBiotype then
      if prompt then
        userAbort := not(GetMissingResults(aParent, reslist, posFlags,
                                           ChkFLags, bioObj))
      else
        userAbort := TRUE;
  end;
  GetBiotype := goodBiotype;
end;

{-------------------------------------------------------------------------}
{-------------------------------------------------------------------------}
function RecomputeBiotype(aParent  : PWindowsObject;
                          resList  : PResults;
                          posFlags,
                          chkFlags : PPanelBitmap;
                          MICData  : PMICData;
                          bioObj   : PBiotypeObject): boolean;
var
  goodBiotype,
  userAbort  : boolean;
begin
  goodBiotype := false;
  userAbort := false;
  while not goodBiotype and not userAbort do
  begin
    goodBiotype := BioObj^.RecomputeBiotype;
    if not goodBiotype then
    userAbort := not(GetMissingResults(aParent, resList, posFlags,
                                       ChkFlags, bioObj));
  end;
  RecomputeBiotype:= goodBiotype;
end;

type
  POrgDlg = ^TOrgDlg;
  TOrgDlg = object(TCenterDlg)
    orgSeq    : ^TSeqNum;
    len       : integer;
    org       : PUIFld;
    iso       : PDBFile;
    listObj   : PListObject;
    {}
    constructor Init(aParent: PWindowsObject; var anOrgSeq: TSeqNum);
    destructor Done; virtual;
    function CanClose: boolean; virtual;
  end;

constructor TOrgDlg.Init(aParent: PWindowsObject; var anOrgSeq: TSeqNum);
begin
  inherited Init(aParent, MakeIntResource(DLG_ORGENTRY));
  orgSeq:= @anOrgSeq;
  orgSeq^:= 0;
  iso:= New(PDBFile, Init(DBIsoFIle, '', dbOpenNormal));
  listObj:= New(PListObject, Init);
  org:= New(PUIFld, Init(@self, iso^.dbd, listObj, true, DBISOOrgRef, IDC_ORGID, 0));
end;

destructor TOrgDlg.Done;
begin
  if iso <> nil then
    Dispose(iso, Done);
  iso:= nil;
  if listObj <> nil then
    Dispose(listObj, Done);
  listObj:= nil;
  inherited Done;
end;

function TOrgDlg.CanClose: boolean;
var
  k   : integer;
begin
  k:= org^.Format;
  if k = 0 then
  begin
    org^.XFerCtlToRec(iso^.dbr);
    iso^.dbr^.GetField(DBISOOrgRef, orgSeq, sizeof(orgSeq^));
    CanClose:= true;
  end
  else
  begin
    ShowUIFldError(@self, k, org);
    CanClose:= false;
  end;
end;

function GetAnOrganism(aParent: PWindowsObject; var orgSeq: TSeqNum): boolean;
{- prompt for an organism. If one is selected, returns organism seq number and
   function result is true, otherwise false }
begin
  GetAnOrganism:= application^.ExecDialog(New(POrgDlg, Init(aParent, orgSeq))) = IDOK;
end;

END.
