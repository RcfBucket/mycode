	page	,128
code	segment	public 'CODE' byte

	assume	cs:code,ds:nothing,es:nothing

     public    unjulian

.8087
;
c1721119_2	dq	1721119.2
c36524_25      dd   36524.25
c365_25		dd	365.25
c30_6		dq	30.6
c4             dd   4.0
c_3            dq   0.3
;
control		dw	0c00h
;
;======================================================================
; Procedure Unjulian( JDN : Longint; VAR Year, Month, Day : Integer );
;
jdn       equ  dword ptr [bp+16]
yrptr     equ  dword ptr [bp+12]
moptr     equ  dword ptr [bp+8]
daptr     equ  dword ptr [bp+4]
psize	equ	16

year	equ	word ptr [bp-2]
month	equ	word ptr [bp-4]
day	equ	word ptr [bp-6]
csave	equ	word ptr [bp-8]
lsize	equ	8

unjulian  proc near
		push	bp			;setup stack
		mov	bp,sp
		sub	sp,lsize
		fstcw	csave			;save old control word
		fldcw	control			;load new CW, (round to zero)
		fild	jdn
		fsub	c1721119_2		;0=n-1721119.2
		fld	st			;0,1=N
		fdiv	c36524_25
		frndint				;0=c,1=N
		fld	st
		fdiv	c4
		frndint				;0=int(c/4),1=c,2=N
		fsubp	st(1),st		;0=c-int(c/4),1=N
		faddp	st(1),st		;0=n'
		fld	c365_25			;0=365.25,1=n'
		fld	st(1)			;0=n',1=..,2=n'
		fdiv	st,st(1)		;0=n'/365.25,1=365.25,2=n'
		frndint				;0=yearoid,1=365.25,2=n'
		fist	year			;save the yearoid off
		fmulp	st(1),st		;0=yearoid*365.25,1=n
		frndint
		fsubp	st(1),st		;0=n''
		fsub	c_3
		fld	c30_6			;0=30.6,1=(n''-d)
		fld	st			;0,1=30.6,2=(n''-d)
		fdivr	st,st(2)		;0=(n''-d)/30.6,1=30.6,2=..
		frndint				;0=m',1=30.6,2=..
		fist	month			;save the monthoid
		fmulp	st(1),st		;0=m'*30.6,1=(n''-d)
		fsubp	st(1),st		;0=D-1
		fld1
		faddp	st(1),st		;0=day
		frndint
		fistp	day
		fldcw	csave			;return old control word
		cmp	month,9			;adjust yearoid and monthoid
		ja	j_1
		add	month,3
		jmp	short j_2
j_1:		sub	month,9
		inc	year
j_2:		les	di,yrptr		;move returns to VAR parms
		mov	ax,year
		mov	es:word ptr [di],ax
		les	di,moptr
		mov	ax,month
		mov	es:word ptr [di],ax
		les	di,daptr
		mov	ax,day
		mov	es:word ptr [di],ax
		mov	sp,bp			; cleanup & outa here
		pop	bp
		ret	psize
unjulian	endp

code		ends

		end
