unit DB2DBIF;

{- Version 3.0 db to DBIF interface module }

INTERFACE

uses
  DBif,
  WinProcs,
  DBTypes,
  Objects;

type
  PDB2Field = ^TDB2Field;         {- database field object }
  TDB2Field = object(TObject)
    fldName   : array[0..25] of char;   {- field Name in DBIF is string[24] }
    fieldNum  : word;
    keyFlag   : boolean;
    UDFlag    : boolean;
    fldType   : TDBFldTypes;
    fldSize   : word;
    value     : PChar;
    orgValue  : string[25];
    valSize   : integer;
    mnemFile  : array[0..15] of char;
    constructor Init(fh: DbifFileHandle; fNum: word);
    destructor Done; virtual;
    procedure SetPValue(aVal: PChar);
    procedure SetValue(aVal: string);
  end;

  PDB2Record = ^TDB2Record;
  TDB2Record = object(TObject)
    fileName    : array[0..15] of char;
    filePath    : array[0..80] of char;
    flds        : PCollection;
    fh          : DBifFileHandle;
    rh          : DBIFRecordHandle;
    constructor Init(aName: PChar);
    destructor Done; virtual;
    function  NumFields: integer;
    function  GetField(fldNum: integer): PDB2Field;
    procedure PutField(fldNum: integer; aVal: PChar);
    procedure ClearRec;
  end;

  PStringList = ^TStringList;
  TStringList = object(TCollection)
    constructor Init;
    function  NumEntries: integer;
    function  GetEntry(idx: integer; aName: PChar; maxlen: integer): PChar;
  end;

var
  workDir : string[80];            {- Read-only }
  dbifDir : string[80];            {- Read-only }

procedure SetDBIFDir(aDir: PChar);
function  DB2OpenFile(fName: PChar): PDB2Record;
procedure DB2CloseFile(var rec: PDB2Record);
function  DB2GetFirst(rec: PDB2Record): boolean;
function  DB2GetNext(rec: PDB2Record): boolean;
function  DB2FindID(rec: PDB2Record): boolean;
procedure GetMnemList(list: PStringList);
procedure GetTestProcList(batID: PChar; list: PStringList);
function  CheckDupTests(aBatID: PChar): integer;
function  DB2RenameFile(oldF, newF: string): boolean;
function NumProcsInBattery(aBatID: PChar): integer;

IMPLEMENTATION

uses
  ApiTools,
  CvtMisc,
  DbifFlds,
  IntlLib,
  IniLib,
  oDateli,
  Strings;

type
  PStringObj = ^TStringObj;
  TStringObj = object(TObject)
    size  : integer;
    name  : PChar;
    constructor Init(aName: PChar);
    destructor Done; virtual;
  end;

constructor TStringObj.Init(aName: PChar);
begin
  inherited Init;
  if (aName = nil) or (strlen(aName) = 0) then
    size:= 20
  else
    size:= StrLen(aName) + 1;
  GetMem(name, size);
  StrCopy(name, '');
  if aName <> nil then
    StrCopy(name, aName);
end;

destructor TStringObj.Done;
begin
  inherited Done;
  if name <> nil then
    FreeMem(name, size);
end;
constructor TStringList.Init;
begin
  inherited Init(25, 25);
end;

function TStringList.NumEntries: integer;
begin
  NumEntries:= count;
end;

function TStringList.GetEntry(idx: integer; aName: PChar; maxlen: integer): PChar;
{- return a mnemonic file name for the idx'th mnemonic file (1 based) }
var
  m   : PStringObj;
begin
  if (idx >= 1) and (idx <= count) then
  begin
    m:= At(idx-1);
    StrLCopy(aName, m^.name, maxLen);
    GetEntry:= aName;
  end
  else
    GetEntry:= nil;
end;

procedure GetTestProcList(batID: PChar; list: PStringList);
{- return a list of test procedures for a battery }
var
  k     : integer;
  m     : PStringObj;
  tstr  : string;
  bid   : string[80];
  pstr  : array[0..100] of char;
  fh    : DbifFileHandle;
  rh    : DbifRecordHandle;
begin
  ChDir(dbifDir);

  list^.FreeAll;

  fh:= DbifOpenFile('BATPROC', DbifShared);
  if DbifGetErrorCode <> 0 then exit;
  rh:= DbifNewRecord(fh);

  bID:= StrPas(batID);
  DbifPutFieldVal(rh, DbifFldBatProcID, bID);
  DbifSearchRecordGE(rh);
  if DbifGetErrorCode = 0 then
  begin
    repeat
      if bID = DbifGetFieldVal(rh, DbifFldBatProcID) then
      begin
        tstr:= DbifGetFieldVal(rh, DbifFldBatProcTestProc);
        StrPCopy(pstr, tstr);
        m:= New(PStringObj, Init(pstr));
        list^.Insert(m);
        DbifNextRecord(rh);
      end;
    until (DbifGetErrorCode <> 0) or
          (DbifGetFieldVal(rh, DbifFldBatProcID) <> bID);
  end;

  DbifCloseRecord(rh);
  DbifCloseFile(fh);

  ChDir(workDir);
end;

function NumProcsInBattery(aBatID: PChar): integer;
var
  sList : PStringList;
begin
  sList:= New(PStringList, Init);
  GetTestProcList(aBatID, sList);
  NumProcsInBattery:= sList^.count;
  Dispose(sList, Done);
end;

procedure GetMnemList(list: PStringList);
{- return a list of mnemonic files }
var
  k     : integer;
  m     : PStringObj;
  tstr  : string;
  pstr  : array[0..100] of char;
begin
  ChDir(dbifDir);

  list^.FreeAll;

  m:= New(PStringObj, Init('SOURCE'));
  list^.Insert(m);
  m:= New(PStringObj, Init('WARD'));
  list^.Insert(m);

  {- now loop thru and find all user defined files }
  for k:= 1 to DbifGetFileCount do
  begin
    if DbifGetFileUDFlag(k) then
    begin
      tstr:= DbifGetFileName(k);
      StrPCopy(pstr, tstr);
      m:= New(PStringObj, Init(pstr));
      list^.Insert(m);
    end;
  end;
  ChDir(workDir);
end;

function GetMnemFileFromField(fieldName: string; fileName: PChar): boolean;
{- given a mnemonic field name, find its appropriate file name }
var
  fh    : DbifFileHandle;
  rh    : DbifRecordHandle;
  tstr  : string[80];
begin
  fh:= DbifOpenFile('MNEMFILE', DbifShared);
  rh:= DBIFNewRecord(fh);
  StrCopy(fileName, '');
  DBIFPutFieldVal(rh, DBIFFldMnemFileDesc, fieldName);
  DBIFGetRecord(rh);
  if DBIFGetErrorCode = 0 then
  begin
    tstr:= DBIFGetFieldVal(rh, DBIFFldMnemFileName);
    StrPCopy(fileName, tstr);
    GetMnemFileFromField:= true;
  end
  else
    GetMnemFileFromField:= false;
  DbifCloseRecord(rh);
  DbifCloseFile(fh);
end;

procedure ConvertDate(oldDate: string; newDate: PChar);
{- convert an old DBIF date to the current 3.0 format }
var
  y, m, d : integer;
  dt      : TIntlDate;
  pstr    : array[0..50] of char;
begin
  BreakDate(oldDate, y, m, d);
  dt:= IntlPackDate(y, m, d);
  IntlDateStr(dt, pstr, sizeof(pstr)-1);
  StrCopy(newDate, pstr);
end;

procedure ConvertTime(oldTime: string; newTime: PChar);
{- convert an old DBIF time to the current 3.0 format }
var
  h, m, s : integer;
  dt      : TIntlTime;
  pstr    : array[0..50] of char;
begin
  BreakTime(oldTime, h, m, s);
  dt:= IntlPackTime(h, m, s, 0);
  IntlTimeStr(dt, IniShowSeconds, pstr, sizeof(pstr)-1);
  StrCopy(newTime, pstr);
end;

constructor TDB2Field.Init(fh: DbifFileHandle; fNum: word);
var
  tstr    : string;
begin
  inherited Init;
  tstr:= DbifGetFieldName(fh, fNum);
  StrPCopy(fldName, tstr);
  value:= nil;
  valSize:= 255;
  fieldNum:= fNum;
  StrCopy(mnemFile, '');

  case DbifGetFieldType(fh, fNum) of
    DbifInvalid:
      fldType:= dbInvalid;
    DbifInteger:
      fldType:= dbLongint;
    DbifNumber:
      fldType:= dbSingle;
    DbifDate:
      fldType:= dbDate;
    DbifTime:
      fldType:= dbTime;
    DbifMnemonic:
      begin
        fldType:= dbSeqRef;
        GetMnemFileFromField(StrPas(fldName), mnemFile);
      end;
    DbifString, DbifText:
      fldType:= dbZString;
    DbifAutoUser, DbifAutoDate, DbifAutoTime, DbifAutoArchive, DbifAutoSession:
      fldType:= dbInvalid;
  end;

  fldSize:= DbifGetFieldSize(fh, fNum);
  keyFlag:= DbifGetFieldKeyFlag(fh, fNum);
  UDFlag:= DbifGetFieldUDFlag(fh, fNum);

  if fldType = dbZString then
    valSize:= fldSize + 1;

  GetMem(value, valSize);
end;

procedure TDB2Field.SetPValue(aVal: PChar);
begin
  StrLCopy(value, aVal, valSize-1);
end;

procedure TDB2Field.SetValue(aVal: string);
begin
  if length(aVal) > valSize-1 then
    aVal[0]:= Chr(valSize-1);
  StrPCopy(value, aVal);
end;

destructor TDB2Field.Done;
begin
  inherited Done;
  if value <> nil then
    FreeMem(value, valSize);
  value:= nil;
end;

constructor TDB2Record.Init(aName: PChar);
var
  rec     : PDB2Record;
  k       : integer;
  fld     : PDB2Field;
begin
  inherited Init;
  fh:= nil;
  rh:= nil;
  flds:= nil;

  StrLCopy(fileName, aName, sizeof(fileName)-1);
  StrCopy(filePath, '');

  fh:= DbifOpenFile(StrPas(aName), DbifShared);
  if DbifGetErrorCode <> 0 then
  begin
    fh:= nil;
    Done;
    fail;
  end;

  rh:= DbifNewRecord(fh);
  if DbifGetErrorCode <> 0 then
  begin
    rh:= nil;
    Done;
    fail;
  end;

  flds:= New(PCollection, Init(25, 25));

  {- now, collect all field information for the record }
  for k:= 1 to DbifGetFieldCount(fh) do
  begin
    fld:= New(PDB2Field, Init(fh, k));

    flds^.Insert(fld);
  end;
end;

destructor TDB2Record.Done;
begin
  inherited Done;
  if fh <> nil then
    DbifCloseFile(fh);
  if rh <> nil then
    DbifCloseRecord(rh);
  if flds <> nil then
    Dispose(flds, Done);
  rh:= nil;
  fh:= nil;
  flds:= nil;
end;

procedure TDB2Record.ClearRec;
{- clear the value of all fields }
var
  k   : integer;
  fld : PDB2Field;
begin
  for k:= 1 to NumFields do
  begin
    fld:= GetField(k);
    if fld <> nil then
      fld^.SetValue('');
  end;
end;

function TDB2Record.NumFields: integer;
begin
  NumFields:= flds^.count;
end;

procedure TDB2Record.PutField(fldNum: integer; aVal: PChar);
var
  fld : PDB2Field;
begin
  fld:= GetField(fldNum);
  if fld <> nil then
  begin
    fld^.SetPValue(aVal);
  end;
end;

function TDB2Record.GetField(fldNum: integer): PDB2Field;
{- return field information for the specified field.
   NOTE: fldNum is passed in as 1 (ONE) based!!}
begin
  if (fldNum >= 1) and (fldNum <= flds^.count) then
    GetField:= flds^.At(fldNum-1)
  else
    GetField:= nil;
end;

function DB2OpenFile(fName: PChar): PDB2Record;
{- open a dbif file and return a pointer to a TRecord }
begin
  if dbifDir = '' then
    DB2OpenFile:= nil
  else
  begin
    ChDir(dbifDir);
    DB2OpenFile:= New(PDB2Record, Init(fName));
    ChDir(workDir);
  end;
end;

procedure DB2CloseFile(var rec: PDB2Record);
{- close a DBIF file and dispose the TRecord }
begin
  if rec = nil then exit;

  ChDir(dbifDir);

  Dispose(rec, Done);
  rec:= nil;

  ChDir(workDir);
end;

procedure GetDataFromRec(rec: PDB2Record);
var
  k   : integer;
  fld : PDB2Field;
  tstr: string;
  pstr: array[0..100] of char;
begin
  for k:= 1 to rec^.NumFields do
  begin
    fld:= rec^.GetField(k);
    tstr:= DbifGetFieldVal(rec^.rh, k);
    if fld <> nil then
    begin
      fld^.orgValue := tstr;
      if fld^.fldType = dbDate then
      begin
        ConvertDate(tstr, pstr);
        fld^.SetPValue(pstr);
      end
      else if fld^.fldType = dbTime then
      begin
        ConvertTime(tstr, pstr);
        fld^.SetPValue(pstr);
      end
      else
        fld^.SetValue(tstr);
    end;
  end;
end;

function DB2GetFirst(rec: PDB2Record): boolean;
var
  k   : integer;
begin
  if rec <> nil then
  begin
    ChDir(dbifDir);

    DbifFirstRecord(rec^.rh);
    if DbifGetErrorCode = 0 then
    begin
      GetDataFromRec(rec);
      DB2GetFirst:= true;
    end
    else
    begin
      rec^.ClearRec;
      DB2GetFirst:= false;
    end;
    ChDir(workDir);
  end;
end;

function DB2FindID(rec: PDB2Record): boolean;
{- given the data in rec, use it to find a record in the DBIF file. All key
   fields will be transferred to the DBIF record }
var
  fld   : PDB2Field;
  tstr  : string;
  k     : integer;
begin
  ChDir(dbifDir);

  for k:= 1 to rec^.NumFields do
  begin
    fld:= rec^.GetField(k);
    if fld^.KeyFlag then
    begin
      tstr:= StrPas(fld^.Value);
      DbifPutFieldVal(rec^.rh, k, tstr);
    end
    else
      DbifPutFieldVal(rec^.rh, k, '');
  end;
  DbifGetRecord(rec^.rh);
  if dbifGetErrorCode = 0 then
  begin
    GetDataFromRec(rec);
    DB2FindID:= true;
  end
  else
    DB2FindID:= false;

  ChDir(workDir);
end;

function DB2GetNext(rec: PDB2Record): boolean;
var
  k   : integer;
begin
  if rec <> nil then
  begin
    ChDir(dbifDir);

    DbifNextRecord(rec^.rh);
    if DbifGetErrorCode = 0 then
    begin
      GetDataFromRec(rec);
      DB2GetNext:= true;
    end
    else
    begin
      rec^.ClearRec;
      DB2GetNext:= false;
    end;
    ChDir(workDir);
  end;
end;

procedure SetDBIFDir(aDir: PChar);
{- set the directory where dbif should operate from }
begin
  dbifDir:= StrPas(aDir);
  if dbifDir[Length(dbifDir)] = '\' then
    Dec(byte(dbifDir[0]));
  ChDir(dbifDir);
  DbifInit;
  ChDir(workDir);
end;

type
  PTest = ^TTest;
  TTest = object(TObject)
    testID  : array[0..15] of char;
    testCat : array[0..15] of char;
    ud      : boolean;
    drugID  : array[0..15] of char;
  end;

function CheckDupTests(aBatID: PChar): integer;
{- this function returns true if there are *any* duplicate tests in a battery }
{- returns -1 if the batID is invalid,
   0 if no duplicates exist,
   >=1 if duplicate tests exist }
var
  batfh, batDefFH: DbifFileHandle;
  batRH, batDefRH: DbifRecordHandle;
  tstr  : string[80];
  tstr2 : string[80];
  tProc : string[80];
  ret   : integer;

  procedure TrimTrail(var tstr: string);
  {- return src with all trailing blanks removed }
  var
    k   : integer;
  begin
    k:= Length(tstr);
    while (k >= 1) and (tstr[k] = ' ') do
      Dec(k);
    tstr[0]:= Chr(k);
  end;

  procedure TrimTrailP(p: PChar);
  {- return p with all trailing blanks removed }
  var
    tstr  : string;
  begin
    tstr:= StrPas(p);
    TrimTrail(tstr);
    StrPCopy(p, tstr);
  end;

  function SearchDef(batID: string): boolean;
  {- search the battery def for a duplicate test return true if found }
  var
    list  : PCollection;
    s1    : string[80];
    t     : PTest;
    idx   : integer;
    found : boolean;
    UDsExist : boolean;
    tstFH    : DbifFileHandle;
    tstRH    : DbifRecordHandle;
    pstr     : array[0..50] of char;

    function Matches(item: PTest): boolean; far;
    {- return true if the testID in item = the testID in [t]. }
    var
      p2  : array[0..50] of char;
      p   : PChar;
    begin
      if item <> t then
      begin
        if (t^.UD) and (not item^.UD) and (StrComp(item^.testCat, t^.testCat) = 0) and
          ((StrComp(t^.testCat, 'MIC') = 0) or (StrComp(t^.testCat, 'NCCLS') = 0)) then
        begin
          Matches:= (StrComp(t^.drugID, item^.drugID) = 0);
(*          StrCopy(p2, item^.testID);*)

(*          {- Strip 0 or 1 from testID }*)
(*          p:= StrEnd(p2);*)
(*          Dec(p);*)
(*          if (p^ = '1') or (p^ = '0') then*)
(*            p^ := #0;*)
(*          TrimTrailP(p2);*)
(*          Matches:= (StrComp(p2, t^.testID) = 0)*)
        end
        else
          Matches:= (StrComp(item^.testID, t^.testID) = 0)
      end
      else
        Matches:= false;
    end;

  begin
    SearchDef:= false;
    list:= New(PCollection, Init(50, 25));
    batDefFH:= DbifOpenFile('BATDEF', DbifShared);
    batDefRH:= DbifNewRecord(batDefFH);

    tstFH:= DbifOpenFile('TEST', DbifShared);
    tstRH:= DbifNewRecord(tstFH);

    {- fill list with test IDs for battery definition }
(*  DbifFldBatteryDefID = 1;*)
(*  DbifFldBatteryDefTestProc = 2;*)
(*  DbifFldBatteryDefTestID = 3;*)
    DbifPutFieldVal(batDefRH, DbifFldBatteryDefID, batID);
    DbifSearchRecordGE(batDefRH);
    UDsExist:= false;
    while (DbifGetErrorCode = 0) and
          (DbifGetFieldVal(batDefRH, DbifFldBatteryDefID) = batID) do
    begin
      s1:= DbifGetFieldVal(batDefRH, DbifFldBatteryDefTestID);
      t:= New(PTest, Init);
      StrPCopy(t^.testID, s1);

      DbifPutFieldVal(tstRH, DbifFldTestID, s1);
      DbifGetRecord(tstFH);
      if DbifGetErrorCode <> 0 then
      begin
        StrCopy(pstr, 'Cannot find test ID: ');
        StrCat(pstr, t^.testID);
        CriticalError(pstr);
      end;
      s1:= DbifGetFieldVal(tstRH, DbifFldTestDrug);
      StrPCopy(t^.drugID, s1);

      s1:= DbifGetFieldVal(tstRH, DbifFldTestCategory);
      StrPCopy(t^.testCat, s1);

      if DbifGetFieldVal(tstRH, DbifFldTestUDFlag) = 'Y' then
      begin
        UDsExist:= true;
        t^.ud:= true;
      end
      else
        t^.ud:= false;

      list^.Insert(t);

      DbifNextRecord(batDefRH);
    end;


    idx:= 0;
    found:= false;
    if list^.count > 0 then
    begin
      while not found and (idx < list^.count) do
      begin
        t:= list^.At(idx);
        found:= list^.FirstThat(@Matches) <> nil;
        Inc(idx);
      end;
    end;

(*    if not found and UDsExist then*)
(*    begin*)
(*      {- for each UD test. See if there is an existing NCCLS/MIC test that matches*)
(*         it. Taking into account the 0 and 1 that is appended to the system tests }*)
(*      while not found and (idx < list^.count) do*)
(*      begin*)
(*        t:= list^.At(idx);*)
(*        if t^.ud then*)
(*        begin*)
(*        end;*)
(*      end;*)
(*    end;*)

    SearchDef:= found;

    DbifCloseRecord(batDefRH);
    DbifCloseFile(batDefFH);
    DbifCloseRecord(tstRH);
    DbifCloseFile(tstFH);

    Dispose(list, Done);
  end;

begin
  CheckDupTests:= -1;
  if dbifDir = '' then exit;
  ret:= -1;

  ChDir(dbifDir);
  batFH:= DbifOpenFile('BATTERY', DbifShared);
  batRH:= DbifNewRecord(batFH);
  tstr:= StrPas(aBatID);

  DbifPutFieldVal(batRH, DbifFldBatteryID, tstr);
  DbifGetRecord(batFH);
  if dbifGetErrorCode = 0 then
  begin
    if SearchDef(tstr) then
      ret:= 1   {- duplicates }
    else
      ret:= 0;  {- no duplicates }
  end;

  DbifCloseRecord(batRH);
  DbifCloseFile(batFH);

  ChDir(workDir);
  CheckDupTests:= ret;
end;

function Upper(StrToConv: string): string;
var
  P : array[0..255] of char;
begin
  StrPCopy(P, StrToConv);
	AnsiUpper(P);
  Upper:= StrPas(P);
end;

function DB2RenameFile(oldF, newF: string): boolean;
var
  k, j  : integer;
  rh    : DbifRecordHandle;
  fh    : DbifFileHandle;
  tstr  : string;
  isDone: boolean;
  ret   : boolean;
begin
  ret:= false;
  ChDir(dbifDir);
  oldF:= Upper(oldF);
  newF:= Upper(newF);

  for k:= 1 to DbifTotFileEntries do
  begin
    if dbifFileList^[k].fileName = oldF then
    begin
      dbifFileList^[k].fileName:= newF;
      for j:= 1 to dbifTotFieldEntries[k] do
      begin
        dbifFieldList^[k, j].fileName:= newF;
      end;
      if dbifFileList^[k].UDFlag then {- is this a mnemonic file }
      begin
        fh:= DbifOpenFile('MNEMFILE', DbifShared);
        if DbifGetErrorCode = 0 then
        begin
          rh:= DbifNewRecord(fh);
          isDone:= true;
          DbifFirstRecord(rh);
          if DbifGetErrorCode = 0 then
          begin
            ret:= true;
            repeat
              tstr:= DBIFGetFieldVal(rh, DBIFFldMnemFileName);
              if tstr = oldF then
              begin
                DbifPutFieldVal(rh, DBIFFldMnemFileName, newF);
                DbifPutRecord(rh);
                isDone:= false;
                DbifFirstRecord(rh);
              end
              else
              begin
                DbifNextRecord(rh);
                isDone:= DbifGetErrorCode <> 0;
              end;
            until isDone;
          end;
          DbifCloseRecord(rh);
          DbifCloseFile(fh);
        end;
      end
      else
        ret:= true;
    end;
  end;
  RefreshDict;
  ChDir(workDir);
  DB2RenameFile:= ret;
end;

BEGIN
  GetDir(0, workDir);
  dbifDir:= '';
END.

{- rcf }
