unit PrtLPR;

{- long patient report printing module }

INTERFACE

uses
  OWindows;

procedure LongPatReports(aParent: PWindowsObject; useMPSS: boolean);

IMPLEMENTATION

uses
  PrnObj,
  StrsW,
  PrnInfo,
  IntlLib,
  APITools,
  GridLib,
  DMSErr,
  ODialogs,
  DlgLib,
  Strings,
  EngDef,
  EngPrint,
  MScan,
  DBIDS,
  Objects,
  DBLib,
  DBTypes,
  DBFile,
  WinTypes,
  WinProcs;

{$R PRTLPR.RES}
{$I PRTLPR.INC}

type
  PLPRDlg = ^TLPRDlg;
  TLPRDlg = object(TCenterDlg)
    grid    : PGrid;
    useMPSS : boolean;
    constructor Init(aParent: PWindowsObject; aUseMPSS: boolean);
    procedure SetupWindow; virtual;
    procedure PrintIt(preview: boolean);
    procedure IDPrint(var msg: TMessage);       virtual ID_FIRST + IDC_PRINT;
    procedure IDPreview(var msg: TMessage);     virtual ID_FIRST + IDC_PREVIEW;
    procedure WMCharToItem(var msg: TMessage);  virtual WM_FIRST + WM_CHARTOITEM;
    procedure WMDrawItem(var msg: TMessage);    virtual WM_FIRST + WM_DRAWITEM;
    procedure WMMeasureItem(var msg: TMessage); virtual WM_FIRST + WM_MEASUREITEM;
    procedure IDGrid(var msg: TMessage);        virtual ID_FIRST + IDC_GRID;
    procedure IDInfo(var msg: TMessage);        virtual ID_FIRST + IDC_INFO;
    procedure EnableButtons; virtual;
    procedure DisableButtons; virtual;
  end;

constructor TLPRDlg.Init(aParent: PWindowsObject; aUseMPSS: boolean);
begin
  inherited Init(aParent, MakeIntResource(DLG_PRTLPR));
  grid:= New(PGrid, InitResource(@self, IDC_GRID, 1, false));
  useMPSS:= aUseMPSS;
end;

procedure TLPRDlg.SetupWindow;
var
  db    : PDBFile;
  pstr  : array[0..100] of char;
  row   : integer;
begin
  inherited SetupWindow;
  grid^.StretchColumn(0);
  grid^.EnableHeader(false);

  db:= New(PDBFile, Init(DBRPTFmtsFile, '', dbOpenNormal));
  if db <> nil then
  begin
    if db^.dbc^.GetFirst(db^.dbr) then
    begin
      repeat
        db^.dbr^.GetFieldAsStr(DBRPTFmtName, pstr, sizeof(pstr)-1);
        row:= grid^.AddString('');
        grid^.SetData(0, row, pstr);
        grid^.SetItemData(row, db^.dbr^.GetSeqValue);
      until not db^.dbc^.GetNext(db^.dbr);
    end;
    MSDisposeObj(db);
  end
  else
    FatalError('Error', 'Cannot open report format file.');
  grid^.SetSelIndex(0);
end;

procedure TLPRDlg.PrintIt(preview: boolean);
var
  seq     : TSeqNum;
  row     : longint;
begin
  row:= grid^.GetSelIndex;
  if row >= 0 then
  begin
    seq:= grid^.GetItemData(row);
    {- print the report }
    PrintCustomReport(@self, seq, useMPSS, preview);
    FocusCtl(hWindow, IDC_GRID);
  end;
end;

procedure TLPRDlg.EnableButtons;
begin
  EnableWindow(GetItemHandle(IDC_PRINT), grid^.GetRowCount > 0);
  EnableWindow(GetItemHandle(IDC_PREVIEW), grid^.GetRowCount > 0);
  EnableWindow(GetItemHandle(IDC_INFO), True);
  EnableWindow(GetItemHandle(IDCANCEL), True);
end;

procedure TLPRDlg.DisableButtons;
begin
  EnableWindow(GetItemHandle(IDC_PRINT), False);
  EnableWindow(GetItemHandle(IDC_PREVIEW), False);
  EnableWindow(GetItemHandle(IDC_INFO), False);
  EnableWindow(GetItemHandle(IDCANCEL), False);
end;

procedure TLPRDlg.WMCharToItem(var msg: TMessage);
begin
  grid^.CharToItem(msg);
end;

procedure TLPRDlg.WMDrawItem(var msg: TMessage);
begin
  grid^.DrawItem(msg);
end;

procedure TLPRDlg.WMMeasureItem(var msg: TMessage);
begin
  grid^.MeasureItem(msg);
end;

procedure TLPRDlg.IDPrint(var msg: TMessage);
begin
  {- this disables the buttons, which is needed because when gathering data for
     the report, handleevents is called. This allows the user to press buttons
     on the window which is not good (so I disable the buttons.) It will get
     re-enabled in DLGLIB when the window get focus back from the printing process}
  DisableButtons;
  PrintIt(false);
end;

procedure TLPRDlg.IDPreview(var msg: TMessage);
begin
  {- see comment in IDPrint for reason behind the EnableWindow call }
  DisableButtons;
  PrintIt(true);
end;

procedure TLPRDlg.IDGrid(var msg: TMessage);
begin
  DefWndProc(msg);
  if msg.lParamHi = LBN_DBLCLK then
    PrintIt(false);
end;

procedure LongPatReports(aParent: PWindowsObject; useMPSS: boolean);
begin
  application^.ExecDialog(New(PLPRDlg, Init(aParent, useMPSS)));
end;

type
  PInfoDlg = ^TInfoDlg;
  TInfoDlg = object(TCenterDlg)
    grid    : PGrid;
    {}
    constructor Init(aParent: PWindowsObject);
    procedure SetupWindow; virtual;
    procedure WMCharToItem(var msg: TMessage);  virtual WM_FIRST + WM_CHARTOITEM;
    procedure WMDrawItem(var msg: TMessage);    virtual WM_FIRST + WM_DRAWITEM;
    procedure WMMeasureItem(var msg: TMessage); virtual WM_FIRST + WM_MEASUREITEM;
(*    procedure IDTest(var msg: TMessage);        virtual ID_FIRST + IDC_TESTPRINT;*)
  end;

constructor TInfoDlg.Init(aParent: PWindowsObject);
begin
  inherited Init(aParent, MakeIntResource(DLG_INFO));
  grid:= New(PGrid, InitResource(@self, IDC_LIST, 2, false));
end;

procedure TInfoDlg.SetupWindow;
var
  prn   : PPrinter;

  procedure Add(p1, p2: PChar);
  var
    row : integer;
  begin
    row:= grid^.AddString('');
    grid^.SetData(0, row, p1);
    grid^.SetData(1, row, p2);
  end;

  procedure AddNum(p1: PChar; num: longint);
  var
    p2  : array[0..100] of char;
    row : integer;
  begin
    row:= grid^.AddString('');
    grid^.SetData(0, row, p1);
    Str(num, p2);
    grid^.SetData(1, row, p2);
  end;

var
  p1, p2  : array[0..100] of char;
  k, j, i : integer;
  pt, pt2 : TPoint;
  rect    : TRect;
begin
  inherited SetupWindow;
  grid^.SetHeader(0, 'Item');
  grid^.SetHeader(1, 'Value');

  prn:= New(PPrinter, Init);

  Add('Device', prn^.device);
  Add('Driver', prn^.driver);
  Add('Port', prn^.port);
  with prn^.DevSettings^ do
  begin
    Add('Device Name', dmDeviceName);
    Add('Mode version', HexStr(dmSpecVersion, 2, p1, sizeof(p1)-1));
    Add('Driver version', HexStr(dmDriverVersion, 2, p1, sizeof(p1)-1));
  end;

  SelectedPaperSize(prn, k, pt);
  IntlRealToStr(pt.x / 1440, p2, 100);
  StrCopy(p1, 'Width:');
  StrCat(p1, p2);
  IntlRealToStr(pt.y / 1440, p2, 100);
  StrCat(p1, '   Height:');
  StrCat(p1, p2);
  Add('Paper Size', p1);

  PrintMargins(prn, rect);
  IntlRealToStr(rect.left / 1440, p2, 100);
  StrCopy(p1, 'Left:');
  StrCat(p1, p2);
  IntlRealToStr(rect.top / 1440, p2, 100);
  StrCat(p1, '   Top:');
  StrCat(p1, p2);
  IntlRealToStr(rect.right / 1440, p2, 100);
  StrCat(p1, '   Right:');
  StrCat(p1, p2);
  IntlRealToStr(rect.Bottom / 1440, p2, 100);
  StrCat(p1, '   Bottom:');
  StrCat(p1, p2);
  Add('Margins', p1);

  StrCopy(p1, '');
  Orientation(prn, k, j);
  if k = DMORIENT_PORTRAIT then
    StrCopy(p2, 'Portrait')
  else
    StrCopy(p2, 'Landscape');
  StrCat(p1, p2);
  StrCat(p1, '   ---    Relationship: ');
  Str(j, p2);
  StrCat(p1, p2);
  StrCat(p1, '�');
  Add('Orientation', p1);

  MaxMinPrintExtents(prn, pt2, pt);
  StrCopy(p1, 'Min X:');
  IntlRealToStr(pt.x / 10 / 25.4, p2, 100);
  StrCat(p1, p2);
  StrCat(p1, ' Min Y:');
  IntlRealToStr(pt.y / 10 / 25.4, p2, 100);
  StrCat(p1, p2);
  StrCat(p1, '--Max X:');
  IntlRealToStr(pt2.x / 10 / 25.4, p2, 100);
  StrCat(p1, p2);
  StrCat(p1, ' Max Y:');
  IntlRealToStr(pt2.y / 10 / 25.4, p2, 100);
  StrCat(p1, p2);
  Add('Min/Max Extents', p1);


(*    dmDriverVersion: Word;*)
(*    dmSize: Word;ne);*)
(*    dmDriverExtra: Word;*)
(*    dmFields: LongInt;(0);*)
(*    dmOrientation: Integer;*)
(*    dmPaperSize: Integer;*)
(*    dmPaperLength: Integer;*)
(*    dmPaperWidth: Integer;oItem(var msg: TMessage);*)
(*    dmScale: Integer;*)
(*    dmCopies: Integer;);*)
(*    dmDefaultSource: Integer;*)
(*    dmPrintQuality: Integer;*)
(*    dmColor: Integer;DrawItem(var msg: TMessage);*)
(*    dmDuplex: Integer;*)

  MSDisposeObj(prn);

  grid^.OptColumnWidth(0);
  grid^.StretchColumn(1);
end;

type
  PTestPrintOut = ^TTestPrintOut;
  TTestPrintOut = object(TPrintOut)
    function GetDialogInfo(var Pages: Integer): boolean; virtual;
    procedure PrintPage(page: word; var rect: TRect; flags: word); virtual;
  end;

procedure TTestPrintOut.PrintPage(page: word; var rect: TRect; flags: word);
var
  pstr  : array[0..100] of char;
begin
  Rectangle(dc, 0,0, size.x, size.y);
  StrCopy(pstr, 'This is the TOP');
  TextOut(dc, 0, 0, pstr, strlen(pstr));
end;

function TTestPrintOut.GetDialogInfo(var Pages: Integer): boolean;
begin
  GetDialogInfo:= false;
  pages:= 1;
end;

(*procedure TInfoDlg.IDTest;*)
(*var*)
(*  prn   : PPrinter;*)
(*  po    : PTestPrintOut;*)
(*begin*)
(*  prn:= New(PPrinter, Init);*)
(*  po:= New(PTestPrintOut, Init('Test Print'));*)
(*  prn^.Print(@self, po);*)
(*  Dispose(po, Done);*)
(*  Dispose(prn, Done);*)
(*end;*)

procedure TInfoDlg.WMCharToItem(var msg: TMessage);
begin
  grid^.CharToItem(msg);
end;

procedure TInfoDlg.WMDrawItem(var msg: TMessage);
begin
  grid^.DrawItem(msg);
end;

procedure TInfoDlg.WMMeasureItem(var msg: TMessage);
begin
  grid^.MeasureItem(msg);
end;

procedure TLPRDlg.IDInfo(var msg: TMessage);
var
  dlg   : PInfoDlg;
begin
  dlg:= New(PInfoDlg, Init(@self));
  application^.ExecDialog(dlg);
end;




END.

{- rcf }
