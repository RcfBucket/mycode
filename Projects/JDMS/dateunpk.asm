.MODEL large, pascal
public  DateUnpk
;
;       New DateUnpack with generic dates
;
;       December 13, 1987       CPD
;
;        ASSUME  CS:CODE,DS:DATA,ES:NOTHING
.DATA

        extrn    ddlm   : byte
        extrn    didx   : byte
        extrn    dbgn   : byte
        extrn    dend   : byte
        extrn    dprev  : byte
        extrn    dthis  : byte
        extrn    dnext  : byte
        extrn    dday   : byte
        extrn    dmon   : byte
        extrn    dqtr   : byte
        extrn    dyr    : byte

.CODE

extrn   unjulian : near
;
;       Procedure Date_Unpack(     DateIn  : Integer;
;                              VAR DateOut : String;
;                                  FourDig : Boolean );
;
;       Convert a packed date into an unpacked date.
;       If fourDigit is true, convert the year as four digits.
;
DateIn  Equ     word Ptr [bp+10]
DateOut Equ     dword Ptr [bp+6]
FourDig Equ     word Ptr [bp+4]
PSize   Equ     8
IsZero  Equ     word Ptr [bp-2]
DateVal Equ     word Ptr [bp-8]
DayNum  Equ     word Ptr [bp-8]
MonNum  Equ     word Ptr [bp-6]
YrNum   Equ     word Ptr [bp-4]
LSize   Equ     8
;
BitBgn  Equ     00000001B       ; mask for begining
BitEnd  Equ     00000010B       ; mask for ending
BitPrev Equ     00000100B       ; mask for previous
BitThis Equ     00001000B       ; mask for this
BitNext Equ     00010000B       ; mask for next
BitMon  Equ     00100000B       ; mask for month
BitQtr  Equ     01000000B       ; mask for quarter
BitYr   Equ     10000000B       ; mask for year
;
DateUnpk Proc near
        push   bp              ; setup stack frame
        mov    bp,sp
        sub    sp,lsize

        sub    ax,ax           ; clear zero flag
        mov    IsZero,ax
        mov    YrNum,ax        ; clear year (so DIV DL can't o'flow)

        les    di,DateOut      ; get string ptr
        cld
        stosb                   ; start string as nil

        mov     bx,DateIn       ; get date to convert
        or      bh,bh           ; check for 0, 00xx, or xxxx
        jnz     normal          ; normal date
        or      bl,bl
        jnz     special         ; genric date
        inc     IsZero          ; 0 date
        jmp     short normal
;
;       Date to unpack is a generic.  Return the chr sequence
;       which produces it.
;
special:
        mov     bh,1            ; test all bits
        xor     si,si           ; chrs are stored to match bits (almost)
sp_loop:
        test    bl,bh
        jz      nxt_bit         ; this bit is clear
        mov     al, dbgn[si]    ; get the special char
        stosb                   ; save it
nxt_bit:
        shl     bh,1
        cmp     bh,BitMon       ; if we are at mon,
        jnz     not_mon
        inc     si              ; inc SI to skip ?dday
not_mon:
        inc     si              ; pt to next chr
        or      bh,bh           ; all bits tested?
        jnz     sp_loop         ; no, loop more
        test    bl,BitMon+BitQtr+BitYr  ; Month, Qtr or Year?
        jnz     not_day
        mov     al, dday        ; save chr for day
        stosb
not_day:
        jmp     du_exit

;
;       Date to unpack is a normal date or 0.  Convert to ascii
;       according to  didx, Fourdigit and IsZero
;
;       First, convert to year,month,day by calling UnJulian.
;       Offset is 2386009(246859H)
;
normal:
        or      bx,bx           ; 0 date?
        jz      convert         ; yes, no need to unjulian
        add     bx,6859H        ; form julian
        adc     ax,24h          ; in AXBX
        push    ax
        push    bx              ; on stack
        push    ss              ; *year
        lea     bx,YrNum
        push    bx
        push    ss              ; *mon
        lea     bx,MonNum
        push    bx
        push    ss              ; *day
        lea     bx,DayNum
        push    bx
        call    unjulian
;
;       DayNum, MonNum,YrNum now contain integer values
;       (or date is 0)
;
convert:
        les     di,DateOut      ; set ptr to dest string
        inc     di
        xor     si,si           ; major loop counter init
cv_loop:
        mov     bl,byte ptr  didx[si]   ; get value for this position
        xor     bh,bh
        shl     bx,1
        xchg    si,bx           ; major index in BX, ptr to value in SI
        mov     ax,DateVal[si]  ; value for this field
        cmp     si,4            ; was this the year?
        jnz     sav_num         ; no, only 2 digits
        mov     dl,100
        div     dl              ; year div 100 in al, mod 100 in ah
        test    FourDig,1       ; all four digits wanted?
        jz      do_year         ; no, do just 2
        call    save_2          ; save century
do_year:
        mov     al,ah

sav_num:
        call    save_2          ; save two digits from AL
        inc     bx              ; inc major loop
        cmp     bx,3            ; all done?
        jz      du_exit
        mov     si,bx           ; save in SI
        mov     al, ddlm
        stosb                   ; save delimitter
        jmp     cv_loop         ; and loop again
;
;       AL contains number to save.  Save as two digits, preserve
;       all registers but DI (which is updated) and DX.
;       If IsZero, save **
;
save_2:
        push    ax
        test    IsZero,1        ; 0 date?
        jz      s_1             ; no
        mov     ax,'**'         ; save **
        jmp     short s_2
s_1:
        sub     ah,ah
        mov     dl,10
        div     dl              ; first digit in al, second in ah
        add     ax,'00'         ; both now ascii
s_2:
        stosb                   ; save first
        mov     al,ah           ; and second
        stosb
        pop     ax
        ret

du_exit:
        mov     ax,di
        mov     di,word ptr DateOut     ; compute string length
        sub     ax,di
        dec     ax
        stosb                   ; save in string[0]

        mov     sp,bp           ; cleanup & exit
        pop     bp
        ret     PSize
DateUnpk Endp

        END

