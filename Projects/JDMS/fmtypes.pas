unit FMTypes;

INTERFACE


uses
  Objects,
  OWindows,
  WinTypes,
  WinDos,
  status,
  MPSS,
  FMConst,
  FMError,
  ApiTools,
  DBIDS,
  DBTypes,
  DBLib,
  DBFile,
  MScan,
  ODialogs,
  DlgLib,
  UIFlds,
  ListLib,
  CtlLib,
  DMString,
  DMSErr,
  userMsgs,
  Strings;

{$I FMUTIL.INC}

Type

  PDlgMergeError = ^TDlgMergeError;
  TDlgMergeError = object(TCenterDlg)
    {}
    specID       : PStatic;
    SpecCollDt   : PStatic;
    dupSpec      : PStatic;
    specIDEntry  : PUIFld;
    listObj      : PListObject;
    dupStr       : array [0..350] of char;
    disDupStr    : array [0..350] of char;
    dupSubstr    : PErrSubst;
    onLnPatDB    : PDBReadWriteFile;
    onLnSpecDB   : PDBReadWriteFile;
    mergePatDB   : PDBReadWriteFile;
    mergePatRef  : TSeqNum;
    onLnPatRef   : TSeqNum;
    mergeSpecRec : PDBRec;
    mergeDir     : array[0..maxPathLen] of char;
    onLnDir      : array[0..maxPathLen] of char;
    constructor Init(AParent: PWindowsObject; aDlg: PChar; aMergePatRef,
            aOnLnPatRef: TSeqNum; aDBr: PDBRec; aMergeDir, aOnLnDir: PChar);
    destructor  Done; virtual;
    procedure   SetupWindow; virtual;
    function    CanClose: boolean; virtual;
    procedure   OK(var msg: TMessage); virtual ID_FIRST + IDOK;
  end;

  PDlgDiskComp = ^TDlgDiskComp;
  TDlgDiskComp = object(TCenterDlg)
    {}
    constructor Init(AParent: PWindowsObject; aDlg: PChar);
    destructor  Done; virtual;
    procedure   SetupWindow; virtual;
    function    CanClose: boolean; virtual;
  end;

  PDlgFMStatus = ^TDlgFMStatus;
  TDlgFMStatus = object(TGenStatDlg)
    pctColor    : TColorRef;
    textColor   : TColorRef;
    pctLow      : array[1..2] of integer;
    pctHigh     : array[1..2] of integer;
    pctCurrent  : array[1..2] of integer;
    pctOld      : array[1..2] of integer;
    displayPct  : boolean;
    percentBar  : integer;
    pctR        : array[1..2] of TRect;
    {}
    constructor Init(AParent: PWindowsObject; DrawTxt: boolean);
    destructor  Done; virtual;

    procedure   SetupWindow; virtual;
    procedure   ProcessDlg(var msg: TMessage); virtual WM_FIRST + WM_FMPROCESS;
    function    CanClose: boolean; virtual;
    procedure   Cancel(var msg: TMessage); virtual ID_FIRST + IDCANCEL;
    procedure   AddPctLevel(pctLevel: integer; line : integer);
    procedure   IncrPctLevel(pctLevel: integer; line : integer);
    procedure   CompPctLevel(cur: longint; total: longint; line : integer);
    procedure   ResetPct(line : integer);
    private
    procedure   WMPaint(var msg: TMessage); virtual $0088;
    procedure   SetDefaults;
    procedure   DrawPct(line : integer);
    procedure   DrawPercent(line : integer);
    procedure   DrawPctText(line : integer);
  end;

  PArchFile = ^TArchfile;
  TArchFile = object(TObject)
    fileName     : array[0..max8dot3] of char;
    numRecords   : longint;
    archiveFlag  : integer;
    Constructor Init(newFName : PChar; newRecNum : longint; newArcFlag : integer);
    destructor  Done; virtual;
  end;


  PSeqNumCollection = ^TSeqNumCollection;
  TSeqNumCollection = Object(TSortedCollection)
    function KeyOf(Item: Pointer): Pointer; virtual;
    function Compare(Key1, Key2: Pointer): Integer; virtual;
  end;

  PAssocEntry  = ^TAssocEntry;
  TAssocEntry = object(TObject)
    fieldNum : integer;
    fileName : array[0..maxFileNameLen] of char;
    Constructor Init(newFNum : integer; newFName : PChar);
    destructor  Done; virtual;
  end;

  PSeqNumEntry  = ^TSeqNumEntry;
  TSeqNumEntry = object(TObject)
    oldSeqNum : TSeqNum;
    newSeqNum : TSeqNum;
    Constructor Init(sNum : TSeqNum);
    destructor  Done; virtual;
  end;

  PFileNameEntry  = ^TFileNameEntry;
  TFileNameEntry = object(TObject)
    fileName    : array[0..maxFileNameLen] of char;
    assocList   : PCollection;
    seqNumList  : PSeqNumCollection;
    Constructor Init(newFName : PChar; newAList : PCollection;
                     newSList : PSeqNumCollection);
    destructor  Done; virtual;
  end;

  PCompressFileEntry  = ^TCompressFileEntry;
  TCompressFileEntry = object(TObject)
    fileName    : array[0..maxFileNameLen] of char;
    numRecords  : longint;
    Constructor Init(newFName : PChar; newRec : longint);
    destructor  Done; virtual;
  end;

  PUserDefineEntry  = ^TUserDefineEntry;
  TUserDefineEntry = object(TObject)
    fileName : array[0..maxFileNameLen] of char;
    Constructor Init(newUDFName : PChar);
    destructor  Done; virtual;
  end;

  xref  = record
    fieldNum       : integer;
    seqCollection  : PSeqNumCollection;
  end;

  PBackupRec = ^TBackupRec;
  TBackupRec = Record
    BType        : char;
    BIdent       : integer;
    status       : byte;
    dt           : TDateTime;
    NumDisks     : integer;
    numBytes     : longint;
    incrDt       : TDateTime;
    NumIncr      : integer;
    numIncrBytes : longint;
  End;

var
  diskspace    : longint;
  diskSizeCt   : longInt;
  totalSpace   : longint;
  totalCt      : longInt;
  totalRecords : longint;
  fmErr_Msg    : array[0..255] of char;
  globalSeq    : TSeqNum;

IMPLEMENTATION

uses
  WinProcs;

{----------------------[TDlgMergeError]-------------------------------------}
{---------------------------------------------------------------------------}
{                                                                           }
{---------------------------------------------------------------------------}
constructor TDlgMergeError.Init(aParent      : PWindowsObject;
                                aDlg         : PChar;
                                aMergePatRef,
                                aOnLnPatRef  : TSeqNum;
                                aDBr         : PDBRec;
                                aMergeDir,
                                aOnLnDir     : PChar);
var
  pstr       : array [0..255] of char;

begin
  inherited Init(aParent, aDlg);
  mergePatRef  := aMergePatRef;
  onLnPatRef   := aOnLnPatRef;
  mergeSpecRec := aDBr;
  StrCopy(mergeDir, aMergeDir);
  StrCopy(onLnDir, aOnLnDir);
  dupSpec    := New(PStatic, InitResource(@Self, IDC_MERGE_DUP, 350));
  specID     := New(PStatic, InitResource(@Self, IDC_MERGE_SPECID, 25));
  specCollDt := New(PStatic, InitResource(@Self, IDC_MERGE_COLLDT, 25));

  listObj := New(PListObject, Init);

  onLnPatDB := New(PDBReadWriteFile, Init(DBPATFile, onLnDir, dbOpenNormal));
  if (onLnPatDB = nil) then
  begin
    StrCopy(fmErr_Msg,onLnDir);
    StrCat(fmErr_Msg,DBPATFile);
    FMShowError(@Self, SR(IDS_MERGEMAINMSG, pstr, 255), fmErr_OpeningFile, fmErr_Msg);
  end;

  onLnSpecDB := New(PDBReadWriteFile, Init(DBSPECFile, onLnDir, dbOpenNormal));
  if (onLnSpecDB = nil) then
  begin
    StrCopy(fmErr_Msg,onLnDir);
    StrCat(fmErr_Msg,DBSPECFile);
    FMShowError(@Self, SR(IDS_MERGEMAINMSG, pstr, 255), fmErr_OpeningFile, fmErr_Msg);
  end;

  mergePatDB := New(PDBReadWriteFile, Init(DBPATFile, mergeDir, dbOpenNormal));
  if (mergePatDB = nil) then
  begin
    StrCopy(fmErr_Msg,mergeDir);
    StrCat(fmErr_Msg,DBPATFile);
    FMShowError(@Self, SR(IDS_MERGEMAINMSG, pstr, 255), fmErr_OpeningFile, fmErr_Msg);
  end;

  specIDEntry := New(PUIFld, Init(@self, onLnSpecDB^.dbd, listObj, TRUE,
                     DBSPECSpecID, IDC_SPECIMENID_ENTRY, 0));
end;

destructor TDlgMergeError.Done;
begin

  if (specIDEntry <> nil) then
    Dispose(specIDEntry, Done);

  if (onLnPatDB <> nil) then
    Dispose(onLnPatDB, Done);
  if (onLnSpecDB <> nil) then
    Dispose(onLnSpecDB, Done);
  if (mergePatDB <> nil) then
    Dispose(mergePatDB, Done);

  if (dupSpec <> nil) then
    Dispose(dupSpec, Done);

  if (specID <> nil) then
    Dispose(specID, Done);

  if (specCollDt <> nil) then
    Dispose(specCollDt, Done);

  inherited Done;
end;


procedure TDlgMergeError.SetupWindow;
var
  pstr       : array [0..255] of char;
  tmp        : integer;
  mergePatId : array [0..25] of char;
  onLnPatId  : array [0..25] of char;
  onLnSpecId : array [0..25] of char;
  onLnCollDt : array [0..25] of char;
  errNum     : integer;

begin
  inherited SetupWindow;

(*  tmp := dupSpec^.GetText(dupStr, 350);*)

  if onLnPatRef <> 0 then
    if onLnPatDB^.dbc^.GetSeq(onLnPatDB^.dbr, onLnPatRef) then
      onLnPatDB^.dbr^.GetFieldAsStr(DBPATPatID, onLnPatId, 25)
    else
      errNum := onLnPatDB^.dbc^.dbErrorNum
  else
    StrCopy(onLnPatId, '000000000000');

  mergeSpecRec^.GetFieldAsStr(DBSPECSpecID, onLnSpecId, 25);
  mergeSpecRec^.GetFieldAsStr(DBSPECCollectDate, onLnCollDt, 25);


  if mergePatRef <> 0 then
    if mergePatDB^.dbc^.GetSeq(mergePatDB^.dbr, mergePatRef) then
      mergePatDB^.dbr^.GetFieldAsStr(DBPATPatID, mergePatId, 25)
    else
      errNum := mergePatDB^.dbc^.dbErrorNum
  else
    StrCopy(mergePatId, '000000000000');


(*  DupSubstr := New(PErrSubst, Init(onLnPatId));*)
(*  DupSubstr^.AddSubstStr(mergePatId);*)
(*  WVSPrintf(disDupStr, dupStr, DupSubstr^.strs);*)
(*  dupSpec^.SetText(disDupStr);*)

  specID^.SetText(onLnSpecId);
  specCollDt^.SetText(onLnCollDt);

end;

function TDlgMergeError.CanClose: boolean;
begin
  CanClose:= inherited CanClose;
end;

procedure TDlgMergeError.OK(var msg: TMessage);
var
  errNum : integer;

begin
  if not specIDEntry^.IsEmpty then
  begin
    onLnSpecDB^.dbr^.CopyRecord(mergeSpecRec);
    specIDEntry^.Format;
    specIDEntry^.XFerCtlToRec(onLnSpecDB^.dbr);

    if onLnSpecDB^.dbc^.InsertRec(onLnSpecDB^.dbr) then
      globalSeq := onLnSpecDB^.dbr^.GetSeqValue
    else
      errNum := onLnSpecDB^.dbc^.dbErrorNum;

    inherited OK(msg);
  end;
end;

{----------------------[TDlgDiskComp]---------------------------------------}
{---------------------------------------------------------------------------}
{                                                                           }
{---------------------------------------------------------------------------}
constructor TDlgDiskComp.Init(aParent: PWindowsObject; aDlg: PChar);
begin
  inherited Init(aParent, aDlg);
end;

destructor TDlgDiskComp.Done;
begin
  inherited Done;
end;

procedure TDlgDiskComp.SetupWindow;
begin
  inherited SetupWindow;
end;

function TDlgDiskComp.CanClose: boolean;
begin
  CanClose:= inherited CanClose;
end;


Constructor TArchFile.Init(newFName : PChar; newRecNum : longint; newArcFlag : integer);
begin
  inherited Init;
  StrCopy(fileName, newFName);
    numRecords := newRecNum;
    archiveFlag := newArcFlag;
end;

destructor  TArchFile.Done;
begin
  inherited Done;
end;

Constructor TAssocEntry.Init(newFNum : integer; newFName : PChar);
begin
  inherited Init;
  fieldNum := newFNum;
  StrCopy(fileName,newFName);
end;

destructor  TAssocEntry.Done;
begin
  inherited Done;
end;

Constructor TUserDefineEntry.Init(newUDFName : PChar);
begin
  inherited Init;
  StrCopy(fileName,newUDFName);
end;

destructor  TUserDefineEntry.Done;
begin
  inherited Done;
end;

Constructor TSeqNumEntry.Init(sNum : TSeqNum);
begin
  inherited Init;
  oldSeqNum  := sNum;
  newSeqNum  := sNum
end;

destructor  TSeqNumEntry.Done;
begin
  inherited Done;
end;

Constructor TFileNameEntry.Init(newFName : PChar; newAList : PCollection;
                                newSList : PSeqNumCollection);
begin
  inherited Init;
  StrCopy(fileName,newFName);
  assocList   := newAList;
  seqNumList  := newSList;
end;

destructor  TFileNameEntry.Done;
begin
  inherited Done;
end;

Constructor TCompressFileEntry.Init(newFName : PChar; newRec : longint);
begin
  inherited Init;
  StrCopy(fileName,newFName);
  numRecords := newRec;
end;

destructor  TCompressFileEntry.Done;
begin
  inherited Done;
end;

function TSeqNumCollection.KeyOf(Item: Pointer): Pointer;
begin
  KeyOf := Pointer(PSeqNumEntry(Item)^.oldSeqNum);
end;

function TSeqNumCollection.Compare(Key1, Key2: Pointer): Integer;
begin
  if TSeqNum(Key1) < TSeqNum(Key2) then
    Compare := -1
  else if TSeqNum(Key1) = TSeqNum(Key2) then
    Compare := 0
  else
    Compare := 1;
end;

{---------------------------------------------------------------------------}
{                      TDlgFMStatus Routines                                  }
{---------------------------------------------------------------------------}
{-------------------------[Init]--------------------------------------------}
{----------------------[TDlgFMStatus]-----------------------------------------}
{---------------------------------------------------------------------------}
{                                                                           }
{---------------------------------------------------------------------------}
constructor TDlgFMStatus.Init(AParent: PWindowsObject; DrawTxt: Boolean);
begin
  inherited Init(AParent, MakeIntResource(DLG_BACKUP));
  DisplayPct:= DrawTxt;
end;

{----------------------------[SetupWindow]------------------------------------}
procedure TDlgFMStatus.SetupWindow;
begin
  inherited SetupWindow;
  SetDefaults;
  DrawPct(1);
  DrawPct(2);
  PostMessage(HWindow, WM_FMPROCESS, 0, 0);
end;

{----------------------------[ProcessDlg]------------------------------------}
procedure TDlgFMStatus.ProcessDlg(var msg: TMessage);
begin
end;

destructor TDlgFMStatus.Done;
begin
  inherited Done;
end;

function TDlgFMStatus.CanClose: boolean;
begin
  CanClose:= inherited CanClose;
end;


procedure TDlgFMStatus.Cancel(var Msg: TMessage);
var
  cancelMsg  : array [0..255] of char;
  confirmMsg : array [0..255] of char;

begin
  MessageBeep(MB_ICONQUESTION);
  SR(IDS_CONFIRM, confirmMsg,255);
  SR(IDS_CANCELMSG, cancelMsg, 255);
  if YesNoMsg(hwindow, confirmMsg, cancelMsg) then
    SetContinue(false);
end;

{---------------------------[SetDefaults]-------------------------------------}
procedure TDlgFMStatus.SetDefaults;
var
  DC     : HDC;
  Point  : TPoint;
  DlgR   : TRect;
  count  : integer;
begin
  for count := 1 to 2 do
  begin
    pctLow[count]:= 0;
    pctHigh[count]:= 100;
    pctCurrent[count]:= PctLow[count];
    pctOld[count]:= -1;
  end;

  GetClientRect(HWindow,DlgR);
  Point.X:= DlgR.left; Point.Y:= DlgR.top;
  ClientToScreen(HWindow,Point);
  DlgR.left:= Point.X; DlgR.top:= Point.Y;
  Point.X:= DlgR.right; Point.Y:= DlgR.bottom;
  ClientToScreen(HWindow,Point);
  DlgR.right:= Point.X; DlgR.bottom:= Point.Y;

  for count := 1 to 2 do
  begin
    GetWindowRect(GetDlgItem(HWindow, 201+count), PctR[count]);
    with PctR[count] do
    begin
      top:= top - DlgR.top;
      bottom:= bottom - DlgR.top;
      left:= left - DlgR.left;
      right:= right - DlgR.left;
    end;
  end;
  PctColor:= RGB(64,64,64);
  TextColor:= RGB(0,0,128);
end;

{---------------------------[incrPctLevel]-----------------------------------}
procedure   TDlgFMStatus.IncrPctLevel(PctLevel : integer; line : integer);
begin
  PctCurrent[line]:= PctLevel;
  if PctCurrent[line] > PctHigh[line] then
    PctCurrent[line]:= PctHigh[line];
  if PctCurrent[line] < PctLow[line] then
    PctCurrent[line]:= PctLow[line];
  Update;
  DrawPct(line);
end;

{---------------------------[compPctLevel]-----------------------------------}
procedure TDlgFMStatus.CompPctLevel(cur: longint; total: longint; line : integer);
begin
  PctCurrent[line]:= integer(round((cur/total) * 100));
  if PctCurrent[line]>PctHigh[line] then
    PctCurrent[line]:=PctHigh[line];
  if PctCurrent[line]<PctLow[line] then
    PctCurrent[line]:=PctLow[line];
  Update;
  DrawPct(line);
end;

{---------------------------[AddPctLevel]-------------------------------------}
procedure TDlgFMStatus.AddPctLevel(PctLevel: integer; line : integer);
begin
  PctCurrent[line]:=PctCurrent[line]+PctLevel;
  if PctCurrent[line] > PctHigh[line] then
    PctCurrent[line]:= PctHigh[line];
  if PctCurrent[line] < PctLow[line] then
    PctCurrent[line]:= PctLow[line];
  Update;
  DrawPct(line);
end;

{---------------------------[ResetPct]----------------------------------------}
procedure TDlgFMStatus.ResetPct(line : integer);
begin
  PctCurrent[line]:= 0;
  if PctCurrent[line] > PctHigh[line] then
    PctCurrent[line]:= PctHigh[line];
  if PctCurrent[line] < PctLow[line] then
    PctCurrent[line]:= PctLow[line];
  Update;
  DrawPct(line);
end;

{---------------------------[DrawPct]-----------------------------------------}
procedure TDlgFMStatus.DrawPct(line : integer);
begin
  if PctOld[line] <> PctCurrent[line] then
  begin
    PctOld[line]:= PctCurrent[line];
    DrawPercent(line);
    if DisplayPct then
      DrawPctText(line);
  end;
  if PctCurrent[line] = PctLow[line] then
    DrawPercent(line);
end;

{---------------------------[DrawPercent]-------------------------------------}
procedure TDlgFMStatus.DrawPercent(line : integer);
var
 InR      : TRect;
 OutR     : TRect;
 TempR    : TRect;
 PaintDC  : HDC;
 TheBrush : HBrush;
 OldBrush : HBrush;
 ThePen   : HPen;
 OldPen   : HPen;
 MemDC    : HDC;
 TheBits  : HBitmap;
 OldBits  : HBitmap;

begin
  TempR:= PctR[line];
  TempR.right:= TempR.right-TempR.left;
  TempR.left:= 0;
  TempR.bottom:= TempR.bottom-TempR.top;
  TempR.top:= 0;
  InR:= TempR;
  OutR:= TempR;
  InflateRect(InR, -1, -1);
  InflateRect(OutR, -1, -1);
  InR.bottom:= InR.bottom + 1;
  InR.right:= InR.right + 1;
  OutR.bottom:= OutR.bottom - 1;
  if (PctCurrent[line]-PctLow[line]) <> 0 then
    InR.left:=InR.left + integer(Trunc((InR.right-InR.left) *
    ((PctCurrent[line] - PctLow[line]) / (PctHigh[line] - PctLow[line]))));

  OutR.right:= InR.left + 1;
  PaintDC:= GetDC(HWindow);
  MemDC:= CreateCompatibleDC(PaintDC);
  TheBits:= CreateCompatibleBitmap(PaintDC, TempR.right, TempR.bottom);
  OldBits:= SelectObject(MemDC, TheBits);

  TheBrush:= GetStockObject(NULL_BRUSH);
  OldBrush:= SelectObject(MemDC, TheBrush);
  ThePen:= CreatePen(PS_SOLID, 1, GetSysColor(COLOR_WINDOWFRAME));
  OldPen:= SelectObject(MemDC, ThePen);
  Rectangle(MemDC, TempR.left, TempR.top, TempR.right, TempR.bottom);
  SelectObject(MemDC, OldBrush);
  DeleteObject(TheBrush);
  SelectObject(MemDC, OldPen);
  DeleteObject(ThePen);

  if (PctCurrent[line] <> PctHigh[line]) then
  begin
    TheBrush:= CreateSolidBrush($00C0C0C0);
    OldBrush:= SelectObject(MemDC,TheBrush);
    ThePen:= GetStockObject(Null_Pen);
    OldPen:= SelectObject(MemDC,ThePen);
    Rectangle(MemDC, InR.left, InR.top, InR.right, InR.bottom);
    InR.right:= InR.right - 2;
    InR.bottom:= InR.bottom - 2;
    InflateRect(InR, -2, -2);
    DrawInFrame(MemDC, InR, true, 1);
    InflateRect(InR, 2, 2);
    InR.right:= InR.right + 2;
    InR.left:= InR.left + 1;
    InR.bottom:= InR.bottom + 2;
    SelectObject(MemDC, OldBrush);
    DeleteObject(TheBrush);
    SelectObject(MemDC, OldPen);
    DeleteObject(ThePen);
  end;

  if PctCurrent[line] <> PctLow[line] then
  begin
    if OutR.right > (TempR.right - 2) then
      OutR.right:= TempR.right - 2;
    DrawOutFrame(MemDC, OutR, true, OutWidth);
  end;

  BitBlt(PaintDC, PctR[line].left, PctR[line].top, TempR.right, TempR.bottom, MemDC, 0, 0, SRCCOPY);
  SelectObject(MemDC, OldBits);
  DeleteObject(TheBits);
  ReleaseDC(HWindow, PaintDC);
  DeleteDC(MemDC);
end;

{---------------------------[DrawPctText]-------------------------------------}
procedure TDlgFMStatus.DrawPctText(line : integer);
var
  paintR   : TRect;
  buffer   : array[0..10] of char;
  extent   : longint;
  paintDC  : HDC;
begin
  PaintDC:= GetDC(HWindow);
  SetTextAlign(PaintDC,TA_TOP or TA_LEFT);
  SetBkMode(PaintDC, Transparent);
  SetTextColor(PaintDC, TextColor);
  Str(PctCurrent[line], buffer);
  StrCat(buffer, '%');
  extent:= GetTextExtent(PaintDC, buffer, StrLen(buffer));
  TextOut(PaintDC,
      PctR[line].left + ((PctR[line].right-PctR[line].left-Loword(Extent)) div 2),
      PctR[line].top  + ((PctR[line].bottom-PctR[line].top-Hiword(Extent)) div 2),
      buffer, StrLen(buffer));
  ReleaseDC(HWindow,PaintDC);
end;

{---------------------------[WMPaint]-----------------------------------------}
procedure TDlgFMStatus.WMPaint(var Msg:TMessage);
var
  count : integer;
begin
  for count := 1 to 2 do
  begin
    PctOld[count]:= -1;
    DrawPct(count);
  end;
end;

{----------------------------------------------------------------------------}
{  Takes a field type and size, and returns a user-recognizable field type   }
{  string. All field types are mapped into either a user-recognizable field  }
{  type string, or to "Internal".                                            }
{                                                                            }
{  Returns : A user-recognizable field type string.                          }
{  InParms : aType     - Field type value.                                   }
{            aSize     - Field size.                                         }
{            aMnemonic - True if mnemonic field, otherwise false.            }
{            maxLen    - Maximum length of return string.                    }
{  OutParms: tStr      - Placeholder for return value.                       }
{----------------------------------------------------------------------------}
function FMUserTypeStr(aType: TDBFldTypes; aSize: word; aMnemonic: boolean;
                       tStr: PChar; maxLen: integer): PChar;
var
  p   : array [0..25] of char;
  p2  : array [0..25] of char;
begin
  case aType of
    dbInteger,
    dbLongInt,
    dbWord,
    dbByte,
    dbShortInt  : SR(IDS_FT_INT, p, 20);
    dbSingle,
    dbDouble    : SR(IDS_FT_NUM, p, 20);
    dbChar      : begin
                    SR(IDS_FT_STR, p, 20);
                    StrCat(p, '[1]');
                  end;
    dbString,
    dbCode,
    dbZString,
    dbZCode     : begin
                    Str(aSize - 1, p2);
                    SR(IDS_FT_STR, p, 20);
                    StrCat(p, '[');
                    StrCat(p, p2);
                    StrCat(p, ']');
                  end;
    dbDate      : SR(IDS_FT_DATE, p, 20);
    dbTime      : SR(IDS_FT_TIME, p, 20);
    dbSeqRef    : begin
                    if aMnemonic then
                      SR(IDS_FT_MNEM, p, 20)
                    else
                      SR(IDS_FT_INTERNAL, p, 20);
                  end;
    dbBoolean,
    dbBlock,
    dbAutoInc   : SR(IDS_FT_INTERNAL, p, 20);
    else
      StrCopy(p, 'Invalid');
  end;
  StrLCopy(tstr, p, maxLen);
  FMUserTypeStr := tstr;
end;

END.

{- DWC }

