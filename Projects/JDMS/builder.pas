Unit Builder;

Interface

Uses
  WinTypes,
  Objects,
  Strings,
  WinCrt,
  IntGlob,
  WinProcs;

const
	FieldDel     = '|';
  RepeatDel    = '\';
  ComponentDel = '^';
  EscapeDel    = '&';

procedure Builder_PutChrField(FieldChar: char);
procedure Builder_PutStrField(FieldString: PChar);
procedure Builder_PutChr(FieldChar: char);
procedure Builder_PutField(FieldString: PChar);
procedure Builder_PutNulFields(NoOfNulFields: integer);
procedure Builder_PutEndRecord;
procedure Builder_ClearRecord;

Implementation

procedure Builder_PutChr(FieldChar: char);
var
	tstr: array[0..1] of char;

begin
	tstr[0] := FieldChar;
	tstr[1] := #0;
  StrCat(SingleRecord, tstr);
end;

procedure Builder_PutChrField(FieldChar: char);
begin
	Builder_PutChr(FieldDel);
  Builder_PutChr(FieldChar);
end;

procedure Builder_PutStrField(FieldString: PChar);
begin
	Builder_PutChr(FieldDel);
  Builder_PutField(FieldString);
end;

procedure Builder_PutField(FieldString: PChar);
begin
  StrCat(SingleRecord, FieldString);
end;

procedure Builder_PutNulFields(NoOfNulFields: integer);
var
  i: integer;

begin
	if NoOfNulFields > 0 then
  	for i := 1 to NoOfNulFields do
			Builder_PutChr(FieldDel);
end;

procedure Builder_PutEndRecord;
var
	tstr: array[0..2] of char;

begin
	Builder_PutChr(#13);
  GetPrivateProfileString('IntCstm', 'EoRec', '', tstr, SizeOf(tstr), 'IntCstm.ini');
  if StrComp(tstr, '10') = 0 then
		Builder_PutChr(#10);
end;


procedure Builder_ClearRecord;
begin
  StrCopy(SingleRecord, '');
end;

Begin
End.
