unit UIResult;

INTERFACE

uses
  reslib,
  Objects,
  ListLib,
  GridLib,
  DBTypes,
  gather,
  oWindows,
  wintypes,
  winprocs;

procedure EditIDResult(aParent: PWindowsObject; aRes: PGatherRes; idx: integer;
                       grid: PGrid; resCol: integer);
procedure EditMICResult(aParent: PWindowsObject; aRes: PGatherRes; idx: integer;
                       grid: PGrid; MICCol, InterpCol: integer);

IMPLEMENTATION

uses
  APITools,
  CtlLib,
  DBIDS,
  ESBL,
  Trays,
  DBLib,
  DMSErr,
  IntrpMap,
  DlgLib,
  MScan,
  ODialogs,
  Strings,
  StrsW,
  UIFlds;

{$R UIRESULT.RES}
{$I UIRESULT.INC}


{----------------------------------------------[ Result Editing Procedures ]--}

type
  PIDEditDlg = ^TIDEditDlg;
  TIDEditDlg = object(TCenterDlg)
    uires     : PGatherRes;
    editIdx   : integer;
    lbl       : PLStatic;
    ed        : PListEdit;
    list      : PCollection;
    isBiotype : boolean;
    resRec    : PResRecObj;
    resIdx    : integer;
    grid      : PGrid;
    resCol    : integer;
    {}
    constructor Init(aParent: PWindowsObject; aRes: PGatherRes; anIdx: integer;
                     aGrid: PGrid; aResCol: integer);
    destructor Done; virtual;
    procedure SetupWindow; virtual;
    procedure SetupControls;
    procedure IDNext(var msg: TMessage); virtual ID_FIRST + IDC_NEXT;
    procedure IDPrev(var msg: TMessage); virtual ID_FIRST + IDC_PREV;
    procedure IDSave(var msg: TMessage); virtual ID_FIRST + IDC_SAVE;
    function DoNext: boolean;
  end;

  PMICEditDlg = ^TMICEditDlg;
  TMICEditDlg = object(TCenterDlg)
    uires       : PGatherRes;
    editIdx     : integer;
    mic         : PListEdit;
    interp      : PListEdit;
    micList     : PCollection;
    interpList  : PCollection;
    micResRec   : PResRecObj;
    interpResRec: PResRecObj;
    micIdx      : integer;
    interpIdx   : integer;
    grid        : PGrid;
    micCol      : integer;
    interpCol   : integer;
    {}
    constructor Init(aParent: PWindowsObject; aRes: PGatherRes; anIdx: integer;
                     aGrid: PGrid; aMICCol, aInterpCol: integer);
    destructor Done; virtual;
    procedure SetupWindow; virtual;
    procedure IDNext(var msg: TMessage); virtual ID_FIRST + IDC_NEXT;
    procedure IDPrev(var msg: TMessage); virtual ID_FIRST + IDC_PREV;
    procedure IDSave(var msg: TMessage); virtual ID_FIRST + IDC_SAVE;
    function DoNext: boolean;
    procedure SetupControls;
  end;

{-----------------------------------------------------------[ TIDEditDlg ]--}

constructor TIDEditDlg.Init(aParent: PWindowsObject; aRes: PGatherRes; anIdx: integer;
                            aGrid: PGrid; aResCol: integer);
var
  p1, p2  : array[0..100] of char;
begin
  inherited Init(aParent, MakeIntResource(DLG_IDEDIT));

  uiRes:= aRes;
  editIdx:= anIdx;
  grid:= aGrid;
  resCol:= aResCol;

  if uiRes = nil then
  begin
    ShowError(nil, IDS_INVRESOBJ, nil, 0, MOD_UIRESULTS, 0);
    Done;
    fail;
  end
  else if not uiRes^.GetIDResIDX(editIdx, resIdx) then
  begin
    ShowError(nil, IDS_INVRESIDX, nil, 0, MOD_UIRESULTS, 0);
    Done;
    fail;
  end;

  {- build possible values list }
  list:= New(PCollection, Init(25,25));

  lbl:= New(PLStatic, InitResource(@self, LBL_TESTNAME, 0, false));
  ed:= New(PListEdit, InitResource(@self, IDC_TEST, list, nil, '', false, UIListKey));
end;

destructor TIDEditDlg.Done;
begin
  inherited Done;
  MSDisposeObj(list);
end;

procedure TIDEditDlg.SetupWindow;
var
  pstr    : array[0..100] of char;
begin
  inherited SetupWindow;
  SetupControls;
end;

procedure TIDEditDlg.SetupControls;
var
  p1, p2  : array[0..100] of char;
begin
  uiRes^.GetIDResIDX(editIdx, resIdx);

  list^.FreeAll;
  resRec:= uiRes^.results^.At(resIdx);
  if StrComp(resRec^.testCatID, 'BIO') <> 0 then
  begin
    isBiotype:= false;
    list^.Insert(New(PListEditItem, Init('+')));
    list^.Insert(New(PListEditItem, Init('-')));
    if StrComp(resRec^.testCatID, ESBLCat) = 0 then
       list^.Insert(New(PListEditItem, Init('?')));
  end
  else
    isBiotype:= true;

  if not uiRes^.GetIDTestName(editIdx, p1, sizeof(p1)-1) then
    StrCopy(p1, '???');
  lbl^.SetText(p1);

  ed^.ChangeList(list);
  ed^.ChangeTitle(p1);

  if isBioType then
    SendMessage(ed^.hWindow, EM_LIMITTEXT, 20, 0);

  StrLCopy(p1, resRec^.result, sizeof(p1)-1);
  ed^.SetText(p1);
end;

function TIDEditDlg.DoNext: boolean;
begin
  if editIdx < (uiRes^.NumIDResults - 1) then
  begin
    Inc(editIdx);
    SetupControls;
    grid^.SetSelIndex(editIdx);
    DoNext:= true;
  end
  else
    DoNext:= false;
end;

procedure TIDEditDlg.IDNext(var msg: TMessage);
begin
  if not DoNext then
    MessageBeep(0);
  FocusCtl(hWindow, IDC_TEST);
end;

procedure TIDEditDlg.IDPrev(var msg: TMessage);
begin
  if editIdx > 0 then
  begin
    Dec(editIdx);
    SetupControls;
    grid^.SetSelIndex(editIdx);
  end
  else
    MessageBeep(0);
  FocusCtl(hWindow, IDC_TEST);
end;

procedure TIDEditDlg.IDSave(var msg: TMessage);
var
  ret   : boolean;
  pstr  : array[0..100] of char;

  function IsValidBio: boolean;
  {- validate a biotype number in pstr }
  var
    k    : integer;
    len  : integer;
    isOK : boolean;
  begin
    isOK:= true;
    len:=strlen(pstr);

    if len>0 then
    for k:= 0 to len-1 do
    begin
      if (pstr[k] <> '-') and ((pstr[k] < '0') or (pstr[k] > '7'))  then
      begin
        isOK:= false;
        break;
      end;
    end;
    IsValidBio:= isOK;
  end;

begin
  ret:= true;

  ed^.GetText(pstr, sizeof(pstr)-1);

  if ed^.IsValid <> 0 then
  begin
    ShowError(@self, IDS_INVRESULT, nil, 0, MOD_UIRESULTS, 0);
    ret:= false;
  end
  else if isBiotype then
  begin
    if not IsValidBio then
    begin
      ShowError(@self, IDS_INVBIOTYPE, nil, 0, MOD_UIRESULTS, 0);
      ret:= false;
    end;
  end;

  if ret then
  begin
    resRec^.SetResult(pstr);
    grid^.SetData(resCol, editIdx, pstr);
    DoNext;
  end;
  FocusCtl(hWindow, IDC_TEST);
end;

{-----------------------------------------------------------[ TMICEditDlg ]--}

constructor TMICEditDlg.Init(aParent: PWindowsObject; aRes: PGatherRes; anIdx: integer;
                             aGrid: PGrid; aMICCol, aInterpCol: integer);
var
  p1, p2 : array[0..100] of char;
  p3     : array[0..100] of char;
  c      : PControl;
  k      : integer;
  ci     : boolean;

begin
  inherited Init(aParent, MakeIntResource(DLG_MICEDIT));

  uiRes:= aRes;
  editIdx:= anIdx;
  micCol:= aMICCol;
  interpCol:= aInterpCol;
  grid:= aGrid;

  if uiRes = nil then
  begin
    ShowError(nil, IDS_INVRESOBJ, nil, 0, MOD_UIRESULTS, 0);
    Done;
    fail;
  end
  else if not uiRes^.GetMICResIDX(editIdx, micIdx, interpIdx) then
  begin
    ShowError(nil, IDS_INVRESIDX, nil, 0, MOD_UIRESULTS, 0);
    Done;
    fail;
  end;

  {- build possible values list }
  miclist:= New(PCollection, Init(25,25));
  interpList:= New(PCollection, Init(25,25));

  c:= New(PLStatic, InitResource(@self, LBL_MIC, 0, false));
  c:= New(PLStatic, InitResource(@self, LBL_INTERP, 0, false));

  mic:= New(PListEdit, InitResource(@self, IDC_MIC, micList,
                        SR(IDS_DILS, p2, sizeof(p2)-1), '', false, UIListKey));

  {- build interpretation list for the selected drug }
  for k:= 0 to 16 do
  begin
    if InterpCodeToStr(k, p1, sizeof(p1)-1, ci) then
    begin
      StrCopy(p2, '');
      InterpCodeToMappedStr(k, p2, sizeof(p1)-1, ci);
      if StrLen(p1) > 0 then
      begin
        StrCat(p1, #9);
        StrCat(p1, p2);
        interplist^.Insert(New(PListEditItem, Init(p1)));
      end;
    end;
  end;

  SR(IDS_DEFAULT, p2, sizeof(p2)-1);
  StrCat(p2, #9);
  StrCat(p2, SR(IDS_MAPPED, p3, sizeof(p3)-1));
  interp:= New(PListEdit, InitResource(@self, IDC_INTERP, interpList, p2, '',
                                       false, UIListKey));
end;

destructor TMICEditDlg.Done;
begin
  inherited Done;
  MSDisposeObj(micList);
  MSDisposeObj(interpList);
end;

procedure TMICEditDlg.SetupWindow;
var
  pstr    : array[0..100] of char;
begin
  inherited SetupWindow;
  SetupControls;
end;

procedure TMICEditDlg.SetupControls;
{- get results from results object and display them. Depends on }
var
  p1, p2 : array[0..100] of char;

  procedure FillMICList;
  var
    tray        : integer;
    rdn         : integer;
    dils        : DrugDilType;
    dilCnt      : integer;
    abbr        : string[20];
    k           : integer;
  begin
    if MICresRec^.tgUD then
    begin
      for k:= 0 to MaxMdaEntries do
      begin
        if Length(uiRes^.results^.traysObj^.gMDA[k]) > 0 then
        begin
          StrPCopy(p1, uiRes^.results^.traysObj^.gMDA[k]);
          miclist^.Insert(New(PListEditItem, Init(p1)));
        end
        else
          break;
      end;
    end
    else
    begin
      with uiRes^, results^ do
      begin
        traysObj^.LoadTray(micResRec^.testGroupID);
        traysObj^.GetDrugRDN(MICresRec^.testID, rdn);
        if rdn <> -1 then
        begin
          for k:= 0 to traysObj^.dilList[rdn].dilCnt do
          begin
            StrPCopy(p1, traysObj^.dilList[rdn].drugDils[k]);
            miclist^.Insert(New(PListEditItem, Init(p1)));
          end;
        end;
      end;
    end;
  end;

begin
  {- set drug name in group box }
  uiRes^.GetMICDrugName(editIdx, p1, sizeof(p1)-1);
  SetDlgItemText(hWindow, IDC_DRUGNAME, p1);

  uiRes^.GetMICResIDX(editIdx, micIdx, interpIdx);

  if micIDX >= 0 then
  begin
    micResRec:= uiRes^.results^.At(micIDX);
    EnableWindow(mic^.HWindow, true);
    EnableWindow(GetItemHandle(LBL_MIC), true);
    StrLCopy(p1, micResRec^.result, sizeof(p1)-1);
    mic^.SetText(p1);
  end
  else
  begin
    micResRec:= nil;
    EnableWindow(mic^.HWindow, false);
    EnableWindow(GetItemHandle(LBL_MIC), false);
  end;

  if interpIdx >= 0 then
  begin
    interpResRec:= uiRes^.results^.At(interpIDX);
    EnableWindow(interp^.HWindow, true);
    EnableWindow(GetItemHandle(LBL_INTERP), true);
    StrLCopy(p1, interpResRec^.result, sizeof(p1)-1);
    interp^.SetText(p1);
  end
  else
  begin
    interpResRec:= nil;
    EnableWindow(interp^.HWindow, false);
    EnableWindow(GetItemHandle(LBL_INTERP), false);
  end;

  {- build MIC list for possible values for the selected drug }
  if micResRec <> nil then
  begin
    micList^.FreeAll;
    FillMICList;
    mic^.ChangeList(micList);
  end;

  uiRes^.GetMICDrugName(editIdx, p1, sizeof(p1)-1);
  mic^.ChangeTitle(p1);
  interp^.ChangeTitle(p1);
end;

procedure TMICEditDlg.IDPrev(var msg: TMessage);
begin
  if editIdx > 0 then
  begin
    Dec(editIdx);
    SetupControls;
    grid^.SetSelIndex(editIdx);
    if micResRec <> nil then
      FocusCtl(hWindow, IDC_MIC)
    else
      FocusCtl(hWindow, IDC_INTERP);
  end
  else
    MessageBeep(0);
end;

function TMICEditDlg.DoNext: boolean;
begin
  if editIdx < (uiRes^.NumMICResults - 1) then
  begin
    Inc(editIdx);
    SetupControls;
    grid^.SetSelIndex(editIdx);
    if micResRec <> nil then
      FocusCtl(hWindow, IDC_MIC)
    else
      FocusCtl(hWindow, IDC_INTERP);
    DoNext:= true;
  end
  else
    DoNext:= false;
end;

procedure TMICEditDlg.IDNext(var msg: TMessage);
begin
  if not DoNext then
    MessageBeep(0);
end;

procedure TMICEditDlg.IDSave(var msg: TMessage);
var
  ret   : boolean;
  pstr  : array[0..100] of char;
begin
  ret:= false;
  if mic^.IsValid = 0 then
  begin
    if interp^.IsValid = 0 then
      ret:= true
    else
    begin
      ShowError(@self, IDS_INVINTERP, nil, 0, MOD_UIRESULTS, 0);
      FocusCtl(hWindow, IDC_INTERP);
    end;
  end
  else
  begin
    ShowError(@self, IDS_INVMIC, nil, 0, MOD_UIRESULTS, 0);
    FocusCtl(hWindow, IDC_MIC);
  end;

  if ret then
  begin
    if micResRec <> nil then
    begin
      mic^.GetText(pstr, sizeof(pstr)-1);
      micResRec^.SetResult(pstr);
      grid^.SetData(MICCol, editIdx, pstr);
    end;
    if interpResRec <> nil then
    begin
      interp^.GetText(pstr, sizeof(pstr)-1);
      interpResRec^.SetResult(pstr);
      {- need to clear contra-indicated flag since user has specifically set an interp}
      uiRes^.results^.SetFlag(interpResRec^.testID, interpResRec^.testCatID, REScontraIndicated, false);
      grid^.SetData(InterpCol, editIdx, pstr);
    end;
    DoNext;
  end;
end;

procedure EditMICResult(aParent: PWindowsObject; aRes: PGatherRes; idx: integer;
                       grid: PGrid; MICCol, InterpCol: integer);
var
  dlg   : PMICEditDlg;
begin
  dlg:= New(PMICEditDlg, Init(aParent, aRes, idx, grid, MICCol, InterpCol));
  application^.ExecDialog(dlg);
end;

procedure EditIDResult(aParent: PWindowsObject; aRes: PGatherRes; idx: integer;
                       grid: PGrid; resCol: integer);
var
  dlg   : PIDEditDlg;
begin
  dlg:= New(PIDEditDlg, Init(aParent, aRes, idx, grid, resCol));
  application^.ExecDialog(dlg);
end;


END.

{- rcf }
