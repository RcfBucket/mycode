program testFont;
uses
 WinCrt, WinTypes, WinProcs,
 IniLib;

procedure MakeScreenFont(var lf: TLogFont; bold, fixedPoint: boolean);
{- JWindows MakeScreenFont }
var
  dc : HDC;
  f  : HFont;
  k  : integer;
begin
  FillChar(lf, sizeof(lf), 0);

  if bold then
    lf.lfWeight:= FW_BOLD
  else
    lf.lfWeight:= FW_NORMAL;
{  lf.lfPitchAndFamily:= FF_MODERN;}
  lf.lfPitchAndFamily:= FIXED_PITCH;
  lf.lfcharSet:= SHIFTJIS_CHARSET;

  {- get the face name windows assigns to the font }
  dc:= GetDC(0);
  k:= INIScreenPointSize;
  lf.lfHeight:= -MulDiv(k, GetDeviceCaps(dc, LOGPIXELSY), 72);
  f:= CreateFontIndirect(lf);
  SelectObject(dc, f);
  GetTextFace(dc, lf_FaceSize, lf.lfFaceName);
  ReleaseDC(0, dc);
  DeleteObject(f);
end;

var
  lf : TLogFont;
begin
  MakeScreenFont(lf, FALSE, TRUE);
  writeln('Hello World');
  writeln(lf.lffacename);
  if (lf.lfPitchAndFamily and FIXED_PITCH <> 0) then
    writeln('Fixed Point')
  else
    writeln('Variable Pitch');
end.
