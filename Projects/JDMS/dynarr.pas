unit DynArr;

INTERFACE

const
  MaxPtrList=MAXINT div SIZEOF(POINTER);
  MaxSinglePtr=MAXINT div SIZEOF(SINGLE);
  MaxintegerPtr=MAXINT div SIZEOF(integer);
  MaxbytePtr=MAXINT div SIZEOF(byte);

type
  TPtrList    = array[0..MaxPtrList] of pointer;
  TSinglePtr  = array[0..MaxSinglePtr] of SINGLE;
  TIntegerPtr = array[0..MaxintegerPtr] of integer;
  TBytePtr    = array[0..MaxbytePtr] of byte;

  PtrarrayPointer = ^TPtrList;
  SinglearrayPointer = ^TSinglePtr;
  IntegerArrayPointer = ^TintegerPtr;
  bytearrayPointer = ^TbytePtr;

  TDynamicArray = object
    MaxElement: integer;
    ElementSize: integer;
    procedure Init(MAX: integer; ES: integer; var p: pointer);
    procedure Done(var p: pointer);
  end;

  TDynamicRecordarray = object(TDynamicArray)
    P: PtrarrayPointer;
    procedure Init(max: integer; ES: integer);
    procedure Done;
  end;

  TDynamicSinglearray = object(TDynamicArray)
    P: SinglearrayPointer;
    procedure Init(MAX: integer);
    procedure Done;
  end;

  TDynamicIntegerArray = object(TDynamicArray)
    P: integerarrayPointer;
    procedure Init(MAX: integer);
    procedure Done;
  end;

  TDynamicbytearray = object(TDynamicArray)
    P: bytearrayPointer;
    procedure Init(MAX: integer);
    procedure Done;
  end;

IMPLEMENTATION

uses
  DMSDebug;

procedure ABORT;
begin
  RunError(0);
end;

procedure TDynamicArray.Init(max: integer; ES: integer; var p: pointer);
begin
  MaxElement:= MAX;
  ElementSize:= ES;
  GetMem(P, (MaxElement+1) * ElementSize);
  if (P = nil) then
    Abort;
  FillChar(P^, (MaxElement+1) * ElementSize, 0);
end;

procedure TDynamicArray.Done(var P: pointer);
begin
  FreeMem(P, (MaxElement+1) * ElementSize);
  p := nil;
end;

procedure TDynamicRecordarray.Init(MAX: integer; ES: integer);
var
  i : integer;
begin
  TDynamicArray.Init(MAX, SIZEOF(POINTER), POINTER(P));
  ElementSize:= ES;
  for i:= 0 to max do
  begin
    GetMem(P^[i], ES);
    if (P^[i] = nil) then
      Abort;
    FillChar(P^[i]^, ES, 0);
  end;
end;

procedure TDynamicRecordarray.Done;
VAR
  i : integer;
begin
  for i:= 0 to MaxElement do
  begin
    FreeMem(P^[i], ElementSize);
  end;
  ElementSize:= SIZEOF(POINTER);
  TDynamicArray.Done(POINTER(P));
end;

procedure TDynamicSinglearray.Init(MAX: integer);
begin
  TDynamicArray.Init(MAX, SIZEOF(SINGLE), POINTER(P));
end;

procedure TDynamicSinglearray.Done;
begin
  TDynamicArray.Done(POINTER(P));
end;

procedure TDynamicintegerarray.Init(MAX: integer);
begin
  TDynamicArray.Init(MAX, SIZEOF(integer), POINTER(P));
end;

procedure TDynamicintegerarray.Done;
begin
  TDynamicArray.Done(POINTER(P));
end;

procedure TDynamicbytearray.Init(MAX: integer);
begin
  TDynamicArray.Init(MAX, SIZEOF(byte), POINTER(P));
end;

procedure TDynamicbytearray.Done;
begin
  TDynamicArray.Done(POINTER(P));
end;

END.
