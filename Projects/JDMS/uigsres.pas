unit UIGSRes;

{- Gram Stain Results Module }

INTERFACE

uses
  OWindows,
  DBTypes;

procedure UIGSResults(aParent: PWindowsObject; aSeq: TSeqNum);
{- Display a gram stain result screen.
   aSeq  : 0 = generic gs results screen (user loads the specimen)
          >0 = Specimen seq number to preload }

IMPLEMENTATION

uses
  objects,
  INILib,
  UIflds,
  UserMsgs,
  UICommon,
  MScan,
  DMSErr,
  DMString,
  ODialogs,
  GSDef,
  DlgLib,
  GridLib,
  spinedit,
  listlib,
  ctlLib,
  apitools,
  DBIDS,
  DBLib,
  DBFile,
  Strings,
  Win31,
  WinProcs,
  WinTypes;

{$R UIGSRES.RES}
{$I UIGSRES.INC}

const
  grid_Abbr     = 0;
  grid_Name     = 1;
  grid_Result   = 2;
  grid_Type     = 3;
  grid_MnemSeq  = 4;

type
  PResSpin = ^TResSpin;
  TResSpin = object(TSpinEdit)
    changeFlag  : boolean;
    listObj     : PListObject;
    listRec     : PMnemListRec;
    mnemSeq     : TSeqNum;
    resType     : char;
    inChange    : integer;
    {}
    constructor InitResource(aParent: PWindowsObject; anID: word; aListObj: PListObject);
    destructor Done; virtual;
    procedure SpinUp; virtual;
    procedure SpinDown; virtual;
    procedure Clear;
    procedure ListData;
    procedure SetData(aTxt: PChar; aResType: Char);
    procedure WMKeyDown(var msg: TMessage); virtual WM_FIRST + WM_KEYDOWN;
    procedure WMChar(var msg: TMessage); virtual WM_FIRST + WM_CHAR;
    procedure WMRButton(var msg: TMessage); virtual WM_FIRST + WM_RBUTTONDOWN;
    procedure HandleChange(var msg: TMessage);
  end;

  TGSResult = record
    mnemSeq   : TSeqNum;
    freeTxt   : array[0..MaxGSFreeText] of char;
  end;

  PResGrid = ^TResGrid;
  TResGrid = object(TGrid)
    procedure RightMouse(aCol: integer; aRow: longint); virtual;
  end;

  PUIGSDlg = ^TUIGSDlg;
  TUIGSDlg = object(TUICommonDlg)
    grid      : PResGrid;
    result    : PResSpin;
    gsDef     : PDBFile;
    gsRes     : PDBFile;
    dbSpec    : PDBFile;
    dbPat     : PDBFile;
    tempSpec  : PDBRec;
    sFld      : PUIFld;
    cFld      : PUIFld;
    pFld      : PUIFld;
    listRec   : PMnemListRec;
    origRes   : array[0..MaxGSSlots] of TGSResult;
    {}
    constructor Init(aParent: PWindowsObject; aSeq: TSeqNum);
    destructor Done; virtual;
    procedure SetupWindow; virtual;
    procedure WMCharToItem(var msg: TMessage);  virtual WM_FIRST + WM_CHARTOITEM;
    procedure WMDrawItem(var msg: TMessage);    virtual WM_FIRST + WM_DRAWITEM;
    procedure WMMeasureItem(var msg: TMessage); virtual WM_FIRST + WM_MEASUREITEM;
    procedure WMFldList(var msg: TMessage);     virtual WM_FIRST + WM_FLDLIST;
    procedure WMCommand(var msg: TMessage);     virtual WM_FIRST + WM_COMMAND;
    procedure EnableButtons; virtual;
    procedure FillScreen;
    procedure FillResults(aSpecSeq: TSeqNum);
    procedure ClearResult(row: integer);
    procedure ClearAllResults;
    procedure ClearFields; virtual;
    procedure IDGrid(var msg: TMessage); virtual ID_FIRST + IDC_GRID;
    procedure SetNewRow(newRow: integer);
    procedure DoSpecCollExit(newFocID: word);
    procedure NextPrev(doNext: boolean); virtual;
    function SaveData(clearAfter: boolean): boolean; virtual;
    function CheckSave(clearAfter: boolean): boolean; virtual;
    function DataChanged: boolean; virtual;
    procedure SetModuleID; virtual;

    procedure WMResChange(var msg: TMessage); virtual WM_FIRST + WM_RESCHANGE;
    procedure WMSpin(var msg: TMessage); virtual WM_FIRST + WM_SPIN;
    procedure WMLostFocus(var msg: TMessage); virtual WM_FIRST + WM_LOSTFOCUS;
    procedure WMRightClick(var msg: TMessage); virtual WM_FIRST + WM_RIGHTCLICK;
  end;

{--------------------------------------------------------------[ TResSpin ]--}

constructor TResSpin.InitResource(aParent: PWindowsObject; anID: word; aListObj: PListObject);
begin
  listRec:= New(PMnemListRec, Init);
  if listRec = nil then
  begin
    ShowError(nil, IDS_CANTALLOCLISTREC, nil, 0, MOD_UIGSRESULTS, 1);
    Halt;
  end;

  inherited InitResource(aParent, anID, 0, 0);
  changeFlag:= false;  {- no changes }
  listObj:= aListObj;
  inChange:= 0;
end;

destructor TResSpin.Done;
begin
  MSDisposeObj(listRec);
  inherited Done;
end;

procedure TResSpin.SpinUp;
begin
  SendMessage(GetParent(HWindow), WM_SPIN, 0, 0);
end;

procedure TResSpin.SpinDown;
begin
  SendMessage(GetParent(HWindow), WM_SPIN, 1, 0);
end;

procedure TResSpin.Clear;
begin
  SetText(nil);
end;

procedure TResSpin.HandleChange(var msg: TMessage);
{- called by the parent dialog on a EN_CHANGE message since the control cant
   trap it }
var
  pstr    : array[0..200] of char;
begin
  if inChange > 0 then exit;
  Inc(inChange);
  GetText(pstr, 200);
  if resType = GSFreeText then
    SendMessage(GetParent(HWindow), WM_RESCHANGE, 0, longint(@pstr))
  else
  begin
    if listObj^.FindMnemID(parent, DBGSMnemFile, pstr, listRec, true) then
      SendMessage(GetParent(HWindow), WM_RESCHANGE, 1, longint(listRec))
    else
      SendMessage(GetParent(HWindow), WM_RESCHANGE, 1, 0)
  end;
  Dec(inChange);
end;

procedure TResSpin.SetData(aTxt: PChar; aResType: Char);
{- if aResType = GSFreeText then aTxt is the free text result otherwise aTxt is the
   sequence number to load from GRAMNEM file }
var
  code  : integer;
begin
  Inc(inChange);
  changeFlag:= false;
  resType:= aResType;
  if resType <> GSFreeText then
  begin
    if strlen(aTxt) = 0 then
      mnemSeq:= 0
    else
      Val(aTxt, mnemSeq, code);
    if (code = 0) and (mnemSeq <> 0) then
    begin
      if not listObj^.FindMnemSeq(DBGSMnemFile, listRec, mnemSeq) then
        mnemSeq:= 0;
    end
    else
      mnemSeq:= 0;

    if mnemSeq <> 0 then
      SetText(listRec^.ID)
    else
      SetText(nil);
  end
  else
  begin
    mnemSeq:= 0;
    SetText(aTxt);
  end;
  Dec(inChange);
end;

procedure TResSpin.ListData;
var
  pstr  : array[0..100] of char;
  isOK  : boolean;
begin
  {- no list for free text }
  if IsWindowEnabled(HWindow) and (resType <> GSFreeText) then
  begin
    GetText(pstr, 100);
    isOK:= listObj^.SelectMnemList(parent, DBGSMnemFile, pstr, listRec);
    HandleEvents(GetParent(hWindow));
    if isOK then
    begin
      SendMessage(GetParent(HWindow), WM_RESCHANGE, 1, longint(listRec));
      SetText(listRec^.ID);
      changeFlag:= false;
    end;
  end
  else
    MessageBeep(0);
end;

procedure TResSpin.WMRButton(var msg: TMessage);
begin
  ListData;
end;

procedure TResSpin.WMChar(var msg: TMessage);
begin
  Inc(inChange);
  if not changeFlag then
  begin
    changeFlag:= true;
    if resType <> GSFreeText then
      SetText(nil);
  end;
  Dec(inChange);
  inherited WMChar(msg);
end;

procedure TResSpin.WMKeyDown(var msg: TMessage);
begin
  if (msg.wParam = UILISTKEY) then
    ListData;
  inherited WMKeyDown(msg);
end;

{--------------------------------------------------------------[ TUIGSDlg ]--}

constructor TUIGSDlg.Init(aParent: PWindowsObject; aSeq: TSeqNum);
var
  c     : PControl;
  subst : PErrSubst;
begin
  inherited Init(aParent, MakeIntResource(DLG_GSRESULTS), aSeq);

  gsDef:= nil;
  gsRes:= nil;
  dbSpec:= nil;
  dbPat:= nil;
  tempSpec:= nil;
  listRec:= nil;
  FillChar(origRes, sizeof(origRes), 0);

  gsDef:= New(PDBFile, Init(DBGSDefFile, '', dbOpenNormal));
  if gsDef = nil then
  begin
    subst:= New(PErrSubst, Init(DBGSDefFile));
    DoFail(IDS_GSDEFOPENERR, dbLastOpenError, subst);
    MSDisposeObj(subst);
    Fail;
  end;

  if not gsDef^.dbc^.GetFirst(gsDef^.dbr) then
  begin
    DoFail(IDS_NODEF, 0, nil);
    Fail;
  end;

  gsRes:= New(PDBFile, Init(DBGSFile, '', dbOpenNormal));
  if gsDef = nil then
  begin
    subst:= New(PErrSubst, Init(DBGSFile));
    DoFail(IDS_GSRESOPENERR, dbLastOpenError, subst);
    MSDisposeObj(subst);
    Fail;
  end;

  dbSpec:= New(PDBFile, Init(DBSPECFile, '', dbOpenNormal));
  if dbSpec = nil then
  begin
    subst:= New(PErrSubst, Init(DBSPECFile));
    DoFail(IDS_ERROROPEN, dbLastOpenError, subst);
    MSDisposeObj(subst);
    Fail;
  end;
  {- get spec file in proper sort order }
  if INISpecimenListSort = 2 then
    dbSpec^.dbc^.SetCurKeyNum(DBSPEC_DATE_KEY)
  else
    dbSpec^.dbc^.SetCurKeyNum(DBSPEC_ID_KEY);
  dbSpec^.dbc^.GetFirst(dbSpec^.dbr);

  dbPat:= New(PDBFile, Init(DBPatFile, '', dbOpenNormal));
  if dbPat = nil then
  begin
    subst:= New(PErrSubst, Init(DBPATFile));
    DoFail(IDS_ERROROPEN, dbLastOpenError, subst);
    MSDisposeObj(subst);
    Fail;
  end;

  tempSpec:= New(PDBRec, Init(dbSpec^.dbc));
  if tempSpec = nil then
  begin
    DoFail(IDS_CANTALLOCREC, 0, nil);
    Fail;
  end;

  listRec:= New(PMnemListRec, Init);
  if listRec = nil then
  begin
    DoFail(IDS_CANTALLOCLISTREC, 0, nil);
    Fail;
  end;

  c:= New(PLStatic, InitResource(@self, IDC_LBLRESULT, 0, false));

  sFld:= LinkUIFld(DBSpecSpecID,      true,  false, 0, IDC_EDSPECID,   IDC_LBLSPECID,   dbSpec^.dbd);
  cFld:= LinkUIFld(DBSpecCollectDate, true,  false, 0, IDC_EDCOLLECT,  IDC_LBLCOLLECT,  dbSpec^.dbd);
         LinkUIFld(DBSpecSrc,         false, false, 0, IDC_SOURCE,     IDC_LBLSOURCE,   dbSpec^.dbd);
         LinkUIFld(DBSpecWard,        false, false, 0, IDC_WARD,       IDC_LBLWARD,     dbSpec^.dbd);
  pFld:= LinkUIFld(DBPatPatID,        false, false, 0, IDC_PATID,      IDC_LBLPATID,    dbPat^.dbd);
         LinkUIFld(DBPatLName,        false, false, 0, IDC_LAST,       IDC_LBLLAST,     dbPat^.dbd);
         LinkUIFld(DBPatFName,        false, false, 0, IDC_FIRST,      IDC_LBLFIRST,    dbPat^.dbd);

  grid:= New(PResGrid, InitResource(@self, IDC_GRID, 5, false));
  result:= New(PResSpin, InitResource(@self, IDC_EDRESULT, listObj));

  grid^.EnableRightMouse(true);
end;

destructor TUIGSDlg.Done;
begin
  inherited Done;
  {- selective clean up }
  MSDisposeObj(gsDef);
  MSDisposeObj(dbSpec);
  MSDisposeObj(dbPat);
  MSDisposeObj(gsRes);
  MSDisposeObj(tempSpec);
  MSDisposeObj(listRec);
end;

procedure TUIGSDlg.SetupWindow;
var
  fi    : TFldInfo;
  w     : word;
  row   : longint;
  p1,p2 : array[0..100] of char;

  procedure DisableIt(aFld: PUIFld); far;
  begin
    if (preLoad > 0) or ((aFld <> sFld) and (aFld <> cFld)) then
      aFld^.Enable(False);
  end;

begin
  inherited SetupWindow;

  flds^.ForEach(@DisableIt);

  gsDef^.dbd^.FieldInfo(DBGSDefAbbr, fi);
  grid^.SetHeader(grid_Abbr, fi.fldName);
  gsDef^.dbd^.FieldInfo(DBGSDefDesc, fi);
  grid^.SetHeader(grid_Name, fi.fldName);
  grid^.SetHeader(grid_Result, SR(IDS_RESULT, p1, sizeof(p1)-1));

  {- now fill grid based on definition }
  gsDef^.dbc^.GetFirst(gsDef^.dbr);
  while gsDef^.dbc^.dbErrorNum = 0 do
  begin
    row:= grid^.AddString('');
    gsDef^.dbr^.GetField(DBGSDefAbbr, @p1, sizeof(p1)-1);
    grid^.SetData(grid_abbr, row, p1);
    gsDef^.dbr^.GetField(DBGSDefDesc, @p1, sizeof(p1)-1);
    grid^.SetData(grid_Name, row, p1);
    gsDef^.dbr^.GetField(DBGSDefResType, @p1[0], 1);
    p1[1]:= #0;
    grid^.SetData(grid_Type, row, p1);
    grid^.SetItemData(row, gsDef^.dbr^.GetSeqValue);

    gsDef^.dbc^.GetNext(gsDef^.dbr);
  end;
  grid^.OptColumnWidth(grid_Name);
  grid^.OptColumnWidth(grid_Abbr);
  grid^.StretchColumn(grid_Result);

  if grid^.GetRowCount = 0 then
    ErrorMsg(HWindow, SR(IDS_ERROR, p1, sizeof(p1)-1), SR(IDS_NOGSCREATED, p2, sizeof(p2)-1));

  if preLoad > 0 then
  begin
    if dbSpec^.dbc^.GetSeq(dbSpec^.dbr, preLoad) then
      FillScreen
    else
      ErrorMsg(HWindow, SR(IDS_ERROR, p1, sizeof(p1)-1), SR(IDS_ERRLODSPEC, p2, sizeof(p2)-1));
  end;
  EnableButtons;
end;

procedure TUIGSDlg.EnableButtons;
var
  seq   : TSeqNum;
begin
  seq:= dbSpec^.dbr^.GetSeqValue;
  EnableWindow(result^.HWindow, (seq > 0));
  EnableWindow(GetItemHandle(IDC_LBLRESULT), (seq > 0));
  EnableWindow(GetItemHandle(IDC_UIPREV), (seq > 0) and (preLoad = 0));
  EnableWindow(GetItemHandle(IDC_UINEXT), preLoad = 0);
  EnableWindow(GetItemHandle(IDC_UISAVE), seq > 0);
  EnableWindow(GetItemHandle(IDC_UICLEAR), preLoad = 0);
end;

procedure TUIGSDlg.SetModuleID;
begin
  modID:= MOD_UIGSRESULTS;
end;

procedure TUIGSDlg.ClearResult(row: integer);
begin
  grid^.SetData(grid_MnemSeq, row, '');
  grid^.SetData(grid_Result, row, '');
end;

procedure TUIGSDlg.ClearAllResults;
var
  k   : integer;
begin
  for k:= 0 to grid^.GetRowCount - 1 do
    ClearResult(k);
  FillChar(origRes, sizeof(origRes), 0);
  result^.Clear;
end;

procedure TUIGSDlg.FillResults(aSpecSeq: TSeqNum);
{- assuming the definition has been loaded into the grid, and the item data for
   each row of the grid is the seq number for the grid test slot }

  procedure PutIntoGrid(aRec: PDBRec);
  {- put the result in aRec into the grid. }
  var
    testRef : TSeqNum;
    pstr    : array[0..200] of char;
    rType   : array[0..10] of char;
    k       : integer;
    seq     : TSeqNum;
  begin
    {- first find the grid row containing the definition for this test }
    aRec^.GetField(DBGSGSTstRef, @testRef, sizeof(testRef));
    for k:= 0 to grid^.GetRowCount - 1 do
    begin
      if grid^.GetItemData(k) = testRef then
      begin
        grid^.GetData(grid_Type, k, rType, 10);
        if rType[0] = GSFreeText then  {- free text }
        begin
          seq:= 0;
          aRec^.GetField(DBGSFreeText, @pstr, 200);
          StrCopy(origRes[k].freeTxt, pstr);
        end
        else
        begin   {- gram stain mnemonic }
          aRec^.GetField(DBGSGSMnemRef, @seq, sizeof(seq));
          if seq <> 0 then
          begin
            if listObj^.FindMnemSeq(DBGSMnemFile, listRec, seq) then
            begin
              StrCopy(pstr, listRec^.desc);
              origRes[k].mnemSeq:= seq;
            end
            else
              strcopy(pstr, '****');
          end
          else
            strcopy(pstr, '');
        end;

        {- string representation of result is now in pstr }
        grid^.SetData(grid_result, k, pstr);
        if seq <> 0 then
        begin
          Str(seq, pstr);
          grid^.SetData(grid_MnemSeq, k, pstr);
        end;
        break;
      end;
    end;
  end;

var
  found   : boolean;
  seq     : TSeqNum;
begin
  ClearAllResults;
  with gsRes^ do
  begin
(* should use get first contains *)
    dbr^.ClearRecord;
    dbr^.PutField(DBGSSpecRef, @aSpecSeq);    {- specimen seq }
    if dbc^.GetGE(dbr) then
    begin
      repeat
        dbr^.GetField(DBGSSpecRef, @seq, sizeof(seq));
        found:= seq = aSpecSeq;
        if found then
        begin {- found a gs test result for this specimen, now find where to
                  put it in the grid. }
          PutIntoGrid(dbr);
        end;
      until not found or not dbc^.GetNext(dbr);
    end;
  end;
  SetNewRow(0);
end;

procedure TUIGSDlg.FillScreen;
var
  seq   : TSeqNum;

  procedure GetIt(aFld: PUIFld); far;
  begin
    if aFld^.GetDBD = dbSpec^.dbd then
      aFld^.XferRecToCtl(dbSpec^.dbr)
    else
      aFld^.XFerRecToCtl(dbPat^.dbr);
  end;

begin
  WaitCursor(true);
  {- first load patient record from specimen reference }
  dbSpec^.dbr^.GetField(DBSpecPatRef, @seq, SizeOf(seq));
  if not dbPat^.dbc^.GetSeq(dbPat^.dbr, seq) then
    dbPat^.dbr^.ClearRecord;

  {- now load specimen fields }
  flds^.ForEach(@GetIt);

  FillResults(dbSpec^.dbr^.GetSeqValue);
  sFld^.Enable(false);
  cFld^.Enable(false);
  FocusCtl(HWindow, result^.GetID);
  EnableButtons;
  WaitCursor(false);
end;

procedure TUIGSDlg.WMCharToItem(var msg: TMessage);
begin
  grid^.CharToItem(msg);
end;

procedure TUIGSDlg.WMDrawItem(var msg: TMessage);
begin
  grid^.DrawItem(msg);
end;

procedure TUIGSDlg.WMMeasureItem(var msg: TMessage);
begin
  grid^.MeasureItem(msg);
end;

procedure TUIGSDlg.WMResChange(var msg: TMessage);
var
  row   : integer;
  p     : PMnemListRec;
  pstr  : array[0..50] of char;
begin
  row:= grid^.GetSelIndex;
  if row < 0 then exit;
  if msg.wParam = 0 then
    grid^.SetData(grid_result, row, PChar(msg.lParam))
  else
  begin
    if msg.lParam = 0 then
    begin
      grid^.SetData(grid_result, row, '');
      grid^.SetData(grid_mnemSeq, row, '');
    end
    else
    begin
      p:= PMnemListRec(msg.lParam);
      grid^.SetData(grid_result, row, p^.desc);
      Str(p^.seq, pstr);
      grid^.SetData(grid_mnemSeq, row, pstr);
    end;
  end;
end;

procedure TUIGSDlg.WMSpin(var msg: TMessage);
var
  k       : integer;
  newRow  : integer;
begin
  k:= grid^.GetSelIndex;
  newRow:= k;
  if k = -1 then
    grid^.SetSelIndex(0)
  else
  begin
    if msg.wParam = 0 then
      newRow:= k - 1
    else if msg.wParam = 1 then
      newRow:= k + 1;

    if newRow < 0 then
      newRow:= 0
    else if newRow > grid^.GetRowCount - 1 then
      newRow:= grid^.GetRowCount - 1;

    SetNewRow(newRow);
  end;
end;

procedure TUIGSDlg.SetNewRow(newRow: integer);
{- set a new row in the grid. Update the result field }
var
  curRow    : integer;
  pstr      : array[0..200] of char;
  rType     : array[0..5] of char;
begin
  curRow:= grid^.GetSelIndex;
  if curRow <> newRow then
  begin
    grid^.SetSelIndex(newRow);
  end;
  grid^.GetData(grid_Type, newRow, rType, 5);
  if rType[0] = GSFreeText then
    grid^.GetData(grid_Result, newRow, pstr, 200)
  else
    grid^.GetData(grid_MnemSeq, newRow, pstr, 200);
  result^.SetData(pstr, rType[0]);
end;

procedure TUIGSDlg.ClearFields;
begin
  inherited ClearFields;
  ClearAllResults;
  dbSpec^.dbr^.ClearRecord;
  EnableButtons;
  sFld^.Enable(true);
  cFld^.Enable(true);
  sFld^.FocusFld;
  grid^.SetSelIndex(-1);
end;

procedure TUIGSDlg.IDGrid(var msg: TMessage);
begin
  if msg.lParamHi = LBN_SELCHANGE then
    SetNewRow(grid^.GetSelIndex)
  else
    DefWndProc(msg);
end;

procedure TUIGSDlg.WMRightClick(var msg: TMessage);
begin
  SetNewRow(grid^.GetSelIndex);
  result^.ListData;
end;

procedure TUIGSDlg.WMLostFocus(var msg: TMessage);
var
  fld   : PUIFld;
  err   : integer;
begin
  fld:= PUIFld(msg.lParam); {- get field pointer to fld losing focus }
  {- first see if the field is valid }
  err:= fld^.IsValid;
  if (err <> 0) and (err <> UIErrRequired) then   {- ignore required fields on field exit }
    ShowUIFldError(@self, err, fld)
  else
    if (fld^.GetID = IDC_EDSPECID) or (fld^.GetID = IDC_EDCOLLECT) then
      DoSpecCollExit(msg.wParam);
  SetWindowLong(HWindow, DWL_MSGRESULT, 0);  {- set message result (for dialogs) }
end;

procedure TUIGSDlg.DoSpecCollExit(newFocID: word);
{- called when specimen/collect data lose focus }
var
  id  : word;
begin
  if (newFocID <> IDC_EDSPECID) and (newFocID <> IDC_EDCOLLECT) then
  begin
    if sFld^.IsEmpty and cFld^.IsEmpty then exit;
    WaitCursor(true);
    uiFldsProcessFocus:= false;
    sFld^.XferCtlToRec(tempSpec);
    cFld^.XferCtlToRec(tempSpec);
    if listObj^.FindKey(@self, tempSpec, true) then
    begin
      {- found a specimen, load screen }
      if tempSpec^.GetSeqValue <> dbSpec^.dbr^.GetSeqValue then
      begin
        dbSpec^.dbc^.GotoRecord(tempSpec);  {- relocate for next/prev }
        dbSpec^.dbr^.CopyRecord(tempSpec);
        FillScreen;
      end;
    end
    else
    begin {- specimen not found (no change) }
      if listObj^.errorNum <> 0 then
        ShowError(@self, IDS_DBERROR, nil, listObj^.errorNum, modID, 0)
      else
        ShowError(@self, IDS_SPECNOTFOUND, nil, 0, modID, 0);
      sFld^.FocusFld;
    end;
    uiFldsProcessFocus:= true;
    WaitCursor(false);
  end;
end;

function TUIGSDlg.SaveData(clearAfter: boolean): boolean;
var
  seq     : TSeqNum;
  specSeq : TSeqNum;
  row     : integer;
  code    : integer;
  pstr    : array[0..200] of char;
  rType   : array[0..5] of char;
  ret     : boolean;
  errNum  : integer;
begin
  ret:= true;
  errNum:= DBBeginTransaction;
  if errNum <> 0 then
  begin
    ret:= false;
    ShowError(@self, IDS_TRANSERROR, nil, errNum, modID, 0);
  end;

  {- first purge any old results }
  if ret then
  begin
    specSeq:= dbSpec^.dbr^.GetSeqValue;
    with gsRes^ do
    begin
      dbr^.ClearRecord;
      dbr^.PutField(DBGSSpecRef, @specSeq);
      if dbc^.GetGE(dbr) then
      begin
        repeat
          dbr^.GetField(DBGSSpecRef, @seq, sizeof(seq));
          if seq = specSeq then
          begin
            if not dbc^.DeleteRec(dbr) then
            begin
              ShowError(@self, IDS_CANNOTSAVE, nil, dbc^.dbErrorNum, modID, 0);
              ret:= false;
            end;
          end;
        until not ret or (seq <> specSeq) or not dbc^.GetNext(dbr);
      end;
    end;
  end;

  {- now go through the grid and save the new data }
  row:= 0;
  while ret and (row < grid^.GetRowCount) do
  begin
    grid^.GetData(grid_Type, row, rType, 5);
    gsRes^.dbr^.ClearRecord;
    gsRes^.dbr^.PutField(DBGSSpecRef, @specSeq);
    seq:= grid^.GetItemData(row);
    gsRes^.dbr^.PutField(DBGSGSTstRef, @seq);
    if rType[0] = GSFreeText then
    begin
      grid^.GetData(grid_Result, row, pstr, 200);
      gsRes^.dbr^.PutField(DBGSFreeText, @pstr);
    end
    else
    begin
      grid^.GetData(grid_MnemSeq, row, pstr, 200);
      Val(pstr, seq, code);
      if (code <> 0) then
        seq:= 0;
      gsRes^.dbr^.PutField(DBGSGSMnemRef, @seq);
    end;
    if not gsRes^.dbc^.InsertRec(gsRes^.dbr) then
    begin
      ShowError(@self, IDS_CANNOTSAVE, nil, gsDef^.dbc^.dbErrorNum, modID, 0);
      ret:= false;
    end;
    Inc(row);
  end;

  if ret then
    DBEndTransaction
  else
    DBAbortTransaction;

  SaveData:= ret;

  if ret and clearAfter then
    ClearFields;

  WaitCursor(false);
end;

function TUIGSDlg.DataChanged: boolean;
var
  different   : boolean;
  row         : integer;
  rType       : array[0..5] of char;
  pstr        : array[0..200] of char;
  seq         : TSeqNum;
  code        : integer;
begin
  {- scan through the grid and compare the results there with the results in
     the orig results array to find any differences }
  different:= false;
  row:= 0;
  while not different and (row < grid^.GetRowCount) do
  begin
    grid^.GetData(grid_Type, row, rType, 5);
    if rType[0] = GSFreeText then  {- free text }
    begin
      grid^.GetData(grid_Result, row, pstr, 200);
      different:= StrComp(pstr, origRes[row].freeTxt) <> 0;
    end
    else
    begin
      grid^.GetData(grid_MnemSeq, row, pstr, 200);
      code:= 0;
      if StrLen(pstr) = 0 then
        seq:= 0
      else
        Val(pstr, seq, code);
      different:= (code <> 0) or (origRes[row].mnemSeq <> seq);
    end;
    Inc(row);
  end;
  DataChanged:= different;
end;

function TUIGSDlg.CheckSave(clearAfter: boolean): boolean;
{- see if user changed any data, and if so prompt for save }
var
  ret   : boolean;
  k     : integer;
  pstr  : array[0..200] of char;
  p2    : array[0..50] of char;
begin
  ret:= true;
  if dbSpec^.dbr^.GetSeqValue > 0 then
  begin
    {- first see if any data has changed }
    if DataChanged then
    begin
      k:= YesNoCancelMsg(HWindow, SR(IDS_CONFIRM, p2, 50), SR(IDS_CONFIRMSAVE, pstr, 200));
      if k = IDYES then
        ret:= SaveData(clearAfter)
      else
        ret:= k <> IDCANCEL;
    end;
  end;
  CheckSave:= ret;
end;

procedure TUIGSDlg.NextPrev(doNext: boolean);
var
  isOK : boolean;
begin
  if CheckSave(false) then
  begin
    if dbSpec^.dbr^.GetSeqValue = 0 then
    begin
      if dbSpec^.dbc^.GetFirst(tempSpec) then
      begin
        dbSpec^.dbr^.CopyRecord(tempSpec);
        FillScreen;
      end
      else
        ShowError(@self, IDS_NOSPECS, nil, dbSpec^.dbc^.dbErrorNum, modID, 0);
    end
    else
    begin
      tempSpec^.CopyRecord(dbSpec^.dbr);
      if doNext then
        isOK:= dbSpec^.dbc^.GetNext(tempSpec)
      else
        isOK:= dbSpec^.dbc^.GetPrev(tempSpec);

      if not isOK then
      begin
        if dbSpec^.dbc^.dbErrorNum = MOD_BTRV + 9 then
          ShowError(@self, IDS_DBNOMOREDATA, nil, dbSpec^.dbc^.dbErrorNum, modID, 0)
        else
          ShowError(@self, IDS_DBERROR, nil, dbSpec^.dbc^.dbErrorNum, modID, 1);
      end
      else
      begin
        dbSpec^.dbr^.CopyRecord(tempSpec);
        FillScreen;
      end;
    end;
  end;
end;

procedure TUIGSDlg.WMFldList(var msg: TMessage);
{- called when user selects LIST in a field }
var
  id    : word;
  pstr  : array[0..100] of char;
  p2    : array[0..100] of char;
  ret   : longint;    {- return value to the UIFld that created this message }
  cont  : boolean;
begin
  ret:= -1;   {- tell UI field passing this message NOT to list (non-zero) }

  uiFldsProcessFocus:= false;   {- turn off field focus processing }

  id:= msg.wParam;
  if (id = IDC_EDSPECID) or (id = IDC_EDCOLLECT) then
  begin
    sFld^.XferCtlToRec(tempSpec);
    cFld^.XferCtlToRec(tempSpec);
    if listObj^.SelectList(@self, tempSpec) then
    begin
      dbSpec^.dbc^.GotoRecord(tempSpec);  {- relocate for next/prev }
      dbSpec^.dbr^.CopyRecord(tempSpec);
      FillScreen;
    end
    else
    begin
      if listObj^.errorNum <> 0 then
      begin
        if listObj^.errorNum = LISTERR_PARTIALKEYNOTFOUND then
          ShowError(@self, IDS_CANNOTLISTNOMATCH, nil, listObj^.errorNum, modID, 0)
        else
          ShowError(@self, IDS_CANNOTLISTBAD, nil, listObj^.errorNum, modID, 0);
      end;
    end;
  end
  else
    ret:= 0;    {- this routine is not handling the list. Let the UIfld handle it }

  uiFldsProcessFocus:= true;  {- turn focus processing back on }

  SetWindowLong(HWindow, DWL_MSGRESULT, ret);  {- set message result (for dialogs) }
end;

procedure TUIGSDlg.WMCommand(var msg: TMessage);
begin
  if (msg.wParam = IDC_EDRESULT) and (msg.lParamHi = EN_CHANGE) then
    result^.HandleChange(msg);
  inherited WMCommand(msg);
end;

{------------------------------------------------------[ TResGrid ]--}

procedure TResGrid.RightMouse(aCol: integer; aRow: longint);
begin
  SendMessage(parent^.HWindow, WM_RIGHTCLICK, 0, aRow);
end;

{------------------------------------------------------[ Exported Function ]--}

procedure UIGSResults(aParent: PWindowsObject; aSeq: TSeqNum);
var
  d   : PDialog;
begin
  d:= New(PUIGSDlg, Init(aParent, aSeq));
  application^.ExecDialog(d);
end;

END.

{- rcf }
