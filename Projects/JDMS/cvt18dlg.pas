unit Cvt18Dlg;

INTERFACE

uses
  Objects,
  CVT18Msc,
  APITools,
  MScan,
  Strings,
  StrsW,
  WinProcs,
  WinTypes,
  CvtCmp,
  CvtMisc,
  Cvt18Col,
  ODialogs,
  GridLib,
  DlgLib,
  DBFile,
  DBLib,
  DBIDS,
  DBTypes,
  OWindows;

{$I CVT18.INC}

function PromptForPanelOrder(aParent: PWindowsObject; panel: PPanelID): boolean;
function PromptForAddDrugOrder(aParent: PWindowsObject; aDrugObj: PTestResObj): boolean;
function PromptForNoDupsOrder(aParent : PWindowsObject;
                              aPanel   : PPanelID;
                              aDrugObj: PTestResObj;
                              aDupObj : PPnlXref): boolean;
function PromptForDupsOrder(aParent  : PWindowsObject;
                            aPanel   : PPanelID;
                            aDrugObj : PTestResObj;
                            aSpecSeq : TSeqNum): boolean;

IMPLEMENTATION

const
  WM_LISTKEY   = WM_USER + 208;
  WM_LOSTFOCUS = WM_USER + 209;
  g_Proc    = 0;
  g_Order   = 1;
  g_Group   = 2;
  g_Iso     = 3;

type
  PDups = ^TDups;
  TDups = record
    ordID    : array[0..9] of char;
    ordDesc  : array[0..33] of char;
    ordRef   : TSeqNum;
    pnlID    : array[0..9] of char;
    pnlDesc  : array[0..33] of char;
    pnlRef   : TSeqNum;
    addID    : array[0..9] of char;
    addDesc  : array[0..33] of char;
    addRef   : TSeqNum;
    addDrug  : DrugList;
  end;

  PDupsOrd = ^TDupsOrd;
  TDupsOrd = record
    pnlOID    : array[0..9] of char;
    pnlODesc  : array[0..33] of char;
    pnlORef   : TSeqNum;
    pnltgID   : array[0..9] of char;
    pnltgDesc : array[0..33] of char;
    pnltgRef  : TSeqNum;
    pnlISO    : array[0..9] of char;
    addOID    : array[0..9] of char;
    addODesc  : array[0..33] of char;
    addORef   : TSeqNum;
    addtgID   : array[0..9] of char;
    addtgDesc : array[0..33] of char;
    addtgRef  : TSeqNum;
    addISO    : array[0..9] of char;
    addDrug   : DrugList;
  end;

  POrd = ^TOrd;
  TOrd = record
    oID     : array[0..9] of char;
    oDesc   : array[0..33] of char;
    oRef    : TSeqNum;
    tgID    : array[0..9] of char;
    tgDesc  : array[0..33] of char;
    gRef    : TSeqNum;
    iSO     : array[0..9] of char;
    addDrug : DrugList;
  end;

  {- This object is used as a collection item for collections passed to the
     compare routines in this module }
  PTestObj = ^TTestObj;
  TTestObj = object(TObject)
    testID    : array[0..8] of char;
    testName  : array[0..31] of char;
    testDrug  : array[0..6] of char;
    testCat   : array[0..6] of char;
    testUD    : boolean;
    resLen    : integer;
    v3testID  : array[0..8] of char;
    {}
    private
    seen      : boolean;
  end;

procedure Build18TestList( drugs: drugList; aTstList: PCollection);
var
  tstdb     : PDBFile;
  drugdb    : PDBFile;
  tob       : PTestObj;
  k, c      : integer;
  drugSeq   : TSeqNum;
  drugStr   : array[0..6] of char;
  pStr      : array[0..50] of char;

begin
  aTstList^.FreeAll;

  tstdb:= OpenV30File(DBTSTFile);
  if tstdb = nil then
    CriticalError('Cannot open Test file.');

  drugdb:= OpenV30File(DBDRUGFile);
  if drugdb = nil then
    CriticalError('Cannot open Drug file.');

  for k := 1 to 5 do
  begin
    if StrLen(drugs[k]) <> 0 then
    begin
      tstdb^.dbr^.PutFieldAsStr(DBTSTID, drugs[k] );
      tstdb^.dbr^.PutFieldAsStr(DBTSTTstCat, 'MIC');
      if not tstdb^.dbc^.GetEQ(tstdb^.dbr) then
      begin
        StrCopy(pstr, 'Cannot find Test: ');
        StrCat(pstr, drugs[k]);
        CriticalError(pstr);
      end;
      tob := New(PTestObj, Init);

      tstdb^.dbr^.GetFieldAsStr(DBTSTID, tob^.testID, 8);
      tstdb^.dbr^.GetFieldAsStr(DBTSTName, tob^.testName, 31);
      tstdb^.dbr^.GetFieldAsStr(DBTSTTstCat, tob^.testCat, 6);
      tob^.testUD := true;
      tob^.resLen := 1;
      tstdb^.dbr^.GetFieldAsStr(DBTSTID, tob^.v3testID, 8);

      tstdb^.dbr^.GetField(DBTSTDrugRef, @drugSeq, sizeOf(drugSeq));
      if not drugdb^.dbc^.GetSeq(drugdb^.dbr, drugSeq) then
      begin
        StrCopy(pstr, 'Cannot find Drug: ');
        StrCat(pstr, drugs[k]);
        CriticalError(pstr);
      end;
      drugdb^.dbr^.GetFieldAsStr(DBTSTID, drugStr, 6);
      StrCopy(tob^.testDrug,drugStr);

      aTstList^.Insert(tob);

      tob := New(PTestObj, Init);
      tstdb^.dbr^.PutFieldAsStr(DBTSTID, drugs[k] );
      tstdb^.dbr^.PutFieldAsStr(DBTSTTstCat, 'NCCLS');
      if not tstdb^.dbc^.GetEQ(tstdb^.dbr) then
      begin
        StrCopy(pstr, 'Cannot find Test: ');
        StrCat(pstr, drugs[k]);
        CriticalError(pstr);
      end;
      tstdb^.dbr^.GetFieldAsStr(DBTSTID, tob^.testID, 8);
      tstdb^.dbr^.GetFieldAsStr(DBTSTName, tob^.testName, 31);
      StrCopy(tob^.testDrug,drugStr);
      tstdb^.dbr^.GetFieldAsStr(DBTSTTstCat, tob^.testCat, 6);
      tob^.testUD := true;
      tob^.resLen := 1;
      tstdb^.dbr^.GetFieldAsStr(DBTSTID, tob^.v3testID, 8);

      aTstList^.Insert(tob);
    end;
  end;

  MSDisposeObj(drugdb);
  MSDisposeObj(tstdb);
end;

function CreateGroup(id, desc: PChar; drugs: drugList): TSeqNum;
var
  grpF      : PDBFile;
  tstdb     : PDBFile;
  xrfdb     : PDBFile;
  bt        : byte;
  b         : boolean;
  len       : integer;
  k         : integer;
  tstSeq    : TSeqNum;
  grpSeq    : TSeqNum;
  tstr      : array[0..5] of char;
  pstr      : array[0..100] of char;

begin
  CreateGroup := 0;

  {create test group }
  grpSeq:= 0;
  grpf:= OpenV30File(DBTSTGRPFile);
  if grpf = nil then
    CriticalError('Cannot open Test Group file');

  len:= 0;
  for k:= 1 to 5 do
    if StrLen(drugs[k]) <> 0 then
      Inc(len, 2);

  {- create the group for the add drugs }
  grpf^.dbr^.PutFieldAsStr(DBTSTGRPID, id);
  grpf^.dbr^.PutFieldAsStr(DBTSTGRPDesc, desc);
  bt:= 8;
  grpf^.dbr^.PutField(DBTSTGRPSet, @bt);   {- 8 for user groups }
  StrCopy(tstr, 'XX');
  grpf^.dbr^.PutFieldAsStr(DBTSTGRPLayout, tstr);  {- XX for user groups }
  grpf^.dbr^.PutField(DBTSTGRPLen, @len);
  b:= true;
  grpf^.dbr^.PutField(DBTSTGRPUDFlag, @b);

  if not grpf^.dbc^.InsertRec(grpf^.dbr) then  {- insert the test group rec}
  begin
    StrCopy(pstr, 'Cannot create Test Group : ');
    StrCat(pstr, id);
    CriticalError(pstr);
  end;
  grpSeq:= grpf^.dbr^.GetSeqValue;  {- assign new group seq to xref}

  xrfdb:= OpenV30File(DBTSTGRPXRFFile);
  if xrfdb = nil then
    CriticalError('Cannot open Test Group Xref file');

  { Create Test group cross reference }
  tstdb:= OpenV30File(DBTSTFile);
  if tstdb = nil then
    CriticalError('Cannot open the V3.02 TEST file.');

  len := 0;
  for k := 1 to 5 do
    if StrLen(drugs[k]) <> 0 then
    begin
      tstdb^.dbr^.PutFieldAsStr(DBTSTID, drugs[k] );
      tstdb^.dbr^.PutFieldAsStr(DBTSTTstCat, 'MIC');
      if not tstdb^.dbc^.GetEQ(tstdb^.dbr) then
      begin
        StrCopy(pstr, 'Cannot find Test: ');
        StrCat(pstr, drugs[k]);
        CriticalError(pstr);
      end;
      tstSeq:= tstdb^.dbr^.GetSeqValue;

      xrfdb^.dbr^.PutField(DBTSTGRPXRFTstGrpRef, @grpSeq);
      xrfdb^.dbr^.PutField(DBTSTGRPXRFTstRef, @tstSeq);
      xrfdb^.dbr^.PutField(DBTSTGRPXRFResBlockPos, @len);
      Inc(len);
      if not xrfdb^.dbc^.InsertRec(xrfdb^.dbr) then
      begin
        StrCopy(pstr, 'Cannot add Test Group Xref entry. Group: ');
        StrCat(pstr, id);
        CriticalError(pstr);
      end;

      tstdb^.dbr^.PutFieldAsStr(DBTSTID, drugs[k] );
      tstdb^.dbr^.PutFieldAsStr(DBTSTTstCat, 'NCCLS');
      if not tstdb^.dbc^.GetEQ(tstdb^.dbr) then
      begin
        StrCopy(pstr, 'Cannot find Test: ');
        StrCat(pstr, drugs[k]);
        CriticalError(pstr);
      end;
      tstSeq:= tstdb^.dbr^.GetSeqValue;

      xrfdb^.dbr^.PutField(DBTSTGRPXRFTstGrpRef, @grpSeq);
      xrfdb^.dbr^.PutField(DBTSTGRPXRFTstRef, @tstSeq);
      xrfdb^.dbr^.PutField(DBTSTGRPXRFResBlockPos, @len);
      Inc(len);
      if not xrfdb^.dbc^.InsertRec(xrfdb^.dbr) then
      begin
        StrCopy(pstr, 'Cannot add Test Group Xref entry. Group: ');
        StrCat(pstr, id);
        CriticalError(pstr);
      end;
    end;

  MSDisposeObj(xrfdb);
  MSDisposeObj(tstdb);
  MSDisposeObj(grpF);
  CreateGroup:= grpSeq;
end;

function VerifyGroup(seq: TSeqNum; drugs: drugList): boolean;
var
  aTstList: PCollection;
begin
  VerifyGroup := false;
  aTstList := new(PCollection,Init(10,10));
  Build18TestList( drugs, aTstList);
  VerifyGroup := CompGroupAndProcList(seq, aTstList);
  MSDisposeObj(aTstList);
end;

function GetTstgrp(oSeq: TSeqNum; id, desc: PChar): TSeqNum;
var
  xrfdb    : PDBFile;
  tstgrpdb : PDBFile;
  gSeq      : TSeqNum;
  count    : integer;
begin
  GetTstgrp:= 0;
  xrfdb:= OpenV30File(DBORDXRFFile);
  if xrfdb = nil then
    CriticalError('Cannot open ORDERXRF file.');

  count:= 0;
  xrfdb^.dbr^.PutField(DBORDXRFOrdRef, @oSeq);
  if xrfdb^.dbc^.GetFirstContains(xrfdb^.dbr) then
  begin
    xrfdb^.dbr^.GetField(DBORDXRFTstGrpRef, @gSeq, sizeof(gSeq));
    if not xrfdb^.dbc^.GetNextContains(xrfdb^.dbr) then
      count:= 1
    else
      count:= 2;
  end;
  MSDisposeObj(xrfdb);

  if count = 1 then
  begin
    tstgrpdb:= OpenV30File(DBTSTGRPFile);
    if tstgrpdb = nil then
      CriticalError('Cannot open Test Group file.');
    tstgrpdb^.dbc^.GetSeq(tstgrpdb^.dbr, gSeq);
    tstgrpdb^.dbr^.GetFieldAsStr(DBTSTGRPID, id, 9);
    tstgrpdb^.dbr^.GetFieldAsStr(DBTSTGRPDesc, desc, 33);
    MSDisposeObj(tstgrpdb);
    GetTstgrp:= gSeq;
  end;
end;

function CompOrderWithTestGroups(oSeq, pSeq, aSeq: TSeqNum):Boolean;
var
  xrfdb    : PDBFile;
  gSeq1    : TSeqNum;
  gSeq2    : TSeqNum;
  count    : integer;

begin
  CompOrderWithTestGroups := false;
  if (pSeq <> 0) and (aSeq <> 0) then
  begin
    xrfdb:= OpenV30File(DBORDXRFFile);
    if xrfdb = nil then
      CriticalError('Cannot open ORDERXRF file.');

    count:= 0;
    xrfdb^.dbr^.PutField(DBORDXRFOrdRef, @oSeq);
    if xrfdb^.dbc^.GetFirstContains(xrfdb^.dbr) then
    begin
      xrfdb^.dbr^.GetField(DBORDXRFTstGrpRef, @gSeq1, sizeof(gSeq1));
      if xrfdb^.dbc^.GetNextContains(xrfdb^.dbr) then
      begin
        xrfdb^.dbr^.GetField(DBORDXRFTstGrpRef, @gSeq2, sizeof(gSeq2));
        if not xrfdb^.dbc^.GetNextContains(xrfdb^.dbr) then
          count:= 2
        else
          count:= 3;
      end
      else
        count := 1;
    end;
    MSDisposeObj(xrfdb);
  end;
  if count = 2 then
    if (((gSeq1 = pSeq) or (gSeq1 = aSeq)) and ((gSeq2 = pSeq) or (gSeq2 = aSeq))) then
      CompOrderWithTestGroups := true;
end;

function VerifyOrderAndTstGrp(orderRef, tstgrpRef: TSeqNum): boolean;
{- this routine searches the OrderXrf file. If they is only one orderxrf it
   compares the tstgrp seqNum with tstGrpRef. If they are equal the order
   contains only the given tstgrp. and the order and the tstgrp are verified.
}
var
  db    : PDBFile;
  seq   : TSeqNum;
  count : integer;
begin
  VerifyOrderAndTstGrp:= FALSE;
  db:= OpenV30File(DBORDXRFFile);
  if db = nil then
    CriticalError('Cannot open ORDERXRF file.');

  count:= 0;
  db^.dbr^.PutField(DBORDXRFOrdRef, @orderRef);
  if db^.dbc^.GetFirstContains(db^.dbr) then
  begin
    db^.dbr^.GetField(DBORDXRFTstGrpRef, @seq, sizeof(seq));
    if not db^.dbc^.GetNextContains(db^.dbr) then
      count:= 1
    else
      count:= 2;
  end;
  MSDisposeObj(db);

  if count = 1 then
  begin
    if seq = tstgrpRef then
      VerifyOrderAndTstGrp:= true;
  end;
end;

function CreateOrder(orderId, orderDesc: PChar; aSet: byte; tstgrpRef: TSeqNum): TSeqNum;
var
  db    : PDBFile;
  dbxrf : PDBFile;
  seq   : TSeqNum;

begin
  CreateOrder := 0;
  db:= OpenV30File(DBORDFile);
  if db = nil then
    CriticalError('Cannot open ORDER file.');

  db^.dbr^.PutFieldAsStr(1, orderId);
  db^.dbr^.PutFieldAsStr(2, orderDesc);
  db^.dbr^.PutField(3, @aSet);
  if db^.dbc^.InsertRec(db^.dbr) then
  begin
    seq := db^.dbr^.GetSeqValue;
    dbxrf:= OpenV30File(DBORDXRFFile);
    if dbxrf = nil then
      CriticalError('Cannot open the Order cross reference file (ORDERXRF).');
    dbxrf^.dbr^.PutField(1, @seq);
    dbxrf^.dbr^.PutField(2, @tstgrpRef);
    if dbxrf^.dbc^.InsertRec(dbxrf^.dbr) then
      CreateOrder := seq;
    MSDisposeObj(dbxrf);
  end;
  MSDisposeObj(db);
end;

function CreateOrder2(orderId, orderDesc: PChar; aSet: byte; pRef, gRef: TSeqNum): TSeqNum;
var
  db    : PDBFile;
  dbxrf : PDBFile;
  seq   : TSeqNum;

begin
  CreateOrder2 := 0;
  db:= OpenV30File(DBORDFile);
  if db = nil then
    CriticalError('Cannot open ORDER file.');

  db^.dbr^.PutFieldAsStr(1, orderId);
  db^.dbr^.PutFieldAsStr(2, orderDesc);
  db^.dbr^.PutField(3, @aSet);
  if db^.dbc^.InsertRec(db^.dbr) then
  begin
    seq := db^.dbr^.GetSeqValue;
    dbxrf:= OpenV30File(DBORDXRFFile);
    if dbxrf = nil then
      CriticalError('Cannot open the Order cross reference file (ORDERXRF).');
    dbxrf^.dbr^.PutField(1, @seq);
    dbxrf^.dbr^.PutField(2, @pRef);
    if dbxrf^.dbc^.InsertRec(dbxrf^.dbr) then
    begin
      dbxrf^.dbr^.PutField(1, @seq);
      dbxrf^.dbr^.PutField(2, @gRef);
      if dbxrf^.dbc^.InsertRec(dbxrf^.dbr) then
        CreateOrder2 := seq;
    end;
    MSDisposeObj(dbxrf);
  end;
  MSDisposeObj(db);
end;

function FindTstGrp(seq: TSeqNum; drugs: drugList; var gSeq: TSeqNum; gId, gDesc: PChar):boolean;
begin
  FindTstGrp := false;
  gSeq := GetTstgrp(seq, gId, gDesc);
  if gSeq > 0 then
    if VerifyGroup(gSeq, drugs) then
      FindTstGrp := true;
end;


type
  PPanelOrderDlg = ^TPanelOrderDlg;
  TPanelOrderDlg = object(TCenterDlg)
    desc:    PEdit;
    grptext: PEdit;
    ordId:   PEdit;
    grpId:   PEdit;
    ordDesc: PEdit;
    grpDesc: PEdit;
    aPanel:  PPanelID;
    aGrpStr: PChar;
    constructor Init(aParent: PWindowsObject; panel: PPanelID; grpStr: PChar);
    procedure SetupWindow; virtual;
    function CanClose: boolean; virtual;
    procedure LostFocus(var msg: TMessage); virtual WM_FIRST + WM_LOSTFOCUS;
    procedure ListKey(var msg: TMessage); virtual WM_FIRST + WM_LISTKEY;
    procedure Cancel(var msg: TMessage); virtual ID_FIRST + ID_CANCEL;
  end;

  PAddDrugOrderDlg = ^TAddDrugOrderDlg;
  TAddDrugOrderDlg = object(TCenterDlg)
    desc     : PEdit;
    grptext  : PEdit;
    ordId    : PEdit;
    grpId    : PEdit;
    ordDesc  : PEdit;
    grpDesc  : PEdit;
    anOrd    : PTestResObj;
    aGrpStr  : PChar;
    aTitleStr: PChar;
    constructor Init(aParent: PWindowsObject; ordr: PTestResObj; str1, str2: PChar);
    procedure SetupWindow; virtual;
    function CanClose: boolean; virtual;
    procedure LostFocus(var msg: TMessage); virtual WM_FIRST + WM_LOSTFOCUS;
    procedure ListKey(var msg: TMessage); virtual WM_FIRST + WM_LISTKEY;
    procedure Cancel(var msg: TMessage); virtual ID_FIRST + ID_CANCEL;
  end;

  PNODupsOrderDlg = ^TNODupsOrderDlg;
  TNODupsOrderDlg = object(TCenterDlg)
    desc      : PEdit;
    ordId     : PEdit;
    ordDesc   : PEdit;
    panelId   : PEdit;
    panelDesc : PEdit;
    addId     : PEdit;
    addDesc   : PEdit;
    dupObj    : PDups;
    aTitle    : PChar;
    constructor Init(aParent: PWindowsObject; aDupObj: PDups; aStr: PChar);
    procedure SetupWindow; virtual;
    function CanClose: boolean; virtual;
    procedure LostFocus(var msg: TMessage); virtual WM_FIRST + WM_LOSTFOCUS;
    procedure ListKey(var msg: TMessage); virtual WM_FIRST + WM_LISTKEY;
    procedure Cancel(var msg: TMessage); virtual ID_FIRST + ID_CANCEL;
  end;

  PDupsOrderDlg = ^TDupsOrderDlg;
  TDupsOrderDlg = object(TCenterDlg)
    grid      : PGrid;
    dupObj    : PDupsOrd;
    specSeq   : TSeqNum;
    aTitle    : PChar;
    constructor Init(aParent: PWindowsObject; aDupObj: PDupsOrd;
                     aStr: PChar; aSpecSeq : TSeqNum);
    procedure SetupWindow; virtual;
    procedure IDDone(var msg: TMessage); virtual ID_FIRST + IDC_DONE;
    procedure IDEdit(var msg: TMessage); virtual ID_FIRST + IDC_EDITMAP;
    procedure WMCharToItem(var msg: TMessage);  virtual WM_FIRST + WM_CHARTOITEM;
    procedure WMDrawItem(var msg: TMessage);    virtual WM_FIRST + WM_DRAWITEM;
    procedure WMMeasureItem(var msg: TMessage); virtual WM_FIRST + WM_MEASUREITEM;
    procedure WMCommand(var msg: TMessage); virtual WM_FIRST + WM_COMMAND;
    procedure Cancel(var msg: TMessage); virtual ID_FIRST + ID_CANCEL;
    procedure FillGrid;
    function CanClose: boolean; virtual;
  end;

  PISOORDDlg = ^TISOORDDlg;
  TISOORDDlg = object(TCenterDlg)
    desc     : PEdit;
    ordId    : PEdit;
    grpId    : PEdit;
    isoId    : PEdit;
    ordDesc  : PEdit;
    grpDesc  : PEdit;
    aType    : integer;
    anOrd    : POrd;
    aTitleStr: PChar;
    constructor Init(aParent: PWindowsObject; ordType : integer; ordr: POrd; str: PChar);
    procedure SetupWindow; virtual;
    function CanClose: boolean; virtual;
    procedure LostFocus(var msg: TMessage); virtual WM_FIRST + WM_LOSTFOCUS;
    procedure ListKey(var msg: TMessage); virtual WM_FIRST + WM_LISTKEY;
    procedure Cancel(var msg: TMessage); virtual ID_FIRST + ID_CANCEL;
  end;

  PListBoxDlg = ^TListBoxDlg;
  TListBoxDlg = object(TCenterDlg)
    grid    : PGrid;
    buf     : PChar;
    maxLen : integer;
    title  : array[0..100] of char;
    fName   : array[0..10] of char;
    constructor Init(aParent: PWindowsObject; aFile, aTitle, aBuf: PChar; aSize: integer);
    procedure SetupWindow; virtual;
    procedure WMCharToItem(var msg: TMessage);  virtual WM_FIRST + WM_CHARTOITEM;
    procedure WMDrawItem(var msg: TMessage);    virtual WM_FIRST + WM_DRAWITEM;
    procedure WMMeasureItem(var msg: TMessage); virtual WM_FIRST + WM_MEASUREITEM;
    procedure WMCommand(var msg: TMessage); virtual WM_FIRST + WM_COMMAND;
    function CanClose: boolean; virtual;
  end;

  PFocEdit = ^TFocEdit;
  TFocEdit = object(TEdit)
    procedure WMKillFocus(var msg: TMessage); virtual WM_FIRST + WM_KILLFOCUS;
    procedure WMKeyDown(var msg: TMessage); virtual WM_FIRST + WM_KEYDOWN;
    procedure WMRClick(var msg: TMessage); virtual WM_FIRST + WM_RBUTTONDOWN;
  end;

procedure TFocEdit.WMKillFocus(var msg: TMessage);
begin
  inherited WMKillFocus(msg);
  PostMessage(parent^.HWindow, WM_LOSTFOCUS, GetID, 0);
end;

procedure TFocEdit.WMKeyDown(var msg: TMessage);
begin
  inherited WMKeyDown(msg);
  if msg.wParam = VK_F3 then
    PostMessage(parent^.hWindow, WM_LISTKEY, GetID, 0);
end;

procedure TFocEdit.WMRClick(var msg: TMessage);
begin
  DefWndProc(msg);
  PostMessage(parent^.hWindow, WM_LISTKEY, GetID, 0);
end;

function ListBoxOrders(aParent: PWindowsObject; buf: PChar; maxLen: integer): boolean;
begin
  ListBoxOrders:= application^.ExecDialog(
               New(PListBoxDlg, Init(aParent, DBORDFile, 'V3.02 Orders', buf, maxLen))) = IDOK;
end;

function ListBoxGroups(aParent: PWindowsObject; buf: PChar; maxLen: integer): boolean;
begin
  ListBoxGroups:= application^.ExecDialog(
               New(PListBoxDlg, Init(aParent, DBTSTGRPFile, 'V3.02 Groups', buf, maxLen))) = IDOK;
end;

(************************************************************************)
(*                                                                      *)
(************************************************************************)
constructor TListBoxDlg.Init(aParent: PWindowsObject; aFile, aTitle, aBuf: PChar; aSize: integer);
begin
  inherited Init(aParent, MakeIntResource(DLG_CVT18LISTDATA));
  grid:= New(PGrid, InitResource(@self, IDC_GRID, 2, false));
  StrLCopy(fName, aFile, sizeof(fname)-1);
  StrLCopy(title, aTitle, sizeof(title)-1);
  buf:= aBuf;
  maxLen:= aSize;
end;

procedure TListBoxDlg.SetupWindow;
var
  db    : PDBFile;
  row   : integer;
  pstr  : array[0..100] of char;
begin
  inherited SetupWindow;
  SetWindowText(hWindow, title);
  grid^.SetHeader(0, 'ID');
  grid^.SetHeader(1, 'Description');
  grid^.SetColumnWidth(0, 10);
  grid^.SetColumnWidth(1, 10);
  db:= OpenV30File(fName);
  if db <> nil then
  begin
    if db^.dbc^.GetFirst(db^.dbr) then
    begin
      repeat
        db^.dbr^.GetFieldAsStr(1, pstr, sizeof(pstr)-1);
        row:= grid^.AddString('');
        grid^.SetData(0, row, pstr);
        db^.dbr^.GetFieldAsStr(2, pstr, sizeof(pstr)-1);
        grid^.SetData(1, row, pstr);
      until not db^.dbc^.GetNext(db^.dbr);
    end;
    Dispose(db, Done);
  end;
  grid^.SetSelIndex(0);
  EnableWindow(GetItemHandle(ID_OK), grid^.GetRowCount > 0);
  grid^.OptColumnWidth(0);
  grid^.StretchColumn(1);
end;

function TListBoxDlg.CanClose: boolean;
var
  k   : integer;
begin
  k:= grid^.GetSelIndex;
  if k >= 0 then
    grid^.GetData(0, k, buf, maxLen)
  else
    StrCopy(buf, '');
  CanClose:= true;
end;

procedure TListBoxDlg.WMCommand;
begin
  inherited WMCommand(msg);
  if (msg.lParamLo <> 0) then   {- message is from a control }
  begin
    if msg.lParamHI = LBN_DBLCLK then
      if CanClose then
        EndDlg(IDOK);
  end;
end;

procedure TListBoxDlg.WMCharToItem(var msg: TMessage);
begin
  grid^.CharToItem(msg);
end;

procedure TListBoxDlg.WMDrawItem(var msg: TMessage);
begin
  grid^.DrawItem(msg);
end;

procedure TListBoxDlg.WMMeasureItem(var msg: TMessage);
begin
  grid^.MeasureItem(msg);
end;

(************************************************************************)
(*                                                                      *)
(************************************************************************)
constructor TPanelOrderDlg.Init(aParent: PWindowsObject; panel: PPanelID; grpStr: PChar);
begin
  inherited Init(aParent, MakeIntResource(DLG_CVT18EDITORD));
  desc:= New(PEdit, InitResource(@self, LBL_DESC, 0));
  grptext:= New(PEdit, InitResource(@self, LBL_GRPID, 0));
  ordId:= New(PFocEdit, InitResource(@self, IDC_ORDID, 9));
  grpId:= New(PFocEdit, InitResource(@self, IDC_GRPID, 9));
  ordDesc:= New(PEdit, InitResource(@self, IDC_ORDDESC, 33));
  grpDesc:= New(PEdit, InitResource(@self, IDC_GRPDESC, 33));
  aPanel := panel;
  aGrpStr := grpStr;
end;

procedure TPanelOrderDlg.SetupWindow;
begin
  inherited SetupWindow;

  desc^.SetText(aPanel^.panelDesc);
  grpText^.SetText(aGrpStr);
  ordId^.SetText(aPanel^.orderId);
  ordDesc^.SetText(aPanel^.orderDesc);
  grpId^.SetText(aPanel^.tstgrp);
  grpDesc^.SetText(aPanel^.panelDesc);

  if aPanel^.tstgrpRef <> 0 then
  begin
    EnableWindow(grpId^.hWindow, false);
    EnableWindow(grpDesc^.hWindow, false);
  end;
end;

procedure TPanelOrderDlg.Cancel(var msg: TMessage);
begin
  if ConfirmExit(hWindow) then
    inherited Cancel(msg);
end;

procedure TPanelOrderDlg.ListKey(var msg: TMessage);
var
  pstr  : array[0..100] of char;
begin
  StrCopy(pstr, '');
  case msg.wParam of
    IDC_ORDID :   {- order list }
      if ListBoxOrders(@self, pstr, sizeof(pstr)-1) then
      begin
        ordId^.SetText(pstr);
        PostMessage(hWindow, WM_LOSTFOCUS, IDC_ORDID, 0);
      end;
    IDC_GRPID :   {- group list }
      if ListBoxGroups(@self, pstr, sizeof(pstr)-1) then
      begin
        grpId^.SetText(pstr);
        PostMessage(hWindow, WM_LOSTFOCUS, IDC_GRPID, 0);
      end;
  end;
end;

procedure TPanelOrderDlg.LostFocus(var msg: TMessage);
var
  pstr  : array[0..50] of char;
  aDesc  : array[0..50] of char;
begin
  if msg.wParam = IDC_GRPID then  {- group lost focus }
  begin
    grpId^.GetText(pstr, sizeof(pstr)-1);
    if (StrComp(pstr, aPanel^.tstgrp) <> 0) then
    begin
      StrCopy(aPanel^.tstgrp, pstr);
      if GroupExists(pstr, aDesc) > 0 then
      begin
        grpDesc^.SetText(aDesc);
      end;
    end;
  end
  else if msg.wParam = IDC_ORDID then  {- order lost focus }
  begin
    ordId^.GetText(pstr, sizeof(pstr)-1);
    if (StrComp(pstr, aPanel^.orderID) <> 0) then
    begin
      StrCopy(aPanel^.orderID, pstr);
      if OrderExists(pstr, aDesc) > 0 then
      begin
        ordDesc^.SetText(aDesc);
(*        EnableWindow(ordDesc^.hWindow, false);*)
      end
      else
      begin
        ordDesc^.SetText('');
(*        EnableWindow(ordDesc^.hWindow, true);*)
      end
    end;
  end;
end;

function TPanelOrderDlg.CanClose: boolean;
var
  ret   : boolean;
  seq   : TSeqNum;
  tw    : TWaitCursor;
  aDesc : array[0..35] of char;
  aStr  : array[0..35] of char;

  procedure CheckLen(s, msg: PChar; id: integer);
  begin
    if ret then
      if StrLen(s) = 0 then
      begin
        ErrorMsg(hWindow, 'Error', msg);
        FocusCtl(hWindow, id);
        ret:= false;
      end;
  end;

begin
  tw.Init;

  {- force focus processing }
  PostMessage(hWindow, WM_LOSTFOCUS, IDC_ORDID, 0);
  PostMessage(hWindow, WM_LOSTFOCUS, IDC_GRPID, 0);

  ret:= true;
  ordId^.GetText(aPanel^.orderId, 10);
  ordDesc^.GetText(aPanel^.orderDesc, 32);
  grpId^.GetText(aPanel^.tstgrp, 10);
  grpDesc^.GetText(aPanel^.panelDesc, 32);

  CheckLen(aPanel^.orderID, 'Order ID is required', IDC_ORDID);

  if ret then
  begin
    seq:= OrderExists(aPanel^.orderID, aDesc);
    {- if order does not exist, then a test group is required }
    if seq = 0 then
    begin
      CheckLen(aPanel^.tstgrp, 'Test group ID is required', IDC_GRPID);
      if ret then
      begin
        ordDesc^.GetText(aPanel^.orderDesc, 32);
        CheckLen(aPanel^.orderDesc, 'Order description is required.', IDC_ORDDESC);
      end;
    end
    else
    begin
      StrCopy(aPanel^.orderDesc, aDesc);
      ordDesc^.SetText(aDesc);
    end;
  end;

  {- if order exists, then make sure its compatible with the test group }
  if ret then
  begin
    if (seq > 0) then
    begin
      ret:= VerifyOrderAndTstGrp(seq, aPanel^.tstgrpRef);
      if not ret then
      begin
        ErrorMsg(hWindow, 'Error', 'Order is not compatible with test group');
        FocusCtl(hWindow, IDC_ORDID);
      end
      else
        aPanel^.orderRef:= seq;
    end;
  end;

  if ret and (aPanel^.orderRef = 0) then  {- if order does not exist }
    aPanel^.orderRef:= CreateOrder(aPanel^.orderId, aPanel^.orderDesc,
                                   aPanel^.tstgrpSet, aPanel^.tstgrpRef);

  CanClose:= ret;
  tw.Done;
end;

(************************************************************************)
(*                                                                      *)
(************************************************************************)
constructor TAddDrugOrderDlg.Init(aParent: PWindowsObject; ordr: PTestResObj;
                                   str1, str2: PChar);
begin
  inherited Init(aParent, MakeIntResource(DLG_CVT18EDITORD));
  desc:= New(PEdit, InitResource(@self, LBL_DESC, 0));
  grptext:= New(PEdit, InitResource(@self, LBL_GRPID, 0));
  ordId:= New(PFocEdit, InitResource(@self, IDC_ORDID, 9));
  grpId:= New(PFocEdit, InitResource(@self, IDC_GRPID, 9));
  ordDesc:= New(PEdit, InitResource(@self, IDC_ORDDESC, 33));
  grpDesc:= New(PEdit, InitResource(@self, IDC_GRPDESC, 33));
  anOrd := ordr;
  aGrpStr := str1;
  aTitleStr := str2;
end;

procedure TAddDrugOrderDlg.SetupWindow;
begin
  inherited SetupWindow;
  desc^.SetText(aTitleStr);
  grpText^.SetText(aGrpStr);
  ordId^.SetText(anOrd^.orderId);
  ordDesc^.SetText(anOrd^.orderDesc);
  grpId^.SetText(anOrd^.tstgrpID);
  grpDesc^.SetText(anOrd^.tstgrpDesc);
end;

procedure TAddDrugOrderDlg.ListKey(var msg: TMessage);
var
  pstr  : array[0..100] of char;
begin
  StrCopy(pstr, 'TEST');
  case msg.wParam of
    IDC_ORDID :   {- order list }
      if ListBoxOrders(@self, pstr, sizeof(pstr)-1) then
      begin
        ordId^.SetText(pstr);
        PostMessage(hWindow, WM_LOSTFOCUS, IDC_ORDID, 0);
      end;
    IDC_GRPID :   {- group list }
      if ListBoxGroups(@self, pstr, sizeof(pstr)-1) then
      begin
        grpId^.SetText(pstr);
        PostMessage(hWindow, WM_LOSTFOCUS, IDC_GRPID, 0);
      end;
  end;
end;

procedure TAddDrugOrderDlg.Cancel(var msg: TMessage);
begin
  if ConfirmExit(hWindow) then
    inherited Cancel(msg);
end;

procedure TAddDrugOrderDlg.LostFocus(var msg: TMessage);
var
  pstr  : array[0..50] of char;
  aDesc : array[0..50] of char;
  gId   : array[0..50] of char;
  gDesc : array[0..50] of char;
  oSeq  : TSeqNum;
  gSeq  : TSeqNum;
  ret   : boolean;

begin
  if msg.wParam = IDC_GRPID then  {- group lost focus }
  begin
    grpId^.GetText(pstr, sizeof(pstr)-1);
    if (StrComp(pstr, anOrd^.tstgrpId) <> 0) then
    begin
      StrCopy(anOrd^.tstgrpId, pstr);
      gSeq := GroupExists(pstr, aDesc);
      if gSeq > 0 then
      begin
        ret := VerifyGroup(gSeq, anOrd^.drugAbbr);
        if ret then
        begin
          grpDesc^.SetText(aDesc);
(*          EnableWindow(grpDesc^.hWindow, false);*)
        end
        else
        begin
          ErrorMsg(hWindow, 'Error', 'Group is not compatible with Additional Drugs.');
          FocusCtl(hWindow, IDC_GRPID);
        end;
      end;
    end;
  end
  else if msg.wParam = IDC_ORDID then  {- order lost focus }
  begin
    ordId^.GetText(pstr, sizeof(pstr)-1);
    if (StrComp(pstr, anOrd^.orderID) <> 0) then
    begin
      StrCopy(anOrd^.orderID, pstr);
      oSeq := OrderExists(pstr, aDesc);
      if oSeq  > 0 then
      begin
        gSeq := GetTstGrp(oSeq, gId, gDesc);
        if gSeq > 0 then
        begin
          ret := VerifyGroup(gSeq, anOrd^.drugAbbr);
          if ret then
          begin
            ordDesc^.SetText(aDesc);
            grpId^.SetText(gId);
            grpDesc^.SetText(gDesc);
            EnableWindow(ordDesc^.hWindow, false);
            EnableWindow(grpId^.hWindow, false);
            EnableWindow(grpDesc^.hWindow, false);
          end
          else
          begin
            ErrorMsg(hWindow, 'Error', 'Order is not compatible with Additional Drugs.');
            FocusCtl(hWindow, IDC_ORDID);
          end;
        end
        else
        begin
          ErrorMsg(hWindow, 'Error', 'Order is not compatible with Additional Drugs.');
          FocusCtl(hWindow, IDC_ORDID);
        end;
      end
      else
      begin
        ordDesc^.SetText('');
        EnableWindow(ordDesc^.hWindow, true);
      end
    end;
  end;
end;

function TAddDrugOrderDlg.CanClose: boolean;
var
  ret   : boolean;
  oSeq  : TSeqNum;
  gSeq  : TSeqNum;
  tw    : TWaitCursor;
  oDesc : array[0..35] of char;
  gDesc : array[0..35] of char;
  aStr  : array[0..35] of char;

  procedure CheckLen(s, msg: PChar; id: integer);
  begin
    if ret then
      if StrLen(s) = 0 then
      begin
        ErrorMsg(hWindow, 'Error', msg);
        FocusCtl(hWindow, id);
        ret:= false;
      end;
  end;

begin
  tw.Init;

  {- force focus processing }
  PostMessage(hWindow, WM_LOSTFOCUS, IDC_ORDID, 0);
  PostMessage(hWindow, WM_LOSTFOCUS, IDC_GRPID, 0);

  ret:= true;
  ordId^.GetText(anOrd^.orderId, 10);
  ordDesc^.GetText(anOrd^.orderDesc, 32);
  grpId^.GetText(anOrd^.tstgrpId, 10);
  grpDesc^.GetText(anOrd^.tstgrpDesc, 32);

  CheckLen(anOrd^.orderID, 'Order ID is required', IDC_ORDID);
  if ret then
    CheckLen(anOrd^.orderDesc, 'Order description is required', IDC_ORDDESC);
  if ret then
    CheckLen(anOrd^.tstgrpID, 'Test group ID is required', IDC_GRPID);
  if ret then
    CheckLen(anOrd^.tstgrpDesc, 'Test group description is required', IDC_GRPDESC);

  if ret then
  begin
    oSeq:= OrderExists(anOrd^.orderID, oDesc);
    gSeq := GroupExists(anOrd^.tstgrpId, gDesc);
    if (oSeq > 0) then
    {- if order exists, then make sure its compatible with the test group }
    begin
      ret:= VerifyOrderAndTstGrp(oSeq, gSeq);
      if not ret then
      begin
        ErrorMsg(hWindow, 'Error', 'Order is not compatible with test group');
        FocusCtl(hWindow, IDC_ORDID);
      end;
    end
    else
    {- if order does not exist, then a test group is required }
    begin
      if gSeq = 0 then
        gSeq := CreateGroup(anOrd^.tstgrpId, anOrd^.tstgrpDesc, anOrd^.drugAbbr)
      else
      begin
        ret := VerifyGroup(gSeq, anOrd^.drugAbbr);
        if not ret then
        begin
          ErrorMsg(hWindow, 'Error', 'Test group is not compatible with additional drugs');
          FocusCtl(hWindow, IDC_ORDID);
        end;
      end;
      if ret then
        oSeq := CreateOrder(anOrd^.orderId, anOrd^.orderDesc, 8, gSeq)
    end;
    if ret then
    begin
      anOrd^.tstgrpRef := gSeq;
      anOrd^.orderRef := oSeq;
    end;
  end;
  CanClose:= ret;
  tw.Done;
end;

(************************************************************************)
(*                                                                      *)
(************************************************************************)
constructor TNODupsOrderDlg.Init(aParent: PWindowsObject; aDupObj: PDups; aStr: PChar);
begin
  inherited Init(aParent, MakeIntResource(DLG_CVT18NODUPS));
  desc := New(PEdit, InitResource(@self, LBL_DESC, 0));
  ordId := New(PFocEdit, InitResource(@self, IDC_ORDID, 9));
  ordDesc := New(PEdit, InitResource(@self, IDC_ORDDESC, 33));
(*  panelId := New(PFocEdit, InitResource(@self, IDC_PANELID, 9));*)
(*  panelDesc := New(PEdit, InitResource(@self, IDC_PANELDESC, 33));*)
  addId := New(PFocEdit, InitResource(@self, IDC_ADDID, 9));
  addDesc := New(PEdit, InitResource(@self, IDC_ADDDESC, 33));
  dupObj := aDupObj;
  aTitle := aStr;
end;

procedure TNODupsOrderDlg.SetupWindow;
begin
  inherited SetupWindow;
  desc^.SetText(aTitle);

(*  FocusCtl(hWindow, IDC_ORDID);*)
(*  panelID^.SetText(dupObj^.pnlID);*)
(*  panelDesc^.SetText(dupObj^.pnlDesc);*)
(*  if dupObj^.pnlRef <> 0 then*)
(*  begin*)
(*    EnableWindow(panelId^.hWindow, false);*)
(*    EnableWindow(panelDesc^.hWindow, false);*)
(*  end;*)

  addId^.SetText(dupObj^.addID);
  addDesc^.SetText(dupObj^.addDesc);
  if dupObj^.addRef <> 0 then
  begin
    EnableWindow(addId^.hWindow, false);
    EnableWindow(addDesc^.hWindow, false);
  end;
end;

procedure TNODupsOrderDlg.Cancel(var msg: TMessage);
begin
  if ConfirmExit(hWindow) then
    inherited Cancel(msg);
end;

procedure TNODupsOrderDlg.ListKey(var msg: TMessage);
var
  pstr  : array[0..100] of char;
begin
  StrCopy(pstr, 'TEST');
  case msg.wParam of
    IDC_ORDID :   {- order list }
      if ListBoxOrders(@self, pstr, sizeof(pstr)-1) then
      begin
        ordId^.SetText(pstr);
        PostMessage(hWindow, WM_LOSTFOCUS, IDC_ORDID, 0);
      end;
    IDC_ADDID :   {- group list }
      if ListBoxGroups(@self, pstr, sizeof(pstr)-1) then
      begin
        addId^.SetText(pstr);
        PostMessage(hWindow, WM_LOSTFOCUS, IDC_ADDID, 0);
      end;
  end;
end;

procedure TNODupsOrderDlg.LostFocus(var msg: TMessage);
var
  pstr  : array[0..50] of char;
  pstr1 : array[0..50] of char;
  aDesc : array[0..50] of char;
  oSeq  : TSeqNum;
  gSeq  : TSeqNum;
  ret   : boolean;

begin
  if msg.wParam = IDC_ADDID then  {- additional drug group lost focus }
  begin
    addId^.GetText(pstr, sizeof(pstr)-1);
    if (StrComp(pstr, dupObj^.addId) <> 0) then
    begin
      StrCopy(dupObj^.addId, pstr);
      gSeq := GroupExists(pstr, aDesc);
      if gSeq > 0 then
      begin
        ret := VerifyGroup(gSeq, dupObj^.addDrug);
        if ret then
        begin
          addDesc^.SetText(aDesc);
          EnableWindow(addDesc^.hWindow, false);
          dupObj^.addRef := gSeq;
        end
        else
        begin
          ErrorMsg(hWindow, 'Error', 'Group is not compatible with Additional Drugs.');
          FocusCtl(hWindow, IDC_ADDID);
        end;
      end;
    end;
  end
  else if msg.wParam = IDC_ORDID then  {- order lost focus }
  begin
    ordId^.GetText(pstr, sizeof(pstr)-1);
    if (StrComp(pstr, dupObj^.ordID) <> 0) then
    begin
(*      addId^.GetText(pstr1, sizeof(pstr)-1);*)
(*      if StrLen(pstr1) <> 0 then*)
(*      begin*)
        StrCopy(dupObj^.ordID, pstr);
        oSeq := OrderExists(pstr, aDesc);
        if oSeq  > 0 then
        begin
          ret := CompOrderWithTestGroups(oSeq, dupObj^.pnlRef, dupObj^.addRef);
          if ret then
          begin
            ordDesc^.SetText(aDesc);
          end
          else
          begin
            ErrorMsg(hWindow, 'Error', 'Order is not compatible with Additional Drugs.');
            FocusCtl(hWindow, IDC_ORDID);
          end;
        end
        else
        begin
          ordDesc^.SetText('');
(*          EnableWindow(ordDesc^.hWindow, true);*)
        end
(*      end*)
(*      else*)
(*      begin*)
(*        ErrorMsg(hWindow, 'Error', 'Order must have a test Group for Additional Drugs.');*)
(*        FocusCtl(hWindow, IDC_ADDID);*)
(*      end;*)
    end;
  end;
end;

function TNODupsOrderDlg.CanClose: boolean;
var
  aDesc : array[0..50] of char;
  ret   : boolean;
  tw    : TWaitCursor;
  oSeq  : TSeqNum;
  gSeq  : TSeqNum;

  procedure CheckLen(s, msg: PChar; id: integer);
  begin
    if ret then
      if StrLen(s) = 0 then
      begin
        ErrorMsg(hWindow, 'Error', msg);
        FocusCtl(hWindow, id);
        ret:= false;
      end;
  end;

begin
  tw.Init;
  {- force focus processing }
  PostMessage(hWindow, WM_LOSTFOCUS, IDC_ORDID, 0);
  PostMessage(hWindow, WM_LOSTFOCUS, IDC_ADDID, 0);

  ret:= true;
  ordId^.GetText(dupObj^.ordId, 10);
  ordDesc^.GetText(dupObj^.ordDesc, 32);
  addId^.GetText(dupObj^.addId, 10);
  addDesc^.GetText(dupObj^.addDesc, 32);

  CheckLen(dupObj^.ordID, 'Order ID is required', IDC_ORDID);
  if ret then
    CheckLen(dupObj^.ordDesc, 'Order description is required', IDC_ORDDESC);
  if ret then
    CheckLen(dupObj^.addID, 'Additional Drug Test group ID is required', IDC_ADDID);
  if ret then
    CheckLen(dupObj^.addDesc, 'Additional Drug Test group description is required', IDC_ADDDESC);

  if ret then
  begin
    oSeq:= OrderExists(dupObj^.ordID, aDesc);
    gSeq := GroupExists(dupObj^.addId, aDesc);
    if (oSeq > 0) then
    {- if order exists, then make sure its compatible with the test group }
    begin
      ret := CompOrderWithTestGroups(oSeq, dupObj^.pnlRef, gSeq);
      if not ret then
      begin
        ErrorMsg(hWindow, 'Error', 'Order is not compatible with test groups');
        FocusCtl(hWindow, IDC_ORDID);
      end;
    end
    else
    begin {- if order does not exist, then a test group is required }
      if gSeq = 0 then
        gSeq := CreateGroup(dupObj^.addID, dupObj^.addDesc, dupObj^.addDrug)
      else
      begin
        ret := VerifyGroup(gSeq, dupObj^.addDrug);
        if not ret then
        begin
          ErrorMsg(hWindow, 'Error', 'Test group is not compatible with additional drugs');
          FocusCtl(hWindow, IDC_ADDID);
        end;
      end;
      if ret then
        oSeq := CreateOrder2(dupObj^.ordId, dupObj^.ordDesc, 8, dupObj^.pnlRef, gSeq);
    end;
    if ret then
    begin
      dupObj^.addRef := gSeq;
      dupObj^.ordRef := oSeq;
    end;
  end;

  CanClose:= ret;
  tw.Done;
end;

(************************************************************************)
(*                                                                      *)
(************************************************************************)
constructor TDupsOrderDlg.Init(aParent: PWindowsObject; aDupObj: PDupsOrd;
                               aStr: PChar; aSpecSeq : TSeqNum);
begin
  inherited Init(aParent, MakeIntResource(DLG_CVT18ISOMAP));
  dupObj := aDupObj;
  grid:= New(PGrid, InitResource(@self, IDC_GRID, 4, false));
  specSeq := aSpecSeq;
  aTitle := aStr;
end;

procedure TDupsOrderDlg.SetupWindow;
begin
  inherited SetupWindow;
  grid^.SetHeader(g_Proc, 'Description');
  grid^.SetHeader(g_Order, 'Order');
  grid^.SetHeader(g_Group, 'Test Group');
  grid^.SetHeader(g_Iso, 'Isolate');
  FillGrid;
  grid^.SetSelIndex(0);
end;

function TDupsOrderDlg.CanClose: boolean;
var
  ret   : boolean;
  db    : PDBFile;

  function IsoExists(isoID: PChar): boolean;
  begin
    db^.dbr^.PutField(DBISOSpecRef, @specSeq);
    db^.dbr^.PutFieldAsStr(DBISOIso, isoID);
    IsoExists:= db^.dbc^.GetEQ(db^.dbr);
  end;

begin
  ret:= true;

  if strlen(dupObj^.pnlOId) = 0 then
  begin
    ErrorMsg(hWindow, 'Error', 'Order ID is required.');
    grid^.SetSelIndex(0);
    ret:= false;
  end;
  if ret then
  begin
    if strlen(dupObj^.pnltgId) = 0 then
    begin
      ErrorMsg(hWindow, 'Error', 'Test Group ID is required.');
      grid^.SetSelIndex(0);
      ret:= false;
    end;
  end;
  if ret then
  begin
    if strlen(dupObj^.pnlISO) = 0 then
    begin
      ErrorMsg(hWindow, 'Error', 'Isolate ID is required.');
      grid^.SetSelIndex(0);
      ret:= false;
    end;
  end;
  if ret then
  begin
    if strlen(dupObj^.addOId) = 0 then
    begin
      ErrorMsg(hWindow, 'Error', 'Order ID is required.');
      grid^.SetSelIndex(1);
      ret:= false;
    end;
  end;
  if ret then
  begin
    if strlen(dupObj^.addtgId) = 0 then
    begin
      ErrorMsg(hWindow, 'Error', 'Test Group ID is required.');
      grid^.SetSelIndex(1);
      ret:= false;
    end;
  end;
  if ret then
  begin
    if strlen(dupObj^.addISO) = 0 then
    begin
      ErrorMsg(hWindow, 'Error', 'Isolate ID is required.');
      grid^.SetSelIndex(1);
      ret:= false;
    end;
  end;
  {- now validate that all isolates are unique and do not already exist }
  if ret then
  begin
    db:= OpenV30File(DBIsoFile);

    if  (StrComp(dupObj^.pnlISO, dupObj^.addISO) = 0) then
    begin
      ErrorMsg(hWindow, 'Error', 'Duplicate isolate id entered.');
      grid^.SetSelIndex(0);
      ret:= false;
    end
    else
    begin
      if IsoExists(dupObj^.pnlISO) then
      begin
        ErrorMsg(hWindow, 'Error', 'Isolate already exists in database.');
        grid^.SetSelIndex(0);
        ret:= false;
      end
      else
      begin
        if IsoExists(dupObj^.addISO) then
        begin
          ErrorMsg(hWindow, 'Error', 'Isolate already exists in database.');
          grid^.SetSelIndex(1);
          ret:= false;
        end;
      end;
    end;
    Dispose(db, Done);
  end;

  FocusCtl(hWindow, IDC_GRID);
  CanClose:= ret;
end;

procedure TDupsOrderDlg.FillGrid;
var
  pstr  : array[0..100] of char;
  row   : integer;
  k     : integer;
  seq   : TSeqNum;
begin
  grid^.SetRedraw(false);
  grid^.ClearList;

  row:= grid^.AddString('');
  grid^.SetData(g_Proc,  row, dupObj^.pnltgDesc);
  grid^.SetData(g_Order, row, dupObj^.pnlOID);
  grid^.SetData(g_Group, row, dupObj^.pnltgID);
  grid^.SetData(g_Iso,   row, dupObj^.pnlISO);
  row:= grid^.AddString('');
  grid^.SetData(g_Proc,  row, 'Additional Drugs');
  grid^.SetData(g_Order, row, dupObj^.addOID);
  grid^.SetData(g_Group, row, dupObj^.addtgID);
  grid^.SetData(g_Iso,   row, dupObj^.addISO);
  grid^.SetColumnWidth(g_Proc, 1);
  grid^.SetColumnWidth(g_Order, 1);
  grid^.SetColumnWidth(g_Group, 1);
  grid^.SetColumnWidth(g_Iso, 1);
  grid^.OptColumnWidth(g_Proc);
  grid^.OptColumnWidth(g_Order);
  grid^.OptColumnWidth(g_Group);
  grid^.StretchColumn(g_Iso);
  grid^.SetRedraw(true);
end;

procedure TDupsOrderDlg.Cancel(var msg: TMessage);
begin
  if ConfirmExit(hWindow) then
    inherited Cancel(msg);
end;

procedure TDupsOrderDlg.WMCommand(var msg: TMessage);
begin
  inherited WMCommand(msg);
  if (msg.lParamLo <> 0) then   {- message is from a control }
  begin
    if msg.lParamHI = LBN_DBLCLK then
      IDEdit(msg);
  end;
end;

procedure TDupsOrderDlg.WMCharToItem(var msg: TMessage);
begin
  grid^.CharToItem(msg);
end;

procedure TDupsOrderDlg.WMDrawItem(var msg: TMessage);
begin
  grid^.DrawItem(msg);
end;

procedure TDupsOrderDlg.WMMeasureItem(var msg: TMessage);
begin
  grid^.MeasureItem(msg);
end;

procedure TDupsOrderDlg.IDEdit(var msg: TMessage);
var
  k     : integer;
  i     : integer;
  aDesc : array [0..50] of char;
  o     : POrd;
  t     : integer;

begin
  if MaxAvail < SizeOf(TOrd) then
    CriticalError('Cannot allocate enough memory to continue to convert data.');
  GetMem(o, SizeOf(TOrd));

  k:= grid^.GetSelIndex;
  if k = 0 then
  begin
    StrCopy(aDesc, dupObj^.pnltgDesc);
    StrCopy(o^.oID, dupObj^.pnlOId);
    StrCopy(o^.oDesc, dupObj^.pnlODesc);
    o^.oRef := dupObj^.pnlORef;
    StrCopy(o^.tgID, dupObj^.pnltgId);
    StrCopy(o^.tgDesc, dupObj^.pnltgDesc);
    o^.gRef := dupObj^.pnltgRef;
    StrCopy(o^.iSO, dupObj^.pnlISO);
    t := 1;
  end;
  if k = 1 then
  begin
    StrCopy(aDesc,'Additional Drugs - ');
    for i := 1 to 5 do
    begin
      StrCat(aDesc, dupObj^.addDrug[i]);
      StrCat(aDesc, ' ');
    end;
    StrCopy(o^.oID, dupObj^.addOId);
    StrCopy(o^.oDesc, dupObj^.addODesc);
    o^.oRef := dupObj^.addORef;
    StrCopy(o^.tgID, dupObj^.addtgId);
    StrCopy(o^.tgDesc, dupObj^.addtgDesc);
    o^.gRef := dupObj^.addtgRef;
    StrCopy(o^.iSO, dupObj^.addISO);
    o^.addDrug := dupObj^.addDrug;
    t := 2;
  end;

  if (application^.ExecDialog(New(PISOORDDlg, Init(@self, t, o, aDesc))) = IDOK) then
  begin
    if k = 0 then
    begin
      StrCopy(dupObj^.pnlOId, o^.oID);
      StrCopy(dupObj^.pnlODesc, o^.oDesc);
      dupObj^.pnlORef := o^.oRef;
      StrCopy( dupObj^.pnltgId, o^.tgID);
      StrCopy(dupObj^.pnltgDesc, o^.tgDesc);
      dupObj^.pnltgRef := o^.gRef;
      StrCopy(dupObj^.pnlISO, o^.iSO);
    end;
    if k = 1 then
    begin
      StrCopy(dupObj^.addOId, o^.oID);
      StrCopy(dupObj^.addODesc, o^.oDesc);
      dupObj^.addORef := o^.oRef;
      StrCopy( dupObj^.addtgId, o^.tgID);
      StrCopy(dupObj^.addtgDesc, o^.tgDesc);
      dupObj^.addtgRef := o^.gRef;
      StrCopy(dupObj^.addISO, o^.iSO);
    end;
  end;
  FillGrid;
  grid^.SetSelIndex(k);

  FreeMem(o, SizeOf(TOrd));
end;

procedure TDupsOrderDlg.IDDone(var msg: TMessage);
begin
  if CanClose then
    EndDlg(IDOK);
end;

(************************************************************************)
(*                                                                      *)
(************************************************************************)
constructor TISOORDDlg.Init(aParent: PWindowsObject; ordType : integer;
                            ordr: POrd; str: PChar);
begin
  inherited Init(aParent, MakeIntResource(DLG_CVT18ISOORD));
  desc:= New(PEdit, InitResource(@self, LBL_DESC, 0));
  ordId:= New(PFocEdit, InitResource(@self, IDC_ORDID, 9));
  grpId:= New(PFocEdit, InitResource(@self, IDC_TSTGRPID, 9));
  isoId:= New(PFocEdit, InitResource(@self, IDC_ISOID, 4));
  ordDesc:= New(PEdit, InitResource(@self, IDC_ORDDESC, 33));
  grpDesc:= New(PEdit, InitResource(@self, IDC_TSTGRPDESC, 33));
  aType := ordType;
  anOrd := ordr;
  aTitleStr:= str;
end;

procedure TISOORDDlg.SetupWindow;
begin
  inherited SetupWindow;

  desc^.SetText(aTitleStr);
  ordId^.SetText(anOrd^.oId);
  grpId^.SetText(anOrd^.tgId);
  isoId^.SetText(anOrd^.iso);
  ordDesc^.SetText(anOrd^.oDesc);
  grpDesc^.SetText(anOrd^.tgDesc);

  if StrLen(anOrd^.oId) <> 0 then
  begin
    EnableWindow(ordId^.hWindow, false);
    EnableWindow(ordDesc^.hWindow, false);
  end;

  if StrLen(anOrd^.tgId) <> 0 then
  begin
    EnableWindow(grpId^.hWindow, false);
    EnableWindow(grpDesc^.hWindow, false);
  end;

end;

procedure TIsoOrdDlg.Cancel(var msg: TMessage);
begin
(*  if ConfirmExit(hWindow) then*)
  inherited Cancel(msg);
end;

procedure TISOORDDlg.ListKey(var msg: TMessage);
var
  pstr  : array[0..100] of char;
begin
  StrCopy(pstr, 'TEST');
  case msg.wParam of
    IDC_ORDID :   {- order list }
      if ListBoxOrders(@self, pstr, sizeof(pstr)-1) then
      begin
        ordId^.SetText(pstr);
        PostMessage(hWindow, WM_LOSTFOCUS, IDC_ORDID, 0);
      end;
    IDC_TSTGRPID :   {- group list }
      if ListBoxGroups(@self, pstr, sizeof(pstr)-1) then
      begin
        grpId^.SetText(pstr);
        PostMessage(hWindow, WM_LOSTFOCUS, IDC_TSTGRPID, 0);
      end;
  end;
end;

procedure TISOORDDlg.LostFocus(var msg: TMessage);
var
  pstr  : array[0..50] of char;
  aDesc : array[0..50] of char;
  gDesc : array[0..50] of char;
  gId   : array[0..50] of char;
  seq   : TSeqNum;
  gSeq  : TSeqNum;
  ret   : boolean;

begin
  if msg.wParam = IDC_ISOID then
  begin
    isoID^.GetText(pstr, 5);
    FormatDMSField(pstr, 3);
    isoID^.SetText(pstr);
  end
  else if msg.wParam = IDC_TSTGRPID then  {- group lost focus }
  begin
    grpId^.GetText(pstr, sizeof(pstr)-1);
    if (StrComp(pstr, anOrd^.tgId) <> 0) then
    begin
      StrCopy(anOrd^.tgId, pstr);
      seq := GroupExists(pstr, aDesc);
      if seq > 0 then
      begin
        ret := VerifyGroup(seq, anOrd^.addDrug);
        if ret then
        begin
          if anOrd^.oRef > 0 then
          begin
            ret:= VerifyOrderAndTstGrp(anOrd^.oRef, seq);
            if not ret then
            begin
              ErrorMsg(hWindow, 'Error', 'Test Group is not compatible with Order.');
              FocusCtl(hWindow, IDC_TSTGRPID);
            end
            else
            begin
              anOrd^.gRef := seq;
              grpDesc^.SetText(aDesc);
              FocusCtl(hWindow, IDC_ISOID);
            end;
          end
          else
          begin
            anOrd^.gRef := seq;
            grpDesc^.SetText(aDesc);
            FocusCtl(hWindow, IDC_ORDID);
          end;
        end
        else
        begin
          ErrorMsg(hWindow, 'Error', 'Group is not compatible with Additional Drugs.');
          FocusCtl(hWindow, IDC_TSTGRPID);
        end;
      end;
    end;
  end
  else if msg.wParam = IDC_ORDID then  {- order lost focus }
  begin
    ordId^.GetText(pstr, sizeof(pstr)-1);
    if (StrComp(pstr, anOrd^.oId) <> 0) then
    begin
      StrCopy(anOrd^.oId, pstr);
      seq := OrderExists(pstr, aDesc);
      if seq > 0 then
      begin
        if anOrd^.gRef > 0 then
        begin
          ret:= VerifyOrderAndTstGrp(seq, anOrd^.gRef);
          if not ret then
          begin
            ErrorMsg(hWindow, 'Error', 'Order is not compatible with Test Group.');
            FocusCtl(hWindow, IDC_ORDID);
          end
          else
          begin
            anOrd^.oRef := seq;
            ordDesc^.SetText(aDesc);
            FocusCtl(hWindow, IDC_ISOID);
          end;
        end
        else
        begin
          if aType = 2 then
          begin
            if FindTstGrp(seq, anOrd^.addDrug, gSeq, gId, gDesc) then
            begin
              anOrd^.oRef := seq;
              anOrd^.gRef := gSeq;
              grpId^.SetText(gId);
              grpDesc^.SetText(gDesc);
              ordDesc^.SetText(aDesc);
              EnableWindow(grpId^.hWindow, false);
              EnableWindow(grpDesc^.hWindow, false);
              EnableWindow(ordId^.hWindow, false);
              EnableWindow(ordDesc^.hWindow, false);
              FocusCtl(hWindow, IDC_ISOID);
            end
            else
            begin
              ErrorMsg(hWindow, 'Error', 'Order is not compatible with Additional Drugs.');
              FocusCtl(hWindow, IDC_ORDID);
            end;
          end
          else
          begin
            anOrd^.oRef := seq;
            ordDesc^.SetText(aDesc);
            FocusCtl(hWindow, IDC_ISOID);
          end;
        end;
      end;
    end;
  end;
end;

function TISOORDDlg.CanClose: boolean;
var
  ret   : boolean;
  oSeq  : TSeqNum;
  gSeq  : TSeqNum;
  tw    : TWaitCursor;
  oDesc : array[0..35] of char;
  gDesc : array[0..35] of char;
  aStr  : array[0..35] of char;

  procedure CheckLen(s, msg: PChar; id: integer);
  begin
    if ret then
      if StrLen(s) = 0 then
      begin
        ErrorMsg(hWindow, 'Error', msg);
        FocusCtl(hWindow, id);
        ret:= false;
      end;
  end;

begin
  tw.Init;

  SendMessage(hWindow, WM_LOSTFOCUS, IDC_ISOID, 0);

  ordId^.GetText(anOrd^.oId, sizeof(anOrd^.oId)-1);
  grpId^.GetText(anOrd^.tgId, sizeof(anOrd^.tgId)-1);
  isoId^.GetText(anOrd^.iso, sizeof(anOrd^.iso)-1);
  ordDesc^.GetText(anOrd^.oDesc, sizeof(anOrd^.oDesc)-1);
  grpDesc^.GetText(anOrd^.tgDesc, sizeof(anOrd^.tgDesc)-1);

  CheckLen(anOrd^.oId, 'Order ID is required', IDC_ORDID);
  if ret then
    CheckLen(anOrd^.oDesc, 'Order description is required', IDC_ORDDESC);
  if ret then
    CheckLen(anOrd^.tgId, 'Test group ID is required', IDC_TSTGRPID);
  if ret then
    CheckLen(anOrd^.tgDesc, 'Test group description is required', IDC_TSTGRPDESC);
  if ret then
    CheckLen(anOrd^.iso, 'Isolate Id is required', IDC_ISOID);

  if ret then
  begin
    oSeq:= OrderExists(anOrd^.oID, oDesc);
    gSeq := GroupExists(anOrd^.tgId, gDesc);
    if (oSeq > 0) then
    {- if order exists, then make sure its compatible with the test group }
    begin
      ret:= VerifyOrderAndTstGrp(oSeq, gSeq);
      if not ret then
      begin
        ErrorMsg(hWindow, 'Error', 'Order is not compatible with test group');
        FocusCtl(hWindow, IDC_ORDID);
      end;
    end
    else
    begin   {- if order does not exist, then a test group is required }
      if gSeq = 0 then
        gSeq := CreateGroup(anOrd^.tgId, anOrd^.tgDesc, anOrd^.addDrug)
      else
      begin
        if aType = 2 then
        begin
          ret := VerifyGroup(gSeq, anOrd^.addDrug);
          if not ret then
          begin
            ErrorMsg(hWindow, 'Error', 'Test group is not compatible with additional drugs');
            FocusCtl(hWindow, IDC_ORDID);
          end;
        end;
      end;
      if ret then
        oSeq := CreateOrder(anOrd^.oId, anOrd^.oDesc, 8, gSeq)
    end;
    if ret then
    begin
      anOrd^.gRef := gSeq;
      anOrd^.oRef := oSeq;
    end;
  end;

  CanClose:= ret;
  tw.Done;
end;

(************************************************************************)
(*                                                                      *)
(************************************************************************)
function PromptForPanelOrder(aParent: PWindowsObject; panel: PPanelID): Boolean;
var
  aStr : array[0..25] of char;
begin
  StrCopy(aStr,'Panel Group');
  PromptForPanelOrder:= application^.ExecDialog(New(PPanelOrderDlg,
                        Init(aParent,panel,aStr))) = IDOK;
end;

function PromptForAddDrugOrder(aParent: PWindowsObject; aDrugObj: PTestResObj): boolean;
var
  aStr1 : array[0..25] of char;
  aStr2 : array[0..100] of char;
  i     : integer;
begin
  StrCopy(aStr1,'Add. Drug Group');
  StrCopy(aStr2,'Additional Drugs - ');
  for i := 1 to 5 do
  begin
    if StrLen(aDrugObj^.drugAbbr[i]) <> 0 then
    begin
      StrCat(aStr2, aDrugObj^.drugAbbr[i]);
      StrCat(aStr2, ' ');
    end;
  end;
  PromptForAddDrugOrder:= application^.ExecDialog(New(PAddDrugOrderDlg,
                          Init(aParent,aDrugObj, aStr1, aStr2))) = IDOK;
end;

function PromptForNoDupsOrder(aParent : PWindowsObject;
                              aPanel   : PPanelID;
                              aDrugObj: PTestResObj;
                              aDupObj : PPnlXref): boolean;
var
  aStr  : array[0..60] of char;
  i     : integer;
  nd    : PDups;
begin
  if MaxAvail < SizeOf(TDups) then
    CriticalError('Cannot allocate enough memory to continue to convert data.');
  GetMem(nd, SizeOf(TDups));

  StrCopy(nd^.ordID, aDupObj^.ordID);
  StrCopy(nd^.ordDesc, aDupObj^.ordDesc);
  nd^.ordRef := aDupObj^.ordSeq;
  StrCopy(nd^.pnlID, aPanel^.tstgrp);
  StrCopy(nd^.pnlDesc, aPanel^.panelDesc);
  nd^.pnlRef := aPanel^.tstgrpRef;
  StrCopy(nd^.addID, aDrugObj^.tstgrpID);
  StrCopy(nd^.addDesc, aDrugObj^.tstgrpDesc);
  nd^.addRef := aDrugObj^.tstgrpRef;
  nd^.addDrug := aDrugObj^.drugAbbr;

  StrCopy(aStr, nd^.pnlDesc);
  StrCat(aStr, '-- ( ');
  for i := 1 to 5 do
  begin
    if StrLen(nd^.addDrug[i]) <> 0 then
    begin
      StrCat(aStr, nd^.addDrug[i]);
      StrCat(aStr, ' ');
    end;
  end;
  StrCat(aStr, ')');

  PromptForNoDupsOrder := application^.ExecDialog(New(PNODupsOrderDlg,
                          Init(aParent, nd, aStr))) = IDOK;

  StrCopy(aDupObj^.ordID, nd^.ordID);
  StrCopy(aDupObj^.ordDesc, nd^.ordDesc);
  aDupObj^.ordSeq := nd^.ordRef;

  StrCopy(aDrugObj^.tstgrpID, nd^.addID);
  StrCopy(aDrugObj^.tstgrpDesc, nd^.addDesc);
  aDrugObj^.tstgrpRef := nd^.addRef;

  FreeMem(nd, SizeOf(TDups));
end;

function PromptForDupsOrder(aParent  : PWindowsObject;
                            aPanel   : PPanelID;
                            aDrugObj : PTestResObj;
                            aSpecSeq : TSeqNum): boolean;
var
  aStr  : array[0..60] of char;
  i     : integer;
  d    : PDupsOrd;
begin
  if MaxAvail < SizeOf(TDupsOrd) then
    CriticalError('Cannot allocate enough memory to continue to convert data.');
  GetMem(d, SizeOf(TDupsOrd));

  StrCopy(d^.pnlOID, aPanel^.orderId);
  StrCopy(d^.pnlODesc, aPanel^.orderDesc);
  d^.pnlORef := aPanel^.orderRef;
  StrCopy(d^.pnltgID, aPanel^.tstgrp);
  StrCopy(d^.pnltgDesc, aPanel^.panelDesc);
  d^.pnltgRef := aPanel^.tstgrpRef;
  StrCopy(d^.pnlISO, aPanel^.isolateID);

  StrCopy(d^.addOID, aDrugObj^.orderId);
  StrCopy(d^.addODesc, aDrugObj^.orderDesc);
  d^.addORef := aDrugObj^.orderRef;
  StrCopy(d^.addtgID, aDrugObj^.tstgrpID);
  StrCopy(d^.addtgDesc, aDrugObj^.tstgrpDesc);
  d^.addtgRef := aDrugObj^.tstgrpRef;
  StrCopy(d^.addISO, aDrugObj^.iso);
  d^.addDrug := aDrugObj^.drugAbbr;

  StrCopy(aStr, d^.pnltgDesc);
  StrCat(aStr, '-- ( ');
  for i := 1 to 5 do
  begin
    if StrLen(d^.addDrug[i]) <> 0 then
    begin
      StrCat(aStr, d^.addDrug[i]);
      StrCat(aStr, ' ');
    end;
  end;
  StrCat(aStr, ')');

  PromptForDupsOrder := application^.ExecDialog(New(PDupsOrderDlg, Init(aParent,
                                     d, aStr, aSpecSeq))) = IDOK;

  StrCopy(aPanel^.orderId, d^.pnlOID);
  StrCopy(aPanel^.orderDesc, d^.pnlODesc);
  aPanel^.orderRef := d^.pnlORef;
  StrCopy(aPanel^.tstgrp, d^.pnltgID);
  StrCopy(aPanel^.panelDesc, d^.pnltgDesc);
  aPanel^.tstgrpRef := d^.pnltgRef;
  StrCopy(aPanel^.isolateID, d^.pnlISO);

  StrCopy(aDrugObj^.orderId, d^.addOID);
  StrCopy(aDrugObj^.orderDesc, d^.addODesc);
  aDrugObj^.orderRef := d^.addORef;
  StrCopy(aDrugObj^.tstgrpID, d^.addtgID);
  StrCopy(aDrugObj^.tstgrpDesc, d^.addtgDesc);
  aDrugObj^.tstgrpRef := d^.addtgRef;
  StrCopy(aDrugObj^.iso, d^.addISO);

  FreeMem(d, SizeOf(TDupsOrd));
end;

END.
