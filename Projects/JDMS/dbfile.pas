unit DBFile;

{- this module provides a container object for the three main DB objects,
   DBReadDesc, DBCursor, and DBRec }
{ It also provides a writable container object for DBReadWriteDesc, DBCursor,
  and DBRec. }

INTERFACE

uses
  Objects,
  DBLib;

type
  {- database file object - should be put into DBLIB !! }
  PDBFile = ^TDBFile;
  TDBFile = object(TObject)
    dbr   : PDBRec;       {- record object for the specified file }
    dbc   : PDBCursor;    {- cursor object for the specified file }
    dbd   : PDBReadDesc;  {- description object for the specified file }
    constructor Init(aFileName, aPath: PChar; openMode: integer);
    destructor Done; virtual;
  end;

  PDBReadWriteFile = ^TDBReadWriteFile;
  TDBReadWriteFile = object(TObject)
    dbr   : PDBRec;       {- record object for the specified file }
    dbc   : PDBCursor;    {- cursor object for the specified file }
    dbd   : PDBReadWriteDesc;  {- description object for the specified file }
    constructor Init(aFileName, aPath: PChar; openMode: integer);
    destructor Done; virtual;
  end;

  PDBCopyFile = ^TDBCopyFile;
  TDBCopyFile = object(TObject)
    dbr   : PDBCopyRec;       {- record object for the specified file }
    dbc   : PDBCopyCursor;    {- cursor object for the specified file }
    dbd   : PDBReadWriteDesc; {- description object for the specified file }
    constructor Init(aFileName, aPath: PChar; openMode: integer);
    destructor Done; virtual;
  end;

  PDBAssocFile = ^TDBAssocFile;
  TDBAssocFile = object(TObject)
    dbr   : PDBRec;         {- record object for the specified file }
    dbc   : PDBAssocCursor; {- cursor object for the specified file }
    dbd   : PDBReadDesc;  {- description object for the specified file }
    constructor Init(fromRec: PDBRec; assocNum: integer; openMode: integer);
    destructor Done; virtual;
  end;

IMPLEMENTATION

uses
  MScan,
  DMSDebug,
  DBTypes;

var
  stuff : TAssocParamList;

constructor TDBFile.Init(aFileName, aPath: PChar; openMode: integer);
{- open the specified file and create a cursor and record object }
begin
  dbd:= nil;
  dbr:= nil;
  dbc:= nil;

  dbd:= New(PDBReadDesc, Init(aFileName, aPath));
  if dbd <> nil then
  begin
    dbc:= New(PDBCursor, Init(dbd, openMode));
    if dbc <> nil then
    begin
      dbr:= New(PDBRec, Init(dbc));
      if dbr = nil then
      begin
        MSDisposeObj(dbd);
        MSDisposeObj(dbc);
        Fail;
      end;
    end
    else
    begin
      MSDisposeObj(dbd);
      Fail;
    end;
  end
  else
    Fail;
end;

destructor TDBFile.Done;
{- destroy the database objects }
begin
  inherited Done;
  MSDisposeObj(dbr);
  MSDisposeObj(dbc);
  MSDisposeObj(dbd);
end;

constructor TDBReadWriteFile.Init(aFileName, aPath: PChar; openMode: integer);
{- open the specified file and create a cursor and record object }
begin
  dbd:= nil;
  dbr:= nil;
  dbc:= nil;

  dbd:= New(PDBReadWriteDesc, Init(aFileName, aPath));
  if dbd <> nil then
  begin
    dbc:= New(PDBCursor, Init(dbd, openMode));
    if dbc <> nil then
    begin
      dbr:= New(PDBRec, Init(dbc));
      if dbr = nil then
      begin
        MSDisposeObj(dbd);
        MSDisposeObj(dbc);
        Fail;
      end;
    end
    else
    begin
      MSDisposeObj(dbd);
      Fail;
    end;
  end
  else
    Fail;
end;

destructor TDBReadWriteFile.Done;
{- destroy the database objects }
begin
  inherited Done;
  MSDisposeObj(dbr);
  MSDisposeObj(dbc);
  MSDisposeObj(dbd);
end;

constructor TDBCopyFile.Init(aFileName, aPath: PChar; openMode: integer);
{- open the specified file and create a cursor and record object }
begin
  dbd:= nil;
  dbr:= nil;
  dbc:= nil;

  dbd:= New(PDBReadWriteDesc, Init(aFileName, aPath));
  if dbd <> nil then
  begin
    dbc:= New(PDBCopyCursor, Init(dbd, openMode));
    if dbc <> nil then
    begin
      dbr:= New(PDBCopyRec, Init(dbc));
      if dbr = nil then
      begin
        MSDisposeObj(dbd);
        MSDisposeObj(dbc);
        Fail;
      end;
    end
    else
    begin
      MSDisposeObj(dbd);
      Fail;
    end;
  end
  else
    Fail;
end;

destructor TDBCopyFile.Done;
{- destroy the database objects }
begin
  inherited Done;
  MSDisposeObj(dbr);
  MSDisposeObj(dbc);
  MSDisposeObj(dbd);
end;


constructor TDBAssocFile.Init(fromRec: PDBRec; assocNum: integer; openMode: integer);
{- open the specified file and create a cursor and record object }
begin
  dbd:= nil;
  dbr:= nil;
  dbc:= nil;

  dbc := New(PDBAssocCursor, Init(fromRec, assocNum, stuff, openMode));
  if dbc <> nil then
  begin
    dbd:= dbc^.Desc;
    dbr:= New(PDBRec, Init(dbc));
    if dbr = nil then
    begin
      MSDisposeObj(dbc);
      Fail;
    end;
  end;
end;

destructor TDBAssocFile.Done;
{- destroy the database objects }
begin
  inherited Done;
  MSDisposeObj(dbr);
  MSDisposeObj(dbc);
end;

END.

{- rcf }
