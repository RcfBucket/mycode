unit SectFlds;

{- generic base object for section editors that contain fields
   ie, Gram Stain and Demographics }

INTERFACE

uses
  Objects,
  UserMsgs,
  WinTypes,
  StatBar,
  DlgLib,
  BoxWin,
  PageWin,
  RptShare,
  OWindows;

type
  PSectPage = ^TSectPage;
  TSectPage = object(TPageDlg)
    pFont   : PRptFont;
    constructor Init(AParent: PWindowsObject; aPage: TRptPage; aFont: PRptFont);
    procedure ShowBoxStatus(aBox: PBoxWin); virtual;
    function GetFontHeight: integer; virtual;
    procedure ClearAllBoxes; virtual;
    procedure ShowAllBoxes(aTitle: PChar); virtual;
  end;

  PSectFldEdit = ^TSectFldEdit;
  TSectFldEdit = object(TCenterDlg)
    sb       : PStatusBar;  {- status bar }
    pw       : PPageDlg;    {- page window }
    popMenu  : HMenu;       {- popup menu handle }
    sbFont   : HFont;       {- status bar font }
    name     : PChar;
    pFont    : PRptFont;    {- pointer to a report font }
    constructor Init(aParent: PWindowsObject; anID: integer; aName: PChar;
                     aSize: TPoint; aFont: PRptFont);
    destructor Done; virtual;
    procedure SetupWindow; virtual;
    procedure WMSize(var msg: TMessage); virtual WM_FIRST + WM_SIZE;
    procedure WMCtlColor(var msg: TMessage); virtual WM_FIRST + WM_CTLCOLOR;

    function InitPage(aSize: TPoint): PSectPage; virtual;

    procedure UMBoxStatus(var msg: TMessage); virtual WM_FIRST + UM_BOXSTATUS;

    {- menu messages }

    {- button messages }
    procedure IDZoom(var msg: TMessage); virtual ID_FIRST + IDC_ZOOM;
    procedure IDField(var msg: TMessage); virtual ID_FIRST + IDC_FIELD;
    procedure IDClose(var msg: TMessage); virtual ID_FIRST + IDC_CLOSE;

    {- public functions }
    procedure GetNewFont(var aFont: TRptFont);
  end;

  PAddFldTypes = ^TAddFldTypes;
  TAddFldTypes = object(TObject)
    name       : PChar;
    num        : integer;
    constructor Init(aName: PChar; aNum: integer);
    destructor Done; virtual;
  end;

  {- TSectFld provides a base box window for section editors that have fields
     like the gram stain and demographics }
  PSectFld = ^TSectFld;
  TSectFld = object(TBoxDlg)
    procedure AlignBox(aParent: PWindowsObject); virtual;
  end;

function GetFieldType(aParent: PWindowsObject; fldTypeStr: PCollection): integer;

IMPLEMENTATION

uses
  DMSDebug,
  CtlLib,
  ApiTools,
  RptUtil,
  RptAlign,
  DMString,
  DMSErr,
  MScan,
  Strings,
  Win31,
  WinProcs,
  ODialogs;

{$I RPTGEN.INC}

constructor TSectPage.Init(aParent: PWindowsObject; aPage: TRptPage;
                           aFont: PRptFont);
begin
  inherited Init(aParent, aPage);
  pFont:= aFont;
end;

procedure TSectPage.ShowBoxStatus(aBox: PBoxWin);
var
  pstr  : array[0..100] of char;
begin
  inherited ShowBoxStatus(aBox);
  if aBox = nil then
    strcopy(pstr, '')
  else
    aBox^.GetName(pstr, 100);
  PSectFldEdit(parent)^.sb^.ChangeSegmentText(1, pstr);
end;

function TSectPage.GetFontHeight: integer;
{- returns the height of the text based on the currently selected font.
   Value returned in is device pixels }
var
  dc    : HDC;
  tm    : TTextMetric;
  f     : HFont;
  h     : integer;
  LTpPY : integer;
begin
  dc:= GetDC(HWindow);
  SetLogicalTwips(dc, GetZoom);
  SelectObject(dc, CreateFontIndirect(pFont^.lf));
  GetTextMetrics(dc, tm);
  LTwipsPerPixel(dc, h, LTpPY);
  ReleaseDC(HWindow, DC);
  h:= tm.tmHeight + tm.tmExternalLeading;
  h:= h div LTpPY;
  GetFontHeight:= h;
end;

procedure TSectPage.ShowAllBoxes(aTitle: PChar);
var
  pstr  : array[0..100] of char;
begin
  inherited ShowAllBoxes(SR(IDS_RPTFIELDS, pstr, 100));
end;

procedure TSectPage.ClearAllBoxes;
var
  p1, p2  : array[0..100] of char;
begin
  if YesNoMsg(GetParentHWnd, SR(IDS_CONFIRM, p1, 100), SR(IDS_CONFIRMREMOVE, p2, 100)) then
    inherited ClearAllBoxes;
end;

procedure TSectFld.AlignBox(aParent: PWindowsObject);
var
  r    : TLRect;
  pstr : array[0..100] of char;
  p1,p2: array[0..100] of char;
begin
  GetSize(r);
  GetName(pstr, 100);
  if GetAlignment(aParent, SR(IDS_ALIGNFLD, p1, 100), SR(IDS_FIELD, p2, 100), pstr, r) then
    SetSize(r);
end;

constructor TAddFldTypes.Init(aName: PChar; aNum: integer);
begin
  inherited Init;
  name:= StrNew(aName);
  num:= aNum;
end;

destructor TAddFldTypes.Done;
begin
  inherited Done;
  MSStrDispose(name);
end;

type
  PGetFldDlg = ^TGetFldDlg;
  TGetFldDlg = object(TCenterDlg)
    sel  : ^integer;
    list : PListBox;
    strs : PCollection;
    constructor Init(aParent: PWindowsObject; aCol: PCollection; var aSel: integer);
    procedure SetupWindow; virtual;
    function Canclose: boolean; virtual;
    procedure IDList(var msg: TMessage); virtual ID_FIRST + IDC_LIST;
  end;

constructor TGetFldDlg.Init(aParent: PWindowsObject; aCol: PCollection; var aSel: integer);
var
  c  : PLStatic;
begin
  inherited Init(aParent, MakeIntResource(DLG_ADDFLD));
  c:= New(PLStatic, InitResource(@self, 300, 0, false));
  list:= New(PListBox, InitResource(@self, IDC_LIST));
  sel:= @aSel;
  strs:= aCol;
end;

procedure TGetFldDlg.SetupWindow;
var
  k  : integer;
begin
  inherited SetupWindow;
  for k:= 0 to strs^.count - 1 do
    list^.AddString(PAddFldTypes(strs^.At(k))^.name);
  list^.SetSelIndex(0);
end;

procedure TGetFldDlg.IDList(var msg: TMessage);
begin
  if msg.lParamHI = LBN_DBLCLK then
    OK(msg);
end;

function TGetFldDlg.CanClose: boolean;
begin
  CanClose:= true;
  if list^.GetSelIndex = -1 then
  begin
    ShowError(@self, IDS_NOFLDTYPE, nil, 0, MOD_RPTGEN, 0);
    CanClose:= false;
  end
  else
    sel^:= list^.GetSelIndex;
end;

function GetFieldType(aParent: PWindowsObject; fldTypeStr: PCollection): integer;
{- return a selection for a field type.  fldTypeStr is a collection of TAddFldTypes,
   returns -1 if no selection made }
var
  d   : PDialog;
  sel : integer;
begin
  d:= New(PGetFldDlg, Init(aParent, fldTypeStr, sel));
  if application^.ExecDialog(d) = IDOK then
    GetFieldType:= sel
  else
    GetFieldType:= -1;
end;

{------------------------------------------------[ TSectFldEdit ]--}

constructor TSectFldEdit.Init(aParent: PWindowsObject; anID: integer; aName: PChar;
                              aSize: TPoint; aFont: PRptFont);
begin
  inherited Init(aParent, MakeIntResource(anID));

  popMenu:= LoadMenu(HInstance, MakeIntResource(MENU_RIGHTPOP));

  pFont:= aFont;
  sb:= New(PStatusBar, Init(@self, SB_WORDSTYLE));
  name:= StrNew(aName);

  pw:= InitPage(aSize);
end;

destructor TSectFldEdit.Done;
begin
  inherited Done;
  MSStrDispose(name);
  DeleteObject(sbFont);
  DestroyMenu(popMenu);
end;

procedure TSectFldEdit.SetupWindow;
var
  r    : TRect;
  pstr : array[0..100] of char;
  p2   : array[0..50] of char;
  k    : integer;
  lf   : TLogFont;
begin
  inherited SetupWindow;
  SetWindowText(HWindow, name);

  {- fill zoom combo box}
  for k:= 1 to 8 do
  begin
    Str(k * 25, pstr);
    strCat(pstr, '%');
    SendDlgItemMsg(IDC_ZOOM, CB_ADDSTRING, 0, longint(@pstr));
  end;
  SendDlgItemMsg(IDC_ZOOM, CB_SETCURSEL, 3, 0);

  sb^.AddSegment('', 200, SB_ALIGNLEFT);

  MakeScreenFont(lf, false, false);
  sbFont:= CreateFontIndirect(lf);
  SendMessage(sb^.HWindow, WM_SETFONT, sbFont, 0);

  GetWindowRect(HWindow, r);
  {- force a WM_Size message so window and status line are adjusted }
  SendMessage(HWindow, WM_SIZE, SIZE_RESTORED, MakeLong(r.right - r.left, r.bottom-r.top));

  {- force focus to page window }
  PostMessage(HWindow, WM_NEXTDLGCTL, pw^.HWindow, 1);
end;

procedure TSectFldEdit.WMSize(var msg: TMessage);
{- adjust status bar. }
var
  r   : TRect;
  r2  : TRect;
  pt  : TPoint;
begin
  DefWndProc(msg);
  GetClientRect(HWindow, r);

  sb^.parentresized(r);

  {- adjust top just below the buttons }
  GetWindowRect(GetDlgItem(HWindow, IDC_CLOSE), r2);
  pt.x:= r2.left;
  pt.y:= r2.bottom;
  ScreenToClient(HWindow, pt);

  r.top:= pt.y + 5;
  MoveWindow(pw^.HWindow, 0, r.top, r.right, sb^.attr.y - r.top, true);
end;

procedure TSectFldEdit.WMCtlColor(var msg: TMessage);
begin
  if msg.lParamHi = CTLCOLOR_DLG then
  begin
    msg.result:= GetStockObject(LTGRAY_BRUSH);
  end
  else
    DefWndProc(msg);
end;

procedure TSectFldEdit.IDField(var msg: TMessage);
var
  h   : HMenu;
  hw  : HWnd;
  r   : TRect;
begin
  FocusCtl(HWindow, IDC_FIELD);
  hw:= GetDlgItem(HWindow, IDC_FIELD);
  GetWindowRect(hw, r);
  h:= GetSubMenu(popMenu, 0);
  TrackPopupMenu(h, TPM_LEFTALIGN or TPM_RIGHTBUTTON, r.left, r.bottom,
                 0, pw^.HWindow, nil);
  FocusCtl(HWindow, IDC_FIELD);
end;

procedure TSectFldEdit.IDZoom(var msg: TMessage);
var
  k  : integer;
begin
  if msg.lParamHi = CBN_SELCHANGE then
  begin
    k:= SendDlgItemMsg(IDC_ZOOM, CB_GETCURSEL, 0, 0);
    if k >= 0 then
    begin
      k:= (k + 1) * 25;
      pw^.SetZoom(k / 100.0);
    end;
  end;
end;

procedure TSectFldEdit.IDClose(var msg: TMessage);
begin
  OK(msg);
end;

procedure TSectFldEdit.UMBoxStatus(var msg: TMessage);
begin
  sb^.ChangeSegmentText(0, PChar(msg.lParam));
end;

function TSectFldEdit.InitPage(aSize: TPoint): PSectPage;
var
  page  : TRptPage;
begin
  FillChar(page, sizeof(page), 0);
  page.size.X:= aSize.x;
  page.size.Y:= aSize.y;
  InitPage:= New(PSectPage, Init(@self, page, pFont));
end;

procedure TSectFldEdit.GetNewFont(var aFont: TRptFont);

  procedure SetIt(b: PBoxWin); far;
  begin
    b^.SetDisplayFont(aFont);
  end;

begin
  if GetAFont(HWindow, aFont) then
  begin
    pw^.boxes^.ForEach(@SetIt);
  end;
end;

END.

{- rcf }
