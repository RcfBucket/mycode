Unit IntQuick;



Interface

Uses
  IntGlob,
  Strings,
	WinProcs;







procedure IntQuick_ReadQuickRanges;
procedure IntQuick_SaveQuickRanges;

Implementation



{
*********************
Non-Object Procedures
*********************
}

procedure IntQuick_ReadQuickRanges;
var
	CTransFlag: array[0..1] of char;

begin
	with Ranges do
  begin
    GetPrivateProfileString('Quick', 'StartRange', '', StartRange, SizeOf(StartRange), 'IntCstm.ini');
    GetPrivateProfileString('Quick', 'EndRange', '', EndRange, SizeOf(EndRange), 'IntCstm.ini');
    GetPrivateProfileString('Quick', 'RequestType', '', RequestType, SizeOf(RequestType), 'IntCstm.ini');
    GetPrivateProfileString('Quick', 'TransmitFlag', '', CTransFlag, SizeOf(CTransFlag), 'IntCstm.ini');
    ReTransmit:= (StrComp(CTransFlag, 'T') = 0);
  end;
end;

procedure IntQuick_SaveQuickRanges;
var
	CTransFlag: array[0..1] of char;

begin
	with Ranges do
  begin
    WritePrivateProfileString('Quick', 'StartRange', StartRange, 'IntCstm.ini');
    WritePrivateProfileString('Quick', 'EndRange', EndRange, 'IntCstm.ini');
    WritePrivateProfileString('Quick', 'RequestType', RequestType, 'IntCstm.ini');
    if ReTransmit then
      StrCopy(CTransFlag, 'T')
    else
      StrCopy(CTransFlag, 'N');
    WritePrivateProfileString('Quick', 'TransmitFlag', CTransFlag, 'IntCstm.ini');
	end;
end;


Begin
End.
