(****************************************************************************

uigsres.inc

produced by Borland Resource Workshop

Gram stain results MOD = $2190 = 8592
*****************************************************************************)

const
  DLG_GSRESULTS       = 8592;


  IDC_GRID            = 201;
  IDC_EDSPECID        = 202;
  IDC_EDCOLLECT       = 203;
  IDC_EDRESULT        = 204;
  IDC_PATID           = 205;
  IDC_WARD            = 206;
  IDC_SOURCE          = 207;
  IDC_LAST            = 208;
  IDC_FIRST           = 209;

  IDC_LBLCOLLECT      = 300;
  IDC_LBLFIRST        = 301;
  IDC_LBLLAST         = 302;
  IDC_LBLPATID        = 303;
  IDC_LBLSOURCE       = 304;
  IDC_LBLSPECID       = 305;
  IDC_LBLWARD         = 306;
  IDC_LBLRESULT       = 307;


	IDS_GSDEFOPENERR	=	8592;
	IDS_GSRESOPENERR	=	8593;
	IDS_UIFLDINIT	=	8594;
	IDS_NODEF	=	8597;
	IDS_RESULT	=	8602;
	IDS_AVAIL2	=	8596;
	IDS_AVAIL3	=	8598;
	IDS_AVAIL4	=	8600;
	IDS_AVAIL5	=	8601;
	IDS_AVAIL6	=	8599;
	IDS_NOGSCREATED	=	8595;
