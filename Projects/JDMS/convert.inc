(****************************************************************************


convert.inc

produced by Borland Resource Workshop


*****************************************************************************)

const
  DLG_CVTMAIN         = 17920;
  DLG_PROMPT4DISK     = 17921;
  DLG_CALCULATOR      = 17922;
  DLG_WORKING         = 17923;
  DLG_MAPFIELD        = 17924;


  IDC_NEXT            = 101;
  IDC_SKIP            = 102;
  IDC_CALCULATE       = 103;

  IDC_FREESPACETXT    = 300;
  IDC_V18BACKUP       = 301;
  IDC_V20BACKUP       = 302;
  IDC_DISKS           = 303;
  IDC_FIELD           = 304;
  IDC_FILE            = 305;
  IDC_FLD_LIST        = 306;

  LBL_BYTES           = 200;
  LBL_SPACEAVAIL      = 202;
  LBL_ENOUGHSPACEMSG  = 204;
  LBL_DRIVELETTER     = 205;
  LBL_SPACEFREE       = 206;
  LBL_SPACEREQ        = 207;

  IDC_LBL1            = 501;
  IDC_LBL2            = 502;

	IDS_ENOUGHSPACE	=	17922;
	IDC_EXIT	=	1;
	ICON_1	=	17920;
	IDC_LOG	=	102;
	IDC_TEXT	=	200;

