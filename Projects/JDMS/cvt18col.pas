unit Cvt18Col;

INTERFACE

uses
  Objects,
  DBTypes,
  Cvt18Msc;

type
  PTestColl = ^TTestColl;
  TTestColl = object(TSortedCollection)
    function  Compare(key1, key2: pointer): integer; virtual;
    function  CreateEntry(panelID: byte; dList: PDrugList; var dupDrug: boolean): integer;
  end;

  drugList = array[1..5] of array[0..3] of char;
  PTestResObj = ^TTestResObj;
  TTestResObj = object(TObject)
    drugAbbr   : drugList;
    tstgrpRef  : TSeqNum;
    tstgrpID   : array[0..9] of char;
    tstgrpDesc : array[0..33] of char;
    orderRef   : TSeqNum;
    orderId    : array[0..9] of char;
    orderDesc  : array[0..33] of char;
    iso        : array[0..3] of char;
  end;

  PPnlXref = ^TPnlXref;
  TPnlXref = object(TObject)
    pnlIdx    : integer;
    drgIdx    : integer;
    ordID     : array[0..9] of char;
    ordDesc   : array[0..33] of char;
    ordSeq    : TSeqNum;
  end;

  PPnlDrgColl = ^TPnlDrgColl;
  TPnlDrgColl = object(TCollection)
    function AddXref(aPnlIdx, aDrgIdx: integer): integer;
  end;


IMPLEMENTATION

uses
  Strings,
  StrsW,
  APITools,
  DBIDs,
  DBFile,
  CvtMisc;

{- Following is a cross reference collection to xref Panel items and additional
   drug items }

function TPnlDrgColl.AddXref(aPnlIdx, aDrgIdx: integer): integer;

  function Matches(item: PPnlXref): boolean; far;
  begin
    Matches:= (item^.pnlIdx = aPnlIdx) and (item^.drgIdx = aDrgIdx);
  end;

var
  p   : PPnlXref;
begin
  p:= FirstThat(@Matches);
  if p = nil then
  begin
    p:= New(PPnlXref, Init);
    p^.pnlIdx:= aPnlIdx;
    p^.drgIdx:= aDrgIdx;
    StrCopy(p^.ordID, '');
    StrCopy(p^.ordDesc, '');
    p^.ordSeq:= 0;
    Insert(p);
  end;
  AddXref:= IndexOf(p);
end;

function FindDupDrug(panelID: byte; drug: PChar): boolean;
var
  i   : integer;
  rc  : boolean;
begin
  rc := FALSE;
  for i := 1 to panels[panelID].numTests do
  begin
    rc := (StrLIComp(panels[panelID].testID[i], drug, 5) = 0) or rc;
  end;
  FindDupDrug := rc;
end;


function FindDrugOrder(db: PDBFile; drug: DrugRec): longint;
var
  dsrt,
  sort   : longint;
  flag   : boolean;
  pstr   : array[0..62] of char;
  seq    : TSeqNum;
  catDB  : PDBFile;
  testDB : PDBFile;
begin
  FindDrugOrder  := 0;
  if drug.drugabbr[0] <> #0 then
  begin
    db^.dbr^.PutFieldAsStr(DBDRUGAbbr, drug.drugAbbr);
    if db^.dbc^.GetEQ(db^.dbr) then
    begin
      db^.dbr^.GetField(DBDRUGDfltSortNum, @dsrt, SizeOf(dsrt));
    end
    else
    begin
      db^.dbc^.SetCurKeyNum(DBDRUG_Sort_KEY);
      db^.dbc^.GetLast(db^.dbr);
      db^.dbr^.GetField(DBDRUGSortNum, @sort, SizeOf(sort));
      Inc(sort);
      db^.dbc^.SetCurKeyNum(DBDRUG_DfltSort_KEY);
      db^.dbc^.GetLast(db^.dbr);
      db^.dbr^.GetField(DBDRUGDfltSortNum, @dsrt, SizeOf(dsrt));
      Inc(dsrt);
      db^.dbr^.PutFieldAsStr(DBDRUGAbbr, drug.drugAbbr);
      db^.dbr^.PutFieldAsStr(DBDRUGName, drug.drugName);
      db^.dbr^.PutFieldAsStr(DBDRUGAltAbbr, drug.drugAbbr);
      flag := FALSE;
      db^.dbr^.PutField(DBDRUGSupp, @flag);
      db^.dbr^.PutField(DBDRUGSortNum, @sort);
      db^.dbr^.PutField(DBDRUGDfltSortNum, @dsrt);
      flag := TRUE;
      db^.dbr^.PutField(DBDRUGUDFlag, @flag);
      if not db^.dbc^.InsertRec(db^.dbr) then
      begin
        HexStr(db^.dbc^.dbErrorNum, SizeOf(db^.dbc^.dbErrorNum), pstr, 10);
        StrCat(pstr, ' error on DRUG insert for ');
        StrCat(pstr, drug.drugAbbr);
        CriticalError(pstr);
      end;
      db^.dbc^.SetCurKeyNum(DBDRUG_ID_KEY);
    end;

    testDB := OpenV30File(DBTSTFile);
    if testDB = nil then
      CriticalError('Cannot open the V3.02 TEST file.');
    catDB := OpenV30File(DBTSTCATFile);
    if catDB = nil then
      CriticalError('Cannot open the V3.02 TEST Category file.');

    testDB^.dbr^.PutFieldAsStr(DBTSTID, drug.drugAbbr);
    testDB^.dbr^.PutFieldAsStr(DBTSTTstCat, 'MIC');
    if testDB^.dbc^.GetEQ(testDB^.dbr) then
    begin
      testDB^.dbr^.GetField(DBTSTDrugRef, @seq, SizeOf(seq));
      if db^.dbr^.GetSeqValue <> seq then
      begin
        StrCopy(pstr, drug.drugAbbr);
        StrCat(pstr, ' drug does not match MIC Test ID: ');
        StrCat(pstr, drug.drugAbbr);
        CriticalError(pstr);
      end;
    end
    else
    begin
      StrCopy(pstr, drug.drugName);
      StrCat(pstr, ' MIC');
      testDB^.dbr^.PutFieldAsStr(DBTSTName, pstr);
      catDB^.dbr^.PutFieldAsStr(DBTSTCATID, 'MIC');
      catDB^.dbc^.GetEQ(catDB^.dbr);
      seq := catDB^.dbr^.GetSeqValue;
      testDB^.dbr^.PutField(DBTSTTstCatRef, @seq);
      seq := db^.dbr^.GetSeqValue;
      testDB^.dbr^.PutField(DBTSTDrugRef, @seq);
      flag := TRUE;
      testDB^.dbr^.PutField(DBTSTUDFlag, @flag);
      if not testDB^.dbc^.InsertRec(testDB^.dbr) then
      begin
        HexStr(testDB^.dbc^.dbErrorNum, SizeOf(testDB^.dbc^.dbErrorNum), pstr, 10);
        StrCat(pstr, ' error on TEST insert for ');
        StrCat(pstr, drug.drugAbbr);
        StrCat(pstr, ' MIC');
        CriticalError(pstr);
      end;
    end;

    testDB^.dbr^.PutFieldAsStr(DBTSTID, drug.drugAbbr);
    testDB^.dbr^.PutFieldAsStr(DBTSTTstCat, 'NCCLS');
    if testDB^.dbc^.GetEQ(testDB^.dbr) then
    begin
      testDB^.dbr^.GetField(DBTSTDrugRef, @seq, SizeOf(seq));
      if db^.dbr^.GetSeqValue <> seq then
      begin
        StrCopy(pstr, drug.drugAbbr);
        StrCat(pstr, ' drug does not match NCCLS Test ID: ');
        StrCat(pstr, drug.drugAbbr);
        CriticalError(pstr);
      end;
    end
    else
    begin
      StrCopy(pstr, drug.drugName);
      StrCat(pstr, ' NCCLS');
      testDB^.dbr^.PutFieldAsStr(DBTSTName, pstr);
      catDB^.dbr^.PutFieldAsStr(DBTSTCATID, 'NCCLS');
      catDB^.dbc^.GetEQ(catDB^.dbr);
      seq := catDB^.dbr^.GetSeqValue;
      testDB^.dbr^.PutField(DBTSTTstCatRef, @seq);
      seq := db^.dbr^.GetSeqValue;
      testDB^.dbr^.PutField(DBTSTDrugRef, @seq);
      flag := TRUE;
      testDB^.dbr^.PutField(DBTSTUDFlag, @flag);
      if not testDB^.dbc^.InsertRec(testDB^.dbr) then
      begin
        HexStr(testDB^.dbc^.dbErrorNum, SizeOf(testDB^.dbc^.dbErrorNum), pstr, 10);
        StrCat(pstr, ' error on TEST insert for ');
        StrCat(pstr, drug.drugAbbr);
        StrCat(pstr, ' NCCLS');
        CriticalError(pstr);
      end;
    end;

    Dispose(catDB, Done);
    Dispose(testDB, Done);
    FindDrugOrder := dsrt;
  end;
end;


function TTestColl.Compare(key1, key2: pointer): integer;
var
  idx, rc : integer;
begin
  rc := 0;
  for idx := 1 to 5 do
  begin
    rc := StrComp(PTestResObj(key1)^.drugAbbr[idx], PTestResObj(key2)^.drugAbbr[idx]);
    if rc <> 0 then
      break;
  end;
  Compare := rc;
end;

function TTestColl.CreateEntry(panelID: byte; dList: PDrugList; var dupDrug: boolean): integer;
var
  db       : PDBFile;
  drugSeq  : array[1..5] of integer;
  i, j     : integer;
  idx      : integer;
  item     : PTestResObj;
begin
  dupDrug := FALSE;
  db := OpenV30File(DBDRUGFile);
  if db = nil then
    CriticalError('Cannot open the V3.02 DRUG file.');
  item := New(PTestResObj, Init);
  for i := 1 to 5 do
  begin
    StrCopy(item^.drugAbbr[i], '');
  end;
  for i := 1 to 5 do
  begin
    j := i;
    dupDrug := FindDupDrug(panelID, dList^[i].drugAbbr) or dupDrug;
    idx := FindDrugOrder(db, dList^[i]);
    if idx <> 0 then
    begin
      while (j > 1) and (drugSeq[j-1] > idx) do
      begin
        drugSeq[j] := drugSeq[j-1];
        StrLCopy(item^.drugAbbr[j], item^.drugAbbr[j-1], 3);
        Dec(j);
      end;
      drugSeq[j] := idx;
      StrLCopy(item^.drugAbbr[j], dList^[i].drugAbbr, 3);
    end;
  end;
  if Search(item, idx) then
  begin
    Dispose(item, Done);
  end
  else
  begin
    item^.tstgrpRef := 0;
    item^.tstgrpID[0] := #0;
    item^.tstgrpDesc[0] := #0;
    item^.orderRef := 0;
    item^.orderId[0] := #0;
    item^.orderDesc[0] := #0;
    item^.iso[0] := #0;
    Insert(item);
    (* !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    << I think that idx may not be set properly here when a new item is inserted.
    << Maybe try using "idx:= IndexOf(item)" here to obtain the index of the
    << just inserted item. [rcf]
    (* !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!*)
  end;

  Dispose(db, Done);
  CreateEntry := idx;
end;


END.

