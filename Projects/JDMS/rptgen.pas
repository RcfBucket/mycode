{----------------------------------------------------------------------------}
{  Module Name  : RPTGEN.PAS                                                 }
{  Programmer   : RCF                                                        }
{  Date Created : 07/13/95                                                   }
{  Requirements :                                                            }
{                                                                            }
{  Purpose - This is the top-level module for the Report Format Editor       }
{    program. The program provides an editor for creating and editing report }
{    formats. The formats are used by the Data Management System when        }
{    printing Long Patient Reports.                                          }
{                                                                            }
{  Assumptions                                                               }
{    unknown                                                                 }
{                                                                            }
{  Limitations                                                               }
{    unknown                                                                 }
{                                                                            }
{  Referenced Documents                                                      }
{    none                                                                    }
{                                                                            }
{  Revision History - This project is under version control, use it to view  }
{    revision history.                                                       }
{                                                                            }
{----------------------------------------------------------------------------}

program RPTGEN;

uses
  {--------------------------------------------------------------------------}
  { Section drivers. The drivers are listed in the order they are to be
  { display when presented to the user. They are included here so they get
  { linked in the program, they are not called directly.
  {--------------------------------------------------------------------------}
  SectDemo,     { demographic/free text driver }
  SectGS,       { gram stain driver }
  SectISum,     { isolate summary }
  SectICmp,     { isolate (compressed) }
  SectIFull,    { isolate (full) }


  {--------------------------------------------------------------------------}
  { Normal uses
  {--------------------------------------------------------------------------}
  DMSDebug,
  Ctl3d,
  SectReg,
  DBFile,
  DBTypes,
  GridLib,
  DlgLib,
  IntlLib,
  userMsgs,
  IniLib,
  FMBackUp,
  FMRstore,
  PrnObj,
  RptShare,
  dbids,
  DBLib,
  MTask,
  DMString,
  DMSErr,
  MScan,
  RptEdit,
  RptPref,
  RptUtil,
  ApiTools,
  RptData,
  StatBar,
  Strings,
  Win31,
  WinProcs,
  WinTypes,
  Objects,
  ODialogs,
  OWindows;

{$R RPTGEN.RES}
{$R FMUTIL.RES}  {- need this because FMBackup and FMRstore do not link it in }
{$I RPTGEN.INC}



                        {----------------------}
                        {   Format Selection   }
                        {        Dialog        }
                        {----------------------}



type
  {--------------------------------------------------------------------------}
  { This object represents a dialog box that lists the report formats
  { stored in the report formats data file. Its primary purpose is to prompt
  { the user to select a report format. It also allows the user to delete
  { report formats.
  {--------------------------------------------------------------------------}
  PSelectFormatDialog = ^TSelectFormatDialog;
  TSelectFormatDialog = object( TCenterDlg )

    { Interface }
    constructor Init( pParent: PWindowsObject; var aSeq: longint );
    destructor Done; virtual;

    { Implementation }
    private
    seq: ^longint;
    grid: PGrid;
    db: PDBFile;
    curs: HCursor;
    procedure SetupWindow; virtual;
    procedure UpdateEnablesAndFocus;
    procedure WMDrawItem( var msg: TMessage ); virtual WM_FIRST + WM_DRAWITEM;
    procedure WMMeasureItem( var msg: TMessage );
      virtual WM_FIRST + WM_MEASUREITEM;
    procedure WMCharToItem( var msg: TMessage );
      virtual WM_FIRST + WM_CHARTOITEM;
    procedure OnDelete( var msg: TMessage ); virtual ID_FIRST + IDC_DELETE;
    function CanClose: boolean; virtual;
    procedure IDList( var msg: TMessage ); virtual ID_FIRST + IDC_LIST;
  end;


const
  {--------------------------------------------------------------------------}
  { These constants name the two columns in the grid.
  {--------------------------------------------------------------------------}
  NAME_COLUMN = 0;
  SN_COLUMN = 1;



{----------------------------------------------------------------------------}
{ The constructor opens the report format file, initializes the bass class,
{ and creates a control object for the grid.
{----------------------------------------------------------------------------}

constructor TSelectFormatDialog.Init( pParent: PWindowsObject;
  var aSeq: longint );
const
  COLUMNS = 2;
  BOLD = true;
begin
  curs := SetCursor( LoadCursor( 0, IDC_WAIT ) );
  db := New( PDBFile, Init( DBRPTFmtsFile, '', dbOpenNormal ) );
  seq := @aSeq;
  if db = nil then
  begin
    ShowError( nil, IDS_CANTOPENFORMAT, nil, dbLastOpenError, MOD_RPTGEN, 3 );
    Fail;
  end
  else
  begin
    inherited Init( pParent, MakeIntResource( DLG_SELECTRPT ) );
    grid := New( PGrid, InitResource( @self, IDC_LIST, COLUMNS, not BOLD ) );
  end;
end;



{----------------------------------------------------------------------------}
{ The destructor directs the bass class to cleanup and then it deallocates
{ the report format file object
{----------------------------------------------------------------------------}

destructor TSelectFormatDialog.Done;
begin
  inherited Done;
  MSDisposeObj( db );
end;



{----------------------------------------------------------------------------}
{ This override calls the base class then initializes the grid, buttons, and
{ cursor.
{----------------------------------------------------------------------------}

procedure TSelectFormatDialog.SetupWindow;
var
  pstr: array[ 0..100 ] of char;
  row: integer;
  tseq: longint;
begin
  { Set up base class }
  inherited SetupWindow;

  { Fill grid with file names }
  with db^ do
  begin
    dbc^.GetFirst( dbr );
    while dbc^.dbErrorNum = 0 do
    begin
      dbr^.GetField( DBRPTFmtName, @pstr, SizeOf( pstr ) - 1 );
      row := grid^.AddString( '' );
      grid^.SetData( NAME_COLUMN, row, pstr );
      tseq := dbr^.GetSeqValue;
      Str( tseq, pstr );
      grid^.SetData( SN_COLUMN, row, pstr );
      dbc^.GetNext( dbr );
    end;
  end;

  { Other grid set up: set column header, size column, preselect top item }
  grid^.SetHeader( NAME_COLUMN, SR( IDS_RPTNAME, pstr, 100 ) );
  grid^.StretchColumn( NAME_COLUMN );
  grid^.SetSelIndex( 0 );

  { Initialize buttons }
  UpdateEnablesAndFocus;

  { Activate cursor }
  SetCursor( curs );
end;



{----------------------------------------------------------------------------}
{ This procedure updates the enable state and focus of the controls.
{----------------------------------------------------------------------------}

procedure TSelectFormatDialog.UpdateEnablesAndFocus;
var
  EnableState: boolean;
begin

  { Determine state: true if list contains one or more items }
  EnableState := grid^.GetRowCount > 0;

  { Update state }
  EnableWindow( GetItemHandle( IDC_DELETE ), EnableState );
  EnableWindow( GetItemHandle( ID_OK ), EnableState );
  if not EnableState then
    FocusCtl( HWindow, IDC_LIST );
end;



{----------------------------------------------------------------------------}
{ This procedure handles the response to the user clicking the button with an
{ ID of IDC_DELETE by deleting the currently selected item in the list.
{----------------------------------------------------------------------------}

procedure TSelectFormatDialog.OnDelete( var msg: TMessage );

  {--------------------------------------------------------------------------}
  { This procedure deletes any sections attached to a report format.
  {--------------------------------------------------------------------------}
  procedure DeleteSections( rptSeq: longint );
  var
    dbx: PDBFile;
    cont: boolean;
    seq: longint;
    sectID: integer;
  begin
    dbx := New( PDBFile, Init( DBRPTSectsFile, '', dbOpenNormal ) );
    if dbx = nil then
      Exit;

    with dbx^ do
    begin
      dbr^.PutField( DBRptSectReportRef, @rptSeq );  {- search for the first
        record for this report}
      dbc^.GetEQ( dbr );
      cont := true;
      while ( dbc^.dbErrorNum = 0 ) and cont do
      begin
        dbr^.GetField( DBRptSectSectRef, @seq, SizeOf( seq ) );
        dbr^.GetField( DBRptSectSectID, @sectID, SizeOf( sectID ) );
        if ( sectID > 0 ) and ( sectID <= sections^.TotalSections ) then
          sections^.sects[ sections^.IDXFromID( sectID ) ].DeleteFunc(
            ACTION_OPEN or ACTION_DELETE or ACTION_CLOSE, rptSeq, seq );

        dbc^.DeleteRec( dbr );
        dbc^.GetNext( dbr );
        if dbc^.dbErrorNum = 0 then
        begin
          dbr^.GetField( DBRptSectReportRef, @seq, SizeOf( seq ) );
          cont := ( seq = rptSeq );
        end;
      end;
    end;
    MSDisposeObj( dbx );
  end;

var
  row: integer;
  tSeq: longint;
  p1: array[ 0..50 ] of char;
  pstr: array[ 0..100 ] of char;
  code: integer;

begin { OnDelete }

  { Get item to delete }
  row := grid^.GetSelIndex;
  if row >= 0 then
  begin
    { Get user confirmation }
    if YesNoMsg( HWindow, SR( IDS_CONFIRM, p1, SizeOf( p1 ) - 1 ),
      SR( IDS_CONFIRMDEL, pstr, SizeOf( pstr ) - 1 ) ) then
    begin
      { Open wait cursor }
      curs := SetCursor( LoadCursor( 0, IDC_WAIT ) );

      { Get SN string and convert to number }
      grid^.GetData( SN_COLUMN, row, pstr, SizeOf( pstr ) - 1 );
      Val( pstr, tSeq, code );

      { Access the record by its SN }
      if db^.dbc^.GetSeq( db^.dbr, tSeq ) then
      begin
        { Delete its sections, itself, and its inclusion in the grid }
        DeleteSections( tSeq );
        db^.dbc^.DeleteRec( db^.dbr );
        grid^.DeleteString( row );
      end
      else
      begin
        { Abnormal case }
        ShowError( @self, IDS_CANTLOCREC, nil, db^.dbc^.dbErrorNum,
          MOD_RPTGEN, 0 );
      end;

      { Close wait cursor }
      SetCursor( curs );

      { The deletion may invalidate the enable state so it is updated. }
      UpdateEnablesAndFocus;
    end;
  end;

end;



{----------------------------------------------------------------------------}
{ This override replaces the closing condition. To close the dialog, the
{ grid must have an item selected. If the condition is met, the selected
{ item's sequence number is transfered to a member variable, otherwise, an
{ error message is displayed.
{
{ Note: this override does not call the inherited CanClose as recommended.
{ This may be intetional or it may be an oversight.
{----------------------------------------------------------------------------}

function TSelectFormatDialog.CanClose: boolean;

  {--------------------------------------------------------------------------}
  { Transfer sequence number from grid row to member variable.
  {--------------------------------------------------------------------------}
  procedure TransferSN( row: integer );
  var
    code: integer;
    p1: array[ 0..50 ] of char;
  begin
    { Transfer sequence number }
    grid^.GetData( SN_COLUMN, row, p1, SizeOf( p1 ) - 1 );
    Val( p1, seq^, code );
    { Assume successful (code = 0) }
  end;

var
  Row: integer;
  CloseOk: boolean;
begin
  { Evaluate closing condition }
  Row := grid^.GetSelIndex;
  CloseOk := Row >= 0;

  { Handle outcome }
  if CloseOk then
    TransferSN( Row )
  else
    ShowError( @self, IDS_MUSTSELRPT, nil, 0, MOD_RPTGEN, 0 );

  { Return condition }
  CanClose := CloseOk;
end;



{----------------------------------------------------------------------------}
{ This procedure passes WM_DRAWITEM messages to the grid.
{----------------------------------------------------------------------------}

procedure TSelectFormatDialog.WMDrawItem( var msg: TMessage );
begin
  grid^.DrawItem( msg );
end;



{----------------------------------------------------------------------------}
{ This procedure passes WM_CHARTOITEM messages to the grid.
{----------------------------------------------------------------------------}

procedure TSelectFormatDialog.WMCharToItem( var msg: TMessage );
begin
  grid^.CharToItem( msg );
end;



{----------------------------------------------------------------------------}
{ This procedure passes WM_MEASUREITEM messages to the grid.
{----------------------------------------------------------------------------}

procedure TSelectFormatDialog.WMMeasureItem( var msg: TMessage );
begin
  grid^.MeasureItem( msg );
end;



{----------------------------------------------------------------------------}
{ This procedure processes messages from the IDC_LIST control. In the case of
{ a double-click message, it calls the dialog's OK method. The purpose of this
{ procedure is to cause the dialog to close when the user double-clicks on an
{ item.
{----------------------------------------------------------------------------}

procedure TSelectFormatDialog.IDList( var msg: TMessage );
begin
  if msg.lParamHi = LBN_DBLCLK then
    OK( msg )
  else
    DefWndProc( msg );
end;



                        {----------------------}
                        {     Main Window      }
                        {----------------------}



type
  {--------------------------------------------------------------------------}
  { This object represents the application's main window. It contains a
  { status bar width information for the current report.
  {   Segment 1: Currently selected section size (width/height, origen)
  {   Segment 2: Currently selected paper size
  {--------------------------------------------------------------------------}
  PRptMain = ^TRptMain;
  TRptMain = object( T3DMDIWindow )

    { Interface }
    constructor Init( AName: PChar; AMenu: HMenu );
    destructor Done; virtual;

    { Implementation }
    private
    pSBar: PStatusBar;
    SBarFont: HFont;
    SeqNum: longint;   {- used to hold seq number for a report (-1 = new report}
    Zoom: real;
    procedure SetupWindow; virtual;
    procedure WMSize( var msg: TMessage ); virtual WM_FIRST + WM_SIZE;
    procedure WMWinIni( var msg: TMessage ); virtual WM_FIRST + WM_WININICHANGE;
    procedure WMDMSQuery( var msg: TMessage );
      virtual WM_FIRST + WM_MAINPROGQUERY;
    procedure GetWindowClass( var aWndClass: TWndClass ); virtual;
    function GetClassName: PChar; virtual;
    procedure UMBoxStatus( var msg: TMessage ); virtual WM_FIRST + UM_BOXSTATUS;
    function InitChild: PWindowsObject; virtual;
    procedure OnExit( var msg: TMessage ); virtual CM_FIRST + CM_FILEEXIT;
    procedure OnPreferences( var msg: TMessage );
      virtual CM_FIRST + CM_PREFERENCES;
    procedure OnPrintSetup( var msg: TMessage );
      virtual CM_FIRST + CM_PRINTSETUP;
    procedure OnNew( var msg: TMessage ); virtual CM_FIRST + CM_NEWRPT;
    procedure OnOpen( var msg: TMessage ); virtual CM_FIRST + CM_OPENRPT;
    procedure OnBackup( var msg: TMessage ); virtual CM_FIRST + CM_BACKUP;
    procedure OnRestore( var msg: TMessage ); virtual CM_FIRST + CM_RESTORE;
    procedure OnTileHorizontal( var msg: TMessage );
      virtual CM_FIRST + CM_TILEHORIZONTAL;
    procedure OnTileVertical( var msg: TMessage );
      virtual CM_FIRST + CM_TILEVERTICAL;
    procedure OnCascade( var msg: TMessage ); virtual CM_FIRST + CM_CASCADE;
    procedure OnArrange( var msg: TMessage ); virtual CM_FIRST + CM_ARRANGE;
    procedure OnZoom( var msg: TMessage ); virtual CM_FIRST + CM_ZOOM;
  end;



{----------------------------------------------------------------------------}
{ The constructor begins the initialization process. It performes
{ construction-time initializations (See SetupWindow for post construction
{ initializations). It calls the base class, adjusts some window attributes,
{ and tends to its own attributes.
{----------------------------------------------------------------------------}

constructor TRptMain.Init( AName: PChar; AMenu: HMenu );
begin
  inherited Init( AName, AMenu );

  { Although the window is initially maximized (see SetupWindow), this block
    of code sets the normal window size to be the same as the maximized size.
    This will make it somewhat difficult for the user to decrease the window
    size. The purpose of all this is not known. }
  attr.x := CW_USEDEFAULT;
  attr.y := CW_USEDEFAULT;
  attr.w := GetSystemMetrics( SM_CXFULLSCREEN );
  attr.h := GetSystemMetrics( SM_CYFULLSCREEN );

  { Construct the status bar }
  pSBar := New( PStatusBar, Init( @self, 1 ) );
  pSBar^.AddSegment( '', 75, SB_ALIGNCENTER );
  pSBar^.ChangeSegmentText( 0, '' );
  pSBar^.ChangeSegmentText( 1, RptGenVersion );

  { Construct the printer object }
  prn := New( PPrinter, Init );

  { Set initial zoom }
  Zoom := 1.0;
end;



{----------------------------------------------------------------------------}
{ After calling the base class, the destructor releases its allocations.
{----------------------------------------------------------------------------}

destructor TRptMain.Done;
begin
  inherited done;
  DeleteObject( SBarFont );
  MSDisposeObj( prn );
end;



{----------------------------------------------------------------------------}
{ This override completes the initialization processs. It performes post
{ construction initializations.
{----------------------------------------------------------------------------}

procedure TRptMain.SetupWindow;
var
  lf: TLogFont;
begin
  { Standard set up }
  inherited SetupWindow;

  { Apply special font }
  MakeScreenFont( lf, false, false );
  SBarFont := CreateFontIndirect( lf );
  SendMessage( pSBar^.HWindow, WM_SETFONT, SBarFont, 0 );

  { Cause window to be maximized }
  ShowWindow( hWindow, SW_SHOWMAXIMIZED );
end;



{----------------------------------------------------------------------------}
{ This procedure adds child window processing to the standard processing of a
{ sizing event.
{----------------------------------------------------------------------------}

procedure TRptMain.wmSize( var msg: TMessage );
const
  REPAINT = true;
  X = 0;
  Y = 0;
var
  ClientRect: TRect;
  Width,
  Height: integer;
begin
  { Standard processing }
  inherited wmSize( msg );

  { Status Bar processing }
  GetClientRect( HWindow, ClientRect );
  pSBar^.ParentResized( ClientRect );

  { MDI client window processing }
  Width := msg.lParamLo + 1;
  Height := msg.lParamHi - pSBar^.attr.h + 1;
  MoveWindow( clientWnd^.HWindow, X, Y, Width, Height, REPAINT );
end;



{----------------------------------------------------------------------------}
{ This override constructs an MDI child window object based on TRptEdit.
{----------------------------------------------------------------------------}

function TRptMain.InitChild: PWindowsObject;
begin
  InitChild := New( PRptEdit, Init( @self, Zoom, SeqNum ) );
end;



{----------------------------------------------------------------------------}
{ This procedure handles a UM_BOXSTATUS message so that ...
{----------------------------------------------------------------------------}

procedure TRptMain.UMBoxStatus( var msg: TMessage );
begin
  pSBar^.ChangeSegmentText( 0, PChar( msg.lParam ) );
end;



{----------------------------------------------------------------------------}
{ This override replaces the default class name with the application's name
{ so that ...
{----------------------------------------------------------------------------}

function TRptMain.GetClassName: PChar;
begin
  GetClassName := application^.name;
end;



{----------------------------------------------------------------------------}
{ This override modifies the default window class to replace the default
{ icon with a custom icon that better represents the report format editor.
{----------------------------------------------------------------------------}

procedure TRptMain.GetWindowClass( var aWndClass: TWndClass );
begin
  inherited GetWindowClass( aWndClass );
  aWndClass.hIcon := LoadIcon( HInstance, MakeIntResource( ICON_MAIN ) );
end;



{----------------------------------------------------------------------------}
{ This procedure responds to the task module and tells it who it is.
{----------------------------------------------------------------------------}

procedure TRptMain.WMDMSQuery( var msg: TMessage );
begin
  StrLCopy( PChar( msg.lParam ), application^.Name, msg.wParam );
  msg.result := DMSMainProgRetCode;
end;



{----------------------------------------------------------------------------}
{ This procedure handles WM_WININICHANGE messages. This message may be sent
{ when a program changes the WIN.INI file. This handler is needed because the
{ international library depends on setting in this file. This procedure
{ reloads the international library settings and reports this to all child
{ windows so they can update any renderings dependent on the library.
{----------------------------------------------------------------------------}

procedure TRptMain.WMWinIni( var msg: TMessage );

  {--------------------------------------------------------------------------}
  { This procedure sends a UM_PREFERENCECHANGE message to the specified
  { window.
  {--------------------------------------------------------------------------}
  procedure SendPreferenceChange( pWin: PWindowsObject ); far;
  begin
    SendMessage( pWin^.HWindow, UM_PREFERENCECHANGE, 0, 0 );
  end;

begin { WMWinIni }

  { Reset international library and report change to child windows. }
  IntlInitFromWinIni;
  ForEach( @SendPreferenceChange );

end;



{----------------------------------------------------------------------------}
{ This procedure handles CM_FILEEXIT command (from the File | Exit menu item)
{ by calling CloswWindow. Depeinding on CanClose, this should close the
{ window.
{
{ Note: Why does this menu item not sent the standard cm_Exit command?
{----------------------------------------------------------------------------}

procedure TRptMain.OnExit( var msg: TMessage );
begin
  CloseWindow;
end;



{----------------------------------------------------------------------------}
{ This procedure handles the CM_PREFERENCES command (from the File |
{ Preferences... menu item). It executes the Preferences dialog box and
{ informs the child windows of possible changes.
{----------------------------------------------------------------------------}

procedure TRptMain.OnPreferences( var msg: TMessage );

  {--------------------------------------------------------------------------}
  { This procedure sends a UM_PREFERENCECHANGE message to the specified
  { window.
  {--------------------------------------------------------------------------}
  procedure SendPreferenceChange( p: PWindowsObject ); far;
  begin
    SendMessage( p^.HWindow, UM_PREFERENCECHANGE, 0, 0 );
  end;

var
  pDialog: PPrefDlg;

begin { OnPreferences }

  pDialog := New( PPrefDlg, Init( @self ) );
  if application^.ExecDialog( pDialog ) = IDOK then
    ForEach( @SendPreferenceChange );

end;



{----------------------------------------------------------------------------}
{ This procedure handles CM_PRINTSETUP commands (from the File | Printer
{ Setup... menu item). It calls the printer's Setup function.
{----------------------------------------------------------------------------}

procedure TRptMain.OnPrintSetup( var msg: TMessage );
begin
  prn^.Setup( @self );
end;



{----------------------------------------------------------------------------}
{ This procedure handles CM_NEWRPT commands (from the File | New menu item).
{
{ Note: Why does this menu item not sent the standard cm_FileNew command?
{----------------------------------------------------------------------------}

procedure TRptMain.OnNew( var msg: TMessage );
begin
  SeqNum := -1;  { flag as new report for .InitChild }
  CreateChild;   { tell MDI frame to create a new child }
end;



{----------------------------------------------------------------------------}
{ This procedure handles CM_OPENRPT commands (from the File | Open... menu
{ item). It prompts the user to select a format then creates a window for it.
{
{ Note: Why does this menu item not sent the standard cm_FileOpen command?
{----------------------------------------------------------------------------}

procedure TRptMain.OnOpen( var msg: TMessage );

  {--------------------------------------------------------------------------}
  { This procedure prompts user to select a report. It returns true if
  { selected, false if canceled. The sequence number of the report will be
  { returned in aSeq.
  {--------------------------------------------------------------------------}
  function SelectFormat( aParent: PWindowsObject; var aSeq: longint ):
    boolean;
  var
    pDialog: PSelectFormatDialog;
  begin
    aSeq := -1;
    pDialog := New( PSelectFormatDialog, Init( aParent, aSeq ) );
    SelectFormat := application^.ExecDialog( pDialog ) = IDOK;
  end;

  {--------------------------------------------------------------------------}
  { This function returns true if SeqNum matches any child window and if so,
  { returns pointer to the matching window.
  {--------------------------------------------------------------------------}
  function isOpen( var pOpenWin: PWindowsObject ): boolean;

    {--------------------------------------------------------------------------}
    { This function returns true if the specified window matches SeqNum.
    {--------------------------------------------------------------------------}
    function Matches( pWin: PWindowsObject ): boolean; far;
    begin
      Matches := SendMessage( pWin^.HWindow, UM_ISRPTLOADED, 0, SeqNum ) = 1;
    end;

  begin
    {- ensure that the selected report is not already loaded }
    pOpenWin := FirstThat( @Matches );
    isOpen := pOpenWin <> nil;
  end;

  {--------------------------------------------------------------------------}
  { This procedure warns the user and then focus the specified window.
  {--------------------------------------------------------------------------}
  procedure WarnAndFocus( pWin: PWindowsObject );
  var
    Title,
    Message: array[ 0..100 ] of char;
  begin

    InfoMsg( HWindow, SR( IDS_INFORMATION, Title, SizeOf( Title ) - 1 ),
      SR( IDS_ALREADYSEL, Message, SizeOf( Message ) - 1 ) );
    SetFocus( pWin^.HWindow );
  end;

var
  pWin: PWindowsObject;

begin { OnOpen }

  if SelectFormat( @self, SeqNum ) then
    if isOpen( pWin ) then
      WarnAndFocus( pWin )
    else
      CreateChild;

end;



{----------------------------------------------------------------------------}
{ This procedure handles CM_ZOOM commands (from the Options | Zoom menu
{ item). It executes the zoom dialog and passes the new zoom setting to the
{ child windows.
{----------------------------------------------------------------------------}

procedure TRptMain.OnZoom( var msg: TMessage );

  {--------------------------------------------------------------------------}
  { This procedure sends a UM_SETZOOM message to the specified window.
  {--------------------------------------------------------------------------}
  procedure SendZoomMessage( p: PWindowsObject ); far;
  begin
    { Note: zoom is scaled up so that it can be passed as an integer. It will
      need to be scaled down at the other end. This method assumes zooms are
      at least 0.01 and less significant digits are not needed. This is valid
      with the current set of zoom values. }
    SendMessage( p^.HWindow, UM_SETZOOM, 0, Trunc( Zoom * 100.0 ) );
  end;

var
  pDialog: PZoomDlg;

begin { OnZoom }

  pDialog := New( PZoomDlg,
    Init( @self, MakeIntResource( DLG_ZOOM ), IDC_ZOOMLIST, Zoom ) );
  if application^.ExecDialog( pDialog ) = IDOK then
    ForEach( @SendZoomMessage );

end;



{----------------------------------------------------------------------------}
{ This procedure handles CM_TILEHORIZONTAL commands (from the Window | Tile
{ Horizontal menu item).
{----------------------------------------------------------------------------}

procedure TRptMain.OnTileHorizontal( var msg: TMessage );
begin
  SendMessage( clientWnd^.HWindow, WM_MDITILE, MDITILE_HORIZONTAL, 0 );
end;



{----------------------------------------------------------------------------}
{ This procedure handles CM_TILEVERTICAL commands (from the Window | Tile
{ Vertical menu item).
{----------------------------------------------------------------------------}

procedure TRptMain.OnTileVertical( var msg: TMessage );
begin
  SendMessage( clientWnd^.HWindow, WM_MDITILE, MDITILE_VERTICAL, 0 );
end;



{----------------------------------------------------------------------------}
{ This procedure handles CM_CASCADE commands (from the Window | Cascade menu
{ item).
{
{ Note: Why does this menu item not sent the standard cm_CascadeChildren
{ command?
{----------------------------------------------------------------------------}

procedure TRptMain.OnCascade( var msg: TMessage );
begin
  SendMessage( clientWnd^.HWindow, WM_MDICascade, 0, 0 );
end;



{----------------------------------------------------------------------------}
{ This procedure handles CM_ARRANGE commands (from the Window | Arrange Icons
{ menu item).
{
{ Note: Why does this menu item not sent the standard cm_ArrangeIcons command?
{----------------------------------------------------------------------------}

procedure TRptMain.OnArrange( var msg: TMessage );
begin
  SendMessage( clientWnd^.HWindow, WM_MDIICONARRANGE, 0, 0 );
end;



{----------------------------------------------------------------------------}
{ This procedure handles CM_BACKUP commands (from the File | Backup... menu
{ item) by calling the FMReportBackup procedure.
{----------------------------------------------------------------------------}

procedure TRptMain.OnBackup( var msg: TMessage );
begin
  FMReportBackup( @self );
end;



{----------------------------------------------------------------------------}
{ This procedure handles CM_RESTORE commands (from the File | Restore... menu
{ item) by calling the FMReportRestore procedure.
{----------------------------------------------------------------------------}

procedure TRptMain.OnRestore( var msg: TMessage );
begin
  FMReportRestore( @self );
end;



                        {----------------------}
                        {  Application Object  }
                        {----------------------}



type
  {--------------------------------------------------------------------------}
  { This object represents the Report Format Editor application program.
  {--------------------------------------------------------------------------}
  TRptMainApp = object( T3DApplication )
    procedure InitMainWindow; virtual;
    procedure InitInstance; virtual;
  end;



{----------------------------------------------------------------------------}
{ This required override constructs the main window object
{----------------------------------------------------------------------------}

procedure TRptMainApp.InitMainWindow;
var
  Title: array[ 0..100 ] of char;
begin
  if INIUse3d then
    Register3dApp( true, true, true );
  SR( IDS_WINTITLE, Title, 100 );
  mainWindow := New( PRptMain, Init( Title, hFileMenu ) );
end;



{----------------------------------------------------------------------------}
{ This procedure performs any initialization necessary for every executing
{ instance of the application. This override adds accelerator table
{ initialization and unit preferences.
{----------------------------------------------------------------------------}

procedure TRptMainApp.InitInstance;
var
  UnitsStr: array[ 0..50 ] of char;
begin

  { Standard initialization }
  inherited InitInstance;

  { Load accelerator table }
  HAccTable := LoadAccelerators( HInstance, MakeIntResource( ACCEL_MAIN ) );

  { Read and set units }
  GetPrivateProfileString( 'Report Generator', 'Units', 'MM', UnitsStr, 50,
    'DMS.INI' );
  AnsiUpper( UnitsStr );
  if strcomp( UnitsStr, 'MM' ) = 0 then
    preferences.units := rptMM
  else if strcomp( UnitsStr, 'CM' ) = 0 then
    preferences.units := rptCM
  else
    preferences.units := rptInch;
end;



                        {----------------------}
                        {    Program Main      }
                        {----------------------}



var
  {--------------------------------------------------------------------------}
  { The application object.
  {--------------------------------------------------------------------------}
  rptApp: TRptMainApp;



{----------------------------------------------------------------------------}
{ This is the program's top-level procedure.
{----------------------------------------------------------------------------}

BEGIN
  if OKtoRun( MOD_RPTGEN, false ) then
  begin
    hFileMenu := LoadMenu( HInstance, MAKEINTRESOURCE( MENU_MAIN ) );
    hRptMenu := LoadMenu( HInstance, MAKEINTRESOURCE( MENU_RPTEDIT ) );
    hFileWindowMenu := GetSubMenu( hFileMenu, 0 );
    hRptWindowMenu := GetSubMenu( hRptMenu, 3 );

    rptApp.Init( GetModuleName( MOD_RPTGEN ) );
    rptApp.Run;
    rptApp.Done;

    DestroyMenu( hFileMenu );
    DestroyMenu( hRptMenu );
  end;
END.