unit IntrpMap;

{-----------------------------------------------------------------------------}
{  Module Name  : IntrpMap                                                    }
{  Programmer   : Kevin Riley                                                 }
{  Date Created : February 23, 1995                                           }
{                                                                             }
{  Purpose -                                                                  }
{  This module provides functions for mapping a Therapy Interpretation code   }
{  to its corresponding textual representations, and for mapping Therapy      }
{  Interpretation text back to its corresponding code. It also provides a     }
{  function to execute a dialog for editing the mapped Interpretation values. }
{  Following are the                                                          }
{  the default codes and text as of 05/25/95:                                 }
{     0  = 'N/A'                                                              }
{     1  = 'TFG'                                                              }
{     2  = 'MS'                                                               }
{     3  = 'S'                                                                }
{     4  = 'R'                                                                }
{     5  = 'N/R'                                                              }
{     6  = '' (this one is hard-coded)                                        }
{     7  = 'BLac'                                                             }
{     8  = 'I'                                                                }
{    13  = 'IB'                                                               }
{    14  = 'MRSA'                                                             }
{    15  = '' (hard-coded contraindication)                                   }
{     Any other code is invalid and returns '****'.                           }
{                                                                             }
{  Assumptions -                                                              }
{  It is assumed that the IntrpMap database files exist and are valid. If     }
{  not, the program will be halted with a fatal error.                        }
{                                                                             }
{  Initialization -                                                           }
{  This module creates the global collection InterpColl and destroys the      }
{  object on program exit.                                                    }
{                                                                             }
{  Revision History -                                                         }
{                                                                             }
{  Version  Date      Who    Did What                                         }
{  -------  --------  ------ -----------------------------------------------  }
{  1.00     02/23/94  KMR    Initial release.                                 }
{                                                                             }
{-----------------------------------------------------------------------------}

INTERFACE

uses
  OWindows;

const
  maxInterpStrLen = 4;  { This constant should be used externally }

function InterpCodeToStr(aCode: integer; aStr: PChar; maxLen: longint; var contraindicated: boolean): boolean;
function InterpCodeToMappedStr(aCode: integer; aStr: PChar; maxLen: longint; var contraindicated: boolean): boolean;
function InterpStrToCode(aStr: PChar; contraindicated: boolean) : integer;
function InterpStrToMappedStr(aStr, aMappedStr: PChar; maxLen: longint; contraindicated: boolean): boolean;
function SetMappedStr(AParent: PWindowsObject; aCode : integer; aStr : PChar) : boolean;
procedure DialogEditInterpMap(AParent: PWindowsObject);

IMPLEMENTATION

uses
  APITools,
  DlgLib,
  DBFile,
  DBIds,
  DBLib,
  DBTypes,
  DMSDebug,
  DMSErr,
  GridLib,
  MScan,
  Objects,
  ODialogs,
  Strings,
  StrsW,
  WinProcs,
  WinTypes;

{$R INTRPMAP.RES}
{$I INTRPMAP.INC}

const
  NumColsInterp         = 3;            { Number of columns in grid }
  ColInterpText         = 0;            { Grid column number of Text }
  ColInterpMappedText   = 1;            { Grid column number of Mapped Text }
  ColInterpModified     = 2;            { Grid column number of Modified Flag }

type
  PInterpRec = ^TInterpRec;
  TInterpRec = object(TObject)
    seq  : TSeqNum;                                 { sequence number from database }
    code : integer;                                 { Interpretation code value }
    text : array[0..maxInterpStrLen] of char;       { Standard Interpretation text }
    mappedText: array[0..maxInterpStrLen] of char;  { Mapped (user-modifiable) Interpretation text }
  end;

  PDlgEditMappedText = ^TDlgEditMappedText;
  TDlgEditMappedText = object(TCenterDlg)
    interpText,                                     { Interpretation Text value }
    interpMappedText : PChar;                       { Interpretation Mapped Text value }
    txtInterpText : PStatic;                        { Static control for Text display }
    edtInterpMappedText : PEdit;                    { Edit control for Mapped Text }
    constructor Init(AParent: PWindowsObject; intText, intMappedText : PChar);
    procedure SetupWindow; virtual;
    function CanClose: boolean; virtual;
  end;

  PDlgInterpMap = ^TDlgInterpMap;
  TDlgInterpMap = object(TCenterDlg)
    grid  : PGrid;                                  { Grid for Interp list }
    constructor Init(AParent: PWindowsObject);
    procedure SetupWindow; virtual;
    function CanClose: boolean; virtual;
    procedure DrawItem (var msg: TMessage); virtual WM_FIRST + WM_DRAWITEM;
    procedure MeasureItem (var msg: TMessage); virtual WM_FIRST + WM_MEASUREITEM;
    procedure CharToItem (var msg: TMessage); virtual WM_FIRST + WM_CHARTOITEM;
    procedure FillGrid; virtual;
    procedure BtnReset(var msg: TMessage); virtual ID_FIRST + IDC_BTN_RESET;
    procedure BtnEdit(var msg: TMessage); virtual ID_FIRST + IDC_BTN_EDIT;
    procedure GrdInterps(var msg: TMessage); virtual ID_FIRST + IDC_GRD_INTERPS;
  end;

var
  interpColl : PCollection;             { Collection of Interpretation mappings }
  exitSave   : Pointer;                 { Pointer to default exit routine }

{-----------------------------------------------------------------------------}
{  This function constructs the "Modify Interpretation Mapping" dialog.       }
{                                                                             }
{  Assumptions -                                                              }
{  None.                                                                      }
{                                                                             }
{  Errors -                                                                   }
{  None.                                                                      }
{                                                                             }
{  Returns : None.                                                            }
{  InParms : aParent - The Parent Window Object.                              }
{            intText - Default text for the Interpretation being modified.    }
{  OutParms: intMappedText - Mapped text for the Interpretation being         }
{                            modified.                                        }
{-----------------------------------------------------------------------------}
constructor TDlgEditMappedText.Init(AParent : PWindowsObject; intText, intMappedText : PChar);
begin
  inherited Init(AParent, MakeIntResource(DLG_EDIT_MAPPED_TEXT));
  txtInterpText:= new(PStatic, InitResource(@Self, IDC_TXT_INTERP, maxInterpStrLen + 1));
  edtInterpMappedText:= new(PEdit, InitResource(@Self, IDC_EDT_INTERP, maxInterpStrLen + 1));
  interpText:= intText;
  interpMappedText:= intMappedText;
end;

{-----------------------------------------------------------------------------}
{  This function sets up the "Modify Interpretation Mapping" dialog.          }
{                                                                             }
{  Assumptions -                                                              }
{  None.                                                                      }
{                                                                             }
{  Errors -                                                                   }
{  None.                                                                      }
{                                                                             }
{  Returns : None.                                                            }
{  InParms : None.                                                            }
{  OutParms: None.                                                            }
{-----------------------------------------------------------------------------}
procedure TDlgEditMappedText.SetupWindow;
begin
  inherited SetupWindow;

  { assign passed-in strings to controls }
  txtInterpText^.SetText(interpText);
  edtInterpMappedText^.SetText(interpMappedText);
end;

{-----------------------------------------------------------------------------}
{  This function processes the OK button of the "Modify Interpretation        }
{  Mapping" dialog.                                                           }
{                                                                             }
{  Assumptions -                                                              }
{  None.                                                                      }
{                                                                             }
{  Errors -                                                                   }
{  None                                                                       }
{                                                                             }
{  Returns : True if no errors, otherwise False.                              }
{  InParms : None.                                                            }
{  OutParms: None.                                                            }
{-----------------------------------------------------------------------------}
function TDlgEditMappedText.CanClose: boolean;
var
  retVal : boolean;
  pstr   : array[0..maxInterpStrLen] of char;
begin
  retVal:= inherited CanClose;

  if retVal then
  begin
    edtInterpMappedText^.GetText(pstr, maxInterpStrLen+1);
    TrimAll(interpMappedText, pstr, ' ', maxInterpStrLen);
  end;
  CanClose:= retVal;
end;

{-----------------------------------------------------------------------------}
{  This function constructs the main "Interpretation Mapping" dialog.         }
{                                                                             }
{  Assumptions -                                                              }
{  None.                                                                      }
{                                                                             }
{  Errors -                                                                   }
{  None.                                                                      }
{                                                                             }
{  Returns : None.                                                            }
{  InParms : aParent - The Parent Window Object.                              }
{  OutParms: None.                                                            }
{-----------------------------------------------------------------------------}
constructor TDlgInterpMap.Init(AParent : PWindowsObject);
begin
  inherited Init(AParent, MakeIntResource(DLG_INTERP_MAP));
  grid:= new(PGrid, InitResource(@Self, IDC_GRD_INTERPS, NumColsInterp, false));
end;

{-----------------------------------------------------------------------------}
{  This function sets up the main "Interpretation Mapping" dialog.            }
{                                                                             }
{  Assumptions -                                                              }
{  This function assumes that a grid has already been constructed.            }
{                                                                             }
{  Errors -                                                                   }
{  None.                                                                      }
{                                                                             }
{  Returns : None.                                                            }
{  InParms : None.                                                            }
{  OutParms: None.                                                            }
{-----------------------------------------------------------------------------}
procedure TDlgInterpMap.SetupWindow;
var
  pStr : array[0..255] of char;
  tInt : integer;
  gridRect: TRect;
begin
  inherited SetupWindow;

  { create two equally wide columns }
  GetClientRect(grid^.HWindow, gridRect);
  tInt:= (gridRect.right - gridRect.left) div 2;
  grid^.SetColumnWidth(ColInterpText, tInt);
  grid^.StretchColumn(ColInterpMappedText);

  grid^.SetHeader(ColInterpText, SR(IDS_INTERPRETATION, pStr, SizeOf(pStr)-1));
  grid^.SetHeader(ColInterpMappedText, SR(IDS_ALTERNATE_TEXT, pStr, SizeOf(pStr)-1));

  FillGrid;
end;

{-----------------------------------------------------------------------------}
{  This function processes the OK button of the main "Interpretation Mapping" }
{  dialog.                                                                    }
{                                                                             }
{  Assumptions -                                                              }
{  None.                                                                      }
{                                                                             }
{  Errors -                                                                   }
{  None.                                                                      }
{                                                                             }
{  Returns : True if no errors, otherwise False.                              }
{  InParms : None.                                                            }
{  OutParms: None.                                                            }
{-----------------------------------------------------------------------------}
function TDlgInterpMap.CanClose: boolean;
var
  retVal : boolean;
  maxRow : longint;
  aRow   : longint;
  interpCode : integer;
  interpMappedText : array[0..maxInterpStrLen] of char;
  interpModified : array[0..1] of char;
begin
  retVal:= inherited CanClose;

  if retVal then
  begin
    maxRow:= grid^.GetRowCount - 1;
    for aRow:= 0 to maxRow do
    begin
      grid^.GetData(ColInterpModified, aRow, interpModified, SizeOf(interpModified)-1);
      { if Mapped Text was modified, then store it }
      if (StrLen(interpModified) > 0) then
      begin
        interpCode:= grid^.GetItemData(aRow);
        grid^.GetData(ColInterpMappedText, aRow, interpMappedText, maxInterpStrLen);
        retVal:= SetMappedStr(@Self, interpCode, interpMappedText);
      end;
    end;
  end;
  CanClose:= retVal;
end;

{-----------------------------------------------------------------------------}
{  This function passes DrawItem messages to the grid of the main             }
{  "Interpretation Mapping" dialog.                                           }
{                                                                             }
{  Assumptions -                                                              }
{  None.                                                                      }
{                                                                             }
{  Errors -                                                                   }
{  None.                                                                      }
{                                                                             }
{  Returns : None.                                                            }
{  InParms : msg - The message being passed.                                  }
{  OutParms: None.                                                            }
{-----------------------------------------------------------------------------}
procedure TDlgInterpMap.DrawItem(var msg: TMessage);
begin
  grid^.DrawItem(msg);
end;

{-----------------------------------------------------------------------------}
{  This function passes MeasureItem messages to the grid of the main          }
{  "Interpretation Mapping" dialog.                                           }
{                                                                             }
{  Assumptions -                                                              }
{  None.                                                                      }
{                                                                             }
{  Errors -                                                                   }
{  None.                                                                      }
{                                                                             }
{  Returns : None.                                                            }
{  InParms : msg - The message being passed.                                  }
{  OutParms: None.                                                            }
{-----------------------------------------------------------------------------}
procedure TDlgInterpMap.MeasureItem(var msg: TMessage);
begin
  grid^.MeasureItem(msg);
end;

{-----------------------------------------------------------------------------}
{  This function passes CharToItem messages to the grid of the main           }
{  "Interpretation Mapping" dialog.                                           }
{                                                                             }
{  Assumptions -                                                              }
{  None.                                                                      }
{                                                                             }
{  Errors -                                                                   }
{  None.                                                                      }
{                                                                             }
{  Returns : None.                                                            }
{  InParms : msg - The message being passed.                                  }
{  OutParms: None.                                                            }
{-----------------------------------------------------------------------------}
procedure TDlgInterpMap.CharToItem(var msg: TMessage);
begin
  grid^.CharToItem(msg);
end;

{-----------------------------------------------------------------------------}
{  This function fills the grid of the main "Interpretation Mapping" dialog.  }
{                                                                             }
{  Assumptions -                                                              }
{  This function assumes that a grid and a collection from which to fill it   }
{  both exist.                                                                }
{                                                                             }
{  Errors -                                                                   }
{  None.                                                                      }
{                                                                             }
{  Returns : None.                                                            }
{  InParms : None.                                                            }
{  OutParms: None.                                                            }
{-----------------------------------------------------------------------------}
procedure TDlgInterpMap.FillGrid;

{-----------------------------------------------------------------------------}
{  This function adds a row to the grid of the main "Interpretation Mapping"  }
{  dialog.                                                                    }
{                                                                             }
{  Assumptions -                                                              }
{  This function assumes that a grid exists.                                  }
{                                                                             }
{  Errors -                                                                   }
{  None.                                                                      }
{                                                                             }
{  Returns : None.                                                            }
{  InParms : interpRec - The record containing data to be added to the grid.  }
{  OutParms: None.                                                            }
{-----------------------------------------------------------------------------}
  procedure AddToGrid(interpRec : PInterpRec); far;
  var
    aRow : longint;
  begin
    { don't put blank interp map records in grid }
    if (StrLen(interpRec^.text) > 0) then
    with grid^ do
    begin
      aRow:= AddString('');
      SetItemData(aRow, interpRec^.code);
      SetData(ColInterpText, aRow, interpRec^.text);
      SetData(ColInterpMappedText, aRow, interpRec^.mappedText);
      SetData(ColInterpModified, aRow, '');
    end;
  end;

begin
  with grid^ do
  begin
    SetRedraw(False);
    ClearList;
    interpColl^.ForEach(@AddToGrid);
    SetSelIndex(0);
    SetRedraw(True);
  end;
end;

{-----------------------------------------------------------------------------}
{  This function processes the Edit button of the main "Interpretation        }
{  Mapping" dialog.                                                           }
{                                                                             }
{  Assumptions -                                                              }
{  This function assumes that a grid exists.                                  }
{                                                                             }
{  Errors -                                                                   }
{  None.                                                                      }
{                                                                             }
{  Returns : None.                                                            }
{  InParms : msg - The message being responded to.                            }
{  OutParms: None.                                                            }
{-----------------------------------------------------------------------------}
procedure TDlgInterpMap.BtnEdit(var msg: TMessage);
var
  interpText,
  interpMappedText : array[0..maxInterpStrLen] of char;
begin
  if (grid^.GetSelIndex >= 0) then
  begin
    grid^.GetData(ColInterpText, grid^.GetSelIndex, interpText, maxInterpStrLen);
    grid^.GetData(ColInterpMappedText, grid^.GetSelIndex, interpMappedText, maxInterpStrLen);
    if (application^.ExecDialog(new(PDlgEditMappedText, Init(@Self, interpText, interpMappedText))) = IDOK) then
    begin
      { A change was made, so update grid and set modified flag }
      grid^.SetData(ColInterpMappedText, grid^.GetSelIndex, interpMappedText);
      grid^.SetData(ColInterpModified, grid^.GetSelIndex, '*');
    end;
  end;
end;

{-----------------------------------------------------------------------------}
{  This function processes the Reset button of the main "Interpretation       }
{  Mapping" dialog.                                                           }
{                                                                             }
{  Assumptions -                                                              }
{  This function assumes that a grid exists.                                  }
{                                                                             }
{  Errors -                                                                   }
{  None.                                                                      }
{                                                                             }
{  Returns : None.                                                            }
{  InParms : msg - The message being responded to.                            }
{  OutParms: None.                                                            }
{-----------------------------------------------------------------------------}
procedure TDlgInterpMap.BtnReset(var msg: TMessage);
var
  maxRow : longint;
  aRow   : longint;
  interpText,
  interpMappedText : array[0..maxInterpStrLen] of char;
begin
  maxRow:= grid^.GetRowCount - 1;
  for aRow:= 0 to maxRow do
  begin
    grid^.GetData(ColInterpText, aRow, interpText, maxInterpStrLen);
    grid^.GetData(ColInterpMappedText, aRow, interpMappedText, maxInterpStrLen);
    if (StrComp(interpText, interpMappedText) <> 0) then
    begin
      { Mapped value not equal to default, so reset value and set modified flag }
      grid^.SetData(ColInterpMappedText, aRow, interpText);
      grid^.SetData(ColInterpModified, aRow, '*');
    end;
  end;
end;

{-----------------------------------------------------------------------------}
{  This function processes messages responded to by the grid of the main      }
{  "Interpretation Mapping" dialog.                                           }
{                                                                             }
{  Assumptions -                                                              }
{  None.                                                                      }
{                                                                             }
{  Errors -                                                                   }
{  None.                                                                      }
{                                                                             }
{  Returns : None.                                                            }
{  InParms : msg - The message being responded to.                            }
{  OutParms: None.                                                            }
{-----------------------------------------------------------------------------}
procedure TDlgInterpMap.GrdInterps(var msg: TMessage);
begin
  if msg.LParamHI = lbn_DblClk then
    BtnEdit(msg);
end;

{-----------------------------------------------------------------------------}
{  This function converts an Interpretation code into its corresponding       }
{  text or mapped text, and returns True if successful, otherwise False.      }
{                                                                             }
{  Assumptions -                                                              }
{  This function assumes that the global collection interpColl exists.        }
{                                                                             }
{  Errors -                                                                   }
{  Returns '****' in aStr if code is not found.                               }
{                                                                             }
{  Returns : False if code is not found, otherwise True.                      }
{  InParms : aCode - The Interpretation code being converted to text.         }
{            maxLen - Maximum length of returned string.                      }
{            useMappedText - Boolean value to tell function whether to        }
{                            convert to standard text or mapped text.         }
{  OutParms: aStr            - Interpretation text.                           }
{            contraindicated - Boolean value to tell if contraindicated.      }
{-----------------------------------------------------------------------------}
function GetInterpStr(aCode: integer; aStr: PChar; maxLen: longint;
                      useMappedText: boolean; var contraindicated: boolean): boolean;

  function Matches(interpRec : PInterpRec) : boolean; far;
  begin
    Matches:= (aCode = interpRec^.code);
  end;

var
  retVal    : boolean;
  interpRec : PInterpRec;
begin
  retVal:= True;
  { a value of 15 means it is contraindicated                   }
  contraindicated:= (aCode = 15);
  { a value of 6 is a special code for a "blank" interpretation }
  If (aCode = 6) or (aCode = 15) then
    StrCopy(aStr, '')
  else
  begin
    interpRec:= interpColl^.FirstThat(@Matches);
    if (interpRec = nil) then  { no match, return invalid text }
    begin
      StrLCopy(aStr, '****', maxLen);
      retVal:= False;
    end
    else if useMappedText then { return mapped text }
      StrLCopy(aStr, interpRec^.mappedText, maxLen)
    else                       { return standard text }
      StrLCopy(aStr, interpRec^.text, maxLen);
  end;
  GetInterpStr:= retVal;
end;

{-----------------------------------------------------------------------------}
{  This function converts an Interpretation code into its corresponding       }
{  text, and returns True if successful, otherwise False.                     }
{                                                                             }
{  Assumptions -                                                              }
{  None.                                                                      }
{                                                                             }
{  Errors -                                                                   }
{  None.                                                                      }
{                                                                             }
{  Returns : False if code is not found, otherwise True.                      }
{  InParms : aCode - The Interpretation code being converted to text.         }
{            maxLen - Maximum length of returned string.                      }
{  OutParms: aStr            - Interpretation text.                           }
{            contraindicated - Boolean value to tell if contraindicated.      }
{-----------------------------------------------------------------------------}
function InterpCodeToStr(aCode: integer; aStr: PChar; maxLen: longint; var contraindicated: boolean): boolean;
begin
  InterpCodeToStr:= GetInterpStr(aCode, aStr, maxLen, False, contraindicated);
end;

{-----------------------------------------------------------------------------}
{  This function converts an Interpretation code into its corresponding       }
{  mapped text, and returns True if successful, otherwise False.              }
{                                                                             }
{  Assumptions -                                                              }
{  None.                                                                      }
{                                                                             }
{  Errors -                                                                   }
{  None.                                                                      }
{                                                                             }
{  Returns : False if code is not found, otherwise True.                      }
{  InParms : aCode - The Interpretation code being converted to mapped text.  }
{            maxLen - Maximum length of returned string.                      }
{  OutParms: aStr            - Interpretation mapped text.                    }
{            contraindicated - Boolean value to tell if contraindicated.      }
{-----------------------------------------------------------------------------}
function InterpCodeToMappedStr(aCode: integer; aStr: PChar; maxLen: longint; var contraindicated: boolean): boolean;
begin
  InterpCodeToMappedStr:= GetInterpStr(aCode, aStr, maxLen, True, contraindicated);
end;

{-----------------------------------------------------------------------------}
{  This function takes an Interpretation text string and returns its          }
{  corresponding code.                                                        }
{                                                                             }
{  Assumptions -                                                              }
{  This function assumes that the global collection interpColl exists.        }
{                                                                             }
{  Errors -                                                                   }
{  Returns -2 if Interpretation text is not found.                            }
{                                                                             }
{  Returns : Integer Interpretation code value, -2 on error.                  }
{  InParms : aStr - Interpretation text being converted to a code.            }
{            contraindicated - Boolean value to tell if contraindicated.      }
{  OutParms: None.                                                            }
{-----------------------------------------------------------------------------}
function InterpStrToCode(aStr: PChar; contraindicated: boolean) : integer;

  function Matches(interpRec : PInterpRec) : boolean; far;
  begin
    Matches:= (StrIComp(aStr, interpRec^.text) = 0);
  end;

var
  retVal    : integer;
  interpRec : PInterpRec;
begin
  { a value of 6 is a special code for a "blank" interpretation }
  if (StrLen(aStr) = 0) then
  begin
    if contraindicated then
      retVal:= 15
    else
      retVal:= 6;
  end
  else
  begin
    interpRec:= interpColl^.FirstThat(@Matches);
    if (interpRec = nil) then { text not found, return invalid code }
      retVal:= -2
    else                      { return corresponding code }
      retVal:= interpRec^.code;
  end;
  InterpStrToCode:= retVal;
end;

{-----------------------------------------------------------------------------}
{  This function converts an Interpretation text string into its corresponding}
{  mapped text, and returns True if successful, otherwise False.              }
{                                                                             }
{  Assumptions -                                                              }
{  None.                                                                      }
{                                                                             }
{  Errors -                                                                   }
{  None.                                                                      }
{                                                                             }
{  Returns : False if code is not found, otherwise True.                      }
{  InParms : aStr - The Interpretation text being converted to mapped text.   }
{            maxLen - Maximum length of returned string.                      }
{            contraindicated - Boolean value to tell if contraindicated.      }
{  OutParms: aMappedStr - Interpretation mapped text.                         }
{-----------------------------------------------------------------------------}
function InterpStrToMappedStr(aStr, aMappedStr: PChar; maxLen: longint; contraindicated: boolean): boolean;
var
  aCode : integer;
begin
  InterpStrToMappedStr:= InterpCodeToMappedStr(InterpStrToCode(aStr, contraindicated), aMappedStr, maxLen, contraindicated);
end;

{-----------------------------------------------------------------------------}
{  This function takes an Interpretation code and replaces its corresponding  }
{  mapped text with a specified string.                                       }
{                                                                             }
{  Assumptions -                                                              }
{  This function assumes that the global collection interpColl exists, and    }
{  that the IntrpMap database files exist and are valid.                      }
{                                                                             }
{  Errors -                                                                   }
{  Returns False if any problem is encountered.                               }
{                                                                             }
{  Returns : Boolean value - True if successful, otherwise False.             }
{  InParms : AParent - The parent window.                                     }
{            aCode - The Interpretation code being modified.                  }
{            aStr - The replacement string for the mapped text.               }
{  OutParms: None.                                                            }
{-----------------------------------------------------------------------------}
function SetMappedStr(AParent: PWindowsObject; aCode : integer; aStr : PChar) : boolean;

  function Matches(interpRec : PInterpRec) : boolean; far;
  begin
    Matches:= (aCode = interpRec^.code);
  end;

var
  retVal    : boolean;
  errSubst  : PErrSubst;               { Error substitution object }
  interpDB  : PDBFile;                 { Interpretation Map dabatase}
  interpRec : PInterpRec;
begin
  retVal:= False;
  errSubst:= new(PErrSubst, Init(DBINTRPMAPFile));
  if (errSubst = nil) then
    ShowError(AParent, IDS_INITERROR, nil, 0, MOD_INTRPMAP, 0)
  else
  begin
    interpDB:= new(PDBFile, Init(DBINTRPMAPFile, '', DBOpenNormal));
    if (interpDB = nil) then  { database failure }
      ShowError(AParent, IDS_CANNOT_OPEN_FILE, errSubst, dbLastOpenError, MOD_INTRPMAP, 0)
    else
    begin
      interpRec:= interpColl^.FirstThat(@Matches);
      if (interpRec <> nil) then { match found }
        if interpDB^.dbc^.GetSeq(interpDB^.dbr, interpRec^.seq) then  { find database record }
          if interpDB^.dbr^.PutField(DBINTRPMAPMappedText, aStr) then  { replace mapped text }
            if interpDB^.dbc^.UpdateRec(interpDB^.dbr) then  { store record }
            begin
              StrCopy(interpRec^.mappedText, aStr); { update collection }
              retVal:= True;
            end;
      Dispose(interpDB, Done);
    end;
    Dispose(errSubst, Done);
  end;
  SetMappedStr:= retVal;
end;

procedure DialogEditInterpMap(AParent: PWindowsObject);
begin
  application^.ExecDialog(new(PDlgInterpMap, Init(AParent)));
end;

function SetupCollection: boolean;
var
  retVal     : boolean;                 { Placeholder for return value }
  errSubst   : PErrSubst;               { Error substitution object }
  interpDB   : PDBFile;                 { Interpretation Map dabatase}
  interpRec  : PInterpRec;              { Interpretation Map record }
begin
  retVal:= True;
  errSubst:= new(PErrSubst, Init(DBINTRPMAPFile));
  if (errSubst = nil) then
  begin
    retVal:= False;
    ShowError(nil, IDS_INITERROR, nil, 0, MOD_INTRPMAP, 0);
  end
  else
  begin
    interpDB:= new(PDBFile, Init(DBINTRPMAPFile, '', DBOpenNormal));
    if (interpDB = nil) then  { database failure }
    begin
      retVal:= False;
      ShowError(nil, IDS_CANNOT_OPEN_FILE, errSubst, dbLastOpenError, MOD_INTRPMAP, 0);
    end
    else
    begin
      interpColl:= new(PCollection, Init(interpDB^.dbc^.NumRecords, 10));
      if (interpColl = nil) then
      begin
        retVal:= False;
        ShowError(nil, IDS_INITERROR, nil, 0, MOD_INTRPMAP, 0);
      end
      else
      begin
        if (interpDB^.dbc^.NumRecords > 0) and interpDB^.dbc^.GetFirst(interpDB^.dbr) then
        repeat
          interpRec:= new(PInterpRec, Init);
          if (interpRec = nil) then
          begin
            retVal:= False;
            ShowError(nil, IDS_INITERROR, nil, 0, MOD_INTRPMAP, 0);
          end
          else
          begin
            interpRec^.seq:= interpDB^.dbr^.GetSeqValue;
            interpDB^.dbr^.GetField(DBINTRPMAPCode, @(interpRec^.code), SizeOf(interpRec^.code));
            interpDB^.dbr^.GetField(DBINTRPMAPDfltText, @(interpRec^.text), SizeOf(interpRec^.text));
            interpDB^.dbr^.GetField(DBINTRPMAPMappedText, @(interpRec^.mappedText), SizeOf(interpRec^.mappedText));
            interpColl^.Insert(interpRec);
          end;
        until not (retVal and interpDB^.dbc^.GetNext(interpDB^.dbr));
      end;
      Dispose(interpDB, Done);
    end;
    Dispose(errSubst, Done);
  end;
  SetupCollection:= retVal;
end;

procedure ExitRoutine; far;
begin
  exitProc:= exitSave;
  if (interpColl <> nil) then
    Dispose(interpColl, Done);
end;

BEGIN
  interpColl:= nil;
  exitSave:= ExitProc;
  ExitProc:= @ExitRoutine;
  if not SetupCollection then
    Halt;
END.
