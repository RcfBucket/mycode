unit ModWin;

{- Modal Window Support }

INTERFACE

uses
  wintypes,
  winprocs,
  oWindows;

type
  PModalWin = ^TModalWin;
  TModalWin = object(TWindow)
    parentHWnd   : HWnd;
    constructor Init(AParent: PWindowsObject; ATitle: PChar; AParentHWnd: HWnd);
    procedure SetupWindow; virtual;
    destructor Done; virtual;
  end;

IMPLEMENTATION

{------------------------------[ TModalWin ]--}

constructor TModalWin.Init(AParent: PWindowsObject; ATitle: PChar; AParentHWnd: HWnd);
begin
  inherited init(AParent, ATitle);
  attr.style:= WS_OVERLAPPEDWINDOW or WS_VISIBLE;
  attr.x:= CW_USEDEFAULT;
  attr.y:= CW_USEDEFAULT;
  attr.w:= CW_USEDEFAULT;
  attr.h:= CW_USEDEFAULT;
  SetFlags(WB_MDIChild, false);
  parentHWnd:= AParentHWnd;
end;

destructor TModalWin.Done;
begin
  EnableWindow(parentHWnd, true);
  SetFocus(parentHWnd);
  inherited Done;
end;

procedure TModalWin.SetupWindow;
begin
  inherited SetupWindow;
  EnableWindow(parentHWnd, false);
end;


END.

{- rcf }
