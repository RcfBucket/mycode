{***
  CustIntr resource constants
  Module ID = $500 = 1280
***}
const
  DLG_EDITINTERP    = 1280;
  DLG_EDITTHERS     = 1281;
  DLG_MAIN          = 1282;
  DLG_PERCENT       = 1283;

  IDC_PERCENT       = 200;

  IDC_SUMMARY       = 106;
  IDC_PREV          = 107;
  IDC_NEXT          = 108;
  IDC_CLOSE         = 109;
  IDC_EDIT          = 110;
  IDC_SAVE          = 111;
  IDC_LIST          = 201;
  IDC_PNLS          = 202;
  IDC_URINELIST     = 203;
  IDC_THERS         = 204;
  IDC_BLOODLIST     = 205;

  IDS_TherDesc      = 1280;
  IDS_S1            = 1281;
  IDS_S2            = 1282;
  IDS_S3            = 1283;
  IDS_S4            = 1284;
  IDS_S5            = 1285;
  IDS_S6            = 1286;
  IDS_S7            = 1287;
  IDS_S8            = 1288;
  IDS_S9            = 1289;
  IDS_S10           = 1290;
  IDS_S11           = 1291;
  IDS_S12           = 1292;
  IDS_S13           = 1293;
  IDS_S14           = 1294;
  IDS_S15           = 1295;
  IDS_S16           = 1296;
  IDS_S17           = 1297;
  IDS_S18           = 1298;
  IDS_S19           = 1299;
  IDS_S20           = 1300;
  IDS_S21           = 1301;
  IDS_S22           = 1302;
  IDS_S23           = 1303;
  IDS_S24           = 1304;
  IDS_S25           = 1305;
  IDS_S26           = 1306;
  IDS_SUMOFCHANGE   = 1400;
  IDS_CANCELSUM     = 1401;
  IDS_CUSTINTERPS   = 1402;
  IDS_DRUGS         = 1403;
  IDS_DRUG          = 1404;
  IDS_MIC           = 1405;
  IDS_SYSTEMIC      = 1406;
  IDS_URINE         = 1407;
  IDS_PNLSAFFECTED  = 1408;
  IDS_NOMODS        = 1409;
  IDS_INTERPSUM     = 1410;
  IDS_PNLGRPS       = 1411;
  IDS_THERTBL       = 1412;
  IDS_CANTREAD      = 1413;
  IDS_CANTWRITE     = 1414;
  IDS_CHANGEMADE    = 1415;
  IDS_SAVECHANGE    = 1416;
  IDS_SAVECONFIRM   = 1417;
  IDS_BLANK         = 1418;
  IDS_ERROR         = 1419;
  IDS_CONFIRM       = 1420;
  IDS_NOCHANGES     = 1421;
  IDS_CANTOPENDRUGS = 1422;

  IDC_DRGLBL        = 209;
  IDC_DRUG          = 210;

  ICON_1            = 1280;
  ICON_2            = 1281;
  ICON_3            = 1282;

