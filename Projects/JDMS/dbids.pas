
unit  DbIds;

INTERFACE

const

  DBPATFile                          = 'patient';
  DBPATPatID                         = 1;
  DBPATLName                         = 2;
  DBPATFName                         = 3;
  DBPATFreeText                      = 4;
  DBPATFlags                         = 5;
  DBPATUDBase                        = 6;

  DBPAT_ID_KEY                       = 1;

  DBSPECFile                         = 'specimen';
  DBSPECSpecID                       = 1;
  DBSPECCollectDate                  = 2;
  DBSPECPatRef                       = 3;
  DBSPECSrc                          = 4;
  DBSPECWard                         = 5;
  DBSPECStat                         = 6;
  DBSPECFreeText                     = 7;
  DBSPECFlags                        = 8;
  DBSPECUDBase                       = 9;

  DBSPEC_ID_KEY                      = 1;
  DBSPEC_Date_KEY                    = 2;
  DBSPEC_Patient_KEY                 = 3;

  DBISOFile                          = 'isolate';
  DBISOIso                           = 1;
  DBISOSpecRef                       = 2;
  DBISOSetFamily                     = 3;
  DBISOOrgRef                        = 4;
  DBISOTstDate                       = 5;
  DBISOOrdRef                        = 6;
  DBISOSess                          = 7;
  DBISOFreeText                      = 8;
  DBISOTransFlag                     = 9;
  DBISOQCFlag                        = 10;
  DBISOSpecID                        = 11;
  DBISOCollectDate                   = 12;
  DBISOFlags                         = 13;
  DBISOUDBase                        = 14;

  DBISO_ID_KEY                       = 1;
  DBISO_Org_KEY                      = 2;
  DBISO_Trans_KEY                    = 3;
  DBISO_QC_KEY                       = 4;
  DBISO_SessionSpecIDCDate_KEY       = 5;
  DBISO_SessionCDateSpecID_KEY       = 6;
  DBISO_SpecIDCDate_KEY              = 7;
  DBISO_CDateSpecID_KEY              = 8;

  DBGSFile                           = 'grmstain';
  DBGSSpecRef                        = 1;
  DBGSGSTstRef                       = 2;
  DBGSGSMnemRef                      = 3;
  DBGSFreeText                       = 4;
  DBGSFlags                          = 5;

  DBGS_ID_KEY                        = 1;
  DBGS_Test_KEY                      = 2;

  DBISOORDFile                       = 'isorder';
  DBISOORDIsoRef                     = 1;
  DBISOORDTstGrpRef                  = 2;
  DBISOORDTstDate                    = 3;
  DBISOORDFlags                      = 4;

  DBISOORD_ID_KEY                    = 1;

  DBAltTstFile                       = 'alttst';
  DBAltTstIsoRef                     = 1;
  DBAltTstTstRef                     = 2;
  DBAltTstRes                        = 3;

  DBALTTST_ID_KEY                    = 1;

  DBRESFile                          = 'result';
  DBRESIsoOrdRef                     = 1;
  DBRESBlock                         = 2;
  DBRESFlags                         = 3;

  DBRES_ID_KEY                       = 1;

  DBSRCFile                          = 'source';
  DBSRCID                            = 1;
  DBSRCDesc                          = 2;
  DBSRCUrine                         = 3;
  DBSRCFlags                         = 4;

  DBSRC_ID_KEY                       = 1;

  DBWARDFile                         = 'ward';
  DBWARDID                           = 1;
  DBWARDDesc                         = 2;
  DBWARDFlags                        = 3;

  DBWARD_ID_KEY                      = 1;

  DBSTATFile                         = 'specstat';
  DBSTATID                           = 1;
  DBSTATDesc                         = 2;
  DBSTATFlags                        = 3;

  DBSTAT_ID_KEY                      = 1;

  DBSESSFile                         = 'session';
  DBSESSID                           = 1;
  DBSESSDesc                         = 2;
  DBSESSFlags                        = 3;

  DBSESS_ID_KEY                      = 1;

  DBORGFile                          = 'organism';
  DBORGOrg                           = 1;
  DBORGSName                         = 2;
  DBORGLName                         = 3;
  DBORGSet                           = 4;
  DBORGFamily                        = 5;
  DBORGUDFlag                        = 6;
  DBORGFlags                         = 7;

  DBORG_ID_KEY                       = 1;

  DBTSTGRPFile                       = 'testgrp';
  DBTSTGRPID                         = 1;
  DBTSTGRPDesc                       = 2;
  DBTSTGRPSet                        = 3;
  DBTSTGRPLayout                     = 4;
  DBTSTGRPLen                        = 5;
  DBTSTGRPUDFlag                     = 6;
  DBTSTGRPFlags                      = 7;

  DBTSTGRP_ID_KEY                    = 1;

  DBTSTGRPXRFFile                    = 'tstgpxrf';
  DBTSTGRPXRFTstGrpRef               = 1;
  DBTSTGRPXRFTstRef                  = 2;
  DBTSTGRPXRFResBlockPos             = 3;
  DBTSTGRPXRFFlags                   = 4;

  DBTSTGRPXRF_ID_KEY                 = 1;
  DBTSTGRPXRF_Test_KEY               = 2;

  DBTSTFile                          = 'test';
  DBTSTID                            = 1;
  DBTSTName                          = 2;
  DBTSTTstCat                        = 3;
  DBTSTTstCatRef                     = 4;
  DBTSTDrugRef                       = 5;
  DBTSTUDFlag                        = 6;
  DBTSTFlags                         = 7;

  DBTST_ID_KEY                       = 1;

  DBTSTCATFile                       = 'testcat';
  DBTSTCATID                         = 1;
  DBTSTCATDesc                       = 2;
  DBTSTCATResLen                     = 3;
  DBTSTCATFlags                      = 4;

  DBTSTCAT_ID_KEY                    = 1;

  DBDRUGFile                         = 'drug';
  DBDRUGAbbr                         = 1;
  DBDRUGName                         = 2;
  DBDRUGAltAbbr                      = 3;
  DBDRUGSupp                         = 4;
  DBDRUGSortNum                      = 5;
  DBDRUGDfltSortNum                  = 6;
  DBDRUGUDFlag                       = 7;
  DBDRUGFlags                        = 8;

  DBDRUG_ID_KEY                      = 1;
  DBDRUG_Sort_KEY                    = 2;
  DBDRUG_DfltSort_KEY                = 3;

  DBGSDEFFile                        = 'gsdef';
  DBGSDEFSlotNum                     = 1;
  DBGSDEFAbbr                        = 2;
  DBGSDEFDesc                        = 3;
  DBGSDEFResType                     = 4;
  DBGSDEFFlags                       = 5;

  DBGSDEF_ID_KEY                     = 1;

  DBGSMNEMFile                       = 'gsmnem';
  DBGSMNEMID                         = 1;
  DBGSMNEMDesc                       = 2;
  DBGSMNEMFlags                      = 3;

  DBGSMNEM_ID_KEY                    = 1;

  DBORDFile                          = 'order';
  DBORDID                            = 1;
  DBORDDesc                          = 2;
  DBORDSet                           = 3;
  DBORDFlags                         = 4;

  DBORD_ID_KEY                       = 1;

  DBORDXRFFile                       = 'orderxrf';
  DBORDXRFOrdRef                     = 1;
  DBORDXRFTstGrpRef                  = 2;
  DBORDXRFFlags                      = 3;

  DBORDXRF_ID_KEY                    = 1;

  DBWAPNLFile                        = 'wapanel';
  DBWAPNLID                          = 1;
  DBWAPNLIsoOrdRef                   = 2;
  DBWAPNLUsedFlag                    = 3;
  DBWAPNLBCPrinted                   = 4;
  DBWAPNLKnownToWA                   = 5;
  DBWAPNLFlags                       = 6;

  DBWAPNL_ID_KEY                     = 1;
  DBWAPNL_IsoOrd_KEY                 = 2;

  DBWAEXFile                         = 'waexlist';
  DBWAEXIsoOrdRef                    = 1;
  DBWAEXPanelID                      = 2;
  DBWAEXTower                        = 3;
  DBWAEXSlot                         = 4;
  DBWAEXPanelName                    = 5;
  DBWAEXStrNum                       = 6;
  DBWAEXString                       = 7;
  DBWAEXFlags                        = 8;

  DBWAEX_ID_KEY                      = 1;
  DBWAEX_Slot_KEY                    = 2;

  DBMPSSRULEFile                     = 'mpssrule';
  DBMPSSRULEFName                    = 1;
  DBMPSSRULEID                       = 2;
  DBMPSSRULEDesc                     = 3;
  DBMPSSRULEFlags                    = 4;

  DBMPSSRULE_ID_KEY                  = 1;

  DBMPSSCONDFile                     = 'mpsscond';
  DBMPSSCONDRuleRef                  = 1;
  DBMPSSCONDIndex                    = 2;
  DBMPSSCONDAndOr                    = 3;
  DBMPSSCONDLParen                   = 4;
  DBMPSSCONDRParen                   = 5;
  DBMPSSCONDFileName                 = 6;
  DBMPSSCONDFieldName                = 7;
  DBMPSSCONDOperator                 = 8;
  DBMPSSCONDValue                    = 9;
  DBMPSSCONDBlock                    = 10;
  DBMPSSCONDFlags                    = 11;

  DBMPSSCOND_ID_KEY                  = 1;

  DBINTRPMAPFile                     = 'intrpmap';
  DBINTRPMAPCode                     = 1;
  DBINTRPMAPDfltText                 = 2;
  DBINTRPMAPMappedText               = 3;
  DBINTRPMAPFlags                    = 4;

  DBINTRPMAP_ID_KEY                  = 1;

  DBMICFile                          = 'mpssmic';
  DBMICCode                          = 1;
  DBMICDesc                          = 2;
  DBMICFlags                         = 3;

  DBMIC_ID_KEY                       = 1;

  DBTSTSUBFile                       = 'tstsubst';
  DBTSTSUBNCCLS                      = 1;
  DBTSTSUBMIC                        = 2;
  DBTSTSUBBiotype                    = 3;
  DBTSTSUBTest                       = 4;
  DBTSTSUBFlags                      = 5;

  DBRPTFmtsFile                      = 'RPTFMTS';
  DBRPTFmtName                       = 1;
  DBRPTFmtVersion                    = 2;
  DBRPTFmtSize                       = 3;
  DBRptFmtMargins                    = 4;
  DBRptFmtRptPerPage                 = 5;
  DBRptFmtOrient                     = 6;
  DBRptFmtPaperIdx                   = 7;
  DBRptFmtSpacing                    = 8;
  DBRptFmtArchive                    = 9;
  DBRptFmtFlags                      = 10;

  DBRPTFMTS_ReportName_KEY           = 1;

  DBRPTSectsFile                     = 'RPTSECTS';
  DBRptSectReportRef                 = 1;
  DBRptSectSectRef                   = 2;
  DBRptSectSectID                    = 3;
  DBRptSectSectFlags                 = 4;

  DBRPTSECTS_ReportSeq_KEY           = 1;

  DBRPTDemoSectsFile                 = 'DEMOSECT';
  DBRptDemoSectFont                  = 1;
  DBRptDemoSectOptions               = 2;
  DBRptDemoSectSize                  = 3;
  DBRptDemoSectFlags                 = 4;

  DBRPTDemoFldsFile                  = 'DEMOFLDS';
  DBRptDemoFldsSectRef               = 1;
  DBRptDemoFldsFldType               = 2;
  DBRptDemoFldsBuf                   = 3;
  DBRptDemoFldsFileName              = 4;
  DBRptDemoFldsFldNum                = 5;
  DBRptDemoFldsOrg                   = 6;
  DBRptDemoFldsExt                   = 7;
  DBRptDemoFldsOpts                  = 8;
  DBRptDemoFldsFlags                 = 9;

  DBRPTDEMOFLDS_SectionSeq_KEY       = 1;

  DBRPTGSSectsFile                   = 'GSSECT';
  DBRptGSSectFont                    = 1;
  DBRptGSSectOptions                 = 2;
  DBRptGSSectSize                    = 3;
  DBRptGSSectFlags                   = 4;

  DBRPTGSFldsFile                    = 'GSFLDS';
  DBRptGSFldsSectRef                 = 1;
  DBRptGSFldsFldType                 = 2;
  DBRptGSFldsBuf                     = 3;
  DBRptGSFldsSlotNum                 = 4;
  DBRptGSFldsOrg                     = 5;
  DBRptGSFldsExt                     = 6;
  DBRptGSFldsOpts                    = 7;
  DBRptGSFldsFlags                   = 8;

  DBRPTGSFLDS_SectionSeq_KEY         = 1;

  DBRptIsoCmpFile                    = 'ICMPSECT';
  DBRptIsoCompSep                    = 1;
  DBRptIsoCompHeaders                = 2;
  DBRptIsoCompULHeader               = 3;
  DBRptIsoCompInterps                = 4;
  DBRptIsoCompMICs                   = 5;
  DBRptIsoCompIsoPerSect             = 6;
  DBRptIsoCompIsoWidth               = 7;
  DBRptIsoCompMicWidth               = 8;
  DBRptIsoCompInterpWidth            = 9;
  DBRptIsoCompMICOrder               = 10;
  DBRptIsoCompMICSpace               = 11;
  DBRptIsoCompDrugDisp               = 12;
  DBRptIsoCompDrugWidth              = 13;
  DBRptIsoCompFont                   = 14;
  DBRptIsoCompOptions                = 15;
  DBRptIsoCompSize                   = 16;
  DBRptIsoCompFlags                  = 17;

  DBRptIsoSumFile                    = 'ISUMSECT';
  DBRptIsoSumSeparators              = 1;
  DBRptIsoSumHeaders                 = 2;
  DBRptIsoSumULHeader                = 3;
  DBRptIsoSumIsoFlds                 = 4;
  DBRptIsoSumFldWidth                = 5;
  DBRptIsoSumFont                    = 6;
  DBRptIsoSumOptions                 = 7;
  DBRptIsoSumSize                    = 8;
  DBRptIsoSumFlags                   = 9;

  DBRptIsoFulFile                    = 'IFULSECT';
  DBRptIsoFullIsoPerSect             = 1;
  DBRptIsoFullSeparators             = 2;
  DBRptIsoFullIsoWidth               = 3;
  DBRptIsoFullDetail                 = 4;
  DBRptIsoFullIsoFlds                = 5;
  DBRptIsoFullLblWidth               = 6;
  DBRptIsoFullValWidth               = 7;
  DBRptIsoFullSubSpace               = 8;
  DBRptIsoFullFont                   = 9;
  DBRptIsoFullOptions                = 10;
  DBRptIsoFullSize                   = 11;
  DBRptIsoFullFlags                  = 12;

  DBDRUGSELNFile                     = 'DRUGSELN';
  DBDRUGSELNID                       = 1;
  DBDRUGSELNDesc                     = 2;
  DBDRUGSELNFlags                    = 3;

  DBDRUGSELN_ID_KEY                  = 1;

  DBDRUGSELXRFFile                   = 'DRUGSELX';
  DBDRUGSELXRFSelRef                 = 1;
  DBDRUGSELXRFDrugRef                = 2;
  DBDRUGSELXRFFlags                  = 3;

  DBDRUGSELXRF_ID_KEY                = 1;

  DBPATIENT_SPECIMEN_ASSOC           = 1;
  DBSPECIMEN_PATIENT_ASSOC           = 1;
  DBSPECIMEN_SOURCE_ASSOC            = 2;
  DBSPECIMEN_WARD_ASSOC              = 3;
  DBSPECIMEN_SPECSTAT_ASSOC          = 4;
  DBSPECIMEN_ISOLATE_ASSOC           = 5;
  DBSPECIMEN_GRMSTAIN_ASSOC          = 6;
  DBISOLATE_SPECIMEN_ASSOC           = 1;
  DBISOLATE_ORGANISM_ASSOC           = 2;
  DBISOLATE_ISORDER_ASSOC            = 3;
  DBISOLATE_ORDER_ASSOC              = 4;
  DBISOLATE_SESSION_ASSOC            = 5;
  DBGRMSTAIN_SPECIMEN_ASSOC          = 1;
  DBGRMSTAIN_GSDEF_ASSOC             = 2;
  DBGRMSTAIN_GSMNEM_ASSOC            = 3;
  DBISORDER_ISOLATE_ASSOC            = 1;
  DBISORDER_TESTGRP_ASSOC            = 2;
  DBISORDER_RESULT_ASSOC             = 3;
  DBISORDER_WAEXLIST_ASSOC           = 4;
  DBISORDER_WAPANEL_ASSOC            = 5;
  DBRESULT_ISORDER_ASSOC             = 1;
  DBTESTGRP_TSTGPXRF_ASSOC           = 1;
  DBTSTGPXRF_TESTGRP_ASSOC           = 1;
  DBTSTGPXRF_TEST_ASSOC              = 2;
  DBTEST_DRUG_ASSOC                  = 1;
  DBTEST_TESTCAT_ASSOC               = 2;
  DBTEST_TSTGPXRF_ASSOC              = 3;
  DBORDER_ORDERXRF_ASSOC             = 1;
  DBORDERXRF_ORDER_ASSOC             = 1;
  DBORDERXRF_TESTGRP_ASSOC           = 2;
  DBMPSSRULE_MPSSCOND_ASSOC          = 1;
  DBMPSSCOND_MPSSRULE_ASSOC          = 1;
  DBTSTSUBST_INTRPMAP_ASSOC          = 1;
  DBTSTSUBST_MPSSMIC_ASSOC           = 2;
  DBWAPANEL_ISORDER_ASSOC            = 1;
  DBWAEXLIST_ISORDER_ASSOC           = 1;
  DBDRUGSELN_DRUGSELX_ASSOC          = 1;
  DBDRUGSELX_DRUGSELN_ASSOC          = 1;
  DBDRUGSELX_DRUG_ASSOC              = 2;

  DBMnemAbbr                         = 1;
  DBMnemDesc                         = 2;

IMPLEMENTATION

END.
