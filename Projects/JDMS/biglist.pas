unit BigList;

{ Description:

  This unit provides a 'Large' collection that goes beyond the 16,380
  item limit imposed by the OWL TCollections.

  BigList collections allow up to Maxlongint items per collection.

  BigList provides 'TLCollection', 'TLSortedCollection' and a
  'TLStrCollection' all of which are functionally identical to their
  OWL equivalents.

  Some validation routines have beed added to the methods that process
  collection element numbers, so that it is no longer possible to
  crash an app when using invalid element numbers.

}

INTERFACE

uses
  Objects,
  Strings,
  WinTypes,
  WinProcs;

const
  MaxLCollectionSize = Maxlongint div sizeof(pointer);

  { Error codes }

  LCE_OK         =  0;   { status OK }
  LCE_IDXLTZERO  = -1;   { index parameter value less than zero }
  LCE_IDXGTCOUNT = -2;   { index parameter value greater than count }
  LCE_MEMHFAIL   = -3;   { GlobalAlloc memory request failed }
  LCE_EMPTY      = -4;   { trying to put or delete an item in an empty collection }
  LCE_ATPUTIDX   = -5;   { trying to AtPut beyond count }

type
  PLCollection = ^TLCollection;
  TLCollection = object(TObject)
    memH      : THandle;
    address   : pointer;
    count     : longint;
    limit     : longint;
    delta     : longint;
    status    : longint;
    errorInfo : longint;
    lastInsert: longint;  {- index of last inserted item
                             (-1 if invalid or nothing inserted }
    constructor Init(alimit, aDelta: longint);
    constructor Load(var S: TStream);
    destructor done; virtual;
    function At(index: longint): pointer;
    procedure AtDelete(index: longint);
    procedure AtFree(index: longint);
    procedure AtInsert(index: longint; item: pointer);
    procedure AtPut(index: longint; item: pointer);
    procedure Delete(item: pointer);
    procedure DeleteAll;
    procedure Error(code, info: longint); virtual;
    function FirstThat(test: pointer): pointer;
    procedure ForEach(action: pointer);
    procedure Free(item: pointer);
    procedure FreeAll;
    procedure FreeItem(item: pointer); virtual;
    function Getitem(var s: TStream): pointer; virtual;
    function IndexOf(item: pointer): longint; virtual;
    procedure Insert(item: pointer); virtual;
    function LastThat(Test: pointer): pointer;
    function MemPtr(Idx: longint) : pointer;
    procedure Pack;
    procedure Putitem(var s: TStream; item: pointer); virtual;
    function Setlimit(alimit: longint) : boolean; virtual;
    procedure Store(var s: TStream);
    {}
    private
    function ValidIdx(idx: longint) : boolean;
  end;

   PLSortedCollection = ^TLSortedCollection;
   TLSortedCollection = object(TLCollection)
      duplicates: boolean;
      constructor Init(alimit, aDelta: longint);
      constructor Load(var s: TStream);
      function Compare(key1, key2: pointer): integer; virtual;
      function indexOf(item: pointer): longint; virtual;
      procedure Insert(item: pointer); virtual;
      function KeyOf(item: pointer): pointer; virtual;
      function Search(key: pointer; var index: longint): boolean; virtual;
      procedure Store(var S: TStream);
   end;

   PLStrCollection = ^TLStrCollection;
   TLStrCollection = object(TLSortedCollection)
      function Compare(key1, key2: pointer): integer; virtual;
      procedure Freeitem(item: pointer); virtual;
      function Getitem(var s: TStream): pointer; virtual;
      procedure Putitem(var s: TStream; item: pointer); virtual;
   end;

IMPLEMENTATION

uses
  DMSDebug;

const
   AHi : word = 8;
   AHs : byte = 3;

{- private procs/funcs }

procedure AHIncr;  far; external 'KERNEL' index 114; {- magic functions }
procedure AHShift; far; external 'KERNEL' index 113;

function MaxFit (size: word; anOffset: word): word;
{- return maximum number of bytes that fit in a Segment}
Inline(
  $5B/                   {  pop bx       ; anOffset}
  $58/                   {  pop ax       ; Size}
  $01/$C3/               {  add bx,ax}
  $73/$02/               {  jnc @@1}
  $29/$D8);              {  sub ax,bx    ; $1.0000-anOffset}
                         {@@1:           ; ax = Min(Size, $1.0000 - anOffset)}

function MaxChunk(size: longint): word;
{- return maximum number of bytes that can be transferred in one block using
   conventional functions }
Inline(
  $5B/                   {  pop bx       ; word(Size)}
  $5A/                   {  pop dx       ; word(Size+2)}
  $B8/$FF/$FF/           {  mov ax,$FFFF}
  $09/$D2/               {  or dx,dx     ; dx = 0 ?}
  $75/$02/               {  jne @@1}
  $89/$D8);              {  mov ax,bx}
                         {@@1:           ; ax = Min($FFFF, Size)}

function LIncPtr(aPtr: pointer; anOffset: longint): pointer; Assembler;
asm
  mov dx, word(anOffset+2)
  mov ax, word(anOffset)
  mov cx, offset AHShift
  shl dx,cl
  add dx, word(aPtr+2)
  add ax, word(aPtr)
  jnc @@1
  add Dx,Offset AHincr
  @@1:
end;

function LDecPtr(aPtr: pointer; anOffset: longint): pointer; Assembler;
asm
  mov bx,word(anOffset+2)
  mov cx,offset AHShift
  shl bx,cl
  mov dx,word(aPtr+2)
  mov ax,word(aPtr)
  sub dx,bx
  sub ax,word(anOffset)
  jnc @@1
  sub dx,offset AHincr
  @@1:
end;

function IncPtrMac(aPtr: pointer; anOffset: word): pointer;
Inline(
  $5B/                   {  pop bx    ; anOffset}
  $58/                   {  pop ax    ; word(aPtr)}
  $5A/                   {  pop dx    ; word(aPtr+2)}
  $01/$D8/               {  add ax,bx}
  $73/$04/               {  jnc @@1}
  $03/$16/>AHI);         {  add dx,[>AHi]}
                         {@@1:}

function DecPtrMac(aPtr: pointer; anOffset: word): pointer;
Inline(
  $5B/                   {  Pop Bx    ; anOffset}
  $58/                   {  Pop Ax    ; word(aPtr)}
  $5A/                   {  Pop Dx    ; word(aPtr+2)}
  $29/$D8/               {  Sub Ax,Bx}
  $73/$04/               {  Jnc @@1}
  $2B/$16/>AHI);         {  Sub Dx,[>AHi]}
                         {@@1:}

function PtrDiff (Ptr1,Ptr2: pointer): longint; Assembler;
asm
  mov dx,word(Ptr1+2)
  mov bx,word(Ptr2+2)
  mov cx,offset AHshift
  shr dx,cl
  shr bx,cl
  mov ax,word(Ptr1)
  sub ax,word(Ptr2)
  sbb dx,bx
  jnc @@1
  neg ax
  adc dx,0
  neg dx
  @@1:
end;

procedure HMove(srcPtr, dstptr: pointer; size: longint);
var
  count : word;
  s     : longint;

  function Min (a,b: word): word;
  begin
    if a > b then
      Min:= b
    else
      Min:= a;
  end;

begin
  s:= Size;
  if longint(dstptr) > longint(srcPtr) then
  begin  {Shift up}
    srcPtr:= LIncPtr(srcPtr, size - 1);
    dstptr:= LIncPtr(dstptr, size - 1);
    while size > 0 do
    begin
      count:= Min(Min(MaxChunk(Size), MaxChunk(longint(word(srcPtr))+1)),
                                      MaxChunk(longint(word(dstptr))+1));
      srcPtr:= DecPtrMac(srcPtr, count-1);
      dstptr:= DecPtrMac(dstptr, count-1);
      Move(srcPtr^, dstptr^, count);
      srcPtr:= DecPtrMac(srcPtr, 1);
      dstptr:= DecPtrMac(dstptr, 1);
      Dec(size, count);
    end;
  end
  else {shift down}
  begin
    while size > 0 do
    begin
      count:= MaxFit( MaxFit(
                      MaxChunk(Size), word(srcPtr)), word(dstptr));
      Move(srcPtr^, dstptr^, count);
      srcPtr:= IncPtrMac(srcPtr, count);
      dstptr:= IncPtrMac(dstptr, count);
      Dec(size, count)
    end;
  end;
end;

{------------------------------------------------[ TLCollection ]--}

constructor TLCollection.Init(alimit, aDelta: longint);
begin
  inherited Init;
  memH:= 0;
  address:= nil;
  count:= 0;
  limit:= 0;
  status:= LCE_OK;
  errorInfo:= 0;
  delta:= aDelta;
  lastInsert:= -1;
  if not Setlimit(alimit) then
    Fail;
end;

constructor TLCollection.Load(var S: TStream);
var
  i  : longint;
begin
  {- read/set object's data variables }
  S.Read(count, sizeof(count));
  S.Read(limit, sizeof(limit));
  S.Read(delta, sizeof(delta));
  memH:= 0;
  Address:= nil;
  Status:= LCE_OK;
  ErrorInfo:= 0;
  { get memory for collection }
  if not Setlimit(limit) then
    Fail
  else
  begin
    for i:= 0 to (count - 1) do
      AtPut(I, GetItem(S));
  end;
end;

destructor TLCollection.Done;
begin
  inherited Done;
  FreeAll;
  Setlimit(0);
end;

function TLCollection.At(index: longint): pointer;
var
  sPtr, tptr : pointer;
begin
  if ValidIdx(index) then
  begin
    sPtr:= MemPtr(index);
    Move(sPtr^, tptr, sizeof(pointer));
    At:= tptr;
  end
  else
    At:= nil;
end;

procedure TLCollection.AtDelete(index: longint);
var
   sPtr, tptr : pointer;
begin
  if count = 0 then
    Error(LCE_EMPTY, index)
  else
  begin
    if ValidIdx(index) then
    begin
      {- if its not the last item in the list then }
      if index <> (count - 1) then
      begin
        {- shift all higher items back one place }
        tptr:= MemPtr(index);
        sPtr:= MemPtr(index+1);
        HMove(sPtr, tptr, (count - 1 - index) * sizeof(pointer));
      end;
      {- set the last element to zero }
      sPtr:= MemPtr(count-1);
      FillChar(sPtr^, sizeof(pointer), 0);
      Dec(count);
    end;
  end;
end;

procedure TLCollection.AtFree(index: longint);
var
   item: pointer;
begin
  item:= At(index);
  AtDelete(index);
  FreeItem(item);
end;

procedure TLCollection.AtInsert(index: longint; item: pointer);
var
  S    : boolean;
  sPtr : pointer;
  tptr : pointer;
begin
  if ValidIdx(index) then
  begin
    { is the collection full? If so then increase size by delta }
    if count = limit then
      s:= Setlimit(limit+Delta)
    else
      s:= true;
    if not s then
      Error(LCE_MEMHFAIL, limit+Delta)
    else
    begin
      {- is it being inserted somewhere in the middle of the collection? }
      if index <= (count-1) then
      begin
        {- shift the existing ptrs up one notch }
        sPtr:= MemPtr(index);
        tptr:= MemPtr(index+1);
        hMove(sPtr, tptr, (count-index) * sizeof(pointer));
      end;
      {- move item into the 'index' position }
      tptr:= MemPtr(index);
      Move(item, tptr^, sizeof(pointer));
      Inc(count);
    end;
  end;
end;

procedure TLCollection.AtPut(index: longint; item: pointer);
var
   tptr : pointer;
begin
  {- empty list? }
  if count = 0 then
    Error(LCE_EMPTY, index)
  else
  begin
    { check index against list range? }
    if index > count then
      Error(LCE_IDXGTCOUNT, index)
    else
    begin
      { valid index? }
      if ValidIdx(index) then
      begin
        {- overwrite current idx pointer with new item }
        tptr:= MemPtr(index);
        Move(item, tptr^, sizeof(pointer));
      end;
    end;
  end;
end;

procedure TLCollection.Delete(item: pointer);
begin
  AtDelete(IndexOf(item));
end;

procedure TLCollection.DeleteAll;
begin
  count:= 0;
end;

procedure TLCollection.Error(code, info: longint);
begin
  status:= code;
  errorInfo:= info;
end;

function TLCollection.FirstThat(test: pointer): pointer;
var
  item  : pointer;
  found : boolean;
  c     : longint;
begin
  found:= false;
  c:= 0;
  while (not found) and (c < count) do
  begin
    item:= At(C);
    asm
      { push the item address }
      push item.word[2]
      push item.word[0]
      {$IFDEF Windows }
       mov ax, [bp]
       and al, 0FEH
       push ax
      {$else}
       push word ptr [bp]
      {$ENDIF}
      Call Test
      mov Found,al
    end;
    INC(C);
  end;
  if Found then
    FirstThat:= item
  else
    FirstThat:= nil
end;

procedure TLCollection.ForEach(Action: pointer);
var
   item : pointer;
   Idx : longint;
begin
   Idx:= 0;
   while Idx < count do
   begin
      item:= At(Idx);
      ASM
         { push the item address }
         push word ptr item.word[2]
         push word ptr item.word[0]
         {$IFDEF Windows }
            mov ax, [bp]
            and al, 0FEH
            push ax
         {$else}
            push word ptr [bp]
         {$ENDIF}
         call Action
      end;
      INC(Idx);
   end;
end;

procedure TLCollection.Free(item: pointer);
begin
   Delete(item);
   Freeitem(item);
end;

procedure TLCollection.FreeAll;
var
   I : longint;
   APtr : pointer;
begin
   for I:= 0 to (count - 1) do
   begin
      ErrorInfo:= I;
      APtr:= At(I);
      if APtr <> nil then
         Freeitem(At(I));
   end;
   count:= 0;
end;

procedure TLCollection.Freeitem(item: pointer);
begin
  if item <> nil then
  begin
    Dispose(PObject(item),Done);
    item:= nil;
  end;
end;

function TLCollection.Getitem(var S: TStream): pointer;
begin
   Getitem:= S.Get;
end;

function TLCollection.indexOf(item: pointer): longint;
var
   C : longint;
begin
   C:= 0;
   while (item <> At(C)) AND (C < count) do
      INC(C);
   if C >= count then
      indexOf:= -1
   else
      indexOf:= C
end;

procedure TLCollection.Insert(item: pointer);
begin
   lastInsert:= count;
   AtInsert(count, item);
end;

function TLCollection.LastThat(Test: pointer): pointer;
var
   item   : pointer;
   Found  : boolean;
   k      : longint;
begin
   Found:= FALSE;
   k:= count - 1;
   while (not found) and (k >= 0) do
   begin
      item:= At(k);
      ASM
         { push the item address }
         push item.word[2]
         push item.word[0]
         {$IFDEF Windows }
            mov ax, [bp]
            and al, 0FEH
            push ax
         {$else}
            push word ptr [bp]
         {$ENDIF}
         call Test
         mov found, al
      end;
      Dec(k);
   end;
   if found then
     LastThat:= item
   else
     LastThat:= nil
end;

procedure TLCollection.Pack;
var
  item : pointer;
  k    : longint;
begin
  k:= count - 1;
  while k >= 0 do
  begin
    item:= At(k);
    if item = nil then
      AtDelete(k);
    Dec(k);
  end;
end;

procedure TLCollection.PutItem(var S: TStream; item: pointer);
begin
   S.Put(item);
end;

function TLCollection.SetLimit(aLimit: longint): boolean;
var
  newMemH     : THandle;
  newAddress  : pointer;
  ok          : boolean;
begin
  ok:= true;
  if aLimit <> limit then
  begin
    if Alimit = 0 then
    begin
      GlobalUnlock(memH);
      newMemH:= GlobalFree(memH);
      newAddress:= nil;
    end
    else
    begin
      if memH = 0 then
        NewmemH:= GlobalAlloc(GMEM_MOVEABLE or GMEM_ZEROINIT,
                              sizeof(pointer) * aLimit)

      else
      begin
        GlobalUnlock(memH);
        newMemH:= GlobalReAlloc(memH, sizeof(pointer) * aLimit,
                                GMEM_MOVEABLE or GMEM_ZEROINIT)
      end;
      if newMemH = 0 then
        ok:= false
      else
        newAddress:= GlobalLock(newMemH);
    end;
    if ok then
    begin
      memH:= NewmemH;
      address:= newAddress;
      limit:= aLimit;
    end;
  end;
  SetLimit:= ok;
end;

procedure TLCollection.Store(var S: TStream);

  procedure doPutitem(P: pointer); FAR;
  begin
    PutItem(S,P);
  end;

begin
  S.Write(count,sizeof(count));
  S.Write(limit,sizeof(limit));
  S.Write(Delta,sizeof(Delta));
  ForEach(@DoPutitem);
end;

{- Private methods }

function TLCollection.MemPtr(idx: longint): pointer;
begin
  MemPtr:= LIncPtr(address, idx * sizeof(pointer))
end;

function TLCollection.ValidIdx(idx: longint): boolean;
begin
  status:= LCE_OK;
  if idx < 0 then
    Error(LCE_IDXLTZERO, idx)
  else
  begin
    if Idx > count then
      Error(LCE_IDXGTCOUNT, idx)
  end;
  ValidIdx:= (status = LCE_OK);
end;

{---------------------------------------------[ TLSortedCollection ]--}

constructor TLSortedCollection.Init(aLimit, aDelta: longint);
begin
  inherited Init(aLimit, aDelta);
  duplicates:= false;
end;

constructor TLSortedCollection.Load(var S: TStream);
begin
  inherited Load(S);
  S.Read(Duplicates,sizeof(boolean));
end;

function TLSortedCollection.Compare(key1, key2: pointer): integer;
begin
  Abstract;
end;

function TLSortedCollection.IndexOf(item: pointer): longint;
var
  k : longint;
begin
  indexOf:= -1;
  if Search(KeyOf(item), k) then
  begin
    if duplicates then
      while (k < count) and (item <> At(k)) do
        Inc(k);
    if k < count then
      IndexOf:= k;
  end;
end;

procedure TLSortedCollection.Insert(item: pointer);
var
  k : longint;
begin
  if not Search(KeyOf(item), k) or duplicates then
  begin
    AtInsert(k, item);
    lastInsert:= k;
  end
  else
  begin
    ErrorInfo:= k;
    lastInsert:= -1;
  end;
end;

function TLSortedCollection.KeyOf(item: pointer): pointer;
begin
  KeyOf:= item;
end;

function TLSortedCollection.Search(Key: pointer; var index: longint): boolean;
var
  l, h, i : longint;
  c       : integer;
begin
  Search:= false;
  l:= 0;
  h:= count - 1;
  while l <= h do
  begin
    i:= (l + h) shr 1;
    ErrorInfo:= i;
    c:= Compare(KeyOf(At(i)), key);
    if c < 0 then
      l:= i + 1
    else
    begin
      h:= i - 1;
      if c = 0 then
      begin
        Search:= true;
        if not duplicates then
          l:= i;
      end;
    end;
  end;
  index:= l;
end;

procedure TLSortedCollection.Store(var S: TStream);
begin
  inherited Store(S);
  S.Write(duplicates, sizeof(boolean));
end;

{------------------------------------------[ TLStrCollection ]--}

function TLStrCollection.Compare(key1, key2: pointer): integer;
begin
  Compare:= StrComp(key1, key2);
end;

procedure TLStrCollection.Freeitem(item: pointer);
begin
  if (item <> nil) then
  begin
    StrDispose(item);
    item:= nil;
  end;
end;

function TLStrCollection.Getitem(var S: TStream): pointer;
begin
  Getitem:= S.StrRead;
end;

procedure TLStrCollection.Putitem(var S: TStream; item: pointer);
begin
  S.StrWrite(item);
end;

END.
