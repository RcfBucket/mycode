{----------------------------------------------------------------------------}
{  Module Name  : RANDDATA.PAS                                               }
{  Programmer   : EJ                                                         }
{  Date Created : 01/16/95                                                   }
{                                                                            }
{  Purpose -                                                                 }
{  This module is used to create data for Nihongo DMS database files.        }
{                                                                            }
{  Assumptions -                                                             }
{  None.                                                                     }
{                                                                            }
{  Initialization -                                                          }
{  None.                                                                     }
{                                                                            }
{  Revision History -                                                        }
{                                                                            }
{  Version  Date      Who    Did What                                        }
{  -------  --------  ------ ----------------------------------------------- }
{  1.00     01/16/94  EJ     Initial release                                 }
{                                                                            }
{----------------------------------------------------------------------------}

program RandData;

uses
  OWindows,
  ODialogs,
  WinTypes,
  WinProcs,
  Strings,
  StrsW,
  INILib,
  Ctl3d,
  CtlLib,
  DlgLib,
  Status,
  APITools,
  IntlLib,
  DateLib,
  MScan,
  DMString,
  DBErrors,
  DBTypes,
  DBIDs,
  DBLib,
  DBFile,
  ResLib,
  Trays,
  IntrpMap,
  DMSDebug;

{$I RandData.INC}
{$R RandData.RES}

const
  AppName: PChar  = 'RandData';

type
  TRandDataApp = object(TApplication)
    procedure InitMainWindow; virtual;
  end;

  PCancelDlg = ^TCancelDlg;
  TCancelDlg = object(TGenStatDlg)
    constructor Init(aParent: PWindowsObject; aName: PChar);
    destructor Done; virtual;
    procedure SetupWindow; virtual;
  end;

  PMainDlg = ^TMainDlg;
  TMainDlg = object(TCenterDlgWindow)
    hour        : PWaitCursor;
    patDB,
    spcDB,
    isoDB,
    gstDB,
    isordDB,
    srcDB,
    wrdDB,
    sessDB,
    orgDB,
    ordDB,
    ordefDB,
    toDB,
    gsdefDB,
    gmnDB       : PDBFile;
    statDlg     : PCancelDlg;
    resultList  : PResults;
    numSess,
    numSpcs,
    numPats,
    numIsos,
    numOrgs,
    numOrds,
    numSrcs     : longint;
    isoVar,
    dateVar     : longint;
    {}
    constructor Init;
    destructor Done; virtual;
    function  GetClassName: PChar; virtual;
    procedure GetWindowClass(var aWndClass: TWndClass); virtual;
    procedure SetupWindow; virtual;
    procedure OpenFiles;
    procedure Process(var Msg: TMessage); virtual id_First + IDC_PROCESS;
    procedure GetValues;
    procedure ShowStat(id: integer; num: longint);
    procedure DoPrep;
    procedure DoMnemonics;
    procedure DoSession;
    procedure DoPatient;
    procedure DoSpecimen;
    procedure JNGram;
    procedure DoGramStain;
    procedure DoIsolate;
    procedure DoIsorder;
    procedure DoResult;
    function  RandOrg: longint;
    function  RandOrder: longint;
    procedure IsoSetFamily(org: longint);
    procedure ResultVal(resRec: PResRecObj);
    procedure RandMIC(resRec: PResRecObj);
    procedure RandInterp(rstr: PChar);
    procedure RandBIO(rstr: PChar);
    procedure RandPlusMinus(rstr: PChar);
  end;


var
  mainApp    : TRandDataApp;
  intlDate   : TIntlDate;
  ioRet      : integer;
  eStr       : array [0..255] of char;


{--------------------------------------------------------------------}

procedure CheckDB(msg: PChar; dbd: PDBFile);
var
  pstr   : array [0..255] of char;
begin
  if dbd = nil then
  begin
    StrCopy(pstr, 'RandData DB Error: ');
    StrLCat(pstr, msg, 255);
    ErrorMsg(0, pstr, dbErrorStr(dbLastOpenError, eStr, 255));
    Halt;
  end;
end;


{---------------------------------------------[ TCancelDlg ]--}
constructor TCancelDlg.Init(aParent: PWindowsObject; aName: PChar);
var
  c   : PControl;
begin
  inherited Init(aParent, aName);
  c := New(PLStatic, InitResource(@self, IDC_LBL1, 0, FALSE));
  c := New(PLStatic, InitResource(@self, IDC_LBL2, 0, FALSE));
  c := New(PLStatic, InitResource(@self, IDC_LBL3, 0, FALSE));
  c := New(PLStatic, InitResource(@self, IDC_LBL4, 0, FALSE));
  c := New(PLStatic, InitResource(@self, IDC_LBL5, 0, FALSE));
end;

destructor TCancelDlg.Done;
begin
  EnableWindow(parent^.HWindow, TRUE);
  SetFocus(parent^.HWindow);
  inherited Done;
end;

procedure TCancelDlg.SetupWindow;
begin
  inherited SetupWindow;
  EnableWindow(parent^.HWindow, FALSE);
end;


{--------------------------------------------------------------------}

procedure TRandDataApp.InitMainWindow;
begin
  if INIUse3D then
    Register3dApp(TRUE, TRUE, TRUE);
  mainWindow := New(PMainDlg, Init);
  if mainWindow = nil then
  begin
    FatalError('Error', 'Cannot initialize main window.');
    Halt;
  end;
end;


constructor TMainDlg.Init;
var
  c   : PControl;
begin
  inherited Init(nil, MakeIntResource(DLG_MAIN));
  c := New(PLStatic, InitResource(@self, IDC_LBL1, 0, FALSE));
  c := New(PLStatic, InitResource(@self, IDC_LBL2, 0, FALSE));
  c := New(PLStatic, InitResource(@self, IDC_LBL3, 0, FALSE));
  c := New(PLStatic, InitResource(@self, IDC_LBL4, 0, FALSE));
  c := New(PLStatic, InitResource(@self, IDC_LBL5, 0, FALSE));
  c := New(PLStatic, InitResource(@self, IDC_LBL6, 0, FALSE));
end;

destructor TMainDlg.Done;
begin
  MSDisposeObj(resultList);
  MSDisposeObj(patDB);
  MSDisposeObj(spcDB);
  MSDisposeObj(isoDB);
  MSDisposeObj(gstDB);
  MSDisposeObj(isordDB);
  MSDisposeObj(srcDB);
  MSDisposeObj(wrdDB);
  MSDisposeObj(sessDB);
  MSDisposeObj(orgDB);
  MSDisposeObj(ordDB);
  MSDisposeObj(ordefDB);
  MSDisposeObj(toDB);
  MSDisposeObj(gsdefDB);
  MSDisposeObj(gmnDB);

  inherited Done;
end;

function TMainDlg.GetClassName: PChar;
begin
  GetClassName := AppName;
end;

procedure TMainDlg.GetWindowClass(var aWndClass: TWndClass);
begin
  inherited GetWindowClass(aWndClass);
  aWndClass.HIcon := LoadIcon(HInstance, MakeIntResource(ICON_1));
end;

procedure TMainDlg.SetupWindow;
var
  tempStr   : array [0..255] of char;
begin
  inherited SetupWindow;

  OpenFiles;
  resultList := New(PResults, Init);     {handle the results part}
  if resultList = nil then
  begin
    ErrorMsg(0, AppName, 'Could not create result list object!');
    Halt;
  end;
  if resultList^.traysObj = nil then
  begin
    ErrorMsg(0, AppName, 'Could not get a trays object!');
    Halt;
  end;
  numSrcs := srcDB^.dbc^.NumRecords;
  numSess := sessDB^.dbc^.NumRecords;
  numPats := patDB^.dbc^.NumRecords;
  numSpcs := spcDB^.dbc^.NumRecords;
  isoVar  := 3;
  dateVar := 30;

  Str(numSrcs, tempStr);
  SetDlgItemText(HWindow, IDC_SOURCE, tempStr);

  Str(numSess, tempStr);
  SetDlgItemText(HWindow, IDC_SESSION, tempStr);

  Str(numPats, tempStr);
  SetDlgItemText(HWindow, IDC_PATIENT, tempStr);

  Str(numSpcs, tempStr);
  SetDlgItemText(HWindow, IDC_SPECIMEN, tempStr);

  Str(isoVar, tempStr);
  SetDlgItemText(HWindow, IDC_ISOLATE, tempStr);
  SendDlgItemMsg(IDC_ISOLATE, EM_LIMITTEXT, 2, 0);

  Str(dateVar, tempStr);
  SetDlgItemText(HWindow, IDC_DATEVAR, tempStr);

  Randomize;
end;

procedure TMainDlg.OpenFiles;
begin
  patDB   := New(PDBFile, Init(DBPATFile, '', dbOpenNormal));
  spcDB   := New(PDBFile, Init(DBSPECFile, '', dbOpenNormal));
  isoDB   := New(PDBFile, Init(DBISOFile, '', dbOpenNormal));
  gstDB   := New(PDBFile, Init(DBGSFile, '', dbOpenNormal));
  isordDB := New(PDBFile, Init(DBISOORDFile, '', dbOpenNormal));
  srcDB   := New(PDBFile, Init(DBSRCFile, '', dbOpenNormal));
  wrdDB   := New(PDBFile, Init(DBWARDFile, '', dbOpenNormal));
  sessDB  := New(PDBFile, Init(DBSESSFile, '', dbOpenNormal));
  orgDB   := New(PDBFile, Init(DBORGFile, '', dbOpenNormal));
  ordDB   := New(PDBFile, Init(DBORDFile, '', dbOpenNormal));
  ordefDB := New(PDBFile, Init(DBORDXRFFile, '', dbOpenNormal));
  toDB    := New(PDBFile, Init(DBTSTGRPFile, '', dbOpenNormal));
  gsdefDB := New(PDBFile, Init(DBGSDEFFile, '', dbOpenNormal));
  gmnDB   := New(PDBFile, Init(DBGSMNEMFile, '', dbOpenNormal));
  CheckDB('Opening PATIENT file', patDB);
  CheckDB('Opening SPECIMEN file', spcDB);
  CheckDB('Opening ISOLATE file', isoDB);
  CheckDB('Opening GRMSTAIN file', gstDB);
  CheckDB('Opening ISORDER file', isordDB);
  CheckDB('Opening SOURCE file', srcDB);
  CheckDB('Opening WARD file', wrdDB);
  CheckDB('Opening SESSION file', sessDB);
  CheckDB('Opening ORGANISM file', orgDB);
  CheckDB('Opening ORDER file', ordDB);
  CheckDB('Opening ORDERXRF file', ordefDB);
  CheckDB('Opening TESTGRP file', toDB);
  CheckDB('Opening GSDEF file', gsdefDB);
  CheckDB('Opening GRAMNEM file', gmnDB);
end;

procedure TMainDlg.Process(var msg: TMessage);
begin
  hour:= New(PWaitCursor, Init);
  GetValues;
  statDlg := New(PCancelDlg, Init(@Self, MakeIntResource(DLG_STATUS)));
  Application^.MakeWindow(statDlg);
  if statDlg^.CanContinue then
    DoPrep;
  if statDlg^.CanContinue then
    DoMnemonics;
  if statDlg^.CanContinue then
    JNGram;
  if statDlg^.CanContinue then
    DoSession;
  if statDlg^.CanContinue then
    DoPatient;
  if statDlg^.CanContinue then
    DoSpecimen;

  statDlg^.SetText('&OK', IDCANCEL);
  while statDlg^.CanContinue do ;  {- wait for user to press OK }
  MSDisposeObj(statDlg);
  MSDisposeObj(hour);
end;

procedure TMainDlg.GetValues;
var
  tempStr   : array [0..255] of char;
  err       : integer;
begin
  GetDlgItemText(HWindow, IDC_SOURCE, tempStr, 255);
  Val(tempStr, numSrcs, err);

  GetDlgItemText(HWindow, IDC_SESSION, tempStr, 255);
  Val(tempStr, numSess, err);

  GetDlgItemText(HWindow, IDC_PATIENT, tempStr, 255);
  Val(tempStr, numPats, err);

  GetDlgItemText(HWindow, IDC_SPECIMEN, tempStr, 255);
  Val(tempStr, numSpcs, err);

  GetDlgItemText(HWindow, IDC_ISOLATE, tempStr, 255);
  Val(tempStr, isoVar, err);

  GetDlgItemText(HWindow, IDC_DATEVAR, tempStr, 255);
  Val(tempStr, dateVar, err);
end;

procedure TMainDlg.ShowStat(id: integer; num: longint);
var
  nstr   : array [0..25] of char;
begin
  Str(num, nstr);
  statDlg^.SetText(nstr, id);
  statDlg^.Update;
end;

procedure TMainDlg.DoPrep;
var
  k       : longint;
begin
  ShowStat(IDC_SOURCE, srcDB^.dbc^.NumRecords);
  ShowStat(IDC_SESSION, sessDB^.dbc^.NumRecords);
  ShowStat(IDC_PATIENT, patDB^.dbc^.NumRecords);
  ShowStat(IDC_SPECIMEN, spcDB^.dbc^.NumRecords);
  numIsos := isoDB^.dbc^.NumRecords;
  ShowStat(IDC_ISOLATE, numIsos);

  {- Deleting a specimen record deletes associated
     GRAMSTN, ISOLATE, ISORDER, and RESULT records }
  k := spcDB^.dbc^.NumRecords;
  if k <> 0 then
  begin
    while (spcDB^.dbc^.dbErrorNum = 0) and spcDB^.dbc^.GetFirst(spcDB^.dbr) do
    begin
      spcDB^.dbc^.DeleteRec(spcDB^.dbr);
      Dec(k);
      numIsos := isoDB^.dbc^.NumRecords;
      ShowStat(IDC_ISOLATE, numIsos);
      ShowStat(IDC_SPECIMEN, k);
    end;
    if spcDB^.dbc^.NumRecords <> 0 then
      ErrorMsg(0, AppName, 'SPECIMEN file is not empty!')
    else
      ShowStat(IDC_SPECIMEN, 0);
  end;

  numOrgs := orgDB^.dbc^.NumRecords;
  numIsos := isoDB^.dbc^.NumRecords;
  ShowStat(IDC_ISOLATE, numIsos);

  k := patDB^.dbc^.NumRecords;
  if k <> 0 then
  begin
    while (patDB^.dbc^.dbErrorNum = 0) and patDB^.dbc^.GetFirst(patDB^.dbr) do
    begin
      ShowStat(IDC_PATIENT, k);
      patDB^.dbc^.DeleteRec(patDB^.dbr);
      Dec(k);
    end;
    if patDB^.dbc^.NumRecords <> 0 then
      ErrorMsg(0, AppName, 'PATIENT file is not empty!')
    else
      ShowStat(IDC_PATIENT, 0);
  end;

  k := sessDB^.dbc^.NumRecords;
  if k <> 0 then
  begin
    while (sessDB^.dbc^.dbErrorNum = 0) and sessDB^.dbc^.GetFirst(sessDB^.dbr) do
    begin
      ShowStat(IDC_SESSION, k);
      sessDB^.dbc^.DeleteRec(sessDB^.dbr);
      Dec(k);
    end;
    if sessDB^.dbc^.NumRecords <> 0 then
      ErrorMsg(0, AppName, 'SESSION file is not empty!')
    else
      ShowStat(IDC_SESSION,  0);
  end;

  k := srcDB^.dbc^.NumRecords;
  if k <> 0 then
  begin
    while (srcDB^.dbc^.dbErrorNum = 0) and srcDB^.dbc^.GetFirst(srcDB^.dbr) do
    begin
      ShowStat(IDC_SOURCE, k);
      srcDB^.dbc^.DeleteRec(srcDB^.dbr);
      Dec(k);
    end;
    if srcDB^.dbc^.NumRecords <> 0 then
      ErrorMsg(0, AppName, 'SOURCE file is not empty!')
    else
      ShowStat(IDC_SOURCE, 0);
  end;

  k := wrdDB^.dbc^.NumRecords;
  if k <> 0 then
  begin
    while (wrdDB^.dbc^.dbErrorNum = 0) and wrdDB^.dbc^.GetFirst(wrdDB^.dbr) do
    begin
      wrdDB^.dbc^.DeleteRec(wrdDB^.dbr);
      Dec(k);
    end;
    if wrdDB^.dbc^.NumRecords <> 0 then
      ErrorMsg(0, AppName, 'WARD file is not empty!');
  end;

  k := gsdefDB^.dbc^.NumRecords;
  if k <> 0 then
  begin
    while (gsdefDB^.dbc^.dbErrorNum = 0) and gsdefDB^.dbc^.GetFirst(gsdefDB^.dbr) do
    begin
      gsdefDB^.dbc^.DeleteRec(gsdefDB^.dbr);
      Dec(k);
    end;
    if gsdefDB^.dbc^.NumRecords <> 0 then
      ErrorMsg(0, AppName, 'GSDEF file is not empty!');
  end;

  k := gmnDB^.dbc^.NumRecords;
  if k <> 0 then
  begin
    while (gmnDB^.dbc^.dbErrorNum = 0) and gmnDB^.dbc^.GetFirst(gmnDB^.dbr) do
    begin
      gmnDB^.dbc^.DeleteRec(gmnDB^.dbr);
      Dec(k);
    end;
    if gmnDB^.dbc^.NumRecords <> 0 then
      ErrorMsg(0, AppName, 'GRAMNEM file is not empty!');
  end;

  k := ordDB^.dbc^.NumRecords;
  if k <> 0 then
  begin
    while (ordDB^.dbc^.dbErrorNum = 0) and ordDB^.dbc^.GetFirst(ordDB^.dbr) do
    begin
      ordDB^.dbc^.DeleteRec(ordDB^.dbr);
      Dec(k);
    end;
    if ordDB^.dbc^.NumRecords <> 0 then
      ErrorMsg(0, AppName, 'ORDER file is not empty!');
  end;
end;

procedure TMainDlg.DoMnemonics;
var
  flg   : boolean;
  k     : longint;
  pstr  : array [0..25] of char;
  id    : array [0..25] of char;
  desc  : array [0..40] of char;
  ordSeq  : TSeqNum;
  toSeq   : TSeqNum;
begin
  srcDB^.dbr^.ClearRecord;
  wrdDB^.dbr^.ClearRecord;

  for k := 1 to numSrcs do
  begin
    ShowStat(IDC_SOURCE, k);
    Str(k, pstr);
    StrCopy(id, 'S');
    StrLCat(id, pstr, 25);
    srcDB^.dbr^.PutFieldAsStr(DBSRCID, id);
    StrCopy(desc, 'Source ');
    StrLCat(desc, pstr, 40);
    srcDB^.dbr^.PutFieldAsStr(DBSRCDesc, desc);
    flg := (Random(2) = 0);
    srcDB^.dbr^.PutField(DBSRCUrine, @flg);
    if not srcDB^.dbc^.InsertRec(srcDB^.dbr) then
      ErrorMsg(0, AppName, 'Cannot insert record in SOURCE file!');

    StrCopy(id, 'W');
    StrLCat(id, pstr, 25);
    wrdDB^.dbr^.PutFieldAsStr(DBWARDID, id);
    StrCopy(desc, 'Ward ');
    StrLCat(desc, pstr, 40);
    wrdDB^.dbr^.PutFieldAsStr(DBWARDDesc, desc);
    if not wrdDB^.dbc^.InsertRec(wrdDB^.dbr) then
      ErrorMsg(0, AppName, 'Cannot insert record in WARD file!');
  end;

  gmnDB^.dbr^.PutFieldAsStr(DBGSMNEMID, '1');
  gmnDB^.dbr^.PutFieldAsStr(DBGSMNEMDesc, '+ - Rare');
  if not gmnDB^.dbc^.InsertRec(gmnDB^.dbr) then
    ErrorMsg(0, AppName, 'Cannot insert record in GRAMNEM file!');

  gmnDB^.dbr^.PutFieldAsStr(DBGSMNEMID, '2');
  gmnDB^.dbr^.PutFieldAsStr(DBGSMNEMDesc, '++ - Few');
  if not gmnDB^.dbc^.InsertRec(gmnDB^.dbr) then
    ErrorMsg(0, AppName, 'Cannot insert record in GRAMNEM file!');

  gmnDB^.dbr^.PutFieldAsStr(DBGSMNEMID, '3');
  gmnDB^.dbr^.PutFieldAsStr(DBGSMNEMDesc, '+++ - Moderate');
  if not gmnDB^.dbc^.InsertRec(gmnDB^.dbr) then
    ErrorMsg(0, AppName, 'Cannot insert record in GRAMNEM file!');

  gmnDB^.dbr^.PutFieldAsStr(DBGSMNEMID, '4');
  gmnDB^.dbr^.PutFieldAsStr(DBGSMNEMDesc, '++++ - Many');
  if not gmnDB^.dbc^.InsertRec(gmnDB^.dbr) then
    ErrorMsg(0, AppName, 'Cannot insert record in GRAMNEM file!');

  ordDB^.dbr^.ClearRecord;
  toDB^.dbc^.GetFirst(toDB^.dbr);
  repeat
    toDB^.dbr^.GetFieldAsStr(DBTSTGRPID, pstr, 25);
    ordDB^.dbr^.PutFieldAsStr(DBORDID, pstr);
    ordDB^.dbr^.PutFieldAsStr(DBORDDesc, pstr);
    toDB^.dbr^.GetFieldAsStr(DBTSTGRPSet, pstr, 25);
    ordDB^.dbr^.PutFieldAsStr(DBORDSet, pstr);
    if not ordDB^.dbc^.InsertRec(ordDB^.dbr) then
      ErrorMsg(0, AppName, 'Cannot insert record in ORDER file!');
    ordSeq := ordDB^.dbr^.GetSeqValue;
    ordefDB^.dbr^.PutField(DBORDXRFOrdRef, @ordSeq);
    toSeq := toDB^.dbr^.GetSeqValue;
    ordefDB^.dbr^.PutField(DBORDXRFTstGrpRef, @toSeq);
    if not ordefDB^.dbc^.InsertRec(ordefDB^.dbr) then
      ErrorMsg(0, AppName, 'Cannot insert record in ORDERXRF file!');
  until not toDB^.dbc^.GetNext(toDB^.dbr);
  numOrds := ordDB^.dbc^.NumRecords;
end;

procedure TMainDlg.DoSession;
var
  k     : longint;
  id    : array [0..25] of char;
  pstr  : array [0..25] of char;
  desc  : array [0..30] of char;
begin
  sessDB^.dbr^.ClearRecord;

  for k := 1 to numSess do
  begin
    ShowStat(IDC_SESSION, k);
    Str(k, pstr);
    StrCopy(id, 'S');
    StrLCat(id, pstr, 25);
    sessDB^.dbr^.PutFieldAsStr(DBSESSID, id);
    StrCopy(desc, 'Session ');
    StrLCat(desc, pstr, 30);
    sessDB^.dbr^.PutFieldAsStr(DBSESSDesc, desc);
    if not sessDB^.dbc^.InsertRec(sessDB^.dbr) then
      ErrorMsg(0, AppName, 'Cannot insert record in SESSION file!');
  end;
end;

procedure TMainDlg.DoPatient;
var
  k     : longint;
  pstr  : array [0..40] of char;
  tstr  : string[25];
  nstr  : string[25];
begin
  patDB^.dbr^.ClearRecord;

  for k := 1 to numPats do
  begin
    ShowStat(IDC_PATIENT, k);
    Str(k, nstr);
    tstr[0] := Chr(patDB^.dbd^.FieldSizeForStr(1) - 1);
    FillChar(tstr[1], Length(tstr),  '0');
    Move(nstr[1], tstr[Length(tstr)-Length(nstr)+1], Length(nstr));
    StrPCopy(pstr, tstr);
    patDB^.dbr^.PutFieldAsStr(DBPATPatID, pstr);
    StrPCopy(pstr, 'Doe ' + tstr);
    patDB^.dbr^.PutFieldAsStr(DBPATLName, pstr);
    StrPCopy(pstr, 'Joe ' + tstr);
    patDB^.dbr^.PutFieldAsStr(DBPATFName, pstr);
    StrPCopy(pstr, 'Patient ' + tstr + ' free text.');
    patDB^.dbr^.PutFieldAsStr(DBPATFreeText, pstr);
    if not patDB^.dbc^.InsertRec(patDB^.dbr) then
      ErrorMsg(0, AppName, 'Cannot insert record in PATIENT file!');

    if not statDlg^.CanContinue then
      Break;
  end;
end;

procedure TMainDlg.DoSpecimen;
var
  k, j  : longint;
  i     : longint;
  pstr  : array [0..40] of char;
  tstr  : string[25];
  nstr  : string[25];
begin
  spcDB^.dbr^.ClearRecord;

  for k := 1 to numSpcs do
  begin
    ShowStat(IDC_SPECIMEN, k);
    j := Random(dateVar + 1);
    intlDate := IntlCurrentDate;
    intlDate := DecDate(intlDate, j);
    spcDB^.dbr^.PutField(DBSPECCollectDate, @intlDate);
    Str(k, nstr);
    tstr[0] := Chr(spcDB^.dbd^.FieldSizeForStr(1) - 1);
    FillChar(tstr[1], Length(tstr),  '0');
    Move(nstr[1], tstr[Length(tstr)-Length(nstr)+1], Length(nstr));
    StrPCopy(pstr, tstr);
    spcDB^.dbr^.PutFieldAsStr(DBSPECSpecID, pstr);
    j := Random(numPats + 1);
    spcDB^.dbr^.PutField(DBSPECPatRef, @j);
    j := Random(numSrcs + 1);
    spcDB^.dbr^.PutField(DBSPECSrc, @j);

    j := Random(numSrcs * 2);
    if j > numSrcs then
      j := 0;
    spcDB^.dbr^.PutField(DBSPECWard, @j);
    spcDB^.dbr^.PutFieldAsStr(DBSPECStat, '1');
    StrPCopy(pstr, 'Specimen ' + tstr + ' free text.');
    spcDB^.dbr^.PutFieldAsStr(DBSPECFreeText, pstr);
    if not spcDB^.dbc^.InsertRec(spcDB^.dbr) then
      ErrorMsg(0, AppName, 'Cannot insert record in SPECIMEN file!');

    if Random(3) = 0 then
      DoGramStain;

    DoIsolate;
    if not statDlg^.CanContinue then
      Break;
  end;
end;

procedure TMainDlg.JNGram;
var
  srcFile   : text;
  srcLine   : string;
  pstr      : array [0..40] of char;
begin
{$I-}
  if IOResult <> 0 then ;

  Assign(srcFile, 'GSDEF.JN');
  Reset(srcFile);
  ioRet := IOResult;
  if ioRet <> 0 then
  begin
    Str(ioRet, eStr);
    ErrorMsg(0, 'RandData - IO Error - 11', eStr);
    Halt;
  end;

  while not Eof(srcFile) and (gsdefDB^.dbc^.dbErrorNum = 0) do
  begin
    ReadLn(srcFile, srcLine);
    ioRet := IOResult;
    if ioRet <> 0 then
    begin
      Str(ioRet, eStr);
      ErrorMsg(0, 'RandData - IO Error - 21', eStr);
      Halt;
    end;
    TrimAll(pstr, StrPCopy(pstr, Copy(srcLine, 1, 2)), ' ', 40);
    gsdefDB^.dbr^.PutFieldAsStr(DBGSDEFSlotNum, pstr);
    TrimAll(pstr, StrPCopy(pstr, Copy(srcLine, 4, 12)), ' ', 40);
    gsdefDB^.dbr^.PutFieldAsStr(DBGSDEFAbbr, pstr);
    TrimAll(pstr, StrPCopy(pstr, Copy(srcLine, 17, 40)), ' ', 40);
    gsdefDB^.dbr^.PutFieldAsStr(DBGSDEFDesc, pstr);
    TrimAll(pstr, StrPCopy(pstr, Copy(srcLine, 58, 1)), ' ', 40);
    gsdefDB^.dbr^.PutFieldAsStr(DBGSDEFResType, pstr);
    if not gsdefDB^.dbc^.InsertRec(gsdefDB^.dbr) then
      ErrorMsg(0, 'JNData', 'Cannot insert record in GSDEF file!');
  end;
  Close(srcFile);
  if IOResult <> 0 then {nothing};
{$I+}
end;

procedure TMainDlg.DoGramStain;
var
  i, j, k  : longint;
begin
  j :=  spcDB^.dbr^.GetSeqValue;
  gstDB^.dbr^.PutField(DBGSSpecRef, @j);

  for k := 1 to 5 do
  begin
    gstDB^.dbr^.PutField(DBGSGSTstRef, @k);
    i := Random(4) + 1;
    gstDB^.dbr^.PutField(DBGSGSMnemRef, @i);
    if not gstDB^.dbc^.InsertRec(gstDB^.dbr) then
      ErrorMsg(0, AppName, 'Cannot insert record in GRMSTAIN file!');
  end;

  gstDB^.dbr^.PutFieldAsStr(DBGSGSTstRef, '6');
  gstDB^.dbr^.PutFieldAsStr(DBGSGSMnemRef, '0');
  gstDB^.dbr^.PutFieldAsStr(DBGSFreeText, 'Obs Free text');
  if not gstDB^.dbc^.InsertRec(gstDB^.dbr) then
    ErrorMsg(0, AppName, 'Cannot insert record in GRMSTAIN file!');
  gstDB^.dbr^.PutFieldAsStr(DBGSFreeText, '');
end;

procedure TMainDlg.DoIsolate;
var
  k, j  : longint;
  count : longint;
  pstr  : array [0..40] of char;
  nstr  : string[25];
  tstr  : string[25];
begin
  isoDB^.dbr^.ClearRecord;

  j :=  spcDB^.dbr^.GetSeqValue;
  isoDB^.dbr^.PutField(DBISOSpecRef, @j);

  {- handle the redundant spec/collect date in iso rec }
  spcDB^.dbr^.GetFieldAsStr(DBSpecSpecID, pstr, sizeof(pstr)-1);
  isoDB^.dbr^.PutFieldAsStr(DBIsoSpecID, pstr);
  spcDB^.dbr^.GetFieldAsStr(DBSpecCollectDate, pstr, sizeof(pstr)-1);
  isoDB^.dbr^.PutFieldAsStr(DBIsoCollectDate, pstr);

  count := Random(isoVar + 1);
  for k := 1 to count do
  begin
    Inc(numIsos);
    ShowStat(IDC_ISOLATE, numIsos);
    Str(k, nstr);
    FillChar(tstr[1], 3, '0');
    tstr[0] := Chr(3 - Length(nstr));
    tstr := tstr + nstr;
    StrPCopy(pstr, tstr);
    isoDB^.dbr^.PutFieldAsStr(DBISOIso, pstr);

    j := RandOrder;      { must be called prior to RandOrg }
    isoDB^.dbr^.PutField(DBISOOrdRef, @j);
    j := RandOrg;
    if j = 0 then
      StrCopy(pstr, '0')
    else
      orgDB^.dbr^.GetFieldAsStr(DBORGFamily, pstr, 40);

    IsoSetFamily(j);

    isoDB^.dbr^.PutField(DBISOOrgRef, @j);

    isoDB^.dbr^.PutField(DBISOTstDate, @intlDate);
    j := 0;
    isoDB^.dbr^.PutField(DBISOTransFlag, @j);
    StrPCopy(pstr, 'Isolate ' + tstr + ' free text.');
    isoDB^.dbr^.PutFieldAsStr(DBISOFreeText, pstr);

    if Random(20) = 0 then
      j := 1
    else
      j := 0;
    isoDB^.dbr^.PutField(DBISOQCFlag, @j);

    j := Random(numSess +  1);
    isoDB^.dbr^.PutField(DBISOSess, @j);

    if not isoDB^.dbc^.InsertRec(isoDB^.dbr) then
      ErrorMsg(0, AppName, 'Cannot insert record in ISOLATE file!');

    DoIsorder;
    DoResult;
  end;
end;

procedure TMainDlg.DoIsorder;
var
  ordef : PDBAssocFile;
  j     : longint;
begin
  ordef := New(PDBAssocFile, Init(ordDB^.dbr, DBORDER_ORDERXRF_ASSOC, dbOpenNormal));

  if ordef^.dbc^.GetFirst(ordef^.dbr) then
    repeat
      isordDB^.dbr^.ClearRecord;

      j :=  isoDB^.dbr^.GetSeqValue;
      isordDB^.dbr^.PutField(DBISOORDIsoRef, @j);

      ordef^.dbr^.GetField(DBORDXRFTstGrpRef, @j, SizeOf(TSeqNum));
      isordDB^.dbr^.PutField(DBISOORDTstGrpRef, @j);

      isordDB^.dbr^.PutField(DBISOORDTstDate, @intlDate);

      if not isordDB^.dbc^.InsertRec(isordDB^.dbr) then
        ErrorMsg(0, AppName, 'Cannot insert record in ISORDER file!');

    until not ordef^.dbc^.GetNext(ordef^.dbr);

  MSDisposeObj(ordef);
end;

procedure TMainDlg.DoResult;
var
  j      : longint;
  resRec : PResRecObj;
begin
  resultList^.LoadResults(isoDB^.dbr);
  j := 0;
  while j < resultList^.count do
  begin
    resRec := resultList^.At(j);
    ResultVal(resRec);
    Inc(j)
  end;
  resultList^.SaveResults;
end;

function  TMainDlg.RandOrg: longint;
var
  j     : longint;
  rset  : byte;
  oset  : byte;
begin
  j := 0;
  oset := 0;
  ordDB^.dbr^.GetField(DBORDSet, @rset, 1);
  while oset <> rset do
  begin
    j := Random(numOrgs) + 1;
    orgDB^.dbc^.GetSeq(orgDB^.dbr, j);
    orgDB^.dbr^.GetField(DBORGSet, @oset, 1);
  end;
  RandOrg := j;
end;

function  TMainDlg.RandOrder: longint;
var
  j     : longint;
  oset  : byte;
begin
  repeat
    j := Random(numOrds) + 1;
    ordDB^.dbc^.GetSeq(ordDB^.dbr, j);
    ordDB^.dbr^.GetField(DBORDSet, @oset, 1);
  until (oset = 1) or (oset = 2) or (oset = 0);
  RandOrder := j;
end;

procedure TMainDlg.IsoSetFamily(org: longint);
var
  setfam : word;
  oset   : byte;
  ofam   : byte;
begin
  oset := 0;
  ofam := 0;
  setfam := 0;

  if org <> 0 then
  begin
    orgDB^.dbr^.GetField(DBORGSet, @oset, 1);
    orgDB^.dbr^.GetField(DBORGFamily, @ofam, 1);
    setfam := MakeSetFamily(oset, ofam);
  end;
  isoDB^.dbr^.PutField(DBISOSetFamily, @setfam);
end;

procedure TMainDlg.ResultVal(resRec: PResRecObj);
{- set the result value based on the data (test category) in the
  current result record }
begin
  StrCopy(resRec^.result, '');
  if StrComp(resRec^.testCatID, 'MIC') = 0 then
    RandMIC(resRec)
  else if StrComp(resRec^.testCatID, 'NCCLS') = 0 then
    RandInterp(resRec^.result)
  else if StrComp(resRec^.testCatID, 'BIO') = 0 then
    RandBIO(resRec^.result)
  else
    RandPlusMinus(resRec^.result);
end;

procedure TMainDlg.RandMIC(resRec: PResRecObj);
var
  rdn       : integer;
  k         : longint;
begin
  with resultList^.traysObj^ do
  begin
    LoadTray(resRec^.testgroupID);
    GetDrugRDN(resRec^.testID, rdn);
    if rdn <> -1 then
    begin
      k := Random(dilList[rdn].dilCnt) + 1;
      StrPCopy(resRec^.result, dilList[rdn].drugDils[k]);
    end;
  end;
end;

procedure TMainDlg.RandInterp(rstr: PChar);
var
  k     : longint;
  ci    : boolean;
begin
  repeat
    k := Random(15);
    InterpCodeToStr(k, rstr, maxInterpStrLen, ci);
  until (StrComp(rstr, '****') <> 0) and
        (StrComp(rstr, '') <> 0);
end;

procedure TMainDlg.RandBIO(rstr: PChar);
var
  len   : longint;
  k     : longint;
begin
  len := Random(10) + 6;
  for k := 0 to len - 1 do
    rstr[k] := Chr(Random(7) + Ord('0'));
  rstr[len] := #0;
end;

procedure TMainDlg.RandPlusMinus(rstr: PChar);
begin
  if Random(2) = 0 then
    StrCopy(rstr, '-')
  else
    StrCopy(rstr, '+');
end;


BEGIN
  mainApp.Init(AppName);
  mainApp.Run;
  mainApp.Done;
END.
