uses
  MTask,
  ApiTools;

begin
  if IsDMSProgRunning then
  begin
    FatalError('DMS Installation', 'A DMS program is running.  Do NOT continue installation!');
    Halt(1);
  end
  else
  begin
(*  FatalError('HEY', 'A DMS program is NOT running.'); *)
    Halt(0);
  end;
end.
