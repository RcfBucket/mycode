;----- Data

DATA SEGMENT BYTE PUBLIC

        EXTRN   WaitOneMS    : word
        EXTRN   LoopsPerTick : dword

DATA    ENDS

;----- Code

CODE    SEGMENT BYTE PUBLIC

        ASSUME  CS:CODE, DS:DATA

        PUBLIC  WaitInit, MSWait


;----- WaitMS

;Delay one millisecond

WaitMS	PROC NEAR

        cmp  al,es:[di]
        jne  WaitMsExit
	loop WaitMS
WaitMsExit:
	ret

WaitMS  ENDP


;様様様様様様様様様様様様様様様様様様様様様様様様様様様 Wait

;procedure MSWait(MS: Word);
;Delay for MS milliseconds

MSecs   EQU  WORD PTR SS:[BX+4]

MSWait    PROC FAR

        mov  bx,sp
        mov  dx,MSecs        ; DX = MS
        or   dx,dx           ; Do nothing if MS = 0
        jz   WaitExit

        mov  di,bx
        mov  ax,ss
        mov  es,ax

        mov  al,es:[di]      ; AL has the value there
        mov  bx,WaitOneMS    ; BX has loop count for one MS

DelayLoop:
        mov  cx,bx           ; loop count into CX
        call WaitMS          ; delay for one MS
        dec  dx              ; decrement counter
        jnz  DelayLoop       ; repeat if not 0

WaitExit:
        ret  2

MSWait    ENDP

;------------ WaitInit


;procedure WaitInit;

WaitInit     PROC FAR

                             ; set up delay count
        mov  ax,40h          ; ax = $40
        mov  es,ax           ; es = $40
        mov  di,6Ch          ; es:di => low word of BIOS timer count
        mov  cx,0FFFFh       ; loop before the timer count changes
        xor  dx,dx
        mov  al,es:[DI]      ; al has first byte there
  WaitForChange:
        cmp  al,es:[di]      ; wait for the byte to change
        jz   WaitForChange

        mov  al,es:[di]      ; see how many times to
                             ;  loop before the timer count changes
  @@1:
        cmp  al,es:[di]
        jne  @@2
	loop @@1
        inc  dx              ; for more than 65535 loops (fast PC)
        jmp  short @@1
  @@2:

        mov  ax,54           ; now calculate OneMS
	xchg cx,ax
        not  ax
        mov  word ptr [LoopsPerTick],ax
        mov  word ptr [LoopsPerTick + 2],dx
        div  cx
        mov  WaitOneMS,ax    ; ax has OneMS
	ret

WaitInit     ENDP

CODE	ENDS

	END

