(****************************************************************************


prtepi.inc

produced by Borland Resource Workshop


*****************************************************************************)

const
	DLG_PRTEPI	=	$4060;

	IDC_GRID	=	200;
	IDC_PRINT	=	101;

	IDS_EPIMIC	=	$4061;
	IDS_EPISUSC	=	$4062;
	IDS_EPIITEM	=	$4063;

	IDS_EPI_MIC_TITLE	=	$4071;
	IDS_ORG_LBL	=	$4072;
	IDS_ISO_LBL	=	$4073;
	IDS_PERCENT_LBL	=	$4074;
	IDS_MIC_LBL	=	$4075;
	IDS_DRUG_LBL	=	$4076;
	IDS_TESTED_LBL	=	$4077;
	IDS_TOTAL_LBL	=	$4078;
	IDS_EPI_ERR	=	$4079;

	IDS_EPI_SUS_TITLE	=	$4081;
	IDS_ORGANISM_LBL	=	$4082;

	IDS_NUM_ORG_LBL	=	$4091;
	IDS_PCT_TOT_LBL	=	$4092;
	IDS_GN_TOT_LBL	=	$4093;
	IDS_GP_TOT_LBL	=	$4094;
	IDS_ANAEROBE_TOT_LBL	=	$4095;
	IDS_HNID_TOT_LBL	=	$4096;
	IDS_YEAST_TOT_LBL	=	$4097;
	IDS_OTHER_TOT_LBL	=	$4098;

	IDS_EPISIR	=	16484;
	IDS_EPI_SIR_TITLE	=	16515;
	IDS_ORGANISM_SNAME	=	16537;
	IDS_TOTAL	=	16538;
