unit Cvt20Map;

INTERFACE

uses
  OWindows;

procedure MapV20Fields(aParent: PWindowsObject);

IMPLEMENTATION

uses
  DBTypes,
  DB2DBIF,
  DBIDS,
  ODialogs,
  WinProcs,
  WinTypes,
  CvtMisc,
  Cvt20Cvt,
  DlgLib,
  GridLib;

{$I CVT20.INC}

type
  PMap20Dlg = ^TMap20Dlg;
  TMap20Dlg = object(TCenterDlg)
    grid    : PMapGrid;
    maps    : PMapCollection;
    constructor Init(aParent: PWindowsObject);
    destructor Done; virtual;
    procedure WMCharToItem(var msg: TMessage);  virtual WM_FIRST + WM_CHARTOITEM;
    procedure WMDrawItem(var msg: TMessage);    virtual WM_FIRST + WM_DRAWITEM;
    procedure WMMeasureItem(var msg: TMessage); virtual WM_FIRST + WM_MEASUREITEM;
    procedure IDMap(var msg: TMessage); virtual ID_FIRST + IDC_MAP;
    procedure ClearMap(var msg: TMessage); virtual ID_FIRST + IDC_CLEARMAP;
    procedure IDNext(var msg: TMessage); virtual ID_FIRST + IDC_NEXT;
    procedure WMCommand(var msg: TMessage); virtual WM_FIRST + WM_COMMAND;
    procedure MapAField;
    function CanClose: boolean; virtual;
    procedure Cancel(var msg: TMessage); virtual ID_FIRST + ID_CANCEL;
  end;

constructor TMap20Dlg.Init(aParent: PWindowsObject);
var
  db2Rec  : PDB2Record;
  k       : integer;
  fld     : PDB2Field;
  num     : integer;
begin
  inherited Init(aParent, MakeIntResource(DLG_V20Map));
  maps:= New(PMapCollection, Init(Version20));

  db2Rec:= DB2OpenFile('PATIENT');
  if db2Rec = nil then
    CriticalError('Cannot open V2.x PATIENT database');

  for k:= 1 to db2Rec^.NumFields do
  begin
    fld:= db2Rec^.GetField(k);
    if fld^.UDFlag then
    begin
      if fld^.fldType = dbZString then
        num:= fld^.FldSize
      else
        num:= 0;
      maps^.AddField(k, num, 'PATIENT', fld^.FldName, fld^.fldType);
    end;
  end;
  DB2CloseFile(db2Rec);

  db2Rec:= DB2OpenFile('SPECIMEN');
  if db2Rec = nil then
    CriticalError('Cannot open V2.x SPECIMEN database');

  for k:= 1 to db2Rec^.NumFields do
  begin
    fld:= db2Rec^.GetField(k);
    if fld^.UDFlag then
    begin
      if fld^.fldType = dbZString then
        num:= fld^.FldSize
      else
        num:= 0;
      maps^.AddField(k, num, 'SPECIMEN', fld^.FldName, fld^.fldType);
    end;
  end;
  DB2CloseFile(db2Rec);

  db2Rec:= DB2OpenFile('BATORD');
  if db2Rec = nil then
    CriticalError('Cannot open V2.x BATORD database');

  for k:= 1 to db2Rec^.NumFields do
  begin
    fld:= db2Rec^.GetField(k);
    if fld^.UDFlag then
    begin
      if fld^.fldType = dbZString then
        num:= fld^.FldSize
      else
        num:= 0;
      maps^.AddField(k, num, 'ISOLATE', fld^.FldName, fld^.fldType);
    end;
  end;
  DB2CloseFile(db2Rec);

  grid:= New(PMapGrid, InitResource(@self, IDC_GRID, maps));
end;

destructor TMap20Dlg.Done;
begin
  Dispose(maps, Done);
  inherited Done;
end;

procedure TMap20Dlg.WMCharToItem(var msg: TMessage);
begin
  grid^.CharToItem(msg);
end;

procedure TMap20Dlg.WMDrawItem(var msg: TMessage);
begin
  grid^.DrawItem(msg);
end;

procedure TMap20Dlg.WMMeasureItem(var msg: TMessage);
begin
  grid^.MeasureItem(msg);
end;

procedure TMap20Dlg.IDMap(var msg: TMessage);
begin
  MapAField;
end;

procedure TMap20Dlg.ClearMap(var msg: TMessage);
begin
  grid^.ClearFieldMap(grid^.GetSelIndex);
end;

procedure TMap20Dlg.IDNext(var msg: TMessage);
begin
  ShowWindow(hWindow, SW_HIDE);
  Cvt20ConvertData(@self, maps);
  EndDlg(IDOK);
end;

procedure TMap20Dlg.WMCommand(var msg: TMessage);
begin
  inherited WMCommand(msg);
  if (msg.lParamLo <> 0) then   {- message is from a control }
  begin
    if msg.lParamHI = LBN_DBLCLK then
      MapAField;
  end;
end;

procedure TMap20Dlg.MapAField;
var
  k   : integer;
begin
  k:= grid^.GetSelIndex;
  if k >= 0 then
    grid^.MapAField(k);
end;

function TMap20Dlg.CanClose: boolean;
begin
  CanClose:= ConfirmExit(hWindow);
end;

procedure TMap20Dlg.Cancel(var msg: TMessage);
begin
  if CanClose then
    inherited Cancel(msg);
end;

procedure MapV20Fields(aParent: PWindowsObject);
begin
  application^.ExecDialog(New(PMap20Dlg, Init(aParent)));
end;

END.

{- rcf}
