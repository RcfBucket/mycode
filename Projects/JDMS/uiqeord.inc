(****************************************************************************


uiqeord.inc

produced by Borland Resource Workshop

Module ID for UIQEORD = $21F0 = 8688
*****************************************************************************)

const
  DLG_UIQEORD         = 8688;

  IDC_EDSPECID        = 200;
  IDC_EDCOLLECT       = 201;
  IDC_EDISO           = 202;
  IDC_EDORDER         = 203;
  IDC_EDSOURCE        = 204;
  IDC_EDPATID         = 205;

  IDC_LBLSPECID       = 300;
  IDC_LBLCOLLECT      = 301;
  IDC_LBLISO          = 302;
  IDC_LBLORDER        = 303;
  IDC_LBLSOURCE       = 304;
  IDC_LBLPATID        = 305;

  IDC_LIST            = 400;

  IDC_SPECIMEN        = 100;
  IDC_PATIENT         = 101;
  IDC_ISOLATE         = 102;
  IDC_IDMIC           = 103;
  IDC_AS4             = 104;
  IDC_BARCODES        = 105;
  IDC_MOVE            = 106;
  IDC_SESSION         = 107;
	DLG_FILL	=	8689;
	IDC_TEXT	=	201;
	IDS_ISOMOVED	=	8688;
	IDC_MODESPEC	=	402;
	IDC_MODEISO	=	401;
	IDS_CANCELFILL	=	8689;
	IDS_TOOMUCH	=	8690;
	IDS_DATAMODIFIED	=	8691;
	IDS_ISONOTINSESSION	=	8692;
	IDS_NOPATIENT	=	8693;
