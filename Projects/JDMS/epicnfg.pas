unit EpiCnfg;

{----------------------------------------------------------------------------}
{  Module Name  : EpiCnfg.PAS                                                }
{  Programmer   : SM                                                         }
{  Date Created : 05/01/98                                                   }
{                                                                            }
{  Purpose - To provide tools for storing, accessing, and editing the        }
{    epidemiology reports configuration. Currently, this only includes       }
{    settings for the EDFilter.                                              }
{                                                                            }
{  Assumptions                                                               }
{    a. Because this unit caches the configuration, a program using this     }
{       unit must assume that no other program has modified the              }
{       configuration in the INI file while it is running. However, the way  }
{       the Nihingo DMS is designed, this may happen because this unit is    }
{       used by the main program and the report generator program. With each }
{       having a local copy of the configuration, a coherency problem may    }
{       arise if the user starts the report generator program, then modifies }
{       the configuration from the main program, then goes back to the       }
{       running report generator.                                            }
{                                                                            }
{       At first thought, adding a cache locking mechanism is the solution,  }
{       but actually, that would only hide the real design flaw. A more      }
{       productive effort would find a way to recombine report generation    }
{       with the main program. Then, the reports menu could be fixed too.    }
{                                                                            }
{    b. The user is not alerted to errors in the INI file, defaults are      }
{       silently used.                                                       }
{                                                                            }
{    c. In this unit, all objects constructed with the New operator are      }
{       assumed valid because out-of-memory is the only probable failure and }
{       in that case nothing usefull (or harmful) can be done.               }
{                                                                            }
{  Initialization - The INI file is only read upon program initialization.   }
{                                                                            }
{  Revision History - This project is under version control, use it to view  }
{    revision history.                                                       }
{                                                                            }
{----------------------------------------------------------------------------}

INTERFACE

uses
  OWindows;


{---- The constants defined here define the fixed aspects of the
      configuration. EC_MinSearchWindow and EC_MaxSearchWindow define the
      minimum and maximum value for the search window
      (TEpiCnfg.SearchWindow).
}
const
  EC_MinSearchWindow =  1;      { Must be > 0 and <= EC_MaxSearchWindow }
  EC_MaxSearchWindow = 31;      { Must be > 0 }


{---- This record defines the variable parameters of the epidemiology reports
      configuration. If parameters not related to duplicate isolates are
      added, then sub records should be created for duplicate isolates and the
      new items.

      SearchWindow - Number of days to search for duplicates,
        EC_MinSearchWindow <= SearchWindow <= EC_MaxSearchWindow
}
type
  TEpiCnfg = record
    OmitDuplicates,  { eliminate duplicates from reports }
    ConsiderSource   { take source into account when eliminating duplicates }
      : boolean;
    SearchWindow
      : integer;
  end;


{---- This procedure permits the user to view and/or edit the configuration
      via a dialog box.
}
procedure CnfgEpiReportsDlg( pParent:PWindowsObject );


{---- This procedure returns a copy of the configuration. To make sure the
      most current configuration is used, call this procedure each time
      the configuration is needed (discard it after one use).
}
procedure getEpiCnfg( var configuration : TEpiCnfg );


{---- This function replaces the configuration with a new one. If the new
      configuration is valid, it returns true, otherwise the configuration
      is not changed and the function returns false. This function should
      not be needed if the CnfgEpiReportsDlg dialog is used.
}
function setEpiCnfg( NewCnfg : TEpiCnfg ) : boolean;


IMPLEMENTATION {=============================================================}

uses
  INILib,
  Validate,
  DLGLib,
  ODialogs,
  Strings,
  WinProcs,
  WinTypes;

{$R Epi.RES}
{$I Epi.INC}

const

  {---- Size for search window text box buffer. }
  SWBTextLen = 10;

  {---- This default configuration contains the values to use when the user
        profile (INI file) contains no values or incorrect values.
  }
  DefaultCnfg : TEpiCnfg = ( OmitDuplicates: false; ConsiderSource: false;
    SearchWindow: 7 );

  {---- Name of section in INI file to store the configuration }
  SectionName = 'Reports-Epi';

  {---- Text for ture/false in the INI file }
  INI_TRUE = 'TRUE';
  INI_FALSE = 'FALSE';


type

  {--------------------------------------------------------------------------}
  { Dialog box object
  {--------------------------------------------------------------------------}
  PCnfgEpiReportsDlg = ^TCnfgEpiReportsDlg;
  TCnfgEpiReportsDlg = object( TCenterDlg )
    pConsiderSourceCtrl,
    pOmitDuplicatesCtrl : PCheckBox;
    pDaysCtrl : PEdit;
    pSWStaticCtrl,
    pDaysStaticCtrl : PStatic;
    constructor Init( pParent:PWindowsObject );
    procedure SetupWindow; virtual;
    procedure OnOmitDuplicates( var Msg: TMessage ); virtual id_First +
      IDC_OMIT_DUPLICATES;
    procedure SetEnables;
  end;

  {--------------------------------------------------------------------------}
  { Dialog Box Transfer buffer. Note: the fields in this record must exactly
  { match the the order in which the corresponding controls are initialized.
  {--------------------------------------------------------------------------}
  TCnfgEpiReportsDlgData = record
    omitDuplicatesBox,
    considerSourceBox : word;
    dupSearchWindowBox : array[0..SWBTextLen-1] of Char;
  end;



{----------------------------------------------------------------------------}
{ This constructor provides additional initializations needed for to
{ implement the transfer buffer, the enable/disable feature of some
{ controls, and data validation.
{----------------------------------------------------------------------------}

constructor TCnfgEpiReportsDlg.Init( pParent: PWindowsObject );
begin
  if not inherited Init( pParent, MakeIntResource( DLG_CNFG_EPI ) ) then
  begin
    Done;
    Fail;
  end;

  {---- In this section, objects are created for controls that require special
        processing. Controls using the transfer buffer are initialized in the
        exact order thay are defined in the transfer record. The static controls
        are only created so thay they can be disabled. }
  pOmitDuplicatesCtrl := New( PCheckBox, InitResource( @Self,
    IDC_OMIT_DUPLICATES ) );
  pConsiderSourceCtrl := New( PCheckBox, InitResource( @Self,
    IDC_CONSIDER_SOURCE ) );
  pDaysCtrl := New( PEdit, InitResource( @Self, IDC_DUP_SRCH_WND,
    SWBTextLen ) );
  pSWStaticCtrl := New( PStatic, InitResource( @Self, IDC_SEARCH_WND, 0 ) );
  pSWStaticCtrl^.DisableTransfer;
  pDaysStaticCtrl := New( PStatic, InitResource( @Self, IDC_DAYS, 0 ) );
  pDaysStaticCtrl^.DisableTransfer;

  {---- Data validation }
  pDaysCtrl^.SetValidator( New( PRangeValidator,
    Init( EC_MinSearchWindow, EC_MaxSearchWindow ) ) );

end;



{----------------------------------------------------------------------------}
{ This procedure reads the state of the "Omit duplicates" check box and
{ updates the state of this feature's sub items as needed. This function must
{ be called initially and each time the state of the check box changes.
{----------------------------------------------------------------------------}

procedure TCnfgEpiReportsDlg.SetEnables;
begin
  if pOmitDuplicatesCtrl^.GetCheck = bf_Checked then
  begin
    pConsiderSourceCtrl^.Enable;
    pDaysCtrl^.Enable;
    pSWStaticCtrl^.Enable;
    pDaysStaticCtrl^.Enable;
  end
  else
  begin
    pConsiderSourceCtrl^.Disable;
    pDaysCtrl^.Disable;
    pSWStaticCtrl^.Disable;
    pDaysStaticCtrl^.Disable;
  end
end;



{----------------------------------------------------------------------------}
{ This override provides additional window set up. Now that the window has
{ been created, the control enables can be initialized (see SetEnables above).
{----------------------------------------------------------------------------}

procedure TCnfgEpiReportsDlg.SetupWindow;
begin
  inherited SetupWindow;
  SetEnables;
end;



{----------------------------------------------------------------------------}
{ This message handler provides additional message processing for the Omit
{ duplicates (OD) check box. Whenever this control is checked or unchecked,
{ the enables must be updated (see SetEnables above).
{----------------------------------------------------------------------------}

procedure TCnfgEpiReportsDlg.OnOmitDuplicates( var Msg: TMessage );
begin
  SetEnables;
end;



{----------------------------------------------------------------------------}
{ This procedure handles a command to view and/or edit the epidemiology
{ reports configuration. It uses a dialog box.
{----------------------------------------------------------------------------}

procedure CnfgEpiReportsDlg( pParent:PWindowsObject );

  {--------------------------------------------------------------------------}
  { This procedure provides verification for operations that should never
  { fail.
  {--------------------------------------------------------------------------}
  procedure Verify( ok : boolean );
  begin
    if not ok then
      MessageBox( pParent^.HWindow, 'EPICNFG', nil, mb_ok );
  end;

  {--------------------------------------------------------------------------}
  { This function converts a boolean value to a checked/unchecked value.
  {--------------------------------------------------------------------------}
  function BoolToCheck( flag : boolean ) : word;
  begin
    if flag then
      BoolToCheck := bf_Checked
    else
      BoolToCheck := bf_Unchecked;
  end;

var
  XBuffer : TCnfgEpiReportsDlgData;
  cnfg : TEpiCnfg;

  {--------------------------------------------------------------------------}
  { This function loads the local configuration into the transfer buffer and
  { returns a pointer to the buffer.
  {--------------------------------------------------------------------------}
  function LoadBuffer : Pointer;
  begin
    XBuffer.omitDuplicatesBox := BoolToCheck( cnfg.omitDuplicates );
    XBuffer.considerSourceBox := BoolToCheck( cnfg.considerSource );
    Str( cnfg.SearchWindow, XBuffer.dupSearchWindowBox );
    LoadBuffer := @XBuffer;
  end;

  {--------------------------------------------------------------------------}
  { This procedure uses data in the transfer buffer to load the local
  { configuration record.
  {--------------------------------------------------------------------------}
  procedure SaveBuffer;
  var
    code : integer;
  begin
    cnfg.omitDuplicates := XBuffer.omitDuplicatesBox = bf_Checked;
    cnfg.considerSource := XBuffer.considerSourceBox = bf_Checked;
    Val( XBuffer.dupSearchWindowBox, cnfg.SearchWindow, code );
  end;

var
  pDlg : PCnfgEpiReportsDlg;

begin { CnfgEpiReportsDlg }

  pDlg := New( PCnfgEpiReportsDlg, Init( pParent ) );
  getEpiCnfg( cnfg );
  pDlg^.TransferBuffer := LoadBuffer;
  if Application^.ExecDialog( pDlg ) <> id_Cancel then
  begin
    SaveBuffer;
    Verify( setEpiCnfg( cnfg ) );
  end;

end;



{----------------------------------------------------------------------------}
{ This function returns true if the given configuration is valid.
{----------------------------------------------------------------------------}

function isValidCnfg( cnfg : TEpiCnfg ) : boolean;
begin
  isValidCnfg := ( cnfg.SearchWindow >= EC_MinSearchWindow ) and
    ( cnfg.SearchWindow <= EC_MaxSearchWindow );
end;



{----------------------------------------------------------------------------}
{ This procedure reads the configuration from the provile (INI file). The
{ coresponding value in the DefaultCnfg is used when a value is not found or
{ is invalid. To insure consistency beteween the INI file and the master
{ configuration variable, this function should only be called during unit
{ initialzation.
{----------------------------------------------------------------------------}

procedure getCnfgINI( var cnfg : TEpiCnfg );

const
  StrLen = 100;
var
  pStr  : array[0..StrLen-1] of char;

  {--------------------------------------------------------------------------}
  { This procedure reads a boolean key from the profile
  {--------------------------------------------------------------------------}
  function BooleanINI( key : PChar; default : boolean ) : boolean;
  begin
    GetPrivateProfileString( SectionName, key, '', pStr, StrLen,
      INIFileName );
    if StrIComp( pStr, INI_TRUE ) = 0 then
      BooleanINI := true
    else if StrIComp( pStr, INI_FALSE ) = 0 then
      BooleanINI := false
    else
      BooleanINI := default;
  end;

  {--------------------------------------------------------------------------}
  { This procedure reads an integer key from the profile
  {--------------------------------------------------------------------------}
  function IntegerINI( key : PChar; default, min, max : integer ) : integer;
  var
    i : longint;
    code : integer;
  begin
    GetPrivateProfileString( SectionName, key, '', pStr, StrLen,
      INIFileName );
    Val( pStr, i, code );
    if ( code = 0 ) and ( i >= min ) and ( i <= max ) then
      IntegerINI := i
    else
      IntegerINI := default;
  end;

begin { getCnfgINI }

  cnfg.OmitDuplicates := BooleanINI( 'OmitDuplicates',
    DefaultCnfg.OmitDuplicates );
  cnfg.ConsiderSource := BooleanINI( 'ConsiderSource',
    DefaultCnfg.ConsiderSource );
  cnfg.SearchWindow := IntegerINI( 'SearchWindow',
    DefaultCnfg.SearchWindow, EC_MinSearchWindow, EC_MaxSearchWindow );

end;



{----------------------------------------------------------------------------}
{ This procedure writes the configuration to the profile (INI file).  To
{ insure consistency beteween the INI file and the master configuration
{ variable, this function should only be called by setEpiCnfg.
{----------------------------------------------------------------------------}

procedure setCnfgINI( cnfg : TEpiCnfg );

  {--------------------------------------------------------------------------}
  { This procedure writes a boolean key to the profile
  {--------------------------------------------------------------------------}
  procedure BooleanINI( key : PChar; flag : boolean );
  var
    pStr : PChar;
  begin
    if flag then
      pStr := INI_TRUE
    else
      pStr := INI_FALSE;
    WritePrivateProfileString( SectionName, key, pStr, INIFileName );
  end;

  {--------------------------------------------------------------------------}
  { This procedure writes an integer key to the profile
  {--------------------------------------------------------------------------}
  procedure IntegerINI( key : PChar; value : integer );
  const
    StrLen = 20;
  var
    pStr  : array[0..StrLen-1] of char;
  begin
    Str( value, pStr );
    WritePrivateProfileString( SectionName, key, pStr, INIFileName );
  end;

begin { setCnfgINI }

  BooleanINI( 'OmitDuplicates', cnfg.OmitDuplicates );
  BooleanINI( 'ConsiderSource', cnfg.ConsiderSource );
  IntegerINI( 'SearchWindow', cnfg.SearchWindow );

end;



{----------------------------------------------------------------------------}
{ This variable is the master configuration. It's only accessed directly
{ during unit initialization and in the two procedures below.
{----------------------------------------------------------------------------}

var
  MasterCnfg : TEpiCnfg;



{----------------------------------------------------------------------------}
{ This procedure returns a copy of the master configuration record.
{----------------------------------------------------------------------------}

procedure getEpiCnfg( var configuration : TEpiCnfg );
begin
  configuration := MasterCnfg;
end;



{----------------------------------------------------------------------------}
{ This function replaces the configuration with a new one. If the new
{ configuration is valid, it returns true, otherwise the configuration is
{ not changed and the function returns false. This procedure replaces the
{ master configuration with a new one and updates the INI file.
{----------------------------------------------------------------------------}

function setEpiCnfg( NewCnfg : TEpiCnfg ) : boolean;
var
  ok : boolean;
begin
  ok := isValidCnfg( NewCnfg );
  if ok then
  begin
    setCnfgINI( NewCnfg );
    MasterCnfg := NewCnfg;
  end;
  setEpiCnfg := ok;
end;



{----------------------------------------------------------------------------}
{ Unit Initialization. The master configuration is initialized using values
{ from the program's INI file. Defaults are used if INI values are not
{ available.
{----------------------------------------------------------------------------}

begin
  getCnfgINI( MasterCnfg );
  if not isValidCnfg( MasterCnfg ) then
     MasterCnfg := DefaultCnfg;
END.
