{----------------------------------------------------------------------------}
{  Module Name  : DATELIB.PAS                                                }
{  Programmer   : JSS                                                        }
{  Date Created : 12/07/94                                                   }
{                                                                            }
{  Purpose - This unit extends the date/time section of the IntlLib unit by  }
{    providing date and time methods for validating, converting and          }
{    manipulating IntlDate and IntlTime variables.                           }
{                                                                            }
{  Assumptions -                                                             }
{  None.                                                                     }
{                                                                            }
{  Initialization -                                                          }
{  None.                                                                     }
{                                                                            }
{  Revision History - This project is under version control, use it to view  }
{    revision history.                                                       }
{                                                                            }
{----------------------------------------------------------------------------}

unit DateLIB;

INTERFACE

uses
  WinDos,
  Strings,
  IntlLib;

const
  badDate         = $FFFFFFFF;
  badTime         = $FFFFFFFF;

type
  TJDate = longint; {Julian date type}
  TSecs  = longint; {type for seconds variables}

{methods to validate date and time types}
function ValidDate(aDate: TIntlDate): boolean;
function ValidTime(aTime: TIntlTime): boolean;

{methods to convert inbetween date and Julian date types}
function DateToJDate(aDate: TIntlDate): TJDate;
function JDateToDate(aJDate: TJDate): TIntlDate;

{methods to convert inbetween time types and seconds}
function TimetoSecs(aTime: TIntlTime): TSecs;
function SecsToTime(aNumOfSecs: TSecs): TIntlTime;

{methods to increment or decrement date and time types}
function IncDate(aDate: TIntlDate; aNumOfDays: integer): TIntlDate;
function DecDate(aDate: TIntlDate; aNumOfDays: integer): TIntlDate;
function IncTime(aTime: TIntlTime; aNumOfSecs: TSecs): TIntlTime;
function DecTime(aTime: TIntlTime; aNumOfSecs: TSecs): TIntlTime;

{method to get delta seconds from two date and time types}
function DateTimeDiffInSecs(aDate1: TIntlDate;
                            aTime1: TIntlTime;
                            aDate2: TIntlDate;
                            aTime2: TIntlTime): TSecs;

{ This function computes the days between dates, inclusive and absolute (see
  description for details).
}
function DaysBetweenDates( date1, date2 : TIntlDate ) : longint;

{ This function compates two dates. It returns a value less than zero if date1
  is before date2, zero if date1 is the same as date2, greater than zero
  otherwise.
}
function CompareDates( date1, date2 : TIntlDate ) : integer;

{ This function returns true if the given value represents an actual valid
  date. }
function isDate( date: TIntlDate): boolean;


IMPLEMENTATION

uses
  DMSDebug;

const

  {---- These two constants represent the supported range for the year field
        in a TIntlDate type. The minYear constant also represents the year
        1600, therfore, this constant must not be changed. The maxYear
        constant must not exceed 3999 unless the code is modified to account
        for calendar rules beyond that date.

        Note: an exception to this is a year less than 100, although such a
        condition is invalid, apparently is happens because the DateToJDate
        function handles it.
  }
  minYear         = 1600;
  maxYear         = 3999;

  threshold2000   = 1950;
  first2Months    = 59;     {1600 was a leap year}
  hoursInDay      = 24;     {number of hours in a day}
  secondsInDay    = 86400;  {number of seconds in a day}
  secondsInHour   = 3600;   {number of seconds in a hour}
  secondsInMinute = 60;     {number of seconds in a minute}
  minutesInHour   = 60;     {number of minutes in a hour}
  upCaseDateChar  = 'T';    {char used for date code, needed for JRES}
  upCaseTimeChar  = 'N';    {char used for time code, needed for JRES}
  incDateChar     = '+';    {char used to indicate incremental days for date}
  decDateChar     = '-';    {char used to indicate decremental days for date}
  id_KanjiDateChar= 101;    {id of date code char in resource}
  id_KanjiTimeChar= 102;    {id of time code char in resource}



{============================================================================}
{  This function checks year to see if it is a leap year. It is used by      }
{  the DaysInMonth function.                                                 }
{                                                                            }
{  Assumptions                                                               }
{  None                                                                      }
{                                                                            }
{  Errors returned                                                           }
{  None                                                                      }
{                                                                            }
{  InParms : aYear - a year to check                                         }
{  OutParms: None                                                            }
{  Returns : TRUE if leap year, FALSE if not leap year                       }
{============================================================================}

function IsLeapYear(aYear: word) : boolean;
begin
  IsLeapYear := (aYear mod 4 = 0) and (aYear mod 4000 <> 0) and
		((aYear mod 100 <> 0) or (aYear mod 400 = 0));
end;



{============================================================================}
{  This function determines the number of days in the specified month.       }
{  Used by ValidDate.                                                        }
{                                                                            }
{  Assumptions                                                               }
{  None                                                                      }
{                                                                            }
{  Errors returned                                                           }
{  0 days if month is invalid                                                }
{                                                                            }
{  InParms : aMonth - a month to check                                       }
{            aYear  - the year the month is in for leap year purpose         }
{  OutParms: None                                                            }
{  Returns : number of days                                                  }
{============================================================================}

function DaysInMonth(aMonth, aYear: word): integer;
begin
  case aMonth of
    1, 3, 5, 7, 8, 10, 12 : DaysInMonth:= 31;
    4, 6, 9, 11           : DaysInMonth:= 30;
    2                     : DaysInMonth:= 28+Ord(IsLeapYear(aYear));
  else
    DaysInMonth:= 0;
  end;
end;



{============================================================================}
{  This function checks the validity of an international date type. Checks   }
{  to see if year is in range and month is a valid month.                    }
{                                                                            }
{  Assumptions                                                               }
{  None                                                                      }
{                                                                            }
{  Errors returned                                                           }
{  None                                                                      }
{                                                                            }
{  InParms : aDate - a date to validate                                      }
{  OutParms: None                                                            }
{  Returns : TRUE if valid, FALSE if not valid                               }
{============================================================================}

function ValidDate(aDate: TIntlDate): boolean;
var
  year, month, day: word;

begin
  ValidDate:= true;
  if aDate <> 0 then
  begin
    IntlExtractDate(aDate, year, month, day);
    if (day < 1) or (year < minYear) or (year > maxYear) then {bad day or year  }
      ValidDate:= False                                       {  is out of range}
    else {valid thus far}
      case month of
        1..12 : ValidDate:= day <= DaysInMonth(month, year); {good month = valid}
      else
        ValidDate:= False; {not valid}
      end;
  end;
end;



{============================================================================}
{ This function returns true if the given value represents an actual valid
{ date.
{
{ Note: this function was added after the ValidDate function. The non-zero
{ code in ValidDate belongs in this function and that function should call
{ this function to get it. As is, this function backs out to the desired
{ result without modifying the existing function
{============================================================================}

function isDate( date: TIntlDate): boolean;
begin
  isDate := ValidDate( date ) and ( date <> 0 );
end;



{============================================================================}
{  This function checks the validity of an international time type. Checks   }
{  to see if hours, minutes and seconds are within their valid ranges.       }
{                                                                            }
{  Assumptions                                                               }
{  None                                                                      }
{                                                                            }
{  Errors returned                                                           }
{  None                                                                      }
{                                                                            }
{  InParms : aTime - a time to validate                                      }
{  OutParms: None                                                            }
{  Returns : TRUE if valid, FALSE if not valid                               }
{============================================================================}

function ValidTime(aTime: TIntlTime): boolean;
var
  hours, minutes, seconds, hSecs: word;

begin
  ValidTime:= true;
  if aTime <> 0 then
  begin
    IntlExtractTime(aTime, hours, minutes, seconds, hSecs);
    if (hours < 0) or (hours > hoursInDay-1) or            {bad hour ranges}
      (minutes < 0) or (minutes > minutesInHour-1) or     {bad minute ranges}
      (seconds < 0) or (seconds > secondsInMinute-1) then {bad second ranges}
      ValidTime:= False
  end;
end;



{============================================================================}
{  This function converts an international date type to a Julian date type.  }
{                                                                            }
{  Assumptions - this function makes some serious assumptions!               }
{                                                                            }
{  Errors returned                                                           }
{  badDate constant                                                          }
{                                                                            }
{  InParms : aDate - a date to convert                                       }
{  OutParms: None                                                            }
{  Returns : Julian date                                                     }
{============================================================================}

function DatetoJDate(aDate: TIntlDate): TJDate;
var
  year, month, day: word;

begin
  IntlExtractDate(aDate, year, month, day);

  if year < 100 then
  begin
    Inc(year, 1900);
    if year < Threshold2000 then
      Inc(year, 100);
  end;

  if not ValidDate(IntlPackDate(year, month, day)) then
    DatetoJDate := badDate
  else
    if (year = minYear) and (month < 3) then
      if month = 1 then
        DatetoJDate:= Pred(day)
      else
        DatetoJDate:= day+30
    else
    begin
      if month > 2 then
        Dec(month, 3)
      else
      begin
        Inc(month, 9);
        Dec(year);
      end;
      Dec(year, minYear);
      DatetoJDate:= ((longint(year div 100)*146097) div 4)+  {!!.09}
                    ((longint(year mod 100)*1461) div 4)+    {!!.09}
                    (((153*month)+2) div 5)+day+first2Months;
    end;
end;



{============================================================================}
{  This function converts an Julian date type to a international date type.  }
{                                                                            }
{  Assumptions                                                               }
{  None                                                                      }
{                                                                            }
{  Errors returned                                                           }
{  None                                                                      }
{                                                                            }
{  InParms : aJDate - a Julian date to convert                               }
{  OutParms: None                                                            }
{  Returns : an international date                                           }
{============================================================================}

function JDateToDate(aJDate: TJDate): TIntlDate;
var
  i, j : longint;
  year, month, day: word;

begin
  if aJDate = badDate then
  begin
    day:= 0;
    month:= 0;
    year:= 0;
  end
  else
    if aJDate <= first2Months then
    begin
      year:= minYear;
      if aJDate <= 30 then
      begin
        month:= 1;
        day:= Succ(aJDate);
      end
      else
      begin
        month:= 2;
        day:= aJDate-30;
      end;
    end
    else
    begin
      i:= (4*longint(aJDate-first2Months))-1;
      j:= (4*((i mod 146097) div 4))+3;
      year := (100*(i div 146097))+(j div 1461);
      i := (5*(((j mod 1461)+4) div 4))-3;
      month := i div 153;
      day := ((i mod 153)+5) div 5;
      if month < 10 then
        Inc(month, 3)
      else
      begin
        Dec(month, 9);
        Inc(year);
      end;
      Inc(year, minYear);
    end;
  JDateToDate:= IntlPackDate(year, month, day);
end;



{============================================================================}
{  Converts an international time type to amount of secs from midnight.      }
{                                                                            }
{  Assumptions                                                               }
{  None                                                                      }
{                                                                            }
{  Errors returned                                                           }
{  None                                                                      }
{                                                                            }
{  InParms : aTime - a time to convert                                       }
{  OutParms: None                                                            }
{  Returns : number of seconds                                               }
{============================================================================}

function TimetoSecs(aTime: TIntlTime): TSecs;
var
  tS                          : TSecs;
  hour, minute, second, hSecs : word;

begin
  IntlExtractTime(aTime, hour, minute, second, hSecs);
  hour:= hour mod hoursInDay; {make hour in 0 - 23 range}
  tS:= (longint(hour)*secondsInHour)+(longint(minute)*secondsInMinute)+second;
  TimeToSecs:= tS mod secondsInDay; {make seconds in 0 - secondsInDay range}
end;



{============================================================================}
{  This function converts a number of seconds to an international time type. }
{                                                                            }
{  Assumptions                                                               }
{  None                                                                      }
{                                                                            }
{  Errors returned                                                           }
{  None                                                                      }
{                                                                            }
{  InParms : aNumOfSecs - number of seconds to convert                       }
{  OutParms: None                                                            }
{  Returns : an international time                                           }
{============================================================================}

function SecsToTime(aNumOfSecs: TSecs): TIntlTime;
var
  hour, minute, second : word;

begin
  aNumOfSecs:= aNumOfSecs mod secondsInDay;{make secs in 0-secondsInDay range}
  hour:= aNumOfSecs div secondsInHour; {get hours in seconds}
  Dec(aNumOfSecs, longint(hour) * secondsInHour); {sub secs by hole hours}
  minute:= aNumOfSecs div secondsInMinute; {get minutes in seconds}
  Dec(aNumOfSecs, longint(minute) * secondsInMinute); {sub secs by hole mins}
  second:= aNumOfSecs; {amount remaing is seconds}
  SecsToTime:= IntlPackTime(hour, minute, second, 0);
end;



{============================================================================}
{  This function converts an internatinal date to a Julian date basing it on }
{  a 28 day month. Used by DecDate and IncDate.                              }
{                                                                            }
{  Assumptions                                                               }
{  None                                                                      }
{                                                                            }
{  Errors returned                                                           }
{  None                                                                      }
{                                                                            }
{  InParms : aDate - a date to convert                                       }
{  OutParms: None                                                            }
{  Returns : a Julian Date                                                   }
{============================================================================}

function AdjustDateToJDate(aDate: TIntlDate): TJDate;
var
  day, month, year : word;
  day28Delta       : integer;
  tmpJDate         : TJDate;

begin
  IntlExtractDate(aDate, year, month, day);
  Day28Delta := Integer(day)-28;
  if Day28Delta < 0 then
    Day28Delta := 0
  else
    day := 28;
  tmpJDate:= DateToJDate(IntlPackDate(year, month, day));
  if tmpJDate <> badDate then
    Inc(tmpJDate, day28Delta);
  AdjustDateToJDate:= tmpJDate;
end;



{============================================================================}
{  Adds the number of specified days to the date given.                      }
{                                                                            }
{  Assumptions                                                               }
{  None                                                                      }
{                                                                            }
{  Errors returned                                                           }
{  None                                                                      }
{                                                                            }
{  InParms : aDate - a base date to adjust                                   }
{            aNumOfDays - the number of days to increment; if negative, the  }
{                         days will be subtracted.                           }
{  OutParms: None                                                            }
{  Returns : an incremented international date                               }
{============================================================================}

function IncDate(aDate: TIntlDate; aNumOfDays: integer): TIntlDate;
var
  tmpJDate         : TJDate;

begin
  tmpJDate:= AdjustDateToJDate(aDate);
  if tmpJDate <> badDate then
    Inc(tmpJDate, aNumOfDays);
  IncDate:= JDateToDate(tmpJDate);
end;



{============================================================================}
{  This function subtracts the number of specified days from the date given. }
{                                                                            }
{  Assumptions                                                               }
{  None                                                                      }
{                                                                            }
{  Errors returned                                                           }
{  None                                                                      }
{                                                                            }
{  InParms : aDate - a base date to adjust                                   }
{            aNumOfDays - the number of days to decrement; if negative, the  }
{                         days will be added.                                }
{  OutParms: None                                                            }
{  Returns : an decremented international date                               }
{============================================================================}

function DecDate(aDate: TIntlDate; aNumOfDays: integer): TIntlDate;
var
  tmpJDate         : TJDate;

begin
  tmpJDate:= AdjustDateToJDate(aDate);
  if tmpJDate <> badDate then
    Dec(tmpJDate, aNumOfDays);
  DecDate := JDateToDate(tmpJDate);
end;



{============================================================================}
{  This function adds the number of specified seconds to the time given.     }
{                                                                            }
{  Assumptions                                                               }
{  None                                                                      }
{                                                                            }
{  Errors returned                                                           }
{  None                                                                      }
{                                                                            }
{  InParms : aTime - a base time to adjust                                   }
{            aNumOfSecs - the number of seconds to increment, if negative    }
{                         seconds will be subtracted.                        }
{  OutParms: None                                                            }
{  Returns : an incremented international time                               }
{============================================================================}

function IncTime(aTime: TIntlTime; aNumOfSecs: TSecs): TIntlTime;
var
  tmpSecs : TSecs;

begin
  tmpSecs:= TimeToSecs(aTime);
  if (aNumOfSecs < 0) and
     (aNumOfSecs > tmpSecs)  then {if seconds are negative, make sure secs}
  begin                                                     {stay positive}
    Dec(aNumOfSecs, tmpSecs); {after dec, remaining secs to sub from midnight}
                                                                 {on prev day}
    tmpSecs:= secondsInDay - aNumOfSecs; {get new prev day time}
  end
  else
    Inc(tmpSecs, aNumOfSecs);
  IncTime:= SecsToTime(tmpSecs);
end;



{============================================================================}
{  This function subtracts the number of specified seconds from the time     }
{  given.                                                                    }
{                                                                            }
{  Assumptions                                                               }
{  None                                                                      }
{                                                                            }
{  Errors returned                                                           }
{  None                                                                      }
{                                                                            }
{  InParms : aTime - a base time to adjust                                   }
{            aNumOfSecs - the number of seconds to decrement, if negative    }
{                         seconds will be added.                             }
{  OutParms: None                                                            }
{  Returns : an decremented international time                               }
{============================================================================}

function DecTime(aTime: TIntlTime; aNumOfSecs: TSecs): TIntlTime;
var
  tmpSecs : TSecs;

begin
  aNumOfSecs:= aNumOfSecs mod secondsInDay;
  tmpSecs:= TimeToSecs(aTime);
  if aNumOfSecs > tmpSecs then {make sure secs stay positive}
  begin
    Dec(aNumOfSecs, tmpSecs); {after dec, remaining secs to sub from midnight}
                                                                 {on prev day}
    tmpSecs:= secondsInDay - aNumOfSecs; {get new prev day time}
  end
  else
    Dec(tmpSecs, aNumOfSecs);
  DecTime:= SecsToTime(tmpSecs);
end;



{============================================================================}
{  This function calculates the absolute difference in seconds inbetween two }
{  date and time pairs.                                                      }
{                                                                            }
{  Assumptions                                                               }
{  None                                                                      }
{                                                                            }
{  Errors returned                                                           }
{  None                                                                      }
{                                                                            }
{  InParms : aDate1 - a date for pair one                                    }
{            aTime1 - a time for pair one                                    }
{            aDate2 - a date for pair two                                    }
{            aTime2 - a time for pair two                                    }
{  OutParms: None                                                            }
{  Returns : Number of seconds                                               }
{============================================================================}

function DateTimeDiffInSecs(aDate1: TIntlDate;
                            aTime1: TIntlTime;
                            aDate2: TIntlDate;
                            aTime2: TIntlTime): TSecs;
var
  jDate1, jDate2          : TJDate;
  tmpJDate                : TJDate;
  totSecs1, totSecs2      : TSecs;
  tTotSecs                : TSecs;
  dateDif                 : TJDate;
  timeDif                 : TSecs;
  dateDifInSecs           : TSecs;
  totDifInSecs            : TSecs;

begin
  {Convert date to a Julian}
  jDate1:= DateToJDate(aDate1);
  jDate2:= DateToJDate(aDate2);

  {Convert time to seconds}
  totSecs1:= TimeToSecs(aTime1);
  totSecs2:= TimeToSecs(aTime2);

  {Make Date1, Time1 to be larger of the two(more recent)}
  if (jDate1 < jDate2) or
     ((jDate1 = jDate2) and (totSecs1 < totSecs2)) then
  begin
    tmpJDate:= jDate1;
    jDate1:= jDate2;
    jDate2:= tmpJDate;
    tTotSecs:= totSecs1;
    totSecs1:= totSecs2;
    totSecs2:= tTotSecs;
  end;

  {If totSecs2 is larger than totSecs1, borrow a day from
   jDate1 and add it (in seconds) to totSecs1}
  if totSecs1 < totSecs2 then
  begin
    Dec(jDate1);
    Inc(totSecs1, secondsInDay);
  end;

  {Subtract Time}
  timeDif:= totSecs1 - totSecs2;
  {Subtract Dates}
  dateDif:= jDate1 - jDate2;
  {convert date diff to seconds}
  dateDifInSecs:= dateDif * secondsInDay;
  {Add difference between times}
  totDifInSecs:= dateDifInSecs + timeDif;
  {If dates were too far apart (24,850) set result to MaxLongInt}
  if dateDif > 24850 then
    DateTimeDiffInSecs:= maxLongint
  else
    DateTimeDiffInSecs:= totDifInSecs
end;



{============================================================================}
{ This function calculates the number of days between two dates. The value
{ returned includes one of the days and is the absolute value. E.g., given the
{ 2nd and 1st of May 1990, the function returns 1; not 0 or -1.
{
{ Assumptions:
{   a. TJDate types may be compared using the < relational operator.
{   b. TJDate types may be subtracted to obtain the number of days
{   c. dates are valid, non zero, and minYear/maxYear is such that this
{      function cannot overflow.
{   d. this function inherrets the serious assumptions in the DateToJDate
{      function.
{
{ Note: with the addition of this function to the unit, the
{   DateTimeDiffInSecs function above can be simplified if it's ever modified.
{
{============================================================================}

function DaysBetweenDates( date1, date2 : TIntlDate ) : longint;

  {==========================================================================}
  { This function switches two Julian dates
  {==========================================================================}
  procedure switch( date1, date2 : TJDate );
  var
    dateHeld : TJDate;
  begin
      dateHeld := date1;
      date1 := date2;
      date2 := dateHeld;
  end;

var
  jDate1, jDate2 : TJDate;
begin

  {---- Convert dates to Julian format }
  jDate1:= DateToJDate( date1 );
  jDate2:= DateToJDate( date2 );

  {---- To produce an absolute difference, date1 must be the larger. }
  if jDate1 < jDate2 then
    switch( jDate1, jDate2 );

  {---- To compute the inclusive difference, simply subtract the dates. }
  DaysBetweenDates := jDate1 - jDate2;
end;



{============================================================================}
{ This function compates two dates. It returns a value less than zero if date1
{  is before date2, zero if date1 is the same as date2, greater than zero
{  otherwise.
{
{ Assumptions:
{   This function indirectly inherrets the serious assumptions in the
{   DateToJDate function.
{============================================================================}

function CompareDates( date1, date2 : TIntlDate ) : integer;
var
  year1, month1, day1,
  year2, month2, day2 : word;
begin
  IntlExtractDate( date1, year1, month1, day1 );
  IntlExtractDate( date2, year2, month2, day2 );

  if year1 < year2 then
    CompareDates := -1
  else if year1 > year2 then
    CompareDates := 1

  { assert: same year }
  else if month1 < month2 then
    CompareDates := -1
  else if month1 > month2 then
    CompareDates := 1

  { assert: same month }
  else if day1 < day2 then
    CompareDates := -1
  else if day1 > day2 then
    CompareDates := 1

  { assert: same day }
  else
    CompareDates := 0;

end;

END.