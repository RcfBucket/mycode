Unit Parser;

{$T-}

{ This unit assumes the string SingleRecord has been initialized
  with the proper record to be parsed. This string is terminated by
  a carriage return.

  The Functions available are get a field from record, get a reapeat
  portion of a specifeid field, get a component portion of a specified
  field, get a component portion of a repeat field, and get the number
  of specified delimiters in a field.

  All functions except the counting of delimiters return a pointer
  to a string.
}


Interface

Uses
  WinTypes,
  Objects,
  WinProcs,
  WinCrt,
  Strings,
  StrsW,
	IntGlob;


Function Parser_GetField(FieldNum: Integer): PChar;
Function Parser_GetFieldRepeat(FieldNum: Integer; RepeatNum: Integer): PChar;
Function Parser_GetFieldComponent(FieldNum: Integer;
  ComponentNum: Integer): PChar;
Function Parser_GetFieldRepCom(FieldNum: Integer; RepeatNum: Integer;
  ComponentNum: Integer): PChar;
Function Parser_GetNumOfDelimiters(FieldNum: Integer;
  DelimiterType: Char): Integer;
Function Parser_GetNextField: PChar;
Function Parser_GetNextRepeatField: PChar;
Function Parser_GetNextComponentField: PChar;


Implementation

const
  NoOfDelimiterTypes = 4;
  CR = #13;

var
  CurrentIndex: Integer;

Function FindField(FieldNum: Integer): Integer;
{ Find specified field in record and return record index pointing to
  first character in field. If record is of type header, 'H', then
  initialize record index and field count to third field.
}

var
  CharIndex: Integer;
  CurrentFieldNum: Integer;

begin
  CurrentFieldNum := 1;
	CharIndex := 0;
  if (RecordID = 'H') and (FieldNum > 2) then
  begin
		CharIndex := NoOfDelimiterTypes + 4;
    CurrentFieldNum := 3;
  end;
  if FieldNum <> 0 then
    while (CurrentFieldNum <> FieldNum) and (SingleRecord[CharIndex] <> CR) do
    begin
      if SingleRecord[CharIndex] = FieldDelimiter then
        CurrentFieldNum := CurrentFieldNum + 1;
      CharIndex := CharIndex + 1;
    end;
  {if CurrentField < FieldNum then
     !error - field specified is beyond record;}
  FindField := CharIndex;
end;

Function Parser_GetField(FieldNum: Integer): PChar;
{ Find field and then get characters of field until reaching end of
  field or end of record.
}

var
  TempStr, TempStr2: array[0..255] of char;
  CharIndex: Integer;
  CharCount: Integer;

begin
  CharCount := 0;
  StrCopy(TempStr, '');
  CharIndex := FindField(FieldNum);
  while (SingleRecord[CharIndex] <> FieldDelimiter)
    and (SingleRecord[CharIndex] <> CR) do
  begin
    TempStr[CharCount] := SingleRecord[CharIndex];
    CharCount := CharCount + 1;
    CharIndex := CharIndex + 1;
  end;
  CurrentIndex := CharIndex;
  TempStr[CharCount] := #0;
  TrimAll(TempStr2, TempStr, ' ', 255);
  Parser_GetField := @TempStr2;
end;

Function Parser_GetNextField: PChar;
{ Get next field and then get characters of field until reaching end of
  field or end of record. Note: Parser_GetField must be called once
  before this routine is called.
}

var
  TempStr, TempStr2: array[0..255] of char;
  CharIndex: Integer;
  CharCount: Integer;

begin
  CharCount := 0;
  StrCopy(TempStr, '');
  CharIndex := CurrentIndex;
  if SingleRecord[CharIndex] <> CR then
  begin
    CharIndex := CharIndex + 1;
    while (SingleRecord[CharIndex] <> FieldDelimiter)
      and (SingleRecord[CharIndex] <> CR) do
    begin
      TempStr[CharCount] := SingleRecord[CharIndex];
      CharCount := CharCount + 1;
      CharIndex := CharIndex + 1;
    end;
  end;
  CurrentIndex := CharIndex;
  TempStr[CharCount] := #0;
  TrimAll(TempStr2, TempStr, ' ', 255);
  Parser_GetNextField := @TempStr2;
end;

Function FindRepeat(CharIndex: Integer; RepeatNum: Integer): Integer;
{ In field, find specified repeat field and return pointer to first
  character in repeat field.
}

var
  CurrentRepeatNum: Integer;

begin
  CurrentRepeatNum := 1;
  if RepeatNum <> 0 then
    while (CurrentRepeatNum <> RepeatNum)
      and (SingleRecord[CharIndex] <> CR)
      and (SingleRecord[CharIndex] <> FieldDelimiter) do
    begin
      if SingleRecord[CharIndex] = RepeatDelimiter then
        CurrentRepeatNum := CurrentRepeatNum + 1;
      CharIndex := CharIndex + 1;
    end;
  {if SingleRecord[CharIndex] = CR then
    !error - repeat field not found, end of record reached;
   if SingleRecord[CharIndex] = FieldDelimiter then
    !error - repeat field not found, end of field reached;}
  FindRepeat := CharIndex;
end;

Function Parser_GetFieldRepeat(FieldNum: Integer; RepeatNum: Integer): PChar;
{ Find field and then locate specified repeat number, transfer characters
  until end of field, repeat, or record.
}

var
  CharIndex: Integer;
  TempStr, TempStr2: array[0..255] of char;
  CharCount: Integer;

begin
  StrCopy(TempStr, '');
  CharCount := 0;
  CharIndex := FindField(FieldNum);
  CharIndex := FindRepeat(CharIndex, RepeatNum);
  while (SingleRecord[CharIndex] <> FieldDelimiter)
    and (SingleRecord[CharIndex] <> RepeatDelimiter)
    and (SingleRecord[CharIndex] <> CR) do
  begin
    TempStr[CharCount] := SingleRecord[CharIndex];
    CharCount := CharCount + 1;
    CharIndex := CharIndex + 1;
  end;
  CurrentIndex := CharIndex;
  TempStr[CharCount] := #0;
  TrimAll(TempStr2, TempStr, ' ', 255);
  Parser_GetFieldRepeat := @TempStr2;
end;

Function Parser_GetNextRepeatField: PChar;
{ Get next repeat field and transfer characters  until end of field,
  repeat, or record. Note: Parser_GetFieldRepeat must be called
  before this routine.
}

var
  CharIndex: Integer;
  TempStr, TempStr2: array[0..255] of char;
  CharCount: Integer;

begin
  StrCopy(TempStr, '');
  CharCount := 0;
  CharIndex := CurrentIndex;
  if   (SingleRecord[CharIndex] <> CR)
    or (SingleRecord[CharIndex] <> FieldDelimiter) then
  begin
    CharIndex := CharIndex + 1;
    while (SingleRecord[CharIndex] <> FieldDelimiter)
      and (SingleRecord[CharIndex] <> RepeatDelimiter)
      and (SingleRecord[CharIndex] <> CR) do
    begin
      TempStr[CharCount] := SingleRecord[CharIndex];
      CharCount := CharCount + 1;
      CharIndex := CharIndex + 1;
    end;
  end;
  CurrentIndex := CharIndex;
  TempStr[CharCount] := #0;
  TrimAll(TempStr2, TempStr, ' ', 255);
  Parser_GetNextRepeatField := @TempStr2;
end;

Function FindComponent(CharIndex: Integer; ComponentNum: Integer): Integer;
{ Locate desired component in specified field, read chars until
  arriving at the desired component, return pointer to first character
  in component.
}

var
  CurrentComponentNum: Integer;

begin
  CurrentComponentNum := 1;
  if ComponentNum <> 0 then
    while (CurrentComponentNum <> ComponentNum)
      and (SingleRecord[CharIndex] <> CR)
      and (SingleRecord[CharIndex] <> FieldDelimiter) do
    begin
      if SingleRecord[CharIndex] = ComponentDelimiter then
        CurrentComponentNum := CurrentComponentNum + 1;
      CharIndex := CharIndex + 1;
    end;
  {if SingleRecord[CharIndex] = CR then
    !error - component not found, end of record reached;
   if SingleRecord[CharIndex] = FieldDelimiter then
    !error - component not found, end of field reached;}
  FindComponent := CharIndex;
end;

Function Parser_GetFieldComponent(FieldNum: Integer;
  ComponentNum: Integer): PChar;
{ Find field and then locate specified component number, transfer characters
  until end of field, component, or record.
}
var
  CharIndex: Integer;
  TempStr, TempStr2: array[0..255] of char;
  CharCount: Integer;

begin
  StrCopy(TempStr, '');
  CharCount := 0;
  CharIndex := FindField(FieldNum);
  CharIndex := FindComponent(CharIndex, ComponentNum);
  while (SingleRecord[CharIndex] <> FieldDelimiter)
    and (SingleRecord[CharIndex] <> ComponentDelimiter)
    and (SingleRecord[CharIndex] <> CR) do
  begin
    TempStr[CharCount] := SingleRecord[CharIndex];
    CharCount := CharCount + 1;
    CharIndex := CharIndex + 1;
  end;
  CurrentIndex := CharIndex;
  TempStr[CharCount] := #0;
  TrimAll(TempStr2, TempStr, ' ', 255);
  Parser_GetFieldComponent := @TempStr2;
end;

Function Parser_GetNextComponentField: Pchar;
{ Get component and transfer characters  until end of field, component,
  or record.
}
var
  CharIndex: Integer;
  TempStr, TempStr2: array[0..255] of char;
  CharCount: Integer;

begin
  StrCopy(TempStr, '');
  CharCount := 0;
  CharIndex := CurrentIndex;
  if   (SingleRecord[CharIndex] <> CR)
    or (SingleRecord[CharIndex] <> FieldDelimiter) then
  begin
    CharIndex := CharIndex + 1;
    while (SingleRecord[CharIndex] <> FieldDelimiter)
      and (SingleRecord[CharIndex] <> ComponentDelimiter)
      and (SingleRecord[CharIndex] <> CR) do
    begin
      TempStr[CharCount] := SingleRecord[CharIndex];
      CharCount := CharCount + 1;
      CharIndex := CharIndex + 1;
    end;
  end;
  CurrentIndex := CharIndex;
  TempStr[CharCount] := #0;
  TrimAll(TempStr2, TempStr, ' ', 255);
  Parser_GetNextComponentField := @TempStr2;
end;

Function Parser_GetFieldRepCom(FieldNum: Integer; RepeatNum: Integer;
  ComponentNum: Integer): PChar;
{ Find field, repeat field and component in that repeat field. Transfer
  characters until end of record, field, component of repeat field.
}

var
  CharIndex: Integer;
  TempStr, TempStr2: array[0..255] of char;
  CharCount: Integer;

begin
  StrCopy(TempStr, '');
  CharCount := 0;
  CharIndex := FindField(FieldNum);
  CharIndex := FindRepeat(CharIndex, RepeatNum);
  CharIndex := FindComponent(CharIndex, ComponentNum);
  while (SingleRecord[CharIndex] <> FieldDelimiter)
    and (SingleRecord[CharIndex] <> ComponentDelimiter)
    and (SingleRecord[CharIndex] <> RepeatDelimiter)
    and (SingleRecord[CharIndex] <> CR) do
  begin
    TempStr[CharCount] := SingleRecord[CharIndex];
    CharCount := CharCount + 1;
    CharIndex := CharIndex + 1;
  end;
  TempStr[CharCount] := #0;
  TrimAll(TempStr2, TempStr, ' ', 255);
  Parser_GetFieldRepCom := @TempStr2;
end;


Function Parser_GetNumOfDelimiters(FieldNum: Integer;
   DelimiterType: Char): Integer;
{ In specified field, count the number of desired delimiters and return
  number.
}

var
  Count: Integer;
  CharIndex: Integer;

begin
  Count := 0;
  CharIndex := FindField(FieldNum);
  while (SingleRecord[CharIndex] <> FieldDelimiter)
    and (SingleRecord[CharIndex] <> CR) do
  begin
    if SingleRecord[CharIndex] = DelimiterType then
      Count := Count + 1;
    CharIndex := CharIndex + 1;
  end;
  Parser_GetNumOfDelimiters := Count;
end;

Begin
End.
