program PrtMain;

{- Main report printing program.  Provides access to all DMS reports }

uses
  PrtLPR,
  PrtEPI,
  Strings,
  PatReps,
  GridLib,
  APITools,
  CtlLib,
  WinTypes,
  WinProcs,
  Win31,
  UserMsgs,
  MTask,
  MScan,
  INILib,
  DlgLib,
  Ctl3d,
  ODialogs,
  OWindows;

{$R PRTMAIN.RES}
{$I PRTMAIN.INC}

type
  TReportApp = object(T3dApplication)
    procedure InitMainWindow; virtual;
  end;

  PReportMain = ^TReportMain;
  TReportMain = object(TCenterDlgWindow)
    grid      : PGrid;
    userRules : PLCheckBox;
    {}
    constructor Init;
    procedure SetupWindow; virtual;
    procedure PrintIt;
    procedure IDPrint(var msg: TMessage); virtual ID_FIRST + IDC_SELECT;
    procedure IDGrid(var msg: TMessage); virtual ID_FIRST + IDC_LIST;
    procedure WMCharToItem(var msg: TMessage);  virtual WM_FIRST + WM_CHARTOITEM;
    procedure WMDrawItem(var msg: TMessage);    virtual WM_FIRST + WM_DRAWITEM;
    procedure WMMeasureItem(var msg: TMessage); virtual WM_FIRST + WM_MEASUREITEM;
    procedure WMDMSQuery(var msg: TMessage); virtual WM_FIRST + WM_MAINPROGQUERY;
    procedure GetWindowClass(var aWndClass: TWndClass); virtual;
    function GetClassName: PChar; virtual;
  end;

procedure TReportApp.InitMainWindow;
begin
  if INIUse3D then
    Register3DApp(true, true, true);
  mainWindow:= New(PReportMain, Init);
end;

constructor TReportMain.Init;
begin
  inherited Init(nil, MakeIntResource(DLG_RPTMAIN));
  grid:= New(PGrid, InitResource(@self, IDC_LIST, 1, false));
  userRules:= New(PLCheckBox, InitResource(@self, IDC_RULES, false));
end;

procedure TReportMain.SetupWindow;
var
  pstr    : array[0..100] of char;

  procedure AddOne(item: integer);
  var
    row       : integer;
  begin
    row:= grid^.AddString('');
    grid^.SetData(0, row, SR(item, pstr, sizeof(pstr)-1));
    grid^.SetItemData(row, item);
  end;

begin
  inherited SetupWindow;
  grid^.SetHeader(0, SR(IDS_RPTHEADER, pstr, sizeof(pstr)-1));
  grid^.StretchColumn(0);
  AddOne(IDS_RPTLPR);
  AddOne(IDS_RPTSPR);
  AddOne(IDS_RPTIsoLog);
  AddOne(IDS_RPTSpecLog);
  AddOne(IDS_RPTEPI);
  grid^.SetSelIndex(0);
  SendDlgItemMsg(IDC_RULES, bm_SetCheck, 1, 0);
end;

procedure TReportMain.PrintIt;
var
  k     : integer;
  p1, p2: array[0..100] of char;
begin
  k:= grid^.GetSelIndex;
  if k >= 0 then
  begin
    case grid^.GetItemData(k) of
      IDS_RPTLPR:     LongPatReports(@self, (userRules^.GetCheck = BF_CHECKED));
      IDS_RPTSPR:     PrintSFR(@self, (userRules^.GetCheck = BF_CHECKED));
      IDS_RPTIsoLog:  PrintIsolateLog(@self, (userRules^.GetCheck = BF_CHECKED));
      IDS_RPTSpecLog: PrintSpecimenLog(@self, (userRules^.GetCheck = BF_CHECKED));
      IDS_RPTEPI:     PrintEPIReports(@self, (userRules^.GetCheck = BF_CHECKED));
      else
      begin
        strcopy(p1, 'Print report : ');
        grid^.GetData(0, k, p2, sizeof(p2)-1);
        strcat(p1, p2);
        strcat(p1, ' (');
        str(grid^.GetItemData(k), p2);
        strcat(p1, p2);
        strcat(p1, ')');
        InfoMsg(HWindow, 'Print', p1);
      end;
    end;
  end;
end;

procedure TReportMain.IDPrint(var msg: TMessage);
begin
  PrintIt;
end;

procedure TReportMain.WMCharToItem(var msg: TMessage);
begin
  grid^.CharToItem(msg);
end;

procedure TReportMain.WMDrawItem(var msg: TMessage);
begin
  grid^.DrawItem(msg);
end;

procedure TReportMain.WMMeasureItem(var msg: TMessage);
begin
  grid^.MeasureItem(msg);
end;

procedure TReportMain.IDGrid(var msg: TMessage);
begin
  DefWndProc(msg);
  if msg.lParamHi = LBN_DBLCLK then
    PrintIt;
end;

procedure TReportMain.WMDMSQuery(var msg: TMessage);
{- respond to the task module and tell it who I am }
begin
  StrLCopy(PChar(msg.lParam), application^.Name, msg.wParam);
  msg.result:= DMSMainProgRetCode;
  SetWindowLong(HWindow, DWL_MSGRESULT, msg.result);  {- set message result (for dialogs) }
end;

function TReportMain.GetClassName: PChar;
begin
  GetClassName:= application^.name;
end;

procedure TReportMain.GetWindowClass(var aWndClass: TWndClass);
begin
  inherited GetWindowClass(aWndClass);
  aWndClass.hIcon:= LoadIcon(HInstance, MakeIntResource(ICON_1));
end;



var
  rptApp  : TReportApp;
BEGIN
  if OKToRun(MOD_PRTMAIN, false) then
  begin
    rptApp.Init(GetModuleName(MOD_PRTMAIN));
    rptApp.Run;
    rptApp.Done;
  end;
END.

{- rcf }
