{----------------------------------------------------------------------------}
{  Module Name  : WAMaint.PAS                                                }
{  Programmer   : EJ                                                         }
{  Date Created : 05/28/95                                                   }
{                                                                            }
{  Purpose -                                                                 }
{  This module provides the user with maint functions available in           }
{  the WA INTERFACE.                                                         }
{                                                                            }
{  Assumptions -                                                             }
{  None.                                                                     }
{                                                                            }
{  Revision History -                                                        }
{                                                                            }
{  Version  Date      Who    Did What                                        }
{  -------  --------  ------ ----------------------------------------------- }
{  1.00     05/28/95  EJ     Initial release.                                }
{                                                                            }
{----------------------------------------------------------------------------}

unit WAMaint;

INTERFACE

uses
  Objects,
  OWindows;

type
  PHdwrStatus = ^THdwrStatus;
  THdwrStatus = object(TObject)
    processor,
    pRom,
    RamMem,
    Lamp,
    PhotoBoard,
    Temperature,
    Motor,
    Dispenser,
    Comms,
    fluor       : integer;
    {}
    constructor Init;
  end;


procedure AccessWA(aParent: PWindowsObject; var somethingHappened: boolean);
procedure WAMaintenance(aParent: PWindowsObject; aStatBuf: PChar; var somethingHappened: boolean);


IMPLEMENTATION

uses
  ODialogs,
  WinProcs,
  WinTypes,
  Strings,
  DMString,
  Bits,
  MScan,
  DlgLib,
  CtlLib,
  APITools,
  IntlLib,
  DateLib,
  WaitLib,
  UserMsgs,
  WAUtil,
  WAGPIB,
  JamCheck,
  DMSErr,
  DMSDebug;

{$I WAMAIN.INC}

const
  secondsInDay      = 86400;  {number of seconds in a day}

type
  PAccessWADLg = ^TAccessWADlg;
  TAccessWADLg = object(TCenterDlg)
    startTime    : longint;
    endTime      : longint;
    MinStr       : array [0..2] of char;
    timerOn      : boolean;
    userCantWait : boolean;
    doorIsOpen   : boolean;
    timeGrp      : PRadioGroup;
    waType       : integer;
    {}
    constructor Init(aParent: PWindowsObject; aName: PChar);
    destructor Done; virtual;
    procedure SetUpWindow; virtual;
    procedure OpenDoor(var msg : TMessage); virtual id_First + IDC_OPENDOOR;
    procedure Cancel(var msg : TMessage); virtual id_First + IDCANCEL;
    procedure TimerTick(var msg: TMessage); virtual wm_First + wm_Timer;
    procedure StopTimer; virtual;
    procedure TimesUp; virtual;
    function  GetNextTime: boolean; virtual;
    function  WAOpenDoor(var howLong, whenOpen: integer): boolean; virtual;
    function  WACloseDoor: boolean; virtual;
    function  WaitForSTIResponse: boolean; virtual;
    procedure WaitForDoorToClose; virtual;
    procedure EnableButtons; virtual;
  end;

  PWAClockDlg = ^TWAClockDlg;
  TWAClockDlg = object(TCenterDlg)
    WATime   : TimeDate;
    {}
    constructor Init(aParent: PWindowsObject; aName: PChar);
    procedure SetUpWindow; virtual;
    procedure OK(var msg: TMessage); virtual id_First + IDOK;
  end;

  PWAReagentDlg = ^TWAReagentDlg;
  TWAReagentDlg = object(TCenterDlg)
    AlphaNap, PotHydrox, SulfAcid, Dimethyl, Peptidase, FerricChlor,
    Kovacs, HNIDIndole, RapidIndole, SodiumHydrox : integer;
    chkAlphaNap,
    chkPotHydrox,
    chkSulfAcid,
    chkDimethyl,
    chkPeptidase,
    chkFerricChlor,
    chkKovacs,
    chkHNIDIndole,
    chkRapidIndole,
    chkSodiumHydrox : PLCheckBox;
    {}
    constructor Init(aParent: PWindowsObject; aName: PChar);
    destructor Done; virtual;
    procedure SetUpWindow; virtual;
    { OK means to purge the reagents }
    procedure OK(var msg: TMessage); virtual id_First + IDOK;
    procedure SetAutoPurge(var msg: TMessage); virtual id_First + IDC_AUTOPURGE;
  end;

  PWAMaintMainDlg = ^TWAMaintMainDlg;
  TWAMaintMainDlg = object(TAccessWADlg)
    accessAcquired : ^boolean;
    statBuf        : PChar;
    {}
    constructor Init(aParent: PWindowsObject; aName: PChar;
                     aStatBuf: PChar; var somethingHappened: boolean);
    procedure SetUpWindow; virtual;
    procedure PurgeReagents(var msg: TMessage); virtual id_First + IDC_REAGENTS;
    procedure SetWAClock(var msg: TMessage); virtual id_First + IDC_WACLOCK;
    procedure ReferenceDisk(var msg: TMessage); virtual id_First + IDC_WACLEAN;
    procedure ShieldClean(var msg: TMessage); virtual id_First + IDC_WASHIELD;
    procedure ShowWAStatus(var msg: TMessage); virtual wm_First + WM_WASTATUS;
    function  WAOpenDoor(var howLong, whenOpen: integer): boolean; virtual;
    function  CannotCloseDoor: boolean;
    procedure TimesUp; virtual;
  end;


{ Methods for THdwrStatus }

constructor THdwrStatus.Init;
var
  waQstCmd   : PWAQSTCmd;
  idPhotoCmd : PWAIDPHOTOcmd;
  readRegCmd : PWAREADREGcmd;
begin
  inherited Init;
  { check various hardware components in WA and
    report OK or NOK (or OFF) to dialog box }
  waQstCmd := New(PWAQSTCmd, Init(WAID));
  if not waQstCmd^.SendWAMessage then
  begin
    processor   := 0;
    pRom        := 0;
    RamMem      := 0;
    Lamp        := 0;
    PhotoBoard  := 0;
    Temperature := 0;
    Motor       := 0;
    Dispenser   := 0;
    Comms       := 0;
    Fluor       := 0;
  end
  else
  begin
    {** Processor system - CPU+PROM+RAM **}
    processor   := integer(TestBit(waQstCmd^.recvMsg^.qstStat, 0));
    pRom        := integer(TestBit(waQstCmd^.recvMsg^.qstStat, 1));
    RamMem      := integer(TestBit(waQstCmd^.recvMsg^.qstStat, 2));
    Lamp        := integer(TestBit(waQstCmd^.recvMsg^.qstStat, 3));
    PhotoBoard  := integer(TestBit(waQstCmd^.recvMsg^.qstStat, 4));
    Temperature := integer(TestBit(waQstCmd^.recvMsg^.qstStat, 5));
    Motor       := integer(TestBit(waQstCmd^.recvMsg^.qstStat, 6));
    Dispenser   := integer(TestBit(waQstCmd^.recvMsg^.qstStat, 7));
    Comms       := integer(TestBit(waQstCmd^.recvMsg^.qstStat, 8));
    Fluor       := integer(TestBit(waQstCmd^.recvMsg^.qstStat, 9));
    idPhotoCmd := New(PWAIDPHOTOcmd, Init(WAID));
    if idPhotoCmd^.SendWAMessage then
    begin
      if idPhotoCmd^.recvMsg^.psiPid <> 1 then
      begin
        readRegCmd := New(PWAREADREGcmd, Init(WAID));
        readRegCmd^.sendMsg^.regNum := 16;     { read register 16 }
        if readRegCmd^.SendWAMessage then
        begin
          if TestBit(readRegCmd^.recvMsg^.regValue, 1) then { Fluor Bit (set is disabled) }
            Fluor := -1;
        end;
        MSDisposeObj(readRegCmd);
      end;
    end;
    MSDisposeObj(idPhotoCmd);
  end;
  if waQstCmd^.WAErrNum <> 0 then
    ShowError(nil, IDS_WA_ERR, nil, waQstCmd^.WAErrNum, MOD_WAMAINT,    1)
  else if waQstCmd^.WAStatNum <> 0 then
    ShowError(nil, IDS_WA_ERR, nil, waQstCmd^.WAStatNum, MOD_WAMAINT,   2);
  MSDisposeObj(waQstCmd);
end;


{ Methods for TAccessWADlg }

{-------------------------------------------------------[ TAccessWADlg ]--}
constructor TAccessWADlg.Init(aParent: PWindowsObject; aName: PChar);
var
  c     : PControl;
  waCmd : PWASTICmd;
begin
  inherited Init(aParent, aName);
  timerOn := FALSE;
  userCantWait := FALSE;
  doorIsOpen := FALSE;
  timeGrp := New(PRadioGroup, Init(@self, IDC_1_MINUTE, 1, FALSE));
  timeGrp^.AddToGroup(IDC_5_MINUTE, 5);
  timeGrp^.AddToGroup(IDC_15_MINUTE, 15);
  timeGrp^.AddToGroup(IDC_30_MINUTE, 30);
  waType := WA[WAID].waType;
  waCmd := New(PWASTICmd, Init(WAID));
  if waCmd^.SendWAMessage then
    doorIsOpen := TestBit(waCmd^.recvMsg^.stiBits, AccessGranted)
  else
  begin
    MSDisposeObj(waCmd);
    Done;
    Fail;
  end;
  MSDisposeObj(waCmd);
end;

destructor TAccessWADlg.Done;
begin
  StopTimer;
  WACloseDoor;
  MSDisposeObj(timeGrp);
  inherited Done;
end;

procedure TAccessWADlg.SetupWindow;
begin
  inherited SetUpWindow;
  timeGrp^.SetSelected(5);
  SetDlgItemText(HWindow, IDC_DOOROPEN, '');
end;

procedure TAccessWADlg.OpenDoor(var msg: TMessage);
var
  howLong  : integer;
  whenOpen : integer;
  temp     : array [0..64] of char;
  tNum     : array [0..16] of char;
  wndw     : HWnd;
begin
  if doorIsOpen then
    Exit;

  EnableWindow(GetItemHandle(IDC_OPENDOOR), FALSE);
  StopTimer;
  howLong := timeGrp^.SelectedVal;
  { show please wait }
  SR(IDS_WAIT, temp, 64);
  SetDlgItemText(HWindow, IDC_WASTAT, temp);
  if WAOpenDoor(howLong, whenOpen) then
  begin
    EnableWindow(GetItemHandle(IDC_1_MINUTE), FALSE);
    EnableWindow(GetItemHandle(IDC_5_MINUTE), FALSE);
    EnableWindow(GetItemHandle(IDC_15_MINUTE), FALSE);
    EnableWindow(GetItemHandle(IDC_30_MINUTE), FALSE);
    Str(whenOpen, tNum);
    SetDlgItemText(HWindow, IDC_DOOROPEN, tNum);
    doorIsOpen := TRUE;
    if WaitForSTIResponse then
    begin
      startTime := TimetoSecs(IntlCurrentTime);
      endTime := startTime + (howLong * 60);
      SetTimer(HWindow, IDT_ACCESS, 1000, nil);
      timerOn := TRUE;
    end;
  end;
  { erase please wait and show time remaining }
  SR(IDS_TIME_LEFT, temp, 63);
  SetDlgItemText(HWindow, IDC_WASTAT, temp);

  if timerOn then
    SetFocus(GetItemHandle(IDCANCEL))
  else
  begin
    EnableWindow(GetItemHandle(IDC_1_MINUTE), TRUE);
    EnableWindow(GetItemHandle(IDC_5_MINUTE), TRUE);
    EnableWindow(GetItemHandle(IDC_15_MINUTE), TRUE);
    EnableWindow(GetItemHandle(IDC_30_MINUTE), TRUE);
  end;
  EnableButtons;
end;

procedure TAccessWADlg.Cancel(var msg: TMessage);
begin
  if doorIsOpen then
  begin
    userCantWait := TRUE;
    EndDlg(IDOK);
  end
  else
    EndDlg(IDCANCEL);
end;

procedure TAccessWADlg.TimerTick(var msg: TMessage);
begin
  if not GetNextTime then
  begin
    StopTimer;
    TimesUp;
  end
  else
    SetDlgItemText(HWindow, IDC_DOOROPEN, MinStr);
  DefWndProc(msg);
end;

procedure TAccessWADlg.StopTimer;
begin
  if timerOn then
  begin
    KillTimer(HWindow, IDT_ACCESS);
    timerOn := FALSE;
  end;
end;

procedure TAccessWADlg.TimesUp;
begin
  SetDlgItemText(HWindow, IDC_DOOROPEN, '0');
  WaitForDoorToClose;
  EndDlg(IDOK);
END;

function TAccessWADlg.GetNextTime: boolean;
var
  h, m, s, code : integer;
  tStr          : string;
  cTime         : TSecs;
begin
  GetNextTime := TRUE;
  cTime := TimetoSecs(IntlCurrentTime);
  if (cTime < startTime) and (endTime >= secondsInDay) then
    Inc(cTime, secondsInDay);

  cTime := endTime - cTime;
  if cTime <= 0 then
  begin
    GetNextTime := FALSE;
    Exit;
  end;
  Str((cTime div 60) + integer((cTime mod 60) <> 0), MinStr);
end;

function TAccessWADlg.WAOpenDoor(var howLong, whenOpen: integer): boolean;
var
  waCmd : PWALARCmd;
begin
  WAOpenDoor := FALSE;
  waCmd := New(PWALARCmd, Init(WAID));
  waCmd^.SetParams(byte(howLong));
  if waCmd^.SendWAMessage then
  begin
    howLong := waCmd^.recvMsg^.arDuratn;
    whenOpen := waCmd^.recvMsg^.arDelay;
    WAOpenDoor := TRUE;
  end
  else
  begin
    howLong := 0;
    whenOpen := 0;
  end;
  if waCmd^.WAErrNum <> 0 then
    ShowError(parent, IDS_WA_ERR, nil, waCmd^.WAErrNum, MOD_WAMAINT, 1);
  if waCmd^.WAStatNum <> 0 then
    ShowError(parent, IDS_WA_ERR, nil, waCmd^.WAStatNum, MOD_WAMAINT, 2);
  MSDisposeObj(waCmd);
end;

function TAccessWADlg.WACloseDoor: boolean;
var
  waCmd    : PWACARCmd;
  WAROMcmd : PWAROMCMDCmd;
begin
  WACloseDoor := TRUE;
  if doorIsOpen then
  begin
    WACloseDoor := FALSE;
    { turn off the alarm }
    WAROMcmd := New(PWAROMCMDCmd, Init(WAID));
    WAROMcmd^.SetParams($31, 0);
    WAROMcmd^.SendWAMessage;
    MSDisposeObj(WAROMcmd);
    waCmd := New(PWACARCmd, Init(WAID));
    if waCmd^.SendWAMessage then
    begin
      doorIsOpen := FALSE;
      WACloseDoor := TRUE;
    end;
    MSDisposeObj(waCmd);
  end;
end;

function TAccessWADlg.WaitForSTIResponse: boolean;
var
  waCmd   : PWASTICmd;
  okFine  : boolean;
begin
  waCmd := New(PWASTICmd, Init(WAID));
  okFine := TRUE;
  repeat
    MSWait(2000);
(*  MSWait(5000); *)
    if not waCmd^.SendWAMessage then
      okFine := FALSE;
    HandleEvents(hWindow);
  until TestBit(waCmd^.recvMsg^.stiBits, AccessGranted) or
        not okFine or (userCantWait);
  MSDisposeObj(waCmd);
  WaitForStiResponse := okFine;
end;

procedure TAccessWADlg.WaitForDoorToClose;
var
  waCmd  : PWASTICmd;
  okFine : boolean;
  temp   : array [0..64] of char;
  s      : array [0..16] of char;
begin
  SR(IDS_CLOSE_DOOR, temp, 64);
  SR(IDS_WARNING, s, 16);
  if waType = ClassicWA then
  begin
    waCmd := New(PWASTICmd, Init(WAID));
    okFine := FALSE;
    repeat
      MSWait(5000);
      if not waCmd^.SendWAMessage then
        Break;
      if TestBit(waCmd^.recvMsg^.stiBits, AccessTimeExpired) then
      begin
        InfoMsg(HWindow, s, temp);
      end
      else
        okFine := TRUE;
    until okFine;
    MSDisposeObj(waCmd);
  end
  else
  begin
    MSWait(5000);
    InfoMsg(HWindow, s, temp);
  end;
end;

procedure TAccessWADlg.EnableButtons;
begin
  EnableWindow(GetItemHandle(IDC_OPENDOOR), not doorIsOpen);
end;

{ Methods for TWAClockDlg }

constructor TWAClockDlg.Init(aParent: PWindowsObject; aName: PChar);
var
  c   : PControl;
begin
  inherited Init(aParent, aName);
  c := New(PLStatic, InitResource(@self, IDC_LBL1, 0, FALSE));
  c := New(PLStatic, InitResource(@self, IDC_LBL2, 0, FALSE));
end;

procedure TWAClockDlg.SetUpWindow;
var
  tempDate,
  tempTime  : array[0..32] of char;
begin
  inherited SetUpWindow;
  IntlDateStr(IntlCurrentDate, tempDate, 32);
  IntlTimeStr(IntlCurrentTime, FALSE, tempTime, 32);
  SetDlgItemText(HWindow, IDC_WADATE, tempDate);
  SetDlgItemText(HWindow, IDC_WATIME, tempTime);
end;

procedure TWAClockDlg.OK(var msg: TMessage);
var
  waCmd     : PWASSTCmd;
  tempDate,
  tempTime  : array[0..32] of char;
  id        : TIntlDate;
  it        : TIntlTime;
  err       : boolean;
  year, month, day, hour, min, sec, hsec : word;

  procedure InvalidEntry(fieldName: PChar);
  var
    InvalidMsg : array[0..64] Of char;
    title      : array[0..32] Of char;
  begin
    SR(IDS_INVALID_FLD, InvalidMsg, 64);
    SR(IDS_ERROR, title, 32);
    StrLCat(InvalidMsg, fieldName, 64);
    ErrorMsg(HWindow, title, InvalidMsg);
  end;

begin
  GetDlgItemText(HWindow, IDC_WADATE, tempDate, 32);
  GetDlgItemText(HWindow, IDC_WATIME, tempTime, 32);
  { set WA date and time }
  id:= IntlStrToDate(tempDate, err);
  if not err and ValidDate(id) then
  begin
    it := IntlStrToTime(tempTime, err);
    if not err and ValidTime(it) then
    begin
      waCmd := New(PWASSTCmd, Init(WAID));
      IntlExtractDate(id, year, month, day);
      IntlExtractTime(it, hour, min, sec, hsec);
      WATime.year := year;
      WATime.month := month;
      WATime.day := day;
      WATime.hour := hour;
      WATime.min := min;
      WATime.sec := sec;
      waCmd^.SetParams(WATime);
      waCmd^.SendWAMessage;
      if waCmd^.WAErrNum <> 0 then
        ShowError(@self, IDS_WA_ERR, nil, waCmd^.WAErrNum, MOD_WAMAINT, 1);
      if waCmd^.WAStatNum <> 0 then
        ShowError(@self, IDS_WA_ERR, nil, waCmd^.WAStatNum, MOD_WAMAINT, 2);
      MSDisposeObj(waCmd);
      inherited OK(msg);
    end
    else
    begin
      GetDlgItemText(HWindow, IDC_LBL2, tempTime, 32);
      InvalidEntry(tempTime);
      SetFocus(GetItemHandle(IDC_WATIME));
    end;
  end
  else
  begin
    GetDlgItemText(HWindow, IDC_LBL1, tempDate, 32);
    InvalidEntry(tempDate);
    SetFocus(GetItemHandle(IDC_WADATE));
  end;
end;

{ Methods for TWAReagentDlg }

constructor TWAReagentDlg.Init(aParent: PWindowsObject; aName: PChar);
begin
  inherited Init(aParent, aName);
  chkAlphaNap     := New(PLCheckBox, InitResource(@self, IDC_ALPHA_NAP, FALSE));
  chkPotHydrox    := New(PLCheckBox, InitResource(@self, IDC_POT_HYDROX, FALSE));
  chkSulfAcid     := New(PLCheckBox, InitResource(@self, IDC_SULF_ACID, FALSE));
  chkDimethyl     := New(PLCheckBox, InitResource(@self, IDC_DIMETHYL, FALSE));
  chkPeptidase    := New(PLCheckBox, InitResource(@self, IDC_PEPTIDASE, FALSE));
  chkFerricChlor  := New(PLCheckBox, InitResource(@self, IDC_FERRIC_CHLOR, FALSE));
  chkKovacs       := New(PLCheckBox, InitResource(@self, IDC_KOVACS, FALSE));
  chkHNIDIndole   := New(PLCheckBox, InitResource(@self, IDC_HNID_INDOLE, FALSE));
  chkRapidIndole  := New(PLCheckBox, InitResource(@self, IDC_RAPID_INDOLE, FALSE));
  chkSodiumHydrox := New(PLCheckBox, InitResource(@self, IDC_SODIUM_HYDROX, FALSE));
end;

procedure TWAReagentDlg.SetUpWindow;
var
  waCmd : PWACommand;
begin
  inherited SetUpWindow;
  { tell W/A to position for purging }
  WACmd := New(PWAMPPURGECmd, Init(WAID));
  WACmd^.SendWAMessage;
  if waCmd^.WAErrNum <> 0 then
    ShowError(@self, IDS_WA_ERR, nil, waCmd^.WAErrNum, MOD_WAMAINT, 1);
  if waCmd^.WAStatNum <> 0 then
    ShowError(@self, IDS_WA_ERR, nil, waCmd^.WAStatNum, MOD_WAMAINT, 2);
  MSDisposeObj(waCmd);
  if PAccessWADlg(parent)^.waType = ClassicWA then
  begin
    AlphaNap     := 0;
    PotHydrox    := 1;
    SulfAcid     := 2;
    Dimethyl     := 3;
    Peptidase    := 4;
    FerricChlor  := 5;
    Kovacs       := 6;
    HNIDIndole   := 7;
    SodiumHydrox := 8;
    RapidIndole  := 11;
  end
  else { WA 40/96 }
  begin
    HNIDIndole   := 0;
    FerricChlor  := 1;
    Kovacs       := 2;
    PotHydrox    := 3;
    AlphaNap     := 4;
    SulfAcid     := 5;
    Dimethyl     := 6;
    Peptidase    := 7;
    SodiumHydrox := 8;
    RapidIndole  := 9;
  end;
  (*EnableWindow(GetItemHandle(IDC_RAPID_INDOLE), FALSE); {Enabled for Nihongo DMS v4.00}*)
  chkAlphaNap^.Check;
  chkPotHydrox^.Check;
  chkSulfAcid^.Check;
  chkDimethyl^.Check;
  chkPeptidase^.Check;
  chkFerricChlor^.Check;
  chkKovacs^.Check;
  chkHNIDIndole^.Check;
  chkRapidIndole^.Check;
  chkSodiumHydrox^.Check;
end;

destructor TWAReagentDlg.Done;
var
  waCmd : PWACommand;
begin
  { Position for Non-Purge }
  waCmd := New(PWAUNMPPURGECmd, Init(WAID));
  WACmd^.SendWAMessage;
  if waCmd^.WAErrNum <> 0 then
    ShowError(@self, IDS_WA_ERR, nil, waCmd^.WAErrNum, MOD_WAMAINT, 1);
  if waCmd^.WAStatNum <> 0 then
    ShowError(@self, IDS_WA_ERR, nil, waCmd^.WAStatNum, MOD_WAMAINT, 2);
  MSDisposeObj(waCmd);
  inherited Done;
end;

procedure TWAReagentDlg.OK(var msg: TMessage);
var
  purgeAll    : boolean;
  waCmd       : PWAPURCmd;
  purgeAllCmd : PWARSTPurCmd;
  okSoFar     : boolean;
  statStr     : array[0..64] of char;
  wc          : PWaitCursor;

  procedure DoIt(chkBox: PLCheckBox; pID, sID: integer);
  begin
    if okSoFar and (chkBox^.GetCheck <> 0) then
    begin
      waCmd^.SetParams(pID);
      SR(sID, statStr, 64);
      SetDlgItemText(HWindow, IDC_WASTAT, statStr);
      okSoFar := waCmd^.SendWaMessage;
      if waCmd^.WAErrNum <> 0 then
        ShowError(@self, IDS_WA_ERR, nil, waCmd^.WAErrNum, MOD_WAMAINT, 1);
      if waCmd^.WAStatNum <> 0 then
        ShowError(@self, IDS_WA_ERR, nil, waCmd^.WAStatNum, MOD_WAMAINT, 2);
    end
    else
      purgeAll := FALSE;
  end;

begin
  wc := New(PWaitCursor, Init);
  purgeAll := TRUE;
  { purge the selected reagents }
  waCmd := New(PWAPURCmd, Init(WAID));
  okSoFar := TRUE;
  DoIt(chkAlphaNap    , AlphaNap    , IDS_PUR_ALPHA_NAP);
  DoIt(chkPotHydrox   , PotHydrox   , IDS_PUR_POT_HYDROX);
  DoIt(chkSulfAcid    , SulfAcid    , IDS_PUR_SULF_ACID);
  DoIt(chkDimethyl    , Dimethyl    , IDS_PUR_DIMETHYL);
  DoIt(chkPeptidase   , Peptidase   , IDS_PUR_PEPTIDASE);
  DoIt(chkFerricChlor , FerricChlor , IDS_PUR_FERRIC_CHLOR);
  DoIt(chkKovacs      , Kovacs      , IDS_PUR_KOVACS);
  DoIt(chkHNIDIndole  , HNIDIndole  , IDS_PUR_HNID_INDOLE);
  DoIt(chkRapidIndole , RapidIndole , IDS_PUR_RAPID_INDOLE);
  DoIt(chkSodiumHydrox, SodiumHydrox, IDS_PUR_SODIUM_HYDROX);

  { If all purged then reset last purge time to NOW }
  if okSoFar and purgeAll then
  begin
    purgeAllCmd := New(PWARSTPurCmd, Init(WAID));
    purgeAllCmd^.SendWaMessage;
    if purgeAllCmd^.WAErrNum <> 0 then
      ShowError(@self, IDS_WA_ERR, nil, purgeAllCmd^.WAErrNum, MOD_WAMAINT, 1);
    if purgeAllCmd^.WAStatNum <> 0 then
      ShowError(@self, IDS_WA_ERR, nil, purgeAllCmd^.WAStatNum, MOD_WAMAINT, 2);
    MSDisposeObj(purgeAllCmd);
  end;
  SR(IDS_PURGE_COMPLETE, statStr, 64);
  SetDlgItemText(HWindow, IDC_WASTAT, statStr);
  MSDisposeObj(waCmd);
  MSDisposeObj(wc);
end;

procedure TWAReagentDlg.SetAutoPurge(var msg: TMessage);
var
  waCmd   : PWASetRegCmd;
  regVal  : longint;
  statStr : array[0..64] of char;
begin
  waCmd := New(PWASetRegCmd, Init(WAID));
  regVal := $FFFF;
  if chkAlphaNap^.GetCheck <> 0 then
    ClearBit(regVal, AlphaNap);
  if chkPotHydrox^.GetCheck <> 0 then
    ClearBit(regVal, PotHydrox);
  if chkSulfAcid^.GetCheck <> 0 then
    ClearBit(regVal, SulfAcid);
  if chkDimethyl^.GetCheck <> 0 then
    ClearBit(regVal, Dimethyl);
  if chkPeptidase^.GetCheck <> 0 then
    ClearBit(regVal, Peptidase);
  if chkFerricChlor^.GetCheck <> 0 then
    ClearBit(regVal, FerricChlor);
  if chkKovacs^.GetCheck <> 0 then
    ClearBit(regVal, Kovacs);
  if chkHNIDIndole^.GetCheck <> 0 then
    ClearBit(regVal, HNIDIndole);
  if chkRapidIndole^.GetCheck <> 0 then
    ClearBit(regVal, RapidIndole);
  if chkSodiumHydrox^.GetCheck <> 0 then
    ClearBit(regVal, SodiumHydrox);
  waCmd^.SetParams(14, regVal);
  if waCmd^.SendWAMessage then
  begin
    SR(IDS_AUTO_PURGE_SET, statStr, 64);
    SetDlgItemText(HWindow, IDC_WASTAT, statStr);
  end;
  if waCmd^.WAErrNum <> 0 then
    ShowError(@self, IDS_WA_ERR, nil, waCmd^.WAErrNum, MOD_WAMAINT, 1);
  if waCmd^.WAStatNum <> 0 then
    ShowError(@self, IDS_WA_ERR, nil, waCmd^.WAStatNum, MOD_WAMAINT, 2);
  MSDisposeObj(waCmd);
end;


{ Methods for TWAMaintMainDlg }

constructor TWAMaintMainDlg.Init(aParent: PWindowsObject; aName: PChar;
                                 aStatBuf: PChar; var somethingHappened: boolean);
begin
  if not inherited Init(aParent, aName) then
    Fail;
  statBuf := aStatBuf;
  accessAcquired := ADDR(somethingHappened);
end;

procedure TWAMaintMainDlg.SetUpWindow;
var
  offStr,
  okStr, nokStr : array[0..6] of char;
  hdwrStat      : PHdwrStatus;
begin
  inherited SetUpWindow;
  if TheJamChecker <> nil then
    TheJamChecker^.SetWAStatusWindow(HWindow, statBuf);
  SetDlgItemText(HWindow, IDC_WASTATE, statBuf);
  { check various hardware components in WA and
    report OK or NOK to dialog box }
  SR(IDS_OK, okStr, 6); { OK }
  SR(IDS_NOK, nokStr, 6); { NOK }
  SR(IDS_OFF, offStr, 6); { OFF }

  hdwrStat := New(PHdwrStatus, Init);

  {** Processor system - CPU+PROM+RAM **}
  if (hdwrStat^.processor <> 0) and
     (hdwrStat^.prom <> 0) and
     (hdwrStat^.ramMem <> 0) then
    SetDlgItemText(HWindow, IDC_WAPROCESSOR_SYS, okStr)
  else
    SetDlgItemText(HWindow, IDC_WAPROCESSOR_SYS, nokStr);

  {** Measurement system - Lamp+A/D+Temp **}
  if (hdwrStat^.lamp <> 0) and
     (hdwrStat^.photoboard <> 0) and
     (hdwrStat^.temperature <> 0) then
    SetDlgItemText(HWindow, IDC_WAMEASURE_SYS, okStr)
  else
    SetDlgItemText(HWindow, IDC_WAMEASURE_SYS, nokStr);

  {** Motion system - Motor+Dispenser **}
  if (hdwrStat^.motor <> 0) and
     (hdwrStat^.dispenser <> 0) then
    SetDlgItemText(HWindow, IDC_WAMOTION_SYS, okStr)
  else
    SetDlgItemText(HWindow, IDC_WAMOTION_SYS, nokStr);

  {** Communications system **}
  if (hdwrStat^.Comms <> 0) then
    SetDlgItemText(HWindow, IDC_WACOMM_SYS, okStr)
  else
    SetDlgItemText(HWindow, IDC_WACOMM_SYS, nokStr);

  {** Fluorometric system - FL OK **}
  if (hdwrStat^.fluor = -1) then
    SetDlgItemText(HWindow, IDC_WAFLUOR_Sys, offStr)
  else if (hdwrStat^.fluor <> 0) then
    SetDlgItemText(HWindow, IDC_WAFLUOR_Sys, okStr)
  else
    SetDlgItemText(HWindow, IDC_WAFLUOR_SYS, nokStr);

  MSDisposeObj(hdwrStat);
end;

procedure TWAMaintMainDlg.PurgeReagents(var msg: TMessage);
var
  statStr    : array[0..64] of char;
  wc         : PWaitCursor;
  ReagentDlg : PWAReagentDlg;
begin
  OpenDoor(msg);
  if userCantWait then
    Exit;
  SetDlgItemText(HWindow, IDC_WAMAINT_STAT, '');
  if waType = ClassicWA then
    ReagentDlg := New(PWAReagentDlg, Init(@Self, MakeIntResource(DLG_CLASSIC_REAGENTS)))
  else
    ReagentDlg := New(PWAReagentDlg, Init(@Self, MakeIntResource(DLG_REAGENTS)));

  Application^.ExecDialog(ReagentDlg);

  if (waType = WA40) or (waType = WA96) then
  begin
    SR(IDS_WAIT_DISP, statStr, 64);
    SetDlgItemText(HWindow, IDC_WAMAINT_STAT, statStr);
    { Wait for Dispense pressure to become normal }

    SetTO(30);
    EnableWindow(HWindow, FALSE);
    repeat
      wc := New(PWaitCursor, Init);
      MSWait(1000);
      MSDisposeObj(wc);
      HandleEvents(HWindow);
    until TimeOut;
    EnableWindow(HWindow, TRUE);

(*  wc := New(PWaitCursor, Init);
    MSWait(30000);
    MSDisposeObj(wc); *)
    SetDlgItemText(HWindow, IDC_WAMAINT_STAT, '');
  end;
end;

procedure TWAMaintMainDlg.SetWAClock(var msg: TMessage);
var
  d : PWAClockDlg;
begin
  d := New(PWAClockDlg, Init(@Self, MakeIntResource(DLG_WACLOCK)));
  Application^.ExecDialog(d);
end;

function TWAMaintMainDlg.CannotCloseDoor: boolean;
var  temp: array[0..64] of char;
begin
  WACloseDoor;
  if DoorIsOpen then
  begin
    SR(IDS_CLOSE_DOOR, temp, 64);
    SetDlgItemText(HWindow, IDC_WAMAINT_STAT, temp);
  end;
  CannotCloseDoor:=DoorIsOpen;
end;

procedure TWAMaintMainDlg.ReferenceDisk(var msg: TMessage);
var
  waCmd : PWAMPRefCmd;
  temp  : array[0..64] of char;
begin
  if userCantWait then
    Exit;

  if CannotCloseDoor then
     exit;

  waCmd := New(PWAMPRefCmd, Init(WAID));
  if waCmd^.SendWAMessage then
  begin
    OpenDoor(msg);
    SR(IDS_RDY_REF_DISK, temp, 64);  { Ready for Reference Disk Cleaning }
    SetDlgItemText(HWindow, IDC_WAMAINT_STAT, temp);
  end;
  if waCmd^.WAErrNum <> 0 then
    ShowError(@self, IDS_WA_ERR, nil, waCmd^.WAErrNum, MOD_WAMAINT, 1);
  if waCmd^.WAStatNum <> 0 then
    ShowError(@self, IDS_WA_ERR, nil, waCmd^.WAStatNum, MOD_WAMAINT, 2);
  MSDisposeObj(waCmd);
end;

procedure TWAMaintMainDlg.ShieldClean(var msg: TMessage);
var
  waCmd : PWAMPSHIELDCmd;
  temp  : array[0..64] of char;
begin
  if userCantWait then
    Exit;

  if CannotCloseDoor then
     exit;

  waCmd := New(PWAMPSHIELDCmd, Init(WAID));
  if waCmd^.SendWAMessage then
  begin
    OpenDoor(msg);
    SR(IDS_RDY_SHIELD, temp, 64); { Ready for Shield Cleaning }
    SetDlgItemText(HWindow, IDC_WAMAINT_STAT, temp);
  end;
  if waCmd^.WAErrNum <> 0 then
    ShowError(@self, IDS_WA_ERR, nil, waCmd^.WAErrNum, MOD_WAMAINT, 1);
  if waCmd^.WAStatNum <> 0 then
    ShowError(@self, IDS_WA_ERR, nil, waCmd^.WAStatNum, MOD_WAMAINT, 2);
  MSDisposeObj(waCmd);
end;

procedure TWAMaintMainDlg.ShowWAStatus(var msg: TMessage);
begin
  SetDlgItemText(HWindow, IDC_WASTATE, pointer(msg.lParam));    { show it and pass it on }
  PostMessage(parent^.HWindow, msg.message, 0, msg.lParam);
end;

function TWAMaintMainDlg.WAOpenDoor(var howLong, whenOpen: integer): boolean;
var
  waCmd : PWAMARCmd;
begin
  WAOpenDoor := FALSE;
  waCmd := New(PWAMARCmd, Init(WAID));
  waCmd^.SetParams(byte(howLong));
  if waCmd^.SendWAMessage then
  begin
    howLong := waCmd^.recvMsg^.arDuratn;
    whenOpen := waCmd^.recvMsg^.arDelay;
    accessAcquired^ := TRUE;
    WAOpenDoor := TRUE;
  end
  else
  begin
    howLong := 0;
    whenOpen := 0;
  end;
  if waCmd^.WAErrNum <> 0 then
    ShowError(@self, IDS_WA_ERR, nil, waCmd^.WAErrNum, MOD_WAMAINT, 1);
  if waCmd^.WAStatNum <> 0 then
    ShowError(@self, IDS_WA_ERR, nil, waCmd^.WAStatNum, MOD_WAMAINT, 2);
  MSDisposeObj(waCmd);
end;

procedure TWAMaintMainDlg.TimesUp;
begin
  SetDlgItemText(HWindow, IDC_DOOROPEN, '0');
end;


{ EXPORTED PROCEDURES }

procedure AccessWA(aParent: PWindowsObject; var somethingHappened: boolean);
var
  d   : PAccessWADlg;
begin
  d := New(PAccessWADlg, Init(aParent, MakeIntResource(DLG_ACCESS_WA)));
  if (d <> nil) and (Application^.ExecDialog(d) = IDOK) then
    somethingHappened := TRUE;
end;

procedure WAMaintenance(aParent: PWindowsObject; aStatBuf: PChar; var somethingHappened: boolean);
var
  d : PWAMaintMainDlg;
begin
  d := New(PWAMaintMainDlg, Init(aParent, MakeIntResource(DLG_WAMAINT),
                                 aStatbuf, somethingHappened));
  if d <> nil then
    Application^.ExecDialog(d);
end;


END.
