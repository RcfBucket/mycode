{----------------------------------------------------------------------------}
{  Module Name  : WAUtil.PAS                                                 }
{  Programmer   : EJ                                                         }
{  Date Created : 05/24/95                                                   }
{                                                                            }
{  Purpose -                                                                 }
{  This module provides utility functions for the WA sub-system.             }
{                                                                            }
{  Assumptions -                                                             }
{  None.                                                                     }
{                                                                            }
{  Revision History -                                                        }
{                                                                            }
{  Version  Date      Who    Did What                                        }
{  -------  --------  ------ ----------------------------------------------- }
{  1.00     05/24/95  EJ     Initial release.                                }
{                                                                            }
{----------------------------------------------------------------------------}

unit WAUtil;

INTERFACE

uses
  Objects,
  OWindows,
  ListLib,
  DBFile,
  ResLib,
  DlgLib,
  UIFlds,
  Gather,
  WAGPIB;

type
  PExceptions  = ^TExceptions;
  TExceptions = object(TObject)
    NeedStS,
    NeedGmS,
    NeedTFG,
    NeedBLac,
    NeedOxac,
    NeedOxid,
    NeedOrg,
    NoID,
    IDTooLow,
    NeedIndole,
    UndefinedBiotype,
    AbortedMissing,
    IDProtVulPNR,
    IDKlebOxyPneu,
    Hours42,
    PossibleESBL,
    PossibleE157     : boolean;
    {}
    constructor Init;
    procedure ClearAllExceptions; virtual;
    function  ExceptionsExist: boolean; virtual;
  end;

  PSelectSessionDlg = ^TSelectSessionDlg;
  TSelectSessionDlg = object(TCenterDlg)
    sess         : PUIFld;
    isoDB        : PDBFile;
    listObj      : PListObject;
    {}
    constructor Init(aParent: PWindowsObject; aIso: PDBFile; aListObj: PListObject);
    destructor Done; virtual;
    procedure SetupWindow; virtual;
    function  CanClose: boolean; virtual;
  end;

  PGatherTGRes = ^TGatherTGRes;
  TGatherTGRes = object(TGatherRes)
    constructor Init(aResults: PResults; aTestGrpID: PChar; aListObj: PListObject);
  end;


procedure FormatReadTime(readTime: TimeDate; formattedRT: PChar; inclDate: boolean);
procedure GetStatNum(stat: integer; var statNum: integer; flags: PPanelFlags);
procedure GetTowerSlot(WASlot: integer; var slot: byte; var tower: integer);
function  GetWASlot(slot: byte; tower: integer): integer;
Function  IndoleDispensedFlag(xPID: Integer): Boolean;

var
  WAID      : integer;


IMPLEMENTATION

uses
  MScan,
  WinTypes,
  Strings,
  APITools,
  IntlLib,
  DBLib,
  DBIDs,
  DMSDebug;

{$I WAMAIN.INC}


{ Methods for TExceptions }

constructor TExceptions.Init;
{**************************************************************************}
{**************************************************************************}
begin
  inherited Init;
  ClearAllExceptions;
end;

function TExceptions.ExceptionsExist: boolean;
{**************************************************************************}
{**************************************************************************}
begin
  ExceptionsExist := FALSE;
  if not NeedStS then
    if not NeedGmS then
      if not NeedTFG then
        if not NeedBLac then
          if not NeedOxac then
            if not NeedOxid then
              if not NeedOrg then
                if not NoID then
                  if not IDTooLow then
                    if not NeedIndole then
                      if not undefinedBiotype then
                        if not AbortedMissing then
                          if not IDProtVulPNR then
                            if not IDKlebOxyPneu then
                              if not Hours42 then
                                 if not PossibleESBL then
                                    if not PossibleE157 then
                                    Exit;
  ExceptionsExist := TRUE;
end;

procedure TExceptions.ClearAllExceptions;
{**************************************************************************}
{**************************************************************************}
begin
  NeedStS := FALSE;
  NeedGmS := FALSE;
  NeedTFG := FALSE;
  NeedBLac := FALSE;
  NeedOxac := FALSE;
  NeedOxid := FALSE;
  NeedOrg := FALSE;
  NoID := FALSE;
  IDTooLow := FALSE;
  NeedIndole := FALSE;
  undefinedBiotype := FALSE;
  AbortedMissing := FALSE;
  IDProtVulPNR := FALSE;
  IDKlebOxyPneu := FALSE;
  Hours42 := FALSE;
  PossibleESBL:=FALSE;
  PossibleE157:=FALSE;
end;


{-------------------------------------------------------[ TSelectSessionDlg ]--}
constructor TSelectSessionDlg.Init(aParent: PWindowsObject; aIso: PDBFile; aListObj: PListObject);
begin
  inherited Init(aParent, MakeIntResource(DLG_SELECT_SESSION));
  isoDB := aIso;
  listObj := aListObj;
  sess := New(PUIFld, Init(@self, isoDB^.dbd, listObj, FALSE, DBIsoSess, IDC_SESSION, 0));
end;

destructor TSelectSessionDlg.Done;
begin
  MSDisposeObj(sess);
  inherited Done;
end;

procedure TSelectSessionDlg.SetupWindow;
begin
  inherited SetupWindow;
  sess^.XFerRecToCtl(isoDB^.dbr);
end;

function TSelectSessionDlg.CanClose;
var
  k   : integer;
begin
  k := sess^.Format;
  if k <> 0 then
  begin
    ShowUIFldError(@self, k, sess);
    CanClose:= FALSE;
  end
  else
  begin
    CanClose:= TRUE;
    sess^.XFerCtlToRec(isoDB^.dbr);
  end;
end;


{ Methods for TGatherTGRes }

constructor TGatherTGRes.Init(aResults: PResults; aTestGrpID: PChar; aListObj: PListObject);
var
  idx       : integer;
  count     : integer;
  listObj   : PListObject;
  pstr      : array[0..100] of char;

  procedure GetIt(idx: integer);
  var
    aRes  : PResRecObj;
    row   : longint;
    rec   : PDBRec;
    dSort : longint;
    obj   : PGatherResObj;
  begin
    aRes := results^.At(idx);
    if aRes^.drugSeq <> 0 then   {- MIC/NCCLS results }
    begin
      obj:= FindDrug(aRes^.drugSeq);  {- see if this drug seq is already collected }
      if obj = nil then
      begin
        rec:= listObj^.PeekSeq(DBDrugFile, aRes^.drugSeq);
        if rec <> nil then
        begin
          rec^.GetField(DBDrugName, @pstr, sizeof(pstr)-1);
          rec^.GetField(DBDrugSortNum, @dSort, sizeof(dSort));
          obj:= New(PGatherResObj, Init(pstr, dSort, aRes^.drugSeq, FALSE));

          rec^.GetField(DBDrugAbbr, @pstr, sizeof(pstr)-1);
          StrCopy(obj^.drugAbbr, pstr);
          rec^.GetField(DBDrugAltAbbr, @pstr, sizeof(pstr)-1);
          StrCopy(obj^.drugAlt, pstr);

          micRes^.Insert(obj);
        end;
      end;
      if StrComp(aRes^.testCatID, 'MIC') = 0 then
        obj^.resIdx:= idx
      else if StrComp(aRes^.testCatID, 'NCCLS') = 0 then
        obj^.ncclsIdx:= idx;
    end
    else    {- non MIC results (id results) }
    begin
      obj:= New(PGatherResObj, Init(aRes^.testName, 0, 0, FALSE));
      idRes^.Insert(obj);
      obj^.resIdx:= idx;
    end;
  end;

begin
  TObject.Init;    {- Do NOT call inherited Init }

  idRes:= nil;
  micRes:= nil;

  if aResults = nil then
  begin
    FatalError('Error', 'Invalid results object passed to TGatherTGRes.Init');
    Done;
    Fail;
  end;

  idRes:= New(PGatherResCollection, Init(25,25));
  micRes:= New(PGatherResCollection, Init(25,25));
  if (idRes = nil) or (micRes = nil) then
  begin
    FatalError('Error', 'Cannot allocate result collection (TGatherTGRes).');
    Done;
    Fail;
  end;

  results:= aResults;

  if aListObj = nil then
  begin
    listObj:= New(PListObject, Init);
    if listObj = nil then
    begin
      FatalError('Error', 'Invalid list object in TGatherRes.');
      Done;
      Fail;
    end;
  end
  else
    listObj:= aListObj;

  count := results^.NumTests(aTestGrpID, idx);
  count := idx + count;

  while idx < count do
  begin
    GetIt(idx);
    Inc(idx);
  end;

  if (aListObj = nil) and (listObj <> nil) then
    MSDisposeObj(listObj);
end;


procedure FormatReadTime(readTime: TimeDate; formattedRT: PChar; inclDate: boolean);
var
  tDate : TIntlDate;
  tTime : TIntlTime;
  temp  : array [0..16] of char;
begin
  with readTime do
  begin
    formattedRT^ := #0;
    if inclDate then
    begin
      tDate := IntlPackDate(year, month, day);
      IntlDateStr(tDate, formattedRT, 16);
      StrCat(formattedRT, ' ');
    end;
    tTime := IntlPackTime(hour, min, sec, 0);
    IntlTimeStr(tTime, TRUE, temp, 16);
    StrCat(formattedRT, temp);
  end;
end;

procedure GetStatNum(stat: integer; var statNum: integer; flags: PPanelFlags);
begin
  case stat of
    SlotEmpty         : statNum := IDS_STS_EMPTY;
    NoDataYet         : statNum := IDS_STS_NO_DATA;
    PanelUnrecognized : statNum := IDS_STS_UNRECOG;
    AbortedPanel      : statNum := IDS_STS_ABORTED;
    PanelMissing      : statNum := IDS_STS_MISSING;
    DuplicatePanelId  : statNum := IDS_STS_DUP_ID;
    LampFailure       : statNum := IDS_STS_LAMPFAIL;
    MICsAvailable :
      begin
        if flags^.SlowGrower then
          statNum := IDS_FLG_SLOW_GROWER
        else
          statNum := IDS_STS_MICS_AVAIL;
      end;
    ReagentsAdded     : statNum := IDS_STS_REAGENTS;
    ProcessComplete   : statNum := IDS_STS_COMPLETE;
    else
      statNum := 0;
  end
end;

procedure GetTowerSlot(WASlot: integer; var slot: byte; var tower: integer);
begin
  if WA[WAID].slotsPerTower <> 0 then
  begin
    tower := (WASlot div WA[WAID].slotsPerTower) + 1;
    slot  := (WASlot mod WA[WAID].slotsPerTower) + 1;
  end
  else
  begin
    tower := 0;
    slot  := 0;
  end;
end;

function GetWASlot(slot: byte; tower: integer): integer;
begin
  GetWASlot := ((tower - 1) * WA[WAID].slotsPerTower) + (slot - 1);
end;

Function IndoleDispensedFlag
  (xPID        : Integer):     {Panel ID, returned from GPnOptCmd}
   Boolean;                    {Return the PDL var setting for USER5, Did_Indole}
Const
  USER5_PDL_Did_Indole = 4;    {Identify PDL var USER5, alias Did_Indole}
Var
  WAGPnOptCmd : PWAGPnOptCmd;  {Get Panel Option}
  xPOpt       : Byte;          {Panel option number, returned from GPnOptCmd}
  xPOptVal    : LongInt;       {Panel option setting, returned from GPnOptCmd}
Begin
  xPOpt := Byte(USER5_PDL_Did_Indole);              {Identify the option}
  WAGPnOptCmd := New(PWAGPnOptCmd, Init(WAID));     {Build the gpib command}
  WAGPnOptCmd^.SetParams(xPID,xPOpt);
  If WAGPnOptCmd^.SendWAMessage then                {Execute the command}
  Begin                                             {The command was understood}
    WAGPnOptCmd^.GetParams(xPID, xPOpt, xPOptVal);  {Get the Did_Indole setting}
    IndoleDispensedFlag := (xPOptVal = longint(3));
  End;
  MSDisposeObj(WAGPnOptCmd);                        {Clean up}
End;

END.
