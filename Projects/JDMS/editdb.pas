{----------------------------------------------------------------------------}
{  Module Name  : EDITDB.PAS                                                 }
{  Programmer   : EJ                                                         }
{  Date Created : 01/24/94                                                   }
{                                                                            }
{  Purpose -                                                                 }
{  This module provides the user the ability to examine DB definitions       }
{  and data in BTRIEVE database files, to modify data at a field level,      }
{  and to add and delete records.                                            }
{                                                                            }
{  Assumptions -                                                             }
{  None.                                                                     }
{                                                                            }
{  Revision History -                                                        }
{                                                                            }
{  Version  Date      Who    Did What                                        }
{  -------  --------  ------ ----------------------------------------------- }
{  1.00     01/24/95  EJ     Initial release.                                }
{                                                                            }
{----------------------------------------------------------------------------}

program EditDB;

uses
  OWindows,
  ODialogs,
  WinProcs,
  WinTypes,
  WinDos,
  Strings,
  StrsW,
  DMString,
  INILib,
  APITools,
  Ctl3d,
  DlgLib,
  CtlLib,
  GridLib,
  ListLib,
  UIFlds,
  DBTypes,
  DBErrors,
  DBLib,
  DBFile,
  MScan,
  PrnPrev,
  DMSErr,
  DMSDebug;

{$I EDITDB.INC}
{$R EDITDB.RES}

const
  AppName: PChar  = 'EDITDB';
  version         = '3.00';

  {-- 3.00  Initial release
  }

  maxFreeTextDisplay  = 25; {- maximum width of free text display }

  wm_StartUp      = wm_User + 101;
  wm_Grid         = wm_User + 102;


type
  PInfoGrid = ^TInfoGrid;
  TInfoGrid = object(TGrid)
    procedure RightMouse(aCol: integer; aRow: longint); virtual;
  end;

  PEditField = ^TEditField;
  TEditField = object(TCenterDlg)
    desc      : PDBReadWriteDesc;
    rec       : PDBRec;
    fieldNum  : integer;
    fEdit     : PUIFld;
    {}
    constructor Init(aParent: PWindowsObject;
                     pdesc  : PDBReadWriteDesc;
                     prec   : PDBRec;
                     aField : integer);
    destructor Done; virtual;
    procedure SetupWindow; virtual;
    function  CanClose: boolean; virtual;
  end;

  PEditRec = ^TEditRec;
  TEditRec = object(TCenterDlg)
    grid    : PInfoGrid;
    desc    : PDBReadWriteDesc;
    cur     : PDBCopyCursor;
    rec     : PDBRec;
    editing : boolean;
    {}
    constructor Init(aParent: PWindowsObject;
                     pdesc    : PDBReadWriteDesc;
                     pcur     : PDBCopyCursor;
                     prec     : PDBRec;
                     aEditing : boolean);
    procedure SetupWindow; virtual;
    procedure FillList;
    procedure HandleList(var msg: TMessage); virtual ID_FIRST + IDC_FIELDLIST;
    procedure EditField(var msg: TMessage); virtual ID_FIRST + IDC_EDITFIELD;
    procedure ShowKeys(var msg: TMessage); virtual ID_FIRST + IDC_KEYSINFO;
    procedure Print(var msg: TMessage); virtual ID_FIRST + IDC_PRINT;
    function  CanClose: boolean; virtual;
    procedure GridMsg(var msg: TMessage); virtual WM_FIRST + wm_Grid;
    procedure DrawItem(var msg: TMessage); virtual WM_FIRST + wm_DrawItem;
    procedure MeasureItem(var msg: TMessage); virtual WM_FIRST + wm_MeasureItem;
    procedure CharToItem(var msg: TMessage); virtual WM_FIRST + wm_CharToItem;
  end;

  PKeysInfoDlg = ^TKeysInfoDlg;
  TKeysInfoDlg = object(TCenterDlg)
    grid    : PInfoGrid;
    desc    : PDBReadDesc;
    {}
    constructor Init(aParent: PWindowsObject; aName: PChar; dbDesc: PDBReadDesc);
    destructor Done; virtual;
    procedure SetupWindow; virtual;
    procedure Print(var msg: TMessage); virtual ID_FIRST + IDC_PRINT;
    procedure GridMsg(var msg: TMessage); virtual WM_FIRST + wm_Grid;
    procedure DrawItem(var msg: TMessage); virtual WM_FIRST + wm_DrawItem;
    procedure MeasureItem(var msg: TMessage); virtual WM_FIRST + wm_MeasureItem;
    procedure CharToItem(var msg: TMessage); virtual WM_FIRST + wm_CharToItem;
  end;

  PSetKeyDlg = ^TSetKeyDlg;
  TSetKeyDlg = object(TKeysInfoDlg)
    cur     : PDBCursor;
    {}
    constructor Init(aParent: PWindowsObject; aName: PChar;
                     dbDesc: PDBReadDesc; aCur: PDBCursor);
    procedure SetupWindow; virtual;
    function  CanClose: boolean; virtual;
  end;

  PEditFileDlg = ^TEditFileDlg;
  TEditFileDlg = object(TCenterDlg)
    grid    : PInfoGrid;
    desc    : PDBReadWriteDesc;
    cur     : PDBCopyCursor;
    rec     : PDBRec;
    fldCnt  : integer;
    {}
    constructor Init(aParent: PWindowsObject; aFile: PChar);
    destructor Done; virtual;
    procedure SetupWindow; virtual;
    procedure FillList(bInit: boolean); virtual;
    procedure EditRec(var msg: TMessage); virtual ID_FIRST + IDC_EDITREC;
    procedure AddRec(var msg: TMessage); virtual ID_FIRST + IDC_ADDREC;
    procedure DeleteRec(var msg: TMessage); virtual ID_FIRST + IDC_DELREC;
    procedure ReadRec(var msg: TMessage); virtual ID_FIRST + IDC_READREC;
    procedure SetKey(var msg: TMessage); virtual ID_FIRST + IDC_SETKEY;
    procedure Print(var msg: TMessage); virtual ID_FIRST + IDC_PRINT;
    procedure HandleList(var msg: TMessage); virtual ID_FIRST + IDC_RECORDLIST;
    procedure GridMsg(var msg: TMessage); virtual WM_FIRST + wm_Grid;
    procedure EnableButtons; virtual;
    procedure DrawItem(var msg: TMessage); virtual WM_FIRST + wm_DrawItem;
    procedure MeasureItem(var msg: TMessage); virtual WM_FIRST + wm_MeasureItem;
    procedure CharToItem(var msg: TMessage); virtual WM_FIRST + wm_CharToItem;
  end;

  PFileInfoDlg = ^TFileInfoDlg;
  TFileInfoDlg = object(TCenterDlg)
    grid    : PInfoGrid;
    desc    : PDBReadDesc;
    {}
    constructor Init(aParent: PWindowsObject; aFile: PChar);
    destructor Done; virtual;
    procedure SetupWindow; virtual;
    procedure ShowKeys(var msg: TMessage); virtual ID_FIRST + IDC_KEYSINFO;
    procedure Print(var msg: TMessage); virtual ID_FIRST + IDC_PRINT;
    procedure GridMsg(var msg: TMessage); virtual WM_FIRST + wm_Grid;
    procedure DrawItem(var msg: TMessage); virtual WM_FIRST + wm_DrawItem;
    procedure MeasureItem(var msg: TMessage); virtual WM_FIRST + wm_MeasureItem;
    procedure CharToItem(var msg: TMessage); virtual WM_FIRST + wm_CharToItem;
  end;

  PPassDlg = ^TPassDlg;
  TPassDlg = object(TCenterDlg)
    pass  : PEdit;
    {}
    constructor Init(aParent: PWindowsObject);
    function CanClose: boolean; virtual;
  end;

  PMainDlg = ^TMainDlg;
  TMainDlg = object(TCenterDlgWindow)
    grid    : PGrid;
    {}
    constructor Init;
    destructor Done; virtual;
    function  GetClassName: PChar; virtual;
    procedure GetWindowClass(var aWndClass: TWndClass); virtual;
    procedure SetupWindow; virtual;
    procedure StartUp(var msg: TMessage); virtual wm_StartUp;
    procedure FillList;
    procedure HandleList(var msg: TMessage); virtual ID_FIRST + IDC_FILELIST;
    procedure EditFile(var msg: TMessage); virtual ID_FIRST + IDC_EDITFILE;
    procedure FileInfo(var msg: TMessage); virtual ID_FIRST + IDC_FILEINFO;
    procedure Print(var msg: TMessage); virtual ID_FIRST + IDC_PRINT;
    procedure EnableButtons; virtual;
    procedure DrawItem(var msg: TMessage); virtual WM_FIRST + wm_DrawItem;
    procedure MeasureItem(var msg: TMessage); virtual WM_FIRST + wm_MeasureItem;
    procedure CharToItem(var msg: TMessage); virtual WM_FIRST + wm_CharToItem;
  end;

  TEditApp = object(T3dApplication)
    procedure InitMainWindow; virtual;
  end;


var
  lf        : TLogFont;
  mnmList   : PListObject;


procedure ShowDBError(aParent: PWindowsObject;  msg: PChar; errNum: integer);
var
  pstr  : PChar;
  pstr1 : PChar;
  err   : PErrSubst;
begin
  GetMem(pstr, 256);
  GetMem(pstr1, 256);

  StrCopy(pstr, msg);
  StrCat(pstr, #10);

  StrCat(pstr, dbErrorStr(errNum, pstr1, 255-StrLen(pstr)));
  err := New(PErrSubst, Init(pstr));
  ShowError(aParent, IDS_PERCENT_S, err, errNum, MOD_EDITDB, 0);

  MSDisposeObj(err);
  MSFreeMem(pstr, 256);
  MSFreeMem(pstr1, 256);
end;

procedure DisplayFlags(HWindow: HWnd; k : integer);
var
  pstr : PChar;
begin
  GetMem(pstr, 101);
  StrCopy(pstr, 'Field Flags:');
  StrLCat(pstr, #10, 100);
  if (k and db_UD) <> 0 then
  begin
    StrLCat(pstr, #10, 100);
    StrLCat(pstr, 'User Defined Field', 100);
  end;
  if (k and db_Mnemonic) <> 0 then
  begin
    StrLCat(pstr, #10, 100);
    StrLCat(pstr, 'Mnemonic Reference Field', 100);
  end;
  if (k and db_Hidden) <> 0 then
  begin
    StrLCat(pstr, #10, 100);
    StrLCat(pstr, 'Hidden Field', 100);
  end;
  if (k and db_UDRec) <> 0 then
  begin
    StrLCat(pstr, #10, 100);
    StrLCat(pstr, 'UD Flag Field', 100);
  end;
  if (k and db_MNMID) <> 0 then
  begin
    StrLCat(pstr, #10, 100);
    StrLCat(pstr, 'Mnemonic ID Field', 100);
  end;
  if (k and db_MNMDescr) <> 0 then
  begin
    StrLCat(pstr, #10, 100);
    StrLCat(pstr, 'Mnemonic Description Field', 100);
  end;
  if (k and db_FreeText) <> 0 then
  begin
    StrLCat(pstr, #10, 100);
    StrLCat(pstr, 'Free Text Field', 100);
  end;
  if (k and db_Flags) <> 0 then
  begin
    StrLCat(pstr, #10, 100);
    StrLCat(pstr, 'Flags Field', 100);
  end;
  if (k and db_ExpandID) <> 0 then
  begin
    StrLCat(pstr, #10, 100);
    StrLCat(pstr, 'Expanded ID Field', 100);
  end;
  InfoMsg(HWindow, 'File Information', pstr);
  MSFreeMem(pstr, 101);
end;

{-------------------------------------------------------[ TInfoGrid ]--}

procedure TInfoGrid.RightMouse(aCol: integer; aRow: longint);
begin
  PostMessage(GetParent(HWindow), WM_Grid, aCol, aRow);
end;


{-------------------------------------------------------[ TEditField ]--}

constructor TEditField.Init(aParent: PWindowsObject;
                            pdesc  : PDBReadWriteDesc;
                            prec   : PDBRec;
                            aField : integer);
var
  c   : PControl;
begin
  inherited Init(aParent, MakeIntResource(DLG_EDITFIELD));
  desc := pdesc;
  rec := prec;
  fieldNum := aField;
  c := New(PLStatic, InitResource(@self, IDC_LBL1, 0, FALSE));
  c := New(PLStatic, InitResource(@self, IDC_LBL2, 0, FALSE));
  c := New(PLStatic, InitResource(@self, IDC_LBL3, 0, FALSE));
  c := New(PLStatic, InitResource(@self, IDC_LBL4, 0, FALSE));
  c := New(PLStatic, InitResource(@self, IDC_LBL5, 0, FALSE));
  fEdit := New(PUIFullEditFld, Init(@self, desc, mnmList, FALSE, fieldNum, IDC_VALUE, 0));
end;

destructor TEditField.Done;
begin
  MSDisposeObj(fEdit);
  inherited Done;
end;

procedure TEditField.SetupWindow;
var
  pstr    : array [0..20] of char;
  fi      : TFldInfo;
begin
  inherited SetupWindow;
  desc^.FieldInfo(fieldNum, fi);

  SetDlgItemText(HWindow, IDC_FIELD, fi.fldName);

  DBTypeStr(fi.fldType, pstr, 20);
  SetDlgItemText(HWindow, IDC_TYPE, pstr);

  Str(fi.fldSize, pstr);
  SetDlgItemText(HWindow, IDC_SIZE, pstr);

  HexStr(fi.userFlag, sizeof(fi.userFlag), pstr, 10);
  SetDlgItemText(HWindow, IDC_FLAG, pstr);

  fEdit^.XferRecToCtl(rec);
end;

function TEditField.CanClose: boolean;
var
  pstr  : array[0..50] of char;
  err : integer;
begin
  CanClose := FALSE;
  if inherited CanClose then
  begin
    err := fEdit^.Format;
    if err <> 0 then
    begin
      case err of
        UIErrRequired     : StrCopy(pstr, 'Field is required');
        UIErrInvalid      : StrCopy(pstr, 'Invalid field format');
        UIErrInvalidMnem  : StrCopy(pstr, 'Invalid association (ID not found)');
        UIErrInvalidNumber: StrCopy(pstr, 'Invalid numeric format');
        UIErrOutOfRange   : StrCopy(pstr, 'Number out of range');
        else
          strcopy(pstr, 'Unknown error');
      end;
      ErrorMsg(HWindow, 'Entry Error', pstr);
      fEdit^.FocusFld;
      Exit;
    end;

    fEdit^.XferCtlToRec(rec);
    if rec^.dbErrorNum <> 0 then
    begin
      ShowDBError(@Self, 'Edit Field', rec^.dbErrorNum);
      fEdit^.FocusFld;
    end
    else
      CanClose := TRUE;
  end;
end;


{---------------------------------------------------------[ TEditRec ]--}

constructor TEditRec.Init(aParent  : PWindowsObject;
                          pdesc    : PDBReadWriteDesc;
                          pcur     : PDBCopyCursor;
                          prec     : PDBRec;
                          aEditing : boolean);
var
  k   : integer;
  c   : PControl;
begin
  inherited Init(aParent, MakeIntResource(DLG_EDITREC));
  c := New(PLStatic, InitResource(@self, IDC_LBL1, 0, FALSE));
  c := New(PLStatic, InitResource(@self, IDC_LBL2, 0, FALSE));
  grid := New(PInfoGrid, InitResource(@Self, IDC_FIELDLIST, 6, FALSE));
  desc := pdesc;
  cur := pcur;
  rec := prec;
  editing := aEditing;
end;

procedure TEditRec.SetupWindow;
var
  filei : TFileInfo;
begin
  inherited SetupWindow;
  grid^.EnableRightMouse(TRUE);

  if editing then
    SetWindowText(HWindow, 'Edit Record')
  else
    SetWindowText(HWindow, 'Add Record');

  desc^.FileInfo(filei);
  SetDlgItemText(HWindow, IDC_FILENAME, filei.fName);
  SetDlgItemText(HWindow, IDC_FILEDESC, filei.desc);
  grid^.SetHeader(0, 'Fld#');
  grid^.SetHeader(1, 'Name');
  grid^.SetHeader(2, 'Value');
  grid^.SetHeader(3, 'Type');
  grid^.SetHeader(4, 'Size');
  grid^.SetHeader(5, 'Flag');
  FillList;
end;

procedure TEditRec.FillList;
var
  pstr    : PChar;
  p2      : array[0..10] of char;
  i, j, k : integer;
  sel     : integer;
  fi      : TFldInfo;
begin
  GetMem(pstr, 256);
  sel := grid^.GetSelIndex;
  Inc(sel);
  k := grid^.GetCount;
  if k > 0 then
    sel := sel mod k;

  grid^.SetRedraw(FALSE);
  grid^.ClearList;
  i := desc^.NumFields;
  for j := 1 to i do
  begin
    desc^.FieldInfo(j, fi);
    Str(j, p2);
    k := grid^.AddString('');
    grid^.SetData(0, k, p2);
    grid^.SetData(1, k, fi.fldName);
    rec^.GetFieldAsStr(j, pstr, 255);
    grid^.SetData(2, k, pstr);
    DBTypeStr(fi.fldType, pstr, 20);
    grid^.SetData(3, k, pstr);
    Str(fi.fldSize, p2);
    grid^.SetData(4, k, p2);
    HexStr(fi.userFlag, sizeof(fi.userFlag), pstr, 10);
    grid^.SetData(5, k, pstr);
  end;
  grid^.OptColumnWidth(0);
  grid^.OptColumnWidth(1);
  grid^.OptColumnWidth(2);
  grid^.OptColumnWidth(3);
  grid^.OptColumnWidth(4);
  grid^.OptColumnWidth(5);
  grid^.StretchColumn(1);
  grid^.SetSelIndex(sel);
  grid^.SetRedraw(TRUE);
  MSFreeMem(pstr, 256);
end;

procedure TEditRec.HandleList(var msg: TMessage);
begin
  if msg.LParamHI = LBN_DBLCLK then
    EditField(msg)
  else
    DefWndProc(msg);
end;

procedure TEditRec.EditField(var msg: TMessage);
var
  k    : integer;
  d    : PEditField;
begin
  k := grid^.GetSelIndex;
  if k <> -1 then
  begin
    d := New(PEditField, Init(@Self, desc, rec, k+1));
    if Application^.ExecDialog(d) = idOK then
      FillList;
  end
  else
    MessageBeep(0);
end;

procedure TEditRec.ShowKeys(var msg: TMessage);
var
  d     : PKeysInfoDlg;
begin
  d := New(PKeysInfoDlg, Init(@Self, MakeIntResource(DLG_KEYSINFO), desc));
  Application^.ExecDialog(d);
end;

procedure TEditRec.Print(var msg: TMessage);
var
  tstr : array [0..80] of char;
  d    : PPrnPrevDlg;
  pi   : PPrintInfo;
begin
  pi := New(PPrintInfo, Init(lf, 10));
  if pi = nil then
  begin
    ErrorMsg(HWindow, 'Print Error', 'Could not create PPrintInfo');
    Exit;
  end;
  with pi^ do
  begin
    header^.AddRow(r_CenterRow, 0);
    GetWindowText(HWindow, tstr, 80);
    header^.AddCell(tstr, LT300, c_Center or c_Bold, 0);
    header^.AddRow(r_Normal, 0);
    header^.AddRow(r_Normal, 0);
    header^.AddCell('Filename', LT75, 0, 0);
    GetDlgItemText(HWindow, IDC_FILENAME, tstr, 80);
    header^.AddCell(tstr, LT150, c_Bold, 0);
    header^.AddCell('Description', LT100, 0, 0);
    GetDlgItemText(HWindow, IDC_FILEDESC, tstr, 80);
    header^.AddCell(tstr, LT200, c_Bold, 0);
    header^.AddRow(r_Normal, 0);
    grid^.GetPrintHeader(header, TRUE);
    grid^.GetPrintBody(pi, TRUE);
  end;
  GetWindowText(HWindow, tstr, 80);
  StrLCat(tstr, ' - Print', 80);
  d := New(PPrnPrevDlg, Init(@self, pi, tstr));
  Application^.ExecDialog(d);
  MSDisposeObj(pi);
end;

function TEditRec.CanClose: boolean;
var
  k   : integer;
begin
  CanClose := FALSE;
  if inherited CanClose then
  begin
    if editing then
    begin
      cur^.UpdateRec(rec);
      k := cur^.dbErrorNum;
      if k <> 0 then
        ShowDBError(@Self, 'Edit Record', k)
      else
        CanClose := TRUE;
    end
    else   {- add new record }
    begin
      cur^.InsertRec(rec);
      k := cur^.dbErrorNum;
      if k <> 0 then
        ShowDBError(@Self, 'Add Record', k)
      else
        CanClose := TRUE;
    end;
  end;
end;

procedure TEditRec.GridMsg(var msg: TMessage);
var
  p2   : array[0..40] of char;
  pstr : PChar;
  code : integer;
  k    : integer;
  fi   : TFldInfo;
  asDB : PDBAssocFile;
  ai   : TAssocInfo;
begin
  case msg.WParam of
    5:
      begin
        grid^.GetData(msg.WParam, msg.LParam, p2, 10);
        k := Integer(HVal(p2, code));
        if k <> 0 then
          DisplayFlags(HWindow, k);
      end;
    else
      begin
        desc^.FieldInfo(msg.LParam + 1, fi);
        k := fi.fldAssoc;
        if desc^.AssocInfo(k, ai) then
        begin
          GetMem(pstr, 101);
          StrCopy(pstr, 'Associated file: ');
          StrLCat(pstr, ai.fName, 100);
          asDB := New(PDBAssocFile, Init(rec, k, dbOpenNormal));
          if asDB <> nil then
          begin
            asDB^.dbd^.FieldInfo(1, fi);
            StrLCat(pstr, #10, 100);
            if asDB^.dbc^.GetFirst(asDB^.dbr) then
            begin
              StrLCat(pstr, 'Field Name: ', 100);
              StrLCat(pstr, fi.fldName, 100);
              StrLCat(pstr, #10, 100);
              asDB^.dbr^.GetFieldAsStr(1, p2, 40);
            end
            else
              StrCopy(p2, 'No associated record!');
            StrLCat(pstr, p2, 100);
            MSDisposeObj(asDB);
          end;
          InfoMsg(HWindow, 'Link Information', pstr);
          MSFreeMem(pstr, 101);
        end;
      end;
  end;
end;

procedure TEditRec.DrawItem(var msg: TMessage);
begin
  grid^.DrawItem(msg);
end;

procedure TEditRec.MeasureItem(var msg: TMessage);
begin
  grid^.MeasureItem(msg);
end;

procedure TEditRec.CharToItem(var msg: TMessage);
begin
  grid^.CharToItem(msg);
end;


{------------------------------------------------------[ TKeysInfoDlg ]--}

constructor TKeysInfoDlg.Init(aParent: PWindowsObject; aName: PChar; dbDesc: PDBReadDesc);
begin
  inherited Init(aParent, aName);
  grid := New(PInfoGrid, InitResource(@Self, IDC_KEYLIST, 6, FALSE));
  desc := dbDesc;
end;

destructor TKeysInfoDlg.Done;
begin
  inherited Done;
end;

procedure TKeysInfoDlg.SetupWindow;
var
  ki      : TKeyInfo;
  i, j, k : integer;
  pstr    : array[0..20] of char;
begin
  inherited SetupWindow;

  grid^.EnableRightMouse(TRUE);

  grid^.SetHeader(0, 'Key #');
  grid^.SetHeader(1, 'Key Name');
  grid^.SetHeader(2, 'Unique');
  grid^.SetHeader(3, 'Flag');
  grid^.SetHeader(4, '# Segs');
  grid^.SetHeader(5, 'Key Len');

  grid^.SetRedraw(FALSE);
  for j := 1 to desc^.NumKeys do
  begin
    desc^.KeyInfo(j, ki);
    Str(j, pstr);
    k := grid^.AddString('');
    grid^.SetData(0, k, pstr);
    grid^.SetData(1, k, ki.keyName);
    grid^.SetData(2, k, BoolToStr(ki.unique, pstr, 10));
    HexStr(ki.keyFlag, sizeof(ki.keyFlag), pstr, 10);
    grid^.SetData(3, k, pstr);
    Str(ki.numSegs, pstr);
    grid^.SetData(4, k, pstr);
    Str(desc^.KeyLength(j), pstr);
    grid^.SetData(5, k, pstr);
  end;

  grid^.OptColumnWidth(0);
  grid^.OptColumnWidth(1);
  grid^.OptColumnWidth(2);
  grid^.OptColumnWidth(3);
  grid^.OptColumnWidth(4);
  grid^.OptColumnWidth(5);
  grid^.StretchColumn(1);
  grid^.SetRedraw(TRUE);
  grid^.SetSelIndex(0);
end;

procedure TKeysInfoDlg.Print(var msg: TMessage);
var
  pstr : PChar;
  p2   : array [0..10] of char;
  d    : PPrnPrevDlg;
  pi   : PPrintInfo;
  k, s : integer;
  f    : integer;
  ki   : TKeyInfo;
  fi   : TFldInfo;
  ai   : TAssocInfo;
  di   : TDelAssocInfo;
begin
  pi := New(PPrintInfo, Init(lf, 10));
  if pi = nil then
  begin
    ErrorMsg(HWindow, 'Print Error', 'Could not create PPrintInfo');
    Exit;
  end;
  GetMem(pstr, 101);
  with pi^ do
  begin
    header^.AddRow(r_CenterRow, 0);
    GetWindowText(HWindow, pstr, 100);
    StrLCat(pstr, ' - ', 100);
    StrLCat(pstr, desc^.FileName(p2, 10), 100);
    header^.AddCell(pstr, LT300, c_Center or c_Bold, 0);
    header^.AddRow(r_Normal, 0);
    grid^.GetPrintHeader(header, TRUE);
    grid^.GetPrintBody(pi, TRUE);
    body^.AddRow(r_Normal, 0);

    for k := 1 to desc^.NumKeys do
    begin
      StrCopy(pstr, 'Key: ');
      Str(k, p2);
      StrLCat(pstr, p2, 100);
      StrLCat(pstr, ' -- ', 100);
      desc^.KeyInfo(k, ki);
      if ki.numSegs = 0 then
        StrLCat(pstr, 'no segments defined', 100)
      else
        for s := 1 to ki.numSegs do
        begin
          f := ki.segList[s].fldNum;
          desc^.FieldInfo(f, fi);
          StrLCat(pstr, fi.fldName, 100);
          if ki.segList[s].descend then
            StrLCat(pstr, ' (D)', 100)
          else
            StrLCat(pstr, ' (A)', 100);
          if s < ki.numSegs then
            StrLCat(pstr, ', ', 100);
        end;
      body^.AddRow(r_Normal, 0);
      body^.AddCell(pstr, LT500, c_WordWrap, 0);
    end;

    if desc^.NumAssocs > 0 then
    begin
      body^.AddRow(r_Normal, 0);
      body^.AddRow(r_Normal, 0);
      body^.AddRow(r_Normal, 0);
      body^.AddCell('Asn', LT50, c_BorderBottom, 0);
      body^.AddCell('Filename', LT100, c_BorderBottom, 0);
      body^.AddCell('Key', LT50, c_BorderBottom, 0);
      body^.AddCell('Cnt', LT50, c_BorderBottom, 0);
      body^.AddCell('Fldnum', LT100, c_BorderBottom, 0);
    end;
    for k := 1 to desc^.NumAssocs do
    begin
      desc^.AssocInfo(k, ai);
      body^.AddRow(r_Normal, 0);
      Str(k, p2);
      body^.AddCell(p2, LT50, 0, 0);
      body^.AddCell(ai.fName, LT100, 0, 0);
      Str(ai.keyNum, p2);
      body^.AddCell(p2, LT50, 0, 0);
      Str(ai.numFields, p2);
      body^.AddCell(p2, LT50, 0, 0);
      if ai.fldNum[1] <> 0 then
      begin
        Str(ai.fldNum[1], p2);
        desc^.FieldInfo(ai.fldNum[1], fi);
        StrLCopy(pstr, fi.fldName, 100);
        StrLCat(pstr, ' -- (', 100);
        StrLCat(pstr, p2, 100);
        StrLCat(pstr, ')', 100);
        body^.AddCell(pstr, LT150, 0, 0);
      end
      else
        body^.AddCell('SeqNum -- (0)', LT150, 0, 0);
    end;

    if desc^.NumDelAssocs > 0 then
    begin
      body^.AddRow(r_Normal, 0);
      body^.AddRow(r_Normal, 0);
      body^.AddRow(r_Normal, 0);
      body^.AddCell('Del', LT50, c_BorderBottom, 0);
      body^.AddCell('Filename', LT100, c_BorderBottom, 0);
      body^.AddCell('Action', LT100, c_BorderBottom, 0);
      body^.AddCell('Rela', LT50, c_BorderBottom, 0);
      body^.AddCell('Fieldname', LT100, c_BorderBottom, 0);
    end;
    for k := 1 to desc^.NumDelAssocs do
    begin
      desc^.DelAssocInfo(k, di);
      body^.AddRow(r_Normal, 0);
      Str(k, p2);
      body^.AddCell(p2, LT50, 0, 0);
      body^.AddCell(di.fName, LT100, 0, 0);
      case di.delAction of
        DISALLOW_DELETE:
          StrLCopy(pstr, 'DISALLOW ', 100);
        DELETE_ASSOCIATED:
          StrLCopy(pstr, 'DELETE   ', 100);
        else
          StrLCopy(pstr, '????     ', 100);
      end;
      body^.AddCell(pstr, LT100, 0, 0);
      if di.bDownline then
        StrLCopy(pstr, 'Down ', 100)
      else
        StrLCopy(pstr, 'Up   ', 100);
      body^.AddCell(pstr, LT50, 0, 0);
      body^.AddCell(di.assocFldName, LT100, 0, 0);
    end;
  end;
  GetWindowText(HWindow, pstr, 100);
  StrLCat(pstr, ' - Print', 100);
  d := New(PPrnPrevDlg, Init(@self, pi, pstr));
  Application^.ExecDialog(d);
  MSDisposeObj(pi);
  MSFreeMem(pstr, 101);
end;

procedure TKeysInfoDlg.GridMsg(var msg: TMessage);
var
  ki   : TKeyInfo;
  fi   : TFldInfo;
  pstr : PChar;
  p2   : array[0..10] of char;
  s, f : integer;
begin
  GetMem(pstr, 101);
  s := msg.LParam + 1;
  desc^.KeyInfo(s, ki);
  StrLCopy(pstr, 'Key:  ', 100);
  Str(s, p2);
  StrLCat(pstr, p2, 100);
  StrLCat(pstr, ' -- ', 100);
  for s := 1 to ki.numSegs do
  begin
    f := ki.segList[s].fldNum;
    desc^.FieldInfo(f, fi);
    StrLCat(pstr, fi.fldName, 100);
    if ki.segList[s].descend then
      StrLCat(pstr, ' (D)', 100)
    else
      StrLCat(pstr, ' (A)', 100);
    if s < ki.numSegs then
      StrLCat(pstr, ', ', 100);
  end;
  InfoMsg(HWindow, 'Key Information', pstr);
  MSFreeMem(pstr, 101);
end;

procedure TKeysInfoDlg.DrawItem(var msg: TMessage);
begin
  grid^.DrawItem(msg);
end;

procedure TKeysInfoDlg.MeasureItem(var msg: TMessage);
begin
  grid^.MeasureItem(msg);
end;

procedure TKeysInfoDlg.CharToItem(var msg: TMessage);
begin
  grid^.CharToItem(msg);
end;


{------------------------------------------------------[ TSetKeyDlg ]--}

constructor TSetKeyDlg.Init(aParent: PWindowsObject; aName: PChar;
                            dbDesc: PDBReadDesc; aCur: PDBCursor);
begin
  inherited Init(aParent, aName, dbDesc);
  cur := aCur;
end;

procedure TSetKeyDlg.SetupWindow;
begin
  inherited SetupWindow;
  grid^.SetSelIndex(cur^.GetCurKeyNum - 1);
end;

function TSetKeyDlg.CanClose: boolean;
var
  k   : integer;
  sel : integer;
begin
  CanClose := FALSE;
  if inherited CanClose then
  begin
    sel := grid^.GetSelIndex;
    if sel = lb_Err then
      Exit;
    cur^.SetCurKeyNum(sel + 1);
    k := cur^.dbErrorNum;
    if k <> 0 then
      ShowDBError(@Self, 'Set Key', k)
    else
      CanClose := TRUE;
  end;
end;


{---------------------------------------------------------[ TEditFileDlg ]--}

constructor TEditFileDlg.Init(aParent: PWindowsObject; aFile: PChar);
begin
  inherited Init(aParent, MakeIntResource(DLG_EDITFILE));
  desc := New(PDBReadWriteDesc, Init(aFile, ''));
  if desc = nil then
  begin {- cant open file }
    ShowDBError(aParent, 'Open file', dbLastOpenError);
    Halt;
  end;
  cur := nil;
  rec := nil;
  fldCnt := desc^.NumFields;
  grid := New(PInfoGrid, InitResource(@Self, IDC_RECORDLIST,
                                      fldCnt + Integer(desc^.IsAutoSeq), FALSE));
end;

destructor TEditFileDlg.Done;
begin
  MSDisposeObj(rec);
  MSDisposeObj(cur);
  MSDisposeObj(desc);
  inherited Done;
end;

procedure TEditFileDlg.SetupWindow;
var
  pstr  : array [0..50] of char;
  p2    : array [0..10] of char;
  j, k  : integer;
  ret   : integer;
  fi    : TFldInfo;
begin
  inherited SetupWindow;
  ret := 0;
  grid^.EnableRightMouse(TRUE);

  StrCopy(pstr, 'Edit File - ');
  StrLCat(pstr, desc^.FileName(p2, 10), 50);
  SetWindowText(HWindow, pstr);

  cur := New(PDBCopyCursor, Init(desc, dbOpenNormal));
  if cur = nil then
  begin
    k := dbLastOpenError;
    StrCopy(pstr, 'Cannot open file: ');
    StrLCat(pstr, p2, 50);
    ShowDBError(@Self, pstr, k);
    ret := idCancel;
  end
  else
  begin
    k := cur^.dbErrorNum;
    if (k <> 0) and (k <> MOD_BTRV + 9) then
      ShowDBError(@Self, '', k);
    k := 0;

    rec := New(PDBRec, Init(cur));
    if rec = nil then
    begin
      k := dbLastOpenError;
      ShowDBError(@Self, 'Cannot create record buffer.', k);
      ret := idCancel;
    end
    else
    begin
      k := rec^.dbErrorNum;
      if k <> 0 then
        ShowDBError(@Self, '', k);
      k := 0;
    end;
  end;

  if k = 0 then
  begin
    j := 0;
    if desc^.IsAutoSeq then
    begin
      grid^.SetHeader(j, 'Seq #');
      Inc(j);
    end;
    {- fill header of list box }
    for k := 1 to fldCnt do
    begin
      desc^.FieldInfo(k, fi);
      grid^.SetHeader(j, fi.fldName);
      Inc(j)
    end;
    FillList(TRUE);
  end
  else
    EndDlg(ret);
end;

procedure TEditFileDlg.FillList(bInit: boolean);
{- fill list box with all records in database }
var
  pstr  : PChar;
  p2    : array [0..10] of char;
  sel   : integer;
  i     : integer;
  j, k  : integer;
begin
  GetMem(pstr, 500);
  sel := grid^.GetSelIndex;
  if sel = lb_Err then
    sel := 0;

  grid^.SetRedraw(FALSE);
  grid^.ClearList;
  j := -1;
  cur^.GetFirst(rec);
  while cur^.dbErrorNum = 0 do
  begin
    if desc^.IsAutoSeq then
    begin
      Str(rec^.GetSeqValue, p2);
      j := grid^.AddString('');
      grid^.SetData(0, j, p2);
      i := 1;
    end
    else
    begin
      rec^.GetFieldAsStr(1, pstr, 499);
      j := grid^.AddString('');
      i := 0;
    end;
    grid^.SetItemData(j, cur^.GetPosition);
    for k := 1 to fldCnt do
    begin
      rec^.GetFieldAsStr(k, pstr, 499);
      grid^.SetData(i, j, pstr);
      Inc(i);
    end;
    cur^.GetNext(rec);
    if bInit and (j = 15) then
(*  if j = 15 then             *)
      for k := 0 to fldCnt - integer(not desc^.IsAutoSeq) do
        grid^.OptColumnWidth(k);
  end;

  if bInit and (j < 15) then
(*if j < 15 then             *)
    for k := 0 to fldCnt - integer(not desc^.IsAutoSeq) do
      grid^.OptColumnWidth(k);
  grid^.SetSelIndex(sel);
  grid^.SetRedraw(TRUE);
  EnableButtons;
  MSFreeMem(pstr, 500);
end;

procedure TEditFileDlg.EditRec(var msg: TMessage);
var
  d     : PEditRec;
  k     : integer;
begin
  k := grid^.GetSelIndex;
  if k >= 0 then
  begin
    cur^.GotoPosition(rec, grid^.GetItemData(k));
    if cur^.dbErrorNum = 0 then
    begin
      d := New(PEditRec, Init(@Self, desc, cur, rec, TRUE));
      if Application^.ExecDialog(d) = idOK then
        FillList(FALSE);
    end
    else
    begin
      ShowDBError(@Self, 'Edit Record', cur^.dbErrorNum);
    end;
  end
  else
    MessageBeep(0);
end;

procedure TEditFileDlg.AddRec(var msg: TMessage);
var
  d   : PEditRec;
  k   : integer;
begin
  rec^.ClearRecord;
  if rec^.dbErrorNum = 0 then
  begin
    d := New(PEditRec, Init(@Self, desc, cur, rec, FALSE));
    if Application^.ExecDialog(d) = idOK then
      FillList(FALSE);
  end
  else
  begin
    ShowDBError(@Self, 'Edit Record', rec^.dbErrorNum);
  end;
end;

procedure TEditFileDlg.DeleteRec(var msg: TMessage);
var
  k   : integer;
begin
  k := grid^.GetSelIndex;
  if k >= 0 then
  begin
    cur^.GotoPosition(rec, grid^.GetItemData(k));
    if cur^.dbErrorNum = 0 then
    begin
      if YesNoMsg(HWindow, 'Edit Record', 'Delete the current record ?') then
      begin
        cur^.DeleteRec(rec);
        if cur^.dbErrorNum <> 0 then
          ShowDBError(@Self, 'Edit Record', cur^.dbErrorNum)
        else
          grid^.SetSelIndex(k-1);
        FillList(FALSE);
      end;
    end
    else
    begin
      ShowDBError(@Self, 'Edit Record', cur^.dbErrorNum);
    end;
  end
  else
    MessageBeep(0);
end;

procedure TEditFileDlg.ReadRec(var msg: TMessage);
begin
  FillList(TRUE);
end;

procedure TEditFileDlg.SetKey(var msg: TMessage);
var
  d     : PSetKeyDlg;
begin
  d := New(PSetKeyDlg, Init(@Self, MakeIntResource(DLG_SETKEY), desc, cur));
  if Application^.ExecDialog(d) = idOK then
    FillList(FALSE);
end;

procedure TEditFileDlg.Print(var msg: TMessage);
var
  tstr : array [0..80] of char;
  d    : PPrnPrevDlg;
  pi   : PPrintInfo;
begin
  pi := New(PPrintInfo, Init(lf, 10));
  if pi = nil then
  begin
    ErrorMsg(HWindow, 'Print Error', 'Could not create PPrintInfo');
    Exit;
  end;
  with pi^ do
  begin
    header^.AddRow(r_CenterRow, 0);
    GetWindowText(HWindow, tstr, 80);
    header^.AddCell(tstr, LT300, c_Center or c_Bold, 0);
    header^.AddRow(r_Normal, 0);
    grid^.GetPrintHeader(header, TRUE);
    grid^.GetPrintBody(pi, TRUE);
  end;
  GetWindowText(HWindow, tstr, 80);
  StrLCat(tstr, ' - Print', 80);
  d := New(PPrnPrevDlg, Init(@self, pi, tstr));
  Application^.ExecDialog(d);
  MSDisposeObj(pi);
end;

procedure TEditFileDlg.HandleList(var msg: TMessage);
begin
  if msg.lParamHi = LBN_SELCHANGE then
    EnableButtons
  else if msg.lParamHI = LBN_DBLCLK then
    EditRec(msg)
  else
    DefWndProc(msg);
end;

procedure TEditFileDlg.GridMsg(var msg: TMessage);
var
  pstr : array[0..100] of char;
  p2   : array[0..40] of char;
  k    : integer;
  fi   : TFldInfo;
  ai   : TAssocInfo;
  asDB : PDBAssocFile;
begin
  k := msg.WParam;
  if not desc^.IsAutoSeq then
    Inc(k);

  if k = 0 then
    Exit;

  desc^.FieldInfo(k, fi);
  k := fi.fldAssoc;
  if desc^.AssocInfo(k, ai) then
  begin
    StrCopy(pstr, 'Associated file: ');
    StrLCat(pstr, ai.fName, 100);
    if msg.LParam >= 0 then
    begin
      cur^.GotoPosition(rec, grid^.GetItemData(msg.LParam));
      if cur^.dbErrorNum <> 0 then
        Exit;

      asDB := New(PDBAssocFile, Init(rec, k, dbOpenNormal));
      if asDB <> nil then
      begin
        asDB^.dbd^.FieldInfo(1, fi);
        StrLCat(pstr, #10, 100);
        if asDB^.dbc^.GetFirst(asDB^.dbr) then
        begin
          StrLCat(pstr, 'Field Name: ', 100);
          StrLCat(pstr, fi.fldName, 100);
          StrLCat(pstr, #10, 100);
          asDB^.dbr^.GetFieldAsStr(1, p2, 40);
        end
        else
          StrCopy(p2, 'No associated record!');
        StrLCat(pstr, p2, 100);
        MSDisposeObj(asDB);
      end;
    end;
    InfoMsg(HWindow, 'Link Information', pstr);
  end;
end;

procedure TEditFileDlg.EnableButtons;
var
  bEnable        : boolean;
begin
  bEnable := (grid^.GetSelIndex <> lb_Err);
  EnableWindow(GetItemHandle(IDC_EDITREC), bEnable);
  EnableWindow(GetItemHandle(IDC_DELREC), bEnable);
end;

procedure TEditFileDlg.DrawItem(var msg: TMessage);
begin
  grid^.DrawItem(msg);
end;

procedure TEditFileDlg.MeasureItem(var msg: TMessage);
begin
  grid^.MeasureItem(msg);
end;

procedure TEditFileDlg.CharToItem(var msg: TMessage);
begin
  grid^.CharToItem(msg);
end;


{------------------------------------------------------[ TFileInfoDlg ]--}

constructor TFileInfoDlg.Init(aParent: PWindowsObject; aFile: PChar);
var
  c   : PControl;
begin
  inherited Init(aParent, MakeIntResource(DLG_FILEINFO));
  c := New(PLStatic, InitResource(@self, IDC_LBL1, 0, FALSE));
  c := New(PLStatic, InitResource(@self, IDC_LBL2, 0, FALSE));
  c := New(PLStatic, InitResource(@self, IDC_LBL3, 0, FALSE));
  c := New(PLStatic, InitResource(@self, IDC_LBL4, 0, FALSE));
  c := New(PLStatic, InitResource(@self, IDC_LBL5, 0, FALSE));
  c := New(PLStatic, InitResource(@self, IDC_LBL6, 0, FALSE));
  c := New(PLStatic, InitResource(@self, IDC_LBL7, 0, FALSE));
  grid := New(PInfoGrid, InitResource(@Self, IDC_FIELDLIST, 6, FALSE));
  desc := New(PDBReadDesc, Init(aFile, ''));
  if desc = nil then
  begin {- cant open file }
    ShowDBError(aParent, 'Open file', dbLastOpenError);
    Halt;
  end;
end;

destructor TFileInfoDlg.Done;
begin
  MSDisposeObj(desc);
  inherited Done;
end;

procedure TFileInfoDlg.SetupWindow;
var
  filei   : TFileInfo;
  fi      : TFldInfo;
  cur     : PDBCursor;
  i, j, k : integer;
  pstr    : array[0..20] of char;
begin
  inherited SetupWindow;

  desc^.FileInfo(filei);
  SetDlgItemText(HWindow, IDC_FILENAME, filei.fName);
  SetDlgItemText(HWindow, IDC_FILEDESC, filei.desc);
  if filei.autoSeq then
    SetDlgItemText(HWindow, IDC_SEQ, 'Yes')
  else
    SetDlgItemText(HWindow, IDC_SEQ, 'No');
  SetDlgItemText(HWindow, IDC_FLAG,
            HexStr(filei.userFlag, sizeof(filei.userFlag), pstr, 20));
  SetDlgItemInt(HWindow, IDC_SIZE, desc^.RecordSize, FALSE);
  SetDlgItemInt(HWindow, IDC_TYPE, desc^.DataType, FALSE);

  cur := New(PDBCursor, Init(desc, dbOpenNormal));
  if cur = nil then
  begin {- cant open file }
    ShowDBError(@Self, 'Open file', dbLastOpenError);
    Halt;
  end;
  SetDlgItemInt(HWindow, IDC_COUNT, cur^.NumRecords, FALSE);
  MSDisposeObj(cur);

  grid^.EnableRightMouse(TRUE);

  grid^.SetHeader(0, 'Fld#');
  grid^.SetHeader(1, 'Name');
  grid^.SetHeader(2, 'Type');
  grid^.SetHeader(3, 'Size');
  grid^.SetHeader(4, 'Flag');
  grid^.SetHeader(5, 'Assoc');

  grid^.SetRedraw(FALSE);
  i := desc^.NumFields;
  for j := 1 to i do
  begin
    desc^.FieldInfo(j, fi);
    Str(j, pstr);
    k := grid^.AddString('');
    grid^.SetData(0, k, pstr);
    grid^.SetData(1, k, fi.fldName);
    DBTypeStr(fi.fldType, pstr, 20);
    grid^.SetData(2, k, pstr);
    Str(fi.fldSize, pstr);
    grid^.SetData(3, k, pstr);
    HexStr(fi.userFlag, sizeof(fi.userFlag), pstr, 10);
    grid^.SetData(4, k, pstr);
    Str(fi.fldAssoc, pstr);
    grid^.SetData(5, k, pstr);
  end;
  grid^.OptColumnWidth(0);
  grid^.OptColumnWidth(1);
  grid^.OptColumnWidth(2);
  grid^.OptColumnWidth(3);
  grid^.OptColumnWidth(4);
  grid^.OptColumnWidth(5);
  grid^.StretchColumn(1);
  grid^.SetRedraw(TRUE);
  grid^.SetSelIndex(0);
end;

procedure TFileInfoDlg.ShowKeys(var msg: TMessage);
var
  d     : PKeysInfoDlg;
begin
  d := New(PKeysInfoDlg, Init(@Self, MakeIntResource(DLG_KEYSINFO), desc));
  Application^.ExecDialog(d);
end;

procedure TFileInfoDlg.Print(var msg: TMessage);
var
  tstr : array [0..80] of char;
  d    : PPrnPrevDlg;
  pi   : PPrintInfo;
begin
  pi := New(PPrintInfo, Init(lf, 10));
  if pi = nil then
  begin
    ErrorMsg(HWindow, 'Print Error', 'Could not create PPrintInfo');
    Exit;
  end;
  with pi^ do
  begin
    header^.AddRow(r_CenterRow, 0);
    GetWindowText(HWindow, tstr, 80);
    header^.AddCell(tstr, LT300, c_Center or c_Bold, 0);
    header^.AddRow(r_Normal, 0);
    header^.AddRow(r_Normal, 0);
    header^.AddCell('Filename', LT100, 0, 0);
    GetDlgItemText(HWindow, IDC_FILENAME, tstr, 80);
    header^.AddCell(tstr, LT100, c_Bold, 0);
    header^.AddCell('Description', LT100, 0, 0);
    GetDlgItemText(HWindow, IDC_FILEDESC, tstr, 80);
    header^.AddCell(tstr, LT150, c_Bold, 0);
    header^.AddRow(r_Normal, 0);
    header^.AddCell('Sequence Num', LT125, 0, 0);
    GetDlgItemText(HWindow, IDC_SEQ, tstr, 80);
    header^.AddCell(tstr, LT75, c_Bold, 0);
    header^.AddCell('Rec Size', LT100, 0, 0);
    GetDlgItemText(HWindow, IDC_SIZE, tstr, 80);
    header^.AddCell(tstr, LT75, c_Bold, 0);
    header^.AddCell('Rec Count', LT100, 0, 0);
    GetDlgItemText(HWindow, IDC_COUNT, tstr, 80);
    header^.AddCell(tstr, LT100, c_Bold, 0);
    header^.AddRow(r_Normal, 0);
    header^.AddCell('File Flag', LT125, 0, 0);
    GetDlgItemText(HWindow, IDC_FLAG, tstr, 80);
    header^.AddCell(tstr, LT75, c_Bold, 0);
    header^.AddCell('Data Type', LT100, 0, 0);
    GetDlgItemText(HWindow, IDC_TYPE, tstr, 80);
    header^.AddCell(tstr, LT100, c_Bold, 0);
    header^.AddRow(r_Normal, 0);
    grid^.GetPrintHeader(header, TRUE);
    grid^.GetPrintBody(pi, TRUE);
  end;
  GetWindowText(HWindow, tstr, 80);
  StrLCat(tstr, ' - Print', 80);
  d := New(PPrnPrevDlg, Init(@self, pi, tstr));
  Application^.ExecDialog(d);
  MSDisposeObj(pi);
end;

procedure TFileInfoDlg.GridMsg(var msg: TMessage);
var
  pstr : array[0..40] of char;
  p2   : array[0..10] of char;
  code : integer;
  k    : integer;
  ai   : TAssocInfo;
begin
  case msg.WParam of
    4:
      begin
        grid^.GetData(msg.WParam, msg.LParam, p2, 10);
        k := Integer(HVal(p2, code));
        if k <> 0 then
          DisplayFlags(HWindow, k);
      end;
    5:
      begin
        grid^.GetData(msg.WParam, msg.LParam, p2, 10);
        k := Integer(HVal(p2, code));
        if desc^.AssocInfo(k, ai) then
        begin
          StrCopy(pstr, 'Associated file: ');
          StrCat(pstr, ai.fName);
          InfoMsg(HWindow, 'File Information', pstr);
        end;
      end;
    else
      begin
(*      StrCopy(pstr, 'Column: ');
        Str(msg.WParam, p2);
        StrCat(pstr, p2);
        StrCat(pstr, '  Row: ');
        Str(msg.LParam, p2);
        StrCat(pstr, p2);
        InfoMsg(HWindow, 'File Information', pstr); *)
      end;
  end;
end;

procedure TFileInfoDlg.DrawItem(var msg: TMessage);
begin
  grid^.DrawItem(msg);
end;

procedure TFileInfoDlg.MeasureItem(var msg: TMessage);
begin
  grid^.MeasureItem(msg);
end;

procedure TFileInfoDlg.CharToItem(var msg: TMessage);
begin
  grid^.CharToItem(msg);
end;


{-------------------------------------------------------[ GetPassword ]--}

function GetPassword(aParent: PWindowsObject): boolean;
var
  d   : PPassDlg;
begin
  if ParamCount > 0 then
  begin
    if ParamStr(1) = 'OTTIS' then
    begin
      GetPassword := TRUE;
      Exit;
    end;
  end;
  d := New(PPassDlg, Init(aParent));
  GetPassword := Application^.ExecDialog(d) = idOK;
end;

constructor TPassDlg.Init(aParent: PWindowsObject);
begin
  inherited Init(aParent, MakeIntResource(DLG_PASSWORD));
  pass := New(PEdit, InitResource(@Self, IDC_PASSWORD, 10));
end;

function TPassDlg.CanClose: boolean;
var
  pstr  : array[0..10] of char;
begin
  CanClose := FALSE;
  pass^.GetText(pstr, 10);
  if StrComp(pstr, 'OTTIS') = 0 then
    CanClose := TRUE
  else
  begin
    ErrorMsg(HWindow, nil, 'Invalid Password');
    SetFocus(pass^.HWindow);
  end;
end;


{-------------------------------------------------------[ TMainDlg ]--}

constructor TMainDlg.Init;
begin
  inherited Init(nil, MakeIntResource(DLG_MAIN));
  grid := New(PGrid, InitResource(@Self, IDC_FILELIST, 2, FALSE));
  MakeScreenFont(lf, FALSE, FALSE);
end;

destructor TMainDlg.Done;
begin
  inherited Done;
end;

function TMainDlg.GetClassName: PChar;
begin
  GetClassName := AppName;
end;

procedure TMainDlg.GetWindowClass(var aWndClass: TWndClass);
begin
  inherited GetWindowClass(aWndClass);
  aWndClass.HIcon := LoadIcon(HInstance, MakeIntResource(ICON_1));
end;

procedure TMainDlg.SetupWindow;
var
  pstr  : PChar;
begin
  inherited SetupWindow;
  GetMem(pstr, 255);
  StrCopy(pstr, 'DB File Editor - ');
  StrCat(pstr, version);
  SetWindowText(HWindow, pstr);
  grid^.SetHeader(0, 'File Name');
  grid^.SetHeader(1, 'Description');
  grid^.SetMaxCharWidth(0, 8);
  grid^.StretchColumn(1);
  MSFreeMem(pstr, 255);
(*PostMessage(HWindow, wm_StartUp, 0, 0); *)
  FillList;
end;

procedure TMainDlg.StartUp(var msg: TMessage);
begin
  if not GetPassword(@Self) then
    PostMessage(HWindow, wm_Close, 0, 0)
  else
    FillList;
end;

procedure TMainDlg.FillList;
{- fill list box with all database files }
var
  sel   : integer;
  k     : integer;
  pstr  : array [0..50] of char;
  dInfo : TSearchRec;
  desc  : PDBReadDesc;
  filei : TFileInfo;
begin
  sel := grid^.GetSelIndex;
  if sel = lb_Err then
    sel := 0;
  grid^.SetRedraw(FALSE);
  grid^.ClearList;
  FindFirst('*.DBI', faArchive, dInfo);
  while DosError = 0 do
  begin
    StrPos(dInfo.Name, '.')^ := #0;
    desc := New(PDBReadDesc, Init(dInfo.Name, ''));
    if desc = nil then
    begin {- cant open file }
      StrCopy(pstr, 'Cannot open file: ');
      StrLCat(pstr, dInfo.Name, 50);
      ShowDBError(@Self, pstr, dbLastOpenError);
    end
    else
    begin
      desc^.FileInfo(filei);
      k := grid^.AddString(dInfo.Name);
      grid^.SetData(0, k, dInfo.Name);
      grid^.SetData(1, k, filei.desc);
      MSDisposeObj(desc);
    end;
    FindNext(dInfo);
  end;
  grid^.SetSelIndex(sel);
  grid^.SetRedraw(TRUE);
  EnableButtons;
end;

procedure TMainDlg.HandleList(var msg: TMessage);
begin
  if msg.lParamHi = LBN_SELCHANGE then
    EnableButtons
  else if msg.lParamHI = LBN_DBLCLK then
    EditFile(msg)
  else
    DefWndProc(msg);
end;

procedure TMainDlg.EditFile(var msg: TMessage);
var
  k     : integer;
  pstr  : array [0..12] of char;
  d     : PEditFileDlg;
begin
  k := grid^.GetSelIndex;
  if k >= 0 then
  begin
    grid^.GetData(0, k, pstr, 12);
    d := New(PEditFileDlg, Init(@Self, pstr));
    Application^.ExecDialog(d);
(*  FillList; *)
  end
  else
    MessageBeep(0);
end;

procedure TMainDlg.FileInfo;
var
  d     : PFileInfoDlg;
  k     : integer;
  pstr  : array [0..12] of char;
begin
  k := grid^.GetSelIndex;
  if k >= 0 then
  begin
    grid^.GetData(0, k, pstr, 12);
    d := New(PFileInfoDlg, Init(@Self, pstr));
    Application^.ExecDialog(d);
  end
  else
    MessageBeep(0);
end;

procedure TMainDlg.Print(var msg: TMessage);
var
  tstr : array [0..80] of char;
  d    : PPrnPrevDlg;
  pi   : PPrintInfo;
begin
  pi := New(PPrintInfo, Init(lf, 10));
  if pi = nil then
  begin
    ErrorMsg(HWindow, 'Print Error', 'Could not create PPrintInfo');
    Exit;
  end;
  with pi^ do
  begin
    header^.AddRow(r_CenterRow, 0);
    GetWindowText(HWindow, tstr, 80);
    header^.AddCell(tstr, LT300, c_Center or c_Bold, 0);
    header^.AddRow(r_Normal, 0);
    grid^.GetPrintHeader(header, TRUE);
    grid^.GetPrintBody(pi, TRUE);
  end;
  GetWindowText(HWindow, tstr, 80);
  StrLCat(tstr, ' - Print', 80);
  d := New(PPrnPrevDlg, Init(@self, pi, tstr));
  Application^.ExecDialog(d);
  MSDisposeObj(pi);
end;

procedure TMainDlg.EnableButtons;
var
  bEnable        : boolean;
begin
  bEnable := (grid^.GetSelIndex <> lb_Err);
  EnableWindow(GetItemHandle(IDC_EDITFILE), bEnable);
  EnableWindow(GetItemHandle(IDC_FILEINFO), bEnable);
end;

procedure TMainDlg.DrawItem(var msg: TMessage);
begin
  grid^.DrawItem(msg);
end;

procedure TMainDlg.MeasureItem(var msg: TMessage);
begin
  grid^.MeasureItem(msg);
end;

procedure TMainDlg.CharToItem(var msg: TMessage);
begin
  grid^.CharToItem(msg);
end;


procedure TEditApp.InitMainWindow;
begin
  if INIUse3D then
    Register3dApp(TRUE, TRUE, TRUE);
  mainWindow := New(PMainDlg, Init);
  if mainWindow = nil then
  begin
    FatalError('Error', 'Cannot initialize main window');
    Halt;
  end;
end;

var
  editApp : TEditApp;

BEGIN
  RegisterErrorTitle('EditDB Error');
  mnmList := New(PListObject, Init);
  editApp.Init(AppName);
  editApp.Run;
  MSDisposeObj(mnmList);
  editApp.Done;
END.
