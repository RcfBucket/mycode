Unit IntPrint;

Interface

Uses
  WinTypes,
  Objects,
  ODialogs,
  OWindows,
  WinProcs,
  IntCstm,
  IntMap,
  FileIo,
  IntGlob,
  PrnPrev,
	Strings;

const
  id_XRefOk    = 115;
  id_Abort     = 116;

Type
  PXRefSelDlg = ^TXRefSelDlg;
  TXRefSelDlg = object(TDialog)
		procedure RadioSelected(var Msg: TMessage);virtual id_First + id_XRefOk;
    procedure AbortSelection(var Msg: TMessage);virtual id_First + id_Abort;
  end;



function IntPrint_PrintParms(CurrentWnd: HWnd; PWindowsObjPtr: PWindowsObject; doPrev: boolean): boolean;
function IntPrint_PrintXRef(CurrentWnd: HWnd; PWindowsObjPtr: PWindowsObject;
  AllXRef: Boolean; doPrev: boolean): boolean;
function IntPrint_PrintMap(CurrentWnd: HWnd; PWindowsObjPtr: PWindowsObject; doPrev: boolean): boolean;
procedure IntPrint_PrintAllCstm(CurrentWnd: HWnd; PWindowsObjPtr: PWindowsObject);


Implementation

Uses
  DBFile,
  DBIDs,
  DBLib,
  DBTypes,
  MScan;

procedure TXRefSelDlg.AbortSelection(var Msg: TMessage);
{This procedure is needed to return a value of 0 rather than that of 2
 with the inherited Cancel procedure.
}
begin
  EndDlg(0);
end;

procedure TXRefSelDlg.RadioSelected(var Msg: TMessage);
var
  CurrentId, Selection, i, Result: integer;
  Selected: boolean;
  TempMsg: array[0..40] of char;
  TempVerMsg: array[0..30] of char;

begin
	Selection := 0;
	Result := 0;
  i := 1;
  CurrentId := 120;
  Selected := False;
	while (CurrentId <= 120+13) do
  begin
    Selection := SendDlgItemMessage(HWindow, CurrentId, bm_GetCheck, 0, 0);
    if Selection <> 0 then
    begin
      Result := Result + i;
      Selected := True;
    end;
    i := i * 2;
    CurrentId := CurrentId + 1;
  end;
  if not Selected then
  begin
    LoadString(HInstance, str_IntBase+str_SelItem, TempMsg, SizeOf(TempMsg));
    LoadString(HInstance, str_IntBase+str_IntMsg, TempVerMsg, SizeOf(TempVerMsg));
    MessageBox(HWindow, TempMsg, TempVerMsg, mb_ok);
  end
  else
    EndDlg(Result);
end;

function IntPrint_PrintParms(CurrentWnd: HWnd; PWindowsObjPtr: PWindowsObject; doPrev: boolean): boolean;
var
  retVal: boolean;
  ParmPrint: PMFIPrintInfo;
  PrintStr, TempName: array[0..50] of char;
  i, j, k: integer;
  TempMsg: array[0..50] of char;
  title: array[0..50] of char;

begin
  retVal:= True;
  LoadString(HInstance, str_IntBase+str_ParmList, title, SizeOf(title));
  ParmPrint := New(PMFIPrintInfo, Init(title));

  IntCstm_InitCustomCfgBuff;
  with CustomCfgBuff do
  begin
    for i := 0 to NoOfCstmParms-1 do
    begin
      StrCopy(PrintStr, CstmParmNames[i]);
      StrCat(PrintStr, ': ');
      StrCat(PrintStr, CstmParmsCfg[i].ComboSelection);
			ParmPrint^.ListLn(PrintStr);
    end;
    LoadString(HInstance, str_IntBase+str_LogLev, TempMsg, SizeOf(TempMsg));
    StrCopy(PrintStr, TempMsg);
    StrCat(PrintStr, ' ');
    StrCat(PrintStr, LogFileCfg.ComboSelection);
    ParmPrint^.ListLn(PrintStr);
    LoadString(HInstance, str_IntBase+str_LogName, TempMsg, SizeOf(TempMsg));
    StrCopy(PrintStr, TempMsg);
    StrCat(PrintStr, ' ');
    StrCat(PrintStr, IntFileNameCfg);
    ParmPrint^.ListLn(PrintStr);
    LoadString(HInstance, str_IntBase+str_TimeOut, TempMsg, SizeOf(TempMsg));
    StrCopy(PrintStr, TempMsg);
    StrCat(PrintStr, ' ');
    StrCat(PrintStr, TimeoutCfg);
    ParmPrint^.ListLn(PrintStr);
    LoadString(HInstance, str_IntBase+str_DBCSMode, TempMsg, SizeOf(TempMsg));
    StrCopy(PrintStr, TempMsg);
    StrCat(PrintStr, ' ');
    StrCat(PrintStr, DBCSModeCfg.ComboSelection);
    ParmPrint^.ListLn(PrintStr);
    LoadString(HInstance, str_IntBase+str_ConvMode, TempMsg, SizeOf(TempMsg));
    StrCopy(PrintStr, TempMsg);
    StrCat(PrintStr, ' ');
    StrCat(PrintStr, ConvModeCfg.ComboSelection);
    ParmPrint^.Listln(PrintStr);
		ParmPrint^.ListLn('');
    LoadString(HInstance, str_IntBase+str_XRefOn, TempMsg, SizeOf(TempMsg));
    ParmPrint^.ListLnSpecial(TempMsg, c_Underline);
    for i := 0 to 13 do
    begin
      StrCopy(PrintStr, '   ');
      k := 0;
			for j := 0 to StrLen(XRefTableNames[i]) do
				if XRefTableNames[i][j] <> '&' then
        begin
					TempName[k] := XRefTableNames[i][j];
          k := k + 1;
				end;
      TempName[k] := #0;
      StrCat(PrintStr, TempName);
      StrCat(PrintStr, ': ');
      if XRefChksCfg[i] = bf_Checked then
        StrCat(PrintStr, 'Y')
      else
        StrCat(PrintStr, 'N');
		 ParmPrint^.ListLn(PrintStr);
    end;
		ParmPrint^.ListLn('');
    LoadString(HInstance, str_IntBase+str_Modem, TempMsg, SizeOf(TempMsg));
    StrCopy(PrintStr, TempMsg);
    StrCat(PrintStr, ' ');
    if ModemInstalledChkCfg = bf_Checked then
      StrCat(PrintStr, 'Y')
    else
      StrCat(PrintStr, 'N');
		ParmPrint^.ListLn(PrintStr);
  end;
  IntCstm_InitStringsBuff;
	ParmPrint^.ListLn('');
  LoadString(HInstance, str_IntBase+str_ModemCfg, TempMsg, SizeOf(TempMsg));
  ParmPrint^.ListLnSpecial(TempMsg, c_Underline);
  with StringsCfgBuff do
  begin
    StrCopy(PrintStr, '   ');
    LoadString(HInstance, str_IntBase+str_ModemInit, TempMsg, SizeOf(TempMsg));
    StrCat(PrintStr, TempMsg);
    StrCat(PrintStr, ' ');
    StrCat(PrintStr, InitStrCfg);
		ParmPrint^.ListLn(PrintStr);
    StrCopy(PrintStr, '   ');
    LoadString(HInstance, str_IntBase+str_ModemDial, TempMsg, SizeOf(TempMsg));
    StrCat(PrintStr, TempMsg);
    StrCat(PrintStr, ' ');
    StrCat(PrintStr, DialStrCfg);
		ParmPrint^.ListLn(PrintStr);
    StrCopy(PrintStr, '   ');
    LoadString(HInstance, str_IntBase+str_ModemHangup, TempMsg, SizeOf(TempMsg));
    StrCat(PrintStr, TempMsg);
    StrCat(PrintStr, ' ');
    StrCat(PrintStr, HangupStrCfg);
		ParmPrint^.ListLn(PrintStr);
    StrCopy(PrintStr, '   ');
    LoadString(HInstance, str_IntBase+str_ModemBreak, TempMsg, SizeOf(TempMsg));
    StrCat(PrintStr, TempMsg);
    StrCat(PrintStr, ' ');
    StrCat(PrintStr, BreakStrCfg);
		ParmPrint^.ListLn(PrintStr);
 end;

 if doPrev then
  application^.ExecDialog(New(PPrnPrevDlg, Init(PWindowsObjPtr, ParmPrint, title)))
 else
  retVal:= PrintPrim(PWindowsObjPtr, ParmPrint, title, false);

 MSDisposeObj(ParmPrint);

 IntPrint_PrintParms:= retVal;
end;

function Bin(Val: integer): integer;
var
  i, Result: integer;

begin
  Result := 1;
  for i := 1 to Val do
     Result := Result * 2;
  Bin := Result;
end;

function GetMnemTitle(NameIndex: integer): PChar;
var
  PrintStr: array[0..MaxListLen] of char;
  i, j: integer;

begin
  j := 0;
	for i := 0 to StrLen(XRefTableNames[NameIndex]) do
		if XRefTableNames[NameIndex][i] <> '&' then
    begin
			PrintStr[j] := XRefTableNames[NameIndex][i];
      j := j + 1;
    end;
  PrintStr[j] := #0;
	GetMnemTitle := PrintStr;
end;

function PrintXRefTables(RadioSel: integer;CurrentWnd: HWnd;PWindowsObjPtr: PWindowsObject; doPrev: boolean): boolean;
const
	TrueStr: array[0..2] of char = 'T';
  AltNameCol = 13;

var
  retVal: boolean;
  i, k: integer;
	PrintStr: array[0..MaxListLen] of char;
	MnemType: array[0..2] of char;
  SecText, PrimText: array[0..MaxAltlen] of char;
	MnemPrimRec: TMnemPrimRec;
	AltPrimRec: TAltPrimRec;
	MnemSearchKey: TMnemSearchKey;
  AltSearchKey: TAltSearchKey;
  XRefPrint: PMFIPrintInfo;
  TempMsg: array[0..35] of char;
  title: array[0..35] of char;

begin
  retVal:= True;
	BtrvData[AltPrimFile].Size.Len       := SizeOf(TAltPrimRec);
	BtrvData[AltPrimFile].FileName.Name  := 'ALTPRIM.DAT   ';
	BtrvData[MnemPrimFile].Size.Len       := SizeOf(TMnemPrimRec);
	BtrvData[MnemPrimFile].FileName.Name  := 'MNEMPRIM.DAT  ';
  OpenCloseFiles(CurrentWnd, B_Open, [MnemPrimFile, AltPrimFile]);

  LoadString(HInstance, str_IntBase+str_XRefTitle, title, SizeOf(title));
  XRefPrint := New(PMFIPrintInfo, Init(title));

	for i := 13 downto 0 do
  begin
    if RadioSel - Bin(i) >= 0 then
    begin
			MnemType[0] := XRefMnemTypes[i];
      MnemType[1] := #0;
      StrCopy(PrintStr, GetMnemTitle(i));
      XRefPrint^.ListLnSpecial(PrintStr, c_Bold);
      XRefPrint^.ListLn('');
      RadioSel := RadioSel - Bin(i);
      LoadString(HInstance, str_IntBase+str_IntToDMS, TempMsg, SizeOf(TempMsg));
      StrCopy(PrintStr, TempMsg);
      XRefPrint^.ListLnSpecial(PrintStr, c_Underline);
      BtrvStat := CallDB(B_Get_Low, AltPrimFile, AltPrimRec.Start, AltSearchKey.Start, 1);
      while (BtrvStat = 0) do
			begin
        if (StrComp(AltPrimRec.IsPrim, TrueStr) = 0) and
           (StrComp(AltPrimRec.MType, MnemType) = 0) then
        begin
          XRefPrint^.body^.AddRow(r_Normal, 0);
          XRefPrint^.body^.AddCell(AltPrimRec.Mnemonic, LT125, c_Normal, 0);
          XRefPrint^.body^.AddCell('<--', LT50, c_Center, 0);
          XRefPrint^.body^.AddCell(AltPrimRec.Alternate, 0, c_Stretch, 0);
        end;
        BtrvStat := CallDB(B_Next, AltPrimFile, AltPrimRec.Start, AltSearchKey.Start, 1);
			end;
      XRefPrint^.ListLn('');
      LoadString(HInstance, str_IntBase+str_DMSToInt, TempMsg, SizeOf(TempMsg));
      StrCopy(PrintStr, TempMsg);
      XRefPrint^.ListLnSpecial(PrintStr, c_Underline);
      BtrvStat := CallDB(B_Get_Low, MnemPrimFile, MnemPrimRec.Start, MnemSearchKey.Start, 1);
      while (BtrvStat = 0) do
			begin
        if (StrComp(MnemPrimRec.IsPrim, TrueStr) = 0) and
           (StrComp(MnemPrimRec.MType, MnemType) = 0) then
        begin
          XRefPrint^.body^.AddRow(r_Normal, 0);
          XRefPrint^.body^.AddCell(MnemPrimRec.Mnemonic, LT125, c_Normal, 0);
          XRefPrint^.body^.AddCell('-->', LT50, c_Center, 0);
          XRefPrint^.body^.AddCell(MnemPrimRec.Alternate, 0, c_Stretch, 0);
         end;
        BtrvStat := CallDB(B_Next, MnemPrimFile, MnemPrimRec.Start, MnemSearchKey.Start, 1);
			end;
      XRefPrint^.ListLn('');
      XRefPrint^.ListLn('');
    end;
  end;
  OpenCloseFiles(CurrentWnd, B_Close, [MnemPrimFile, AltPrimFile]);

 if doPrev then
  application^.ExecDialog(New(PPrnPrevDlg, Init(PWindowsObjPtr, XRefPrint, title)))
 else
  retVal:= PrintPrim(PWindowsObjPtr, XRefPrint, title, false);

  MSDisposeObj(XRefPrint);

  PrintXRefTables:= retVal;
end;

function IntPrint_PrintXRef(CurrentWnd: HWnd; PWindowsObjPtr: PWindowsObject;
  AllXRef: Boolean; doPrev: boolean): boolean;
var
  retVal: boolean;
  XRefSelDlg: PXRefSelDlg;
  RadioSel: integer;

begin
  retVal:= True;
	if not AllXRef then {Print selected tables}
  begin
    XRefSelDlg := New(PXRefSelDlg, Init(PWindowsObjPtr, 'INT_PRINT_XREF'));
		RadioSel := Application^.ExecDialog(XRefSelDlg);
  end
	else {Print all tables}
		RadioSel := Bin(14)-1;
	if RadioSel <> 0 then
    retVal:= PrintXRefTables(RadioSel, CurrentWnd, PWindowsObjPtr, doPrev);
  IntPrint_PrintXRef:= retVal;
end;

function PrintMapRow(aDB: PDBFile; DMSIdx, IntIdx: integer; MapPrint: PMFIPrintInfo): PChar;
var
  FieldNameP : Array[0..24] Of Char;
  i, FieldNo, CompNo, RepNo: integer;
  FieldName: array[0..SetupNameLen] of char;
  BlankChar: char;
  fName: array[0..8] of char;

begin
  aDB^.dbd^.FileName(fName, SizeOf(fName)-1);
  StrCopy(FieldNameP, '');
  if (DMSIdx > 0) then
    aDB^.dbd^.FieldName(DMSIdx, FieldNameP, SizeOf(FieldNameP)-1);
(*  else*)
(*  begin*)
(*    if (StrComp(fName, DBSPECFile) = 0) and (DMSIdx = -1) then*)
(*      StrCopy(FieldNameP, 'Patient ID')*)
(*    else if (StrComp(fName, DBISOFile) = 0) and (DMSIdx = -1) then*)
(*      StrCopy(FieldNameP, 'Specimen ID')*)
(*    else if (StrComp(fName, DBISOFile) = 0) and (DMSIdx = -2) then*)
(*      StrCopy(FieldNameP, 'Collect Date');*)
(*  end;*)

  MapPrint^.body^.AddRow(r_Normal, 0);
  MapPrint^.body^.AddCell(FieldNameP, LT200, c_Normal, 0);

  Reset(SetupFile);
  for i := 0 to IntIdx do
    Readln(SetupFile, FieldNo, CompNo, RepNo, BlankChar, FieldName);
  Close(SetupFile);
  MapPrint^.body^.AddCell(FieldName, 0, c_Stretch, 0);
end;

procedure PrintMappedFields(MapPrint: PMFIPrintInfo;CurrWnd:HWnd);
var
  Text: array[0..65] of char;
  aDB: PDBFile;
  i, j: integer;
	ToDMSMapRec: TToDMSMapRec;
	ToIntMapRec: TToIntMapRec;
	IntKey: TIntKey;
	DMSKey: TDMSKey;
  TempMsg: array[0..35] of char;

begin
  LoadString(HInstance, str_IntBase+str_MapTitle, TempMsg, SizeOf(TempMsg));
  StrCopy(Text, TempMsg);
  MapPrint^.ListLnSpecial(Text, c_Bold);
	for j := 0 to 2 do
  begin
		MapPrint^.ListLn('');
    LoadString(HInstance, str_IntBase+str_FileTitle+j, TempMsg, SizeOf(TempMsg));
    StrCopy(Text, TempMsg);
    StrCat(Text, ' - ');
    LoadString(HInstance, str_IntBase+str_IntToDMS, TempMsg, SizeOf(TempMsg));
    StrCat(Text, TempMsg);
    MapPrint^.ListLnSpecial(Text, c_Underline);
    aDB:= New(PDBFile, Init(DrivingFiles[j], '', dbOpenNormal));
		BtrvData[ToDMSMapFile].Size.Len       := SizeOf(TToDMSMapRec);
		BtrvData[ToDMSMapFile].FileName.Name  := 'TODMSMAP.DAT  ';
    OpenCloseFiles(CurrWnd, B_Open, [ToDMSMapFile]);
		for i := 0 to 1 do
		begin
      LoadString(HInstance, str_IntBase+str_Files+i, TempMsg, SizeOf(TempMsg));
      MapPrint^.ListLn(TempMsg);
      Assign(SetupFile, SetupFiles[i]);
      BtrvStat := CallDB(B_Get_Low, ToDMSMapFile, ToDMSMapRec.Start, IntKey.Start, 1);
			while BtrvStat = 0 do
    	begin
				if (ToDMSMapRec.DMSFileNo = j) and (ToDMSMapRec.IntRecNo = i) then
        begin
          PrintMapRow(aDB, ToDMSMapRec.DMSIdxNo, ToDMSMapRec.IntIdxNo, MapPrint);
        end;
        BtrvStat := CallDB(B_Next, ToDMSMapFile, ToDMSMapRec.Start, IntKey.Start, 1);
			end;
    end;
    OpenCloseFiles(CurrWnd, B_Close, [ToDMSMapFile]);
		MapPrint^.ListLn('');
    LoadString(HInstance, str_IntBase+str_FileTitle+j, TempMsg, SizeOf(TempMsg));
    StrCopy(Text, TempMsg);
    StrCat(Text, ' - ');
    LoadString(HInstance, str_IntBase+str_DMSToInt, TempMsg, SizeOf(TempMsg));
    StrCat(Text, TempMsg);
    MapPrint^.ListLnSpecial(Text, c_Underline);
		BtrvData[ToIntMapFile].Size.Len       := SizeOf(TToIntMapRec);
		BtrvData[ToIntMapFile].FileName.Name  := 'TOINTMAP.DAT  ';
    OpenCloseFiles(CurrWnd, B_Open, [ToIntMapFile]);
		for i := 0 to 1 do
		begin
      LoadString(HInstance, str_IntBase+str_Files+i, TempMsg, SizeOf(TempMsg));
      MapPrint^.ListLn(TempMsg);
      Assign(SetupFile, SetupFiles[i]);
      BtrvStat := CallDB(B_Get_Low, ToIntMapFile, ToIntMapRec.Start, DMSKey.Start, 1);
			while BtrvStat = 0 do
    	begin
				if (ToIntMapRec.DMSFileNo = j) and (ToIntMapRec.IntRecNo = i) then
        begin
          PrintMapRow(aDB, ToIntMapRec.DMSIdxNo, ToIntMapRec.IntIdxNo, MapPrint);
				end;
        BtrvStat := CallDB(B_Next, ToIntMapFile, ToIntMapRec.Start, DMSKey.Start, 1);
			end;
    end;
    OpenCloseFiles(CurrWnd, B_Close, [ToIntMapFile]);
    MSDisposeObj(aDB);
	end;
end;

procedure PrintInterfaceFields(MapPrint: PMFIPrintInfo);
var
  FieldNo, CompNo, RepNo, i: integer;
  FieldName: array[0..SetupNameLen] of char;
  BlankChar: char;
  FieldNoS, CompNoS, RepNoS: array[0..3] of char;
  TempMsg: array[0..50] of char;

begin
	MapPrint^.ListLn('');
	MapPrint^.ListLn('');
  LoadString(HInstance, str_IntBase+str_FieldTitle, TempMsg, SizeOf(TempMsg));
  MapPrint^.ListLnSpecial(TempMsg, c_Bold);
  for i := 0 to 1 do
  begin
		MapPrint^.ListLn('');
    LoadString(HInstance, str_IntBase+str_RecTitle+i, TempMsg, SizeOf(TempMsg));
    MapPrint^.ListLnSpecial(TempMsg, c_Underline);
    MapPrint^.body^.AddRow(r_Normal, 0);
    LoadString(HInstance, str_IntBase+str_FieldNum, TempMsg, SizeOf(TempMsg));
    MapPrint^.body^.AddCell(TempMsg, LT75, c_Underline or c_Right, 0);
    LoadString(HInstance, str_IntBase+str_CompNum, TempMsg, SizeOf(TempMsg));
    MapPrint^.body^.AddCell(TempMsg, LT100, c_Underline or c_Right, 0);
    LoadString(HInstance, str_IntBase+str_RepeatNum, TempMsg, SizeOf(TempMsg));
    MapPrint^.body^.AddCell(TempMsg, LT75, c_Underline or c_Right, 0);
    LoadString(HInstance, str_IntBase+str_FieldName, TempMsg, SizeOf(TempMsg));
    MapPrint^.body^.AddCell(' ', LT25, c_Normal, 0);
    MapPrint^.body^.AddCell(TempMsg, 0, c_Underline or c_Stretch, 0);
    Assign(SetupFile, SetupFiles[i]);
    Reset(SetupFile);
    while not Eof(SetupFile) do
    begin
      readln(SetupFile, FieldNo, CompNo, RepNo, BlankChar, FieldName);
      MapPrint^.body^.AddRow(r_Normal, 0);
      Str(FieldNo, FieldNoS);
      MapPrint^.body^.AddCell(FieldNoS, LT75, c_Right, 0);
      Str(CompNo, CompNoS);
      MapPrint^.body^.AddCell(CompNoS, LT100, c_Right, 0);
      Str(RepNo, RepNoS);
      MapPrint^.body^.AddCell(RepNoS, LT75, c_Right, 0);
      MapPrint^.body^.AddCell(' ', LT25, c_Normal, 0);
      MapPrint^.body^.AddCell(FieldName, 0, c_Stretch, 0);
    end;
    Close(SetupFile);
  end;
end;

function IntPrint_PrintMap(CurrentWnd: HWnd; PWindowsObjPtr: PWindowsObject; doPrev: boolean): boolean;
var
  retVal: boolean;
  MapPrint: PMFIPrintInfo;
  title: array[0..25] of char;

begin
  retVal:= True;
  LoadString(HInstance, str_IntBase+str_MapTitle, title, SizeOf(title));
  MapPrint := New(PMFIPrintInfo, Init(title));
  PrintMappedFields(MapPrint, CurrentWnd);
  PrintInterfaceFields(MapPrint);

 if doPrev then
  application^.ExecDialog(New(PPrnPrevDlg, Init(PWindowsObjPtr, MapPrint, title)))
 else
  retVal:= PrintPrim(PWindowsObjPtr, MapPrint, title, false);

  MSDisposeObj(MapPrint);

  IntPrint_PrintMap:= retVal;
end;

procedure IntPrint_PrintAllCstm(CurrentWnd: HWnd; PWindowsObjPtr: PWindowsObject);
begin
  if IntPrint_PrintParms(CurrentWnd, PWindowsObjPtr, false) then
    if IntPrint_PrintMap(CurrentWnd, PWindowsObjPtr, false) then
      IntPrint_PrintXRef(CurrentWnd, PWindowsObjPtr, True, false);
end;

Begin
End.
