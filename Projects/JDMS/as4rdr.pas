unit AS4Rdr;
{----------------------------------------------------------------------------}
{  Module Name  : AS4RDR.PAS                                                 }
{  Programmer   : MJ                                                         }
{  Date Created : 05/95                                                      }
{                                                                            }
{  Purpose - This module contains the implementation for the AS4 Panel Reader}
{            Screen.  This module uses the AUTOSCAN module for actually      }
{            communicating with the AS4 instrument and uses the PNLVIEW      }
{            module for routines common to the W/A Panel Read Screen.        }
{                                                                            }
{  Assumptions -                                                             }
{              None.                                                         }
{                                                                            }
{  Revision History -                                                        }
{                                                                            }
{  Version  Date      Who    Did What                                        }
{  -------  --------  ------ ----------------------------------------------- }
{  1.00     05/95     MJ     Initial release.                                }
{                                                                            }
{----------------------------------------------------------------------------}

INTERFACE

uses
  DBTypes,
  OWindows;

procedure AS4ReadSession(aParent: PWindowsObject; seq: TSeqNum);

{$I AS4RDR.INC}
{$R AS4RDR.RES}

IMPLEMENTATION

uses
  CtlLib,
  Autoscan,
  APITools,
  UICommon,
  AS4QC,
  Bits,
  Biotype,
  DBFile,
  DBIds,
  DLGLIB,
  IntlLib,
  ResLib,
  DMSErr,
  INILib,
  IDLib,
  ListLib,
  MScan,
  PanDump,
  PnlView,
  Sessions,
  Strings,
  Strsw,
  PDLObj,
  PrnPrev,
  PrnObj,
  Trays,
  Win31,
  UserMsgs,
  UIIDMIC,
  WinProcs,
  WinTypes;

type
  PAS4Reader = ^TAS4Reader;
  TAS4Reader = object(TCenterDlg)
    constructor Init(aParent: PWindowsObject; seq: TSeqNum);
    destructor  Done; virtual;
    {}
    private
    wellFile      : Text;
    isolateInfo,
    recordLocked  : boolean;
    micList       : PSpaceList;
    bioChemList   : PSpaceList;
    as4Obj        : PautoScanObject;
    pdlObj        : PPdlObject;
    isoDB         : PDBFile;
    isoRef        : TSeqNum;
    biotypeObj    : PBiotypeObject;
    abbr          : string[80];
    drugDils      : DrugDilType;
    dilCnt        : integer;
    panelRead     : boolean;
    dataSaved     : boolean;
    probTxt       : PChar;
    tstGrp        : PDBFile;
    specDB        : PDBFile;
    orgDB         : PDBFile;
    sessFile      : PDBFile;
    isordDB       : PDBAssocFile;
    isoOrgRef     : TSeqNum;
    newOrgRef     : TSeqNum;
    resList       : PResults;
    curSessRef    : TSeqNum;
    setFamily     : word;
    AS4Text       : array[0..127] of char;
    {}
    procedure SetupWindow; virtual;
    procedure ShowOrganism;
    procedure ShowCurrentInfo(clearFlds: boolean);
    function  CanClose: boolean; virtual;
    procedure HandleMICList(var msg: Tmessage); virtual ID_FIRST + IDC_MICLIST;
    procedure HandleBioChemList(var msg: Tmessage); virtual ID_FIRST + IDC_BIOCHEMLIST;
    procedure FirstIsolate(seq: TSeqNum);
    procedure NextIsolate;
    procedure ShowStatus(statMsg: PChar);
    procedure ClearStatus;
    procedure DoQC(var msg: TMessage); virtual WM_FIRST + WM_GETQC;
    procedure AddExtras;
    procedure ShowBiochems;
    {- Button routines }
    procedure Next(var msg: TMessage); virtual ID_FIRST + BTN_NEXT;
    procedure ViewResults(var msg: TMessage); virtual ID_FIRST + BTN_VIEW;
    procedure EditBiotype(var msg: TMessage); virtual ID_FIRST + BTN_BIOTYPE;
    procedure SaveTests(var msg: TMessage); virtual ID_FIRST + BTN_SAVE;
    procedure SaveTheTests;
    procedure EditCurrentBiochem; virtual;
    procedure EditCurrentMIC; virtual;
    procedure PrintLog(var msg: TMessage); virtual ID_FIRST + BTN_PRINT;
    procedure ReadPanel(var msg: TMessage); virtual ID_FIRST + BTN_READ;
    procedure Calibrate(var msg: TMessage); virtual ID_FIRST + BTN_CALIBRATE;
    procedure Search(var msg: TMessage); virtual ID_FIRST + BTN_SEARCH;
    procedure DoOrganism(var msg: TMessage); virtual ID_FIRST + BTN_ORGANISM;
    function  InitIsoInfo: boolean;
    procedure EnableButtons; virtual;
    function  AskUserToSaveTests:boolean;
    procedure InitVars;
    function  LockIt: boolean;
    function  GetAS4Text(strNum: integer): PChar;
(*    procedure PrintCalReport;*)
  end;

const
  as4strBase = 2640;
  Extras     = [4,8,9];

{---------------------------------------------------------[ TAS4Reader ]--}

constructor TAS4Reader.Init(aParent: PWindowsObject; seq:TSeqNum);
var
  wait       : PWaitCursor;
  specRef    : TSeqNum;
  curSession : PChar;
  pStr,
  pStr1      : array[0..63] of char;
  c          : PLStatic;
begin
  {$IFOPT I+}
    {$DEFINE chki}
  {$ENDIF}
  {$I-}

  isordDB     := nil;
  isoOrgRef   := 0;
  newOrgRef   := 0;
  resList     := nil;
  micList     := nil;
  bioChemList := nil;
  isoDB       := nil;
  specDB      := nil;
  tstGrp      := nil;
  orgDB       := nil;
  sessFile    := nil;
  as4Obj      := nil;
  probTxt     := nil;
  biotypeObj  := nil;
  curSession  := nil;
  pdlObj      := nil;
  panelRead   := false;

  if not inherited Init(aParent, MakeIntResource(DLG_AS4READER)) then
  begin
    Done;
    fail;
  end;

  c:= New(PLStatic, InitResource(@self, IDC_LBL1, 0, false));
  c:= New(PLStatic, InitResource(@self, IDC_LBL2, 0, false));
  c:= New(PLStatic, InitResource(@self, IDC_LBL3, 0, false));
  c:= New(PLStatic, InitResource(@self, IDC_LBL4, 0, false));
  c:= New(PLStatic, InitResource(@self, IDC_LBL5, 0, false));
  c:= New(PLStatic, InitResource(@self, IDC_LBL6, 0, false));
  c:= New(PLStatic, InitResource(@self, IDC_FootnoteText, 0, false));
  c:= New(PLStatic, InitResource(@self, IDC_SpclCharText, 0, false));

  wait:= New(PWaitCursor, Init);
  micList:= New(PSpaceList, InitResource(@self, IDC_MICLIST));
  bioChemList:= New(PSpaceList, InitResource(@self, IDC_BIOCHEMLIST));
  as4Obj := New(PautoSCANObject, init);
  if as4Obj <> nil then
    if as4Obj^.as4Connected then
    begin
      PDLObj := New(PPDLObject, Init(@AS4Obj^.RawData));
      resList := New(PResults, Init);
      isoDB:= New(PDBFile, Init(DBISOFile, '', DBOpenNormal));
      specDB := New(PDBFile, Init(DBSPECFile, '', DBOpenNormal));
      tstGrp:= New(PDBFile, Init(DBTstGrpFile, '', DBOpenNormal));
      orgDB := New(PDBFile, Init(DBOrgFile, '', DBOpenNormal));
      sessFile := New(PDBFile, Init(DBSESSFile, '', DBOpenNormal));
      GetMem(probTxt, 256);
      Assign(wellFile, 'DataList.TXT');
      Reset(wellFile);
      {$IFDEF chki}
        {$I+}
        {$UNDEF chki}
      {$ENDIF}
      if (IOResult = 0) and
         (MICList <> nil) and
         (bioChemList <> nil) and
         (isoDB <> nil) and
         (specDB <> nil) and
         (tstGrp <> nil) and
         (orgDB <> nil) and
         (probTxt <> nil) and
         (sessFile <> nil) and
         (PDLObj <> nil) and
         (resList <> nil) then
      begin
        GetMem(curSession, 64);
        curSessRef := GetCurrentSession(curSession, 63);
        if INISpecimenListSort = 2 then
          if curSessRef = 0 then
            isoDB^.dbc^.SetCurKeyNum(DBISO_CDateSpecID_KEY)
          else
            isoDB^.dbc^.SetCurKeyNum(DBISO_SessionCDateSpecID_KEY)
        else if curSessRef = 0 then
          isoDB^.dbc^.SetCurKeyNum(DBISO_SpecIDCDate_KEY)
        else
          isoDB^.dbc^.SetCurKeyNum(DBISO_SessionSpecIDCDate_KEY);
        MSFreeMem(curSession, 64);
        recordLocked := false;
        FirstIsolate(seq);
        MSDisposeObj(wait);
        Exit;
      end;
    end;
  MSDisposeObj(wait);
  InfoMsg(application^.mainWindow^.hWindow,
          SR(IDS_AS4TITLE, pStr1, sizeof(pStr1)-1),
          SR(IDS_AS4NOTREADY, pStr, sizeof(pStr)-1));
  Done;
  Fail;
end;

destructor TAS4Reader.Done;
begin
  MSDisposeObj(isoDB);
  MSDisposeObj(specDB);
  MSDisposeObj(resList);
  MSDisposeObj(tstGrp);
  MSDisposeObj(orgDB);
  MSDisposeObj(sessFile);
  MSFreeMem(probTxt, 256);
  MSDisposeObj(as4Obj);
  MSDisposeObj(PDLObj);
  MSDisposeObj(biotypeObj);
  MSDisposeObj(isordDB);

  {$IFOPT I+}
    {$DEFINE chki}
  {$ENDIF}
  {$I-}

  Close(wellFile);
  if IoResult <> 0 then;

  {$IFDEF chki}
    {$I+}
    {$UNDEF chki}
  {$ENDIF}
  inherited Done;
end;

procedure TAS4Reader.SetupWindow;
var
  r    : TRect;
  font : HFont;
  isOK : boolean;
  pStr : array [0..63] of char;
begin
  inherited SetupWindow;

  {- divide biochem list box into 3 columns }
  GetClientRect(biochemList^.hWindow, r);
  SendDlgItemMsg(IDC_BIOCHEMLIST, LB_SETCOLUMNWIDTH, word((r.right-r.left) div 3), 0);

  {- divide MIC list box into 3 columns }
  GetClientRect(micList^.hWindow, r);
  SendDlgItemMsg(IDC_MICLIST, LB_SETCOLUMNWIDTH, word((r.right-r.left) div 3), 0);

  font:= GetStockObject(SYSTEM_FIXED_FONT);

  SendDlgItemMsg(IDC_BIOCHEMLIST, WM_SETFONT, font, 0);
  SendDlgItemMsg(IDC_MICLIST, WM_SETFONT, font, 0);

  if not as4Obj^.as4Connected then
  begin
    ShowStatus(SR(IDS_AS4NOTREADY, pStr, sizeof(pStr)-1));
  end
  else
  begin
    ShowCurrentInfo(true);
    PDLObj^.SetHWindow(hWindow);
    as4Obj^.SetHWindow(hWindow);
    ShowStatus(SR(IDS_SELECTAS4OPT, pStr, sizeof(pStr)-1)); {- select desired operation }
    if as4Obj^.DailyQCDue then
    begin
      PostMessage(hWindow, WM_GETQC, 0, 0);
      ShowStatus(SR(IDS_MUSTCALIBRATE, pStr, sizeof(pStr)-1));
    end
    else if as4Obj^.CalibrationDue then
    begin
      ShowStatus(SR(IDS_MUSTCALIBRATE, pStr, sizeof(pStr)-1));
    end
    else if not as4Obj^.inControl then
      ShowStatus(SR(IDS_OUTCALIBRATE, pStr, sizeof(pStr)-1));
  end;
  EnableButtons;
end;

function TAS4Reader.CanClose: boolean;
var
  pStr   : array[0..128] of char;
  pTitle : array[0..32] of char;
  wait       : PWaitCursor;
begin
  wait:= New(PWaitCursor, Init);
  CanClose := false;
  if inherited CanClose then
    if AskUserToSaveTests then
    begin
      if as4Obj <> nil then
        if as4Obj^.AS4Connected then
        begin
          InfoMsg(hWindow, SR(IDS_AS4CAUTION, pTitle, sizeof(pTitle)-1),
                  SR(IDS_CLOSEDRAWER, pStr, sizeof(pStr)-1));
          as4Obj^.CloseDrawer;
        end;
      if recordLocked then
      begin
        IsoDB^.dbc^.UnlockRec(IsoDB^.dbr);
        recordLocked := false;
      end;
      CanClose := true;
    end;
  MSDisposeObj(wait);
  EnableButtons;
end;

function TAS4Reader.GetAS4Text(strNum: integer): PChar;
begin
  SR(strNum, AS4Text, sizeof(AS4Text)-1);
  GetAS4Text:= AS4Text;
end;

function TAS4Reader.AskUserToSaveTests:boolean;
var
  pStr : array[0..63] of Char;
begin
  AskUserToSaveTests := true;
  if panelRead AND not dataSaved then
    case YesNoCancelMsg(hWindow, '', SR(IDS_AS4SAVE, pStr, sizeof(pStr)-1)) of
      IDYES   : SaveTheTests;
      IDCANCEL: AskUserToSaveTests := false;
    END;
end;

procedure TAS4Reader.EnableButtons;
begin
  if not as4Obj^.as4Connected then
  begin
    EnableWindow(GetItemHandle(BTN_READ), false);
    EnableWindow(GetItemHandle(BTN_CALIBRATE), false);
    EnableWindow(GetItemHandle(BTN_NEXT), false);
    EnableWindow(GetItemHandle(BTN_BIOTYPE), false);
    EnableWindow(GetItemHandle(BTN_VIEW), false);
    EnableWindow(GetItemHandle(BTN_SAVE), false);
    EnableWindow(GetItemHandle(BTN_PRINT), false);
    EnableWindow(GetItemHandle(BTN_SEARCH), false);
    EnableWindow(GetItemHandle(BTN_ORGANISM), false);
    EnableWindow(GetItemHandle(IDC_BIOCHEMLIST), false);
    EnableWindow(GetItemHandle(IDC_MICLIST), false);
  end
  else
  begin
    EnableWindow(GetItemHandle(BTN_CALIBRATE), true);
    EnableWindow(GetItemHandle(BTN_NEXT), isolateInfo);
    EnableWindow(GetItemHandle(BTN_VIEW), isolateInfo);
    EnableWindow(GetItemHandle(BTN_PRINT), true);
    EnableWindow(GetItemHandle(BTN_SEARCH), isolateInfo);
    EnableWindow(GetItemHandle(BTN_ORGANISM), isolateInfo and recordLocked);

    if as4Obj^.CalibrationDue or as4Obj^.DailyQCDue or
       not as4Obj^.inControl then
    begin
      EnableWindow(GetItemHandle(BTN_READ), false);
      EnableWindow(GetItemHandle(BTN_SAVE), false);
      EnableWindow(GetItemHandle(BTN_BIOTYPE), false);
      EnableWindow(GetItemHandle(IDC_BIOCHEMLIST), false);
      EnableWindow(GetItemHandle(IDC_MICLIST), false);
    end
    else
    begin
      EnableWindow(GetItemHandle(BTN_READ), isolateInfo and recordLocked);
      EnableWindow(GetItemHandle(BTN_SAVE), panelRead);
      if not isolateInfo or not recordLocked or
         (resList^.traysObj^.tfsRec.idRec = -1) then {MIC Only Panel}
      begin
        EnableWindow(GetItemHandle(BTN_BIOTYPE), FALSE);
        EnableWindow(GetItemHandle(IDC_BIOCHEMLIST), (BiochemList<>Nil) and (BiochemList^.GetCount>0));
      end
      else
      begin
        EnableWindow(GetItemHandle(BTN_BIOTYPE), panelRead);
        EnableWindow(GetItemHandle(IDC_BIOCHEMLIST), TRUE);
      end;
      EnableWindow(GetItemHandle(IDC_MICLIST), isolateInfo and recordLocked);
    end;
  end;
end;

procedure TAS4Reader.InitVars;
begin
  panelRead := false;
  dataSaved := false;
  StrCopy(probTxt, '');
  biochemList^.ClearList;
  micList^.ClearList;
  isoOrgref := 0;
  newOrgRef := 0;
end;

function TAS4Reader.InitIsoInfo: boolean;
var
  seqRef : TSeqNum;
begin
  { the isolate record and the TstGrp record have already been read in.}
  InitIsoInfo := false;
  {Read in specimen}
  if isoDB^.dbr^.GetField(DBISOSpecRef, @seqRef, sizeof(seqRef)) then
    if specDB^.dbc^.GetSeq(specDB^.dbr, seqRef) then
    begin
      isoRef := isoDB^.dbr^.GetSeqValue;
      isoDB^.dbr^.GetField(DBISOOrgRef, @isoOrgRef, sizeof(isoOrgRef));
      if (isoOrgRef <> 0) then
        orgDB^.dbc^.GetSeq(orgDB^.dbr, isoOrgRef);
      if isoDB^.dbr^.GetField(DBISOSetFamily, @setfamily, sizeof(setFamily)) then
        InitIsoInfo := true;
    end;
  ClearStatus;
end;

procedure TAS4Reader.ShowCurrentInfo(ClearFlds: boolean);
var
  pstr  : PChar;
  pstr2 : PChar;
  oSeq  : TSeqNum;
begin
  if not isolateInfo then
    Exit;

  GetMem(pstr, 256);
  GetMem(pstr2, 256);
  with specDB^.dbr^ do
  begin
    GetFieldAsStr(DBSPECSpecID, pstr, 255);
    StrLCat(pstr, '   ', 255);
    GetFieldAsStr(DBSPECCollectDate, pstr2, 255);
    StrLCat(pstr, pstr2, 255);
    SetDlgItemText(hWindow, IDC_SPECNUM, pstr);
  end;
  with isoDB^.dbr^ do
  begin
    GetFieldAsStr(DBISOIso, pstr, 255);
    SetDlgItemText(hWindow, IDC_ISONUM, pstr);

    SetDlgItemText(hWindow, IDC_PANELTYPE, resList^.traysObj^.tfsRec.tName);
    GetField(DBIsoOrgRef, @oSeq, sizeof(oSeq));
    if oSeq <> 0 then
    begin
      if orgDB^.dbc^.GetSeq(orgDB^.dbr, oSeq) then
      begin
        orgDB^.dbr^.GetFieldAsStr(DBORGSName, pstr, 255);
        SetDlgItemText(hWindow, IDC_STOREDORG, pstr);
      end
      else
        SetDlgItemText(hWindow, IDC_STOREDORG, '');
    end
    else
      SetDlgItemText(hWindow, IDC_STOREDORG, '');

   if not isoDB^.dbr^.FieldIsEmpty(DBISOTstDate) then
      SR(IDS_DATASTORED, pStr, 255)
    else
      StrCopy(pstr, '');
    SetDlgItemText(hWindow, IDC_HIDDENSTATUS, pstr);

    if clearFlds then
    begin
      SetDlgItemText(hWindow, IDC_BIOTYPE, '');
      SetDlgItemText(hWindow, IDC_ORGANISM, '');
      SetDlgItemText(hWindow, IDC_Footnotes, '');
      SetDlgItemText(hWindow, IDC_SpclChars, '');
    end;
  end;
  MSFreeMem(pstr, 256);
  MSFreeMem(pstr2, 256);
end;

procedure TAS4Reader.ShowStatus(statMsg: pChar);
begin
  SetDlgItemText(hWindow, IDC_STATUS, statMsg);
end;

procedure TAS4Reader.ClearStatus;
begin
  SetDlgItemText(hWindow, IDC_STATUS, '');
end;

procedure TAS4Reader.HandleMICList(var msg: Tmessage);
begin
  if Msg.LParamHi = LBN_DBLCLK then
    EditCurrentMIC;
end;

procedure TAS4Reader.HandleBioChemList(var msg: Tmessage);
begin
  if Msg.LParamHi = LBN_DBLCLK then
    EditCurrentBiochem;
end;

procedure TAS4Reader.EditCurrentMIC;
var
  drugRDN,
  dilStep : integer;
  pstr,
  newMIC  : PChar;
  MICOnlyPanel: boolean;
begin
  drugRDN:= MICList^.GetSelIndex;
  if drugRDN = -1 then
  begin
    MICList^.SetSelIndex(0);
    Exit;
  end;
  if drugRDN >= 0 then
  begin
    GetMem(pstr, 256);
    GetMem(newMIC, 32);
    with PDLObj^ do
      if EditMIC(@self, @MICData, resList^.traysObj, drugRDN, newMIC) then
      begin
        StrLCopy(pstr, resList^.traysObj^.dilList[drugRDN+1].drugname, 255);
        Pad(pStr,pStr, ' ', 4);
        StrLCat(pstr, newMIC, 255);
        MICList^.Deletestring(drugRDN);
        MICList^.Insertstring(pstr, drugRDN);
        MICList^.SetSelIndex(drugRDN);
        MICOnlyPanel := resList^.traysObj^.tfsRec.idRec = -1;
        if not MICOnlyPanel and
           biotypeObj^.DrugUsed(drugRDN+1) then
        begin
          GetBiotype(@Self,true,PDLDATA[ORD(class)],
                     resList, @POSFlags, @CHKFlags,
                     nil, @MICData, biotypeObj);
          StrPCopy(pstr, biotypeObj^.biotype);
          SetDlgItemText(hWindow, IDC_BIOTYPE, pstr);
          ShowOrganism;
          ShowBiochems;
        end;
      end;
    MSFreeMem(pstr, 256);
    MSFreeMem(newMIC, 32);
  end;
end;

procedure TAS4Reader.ShowOrganism;
var
  orgNum       : integer;
  pstr         : array [0..255] of char;
  FootNotes    : String;
  SpecialChars : String;
begin
  newOrgRef := 0;
  FootNotes := '';
  SpecialChars := '';
  if biotypeObj^.GetTopID(orgNum, probTxt, Addr(Footnotes), Addr(SpecialChars)) then
  begin
    if orgNum = 0 then
    begin
      SR(IDS_VRBIOTYPE, pStr, 255);
      newOrgRef:=0;
    end
    else
    begin
      Str(orgNum, pStr);
      orgDB^.dbr^.ClearRecord;
      if orgDB^.dbr^.PutFieldAsStr(DBORGOrg, pStr) then
        if orgDB^.dbc^.GetEQ(orgDB^.dbr) then
        begin
          newOrgRef := orgDB^.dbr^.GetSeqValue;
          orgDB^.dbr^.GetFieldAsStr(DBORGSName, pStr, 255);
          StrLCat(pstr, '  ', 255);
          StrLCat(pstr, probTxt, 255);
        end;
    end;
  end
  else
    SR(IDS_AS4INVBIOTYPE, pStr, 255);
  SetDlgItemText(hWindow, IDC_ORGANISM, pstr);
  Footnotes := concat(Footnotes,chr(0));
  StrCopy(pstr,Addr(Footnotes[1]));
  SetDlgItemText(hWindow, IDC_Footnotes, pstr);
  SpecialChars := concat(SpecialChars,chr(0));
  StrCopy(pstr,Addr(SpecialChars[1]));
  SetDlgItemText(hWindow, IDC_SpclChars, pstr);
end;

procedure TAS4Reader.EditCurrentBiochem;

  procedure ToggleExtra(Idx: Integer);
  var XtraID : array[0..15] of char;
      XtraRes: array[0.. 4] of char;
      WellNum: Integer;
  begin
    {Prepare the xtra test id and result}
    biochemList^.GetSelString(XtraID, 15);
    StrCopy(XtraRes,@XtraID[StrLen(XtraID)-1]);
    StrLCopy(XtraID, XtraID, 4);
    TrimAll(XtraID, XtraID, ' ', 15);

    {Where on the panel is this Xtratest?}
    ResList^.TraysObj^.GetXtraWellNumber(wellFile, XtraID, WellNum);

    {Let's toggle its result}
    Case XtraRes[0] of
      '+': ClearBit(PDLObj^.PosFlags[WellNum mod 12], (WellNum div 12));
      '-': SetBit(PDLObj^.PosFlags[WellNum mod 12], (WellNum div 12));
    end;
  end;

var
  curIdx       : integer;
  pstr         : PChar;
  bitNum       : integer;
  goodBiotype,
  editOK       : boolean;
  sugar        : boolean;
  aSet         : byte;
  MICOnly      : boolean;
begin
  curIdx:= biochemList^.GetSelIndex;
  if curIdx = -1 then
  begin
    bioChemList^.SetSelIndex(0);
    Exit;
  end;

  MICOnly:=(resList^.traysObj^.tfsRec.idRec = -1) or (biotypeObj = nil);

  if (curIdx >= 0) then
  if (not MICOnly) and (curIdx <=BioTypeObj^.MaxbitNum) then
  begin
    GetMem(pstr, 256);
    bitNum:= BioTypeObj^.MaxbitNum - curIdx;
    with PDLObj^ do
      editOK := EditBiochem(@self, biotypeObj, bitNum, resList, pstr, @posFlags, sugar);
    if editOK then
    begin
      if sugar and AllSugarsNeg(@(PDLOBJ^.posFlags)) then
      begin
        PDLObj^.PDLDATA[ORD(class)] := nonfermenter;
        biotypeObj^.SetBioClass(nonfermenter);
        aSet := ExtractSet(setFamily);
        setFamily := MakeSetFamily(aSet, biotypeObj^.class);
        isoDB^.dbr^.PutField(DBISOSetFamily, @setFamily);
      end;
      with PDLObj^ do
        goodBiotype := GetBiotype(@Self,true,PDLDATA[ORD(class)],
                                  resList, @POSFlags, @CHKFlags,
                                  nil, @MICData, biotypeObj);
      StrPCopy(pStr, biotypeObj^.biotype);
      SetDlgItemText(hWindow, IDC_BIOTYPE, pStr);
      ShowOrganism;
      ShowBiochems;
      biochemList^.SetSelIndex(curIdx);
    end;
    MSFreeMem(pstr, 256);
    biochemList^.SetSelIndex(curIdx);
  end
  else
  begin
    ToggleExtra(curIdx);
    ShowBiochems;
    biochemList^.SetSelIndex(curIdx);
  end;
  EnableButtons;
end;

function TAS4Reader.LockIt: boolean;
var
  pStr      : PChar;
begin
  LockIt := true;
  if not isoDB^.dbc^.GetLock(isoDB^.dbr) then
  begin
    LockIt := false;
    GetMem(pStr, 256);
    InfoMsg(hWindow, '', SR(IDS_RECORDLOCKED, pStr, 255));
    MSFreeMem(pStr, 256);
  end;
end;

procedure TAS4Reader.FirstIsolate(seq: TSeqNum);
var
  tstgrpref: TSeqNum;
  isOk     : boolean;
  udFlag   : boolean;
  tmpStr   : array[0..63] of char;
begin
  if recordLocked then
  begin
    IsoDB^.dbc^.UnlockRec(IsoDB^.dbr);
    recordLocked := false;
  end;

  isoRef := seq;
  isolateInfo := false;
  isoDB^.dbr^.ClearRecord;
  if curSessRef = 0 then
    isOK := isoDB^.dbc^.GetFirst(isoDB^.dbr)
  else
  begin
    isoDB^.dbr^.PutField(DBISOSess, @curSessRef);
    isOK := isoDB^.dbc^.GetFirstContains(isoDB^.dbr);
    { if isolate was selected, position on that isolate }
    if isoRef <> 0 then
      isOK := isoDB^.dbc^.GetSeq(isoDB^.dbr, isoRef);
  end;
  if isOK then
  begin
    repeat
      MSDisposeObj(isordDB);
      isordDB := New(PDBAssocFile, Init(isoDB^.dbr,DBISOLATE_ISORDER_ASSOC,dbOpenNormal));
      if isordDB^.dbc^.GetFirst(isordDB^.dbr) then
      begin
        repeat
          isordDB^.dbr^.GetField(DBISOORDTstGrpRef, @tstGrpRef, sizeof(tstGrpRef));
          tstGrp^.dbc^.GetSeq(tstGrp^.dbr, tstGrpRef);
          tstGrp^.dbr^.GetField(DBTSTGRPUDFlag, @udFlag, SizeOf(boolean));
          if not udFlag then
          begin
            tstGrp^.dbr^.GetFieldAsStr(DBTSTGrpID, tmpStr, sizeof(tmpStr)-1);
            resList^.traysObj^.LoadTray(tmpStr);
            if not resList^.traysObj^.TrayFlo then
            begin
              InitVars;
              isolateInfo := InitIsoInfo;
              if isolateInfo then
                recordLocked := LockIt;
            end;
          end;
        until isolateInfo or not isordDB^.dbc^.GetNext(isordDB^.dbr);
      end;
      if not isolateInfo then
        if curSessRef = 0 then
          isOK := isoDB^.dbc^.GetNext(isoDB^.dbr)
        else
          isOK := isoDB^.dbc^.GetNextContains(isoDB^.dbr);
    until (not isOK) or (isolateInfo);
  end;
end;

procedure TAS4Reader.NextIsolate;
var
  tstGrpRef : TSeqNum;
  isOk      : boolean;
  udFlag    : boolean;
  tmpStr    : array[0..63] of char;
  tw        : TWaitCursor;
begin
  isoRef := 0;
  isolateInfo := false;
  if recordLocked then
  begin
    IsoDB^.dbc^.UnlockRec(IsoDB^.dbr);
    recordLocked := false;
  end;

  tw.Init;
  isOK := isordDB^.dbc^.GetNext(isordDB^.dbr);
  repeat
    while not isolateInfo and isOK do
    begin
      isordDB^.dbr^.GetField(DBISOORDTstGrpRef, @tstGrpRef, sizeof(tstGrpRef));
      tstGrp^.dbc^.GetSeq(tstGrp^.dbr, tstGrpRef);
      tstGrp^.dbr^.GetField(DBTSTGRPUDFlag, @udFlag, SizeOf(boolean));
      if not udFlag then
      begin
        tstGrp^.dbr^.GetFieldAsStr(DBTSTGrpID, tmpStr, sizeof(tmpStr)-1);
        resList^.traysObj^.LoadTray(tmpStr);
        if not resList^.traysObj^.TrayFlo then
        begin
          InitVars;
          isolateInfo := InitIsoInfo;
          if isolateInfo then
            recordLocked := LockIt;
        end;
      end;
      if not isolateInfo then
        isOK := isordDB^.dbc^.GetNext(isordDB^.dbr);
    end;
    if not isolateInfo then
    begin
      repeat
        if curSessRef = 0 then
          isOK := isoDB^.dbc^.GetNext(isoDB^.dbr)
        else
          isOK := isoDB^.dbc^.GetNextContains(isoDB^.dbr);
        if not isOK then
          Break;
        MSDisposeObj(isordDB);
        isordDB := New(PDBAssocFile, Init(isoDB^.dbr, DBISOLATE_ISORDER_ASSOC, dbOpenNormal));
        isOK := isordDB^.dbc^.GetFirst(isordDB^.dbr);
      until isOK;
    end;
  until (not isOK) or (isolateInfo);
  if not isolateInfo then
    FirstIsolate(0);

  tw.Done;
end;

procedure TAS4Reader.ViewResults(var msg: TMessage);
begin
  UIIDMICEntry(@self, isoRef, true);
  EnableButtons;
end;

procedure TAS4Reader.EditBiotype(var msg: TMessage);

 function BioTestsNeeded: boolean;
 var i: integer;
 begin
  BioTestsNeeded:=False;
  for i := 0 to BioTypeObj^.MaxBitNum do
    if TestBioBit(BioTypeObj^.neededResults, i) then
       BioTestsNeeded:=True;
 end;

var
  pstr         : array[0..127] of Char;
  isok         : boolean;
  aSet         : byte;
  aFamily      : byte;
  OrgList      : TOrgList;        {List of found organisms}
  SelectedOrg  : integer;         {Organism selected in the biotype editor window}
  Footnotes    : string;          {Footnotes for selected organism}
  SpecialChars : string;          {Special chars for selected organism}
  j            : integer;
begin
 with PDLObj^ do
   if not GetBiotype(@Self,true,aFamily, resList, @POSFlags,
                     @CHKFlags, nil, @MICData, biotypeObj) then
   if BioTestsNeeded then exit;

  with PDLObj^ do
    isok := BiotypeEditor(@self, biotypeObj, resList, orgDB, probTxt, @posFlags, addr(SelectedOrg));
  if isok then
  begin
    aSet := ExtractSet(setFamily);
    setFamily := MakeSetFamily(aSet, biotypeObj^.class);
    isoDB^.dbr^.PutField(DBISOSetFamily, @setFamily);
    if biotypeObj^.identify^.IDList.NumOrgs > 0 then
    begin
      {Get org footnotes}
      For j := 1 To BiotypeObj^.Identify^.IDList.NumOrgs Do
        OrgList[j] := BiotypeObj^.Identify^.IDList.ReportOrgs.P^[j];
      OrgObject.ReportFootNotes(BiotypeObj^.Identify^.IDList.NumOrgs,SelectedOrg,OrgList,BiotypeObj^.BioType,Footnotes);
      Footnotes := concat(Footnotes,chr(0));
      StrCopy(pstr,Addr(Footnotes[1]));
      SetDlgItemText(hWindow, IDC_Footnotes, pstr);

      {Get org special characteristics}
      OrgObject.ReportSpclChars(BiotypeObj^.Identify^.IDList.ReportOrgs.P^[SelectedOrg],30,SpecialChars);
      SpecialChars := concat(SpecialChars,chr(0));
      StrCopy(pstr,Addr(SpecialChars[1]));
      SetDlgItemText(hWindow, IDC_SpclChars, pstr);

      {Get org name and probability}
      orgDB^.dbr^.GetFieldAsStr(DBORGSName, pStr, sizeof(pStr)-1);
      newOrgRef := orgDB^.dbr^.GetSeqValue;
      StrLCat(pstr, '  ', sizeof(pStr)-1);
      StrLCat(pstr, probTxt, sizeof(pStr)-1);
    end
    else
    begin
      newOrgRef := 0;
      SR(IDS_VRBIOTYPE, pStr, sizeof(pStr)-1);
    end;
    SetDlgItemText(hWindow, IDC_ORGANISM, pstr);
    StrPCopy(pstr, biotypeObj^.biotype);
    SetDlgItemText(hWindow, IDC_BIOTYPE, pstr);
    ShowBiochems;
    dataSaved:= false;
  end;
  EnableButtons;
end;

procedure TAS4Reader.Search(var msg: TMessage);
var
  p          : PSearchDlg;
  tw         : TWaitCursor;
  tmpStr     : array[0..63] of char;
  tempSpec,
  tempIso,
  tempTstGrp : longint;
  tempSeq    : TSeqNum;
begin
  if not AskUserToSaveTests then
    Exit;

  if recordLocked then
  begin
    isoDB^.dbc^.UnlockRec(isoDB^.dbr);
    recordLocked := FALSE;
  end;

  tempSpec := specDB^.dbc^.GetPosition;
  tempIso := isoDB^.dbc^.GetPosition;
  tempTstGrp := tstGrp^.dbc^.GetPosition;
  tempSeq := isordDB^.dbr^.GetSeqValue;

  p:= New(PSearchDlg, Init(@self, specDB, isoDB, tstGrp, isordDB));
  if Application^.ExecDialog(p) = idOK then
  begin
    tw.Init;
    tstGrp^.dbr^.GetFieldAsStr(DBTSTGrpID, tmpStr, sizeof(tmpStr)-1);
    resList^.traysObj^.LoadTray(tmpStr);
    if not resList^.traysObj^.TrayFlo then
    begin
      InitVars;
      isolateInfo := InitIsoInfo;
      if isolateInfo then
      begin
        recordLocked := LockIt;
        ShowCurrentInfo(true);
      end;
    end
    else
    begin
      InfoMsg(hWindow, '', SR(IDS_NORAPIDS, tmpStr, sizeof(tmpStr)-1));
      specDB^.dbc^.GoToPosition(specDB^.dbr, tempSpec);
      isoDB^.dbc^.GoToPosition(isoDB^.dbr, tempIso);
      tstGrp^.dbc^.GoToPosition(tstGrp^.dbr, tempTstGrp);
      recordLocked := LockIt;
      MSDisposeObj(isordDB);
      isordDB := New(PDBAssocFile, Init(isoDB^.dbr, DBISOLATE_ISORDER_ASSOC, dbOpenNormal));
      isordDB^.dbc^.GetSeq(isordDB^.dbr, tempSeq);
    end;
    tw.Done;
  end
  else
  begin
    specDB^.dbc^.GoToPosition(specDB^.dbr, tempSpec);
    isoDB^.dbc^.GoToPosition(isoDB^.dbr, tempIso);
    tstGrp^.dbc^.GoToPosition(tstGrp^.dbr, tempTstGrp);
    recordLocked := LockIt;
    MSDisposeObj(isordDB);
    isordDB := New(PDBAssocFile, Init(isoDB^.dbr, DBISOLATE_ISORDER_ASSOC, dbOpenNormal));
    isordDB^.dbc^.GetSeq(isordDB^.dbr, tempSeq);
  end;
  EnableButtons;
end;

procedure TAS4Reader.DoOrganism(var msg: TMessage);
var
  pStr : array[0..128] of char;
begin
  if GetAnOrganism(@Self, newOrgRef) then
  begin
    dataSaved:= false;
    orgDB^.dbc^.GetSeq(orgDB^.dbr, newOrgRef);
    orgDB^.dbr^.GetFieldAsStr(DBORGSName, pStr, sizeof(pStr)-1);
    SetDlgItemText(hWindow, IDC_ORGANISM, pStr);
    SetDlgItemText(hWindow, IDC_Footnotes, '');
    SetDlgItemText(hWindow, IDC_SpclChars, '');
    ShowBioChems;
  end;
  EnableButtons;
end;

procedure TAS4Reader.DoQC(var msg: TMessage);
var
  abort : boolean;
  c     : boolean;
  pStr  : array[0..63] of char;
  pTitle: array[0..32] of char;
begin
  abort:= false;
  while as4Obj^.DailyQCDue and not abort do
  begin
    abort := not YesNoMsg(hWindow, SR(IDS_AS4MUSTQC, pTitle, sizeof(pTitle)-1), SR(IDS_AS4ASKQC, pStr, sizeof(pStr)-1));
    if not abort then
    begin
      DoQCDiag(@self);
      as4Obj^.LoadCal;
      as4Obj^.Opendrawer;
    end;
  end;
  if abort then
  begin
    SetDlgItemText(hWindow, IDC_Status,GetAS4Text(IDS_MUSTQC));
  end;
  EnableButtons;
end;

procedure TAS4Reader.Calibrate(var msg: TMessage);
var
  wait   : PWaitCursor;
  pStr   : array[0..63] of char;
  pTitle : array[0..32] of char;
begin
  if YesNoMsg(hWindow, SR(IDS_AS4TITLE, pTitle, sizeof(ptitle)-1), SR(IDS_SURECALIBRATE, pStr, sizeof(pStr)-1)) then
  begin
    wait:= New(PWaitCursor, Init);
    ShowWindow(GetItemHandle(IDC_AbortCal),SW_SHOW);
    RedrawWindow(hWindow,Nil,0,RDW_UPDATENOW);
      as4Obj^.DoCalibrate;
    ShowWindow(GetItemHandle(IDC_AbortCal),SW_HIDE);
    MSDisposeObj(wait);
  end;
  EnableButtons;
end;

procedure TAS4Reader.PrintLog(var msg: TMessage);
var
  dump : PAS4PanelDump;
  pStr : array[0..63] of char;
begin
  if as4Obj^.inControl or True then
  begin
    dump:= New(Pas4PanelDump,
           Init(@self, isoDB, specDB, resList, biotypeObj, panelRead, PDLObj, as4Obj));
    if dump <> nil then
    begin
      dump^.PrintPanelDump;
      MSDisposeObj(dump);
    end;
  end
  else
    ShowStatus(SR(IDS_MUSTCALIBRATE, pStr, sizeof(pStr)-1));
  EnableButtons;
end;

procedure TAS4Reader.SaveTheTests;

  procedure RefreshOrgDisplay;
  var pStr: array[0..128] of char;
  begin
    orgDB^.dbc^.GetSeq(orgDB^.dbr, newOrgRef);
    orgDB^.dbr^.GetFieldAsStr(DBORGSName, pStr, sizeof(pStr)-1);
    SetDlgItemText(hWindow, IDC_ORGANISM, pStr);
    SetDlgItemText(hWindow, IDC_Footnotes, '');
    SetDlgItemText(hWindow, IDC_SpclChars, '');
  end;

var
  canceled : boolean;
  bioStr: array[0..32] of char;
  pStr: array[0..128] of char;
  pS  : array[0..128] of char;

begin
  if newOrgRef = 0 then
  begin
    if isoDB^.dbr^.FieldIsEmpty(DBISOOrgRef) then
    begin
      if GetAnOrganism(@self, newOrgRef) then
      begin
        orgDB^.dbc^.GetSeq(orgDB^.dbr, newOrgRef);
        orgDB^.dbr^.GetFieldAsStr(DBORGSName, pStr, sizeof(pStr)-1);
        SetDlgItemText(hWindow, IDC_ORGANISM, pStr);
      end
      else
        Exit;
    end
    {else
      newOrgRef := isoOrgRef;}
  end;

  with PDLObj^ do
  begin
    StrPCopy(bioStr, biotypeObj^.biotype);

    SaveIsolateTests(@self, true, true, true, true, true, canceled,
                     wellFile, orgDB, newOrgRef, bioStr,  @MICData,
                     isoDB, specDB, resList, @PosFlags);
  end;

  if not canceled then
  begin
    dataSaved:= true;

    InitIsoInfo;
    {For VRB reload from DB}
    if (NewOrgRef=0) then
        NewOrgRef:=isoOrgRef;

    recordLocked := LockIt;
    RefreshOrgDisplay;
    ShowBioChems;
  end;
  ShowCurrentInfo(false);
end;

procedure TAS4Reader.SaveTests(var msg: TMessage);
begin
  SaveTheTests;
  EnableButtons;
end;

{- Display TFG, StS, GmS, BL if present on the panel}
procedure TAS4Reader.AddExtras;
var
    i, res    : integer;
    TheOrg    : integer;

    XTraTbl   : XtraType;
    NumXtras  : integer;

    XtraID    : array[0..15] of char;
    XtraRes   : array[0.. 4] of char;
    OrgPChar  : array[0..15] of char;
    PrbTxt    : array[0..10] of char;

begin
  {Get org and extra tests}
  TheOrg:=-1;
  if NewOrgRef<>0 then
  begin
    orgDB^.dbc^.GetSeq(orgDB^.dbr, newOrgRef);
    orgDB^.dbr^.GetFieldAsStr(DBOrgOrg, orgPChar, 15);
    Val(orgPChar,TheOrg,i);
  end
  else biotypeObj^.GetTopID(TheOrg, prbTxt, nil, nil);

  {Get extra tests}
  if (TheOrg <> -1) and (not resList^.traysObj^.TrayFlo) then
  begin
    ResList^.traysObj^.GetXtra(XTraTbl, NumXtras);

    for i:=1 to NumXtras do
    begin
      {Do we care for this ExtraTest?}
      if XtraTbl[i] in Extras then
      begin
        With PDLObj^ do
        res:=GetETResult(i, XtraID, XtraTbl, resList, TheOrg,
                         wellFile, @posFlags, PMICData(@micData));
        Case res of
          1: StrCopy(XtraRes,'-');
          2: StrCopy(XtraRes,'+');
          3: StrCopy(XtraRes,'*');
        End;

        {Build the display string}
        if res<>0 then
        begin
          Pad(XtraID, XtraID, ' ', 6);
          StrCat(XtraID,XtraRes);
          BiochemList^.Addstring(@XtraID[1]);
        end;
      end;
    end;
  end;
end;

procedure TAS4Reader.ShowBiochems;
var
  j, i, k,
  group   : integer;
  pstr    : PChar;
  res     : array[0..32] of char;
  MICOnly : Boolean;

begin
  GetMem(pstr, 256);
  SendDlgItemMsg(IDC_BIOCHEMLIST, WM_SetRedraw, 0, 0);
  biochemList^.ClearList;

  MICOnly:=ResList^.traysObj^.tfsRec.idRec = -1;
  if not MICOnly then
  For i:= BiotypeObj^.MaxBitNum downTo 0 do
  begin
    StrLCopy(pstr, '***', 255);
    group := 0;  {assume bit is a biochem, not an MIC value}
    if BiotypeObj^.BitInfo(i, j, k, group, pstr) then
    begin
      Pad(pStr, pStr, ' ', 6);
      case j of
        0: StrLCat(pstr, '-', 255);
        1: StrLCat(pstr, '+', 255);
       -1: { if indole or oxidase, get value from DB }
           begin
             if i = biotypeObj^.oxidaseBitNum then
               if resList^.GetResult('.OXI', 'OXI', res, sizeof(res)-1) then
                 StrLCat(pstr, res, 255);
             if i = biotypeObj^.indoleBitNum then
               if resList^.GetResult('.IND', 'IND', res, sizeof(res)-1) then
                 StrLCat(pstr, res, 255);
           end;
        else
          StrLCat(pstr, '?', 255);
      end;
    end;
    BiochemList^.Addstring(pstr);
  end;

  AddExtras;

  SendDlgItemMsg(IDC_BIOCHEMLIST, WM_SetRedraw, 1, 0);
  InvalidateRect(BiochemList^.hWindow, nil, true);
  MSFreeMem(pstr, 256);
end;

procedure TAS4Reader.ReadPanel(var msg: TMessage);
var
  xtraTbl    : XtraType;
  numXTests,
  i, dCnt    : integer;
  goodBiotype: boolean;
  pstr       : PChar;
  hour       : PWaitCursor;
  dilstep,
  aTest      : integer;
  aSet,
  aFamily    : byte;
  tmpStr     : array[0..64] of char;
begin
  GetMem(pStr, 128);
  if resList^.traysObj^.TrayFlo then
  begin
    InfoMsg(hWindow, '', SR(IDS_NORAPIDS, pStr, 127));
    Exit;
  end;

  if not AskUserToSaveTests then
    Exit;

  biochemList^.ClearList;
  micList^.ClearList;

  if as4Obj^.DailyQCDue then
  begin
    PostMessage(hWindow, WM_GETQC, 0, 0);
    Exit;
  end;

  if as4Obj^.CalibrationDue then
  begin
    ShowStatus(SR(IDS_MUSTCALIBRATE, tmpStr, sizeof(tmpStr)-1));
    Exit;
  end;

  if not as4Obj^.inControl then
  begin
    ShowStatus(SR(IDS_OUTCALIBRATE, tmpStr, sizeof(tmpStr)-1));
    Exit;
  end;

  if not as4Obj^.AS4Ready then
  begin
    ShowStatus(SR(IDS_MUSTCALIBRATE, tmpStr, sizeof(tmpStr)-1));
    Exit;
  end;

  {$IFOPT I+}
    {$DEFINE chki}
  {$ENDIF}
  {$I-}

  hour:= New(PWaitCursor, Init);
  ShowStatus('. . .');
  MSDisposeObj(biotypeObj);
  resList^.LoadResults(isoDB^.dbr);
  with resList^.traysObj^ do
    BiotypeObj:= New(PBiotypeObject, Init(@tfsRec));

  Reset(wellFile);
  MSDisposeObj(hour);
  if IoResult <> 0 then
    ErrorMsg(hWindow,'',SR(IDS_WELLFILERESETERROR, pStr,127))
  else
  begin
    hour:= New(PWaitCursor, Init);
    MICList^.ClearList;
    panelRead := false;
    dataSaved := false;
    SendDlgItemMsg(IDC_MICLIST, WM_SETREDRAW, 0, 0);
    ShowStatus(SR(IDS_PRESSREAD, pStr, 127));

    as4Obj^.ReadTray;   {- Tell AS4 to read the panel }

    if not as4Obj^.abort then
    begin
(*      SetDlgItemText(hWindow, IDC_STATUS, GetAS4Text(IDS_OPCOMPLETE));*)
      with PDLObj^ do
      begin
        FillChar(ChkFLags, SIZEOF(ChkFlags), CHR(0));
        FillChar(PosFlags, SIZEOF(PosFlags), CHR(0));
        FillChar(FormData, SIZEOF(FormData), CHR(0));
        FillChar(MICData, SIZEOF(MICData), CHR(0));
        PDLDATA[Ord(Class)]:= -1;
        aSet := ExtractSet(setFamily);
        aFamily := ExtractFamily(setFamily);
        PDLDATA[ORD(class)] := aFamily;
        if (PDLDATA[ORD(Class)] <= 0) then
          PDLDATA[ORD(Class)]:= 1;
        PDLDATA[ORD(trayedited)]:= PDLfalse;
        PDLDATA[ORD(micssaved)]:= PDLfalse;
        EvalPanel(@as4Obj^.RawData,resList^.traysObj^.tfsRec.TrayNum);
        aFamily := PDLDATA[ORD(Class)];
      end;

      if PDLObj^.ExtNum <> resList^.traysObj^.tfsRec.TrayNum then
        SetDlgItemtext(hWindow, IDC_STATUS, GetAS4Text(IDS_PANELNOMATCH))
      else
      begin
        setFamily := MakeSetFamily(aSet, aFamily);
        isoDB^.dbr^.PutField(DBISOSetFamily, @setFamily);
        dCnt := resList^.traysObj^.DrugCnt;

        if PDLObj^.PDLData[ORD(skips)] <> 0 then
          SetDlgItemtext(hWindow, IDC_STATUS, GetAS4Text(IDS_SKIPPEDWELLS))
        else
          ShowStatus(SR(IDS_OPCOMPLETE, pStr, 127));

        for i:= 1 To dCnt do
          with resList^.traysObj^.dilList[i] do
          begin
            StrLCopy(pstr, drugname, 127);
            dilstep := PDLObj^.MICData[i];
            if (dilstep >= 0) and (dilstep <= dilCnt) then
            begin
              Pad(pstr, pStr, ' ', 4);
              StrLCat(pstr, drugDils[dilstep], 127);
            end;
            MICList^.Addstring(pstr);
          end;

        if resList^.traysObj^.tfsRec.IDRec <> -1 then {this is not an MIC only panel }
        begin
          with PDLObj^ do
            goodBiotype := GetBiotype(@Self,true,aFamily, resList, @POSFlags,
                                      @CHKFlags, nil, @MICData, biotypeObj);
          SetDlgItemText(hWindow, IDC_BIOTYPE, '');
          SetDlgItemText(hWindow, IDC_ORGANISM, '');
          SetDlgItemText(hWindow, IDC_Footnotes, '');
          SetDlgItemText(hWindow, IDC_SpclChars, '');
          StrCopy(probTxt, '');
          StrCopy(pstr, '');
          newOrgRef := 0;
          if goodBiotype then
          begin
            ShowBiochems;
            StrPCopy(pstr, biotypeObj^.biotype);
            SetDlgItemText(hWindow, IDC_BIOTYPE, pstr);
            ShowOrganism;
          end;
        end;

        SendDlgItemMsg(IDC_MICLIST, WM_SETREDRAW, 1, 0);
        InvalidateRect(MICList^.hWindow, nil, true);
        panelRead := true;
      end;
    end;
    MSDisposeObj(hour);
  end;
  MSFreeMem(pStr, 128);
  EnableButtons;
  {$IFDEF chki}
    {$I+}
    {$UNDEF chki}
  {$ENDIF}
end;

procedure TAS4Reader.Next(var msg: TMessage);
begin
  if AskUserToSaveTests then
  begin
    NextIsolate;
    ShowCurrentInfo(true);
    EnableButtons;
  end;
end;

(*
procedure TAS4Reader.PrintCalReport;
var
  printInfo : PPrintInfo;
  printFont : TLogFont;
  pstr      : array[0..127] of Char;
  i         : integer;
  calDate   : TIntlDate;
  calTime   : TIntlTime;
begin
  MakeScreenFont(printFont, false, false);
  printInfo := New(PPrintInfo, Init(printFont));

  printInfo^.header^.AddRow(r_borderBottom, 0);
  printInfo^.header^.AddCell(GetAS4Text(IDS_QCDIAG), LT200, c_Normal, 12);

  printInfo^.body^.AddRow(0, 0);
  printInfo^.body^.AddCell('', LT200, c_normal, 12);
  printInfo^.body^.AddRow(0, 0);
  printInfo^.body^.AddCell('', LT200, c_normal, 12);
  For i:= 1 To 5 do
    printInfo^.body^.AddRow(0, 0);

  printInfo^.body^.AddRow(0, 0);
  printInfo^.body^.AddCell(GetAS4Text(IDS_AS4HEADING), LT200, c_normal, 12);
  printInfo^.body^.AddRow(0, 0);
  printInfo^.body^.AddCell(GetAS4Text(IDS_CALRESULTS), LT200, c_normal, 12);
  printInfo^.body^.AddRow(0, 0);
  printInfo^.body^.AddRow(0, 0);
  printInfo^.body^.AddRow(0, 0);
  calDate := IntlCurrentDate;
  IntlDateStr(calDate, pStr, sizeof(pStr)-1);
  printInfo^.body^.AddCell(pStr, LT200, c_normal, 12);
  printInfo^.body^.AddCell('   ', LT200, c_normal, 12);

  calTime := IntlCurrentTime;
  IntlTimeStr(calTime, false, pStr, sizeof(pStr)-1);
  printInfo^.body^.AddCell(pStr, LT200, c_normal, 12);
  printInfo^.body^.AddRow(0, 0);
  printInfo^.body^.AddRow(0, 0);

  StrLCopy(pstr, GetAS4Text(IDS_CALVALUES), sizeof(pStr)-1); { "Cal" heading }
  printInfo^.body^.AddCell(pStr, LT200, c_normal, 12);
  for i := 1 to 75-StrLen(pstr) do
    printInfo^.body^.AddCell(' ', LT200, c_normal, 12);
  printInfo^.body^.AddRow(0, 0);
  printInfo^.body^.AddCell(GetAS4Text(IDS_GAINVALS), LT200, c_normal, 12);
  printInfo^.body^.AddRow(0, 0);
  printInfo^.body^.AddCell(GetAS4Text(IDS_UNDERLINEVALS), LT200, c_normal, 12);

  printDlg := New(PPrnPrevDlg, Init(aParent, printInfo, SR(IDS_CALRPTTITLE, pStr, sizeof(pStr)-1))));
  application^.ExecDialog(printInfo);

  MSDisposeObj(printInfo);
end;

*)
{--------------------------------------------------------------------------}

procedure AS4ReadSession(aParent: PWindowsObject; seq: TSeqNum);
{- reads all batterys in the current session }
var
  d : PAS4Reader;
begin
  d := nil;
  d:= New(PAS4Reader, Init(aParent, seq));
  if d <> nil then
    Application^.ExecDialog(d)
  else
    SetFocus(aparent^.hWindow);
end;

END.

