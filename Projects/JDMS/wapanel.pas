{----------------------------------------------------------------------------}
{  Module Name  : WAPanel.PAS                                                }
{  Programmer   : EJ                                                         }
{  Date Created : 05/24/95                                                   }
{                                                                            }
{  Purpose -                                                                 }
{  This module provides the user the ability to manually ID panels           }
{  that the WA instrument could not identify.                                }
{                                                                            }
{  Assumptions -                                                             }
{  The parent of the dialogs is WAMainDlg.                                   }
{                                                                            }
{  Revision History -                                                        }
{                                                                            }
{  Version  Date      Who    Did What                                        }
{  -------  --------  ------ ----------------------------------------------- }
{  1.00     05/24/95  EJ     Initial release.                                }
{                                                                            }
{----------------------------------------------------------------------------}

unit WAPanel;

INTERFACE

uses
  OWindows,
  WinTypes,
  DlgLib,
  ListLib,
  GridLib,
  DBFile,
  WAMain,
  ResLib;

type
  PIDPanelDlg = ^TIDPanelDlg;
  TIDPanelDlg = object(TCenterDlg)
    panelGrid : PGrid;
    info      : TWAGridInfo;
    listObj   : PListObject;
    wapDB     : PDBFile;
    {}
    constructor Init(aParent: PWindowsObject; aName: PChar; aInfo: TWAGridInfo);
    destructor Done; virtual;
    procedure SetupWindow; virtual;
    function  CanClose: boolean; virtual;
    procedure FillGrid;
    procedure PanelList(var msg: TMessage); virtual id_First + IDC_PANEL_LIST;
    procedure Search(var msg: TMessage); virtual id_First + IDC_SEARCH;
    procedure DeleteID(var msg: TMessage); virtual ID_FIRST + IDC_DELETEID;
    procedure EnableButtons; virtual;
    procedure DrawItem(var msg: TMessage); virtual WM_FIRST + wm_DrawItem;
    procedure MeasureItem(var msg: TMessage); virtual WM_FIRST + wm_MeasureItem;
    procedure CharToItem(var msg: TMessage); virtual WM_FIRST + wm_CharToItem;
  end;

  PConfirmDlg = ^TConfirmDlg;
  TConfirmDlg = object(TCenterDlg)
    info      : TWAGridInfo;
    s3, s4,
    s5, s6    : PChar;
    {}
    constructor Init(aParent: PWindowsObject; aName: PChar; aInfo: TWAGridInfo;
                     specID, cDate, isoID, tgID: PChar);
    procedure SetupWindow; virtual;
    procedure Yes(var msg: TMessage); virtual id_First + IDYES;
    procedure No(var msg: TMessage); virtual id_First + IDNO;
  end;

  PPanelInfoDlg = ^TPanelInfoDlg;
  TPanelInfoDlg = object(TCenterDlg)
    dlgParent : PWAMainDlg;
    {}
    constructor Init(aParent: PWAMainDlg; aName: PChar);
    procedure SetupWindow; virtual;
  end;


IMPLEMENTATION

uses
  ODialogs,
  WinProcs,
  Strings,
  StrsW,
  DMString,
  MScan,
  CtlLib,
  APITools,
  DBLib,
  DBIDs,
  DBTypes,
  PnlView,
  Barcodes,
  WAUtil,
  WAGPIB,
  DMSErr,
  DMSDebug;

function PanelHoldStatus
{===================================================================}
{  Get a PDL hold setting from the DMS.INI check if it is enabled   }
{===================================================================}
  (pkHoldName : PChar)   {Which hold to check}
  : Boolean;
Const
  EnabledFlag    = 'Enabled';
var
  HoldStatus : array[0..20] of char;
begin
  {Define default return value}
  PanelHoldStatus := FALSE;

  {Get the hold status and check it}
  GetPrivateProfileString(appName, pkHoldName, 'NONE', HoldStatus, sizeof(HoldStatus)-1, profFile);
  if (StrComp(HoldStatus,EnabledFlag)=0) then
    PanelHoldStatus := TRUE;
end;

{3:56pm 5/28/98}
Function ConvertClass
{===================================================================}
{  Change class value to convey OxMRSA or VaStS hold status to PDL  }
{===================================================================}
  (CurrentClass    : Integer;  {Unmodified class}
   PanelNum        : Integer;  {Current Panel}
   dbr             : PDBRec)   {Pointer to results}
  : Integer;
Const
  StrepClass           = 1;  {System defined strep}
  StaphClass           = 2;  {System defined staph}
  VaStSHoldClass       = 3;  {Indicates to PDL to hold strep based on Va/StS}
  VaOnlyHoldClass      = 5;  {Indicates to PDL to hold strep based on Va only}
  OxMultResHoldClass   = 4;  {Indicates to PDL to hold staph based on multiple resistance}
  PosID1ProbSet        = 5;  {Probability set for PID1}
  PosID2ProbSet        = 2;  {Probability set for PID2}
Var
  results              : PResults;              {Points to a result record}
  res                  : array[0..32] of char;  {Returned extra test values}
  IsPanelConv          : Boolean;               {Panel check}
  IsPanelGramPos       : boolean;               {Panel check}
  IsBetaHemNeg         : boolean;               {Result check}
  PanelNumStr          : string;                {String representation of panel number}
  PanelProbSet         : Integer;               {Probability set of the panel}
  VaStSHoldEnabled     : Boolean;               {Hold status}
  VaOnlyHoldEnabled    : Boolean;               {Hold status}
  OxMultResHoldEnabled : Boolean;               {Hold status}
Begin
  {Get the panel record from the database}
  results := New(PResults, Init);
  results^.traysObj^.LoadTrayByNum(PanelNum+1); {offset panel for tray database}
  results^.LoadResults(dbr);

  {Get some facts about the panel}
  IsPanelGramPos       := (results^.TraysObj^.TFSRec.PSet=PosID1ProbSet) or
                          (results^.TraysObj^.TFSRec.PSet=PosID2ProbSet);
  IsPanelConv          := (not results^.TraysObj^.Trayflo);
  VaStSHoldEnabled     := PanelHoldStatus('VaStSHold');
  VaOnlyHoldEnabled    := PanelHoldStatus('VaOnlyHold');
  OxMultResHoldEnabled := PanelHoldStatus('OxMultResHold');

  {Set the default class value}
  ConvertClass := CurrentClass;

  {Determine if the class must change}
  If ( IsPanelConv and IsPanelGramPos ) then
  Begin
    if (CurrentClass=StrepClass) then
    Begin
      {Determine the beta-hemolysis result}
      IsBetaHemNeg := TRUE;
      If results^.GetResult('.HEM', 'HEM', res, 32) then
        If StrComp(res, '') <> 0 then
          If StrComp(res, '+') = 0 then
            IsBetaHemNeg := FALSE;
      If ( IsBetaHemNeg and VaOnlyHoldEnabled ) Then
        If ( VaStSHoldEnabled ) Then
          ConvertClass := VaStSHoldClass
        Else
          ConvertClass := VaOnlyHoldClass;
    End
    Else
      If ( CurrentClass=StaphClass ) and OxMultResHoldEnabled Then
        ConvertClass := OxMultResHoldClass;
  End;
  MSDisposeObj(results);
End;

{-------------------------------------------------------[ TIDPanelDlg ]--}
constructor TIDPanelDlg.Init(aParent: PWindowsObject; aName: PChar; aInfo: TWAGridInfo);
var
  c   : PControl;
begin
  inherited Init(aParent, aName);
  c := New(PLStatic, InitResource(@self, IDC_LBL1, 0, FALSE));
  c := New(PLStatic, InitResource(@self, IDC_LBL2, 0, FALSE));
  info := aInfo;
  listObj := New(PListObject, Init);
  wapDB := PWAMainDlg(aParent)^.wapDB;
  panelGrid := New(PGrid, InitResource(@self, IDC_PANEL_LIST, 5, FALSE));
end;

destructor TIDPanelDlg.Done;
begin
  MSDisposeObj(listObj);
  inherited Done;
end;

procedure TIDPanelDlg.SetupWindow;
var
  pstr  : array [0..40] of char;
begin
  inherited SetupWindow;
  Str(info.tower, pstr);
  SetDlgItemText(HWindow, IDC_TOWER, pstr);
  Str(info.slot, pstr);
  SetDlgItemText(HWindow, IDC_SLOT, pstr);
  panelGrid^.SetHeader(0, SR(IDS_SPECID, pstr, 40));
  panelGrid^.SetHeader(1, SR(IDS_COLLDT, pstr, 40));
  panelGrid^.SetHeader(2, SR(IDS_ISOID, pstr, 40));
  panelGrid^.SetHeader(3, SR(IDS_TEST_GROUP, pstr, 40));
  panelGrid^.SetHeader(4, SR(IDS_PANELID, pstr, 40));
  FillGrid;
  panelGrid^.OptColumnWidth(0);
  panelGrid^.OptColumnWidth(1);
  panelGrid^.OptColumnWidth(2);
  panelGrid^.OptColumnWidth(3);
  panelGrid^.OptColumnWidth(4);
  panelGrid^.SetSearchColumn(4);
  EnableButtons;
end;

function TIDPanelDlg.CanClose: boolean;
var
  sel      : integer;
  seq      : TSeqNum;
  dbr      : PDBRec;
  pstr     : array [0..20] of char;
  WASSCCmd : PWASSCCmd;
  panelNum : byte;
  setfam   : word;
  family   : byte;
  panelID  : integer;
  flag     : boolean;
  wc       : PWaitCursor;
begin
  CanClose := FALSE;
  sel := panelGrid^.GetSelIndex;
  if sel = lb_Err then
  begin
    ShowError(@self, IDS_WA_ERR, nil, 1, MOD_WAPANEL, 1);
    Exit;
  end;

  panelID := panelGrid^.GetItemData(sel);
  wapDB^.dbr^.PutField(DBWAPNLID, @panelID);
  if not wapDB^.dbc^.GetEQ(wapDB^.dbr) then
  begin
    ShowError(@self, IDS_WA_ERR, nil, wapDB^.dbc^.dbErrorNum, MOD_WAPANEL, 2);
    Exit;
  end;

  panelGrid^.GetData(3, sel, pstr, 20);
  panelNum := PWAMainDlg(parent)^.trayObj^.MapTestGroupID(pstr)-1;
  wapDB^.dbr^.GetField(DBWAPNLIsoOrdRef, @seq, SizeOf(TSeqNum));
  dbr := listObj^.PeekSeq(DBISOORDFile, seq); { Read-only return value }
  if dbr = nil then
  begin
    ShowError(@self, IDS_WA_ERR, nil, 1, MOD_WAPANEL, 3);
    Exit;
  end;
  dbr^.GetField(DBISOORDIsoRef, @seq, SizeOf(TSeqNum));
  dbr := listObj^.PeekSeq(DBISOFile, seq); { Read-only return value }
  if dbr = nil then
  begin
    ShowError(@self, IDS_WA_ERR, nil, 1, MOD_WAPANEL, 4);
    Exit;
  end;
  dbr^.GetField(DBISOSetFamily, @setfam, SizeOf(setfam));
  family := ExtractFamily(setfam);

  {Convert organism class to support the MRSA/VRE holds, JDMS v4.00, ghg 5/28/98}
  Family := ConvertClass
    ( Family,      {CurrentClass : Integer}
      PanelNum,    {PanelNum     : Integer}
      dbr );       {dbr          : PDBRec}

  if not wapDB^.dbc^.GetLock(wapDB^.dbr) then
  begin
    ShowError(@self, IDS_WA_ERR, nil, wapDB^.dbc^.dbErrorNum, MOD_WAPANEL, 5);
    Exit;
  end;

  wc := New(PWaitCursor, Init);
  WASSCCmd := New(PWASSCCmd,Init(WAID));
  WASSCCmd^.SetParams(GetWASlot(info.slot, info.tower), panelNum, family, panelID);
  if WASSCCmd^.SendWAMessage then
  begin
    flag := TRUE;
    wapDB^.dbr^.PutField(DBWAPNLKnownToWA, @flag);
    if not wapDB^.dbc^.UpdateRec(wapDB^.dbr) then
      ShowError(@self, IDS_WA_ERR, nil, wapDB^.dbc^.dbErrorNum, MOD_WAPANEL, 6)
    else
      CanClose := TRUE;
  end
  else
  begin
    wapDB^.dbc^.UnlockRec(wapDB^.dbr);
    ShowError(@self, IDS_WA_ERR, nil, WASSCCmd^.WAErrNum, MOD_WAPANEL, 7);
  end;
  MSDisposeObj(wc);
  MSDisposeObj(WASSCCmd);
end;

procedure TIDPanelDlg.FillGrid;
var
  k      : integer;
  id     : integer;
  flag   : boolean;
  seq    : TSeqNum;
  dbr    : PDBRec;
  dbr2   : PDBRec;
  pstr   : array [0..20] of char;
  used   : boolean;
begin
  if wapDB^.dbc^.GetFirst(wapDB^.dbr) then
  begin
    panelGrid^.ClearList;
    repeat
      wapDB^.dbr^.GetField(DBWAPNLBCPrinted, @flag, SizeOf(flag));
      wapDB^.dbr^.GetField(DBWAPNLUsedFlag, @used, SizeOf(used));
      if flag and used then
      begin
        wapDB^.dbr^.GetField(DBWAPNLKnownToWA, @flag, SizeOf(flag));
        if not flag then
        begin
          wapDB^.dbr^.GetFieldAsStr(DBWAPNLID, pstr, 20);
          k := panelGrid^.AddString(pstr);
          wapDB^.dbr^.GetField(DBWAPNLID, @id, SizeOf(id));
          panelGrid^.SetItemData(k, id);
          panelGrid^.SetData(4, k, pstr);
          wapDB^.dbr^.GetField(DBWAPNLIsoOrdRef, @seq, SizeOf(TSeqNum));
          dbr := listObj^.PeekSeq(DBISOORDFile, seq); { Read-only return value }
          if dbr = nil then
            Continue;
          dbr^.GetField(DBISOORDTstGrpRef, @seq, SizeOf(TSeqNum));
          dbr2 := listObj^.PeekSeq(DBTSTGRPFile, seq); { Read-only return value }
          if dbr2 = nil then
            Continue;
          dbr2^.GetFieldAsStr(DBTSTGRPID, pstr, 20);
          panelGrid^.SetData(3, k, pstr);
          dbr^.GetField(DBISOORDIsoRef, @seq, SizeOf(TSeqNum));
          dbr2 := listObj^.PeekSeq(DBISOFile, seq); { Read-only return value }
          if dbr2 = nil then
            Continue;
          dbr2^.GetFieldAsStr(DBISOIso, pstr, 20);
          panelGrid^.SetData(2, k, pstr);
          dbr2^.GetFieldAsStr(DBISOSpecID, pstr, 20);
          panelGrid^.SetData(0, k, pstr);
          dbr2^.GetFieldAsStr(DBISOCollectDate, pstr, 20);
          panelGrid^.SetData(1, k, pstr);
        end;
      end;
    until not wapDB^.dbc^.GetNext(wapDB^.dbr);
  end;
end;

procedure TIDPanelDlg.PanelList(var msg: TMessage);
begin
  if msg.lParamHi = LBN_SELCHANGE then
    EnableButtons
  else
    DefWndProc(msg);
end;

procedure TIDPanelDlg.Search(var msg: TMessage);
var
  p          : PSearchDlg;
  isoDB,
  specDB,
  tstGrp     : PDBFile;
  isoOrdDB   : PDBAssocFile;
  seq        : TSeqNum;
  pstr       : array [0..5] of char;
  specID     : array [0..16] of char;
  cDate      : array [0..16] of char;
  isoID      : array [0..5] of char;
  tgID       : array [0..16] of char;
  sel        : integer;
  pID        : integer;
  flag       : boolean;

  function UserConfirms: boolean;
  var
    d    : PConfirmDlg;
  begin
    isoDB^.dbr^.GetFieldAsStr(DBISOSpecID, specID, 16);
    isoDB^.dbr^.GetFieldAsStr(DBISOCollectDate, cDate, 16);
    isoDB^.dbr^.GetFieldAsStr(DBISOIso, isoID, 5);
    tstGrp^.dbr^.GetFieldAsStr(DBTSTGRPID, tgID, 16);
    d := New(PConfirmDlg, Init(@self, MakeIntResource(DLG_CONFIRM),
                              info, specID, cDate, isoID, tgID));
    UserConfirms := Application^.ExecDialog(d) = IDYES;
  end;

begin
  specDB := New(PDBFile, Init(DBSPECFile, '', dbOpenNormal));
  isoDB := New(PDBFile, Init(DBISOFile, '', dbOpenNormal));
  tstGrp := New(PDBFile, Init(DBTstGrpFile, '', DBOpenNormal));
  isoOrdDB := New(PDBAssocFile, Init(isoDB^.dbr, DBISOLATE_ISORDER_ASSOC, dbOpenNormal));
  p := New(PSearchDlg, Init(@self, specDB, isoDB, tstGrp, isoOrdDB));
  if (Application^.ExecDialog(p) = IDOK) and UserConfirms then
  begin
    wapDB^.dbc^.SetCurKeyNum(DBWAPNL_IsoOrd_KEY);
    wapDB^.dbr^.ClearRecord;
    seq := isoOrdDB^.dbr^.GetSeqValue;
    wapDB^.dbr^.PutField(DBWAPNLIsoOrdRef, @seq);
    if not wapDB^.dbc^.GetEQ(wapDB^.dbr) then
    begin
      flag := TRUE;
      wapDB^.dbr^.PutField(DBWAPNLUsedFlag, @flag);
      pID := GetNextPanelID;
      wapDB^.dbr^.PutField(DBWAPNLID, @pID);
      StampPanelID(PId);
      if wapDB^.dbc^.InsertRec(wapDB^.dbr) then
        wapDB^.dbc^.GetEQ(wapDB^.dbr);
    end;

    wapDB^.dbr^.GetFieldAsStr(DBWAPNLID, pstr, SizeOf(pstr));
    sel := panelGrid^.FindString(pstr, -1, TRUE);
    if sel = -1 then
    begin
      sel := panelGrid^.AddString(pstr);
      wapDB^.dbr^.GetField(DBWAPNLID, @pID, SizeOf(pID));
      panelGrid^.SetItemData(sel, pID);
      panelGrid^.SetData(4, sel, pstr);
      panelGrid^.SetData(0, sel, specID);
      panelGrid^.SetData(1, sel, cDate);
      panelGrid^.SetData(2, sel, isoID);
      panelGrid^.SetData(3, sel, tgID);
    end;

    panelGrid^.SetSelIndex(sel);
    wapDB^.dbc^.SetCurKeyNum(DBWAPNL_ID_KEY);

    if CanClose then
      EndDlg(IDOK);
  end;
  MSDisposeObj(specDB);
  MSDisposeObj(isoDB);
  MSDisposeObj(tstGrp);
  MSDisposeObj(isoOrdDB);
end;

procedure TIDPanelDlg.DeleteID(var msg: TMessage);
{- for the selected wapanel id record, flag it as unused }
var
  row   : integer;
  id    : integer;
  used  : boolean;
begin
  row:= panelGrid^.GetSelIndex;
  if row >= 0 then
  begin
    id:= integer(panelGrid^.GetItemData(row));
    wapDB^.dbr^.ClearRecord;
    wapDB^.dbr^.PutField(DBWAPNLID, @id);
    if wapDB^.dbc^.GetEQ(wapDB^.dbr) then
    begin
      used:= false;
      wapDB^.dbr^.PutField(DBWAPNLUsedFlag, @used);
      if not wapDB^.dbc^.UpdateRec(wapDB^.dbr) then
        ShowError(@self, IDS_WA_ERR, nil, wapDB^.dbc^.dbErrorNum, MOD_WAPANEL, 8)
      else
        FillGrid;
    end;
  end;
  FocusCtl(hWindow, IDC_PANEL_LIST);
end;

procedure TIDPanelDlg.EnableButtons;
var
  bEnable        : boolean;
begin
  bEnable := (panelGrid^.GetSelIndex <> lb_Err);
  EnableWindow(GetItemHandle(IDOK), bEnable);
  EnableWindow(GetItemHandle(IDC_DELETEID), bEnable);
end;

procedure TIDPanelDlg.DrawItem(var msg: TMessage);
begin
  panelGrid^.DrawItem(msg);
end;

procedure TIDPanelDlg.MeasureItem(var msg: TMessage);
begin
  panelGrid^.MeasureItem(msg);
end;

procedure TIDPanelDlg.CharToItem(var msg: TMessage);
begin
  panelGrid^.CharToItem(msg);
end;


{-------------------------------------------------------[ TConfirmDlg ]--}
constructor TConfirmDlg.Init(aParent: PWindowsObject; aName: PChar; aInfo: TWAGridInfo;
                             specID, cDate, isoID, tgID: PChar);
var
  c   : PControl;
begin
  inherited Init(aParent, aName);
  info := aInfo;
  s3 := specID;
  s4 := cDate;
  s5 := isoID;
  s6 := tgID;
  c := New(PLStatic, InitResource(@self, IDC_LBL1, 0, FALSE));
  c := New(PLStatic, InitResource(@self, IDC_LBL2, 0, FALSE));
  c := New(PLStatic, InitResource(@self, IDC_LBL3, 0, FALSE));
  c := New(PLStatic, InitResource(@self, IDC_LBL4, 0, FALSE));
  c := New(PLStatic, InitResource(@self, IDC_LBL5, 0, FALSE));
  c := New(PLStatic, InitResource(@self, IDC_LBL6, 0, FALSE));
end;

procedure TConfirmDlg.SetupWindow;
var
  titl : array [0..16] of char;
begin
  inherited SetupWindow;
  Str(info.tower, titl);
  SetDlgItemText(hWindow, IDC_TOWER, titl);
  Str(info.slot, titl);
  SetDlgItemText(hWindow, IDC_SLOT, titl);
  SetDlgItemText(hWindow, IDC_SPECID, s3);
  SetDlgItemText(hWindow, IDC_COLLDT, s4);
  SetDlgItemText(hWindow, IDC_ISOID, s5);
  SetDlgItemText(hWindow, IDC_PANELTYP, s6);
end;

procedure TConfirmDlg.Yes(var msg: TMessage);
begin
  EndDlg(IDYES);
end;

procedure TConfirmDlg.No(var msg: TMessage);
begin
  EndDlg(IDNO);
end;


{-------------------------------------------------------[ TPanelInfoDlg ]--}
constructor TPanelInfoDlg.Init(aParent: PWAMainDlg; aName: PChar);
var
  c   : PControl;
begin
  inherited Init(aParent, aName);
  dlgParent := aParent;
  c := New(PLStatic, InitResource(@self, IDC_LBL1, 0, FALSE));
  c := New(PLStatic, InitResource(@self, IDC_LBL2, 0, FALSE));
  c := New(PLStatic, InitResource(@self, IDC_LBL3, 0, FALSE));
  c := New(PLStatic, InitResource(@self, IDC_LBL4, 0, FALSE));
  c := New(PLStatic, InitResource(@self, IDC_LBL5, 0, FALSE));
  c := New(PLStatic, InitResource(@self, IDC_LBL6, 0, FALSE));
  c := New(PLStatic, InitResource(@self, IDC_LBL7, 0, FALSE));
  c := New(PLStatic, InitResource(@self, IDC_LBL8, 0, FALSE));
  c := New(PLStatic, InitResource(@self, IDC_LBL9, 0, FALSE));
  c := New(PLStatic, InitResource(@self, IDC_LBL10, 0, FALSE));
  c := New(PLStatic, InitResource(@self, IDC_LBL11, 0, FALSE));
end;

procedure TPanelInfoDlg.SetupWindow;
var
  pstr  : array [0..255] of char;
  sel   : integer;
begin
  inherited SetupWindow;
  sel := dlgParent^.lsmGrid^.GetSelIndex;
  if dlgParent^.lsmGrid^.GetData(LSM_SPECID,    sel, pstr, 255) then
    SetDlgItemText(HWindow, IDC_SPECID,    pstr);
  if dlgParent^.lsmGrid^.GetData(LSM_COLLDT,    sel, pstr, 255) then
    SetDlgItemText(HWindow, IDC_COLLDT,    pstr);
  if dlgParent^.lsmGrid^.GetData(LSM_ISOID,     sel, pstr, 255) then
    SetDlgItemText(HWindow, IDC_ISOID,     pstr);
  if dlgParent^.lsmGrid^.GetData(LSM_PATID,     sel, pstr, 255) then
    SetDlgItemText(HWindow, IDC_PATID,     pstr);
  if dlgParent^.lsmGrid^.GetData(LSM_PANELTYPE, sel, pstr, 255) then
    SetDlgItemText(HWindow, IDC_PANELTYP, pstr);
  if dlgParent^.lsmGrid^.GetData(LSM_PANELID,   sel, pstr, 255) then
    SetDlgItemText(HWindow, IDC_PANELID,   pstr);
  if dlgParent^.lsmGrid^.GetData(LSM_TOWER,     sel, pstr, 255) then
    SetDlgItemText(HWindow, IDC_TOWER,     pstr);
  if dlgParent^.lsmGrid^.GetData(LSM_SLOT,      sel, pstr, 255) then
    SetDlgItemText(HWindow, IDC_SLOT,      pstr);
  if dlgParent^.lsmGrid^.GetData(LSM_LASTREAD,  sel, pstr, 255) then
    SetDlgItemText(HWindow, IDC_LASTREAD,  pstr);
  if dlgParent^.lsmGrid^.GetData(LSM_NEXTREAD,  sel, pstr, 255) then
    SetDlgItemText(HWindow, IDC_NEXTREAD,  pstr);
  if dlgParent^.lsmGrid^.GetData(LSM_STATUS,    sel, pstr, 255) then
    SetDlgItemText(HWindow, IDC_STATUS,    pstr);
end;


END.
