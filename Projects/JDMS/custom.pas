unit Custom;
{----------------------------------------------------------------------------}
{  Module Name  : CUSTOM.PAS                                                 }
{  Programmer   : MJ                                                         }
{  Date Created : 04/94                                                      }
{                                                                            }
{  Purpose -                                                                 }
{                                                                            }
{  Assumptions -                                                             }
{  None.                                                                     }
{                                                                            }
{  Revision History -                                                        }
{                                                                            }
{  Version  Date      Who    Did What                                        }
{  -------  --------  ------ ----------------------------------------------- }
{  1.00     04/95     MJ     Initial release.                                }
{                                                                            }
{----------------------------------------------------------------------------}

INTERFACE

uses
  DMSDebug,
  OWindows;

procedure EditMnemonicFiles(aParent: PWindowsObject);
procedure DefineGSVals(aParent: PWindowsObject);
procedure DefineDrugs(aParent: PWindowsObject);
procedure DefineTests(aParent: PWindowsObject);
procedure DefineTestGroups(aParent: PWindowsObject);
procedure DefineOrders(aParent: PWindowsObject);
procedure DefineOrganisms(aParent: PWindowsObject);

IMPLEMENTATION

uses
  APITools,
  CTLLIB,
  DlgLib,
  DBIDs,
  GridLib,
  ListLib,
  DBErrors,
  DBTypes,
  DBLib,
  DBFile,
  DMSErr,
  StrsW,
  Strings,
  DMString,
  MScan,
  ODialogs,
  PrtMnem,
  WinProcs,
  WinTypes,
  WinDos;

{$I CUSTOM.INC}
{$R CUSTOM.RES}

type
  PMainDlg = ^TMainDlg;
  TMainDlg = object(TCenterDlg)
    constructor Init(aParent: PWindowsObject);
    destructor Done; virtual;
    private
    mnemList : PListObject;
    grid     : PGrid;
    {}
    procedure SetupWindow; virtual;
    procedure HandleList(var msg: TMessage); virtual ID_FIRST + IDC_FILELIST;
    procedure EditFile(var msg: TMessage); virtual ID_FIRST + BTN_EDITFILE;
    procedure PrintFile(var msg: TMessage); virtual ID_FIRST + BTN_PRINT;
    procedure Print(row: integer);
    procedure DrawItem(var msg: TMessage); virtual WM_FIRST + WM_DRAWITEM;
    procedure MeasureItem(var msg: TMessage); virtual WM_FIRST + WM_MEASUREITEM;
    procedure CharToItem(var msg: TMessage); virtual WM_FIRST + WM_CHARTOITEM;
  end;

  PPrintMnemDlg = ^TPrintMnemDlg;
  TPrintMnemDlg = object(TCenterDlg)
    constructor Init(aParent: PWindowsObject; var onlySelected: boolean);
    private
    selectedFileOnly: ^boolean;
    procedure SetupWindow; virtual;
    function CanClose: boolean; virtual;
  end;

{-------------------------------------------------------[ TMainDlg ]--}

constructor TMainDlg.Init(aParent: PWindowsObject);
begin
  mnemList := nil;
  grid     := nil;

  if not inherited Init(aParent, MakeIntResource(DLG_MAIN)) then
  begin
    Done;
    Fail;
  end;

  mnemList := New(PListObject, Init);
  if MnemList = nil then
  begin
    Done;
    Fail;
  end;

  grid := New(PGrid, InitResource(@Self, IDC_FILELIST, 2, false));
  if grid = nil then
  begin
    Done;
    Fail;
  end;

end;

destructor TMainDlg.Done;
begin
  inherited Done;
  MSDisposeObj(mnemList);
end;

procedure TMainDlg.SetupWindow;
var
  k     : integer;
  tstr  : string;
  dInfo : TSearchRec;
  desc  : PDBReadDesc;
  filei : TFileInfo;
  pStr  : array[0..64] of char;
  pc    : PChar;
begin
  inherited SetupWindow;
  grid^.SetHeader(0, SR(IDS_FILENAME, pStr, sizeof(pStr)-1));
  grid^.SetHeader(1, SR(IDS_DESCRIPTION, pStr, sizeof(pStr)-1));
  grid^.SetMaxCharWidth(0, 8);
  grid^.StretchColumn(1);
  grid^.SetRedraw(FALSE);
  StrLCopy(pstr, '*', sizeof(pStr)-1);
  StrLCat(pstr, fileDescExt, sizeof(pStr)-1);    {- fileDescExt is from DBTypes (*.DBI) }
  FindFirst(pstr, faArchive, dInfo);
  while DosError = 0 do
  begin
    pc:= StrPos(dInfo.Name, '.');
    if pc <> nil then
    begin
      pc^:= #0; {- remove extension }
      desc:= New(PDBReadDesc, Init(dInfo.Name, ''));
      if desc <> nil then
      begin
        desc^.FileInfo(filei);
        if (filei.userFlag and db_Mnemonic) <> 0 then
        begin
          k := grid^.AddString(dInfo.Name);
          grid^.SetData(1, k, filei.desc);
          grid^.SetData(0, k, filei.fName);
        end;
        MSDisposeObj(desc);
      end;
    end;
    FindNext(dInfo);
  end;
  grid^.SetRedraw(true);
  grid^.SetSelIndex(0);
end;

procedure TMainDlg.HandleList(var msg: TMessage);
begin
  if msg.lParamHI = LBN_DBLCLK then
    EditFile(msg)
  else
    DefWndProc(msg);
end;

procedure TMainDlg.EditFile(var msg: TMessage);
var
  k     : integer;
  pstr  : array[0..200] of char;
begin
  k := grid^.GetSelIndex;
  if k >= 0 then
  begin
    grid^.GetData(0, k, pstr, sizeof(pStr)-1);
    if not mnemList^.EditableMnemList(@self, pStr, '') then
      ShowError(@self, IDS_OPENFILEERROR, nil, mnemList^.errorNum, MOD_CUSTOM, 1);
  end
  else
    MessageBeep(0);
end;

procedure TMainDlg.Print(row: integer);
var
  pstr  : PChar;
  tstr  : array[0..20] of char;
  len   : integer;
  k     : integer;
begin
  {- get enough memory to potentialy hold file names for all rows (plus some fluff)}
  len:= (grid^.GetRowCount * 12) + 50;
  GetMem(pstr, len);

  if row >= 0 then
    grid^.GetData(0, row, pstr, len-1)
  else
  begin
    StrCopy(pstr, '');
    for k:= 0 to grid^.GetRowCount - 1 do
    begin
      grid^.GetData(0, k, tstr, sizeof(tstr)-1);
      StrLCat(pstr, tstr, len-1);
      if k < grid^.GetRowCount - 1 then
        StrLCat(pstr, #9, len-1); {- add separator }
    end;
  end;

  PrintMnemFile(@self, pstr);

  MSFreeMem(pstr, len);
end;

procedure TMainDlg.PrintFile(var msg: TMessage);
var
  d                : PDialog;
  selectedFileOnly : boolean;
  k                : integer;
begin
  if grid^.GetRowCount = 0 then
    exit;

  d:= New(PPrintMnemDlg, Init(@self, selectedFileOnly));
  if d <> nil then
  begin
    if application^.ExecDialog(d) = IDOK then
    begin
      if selectedFileOnly then  { print only the selected file }
        Print(grid^.GetSelIndex)
      else
        Print(-1);
    end;
  end;
end;

procedure TMainDlg.DrawItem(var msg: TMessage);
begin
  grid^.DrawItem(msg);
end;

procedure TMainDlg.MeasureItem(var msg: TMessage);
begin
  grid^.MeasureItem(msg);
end;

procedure TMainDlg.CharToItem(var msg: TMessage);
begin
  grid^.CharToItem(msg);
end;

constructor TPrintMnemDlg.Init(aParent: PWindowsObject; var onlySelected: boolean);
{- onlySelected - will be set to true to print only selected file, false to
                  print all files }
var
  c   : PControl;
begin
  if not inherited Init(aParent, MakeIntResource(DLG_PRINT)) then
  begin
    Done;
    Fail;
  end;
  selectedFileOnly:= @onlySelected;
  c:= New(PLRadioButton, InitResource(@self, IDC_ONEFILE, false));
  c:= New(PLRadioButton, InitResource(@self, IDC_ALLFILES, false));
end;

procedure TPrintMnemDlg.SetupWindow;
begin
  inherited SetupWindow;
  SendDlgItemMsg(IDC_ONEFILE, BM_SETCHECK, BF_CHECKED, 0);
  SendDlgItemMsg(IDC_ALLFILES, BM_SETCHECK, BF_UNCHECKED, 0);
end;

function TPrintMnemDlg.CanClose: boolean;
begin
  CanClose := false;
  if inherited CanClose then
  begin
    if SendDlgItemMsg(IDC_ONEFILE, BM_GETCHECK, 0, 0) <> 0 then
      selectedFileOnly^:= true
    else
      selectedFileOnly^:= false;
    CanClose:= true;
  end;
end;

{** Interface implementation **}

procedure EditMnemonicFiles(aParent: PWindowsObject);
begin
  Application^.ExecDialog(new(PMainDlg, Init(aParent)));
end;

procedure DefineGSVals(aParent: PWindowsObject);
{- call the gram stain mnemonic file editor }
var
  listObj   : PListObject;
begin
  listObj:= New(PListObject, Init);
  if listObj <> nil then
  begin
    if not listObj^.EditableMnemList(aParent, DBGSMnemFile, '') then
      ShowError(aParent, IDS_OPENFILEERROR, nil, listObj^.errorNum, MOD_CUSTOM, 3);
    MSDisposeObj(listObj);
  end;
end;

procedure DefineDrugs(aParent: PWindowsObject);
{- call the drug mnemonic file editor }
var
  listObj   : PListObject;
begin
  listObj:= New(PListObject, Init);
  if listObj <> nil then
  begin
    if not listObj^.EditableMnemList(aParent, DBDrugFile, '') then
      ShowError(aParent, IDS_OPENFILEERROR, nil, listObj^.errorNum, MOD_CUSTOM, 4);
    MSDisposeObj(listObj);
  end;
end;

procedure DefineTests(aParent: PWindowsObject);
{- call the Test mnemonic file editor }
var
  listObj   : PListObject;
begin
  listObj:= New(PListObject, Init);
  if listObj <> nil then
  begin
    if not listObj^.EditableMnemList(aParent, DBTSTFile, '') then
      ShowError(aParent, IDS_OPENFILEERROR, nil, listObj^.errorNum, MOD_CUSTOM, 5);
    MSDisposeObj(listObj);
  end;
end;

procedure DefineTestGroups(aParent: PWindowsObject);
{- call the Test Group mnemonic file editor }
var
  listObj   : PListObject;
begin
  listObj:= New(PListObject, Init);
  if listObj <> nil then
  begin
    if not listObj^.EditableMnemList(aParent, DBTSTGRPFile, '') then
      ShowError(aParent, IDS_OPENFILEERROR, nil, listObj^.errorNum, MOD_CUSTOM, 6);
    MSDisposeObj(listObj);
  end;
end;

procedure DefineOrders(aParent: PWindowsObject);
{- call the Orders mnemonic file editor }
var
  listObj   : PListObject;
begin
  listObj:= New(PListObject, Init);
  if listObj <> nil then
  begin
    if not listObj^.EditableMnemList(aParent, DBOrdFile, '') then
      ShowError(aParent, IDS_OPENFILEERROR, nil, listObj^.errorNum, MOD_CUSTOM, 7);
    MSDisposeObj(listObj);
  end;
end;

procedure DefineOrganisms(aParent: PWindowsObject);
{- call the Orders mnemonic file editor }
var
  listObj   : PListObject;
begin
  listObj:= New(PListObject, Init);
  if listObj <> nil then
  begin
    if not listObj^.EditableMnemList(aParent, DBOrgFile, '') then
      ShowError(aParent, IDS_OPENFILEERROR, nil, listObj^.errorNum, MOD_CUSTOM, 8);
    MSDisposeObj(listObj);
  end;
end;

END.
