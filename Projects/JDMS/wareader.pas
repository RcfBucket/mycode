{----------------------------------------------------------------------------}
{  Module Name  : WAREADER.PAS                                               }
{  Programmer   : EJ                                                         }
{  Date Created : 06/08/95                                                   }
{                                                                            }
{  Purpose -                                                                 }
{  This module provides the user with WA panel reader functions              }
{  available in the WA INTERFACE.                                            }
{                                                                            }
{  Assumptions -                                                             }
{  None.                                                                     }
{                                                                            }
{  Revision History -                                                        }
{                                                                            }
{  Version  Date      Who    Did What                                        }
{  -------  --------  ------ ----------------------------------------------- }
{  1.00     06/08/95  EJ     Initial release.                                }
{                                                                            }
{----------------------------------------------------------------------------}

unit WAReader;

INTERFACE

uses
  OWindows,
  DBFile,
  DBTypes;


procedure ResultsInquiry(aParent: PWindowsObject; aSessRef: TSeqNum;
                         exceptDB: PDBFile);


IMPLEMENTATION

uses
  Bits,
  ODialogs,
  WinProcs,
  WinTypes,
  Win31,
  MScan,
  UserMsgs,
  ESBL,
  Screens,
  Strings,
  StrsW,
  DMString,
  APITools,
  INILib,
  DlgLib,
  CtlLib,
  UIFlds,
  ListLib,
  DBIDs,
  DBLib,
  ResLib,
  UIIDMIC,
  Trays,
  IDLib,
  Biotype,
  PnlView,
  PanDump,
  WAMain,
  WAUtil,
  WAGPIB,
  DMSErr,
  DMSDebug,
  Barcodes;

{$I WAMAIN.INC}

const
  Extras     = [4,8,9];
  ExtraStr   : array [1..9] of pchar =
  ('', '', '', 'StS', '', '', '', 'GmS', 'TFG');

type
  PWAPanelViewDlg = ^TWAPanelViewDlg;
  TWAPanelViewDlg = object(TCenterDlg)
    curSessRef    : TSeqNum;
    micList       : PSpaceList;
    bioChemList   : PSpaceList;
    isoDB         : PDBFile;
    tstGrp        : PDBFile;
    specDB        : PDBFile;
    orgDB         : PDBFile;
    wapDB         : PDBFile;
    waexlistDB    : PDBFile;
    isordDB       : PDBAssocFile;
    resList       : PResults;
    exceptions    : PExceptions;
    wellFile      : Text;
    isolateInfo   : boolean;
    recordLocked  : boolean;
    isoRef        : TSeqNum;
    isoOrgRef     : TSeqNum;
    newOrgRef     : TSeqNum;
    tstGrpID      : array[0..32] of char;
    biotypeObj    : PWABiotypeObject;
    setFamily     : word;
    dataSaved     : boolean;
    panelRead     : boolean;
    panelInWA     : boolean;
    MICsLoaded    : boolean;
    IDReady       : boolean;
    PosFlagsReady : boolean;
    panelID       : integer;
    micData       : PMIArray;
    posFlags      : TPanelBitmap;
    virtualTests  : TPanelBitmap;
    class         : byte;
    panelStatus   : byte;
    bLacResult,
    oxidaseResult,
    indoleResult,
    orgNum        : integer;
    waSlot        : integer;
    ValidResults  : Boolean;
    {}
    constructor Init(aParent: PWindowsObject; aName: PChar; aSessRef: TSeqNum;
                     exceptDB: PDBFile);
    destructor Done; virtual;
    procedure SetupWindow; virtual;
    procedure InitSession;
    procedure FirstTestGroup;
    procedure NextTestGroup;
    procedure InitVars;
    function  InitIsoInfo: boolean;
    procedure ShowCurrentInfo;
    procedure ShowMICs;
    procedure ShowBiochems;
    procedure ShowBiotype;
    procedure ShowOrganism;
    procedure HandlemicList(var msg: TMessage); virtual ID_FIRST + IDC_MICLIST;
    procedure HandleBiochemList(var msg: TMessage); virtual ID_FIRST + IDC_BIOCHEMLIST;
    procedure AddExtras;
    procedure GetPosFlags;

    {- Button routines }
    procedure ReadPanel(var msg: TMessage); virtual ID_FIRST + BTN_READ;
    procedure Next(var msg: TMessage); virtual ID_FIRST + BTN_NEXT;
    procedure EditBiotype(var msg: TMessage); virtual ID_FIRST + BTN_BIOTYPE;
    procedure ViewResults(var msg: TMessage); virtual ID_FIRST + BTN_VIEW;
    procedure SaveTests(var msg: TMessage); virtual ID_FIRST + BTN_SAVE;
    procedure PrintPanelDump(var msg: TMessage); virtual ID_FIRST + BTN_PRINT;
    procedure Search(var msg: TMessage); virtual ID_FIRST + BTN_SEARCH;
    procedure Resolve(var msg: TMessage); virtual ID_FIRST + BTN_RESOLVE;
    procedure AbortPanel(var msg: TMessage); virtual ID_FIRST + BTN_ABORT_PNL;
    procedure Cancel(var msg: TMessage); virtual ID_FIRST + IDCANCEL;
    procedure EnableButtons; virtual;
    function  UploadWAData: boolean;
    function  GetPanelID: boolean;
    function  GetPanelUsedFlag: boolean;
    function  PanelStatusInquiry(var slot, panelType : byte;
                                 var flgs: integer;
                                 var lastRead, nextRead: TimeDate): integer;
    procedure PreScreenResults(Organism: Integer);
    function  GetPanelData(flgs: integer; LastRead: TimeDate): integer;
    function  ProblemsOccurred(flagsObj: PPanelFlags): boolean;
    function  EvaluateStatus(flagsObj: PPanelFlags): boolean;
    function  EvaluateFlags(flagsObj: PPanelFlags): boolean;
    function  PanelMICsInquiry: integer;
    function  VirtualTestsInquiry(flagsObj: PPanelFlags): integer;
    function  PanelResultsInquiry(prompt: boolean): integer;
    procedure CheckExtraTests;
    procedure EditCurrentBiochem; virtual;
    procedure EditCurrentMIC; virtual;
    function  SaveTheTests: boolean;
    procedure ClearPanelID;
    procedure ShowExceptions;
    procedure SetWAError(errNum: integer);
    procedure SetException(exceptionNum: integer);
    procedure SetWAFlag(flgNum: integer);
    procedure ClearStatus;
    procedure ClearWAExList;
    function  LockIt: boolean;
    function  AskUserToSaveTests: boolean;
    procedure ReDrawExceptions;
  end;

  PResolveDlg = ^TResolveDlg;
  TResolveDlg = object(TCenterDlg)
    orgRef          : ^TSeqNum;
    bLacResult,
    oxidaseResult,
    indoleResult    : ^integer;
    orgRequired,
    bLacRequired,
    oxidaseRequired,
    indoleRequired  : boolean;
    listObj         : PListObject;
    tempRec         : PDBRec;
    fld             : PUIFld;
    {}
    constructor Init(aParent: PWindowsObject; dbIso: PDBFile;
                     exceptions: PExceptions;
                     bLacRes, oxidaseRes, indoleRes, orgRes: pointer);
    destructor Done; virtual;
    procedure SetUpWindow; virtual;
    function  CanClose: boolean; virtual;
  end;



constructor TWAPanelViewDlg.Init(aParent: PWindowsObject; aName: PChar;
                                 aSessRef: TSeqNum; exceptDB: PDBFile);
var
  c    : PControl;
begin
  inherited Init(aParent, aName);
  curSessRef := aSessRef;
  waexlistDB := exceptDB;
  micList := New(PSpaceList, InitResource(@self, IDC_MICLIST));
  bioChemList := New(PSpaceList, InitResource(@self, IDC_BIOCHEMLIST));
  isoDB := New(PDBFile, Init(DBISOFile, '', DBOpenNormal));
  specDB := New(PDBFile, Init(DBSPECFile, '', DBOpenNormal));
  tstGrp := New(PDBFile, Init(DBTstGrpFile, '', DBOpenNormal));
  orgDB := New(PDBFile, Init(DBOrgFile, '', DBOpenNormal));
  wapDB := New(PDBFile, Init(DBWAPNLFile, '', DBOpenNormal));
  isordDB := nil;
  resList    := New(PResults, Init);
  exceptions := New(PExceptions, Init);
  isoRef := 0;
  biotypeObj := nil;
  setFamily := 0;
  recordLocked := FALSE;

  {$IFOPT I+}
    {$DEFINE chki}
  {$ENDIF}
  {$I-}
  if IOResult <> 0 then ; { clear it }
  Assign(wellFile, 'DataList.TXT');
  Reset(wellFile);
  if (IOResult <> 0) or (resList = nil)  or
     (micList = nil) or (bioChemList = nil) or
     (isoDB = nil) or (specDB = nil) or (tstGrp = nil) or (orgDB = nil) then
  begin
    ShowError(aParent, IDS_WA_ERR, nil, MOD_WAREADER+1, MOD_WAREADER, 0);
    Done;
    Fail;
  end;
  {$IFDEF chki}
    {$I+}
    {$UNDEF chki}
  {$ENDIF}
  wapDB^.dbc^.SetCurKeyNum(DBWAPNL_IsoOrd_KEY);
  c := New(PLStatic, InitResource(@self, IDC_LBL1, 0, FALSE));
  c := New(PLStatic, InitResource(@self, IDC_LBL2, 0, FALSE));
  c := New(PLStatic, InitResource(@self, IDC_LBL3, 0, FALSE));
  c := New(PLStatic, InitResource(@self, IDC_LBL4, 0, FALSE));
  c := New(PLStatic, InitResource(@self, IDC_LBL5, 0, FALSE));
  c := New(PLStatic, InitResource(@self, IDC_LBL6, 0, FALSE));
  c := New(PLStatic, InitResource(@self, IDC_FootnoteText, 0, FALSE));
  c := New(PLStatic, InitResource(@self, IDC_SpclCharText, 0, FALSE));
end;

destructor TWAPanelViewDlg.Done;
begin
  {$IFOPT I+}
    {$DEFINE chki}
  {$ENDIF}
  {$I-}
  Close(wellFile);
  if IOResult <> 0 then ; { clear it }
  {$IFDEF chki}
    {$I+}
    {$UNDEF chki}
  {$ENDIF}

  if recordLocked then
  begin
    isoDB^.dbc^.UnlockRec(isoDB^.dbr);
    recordLocked := FALSE;
  end;

  MSDisposeObj(biotypeObj);
  MSDisposeObj(exceptions);
  MSDisposeObj(resList);
  MSDisposeObj(isordDB);
  MSDisposeObj(wapDB);
  MSDisposeObj(orgDB);
  MSDisposeObj(tstGrp);
  MSDisposeObj(specDB);
  MSDisposeObj(isoDB);
  inherited Done;
end;

procedure TWAPanelViewDlg.SetupWindow;
var
  r    : TRect;
  font : HFont;
begin
  inherited SetupWindow;
  {- divide biochem list box into 3 columns }
  GetClientRect(biochemList^.hWindow, r);
  SendDlgItemMsg(IDC_BIOCHEMLIST, LB_SETCOLUMNWIDTH, word((r.right-r.left) div 3), 0);

  {- divide MIC list box into 3 columns }
  GetClientRect(micList^.hWindow, r);
  SendDlgItemMsg(IDC_MICLIST, LB_SETCOLUMNWIDTH, word((r.right-r.left) div 3), 0);

  font:= GetStockObject(SYSTEM_FIXED_FONT);

  SendDlgItemMsg(IDC_BIOCHEMLIST, WM_SETFONT, font, 0);
  SendDlgItemMsg(IDC_MICLIST, WM_SETFONT, font, 0);

  InitSession;
  EnableButtons;
end;

procedure TWAPanelViewDlg.InitSession;
var
  pstr : array[0..40] of char;
begin
  if INISpecimenListSort = 2 then
    if curSessRef = 0 then
      isoDB^.dbc^.SetCurKeyNum(DBISO_CDateSpecID_KEY)
    else
      isoDB^.dbc^.SetCurKeyNum(DBISO_SessionCDateSpecID_KEY)
  else
    if curSessRef = 0 then
      isoDB^.dbc^.SetCurKeyNum(DBISO_SpecIDCDate_KEY)
    else
      isoDB^.dbc^.SetCurKeyNum(DBISO_SessionSpecIDCDate_KEY);

  InitVars;
  FirstTestGroup;
  if not isolateInfo then
    InfoMsg(hWindow, '', SR(IDS_NO_DATA_IN_SESS, pstr, 40));

  ShowCurrentInfo;
end;

procedure TWAPanelViewDlg.FirstTestGroup;
var
  tstGrpRef : TSeqNum;
  isOk      : boolean;
  udFlag    : boolean;
begin
  if recordLocked then
  begin
    isoDB^.dbc^.UnlockRec(isoDB^.dbr);
    recordLocked := FALSE;
  end;

  isolateInfo := FALSE;
  isoDB^.dbr^.ClearRecord;
  if curSessRef = 0 then
    isOK := isoDB^.dbc^.GetFirst(isoDB^.dbr)
  else
  begin
    isoDB^.dbr^.PutField(DBISOSess, @curSessRef);
    isOK := isoDB^.dbc^.GetFirstContains(isoDB^.dbr);
  end;
  if isOK then
  begin
    repeat
      MSDisposeObj(isordDB);
      isordDB := New(PDBAssocFile, Init(isoDB^.dbr, DBISOLATE_ISORDER_ASSOC, dbOpenNormal));
      if isordDB^.dbc^.GetFirst(isordDB^.dbr) then
      begin
        repeat
          isordDB^.dbr^.GetField(DBISOORDTstGrpRef, @tstGrpRef, sizeof(tstGrpRef));
          if not tstGrp^.dbc^.GetSeq(tstGrp^.dbr, tstGrpRef) then
          begin
            ShowError(@self, IDS_DATABASE_ERR, nil, tstGrp^.dbc^.dbErrorNum, MOD_WAREADER, 1);
            Continue;
          end;
          tstGrp^.dbr^.GetField(DBTSTGRPUDFlag, @udFlag, SizeOf(boolean));
          if not udFlag then
          begin
            tstGrp^.dbr^.GetFieldAsStr(DBTSTGrpID, tstGrpID, 32);
            resList^.traysObj^.LoadTray(tstGrpID);
            InitVars;
            isolateInfo := InitIsoInfo;
            recordLocked := LockIt;
          end;
        until isolateInfo or not isordDB^.dbc^.GetNext(isordDB^.dbr);
      end;
      if not isolateInfo then
        if curSessRef = 0 then
          isOK := isoDB^.dbc^.GetNext(isoDB^.dbr)
        else
          isOK := isoDB^.dbc^.GetNextContains(isoDB^.dbr);
    until (not isOK) or isolateInfo;
  end;
end;

procedure TWAPanelViewDlg.NextTestGroup;
var
  tstGrpRef : TSeqNum;
  isOk      : boolean;
  udFlag    : boolean;
begin
  isoRef := 0;
  isolateInfo := FALSE;
  if recordLocked then
  begin
    IsoDB^.dbc^.UnlockRec(IsoDB^.dbr);
    recordLocked := FALSE;
  end;

  isOK := isordDB^.dbc^.GetNext(isordDB^.dbr);
  repeat
    while not isolateInfo and isOK do
    begin
      isordDB^.dbr^.GetField(DBISOORDTstGrpRef, @tstGrpRef, sizeof(tstGrpRef));
      if not tstGrp^.dbc^.GetSeq(tstGrp^.dbr, tstGrpRef) then
      begin
        ShowError(@self, IDS_DATABASE_ERR, nil, tstGrp^.dbc^.dbErrorNum, MOD_WAREADER, 2);
        Continue;
      end;
      tstGrp^.dbr^.GetField(DBTSTGRPUDFlag, @udFlag, SizeOf(boolean));
      if not udFlag then
      begin
        tstGrp^.dbr^.GetFieldAsStr(DBTSTGrpID, tstGrpID, 32);
        resList^.traysObj^.LoadTray(tstGrpID);
        InitVars;
        isolateInfo := InitIsoInfo;
        recordLocked := LockIt;
      end;
      if not isolateInfo then
        isOK := isordDB^.dbc^.GetNext(isordDB^.dbr);
    end;
    if not isolateInfo then
    begin
      repeat
        if curSessRef = 0 then
          isOK := isoDB^.dbc^.GetNext(isoDB^.dbr)
        else
          isOK := isoDB^.dbc^.GetNextContains(isoDB^.dbr);
        if not isOK then
          Break;

        MSDisposeObj(isordDB);
        isordDB := New(PDBAssocFile, Init(isoDB^.dbr, DBISOLATE_ISORDER_ASSOC, dbOpenNormal));
        isOK := isordDB^.dbc^.GetFirst(isordDB^.dbr);
      until isOK;
    end;
  until (not isOK) or isolateInfo;

  if not isolateInfo then
    FirstTestGroup;
end;

procedure TWAPanelViewDlg.InitVars;
begin
  orgNum := -1;
  panelRead := FALSE;
  panelInWA := FALSE;
  IDReady := FALSE;
  MICsLoaded := FALSE;
  dataSaved := FALSE;
  ValidResults := False;
  panelID := 0;
  SetDlgItemText(hWindow, IDC_BIOTYPE, '');
  newOrgRef := 0;
  SetDlgItemText(hWindow, IDC_ORGANISM, '');
  SetDlgItemText(hWindow, IDC_Footnotes, '');
  SetDlgItemText(hWindow, IDC_SpclChars, '');
  biochemList^.ClearList;
  micList^.ClearList;
  indoleResult := -1;
  oxidaseResult := -1;
  bLacResult := -1;
  ClearStatus;
end;

function TWAPanelViewDlg.InitIsoInfo : boolean;
var
  seqRef   : TSeqNum;
  orgPChar : array[0..15] of char;
  code     : integer;
begin
  { the isolate record and the TstGrp record have already been read in.}
  InitIsoInfo := FALSE;
  isoOrgRef := 0;
  {Read in specimen}
  if isoDB^.dbr^.GetField(DBISOSpecRef, @seqRef, sizeof(seqRef)) then
    if specDB^.dbc^.GetSeq(specDB^.dbr, seqRef) then
    begin
      isoRef := isoDB^.dbr^.GetSeqValue;
      isoDB^.dbr^.GetField(DBISOOrgRef, @isoOrgRef, sizeof(isoOrgRef));
      if (isoOrgRef <> 0) then
        if orgDB^.dbc^.GetSeq(orgDB^.dbr, isoOrgRef) then
        begin
          orgDB^.dbr^.GetFieldAsStr(DBOrgOrg, orgPChar, 15);
          Val(orgPChar, orgNum, code);
        end;
      MSDisposeObj(biotypeObj);
      biotypeObj := New(PWABiotypeObject, Init(@resList^.traysObj^.tfsRec));
      if biotypeObj <> nil then
        if isoDB^.dbr^.GetField(DBISOSetFamily, @setfamily, sizeof(setFamily)) then
          InitIsoInfo := TRUE;
    end;
end;

procedure TWAPanelViewDlg.ShowCurrentInfo;
var
  pstr  : PChar;
  pstr2 : PChar;
begin
  if not isolateInfo then
    Exit;

  GetMem(pstr, 101);
  GetMem(pstr2, 31);

  with specDB^.dbr^ do
  begin
    GetFieldAsStr(DBSPECSpecID, pstr, 30);
    StrCat(pstr, '   ');
    GetFieldAsStr(DBSPECCollectDate, pstr2, 30);
    StrCat(pstr, pstr2);
    SetDlgItemText(hWindow, IDC_SPECNUM, pstr);
  end;
  with isoDB^.dbr^ do
  begin
    GetFieldAsStr(DBISOIso, pstr, 10);
    SetDlgItemText(hWindow, IDC_ISONUM, pstr);
    SetDlgItemText(hWindow, IDC_PANELTYPE, tstGrpID);
    if isoOrgRef <> 0 then
    begin
      orgDB^.dbc^.GetSeq(orgDB^.dbr, isoOrgRef);
      orgDB^.dbr^.GetFieldAsStr(DBORGSName, pstr, 100);
      SetDlgItemText(hWindow, IDC_STOREDORG, pstr);
    end
    else
      SetDlgItemText(hWindow, IDC_STOREDORG, '');

    if isoDB^.dbr^.FieldIsEmpty(DBISOTstDate) then
      StrCopy(pstr, '')
    else
      SR(IDS_DATASTORED, pStr, 100);
    SetDlgItemText(hWindow, IDC_HIDDENSTATUS, pstr);
  end;
  MSFreeMem(pstr, 101);
  MSFreeMem(pstr2, 31);
end;

procedure TWAPanelViewDlg.ShowMICs;
var
  i       : integer;
  dilstep : integer;
  pstr    : array[0..40] of char;
begin
  micList^.ClearList;
  for i:= 1 to resList^.traysObj^.DrugCnt do
    with resList^.traysObj^.dilList[i] do
    begin
      StrLCopy(pstr, drugname, 40);
      dilstep := micData[i];
      if (dilstep >= 0) and (dilstep <= dilCnt) then
      begin
        Pad(pstr, pstr, ' ', 4);
        StrLCat(pstr, drugDils[dilstep], 40);
      end;
      micList^.AddString(pstr);
    end;
end;

procedure TWAPanelViewDlg.GetPosFlags;
var XTraTbl   : XtraType;
    NumXtras,i: integer;
    Bits      : PRIArray;
    posXtras  : TPanelBitmap;
    WACmd     : PWAPRICmd;
    WellNum   : Integer;
begin
  if not resList^.traysObj^.TrayFlo then
  begin
    PosFlagsReady:=True;
    ResList^.traysObj^.GetXtra(XTraTbl, NumXtras);
    if NumXtras>0 then
    begin
      WACmd := New(PWAPRICmd, Init(WAID));
      WACmd^.SetParams(panelID);
      if WACmd^.SendWAMessage and (WACmd^.WAStatNum=0) then
      begin
        {Set relevant xtra bits in posFlags}
        Move(WACmd^.recvMsg^.priBits, posXtras, 12);
        for i:=1 to NumXtras do
        if (XtraTbl[i] in Extras) and
           resList^.traysObj^.GetXtraWellNumber(wellFile, ExtraStr[XtraTbl[i]], WellNum) and
           TestBit(posXtras[wellNum mod 12], (wellNum div 12)) then
           SetBit (posFlags[wellNum mod 12], (wellNum div 12));
      end;
      MSDisposeObj(WACmd);
    end;
  end;
end;

{- Display TFG, StS, GmS, BL if present on the panel}
procedure TWAPanelViewDlg.AddExtras;
var
    i, res    : integer;
    TheOrg    : integer;

    XTraTbl   : XtraType;
    NumXtras  : integer;

    XtraID    : array[0..15] of char;
    XtraRes   : array[0.. 4] of char;
    OrgPChar  : array[0..15] of char;
    ProbTxt   : array[0..10] of char;

begin
  {Get org and extra tests}
  TheOrg:=-1;
  if NewOrgRef<>0 then
  begin
    orgDB^.dbc^.GetSeq(orgDB^.dbr, newOrgRef);
    orgDB^.dbr^.GetFieldAsStr(DBOrgOrg, orgPChar, 15);
    Val(orgPChar,TheOrg,i);
  end
  else biotypeObj^.GetTopID(TheOrg, probTxt, nil, nil);

  {Get extra tests}
  if (PosFlagsReady) and (TheOrg <> -1) and (not resList^.traysObj^.TrayFlo) then
  begin
    ResList^.traysObj^.GetXtra(XTraTbl, NumXtras);

    for i:=1 to NumXtras do
    begin
      {Do we care for this ExtraTest?}
      if XtraTbl[i] in Extras then
      begin
        res:=GetETResult(i, XtraID, XtraTbl, resList, TheOrg,
                         wellFile, @posFlags, PMICData(@micData));
        Case res of
          1: StrCopy(XtraRes,'-');
          2: StrCopy(XtraRes,'+');
          3: StrCopy(XtraRes,'*');
        End;

        {Build the display string}
        if res<>0 then
        begin
          Pad(XtraID, XtraID, ' ', 6);
          StrCat(XtraID,XtraRes);
          BiochemList^.Addstring(@XtraID[1]);
        end;
      end;
    end;
  end;
end;

procedure TWAPanelViewDlg.ShowBiochems;
var
  i, k,
  val,
  group   : integer;
  pstr    : array[0..40] of char;
  MICOnly : Boolean;
begin
  biochemList^.ClearList;
  MICOnly:=ResList^.traysObj^.tfsRec.idRec = -1;
  if (not MICOnly) and IDReady then
  for i:= biotypeObj^.MaxBitNum downto 0 do
  begin
    StrCopy(pstr, '***');
    group := 0;  {assume bit is a biochem, not an MIC value}
    if biotypeObj^.BitInfo(i, val, k, group, pstr) then
    begin
      Pad(pstr, pstr, ' ', 6);
      case val of
        -1 :
          begin
            if biotypeObj^.tfsRec^.pSet = 1 then
              resList^.GetResult('.OXI', 'OXI', @pstr[6], 20)
            else if biotypeObj^.tfsRec^.pSet = 3 then
              resList^.GetResult('.IND', 'IND', @pstr[6], 20)
            else
              StrCat(pstr, ' ');
          end;
        0 : StrCat(pstr, '-');
        1 : StrCat(pstr, '+');
        else
          StrLCat(pstr, '?', 255);
      end;
    end;
    biochemList^.AddString(pstr);
  end;
  AddExtras;
end;

procedure TWAPanelViewDlg.ShowBiotype;
var
  pstr   : array[0..30] of char;
begin
  StrPCopy(pstr, biotypeObj^.biotype);
  SetDlgItemText(hWindow, IDC_BIOTYPE, pstr);
end;

procedure TWAPanelViewDlg.ShowOrganism;
var
  pstr    : array [0..100] of char;
  probTxt : array [0..10] of char;
  Footnotes    : String;
  SpecialChars : String;
begin
  Footnotes := '';
  SpecialChars := '';
  if biotypeObj^.GetTopID(orgNum, probTxt, Addr(Footnotes), Addr(SpecialChars)) then
  begin
    if orgNum = 0 then
    begin
      SR(IDS_VRBIOT, pstr, 100);
      newOrgRef:=0;
    end
    else
    begin
      Str(orgNum, pstr);
      orgDB^.dbr^.ClearRecord;
      if orgDB^.dbr^.PutFieldAsStr(DBORGOrg, pstr) then
        if orgDB^.dbc^.GetEQ(orgDB^.dbr) then
        begin
          newOrgRef := orgDB^.dbr^.GetSeqValue;
          orgDB^.dbr^.GetFieldAsStr(DBORGSName, pstr, 100);
          StrLCat(pstr, '  ', 100);
          StrLCat(pstr, probTxt, 100);
        end;
    end;
  end
  else
    SR(IDS_INVBIOT, pstr, 100);
  SetDlgItemText(hWindow, IDC_ORGANISM, pstr);
  Footnotes := concat(Footnotes,chr(0));
  StrCopy(pstr,Addr(Footnotes[1]));
  SetDlgItemText(hWindow, IDC_Footnotes, pstr);
  SpecialChars := concat(SpecialChars,chr(0));
  StrCopy(pstr,Addr(SpecialChars[1]));
  SetDlgItemText(hWindow, IDC_SpclChars, pstr);
end;

procedure TWAPanelViewDlg.HandleMICList(var msg: Tmessage);
begin
  if Msg.LParamHi = lbn_DblClk then
    EditCurrentMIC;
end;

procedure TWAPanelViewDlg.HandleBiochemList(var msg: Tmessage);
begin
  if Msg.LParamHi = lbn_DblClk then
    EditCurrentBiochem;
end;

procedure TWAPanelViewDlg.ReadPanel(var msg: TMessage);
var
  wc       : PWaitCursor;
begin
  if not AskUserToSaveTests then
    Exit;

  wc := New(PWaitCursor, Init);
  ClearStatus;
  InitVars;
  InitIsoInfo;

  MSDisposeObj(biotypeObj);   {- remove any previous biotype objects }
  biotypeObj := New(PWABiotypeObject, Init(@resList^.traysObj^.tfsRec));

  resList^.LoadResults(isoDB^.dbr);

  if UploadWaData then
  begin
    panelRead := TRUE;
    if MICsLoaded then
      ShowMICs;

    if IDReady then
    begin
      ShowBiotype;
      ShowOrganism;
    end;
    ShowBiochems;
  end;
  MSDisposeObj(wc);
  EnableButtons;
end;

procedure TWAPanelViewDlg.Next(var msg: TMessage);
var
  wc       : PWaitCursor;
begin
  if not AskUserToSaveTests then
    Exit;

  wc := New(PWaitCursor, Init);
  NextTestGroup;
  ShowCurrentInfo;
  MSDisposeObj(wc);
  EnableButtons;
end;

procedure TWAPanelViewDlg.EditBiotype(var msg: TMessage);
var
  pstr         : array [0..127] of char;
  probTxt      : array [0..10] of char;
  aSet         : byte;
  OrgList      : TOrgList;        {List of found organisms}
  SelectedOrg  : integer;         {Organism selected in the biotype editor window}
  Footnotes    : string;          {Footnotes for selected organism}
  SpecialChars : string;          {Special chars for selected organism}
  j            : integer;
begin
  if (biotypeObj = nil) or (not panelRead) or
     (resList^.traysObj^.tfsRec.idRec = -1) then
    Exit;
  if (biotypeObj^.biotype[0] = Chr(0)) then
    Exit;

  if BiotypeEditor(@self, biotypeObj, resList, orgDB, probTxt, @posFlags, addr(SelectedOrg)) then
  begin
    dataSaved := FALSE;
    aSet := ExtractSet(setFamily);
    class := biotypeObj^.class;
    setFamily := MakeSetFamily(aSet, biotypeObj^.class);
    isoDB^.dbr^.PutField(DBISOSetFamily, @setFamily);
    if biotypeObj^.identify^.IDList.NumOrgs > 0 then
    begin
      {Get org footnotes}
      For j := 1 To BiotypeObj^.Identify^.IDList.NumOrgs Do
        OrgList[j] := BiotypeObj^.Identify^.IDList.ReportOrgs.P^[j];
      OrgObject.ReportFootNotes(BiotypeObj^.Identify^.IDList.NumOrgs,SelectedOrg,OrgList,BiotypeObj^.BioType,Footnotes);
      Footnotes := concat(Footnotes,chr(0));
      StrCopy(pstr,Addr(Footnotes[1]));
      SetDlgItemText(hWindow, IDC_Footnotes, pstr);

      {Get org special characteristics}
      OrgObject.ReportSpclChars(BiotypeObj^.Identify^.IDList.ReportOrgs.P^[SelectedOrg],30,SpecialChars);
      SpecialChars := concat(SpecialChars,chr(0));
      StrCopy(pstr,Addr(SpecialChars[1]));
      SetDlgItemText(hWindow, IDC_SpclChars, pstr);

      {Get org name and probability}
      orgDB^.dbr^.GetFieldAsStr(DBORGSName, pStr, sizeof(pStr)-1);
      newOrgRef := orgDB^.dbr^.GetSeqValue;
      StrLCat(pstr, '  ', sizeof(pStr)-1);
      StrLCat(pstr, probTxt, sizeof(pStr)-1);
    end
    else
    begin
      newOrgRef := 0;
      SR(IDS_VRBIOT, pStr, 127);
      SetDlgItemText(hWindow, IDC_Footnotes, '');
      SetDlgItemText(hWindow, IDC_SpclChars, '');
    end;
    SetDlgItemText(hWindow, IDC_ORGANISM, pstr);
    ShowBiotype;
    ShowBiochems;
  end;
  EnableButtons;
end;

procedure TWAPanelViewDlg.ViewResults(var msg: TMessage);
begin
  if isoRef <> 0 then
  begin
    UIIDMICEntry(@self, isoRef, TRUE);
    EnableButtons;
  end;
end;

procedure TWAPanelViewDlg.SaveTests(var msg: TMessage);
var
  wc       : PWaitCursor;
begin
  wc := New(PWaitCursor, Init);
  if SaveTheTests then
    ShowCurrentInfo;
  MSDisposeObj(wc);
  EnableButtons;
end;

procedure TWAPanelViewDlg.PrintPanelDump(var msg: TMessage);
var
  dump : PWAPanelDump;
begin
  if not panelRead then
    Exit;

  dump := New(PWAPanelDump,
              Init(@self, isoDB, specDB, resList, biotypeObj, panelID));
  if dump <> nil then
  begin
    dump^.PrintPanelDump;
    MSDisposeObj(dump);
  end;
  EnableButtons;
end;

procedure TWAPanelViewDlg.Search(var msg: TMessage);
var
  p          : PSearchDlg;
  tempSpec,
  tempIso,
  tempTstGrp : longint;
  tempSeq    : TSeqNum;
  wc         : PWaitCursor;
begin
  if not AskUserToSaveTests then
    Exit;

  if recordLocked then
  begin
    isoDB^.dbc^.UnlockRec(isoDB^.dbr);
    recordLocked := FALSE;
  end;

  tempSpec := specDB^.dbc^.GetPosition;
  tempIso := isoDB^.dbc^.GetPosition;
  tempTstGrp := tstGrp^.dbc^.GetPosition;
  tempSeq := isordDB^.dbr^.GetSeqValue;

  p := New(PSearchDlg, Init(@self, specDB, isoDB, tstGrp, isordDB));
  if Application^.ExecDialog(p) = IDOK then
  begin
    wc := New(PWaitCursor, Init);
    tstGrp^.dbr^.GetFieldAsStr(DBTSTGrpID, tstGrpID, 32);
    resList^.traysObj^.LoadTray(tstGrpID);
    InitVars;
    isolateInfo := InitIsoInfo;
    recordLocked := LockIt;
    ShowCurrentInfo;
    MSDisposeObj(wc);
    EnableButtons;
  end
  else
  begin
    specDB^.dbc^.GoToPosition(specDB^.dbr, tempSpec);
    isoDB^.dbc^.GoToPosition(isoDB^.dbr, tempIso);
    tstGrp^.dbc^.GoToPosition(tstGrp^.dbr, tempTstGrp);
    recordLocked := LockIt;
    MSDisposeObj(isordDB);
    isordDB := New(PDBAssocFile, Init(isoDB^.dbr, DBISOLATE_ISORDER_ASSOC, dbOpenNormal));
    isordDB^.dbc^.GetSeq(isordDB^.dbr, tempSeq);
  end;
end;

procedure TWAPanelViewDlg.Resolve(var msg: TMessage);
var
  d      : PResolveDlg;
  nI,
  nO,
  nB     : boolean;
  pstr   : PChar;
  wc     : PWaitCursor;

begin
  if not panelRead then
    Exit;

  d := New(PResolveDlg,Init(@self, isoDB, exceptions,
           @bLacResult, @oxidaseResult, @indoleResult, @newOrgRef));

  if Application^.ExecDialog(d) = IDOK then
  begin
    wc := New(PWaitCursor, Init);

    if exceptions^.NeedOrg or (newOrgRef <> 0) then
    begin
      dataSaved := FALSE;
      GetMem(pstr, 101);
      orgDB^.dbc^.GetSeq(orgDB^.dbr, newOrgRef);
      orgDB^.dbr^.GetFieldAsStr(DBORGSName, pstr, 100);
      SetDlgItemText(hWindow, IDC_ORGANISM, pstr);
      exceptions^.NeedOrg := FALSE;
      SetDlgItemText(hWindow, IDC_Footnotes, '');
      SetDlgItemText(hWindow, IDC_SpclChars, '');
      MSFreeMem(pstr, 101);
      ShowBiochems;
    end;

    if exceptions^.NeedIndole or
       exceptions^.NeedOxid or
       exceptions^.NeedBLac  then
    begin
      resList^.LoadResults(isoDB^.dbr);
      if exceptions^.NeedOxid then
        case oxidaseResult of
          0 : resList^.SetResult('.OXI', 'OXI', '-', TRUE);
          1 : resList^.SetResult('.OXI', 'OXI', '+', TRUE);
        end;

      if exceptions^.NeedBLac then
        case BlacResult of
          0 : resList^.SetResult('.BL', 'BL', '-', TRUE);
          1 : resList^.SetResult('.BL', 'BL', '+', TRUE);
        end;

      if exceptions^.NeedIndole then
        case indoleResult of
          0 : resList^.SetResult('.IND', 'IND', '-', TRUE);
          1 : resList^.SetResult('.IND', 'IND', '+', TRUE);
        end;

      dataSaved := FALSE;
      ClearStatus;
      UploadWAData;
      if MICsLoaded then
        ShowMICs;
      if IDReady then
      begin
        ShowBiotype;
        ShowOrganism;
      end;
      ShowBiochems;
    end;

    MSDisposeObj(wc);
  end;
  EnableButtons;
end;

procedure TWAPanelViewDlg.AbortPanel(var msg: TMessage);
var
  WACmd     : PWAKILLCmd;
  title     : array [0..20] of char;
  confMsg   : array [0..40] of char;
  wc        : PWaitCursor;
begin
  if panelID = 0 then
    Exit;

  if not YesNoMsg(hWindow, SR(IDS_CONFIRM, title, 20),
                           SR(IDS_CONFIRM_ABORT, confMsg, 40)) then
    Exit;

  wc := New(PWaitCursor, Init);
  WACmd := New(PWAKILLCmd, Init(WAID));
  WACmd^.SetParams(panelID);
  if WACmd^.SendWAMessage then
  begin
    ClearWAExList;
    ClearPanelID;
    SetWAError(IDS_USER_ABORTED);
  end
  else
    SetWAError(IDS_ABORT_FAILED);
  MSDisposeObj(WACmd);
  MSDisposeObj(wc);
  EnableButtons;
end;

procedure TWAPanelViewDlg.Cancel(var msg: TMessage);
begin
  if not AskUserToSaveTests then
    Exit;
  inherited Cancel(msg);
end;

procedure TWAPanelViewDlg.EnableButtons;
begin
  EnableWindow(GetItemHandle(BTN_READ), isolateInfo and recordLocked);
  EnableWindow(GetItemHandle(BTN_NEXT), isolateInfo);
  if not isolateInfo or not recordLocked or not IDReady or
     (resList^.traysObj^.tfsRec.idRec = -1) then {MIC Only Panel}
  begin
    EnableWindow(GetItemHandle(BTN_BIOTYPE), FALSE);
    EnableWindow(GetItemHandle(IDC_BIOCHEMLIST), (BiochemList<>Nil) and (BiochemList^.GetCount>0));
  end
  else
  begin
    EnableWindow(GetItemHandle(BTN_BIOTYPE), panelRead);
    EnableWindow(GetItemHandle(IDC_BIOCHEMLIST), TRUE);
  end;
  with exceptions^ do
    EnableWindow(GetItemHandle(IDC_MICLIST),
                 panelRead and (micList^.GetCount>0) and
                 not (NeedOxid or NeedIndole or NeedBLac or NeedOrg));
  EnableWindow(GetItemHandle(BTN_VIEW), isolateInfo);
  with exceptions^ do
  begin
    if NeedOxid or NeedIndole or NeedBLac or NeedOrg or
       NeedStS or NeedGmS or NeedTFG or NeedOxac or AbortedMissing then
      EnableWindow(GetItemHandle(BTN_SAVE), FALSE)
    else
      EnableWindow(GetItemHandle(BTN_SAVE), panelRead);
  end;
  EnableWindow(GetItemHandle(BTN_PRINT), panelRead);
  EnableWindow(GetItemHandle(BTN_SEARCH), isolateInfo);
  with exceptions^ do
    EnableWindow(GetItemHandle(BTN_RESOLVE), panelRead or
                    (NeedOxid or NeedIndole or NeedBLac or NeedOrg));
  EnableWindow(GetItemHandle(BTN_ABORT_PNL), (panelID <> 0));
end;

function TWAPanelViewDlg.UploadWAData: boolean;
var
  slot, pTyp   : byte;
  flgs         : integer;
  lastRead     : TimeDate;
  nextRead     : TimeDate;
  ret          : integer;
begin
  UploadWAData := FALSE;
  if GetPanelID then
  begin
    ret := PanelStatusInquiry(slot, pTyp, flgs, lastRead, nextRead);
    if ret = $46A then
      SetWAError(IDS_PANEL_NOT_IN_WA)
    else
    begin
      if ret = 0 then
      begin
        ret := GetPanelData(flgs,LastRead);
      end;
      if ret = 0 then
        UploadWAData := TRUE;
    end;
  end
  else
    SetWAError(IDS_NO_PANEL_ID);
end;

function TWAPanelViewDlg.GetPanelID: boolean;
var
  seq    : TSeqNum;
begin
  panelID := 0;
  wapDB^.dbr^.ClearRecord;
  seq := isordDB^.dbr^.GetSeqValue;
  wapDB^.dbr^.PutField(DBWAPNLIsoOrdRef, @seq);
  if wapDB^.dbc^.GetEQ(wapDB^.dbr) then
    wapDB^.dbr^.GetField(DBWAPNLID, @panelID, SizeOf(panelID));
  GetPanelID := (panelID <> 0);
end;

function TWAPanelViewDlg.GetPanelUsedFlag: boolean;
var
  seq    : TSeqNum;
  flg    : boolean;
begin
  flg := FALSE;
  wapDB^.dbr^.ClearRecord;
  seq := isordDB^.dbr^.GetSeqValue;
  wapDB^.dbr^.PutField(DBWAPNLIsoOrdRef, @seq);
  if wapDB^.dbc^.GetEQ(wapDB^.dbr) then
    wapDB^.dbr^.GetField(DBWAPNLUsedFlag, @flg, SizeOf(flg));
  GetPanelUsedFlag := flg;
end;

function TWAPanelViewDlg.PanelStatusInquiry(var slot, panelType: byte;
                                            var flgs: integer;
                                            var lastRead, nextRead: TimeDate): integer;
var
  WACmd     : PWAPSICmd;
  pId       : integer;
  pIDStr    : string;

begin
  PanelStatusInquiry := 0;
  WACmd := New(PWAPSICmd, Init(WAID));
  WACmd^.SetParams(panelID);
  if WACmd^.SendWAMessage then
  begin
    if WACmd^.WAStatNum = 0 then
    begin
      WACmd^.GetParams(panelId, slot, panelStatus, panelType, flgs, lastRead);
      panelType := panelType + 1;  { 1-based for trays methods }
      waSlot := slot;
    end
    else
      PanelStatusInquiry := WACmd^.WAStatNum;
  end
  else
    PanelStatusInquiry := WACmd^.WAErrNum;
  MSDisposeObj(WACmd);
end;

procedure TWAPanelViewDlg.PreScreenResults(Organism: Integer);
var i          : integer;
    dName      : array[0..63] of char;
    tempResList: PResults;
    E157Applies: Boolean;
    ESBLApplies: Boolean;
    BioStr     : array[0..32] of char;
begin
  exceptions^.PossibleE157:=false;
  exceptions^.PossibleESBL:=false;

  {Do not rescreen already stored data}
  if isoDB^.dbr^.FieldIsEmpty(DBISOTstDate) then
  if ScreenOn(E157Screen) or ScreenOn(ESBLScreen) then
  begin
    TempResList:=New(PResults,Init);
    TempResList^.LoadResults(isoDB^.dbr);
    TempResList^.TraysObj^.LoadTrayByNum(ResList^.TraysObj^.tfsrec.traynum);

    for i := 1 to TempResList^.TraysObj^.DrugCnt do
        with TempResList^.TraysObj^.dilList[i] do
        begin
          StrLCopy(dName, drugName, sizeof(dName)-1);
          TrimTrail(dName, dName, ' ', sizeof(dName)-1);
          TempResList^.SetResult(dname, 'MIC', drugDils[MICData[i]], True);
        end;

    if IDReady
       then StrPCopy(bioStr, biotypeObj^.biotype)
       else StrCopy(bioStr, '');

    TempResList^.SetResult('.BIO', 'BIO', BioStr, true);

    E157Applies:=E157Criterion(Organism,ExtractSet(SetFamily),TempResList);
    ESBLApplies:=not E157Applies and ESBLCriterion(Organism,TempResList);

    exceptions^.PossibleE157:=E157Applies;
    exceptions^.PossibleESBL:=ESBLApplies;
    MSDisposeObj(TempResList);
  end;
end;

function TWAPanelViewDlg.GetPanelData(flgs: integer; LastRead: TimeDate): integer;
{**************************************************************************}
{*  This method assumes that a PSI command was issued to the W/A for this *}
{*  panel and the status and flags are read.                              *}
{**************************************************************************}
var
  ret      : integer;
  flagsObj : PPanelFlags;
begin
  ret := 9999;
  flagsObj := New(PPanelFlags, Init(flgs));
  exceptions^.ClearAllExceptions;
  ClearWAExList;

  PosFlagsReady:=False;
  FillChar(micData, SizeOf(micData), #0);
  FillChar(posFlags, SizeOf(TPanelBitmap), #0);
  FillChar(virtualTests, SizeOf(TPanelBitmap), #0);

  ValidResults:=ValidResultSet(PanelID, LastRead);

  if not ProblemsOccurred(flagsObj) then
  begin
    ret := 0;

    case panelStatus of
      MICsAvailable:
        if ValidResults then
        begin
          ret := PanelMICsInquiry;
          if (ret = 0) and (resList^.traysObj^.tfsRec.idRec <> -1) then
            ret := VirtualTestsInquiry(flagsObj);
        end
        else ret := 9999;
      reagentsAdded:
        begin
          ret := 9999;
          { wait for process to be complete }
        end;
      processComplete:
        if ValidResults then
        begin
          { if there are MICs or extra tests on this panel, get em }
          ret := PanelMICsInquiry;
          GetPosFlags;

          if (ret = 0) and (resList^.traysObj^.tfsRec.idRec <> -1) then
          begin
            ret := VirtualTestsInquiry(flagsObj);
            { if this is not an MIC only panel then get the ID }
            if ret = 0 then
               ret := PanelResultsInquiry(FALSE);
          end;

          if (OrgNum<0)
             then exceptions^.NeedOrg :=
                  (resList^.traysObj^.tfsRec.idRec = -1)
             else PreScreenResults(OrgNum);
        end
        else ret := 9999;
      Else {Case..Else}
        begin
          { wait for process to be complete }
          ret := 9999;
          SetWAError(IDS_STS_No_Data);
        end;
      end;
    if ret = 0 then
      CheckExtraTests;
    ShowExceptions;
  end
  else if panelStatus = PanelMissing then
  begin
    exceptions^.AbortedMissing := TRUE;
    ShowExceptions;
  end;
  MSDisposeObj(flagsObj);
  GetPanelData := ret;
end;

function TWAPanelViewDlg.ProblemsOccurred(flagsObj: PPanelFlags): boolean;
begin
  ProblemsOccurred := FALSE;
  if EvaluateStatus(flagsObj) then
    ProblemsOccurred := TRUE;
  if EvaluateFlags(flagsObj) then
    ProblemsOccurred := TRUE;
end;

function TWAPanelViewDlg.EvaluateStatus(flagsObj: PPanelFlags): boolean;
var
  statNum    : integer;
begin
  if (panelStatus = 0) or
     (panelStatus = MICsAvailable) or
     (panelStatus = ReagentsAdded) or
     (panelStatus = ProcessComplete) or
     ((panelStatus = LampFailure) and resList^.traysObj^.TrayFlo) then
    EvaluateStatus := FALSE
  else
  begin
    EvaluateStatus := TRUE;
    GetStatNum(panelStatus, statNum, flagsObj);
    if statNum <> 0 then
      SetWAError(statNum);
  end;
end;

function TWAPanelViewDlg.EvaluateFlags(flagsObj: PPanelFlags): boolean;
Const
  StaphFamily = 2;
  RapNegID3SetClass = 221;
Var
  AFamily   : Integer;
  EFlags    : boolean;
begin
  EFlags := FALSE;
  if ValidResults and (flagsObj^.flagVal <> 0) then
  begin
    if flagsObj^.powerFailure then SetWaFlag(IDS_FLG_POWER_FAIL);
    if flagsObj^.ISFGrowth then SetWaFlag(IDS_FLG_ISF_GROWTH);
    if flagsObj^.BadSterileWell then SetWaFlag(IDS_FLG_BAD_STERILE_WELL);
    if flagsObj^.SkippedWells then SetWaFlag(IDS_FLG_SKIP_WELL);
    if flagsObj^.VerifyPanelId then SetWaFlag(IDS_FLG_VERIFY_ID);
    panelInWA := flagsObj^.panelInWA;
    if not panelInWA then SetWaFlag(IDS_FLG_PANEL_NOT_IN_WA);
    if flagsObj^.slowGrower then
      if panelStatus <> processComplete then
      begin
        exceptions^.Hours42 := TRUE;
        if panelStatus <> MICsAvailable then
          SetWaFlag(IDS_FLG_SLOW_GROWER);
      end;
    if flagsObj^.possibleMRSASE then
      if panelStatus <> processComplete then
      Begin
        AFamily := ExtractFamily(setFamily);
        If AFamily = StaphFamily then
          SetWaFlag(IDS_FLG_MRSA_SE)
        Else
          SetWaFlag(IDS_FLG_VRE_Hold);
      End;
    if flagsObj^.DelayedRead then SetWaFlag(IDS_FLG_DELAYED_READ);
    if flagsObj^.InvSterileWell then SetWaFlag(IDS_FLG_INV_STERILE_WELL);
    if flagsObj^.IDAvailOnRapid then SetWaFlag(IDS_FLG_IDS_AVAIL_RAPID);
    if flagsObj^.NoDispense then
    Begin
      SetWaFlag(IDS_FLG_NO_DISPENSE);
      isoDB^.dbr^.GetField(DBISOSetFamily, @setFamily, sizeof(setFamily));
      If ((ExtractSet(SetFamily)*10) + ExtractFamily(SetFamily)) = RapNegID3SetClass then
        Exceptions^.NeedIndole := TRUE;
    End;

    {** if rapid anaerobe panel or rapid yeast panels **}
    if (StrComp(tstGrpID, 'RAID') = 0) or
       (StrComp(tstGrpID, 'RYID') = 0) then
    begin
      if (flagsObj^.BadDCB) then
      begin
        flagsObj^.BadNPC := TRUE;
        SetWaFlag(IDS_FLG_BAD_NPC);
        EFlags := TRUE;
      end
      else if (flagsObj^.BadOFB) then
      begin
        flagsObj^.BadBNAC := TRUE;
        SetWaFlag(IDS_FLG_BAD_BNAC);
        EFlags := TRUE;
      end
    end
    else
    begin
      if flagsObj^.BadDCB then SetWaFlag(IDS_FLG_BAD_DCB);
      if flagsObj^.BadOFB then SetWaFlag(IDS_FLG_BAD_OFB);
    end;
  end;
  EvaluateFlags := EFlags;
end;

function TWAPanelViewDlg.PanelMICsInquiry: integer;
var
  WACmd     : PWAPMICmd;
  pId       : integer;
  slot      : byte;
  count     : byte;
begin
  PanelMICsInquiry := 0;
  WACmd := New(PWAPMICmd, Init(WAID));
  WACmd^.SetParams(panelID);
  if WACmd^.SendWAMessage then
  begin
    if WACmd^.WAStatNum = 0 then
    begin
      WACmd^.GetParams(pId, slot, count, micData);
(*    Move(micData[MaxMICLen+1], extraData[1], MaxExtra); *)
      { the MICs are stored after the PRI call because the org is needed for
        calculating some of the interpretations }
      MICsLoaded := TRUE;
    end
    else
      PanelMICsInquiry := WACmd^.WAStatNum;
  end
  else
    PanelMICsInquiry := WACmd^.WAErrNum;
  MSDisposeObj(WACmd);
end;

function TWAPanelViewDlg.VirtualTestsInquiry(flagsObj: PPanelFlags): integer;
var
  WACmd     : PWAPEICmd;
begin
  VirtualTestsInquiry := 0;
  FillChar(virtualTests, SizeOf(TPanelBitmap), #0);
  if not (flagsObj^.VirtualTests) then
    Exit;
  WACmd := New(PWAPEICmd, Init(WAID));
  WACmd^.SetParams(panelID);
  if WACmd^.SendWAMessage then
  begin
    if waCmd^.WAStatNum = 0 then
      Move(WACmd^.recvMsg^.priBits[1], virtualTests[0], SizeOf(PRIArray))
    else
      VirtualTestsInquiry := WACmd^.WAStatNum;
  end
  else
    VirtualTestsInquiry := WACmd^.WAErrNum;
  MSDisposeObj(WACmd);
end;

function TWAPanelViewDlg.PanelResultsInquiry(prompt: boolean): integer;
{**************************************************************************}
{ This procedure assumes a PMICmd has been executed }
{**************************************************************************}
Const
  RNID3_IDRec  = 20;        {Identifies BDesc record number for RNID3 panels}
var
  WACmd       : PWAPRICmd;
  pId         : integer;
  slot        : byte;
  aSet        : byte;
  bits        : PRIArray;
  gdBiotype   : boolean;

begin

  {Get the PDL var Did_Indole value for this panel}
  BioTypeObj^.IndWasDispd := False;
  If (resList^.traysObj^.tfsRec.IDRec = RNID3_IDRec) then
    BioTypeObj^.IndWasDispd := IndoleDispensedFlag(PanelID);

  PanelResultsInquiry := 0;
  WACmd := New(PWAPRICmd, Init(WAID));
  WACmd^.SetParams(panelID);
  if WACmd^.SendWAMessage then
  begin
    if WACmd^.WAStatNum = 0 then
    begin
      WACmd^.GetParams(pId, slot, class, bits);

      aSet := ExtractSet(setFamily);
      setFamily := MakeSetFamily(aSet, class);
      isoDB^.dbr^.PutField(DBISOSetFamily, @setFamily);

      Move(bits, posFlags, 12);
      gdBiotype := GetBiotype(@self, prompt, class, resList,
                              @posFlags, nil, @virtualTests,
                              PMICData(@micData), biotypeObj);
      if gdBiotype then
      begin
        { determine the organism and store it }
        if not biotypeObj^.GetTopID(orgNum, nil, nil, nil) then
        begin
          if not biotypeObj^.Identify^.InvalidBiotype then
            exceptions^.undefinedBiotype := TRUE;
        end
        else
        begin
          IDReady := TRUE;
          if biotypeObj^.Identify^.IDList.NumOrgs <= 0 then
          begin
            exceptions^.NoID := TRUE;
          end
          else
          begin
            with biotypeObj^.Identify^.IDList do
              if (PROBS.P^[1] <= 85) then
                exceptions^.IDTooLow := TRUE;
          end;
        end
      end
      else
      begin
        if (biotypeObj^.oxidaseBitNum <> -1) and
           TestBioBit(biotypeObj^.neededResults, biotypeObj^.oxidaseBitNum) then
          exceptions^.NeedOxid := TRUE; { Needs Oxidase result }
        if (biotypeObj^.indoleBitNum <> -1) and
           TestBioBit(biotypeObj^.neededResults, biotypeObj^.indoleBitNum) then
          exceptions^.NeedIndole := TRUE;
      end;
    end
    else
      PanelResultsInquiry := WACmd^.WAStatNum;
  end
  else
    PanelResultsInquiry := WACmd^.WAErrNum;
  MSDisposeObj(WACmd);
end;

procedure TWAPanelViewDlg.CheckExtraTests;
var
  testID    : array [0..32] of char;
  xtraTbl   : XtraType;
  numXTests : integer;
  i         : integer;
begin
  if (orgNum <> -1) and not resList^.traysObj^.TrayFlo then
  begin
    resList^.traysObj^.GetXtra(xtraTbl, numXTests);
    for i := 1 to numXTests do
    begin
      if GetETResult(i, testID, xtraTbl, resList, orgNum,
                     wellFile, @posFlags, PMICData(@micData)) = 3 then
      begin
        if StrComp(testID,'.StS') = 0 then
          exceptions^.NeedStS := TRUE
        else if StrComp(testID,'.BL') = 0 then
          exceptions^.NeedBLac := TRUE
        else if StrComp(testID,'.GmS') =  0 then
          exceptions^.NeedGmS := TRUE
        else if StrComp(testID,'.TFG') = 0 then
          exceptions^.NeedTFG := TRUE;
      end;
    end;  {for i := 1 to numxtests}
  end;  {if not stored results to follow}
end;

procedure TWAPanelViewDlg.EditCurrentMIC;
var
  drugRDN,
  dilStep : integer;
  pstr,
  newMIC  : PChar;
begin
  if not panelRead then
    Exit;

  drugRDN := micList^.GetSelIndex;
  if drugRDN = -1 then
  begin
    micList^.SetSelIndex(0);
    Exit;
  end
  else if drugRDN >= 0 then
  begin
    GetMem(pstr, 255);
    GetMem(newMIC, 21);
    if EditMIC(@self, PMICData(@micData), resList^.traysObj, drugRDN, newMIC) then
    begin
      StrCopy(pstr, resList^.traysObj^.dilList[drugRDN+1].drugname);
      Pad(pstr, pstr, ' ', 4);
      StrCat(pstr, newMIC);
      micList^.DeleteString(drugRDN);
      micList^.InsertString(pstr, drugRDN);
      micList^.SetSelIndex(drugRDN);
      if (resList^.traysObj^.tfsRec.idRec <> -1) and
         biotypeObj^.DrugUsed(drugRDN+1) then
      begin
        GetBiotype(@self, FALSE, class, resList,
                   @posFlags, nil, @virtualTests,
                   PMICData(@micData), biotypeObj);
        ShowBiochems;
        ShowBiotype;
        ShowOrganism;
      end;
      ReDrawExceptions;
    end;
    MSFreeMem(pstr, 255);
    MSFreeMem(newMIC, 21);
  end;
end;

procedure TWAPanelViewDlg.EditCurrentBiochem;

  procedure ToggleExtra(Idx: Integer);
  var XtraID : array[0..15] of char;
      XtraRes: array[0.. 4] of char;
      WellNum: Integer;
  begin
    {Prepare the xtra test id and result}
    biochemList^.GetSelString(XtraID, 15);
    StrCopy(XtraRes,@XtraID[StrLen(XtraID)-1]);
    StrLCopy(XtraID, XtraID, 4);
    TrimAll(XtraID, XtraID, ' ', 15);

    {Where on the panel is this Xtratest?}
    ResList^.TraysObj^.GetXtraWellNumber(wellFile, XtraID, WellNum);

    {Let's toggle its result}
    Case XtraRes[0] of
      '+': ClearBit(PosFlags[WellNum mod 12], (WellNum div 12));
      '-': SetBit(PosFlags[WellNum mod 12], (WellNum div 12));
    end;
  end;

var
  curIdx     : integer;
  pstr       : PChar;
  bitNum     : integer;
  sugar      : boolean;
  aSet       : byte;
  MICOnly    : boolean;
begin

  if not panelRead then
    Exit;

  MICOnly:=(resList^.traysObj^.tfsRec.idRec = -1) or (biotypeObj = nil);

  curIdx := biochemList^.GetSelIndex;
  if curIdx = -1 then
  begin
    biochemList^.SetSelIndex(0);
    Exit;
  end;

  if curIdx >= 0 then
  if (not MICOnly) and (curIdx <=BioTypeObj^.MaxbitNum) then
  begin
    GetMem(pstr, 255);
    bitNum := BioTypeObj^.MaxbitNum - curIdx;
    if EditBiochem(@self, biotypeObj, bitNum, resList, pstr, @posFlags, sugar) then
    begin
      if sugar and AllSugarsNeg(@posFlags) then
      begin
        class := NONFERMENTER;
        biotypeObj^.SetBioClass(class);
        aSet := ExtractSet(setFamily);
        setFamily := MakeSetFamily(aSet, biotypeObj^.class);
        isoDB^.dbr^.PutField(DBISOSetFamily, @setFamily);
      end;

      GetBiotype(@self, FALSE, class, resList,
                 @posFlags, nil, @virtualTests,
                 PMICData(@micData), biotypeObj);

      ShowBiotype;
      ShowOrganism;
      ShowBiochems;
      biochemList^.SetSelIndex(curIdx);

      ReDrawExceptions;
    end;
    MSFreeMem(pstr, 255);
  end
  else
  begin
    ToggleExtra(curIdx);
    ShowBiochems;
    biochemList^.SetSelIndex(curIdx);
  end;
  EnableButtons;
end;

function TWAPanelViewDlg.SaveTheTests: boolean;

  procedure ReLoadIsoInfo;
  var btype: PWABiotypeObject;
  begin
    {Save current biotype}
    btype:=biotypeObj;
    biotypeObj:=nil;

    {Reload information from the Isolate}
    isolateInfo := InitIsoInfo;

    if (NewOrgRef=0) then
        NewOrgRef:=isoOrgRef;

    {Restore the biotype}
    MSDisposeObj(biotypeObj);
    biotypeObj:=btype;
  end;

  procedure RefreshOrgDisplay;
  var pStr: array[0..128] of char;
  begin
    orgDB^.dbc^.GetSeq(orgDB^.dbr, newOrgRef);
    orgDB^.dbr^.GetFieldAsStr(DBORGSName, pStr, sizeof(pStr)-1);
    SetDlgItemText(hWindow, IDC_ORGANISM, pStr);
    SetDlgItemText(hWindow, IDC_Footnotes, '');
    SetDlgItemText(hWindow, IDC_SpclChars, '');
    ShowBiochems;
  end;

var
  canceled : boolean;
  TrayType : Integer;
  bioStr   : array[0..32] of char;
  pStr     : array[0..128] of char;

begin
  canceled := FALSE;
  SaveTheTests := FALSE;

  if IDReady then
    StrPCopy(bioStr, biotypeObj^.biotype)
  else
    StrCopy(bioStr, '');

  if newOrgRef = 0 then
  begin
    if isoOrgRef = 0 then
    begin
      if GetAnOrganism(@self, newOrgRef) then
      begin
        orgDB^.dbc^.GetSeq(orgDB^.dbr, newOrgRef);
        orgDB^.dbr^.GetFieldAsStr(DBORGSName, pStr, 128);
        SetDlgItemText(hWindow, IDC_ORGANISM, pStr);
      end
      else
        Exit;
    end
    {else
      newOrgRef := isoOrgRef;}
  end;


  {Save the traynum, SaveIsolateTests might change it}
  TrayType :=ResList^.TraysObj^.tfsrec.traynum;

  if SaveIsolateTests(@self, TRUE, TRUE, TRUE, TRUE, TRUE, canceled,
                      wellFile, orgDB, newOrgRef, bioStr,
                      PMICData(@micData), isoDB, specDB, resList, @posFlags) then
    if not canceled then
    begin
      dataSaved := TRUE;
      ClearWAExList;
      ClearStatus;
      if panelStatus = processComplete then
        ClearPanelID;
      recordLocked := LockIt;
    end;

  {Restore the TrayType, reload from DB, update org display}
  ResList^.TraysObj^.LoadTrayByNum(TrayType);
  if not canceled then
  begin
    ReloadIsoInfo;
    RefreshOrgDisplay;
  end;

  SaveTheTests := TRUE;
end;

procedure TWAPanelViewDlg.ClearPanelID;
var
  flg  : boolean;
begin
  if panelID <> 0 then
  begin
    { Clear the used flag in wapDB for isolate order
      Cannot delete it because may need it back to re-read from WA }
    flg := FALSE;
    wapDB^.dbr^.PutField(DBWAPNLUsedFlag, @flg);
    wapDB^.dbc^.UpdateRec(wapDB^.dbr);
  end;
end;

procedure TWAPanelViewDlg.ShowExceptions;
begin
  if (exceptions^.ExceptionsExist) and (ValidResults) then
  begin
    ReDrawExceptions;
    if exceptions^.PossibleESBL then
      SetException(IDS_EXC_PossibleESBL);
    if exceptions^.PossibleE157 then
      SetException(IDS_EXC_PossibleE157);
    if exceptions^.NeedStS then
      SetException(IDS_EXC_NEEDS_STS);
    if exceptions^.NeedGmS then
      SetException(IDS_EXC_NEEDS_GMS);
    if exceptions^.NeedTFG then
      SetException(IDS_EXC_NEEDS_TFG);
    if exceptions^.NeedBLac then
      SetException(IDS_EXC_NEEDS_BLAC);
    if exceptions^.NeedOxac then
      SetException(IDS_EXC_NEEDS_OXAC);
    if exceptions^.NeedOxid then
      SetException(IDS_EXC_NEEDS_OXID);
    if exceptions^.NeedOrg then
      SetException(IDS_EXC_NEEDS_ORG);
    if exceptions^.NoID then
      SetException(IDS_EXC_NO_ID);
(*    SetException(IDS_RARE_BIOTYPE); *)
    if exceptions^.IDTooLow then
      SetException(IDS_EXC_ID_TOO_LOW);
    if exceptions^.NeedIndole then
      SetException(IDS_EXC_NEEDS_INDOLE);
    if exceptions^.UndefinedBiotype then
      SetException(IDS_UNDEF_BIOTYPE);
    if exceptions^.AbortedMissing then
      SetException(IDS_EXC_ABORTED_MISSING);
    if exceptions^.Hours42 then
      SetException(IDS_EXC_HOURS_42);
  end;
end;

procedure TWAPanelViewDlg.ReDrawExceptions;

  procedure RedrawEx(ExNum: Integer);
  var msg : array[0..120] of char;
  begin
    SR(ExNum, msg, 120);
    SendDlgItemMsg(IDC_WAPNL_STATUS, lb_InsertString, word(-1), longint(@msg));
  end;

var flagsObj  : PPanelFlags;
    slot, pTyp: byte;
    WAFlags   : integer;
    lastRead  : TimeDate;
    nextRead  : TimeDate;
begin
  { Clear the status box, redraw exceptions }
  ClearStatus;
  if (exceptions^.ExceptionsExist) and (ValidResults) then
  begin
    if exceptions^.NeedStS then            RedrawEx(IDS_EXC_NEEDS_STS);
    if exceptions^.NeedGmS then            RedrawEx(IDS_EXC_NEEDS_GMS);
    if exceptions^.NeedTFG then            RedrawEx(IDS_EXC_NEEDS_TFG);
    if exceptions^.NeedBLac then           RedrawEx(IDS_EXC_NEEDS_BLAC);
    if exceptions^.NeedOxac then           RedrawEx(IDS_EXC_NEEDS_OXAC);
    if exceptions^.NeedOxid then           RedrawEx(IDS_EXC_NEEDS_OXID);
    if exceptions^.NeedOrg then            RedrawEx(IDS_EXC_NEEDS_ORG);
    if exceptions^.NoID then               RedrawEx(IDS_EXC_NO_ID);
    if exceptions^.IDTooLow then           RedrawEx(IDS_EXC_ID_TOO_LOW);
    if exceptions^.NeedIndole then         RedrawEx(IDS_EXC_NEEDS_INDOLE);
    if exceptions^.UndefinedBiotype then   RedrawEx(IDS_UNDEF_BIOTYPE);
    if exceptions^.AbortedMissing then     RedrawEx(IDS_EXC_ABORTED_MISSING);
    if exceptions^.Hours42 then            RedrawEx(IDS_EXC_HOURS_42);
    if exceptions^.PossibleESBL then       RedrawEx(IDS_EXC_PossibleESBL);
    if exceptions^.PossibleE157 then       RedrawEx(IDS_EXC_PossibleE157);
  end;

  if ValidResults then
  begin
    { Redraw WA Errors that depend on WAFlags }
    if GetPanelID then
    begin
      if PanelStatusInquiry(slot, pTyp, WAFlags, lastRead, nextRead) = $46A then
         SetWAError(IDS_PANEL_NOT_IN_WA);
    end
    else SetWAError(IDS_NO_PANEL_ID);

    flagsObj:=New(PPanelFlags, Init(WAFlags));
    ProblemsOccurred(flagsObj);
    MSDisposeObj(flagsObj);
  end;
end;

procedure TWAPanelViewDlg.SetWAError(errNum: integer );
var
  msg : array[0..120] of char;
begin
  SR(errNum, msg, 120);
  SendDlgItemMsg(IDC_WAPNL_STATUS, lb_InsertString, word(-1), longint(@msg));
end;

procedure TWAPanelViewDlg.SetException(exceptionNum : integer);
var
  tower : integer;
  slot  : byte;
begin
  if GetPanelUsedFlag then
  begin
    GetTowerSlot(waSlot, slot, tower);
    waexlistDB^.dbr^.PutField(DBWAEXPanelID, @panelID);
    waexlistDB^.dbr^.PutField(DBWAEXTower, @tower);
    tower := slot;  { for some reason slot is a byte in GetTowerSlot and an int in the DB }
    waexlistDB^.dbr^.PutField(DBWAEXSlot, @tower);
    waexlistDB^.dbr^.PutFieldAsStr(DBWAEXPanelName, tstGrpID);
    waexlistDB^.dbr^.PutField(DBWAEXStrNum, @exceptionNum);
    waexlistDB^.dbr^.ClearField(DBWAEXString);
    waexlistDB^.dbc^.InsertRec(waexlistDB^.dbr);
  end;
end;

procedure TWAPanelViewDlg.SetWAFlag(flgNum: integer);
var
  msg : array[0..120] of char;
begin
  SR(flgNum, msg, 120);
  SendDlgItemMsg(IDC_WAPNL_STATUS, lb_InsertString, word(-1), longint(@msg));
end;

procedure TWAPanelViewDlg.ClearStatus;
begin
  SendDlgItemMsg(IDC_WAPNL_STATUS, lb_ResetContent, 0, 0);
end;

procedure TWAPanelViewDlg.ClearWAExList;
var
  seq  : TSeqNum;
begin
  waexlistDB^.dbr^.ClearRecord;
  seq := isordDB^.dbr^.GetSeqValue;
  waexlistDB^.dbr^.PutField(DBWAEXIsoOrdRef, @seq);
  while waexlistDB^.dbc^.GetEQ(waexlistDB^.dbr) do
  begin
    waexlistDB^.dbc^.DeleteRec(waexlistDB^.dbr);
    if waexlistDB^.dbc^.dbErrorNum <> 0 then
      Break;
  end;
end;

function TWAPanelViewDlg.LockIt: boolean;
var
  pStr      : PChar;
begin
  LockIt := TRUE;
  if not isoDB^.dbc^.GetLock(isoDB^.dbr) then
  begin
    LockIt := FALSE;
    GetMem(pStr, 256);
    InfoMsg(hWindow, '', SR(IDS_RECORDLOCKED, pStr, 255));
    MSFreeMem(pStr, 256);
  end;
end;

function TWAPanelViewDlg.AskUserToSaveTests: boolean;
var
  pStr : array[0..63] of Char;
begin
  AskUserToSaveTests := true;
  if panelRead and not dataSaved then
    case YesNoCancelMsg(hWindow, '', SR(IDS_SAVE_DATA, pStr, 63)) of
      IDYES   : AskUserToSaveTests := SaveTheTests;
      IDCANCEL: AskUserToSaveTests := false;
    end;
end;


constructor TResolveDlg.Init(aParent: PWindowsObject; dbIso: PDBFile;
                             exceptions: PExceptions;
                             bLacRes, oxidaseRes, indoleRes, orgRes: pointer);
begin
  inherited Init(aParent, MakeIntResource(DLG_RESOLVE));
  oxidaseRequired := exceptions^.NeedOxid;
  indoleRequired := exceptions^.NeedIndole;
  bLacRequired := exceptions^.NeedBLac;
  orgRequired := exceptions^.NeedOrg;
  oxidaseResult := oxidaseRes;
  indoleResult := indoleRes;
  bLacResult := bLacRes;
  orgRef := orgRes;

  listObj := New(PListObject, Init);
  tempRec := New(PDBRec, Init(dbIso^.dbc));
  tempRec^.PutField(DBISOOrgRef, orgRef);

  fld := New(PUIFld, Init(@self, dbIso^.dbd, listObj, orgRequired, DBISOOrgRef, IDC_ORGID, 0));
end;

destructor TResolveDlg.Done;
begin
  MSDisposeObj(tempRec);
  MSDisposeObj(listObj);
  inherited Done;
end;

procedure TResolveDlg.SetUpWindow;
begin
  inherited SetUpWindow;
  fld^.XferRecToCtl(tempRec);

  {fla, always enable the OrgNum edit box}
  EnableWindow(GetItemHandle(IDC_LBL1), True);
  EnableWindow(GetItemHandle(IDC_ORGID), True);

  if not indoleRequired then
  begin
    EnableWindow(GetItemHandle(IDC_LBL2), FALSE);
    EnableWindow(GetItemHandle(IDC_INDPOS), FALSE);
    EnableWindow(GetItemHandle(IDC_INDNEG), FALSE);
  end;
  if not oxidaseRequired then
  begin
    EnableWindow(GetItemHandle(IDC_LBL3), FALSE);
    EnableWindow(GetItemHandle(IDC_OXIPOS), FALSE);
    EnableWindow(GetItemHandle(IDC_OXINEG), FALSE);
  end;
  if not bLacRequired then
  begin
    EnableWindow(GetItemHandle(IDC_LBL4), FALSE);
    EnableWindow(GetItemHandle(IDC_BLACPOS), FALSE);
    EnableWindow(GetItemHandle(IDC_BLACNEG), FALSE);
  end;
end;

function TResolveDlg.CanClose : boolean;
var
  okToClose : boolean;
begin
  CanClose := FALSE;
  if inherited CanClose then
  begin
    okToClose := TRUE;
    if indoleRequired then
      if (SendDlgItemMsg(idc_IndNeg, bm_GetCheck, 1, 0) = 0) and
         (SendDlgItemMsg(idc_IndPos, bm_GetCheck, 1, 0) = 0) then
        okToClose := FALSE
      else
        if (SendDlgItemMsg(idc_IndPos, bm_GetCheck, 1, 0) <> 0) then
          indoleResult^ := 1
        else
          indoleResult^ := 0;

    if oxidaseRequired then
      if (SendDlgItemMsg(idc_oxiNeg, bm_GetCheck, 1, 0) = 0) and
         (SendDlgItemMsg(idc_oxiPos, bm_GetCheck, 1, 0) = 0) then
        okToClose := FALSE
      else
        if (SendDlgItemMsg(idc_oxiPos, bm_GetCheck, 1, 0) <> 0) then
          oxidaseResult^ := 1
        else
          oxidaseResult^ := 0;

    if bLacRequired then
      if (SendDlgItemMsg(idc_BLacNeg, bm_GetCheck, 1, 0) = 0) and
         (SendDlgItemMsg(idc_BLacPos, bm_GetCheck, 1, 0) = 0) then
        okToClose := FALSE
      else
        if (SendDlgItemMsg(idc_BLacPos, bm_GetCheck, 1, 0) <> 0) then
          bLacResult^ := 1
        else
          bLacResult^ := 0;


    {If the user did not enter an OrgNum, and it was}
    {required, do not allow them to close the window}
    if fld^.Format <> 0 then
       okToClose := okToClose and (not orgRequired)
    else
    {Always use the OrgNum supplied by the user, if any}
    if okToClose then
    begin
      fld^.XferCtlToRec(tempRec);
      tempRec^.GetField(DBISOOrgRef, orgRef, SizeOf(TSeqNum));
    end;

    CanClose := okToClose;
  end;
end;


{ EXPORTED PROCEDURES }

procedure ResultsInquiry(aParent: PWindowsObject; aSessRef: TSeqNum;
                         exceptDB: PDBFile);
var
  d : PWAPanelViewDlg;
begin
  d := New(PWAPanelViewDlg, Init(aParent, MakeIntResource(DLG_WAPANEL_VIEW),
                                 aSessRef, exceptDB));
  if d <> nil then
    Application^.ExecDialog(d);
end;


END.
