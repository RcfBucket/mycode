unit PgSetup;

INTERFACE

uses
  DMSDebug,
  DMString,
  ctlLib,
  ApiTools,
  IntlLib,
  RptUtil,
  PrnInfo,
  RptShare,
  RptData,
  DlgLib,
  MScan,
  SpinEdit,
  Objects,
  oDialogs,
  oWindows,
  WinProcs,
  WinTypes,
  Strings;

{$I RPTGEN.INC}

type
  PPageSetupDlg = ^TPageSetupDlg;
  TPageSetupDlg = object(TCenterDlg)
    constructor Init(AParent: PWindowsObject; var APgDesc: TRptPage);
    destructor Done; virtual;
    procedure SetupWindow; virtual;
    procedure wmCommand(var msg: TMessage); virtual wm_First + WM_COMMAND;
    function CanClose: Boolean; virtual;
    private
    width       : PSpinReal;
    height      : PSpinReal;
    leftM       : PSpinReal;
    topM        : PSpinReal;
    rightM      : PSpinReal;
    bottomM     : PSpinReal;
    rptPer      : PSpinInt;
    spacing     : PSpinReal;
    papers      : PComboBox;
    pInfo       : PCollection;
    pgDesc      : PRptPage;
    ignoreEditChange : Boolean;
    inLandscape : Boolean;
    maxWidth    : longint;
  end;


IMPLEMENTATION

{-------------------------------[ TPageSetupDlg ]--}

constructor TPageSetupDlg.Init(AParent: PWindowsObject; var aPgDesc: TRptPage);
var
  amax  : real;
  c     : PControl;
  k     : integer;
begin
  inherited Init(AParent, MAKEINTRESOURCE(DLG_PAGESET));

  for k:= 400 to 407 do
    c:= New(PLStatic, InitResource(@self, k, 0, false));
  for k:= 300 to 306 do
    c:= New(PLStatic, InitResource(@self, k, 0, false));
  c:= New(PLRadioButton, InitResource(@self, IDC_PORT, false));
  c:= New(PLRadioButton, InitResource(@self, IDC_LAND, false));

  maxWidth:= 1440 * 22;  {- maximum width allowed }
  LTwipsToUnits(maxWidth, aMax);

  papers:= New(PComboBox, InitResource(@self, IDC_PAPERSIZE, 100));

  width:= New(PSpinReal, InitResource(@self, IDC_WIDTH, 7,0, 0,aMax,0.1));
  height:= New(PSpinReal, InitResource(@self, IDC_HEIGHT, 7,0, 0,aMax,0.1));
  leftM:= New(PSpinReal, InitResource(@self, IDC_LEFT, 7,0, 0,aMax,0.1));
  rightM:= New(PSpinReal, InitResource(@self, IDC_RIGHT, 7,0, 0,aMax,0.1));
  topM:= New(PSpinReal, InitResource(@self, IDC_TOP, 7,0, 0,aMax,0.1));
  bottomM:= New(PSpinReal, InitResource(@self, IDC_BOTTOM, 7,0, 0,aMax,0.1));

  rptPer:= New(PSpinInt, InitResource(@self, IDC_RPTPERPAGE, 3,0, 1, MaxRptsPerPage, 1));

  LTwipsToUnits(720, aMax);
  spacing:= New(PSpinReal, InitResource(@self, IDC_SPACING, 5, 0, 0,aMax,0.1));

  pInfo:= New(PCollection, Init(25, 8));
  pgDesc:= @aPgDesc;
  ignoreEditChange:= true;
end;

destructor TPageSetupDlg.Done;
begin
  MSDisposeObj(pInfo);
  inherited Done;
end;

procedure TPageSetupDlg.SetupWindow;
var
  k     : Integer;
  pstr  : array[0..100] of char;
  tinfo  : PCollection;
  item  : PPaperInfo;
  idx   : Integer;
  sz    : TPoint;
  iData : Longint;

  function Matches(p: PPaperInfo): Boolean; far;
  begin
    Matches:= (idx = p^.idx);
  end;

  procedure SetNum(ed: PSpinReal; num: longint);
  {- set a number in an edit field, converting num from LTwips to the currently
     selected units }
  var
    units: Real;
  begin
    LTwipsToUnits(num, units);
    ed^.SetNum(units);
  end;

begin
  inherited SetupWindow;

  PaperInfo(nil, pinfo);  {- get all default paper info }

  tInfo:= New(PCollection, Init(10,8)); {- get printer specific paper info }
  PaperInfo(prn, tInfo);

  MergePapers(pInfo, tInfo);
  MSDisposeObj(tInfo);

  {- add user defined paper size if the printer does not already support it}
  idx:= DMPAPER_USER;
  if pInfo^.FirstThat(@Matches) = nil then
  begin
    sz.x:= 0;
    sz.y:= 0;
    pinfo^.Insert(New(PPaperInfo, Init(SR(IDS_USERDEFSIZE, pstr, sizeof(pstr)-1), sz, DMPAPER_USER)));
  end;

  {- build list box for all paper information }
  for k:= 0 to pinfo^.count - 1 do
  begin
    item:= pinfo^.At(k);
    idx:= papers^.AddString(item^.name);
    SendDlgItemMsg(IDC_PAPERSIZE, CB_SETITEMDATA, idx, item^.idx);
  end;

  {- displays "inches", "mm", or "cm" for all edit fields }
  UnitsStr(true, pstr, 100);
  for k:= 300 to 306 do
    SendDlgItemMsg(k, WM_SETTEXT, 0, longint(@pstr));

  {- set orientation of selected paper }
  SendDlgItemMsg(IDC_PORT, BM_SETCHECK, bf_Unchecked, 0); {- first must clear to }
  SendDlgItemMsg(IDC_LAND, BM_SETCHECK, bf_Unchecked, 0); {- make windows happy }
  inLandScape:= pgDesc^.orient = DMORIENT_LANDSCAPE;
  if inLandScape then
    SendDlgItemMsg(IDC_LAND, BM_SETCHECK, bf_Checked, 0)
  else
    SendDlgItemMsg(IDC_PORT, BM_SETCHECK, bf_Checked, 0);

  {- width }
  SetNum(width, pgDesc^.size.x);
  SetNum(height, pgDesc^.size.y);
  {- margins }
  SetNum(leftM, pgDesc^.margin.left);
  SetNum(rightM, pgDesc^.margin.right);
  SetNum(topM, pgDesc^.margin.top);
  SetNum(bottomM, pgDesc^.margin.bottom);

  {- set reports per page }
  rptPer^.SetNum(pgDesc^.rptsPerPage);

  {- set spacing }
  SetNum(spacing, pgDesc^.spacing);

  {- set list index to proper paper size }
  idx:= pgDesc^.paperIdx;
  item:= pInfo^.FirstThat(@Matches);
  if item <> nil then
  begin
    k:= 0;
    while (k < papers^.GetCount) do
    begin
      iData:= SendDlgItemMsg(IDC_PAPERSIZE, CB_GETITEMDATA, k, 0);
      if iData = idx then
      begin
        papers^.SetSelIndex(k);
        k:= papers^.GetCount;
      end;
      Inc(k);
    end;
  end
  else
    papers^.SetSelIndex(0);

  ignoreEditChange:= false;
end;

procedure TPageSetupDlg.WMCommand(var msg: TMessage);
var
  pstr, p2   : array[0..100] of char;

  function MatchWH(sz: TPoint; w, h: longint): boolean;
  {- attempt to match width and height with a paper size.
     Takes into account the current paper orientation }
  begin
    if inLandscape then
      MatchWH:= (w = sz.y) and (h = sz.x)
    else
      MatchWH:= (w = sz.x) and (h = sz.y);
  end;

  procedure MatchPaper;
  {- attempt to match user specified width with an
     existing paper size }
  var
    item   : PPaperInfo;
    num    : Real;
    w, h   : longint;
    k      : Integer;
    found  : boolean;
  begin
    item:= nil;
    w:= 0;
    h:= 0;

    UnitsToLTwips(width^.GetDouble, w);
    UnitsToLTwips(height^.GetDouble, h);

    {- find a paper that matches }
    k:= 0;
    found:= false;
    while not found and (k < pInfo^.count) do
    begin
      item:= pInfo^.At(k);
      if MatchWH(item^.size, w, h) then
        found:= true
      else
        Inc(k);
    end;

    if not found then
      k:= papers^.GetCount - 1;  {- user defined }
    papers^.SetSelIndex(k);
  end;

  procedure PaperChange;
  {- set width and height to match paper selected }
  var
    k   : Integer;
    item: PPaperInfo;
    num : Real;
    wIdx, hIdx: PSpinReal;
  begin
    ignoreEditChange:= true;

    k:= papers^.GetSelIndex;
    strcopy(pstr, '');
    if k = -1 then
    begin
      width^.Clear;
      height^.Clear;
    end
    else
    begin
      if inLandscape then  { convert width and height depending on orientation}
      begin
        wIdx:= height;
        hIdx:= width;
      end
      else
      begin
        wIdx:= width;
        hIdx:= height;
      end;

      item:= pInfo^.At(k);
      LTwipsToUnits(item^.size.x, num);
      wIdx^.SetNum(num);

      LTwipsToUnits(item^.size.y, num);
      hIdx^.SetNum(num);
    end;
    ignoreEditChange:= false;
  end;

  procedure OrientChange;
  var
    p1, p2 : array[0..100] of char;
    o,r    : integer;
    n1,n2  : real;
  begin
    if ((SendDlgItemMsg(IDC_LAND, BM_GETCHECK, 0,0) = bf_checked) and not inLandscape) or
       ((SendDlgItemMsg(IDC_PORT, BM_GETCHECK, 0,0) = bf_checked) and inLandScape) then
    begin
      {- swap width and height }
      ignoreEditChange:= true;
      inLandscape:= SendDlgItemMsg(IDC_LAND, BM_GETCHECK, 0,0) = bf_checked;
      n1:= height^.GetDouble;
      height^.SetNum(width^.GetDouble);
      width^.SetNum(n1);

      {- swap margins }
      Orientation(prn, o, r);
      if r = 90 then
      begin
        if inLandscape then
        begin
          n1:= leftM^.GetDouble;   { n1 = left }
          n2:= topM^.GetDouble;    { n2 = top }
          topM^.SetNum(n1);     { top = left(n1) }
          n1:= rightM^.GetDouble;  { n1 = right }
          rightM^.SetNum(n2);   { right= top(n2) }
          n2:= bottomM^.GetDouble; { n2 = bottom }
          bottomM^.SetNum(n1);  { bottom = right(n1) }
          leftM^.SetNum(n2);    { left = bottom(n2) }
        end
        else
        begin
          n1:= leftM^.GetDouble;   { n1 = left }
          n2:= topM^.GetDouble;    { n2 = top }
          leftM^.SetNum(n2);    { left = top (n2) }
          n2:= rightM^.GetDouble;  { n2 = right }
          topM^.SetNum(n2);     { top = right (n2)}
          n2:= bottomM^.GetDouble; { n2 = bottom }
          rightM^.SetNum(n2);   { right = bottom (n2) }
          bottomM^.SetNum(n1);  { bottom = left (n1) }
        end;
      end
      else
      begin
        if inLandscape then
        begin
          n1:= topM^.GetDouble;    { n1 = top }
          n2:= rightM^.GetDouble;  { n2 = right }
          topM^.SetNum(n2);     { top = right (n2) }
          n2:= bottomM^.GetDouble; { n2 = bottom }
          rightM^.SetNum(n2);   { right= bottom (n2) }
          n2:= leftM^.GetDouble;   { n2 = left }
          bottomM^.SetNum(n2);  { bottom = left (n2) }
          leftM^.SetNum(n1);    { left = top (n1) }
        end
        else
        begin
          n1:= rightM^.GetDouble;  { n1 = right }
          n2:= topM^.GetDouble;    { n2 = top }
          rightM^.SetNum(n2);   { right = top (n2) }
          n2:= bottomM^.GetDouble; { n2 = bottom }
          bottomM^.SetNum(n1);  { bottom = right (n1)}
          n1:= leftM^.GetDouble;   { n1 = left }
          leftM^.SetNum(n2);    { left = bottom (n2) }
          bottomM^.SetNum(n1);  { bottom = left (n1) }
        end;
      end;

      ignoreEditChange:= false;
    end;
  end;

begin
  if msg.lParamLo > 0 then  {- not from menu }
  begin
    case msg.lParamHi of  {- check notification code }
      EN_CHANGE:
        if not ignoreEditChange and ((msg.wParam = IDC_HEIGHT) or (msg.wParam = IDC_WIDTH)) then
          MatchPaper;
      LBN_SELCHANGE:
        if (msg.wParam = papers^.GetID) then
          PaperChange;
      BN_CLICKED:
        if (msg.wParam = IDC_LAND) or (msg.wParam = IDC_PORT) then
          OrientChange;
    end;
  end;
  inherited wmCommand(msg);
end;

function TPageSetupDlg.CanClose: Boolean;
var
  ret      : Boolean;
  tPage    : TRptPage;

  function CheckNumber(ed: PSpinReal; lo, hi: Integer; var result: integer): boolean;
  {- check number to see if it is valid and in a range,
     lo and hi are specified in LTwips and are inclusive }
  var
    rNum     : real;
    p2, pstr : array[0..100] of char;
    n1, n2   : array[0..25] of char;
    l        : longint;
    pc       : array[0..1] of PChar;
  begin
    CheckNumber:= false;
    if ed^.FldIsBlank or not ed^.NumIsValid then
    begin
      ErrorMsg(HWindow, SR(IDS_ERROR, p2, sizeof(p2)-1), SR(IDS_INVNUMFORMAT, pstr, sizeof(pstr)-1));
      FocusCtl(HWindow, ed^.GetID);
    end
    else
    begin
      rNum:= ed^.GetDouble;
      UnitsToLTwips(rNum, l);
      if (l < lo) or (l > hi) then
      begin
        SR(IDS_NOTINRANGE, p2, sizeof(p2)-1);
        pc[0]:= n1;
        pc[1]:= n2;
        LTwipsToUnits(lo, rnum);
        IntlRealToStr(rnum, n1, 25);
        LTwipsToUnits(hi, rnum);
        IntlRealToStr(rnum, n2, 25);

        WVsprintf(pstr, p2, pc);
        strcat(pstr, ' ');
        UnitsStr(true, p2, 30);
        strcat(pstr, p2);
        ErrorMsg(HWindow, SR(IDS_ERROR, p2, sizeof(p2)-1), pstr);
        FocusCtl(HWindow, ed^.GetID);
      end
      else
      begin
        result:= integer(l);
        CheckNumber:= true;
      end;
    end;
  end;

  function CheckPaper: Boolean;
  {- check paper to see if it supported by current printer }
  {- if not, prompt, to see if user cares }
  var
    k   : Integer;
    isOK: Boolean;
    tinf: PCollection;
    p1  : array[0..50] of char;
    p2  : array[0..200] of char;
  begin
    isOK:= False;
    if papers^.GetSelIndex = -1 then
    begin
      ErrorMsg(HWindow, SR(IDS_ERROR, p2, sizeof(p2)-1), SR(IDS_PAPERMUSTBESEL, p1, sizeof(p1)-1));
      FocusCtl(HWindow, IDC_PAPERSIZE);
    end
    else
    begin
      k:= papers^.GetSelIndex;
      tPage.paperIdx:= Integer(SendDlgItemMsg(IDC_PAPERSIZE, CB_GETITEMDATA, k, 0));

      tInf:= New(PCollection, Init(25, 8));
      if PaperInfo(prn, tinf) then
      begin
        k:= 0;
        while not isOK and (k < tinf^.count) do
        begin
          isOK:= PPaperInfo(tinf^.At(k))^.idx = tPage.paperIdx;
          Inc(k);
        end;
        if not isOK then
        begin
          isOK:= YesNoMsg(HWindow, SR(IDS_ERROR, p1, sizeof(p1)-1),
                                   SR(IDS_PAPERNOTSUP, p2, sizeof(p2)-1));

        end;
      end;
      MSDisposeObj(tInf);

      {- now check width fields }
      if isOK then
      begin
        isOK:= CheckNumber(width, 0, maxWidth, tPage.size.x);
        if isOK then
          isOK:= CheckNumber(height, 0, maxWidth, tPage.size.y);
      end;
    end;
    CheckPaper:= isOK;
  end;

  function CheckMargins: Boolean;
  {- check margins to see if they are in range with the printer }
  var
    isOK  : Boolean;
    mrg   : TRect;
    p1    : array[0..50] of char;
    p2    : array[0..200] of char;
  begin
    isOK:= CheckNumber(leftM, 0, 2*1440, tPage.margin.left);
    if isOK then
      isOK:= CheckNumber(rightM, 0, 2*1440, tPage.margin.right);
    if isOK then
      isOK:= CheckNumber(topM, 0, 2*1440, tpage.margin.top);
    if isOK then
      isOK:= CheckNumber(bottomM, 0, 2*1440, tpage.margin.bottom);

    if isOK then
    begin
      {- make sure margins are within printers range }
      PrintMargins(prn, mrg);
      if (mrg.left > tPage.margin.left) or
         (mrg.top > tpage.margin.top) or
         (mrg.bottom > tpage.margin.bottom) or
         (mrg.right > tpage.margin.right) then
      begin
        isOK:= YesNoMsg(HWindow, SR(IDS_WARNING, p1, sizeof(p1)-1),
                                 SR(IDS_BADMARGINS, p2, sizeof(p2)-1));
      end;
    end;
    CheckMargins:= isOK
  end;

  function CheckSpacing: boolean;
  var
    rNum     : real;
    l        : longint;
    pstr, p2 : array[0..100] of char;
    p1       : array[0..25] of char;
    pc       : PChar;
  begin
      rNum:= spacing^.GetDouble;
      UnitsToLTwips(rNum, l);
      if (l < 0) or (l > 720) or not spacing^.NumIsValid then
      begin
        SR(IDS_INVSPACING, p2, sizeof(p2)-1);
        LTwipsToUnits(720, rnum);
        IntlRealToStr(rnum, p1, 25);
        pc:= p1;
        WVSprintf(pstr, p2, pc);
        StrCat(pstr, ' ');
        UnitsStr(true, p2, 30);
        StrCat(pstr, p2);
        ErrorMsg(HWindow, SR(IDS_ERROR, p2, sizeof(p2)-1), pstr);
        FocusCtl(HWindow, spacing^.GetID);
        CheckSpacing:= false;
      end
      else
      begin
        tpage.spacing:= integer(l);
        CheckSpacing:= true;
      end;
  end;

  function CheckOrient: Boolean;
  begin
    if SendDlgItemMsg(IDC_PORT, BM_GETCHECK, 0, 0) = bf_Checked then
      tPage.orient:= DMORIENT_PORTRAIT
    else
      tPage.orient:= DMORIENT_LANDSCAPE;
    CheckOrient:= true;
  end;

  function CheckRptPerPage: Boolean;
  var
    p2,pstr: array[0..100] of char;
    n1, n2   : array[0..25] of char;
    pc       : array[0..1] of PChar;
    code  : integer;
    num   : integer;
    isOK  : Boolean;
  begin
    isOK:= false;
    num:= integer(rptPer^.GetLong);
    if not rptPer^.NumIsValid then
    begin
      ErrorMsg(HWindow, SR(IDS_ERROR, p2, sizeof(p2)-1), SR(IDS_INVNUMFORMAT, pstr, sizeof(pstr)-1));
      FocusCtl(HWindow, rptPer^.GetID);
    end
    else if (num > 0) and (num <= maxRptsPerPage) then
    begin
      isOK:= true;
      tPage.rptsPerPage:= num;
    end
    else
    begin
      SR(IDS_NOTINRANGE, p2, sizeof(p2)-1);
      pc[0]:= n1;
      pc[1]:= n2;
      StrCopy(n1, '1');
      Str(maxRptsPerPage, n2);
      WVsprintf(pstr, p2, pc);
      ErrorMsg(HWindow, SR(IDS_ERROR, p2, sizeof(p2)-1), pstr);
      FocusCtl(HWindow, rptPer^.GetID);
    end;
    CheckRptPerPage:= isOK;
  end;

begin
  fillChar(tPage, sizeof(tpage), 0);

  ret:= CheckPaper;
  if ret then
    ret:= CheckMargins;
  if ret then
    ret:= CheckOrient;
  if ret then
    ret:= CheckRptPerPage;
  if ret then
    ret:= CheckSpacing;

  {- transfer values to pgDesc record for return }
  if ret then
    pgDesc^:= tPage;

  CanClose:= ret;
end;

END.

{- rcf }
