(****************************************************************************


uispec.inc

produced by Borland Resource Workshop

User interface constants for UISpecimen
Module ID = $2100 = 8448

*****************************************************************************)

const
  DLG_UISPECIMEN      = 8448;

  IDC_EDCOLLECT       = 201;
  IDC_EDFREETEXT      = 202;
  IDC_EDPATID         = 203;
  IDC_EDSOURCE        = 204;
  IDC_EDSPECID        = 205;
  IDC_EDSTATUS        = 206;
  IDC_EDWARD          = 207;

  IDC_EDUSER1         = 400;
  IDC_EDUSER2         = 401;
  IDC_EDUSER3         = 402;
  IDC_EDUSER4         = 403;
  IDC_EDUSER5         = 404;
  IDC_EDUSER6         = 405;
  IDC_EDUSER7         = 406;
  IDC_EDUSER8         = 407;
  IDC_EDUSER9         = 408;
  IDC_EDUSER10        = 409;

  IDC_LBLCOLLECT      = 301;
  IDC_LBLFIRST        = 302;
  IDC_LBLFREETEXT     = 303;
  IDC_LBLLAST         = 304;
  IDC_LBLPATID        = 305;
  IDC_LBLSOURCE       = 306;
  IDC_LBLSPECID       = 307;
  IDC_LBLSTATUS       = 308;
  IDC_LBLWARD         = 309;

  IDC_LBLUSER1        = 500;
  IDC_LBLUSER2        = 501;
  IDC_LBLUSER3        = 502;
  IDC_LBLUSER4        = 503;
  IDC_LBLUSER5        = 504;
  IDC_LBLUSER6        = 505;
  IDC_LBLUSER7        = 506;
  IDC_LBLUSER8        = 507;
  IDC_LBLUSER9        = 508;
  IDC_LBLUSER10       = 509;

  IDC_TXTFIRST        = 600;
  IDC_TXTLAST         = 601;
  IDC_MODE            = 602;

  IDC_GS              = 103;
  IDC_REVIEW          = 108;
  IDC_PATIENT         = 110;

