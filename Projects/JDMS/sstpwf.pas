(***************************************************************************
*
* SSTPWF.PAS
*
* Description - This unit is for the SSTPWF.OBJ file. It defines the 
*               entry points for that driver.
*
***************************************************************************)

unit SSTPWF;

interface

function ssquery( VAR querystr : STRING; len : WORD ) : WORD;

implementation

function ssquery( VAR querystr : STRING; len : WORD ) : WORD ; external ;

{$L SSTPWF.OBJ}

end.