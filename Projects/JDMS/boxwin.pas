{$F+}
unit BoxWin;

{- provides a window object to represent sections and fields in sections }

INTERFACE

uses
  userMsgs,
  DMString,
  DMSDebug,
  Ctl3d,
  DlgLib,
  APITools,
  Intllib,
  RptAlign,
  RptShare,
  Rptutil,
  Strings,
  Odialogs,
  Owindows,
  Objects,
  Win31,
  Winprocs,
  Wintypes;

{$I RPTGEN.INC}

type
  PBoxWin = ^TBoxWin;
  TBoxWin = object(T3DWindow)
    {- the following are read only! }
    focused  : boolean;
    boxSeq   : longint;  {- db sequence number for the box. Set from LoadData }
    inChange : boolean;
    {}
    constructor Init(AParent: PWindowsObject; ATitle: PChar;
                     rect: TRect; AnOrigin: TPoint; AZoom: real;
                     aPage: TRptPage; AFont: TRptFont);
    destructor Done; virtual;
    procedure SetupWindow; virtual;
    procedure ChangeOrigin(newOrigin: TPoint);
    procedure ChangeZoom(newOrigin: TPoint; aZoom: real);

    {- windows related methods }
    function GetClassName: PChar; virtual;
    procedure GetWindowClass(var AWndClass: TWndClass); virtual;
    procedure WMRButtonDown(var msg: Tmessage); virtual    WM_FIRST + WM_RBUTTONDOWN;
    procedure WMLButtonDown(var msg: Tmessage); virtual    WM_FIRST + WM_LBUTTONDOWN;
    procedure WMLButtonDblClk(var msg: Tmessage); virtual  WM_FIRST + WM_LBUTTONDBLCLK;
    procedure WMSize(var msg: TMessage); virtual           WM_FIRST + WM_SIZE;
    procedure WMMove(var msg: TMessage); virtual           WM_FIRST + WM_MOVE;
    procedure NCHitTest(var msg: TMessage); virtual        WM_FIRST + WM_NCHITTEST;
    procedure NCLButtonDown(var msg: TMessage); virtual    WM_FIRST + WM_NCLBUTTONDOWN;
    procedure NCLButtonDblClk(var msg: TMessage); virtual  WM_FIRST + WM_NCLBUTTONDBLCLK;
    procedure NCRButtonDown(var msg: TMessage); virtual    WM_FIRST + WM_NCRBUTTONDOWN;
    procedure Paint(dc: HDC; var ps: TPaintStruct); virtual;
    procedure WMMinMax(var msg: TMessage); virtual         WM_FIRST + WM_GETMINMAXINFO;
    procedure WMSetFocus(var msg: TMessage); virtual       WM_FIRST + WM_SETFOCUS;
    procedure WMKillFocus(var msg: TMessage); virtual      WM_FIRST + WM_KILLFOCUS;
    procedure WMChar(var msg: TMessage); virtual           WM_FIRST + WM_char;
    procedure WMKeyDown(var msg: Tmessage); virtual        WM_FIRST + WM_KEYDOWN;
    procedure GetSize(var ASize: TLRect);
    procedure SetSize(aSize: TLRect);
    procedure ChangePage(ANewPage: TRptPage);
    procedure GetName(aName: PChar; maxlen: integer); virtual;
    function  GetNameLength: integer; virtual;
    procedure SetDisplayFont(aFont: TRptFont);
    procedure DrawCaption(dc: HDC); virtual;
    function BuildBoxFont(aLogFont: TLogFont): HFont; virtual;

    procedure BringToTop;

    {- user methods, should be overridden by descendents }
    procedure AlignBox(aParent: PWindowsObject); virtual;
    procedure AlignBoxPrim;
    procedure DefineBox(aParent: PWindowsObject); virtual;
    procedure DefineBoxPrim;
    function DeleteBox(aParent: PWindowsObject): boolean; virtual;
    procedure DeleteBoxPrim;
    procedure ResetDefaults; virtual;

    {- load and save functions }
    function SavePrep: boolean; virtual;
    function SaveData(var dataSeq: longint): boolean; virtual;
    procedure SaveComplete; virtual;
    function LoadData(aBoxSeq: longint): boolean; virtual;

    function GetParentHWnd: HWnd; virtual;
    function GetParentObj: PWindowsObject; virtual;
    function GetBoxType: integer; virtual;

    private
    LTpPX    : integer;  {- logical twips per pixel }
    LTpPY    : integer;  {- used for LTwip to pixel conversions }
    org      : TLPoint;  {- origin of section (LTwips) relative to page }
    ext      : TLPoint;  {- extent of section (LTwips) }
    rFont    : TRptFont; {- font for window display }
    zoom     : real;     {- zoom factor, passed from parent }
    pageOrg  : TPoint;   {- origin of parent page (device units) }
    page     : TRptPage;
  end;

  {- TBoxDlg is provided for use in dialog boxes. It is used by TPageDlg }
  PBoxDlg = ^TBoxDlg;
  TBoxDlg = object(TBoxWin)
    procedure WMGetDlgCode(var msg: TMessage); virtual WM_FIRST + WM_GETDLGCODE;
    procedure WMKeyDown(var msg: TMessage); virtual WM_FIRST + WM_KEYDOWN;
    procedure WMChar(var msg: Tmessage); virtual WM_FIRST + WM_CHAR;
    procedure NCLButtonDown(var msg: TMessage); virtual WM_FIRST + WM_NCLBUTTONDOWN;
    procedure NCRButtonDown(var msg: TMessage); virtual WM_FIRST + WM_NCRBUTTONDOWN;
  end;

IMPLEMENTATION

constructor TBoxWin.Init(AParent: PWindowsObject; ATitle: PChar;
                          rect: TRect; AnOrigin: TPoint; AZoom: real;
                          APage: TRptPage; AFont: TRptFont);
{- rect  - device coordinates for the initial size of section box}
begin
  inherited Init(AParent, ATitle);
  SetFlags(WB_MDICHILD, false);
  attr.style:= WS_CHILD or WS_VISIBLE or WS_CLIPSIBLINGS or WS_TABSTOP;

  attr.x:= rect.left;
  attr.y:= rect.top;
  attr.w:= rect.right - rect.left;
  attr.h:= rect.bottom - rect.top;

  pageOrg:= AnOrigin;
  zoom:= AZoom;
  ChangePage(APage);
  focused:= true;
  rFont:= AFont;
  boxSeq:= -1;
  inchange:= false;
  ResetDefaults;
end;

destructor TBoxWin.Done;
{- post a box closing message to parent so it can clean up }
begin
  SendMessage(GetParent(HWindow), UM_BOXCLOSING, HWindow, longint(@self));
  inherited Done;
end;

procedure TBoxWin.SetupWindow;
var
  dc    : HDC;
begin
  inherited SetupWindow;

  {- force to top of ZOrder }
  SetWindowPos(HWindow, HWND_TOP, 0,0,0,0, SWP_NOMOVE or SWP_NOSIZE);

  {- get device dependent constants }
  dc:= GetDC(HWindow);
  SetLogicalTwips(dc, 1.0);
  LTwipsPerPixel(dc, LTpPX, LTpPY);
  ReleaseDC(HWindow, dc);

  {- set org and ext for the window (in LTwips) }
  { org is distance from page origin in LTwips. Remember to apply the
    zoom factor when converting pixels }
  org.x:= Trunc((attr.x - pageOrg.x) / zoom) * LTpPX;
  org.y:= Trunc((attr.y - pageOrg.y) / zoom) * LTpPY;

  {- ext is width and height of window in LTwips. Remember to apply the
     zoom factor when converting pixels }
  ext.x:= Round(attr.w / zoom) * LTpPX;
  ext.y:= Round(attr.h / zoom) * LTpPY;

  SetFocus(HWindow);
end;

function TBoxWin.GetClassName: PChar;
begin
  GetClassName:= 'RCFBoxWin';
end;

procedure TBoxWin.GetWindowClass(var AWndClass: TWndClass);
begin
  inherited GetWindowClass(AWndClass);
end;

procedure TBoxWin.ChangeOrigin(newOrigin: TPoint);
{- called by parent when page origin changes position }
{- newOrigin is in device pixels }
begin
  pageOrg:= newOrigin;
  {- recalculate distance from page origin }
  org.x:= Trunc((attr.x - pageOrg.x) / zoom) * LTpPX;
  org.y:= Trunc((attr.y - pageOrg.y) / zoom) * LTpPX;
  SendMessage(GetParent(HWindow), UM_BOXSIZEMOVE, HWindow, longint(@self));
  InvalidateRect(HWindow, nil, true);
end;

procedure TBoxWin.ChangeZoom(newOrigin: TPoint; AZoom: real);
{- parent windows page has changed the origin (or zoom), this method updates
   internal structures to reflect the change }
var
  pt : TLPoint;
  pt2: TLPoint;
begin
  pageOrg:= newOrigin;  {- save new values }
  zoom:= AZoom;

  {- calculate new window position, converting from LTwips }
  pt.x:= Trunc((org.x / LTpPX) * zoom);
  pt.y:= Trunc((org.y / LTpPY) * zoom);

  {- window size remains the same, convert LTwips to pixels }
  pt2.x:= Round((ext.x / LTpPX) * zoom);
  pt2.y:= Round((ext.y / LTpPY) * zoom);

  inChange:= true;
  MoveWindow(HWindow, pageOrg.x + pt.x, pageOrg.y + pt.y, pt2.x, pt2.y, true);
  inChange:= false;
end;

procedure TBoxWin.WMRbuttonDown(var msg: TMessage);
{- send a right mouse button down message to parent }
begin
  SendMessage(GetParent(HWindow), UM_RIGHTMOUSE, HWindow, msg.lParam);
  msg.result:= 0;
end;

procedure TBoxWin.WMLbuttonDown(var msg: TMessage);
{- set the focus to the box on left mouse button click }
begin
  SetFocus(HWindow);
end;

procedure TBoxWin.WMLbuttonDblClk(var msg: TMessage);
{- respond to double-clicks by defining the box }
begin
  DefineBoxPrim;
end;

procedure TBoxWin.NCHitTest(var msg: TMessage);
{- return HTCAPTION regardless of mouse pos so user can move window by grabbing
   anywhere with the mouse.}
var
  x, y  : integer;
  r     : TRect;
begin
  GetWindowRect(HWindow, r);
  x:= msg.lParamLo;
  y:= msg.lParamHi;
  if x <= r.left+4 then
    msg.Result:= HTLEFT
  else if x >= r.right - 4 then
    msg.Result:= HTRIGHT
  else if y >= r.bottom - 4 then
    msg.Result:= HTBOTTOM
  else if y <= r.top + 4 then
    msg.result:= HTTOP
  else
    msg.result:= HTCAPTION
end;

procedure TBoxWin.NCLButtonDown(var msg: TMessage);
begin
  SetFocus(HWindow);
  DefWndProc(msg);
end;

procedure TBoxWin.NCRButtonDown(var msg: TMessage);
{- capture and pass on NCRButtons to WMRButtons }
begin
  SetFocus(HWindow);
  if msg.wParam = HTCAPTION then
  begin
    SendMessage(HWindow, WM_RBUTTONDOWN, 0, msg.lParam);
    msg.result:= 0;
  end
  else
    DefWndProc(msg);
end;

procedure TBoxWin.NCLButtonDblClk(var msg: TMessage);
{- capture and pass on NC DBl clicks to WM dblclicks}
begin
  if msg.wParam = HTCAPTION then
    PostMessage(HWindow, WM_LBUTTONDBLCLK, 0, msg.lParam)
  else
    DefWndProc(msg);
end;

procedure TBoxWin.WMMove(var msg: TMessage);
{- set internal positions and tell parent that box has moved }
begin
  inherited WMMove(msg);
  if not inChange then
  begin
    org.x:= Trunc((attr.x - pageOrg.x) / zoom) * LTpPX;
    org.y:= Trunc((attr.y - pageOrg.y) / zoom) * LTpPY;
    SendMessage(GetParent(HWindow), UM_BOXSIZEMOVE, HWindow, longint(@self));
(*    InvalidateRect(HWindow, nil, false);*)
  end;
end;

procedure TBoxWin.WMSize(var msg: TMessage);
{- set internal positions and tell parent that box has sized }
begin
  inherited wmSize(msg);
  if not inChange then
  begin
    ext.x:= Round(attr.w / zoom) * LTpPX;
    ext.y:= Round(attr.h / zoom) * LTpPY;
    SendMessage(GetParent(HWindow), UM_BOXSIZEMOVE, HWindow, longint(@self));
  end;
end;

function TBoxWin.BuildBoxFont(aLogFont: TLogFont): HFont;
{- build the font for the box based on the logical font passed }
begin
  BuildBoxFont:= CreateFontIndirect(aLogFont);
end;

procedure TBoxWin.Paint(dc: HDC; var ps: TPaintStruct);
var
  r         : TRect;
  num       : real;
  orgPt     : TPoint;  {- parent page origin (LTwips) }
  oldF      : HFont;
  oldP      : HPen;
  oldC      : TColorRef;
begin
  {- set border before changing mapping mode, its easier this way! }
  GetClientRect(HWindow, r);
  if focused then {- draw rectangle in RED if focused }
    oldP:= SelectObject(dc, CreatePen(PS_SOLID, 1, RGB($FF,0,0)));
  Rectangle(dc, 0, 0, r.right, r.bottom);
  if focused then
    DeleteObject(SelectObject(dc, oldP));

  SetLogicalTwips(dc, zoom);  {- set LTwips mode w/zoom }

  oldf:= SelectObject(dc, BuildBoxFont(rFont.lf));

  SetBkMode(dc, transparent);
  oldC:= SetTextColor(dc, rFont.color);

  DrawCaption(dc);

  SetTextColor(dc, oldC);
  DeleteObject(SelectObject(dc, oldF));
end;

procedure TBoxWin.SetDisplayFont(AFont: TRptFont);
begin
  rFont:= AFont;
  rFont.lf.lfHeight:= -Trunc(rFont.ptSize div 10 * 20);
  InvalidateRect(HWindow, nil, true);
end;

procedure TBoxWin.DrawCaption(dc: HDC);
{- default section window title }
var
  r   : TRect;
  pstr: PChar;
  k   : integer;
begin
  r.top:= 0;
  r.bottom:= ext.y - r.top;
  r.left:= LTpPX * 2;
  r.right:= ext.x - r.left;

  k:= GetNameLength;
  GetMem(pstr, k + 1);
  GetName(pstr, k);
  DrawText(dc, pstr, strlen(pstr), r, DT_CENTER or DT_WORDBREAK or DT_NOPREFIX);
  FreeMem(pstr, k + 1);
end;

procedure TBoxWin.BringToTop;
begin
  SetWindowPos(HWindow, HWND_TOP, 0,0,0,0, SWP_NOMOVE or SWP_NOSIZE);
end;

procedure TBoxWin.WMMinMax(var msg: Tmessage);
{- control maximum size of section box }
var
  maxX : integer;
  pInfo: PMinMaxInfo;
begin
  DefWndProc(msg);
  pInfo:= PMinMaxInfo(msg.lParam);
  with page do
  begin
    pInfo^.ptMaxTrackSize.x:= Trunc(((size.X - (margin.left+margin.right)) / LTpPX) * zoom);
    pInfo^.ptMaxTrackSize.y:= Trunc(((size.Y - (margin.top+margin.bottom)) / LTpPY) * zoom);
  end;
end;

procedure TBoxWin.WMSetFocus(var msg: TMessage);
{- pass size/move message to parent so it will update the status bar }
var
  pstr : array[0..100] of char;
begin
  focused:= true;
  InvalidateRect(HWindow, nil, false);
  SetWindowPos(HWindow, HWND_TOP, 0,0,0,0, SWP_NOMOVE or SWP_NOSIZE);
  SendMessage(GetParent(HWindow), UM_BOXSIZEMOVE, HWindow, longint(@self));
  msg.result:= 0;
end;

procedure TBoxWin.WMKillFocus(var msg: TMessage);
begin
  focused:= false;
  InvalidateRect(HWindow, nil, false);
  msg.Result:= 0;
end;

procedure TBoxWin.WMChar(var msg: TMessage);
begin
  if msg.wParam = ord('-') then
    SetWindowPos(HWindow, HWND_BOTTOM, 0,0,0,0, SWP_NOMOVE or SWP_NOSIZE)
  else if msg.wParam = ord('+') then
    SetWindowPos(HWindow, HWND_TOP, 0,0,0,0, SWP_NOMOVE or SWP_NOSIZE)
end;

procedure TBoxWin.WMKeyDown(var msg: Tmessage);
{- pass key downs to parent }
begin
  SendMessage(GetParent(HWindow), WM_KEYDOWN, msg.wParam, msg.lParam);
end;

procedure TBoxWin.GetSize(var ASize: TLRect);
{- return origin (from paper org) and extent of section in LTwips.
   aSize.left/top = origin
   aSize.right/bottom = extent }
begin
  ASize.left:= org.x;
  ASize.top:= org.y;
  ASize.right:= ext.x;
  ASize.bottom:= ext.y;
end;

procedure TBoxWin.SetSize(ASize: TLRect);
{- set origin (from paper org) and extent of section in LTwips }
var
  pt, pt2  : TPoint;
begin
  org.x:= ASize.left;
  org.y:= ASize.top;
  ext.x:= ASize.right;
  ext.y:= ASize.bottom;

  pt.x:= Trunc((org.x / LTpPX) * zoom);
  pt.y:= Trunc((org.y / LTpPY) * zoom);

  {- window size remains the same, convert LTwips to pixels }
  pt2.x:= Round((ext.x / LTpPX) * zoom);
  pt2.y:= Round((ext.y / LTpPY) * zoom);

  inChange:= true;
  MoveWindow(HWindow, pageOrg.x+pt.x, pageOrg.y+pt.y, pt2.x, pt2.y, true);
  inChange:= false;
  SendMessage(GetParent(HWindow), UM_BOXSIZEMOVE, HWindow, longint(@self));
end;

procedure TBoxWin.ChangePage(ANewPage: TRptPage);
{- change paper size (and orientation) }
begin
  page:= ANewPage;
end;

function TBoxWin.GetNameLength: integer;
{- return number of chars that getname will return. Does not include the
   termincating null }
begin
  GetNameLength:= SendMessage(HWindow, WM_GETTEXTLENGTH, 0,0);
end;

procedure TBoxWin.GetName(AName: PChar; maxLen: integer);
{- return display name of section. Default returns section caption }
begin
  GetWindowText(HWindow, AName, maxLen+1);
end;

procedure TBoxWin.AlignBoxPrim;
begin
  Alignbox(GetParentObj);
end;

procedure TBoxWin.AlignBox(aParent: PWindowsObject);
var
  r    : TLRect;
  pstr : array[0..100] of char;
  p1, p2: array[0..25] of char;
begin
  GetSize(r);
  GetName(pstr, 100);
  if GetAlignment(aParent, SR(IDS_ALIGN, p1, 25), SR(IDS_BOX, p2, 25), pstr, r) then
    SetSize(r);
end;

procedure TBoxWin.DefineBox(aParent: PWindowsObject);
{- method that should invoke the section editor for the section.
   the default section editor does nothing. You should respond
   to this method appropriately for the type of section }
var
  p1, p2    : array[0..100] of char;
begin
  InfoMsg(aParent^.HWindow, SR(IDS_DEFINE, p1, 100), SR(IDS_DEFINEBOX, p2, 100));
end;

procedure TBoxWin.DefineBoxPrim;
begin
  DefineBox(GetParentObj);
end;

procedure TBoxWin.ResetDefaults;
{- reset definition of section to defaults. Called when a box is first created
   and when the user clears the box of data }
begin
  {- does nothing }
end;

function TBoxWin.SavePrep: boolean;
{- this function is called to tell the box object to prepare for saving of data.
   That means that descendants must open files. After the owner calls this
   function, it will then begin a transaction (calling SaveData for all box
   objects. This function should return false if it cannot prepare to save.
   This function is called *BEFORE* a save transaction is started! }
begin
  SavePrep:= true;
end;

procedure TBoxWin.SaveComplete;
{- this routine will be the last called in the save process.  It should be used
   to clean up after the save (close files, etc.) }
begin
end;

function TBoxWin.SaveData(var dataSeq: longint): boolean;
{- This routine is called to save the data for the box.  The box is responsible
   for all of its data (format, location, file name etc). The only thing the
   box is required to do, is to return a unique sequence number so the report
   generator can create a cross reference file for the report format. This
   sequence number probably should be the seq of the record in which the box
   data was stored. If the function is unable to save the data or an error
   occurs, it should return false.  Returning false will abort the save process
   and force the report generator to call abort save for all boxes.

   The report generator will call the sections delete function to clear any old
   data that may exist so this function can assume that it always should insert
   new records. }
begin
  {- default does nothing }
  dataSeq:= -1;
  SaveData:= true;
end;

function TBoxWin.LoadData(aBoxSeq: longint): boolean;
begin
  boxSeq:= aBoxSeq;
  LoadData:= true;
end;

function TBoxWin.GetParentHWnd: HWnd;
{- return the of the main parent window containing the page and boxes }
begin
  GetParentHWnd:= GetParent(GetParent(HWindow));
end;

function TBoxWin.GetParentObj: PWindowsObject;
{- return a pointer to the main parent object }
begin
  GetParentObj:= parent^.parent;
end;

function TBoxWin.DeleteBox(aParent: PWindowsObject): boolean;
{- called when the user attempts to delete the box }
var
  p2, pstr  : array[0..200] of char;
begin
  GetName(pstr, 200);
  SR(IDS_REMOVE, p2, sizeof(p2)-1);
  StrCat(p2, #10#10);
  StrCat(p2, pstr);
  StrCat(p2, '?');
  If YesNoMsg(aParent^.HWindow, SR(IDS_CONFIRM, pstr, sizeof(pstr)-1), p2) then
  begin
    CloseWindow;
    boxSeq:= -1;
    DeleteBox:= true;
  end
  else
    DeleteBox:= false;
end;

procedure TBoxWin.DeleteBoxPrim;
begin
  DeleteBox(GetParentObj);
end;

function TBoxWin.GetBoxType: integer;
{- this function is used to identify the box type. This is used by descendents
   to identify the type of section the box represents }
begin
  GetBoxType:= 0;
end;

{-------------------------------------------------------[ TBoxDlg ]--}

procedure TBoxDlg.WMGetDlgCode(var msg: TMessage);
begin
  msg.result:= DLGC_WANTMESSAGE {or DLGC_UNDEFPUSHBUTTON};
end;

procedure TBoxDlg.WMChar(var msg: TMessage);
{- pass along WM_CHAR to the parent window }
begin
  SendMessage(parent^.HWindow, WM_CHAR, msg.wParam, msg.lParam);
end;

procedure TBoxDlg.WMKeyDown(var msg: TMessage);
{- pass along WM_KEYDOWN to the parent window }
begin
  SendMessage(parent^.HWindow, WM_KEYDOWN, msg.wParam, msg.lParam);
end;

procedure TBoxDlg.NCLButtonDown(var msg: TMessage);
{- force focus to parent window }
begin
  SendMessage(GetParentHWnd, WM_NEXTDLGCTL, HWindow, 1);
  DefWndProc(msg);
end;

procedure TBoxDlg.NCRButtonDown(var msg: TMessage);
{- force focus to parent window }
begin
  SendMessage(GetParentHWnd, WM_NEXTDLGCTL, HWindow, 1);
  if msg.wParam = HTCAPTION then
    SendMessage(HWindow, WM_RBUTTONDOWN, 0, msg.lParam)
  else
    DefWndProc(msg);
end;


END.

{- rcf }
