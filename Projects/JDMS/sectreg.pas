unit SectReg;

{- Section Registration Module }

{- adding sections requires the following:

1 Create a unique ID for the section. This id will be used when section is
  registered. IDs are in RPTSHARE.PAS.

2 Create a descendent from TBoxWin that will be used to represent the section
  box on the report gen screen. The methods to overwrite are Define, Save and Restore

3 Register a function to create an instance of the section window in step 1 and
  to delete data for the section (see below).

4 Create a dialog or window object to be called up when the sections (created in
  step 1) definebox method is called. This object will be responsible for
  defining the section.

5 Add the unit name to the uses clause in the RPTEDIT.PAS module. }


INTERFACE

uses
  BoxWin,
  RptShare,
  WinTypes,
  ODialogs,
  DlgLib,
  Objects,
  OWindows;

const
  MaxSections     = 10;   {- maximum number of section types available }

  ACTION_OPEN     = $0001;  {- actions that are passed to section delete function}
  ACTION_DELETE   = $0002;  {- These are used to tell the function the actions to }
  ACTION_CLOSE    = $0004;  {- perform. This functionality is used to accomodate }
                            {- Btrieve transactions. }

type
  {- this function describes the section initialization function.
     All section drivers must provide a function of this type that will
     initialize an instance of that section }
  TSectionInitFunc = function(aParent: PWindowsObject; r: TRect; origin: TPoint;
                              aZoom: Real; page: TRptPage; aFont: TRptFont): PBoxWin;

  {- This function type describes a function that will be called to delete a
     section from the database.  The report generator will call this function
     to delete all section data for the specified report sequence number and
     the section sequence number. See ACTION_xxx constants above for action param}
  TSectionDeleteFunc = function(action: integer; aRptSeq, aSectSeq: longint): boolean;

  TRegSect = record
    initFunc  : TSectionInitFunc; {- section box initialization function pointer }
    deleteFunc: TSectionDeleteFunc; {- section deletion function }
    name      : PChar;            {- section name (for list boxes) }
    id        : integer;          {- unique section ID }
  end;

  PSections = ^TSections;
  TSections = object(TObject)
    sects    : array[1..MaxSections] of TRegSect;   {- read/only !}
    constructor Init;
    destructor Done; virtual;
    function TotalSections: integer;
    function RegisterSection(anInitFunc: TSectionInitFunc;
                             aDelFunc: TSectionDeleteFunc;
                             aName: PChar; anID: integer): boolean;
    function IDXFromID(id: integer): integer;
    private
    numSects : integer;
  end;

  {- provide a list of all registered sections. Return the index of the
     chosen section }
  PGetSection = ^TGetSection;
  TGetSection = object(TCenterDlg)
    choice   : ^Integer;
    list     : PListBox;
    constructor Init(AParent: PWindowsObject; var AChoice: Integer);
    procedure SetupWindow; virtual;
    procedure WMCommand(var msg: TMessage); virtual WM_FIRST + WM_COMMAND;
    function CanClose: Boolean; virtual;
  end;

var
  sections  : PSections;

IMPLEMENTATION

uses
  DMSDebug,
  MScan,
  CtlLib,
  APITools,
  DMSerr,
  Strings,
  WinProcs;

{$I DMSTRING.INC}
{$I RPTGEN.INC}

{-------------[ Section Registration ]--}


constructor TSections.Init;
begin
  inherited Init;
  numSects:= 0;
  FillChar(sects, sizeof(sects), 0);
end;

destructor TSections.Done;
var
  k  : integer;
begin
  inherited Done;
  for k:= 1 to numSects do
    MSStrDispose(sects[k].name);
end;

function TSections.TotalSections: integer;
begin
  TotalSections:= numSects;
end;

function TSections.RegisterSection(anInitFunc: TSectionInitFunc;
                                   aDelFunc: TSectionDeleteFunc;
                                   aName: PChar; anID: integer): boolean;
var
  k    : integer;
  pstr : array[0..100] of char;
  p2   : array[0..100] of char;
begin
  if numSects < MaxSections then
  begin
    {- see if section ID already exists, if so, halt }
    k:= 1;
    while (k <= numSects) do
    begin
      if sects[k].ID = anID then
      begin
        SR(IDS_DUPSECTID, pstr, sizeof(pstr)-1);
        strcat(pstr, ' [ ');
        str(anID, p2);
        strcat(pstr, p2);
        strcat(pstr, ' ]');
        MessageBox(0, pstr, SR(IDS_GETRPTSECT, p2, 100), MB_SYSTEMMODAL);
        Halt(255);
      end;
      Inc(k);
    end;

    RegisterSection:= true;
    Inc(numSects);
    sects[numSects].name:= StrNew(aName);
    sects[numSects].initFunc:= anInitFunc;
    sects[numSects].deleteFunc:= aDelFunc;
    sects[numSects].id:= anID;
  end
  else
    RegisterSection:= false;
end;

function TSections.IDXFromID(id: integer): integer;
{- return a section's reletive index from it's ID number that it was registered
   under. Returns 0 if no section has been registered under the specified ID }
var
  k     : integer;
  found : boolean;
begin
  k:= 1;
  found:= false;
  while not found and (k <= numSects) do
  begin
    found:= (sects[k].id = id);
    if not found then
      Inc(k);
  end;
  if found then
    IDXFromID:= k
  else
    IDXFromID:= 0;
end;

{----------------------------------------[ TGetSection ]--}

constructor TGetSection.Init(AParent: PWindowsObject; var AChoice: Integer);
var
  c  : PControl;
begin
  inherited Init(AParent, MAKEINTRESOURCE(DLG_ADDSECTION));
  list:= New(PListBox, InitResource(@self, IDC_LIST));
  c:= New(PLStatic, InitResource(@self, 300, 0, false));
  choice:= @achoice;
end;

procedure TGetSection.SetupWindow;
var
  k   : Integer;
begin
  inherited SetupWindow;
  for k:= 1 to sections^.TotalSections do
  begin
    list^.AddString(sections^.sects[k].name);
  end;
  list^.SetSelIndex(0);
end;

procedure TGetSection.wmCommand(var msg: TMessage);
begin
  if msg.wParam = list^.GetID then
  begin
    if msg.lParamHi = LBN_DBLCLK then
      OK(msg)
    else
      inherited wmCommand(msg);
  end
  else
    inherited wmCommand(msg);
end;

function TGetSection.CanClose: Boolean;
begin
  CanClose:= false;
  if list^.GetSelIndex = -1 then
    ShowError(@self, IDS_MUSTMAKESEL, nil, 0, MOD_RPTGEN, 0)
  else
  begin
    choice^:= list^.GetSelIndex + 1;
    CanClose:= true;
  end;
end;

{--------[ Private initialization ]--}

var
  saveExit  : Pointer;

procedure BoxExit; far;
begin
  exitProc:= SaveExit;
  MSDisposeObj(sections);
end;

BEGIN
  saveExit:= exitProc;
  exitProc:= @BoxExit;
  sections:= New(PSections, Init);
END.

{- rcf }
