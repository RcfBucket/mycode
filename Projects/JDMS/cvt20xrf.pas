unit Cvt20Xrf;

INTERFACE

uses
  Objects,
  DBTypes;

type
  POrder = ^TOrder;
  TOrder = object(TObject)
    orderSeq  : TSeqNum;
    grpSeq    : TSeqNum;
    origProc  : array[0..10] of char;
    isoID     : array[0..5] of char;
    grpID     : array[0..10] of char;
    orderID   : array[0..10] of char;
    ordDesc   : array[0..35] of char;
    grpDesc   : array[0..35] of char;
    aSet      : byte;
    constructor Init;
  end;

  PBatID = ^TBatID;
  TBatID = object(TObject)
    orders  : PCollection;
    batID   : array[0..10] of char;
    constructor Init(aBatID: PChar);
    destructor Done; virtual;
    procedure AddAnOrder(aOrderSeq, aGrpSeq: TSeqNum; aProc, aGrpID: PChar);
  end;

var
  batIDS    : PCollection;  {- battery ID xreference }

IMPLEMENTATION

uses
  Strings;

var
  exitSave  : pointer;

constructor TOrder.Init;
begin
  inherited Init;
  orderSeq:= 0;
  grpSeq:= 0;
  StrCopy(origProc, '');
  StrCopy(isoID, '');
  StrCopy(orderID, '');
  StrCopy(grpID, '');
  StrCopy(ordDesc, '');
  StrCopy(grpDesc, '');
end;

constructor TBatID.Init(aBatID: PChar);
begin
  inherited Init;
  orders:= New(PCollection, Init(25, 25));
  StrLCopy(batID, aBatID, sizeof(batID)-1);
end;

destructor TBatID.Done;
begin
  inherited Done;
  Dispose(orders, Done);
end;

procedure TBatID.AddAnOrder(aOrderSeq, aGrpSeq: TSeqNum; aProc, aGrpID: PChar);
var
  o   : POrder;
begin
  o:= New(POrder, Init);
  o^.orderSeq:= aOrderSeq;
  o^.grpSeq:= aGrpSeq;
  StrCopy(o^.origProc, aProc);
  StrCopy(o^.grpID, aGrpID);
  orders^.Insert(o);
end;

procedure ExitRoutine; far;
begin
  exitProc:= exitSave;
  Dispose(batIDS, Done);
end;

BEGIN
  batIDs:= New(PCollection, Init(25, 25));
  exitSave:= exitProc;
  exitProc:= @ExitRoutine;
END.

{- rcf }
