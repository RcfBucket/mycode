unit CustUtil;

{-------------------------------------------------------------------------}
{ This unit provides access to the therapy.dat file. It also              }
{ provides some other functionality unique to CustInterps                 }
{                                                                         }
{-------------------------------------------------------------------------}
{                                                                         }
{ This module provides an interface to the old code (version 19.xx), in   }
{ an attempt to isolate the interp editor from having to directly access  }
{ the therapy file.                                                       }
{                                                                         }
{-------------------------------------------------------------------------}
{ Who  When     What                                                      }
{ ---- -------- --------------------------------------------------------- }
{ RCF  10/25/94 Created module                                            }
{-------------------------------------------------------------------------}

INTERFACE

uses
  Interps,
  Trays,
  Objects,
  MScan;

type
  TPanelGroup = record
    numInGroup   : integer;  {- number of panels in group }
    headDRN      : integer;  {- DRN of head therapy table }
    pnls         : array[1..MaxTray] of integer;
  end;

  TTherRec = record
    numThers : integer;                  {- number of therapy entries }
    txtIdx   : integer;                  {- exception text index }
    drn      : integer;                  {- the DRN where the record was read from }
    MDIs     : array[1..MaxMDI] of byte; {- MDI values from the therapy record }
    intrps   : array[1..MaxMDI] of byte; {- actual blood/urine nibble pair }
    modified : boolean;                  {- set to true if changed since loaded from disk }
    fileModified : boolean;              {- if true, record was previously modified }
  end;

  TTherInfo = record
    numRecs  : integer; {- number of therapy records }
    therRecs : array[1..MaxExc] of TTherRec;
  end;

  TPanelGroupInfo = record
    ADN        : integer;   {- drug ADN }
    numGroups  : integer;   {- number of groups for an ADN }
    group      : array[1..MaxTray] of TPanelGroup;  {- group information }
    therInfo   : TTherInfo;  {- therapy info for a group, Load therapy info on demand, v4.00, ghg 8/3/98 }
  end;

  TLocalTrayRec = record
    id      : idType;
    sup     : SupType;
    grp     : byte;
    drgCnt  : byte;
    drugDrn : array[1..MaxDrugs] of DrnType;
    adnTbl  : array[1..44] of byte;
    filler  : array[1..64] of byte;     { Supports ExcType change, ghg, v4.00}
  end;

 {- this is the interface to the therapy and tray files. All access will be
     through this object to centeralize the old stuff in one area }
  PCustIntr = ^TCustIntr;
  TCustIntr = object(TObject)
    curADN     : integer;   {- current ADN }
    grpInfo    : TPanelGroupInfo;  {- all panel group information }
    mdaTbl     : MastDilArray;
    validDils  : array[1..MaxTray, 0..MaxMDI] of boolean;
    {}
    constructor Init;
    destructor Done; virtual;

    procedure BuildPanelGroups(drgADN: Integer);
    function SaveChanges: integer;

    function IsTherModified(aGroup, aTherRec: integer): boolean;
    function IsAnyTherModified: boolean;
    function IsFileModified(aGroup, aTherRec: integer): boolean;

    function DrugAbbr: string;
    function CurrentADN: Integer;

    function NumPanelGroups: integer;
    function PanelsInGroup(grp: integer): integer;
    procedure PanelName(grp, idx: integer; name: PChar; maxLen: integer);
    function PanelNumber(grp, idx: integer): integer;
    function PanelHeadDRN(grp: integer): integer;

    function NumExecpts(grp: integer): integer;
    procedure ExceptName(grp, idx: integer; name: PChar; maxLen: integer);

    procedure LoadTherInfo(grp: Integer);
    function TotalDrugADNs: integer;
    function InterpVal(blood: Boolean; value: Byte): Integer;
    procedure InterpValStr(blood: boolean; interp: byte; st: PChar; maxLen: integer);
    function MergeInterp(blood: boolean; oldVal, newVal: byte): byte;
    function GetTherapy(aGroup, aTherRec, aDilStep: integer): byte;
    procedure SetTherapy(aGroup, aTherRec, aDilStep: integer; aTher: byte);
    {}
    private
    CustIntrTray       : PTraysObject;
    intrps     : PInterpsObject;

    {- functions from old platform }
    function GoodTray(num: Integer; var tBuf:TLocaltrayrec): boolean;
    function CalcTryDRN(tType, age: integer): integer;
    function DrugOnTray(adn: Integer; var rdn: integer; var tBuf:TLocalTrayRec): integer;
    procedure AddPnlGrp(drn, rdn, pnl: Integer);
    procedure SetVDils(pnl, rdn, GrpNum: integer);
  end;

IMPLEMENTATION

uses
  StrsW,
  ApiTools,
  Strings,
  Bits,
  WinProcs,
  IntrpMap,
  WinTypes;

{$I CUSTINTR.INC}

type
  IdType     = byte;
  SupType    = byte;
  AdnType    = byte;
  DrnType    = integer;
  ExcType    = integer;
  TherType   = byte;

  TPnlGroup = record
    case Integer of
      1:(therDRN : integer;               {- therapy head rec DRN }
         pnls    : array[1..13] of byte); {- panel bitmap panels using therapy}
      2:(numGrps   : Integer);
  end;
  TPnlList = array[0..MaxTray] of TPnlGroup;

  TChar16 = array[0..16] of char;
  TPanelNames = array[1..MaxTray] of TChar16;

procedure TherDescr(idx: Integer; var s: string);
{- return a string describing the therapy record }
var
   tstr   : string[80];
   tstr2  : string[80];
   pstr: array[0..200] of char;
begin
  Str(idx, tstr);
  tstr:= '(' + tstr + ') ';
   if idx > 0 then
     LoadString(HInstance, IDS_THERDESC + idx - 1, pstr, 200);
   s:= tstr + StrPas(pstr);
end;

constructor TCustIntr.Init;
begin
  inherited Init;
  CustIntrTray:= New(PTraysObject, Init);
  intrps:= New(PInterpsObject, Init(CustIntrTray));
  BuildPanelGroups(1);  {- init with panel groups for ADN 1 }

  {- build a custom version of the master dilution array }
  {- that include '>' }
  mdaTbl:= CustIntrTray^.gMDA;
  mdaTbl[MaxMDI]:= ' >  ';
end;

destructor TCustIntr.Done;
begin
  inherited Done;
  MSDisposeObj(intrps);
  MSDisposeObj(CustIntrTray);
end;

function TCustIntr.GoodTray(num: Integer; var tBuf:TLocalTrayRec): Boolean;
{- return true if tray is valid for this program. That is, the tray is
   not Obsolete and there are drugs on the tray (non-ID) }
Begin
   CustIntrTray^.LoadTrayByNum(num);
   GoodTray:= not CustIntrTray^.TrayObsolete and (tbuf.drgcnt > 0);
End;

function TCustIntr.CalcTryDRN(tType, age: integer): integer;
{- extracted from old DMS. Calculates a DRN based on tray type
   and age }
begin
  CalcTryDrn:= ((tType - 1) * (MaxAge + 1) + age + offset + 1);
end;

function TCustIntr.DrugOnTray(adn: Integer; var rdn: integer; var tBuf:TLocalTrayRec): integer;
{- return whether or not adn is on the tray. return drug rec DRN if found
   otherwise return 0 }
var
  found : Boolean;
begin
  rdn:= 1;
  found:= false;
  with tbuf do
  begin
    while (rdn <= drgcnt) and (not found) do
    begin
      if (adntbl[rdn] = adn) then
        found:= True
      else
        rdn:= rdn + 1;
    end;
    if found then
      DrugOnTray:= drugDRN[rdn]
    else
      DrugOnTray:= 0;
  end;
end;

procedure TCustIntr.BuildPanelGroups(drgADN: Integer);
{- build panel groups for a particular drug ADN. This is the main
   routine. It should be called before any other methods to load
   the panel groups and therapy records into memory for the passed
   ADN }
var
  rdn      : integer;
  trayNum  : integer;
  grpDRN   : integer;   {- use therapy record DRN as group ID }
  tBuf     : TLocalTrayRec;
  drn      : integer;
  k        : integer;
  p1, p2   : array[0..100] of char;
begin
  if (drgADN > 0) and (drgADN <= TotalDrugADNs) then
  begin
    curADN:= drgADN;
    FillChar(grpInfo, SizeOf(grpInfo), 0);
    FillChar(ValidDils, Sizeof(ValidDils), 0);
    for trayNum:= 1 to CustIntrTray^.gTrayMax do
    begin
      drn:= CalcTryDrn(trayNum, 0);
      {- get the tray record }
      if intrps^.TherRecRead(drn, @tBuf, TryRecType) then
      begin
        {- is it a valid tray (panel) }
        if GoodTray(trayNum, tBuf) Then
        begin
          {- is the specified drug on the panel ? }
          grpDRN:= DrugOnTray(drgADN, rdn, tbuf);
          if grpDRN > 0 then {- add the panel to the group list }
            AddPnlGrp(grpDRN, rdn, TrayNum);
        end;
      end;
    end;
    for k:= 1 to NumPanelGroups do
      LoadTherInfo(k);
  end
  else
  begin
    StrCopy(p1, 'Invalid ADN passed to BuildPanelGroups: ');
    Str(drgADN, p2);
    StrCat(p1, p2);
    FatalError('Fatal Error', p1);
    Halt;
  end;
end;

procedure TCustIntr.AddPnlGrp(drn, rdn, pnl: Integer);
{- add pnl num to group. If already in list then just set panel bit to
   indicate the inclusion of panel in the group }
var
   GrpNum    : Integer;
   found     : Boolean;
begin
  GrpNum:= 1;
  found:= False;

  {- see if DRN is already defined }
  while (grpNum <= grpInfo.numGroups) and not found do
  begin
    if grpInfo.group[grpNum].headDRN = drn then
      found:= true
    else
      Inc(grpNum);
  end;
  if not found then
  begin
    Inc(grpInfo.numGroups);
    with grpInfo.group[grpInfo.numGroups] do
    begin
      numInGroup:= 1;
      headDRN:= drn;
      pnls[1]:= pnl;
    end;
  end
  else
  begin
    with grpInfo.group[grpNum] do
    begin
      Inc(numInGroup);
      pnls[numInGroup]:= pnl;
    end;
  end;
  SetVDils(pnl, rdn, GrpNum);
end;

procedure TCustIntr.SetVDils(pnl, rdn, GrpNum: integer);
{ Set the valid dilutions for a panel group }
var
  DilStep     : integer;
  TmpMdi      : integer;
begin
  CustIntrTray^.LoadTrayByNum(pnl);
  for DilStep:= 1 to (CustIntrTray^.dilList[rdn].DilCnt - 1) do    { Throw away ">" step }
  begin
    TmpMdi:= CustIntrTray^.GetMDI(rdn, DilStep, False);
    if DilStep = 1 then
      TmpMdi:= TmpMdi - 64;         { Clean off high bits }
    ValidDils[GrpNum][TmpMdi]:= True;
  end;
  ValidDils[GrpNum][31]:= True;     { Set ">" step }
end;

function TCustIntr.DrugAbbr: string;
begin
  DrugAbbr:= CustIntrTray^.abrTbl[curADN];
end;

function TCustIntr.CurrentADN: Integer;
begin
  CurrentADN:= curADN;
end;

function TCustIntr.NumPanelGroups: integer;
begin
  NumPanelGroups:= grpInfo.numGroups;
end;

function TCustIntr.TotalDrugADNs: integer;
begin
  TotalDrugADNs:= CustIntrTray^.lastAbr;
end;

function TCustIntr.PanelsInGroup(grp: integer): integer;
{- return number of panels in a group }
var
  k    : integer;
  count: integer;
begin
  if (grp < 1) or (grp > NumPanelGroups) then
    PanelsInGroup:= 0
  else
    PanelsInGroup:= grpInfo.group[grp].numInGroup;
end;

function TCustIntr.PanelHeadDRN(grp: integer): integer;
begin
  if ((grp > 0) and (grp <= NumPanelGroups)) then
    PanelHeadDRN:= grpInfo.group[grp].headDRN
  else
    PanelHeadDRN:= 0;
end;

function TCustIntr.PanelNumber(grp, idx: integer): integer;
begin
  if ((grp > 0) and (grp <= NumPanelGroups)) and
     ((idx > 0) and (idx <= PanelsInGroup(grp))) then
    PanelNumber:= grpInfo.group[grp].pnls[idx]
  else
    PanelNumber:= 0;
end;

procedure TCustIntr.PanelName(grp, idx: integer; name: PChar; maxLen: integer);
{- find the actual panel number from the pnlGroups bitmap. Using idx
   scan the bitmap and figure out the actual panel number }
begin
  strcopy(name, '');
  if ((grp > 0) and (grp <= NumPanelGroups)) and
     ((idx > 0) and (idx <= PanelsInGroup(grp))) then
  begin
    CustIntrTray^.LoadTrayByNum(grpInfo.group[grp].pnls[idx]);
    StrLCopy(name, CustIntrTray^.tfsRec.tName, maxLen);
  end;
end;

function TCustIntr.NumExecpts(grp: integer): integer;
begin
end;

procedure TCustIntr.ExceptName(grp, idx: integer; name: PChar; maxLen: integer);
begin
end;


procedure TCustIntr.LoadTherInfo(grp: Integer);
{- load the DRNs for the head record of the specified group and all
   of it's exception records as well as load the txtIdx parameter
   in the ther Rec. }
var
   drec      : interps.DrugRec;
   dRec2     : interps.DrugRec;
   k, j      : integer;
   temp      : integer;
   hDRN      : integer;
   allDone   : boolean;
   TherRecCnt : Integer;
   MDICnt     : Integer;
begin
  hDrn:= grpInfo.group[grp].headDRN;
  if intrps^.TherRecRead(hDrn, @dRec, DrgRecType) then
  begin
    k:= 1;
    allDone:= False;
    GrpInfo.TherInfo.NumRecs := 0;
    For TherRecCnt := 1 to MaxExc do
    Begin
      GrpInfo.TherInfo.TherRecs[TherRecCnt].NumThers:=0;
      GrpInfo.TherInfo.TherRecs[TherRecCnt].TxtIdx:=0;
      GrpInfo.TherInfo.TherRecs[TherRecCnt].Drn:=0;
      GrpInfo.TherInfo.TherRecs[TherRecCnt].Modified:=False;
      GrpInfo.TherInfo.TherRecs[TherRecCnt].FileModified:=False;
      For MDICnt := 1 to MaxMDI do
      Begin
        GrpInfo.TherInfo.TherRecs[TherRecCnt].MDIs[MDICnt]:=Byte(0);
        GrpInfo.TherInfo.TherRecs[TherRecCnt].Intrps[MDICnt]:=Byte(0);
      End;
    End;

    while not allDone and (k < MaxExc) do begin
      if dRec.next[k] = 0 then
        temp:= hDRN
      else
        temp:= dRec.next[k];
      intrps^.TherRecRead(temp, @dRec2, DrgRecType);

      {- check to see if record has NCCLS interps and is not suppressed ($89) }
      if dRec2.ncON and (dRec2.sup <> $89) then
      with grpInfo.therInfo do { Load therapy info on demand, v4.00, ghg 8/3/98 }
      begin
        Inc(numRecs);
        with therRecs[numRecs] do
        begin
          drn:= temp;
          txtIdx:= drec2.next[MaxExc];
          fileModified:= TestBit(dRec2.exc[MaxExc][0], 1);
          modified:= false;
          for j:= 1 To MaxMDI do begin
            if validDils[grp, j] then
            begin
              Inc(numThers);
              MDIs[numThers]:= j;
              intrps[numThers]:= dRec2.ncTher[j];
            end;
          end;
        end;
      end;
      allDone:= (dRec.exc[k][0] = 0) and (dRec.exc[k][1] = 0);
      k:= k + 1;
    end;
  end;
end;

function TCustIntr.InterpVal(blood: Boolean; value: Byte): Integer;
{- return the interpretation nibble based on Blood/Urine }
begin
  if blood Then
    InterpVal:= (value and $F0) shr 4
  else
    InterpVal:= value and $0F;
end;

procedure TCustIntr.InterpValStr(blood: boolean; interp: byte; st: PChar; maxLen: integer);
{- return the interpretation as a string }
var
  pstr   : array[0..25] of char;
  b      : byte;
  ci     : boolean; {- contra indicated }
begin
  b:= InterpVal(blood, interp);
  if b = 6 then
    strcopy(pstr, '<blank>')
  else
    InterpCodeToStr(b, pstr, sizeof(pstr)-1, ci);
  StrLCopy(st, pstr, maxLen);
end;

function TCustIntr.MergeInterp(blood: boolean; oldVal, newVal: byte): byte;
{- merge new therapy into original therapy byte }
{- blood - true  = merge value into blood nibble
         - false = merge value into urine nibble.
   returns the merged therapy byte }
begin
  if blood then
    MergeInterp:= (oldVal and $0F) or ((newVal shl 4) and $F0)
  else
    MergeInterp:= (oldVal and $F0) or (newVal and $0F);
end;

function TCustIntr.GetTherapy(aGroup, aTherRec, aDilStep: integer): byte;
{- return a urine/system therapy byte based on group/therapy record and dil step.
   returns $FF is an invalid parameter is passed }
begin
  with grpInfo do
  begin
    if (aGroup > 0) and (aGroup <= NumPanelGroups) then
    Begin
(*      LoadTherInfo(aGroup); { Load therapy info on demand, v4.00, ghg 8/3/98 }*)
      If (aTherRec > 0) and (aTherRec <= therInfo.numRecs) and
         (aDilStep > 0) and (aDilStep <= therInfo.therRecs[aTherRec].numThers) then
        GetTherapy:= therInfo.therRecs[aTherRec].intrps[aDilStep]
      else
        GetTherapy:= $FF;
    End;
  end;
end;

procedure TCustIntr.SetTherapy(aGroup, aTherRec, aDilStep: integer; aTher: byte);
{- set a therapy in the specified therapy record.
   inParm : aGroup    - panel group to modify
            aTherRec  - Therapy record to modify
            aDilStep  - MIC dilution step (0-31)
            aTher     - The new therapy for the specified therapy record/dil step
  If the therapy being set is changed, then the modified flag will be set to
  true for the entire therapy record.
}
begin
  with grpInfo do
  begin
    if (aGroup > 0) and (aGroup <= NumPanelGroups) and
       (aTherRec > 0) and (aTherRec <= therInfo.numRecs) and
       (aDilStep > 0) and (aDilStep <= therInfo.therRecs[aTherRec].numThers) then
    begin
      with therInfo.therRecs[aTherRec] do
      begin
        if not modified then
          modified:= aTher <> intrps[aDilStep];
        intrps[aDilStep]:= aTher;
      end;
    end;
  end;
end;

function TCustIntr.SaveChanges: integer;
{- check all load panel groups and all therapy records in those groups
   and save any that have changed.
   returns : 0 if record written
             1 if record cannot be re-read
             2 if record cannot be written }
var
  g, t  : integer;
  k     : integer;
  drec  : interps.DrugRec;
  aDrn  : integer;
  idx   : integer;
begin
  SaveChanges:= 0;
  with grpInfo do
  begin
    for t:= 1 to therInfo.numRecs do
    begin
      if therInfo.therRecs[t].modified then
      begin {- found a modified therapy record }
        {- reread the old drug record from THERAPY.DAT }
        aDRN:= therInfo.therRecs[t].drn;
        if intrps^.TherRecRead(aDrn, @dRec, DrgRecType) then
        begin
          {- copy therapy record into a drugRecord for writing }
          for k:= 1 to therInfo.therRecs[t].numThers do
          begin
            idx:= therInfo.therRecs[t].MDIs[k]; {- index to drugRec data }
            dRec.ncTher[idx]:= therInfo.therRecs[t].intrps[k];
          end;
          {- set modified bit in file }
          SetBit(dRec.exc[MaxExc][0], 1);

          {- write the record to disk }
          if not intrps^.TherRecWrite(aDRN, @dRec, DrgRecType) then
          begin
            SaveChanges:= 2;
            exit;
          end
          else
            therInfo.therRecs[t].modified:= false; {- flag as unchanged }
        end
        else
        begin
          SaveChanges:= 1;
          exit;
        end;
      end;
    end;
  end;
end;

function TCustIntr.IsFileModified(aGroup, aTherRec: integer): boolean;
{- return true if the specified therapy record has its modified bit set. }
begin
  IsFileModified:= false;
  with grpInfo do
  begin
    if (aGroup > 0) and (aGroup <= NumPanelGroups) then
    Begin
      LoadTherInfo(aGroup); { Load therapy info on demand, v4.00, ghg 8/3/98 }
      if (aTherRec > 0) and (aTherRec <= therInfo.numRecs) then
      begin
        IsFileModified:= therInfo.therRecs[aTherRec].fileModified;
      end;
    End;
  end;
end;

function TCustIntr.IsTherModified(aGroup, aTherRec: integer): boolean;
{- return true if the specified therapy record has been modified for the
   current editing session }
begin
  IsTherModified:= false;
  with grpInfo do
  begin
    if (aGroup > 0) and (aGroup <= NumPanelGroups) then
    Begin
      if (aTherRec > 0) and (aTherRec <= therInfo.numRecs) then
      begin
        IsTherModified:= therInfo.therRecs[aTherRec].modified;
      end;
    End;
  end;
end;

function TCustIntr.IsAnyTherModified: boolean;
var
  isMod  : boolean;
  j, i: integer;
begin
  isMod:= false;
  with grpInfo do
  begin
    j:= 1;
    while (j <= therInfo.numRecs) and not isMod do
    begin
      isMod:= therInfo.therRecs[j].modified;
      Inc(j);
    end;
  end;
  IsAnyTherModified:= isMod;
end;


END.

{- rcf}
