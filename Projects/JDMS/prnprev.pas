unit PrnPrev;

(**
   print from preview window, change setup, page does not rescan }

   if devSettings in TPrinter is nil then ExtDeviceMode is not supported

   date/time will change over a long report.
**)

{- PrnPrev - Provides Print/Preview functions. }

INTERFACE

uses
  DlgLib,
  BigList,
  PrnObj,
  WinTypes,
  OWindows,
  ODialogs,
  objects;

{$R PRNPREV.RES}
{$I PRNPREV.INC}

const
  {- common measurement constants }
  LT08           = 1440 div 8;     {- 1/8  inches }
  LT25           = 1440 div 4;     {- 0.25 inches }
  LT50           = 1440 div 2;     {- 0.5  inches }
  LT75           = 1440 * 3 div 4; {- 0.75 inches }
  LT100          = 1440;           {- 1.0  inches }
  LT125          = LT100 + LT25;   {- 1.25 inches }
  LT150          = LT100 + LT50;   {- 1.5  inches }
  LT175          = LT100 + LT75;   {- 1.75 inches }
  LT200          = 1440 * 2;       {- 2.0  inches }
  LT300          = 1440 * 3;       {- 3.0  inches }
  LT400          = 1440 * 4;       {- 4.0  inches }
  LT500          = 1440 * 5;       {- 5.0  inches }
  LT600          = 1440 * 6;       {- 6.0  inches }
  LT700          = 1440 * 7;       {- 7.0  inches }
  LT800          = 1440 * 8;       {- 8.0  inches }

  DefaultMargin  = LT50;  {- default .5 inch margin }
  DefaultPoints  = 10;
  intMarg        = 1;     {- interior margin for cells (in PTs) }
  fontExtra      = 2;     {- amount to add to point size to correct
                             for proper cell size (in points) }
  edgeX          = LT08;  {- distance from edge of window to paper (for }
  edgeY          = LT08;  {  screen preview) }

  {- CELL FLAGS }
  c_Normal       = $00000000;  {- normal font }
  c_Italic       = $00000001;  {- italic }
  c_Bold         = $00000002;  {- bold }
  c_Underline    = $00000004;  {- underline }
  c_Strikeout    = $00000008;  {- strike out }

  {- text alignment flags (within cell) }
  c_Left         = $00000010;  {- left align (default) }
  c_Top          = $00000020;  {- top align }
  c_Right        = $00000040;  {- right align }
  c_Bottom       = $00000080;  {- bottom align (default) }
  c_Center       = $00000100;  {- horizontal center }
  c_VCenter      = $00000200;  {- vertical center }
  c_Baseline     = $00000400;  {- align text to font baseline of entire row }

  {- cell alignment flags (alignment on page) }
  c_PageLeft     = $00000800;  {- align cell to left margin }
  c_PageRight    = $00001000;  {- align cell to right margin }
  c_PageCenter   = $00002000;  {- align cell to center of page }
  c_Stretch      = $00004000;  {- stretch cell to right margin }

  {- misc cell format flags }
  c_BorderLeft   = $00008000;
  c_BorderTop    = $00010000;
  c_BorderRight  = $00020000;
  c_BorderBottom = $00040000;
  c_BorderAll    = c_BorderLeft or c_BorderTop or
                   c_BorderRight or c_BorderBottom;  {- outline the cell }
  c_WordWrap     = $00080000;  {- wrap text in cell }

  {- print-time calculated variables }
  {- the following flags rely on the fact that each flag has a corresponding
     format character in the cell text. The format characters are shown below
     as ^x where x is defined below.
     For example:  Cell string = 'Page ^p of ^n'
                   Cell flag   = c_PageNum or c_NumPages }
  c_PageNum      = $00100000;  {- ^p  inserts the page number into the cell }
  c_NumPages     = $00200000;  {- ^n  inserts the total pages into the cell }
  c_LineNum      = $00400000;  {- ^l  inserts the line number into the cell }
  c_LineOnPage   = $00800000;  {- ^c  inserts the cells relative line number on page}
  c_Date         = $01000000;  {- ^d  inserts the date for report (uses intl short fmt)}
  c_Time         = $02000000;  {- ^t  inserts the time for report (uses intl time fmt)}

  {- ROW FLAGS }
  r_Normal       = 0;          {- row border flags ----- }
  r_BorderTop    = $00000001;
  r_BorderBottom = $00000002;
  r_BorderLeft   = $00000004;
  r_BorderRight  = $00000008;

  r_BorderAll    = $0000000F;

  r_PageBreak    = $00000010;  {- force page break *before* line. page breaks within a
                                  group are not allowed }
  r_CenterRow    = $00000020;  {- center entire line on page }
                               {- the following flags are ignored for the entire row
                                  when this option is ON.
                                  c_PageLeft, c_PageRight, c_PageCenter, c_Stretch }
  r_StartGroup   = $00000040;  {- start a logical group of rows }
  r_EndGroup     = $00000080;  {- end a logical group of rows }
  r_CellWrap     = $00000100;  {- wrap cells that exceed right border }
                               {- all wrapped cells are same height as largest cell}
                               {- cell page alignment/stretching  should not be used when wrapping
                                  cells on a page }
  r_SysFlags     = $F0000000;  {- reserved by system for special formatting }

type
  PPrintData = ^TPrintData;
  TPrintData = object(TObject)
    constructor Init(aDefPtSize: integer);
    destructor Done; virtual;
    procedure AddRow(aflags: longint; wrapMarg: integer);
    function AddCell(aStr: PChar; aWidth, flags: longint; aPtSize: integer): integer;
    procedure AddEndGroupToLastRow;
    function TotalRecs: longint;
    {}
    private
    data       : PLCollection;
    defPtSize  : integer;
    function TotalRowWidth(aRecNum: longint): integer;
    function MaxLinePtSize(aRecNum: longint): integer;
    function MinLinePtSize(aRecNum: longint): integer;
    function FirstRecInLine(aRecNum: longint): longint;
    function LastRecInLine(aRecNum: longint): longint;
    function GetCellStr(aRecNum: longint; aStr: PChar; maxLen: integer): PChar;
    function GetCellStrSize(aRecNum: longint): integer;
    function IsBOL(aRecNum: longint): boolean;
  end;

  {- main print information object }
  PPrintInfo = ^TPrintInfo;
  TPrintInfo = object(TObject)
    header  : PPrintData;
    footer  : PPrintData;
    body    : PPRintData;
    constructor Init(aFont: TLogFont; defaultPtSize: integer);
    destructor Done; virtual;
    function GetMaxCharWidth(numChars: integer; ptSize: integer): integer;
    function GetAvgCharWidth(numChars: integer; ptSize: integer): integer;
    procedure SetMargins(left, top, right, bottom: integer);
    procedure SetRowSpacing(spacing: integer);
    {}
    private
    logFont     : TLogFont;
    lMargin     : integer;  {- left margin (in LTwips) }
    tMargin     : integer;  {- top margin (in LTwips) }
    bMargin     : integer;  {- bottom margin (in LTwips) }
    rMargin     : integer;  {- right margin (in LTwips) }
    rowSpacing : integer;  {- line spacing between rows (in PTs) }

    curDev      : PChar;    {- current print device }
    pageSize    : TPoint;   {- page size (LTwips) }
    minMarg     : TRect;    {- minimum margins allowed for device (LTwips) }
    footSize    : integer;  {- total size of footer }
    headSize    : integer;  {- total size of header }
    bdyInfo     : PLCollection; {- row info for sections }
    hdrInfo     : PLCollection;
    ftrInfo     : PLCollection;
    bLimit      : integer;  {- bottom limit for printing }
    rLimit      : integer;  {- right limit for printing }
    startAt     : TPoint;   {- starting points for the first cell on the page
                               including header }
    prn         : PPrinter;
    totalPages  : integer;
    defPtSize   : integer;  {- default point size for report }
    function NeedToScan  : boolean;
    function NumPages    : integer;
  end;

  PPrnPrevDlg = ^TPrnPrevDlg;
  TPrnPrevDlg = object(TCenterDlg)
    pInfo   : PPrintInfo;
    constructor Init(aParent: PWindowsObject; aPInfo: PPrintInfo; aRptTitle: PChar);
    procedure SetupWindow; virtual;
    procedure IDSetup(var msg: TMessage); virtual ID_FIRST + IDC_SETUP;
    procedure IDPreview(var msg: TMessage); virtual ID_FIRST + IDC_PREVIEW;
    procedure IDPrint(var msg: TMessage); virtual ID_FIRST + IDC_PRINT;
    procedure ShowPrinterName;
    procedure EnableButtons; virtual;
    private
    rptTitle: PChar;
  end;

function PrintPrim(aParent: PWindowsObject; pInfo: PPrintInfo; rptTitle: PChar;
                    promptPrintSel: boolean): boolean;

IMPLEMENTATION

uses
  ApiTools,
  Status,
  Intllib,
  SpinEdit,
  PrnInfo,
  Modwin,
  DMString,
  MScan,
  Strings,
  INILib,
  Win31,
  WinProcs,
  DMSDebug;

const
  R_GROUPBREAK  = $80000000;  {- indicates a group page break }

  FORMULA_MASK  = $03F00000;  {- used as mask for formula flags}
  FMT_MASK      = $0F;        {- used to mask off style info from flags }

{ Note: The width of a line element is in logical twips. That is, there
        are 1440 LTwips in one inch. }

type
  TCurFontInfo = record {- record to describe the current font style}
    fmt    : longint;
    ptSize : integer;
    lf     : TLogFont;
  end;

  PCellRec = ^TCellRec;
  TCellRec = object(TObject)
    flags   : longint;  {- print flags (see c_xxx constants or r_xxx constants) }
    width   : integer;  {- width of cell (in LTwips). if cell specifies a row
                           description record, then width specifies a wrap margin}
    ptSize  : integer;  {- point size for string }
    rowDesc : boolean;  {- true if object defines a row. false if objet describes
                           a cell in a row }
    pstr    : PChar;    {- cell text }
    constructor Init;
    destructor Done; virtual;
  end;

  PRowInfo = ^TRowInfo;
  TRowInfo = object(TObject)
    height     : integer;  {- maximum height of line at print time }
    maxDescent : integer;  {- maximum font descent for line }
    newPageRec : longint;  {- if non zero, then specifies the cell starting record
                              for a new page.}
    width      : integer;
    pageNum    : integer;
    constructor Init;
  end;

  PPrintWin = ^TPrintWin;
  TPrintWin = object(TModalWin)
    constructor Init(aParent: PWindowsObject; aTitle: PChar; aParentHWnd: HWnd;
                     aPrintInfo: PPrintInfo);
    destructor Done; virtual;
    procedure SetupWindow; virtual;
    procedure WMPaint(var msg: TMessage);     virtual WM_FIRST + WM_PAINT;
    procedure CMCloseWin(var msg: TMessage);  virtual CM_FIRST + CM_CLOSEWIN;
    procedure CMSetup(var msg: TMessage);     virtual CM_FIRST + CM_SETUP;
    procedure CMPrint(var msg: TMessage);     virtual CM_FIRST + CM_PRINT;
    procedure CMBottom(var msg: TMessage);    virtual CM_FIRST + CM_BOTTOM;
    procedure CMTop(var msg: TMessage);       virtual CM_FIRST + CM_TOP;
    procedure cmDebug(var msg: TMessage);     virtual CM_FIRST + CM_DEBUG;
    procedure DebugDump;
    procedure cmGoto(var msg: TMessage);      virtual CM_FIRST + CM_GOTO;
    procedure GetWindowClass(var aWndClass: TWndClass); virtual;
    function GetClassName: PChar; virtual;
    procedure WMCommand(var msg: TMessage);   virtual WM_FIRST + WM_COMMAND;
    procedure WMKeyDown(var msg: Tmessage);   virtual WM_FIRST + WM_KEYDOWN;
    procedure SetZoom(aZoom: real);
    procedure NextPage;
    procedure PrevPage;
    procedure ShowPageNum;
    {}
    private
    winTitle    : PChar;
    pInfo       : PPrintInfo;
    zoom        : real;
    ppiX        : integer;
    ppiY        : integer;
    scrollUnit  : integer;
    curPage     : integer;
    pageStr     : array[0..25] of char;
    ofStr       : array[0..25] of char;
    procedure SetupScroller;
  end;

  PPrnReport = ^TPrnReport;
  TPrnReport = object(TPrintOut)
    pInfo     : PPrintInfo;
    doPrompt  : boolean;  {- if true, prompt for print selection }
    {}
    constructor Init(aTitle: PChar; aPrnInfo: PPrintInfo; promptSel: boolean);
    function HasNextPage(page: word): boolean; virtual;
    procedure SetPrintParams(aDC: HDC; aSize: TPoint); virtual;
    procedure BeginPrinting; virtual;
    procedure PrintPage(page: word; var rect: TRect; flags: word); virtual;
    function GetDialogInfo(var pages: integer): boolean; virtual;
  end;

procedure ShowPrinterError(hw: HWnd; aPrn: PPrinter);
var
  k        : integer;
  pstr, p2 : array[0..100] of char;
begin
  if aprn^.status <> PS_OK then
  begin
    if aprn^.status = PS_INVALIDDEVICE then
      k:= IDS_INVALIDDEVICE
    else
      k:= IDS_NOPRINTER;
    SR(k, pstr, sizeof(pstr)-1);
    SR(IDS_ERROR, p2, sizeof(p2)-1);
    ErrorMsg(hw, p2, pstr);
  end;
end;

{------------------------------------------------------------[ TRowInfo ]--}

constructor TRowInfo.Init;
begin
  inherited Init;
  height:= 0;
  maxDescent:= 0;
  newPageRec:= 0;
  pageNum:= 0;
  width:= 0;
end;

procedure FillCurFontInfo(lf: TLogFont; var cur: TCurFontInfo; ptSize: integer);
{- set the curFont info to the default font information for lf }
begin
  with lf do
  begin
    cur.lf:= lf;
    cur.lf.lfHeight:= -(ptSize * 20);
    cur.ptSize:= ptSize;
    cur.fmt:= 0;
    if lfItalic <> 0 then
      cur.fmt:= cur.fmt or c_Italic;
    if lfWeight > FW_NORMAL then
      cur.fmt:= cur.fmt or c_Bold;
    if lfUnderline <> 0 then
      cur.fmt:= cur.fmt or c_Underline;
    if lfStrikeOut <> 0 then
      cur.fmt:= cur.fmt or c_StrikeOut;
  end;
end;

function CompareFont(var curFont: TCurFontInfo; aFmt: longint; aPtSize: integer): boolean;
{- compare the current font information with the specified format info.
   if the different return true, else return false }
begin
  CompareFont:= (curFont.fmt <> (aFmt and fmt_Mask)) or
                (curFont.ptSize <> aPtSize);
end;

procedure BuildStyle(var curFont: TCurFontInfo; aSize: integer; aFmt: longint);
{- build a cell style into the log font from size(points) and fmt }
begin
  with curFont, lf do
  begin
    fmt:= aFmt and fmt_Mask;
    ptSize:= aSize;
    lfHeight:= -(aSize * 20); {- convert height to LTwips}
    if fmt and c_bold <> 0 then
      lfWeight:= FW_BOLD
    else
      lfWeight:= FW_NORMAL;
    if fmt and c_underline <> 0 then
      lfUnderLine:= 1
    else
      lfUnderLine:= 0;
    if fmt and c_italic <> 0 then
      lfItalic:= 1
    else
      lfItalic:= 0;

    if fmt and c_StrikeOut <> 0 then
      lfStrikeOut:= 1
    else
      lfStrikeOut:= 0;
  end;
end;

procedure ExpandFormulae(src: PChar; pInfo: PPrintInfo; flags: longint;
                         maxLen, line, lOnPage, page: integer);
{- expand any formula codes in flags, inserting them in the specified place in the
   src string. src string should be formatted with substitution characters as follows:
   ^p  - page number
   ^n  - total pages
   ^l  - line number
   ^c  - current line on page
   ^d  - date
   ^t  - time
   if src does not specify a substitution char, then the expanded formula will just
   be appended to src.  result MUST be large enough to accept the resultant string. }
const
  hideChar = #1;
var
  src2  : PChar;
  format: PChar;
  sub   : array[0..75] of char;
  sStr  : array[0..2] of char;

  procedure HidePercents;
  var
    p  : PChar;
  begin
    repeat
      p:= StrScan(src, '%');
      if p <> nil then
        p^:= hideChar;
    until p = nil;
  end;

  procedure UnhidePercents;
  var
    p  : PChar;
  begin
    repeat
      p:= StrScan(src, hideChar);
      if p <> nil then
        p^:= '%';
    until p = nil;
  end;

  procedure InsSubString(substitute, subStr: PChar);
  {- insert subStr into result replacing the substitution string [substitute] }
  {- on entry, src has the current format string }
  {- on exit, src will have the new format string }
  type
    TArgList = array[0..1] of PChar;
  var
    p     : PChar;
    count : integer;
    arg   : ^TArglist;
    k     : integer;
  begin
    count:= 0;
    repeat
      p:= StrPos(src, substitute);  {- find substitute code }
      if p = nil then
        p:= StrPos(src, StrUpper(substitute));
      if (p <> nil) and ((strlen(src) - 2 + strlen(subStr)) <= maxLen) then
      begin
        p^:= '%';
        (p+1)^:= 's';
        Inc(count);
      end;
    until p = nil;
    if count > 0 then
    begin
      GetMem(arg, sizeof(PChar)*count);
      {$IFOPT R+}
        {$DEFINE rplus}
      {$ENDIF}
      {$R-}
      for k:= 1 to count do
        arg^[k-1]:= subStr;
      {$IFDEF rplus}
        {$R+}
        {$UNDEF rplus}
      {$ENDIF}
      wvsprintf(src2, src, arg^);
      StrCopy(src, src2);
      MSFreeMem(arg, sizeof(PChar)*count);
    end
    else {- no format substitution code specified, so just add string to end of src}
      StrLCat(src, substr, maxLen);
  end;

begin
  HidePercents;
  GetMem(src2, maxLen+1);
  StrCopy(src2, '');
  if flags and c_PageNum <> 0 then
  begin
    IntlIntToStr(page, sub, 100);
    StrCopy(sStr, '^p');
    InsSubString(sStr, sub);
  end;
  if flags and c_NumPages <> 0 then
  begin
    IntlIntToStr(pInfo^.NumPages, sub, 100);
    StrCopy(sStr, '^n');
    InsSubString(sStr, sub);
  end;
  if flags and c_LineNum <> 0 then
  begin
    IntlIntToStr(line, sub, 100);
    StrCopy(sStr, '^l');
    InsSubString(sStr, sub);
  end;
  if flags and c_LineOnPage <> 0 then
  begin
    IntlIntToStr(lOnPage, sub, 100);
    StrCopy(sStr, '^c');
    InsSubString(sStr, sub);
  end;
  if flags and c_Date <> 0 then
  begin
    IntlDateStr(IntlCurrentDate, sub, 100);
    StrCopy(sStr, '^d');
    InsSubString(sStr, sub);
  end;
  if flags and c_Time <> 0 then
  begin
    IntlTimeStr(IntlCurrentTime, false, sub, 100);
    StrCopy(sStr, '^t');
    InsSubString(sStr, sub);
  end;
  MSFreeMem(src2, maxLen+1);
  UnhidePercents;
end;

procedure SetPrintRect(pInfo: PPrintInfo);
{- initialize the actual starting coordinates for a page (in LTwips)
   and calculates a right limit (rLimit) and a bottom limit (bLimit).
   These two values are the out of bounds markers for printing.
   inParms: dc    - device context
   outParm: aRect - the initial print rect for the page

   Sets fields in pInfo as follows:
   rLimit- The rightmost limit for printing. No printing will
           exceed past this number.
   bLimit- the bottom limit for printing. Printing of the body
           will not exceed this number. The footer will start
           at bLimit.
   Assumes that the correct footer size has already been calculated.
}
begin
  with pInfo^ do
  begin
    {- adjust starting position by subtracting the minimum margin for the
       dc (if any) from the actual margin thus calculating the actual
       "real" distance from the DC's origin }
    startAt.x:= lMargin - minMarg.left;
    startAt.y:= tMargin - minMarg.top;
    rLimit:= startAt.x + pageSize.x - rMargin - lMargin;
    bLimit:= startAt.y + pageSize.y - bMargin - tMargin - footSize;
  end;
end;

function CalcRowHeight(DC: HDC; pInfo: PPrintInfo; pData: PPrintData; aRec: longint;
                       var maxDescent: integer; var curFont: TCurFontInfo): integer;
{- calculate the maximum rectangle height required for the row to
   which aRec belongs. Also returns the maximum descent for the row.
   Assumes the correct font has already been selected into the DC }
var
  maxPt: integer;
  tm   : TTextMetric;
  maxH : integer;  {- max height  }
  tMax : integer;
  w    : integer;
  pWid : integer;
  k    : longint;
  j    : integer;
  p    : PCellRec;
  tr   : TRect;
  pstr : PChar;
  wExt : longint;
  vExt : longint;
begin
  wExt:= GetWindowExt(dc);   {- save current window and viewport extents }
  vExt:= GetViewPortExt(dc);

  SetLogicalTwips(dc, 1.0); {- set to zoom 1.0 for consistent measurements }

  maxPt:= pData^.MaxLinePtSize(aRec);  {- calculate the maxumum point size for the row }

  if CompareFont(curFont, 0, maxPt) then
  begin
    curFont.fmt:= 0;
    curFont.ptSize:= maxPt;

    curFont.lf.lfHeight:= -(maxPt * 20); {- convert point size to height in LTwips }

    {- select a font of the largest point size into the DC to calculate the
       char descent distance }
    DeleteObject(SelectObject(dc, CreateFontIndirect(curFont.lf)));
  end;

  GetTextMetrics(dc, tm);

  maxH:= tm.tmHeight + tm.tmExternalLeading + fontExtra*20;
  maxDescent:= tm.tmDescent;       {- get max char descent (used to align
                                      text baselines }

  {- now given the maximum height for single row, see if any lines
     are specified as wrapped text. If so, calulcate the height of
     the rectangle based on its font. if this height is larger than
     the single row max, then adjust max to this new height }

  w:= 0;
  pWid:= pInfo^.pageSize.x - pInfo^.lMargin - pInfo^.rMargin; {- printable width }

  {- add one (in for loop init) to skip the row desc record }
  for k:= pData^.FirstRecInLine(aRec) + 1 to pData^.LastRecInLine(aRec) do
  begin
    p:= pData^.data^.At(k);
    if p^.flags and c_WordWrap <> 0 then
    begin
      if CompareFont(curFont, p^.flags, p^.ptSize) then
      begin
        BuildStyle(curFont, p^.ptSize, p^.flags);
        DeleteObject(SelectObject(dc, CreateFontIndirect(curFont.lf)));
      end;
      GetTextMetrics(dc, tm);

      tMax:= pData^.GetCellStrSize(k);
      GetMem(pstr, tMax);
      pData^.GetCellStr(k, pstr, tMax-1);

      if p^.flags and c_Stretch <> 0 then
      begin
        p^.width:= pWid - w;
        if p^.width < 0 then
          p^.width:= 0;
        Inc(w, p^.width);
      end;

      {- adjust the width of the rectangle until CALCRECT comes up with
         a rectangle that has a width at least as wide as width specified}
      j:= 0;
      repeat
        SetRect(tr, 0, 0, p^.width - (j*LT08), 0);
        DrawText(dc, pstr, strLen(pstr), tr, DT_CALCRECT or DT_EXTERNALLEADING or DT_WORDBREAK);
        Inc(j);
      until (tr.right - tr.left <= p^.width) or (j > 8);

      MSFreeMem(pstr, tMax);

(*      tMax:= tr.bottom div tm.tmHeight; {- number of lines in wrapped cell}
      tMax:= tMax * p^.ptSize * 20; {- mult by point size and convert to lTwips}
      Inc(tMax, fontExtra * 20);
      if tMax > maxH then
        maxH:= tMax;*)

      if tr.bottom - tr.top > maxH then
        maxH:= (tr.bottom - tr.top) + (fontExtra * 20);
    end
    else
      Inc(w, p^.width);
  end;

  CalcRowHeight:= maxH + (pInfo^.rowSpacing * 20);
  {- restore original extents }
  SetWindowExt(dc, LoWord(wExt), HiWord(wExt));
  SetViewPortExt(dc, LoWord(vExt), HiWord(vExt));
end;

function CalcWrappedCellHeight(pInfo: PPrintInfo; pData: PPrintData; aRec: longint;
                                rowH: integer): integer;
{- rowH is maximum height calculated from CalcRowHeight. aRec specifies a record
   in the row being calculated. Assumes rLimit has already been set }
var
  k, j     : longint;
  h, w     : integer;
  p        : PCellRec;
  wrapMarg : integer;
begin
  w:= pInfo^.startAt.x;
  h:= rowH;
  j:= pData^.FirstRecInLine(aRec);
  p:= pData^.data^.At(j);   {- get wrap margin (indent) }
  wrapMarg:= p^.width;
  for k:= j + 1 to pData^.LastRecInLine(aRec) do
  begin
    p:= pData^.data^.At(k);
    if w + p^.width > pInfo^.rLimit then  {- will need to wrap this cell }
    begin
      Inc(h, rowH);
      w:= pInfo^.startAt.x + wrapMarg;
    end
    else
      Inc(w, p^.width);
  end;
  CalcWrappedCellHeight:= h;
end;

procedure CalcFooterSize(dc: HDC; pInfo: PPrintInfo; var curFont: TCurFontInfo);
var
  k     : integer;
  pr    : PRowInfo;
begin
  pInfo^.ftrInfo^.FreeAll;
  pInfo^.footSize:= 0;
  with pInfo^, footer^ do
  begin
    for k:= 0 to data^.count - 1 do
    begin
      if IsBOL(k) then
      begin
        pr:= New(PRowInfo, Init);
        pr^.height:= CalcRowHeight(dc, pInfo, footer, k, pr^.maxDescent, curFont);
        pr^.width:= TotalRowWidth(k);
        Inc(footSize, pr^.height);
        ftrInfo^.Insert(pr);
      end;
    end;
  end;
end;

procedure CalcHeaderSize(dc: HDC; pInfo: PPrintInfo; var curFont: TCurFontInfo);
var
  k     : integer;
  pr    : PRowInfo;
begin
  pInfo^.hdrInfo^.FreeAll;
  pInfo^.headSize:= 0;
  with pInfo^, header^ do
  begin
    for k:= 0 to data^.count - 1 do
    begin
      if IsBOL(k) then
      begin
        pr:= New(PRowInfo, Init);
        pr^.height:= CalcRowHeight(dc, pInfo, pInfo^.header, k, pr^.maxDescent, curFont);
        pr^.width:= TotalRowWidth(k);
        Inc(pInfo^.headSize, pr^.height);
        pInfo^.hdrInfo^.Insert(pr);
      end;
    end;
  end;
end;

procedure ScanPrintData(pInfo: PPrintInfo);
var
  rec        : longint;  {- current cell record num (should always be pointing to a
                            row desc record }
  pr         : PRowInfo; {- holds pointer to a row record }
  pc         : PCellRec; {- holds pointer for a cell rec }
  curHeight  : integer;  {- hold cur calc'ed height for the cur page }
  curs       : HCursor;  {- holds handle of old cursor}
  sw         : PPctStatDlg; {- status window pointer }
  dc         : HDC;
  sGrp       : longint;  {- starting group record }
  sGrpRow    : longint;  {- row where group started }
  htAtGrp    : integer;  {- current height at start of group }
  inGrp      : boolean;  {- true if processing in a group }
  grpRec     : PCellRec; {- pointer to cell at start of group }
  rowOnPage  : integer;  {- keeps track of the current row on the page }
  lookForEnd : boolean;  {- true when looking for an end group. used for syntax
                            checking start/end group pairs }
  curFont    : TCurFontInfo;
  pgNum      : integer;
  pstr       : array[0..100] of char;

  procedure ClearSysFlags(cell: PCellRec); far;
  begin
    cell^.flags:= cell^.flags and not r_SysFlags;
  end;

begin
  StrDispose(pInfo^.curDev);
  pInfo^.curDev:= StrNew(pInfo^.prn^.device);

  {- get a DC. If printer is not connected, then use the screen }
  if pInfo^.prn^.status <> ps_OK then
    dc:= GetDC(0)
  else
    dc:= pInfo^.prn^.GetDC;

  FillCurFontInfo(pInfo^.logFont, curFont, pInfo^.defPtSize);
  SelectObject(dc, CreateFontIndirect(curFont.lf));

  if pInfo^.body^.TotalRecs > 50 then
  begin
    sw:= New(PPctStatDlg, Init(application^.mainWindow, MakeIntResource(DLG_PERCENT), true, IDC_PERCENT));
    application^.MakeWindow(sw);
    sw^.CompPctLevel(0, 0);
  end
  else
  begin
    sw:= nil;
  end;

  pInfo^.bdyInfo^.FreeAll;

  CalcFooterSize(dc, pInfo, curFont);  {- set footer size }
  CalcHeaderSize(dc, pInfo, curFont);  {- set header size }
  SetPrintRect(pInfo);    {- set bLimit/rLimit and startX/Y }

  {- clear any system flags set by previous scan}
  pInfo^.body^.data^.ForEach(@ClearSysFlags);

  curHeight:= pInfo^.headSize + pInfo^.startAt.y;
  rec:= 0;
  inGrp:= false;
  lookForEnd:= false;
  sGrp:= 0;
  sGrpRow:= 0;
  grpRec:= nil;
  htAtGrp:= 0;
  pgNum:= 1;
  rowOnPage:= 1;
  while rec < pInfo^.body^.TotalRecs do
  begin
    {- display progress }
    if ((sw <> nil) and (rec mod 50 <= 10)) then
      sw^.CompPctLevel(rec, pInfo^.body^.totalRecs);

    pc:= pInfo^.body^.data^.At(rec);  {- get cell pointer }
    pr:= New(PRowInfo, Init);         {- initialize a row element }

    {- if in a group and a manual page break found, desregard the group.}
    if inGrp and (pc^.flags and r_PageBreak <> 0) then
    begin
      inGrp:= false;
      sGrp:= 0;
      sGrpRow:= 0;
      grpRec:= nil;
    end;

    pr^.Height:= CalcRowHeight(dc, pInfo, pInfo^.body, rec, pr^.maxdescent, curFont);
    pr^.width:= pInfo^.body^.TotalRowWidth(rec);
    if pc^.flags and r_CellWrap <> 0 then
    begin
      Inc(curHeight, CalcWrappedCellHeight(pInfo, pInfo^.body, rec, pr^.height));
    end
    else
      Inc(curHeight, pr^.height);

    if (pc^.flags and r_PageBreak <> 0) or (curHeight > pInfo^.bLimit) then
    begin
      Inc(pgNum);
      rowOnPage:= 0;
      if not inGrp then
      begin
        pr^.newPageRec:= rec;
        pr^.pageNum:= pgNum;
      end
      else
      begin
        {- natural break inside of a group. need to adjust the group }
        PRowInfo(pInfo^.bdyInfo^.At(sGrpRow))^.newPageRec:= sGrp;
        PRowInfo(pInfo^.bdyInfo^.At(sGrpRow))^.pageNum:= pgNum;
        grpRec^.flags:= grpRec^.flags or r_GroupBreak;
        inGrp:= false;
        sGrp:= 0;
        grpRec:= nil;
        {- reset curHeight }
        curHeight:= pInfo^.startAt.y + pInfo^.headSize + (curHeight - htAtGrp);
        htAtGrp:= 0;
      end;
    end;

    {- if new page started }
    if pr^.newPageRec <> 0 then
      {- reset curHeight}
      curHeight:= pInfo^.headSize + pInfo^.startAt.y + pr^.height;

    {- see if a group is specified }
    if pc^.rowDesc and (pc^.flags and (r_StartGroup or r_EndGroup) <> 0) then
    begin
      {- if end group found and already in a group }
      if (pc^.flags and r_EndGroup <> 0) and (inGrp or lookForEnd) then
      begin
        inGrp:= false;
        lookForEnd:= false;
        sGrp:= 0;
        sGrpRow:= 0;
        grpRec:= nil;
        htAtGrp:= 0;
      end;
      {- if not already looking for an End Group then process the Start group }
      if (pc^.flags and r_StartGroup <> 0) and not lookForEnd then
      begin
        {- if this is already the first row on the page, there is no need to
           process the group }
        if rowOnPage > 1 then
        begin
          inGrp:= true;
          lookForEnd:= true;
          sGrp:= rec;  {- save group cell start record }
          sGrpRow:= pInfo^.bdyInfo^.count; {- save row record }
          grpRec:= pc;
          htAtGrp:= curHeight - pr^.height;
        end
        else
        begin
          lookForEnd:= true;
          inGrp:= false;
          sGrp:= 0;
          sGrpRow:= 0;
          grpRec:= nil;
        end;
      end;
    end;

    pInfo^.bdyInfo^.Insert(pr);
    rec:= pInfo^.body^.LastRecInLine(rec) + 1;  {- go to next line }
    Inc(rowOnPage);
  end;
  pInfo^.totalPages:= pgNum;

  if (sw <> nil) then
  begin
    sw^.CompPctLevel(100, 100); {- complete the percent bar to 100% }
  end;

  MSDisposeObj(sw);

  DeleteObject(SelectObject(dc, GetStockObject(SYSTEM_FONT)));

  if pInfo^.prn^.status <> ps_OK then
    ReleaseDC(0, dc)
  else
    DeleteDC(dc);
end;

function FindRowForPage(pInfo: PPrintInfo; pageNum: integer): longint;
{- find first row information record for the body of report for
   the specified page number }
var
  curPage : integer;
  k       : longint;
  row     : PRowInfo;

  function Matches(p: PRowInfo): boolean; far;
  begin
    Matches:= p^.pageNum = pageNum;
    Inc(k);
  end;

begin
  k:= -1;
  row:= pInfo^.bdyInfo^.FirstThat(@Matches);
  if row <> nil then
    FindRowForPage:= k
  else
    FindRowForPage:= 0;
end;

function PrintPrim(aParent: PWindowsObject; pInfo: PPrintInfo; rptTitle: PChar;
                   promptPrintSel: boolean): boolean;
{- print data in the print info object. If promptPrintSel = true, the the user
   will be prompted for a print selected (pages to print etc).
   If the print was successful (no errors and not cancelled), then the function
   returns true, otherwise, this function returns false }
var
  pstr, p2  : array[0..100] of char;
  pIdx      : integer;
  rpt       : PPrnReport;
begin
  PrintPrim:= false;
  if pInfo^.prn^.status = ps_OK then
  begin
    if pInfo^.NeedToScan then
    begin
      SelectedPaperSize(pInfo^.prn, pIdx, pInfo^.pageSize);
      PrintMargins(pInfo^.prn, pInfo^.minMarg);
      ScanPrintData(pInfo);
    end;
    rpt:= New(PPrnReport, Init(rptTitle, pInfo, promptPrintSel));
    PrintPrim:= pInfo^.prn^.Print(aParent, rpt);
    MSDisposeObj(rpt);
  end
  else
  begin
    ShowPrinterError(aParent^.hWindow, pInfo^.prn);
  end;
end;

function PaintData(dc: HDC; pInfo: PPrintInfo; pageNum: integer): integer;
{- print a page of data starting from the page specified.
   Assumes logical pixels are already selected in DC.
   returns : the record number for the next line to be printed }
var
  r           : TRect;     {- used for printing rectangle }
  curDescent  : integer;   {- descent for current cell }
  curFont     : TCurFontInfo;
  LTpPX, LTpPY: integer;

  procedure CheckFont(cur: PCellRec);
  {- check current cell's font requirements to that of the
     currently selected font. if different, build and select
     a new font }
  var
    tm  : TTextMetric;
  begin
    if CompareFont(curFont, cur^.flags, cur^.ptSize) then
    begin
      BuildStyle(curFont, cur^.ptSize, cur^.flags);
      {- remove current font }
      DeleteObject(SelectObject(dc, CreateFontIndirect(curFont.lf)));
      GetTextMetrics(dc, tm);
      curDescent:= tm.tmDescent;
    end;
  end;

  procedure SetRectWidth(cur: PCellRec);
  {- calculate the width of the rectangle for the current cell }
  var
    k   : integer;
  begin
    {- first check for cell alignment options }
    with pInfo^ do
    begin
      if cur^.flags and c_PageLeft <> 0 then
        r.left:= startAt.x
      else if cur^.flags and c_PageRight <> 0 then
        r.left:= rLimit - cur^.width
      else if cur^.flags and c_PageCenter <> 0 then
      begin
        k:= startAt.x + ((rLimit - startAt.x) div 2);
        Dec(k, cur^.width div 2);
        r.left:= k;
      end;
    end;

    if cur^.flags and c_Stretch <> 0 then
      r.right:= pInfo^.rLimit
    else
      r.right:= r.left + cur^.width + LTpPX;
  end;

  procedure InitPrint;
  {- set up for printing, create the default font and set the
     default font styles (curFmt) and set the initial rectangle }
  var
    tm  : TTextMetric;
  begin
    LTwipsPerPixel(dc, LTpPX, LTpPY);
    SelectObject(dc, GetStockObject(NULL_BRUSH));

    {- create font with default height and select into DC }
    FillCurFontInfo(pInfo^.logFont, curFont, pInfo^.defPtSize);
    SelectObject(dc, CreateFontIndirect(curFont.lf));
    GetTextMetrics(dc, tm);
    curDescent:= tm.tmDescent;

    with pInfo^ do
      SetRect(r, startAt.x, startAt.y, startAt.x, startAt.y);
  end;

  procedure PrintOne(pData: PPrintData; curRowInfo: PLCollection; curRowNum: longint);
  var
    pRect      : TRect;
    dt_Fmt     : integer;
    pstr       : PChar;
    strSize    : integer;
    cur        : PCellRec;  {- current print cell }
    rec        : longint;
    row        : PRowInfo;
    breakPage  : boolean;
    firstLine  : boolean;
    curLine    : longint;
    tWidth     : integer;
    inWrapRow  : boolean;
    wrapMarg   : integer;
  begin
    {- allocate print string buffer }
    strSize:= 256;   {- a reasonable text size }
    GetMem(pstr, strSize);

    inWrapRow:= false;
    breakPage:= false;
    firstLine:= true;
    row:= curRowInfo^.At(curRowNum);
    rec:= row^.newPageRec;
    curLine:= 0;

    SetBKMode(dc, transparent);
    Dec(curRowNum);  {- this needs to be done because of the structure of the
                        following loop and because line break cells come before
                        the actual data. the curRowNum will be incremented back
                        immediately when the loop first starts.
                     }
    while (rec < pData^.data^.count) and
          (((r.bottom <= pInfo^.bLimit) and (pData <> pInfo^.footer)) or
          (pData = pInfo^.footer)) and (not breakPage) do
    begin
      cur:= pData^.data^.At(rec);

      {- if a new row is found or current row exceeds the right margin }
      {- then go on to next row }
      if cur^.rowDesc or (r.left > (pInfo^.rLimit + LTpPX)) then
      begin
        breakPage:=  not firstLine and
                     (cur^.flags and (r_PageBreak or r_GroupBreak) <> 0);
        firstLine:= false;

        inWrapRow:= (cur^.flags and r_CellWrap <> 0);
        if inWrapRow then
          wrapMarg:= cur^.width;

        Inc(curLine);
        Inc(curRowNum);
        row:= curRowInfo^.At(curRowNum);  {- get next for information record }

        r.left:= pInfo^.startAt.x;
        r.top:= r.bottom - LTpPY;
        r.bottom:= r.top + row^.height + LTpPY;

        {- check for row borders }
        if cur^.flags and r_BorderTop <> 0 then
        begin
          MoveTo(dc, r.left, r.top);
          LineTo(dc, pInfo^.rLimit, r.top);
        end;
        if cur^.flags and r_BorderBottom <> 0 then
        begin
          MoveTo(dc, r.left, r.bottom - LTpPY);
          LineTo(dc, pInfo^.rLimit, r.bottom - LTpPY);
        end;
        if cur^.flags and r_BorderLeft <> 0 then
        begin
          MoveTo(dc, r.left, r.top);
          LineTo(dc, r.left, r.bottom);
        end;
        if cur^.flags and r_BorderRight <> 0 then
        begin
          MoveTo(dc, pInfo^.rLimit, r.top);
          LineTo(dc, pInfo^.rLimit, r.bottom);
        end;

        {- if center row specified, adjust current rectangle accordingly }
        if cur^.flags and r_CenterRow <> 0 then
        begin
          tWidth:= row^.width;
          r.left:= r.left + ((pInfo^.rLimit - r.left) div 2) - (tWidth div 2);
          Inc(r.left, 2*LTpPX);  { ?? }
        end;
      end
      else
      begin {- actual cell data }
        CheckFont(cur); {- make sure font is ok for current cell }
        SetRectWidth(cur);

        if (r.right > (pInfo^.rLimit + LTpPX)) and inWrapRow then
        begin
          {- need to adjust rectangle to next line (wrap it) }
          r.left:= pInfo^.startAt.x;
          if wrapMarg > 0 then
            Inc(r.left, wrapMarg);
          r.top:= r.bottom - LTpPY;
          r.bottom:= r.top + row^.height + LTpPY;
          if cur^.flags and c_Stretch <> 0 then
            r.right:= pInfo^.rLimit
          else
            r.right:= r.left + cur^.width + LTpPX;
        end
        else  {- if right goes beyond the right margin then truncate }
          if r.right > (pInfo^.rLimit + LTpPX) then
            r.right:= pInfo^.rLimit;

        {- check to see if buffer is large enough. reallocate if not }
        if pData^.GetCellStrSize(rec) > strSize then
        begin
          FreeMem(pstr, strSize);
          strSize:= pData^.GetCellStrSize(rec);
          if cur^.flags and Formula_Mask <> 0 then
            Inc(strSize, 128);  {- make room for formula expansion }
          GetMem(pstr, strSize);
        end;

        pData^.GetCellStr(rec, pstr, strSize-1);
        if cur^.flags and Formula_Mask <> 0 then
          ExpandFormulae(pstr, pInfo, cur^.flags, strSize-1,
                         curRowInfo^.IndexOf(row), curLine, pageNum);

        pRect:= r;  {- get printing rectangle }
        InflateRect(pRect, -intMarg*20, -intMarg*20); {- deflate for interior margin}

        with cur^ do
        begin
          {- adjust the text in the cell to line up with the font baseline for
             the row (n/a for wordwrap cells }
          if (flags and c_WordWrap = 0) and (flags and c_Baseline <> 0) then
            Dec(pRect.bottom, row^.maxDescent - curDescent);

          dt_Fmt:= DT_SINGLELINE; {- default }

          if flags and c_WordWrap <> 0 then
            dt_Fmt:= (dt_Fmt or DT_WORDBREAK or DT_EXTERNALLEADING) and not (DT_SINGLELINE or DT_BOTTOM or DT_VCENTER);

          if flags and c_Left <> 0 then
            dt_Fmt:= (dt_Fmt or DT_LEFT) and not (DT_RIGHT or DT_CENTER);

          if flags and c_Right <> 0 then
            dt_Fmt:= (dt_Fmt or DT_RIGHT) and not (DT_LEFT or DT_CENTER);

          if flags and c_Top <> 0 then
            dt_Fmt:= (dt_Fmt or DT_TOP) and not (DT_BOTTOM or DT_VCENTER);

          if (flags and c_Bottom <> 0) or (flags and c_Baseline <> 0) then
            dt_Fmt:= (dt_Fmt or DT_BOTTOM) and not (DT_TOP or DT_VCENTER);

          if flags and c_Center <> 0 then
            dt_Fmt:= (dt_Fmt or DT_CENTER) and not DT_LEFT and not DT_RIGHT;

          if (flags and c_VCenter <> 0) and (flags and c_WordWrap = 0) then
            dt_Fmt:= (dt_Fmt or DT_VCENTER) and not DT_TOP and not DT_BOTTOM;
        end;

        DrawText(dc, pstr, strlen(pstr), pRect, dt_Fmt);

        if (cur^.flags and c_BorderAll <> 0) then
        begin
          if cur^.flags and c_BorderAll = c_BorderAll then
            Rectangle(dc, r.left, r.top, r.right, r.bottom)
          else
          begin
            if cur^.flags and c_BorderTop <> 0 then
            begin
              MoveTo(dc, r.left, r.top);
              LineTo(dc, r.right, r.top);
            end;
            if cur^.flags and c_BorderBottom <> 0 then
            begin
              MoveTo(dc, r.left, r.bottom - LTpPY);
              LineTo(dc, r.right, r.bottom - LTpPY);
            end;
            if cur^.flags and c_BorderLeft <> 0 then
            begin
              MoveTo(dc, r.left, r.top);
              LineTo(dc, r.left, r.bottom);
            end;
            if cur^.flags and c_BorderRight <> 0 then
            begin
              MoveTo(dc, r.right - LTpPX, r.top);
              LineTo(dc, r.right - LTpPX, r.bottom);
            end;
          end;
        end;

        r.left:= r.right - LTpPX; {- increment to next position to
                                     the right of current cell}
      end;

      Inc(rec); {- go to next cell record }
    end;
    MSFreeMem(pstr, strSize);
  end;

begin
  InitPrint;
  if pInfo^.header^.data^.count > 0 then
    PrintOne(pInfo^.header, pInfo^.hdrInfo, 0);

  if pInfo^.body^.data^.count > 0 then
    PrintOne(pInfo^.body, pInfo^.bdyInfo, FindRowForPage(pInfo, pageNum));

  if pInfo^.footer^.data^.count > 0 then
  begin
    r.top:= pInfo^.bLimit;  {- set distance for footer }
    r.bottom:= pInfo^.bLimit;
    PrintOne(pInfo^.footer, pInfo^.ftrInfo, 0);
  end;
  DeleteObject(SelectObject(dc, GetStockObject(SYSTEM_FONT)));
end;

{---------------------------------------------[ TPrintInfo ]--}

constructor TPrintInfo.Init(aFont: TLogFont; defaultPtSize: integer);
begin
  inherited Init;
  if defaultPtSize > 0 then
    defPtSize:= defaultPtSize
  else
    defPtSize:= 10;
  body:= New(PPrintData, Init(defPtSize));
  footer:= New(PPrintData, Init(defPtSize));
  header:= New(PPrintData, Init(defPtSize));
  logFont:= aFont;
  rowSpacing:= 0;
  SetMargins(DefaultMargin, DefaultMargin, DefaultMargin, DefaultMargin);
  bdyInfo:= New(PLCollection, Init(500, 500));
  hdrInfo:= New(PLCollection, Init(20, 20));
  ftrInfo:= New(PLCollection, Init(20, 20));
  prn:= New(PPrinter, Init);

  curDev:= StrNew(prn^.device);
  longint(pageSize):= 0;
  SetRect(minMarg, 0,0,0,0);
  footSize:= 0;
  headSize:= 0;
  bLimit:= 0;
  rLimit:= 0;
  longint(startAt):= 0;
  totalPages:= 0;
end;

destructor TPrintInfo.Done;
begin
  inherited Done;
  MSDisposeObj(body);
  MSDisposeObj(footer);
  MSDisposeObj(header);
  MSDisposeObj(bdyInfo);
  MSDisposeObj(hdrInfo);
  MSDisposeObj(ftrInfo);
  MSDisposeObj(prn);
  MSStrDispose(curDev);
end;

function TPrintInfo.GetAvgCharWidth(numChars: integer; ptSize: integer): integer;
{- This function returns an appoximate width for a specified number of characters
   and a specific point size. This width can be used for cell widths when adding
   cells to a report }
var
  dc    : HDC;
  lf    : TLogFont;
  font  : HFont;
  tm    : TTextMetric;
begin
  dc:= GetDC(0);
  SetLogicalTwips(dc, 1.0); {- set to zoom 1.0 for consistent measurements }

  lf:= logFont;   {- make copy of font }
  if ptSize = 0 then
    ptSize:= defPtSize
  else if ptSize < 0 then
    ptSize:= defPtSize + Abs(ptSize); {- offset to default point size }
  lf.lfHeight:= -(ptSize * 20);

  font:= CreateFontIndirect(lf);
  if font = 0 then
    SelectObject(dc, GetStockObject(SYSTEM_FONT))
  else
    SelectObject(dc, font);

  GetTextMetrics(dc, tm);

  GetAvgCharWidth:= integer(Round(INIAvgColumnFluff * (numChars * tm.tmAveCharWidth)));

  if font <> 0 then
    DeleteObject(font);

  ReleaseDC(0, dc);
end;

function TPrintInfo.GetMaxCharWidth(numChars: integer; ptSize: integer): integer;
{- This function returns a maximum width for a specified number of characters
   and a specific point size. This width can be used for cell widths when adding
   cells to a report }
var
  dc    : HDC;
  lf    : TLogFont;
  font  : HFont;
  tm    : TTextMetric;
begin
  dc:= GetDC(0);
  SetLogicalTwips(dc, 1.0); {- set to zoom 1.0 for consistent measurements }

  lf:= logFont;   {- make copy of font }
  if ptSize = 0 then
    ptSize:= defPtSize
  else if ptSize < 0 then
    ptSize:= defPtSize + Abs(ptSize); {- offset to default point size }
  lf.lfHeight:= -(ptSize * 20);

  font:= CreateFontIndirect(lf);
  if font = 0 then
    SelectObject(dc, GetStockObject(SYSTEM_FONT))
  else
    SelectObject(dc, font);

  GetTextMetrics(dc, tm);

  GetMaxCharWidth:= (numChars * tm.tmMaxCharWidth);

  if font <> 0 then
    DeleteObject(font);

  ReleaseDC(0, dc);
end;

procedure TPrintInfo.SetRowSpacing(spacing: integer);
begin
  if (spacing >= 0) then
    rowSpacing:= spacing;
end;

function TPrintInfo.NeedToScan: boolean;
{- return true if the data needs to be repaginated. This is true when if following
   occurs: printer changes, paper size changes. (note: changing orientation also
   changes the paper size) }
var
  tSize  : TPoint;
  pIdx   : integer;
begin
  SelectedPaperSize(prn, pIdx, tSize);
  if longint(tsize) <> longint(pageSize) then
    NeedToScan:= true
  else if (prn^.status = ps_OK) and (strcomp(prn^.device, curDev) <> 0) then
    NeedToScan:= true
  else
    NeedToScan:= false;
end;

procedure TPrintInfo.SetMargins(left, top, right, bottom: integer);
{- set the margins (in LTwips) }
begin
  lMargin:= left;
  rMargin:= right;
  bMargin:= bottom;
  tMargin:= top;
end;

function TPrintInfo.NumPages: integer;
{- return number of pages required to print data}
begin
  NumPages:= totalPages;
end;

{---------------------------------------------[ TPrintWin ]--}

constructor TPrintWin.Init(aParent: PWindowsObject; aTitle: PChar;
                           aParentHWnd: HWnd; aPrintInfo: PPrintInfo);
begin
  inherited Init(aParent, aTitle, aParentHWnd);
  winTitle:= StrNew(aTitle);
  attr.menu:= LoadMenu(HInstance, MakeIntResource(MENU_MAIN));
  attr.style:= attr.style or WS_HSCROLL or WS_VSCROLL;
  attr.X:= CW_USEDEFAULT;
  attr.Y:= CW_USEDEFAULT;
  attr.w:= GetSystemMetrics(SM_CXFULLSCREEN);
  attr.h:= GetSystemMetrics(SM_CYFULLSCREEN);
  scroller:= New(PScroller, Init(@self, 1, 1, 100, 100));
  scroller^.trackMode:= false;
  scroller^.autoMode:= false;
  pInfo:= aPrintInfo;
  scrollUnit:= 1440 div 2;  { .5 inches }
  zoom:= 1.0;
  curPage:= 1;
  {$IFNDEF DEBUG}
  {- if debug is not on, then disable the debug menu item }
  RemoveMenu(attr.menu, CM_DEBUG, MF_BYCOMMAND);
  {$ENDIF}
end;

destructor TPrintWin.Done;
begin
  inherited Done;
  MSStrDispose(winTitle);
end;

procedure TPrintWin.SetupWindow;
var
  dc  : HDC;
  pIdx: integer;
begin
  inherited SetupWindow;
  ShowWindow(hWindow, SW_SHOWMAXIMIZED);

  dc:= GetDC(HWindow);
  ppiX:= GetDeviceCaps(dc, LOGPIXELSX);
  ppiY:= GetDeviceCaps(dc, LOGPIXELSY);
  ReleaseDC(HWindow, dc);

  {- set minimum margins for printing on screen. Since there is no
     minimum margin on the screen, just set it to the opposite of
     the distance from the edge of the paper, that way the real margins
     will look correct on the screen }
  SetRect(pInfo^.minMarg, -edgeX, -edgeY, 0, 0);
  if pInfo^.NeedToScan then
  begin
    SelectedPaperSize(pInfo^.prn, pIdx, pInfo^.pageSize);
    ScanPrintData(pInfo);
  end
  else
    SetPrintRect(pInfo);

  SetupScroller;

  SR(IDS_PAGE, pageStr, sizeof(pageStr)-1);
  StrCat(pageStr, ' ');
  SR(IDS_OF, ofStr, sizeof(pageStr)-1);
  StrCat(ofStr, ' ');

  ShowPageNum;
end;

procedure TPrintWin.SetupScroller;
{- set up a scroller in 'device' coordinates since this is how it likes to work.
   Also, take into account the zoom factor. The scroll units are [scrollUnit]
   LTwips. To convert to device coordinates; there are 1440 LTwips/inch and
   GetDeviceCaps(dc, LOGPIXELS) pixels/inch. Dividing we get LTwips/Pixel,
   which gives us the conversion factor to convert LTwips to device pixels }
var
  LTpPX  : Integer;  {- LTwips per Pixel (x direction) }
  LTpPY  : Integer;  {- LTwips per Pixel (y direction) }
  AUnit  : Integer;
  LTwidth: longint;  {- LTwips width }
  LTHeight: longint; {- LTwips height }
begin
  LTpPX:= 1440 div ppiX;   { Logical Twips per inch / Physical pixels per inch }
  LTpPY:= 1440 div ppiY;

  {- scroll unit in LTwips, apply zoom factor to get actual scroll unit
     in LTwips }
  AUnit:= Trunc(scrollUnit * zoom);

  {- tell scroller units (converting to device pixels) }
  scroller^.SetUnits(AUnit div LTpPX, AUnit div LTpPY);

  {- caclulate total page height in LTwips }
  LTWidth:= Trunc(pInfo^.pageSize.X * zoom);
  LTHeight:= Trunc(pInfo^.pageSize.Y * zoom);

  scroller^.SetRange(LTWidth div AUnit, LTHeight div AUnit);
end;

procedure TPrintWin.WMPaint(var msg: TMessage);
var
  ps    : TPaintStruct;
  curs  : HCursor;

  procedure PaintPaper(dc: HDC);
  var
    bdrPen  : HPen;
    LTpPX, LTpPY: integer;
  begin
    LTwipsPerPixel(dc, LTpPX, LTpPY);
    with pInfo^ do
    begin
      Rectangle(dc, edgeX, edgeY, pInfo^.pagesize.X + edgeX + LTpPX,
                    pageSize.Y + edgeY + LTpPY);

      bdrPen:= SelectObject(dc, CreatePen(PS_SOLID, 25, RGB(0,0,0)));
      MoveTo(dc, edgeX + pageSize.X + LTpPX, edgeY + 100);
      LineTo(dc, edgeX + pageSize.X + LTpPX, edgeY + pageSize.Y + LTpPY);
      LineTo(dc, edgeX + 100, edgeY + pageSize.Y + LTpPY);
      DeleteObject(SelectObject(dc, bdrPen));
    end;
  end;

begin
  curs:= SetCursor(LoadCursor(0, IDC_WAIT));
  BeginPaint(HWindow, ps);
  SetLogicalTwips(ps.hdc, zoom);

  if scroller <> nil then
    scroller^.BeginView(ps.HDC, ps);

  PaintPaper(ps.hdc);

  PaintData(ps.HDC, pInfo, curPage);

  if Scroller <> nil then
    scroller^.EndView;

  EndPaint(HWindow, ps);
  SetCursor(curs);
end;

procedure TPrintWin.CMDebug(var msg: TMessage);
begin
  DebugDump;
end;

procedure TPrintWin.DebugDump;
var
  fil  : text;
  pstr, p2 : array[0..100] of char;
  row   : prowinfo;

  procedure DumpRowInfo(info: PLCollection);
  var
    k: longint;
    sum : longint;
  begin
    sum:= 0;
    writeln(fil, 'Rows: ', Info^.count);
    for k:= 0 to info^.count - 1 do
    begin
      row:= info^.At(k);
      write(fil, '  Row: ', k:3, ' Ht: ', row^.height:4, ' MxDescent: ', row^.maxDescent:4);
      write(fil, ' NewPgRec: ', row^.newPageRec:4);
      if row^.newPageRec > 0 then
       sum:= 0;
      inc(sum, row^.height);
      write(fil, ' Tot height: ', sum);
      writeln(fil);
    end;
  end;

  procedure DumpData(pData: PPrintData);
  var
    k: longint;
    c: PCellRec;
  begin
    with pData^ do
    begin
      writeln(fil, 'Number of cells : ', data^.count);
      writeln(fil, 'Total recs      : ', TotalRecs);


      for k:= 0 to data^.count - 1 do
      begin
        c:= data^.At(k);
        write(fil, k:4, ' Flags: ', c^.flags:5, ' Width: ', c^.width:5);
        write(fil, ' ptSize: ', c^.ptSize:3);
        write(fil, ' RowDesc: ', c^.rowDesc:5);
        write(fil, ' GrpBreak: ', (c^.flags and r_GroupBreak <> 0):5);
        if c^.rowDesc then
        begin
          if c^.flags and r_StartGroup <> 0 then
            write(fil, ' STARTGROUP');
          if c^.flags and r_EndGroup <> 0 then
            write(fil, ' ENDGROUP  ');
        end;
        writeln(fil);
        write(fil, '     MaxPt: ', MaxLinePtSize(k):3);
        write(fil, ' MinPt: ', MinLinePtSize(k):3);
        write(fil, ' FirstRec: ', FirstRecInLine(k):4);
        write(fil, ' LastRec: ', LastRecInLine(k):4);
        write(fil, ' Is BOL: ', IsBOL(k));
        writeln(fil);
        write(fil, '     Data: ', c^.pstr);
        writeln(fil);
      end;
    end;
  end;

begin
  MessageBeep(0);
  assign(fil, 'prndump.txt');
  rewrite(fil);
  with pInfo^ do
  begin
    writeln(fil, 'Print Info stuff');
    writeln(fil, 'Page size    : ', pageSize.x, ' x ', pageSize.y);
    writeln(fil, 'MinMarg      : L=', minMarg.left, ' T=',minmarg.top, ' R=', minMarg.right, ' B=', minmarg.bottom);
    writeln(fil, 'Foot Size    : ', footSize);
    writeln(fil, 'Header size  : ', headSize);
    writeln(fil, 'Bottom limit : ', bLimit);
    writeln(fil, 'Right limit  : ', rLimit);
    writeln(fil, 'Start At     : X=', startAt.x, ' Y=', startAt.y);
    writeln(fil);

    writeln(fil, 'Header Row information');
    DumpRowInfo(pInfo^.hdrInfo);

    writeln(fil, 'Footer Row Information');
    DumpRowInfo(pInfo^.ftrInfo);

    writeln(fil, 'Body Row information');
    DumpRowInfo(pInfo^.bdyInfo);

    writeln(fil); writeln(fil);
    writeln(fil, 'Body information');
    DumpData(pInfo^.body);

  end;
  Close(fil);
  MessageBeep(0);
end;

procedure TPrintWin.CMTop(var msg: TMessage);
begin
  if curPage <> 1 then
  begin
    curPage:= 1;
    InvalidateRect(HWindow, nil, true);
    ShowPageNum;
  end;
end;

procedure TPrintWin.CMBottom(var msg: TMessage);
var
  k  : integer;
begin
  k:= pInfo^.NumPages;
  if curPage <> k then
  begin
    curPage:= k;
    InvalidateRect(HWindow, nil, true);
    ShowPageNum;
  end;
end;

procedure TPrintWin.CMGoto(var msg: TMessage);
var
  data : record
           num  : array[0..10] of char;
         end;
  p    : PDialog;
  e    : PSpinInt;
  k,code: integer;
begin
  Str(curPage, data.num);
  p:= New(PDialog, Init(@self, MakeIntResource(DLG_GOTO)));
  e:= New(PSpinInt, InitResource(p, IDC_GOTONUM, 6, 0, 1, pInfo^.NumPages, 1));
  p^.TransferBuffer:= @data;
  p^.EnableTransfer;
  if application^.ExecDialog(p) = IDOK then
  begin
    Val(data.num, k, code);
    if (code = 0) and (k >= 1) and (k <= pInfo^.NumPages) then
    begin
      curPage:= k;
      InvalidateRect(HWindow, nil, true);
      ShowPageNum;
    end;
  end;
end;

procedure TPrintWin.CMPrint(var msg: TMessage);
var
  rpt  : PPrnReport;
  tMarg: TRect;
  pIdx : integer;
  tPage: longint;
begin
  {- since the screen minimum margins are different, save them
     and get the minimum margins from the printer }
  CopyRect(tMarg, pInfo^.minMarg);  {- save current screen margins }
  tPage:= longint(pInfo^.pageSize);

  rpt:= New(PPrnReport, Init(winTitle, pInfo, true));

  pInfo^.prn^.Print(@self, rpt);

  MSDisposeObj(rpt);
  UpdateWindow(HWindow);

  {- restore screen margin information for screen display}
  {- need to rescan just in case user changed paper size
     while printing }
  CopyRect(pInfo^.minMarg, tMarg);
  SetPrintRect(pInfo);
  if pInfo^.NeedToScan then
  begin
    SelectedPaperSize(pInfo^.prn, pIdx, pInfo^.pageSize);
    ScanPrintData(pInfo);
  end;
  SetupScroller;
  scroller^.ScrollTo(0,0);

  InvalidateRect(HWindow, nil, true);
  ShowPageNum;
end;

procedure TPrintWin.CMSetup(var msg: TMessage);
var
  pIdx : integer;
  tSize: TPoint;
  pdev : PChar;
  p1,p2: array[0..100] of char;
begin
  if pInfo^.prn^.status <> ps_OK then
  begin
    if pInfo^.prn^.status = ps_InvalidDevice then
      pIdx:= IDS_INVALIDDEVICE
    else
      pIdx:= IDS_NOPRINTER;
    MessageBox(HWindow, SR(pIdx, p1, 100),
               SR(IDS_ERROR, p2, 100), MB_APPLMODAL or MB_ICONEXCLAMATION);
  end
  else
  begin
    tSize:= pInfo^.pageSize;
    pDev:= StrNew(pInfo^.prn^.device);

    pInfo^.prn^.Setup(@self);
    UpdateWindow(HWindow);

    pInfo^.pageSize:= tSize; {- restore temp }

    if pInfo^.NeedToScan then
    begin
      SelectedPaperSize(pInfo^.prn, pIdx, pInfo^.pageSize);
      ScanPrintData(pInfo);
      curPage:= 1;
      SetupScroller;
      scroller^.ScrollTo(0, 0);
      InvalidateRect(HWindow, nil, true);
      ShowPageNum;
    end;
    MSStrDispose(pDev);
  end;
end;

procedure TPrintWin.CMCloseWin(var msg: TMessage);
begin
  CloseWindow;
end;

procedure TPrintWin.GetWindowClass(var aWndClass: TWndClass);
begin
  inherited GetWindowClass(aWndClass);
  aWndClass.hbrBackground:= GetStockObject(LTGRAY_BRUSH);
end;

function TPrintWin.GetClassName: PChar;
begin
  GetClassName:= 'RCFPrnWin';
end;

procedure TPrintWin.SetZoom(aZoom: real);
begin
  zoom:= aZoom;
  SetupScroller;
  InvalidateRect(HWindow, nil, true);
end;

procedure TPrintWin.WMKeyDown(var msg: TMessage);
var
  k  : integer;
begin
  msg.result:= 0;
  with scroller^ do
  begin
    case msg.wParam of
      VK_F12:
        if ((GetKeyState(VK_CONTROL) and $80) <> 0) and
           ((GetKeyState(VK_SHIFT) and $80) <> 0) then
          DebugDump;
      VK_NEXT:   NextPage;
      VK_PRIOR:  PrevPage;
      VK_DOWN:   begin
                   if GetKeyState(VK_CONTROL) and $8000 > 0 then
                     k:= YPage
                   else
                     k:= YLine;
                   ScrollBy(0, k);
                 end;
      VK_UP:     begin
                   if GetKeyState(VK_CONTROL) and $8000 > 0 then
                     k:= YPage
                   else
                     k:= YLine;
                   ScrollBy(0, -k);
                 end;
      VK_LEFT:   begin
                   if GetKeyState(VK_CONTROL) and $8000 > 0 then
                     k:= XPage
                   else
                     k:= XLine;
                   ScrollBy(-k, 0);
                 end;
      VK_RIGHT:  begin
                   if GetKeyState(VK_CONTROL) and $8000 > 0 then
                     k:= XPage
                   else
                     k:= XLine;
                   ScrollBy(k, 0);
                 end;
      else
        DefWndProc(msg);
    end;
  end;
end;

procedure TPrintWin.WMCommand(var msg: TMessage);
begin
  if (msg.lParam = 0) then
  begin {- from menu }
    if (msg.wParam >= CM_25) and (msg.wParam <= CM_200) then
      SetZoom((msg.wParam - CM_25 + 1) * 0.25)
    else if msg.wParam = CM_NEXT then
      NextPage
    else if msg.wParam = CM_PREVIOUS then
      PrevPage
    else
      inherited WMCommand(msg);
  end
  else
    inherited WMCommand(msg);
end;

procedure TPrintWin.NextPage;
begin
  if curPage < pInfo^.NumPages then
  begin
    Inc(curPage);
    InvalidateRect(HWindow, nil, true);
  end;
  ShowPageNum;
end;

procedure TPrintWin.PrevPage;
begin
  if curPage > 1 then
  begin
    Dec(curPage);
    InvalidateRect(HWindow, nil, true);
  end;
  ShowPageNum;
end;

procedure TPrintWin.ShowPageNum;
{- show page number in window title }
var
  pstr  : array[0..200] of char;
  p2    : array[0..25] of char;
begin
  if winTitle <> nil then
    StrCopy(pstr, winTitle)
  else
    StrCopy(pstr, '');
  Str(curPage, p2);
  strcat(pstr, ' --- ');
  strcat(pstr, pageStr);
  strcat(pstr, p2);
  strcat(pstr, ' ');
  strcat(pstr, ofStr);
  IntlIntToStr(pInfo^.totalPages, p2, 25);
  strcat(pstr, p2);
  SetWindowText(HWindow, pstr);
end;

{------------------------------------------------------[ TPrnReport ]--}

constructor TPrnReport.Init(aTitle: PChar; aPrnInfo: PPrintInfo; promptSel: boolean);
begin
  inherited Init(aTitle);
  doPrompt:= promptSel;
  pInfo:= aPrnInfo;
end;

procedure TPrnReport.SetPrintParams(aDC: HDC; aSize: TPoint);
var
  pIdx : integer;
begin
  inherited SetPrintParams(aDC, aSize);
  PrintMargins(pInfo^.prn, pInfo^.minMarg);
  if pInfo^.NeedToScan then
  begin
    SelectedPaperSize(pInfo^.prn, pIdx, pInfo^.pageSize);
    ScanPrintData(pInfo);
  end
  else
  begin
    SelectedPaperSize(pInfo^.prn, pIdx, pInfo^.pageSize);
    SetPrintRect(pInfo);
  end;
end;

function TPrnReport.GetDialogInfo(var pages: Integer): boolean;
begin
  pages:= pInfo^.NumPages;
  GetDialogInfo:= doPrompt;
end;

function TPrnReport.HasNextPage(page: word): boolean;
begin
  HasNextPage:= (page <= pInfo^.NumPages);
end;

procedure TPrnReport.BeginPrinting;
var
  pIdx  : integer;
begin
(*  if pInfo^.NeedToScan then
  begin
    SetLogicalTwips(dc, 1.0);
    SelectedPaperSize(pInfo^.prn, pIdx, pInfo^.pageSize);
    PrintMargins(pInfo^.prn, pInfo^.minMarg);
    ScanPrintData(pInfo);
  end;
*)
end;

procedure TPrnReport.PrintPage(page: word; var rect: TRect; flags: word);
begin
  SetLogicalTwips(dc, 1.0);
  PaintData(dc, pInfo, page);
end;

{------------------------------------------------------[ TLineRec ]--}

constructor TCellRec.Init;
begin
  inherited Init;
  flags:= 0;
  width:= 0;
  width:= 0;
  pstr:= nil;
  ptSize:= 0;  {- default point size }
  rowDesc:= false;
end;

destructor TCellRec.Done;
begin
  inherited Done;
  MSStrDispose(pstr);
end;

{------------------------------------------------------[ TPrintData ]--}

constructor TPrintData.Init(aDefPtSize: integer);
begin
  inherited Init;
  data:= New(PLCollection, Init(5000, 1000));
  defPtSize:= aDefPtSize;
end;

destructor TPrintData.Done;
begin
  inherited Done;
  MSDisposeObj(data);
end;

procedure TPrintData.AddRow(aflags: longint; wrapMarg: integer);
var
  p   : PCellRec;
begin
  p:= New(PCellRec, Init);

  p^.rowDesc:= true;

  if data^.count = 0 then
    aflags:= aflags and not r_PageBreak;


  aFlags:= aFlags and not r_SysFlags;  {- dont allow user to specify system flags}

  {- cannot specify start and end group for one row }
  if (aFlags and r_StartGroup and r_EndGroup) <> 0 then
    aFlags:= aFlags and not (r_StartGroup or r_EndGroup);

  if (aFlags and r_CellWrap <> 0) and (wrapMarg > 0) then
    p^.width:= wrapMarg;

  p^.flags:= aFlags;
  data^.Insert(p);
end;

procedure TPrintData.AddEndGroupToLastRow;
var
  p   : PCellRec;
  k   : longint;
begin
  k:= data^.count;
  if k > 0 then
  begin
    repeat
      p:= data^.At(k - 1);
      if p^.rowDesc then
      begin
        p^.flags:= (p^.flags or r_EndGroup) and not r_StartGroup;
        k:= 0;
      end;
      Dec(k);
    until k <= 0;
  end;
end;

function TPrintData.AddCell(aStr: PChar; aWidth, flags: longint; aPtSize: integer): integer;
{- add a string to the current line. The string is appended to the current
   line. aWidth specifies the "cell" width for the string being added.
   if aPtSize = 0 then the default font height for the device will be used.
   returns -1 if no row description record has been added yet.
           -2 if a memory error occured.
            0 if everything is OK }
var
  p  : PCellRec;
  rec: integer;
  r  : PCellRec;
  ret: integer;
begin
  ret:= 0;
  if data^.count = 0 then
    ret:= -1;

  if ret = 0 then
  begin
    AddCell:= 0;
    p:= New(PCellRec, Init);
    if p = nil then
      ret:= -2;
  end;

  if ret = 0 then
  begin
    p^.rowDesc:= false;
    if (aStr = nil) or (StrLen(aStr) = 0) then
      p^.pstr:= nil
    else
      p^.pstr:= StrNew(aStr);
    if p^.pstr = nil then
    begin
      ret:= -2;
      MSDisposeObj(p);   {- remove the cell reference just created above}
    end;
  end;

  if ret = 0 then
  begin
    if aPtSize > 0 then
      p^.ptSize:= aPtSize
    else if aPtSize < 0 then
      p^.ptSize:= defPtSize + Abs(aPtSize)   {- offset to default point size }
    else  {- zero specifies default font }
      p^.ptSize:= defPtSize;

    p^.width:= aWidth;
    {- set default alignment for text (top/left) }
    if flags and (c_Top or c_Bottom or c_VCenter or c_Baseline) = 0 then
      flags:= flags or c_Top;
    if flags and (c_Left or c_Right or c_Center) = 0 then
      flags:= flags or c_Left;

    p^.flags:= flags;

    data^.Insert(p);
    if data^.status <> LCE_OK then
    begin
      ret:= -2;
      MSDisposeObj(p);
    end;
  end;

  if ret = 0 then
  begin
    {- find row description record }
    r:= data^.At(FirstRecInLine(data^.count - 1));
    if r^.flags and r_CenterRow <> 0 then
    begin
      {- if row center is specified, then remove invalid flags from the cell that
        was just added.}
      p^.flags:= p^.flags and
                not (c_PageCenter or c_PageLeft or c_PageRight or c_Stretch);
    end;
  end;

  AddCell:= ret;
end;

function TPrintData.MaxLinePtSize(aRecNum: longint): integer;
{- returns the maximum height (in points) for a line.
   aRecNum : This parameter indicates a record in the list data stream.
   returns : -1    if an invalid record number is passed,
              0    for default line height to be used
              > 0  the maximum point size for all cells in the line }
var
  max : integer;
  k   : longint;
  p   : PCellRec;
begin
  MaxLinePtSize:= -1;
  if (aRecNum >= 0) and (aRecNum < data^.count) then
  begin
    k:= FirstRecInLine(aRecNum);
    if k >= 0 then
    begin
      max:= 0;
      if k < data^.count - 1 then {- if not last record }
      begin
        Inc(k);
        {- now k is pointing the first text cell record for the line
           containing aRecNum }
        p:= data^.At(k);
        if not p^.rowDesc then
          repeat
            if p^.ptSize > max then
              max:= p^.ptSize;
            Inc(k);
            if k < data^.count then
              p:= data^.At(k);
          until (k >= data^.count) or p^.rowDesc;
      end;
      if max = 0 then
        max:= defPtSize;
      MaxLinePtSize:= max;
    end;
  end;
end;

function TPrintData.MinLinePtSize(aRecNum: longint): integer;
{- returns the minimum height (in points) for a line.
   aRecNum : This parameter indicates a record in the list data stream.
   returns : -1    if an invalid record number is passed,
              >= 0  the minimum point size for all cells in the line (0=default)}
var
  min : integer;
  k   : longint;
  p   : PCellRec;
begin
  MinLinePtSize:= -1;
  if (aRecNum >= 0) and (aRecNum < data^.count) then
  begin
    k:= FirstRecInLine(aRecNum);
    if k >= 0 then
    begin
      min:= maxInt;
      if k < data^.count - 1 then
      begin
        Inc(k);
        {- now k is pointing the first cell record for the line
           containing aRecNum }
        p:= data^.At(k);
        if not p^.rowDesc then
        begin
          repeat
            if p^.ptSize < min then
              min:= p^.ptSize;
            Inc(k);
            if k < data^.count then
              p:= data^.At(k);
          until (k >= data^.count) or p^.rowDesc;
        end
        else
          min:= 0;
      end
      else
        min:= 0;
      MinLinePtSize:= min;
    end;
  end;
end;

function TPrintData.TotalRecs: longint;
{- return the total number of print records in the data stream. }
begin
  TotalRecs:= data^.count;
end;

function TPrintData.LastRecInLine(aRecNum: longint): longint;
{- return the last cell record for the line containing aRecNum.
   return -1 if aRecNum is invalid }
var
  k  : longint;
  p  : PCellRec;
begin
  if (aRecNum >= 0) and (aRecNum < data^.count) then
  begin
    k:= aRecNum;
    if k < (data^.count - 1) then
    begin
      Inc(k);
      repeat
        p:= data^.At(k);
        if not p^.rowDesc then
          Inc(k);
      until (k >= data^.count) or p^.rowDesc;
      LastRecInLine:= k - 1;
    end
    else
      LastRecInLine:= k;  {- aRecNum was last rec in list }
  end
  else
    LastRecInLine:= -1;
end;

function TPrintData.TotalRowWidth(aRecNum: longint): integer;
{- calculate the total user specified width of a row. Does not take into account
   cells that are to be stretched at print time }
var
  k  : longint;
  w  : integer;
  c  : PCellRec;
begin
  w:= 0;
  if (aRecNum >= 0) and (aRecNum < data^.count) then
    for k:= FirstRecInLine(aRecNum) + 1 to LastRecInLine(aRecNum) do
    begin
      c:= data^.At(k);
      Inc(w, c^.width + (intMarg * 20));
    end;
  TotalRowWidth:= w;
end;

function TPrintData.FirstRecInLine(aRecNum: longint): longint;
{- return the first cell record for the line containing aRecNum.
   this rec will be the Line Description record for the line
   return -1 if aRecNum is invalid }
var
  k  : longint;
  p  : PCellRec;
begin
  if (aRecNum >= 0) and (aRecNum < data^.count) then
  begin
    k:= aRecNum;
    if k >= 0 then
    begin
      repeat
        p:= data^.At(k);
        if not p^.rowDesc then
          Dec(k);
      until (k < 0) or p^.rowDesc;
      FirstRecInLine:= k;
    end
    else
      FirstRecInLine:= -1;
  end
  else
    FirstRecInLine:= -1;
end;

function TPrintData.GetCellStrSize(aRecNum: longint): integer;
{- return the size of the text stored in the specified cell }
var
  p  : PCellRec;
begin
  if (aRecNum >= 0) and (aRecNum < data^.count) then
  begin
    p:= data^.At(aRecNum);
    if p^.pstr <> nil then
      GetCellStrSize:= StrLen(p^.pstr) + 1
    else
      GetCellStrSize:= 0;
  end
  else
    GetCellStrSize:= 0;
end;

function TPrintData.GetCellStr(aRecNum: longint; aStr: PChar; maxLen: integer): PChar;
var
  p  : PCellRec;
begin
  aStr^:= #0;
  if (aRecNum >= 0) and (aRecNum < data^.count) then
  begin
    p:= data^.At(aRecNum);
    if p^.pstr <> nil then
      StrLCopy(aStr, p^.pstr, maxLen);
  end;
  GetCellStr:= aStr;
end;

function TPrintData.IsBOL(aRecNum: longint): boolean;
{- return true if specified record is the beginning of line rec (BOL) }
begin
  if (aRecNum >= 0) and (aRecNum < data^.count) then
    IsBOL:= PCellRec(data^.At(aRecNum))^.rowDesc
  else
    IsBOL:= false;
end;

{-------------------------------------------[ TPrnPrevDlg ]--}

constructor TPrnPrevDlg.Init(aParent: PWindowsObject; aPInfo: PPrintInfo; aRptTitle: PChar);
begin
  inherited Init(aParent, MakeIntResource(DLG_PRINTPREV));
  pInfo:= aPInfo;
  rptTitle:= aRptTitle;
  if pInfo = nil then
    Fail;
end;

procedure TPrnPrevDlg.SetupWindow;
var
  pstr  : array[0..100] of char;
begin
  inherited SetupWindow;
  SendDlgItemMsg(IDC_TITLE, WM_SETTEXT, 0, longint(rptTitle));
  ShowPrinterName;
end;

procedure TPrnPrevDlg.ShowPrinterName;
var
  pstr  : array[0..100] of char;
begin
  if pInfo^.prn^.device <> nil then
  begin
    strCopy(pstr, pInfo^.prn^.device);
    StrCat(pstr, ', ');
    StrCat(pstr, pInfo^.prn^.port);
  end
  else
    SR(IDS_NOPRINTER, pstr, sizeof(pstr)-1);
  SendDlgItemMsg(IDC_PNAME, WM_SETTEXT, 0, longint(@pstr));
end;

procedure TPrnPrevDlg.EnableButtons;
begin
  inherited EnableButtons;
  ShowPrinterName;
end;

procedure TPrnPrevDlg.IDPrint(var msg: TMessage);
begin
  if pInfo^.prn^.status = ps_OK then
  begin
    PrintPrim(@self, pInfo, rptTitle, false);
    EndDlg(IDOK);
  end
  else
    ShowPrinterError(hWindow, pInfo^.prn);
end;

procedure TPrnPrevDlg.IDPreview(var msg: TMessage);
begin
  if pInfo^.prn^.status = ps_OK then
    application^.MakeWindow(
         New(PPrintWin, init(@self, rptTitle, HWindow, pInfo)))
  else
    ShowPrinterError(hWindow, pInfo^.prn);
end;

procedure TPrnPrevDlg.IDSetup(var msg: TMessage);
{- display printer setup dialog }
begin
  if pInfo^.prn^.status = ps_OK then
  begin
    pInfo^.prn^.Setup(@self);
    ShowPrinterName;
  end
  else
    ShowPrinterError(hWindow, pInfo^.prn);
end;

END.

{- rcf }
