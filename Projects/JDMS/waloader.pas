Program WALoader;
{$R WALOADER.RES}

Uses
  WObjects,
  WinTypes,
  WinProcs,
  Strings,
  WLWaGpib,
  AutoLdr,
  InterLdr,
  WLDMStrg;

Const
  cm_WALoaderExec      = 101;
  cm_WALoaderQuit      = 102;
  wm_AutoLoad          = wm_User+100;

  WaBase               = 6100;

Type
  { W/A Loader Application object }
  TWALoaderApp = object(TApplication)
    procedure InitMainWindow; virtual;
  End;

  { Application main window }
  PMainWindow = ^TMainWindow;
  TMainWindow = object(TWindow)
    LogoDisplayed: Boolean;
    InteractiveMode: Boolean;
    AutoModeStarted: Boolean;
    Procedure Paint(PaintDC: HDC; Var PaintInfo: TPaintStruct); Virtual;
    Constructor Init(AParent: PWindowsObject; TheTitle: PChar);
    Procedure SetupWindow; virtual;
    function  CanClose: Boolean; virtual;
    function  GetClassName: PChar; virtual;
    procedure GetWindowClass(Var AWndClass: TWndClass); virtual;
    procedure AutoLoadFiles(Var Msg: TMessage); virtual wm_AutoLoad;
    procedure InteractiveWALoader(Var Msg: TMessage); virtual cm_First + cm_WALoaderExec;
    procedure QuitWALoader(Var Msg: TMessage); virtual cm_First + cm_WALoaderQuit;
    procedure WMKillFocus(Var Msg: TMessage); virtual wm_First + wm_KillFocus;
  End;

Var
  WALoaderApp       : TWALoaderApp;
  WALoaderWin       : HWnd;

Procedure TMainWindow.SetupWindow;
Begin
  TWindow.SetupWindow;
{  SetUpWaType; }
End;

Function TMainWindow.CanClose: Boolean;
Var
  Rslt: LongInt;
  PStr, PStr2: Array [0..50] Of Char;

Begin
  If Not InteractiveMode Then
    CanClose:= True
  Else
  Begin
    GetStr (WaBase + 16, PStr, 50);     { OK to exit Walkaway Loader? }
    GetStr (WaBase + 17, PStr2, 50);    { Quit Program }
    Rslt:= MessageBox(HWindow, PStr, PStr2, mb_IconHand + mb_YesNo);
    if Rslt = id_Yes Then
      CanClose:= True
    Else
      CanClose:= False;
  End;
End;

Function TMainWindow.GetClassName: PChar;
Begin
  GetClassName:= 'TMainWindow';
End;

Procedure TMainWindow.GetWindowClass(Var AWndClass: TWndClass);
Begin
  TWindow.GetWindowClass(AWndClass);
  AWndClass.hIcon:= LoadIcon(hInstance,'WALOADER');
End;

Procedure TMainWindow.Paint;
Var
  Logo       : HBitMap;
  SaveDC     : HDC;
Begin
  TWindow.Paint(PaintDC,PaintInfo);
  If (Not LogoDisplayed) and (InteractiveMode) Then
  Begin
    Logo:= LoadBitmap(HInstance,'LOGO');
    SaveDC := CreateCompatibleDC(PaintDC);
    SelectObject(SaveDC,Logo);
    BitBlt(PaintDC, 70, 110, 500, 240, SaveDC, 0, 0, SrcCopy);
    DeleteDC(SaveDC);
    LogoDisplayed:= True;
    DeleteObject (Logo);
  End
  Else if (Not InteractiveMode) and (Not AutoModeStarted) Then
  Begin
    PostMessage(HWindow,wm_AutoLoad, 0, 0);
    AutoModeStarted:= True;
  End;
End;

Procedure TMainWindow.WMKillFocus(Var Msg: TMessage);
Begin
  If LogoDisplayed Then
    InvalidateRect(HWindow,Nil,True);
  DefWndProc(Msg)
End;

{ TMainWindow Initialization }
constructor TMainWindow.Init(AParent: PWindowsObject; TheTitle: PChar);
Begin
  InteractiveMode:= False;
  TWindow.Init(AParent, TheTitle);
  If (Pos('/I',ParamStr(1)) <> 0) or (Pos('/i',ParamStr(1)) <> 0) Then
  Begin
    InteractiveMode:= True;
    Attr.Menu := LoadMenu(HInstance, 'MAIN_MENU');
  End;
  Attr.Style:= ws_Overlapped + ws_Visible + ws_Caption + ws_SysMenu + ws_MinimizeBox + ws_MaximizeBox;
  Attr.X:= 0; Attr.Y:= 0;
  Attr.W:= GetSystemMetrics(sm_CXScreen);
  Attr.H:= GetSystemMetrics(sm_CYScreen);
  LogoDisplayed:= False;
  AutoModeStarted:= False;
End;

Procedure TMainWindow.QuitWALoader(Var Msg: TMessage);
Begin
  CloseWindow;
End;


Procedure TMainWindow.InteractiveWALoader(Var Msg:TMessage);
Begin
  ExecInteractiveLdr(HWindow,@Self);
End;

Procedure TMainWindow.AutoLoadFiles(Var Msg: TMessage);
Begin
  ExecAutoLoad(HWindow,@Self);
  CloseWindow;
End;

Procedure TWALoaderApp.InitMainWindow;
Var
   PStr: Array [0..50] Of Char;

Begin
  GetStr (WaBase + 01, PStr, 50);       { Walkaway Loader }
  MainWindow := New(PMainWindow, Init(nil,PStr));
End;

Begin
  If WaID <> -1 Then
  Begin
	 WALoaderApp.Init('WALoaderApp');
	 WALoaderWin:= WALoaderApp.MainWindow^.HWindow;
	 WALoaderApp.Run;
	 WALoaderApp.Done;
  End;
End.
