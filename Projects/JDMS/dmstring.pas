unit DMString;

{- This module includes the common string resources and common string constants}

INTERFACE

{$R DMSTRING.RES}
{$I DMSTRING.INC}

function BoolToStr(boolVal : boolean; dest : PChar; maxLen : word) : PChar;
function StrToBool(testStr : PChar; var errCode : integer) : boolean;

IMPLEMENTATION

uses
  MScan,
  DMSDebug,
  APITools,
  DBTypes,
  Strings;

var
  exitSave  : Pointer;
  yesNoSize : word;
  yesStr    : PChar;
  noStr     : PChar;

{-----------------------------------------------------------------------------}
{  Converts a boolean value of true/false to a string of 'Yes'/'No'.          }
{                                                                             }
{  Returns : PChar containing 'Yes' if boolean value is true, otherwise 'No'. }
{  InParms : boolVal - boolean value being converted to a string.             }
{            maxLen  - maximum length of return value.                        }
{  OutParms: dest    - Placeholder for return value.                          }
{-----------------------------------------------------------------------------}
function BoolToStr(boolVal : boolean; dest : PChar; maxLen : word) : PChar;
begin
  if boolVal then
    StrLCopy(dest, yesStr, maxLen)
  else
    StrLCopy(dest, noStr, maxLen);
  BoolToStr:= dest;
end;

{-----------------------------------------------------------------------------}
{  Converts a string of 'Yes'/'No' to a boolean value.                        }
{                                                                             }
{  Returns : True if string = 'Yes' (case insensitive), otherwise False.      }
{  InParms : testStr - string to test.                                        }
{  OutParms: errCode - Zero if no error, otherwise non-zero.                  }
{-----------------------------------------------------------------------------}
function StrToBool(testStr : PChar; var errCode : integer) : boolean;
var
  retVal : boolean;
begin
  errCode := 0;
  retVal:= (StrIComp(yesStr, testStr) = 0);
  if (not retVal) then
    errCode:= StrIComp(noStr, testStr);
  StrToBool:= retVal;
end;

procedure ExitRoutine; far;
begin
  exitProc:= exitSave;
  MSFreeMem(yesStr, yesNoSize);
  MSFreeMem(noStr, yesNoSize);
end;

BEGIN
  exitSave:= ExitProc;
  ExitProc:= @ExitRoutine;
  yesNoSize:= DBStrSizeForType(dbBoolean, 0);
  GetMem(yesStr, yesNoSize);
  GetMem(noStr, yesNoSize);
  SR(IDS_YES, yesStr, yesNoSize-1);
  SR(IDS_NO, noStr, yesNoSize-1);
END.
