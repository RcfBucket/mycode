unit Threshld;
{----------------------------------------------------------------------------}
{  Module Name  : Threshld.PAS                                                  }
{  Programmer   : MJS                                                        }
{  Date Created : 03/95                                                      }
{                                                                            }
{  Purpose - This module implements the setting of the database thresholds.  }
{            It currently uses the DMS.ini file to store the thresholds.     }
{            This module also implements the database statistics feature.    }
{            This module calculates the file sizes and percentages of disk   }
{            used for the patient, specimen, isolate and test results files. }
{  Assumptions -                                                             }
{  None                                                                      }
{                                                                            }
{  Initialization -                                                          }
{  None                                                                      }
{                                                                            }
{  Revision History -                                                        }
{                                                                            }
{  Version  Date      Who    Did What                                        }
{  -------  --------  ------ ----------------------------------------------- }
{  1.00     03/95     MJS    Created unit.                                   }
{                                                                            }
{----------------------------------------------------------------------------}

INTERFACE

uses
  OWindows;

procedure DBOptions(aParent:PWindowsObject);
procedure DatabaseStatistics(aParent: PWindowsObject);

IMPLEMENTATION

uses
  MScan,
  APITools,
  IniLib,
  Objects,
  DBFile,
  DBIDs,
  DBLib,
  DBTypes,
  DMSDebug,
  WinDos,
  DMString,
  WinProcs,
  WinTypes,
  Win31,
  DLGLib,
  Strings,
  ChkSpace;

{$R THRESHLD.RES}
{$I THRESHLD.INC}

type
  PDBStatusDialog = ^TDBStatusDialog;
  TDBStatusDialog = object(TCenterDlg)
    procedure SetupWindow; virtual;
    private
    function GetDiskUsed : longint;
    procedure ShowPercentUsed(diskUsed : longint);
    function ShowFileSize(fileName:pchar):longint;
    procedure ShowPercent(fSize: longint; cntlID: longint);
    procedure ShowRecordCount(fileName: PChar; cntlID: longint);
  end;

  PDBOptDlg = ^TDBOptDlg;
  TDBOptDlg = object(TCenterDlg)
    constructor Init(aParent:PWindowsObject);
    procedure SetupWindow; virtual;
    function CanClose: boolean; virtual;
    private
    function CheckRange(threshold: word; cntlID: longint): boolean;
    function GetThresholdSetting(cntlID: longint): byte;
  end;

var
  dbOptDlg : PDBOptDlg;

constructor TDBOptDlg.Init(aParent: PWindowsObject);
begin
  if not inherited Init(aParent, 'THRESHOLD') then
  begin
    Done;
    Fail;
  end;
end;

{-----------------------[ SetUpWindow ]--------------------------------------}
{  This procedure sets up the dialog box.  It calls WarningThreshold and     }
{  PreventThreshold which can be found in ChkSpace.pas.                      }
{----------------------------------------------------------------------------}
procedure TDBOptDlg.SetupWindow;
var
  tempArray 	: array [0..10] of char;
begin
  inherited SetUpWindow;
  Str(warningThreshold, tempArray);
  SetDlgItemText(hWindow, IDC_WARNTHRESHOLD, tempArray);
  Str(preventThreshold, tempArray);
  SetDlgItemText(hWindow, IDC_PREVENTTHRESHOLD, tempArray);
end;

function TDBOptDlg.CheckRange(threshold : word;
                              cntlID    : longint) : boolean;
var
  tempMsg  : array [0..255] of char;
begin
  CheckRange := false;
  if (threshold < minThreshold) or (threshold > maxThreshold) then
  begin
    SR(IDS_OUTOFRANGE, tempMsg, 255);
    ErrorMsg(hWindow, '', tempMsg);
    FocusCtl(hWindow, cntlID);
    CheckRange := true;
  end;
end;

function TDBOptDlg.GetThresholdSetting(cntlID: longint): byte;
var
  tempStr    : string;
  tempArray  : array [0..255] of char;
  tempVal    : byte;
  code       : integer;
begin
  GetDlgItemText(hWindow, cntlID, tempArray, 255);
  Val(tempArray, tempVal, code);
  GetThresholdSetting := tempVal;
end;

{------------------------------[ CanClose ]----------------------------------}
{  This function checks the thresholds and display a warning if the          }
{  thresholds are out of range or if the prevention threshold is smaller     }
{  than the warning threshold.                                               }
{                                                                            }
{  OutParms: true if the dialog box may close, and false otherwise.          }
{----------------------------------------------------------------------------}
function TDBOptDlg.CanClose;
var
  code 		: integer;
  tempWarning   : word;
  tempPrevent   : word;
  tempArray     : array [0..63] of char;
begin
  CanClose := false;
  if inherited CanClose then
  begin
    tempWarning := GetThresholdSetting(IDC_WARNTHRESHOLD);
    if CheckRange(tempWarning, IDC_WARNTHRESHOLD) then
      exit;
    tempPrevent := GetThresholdSetting(IDC_PREVENTTHRESHOLD);
    if CheckRange(tempPrevent, IDC_PREVENTTHRESHOLD) then
      exit;
    if TempPrevent <= TempWarning then
    begin
      SR(IDS_WARNINGMSG, tempArray, 64);
      ErrorMsg(hWindow, '', tempArray);
      FocusCtl(hWindow, IDC_WARNTHRESHOLD);
      exit;
    end;
    warningThreshold := tempWarning;
    preventThreshold := tempPrevent;
    WriteWarningThreshold(tempWarning);
    WritePreventionThreshold(tempPrevent);
    CanClose := true;
  end;
end;

procedure TDBStatusDialog.SetupWindow;
var
  fSize 	: longint;
  diskUsed	: longint;

begin
  inherited SetUpWindow;

  ShowRecordCount(DBPATFile, IDC_PATIENTCOUNT);
  fSize := ShowFileSize(DBPATFile + datFileExt);
  ShowPercent(fSize,IDC_PATIENTPERCENT);

  ShowRecordCount(DBSPECFile,IDC_SPECIMENCOUNT);
  fSize := ShowFileSize(DBSPECFile + datFileExt);
  ShowPercent(fSize,IDC_SPECIMENPERCENT);

  ShowRecordCount(DBISOFile,IDC_ISOLATECOUNT);
  fSize := ShowFileSize(DBISOFile + datFileExt);
  ShowPercent(fSize,IDC_ISOLATEPERCENT);

  ShowRecordCount(DBRESFile,IDC_RESULTCOUNT);
  fSize := ShowFileSize(DBRESFile + datFileExt);
  ShowPercent(fSize,IDC_RESULTPERCENT);

  ShowPercentUsed(GetDiskUsed);
end;

function TDBStatusDialog.GetDiskUsed: longint;
var
  diskUsed      : longint;
  df : longint;
begin
  df := DiskFree(0);
  diskUsed := (DiskSize(0) - df) div 1024;
  GetDiskUsed := diskUsed;
end;

procedure TDBStatusDialog.ShowPercentUsed(diskUsed : longint);
var
  strArray      : array [0..31] of char;
begin
  Str(((diskUsed * 100) div (DiskSize(0) div 1024)) + 1, strArray);
  SendDlgItemMsg(IDC_DISKPERCENT, WM_SETTEXT, word(0), longint(@strArray));
end;

procedure TDBStatusDialog.ShowRecordCount(fileName: PChar; cntlID: longint);
var
  dbFile 	: PDBFile;
  strArray      : array [0..31] of char;
begin
  dbFile:= New(PDBFile, Init(fileName, '', dbOpenNormal));
  if dbFile <> nil then
  begin
    Str(dbFile^.dbc^.NumRecords, strArray);
    MSDisposeObj(dbFile);
  end;
  SendDlgItemMsg(cntlID, WM_SETTEXT, word(0), longint(@strArray));
end;

function TDBStatusDialog.ShowFileSize(fileName: PChar): longint;
var
  fSize        : longint;
  tempFile     : file of array[0..1023] of Byte;
  tempErrMode  : integer;
  tempFilMode  : integer;
begin
  tempErrMode:= SetErrorMode(SEM_FAILCRITICALERRORS);
  tempFilMode:= FileMode;
  fSize := 0;

  {$IFOPT I+}
    {$DEFINE CHECKI}
  {$ENDIF}
  {$I-}

  Assign(tempFile,fileName);
  Reset(tempFile);
  if IOResult = 0 then
    fSize := FileSize(tempFile);
  Close(tempFile);
  if IOResult = 0 then;

  {$IFDEF CHECKI}
    {$I+}
    {$UNDEF CHECKI}
  {$ENDIF}

  ShowFileSize:= fSize;
  FileMode:= tempFilMode;
  SetErrorMode(tempErrMode);
end;

procedure TDBStatusDialog.ShowPercent(fSize: longint; cntlID: longint);
var
  strArray      : array [0..31] of char;
begin
  Str(((fSize * 100) div 32767) + 1, strArray);
  SendDlgItemMsg(cntlID, WM_SETTEXT, word(0), longint(@strArray));
end;


{------[ Interfaced Procedures ]--}

procedure DatabaseStatistics(aParent: PWindowsObject);
begin
  application^.ExecDialog(new(PDBStatusDialog, Init(aParent, 'STATISTICS')));
end;

procedure DBOptions(aParent:PWindowsObject);
begin
  dbOptDlg := New(PDBOptDlg, Init(aParent));
  if dbOptDlg <> nil then
    Application^.ExecDialog(dbOptDlg);
end;

END.
