{----------------------------------------------------------------------------}
{  Module Name  : DBTYPES.PAS                                                }
{  Programmer   : EJ                                                         }
{  Date Created : 10/28/94                                                   }
{                                                                            }
{  Purpose -                                                                 }
{  This module provides all constants and data structures for the DBLib      }
{  library.                                                                  }
{                                                                            }
{  Assumptions -                                                             }
{  Before blindly modifying and constants, be sure you know what affect      }
{  it will have in the dbLib system and in BTRIEVE.                          }
{                                                                            }
{  Initialization -                                                          }
{  None.                                                                     }
{                                                                            }
{  Revision History -                                                        }
{                                                                            }
{  Version  Date      Who    Did What                                        }
{  -------  --------  ------ ----------------------------------------------- }
{  1.00     11/01/94  EJ     Initial release.                                }
{                                                                            }
{----------------------------------------------------------------------------}
unit DBTypes;

INTERFACE

uses
  btrconst;

type
  TSeqNum = longint;  {- type used for sequence numbers and autoInc fields}

const
  dbHeader            = 'dbLib';  {- header used for .DEF files }
  dbVersion           = '03.00';  {- version for dbLib stored in .DBI files}

  datFileExt          = '.DBD';   {- BTRIEVE data filename extention }
  fileDescExt         = '.DBI';   {- dbLib information file extension }
  tempDBFileName      = 'DBTEMP'; {- filename of temporary database file }

  maxPathLen          = 100;  {- maximum length of a path }
  maxFldNameLen       = 25;   {- maximum length of a field name }
  maxKeyNameLen       = 25;   {- maximum length of a key name }
  maxAssocNameLen     = 25;   {- maximum length of a assoc name }
  maxDelAssocNameLen  = 25;   {- maximum length of a del assoc name }
  maxFileDescLen      = 48;   {- maximum length of a DB system description }
  maxFileNameLen      = 8;    {- DOS limit }

  {- record size constants }
  minRecordSize       = 4;  {- minimum record size }
  maxRecordSize       = MAX_FIXED_RECORD_LEN - sizeof(TSeqNum);
  {- maximum record size (for fixed length records)}
  {- subtract sequence size for system defined sequence number in all
     records}

  maxKeyLen           = 255;  {- maximum length of a key (BTR requirement)}
  maxSegs             = 6;    {- maximum number of segments allowed in a key }
  maxSysKeys          = 9;    {- total number of keys allowed in a file }
  { 9 * 6 = 54 total key segments allowed }
  maxKeys             = maxSysKeys-1;  {- maximum number of User keys per
                                          file. One is subtracted to make
                                          room for system defined key }
  maxPageSize         = 4096; {- must be multiple of 512 and <= 4096 }
  maxFldsPerRecord    = 30;   {- maximum number of fields in a record }
  maxAssocs           = 16;   {- maximum number of assocs per file }
  maxDelAssocs        = 16;   {- maximum number of del assocs per file }
  maxConstLen         = 25;   {- maximum length of assoc constants }
  maxAssocFlds        = 4;    {- maximum number of fields in an assoc }
  maxUDFlds           = 10;   {- maximum number of user defined fields }

  dbEmptyVal : byte   = 0;    {- value used to clear fields and records }

  {- flags used in file.userFlag }
  db_UDFile           = $0001; {- file is User Defined }
  db_MnemonicFile     = $0002; {- file is a Mnemonic file }
  db_UserEdit         = $0004; {- file is User Editable }
  db_ReadOnly         = $0008; {- file is Read Only, user cannot add/update records }
  db_ReportFile       = $0040; {- file is a report file }
  db_ExtractFile      = $0080; {- file is used when extracting data }
  {- flags used in fld.userFlag }
  db_UD               = $0001; {- field is User Defined }
  db_Mnemonic         = $0002; {- field is Mnemonic }
  db_Hidden           = $0004; {- field is hidden }
  db_UDRec            = $0008; {- field is flag for User Defined record }
  db_MNMID            = $0010; {- mnemonic ID field }
  db_MNMDescr         = $0020; {- mnemonic description field }
  db_Flags            = $1000; {- flags field }
  db_FreeText         = $4000; {- free text field }
  db_ExpandID         = $8000; {- expands IDs in the form X.n }

  {- bit positions in Flags byte field (zero based) }
  db_ArchivePos       = 7;

  {- flags used in fld.sysFlag }

  {- valid open modes (used in TDBCursor.Init) }
  dbOpenNormal        = NORMAL;
  dbOpenReadOnly      = READONLY;
  dbOpenExclusive     = EXCLUSIVE;

  {- useful error constants }
  dbEOF               = B_END_OF_FILE;
  dbTOF               = B_END_OF_FILE;
  dbNotFound          = B_KEY_VALUE_NOT_FOUND;

type
  TDBFldTypes = (dbInvalid,
                 dbInteger,   { 2 bytes }
                 dbLongInt,   { 4 bytes }
                 dbWord,      { unsigned 2 bytes }
                 dbBoolean,   { 1 byte boolean value }
                 dbByte,      { 1 byte }
                 dbShortInt,  { 1 byte unsigned }
                 dbChar,      { 1 byte (char) }
                 dbSingle,    { 4 byte real }
                 dbDouble,    { 8 byte real }
                 dbDate,      { 4 byte date }
                 dbTime,      { 4 byte time }
                 dbString,    { user specified length, length byte at
                                beginning of string }
                 dbCode,      { same as String but key segments on this
                                type of field are case insensitive }
                 dbZString,   { zero terminated string of user defined length }
                 dbZCode,     { same as ZString but key segments on this
                                type of field are case insensitive }
                 dbBlock,     { user defined block of data }
                 dbSeqRef,    { sequence reference }
                 dbAutoInc    { auto increment field }
                );

  TDBDelActions = (NO_ACTION,
                   DISALLOW_DELETE,
                   DELETE_ASSOCIATED
                  );

const
  dbFirstType = dbInteger;
  dbLastType  = dbAutoInc;

type

  {- file information header. Describes a dbLib file }
  TFileInfo = record
    fName     : array[0..maxFileNameLen] of char;
    desc      : array[0..maxFileDescLen] of char;
    pageSize  : integer; {- set to maxPageSize (4096) }
    autoSeq   : boolean; {- use auto sequence number }
    userFlag  : word;    {- available for user defined data }
  end;

  {- field information structure. Describes fields in a dbLib file }
  TFldInfo = record
    fldName  : array[0..maxFldNameLen] of char; {- name of field }
    fldSize  : word;                  {- total size (bytes) of field }
    fldPos   : word;                  {- position within record buffer }
    fldType  : TDBFldTypes;           {- type of field }
    fldAssoc : word;                  {- association number }
    userFlag : word;                  {- user defined area }
    sysFlag  : word;                  {- system defined area }
  end;

  {- key segment information structure. Describes keys segments for a key }
  TSegmentInfo = record
    fldNum   : integer; { field number that segment references }
    descend  : boolean; { True = descending order, False = ascending order}
  end;

  {- list of segment numbers for a key }
  TSegmentList = array[1..maxSegs] of TSegmentInfo;

  {- key information structure. Describes a key for a dbLib file }
  TKeyInfo = record
    keyName  : array[0..maxKeyNameLen] of char;  {- name of key }
    keyFlag  : word;                   {- available for user defined data}
    unique   : boolean;                {- True = unique key,
                                          False = allow duplicates }
    numSegs  : integer;                {- number of segments in the key }
    segList  : TSegmentList;           {- list of segment references }
  end;

  {- list of segment numbers for a key }
  TAssocParamList = array[1..maxAssocFlds] of pointer;

  {- assoc information structure. Describes an assoc for a dbLib file }
  TAssocInfo = record
    fName           : array[0..maxFileNameLen] of char;
    keyNum          : integer;
    numFields       : integer;
    fldNum          : array[1..maxAssocFlds] of integer;
    constFldValue   : array[0..maxConstLen] of char;
  end;
  {-
  Note: a) If keyNum = 0 then fldNum[1] is a dbSeqRef.
        b) If keyNum > 0 then fldNum[] are FieldNums.
        c) If fldNum[] is -1 then the driving value is derived
           externaly from the parameter list.
           -1 through -4 correspond to parameters 1 through 4 respectively.
        d) If fldNum[] is -5 then the driving value is derived from constant.
  }

  {- del assoc information structure. Describes a delete assoc for DBLib }
  TDelAssocInfo = record
    fName           : array[0..maxFileNameLen] of char;
    delAction       : TDBDelActions;
    bDownline       : boolean;
    assocFldName    : array[0..maxFldNameLen] of char; {- name of field }
  end;
  {-
  Note: Only the following combinations are supported -
    If bDownline then
      a) If DelAction = DISALLOW_DELETE then check for no associated records
      b) If DelAction = DELETE_ASSOCIATED then delete associated records
    Else
      a) If DelAction = DISALLOW_DELETE then table scan for SegNum in
                                             specified field name
  }

  TFldList = array[1..maxFldsPerRecord] of TFldInfo;
  TKeyList = array[1..MaxKeys] of TKeyInfo;
  TAssocList = array[1..MaxAssocs] of TAssocInfo;
  TDelAssocList = array[1..MaxDelAssocs] of TDelAssocInfo;

  TPosBlk = array[0..127] of byte;  {- used to pass to BTRIEVE }
  PPosBlk = ^TPosBlk;

  PKeyBuf = ^TKeyBuf; {- used to pass to BTRIEVE }
  TKeyBuf = array[0..maxKeyLen-1] of char;

  {- used for typecasting the record buffer pointer only }
  {- this is useful in the debugger because you can do things like
     evaluate recBuf^[30] }
  TDataArray = array[0..maxRecordSize+sizeof(TSeqNum)] of byte;
  PDataArray = ^TDataArray;

  {- BTRIEVE record formats. These are BTRIEVE structures used in some
     BTRIEVE function calls }
  TBTFileSpec = record
    recordLen   : word;
    pageSize    : word;
    totalKeys   : word;
    case integer of
    1:   {- Create params }
      (notUsed           : longint;
       cfileFlags        : integer;
       creserved         : word;
       allocation        : integer);
    2:   {- Stat params }
      (numRecords        : longint;
       sfileFlags        : integer;
       sreserved         : word;
       unusedPages       : integer);
  end;

  TBTKeySpec = record
    keyPos            : integer;
    keyLength         : integer;
    keyFlags          : integer;
    case integer of
    1:   {- create params }
      (notUsed           : longint;
       cExtKeyType       : byte;
       cNullValue        : byte;
       cReserved         : longint);
    2:   {- stat params }
      (uniqueKeys        : longint;
       sExtKeyType       : byte;
       sNullValue        : byte;
       sReserved         : word;
       keyNumber         : byte;
       ACSNumber         : byte);
  end;

  TBTKeySpecList = array[1..24] of TBTKeySpec; {- maximum of 24 keys segments in a BT file }

  {- used for Create(14) }
  TBTFileInfo = record
    fSpec  : TBTFileSpec;
    kSpec  : TBTKeySpecList;
  end;

function DBTypeStr(aType: TDBFldTypes; tStr: PChar; maxLen: integer): PChar;
function DBStrType(aFieldTypeStr: PChar): TDBFldTypes;
function DBSizeFromType(aType: TDBFldTypes; ALen: word): word;
function DBStrSizeForType(aType: TDBFldTypes; ALen: word): word;
function BTExtKeyType(aType: TDBFldTypes): integer;
function DBUserTypeStr(aType: TDBFldTypes; aSize: word; aMnemonic: boolean;
                       fName: PChar; tStr: PChar; maxLen: integer): PChar;
function GetDBTypeFromUserType(aUserType: Integer): TDBFldTypes;

IMPLEMENTATION

uses
  APITools,
  DMString,
  Strings,
  DMSDebug;

function DBTypeStr(aType: TDBFldTypes; tStr: PChar; maxLen: integer): PChar;
{- return a string representation of TDBFldTypes }
var
  p   : array[0..25] of char;
begin
  case aType of
    dbInteger: StrCopy(p, 'Integer');
    dbLongInt: StrCopy(p, 'LongInt');
    dbSeqRef:  StrCopy(p, 'SeqRef');
    dbWord:    StrCopy(p, 'Word');
    dbBoolean: StrCopy(p, 'Boolean');
    dbByte:    StrCopy(p, 'Byte');
    dbShortInt:StrCopy(p, 'ShortInt');
    dbChar:    StrCopy(p, 'Char');
    dbSingle:  StrCopy(p, 'Single');
    dbDouble:  StrCopy(p, 'Double');
    dbDate:    StrCopy(p, 'Date');
    dbTime:    StrCopy(p, 'Time');
    dbString:  StrCopy(p, 'String');
    dbCode:    StrCopy(p, 'Code');
    dbZString: StrCopy(p, 'ZString');
    dbZCode:   StrCopy(p, 'ZCode');
    dbBlock:   StrCopy(p, 'Block');
    dbAutoInc: StrCopy(p, 'AutoInc');
    else
      strcopy(p, 'Invalid');
  end;
  StrLCopy(tstr, p, maxLen);
  DBTypeStr := tstr;
end;

{return a TDBFldTypes from a string respresentation}
function DBStrType(aFieldTypeStr: PChar): TDBFldTypes;
var
  tTypeStr : string;
  i        : integer;
  fldType  : TDBFldTypes;

begin
  tTypeStr:= StrPas(aFieldTypeStr);
  for i := 1 to Length(tTypeStr) do
    tTypeStr[i] := UpCase(tTypeStr[i]);

  if      tTypeStr = 'INTEGER'  then fldType := dbInteger
  else if tTypeStr = 'LONGINT'  then fldType := dbLongInt
  else if tTypeStr = 'WORD'     then fldType := dbWord
  else if tTypeStr = 'BOOLEAN'  then fldType := dbBoolean
  else if tTypeStr = 'BYTE'     then fldType := dbByte
  else if tTypeStr = 'SHORTINT' then fldType := dbShortInt
  else if tTypeStr = 'CHAR'     then fldType := dbChar
  else if tTypeStr = 'SINGLE'   then fldType := dbSingle
  else if tTypeStr = 'DOUBLE'   then fldType := dbDouble
  else if tTypeStr = 'DATE'     then fldType := dbDate
  else if tTypeStr = 'TIME'     then fldType := dbTime
  else if tTypeStr = 'STRING'   then fldType := dbString
  else if tTypeStr = 'ZSTRING'  then fldType := dbZString
  else if tTypeStr = 'CODE'     then fldType := dbCode
  else if tTypeStr = 'ZCODE'    then fldType := dbZCode
  else if tTypeStr = 'BLOCK'    then fldType := dbBlock
  else if tTypeStr = 'SEQREF'   then fldType := dbSeqRef
  else if tTypeStr = 'AUTOINC'  then fldType := dbAutoInc
  else fldType:= dbInvalid;

  DBStrType:= fldType;
end;

function DBSizeFromType(aType: TDBFldTypes; ALen: word): word;
{- return the amount of space required for a specific type. For string
   types ALen will specify the size of the string }
begin
  case aType of
    dbInteger:
      DBSizeFromType := SizeOf(integer);
    dbLongint:
      DBSizeFromType := SizeOf(LongInt);
    dbSeqRef:
      DBSizeFromType := SizeOf(TSeqNum);
    dbWord:
      DBSizeFromType := SizeOf(word);
    dbBoolean:
      DBSizeFromType := SizeOf(boolean);
    dbByte, dbChar, dbShortInt:
      DbSizeFromType := SizeOf(byte);
    dbSingle:
      DbSizeFromType := sizeof(single);
    dbDouble:
      DBSizeFromType := sizeof(double);
    dbString, dbZString, dbCode, dbZCode:
      DBSizeFromType := ALen;
    dbDate, dbTime:
      DBSizeFromType := 4;
    dbBlock:
      DBSizeFromType := ALen;
    dbAutoInc:
      DBSizeFromType := sizeOf(TSeqNum);
    else
      DBSizeFromType := 0;
  end;
end;

function DBStrSizeForType(aType: TDBFldTypes; ALen: word): word;
{- return the amount of space required to represent a specific type as a string. For string
   types ALen will specify the size of the string }
begin
  case aType of
    dbInteger:    { -32,768 thru 32,767 }
      DBStrSizeForType := 7+1;
    dbSeqRef, dbLongInt, dbAutoInc:    { -2,147,483,648 thru 2,147,483,647 }
      DBStrSizeForType := 14+1;
    dbWord:       { 0 thru 65,535 }
      DBStrSizeForType := 6 + 1;
    dbChar:
      DBStrSizeForType := 1 + 1;
    dbBoolean:
      DBStrSizeForType := 11 + 1;
    dbByte:
      DBStrSizeForType := 3 + 1;   { 0 thru 255 }
    dbShortInt:
      DBStrSizeForType := 4 + 1;   { -128 thru 127 }
    dbSingle:
      DBStrSizeForType := 25 + 1;
    dbDouble:
      DBStrSizeForType := 50 + 1;
    dbDate:
      DBStrSizeForType := 10 + 1;  { MM-DD-YYYY }
    dbTime:
      DBStrSizeForType := 17 + 1;  { hh:mm:ss 12345678   <- intl time can have 8 byte ampm string appended }
    dbString, dbCode, dbZString, dbZCode:
      DBStrSizeForType := aLen;
    dbBlock:
      DBStrSizeForType := (aLen * 3);
    else
      DBStrSizeForType := 0;
  end;
end;

function BTExtKeyType(aType: TDBFldTypes): integer;
{- return the BTrieve extended key type for a TDBFldTypes }
var
  retVal  : integer;
begin
  case aType of
    dbInteger:        retVal := 1;   { integer 2 byte }
    dbLongint:        retVal := 1;   { integer 4 byte }
    dbSeqRef:         retVal := 1;   { integer 4 byte }
    dbWord:           retVal := 14;  { unsigned binary 2 byte }
    dbBoolean:        retVal := 7;   { logical }
    dbShortInt,
    dbByte, dbChar:   retVal := 0;   { string 1 byte }
    dbSingle:         retVal := 2;   { float 4 bytes }
    dbDouble:         retVal := 2;   { float 8 bytes }
    dbDate:           retval := 3;   { Btrieve date type }
    dbTime:           retVal := 4;   { Btrieve time type }
    dbString:         retVal := 10;  { LString }
    dbCode:           retVal := 10;  { same as string }
    dbZString:        retVal := 11;  { ZString }
    dbZCode:          retVal := 11;  { ZString }
    dbBlock:          retVal := 0;   { string }
    dbAutoInc:        retVal := 15;  { auto increment }
    else
      retVal := 0;
  end;
  BTExtKeyType := retVal;
end;

{----------------------------------------------------------------------------}
{  Takes a field type and size, and returns a user-recognizable field type   }
{  string. All field types are mapped into either a user-recognizable field  }
{  type string, or to "Internal".                                            }
{                                                                            }
{  Returns : A user-recognizable field type string.                          }
{  InParms : aType     - Field type value.                                   }
{            aSize     - Field size.                                         }
{            aMnemonic - True if mnemonic field, otherwise false.            }
{            fName     - Name of associated file (mnemonic fields only)      }
{            maxLen    - Maximum length of return string.                    }
{  OutParms: tStr      - Placeholder for return value.                       }
{----------------------------------------------------------------------------}
function DBUserTypeStr(aType: TDBFldTypes; aSize: word; aMnemonic: boolean;
                       fName: PChar; tStr: PChar; maxLen: integer): PChar;
var
  p   : array [0..25] of char;
  p2  : array [0..25] of char;
begin
  case aType of
    dbInteger,
    dbLongInt,
    dbWord,
    dbByte,
    dbShortInt  : SR(IDS_FT_INT, p, 20);
    dbSingle,
    dbDouble    : SR(IDS_FT_NUM, p, 20);
    dbChar      : begin
                    SR(IDS_FT_STR, p, 20);
                    StrCat(p, '[1]');
                  end;
    dbString,
    dbCode,
    dbZString,
    dbZCode     : begin
                    Str(aSize - 1, p2);
                    SR(IDS_FT_STR, p, 20);
                    StrCat(p, '[');
                    StrCat(p, p2);
                    StrCat(p, ']');
                  end;
    dbDate      : SR(IDS_FT_DATE, p, 20);
    dbTime      : SR(IDS_FT_TIME, p, 20);
    dbSeqRef    : begin
                    if aMnemonic then
                    begin
                      SR(IDS_FT_MNEM, p, 20);
                      StrCat(p, ' - ');
                      StrCat(p, fName);
                    end
                    else
                      SR(IDS_FT_INTERNAL, p, 20);
                  end;
    dbBoolean,
    dbBlock,
    dbAutoInc   : SR(IDS_FT_INTERNAL, p, 20);
    else
      SR(IDS_INVALID, p, 25);
  end;
  StrLCopy(tstr, p, maxLen);
  DBUserTypeStr := tstr;
end;

{----------------------------------------------------------------------------}
{  Converts a user-recognizable field type string to its associated actual   }
{  field type value.                                                         }
{                                                                            }
{  Returns : Field type value.                                               }
{  InParms : aUserType - User-recognizable field type string.                }
{  OutParms: None.                                                           }
{----------------------------------------------------------------------------}
function GetDBTypeFromUserType(aUserType: Integer): TDBFldTypes;
begin
  case aUserType of
    0: GetDBTypeFromUserType := dbZString;
    1: GetDBTypeFromUserType := dbSeqRef;
    2: GetDBTypeFromUserType := dbDate;
    3: GetDBTypeFromUserType := dbTime;
    4: GetDBTypeFromUserType := dbLongInt;
    5: GetDBTypeFromUserType := dbSingle;
  else
    GetDBTypeFromUserType := dbInvalid;
  end;
end;

END.

