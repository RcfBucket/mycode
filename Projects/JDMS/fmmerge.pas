{----------------------------------------------------------------------------}
{  Module Name  : FMMRGE.PAS                                                 }
{  Programmer   : DWC                                                        }
{  Date Created : 05/11/95                                                   }
{                                                                            }
{  Purpose -                                                                 }
{  This module allows the user to merge data into a Nihongo databases.       }
{                                                                            }
{  Assumptions -                                                             }
{  None.                                                                     }
{                                                                            }
{  Initialization -                                                          }
{  None.                                                                     }
{                                                                            }
{  Revision History -                                                        }
{                                                                            }
{  Version  Date      Who    Did What                                        }
{  -------  --------  ------ ----------------------------------------------- }
{  1.00     05/11/95  DWC     Initial release.                               }
{                                                                            }
{----------------------------------------------------------------------------}

unit FMMERGE;

INTERFACE

uses
  OWindows;

procedure FMMrge(aParent: PWindowsObject);

IMPLEMENTATION

uses
  iniLib,
  Objects,
  GridLib,
  Mscan,
  fmconst,
  fmtypes,
  fmglobal,
  fmerror,
  ApiTools,
  DBIDS,
  DBTypes,
  DBLib,
  DBFile,
  WinDOS,
  WinProcs,
  WinTypes,
  DlgLib,
  PrnPrev,
  userMsgs,
  DMString,
  Strings;

type

PDlgStructAnalysis = ^TDlgStructAnalysis;
TDlgStructAnalysis = object(TCenterDlg)
  fieldList     : PGrid;
  fileList      : PCollection;
  dbOnLineDesc  : PDBReadDesc;
  dbMergeDesc   : PDBReadDesc;
  onLineDir     : array [0..255] of char;
  {}
  constructor Init(AParent: PWindowsObject; aDlg: PChar; uDList: PCollection; dbDir: PChar);
  destructor  Done; virtual;
  procedure   SetupWindow; virtual;
  function    CanClose: boolean; virtual;
  procedure   BtnPrint(var msg: TMessage); virtual ID_FIRST + IDC_PRINT_STRUCT_LIST;

  {Grid Messages}
  procedure WMDrawItem(var Msg: TMessage); virtual wm_First + WM_DRAWITEM;
  procedure WMMeasureItem(var Msg: TMessage); virtual wm_First + WM_MEASUREITEM;
  procedure WMCharToItem(var Msg: TMessage); virtual wm_First + WM_CHARTOITEM;

end;


PDlgMerge = ^TDlgMerge;
TDlgMerge = object(TDlgFMStatus)
  {}
  procedure   ProcessDlg(var msg: TMessage); virtual WM_FIRST + WM_FMPROCESS;
end;

{----------------------[TDlgStructAnalysis]---------------------------------}
{---------------------------------------------------------------------------}
{                                                                           }
{---------------------------------------------------------------------------}
constructor TDlgStructAnalysis.Init(aParent: PWindowsObject; aDlg: PChar;
                                    uDlist: PCollection; dbDir: PChar);
begin
  inherited Init(aParent, aDlg);
  fieldList := new(PGrid, InitResource(@Self, IDC_STRUCT_LIST, 6, false));
  fileList := uDList;
  StrCopy(onLineDir, dbDir);
end;

destructor TDlgStructAnalysis.Done;
begin
  inherited Done;
end;

procedure TDlgStructAnalysis.SetupWindow;
var
  CurrentRow  : integer;
  i           : integer;
  j           : integer;
  uDEntry     : PUserDefineEntry;
  fieldCount  : integer;
  fileName    : array [0..255] of char;
  onLineBool  : boolean;
  onLineField : TFldInfo;
  onLineFName : array [0..255] of char;
  onLineType  : array [0..25] of char;
  onLineAssoc : TAssocInfo;
  onLineAName : array [0..255] of char;
  isOnLineMnem: boolean;
  mergeBool   : boolean;
  mergeField  : TFldInfo;
  mergeFName  : array [0..255] of char;
  mergeType   : array [0..25] of char;
  mergeAssoc  : TAssocInfo;
  mergeAName  : array [0..255] of char;
  isMergeMnem : boolean;
  actionType  : array [0..10] of char;
  fldHeader   : array [0..25] of char;

begin
  inherited SetupWindow;

  {set up grid headers}
  fieldList^.SetHeader(0, SR(IDS_FMFILEMSG, fldHeader, 25));
  fieldList^.SetColumnWidth(0,78);

  fieldList^.SetHeader(1, SR(IDS_FMFIELDNAME, fldHeader, 25));
  fieldList^.SetColumnWidth(1,110);

  fieldList^.SetHeader(2, SR(IDS_FMDATATYPE, fldHeader, 25));
  fieldList^.SetColumnWidth(2,127);

  fieldList^.SetHeader(3, SR(IDS_FMACTION, fldHeader, 25));
  fieldList^.SetColumnWidth(3,50);

  fieldList^.SetHeader(4, SR(IDS_FMFIELDNAME, fldHeader, 25));
  fieldList^.SetColumnWidth(4,110);

  fieldList^.SetHeader(5, SR(IDS_FMDATATYPE, fldHeader, 25));
  fieldList^.SetColumnWidth(5,127);


  {start comparing the online data with the inited structure file}
  for i := 0 to fileList^.Count -1 do
  begin
    uDEntry := fileList^.At(i);
    dbOnLineDesc := New(PDBReadDesc, Init(uDEntry^.fileName, onLineDir));
    dbMergeDesc  := New(PDBReadDesc, Init(uDEntry^.fileName, 'A:'));

    strCopy(fileName, uDEntry^.fileName);

    if dbOnLineDesc^.NumFields >= dbMergeDesc^.NumFields then
      fieldCount := dbOnLineDesc^.NumFields
    else
      fieldCount := dbMergeDesc^.NumFields;

    for j := 1 to fieldCount do
    begin

      if j <= dbOnLineDesc^.NumFields then
      begin
        onLineBool := true;
        dbOnLineDesc^.fieldInfo(j,onLineField);
        StrCopy(onLineFName, onLineField.fldName);
        if (onLineField.userFlag and db_Mnemonic) = db_Mnemonic then
        begin
          isOnLineMnem := true;
          dbOnLineDesc^.AssocInfo(onLineField.fldAssoc, onLineAssoc);
          StrCopy(onLineAName, onLineAssoc.fName);
        end
        else
        begin
          isOnLineMnem := false;
          StrCopy(onLineAName, '');
        end;
        DBUserTypeStr(onLineField.fldType, onLineField.fldSize, isOnLineMnem , onLineAName,
                      onLineType, 20);
      end
      else
      Begin
        onLineBool := false;
        StrCopy(onLineFName, '  ');
        StrCopy(onLineType, '  ');
      end;
      if j <= dbMergeDesc^.NumFields then
      begin
        mergeBool := true;
        dbMergeDesc^.fieldInfo(j,mergeField);
        StrCopy(mergeFName, mergeField.fldName);
        if (mergeField.userFlag and db_Mnemonic) = db_Mnemonic then
        begin
          isMergeMnem := true;
          dbMergeDesc^.AssocInfo(mergeField.fldAssoc, mergeAssoc);
          StrCopy(mergeAName, mergeAssoc.fName);
        end
        else
        begin
          isMergeMnem := false;
          StrCopy(mergeAName, '');
        end;
        DBUserTypeStr(mergeField.fldType, mergeField.fldSize, isMergeMnem, mergeAName,
                      mergeType, 20);
      end
      else
      begin
        mergeBool := false;
        StrCopy(mergeFName, '  ');
        StrCopy(MergeType, '  ');
      end;

      if onLineBool and mergeBool then
      begin
        if (StrIComp(onLineField.fldName, mergeField.fldName) <> 0) or
           (StrIComp(onLineAName, mergeAName) <> 0)  or
           (onLineField.fldSize <> mergeField.fldSize) or
           (onLineField.fldType <> mergeField.fldType) then
          SR(IDS_FMCHANGE, actionType, 10)
        else
          SR(IDS_FMNONE, actionType, 10);
      end
      else if onlineBool then
        SR(IDS_FMDELETE, actionType, 10)
      else
        SR(IDS_FMADD, actionType, 10);

      currentRow := fieldList^.AddString('  ');

      fieldList^.SetData(0, currentRow, fileName);
      fieldList^.SetData(1, currentRow, onLineFName);
      fieldList^.SetData(2, currentRow, onLineType);
      fieldList^.SetData(3, currentRow, actionType);
      fieldList^.SetData(4, currentRow, mergeFName);
      fieldList^.SetData(5, currentRow, mergeType);
      strCopy(fileName, '  ');

    end;
    currentRow := fieldList^.AddString('  ');


    MSDisposeObj(dbOnLineDesc);
    MSDisposeObj(dbMergeDesc);
  end;


end;

function TDlgStructAnalysis.CanClose: boolean;
begin
  CanClose:= inherited CanClose;
end;

procedure TDlgStructAnalysis.BtnPrint(var msg: TMessage);
var
  title     : array[0..50] of char;
  pstr      : array[0..200] of char;
  pd        : PPrnPrevDlg;
  pInfo     : PPrintInfo;
  lf        : TLogFont;
  aRow      : longint;
  aCol      : integer;
begin
  MakeScreenFont(lf, false, false);
  pInfo:= New(PPrintInfo, Init(lf, IniDefPointSize));

  with pInfo^ do
  begin
    header^.AddRow(r_BorderTop, 0);
    GetWindowText(HWindow, title, SizeOf(title)-1);
    header^.AddCell(title, 0, c_Stretch or c_Bold, -2);
    header^.AddCell(SR(IDS_FMPRINTEDON, pstr, SizeOf(pstr)-1), LT200, c_PageRight or c_Right or c_Date, 0);
    header^.AddRow(r_Normal, 0);

    footer^.AddRow(r_CenterRow or r_BorderTop, 0);
    footer^.AddCell(SR(IDS_FMPAGEPOFN, pstr, SizeOf(pstr)-1), LT300, c_Center or c_PageNum or c_NumPages, 0);

    fieldList^.GetPrintHeader(header, True);
    fieldList^.GetPrintBody(pInfo, False);

    pd:= New(PPrnPrevDlg, Init(@self, pInfo, title));
    application^.ExecDialog(pd);
  end;
  MSDisposeObj(pInfo);
end;


 {grid methods}
procedure TDlgStructAnalysis.WMDrawItem(var Msg: TMessage);
begin
  fieldList^.DrawItem(Msg);
end;

procedure TDlgStructAnalysis.WMMeasureItem(var Msg: TMessage);
begin
  fieldList^.MeasureItem(Msg);
end;

procedure TDlgStructAnalysis.WMCharToItem(var Msg: TMessage);
begin
  fieldList^.CharToItem(Msg);
end;


{--------------------------[CopyExtractFiles]-------------------------------}
{---------------------------------------------------------------------------}
{                                                                           }
{---------------------------------------------------------------------------}
function CopyExtractFiles(dlg        : PDlgFMStatus;
                          workDir,
                          incrDir    : PChar;
                          var setRec : TBackupRec): integer;
var
  diskId          : integer;
  retVal          : integer;
  lastDisk        : boolean;
  backupType      : char;
  lastFileName    : array[0..max8dot3] of char;
  backupId        : integer;
  backupIncr      : integer;
  numDisks        : integer;
  aTitle          : array[0..255] of char;
  msgStr          : array[0..255] of char;
  msgStr1         : array[0..50] of char;
  msgStr2         : array[0..50] of char;
  tempStr         : array[0..10] of char;
  srstr           : array[0..25] of char;
  pStr            : array[0..50] of char;

begin
  retVal := continueJob;

  dlg^.SetText(SR(IDS_PERDISK,  pStr, 50),IDC_PB1_TEXT);
  dlg^.SetText(SR(IDS_PERTOTAL, pStr, 50),IDC_PB2_TEXT);
  dlg^.ResetPct(1);
  dlg^.ResetPct(2);
  totalCt := 0;
  totalSpace := setRec.numBytes + setRec.numIncrBytes;
  diskSizeCt := 0;
  diskSpace := floppyBytes;

  backupType := setRec.BType;
  backupIncr := setRec.numIncr;
  numDisks   := setRec.numDisks;
  backupId   := setRec.BIdent;
  strCopy(lastFileName,'');

  SR(IDS_FMMERGE, aTitle, 255);
  if backupType = FullCatalog then
    SR(IDS_FULLBACKUPSET, msgStr2, 255)
  else
    SR(IDS_EXTRACTDISKSET, msgStr2, 255);

  StrCopy(msgStr,SR(IDS_FMDISK1OF, srstr, 25));
  StrCat(msgStr,' ');
  str(numDisks,tempStr);
  StrCat(msgStr,tempStr);
  dlg^.SetText(msgStr, IDC_TEXT2);
  {- Restore Extract files -}
  lastDisk := false;
  diskId := 1;
  while  (not lastDisk) and (retVal = continueJob) and dlg^.CanContinue do
  begin
    diskSizeCt := 0;
    dlg^.ResetPct(1);
    retVal := restoreFiles(dlg, workDir, lastFileName);
    if (retVal = continueJob) and dlg^.CanContinue then
    begin
      inc(diskId);
      if diskId <= numDisks then
        retVal := InsertRestoreDisk(dlg,diskId,numDisks,backupType,backupId,msgStr2, aTitle)
      else
        lastDisk := true;
    end;
  end;
  dlg^.ResetPct(1);

  if (backupType = FullCatalog) and (backupIncr = 1) and
     (retVal = continueJob)     and dlg^.CanContinue then
  begin
    {- Restore Incremetal Backup -}
    CleanDir(incrDir,'*.*');
    SR(IDS_INCRBACKUPSET, msgStr, 255);
    retVal := getSetInfo(dlg, setRec, msgStr, aTitle, IncrCatalog, backupId);
    if (retVal = continueJob) and dlg^.CanContinue then
    begin
      dlg^.SetText(SR(IDS_RESTOREINCR, msgStr, 255),IDC_TEXT2);
      numDisks := setRec.numDisks;
      StrCopy(msgStr1,SR(IDS_FMDISK1OF, srstr, 25));
      StrCat(msgStr1,' ');
      str(numDisks,tempStr);
      StrCat(msgStr1,tempStr);
      dlg^.SetText(msgStr1, IDC_TEXT2);
      strCopy(lastFileName,'');
      diskId := 1;
      lastDisk := false;
      while  (not lastDisk) and (retVal = continueJob) and dlg^.CanContinue do
      begin
        diskSizeCt := 0;
        restoreFiles(dlg, incrDir, lastFileName);
        inc(diskId);
        if diskId <= numDisks then
          InsertRestoreDisk(dlg, diskId, numDisks, IncrCatalog, backupId, msgStr, aTitle)
        else
          lastDisk := true;
      end;
      restoreData(dlg, workDir, incrDir);
    end;
  end;

  CopyExtractFiles := retVal;
end;

{---------------------------[BuildMergeFiles]-------------------------------}
{---------------------------------------------------------------------------}
{                                                                           }
{---------------------------------------------------------------------------}
function BuildMergeFiles(dlg      : PDlgFMStatus;
                         fNList   : PCollection;
                         uDList   : PCollection;
                         mergeDir,
                         anExt    : PChar):integer;
var
  filePath    : array[0..maxPathLen] of char;
  fileDir     : array[0..maxPathLen] of char;
  fileName    : array[0..maxFileNameLen] of char;
  fileExt     : array[0..fsExtension] of char;
  dirInfo     : TSearchRec;
  dbFile      : PDBFile;
  fileInfo    : TFileInfo;
  assocInfo   : TAssocInfo;
  fldInfo     : TFldInfo;
  i           : integer;
  assocFName  : array[0..maxFileNameLen] of char;
  retVal      : integer;
  assocList   : PCollection;
  seqNumList  : PSeqNumCollection;
  seqNum      : TSeqNum;
  oldMode     : Word;

begin
  oldMode := SetErrorMode(SEM_FAILCRITICALERRORS);
  retVal := continueJob;

  StrCopy(filePath, mergeDir);
  StrCat(filePath, '*');
  StrCat(filePath, anExt);
  FindFirst(filePath, faAnyFile - faDirectory - faVolumeID, dirInfo);
  if DosError = 0 then
  Begin
    while (DosError = 0) and (retVal = continueJob) and dlg^.CanContinue  do
    begin
      FileSplit(dirInfo.Name, fileDir, fileName, fileExt);
      dbFile := New(PDBFile, Init(fileName, mergeDir, dbOpenNormal));
      if (dbFile = nil) then
      begin
        retVal := fmErr_OpeningFile;
        StrCopy(fmErr_Msg,mergeDir);
        StrCat(fmErr_Msg,fileName);
      end
      else
      begin
        dbFile^.dbd^.FileInfo(fileInfo);
        if ((fileInfo.userFlag and db_UDFile) = db_UDFile) then
        begin
          uDList^.Insert(New(PUserDefineEntry, Init(fileName)));
        end;
        if ((fileInfo.userFlag and db_ExtractFile) = db_ExtractFile) or
           ((fileInfo.userFlag and db_UDFile) = db_UDFile) then
        begin
          assocList := New(PCollection, Init(5,2));
          seqNumList := New(PSeqNumCollection, Init(100,20));
          for i := 1 to dbFile^.dbd^.NumFields do
          begin
            dbFile^.dbd^.FieldInfo(i, fldInfo);
            if fldInfo.fldType = dbSeqRef then
            begin
              if fldInfo.fldAssoc > 0 then
              begin
                dbFile^.dbd^.AssocInfo(fldInfo.fldAssoc, assocInfo);
                assocList^.Insert(New(PAssocEntry, Init(i,assocInfo.fname)));
              end
              else
              begin
              {- Error cannot continue -}
              end;
            end;
          end;
          if dbFile^.dbc^.GetFirst(dbFile^.dbr) then
          repeat
            seqNum := dbFile^.dbr^.GetSeqValue;
            seqNumList^.Insert(New(PSeqNumEntry ,Init(seqNum)));
          until (not dbFile^.dbc^.GetNext(dbFile^.dbr)) or (not dlg^.CanContinue);
          fNList^.Insert(New(PFileNameEntry, Init(fileName,assocList, seqNumList)));
        end;
     end;
     FindNext(dirInfo);
     MSDisposeObj(dbFile);
    end;
  end
  else
    retVal := DosError;

  BuildMergeFiles := retVal;
  SetErrorMode(oldMode);
end;

{-------------------------[DisplayMergeError]-------------------------------}
{---------------------------------------------------------------------------}
{                                                                           }
{---------------------------------------------------------------------------}
function DisplayMergeError(dlg          : PDlgFMStatus;
                           mergePatRef,
                           onLnPatRef,
                           onLnSpecRef  : TSeqNum;
                           dbr          : PDBRec;
                           mergeDir,
                           onLnDir      : PChar): TSeqNum;
var
  dlgMergeErr : PDlgMergeError;

Begin
  globalSeq := onLnSpecRef;

  dlgMergeErr:= New(PDlgMergeError, Init(dlg, MakeIntResource(DLG_MERGEERROR),
                             mergePatRef, onLnPatRef, dbr, mergeDir, onLnDir));
  if application^.ExecDialog(dlgMergeErr) = IDOK then
  begin

  end;

  DisplayMergeError := globalSeq;
end;

{-------------------------[findNewSeq]--------------------------------------}
{---------------------------------------------------------------------------}
{                                                                           }
{---------------------------------------------------------------------------}
function findNewSeq(sList : PSeqNumCollection; seq : TSeqNum): TSeqNum;

  function Matches(aSeqNumEntry : PSeqNumEntry) : boolean; far;
  begin
    Matches := (aSeqNumEntry^.oldSeqNum = seq);
  end;

var
  aSeqNumEntry : PSeqNumEntry;

begin
  aSeqNumEntry := sList^.FirstThat(@Matches);
  findNewSeq := aSeqNumEntry^.newSeqNum;
end;


{------------------------------[MergeFile]----------------------------------}
{---------------------------------------------------------------------------}
{                                                                           }
{---------------------------------------------------------------------------}
function MergeFile(dlg       : PDlgFMStatus;
                   fEntry    : PFileNameEntry;
                   fNList    : PCollection;
                   mergeDir,
                   localDir  : PChar):integer;
var
  dbMergeFile : PDBReadWriteFile;
  dbLocalFile : PDBReadWriteFile;
  fileInfo    : TFileInfo;
  assocInfo   : TAssocInfo;
  fldInfo     : TFldInfo;
  j           : integer;
  recMap      : array[0..maxFldsPerRecord] of xref;
  recCnt      : integer;
  indx        : integer;
  fileName    : array[0..maxFileNameLen] of char;
  assocFName  : array[0..maxFileNameLen] of char;
  retVal      : integer;
  assocList   : PCollection;
  seqNumList  : PSeqNumCollection;
  fEntry2     : PFileNameEntry;
  assocEntry  : PAssocEntry;
  seqNumEntry : PSeqNumEntry;
  seq         : TSeqNum;
  newSeq      : TSeqNum;
  tmpSeq      : TSeqNum;
  onLnPatRef  : TSeqNum;
  mergePatRef : TSeqNum;

begin
  retVal := continueJob;

  StrCopy(fileName,fEntry^.fileName);
  assocList := fEntry^.assocList;
  seqNumList := fEntry^.seqNumList;

  recCnt := 0;

  for j := 0 to assocList^.count-1 do
  Begin
    assocEntry := assocList^.At(j);
    fEntry2 := SearchForFile(fNList, assocEntry^.FileName);
    recMap[recCnt].fieldNum := assocEntry^.fieldNum;
    recMap[recCnt].seqCollection := fEntry2^.seqNumList;
    inc(recCnt);
  end;

  dbMergeFile := New(PDBReadWriteFile, Init(fileName, mergeDir, dbOpenNormal));
  if (dbMergeFile = nil) then
  begin
    retVal := fmErr_OpeningFile;
    StrCopy(fmErr_Msg,mergeDir);
    StrCat(fmErr_Msg,fileName);
  end;

  dbLocalFile := New(PDBReadWriteFile, Init(fileName, localDir, dbOpenNormal));
  if (dbLocalFile = nil) then
  begin
    retVal := fmErr_OpeningFile;
    StrCopy(fmErr_Msg,localDir);
    StrCat(fmErr_Msg,fileName);
  end;

  j := 0;
  while (j <= seqNumList^.count-1) and (retVal = continueJob) and dlg^.CanContinue do
  begin
    seqNumEntry := seqNumList^.At(j);
    dbMergeFile^.dbc^.GetSeq(dbMergeFile^.dbr, seqNumEntry^.oldSeqNum);

    {- save off the patref. -}
    if StrIComp(fileName, DBSPECFile) = 0 then
    begin
      dbMergeFile^.dbr^.GetField(DBSPECPatRef, @mergePatRef, sizeOf(mergePatRef));
    end;

    {- substitue old seq numbers with new ones. -}
    for indx := 0 to recCnt-1 do
    begin
      dbMergeFile^.dbr^.GetField(recMap[indx].fieldNum, @seq, sizeOf(seq));
      if seq > 0 then
      begin
        newSeq := findNewSeq(recMap[indx].seqCollection,seq);
        dbMergeFile^.dbr^.PutField(recMap[indx].fieldNum, @newSeq);
      end;
    end;

    dbLocalFile^.dbr^.CopyRecord(dbMergeFile^.dbr);
    if not dbLocalFile^.dbc^.InsertRec(dbLocalFile^.dbr) then
    begin
      {- Duplicate key -}
      if dbLocalFile^.dbc^.dbErrorNum = (MOD_BTRV + 5) then
      begin
        dbLocalFile^.dbc^.GetEQ(dbLocalFile^.dbr);
        tmpSeq := dbLocalFile^.dbr^.GetSeqValue;
        if StrIComp(fileName, DBSPECFile) = 0 then
        begin
          dbLocalFile^.dbr^.GetField(DBSPECPatRef, @onLnPatRef, sizeOf(onLnPatRef));
          if (mergePatRef <> onLnPatRef) then
            tmpSeq := DisplayMergeError(dlg, mergePatRef, onLnPatRef, tmpSeq, dbMergeFile^.dbr,
                                        mergeDir, localDir);
        end;
        seqNumEntry^.newSeqNum := tmpSeq;
      end;
    end
    else
    begin
      tmpSeq := dbLocalFile^.dbr^.GetSeqValue;
      seqNumEntry^.newSeqNum := tmpSeq;
    end;
    inc(j);
  end;

  MSDisposeObj(dbMergeFile);
  MSDisposeObj(dbLocalFile);

  MergeFile := retVal;
end;

{------------------------------[MergeFiles]---------------------------------}
{---------------------------------------------------------------------------}
{                                                                           }
{---------------------------------------------------------------------------}
function MergeFiles(dlg      : PDlgFMStatus;
                    fNList   : PCollection;
                    uDList   : PCollection;
                    mergeDir,
                    localDir : PChar):integer;
var
  retVal      : integer;
  j           : integer;
  fEntry      : PFileNameEntry;
  uDEntry     : PUserDefineEntry;

begin
  retVal := continueJob;

  j := 0;
  while (j <= uDList^.count-1) and (retVal = continueJob) and dlg^.CanContinue do
  begin
    uDEntry := uDList^.At(j);
    if (retVal = continueJob) and dlg^.CanContinue then
    begin
      fEntry := SearchForFile(fNList, uDEntry^.fileName);  {User Defined Files}
      retVal := MergeFile(dlg, fEntry, fNlist, mergeDir, localDir);
    end;
    inc(j);
  end;

  if (retVal = continueJob) and dlg^.CanContinue then
  begin
    dlg^.SetText(DBSRCFile, IDC_PB1_TEXT);
    fEntry := SearchForFile(fNList,DBSRCFile );  {source}
    retVal := MergeFile(dlg, fEntry, fNlist, mergeDir, localDir);
  end;

  if (retVal = continueJob) and dlg^.CanContinue then
  begin
    dlg^.SetText(DBWARDFile, IDC_PB1_TEXT);
    fEntry := SearchForFile(fNList,DBWARDFile );  {ward}
    retVal := MergeFile(dlg, fEntry, fNlist, mergeDir, localDir);
  end;

  if (retVal = continueJob) and dlg^.CanContinue then
  begin
    dlg^.SetText(DBSTATFile, IDC_PB1_TEXT);
    fEntry := SearchForFile(fNList,DBSTATFile );  {specstat}
    retVal := MergeFile(dlg, fEntry, fNlist, mergeDir, localDir);
  end;

  if (retVal = continueJob) and dlg^.CanContinue then
  begin
    dlg^.SetText(DBSESSFile, IDC_PB1_TEXT);
    fEntry := SearchForFile(fNList,DBSESSFile );  {session}
    retVal := MergeFile(dlg, fEntry, fNlist, mergeDir, localDir);
  end;

  if (retVal = continueJob) and dlg^.CanContinue then
  begin
    dlg^.SetText(DBORGFile, IDC_PB1_TEXT);
    fEntry := SearchForFile(fNList,DBORGFile );  {organism}
    retVal := MergeFile(dlg, fEntry, fNlist, mergeDir, localDir);
  end;

  if (retVal = continueJob) and dlg^.CanContinue then
  begin
    dlg^.SetText(DBTSTGRPFile, IDC_PB1_TEXT);
    fEntry := SearchForFile(fNList,DBTSTGRPFile ); {testgrp}
    retVal := MergeFile(dlg, fEntry, fNlist, mergeDir, localDir);
  end;

  if (retVal = continueJob) and dlg^.CanContinue then
  begin
    dlg^.SetText(DBTSTCATFile, IDC_PB1_TEXT);
    fEntry := SearchForFile(fNList,DBTSTCATFile );  {testcat}
    retVal := MergeFile(dlg, fEntry, fNlist, mergeDir, localDir);
  end;

  if (retVal = continueJob) and dlg^.CanContinue then
  begin
    dlg^.SetText(DBDRUGFile, IDC_PB1_TEXT);
    fEntry := SearchForFile(fNList,DBDRUGFile );  {drug}
    retVal := MergeFile(dlg, fEntry, fNlist, mergeDir, localDir);
  end;

  if (retVal = continueJob) and dlg^.CanContinue then
  begin
    dlg^.SetText(DBGSDEFFile, IDC_PB1_TEXT);
    fEntry := SearchForFile(fNList,DBGSDEFFile );  {gsdef}
    retVal := MergeFile(dlg, fEntry, fNlist, mergeDir, localDir);
  end;

  if (retVal = continueJob) and dlg^.CanContinue then
  begin
    dlg^.SetText(DBGSMNEMFile, IDC_PB1_TEXT);
    fEntry := SearchForFile(fNList,DBGSMNEMFile );  {gsmnem}
    retVal := MergeFile(dlg, fEntry, fNlist, mergeDir, localDir);
  end;

  if (retVal = continueJob) and dlg^.CanContinue then
  begin
    dlg^.SetText(DBORDFile, IDC_PB1_TEXT);
    fEntry := SearchForFile(fNList,DBORDFile );  {order}
    retVal := MergeFile(dlg, fEntry, fNlist, mergeDir, localDir);
  end;

  if (retVal = continueJob) and dlg^.CanContinue then
  begin
    dlg^.SetText(DBTSTFile, IDC_PB1_TEXT);
    fEntry := SearchForFile(fNList,DBTSTFile );  {test}
    retVal := MergeFile(dlg, fEntry, fNlist, mergeDir, localDir);
  end;

  if (retVal = continueJob) and dlg^.CanContinue then
  begin
    dlg^.SetText(DBPATFile, IDC_PB1_TEXT);
    fEntry := SearchForFile(fNList,DBPATFile );  {patient}
    retVal := MergeFile(dlg, fEntry, fNlist, mergeDir, localDir);
  end;

  if (retVal = continueJob) and dlg^.CanContinue then
  begin
    dlg^.SetText(DBSPECFile, IDC_PB1_TEXT);
    fEntry := SearchForFile(fNList,DBSPECFile );  {specimen}
    retVal := MergeFile(dlg, fEntry, fNlist, mergeDir, localDir);
  end;

  if (retVal = continueJob) and dlg^.CanContinue then
  begin
    dlg^.SetText(DBISOFile, IDC_PB1_TEXT);
    fEntry := SearchForFile(fNList,DBISOFile );  {isolate}
    retVal := MergeFile(dlg, fEntry, fNlist, mergeDir, localDir);
  end;

  if (retVal = continueJob) and dlg^.CanContinue then
  begin
    dlg^.SetText(DBGSFile, IDC_PB1_TEXT);
    fEntry := SearchForFile(fNList,DBGSFile );  {grmstain}
    retVal := MergeFile(dlg, fEntry, fNlist, mergeDir, localDir);
  end;

  if (retVal = continueJob) and dlg^.CanContinue then
  begin
    dlg^.SetText(DBISOORDFile, IDC_PB1_TEXT);
    fEntry := SearchForFile(fNList,DBISOORDFile );  {isorder}
    retVal := MergeFile(dlg, fEntry, fNlist, mergeDir, localDir);
  end;

  if (retVal = continueJob) and dlg^.CanContinue then
  begin
    dlg^.SetText(DBRESFile, IDC_PB1_TEXT);
    fEntry := SearchForFile(fNList,DBRESFile );  {result}
    retVal := MergeFile(dlg, fEntry, fNlist, mergeDir, localDir);
  end;

  if (retVal = continueJob) and dlg^.CanContinue then
  begin
    dlg^.SetText(DBRESFile, IDC_PB1_TEXT);
    fEntry := SearchForFile(fNList,DBTSTGRPXRFFile );  {tstgpxrf}
    retVal := MergeFile(dlg, fEntry, fNlist, mergeDir, localDir);
  end;

  if (retVal = continueJob) and dlg^.CanContinue then
  begin
    dlg^.SetText(DBORDXRFFile, IDC_PB1_TEXT);
    fEntry := SearchForFile(fNList,DBORDXRFFile );  {orderxrf}
    retVal := MergeFile(dlg, fEntry, fNlist, mergeDir, localDir);
  end;

  dlg^.SetText('  ', IDC_PB1_TEXT);
  MergeFiles := retVal;
end;

{----------------------------[CompareSchema]--------------------------------}
{---------------------------------------------------------------------------}
{                                                                           }
{---------------------------------------------------------------------------}
function CompareSchema(dlg: PDlgFMStatus; dbDir: PChar; var setRec: TBackupRec): integer;
var
  filePath     : array[0..maxPathLen] of char;
  fileDir      : array[0..maxPathLen] of char;
  fileName     : array[0..maxFileNameLen] of char;
  fileExt      : array[0..fsExtension] of char;
  dirInfo      : TSearchRec;
  dbOnLineDesc : PDBReadDesc;
  dbMergeDesc  : PDBReadDesc;
  fileInfo     : TFileInfo;
  assocInfo    : TAssocInfo;
  fldInfo      : TFldInfo;
  i            : integer;
  retVal       : integer;
  aTitle       : array[0..255] of char;
  msgStr2      : array[0..50] of char;
  uDList       : PCollection;
  dlgSA        : PDlgStructAnalysis;
  oldMode      : Word;

begin
  oldMode := SetErrorMode(SEM_FAILCRITICALERRORS);
  retVal := continueJob;

  uDList := new(PCollection, Init(5,2));

  SR(IDS_FMMERGE, aTitle, 255);
  SR(IDS_DISKSET, msgStr2, 255);
  retVal := getSetInfo(dlg, setRec, msgStr2, aTitle, ExtractCatalog, 0);
  if (retVal = continueJob) and dlg^.CanContinue then
  begin
    StrCopy(filePath, dbDir);
    StrCat(filePath, '*');
    StrCat(filePath, fileDescExt);
    FindFirst(filePath, faAnyFile - faDirectory - faVolumeID, dirInfo);
    if DosError = 0 then
    begin
      while (DosError = 0) and (retVal = continueJob) and dlg^.CanContinue  do
      begin
        FileSplit(dirInfo.Name, fileDir, fileName, fileExt);
        dbOnLineDesc := New(PDBReadDesc, Init(fileName, dbDir));
        if (dbOnLineDesc = nil) then
        begin
          retVal := fmErr_OpeningFile;
          StrCopy(fmErr_Msg,dbDir);
          StrCat(fmErr_Msg,fileName);
        end
        else
        begin
          dbOnLineDesc^.FileInfo(fileInfo);
          if ((fileInfo.userFlag and db_UserEdit) = db_UserEdit) then
          begin
            dbMergeDesc:= New(PDBReadDesc, Init(fileName, 'A:'));
            if (dbMergeDesc = nil) then
            begin
              (* The file may not exist on drive A.*)
              (*retVal := fmErr_OpeningFile;*)
              (*StrCopy(fmErr_Msg,'A:');*)
              (*StrCat(fmErr_Msg,fileName);*)
            end
            else
            begin
              if not dbMergeDesc^.CompareRecStruct(dbOnLineDesc) then
              begin
                uDList^.Insert(New(PUserDefineEntry, Init(fileName)));
              end;
              MSDisposeObj(dbMergeDesc);
            end;
          end;
          FindNext(dirInfo);
          MSDisposeObj(dbOnLineDesc);
        end;
      end;
    end
    else
      retVal := DosError;
  end;

  if (uDList^.Count <> 0) and (retVal = continueJob) and dlg^.CanContinue then
  begin
    retVal := fmErr_NoMatchSchema;
    dlgSA:= New(PDlgStructAnalysis, Init(dlg, MakeIntResource(DLG_STRUCT_ANALYSIS),uDList, dbDir));
    application^.ExecDialog(dlgSA);
  end;

  MSDisposeObj(uDList);
  CompareSchema := retVal;
  SetErrorMode(oldMode);
end;

{------------------------[TDlgMerge.ProcessDlg]-----------------------------}
{---------------------------------------------------------------------------}
{                                                                           }
{---------------------------------------------------------------------------}
procedure TDlgMerge.ProcessDlg(var msg: TMessage);
var
  retVal         : integer;
  saveVal        : integer;
  updateMsg      : array [0..255] of char;
  aTitle         : array [0..255] of char;
  diskNum        : integer;
  estDiskNum     : integer;
  dbDir          : array[0..maxPathLen] of char;
  saveDir        : array[0..maxPathLen] of char;
  incrDir        : array[0..maxPathLen] of char;
  workDir        : array[0..maxPathLen] of char;
  fileNameList   : PCollection;
  userDefineList : PCollection;
  setRec         : TBackupRec;
  freeBytes      : longint;
  neededBytes    : longint;
  drive          : byte;
  verifyMerge    : Boolean;
  vSet           : Boolean;

begin
  verifyMerge := INIVerifyBackup;
  diskNum := 0;
  estDiskNum := 0;
  retVal := continueJob;
  StrCopy(fmErr_Msg,'');

  fileNameList := New(PCollection, Init(20, 5));
  userDefineList := New(PCollection, Init(20, 5));

  SetText('    ',0);
  SetText('    ',IDC_TEXT1);
  SetText('    ',IDC_TEXT2);
  SetText('    ',IDC_PB1_TEXT);
  SetText('    ',IDC_PB1);
  SetText('    ',IDC_PB2_TEXT);
  SetText('    ',IDC_PB2);

  SR(IDS_MERGEMAINMSG, updateMsg, 255);
  SetText(updateMsg,0);
  SR(IDS_MERGESET, updateMsg, 255);
  SetText(updateMsg,IDC_TEXT2);
  Update;

  if (CanContinue) then
  begin
    GetCurrentDir(dbDir, drive);
    GetWorkingDir(dbDir, saveDir, restoreDir);
    GetWorkingDir(dbDir, workDir, backupDir);
    GetWorkingDir(dbDir, incrDir, mergeDir);
  end;

  if (CanContinue) then
    CleanDir(workDir,'*.*');

  retVal := CompareSchema(@self, dbDir, setRec);
  {Check the drive to make sure there is enough free disk space to perform }
  {this operation.                                                         }
  freeBytes := DiskFree(drive);
  neededBytes := setRec.numBytes + setRec.numIncrBytes;
  if (retVal = continueJob) and (neededBytes < freeBytes) then
  begin
    {* Save the onLine database before proceeding *}
    SetText(SR(IDS_SAVINGFILES, updateMsg, 255),IDC_TEXT2);
    CleanDir(saveDir,'*.*');
    if (retVal = continueJob) and (CanContinue) then
    begin
      ShowWindow(GetDlgItem(HWindow, IDCANCEL), SW_HIDE);
      retVal := BuildArchiveFiles(@Self, fileNameList, dbDir, fileDescExt,backupAllFiles);
      retval := CopyFiles(@Self, fileNameList, dbDir, saveDir, fileDescExt,
                          diskNum, setRec, false, false, false, nil);
      retVal := CopyFiles(@Self, fileNameList, dbDir, saveDir, datFileExt,
                          diskNum, setRec, false, false, false, nil);
      ShowWindow(GetDlgItem(HWindow, IDCANCEL), SW_SHOW);
    end;

    if (retVal = continueJob) and (CanContinue) then
    begin
      SR(IDS_MERGECOPY, updateMsg, 255);
      SetText(updateMsg,IDC_TEXT1);
      if VerifyMerge then
      begin
        GetVerify(vSet);
        SetVerify(True);
      end;
      retVal := CopyExtractFiles(@Self, workDir, incrDir, setRec);
      if VerifyMerge then
        SetVerify(vSet);
    end;
    if (retVal = continueJob) and CanContinue then
    begin
      SetText('    ',IDC_TEXT1);
      SetText(SR(IDS_MERGETABLE, updateMsg, 255),IDC_TEXT2);
      SetText('    ',IDC_PB1_TEXT);

      ShowWindow(GetDlgItem(HWindow, IDC_PB1), SW_HIDE);
      ShowWindow(GetDlgItem(HWindow, IDC_PB2_TEXT), SW_HIDE);
      ShowWindow(GetDlgItem(HWindow, IDC_PB2), SW_HIDE);
      fileNameList^.DeleteAll;
      retVal := BuildMergeFiles(@Self, fileNameList, userDefineList, workDir, fileDescExt);
      if (retVal = continueJob) and CanContinue then
      begin
        SR(IDS_MERGERESTORE, updateMsg, 255);
        SetText(updateMsg,IDC_TEXT2);
        retVal := MergeFiles(@Self, fileNameList, userDefineList, workDir, dbDir);
      end;
    end;

    ShowWindow(GetDlgItem(HWindow, IDC_PB1_TEXT), SW_HIDE);
    ShowWindow(GetDlgItem(HWindow, IDC_PB1), SW_HIDE);
    ShowWindow(GetDlgItem(HWindow, IDC_PB2_TEXT), SW_HIDE);
    ShowWindow(GetDlgItem(HWindow, IDC_PB2), SW_HIDE);
    if (retVal = continueJob) and (CanContinue) then
    begin
      {- Delete temporary database -}
      SetText(SR(IDS_MERGESUCCESS, updateMsg, 255),IDC_TEXT1);
      SetText(SR(IDS_CleanDir, updateMsg, 255),IDC_TEXT2);
      CleanDir(saveDir,'*.DB*');
    end
    else
    begin
      {- Restore failed must restore online database. -}
      ShowWindow(GetDlgItem(HWindow, IDCANCEL), SW_HIDE);
      SetText(SR(IDS_MERGETEMPORARY, updateMsg, 255),IDC_TEXT1);
      SetText(SR(IDS_RESTOREOLD, updateMsg, 255),IDC_TEXT2);
      {- reset variables so that functions will work -}
      saveVal := retVal;
      retVal := continueJob;
      SetContinue(true);
      BuildArchiveFiles(@Self, fileNameList, saveDir,fileDescExt,backupAllFiles);
      CopyFiles(@Self, fileNameList, saveDir,  dbDir, fileDescExt,diskNum, setRec, false, false, false, nil);
      CopyFiles(@Self, fileNameList, saveDir,  dbDir, datFileExt, diskNum, setRec, false, false, false, nil);
      ShowWindow(GetDlgItem(HWindow, IDCANCEL), SW_SHOW);
      retVal := saveVal;
    end;

    if (retVal <> continueJob) and (retVal <> cancelJob) then
    begin
      SR(IDS_MERGETITLE, updateMsg, 255);
      FMShowError(@Self, updateMsg, retVal, fmErr_Msg);
    end;
  end;

  {-  insufficient disk space to perform this operation. -}
  if (neededBytes > freeBytes) and (retVal = continueJob) then
  begin
    SR(IDS_INFORMATION, aTitle, 255);
    SR(IDS_INSUFFDISKSPACE, updateMsg, 255);
    InfoMsg(self.HWindow, aTitle, updateMsg);
  end;

  SetText(' ', IDC_TEXT1);
  SetText(SR(IDS_CLEANDIR, updateMsg, 255),IDC_TEXT2);
  CleanDir(incrDir,'*.DB*');
  CleanDir(saveDir,'*.DB*');
  CleanDir(workDir,'*.DB*');
  MSDisposeObj(fileNameList);
  MSDisposeObj(userDefineList);

  EndDlg(IDCancel);
end;

{------------------------[FMMerge]------------------------------------------}
{---------------------------------------------------------------------------}
{                                                                           }
{---------------------------------------------------------------------------}
procedure FMMrge(aParent: PWindowsObject);
var
  dlgMerge     : PDlgMerge;

begin
  dlgMerge:= New(PDlgMerge, Init(aParent,true));
  application^.ExecDialog(dlgMerge);
end;

End.

{- DWC }
