{$F+}
unit SectGS;

{- Gram Stain / Free Text section driver }

INTERFACE

{- this module interfaces via SECTREG }

IMPLEMENTATION

uses
  DMSDebug,
  DBLib,
  DBTypes,
  DBErrors,
  DBFile,
  CtlLib,
  GridLib,
  DMSErr,
  MScan,
  DlgLib,
  dbids,
  StrsW,
  SectReg,
  userMsgs,
  ApiTools,
  RptUtil,
  SectFlds,
  BoxWin,
  RptShare,
  StatBar,
  PageWin,
  Objects,
  Strings,
  Win31,
  WinProcs,
  WinTypes,
  OWindows,
  ODialogs;

{$I RPTGEN.INC}

const
  GRID_SLOT   = 0;
  GRID_ABBREV = 1;
  GRID_NAME   = 2;

type
  {- describes the fields that are inserted into a demographic/free text section}
  PSectGSFld = ^TSectGSFld;
  TSectGSFld = object(TObject)
    fldType     : integer;  {- 0 = free text, 1 = label, 2 = abbrev 3 = value }
    buf         : array[0..MaxRptFreeText] of char;  {- free text -or- field
                                                        label (depends on type) }
    slotNum     : integer;  {- gram stain slot number }
    org         : TPoint;   {- position/size of field }
    ext         : TPoint;
    opts        : TFldOpts;
    constructor Init;
    procedure ChangeBuf(aNewBuf: PChar);
  end;

  PGSOpts = ^TGSOpts;
  TGSOpts = record
    font        : TRptFont;
    opts        : TSectOpt;
    flds        : PCollection;  {- collection of fields in section }
  end;

  {- box window - for insertion onto report format }
  PSectGS = ^TSectGS;
  TSectGS = object(TBoxWin)
    gsCfg   : TGSopts;
    gsDB    : PDBFile;
    gsFldDB : PDBFile;
    constructor Init(AParent: PWindowsObject; ATitle: PChar;
                     rect: TRect; AnOrigin: TPoint; AZoom: Real;
                     aPage: TRptPage; AFont: TRptFont);
    destructor Done; virtual;
    procedure DefineBox(aParent: PWindowsObject); virtual;
    procedure ResetDefaults; virtual;
    function GetBoxType: integer; virtual;

    function SavePrep: boolean; virtual;
    function SaveData(var dataSeq: longint): boolean; virtual;
    procedure SaveComplete; virtual;
    function LoadData(aBoxSeq: longint): boolean; virtual;
  end;

  PSectGSEdit = ^TSectGSEdit;
  TSectGSEdit = object(TSectFldEdit)
    cfg     : PGSOpts;
    tcfg    : TGSOpts;
    constructor Init(aParent: PWindowsObject; var aCfg: TGSOpts; aSize: TPoint);
    procedure SetupWindow; virtual;
    function InitPage(aSize: TPoint): PSectPage; virtual;
    procedure IDFont(var msg: TMessage); virtual ID_FIRST + IDC_FONT;
    procedure IDOptions(var msg: TMessage); virtual ID_FIRST + IDC_OPTIONS;
    function CanClose: boolean; virtual;
    procedure CollectFields;
  end;

  {- special pagewindow for use in dialog boxes }
  PGSPage = ^TGSPage;
  TGSPage = object(TSectPage)
    fldList : PCollection;
    constructor Init(AParent: PWindowsObject; aPage: TRptPage;
                     aFont: PRptFont; aFldList: PCollection);
    function InitBox(rect: TRect; anOrigin: TPoint): PBoxWin; virtual;
    procedure UMBoxClose(var msg: TMessage); virtual WM_FIRST + UM_BOXCLOSING;
  end;

  {- Gram stain field (box window) }
  PGSFld = ^TGSFld;
  TGSFld = object(TSectFld)
    fld     : PSectGSFld;
    constructor Init(AParent: PWindowsObject; rect: TRect; AnOrigin: TPoint; AZoom: Real;
                     aPage: TRptPage; AFont: TRptFont; aFld: PSectGSFld);
    procedure DefineBox(aParent: PWindowsObject); virtual;
    procedure GetName(AName: PChar; maxlen: Integer); virtual;
    function  GetNameLength: integer; virtual;
    function  BuildBoxFont(aLogFont: TLogFont): HFont; virtual;
    procedure DrawCaption(dc: HDC); virtual;
  end;

  {- Gram stain field definition dialog box }
  PDefineFldDlg = ^TDefineFldDlg;
  TDefineFldDlg = object(TCenterDlg)
    fld     : PSectGSFld;
    grid    : PGrid;
    constructor Init(aParent: PWindowsObject; aFld: PSectGSFld);
    procedure SetupWindow; virtual;
    function CanClose: boolean; virtual;
    procedure WMMeasureItem(var msg: TMessage); virtual WM_First + WM_MEASUREITEM;
    procedure WMDrawItem(var msg: TMessage); virtual WM_First + WM_DRAWITEM;
    procedure WMCharToItem(var msg: TMessage); virtual WM_First + WM_CHARTOITEM;
    procedure IDGrid(var msg: TMessage); virtual ID_FIRST + IDC_LIST;
  end;

var
  sectionName  : array[0..30] of char;

{-----------------------------------------------------------------[ TGSFld ]--}

constructor TGSFld.Init(AParent: PWindowsObject; rect: TRect; AnOrigin: TPoint; AZoom: Real;
                        aPage: TRptPage; AFont: TRptFont; aFld: PSectGSFld);
begin
  inherited Init(aParent, '', rect, anOrigin, aZoom, aPage, aFont);
  fld:= aFld;
end;

procedure TGSFld.DefineBox;
var
  d   : PDialog;
begin
  d:= New(PDefineFldDlg, Init(GetParentObj, fld));
  application^.ExecDialog(d);
end;

function TGSFld.BuildBoxFont(aLogFont: TLogFont): HFont;
begin
  if fld^.opts.bold then
    aLogFont.lfWeight:= FW_BOLD
  else
    aLogFont.lfWeight:= FW_NORMAL;

  if fld^.opts.underline then
    aLogFont.lfUnderline:= 1
  else
    aLogFont.lfUnderline:= 0;

  BuildBoxFont:= CreateFontIndirect(aLogFont);
end;

procedure TGSFld.DrawCaption(dc: HDC);
{- draw field name on screen }
var
  r       : TRect;
  pstr    : PChar;
  strSize : integer;
  sz      : TLRect;
  LTpPX   : integer;
  LTpPY   : integer;
  k       : integer;
begin
  GetSize(sz);
  LTwipsPerPixel(dc, LTpPX, LTpPY);

  r.top:= 0;
  r.bottom:= sz.bottom - r.top;
  r.left:= LTpPX * 2;
  r.right:= sz.right - r.left;
  pstr:= nil;
  if fld^.fldType = 0 then  {- free text }
  begin
    strSize:= GetNameLength;
    GetMem(pstr, strSize + 1);
    GetName(pstr, strSize);
  end
  else
  begin
    strSize:= 255; {- some length of adequate size }
    GetMem(pstr, strSize + 1);
    {- if fld is the field label then use that, otherwise, figure out the
       proper length for the field }
    if (fld^.fldType = 1) or (fld^.fldType = 2) then
      StrLCopy(pstr, fld^.buf, strSize)  {- label/abbrev is in buf }
    else
    begin {- field value }
      k:= 61;
      CharStr('X', pstr, k - 1);
    end;
  end;

  DrawText(dc, pstr, strlen(pstr), r, fld^.opts.just or DT_WORDBREAK or DT_NOPREFIX);
  MSFreeMem(pstr, strSize + 1);
end;

procedure TGSFld.GetName(aName: PChar; maxlen: Integer);
var
  pstr  : array[0..100] of char;
  p2    : array[0..100] of char;
begin
  StrCopy(aName, '?');
  case fld^.fldType of
    0 : {- free text }
      begin
        if strlen(fld^.buf) = 0 then
          SR(IDS_FREETEXT2, aName, maxLen)
        else
          StrlCopy(aName, fld^.buf, maxLen);
      end;
    1 : {- field label }
      begin
        SR(IDS_NAME, pstr, sizeof(pstr)-1);
        StrCat(pstr, ' - ');
        StrCat(pstr, fld^.buf);
        StrLCopy(aName, pstr, maxLen);
      end;
    2 : {- field abbreviation }
      begin
        SR(IDS_ABBREV, pstr, sizeof(pstr)-1);
        StrCat(pstr, ' - ');
        StrCat(pstr, fld^.buf);
        StrLCopy(aName, pstr, maxLen);
      end;
    3 : {- field value }
      begin
        SR(IDS_VALUE, pstr, sizeof(pstr)-1);
        StrCat(pstr, ' - ');
        StrCat(pstr, fld^.buf);
        StrLCopy(aName, pstr, maxLen);
      end;
  end;
end;

function TGSFld.GetNameLength: integer;
var
  pstr  : array[0..256] of char;
begin
  case fld^.fldType of
    0 : {- free text }
      if strLen(fld^.buf) = 0 then
      begin
        GetNameLength:= StrLen(SR(IDS_FREETEXT, pstr, 100)) + 2;
      end
      else
        GetNameLength:= StrLen(fld^.buf);
    1,2,3 : {- field label/label/abbrev }
      begin
        GetName(pstr, 256);
        GetNameLength:= StrLen(pstr);
      end;
    else
      GetNameLength:= 0;
  end;
end;

{-----------------------------------------------------------[ TSectGSFld ]--}

constructor TSectGSFld.Init;
begin
  inherited Init;
  fldType:= 0;
  ChangeBuf('');
  slotNum:= 0;
  FillChar(org, sizeof(org), 0);
  FillChar(ext, sizeof(ext), 0);
  opts.just:= DT_LEFT;
  opts.bold:= false;
  opts.underline:= false;
end;

procedure TSectGSFld.ChangeBuf(aNewBuf: PChar);
begin
  StrCopy(buf, aNewBuf);
end;

{---------------------------------------------[ TDefineFldDlg ]--}

constructor TDefineFldDlg.Init(aParent: PWindowsObject; aFld: PSectGSFld);
var
  c  : PControl;
  lf : TLogFont;
begin
  fld:= aFld;
  grid:= nil;
  if fld^.fldType > 0 then
  begin
    inherited Init(aParent, MakeIntResource(DLG_FLDLIST));
    c:= New(PLStatic, InitResource(@self, 300, 0, false));
    grid:= New(PGrid, InitResource(@self, IDC_LIST, 3, false));
  end
  else
    inherited Init(aParent, MakeIntResource(DLG_FREETEXT));

  c:= New(PLRadioButton, InitResource(@self, IDC_LEFT, false));
  c:= New(PLRadioButton, InitResource(@self, IDC_CENTER, false));
  c:= New(PLRadioButton, InitResource(@self, IDC_RIGHT, false));
  c:= New(PLCheckBox, InitResource(@self, IDC_BOLD, false));
  c:= New(PLCheckBox, InitResource(@self, IDC_UNDERLINE, false));
end;

procedure TDefineFldDlg.SetupWindow;
var
  fi      : TFldInfo;
  db      : PDBFile;
  focRow  : integer;
  pstr    : array[0..100] of char;
  p1      : array[0..100] of char;
  j       : longint;
  k       : integer;
  tSlot   : byte;
begin
  inherited SetupWindow;
  if fld^.fldType = 0 then
    SendDlgItemMsg(IDC_FREETEXT, WM_SETTEXT, 0, longint(@fld^.buf))
  else
  begin
    GetWindowText(HWindow, pstr, 100);

    if fld^.fldType = 1 then
      SR(IDS_NAME, p1, sizeof(p1)-1)
    else if fld^.fldType = 2 then
      SR(IDS_ABBREV, p1, sizeof(p1)-1)
    else
      SR(IDS_VALUE, p1, sizeof(p1)-1);
    AnsiUpper(p1);

    StrCat(pstr, ' - ');
    StrCat(pstr, p1);

    SetWindowText(HWindow, pstr);

    grid^.SetColumnWidth(GRID_SLOT, 0);
    grid^.SetColumnWidth(GRID_ABBREV, 100);
    grid^.StretchColumn(GRID_NAME);
    grid^.SetHeader(GRID_ABBREV, SR(IDS_ABBREV, p1, sizeof(p1)-1));
    grid^.SetHeader(GRID_NAME, SR(IDS_DESCRIPTION, p1, sizeof(p1)-1));

    focRow:= 0;

    db:= New(PDBFile, Init(DBGSDefFile, '', dbOpenNormal));
    if db = nil then
      ShowError(nil, IDS_CANTOPENGSDEF, nil, dbLastOpenError, MOD_RPTGEN, 0)
    else
    begin
      with db^ do
      begin
        dbc^.GetFirst(dbr);
        while dbc^.dbErrorNum = 0 do
        begin
          dbr^.GetField(DBGSDefSlotNum, @tSlot, sizeof(tSlot));
          Str(tSlot, pstr);
          j:= grid^.AddString('');
          grid^.SetData(GRID_SLOT, j, pstr);

          dbr^.GetField(DBGSDefAbbr, @pstr, sizeof(pstr)-1);
          grid^.SetData(GRID_ABBREV, j, pstr);

          dbr^.GetField(DBGSDefDesc, @pstr, sizeof(pstr)-1);
          grid^.SetData(GRID_NAME, j, pstr);

          if fld^.slotNum = tSlot then
            focRow:= j;

          dbc^.GetNext(dbr);
        end;
      end;
      MSDisposeObj(db);
    end;

    grid^.SetSelIndex(focRow);
  end;

  if fld^.opts.bold then
    SendDlgItemMsg(IDC_BOLD, BM_SETCHECK, BF_CHECKED, 0)
  else
    SendDlgItemMsg(IDC_BOLD, BM_SETCHECK, BF_UNCHECKED, 0);
  if fld^.opts.underline then
    SendDlgItemMsg(IDC_UNDERLINE, BM_SETCHECK, BF_CHECKED, 0)
  else
    SendDlgItemMsg(IDC_UNDERLINE, BM_SETCHECK, BF_UNCHECKED, 0);

  SendDlgItemMsg(IDC_LEFT, BM_SETCHECK, BF_UNCHECKED, 0);
  SendDlgItemMsg(IDC_CENTER, BM_SETCHECK, BF_UNCHECKED, 0);
  SendDlgItemMsg(IDC_RIGHT, BM_SETCHECK, BF_UNCHECKED, 0);
  case fld^.opts.just of
    DT_LEFT:
      SendDlgItemMsg(IDC_LEFT, BM_SETCHECK, BF_CHECKED, 0);
    DT_CENTER:
      SendDlgItemMsg(IDC_CENTER, BM_SETCHECK, BF_CHECKED, 0);
    DT_RIGHT:
      SendDlgItemMsg(IDC_RIGHT, BM_SETCHECK, BF_CHECKED, 0);
  end;
end;

procedure TDefineFldDlg.IDGrid(var msg: TMessage);
begin
  if (msg.lParamHi = LBN_DBLCLK) then
    OK(msg);
end;

procedure TDefineFldDlg.WMMeasureItem(var msg: TMessage);
begin
  if grid <> nil then
    grid^.MeasureItem(msg);
end;

procedure TDefineFldDlg.WMDrawItem(var msg: TMessage);
begin
  if grid <> nil then
    grid^.DrawItem(msg);
end;

procedure TDefineFldDlg.WMCharToItem(var msg: TMessage);
begin
  if grid <> nil then
    grid^.CharToItem(msg);
end;

function TDefineFldDlg.CanClose: boolean;
var
  ret   : boolean;
  tstr  : array[0..MaxRptFreeText] of char;
  code  : integer;
  k     : integer;
begin
  ret:= true;
  if fld^.fldType = 0 then
  begin
    SendDlgItemMsg(IDC_FREETEXT, WM_GETTEXT, MaxRptFreeText, longint(@tstr));
    fld^.ChangeBuf(tstr);
  end
  else
  begin
    k:= grid^.GetSelIndex;
    if k = -1 then
    begin
      ShowError(@self, IDS_MUSTSELFLD, nil, 0, MOD_RPTGEN, 2);
      ret:= false;
    end
    else
    begin
      if fld^.fldType = 2 then
        grid^.GetData(GRID_ABBREV, k, tstr, MaxRptFreeText)
      else
        grid^.GetData(GRID_NAME, k, tstr, MaxRptFreeText);
      fld^.ChangeBuf(tstr);
      grid^.GetData(GRID_SLOT, k, tstr, MaxRptFreeText);
      Val(tstr, fld^.slotNum, code);
    end;
  end;
  if ret then
  begin
    if SendDlgItemMsg(IDC_CENTER, BM_GETCHECK, 0,0) = BF_CHECKED then
      fld^.opts.just:= DT_CENTER
    else if SendDlgItemMsg(IDC_RIGHT, BM_GETCHECK, 0,0) = BF_CHECKED then
      fld^.opts.just:= DT_RIGHT
    else
      fld^.opts.just:= DT_LEFT;
    fld^.opts.bold:= (SendDlgItemMsg(IDC_BOLD, BM_GETCHECK, 0,0) = BF_CHECKED);
    fld^.opts.underline:= (SendDlgItemMsg(IDC_UNDERLINE, BM_GETCHECK, 0,0) = BF_CHECKED);
  end;
  CanClose:= ret;
end;

{---------------------------------------------[ TSectGSEdit ]--}

constructor TSectGSEdit.Init(aParent: PWindowsObject; var aCfg: TGSOpts;
                             aSize: TPoint);
begin
  cfg:= @aCfg;
  tcfg:= cfg^;
  inherited Init(aParent, DLG_FLDEDIT, sectionName, aSize, @tcfg.font);
end;

procedure TSectGSEdit.SetupWindow;
var
  LTpPX, LTpPY  : integer;
  dc            : HDC;
  origin        : TPoint;

  procedure DisplayIt(fld: PSectGSFld); far;
  var
    r   : TRect;
    p   : PGSFld;
    org : TPoint;
  begin
    with pw^ do
    begin
      r.left:= (fld^.org.x + edgeX + page.margin.left) div LTpPX;
      r.top:=  (fld^.org.y + edgeY + page.margin.top) div LTpPY;

      r.right:= r.left + (fld^.ext.x div LTpPX);
      r.bottom:= r.top + (fld^.ext.y div LTpPY);

      GetOrigin(org);
      p:= New(PGSFld, Init(pw, r, org, GetZoom, page, tcfg.font, fld));
      application^.MakeWindow(p);
      boxes^.Insert(p);
      SetFocus(p^.HWindow);
      UpdateFocusChild;
    end;
  end;

begin
  inherited SetupWindow;

  {- need to display all fields here ! }

  dc:= GetDC(HWindow);
  SetLogicalTwips(dc, 1.0);
  LTwipsPerPixel(dc, LTpPX, LTpPY);
  ReleaseDC(HWindow, dc);

  tcfg.flds^.ForEach(@DisplayIt);
end;

procedure TSectGSEdit.IDOptions(var msg: TMessage);
begin
  GetSectOpt(@self, tCfg.opts);
end;

procedure TSectGSEdit.IDFont(var msg: TMessage);
begin
  GetNewFont(tcfg.font);
end;

function TSectGSEdit.InitPage(aSize: TPoint): PSectPage;
var
  page  : TRptPage;
begin
  FillChar(page, sizeof(page), 0);
  page.size.X:= aSize.x;
  page.size.Y:= aSize.y;
  InitPage:= New(PGSPage, Init(@self, page, @tcfg.font, tcfg.flds));
end;

function TSectGSEdit.CanClose: boolean;
var
  k  : integer;
begin
  CollectFields;
  cfg^:= tcfg;
end;

procedure TSectGSEdit.CollectFields;
{- gather all field definitions from the child box field windows }

  procedure GetIt(p: PGSFld); far;
  {- extract size from box field and update the field info record }
  var
    sz  : TLRect;
  begin
    p^.GetSize(sz);

    p^.fld^.org.x:= sz.left;
    p^.fld^.org.y:= sz.top;
    p^.fld^.ext.x:= sz.right;
    p^.fld^.ext.y:= sz.bottom;
  end;

begin
  with tcfg do
  begin
    pw^.boxes^.ForEach(@GetIt);
  end;
end;

{--------------------------------------------------------------[ TGSPage ]--}

constructor TGSPage.Init(AParent: PWindowsObject; aPage: TRptPage;
                         aFont: PRptFont; aFldList: PCollection);
begin
  inherited Init(aParent, aPage, aFont);
  fldList:= aFldList;
end;

function TGSPage.InitBox(rect: TRect; anOrigin: TPoint): PBoxWin;
var
  sel  : integer;
  c    : PCollection;
  pstr : array[0..100] of char;
  fld  : PSectGSFld;
  d    : PDialog;
begin
  InitBox:= nil;

  c:= New(PCollection, Init(10,10));
  c^.Insert(New(PAddFldTypes, Init(SR(IDS_FREETEXT, pstr, 100), 0)));
  c^.Insert(New(PAddFldTypes, Init(SR(IDS_NAME, pstr, 100), 1)));
  c^.Insert(New(PAddFldTypes, Init(SR(IDS_ABBREV, pstr, 100), 2)));
  c^.Insert(New(PAddFldTypes, Init(SR(IDS_GSVAL, pstr, 100), 2)));

  sel:= GetFieldType(parent, c);
  if sel >= 0 then
  begin
    fld:= New(PSectGSFld, Init);
    fld^.fldType:= sel;
    d:= New(PDefineFldDlg, Init(parent, fld));
    if application^.ExecDialog(d) = IDOK then
    begin
      rect.bottom:= rect.top + GetFontHeight;
      InitBox:= New(PGSFld,
        Init(@self, rect, anOrigin, GetZoom, page, pFont^, fld));
      fldList^.Insert(fld);
    end
    else
      MSDisposeObj(fld);
  end;
  MSDisposeObj(c);
end;

procedure TGSPage.UMBoxClose(var msg: TMessage);
{- when a box closes, be sure to remove the fld definition object
   from the list of fields in the configuration for the section }
var
  bw    : PGSFld;
  f     : PSectGSFld;
  k   : integer;
begin
  bw:= PGSFld(msg.lParam);

  k:= fldList^.IndexOf(bw^.fld);
  if k <> -1 then
  begin
    fldList^.AtFree(k);
  end;
  inherited UMBoxClose(msg);
end;

{---------------------------------------[ TSectGS ]--}

constructor TSectGS.Init(AParent: PWindowsObject; ATitle: PChar;
                     rect: TRect; AnOrigin: TPoint; AZoom: Real;
                     aPage: TRptPage; aFont: TRptFont);
begin
  {- need to init flds before calling inherited Init because the ancestor
    calls ResetDefaults which uses flds and expects it to be valid }
  FillChar(gsCfg, sizeof(gsCfg), 0);
  gsCfg.flds:= New(PCollection, Init(20, 10));
  gsDB:= nil;
  gsFldDB:= nil;

  inherited Init(aParent, aTitle, rect, anOrigin, aZoom, aPage, aFont);
end;

destructor TSectGS.Done;
begin
  inherited Done;
  MSDisposeObj(gsCfg.flds);
end;

procedure TSectGS.DefineBox;
var
  sz    : TLRect;
  aSize : TPoint;
begin
  GetSize(sz);
  aSize.x:= integer(sz.right);
  aSize.y:= integer(sz.bottom);
  application^.ExecDialog(New(PSectGSEdit, Init(GetParentObj, gsCfg, aSize)));
  SetDisplayFont(gsCfg.Font);
end;

procedure TSectGS.ResetDefaults;
begin
  with gsCfg do
  begin
    with opts do
    begin
      bkMode:= OPAQUE;
      border:= -1;  {- none }
    end;
    MakeDefaultRFont(font);
    flds^.FreeAll;
  end;
end;

function TSectGS.GetBoxType: integer;
begin
  GetBoxType:= SectGSID;
end;

function TSectGS.SavePrep: boolean;
{- open database files and prepare to save }
var
  pstr: array[0..200] of char;
begin
  gsFldDB:= nil;
  gsDB:= New(PDBFile, Init(DBRPTGSSectsFile, '', dbOpenNormal));
  if gsDB = nil then
  begin
    ShowError(GetParentObj, IDS_CANTOPENGSSECT, nil, dbLastOpenError, MOD_RPTGEN, 1);
  end
  else
  begin
    gsFldDB:= New(PDBFile, Init(DBRPTGSFldsFile, '', dbOpenNormal));
    if gsFldDB = nil then
    begin
      ShowError(GetParentObj, IDS_CANTOPENGSFLDS, nil, dbLastOpenError, MOD_RPTGEN, 1);
    end;
  end;
  SavePrep:= (gsDB <> nil) and (gsFldDB <> nil);
end;

function TSectGS.SaveData(var dataSeq: longint): boolean;
var
  pstr: array[0..200] of char;
  sz  : TLRect;
  ret : boolean;

  procedure SaveFld(fld: PSectGSFld); far;
  {- save data for specified field. Assumes boxSeq has the Seq number for the
     section record }
  begin
    if not ret then exit;
    with gsFldDB^ do
    begin
      dbr^.PutField(DBRptGSFldsSectRef, @boxSeq);
      with fld^ do
      begin
        dbr^.PutField(DBRptGSFldsFldType, @fldType);
        dbr^.PutField(DBRptGSFldsBuf, @buf);
        dbr^.PutField(DBRptGSFldsSlotNum, @slotNum);
        dbr^.PutField(DBRptGSFldsOrg, @org);
        dbr^.PutField(DBRptGSFldsExt, @ext);
        dbr^.PutField(DBRptGSFldsOpts, @opts);
      end;
      dbc^.InsertRec(dbr);
      if dbc^.dbErrorNum <> 0 then
      begin
        ret:= false;
        ShowError(GetParentObj, IDS_CANTINSERTGSFLD, nil, dbc^.dbErrorNum, MOD_RPTGEN, 0);
      end;
    end;
  end;

begin
  ret:= true;
  with gsDB^ do
  begin
    GetSize(sz);

    dbr^.PutField(DBRptGSSectFont, @gsCfg.font);
    dbr^.PutField(DBRptGSSectOptions, @gsCfg.opts);
    dbr^.PutField(DBRptGSSectSize, @sz);

    dbc^.InsertRec(dbr);
    if dbc^.dbErrorNum = 0 then
      boxSeq:= dbr^.GetSeqValue  {- save new seq number }
    else
      ret:= false;
    dataSeq:= boxSeq;

    if ret then  {- now save all fields for the section }
      gsCfg.flds^.ForEach(@SaveFld);
  end;
  SaveData:= ret;
end;

procedure TSectGS.SaveComplete;
begin
  MSDisposeObj(gsDB);
  MSDisposeObj(gsFldDB);
end;

function TSectGS.LoadData(aBoxSeq: longint): boolean;
var
  ret : boolean;
  r   : TLRect;
  db  : PDBFile;
  p1  : array[0..200] of char;

  procedure LoadFields;
  {- load fields from database. Assumes that boxSeq is the seq for the section }
  var
    seq    : longint;
    dbFlds : PDBFile;
    f      : PSectGSFld;
  begin
    gsCfg.flds^.FreeAll;
    dbFlds:= New(PDBFile, Init(DBRPTGSFldsFile, '', dbOpenNormal));
    if dbFlds = nil then
    begin
      ret:= false;
      ShowError(nil, IDS_CANTOPENGSFLDS, nil, dbLastOpenError, MOD_RPTGEN, 2);
    end
    else
    begin
      with dbFlds^ do
      begin
        seq:= boxSeq;
        dbr^.PutField(DBRptGSFldsSectRef, @boxSeq);
        dbc^.GetEQ(dbr);
        while (dbc^.dbErrorNum = 0) and (seq = boxSeq) do
        begin
          f:= New(PSectGSFld, Init);
          dbr^.GetField(DBRptGSFldsFldType, @f^.fldType,  sizeof(f^.fldType));
          dbr^.GetField(DBRptGSFldsBuf,     @f^.buf,      sizeof(f^.buf));
          dbr^.GetField(DBRptGSFldsSlotNum, @f^.slotNum,  sizeof(f^.slotNum));
          dbr^.GetField(DBRptGSFldsOrg,     @f^.org,      sizeof(f^.org));
          dbr^.GetField(DBRptGSFldsExt,     @f^.ext,      sizeof(f^.ext));
          dbr^.GetField(DBRptGSFldsOpts,    @f^.opts,     sizeof(f^.opts));
          gsCfg.flds^.Insert(f);
          dbc^.GetNext(dbr);
          if dbc^.dbErrorNum = 0 then
            dbr^.GetField(DBRptGSFldsSectRef, @seq, sizeof(seq));
        end;
      end;
      MSDisposeObj(dbFlds);
    end;
  end;

begin
  inherited LoadData(aBoxSeq);
  ret:= true;

  db:= New(PDBFile, Init(DBRPTGSSectsFile, '', dbOpenNormal));
  if db = nil then
  begin
    ShowError(nil, IDS_CANTOPENGSSECT, nil, dbLastOpenError, MOD_RPTGEN, 2);
    CloseWindow;  {- delete the section from the report }
    boxSeq:= -1;
    ret:= false;
  end
  else
  begin
    boxSeq:= aBoxSeq;

    with db^ do
    begin
      dbc^.GetSeq(dbr, boxSeq);
      if dbc^.dbErrorNum = 0 then
      begin
        dbr^.GetField(DBRptGSSectFont, @gsCfg.font, sizeof(gsCfg.font));
        SetDisplayFont(gsCfg.font);

        dbr^.GetField(DBRptGSSectOptions, @gsCfg.opts, sizeof(gsCfg.opts));

        dbr^.GetField(DBRptGSSectSize, @r, sizeof(r));
        SetSize(r);

        {- now load all fields for the section }
        LoadFields;
      end
      else
      begin
        ShowError(nil, IDS_CANTLOCSECTRECT, nil, dbc^.dbErrorNum, MOD_RPTGEN, 4);
        CloseWindow;  {- delete the section from the report }
        boxSeq:= -1;
        ret:= false;
      end;
    end;
    MSDisposeObj(db);
  end;
  LoadData:= ret;
end;

{-----------------[ Section Registration ]--}

function SectGSDelete(action: integer; aRptSeq, aSectSeq: longint): boolean;
{- section delete function (for section registration }
const
  dbSect : PDBFile = nil;
  dbFlds : PDBFile = nil;
var
  ret   : boolean;
begin
  ret:= true;
  if (action and ACTION_OPEN <> 0) then
  begin
    if dbSect = nil then
    begin
      dbSect:= New(PDBFile, Init(DBRPTGSSectsFile, '', dbOpenNormal));
      if dbSect = nil then
      begin
        ShowError(nil, IDS_CANTOPENGSSECT, nil, dbLastOpenError, MOD_RPTGEN, 3);
        ret:= false;
      end;
    end;
    if dbFlds = nil then
    begin
      dbFlds:= New(PDBFile, Init(DBRPTGSFldsFile, '', dbOpenNormal));
      if dbFlds = nil then
      begin
        ShowError(nil, IDS_CANTOPENGSFLDS, nil, dbLastOpenError, MOD_RPTGEN, 3);
        ret:= false;
      end;
    end;
  end;

  if ret then
  begin
    if (action and ACTION_DELETE <> 0) then
    begin
      if (dbSect = nil) or (dbFlds = nil) then
        ret:= false
      else
      begin
        with dbSect^ do
        begin
          dbc^.GetSeq(dbr, aSectSeq);
          if dbc^.dbErrorNum = 0 then
            dbc^.DeleteRec(dbr);
        end;
        with dbFlds^ do
        begin
          dbr^.PutField(DBRptGSFldsSectRef, @aSectSeq);
          while (dbc^.dbErrorNum = 0) do
          begin
            dbc^.GetEQ(dbr);
            if dbc^.dbErrorNum = 0 then
              dbc^.DeleteRec(dbr);
          end;
        end;
      end;
    end;
  end;

  if ret and (action and ACTION_CLOSE <> 0) then
  begin
    MSDisposeObj(dbSect);
    MSDisposeObj(dbFlds);
  end;

  SectGSDelete:= ret;
end;

function SectGSInit(aParent: PWindowsObject; r: TRect; origin: TPoint;
                      aZoom: Real; page: TRptPage; aFont: TRptFont): PBoxWin;
begin
  SectGSInit:= New(PSectGS, Init(aParent, sectionName, r, origin, azoom, page, aFont));
end;

BEGIN
  sections^.RegisterSection(SectGSInit, SectGSDelete,
                  SR(IDS_GS, sectionName, sizeof(sectionName)), sectGSID);
END.

{- rcf }
