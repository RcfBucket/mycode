{----------------------------------------------------------------------------}
{  Module Name  : WAMAIN.PAS                                                 }
{  Programmer   : EJ                                                         }
{  Date Created : 05/04/95                                                   }
{                                                                            }
{  Purpose -                                                                 }
{  This module provides the user the ability to access and interact          }
{  with the WA instrument and related functionality.                         }
{                                                                            }
{  Assumptions -                                                             }
{  None.                                                                     }
{                                                                            }
{  Revision History -                                                        }
{                                                                            }
{  Version  Date      Who    Did What                                        }
{  -------  --------  ------ ----------------------------------------------- }
{  1.00     05/04/95  EJ     Initial release.                                }
{                                                                            }
{----------------------------------------------------------------------------}

unit WAMain;

INTERFACE

uses
  OWindows,
  WinTypes,
  UserMsgs,
  DlgLib,
  CtlLib,
  SpinEdit,
  GridLib,
  Sessions,
  Trays,
  DBFile,
  DBTypes,
  WAGPIB;

{$I WAMAIN.INC}

const
  LSM_TOWER         = 0;
  LSM_SLOT          = 1;
  LSM_PANELID       = 2;
  LSM_PANELTYPE     = 3;
  LSM_SPECID        = 4;
  LSM_COLLDT        = 5;
  LSM_ISOID         = 6;
  LSM_PATID         = 7;
  LSM_LASTREAD      = 8;
  LSM_NEXTREAD      = 9;
  LSM_STATUS        = 10;

type
  PWAMainDlg = ^TWAMainDlg;
  TWAMainDlg = object(TCenterDlgWindow)
    lsmGrid      : PGrid;
    trayObj      : PTraysObject;
    isoordDB     : PDBFile;
    wapDB        : PDBFile;
    waexlistDB   : PDBFile;
    cAutoUpload  : PLCheckBox;
    inAutoUpload : boolean;
    {}
    constructor Init;
    destructor Done; virtual;
    function  GetClassName: PChar; virtual;
    procedure GetWindowClass(var aWndClass: TWndClass); virtual;
    procedure WMDMSQuery(var msg: TMessage); virtual WM_FIRST + WM_MAINPROGQUERY;
    procedure SetupWindow; virtual;
    procedure AutoUpload(var msg: TMessage); virtual ID_FIRST + IDC_AUTO_UPLOAD;
    procedure ShowMap(var msg: TMessage); virtual ID_FIRST + IDC_SHOW_MAP;
    procedure PrintMap(var msg: TMessage); virtual ID_FIRST + IDC_PRINT_MAP;
    procedure IDPanel(var msg: TMessage); virtual ID_FIRST + IDC_IDPANEL;
    procedure PanelInfo(var msg: TMessage); virtual ID_FIRST + IDC_PANEL_INFO;
    procedure PrintCPSR(var msg: TMessage); virtual ID_FIRST + IDC_PRINT_CPSR;
    procedure AccessWA(var msg: TMessage); virtual ID_FIRST + IDC_ACCESS_WA;
    procedure PrintQC(var msg: TMessage); virtual ID_FIRST + IDC_PRINT_QC;
    procedure Maint(var msg: TMessage); virtual ID_FIRST + IDC_MAINTENANCE;
    procedure Barcodes(var msg: TMessage); virtual ID_FIRST + IDC_BARCODES;
    procedure ResultsInquiry(var msg: TMessage); virtual ID_FIRST + IDC_RESULTS_INQ;
    procedure Session(var msg: TMessage); virtual ID_FIRST + BTN_SESSION;
    procedure PrintExceptions(var msg: TMessage); virtual ID_FIRST + IDC_PRINT_EXCEPTIONS;
    procedure HandleGrid(var msg: TMessage); virtual ID_FIRST + IDC_LOADSTAT_GRID;
    procedure Cancel(var msg: TMessage); virtual ID_FIRST + IDCANCEL;
    procedure FillGrid;
    procedure EnableButtons; virtual;
    procedure WaitForBCSResponse;
    procedure CheckPanels;
    procedure SetSession;
    procedure TimerTick(var msg: TMessage); virtual WM_FIRST + wm_Timer;
    procedure ShowWAStatus(var msg: TMessage); virtual WM_FIRST + WM_WASTATUS;
    procedure DrawItem(var msg: TMessage); virtual WM_FIRST + wm_DrawItem;
    procedure MeasureItem(var msg: TMessage); virtual WM_FIRST + wm_MeasureItem;
    procedure CharToItem(var msg: TMessage); virtual WM_FIRST + wm_CharToItem;
    {}
    private
    spnTower     : PSpinInt;
    lsmTower     : integer;
    patDB        : PDBFile;
    spcDB        : PDBFile;
    isoDB        : PDBFile;
    checkingBCS  : boolean;
    userCantWait : boolean;
    curSessRef   : TSeqNum;
    waStatBuf    : array [0..128] of char;
    timerCntr    : word;
  end;

  TWAGridInfo = record
    tower   : integer;
    slot    : byte;
    bsiStat : byte;
  end;

procedure Start;


IMPLEMENTATION

uses
  ODialogs,
  WinProcs,
  Strings,
  Win31,
  MTask,
  StrsW,
  MScan,
  INILib,
  Ctl3D,
  APITools,
  ListLib,
  PrnPrev,
  Report,
  WaitLib,
  Bits,
  DBIDs,
  DBLib,
  Barcodes,
  JamCheck,
  WAMaint,
  WAPanel,
  WACPSR,
  WAQC,
  WAReader,
  WAUpload,
  WAUtil,
  DMSErr,
  DMSDebug;

{$R WAMAIN.RES}


const
  LAST_TOWER        = 8;
  LSM_COLS          = 11;
  IDT_AUTO_UPLOAD   = 200;
  AUTO_TIMEOUT      = 10000;


type
  TWAMainApp = object(T3dApplication)
    procedure InitMainWindow; virtual;
  end;

var
  waMainApp : TWAMainApp;
  errTitle  : array [0..40] of char;

{-------------------------------------------------------[ TWAMainDlg ]--}

constructor TWAMainDlg.Init;
var
  c    : PControl;
  pStr : array[0..10] of char;
begin
  inherited Init(nil, MakeIntResource(DLG_WAMAIN));
  cAutoUpload := New(PLCheckBox, InitResource(@self, IDC_AUTO_UPLOAD, FALSE));
  c := New(PLStatic, InitResource(@self, IDC_LBL1, 0, FALSE));
  spnTower := New(PSpinInt, InitResource(@self, IDC_TOWER, 1, 0,
                                         0, LAST_TOWER, 1));
  lsmGrid := New(PGrid, InitResource(@self, IDC_LOADSTAT_GRID, LSM_COLS, FALSE));
  trayObj := New(PTraysObject, Init);
  userCantWait := FALSE;
  checkingBCS := FALSE;
  inAutoUpload := FALSE;
  timerCntr := 0;


  patDB := New(PDBFile, Init(DBPATFile, '', dbOpenNormal));
  if patDB = nil then
  begin
    ShowError(Application^.MainWindow, IDS_DATABASE_ERR, nil, dbLastOpenError, MOD_WA, 1);
    Halt;
  end;
  spcDB := New(PDBFile, Init(DBSPECFile, '', dbOpenNormal));
  if spcDB = nil then
  begin
    ShowError(Application^.MainWindow, IDS_DATABASE_ERR, nil, dbLastOpenError, MOD_WA, 2);
    Halt;
  end;
  isoDB := New(PDBFile, Init(DBISOFile, '', dbOpenNormal));
  if isoDB = nil then
  begin
    ShowError(Application^.MainWindow, IDS_DATABASE_ERR, nil, dbLastOpenError, MOD_WA, 3);
    Halt;
  end;
  isoordDB := New(PDBFile, Init(DBISOORDFile, '', dbOpenNormal));
  if isoordDB = nil then
  begin
    ShowError(Application^.MainWindow, IDS_DATABASE_ERR, nil, dbLastOpenError, MOD_WA, 4);
    Halt;
  end;
  wapDB := New(PDBFile, Init(DBWAPNLFile, '', dbOpenNormal));
  if wapDB = nil then
  begin
    ShowError(Application^.MainWindow, IDS_DATABASE_ERR, nil, dbLastOpenError, MOD_WA, 5);
    Halt;
  end;
  waexlistDB := New(PDBFile, Init(DBWAEXFile, '', dbOpenNormal));
  if waexlistDB = nil then
  begin
    ShowError(Application^.MainWindow, IDS_DATABASE_ERR, nil, dbLastOpenError, MOD_WA, 6);
    Halt;
  end;
  curSessRef := GetCurrentSession(pStr, 10);
  SetSession;
end;

destructor TWAMainDlg.Done;
begin
  if inAutoUpload then
    KillTimer(HWindow, IDT_AUTO_UPLOAD);
  INISetAutoUploadWA(inAutoUpload);
  MSDisposeObj(waexlistDB);
  MSDisposeObj(wapDB);
  MSDisposeObj(isoordDB);
  MSDisposeObj(isoDB);
  MSDisposeObj(spcDB);
  MSDisposeObj(patDB);
  MSDisposeObj(trayObj);
  inherited Done;
end;

function TWAMainDlg.GetClassName: PChar;
begin
  GetClassName := application^.name;
end;

procedure TWAMainDlg.GetWindowClass(var aWndClass: TWndClass);
begin
  inherited GetWindowClass(aWndClass);
  aWndClass.HIcon := LoadIcon(HInstance, MakeIntResource(ICON_1));
end;

procedure TWAMainDlg.WMDMSQuery(var msg: TMessage);
{- respond to the task module and tell it who I am }
begin
  StrLCopy(PChar(msg.lParam), application^.Name, msg.wParam);
  msg.result:= DMSMainProgRetCode;
  SetWindowLong(HWindow, DWL_MSGRESULT, msg.result);  {- set message result (for dialogs) }
end;

procedure TWAMainDlg.SetupWindow;
var
  pstr  : array [0..40] of char;
  wc    : PWaitCursor;
begin
  wc := New(PWaitCursor, Init);
  inherited SetupWindow;
  WAID := InitWA(1);     { returns -1 if no WA attached }
  Application^.MakeWindow(New(PJamCheckerWindow, Init(@self, 'Jam Checker', WAID)));
  if TheJamChecker <> nil then
    TheJamChecker^.SetWAStatusWindow(HWindow, waStatBuf);
  lsmGrid^.SetHeader(LSM_TOWER,     SR(IDS_TOWER, pstr, 40));
  lsmGrid^.SetHeader(LSM_SLOT,      SR(IDS_SLOT, pstr, 40));
  lsmGrid^.SetHeader(LSM_PANELID,   SR(IDS_PANELID, pstr, 40));
  lsmGrid^.SetHeader(LSM_PANELTYPE, SR(IDS_PANELTYPE, pstr, 40));
  lsmGrid^.SetHeader(LSM_SPECID,    SR(IDS_SPECID, pstr, 40));
  lsmGrid^.SetHeader(LSM_COLLDT,    SR(IDS_COLLDT, pstr, 40));
  lsmGrid^.SetHeader(LSM_ISOID,     SR(IDS_ISOID, pstr, 40));
  lsmGrid^.SetHeader(LSM_PATID,     SR(IDS_PATID, pstr, 40));
  lsmGrid^.SetHeader(LSM_LASTREAD,  SR(IDS_LASTREAD, pstr, 40));
  lsmGrid^.SetHeader(LSM_NEXTREAD,  SR(IDS_NEXTREAD, pstr, 40));
  lsmGrid^.SetHeader(LSM_STATUS,    SR(IDS_STATUS, pstr, 40));
  lsmGrid^.SetMaxCharWidth(LSM_TOWER, 2);
  lsmGrid^.SetMaxCharWidth(LSM_SLOT, 2);
  lsmGrid^.SetMaxCharWidth(LSM_PANELID, 2);
  lsmGrid^.SetMaxCharWidth(LSM_PANELTYPE, 5);
  lsmGrid^.SetColumnWidth(LSM_SPECID, 3);
  lsmGrid^.SetColumnWidth(LSM_COLLDT, 3);
  lsmGrid^.SetColumnWidth(LSM_ISOID, 3);
  lsmGrid^.SetColumnWidth(LSM_PATID, 3);
  lsmGrid^.SetAvgCharWidth(LSM_LASTREAD, 7);
  lsmGrid^.SetAvgCharWidth(LSM_NEXTREAD, 7);
  lsmGrid^.SetMaxCharWidth(LSM_STATUS, 15);
  lsmGrid^.StretchColumn(LSM_STATUS);
  if (WAID = -1) then
  begin
    inAutoUpload := FALSE;
    EnableWindow(GetItemHandle(IDC_AUTO_UPLOAD), FALSE);
  end
  else
  begin
    CheckPanels;
    inAutoUpload := INIGetAutoUploadWA;
    if inAutoUpload then
    begin
      cAutoUpload^.Check;
      SetTimer(HWindow, IDT_AUTO_UPLOAD, AUTO_TIMEOUT, nil);
    end;
  end;
  MSDisposeObj(wc);
end;

procedure TWAMainDlg.AutoUpload(var msg: TMessage);
begin
  if msg.lParamHi = BN_CLICKED then
  begin
    timerCntr := 0;
    inAutoUpload := cAutoUpload^.GetCheck = bf_Checked;
    if inAutoUpload then
      SetTimer(HWindow, IDT_AUTO_UPLOAD, AUTO_TIMEOUT, nil)
    else
      KillTimer(HWindow, IDT_AUTO_UPLOAD);
  end;
  EnableButtons;
  DefWndProc(msg);
end;

procedure TWAMainDlg.ShowMap(var msg: TMessage);
begin
  if not spnTower^.NumInRange then
  begin
    ShowError(@self, IDS_TOWER_ERR, nil, 0, 0, 0);
    FocusCtl(HWindow, IDC_TOWER);
    Exit;
  end;
  lsmTower := spnTower^.GetLong;
  lsmGrid^.SetRedraw(FALSE);
  lsmGrid^.ClearList;
  FillGrid;
  lsmGrid^.SetSelIndex(0);
  lsmGrid^.SetRedraw(TRUE);
  EnableButtons;
end;

procedure TWAMainDlg.PrintMap(var msg: TMessage);
var
  tstr : array [0..80] of char;
  d    : PPrnPrevDlg;
  pi   : PPrintInfo;
  i    : integer;
  lf   : TLogFont;
begin
  MakeScreenFont(lf, FALSE, FALSE);

  pi := New(PPrintInfo, Init(lf, INIWAEPIPointSize));
  if pi = nil then
    Exit;
  if inAutoUpload then
    KillTimer(HWindow, IDT_AUTO_UPLOAD);

  {print title}
  pi^.header^.AddRow(r_CenterRow, 0);
  SR(IDS_LOADSTATUS, tstr, 80);
  pi^.header^.AddCell(tstr, LT300, c_Center or c_Bold, 0);
  pi^.header^.AddRow(0, 0);

  { print the column headers }
  pi^.header^.AddRow(0, 0);

  {tower}
  lsmGrid^.GetHeader(0, tStr, 80);
  pi^.header^.AddCell(tstr, LT50, c_Left, 0);

  {slot}
  lsmGrid^.GetHeader(1, tStr, 80);
  pi^.header^.AddCell(tstr, LT50, c_Left, 0);

  {panel ID}
  lsmGrid^.GetHeader(2, tStr, 80);
  pi^.header^.AddCell(tstr, LT75, c_Left, 0);

  {panel type}
  lsmGrid^.GetHeader(3, tStr, 80);
  pi^.header^.AddCell(tstr, LT100, c_Left, 0);

  {specimen ID}
  lsmGrid^.GetHeader(4, tStr, 80);
  pi^.header^.AddCell(tstr, LT100, c_Left, 0);

  {collect date}
  lsmGrid^.GetHeader(5, tStr, 80);
  pi^.header^.AddCell(tstr, LT100, c_Left, 0);

  {isolate ID}
  lsmGrid^.GetHeader(6, tStr, 80);
  pi^.header^.AddCell(tstr, LT50, c_Left, 0);

  {patient ID}
  {not included in printout}

  {last read}
  {not included in printout}

  {next read}
  lsmGrid^.GetHeader(9, tStr, 80);
  pi^.header^.AddCell(tstr, LT100, c_Left, 0);

  {status}
  lsmGrid^.GetHeader(10, tStr, 80);
  pi^.header^.AddCell(tstr, LT200, c_Left, 0);
  pi^.header^.AddRow(0, 0);

  {print the data }

  for i:= 0 to lsmGrid^.GetRowCount - 1 do
  begin
    pi^.body^.AddRow(0, 0);

    {tower}
    lsmGrid^.GetData(0, i, tStr, 80);
    pi^.body^.AddCell(tstr, LT50, c_Left, 0);

    {slot}
    lsmGrid^.GetData(1, i, tStr, 80);
    pi^.body^.AddCell(tstr, LT50, c_Left, 0);

    {panel ID}
    lsmGrid^.GetData(2, i, tStr, 80);
    if tStr[0] = #0 then
      StrCopy(tStr, ' ');
    pi^.body^.AddCell(tstr, LT75, c_Left, 0);

    {panel type}
    lsmGrid^.GetData(3, i, tStr, 80);
    if tStr[0] = #0 then
      StrCopy(tStr, ' ');
    pi^.body^.AddCell(tstr, LT100, c_Left, 0);

    {specimen ID}
    lsmGrid^.GetData(4, i, tStr, 80);
    if tStr[0] = #0 then
      StrCopy(tStr, ' ');
    pi^.body^.AddCell(tstr, LT100, c_Left, 0);

    {collect date}
    lsmGrid^.GetData(5, i, tStr, 80);
    if tStr[0] = #0 then
      StrCopy(tStr, ' ');
    pi^.body^.AddCell(tstr, LT100, c_Left, 0);

    {isolate ID}
    lsmGrid^.GetData(6, i, tStr, 80);
    if tStr[0] = #0 then
      StrCopy(tStr, ' ');
    pi^.body^.AddCell(tstr, LT50, c_Left, 0);

    {patient ID}
    {not included in printout}

    {last read}
    {not included in printout}

    {next read}
    lsmGrid^.GetData(9, i, tStr, 80);
    if tStr[0] = #0 then
      StrCopy(tStr, ' ');
    pi^.body^.AddCell(tstr, LT100, c_Left, 0);

    {status}
    lsmGrid^.GetData(10, i, tStr, 80);
    if tStr[0] = #0 then
      StrCopy(tStr, ' ');
    pi^.body^.AddCell(tstr, LT200, c_Left, 0);
  end;

  SR(IDS_PRINTLOADSTATUS, tstr, 80);
  d := New(PPrnPrevDlg, Init(@self, pi, tstr));
  Application^.ExecDialog(d);
  MSDisposeObj(pi);
  if inAutoUpload then
    SetTimer(HWindow, IDT_AUTO_UPLOAD, AUTO_TIMEOUT, nil);
end;

procedure TWAMainDlg.IDPanel(var msg: TMessage);
var
  info  : TWAGridInfo;
  d     : PIDPanelDlg;
  sel   : integer;
  wc    : PWaitCursor;
begin
  sel := lsmGrid^.GetSelIndex;
  if sel = lb_Err then
    Exit;
  wc := New(PWaitCursor, Init);
  longint(info) := lsmGrid^.GetItemData(sel);
  if info.bsiStat = 2 then
  begin
    if inAutoUpload then
      KillTimer(HWindow, IDT_AUTO_UPLOAD);
    d := New(PIDPanelDlg, Init(@self, MakeIntResource(DLG_IDPANEL), info));
    if Application^.ExecDialog(d) = IDOK then
    begin
      lsmGrid^.SetRedraw(FALSE);
      lsmGrid^.ClearList;
      FillGrid;
      lsmGrid^.SetSelIndex(sel);
      lsmGrid^.SetRedraw(TRUE);
      EnableButtons;
    end;
    if inAutoUpload then
      SetTimer(HWindow, IDT_AUTO_UPLOAD, AUTO_TIMEOUT, nil);
  end;
  MSDisposeObj(wc);
end;

procedure TWAMainDlg.PanelInfo(var msg: TMessage);
var
  info  : TWAGridInfo;
  d     : PPanelInfoDlg;
  sel   : integer;
begin
  sel := lsmGrid^.GetSelIndex;
  if sel = lb_Err then
    Exit;
  longint(info) := lsmGrid^.GetItemData(sel);
  if (info.bsiStat <> 0) and (info.bsiStat <> 2) then
  begin
    if inAutoUpload then
      KillTimer(HWindow, IDT_AUTO_UPLOAD);
    d := New(PPanelInfoDlg, Init(@self, MakeIntResource(DLG_PANELINFO)));
    Application^.ExecDialog(d);
    if inAutoUpload then
      SetTimer(HWindow, IDT_AUTO_UPLOAD, AUTO_TIMEOUT, nil);
  end;
end;

procedure TWAMainDlg.PrintCPSR(var msg: TMessage);
begin
  if inAutoUpload then
    KillTimer(HWindow, IDT_AUTO_UPLOAD);

  isoDB^.dbr^.ClearRecord;
  isoDB^.dbr^.PutField(DBISOSess, @curSessRef);
  WACPSR.PrintCPSR(@self, isoDB, patDB, spcDB, wapDB);
  EnableButtons;
  if inAutoUpload then
    SetTimer(HWindow, IDT_AUTO_UPLOAD, AUTO_TIMEOUT, nil);
end;

procedure TWAMainDlg.AccessWA(var msg: TMessage);
var
  somethingHappened : boolean;
begin
  if inAutoUpload then
    KillTimer(HWindow, IDT_AUTO_UPLOAD);
  somethingHappened := FALSE;
  checkingBCS := TRUE;
  WAMaint.AccessWA(@self, somethingHappened);
  if somethingHappened then
  begin
    WaitForBCSResponse;
  end;
  checkingBCS := FALSE;
  EnableButtons;
  if inAutoUpload then
    SetTimer(HWindow, IDT_AUTO_UPLOAD, AUTO_TIMEOUT, nil);
end;

procedure TWAMainDlg.PrintQC(var msg: TMessage);
begin
  if inAutoUpload then
    KillTimer(HWindow, IDT_AUTO_UPLOAD);
  WAQC.PrintQC(@self);
  EnableButtons;
  if inAutoUpload then
    SetTimer(HWindow, IDT_AUTO_UPLOAD, AUTO_TIMEOUT, nil);
end;

procedure TWAMainDlg.Maint(var msg: TMessage);
var
  somethingHappened : boolean;
begin
  if inAutoUpload then
    KillTimer(HWindow, IDT_AUTO_UPLOAD);
  somethingHappened := FALSE;
  checkingBCS := TRUE;
  WAMaintenance(@self, waStatBuf, somethingHappened);
  if TheJamChecker <> nil then
    TheJamChecker^.SetWAStatusWindow(HWindow, waStatBuf);
  if somethingHappened then
  begin
    WaitForBCSResponse;
  end;
  checkingBCS := FALSE;
  EnableButtons;
  if inAutoUpload then
    SetTimer(HWindow, IDT_AUTO_UPLOAD, AUTO_TIMEOUT, nil);
end;

procedure TWAMainDlg.Barcodes(var msg: TMessage);
var
  listObj : PListObject;
begin
  listObj := New(PListObject, Init);
  if listObj = nil then
    Exit;
  if inAutoUpload then
    KillTimer(HWindow, IDT_AUTO_UPLOAD);
  PrintBarcodes(@self, MakeIntResource(DLG_WA_BARCODES), curSessRef, 0,
                trayObj, listObj, isoDB, wapDB);
  MSDisposeObj(listObj);
  if inAutoUpload then
    SetTimer(HWindow, IDT_AUTO_UPLOAD, AUTO_TIMEOUT, nil);
end;

procedure TWAMainDlg.ResultsInquiry(var msg: TMessage);
begin
  if inAutoUpload then
    KillTimer(HWindow, IDT_AUTO_UPLOAD);
  WAReader.ResultsInquiry(@self, curSessRef, waexlistDB);
  if inAutoUpload then
    SetTimer(HWindow, IDT_AUTO_UPLOAD, AUTO_TIMEOUT, nil);
end;

procedure TWAMainDlg.Session(var msg: TMessage);
var
  listObj : PListObject;
begin
  listObj := New(PListObject, Init);
  if listObj = nil then
    Exit;
  if inAutoUpload then
    KillTimer(HWindow, IDT_AUTO_UPLOAD);

  { prompt for session }
  isoDB^.dbr^.PutField(DBISOSess, @curSessRef);
  if application^.ExecDialog(New(PSelectSessionDlg,
               Init(@self, isoDB, listObj))) = IDOK then
  begin
    isoDB^.dbr^.GetField(DBISOSess, @curSessRef, SizeOf(curSessRef));
    SetSession;
    EnableButtons;
  end;
  MSDisposeObj(listObj);
  if inAutoUpload then
    SetTimer(HWindow, IDT_AUTO_UPLOAD, AUTO_TIMEOUT, nil);
end;

procedure TWAMainDlg.PrintExceptions(var msg: TMessage);
var
  WAPrintObj : PReport;
  temp       : array [0..64] of char;
  seq        : TSeqNum;
  exNum      : integer;
  wc         : PWaitCursor;
begin
  if inAutoUpload then
    Exit;

  SR(IDS_WAEXCEPT_REPORT, temp, 64);
  WAPrintObj := New(PReport, Init(@self, temp, 120, 60));
  if WAPrintObj = nil then
  begin
    ShowError(@self, IDS_WA_ERR, nil, 0, MOD_WA, 7);
    Exit;
  end;

  wc := New(PWaitCursor, Init);
  WAPrintObj^.abortPrinting := FALSE;
  WAPrintObj^.InitHeadings;
  WAPrintObj^.prnInfo^.header^.AddRow(r_Normal or r_CellWrap or r_BorderBottom, 0);
  SR(IDS_TOWER, temp, 64); { Tower }
  WAPrintObj^.prnInfo^.header^.AddCell(temp, LT50, c_Normal, 0);
  SR(IDS_SLOT, temp, 64); { Slot }
  WAPrintObj^.prnInfo^.header^.AddCell(temp, LT50, c_Normal, 0);
  SR(IDS_PANELID, temp, 64); { Panel ID }
  WAPrintObj^.prnInfo^.header^.AddCell(temp, LT75, c_Normal, 0);
  WAPrintObj^.prnInfo^.header^.AddCell('', LT08, c_Normal, 0);
  SR(IDS_SPECID, temp, 64); { Specimen ID }
  WAPrintObj^.prnInfo^.header^.AddCell(temp, LT75, c_Normal, 0);
  WAPrintObj^.prnInfo^.header^.AddCell('', LT08, c_Normal, 0);
  SR(IDS_COLLDT, temp, 64); { Collect Date }
  WAPrintObj^.prnInfo^.header^.AddCell(temp, LT100, c_Normal, 0);
  WAPrintObj^.prnInfo^.header^.AddCell('', LT08, c_Normal, 0);
  SR(IDS_ISOID, temp, 64); { Isolate ID }
  WAPrintObj^.prnInfo^.header^.AddCell(temp, LT50, c_Normal, 0);
  WAPrintObj^.prnInfo^.header^.AddCell('', LT08, c_Normal, 0);
  SR(IDS_PANELTYPE, temp, 64); { Panel Type }
  WAPrintObj^.prnInfo^.header^.AddCell(temp, LT100, c_Normal, 0);
  WAPrintObj^.prnInfo^.header^.AddCell('', LT08, c_Normal, 0);
  SR(IDS_STATUS, temp, 64); { Status }
  WAPrintObj^.prnInfo^.header^.AddCell(temp, LT300, c_WordWrap or c_Stretch, 0);
  WAPrintObj^.prnInfo^.header^.AddRow(r_CellWrap or r_Normal, 0);

  waexlistDB^.dbc^.SetCurKeyNum(DBWAEX_Slot_KEY);
  if not waexlistDB^.dbc^.GetFirst(waexlistDB^.dbr) then
  begin
    WAPrintObj^.prnInfo^.body^.AddRow(r_Normal, 0);
    WAPrintObj^.prnInfo^.body^.AddRow(r_Normal, 0);
    SR(IDS_WAEXCEPT_NODATA, temp, 64);
    WAPrintObj^.prnInfo^.body^.AddCell(temp, LT300, c_Bold, 0);
  end
  else
    repeat
      if not waexlistDB^.dbr^.GetField(DBWAEXIsoOrdRef, @seq, SizeOf(TSeqNum)) or
         not isoordDB^.dbc^.GetSeq(isoordDB^.dbr, seq) or
         not isoordDB^.dbr^.GetField(DBISOORDIsoRef, @seq, SizeOf(TSeqNum)) or
         not isoDB^.dbc^.GetSeq(isoDB^.dbr, seq) then
         Continue;

      WAPrintObj^.prnInfo^.body^.AddRow(r_CellWrap, 0);
      waexlistDB^.dbr^.GetFieldAsStr(DBWAEXTower, temp, 64);        { Tower }
      WAPrintObj^.prnInfo^.body^.AddCell(temp, LT50, c_Normal, 0);
      waexlistDB^.dbr^.GetFieldAsStr(DBWAEXSlot, temp, 64);         { Slot }
      WAPrintObj^.prnInfo^.body^.AddCell(temp, LT50, c_Normal, 0);
      waexlistDB^.dbr^.GetFieldAsStr(DBWAEXPanelID, temp, 64);      { Panel ID }
      WAPrintObj^.prnInfo^.body^.AddCell(temp, LT75, c_Normal, 0);
      WAPrintObj^.prnInfo^.body^.AddCell('', LT08, c_Normal, 0);
      isoDB^.dbr^.GetFieldAsStr(DBISOSpecID, temp, 64);             { Specimen ID }
      WAPrintObj^.prnInfo^.body^.AddCell(temp, LT75, c_Normal, 0);
      WAPrintObj^.prnInfo^.body^.AddCell('', LT08, c_Normal, 0);
      isoDB^.dbr^.GetFieldAsStr(DBISOCollectDate, temp, 64);        { Collect Date }
      WAPrintObj^.prnInfo^.body^.AddCell(temp, LT100, c_Normal, 0);
      WAPrintObj^.prnInfo^.body^.AddCell('', LT08, c_Normal, 0);
      isoDB^.dbr^.GetFieldAsStr(DBISOIso, temp, 64);                { Isolate ID }
      WAPrintObj^.prnInfo^.body^.AddCell(temp, LT50, c_Normal, 0);
      WAPrintObj^.prnInfo^.body^.AddCell('', LT08, c_Normal, 0);
      waexlistDB^.dbr^.GetFieldAsStr(DBWAEXPanelName, temp, 64);    { Panel Type }
      WAPrintObj^.prnInfo^.body^.AddCell(temp, LT100, c_Normal, 0);
      WAPrintObj^.prnInfo^.body^.AddCell('', LT08, c_Normal, 0);
      waexlistDB^.dbr^.GetField(DBWAEXStrNum, @exNum, SizeOf(exNum));
      SR(exNum, temp, 64);                                          { Status }
      WAPrintObj^.prnInfo^.body^.AddCell(temp, LT300, c_WordWrap or c_Stretch, 0);
    until not waexlistDB^.dbc^.GetNext(waexlistDB^.dbr);
  MSDisposeObj(wc);
  MSDisposeObj(WAPrintObj);
  waexlistDB^.dbc^.SetCurKeyNum(DBWAEX_ID_KEY);
end;

procedure TWAMainDlg.HandleGrid(var msg: TMessage);
begin
  if msg.lParamHi = LBN_SELCHANGE then
    EnableButtons
  else if msg.lParamHI = LBN_DBLCLK then
  begin
    (* available action here *);
  end
  else
    DefWndProc(msg);
end;

procedure TWAMainDlg.Cancel(var msg: TMessage);
begin
  userCantWait := TRUE;
  if not checkingBCS then
  begin
    if inAutoUpload then
      KillTimer(HWindow, IDT_AUTO_UPLOAD);

    inherited Cancel(msg);
  end;
end;

procedure TWAMainDlg.FillGrid;
{- fill list box with tower info }
var
  wc       : PWaitCursor;
  start    : integer;
  stop     : integer;
  slotCnt  : integer;
  k        : integer;
  info     : TWAGridInfo;
  okFine   : boolean;
  printStr : array[0..120] of char;
  WABSICmd : PWABSICmd;
  WASCICmd : PWASCICmd;
  WAPSICmd : PWAPSICmd;
  WAExDB   : PDBFile;

  function GetSlotInfo: boolean;
  { return the tower/slot based on slotCnt.  Returns TRUE if the slot is     }
  { used by the WA and FALSE if NOT.  (ie. tower 1 slot 6 on the WA 40.      }
  begin
    GetSlotInfo := FALSE;
    GetTowerSlot(slotCnt, info.slot, info.tower);
    if (WA[WAID].waType = WA40) and (info.slot > 5) then  {WA 40, ignore slots 6-12}
      Exit;
    GetSlotInfo := TRUE;
  end;

  Function IsOnWAExDB(PId, Tower, Slot: Integer): boolean;
  var  ExTower,
       ExSlot,
       ExPId    : Integer;
       IsoOrdSeq: TSeqNum;
       ExOrdSeq : TSeqNum;
  begin
    { Populate Key fields to search on }
    ExPId    :=PId;
    ExTower  :=Tower;
    ExSlot   :=Slot;
    IsoOrdSeq:=0;

    { Get the ISOOrdSeq, to avoid chances of collision }
    wapDB^.dbr^.ClearRecord;
    wapDB^.dbr^.PutField(DBWAPNLID, @PId);
    if wapDB^.dbc^.GetEQ(wapDB^.dbr) then
       wapDB^.dbr^.GetField(DBWAPNLIsoOrdRef, @IsoOrdSeq, SizeOf(TSeqNum));

    WAExDB^.dbr^.ClearRecord;
    WAExDB^.dbr^.PutField(dbWAEXPanelID,    @ExPId);
    WAExDB^.dbr^.PutField(dbWAExTower     , @ExTower);
    WAExDB^.dbr^.PutField(dbWAExSlot      , @ExSlot);
    WAExDB^.dbr^.PutField(dbWAExIsoOrdRef , @IsoOrdSeq);

    { Is this panel on the WA Exception List? }
    if WAExDB^.dbc^.GetGE(WAExDB^.dbr) then
    begin
      WAExDB^.dbr^.GetField(dbWAEXPanelID  , @ExPId  , SizeOf(ExPId  ));
      WAExDB^.dbr^.GetField(dbWAExTower    , @ExTower, SizeOf(ExTower));
      WAExDB^.dbr^.GetField(dbWAExSlot     , @ExSlot , SizeOf(ExSlot ));
      WAExDB^.dbr^.GetField(dbWAExIsoOrdRef, @ExOrdSeq, SizeOf(ExOrdSeq));

      IsOnWAExDB:=(ExPId=PId) and (ExTower=Tower) and
                  (ExSlot=Slot) and (ExOrdSeq=IsoOrdSeq);
    end
    else IsOnWAExDB:=False;
  end;

  procedure GetPanelID;
  var
    tempStr : array [0..9] of char;
  begin
    Str(WASCICmd^.recvMsg^.psiPid, tempStr);
    LeftPad(printStr, tempStr, '0', 4);

    { mark it with a *, ala ccidos wasup }
    if IsOnWAExDB(WASCICmd^.recvMsg^.psiPid,info.tower,info.slot)
       then printStr[0]:='*'
       else printStr[0]:=' ';

    lsmGrid^.SetData(LSM_PANELID, k, printStr);
  end;

  procedure GetPanelName;
  begin
    lsmGrid^.SetData(LSM_PANELTYPE, k,
                     trayObj^.mapBat^.batIDList[WASCICmd^.recvMsg^.psiPtyp+1]);
  end;

  procedure GetSpecimenStuff;
  var
    seq  : TSeqNum;
  begin
    wapDB^.dbr^.PutField(DBWAPNLID, @WASCICmd^.recvMsg^.psiPid);
    if not wapDB^.dbc^.GetEQ(wapDB^.dbr) then
      Exit;
    wapDB^.dbr^.GetField(DBWAPNLIsoOrdRef, @seq, SizeOf(TSeqNum));
    if not isoordDB^.dbc^.GetSeq(isoordDB^.dbr, seq) then
      Exit;
    isoordDB^.dbr^.GetField(DBISOORDIsoRef, @seq, SizeOf(TSeqNum));
    if not isoDB^.dbc^.GetSeq(isoDB^.dbr, seq) then
      Exit;

    isoDB^.dbr^.GetFieldAsStr(DBISOIso, printStr, 120);
    lsmGrid^.SetData(LSM_ISOID, k, printStr);
    isoDB^.dbr^.GetFieldAsStr(DBISOSpecID, printStr, 120);
    lsmGrid^.SetData(LSM_SPECID, k, printStr);
    isoDB^.dbr^.GetFieldAsStr(DBISOCollectDate, printStr, 120);
    lsmGrid^.SetData(LSM_COLLDT, k, printStr);

    isoDB^.dbr^.GetField(DBISOSpecRef, @seq, SizeOf(TSeqNum));
    if not spcDB^.dbc^.GetSeq(spcDB^.dbr, seq) then
      Exit;
    spcDB^.dbr^.GetField(DBSPECPatRef, @seq, SizeOf(TSeqNum));
    if not patDB^.dbc^.GetSeq(patDB^.dbr, seq) then
      Exit;

    patDB^.dbr^.GetFieldAsStr(DBPATPatID, printStr, 120);
    lsmGrid^.SetData(LSM_PATID, k, printStr);
  end;

  procedure GetNextRead;
  begin
    printStr[0] := #0;
    FormatReadTime(WASCICmd^.recvMsg^.psiLastRd, printStr, FALSE);
    lsmGrid^.SetData(LSM_NEXTREAD, k, printStr);
  end;

  procedure GetLastRead;
  begin
    printStr[0] := #0;
    FormatReadTime(WAPSICmd^.recvMsg^.psiLastRd, printStr, FALSE);
    lsmGrid^.SetData(LSM_LASTREAD, k, printStr);
  end;

  procedure SetWAStatus(statNum: integer);
  var
    statStr : array [0..120] of char;
  begin
    if statNum <> 0 then
    begin
      if printStr[0] <> #0 then
        StrLCat(printStr, ', ', 120);
      SR(statNum, statStr, 120);
      StrLCat(printStr, statStr, 120);
    end;
    lsmGrid^.SetData(LSM_STATUS, k, printStr);
  end;

  procedure SetWAFlag(flgNum: integer);
  var
    flg : array [0..120] of char;
  begin
    if printStr[0] <> #0 then
      StrLCat(printStr, ', ', 120);
    SR(flgNum, flg, 120);
    StrLCat(printStr, flg, 120);
  end;

  procedure EvaluateFlags(flagsObj: PPanelFlags);
  Const
    StaphFamily = 2;
    RapNegID3SetClass = 221;
  Var
    SetFamily : Word;
    AFam      : Integer;
  begin
    if flagsObj^.flagVal <> 0 then
    begin
      if WASCICmd^.recvMsg^.psiStat <> processComplete then
      begin
        if flagsObj^.PossibleMRSASE then
        Begin
          isoDB^.dbr^.GetField(DBISOSetFamily, @setFamily, sizeof(setFamily));
          aFam := ExtractFamily(setFamily);
          If AFam = StaphFamily then
            SetWaFlag(IDS_FLG_MRSA_SE)
          Else
            SetWaFlag(IDS_FLG_VRE_Hold);
        End
        else if flagsObj^.SlowGrower then
        begin
          if WASCICmd^.recvMsg^.psiStat <> MICsAvailable then
            SetWaFlag(IDS_FLG_SLOW_GROWER);
        end;
      end;
      if flagsObj^.IDAvailOnRapid then
        SetWaFlag(IDS_FLG_IDS_AVAIL_RAPID);
      if flagsObj^.NoDispense then
      Begin
        SetWaFlag(IDS_FLG_NO_DISPENSE);
        isoDB^.dbr^.GetField(DBISOSetFamily, @setFamily, sizeof(setFamily));
        If ((ExtractSet(SetFamily)*10) + ExtractFamily(SetFamily)) = RapNegID3SetClass then
          {Exceptions^.NeedIndole := TRUE};
      End;
    end;
  end;

  procedure GetStatus;
  var
    statNum  : integer;
    flagsObj : PPanelFlags;
  begin
    flagsObj := New(PPanelFlags, Init(WASCICmd^.recvMsg^.psiFlags));
    if WASCICmd^.recvMsg^.psiStat <> 0 then
      GetStatNum(WASCICmd^.recvMsg^.psiStat, statNum, flagsObj)
    else   { no status }
      statNum := IDS_STS_NONE;
    SR(statNum, printStr, 63);
    EvaluateFlags(flagsObj);
    SetWAStatus(0);
    MSDisposeObj(flagsObj);
  end;

begin
  if (WAID = -1) or
     (WA[WAID].slotsPerTower = 0) or (WA[WAID].slotTotal = 0) then
    Exit;

  wc := New(PWaitCursor, Init);
  WABSICmd := New(PWABSICmd, Init(WAID));
  WASCICmd := New(PWASCICmd, Init(WAID));
  WAPSICmd := New(PWAPSICmd, Init(WAID));

  if lsmTower = 0 then
  begin
    start := 0;
    stop := WA[WAID].slotTotal - 1;
  end
  else
  begin
    start := (lsmTower * WA[WAID].slotsPerTower) - WA[WAID].slotsPerTower;
    stop  := (start + (WA[WAID].slotsPerTower - 1));
  end;

  if WABSICmd^.SendWAMessage and
     (WABSICmd^.WAErrNum = 0) then
  begin
    slotCnt := start;
    okFine := TRUE;

    { save current WAExList's key, and index by ID }
    WaExDB:= New(PDBFile, Init(DBWAEXFile, '', dbOpenReadOnly));

    while okFine and (slotCnt <= stop) do
    begin
      if GetSlotInfo then
      begin
        k := lsmGrid^.AddString('');
        Str(info.tower, printStr);
        lsmGrid^.SetData(LSM_TOWER, k, printStr);
        Str(info.slot, printStr);
        lsmGrid^.SetData(LSM_SLOT, k, printStr);
        case WABSICmd^.recvMsg^.bsiInfo[slotCnt + 1] of
          0 : { slot is empty }
            begin
              printStr[0] := #0;
              SetWAStatus(IDS_STS_EMPTY);
            end;
          1,3,4 : { slot has a panel in it - good barcode }
            begin
              WASCICmd^.SetParams(slotCnt);
              if WASCICmd^.SendWAMessage and
                 (WASCICmd^.WAStatNum = 0) then
              begin
                WAPSICmd^.SetParams(WASCICmd^.recvMsg^.psiPid);
                WAPSICmd^.SendWAMessage;
                { send formatted information to the listbox }
                GetPanelID;
                GetPanelName;
                GetSpecimenStuff;
                GetNextRead;
                GetLastRead;
                GetStatus;
              end
              else
                okFine := FALSE;
            end;
          2 : { bad barcode scan }
            begin
              printStr[0] := #0;
              SetWAStatus(IDS_STS_BC_ERR);  { Barcode Read Error }
            end;
        end; { case }
        info.bsiStat := WABSICmd^.recvMsg^.bsiInfo[slotCnt + 1];
        lsmGrid^.SetItemData(k, longint(info));
      end;
      Inc(slotCnt);
    end; { while }

    {Dispose the WAExDB pointer}
    MSDisposeObj(WAExDB);
  end;{ if }
  MSDisposeObj(WAPSICmd);
  MSDisposeObj(WASCICmd);
  MSDisposeObj(WABSICmd);
  MSDisposeObj(wc);
end;

procedure TWAMainDlg.EnableButtons;
var
  sel       : integer;
  info      : TWAGridInfo;
  bEnable   : boolean;
begin
  bEnable := (not checkingBCS) and (WAID <> -1);

  EnableWindow(GetItemHandle(IDC_SHOW_MAP), bEnable);
  EnableWindow(GetItemHandle(IDC_PRINT_MAP), TRUE);

  sel := lsmGrid^.GetSelIndex;
  if sel = lb_Err then
  begin
    EnableWindow(GetItemHandle(IDC_IDPANEL), FALSE);
    EnableWindow(GetItemHandle(IDC_PANEL_INFO), FALSE);
  end
  else
  begin
    longint(info) := lsmGrid^.GetItemData(sel);
    if info.bsiStat = 2 then
    begin
      EnableWindow(GetItemHandle(IDC_IDPANEL), bEnable);
      EnableWindow(GetItemHandle(IDC_PANEL_INFO), FALSE);
    end
    else if info.bsiStat <> 0 then
    begin
      EnableWindow(GetItemHandle(IDC_IDPANEL), FALSE);
      EnableWindow(GetItemHandle(IDC_PANEL_INFO), TRUE);
    end
    else
    begin
      EnableWindow(GetItemHandle(IDC_IDPANEL), FALSE);
      EnableWindow(GetItemHandle(IDC_PANEL_INFO), FALSE);
    end;
  end;

  EnableWindow(GetItemHandle(IDC_ACCESS_WA), bEnable);
  EnableWindow(GetItemHandle(IDC_PRINT_QC), bEnable);
  EnableWindow(GetItemHandle(IDC_RESULTS_INQ), bEnable);
(*EnableWindow(GetItemHandle(IDC_BARCODES), bEnable); *)
  EnableWindow(GetItemHandle(IDC_MAINTENANCE), bEnable);
  EnableWindow(GetItemHandle(IDC_PRINT_EXCEPTIONS), bEnable and not inAutoUpload);
  EnableWindow(GetItemHandle(IDC_PRINT_CPSR), bEnable);
end;

procedure TWAMainDlg.WaitForBCSResponse;
var
  waCmd   : PWASTICmd;
  okFine  : boolean;
  wc      : PWaitCursor;
begin
  waCmd := New(PWASTICmd, Init(WAID));
  okFine := TRUE;
  userCantWait := FALSE;
  repeat
    wc := New(PWaitCursor, Init);
    MSWait(5000);
    MSDisposeObj(wc);
    okFine := waCmd^.SendWAMessage;
    HandleEvents(hWindow);
  until not okFine or TestBit(waCmd^.recvMsg^.stiBits, BarcodeScanDone)
        or userCantWait or not TestBit(waCmd^.recvMsg^.stiBits, AccessNixed);

  MSDisposeObj(WACmd);
  if userCantWait then
    PostMessage(hWindow, WM_CLOSE, 0, 0)
  else
  begin
    wc := New(PWaitCursor, Init);
    CheckPanels;
    MSDisposeObj(wc);
  end;
end;

procedure TWAMainDlg.CheckPanels;
var
  slotCnt  : integer;
  stop     : integer;
  okFine   : boolean;
  flag     : boolean;
  WABSICmd : PWABSICmd;
  WASCICmd : PWASCICmd;
begin
  WABSICmd := New(PWABSICmd, Init(WAID));
  WASCICmd := New(PWASCICmd, Init(WAID));
  if WABSICmd^.SendWAMessage and
     (WABSICmd^.WAErrNum = 0) then
  begin
    slotCnt := 0;
    stop := WA[WAID].slotTotal - 1;
    okFine := TRUE;
    while okFine and (slotCnt <= stop) do
    begin
      case WABSICmd^.recvMsg^.bsiInfo[slotCnt + 1] of
        1,3,4 : { slot has a panel in it - good barcode }
          begin
            WASCICmd^.SetParams(slotCnt);
            if WASCICmd^.SendWAMessage and
               (WASCICmd^.WAStatNum = 0) then
            begin
              wapDB^.dbr^.ClearRecord;
              wapDB^.dbr^.PutField(DBWAPNLID, @WASCICmd^.recvMsg^.psiPid);
              if wapDB^.dbc^.GetEQ(wapDB^.dbr) then
              begin
                wapDB^.dbr^.GetField(DBWAPNLKnownToWA, @flag, SizeOf(flag));
                if not flag then
                begin
                  flag := TRUE;
                  wapDB^.dbr^.PutField(DBWAPNLKnownToWA, @flag);
                  wapDB^.dbc^.UpdateRec(wapDB^.dbr);
                end;
              end;
            end
            else
              okFine := FALSE;
          end;
      end; { case }
      Inc(slotCnt);
    end; { while }
  end;
  MSDisposeObj(WASCICmd);
  MSDisposeObj(WABSICmd);
end;

procedure TWAMainDlg.SetSession;
begin
  if INISpecimenListSort = 2 then
    if curSessRef = 0 then
      isoDB^.dbc^.SetCurKeyNum(DBISO_CDateSpecID_KEY)
    else
      isoDB^.dbc^.SetCurKeyNum(DBISO_SessionCDateSpecID_KEY)
  else
    if curSessRef = 0 then
      isoDB^.dbc^.SetCurKeyNum(DBISO_SpecIDCDate_KEY)
    else
      isoDB^.dbc^.SetCurKeyNum(DBISO_SessionSpecIDCDate_KEY);
end;

procedure TWAMainDlg.TimerTick(var msg: TMessage);
var
  wc  : PWaitCursor;
begin
  DefWndProc(msg);
  KillTimer(HWindow, IDT_AUTO_UPLOAD);
  Inc(timerCntr);
  if (timerCntr mod 12) = 0 then
  begin
    timerCntr := 0;
    wc := New(PWaitCursor, Init);
    UploadResults;
    MSDisposeObj(wc);
  end;
  if inAutoUpload then
    SetTimer(HWindow, IDT_AUTO_UPLOAD, AUTO_TIMEOUT, nil);
end;

procedure TWAMainDlg.ShowWAStatus(var msg: TMessage);
var
  ret : longint;
begin
  ret := SendDlgItemMsg(IDC_WASTAT_LIST, lb_InsertString, 0, msg.lParam);
  if ret = LB_ERRSPACE then
  begin
    ret := SendDlgItemMsg(IDC_WASTAT_LIST, lb_GetCount, 0, 0);
    ret := SendDlgItemMsg(IDC_WASTAT_LIST, lb_DeleteString, ret-1, 0);
    ret := SendDlgItemMsg(IDC_WASTAT_LIST, lb_DeleteString, ret-2, 0);
    ret := SendDlgItemMsg(IDC_WASTAT_LIST, lb_InsertString, 0, msg.lParam);
  end;

  { show it and handle queue  IDC_WASTAT_LIST delete last if size > 100 ?  50 ? }
end;

procedure TWAMainDlg.DrawItem(var msg: TMessage);
begin
  lsmGrid^.DrawItem(msg);
end;

procedure TWAMainDlg.MeasureItem(var msg: TMessage);
begin
  lsmGrid^.MeasureItem(msg);
end;

procedure TWAMainDlg.CharToItem(var msg: TMessage);
begin
  lsmGrid^.CharToItem(msg);
end;


{-------------------------------------------------------[ TWAMainApp ]--}
procedure TWAMainApp.InitMainWindow;
begin
  if INIUse3D then
    Register3dApp(TRUE, TRUE, TRUE);
  mainWindow := New(PWAMainDlg, Init);
end;

procedure Start;
begin
  if OKToRun(MOD_WA, false) then
  begin
    RegisterErrorTitle(SR(IDS_ERR_TITLE, errTitle, 40));
    waMainApp.Init(GetModuleName(MOD_WA));
    waMainApp.Run;
    waMainApp.Done;
  end;
end;

END.
