{----------------------------------------------------------------------------}
{  Module Name  : Sort_Dlg.PAS                                               }
{  Programmer   : Steven Mercier                                             }
{  Date Created : 06/22/98                                                   }
{                                                                            }
{  Purpose - To provide a dialog box to view and edit sort parameters.       }
{                                                                            }
{  Assumptions                                                               }
{                                                                            }
{  Initialization - none.                                                    }
{                                                                            }
{  Revision History - This project is under version control, use it to view  }
{    revision history.                                                       }
{                                                                            }
{----------------------------------------------------------------------------}
unit Sort_Dlg;

INTERFACE

uses
  Sort_Par,
  OWindows;

const
  SDLG_MAXLEVELS = 3;  { Number of levels supported by the dialog box }
  id_Back = 201;       { application defined return value meaning the Back
                         button was pressed.}
  id_Next = 202;       { application defined return value meaning the Next
                         button was pressed.}

type
  {--------------------------------------------------------------------------}
  { This object provids the dialog box with key information. An object
  { must be derived from this object that defines the number of keys and
  { their names. See member descriptions in the implementation section for
  { more information.
  {--------------------------------------------------------------------------}
  PKeyInformation = ^TKeyInformation;
  TKeyInformation = object
    constructor Init;
    function GetKeyName( KeyNumber: integer; KeyName: PChar; Size: integer ):
      PChar; virtual;
    function NumberOfKeys: integer; virtual;
    destructor Done;
  end;


{ This procedure permits the user to view and/or edit the parameters
  via a dialog box. }
function SortParametersDialog( pParent: PWindowsObject; Title: PChar;
  pKeyInformation: PKeyInformation; EnableBackButton: boolean;
  var SortParameters: SortEngineParameters ): integer;



IMPLEMENTATION

uses
  APITools,
  DlgLib,
  MScan,
  Objects,
  ODialogs,
  Strings,
  Win31,
  WinProcs,
  WinTypes;

{$R Sort.RES}
{$I Sort.INC}



                        {----------------------}
                        {   Local Functions    }
                        {----------------------}



{----------------------------------------------------------------------------}
{ This procedure handles assertions for this unit. If an assertion is
{ false, it halts the program. Otherwise, it does nothing.
{----------------------------------------------------------------------------}

procedure Assert( flag: boolean );
begin
  if not flag then
  begin
    MessageBox( 0, 'SORT_DLG.PAS', 'Error', MB_OK );
    Halt;
  end;
end;



                        {----------------------}
                        {    Key Information   }
                        {----------------------}



{----------------------------------------------------------------------------}
{ Constructor
{----------------------------------------------------------------------------}

constructor TKeyInformation.Init;
begin
end;



{----------------------------------------------------------------------------}
{ Destructor
{----------------------------------------------------------------------------}

destructor TKeyInformation.Done;
begin
end;



{----------------------------------------------------------------------------}
{ This function gets the name of a key given its number. This function must
{ be overriden to supply names for each key.
{
{ Inputs:
{   KeyNumber - the number of the key, 0 <= KeyNumber < NumberOfKeys
{   KeyName - pointer to a character string buffer to receive the name
{   Size - the buffer size in bytes, 0 <= size
{
{ Note: if using string resources, size need not exceed 256 bytes.
{----------------------------------------------------------------------------}

function TKeyInformation.GetKeyName( KeyNumber: integer; KeyName: PChar;
  Size: integer ): PChar;
begin
  Abstract;
end;



{----------------------------------------------------------------------------}
{ This function reports the number of keys this object contains information
{ on. This function must be overriden to indicate the number of keys.
{----------------------------------------------------------------------------}

function TKeyInformation.NumberOfKeys: integer;
begin
  Abstract;
end;



                        {----------------------}
                        {      Dialog Box      }
                        {----------------------}



type
  {--------------------------------------------------------------------------}
  { This is the sort dialog box object. It is used to view/edit the sort
  { parameters. It's layout employs Back, Next, and Cancel buttons so that
  { it may be used in a series of dialog boxes.
  {--------------------------------------------------------------------------}
  PSortDialog = ^TSortDialog;
  TSortDialog = object( TCenterDlg )
    pLists: array [1..SDLG_MAXLEVELS] of PComboBox;
    pReverseBoxes: array [1..SDLG_MAXLEVELS] of PCheckBox;
    pSortParameters: PSortEngineParameters;
    pKeyInformation: PKeyInformation;
    pBackButton: PButton;
    EnableBackButton: boolean;
    Title: PChar;
    constructor Init( pParent: PWindowsObject;
      input_pKeyInformation: PKeyInformation;
      input_pSortParameters: PSortEngineParameters;
      input_EnableBackButton: boolean;
      input_Title: PChar );
    destructor Done; virtual;
    procedure SetupWindow; virtual;
    procedure UpdateEnables;
    function CanClose: boolean; virtual;
    procedure ReadControls( pParameters: PSortEngineParameters );

    { message handlers }
    procedure OnL1List( var Msg: TMessage ); virtual id_First +
      IDC_L1_LIST;
    procedure OnL2List( var Msg: TMessage ); virtual id_First +
      IDC_L2_LIST;
    procedure OnL3List( var Msg: TMessage ); virtual id_First +
      IDC_L3_LIST;
    procedure OnList( var Msg: TMessage; level: integer );
    procedure OnBack( var Msg: TMessage ); virtual id_First +
      IDC_BACK;
    procedure OnNext( var Msg: TMessage ); virtual id_First +
      IDC_NEXT;
  end;



{----------------------------------------------------------------------------}
{ This destructor cleans up the object.
{----------------------------------------------------------------------------}

destructor TSortDialog.Done;
begin
  MSStrDispose( Title );
  inherited Done;
end;



{----------------------------------------------------------------------------}
{ This constructor provides additional initializations needed for to
{ implement the transfer buffer, the enable/disable feature of some
{ controls, and data validation.
{
{ Inputs:
{   pParent
{   input_pKeyInformation - pointer to user defined key information
{   input_pSortParameters - pointer to the parameters to view/edit
{   input_EnableBackButton - set true to enable the Back button
{   input_Title - points to a user defined dialog box title.
{
{----------------------------------------------------------------------------}

constructor TSortDialog.Init( pParent: PWindowsObject;
  input_pKeyInformation: PKeyInformation;
  input_pSortParameters: PSortEngineParameters;
  input_EnableBackButton: boolean;
  input_Title: PChar );
type
  TID = array[1..SDLG_MAXLEVELS] of integer;
const
  ListID: TID = ( IDC_L1_LIST, IDC_L2_LIST, IDC_L3_LIST );
  BoxID: TID = ( IDC_L1_REVERSE, IDC_L2_REVERSE, IDC_L3_REVERSE );
var
  i: integer;
begin
  if not inherited Init( pParent, MakeIntResource( DLG_SORT ) ) then
    Fail;

  { Set attributes }
  Assert( input_pKeyInformation <> nil );
  Assert( input_pSortParameters <> nil );
  pKeyInformation := input_pKeyInformation;
  pSortParameters := input_pSortParameters;
  Title := StrNew( input_Title );

  { Create objects for controls that require special processing. }
  for i:=1 to SDLG_MAXLEVELS do
  begin
    pLists[ i ] := New( PComboBox, InitResource( @Self, ListID[ i ], 30 ) );
    pReverseBoxes[ i ] := New( PCheckBox, InitResource( @Self, BoxID[ i ] ) );
  end;
  pBackButton := New( PButton, InitResource( @Self, IDC_BACK ) );
  EnableBackButton := input_EnableBackButton;

end;



{----------------------------------------------------------------------------}
{ This override provides additional window set up. Now that the window has
{ been created, the control enables can be initialized.
{----------------------------------------------------------------------------}

procedure TSortDialog.SetupWindow;

  {--------------------------------------------------------------------------}
  { This function converts a boolean value to a checked/unchecked value.
  {--------------------------------------------------------------------------}
  function BoolToCheck( flag: boolean ): word;
  begin
    if flag then
      BoolToCheck := bf_Checked
    else
      BoolToCheck := bf_Unchecked;
  end;

const
  BUFFER_SIZE = 64;
var
  key,
  index,
  level: integer;
  Buffer: array [0..BUFFER_SIZE-1] of char;

begin { SetupWindow }

  inherited SetupWindow;

  { Override default title }
  SetWindowText( HWindow, title );

  {Add the "none" item}
  for level := 1 to SDLG_MAXLEVELS do
  begin
    index := pLists[ level ]^.AddString( SR( IDS_NONE, Buffer,
      SizeOf( Buffer ) ) );
    Assert( index = 0 );
  end;

  {Add Keys - insure index = key + 1 so the reverse operation will work }
  for key := 0 to pKeyInformation^.NumberOfKeys - 1 do
    for level := 1 to SDLG_MAXLEVELS do
    begin
      index := pLists[ level ]^.AddString( pKeyInformation^.GetKeyName(
        key, Buffer, SizeOf( Buffer ) ) );
      Assert( index = key + 1 );
    end;

  {Transfer Data to Dialog Box}
  level := 1;
  Assert( pSortParameters^.NumberOfLevels <= SDLG_MAXLEVELS );
  while level <= pSortParameters^.NumberOfLevels do
  begin

    { List Box }
    key := pSortParameters^.Levels[ level ].KeyNumber;
    pKeyInformation^.GetKeyName( key, Buffer, SizeOf( Buffer ) );
    index := pLists[ level ]^.SetSelString( Buffer, -1 );
    Assert( index >= 0 );

    { Check Box }
    pReverseBoxes[ level ]^.SetCheck( BoolToCheck(
      pSortParameters^.Levels[ level ].Reverse ) );

    Inc( level );
  end;

  {Initial update of control enables }
  UpdateEnables;

end;



{----------------------------------------------------------------------------}
{ This procedure tramsfers data from the dialog box controls to a parameter
{ object.
{----------------------------------------------------------------------------}

procedure TSortDialog.ReadControls( pParameters: PSortEngineParameters );

  {--------------------------------------------------------------------------}
  { This function converts a check flag to a boolean value.
  {--------------------------------------------------------------------------}
  function CheckToBool( CheckFlag: Word ): boolean;
  begin
    CheckToBool := CheckFlag = bf_Checked;
  end;

var
  index,
  level: integer;

begin { ReadControls }

  {Transfer Data from Dialog Box}
  pParameters^.NumberOfLevels := 0;
  level := 1;
  Assert( SE_MAXLEVELS <= SDLG_MAXLEVELS );
  index := pLists[ level ]^.GetSelIndex;
  while index > 0 do
  begin
    { Count }
    Inc( pParameters^.NumberOfLevels );

    { List Box - reverse the add key operation }
    pParameters^.Levels[ level ].KeyNumber := index - 1;

    { Check Box }
    pParameters^.Levels[ level ].Reverse := CheckToBool(
      pReverseBoxes[ level ]^.GetCheck );

    { Set up next iteration }
    Inc( level );
    if level <= SDLG_MAXLEVELS then
      index := pLists[ level ]^.GetSelIndex
    else
      index := 0;
  end;
end;



{----------------------------------------------------------------------------}
{ This function returns true if it is Ok to close the dialog box, i.e, if it
{ contains valid data. The test involves control-level validation and
{ window-level validation, i.e., each control contains a valid value and all
{ the values are valid together.
{
{ Note: CheckValid could replace most of this code but another parameter
{ buffer would need to be created to hold the copy to be validated so that
{ if the user cancels, the original parameters are not modified.
{----------------------------------------------------------------------------}

function TSortDialog.CanClose: boolean;

  {--------------------------------------------------------------------------}
  { This procedure diaplays a message alerting the user that a list contains
  { the same selection as one above it and that he should make another
  { selection.
  {--------------------------------------------------------------------------}
  procedure WarnAndFocus( Level: integer );
  const
    BUFFER_SIZE = 256;
  var
    Buffer: array [0..BUFFER_SIZE-1] of char;
  begin
    pLists[ level ]^.Focus;
    MessageBox( 0, SR( IDS_SAME_AS_ABOVE, Buffer, SizeOf( Buffer ) ),
      'Validator', mb_IconExclamation or mb_Ok );
  end;

var
  InvalidLevel: integer;
  Valid: boolean;
  CandidateParameters: SortEngineParameters;

begin { CanClose }

  {---- Control-level validation. This should pass. }
  Valid := inherited CanClose;

  {---- Window-level validation. Read the controls and see if the resulting
        parameter object is valid. If not, the only posibility is that it
        contains duplicate keys, thus a warning is displayed. }
  if Valid then
  begin
    ReadControls( @CandidateParameters );
    Valid := CandidateParameters.CheckValid( InvalidLevel );
    if not Valid then
    begin
      Assert( InvalidLevel > 0 );
      WarnAndFocus( InvalidLevel );
    end;
  end;


  {---- Return overall result }
  CanClose := Valid;

end;



{----------------------------------------------------------------------------}
{ This procedure enables and disables controls based on the current
{ selections. This prevents the user from entering data at the wrong time.
{----------------------------------------------------------------------------}

procedure TSortDialog.UpdateEnables;
var
  level: integer;
begin

  {---- Disable Back button if requested }
  if not EnableBackButton then
    pBackButton^.Disable;

  {---- Set list and check enables at each level }
  level := 1;
  while ( level <= SDLG_MAXLEVELS ) and
    ( pLists[ level ]^.GetSelIndex > 0 ) do
  begin
    { Enable/Enable - levels having an item other than "none" }
    pReverseBoxes[ level ]^.Enable;
    pLists[ level ]^.Enable;
    Inc( level );
  end;
  if level <= SDLG_MAXLEVELS then
  begin
    { Disable/Enable - level just below levels above }
    pReverseBoxes[ level ]^.Disable;
    pLists[ level ]^.Enable;
    Inc( level );
    while level <= SDLG_MAXLEVELS do
    begin
      { Disable/Disable - levels below above levels }
      pReverseBoxes[ level ]^.Disable;
      pLists[ level ]^.Disable;
      Inc( level );
    end;
  end;

end;



{----------------------------------------------------------------------------}
{ These message handlers provides additional message processing for the list
{ boxes. A handler is called whenever the user acts upon the list. The
{ purpose of these handlers is to clear the select box when the user selects
{ the "none" item -- which looks better than showing the "none" text.
{----------------------------------------------------------------------------}

procedure TSortDialog.OnList( var Msg: TMessage; level: integer );
var
  index: integer;
begin
  if ( Msg.lParamHi = cbn_Closeup ) or ( Msg.lParamHi = cbn_KillFocus ) then
  begin
    if pLists[ level ]^.GetSelIndex = 0 then
      pLists[ level ]^.SetSelIndex( -1 );
  end;
  UpdateEnables;
end;

procedure TSortDialog.OnL1List( var Msg: TMessage );
begin
  OnList( Msg, 1 );
end;

procedure TSortDialog.OnL2List( var Msg: TMessage );
begin
  OnList( Msg, 2 );
end;

procedure TSortDialog.OnL3List( var Msg: TMessage );
begin
  OnList( Msg, 3 );
end;



{----------------------------------------------------------------------------}
{ This procedure handles the Back button command. It's like the TDialog.Ok
{ handler but it returns id_Back.
{----------------------------------------------------------------------------}

procedure TSortDialog.OnBack( var Msg: TMessage );
begin
  if CanClose then
  begin
    ReadControls( pSortParameters );
    EndDlg( id_Back );
  end;
end;



{----------------------------------------------------------------------------}
{ This procedure handles the Back button command. It's like the TDialog.Ok
{ handler but it returns id_Back.
{----------------------------------------------------------------------------}

procedure TSortDialog.OnNext( var Msg: TMessage );
begin
  if CanClose then
  begin
    ReadControls( pSortParameters );
    EndDlg( id_Next );
  end;
end;



                        {----------------------}
                        {  EXPORTED procedures }
                        {----------------------}



{----------------------------------------------------------------------------}
{ This procedure handles a command to view and/or edit the sort parameters.
{ It uses a dialog box.
{
{ Inputs:
{   pParent - pointer to parent window
{   Title - pointer to a string to be used for the dialog box's title
{   pKeyInformation - an information object derived from TKeyInformation
{   EnableBackButton - if true, the Back button will be enabled
{   SortParameters - input parameters
{ Output:
{   SortParameters - output parameters, unchanged if user cancels
{
{ Return values: id_Back, id_Next, or id_Cancel depending on button selected.
{
{----------------------------------------------------------------------------}

function SortParametersDialog( pParent: PWindowsObject; Title: PChar;
  pKeyInformation: PKeyInformation; EnableBackButton: boolean;
  var SortParameters: SortEngineParameters ): integer;

var
  pDialog: PSortDialog;

begin { SortParametersDialog }

  pDialog := New( PSortDialog, Init( pParent, pKeyInformation,
    @SortParameters, EnableBackButton, Title ) );
  SortParametersDialog := Application^.ExecDialog( pDialog );

end;

END.