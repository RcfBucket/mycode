unit BlockCvt;

{- This unit is provided to convert blocks to hex string and
   strings to blocks.

}

INTERFACE

uses
  Strings;

procedure CvtBlockToStr(dest: PChar; src: pointer; blockLen, maxDestLen: integer);
function  CvtStrToBlock(dest: pointer; src: PChar; maxDestLen: integer): integer;

IMPLEMENTATION

uses
  strsw,
  dmsdebug;

procedure CvtBlockToStr(dest: PChar; src: pointer; blockLen, maxDestLen: integer);
var
  k     : integer;
  b     : byte;
  tmp   : array[0..50] of char;
begin
  k:= 0;
  StrCopy(dest, '');
  while (k + 3 < maxDestLen) and (k < blockLen) do
  begin
    b:= byte((pchar(src)+k)^);

    HexStr(b, 1, tmp, 50);
    (* need to tell evan about this so getfldasstr can be fixed *)
    if (k <> (blockLen - 1)) then
      StrCat(tmp, ' ');

    strlcat(dest, tmp, maxDestLen);
    Inc(k);
  end;
end;

function  CvtStrToBlock(dest: pointer; src: PChar; maxDestLen: integer): integer;
{- this routine converts a string representation of a block back into a block.
   it assumes that the string [src] is in the following format:
     hh hh hh hh hh ...
   where hh are hex valid digits. The number of spaces between digits must
   be at least 1.  If a hex digit is invalid, then the value will be converted
   as zero. }
var
  k     : integer;
  j     : integer;
  bSize : integer;
  tmp   : array[0..50] of char;
  code  : integer;
  ret   : integer;
  aVal  : byte;
  sLen  : integer;
begin
  ret:= 0;
  k:= 0;
  j:= 0;
  bSize:= 0;
  sLen:= StrLen(src);
  while (j < sLen) and (bSize < maxDestLen) do
  begin
    {- skip over any leading spaces}
    while (src[j] = ' ') and (j < sLen) do
      Inc(j);

    k:= 0;
    while (j < sLen) and (src[j] <> ' ') and (k < 5) do
    begin
      tmp[k]:= src[j];
      Inc(j);
      Inc(k);
    end;
    tmp[k]:= #0;
    aVal:= byte(HVal(tmp, code));
    if (code <> 0) or (k > 3) then
    begin
      aVal:= 0;
      if ret = 0 then
        ret := j + code;
    end;
    Move(aVal, (PChar(dest) + bSize)^, 1);
    Inc(bSize);
  end;
  FillChar((PChar(dest) + bSize)^, maxDestLen - bSize, 0);
  CvtStrToBlock:= ret;
end;

END.

{- rcf }
