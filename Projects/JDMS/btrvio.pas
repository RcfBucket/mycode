{- This file is used by the OLD dbif unit. It is here only to support the old
   dbif and the convert program  (rcf) }
Unit BtrvIO;

Interface
  Function Btrv(Operation:Word;Var Pos,Data;Var DataLen:Word;Var Kbuf;KeyLen,Key:Byte):Word;
  Function BtrvInit(InitStr: PChar):Word;
  Function BShellInit(Var OpStr):Word;
  Function BtrvStop:Word;
  Function BrqShellInit(Var OpStr):Word;

Implementation

  Function Btrv;         external 'WBTRCALL' index 1;
  Function BShellInit;   external 'WBTRCALL' index 2;
  Function BtrvInit;     external 'WBTRCALL' index 3;
  Function BtrvStop;     external 'WBTRCALL' index 4;
  Function BrqShellInit; external 'WBTRCALL' index 5;

Var
	SavedExitProc : Pointer;

Procedure BtrvDone; Far;

Begin
	ExitProc := SavedExitProc;
	BtrvStop;
End;

Begin
  SavedExitProc := ExitProc;
  ExitProc := @BtrvDone;
  BtrvInit('/M:64 /F:50 /P:1024')
End.
