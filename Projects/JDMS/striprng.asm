.MODEL TPASCAL

PUBLIC        StripRange

.CODE
;========================================================== StripRange
;
;  function StripRange(dest, src: PChar; maxLen: integer; low, high: Char): PChar;
;
;  Removes all occurances of characters in tstr that fall within the
;  low-high range.
;--------------------------------------------------------------------
;
;     2       4       2      2      2        4     4
;   | BP | RetAddr | high | low | maxLen |  src | dest | <----- Stack Usage
;   `--------------------------------------------------'
;                  ^=+6   ^=+8  ^=+10    ^=+12  ^=+16
;
StripRange        PROC FAR

; Parameters

dest    equ dword ptr   ss:[bp+16]
src     equ dword ptr   ss:[bp+12]
maxLen  equ word ptr    ss:[bp+10]
lo      equ byte ptr    ss:[bp+8]
hi      equ byte ptr    ss:[bp+6]
pSize   equ 14

        push    bp
        mov     bp,sp                ; get pointer into stack
        push    ds                   ; save Turbo's DS in DX

        cld                          ; direction ->
        lds     si, src              ; DS:SI -> tstr
        les     di, dest             ; ES:DI -> result

        mov     cx, maxLen

startLoop:
        lodsb                        ; al = tstr[si]

        cmp     al,0                 ; end of string ?
        je      done

        cmp     al,lo
        jb      stowit               ; < low then store it
        cmp     al,hi
        ja      stowit               ; > high then store it
        jmp     endLoop              ; char in range skip it
stowIt:
        stosb
endLoop:
        loop    startLoop
done:
        mov     al,0
        stosb                        ; save terminating null

        mov     ax, word ptr dest
        mov     dx, word ptr dest+2

        pop        ds                ; restore some regs
        pop        bp
        ret        pSize
StripRange        ENDP

;--------------------------------------------------------------------
        END
