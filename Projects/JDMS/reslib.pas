{----------------------------------------------------------------------------}
{  Module Name  : RESLIB.PAS                                                 }
{  Programmer   : EJ                                                         }
{  Date Created : 01/30/95                                                   }
{                                                                            }
{  Purpose - This unit provides access to the result data file (RESULT.DBD). }
{                                                                            }
{  Assumptions - Each isolate order defined will have ONE result record that }
{    will contain all test results for the related test order packed into    }
{    that result record.  However, isolates may have more than one isolate   }
{    order and therefore more than one result record. The PResults object    }
{    will contain ALL results for a specified isolate.                       }
{                                                                            }
{    MaxMDAEntries must not create an overflow in the MIC byte.              }
{                                                                            }
{    Once constructed, the content of the traysObj^.gMda array will not      }
{      change.                                                               }
{                                                                            }
{  Initialization - Query result field size from database.                   }
{                                                                            }
{  Format of in memory records (using LoadResults) are as follows:           }
{                                                                            }
{    TESTGROUPID--TESTID--TESTCATID--TESTREF--DRUGREF--RESULT                }
{    ..                                                                      }
{    ..                                                                      }
{                                                                            }
{  The result list in memory is sorted by TESTGROUPID-TESTID-TESTCATID.      }
{                                                                            }
{  Special                                                                   }
{    To maintain the set of unit error numbers, whenever the Assert is       }
{    added, search for Assert and renumber them with unique values.          }
{                                                                            }
{  Note: Currently calling HandleEvents at all times. May consider adding a  }
{    flag to indicate if it should be called                                 }
{                                                                            }
{  Revision History - This project is under version control, use it to view  }
{    revision history.                                                       }
{                                                                            }
{----------------------------------------------------------------------------}

unit ResLib;

INTERFACE

uses
  DBTypes,
  DBFile,
  DBLib,
  Trays,
  Objects;

const
  {--------------------------------------------------------------------------}
  {--------------------------------------------------------------------------}
  RESmaxStrResult    = 20;   {- maximum length of an unpacked result }
  REStestgroupIDLen  = 9;    {- maximum lengths of fields (from DBLib) }
  REStestIDLen       = 8;
  REStestNameLen     = 33;
  REStestCatIDLen    = 6;

  {--------------------------------------------------------------------------}
  { Result Flags - Used in SetFlag and GetFlag
  {
  { Flaw: an object should know its data and not provide a blank flag field
  { for unknown use. A function such as IsContraIndicated and
  { SetContraIndicated would be much better. And how else can the modified
  { flag be maintained?
  {--------------------------------------------------------------------------}
  REScontraIndicated = 0;

  {--------------------------------------------------------------------------}
  { Used by Additional Drugs
  {--------------------------------------------------------------------------}
  NullTestGroupID = #255#0;



type
  {--------------------------------------------------------------------------}
  { This object represents a result record.
  {
  { Violation in gather.pas, it directly modifies drugSeq! Thus, drugSeq
  { cannot be private and given an accessor method.
  {
  { Violation in this file and elswhere, flag is passed to TestBit which
  { requires a var. TestBit should not require a var and flag should be made
  { into a function or replaced with the IsContraIndicated and
  { SetContraIndicated functions discussed above.
  {
  { Flaw: why is result exposed (and used in MPSS) when it has get/set
  { functions? MPSS needs to use the access functions so this variable can be
  { private.
  {--------------------------------------------------------------------------}
  PResRecObj = ^TResRecObj;
  TResRecObj = object( TObject )

    { Interface }
    testgroupID : array [ 0..REStestgroupIDLen-1 ] of char; { read-only }
    testID      : array [ 0..REStestIDLen-1 ] of char;      { read-only }
    testName    : array [ 0..REStestNameLen-1 ] of char;    { read-only }
    testCatID   : array [ 0..REStestCatIDLen-1 ] of char;   { read-only }
    result      : array [ 0..RESmaxStrResult ] of char;     { read-only - ! }
    drugSeq     : TSeqNum;                                  { read-only - ? }
    flag        : word;                                     { read-only - ? }
    constructor Init( aTestGroupID: PChar; aTGUD: boolean; aTestID,
      aTestCatID, aTestName: PChar; aIsordSeq, aTestSeq, aDrugSeq: TSeqNum;
      aResult: PChar; aPosition: integer; aMaxResLen: byte; aFlag: word );
    destructor Done; virtual;
    function tgUD: boolean;
    function testSeq: TSeqNum;
    function GetResult( aResult: PChar; maxLen: integer ): PChar;
    procedure SetResult( aResult: PChar );
    procedure ClearModify;

    { Implementation }
    private
    _tgUD: boolean;
    _testSeq: TSeqNum;
    isordSeq: TSeqNum;
    modified: boolean;
    maxResLen: byte;
    pos: integer;
  end;

  {--------------------------------------------------------------------------}
  { This object represents a collection of results.
  {--------------------------------------------------------------------------}
  PResults = ^TResults;
  TResults = object( TSortedCollection )

    { Interface }
    traysObj      : PTraysObject;
    dbErrorNum    : integer;
    doYield       : boolean;
    constructor Init;
    destructor Done; virtual;
    function  Compare( key1, key2: pointer ): integer; virtual;
    function  CanLoad( isoRec: PDBRec  ): boolean; virtual;
    function  LoadResults( isoRec: PDBRec ): boolean; virtual;
    function  SaveResults: boolean; virtual;
    function  DeleteResults: boolean; virtual;
    function  IsModified: boolean; virtual;
    procedure ClearModify; virtual;
    function  GetResult( aTestID, aTestCatID, res: PChar; maxChars: integer ):
      boolean;
    function  SetResult( aTestID, aTestCatID, res: PChar; overwrite: boolean ):
      boolean;
    function  GetFlag( aTestID, aTestCatID: PChar; flagPos: byte ): boolean;
    procedure SetFlag( aTestID, aTestCatID: PChar; flagPos: byte;
      aVal: boolean );
    function  GetDrugResults( aDrugSeq: TSeqNum; MICres, NCCLSres: PChar;
      maxChars: integer; var contraInd: boolean ): boolean;
    function  AnyResultsExist: boolean;
    function  IDResultsExist: boolean;
    function  MICResultsExist: boolean;
    function  FindTestGroup( aTestGroupID: PChar ): integer;
    function  NextTestGroup( aTestGroupID: PChar; var index: integer ):
      boolean;
    function  NumTests( aTestGroupID: PChar; var first: integer ): integer;
    procedure ByteToMIC( mic: PChar; bVal: byte );
    procedure MICToByte( mic: PChar; var bVal: byte );
    function  ESBLChanged( Organism: Integer ): boolean;
    function  ESBLResultExists: boolean;
    function  SetESBLResult( res: PChar; overwrite: boolean ): boolean;
    function  GetESBLResult( res: PChar; maxChars: integer ): boolean;
    function  BuildAddResObj( TstSeqRef, ISOSeqRef: TSeqNum ): PResRecObj;
    procedure LoadAltResults( ISOSeqRef: TSeqNum );
    procedure SaveAltResults;

    {MFI uses it to 'unhide' ESBL and set its value}
    procedure ReInsertESBLObj;

    { Implementation }
    private
    resultsLoaded: boolean;
    resBuf       : PChar;    {- buffer to hold packed results }
    resDB        : PDBFile;
    ESBLState    : Boolean;
    ESBLObj      : PResRecObj;
    procedure UnPackResults( resultStr, pTestCatID: PChar;
      pos, resultLen: integer; var flag: word );
    procedure PackResults( p: PResRecObj );
    function  ESBLPointer: PResRecObj;
    procedure RemoveESBLObj;
    function  ESBLModified: Boolean;
  end;


IMPLEMENTATION

uses
  ApiTools,
  Bits,
  IntrpMap,
  DBIDs,
  ESBL,
  MScan,
  DMString,
  DMSErr,
  StrsW,
  Strings,
  DMSDebug,
  WinApi,
  WinProcs;

const
  {--------------------------------------------------------------------------}
  { The following constants define the packing of MIC information in an 8-bit
  { byte. If all 8-bits are set then the MIC is undefined. The byte is
  { invalid if the greater than and less than bits are both set. Otherwise,
  { the byte and its associated array of values indicates a MIC value.
  {--------------------------------------------------------------------------}
  MB_INDEX        = $1F; { bits 0-4 are an index }
  MB_GREATER_THAN = $20; { bit 5 indicates greater than }
  MB_LESS_THAN    = $40; { bit 6 indicates less than }


var
  {--------------------------------------------------------------------------}
  { This flag has the very first instance of a TResults object validate some
  { field sizes.
  {--------------------------------------------------------------------------}
  FieldSizesNeedValadation: boolean;


  {--------------------------------------------------------------------------}
  { Unit initialization will set this varaible equal the the size of
  { the DBRESBlock field of the results data file.
  {--------------------------------------------------------------------------}
  resFieldSize: word;




                        {----------------------}
                        {  Helper Functions    }
                        {----------------------}



{--------------------------------------------------------------------------}
{ This procedure handles assertions for this unit. If an assertion is
{ false, it calls show error with a number and halts. Otherwise, it does
{ nothing.
{--------------------------------------------------------------------------}

procedure Assert( Valid: boolean; UnitErrorNumber: integer );
begin
  if not Valid then
  begin
    ShowError( nil, IDS_PROGHALT, nil, MOD_RESLIB, MOD_RESLIB,
      UnitErrorNumber );
    Halt;
  end;
end;



                        {----------------------}
                        {     TResRecObj       }
                        {----------------------}



{----------------------------------------------------------------------------}
{ The constructor initializes the record with initial values. At this point,
{ the record is marked not modified.
{
{ Note: the job of a constructor is to place the object into a known and valid
{ state. This constructor fails to validate the input values before assigning
{ them to member varaibles. The member variables should be private so that
{ the modified attribute can be controlled. The way private works, this object
{ would need to be moved into its own file.
{
{ Why are strings copied into fixed length arrays? Allocating strings on
{ demand would save memory, eliminate clipping, and simplify the interface.
{----------------------------------------------------------------------------}

constructor TResRecObj.Init( aTestGroupID: PChar; aTGUD: boolean;
  aTestID, aTestCatID, aTestName: PChar; aIsordSeq, aTestSeq,
  aDrugSeq: TSeqNum; aResult: PChar; aPosition: integer; aMaxResLen: byte;
  aFlag: word );
begin
  inherited Init;

  { Store inputs }
  maxResLen := aMaxResLen;
  pos := aPosition;
  Assert( pos < resFieldSize, 1 );

  { Store result. Why not use SetResult to validate this input? }
  StrLCopy( result, aResult, RESmaxStrResult );

  { Store inputs }
  StrLCopy( testgroupID, aTestGroupID, REStestgroupIDLen - 1 );
  _tgUD := aTGUD;
  StrLCopy( testID, aTestID, REStestIDLen - 1 );
  StrLCopy( testName, aTestName, REStestNameLen - 1 );
  StrLCopy( testCatID, aTestCatID, REStestCatIDLen - 1 );
  isordSeq := aIsordSeq;
  _testSeq := aTestSeq;
  drugSeq := aDrugSeq;
  flag := aFlag;

  { Set state }
  ClearModify;
end;



{----------------------------------------------------------------------------}
{ The destructor performes object cleanup.
{----------------------------------------------------------------------------}

destructor TResRecObj.Done;
begin
  inherited Done;
end;



{----------------------------------------------------------------------------}
{ This function provides read-only access to tgUD.
{----------------------------------------------------------------------------}
function TResRecObj.tgUD: boolean;
begin
  tgUD := _tgUD;
end;



{----------------------------------------------------------------------------}
{ This function provides read-only access to testSeq.
{----------------------------------------------------------------------------}
function TResRecObj.testSeq: TSeqNum;
begin
  testSeq := _testSeq;
end;



{----------------------------------------------------------------------------}
{ This procedure sets the result string. If the new and current strings are
{ the same, nothing happens.
{
{ Side affect: In either case, the current string is trimmed.
{
{ Side affect: the flag is zeroed.
{
{ Warning: It's possible to set Result without using this function.
{----------------------------------------------------------------------------}

procedure TResRecObj.SetResult( aResult: PChar );
var
  TrimmedNew,
  TrimmedOld: Array [ 0..RESmaxStrResult ] of char;
begin
  { If Result was completly protected, it would not be necessary to re-trim it
    here. A flaw exists when an object cannot trust its own data. }

  TrimAll( TrimmedNew, aResult, ' ', RESmaxStrResult );
  TrimAll( TrimmedOld, Result, ' ', RESmaxStrResult );
  if StrLComp( TrimmedNew, TrimmedOld, RESmaxStrResult ) <> 0 then
  begin
    StrLCopy( result, aResult, RESmaxStrResult );
    modified := TRUE;
    flag := 0;
  end;
end;



{----------------------------------------------------------------------------}
{ This function copies the result string and returns a pointer to the copy.
{----------------------------------------------------------------------------}

function TResRecObj.GetResult( aResult: PChar; maxLen: integer ): PChar;
begin

  { Should a nil pointer raise an assertion? I would rewrite as follows:

    Assert( aResult <> nil, XX );
    GetResult := StrLCopy( aResult, result, maxLen );
  }

  { If the copy is truncated, should the caller be warned, or an assertion
    raised? Is it ok to chop results? I would add:

    Assert( StrLen( result ) <= maxLen, YY );
  }

  if aResult <> nil then
    StrLCopy( aResult, result, maxLen );
  GetResult := aResult;
end;



{----------------------------------------------------------------------------}
{ This procedure forces the object into the unmodified state.
{
{ Warning: Although modified was moved to private, TResult below can (and
{ does) clear this variable without using this function.
{----------------------------------------------------------------------------}

procedure TResRecObj.ClearModify;
begin
  modified := FALSE;
end;



                        {----------------------}
                        {        TResults      }
                        {----------------------}



{----------------------------------------------------------------------------}
{ The constructor initializes the object. After construction, results can be
{ loaded.
{----------------------------------------------------------------------------}

constructor TResults.Init;
begin
  inherited Init( 60, 60 );

  doYield := FALSE;
  resultsLoaded := FALSE;
  dbErrorNum := 0;
  GetMem( resBuf, resFieldSize );
  Assert( resBuf <> nil, 2 );
  traysObj := New( PTraysObject, Init );
  Assert( traysObj <> nil, 3 );
  resDB := New( PDBFile, Init( DBRESFile, '', dbOpenNormal ) );
  Assert( resDB <> nil, 4 );

  ESBLObj := Nil;
  ESBLState := False;
end;



{----------------------------------------------------------------------------}
{ The destructor performes object cleanup.
{----------------------------------------------------------------------------}

destructor TResults.Done;
begin
  MSDisposeObj( resDB );
  MSDisposeObj( traysObj );
  MSFreeMem( resBuf, resFieldSize );
  inherited Done;
end;



{----------------------------------------------------------------------------}
{ This function compares two results by: TESTGROUPID--TESTID--TESTCATID.
{----------------------------------------------------------------------------}

function TResults.Compare( key1, key2: pointer ): integer;
var
  k1,
  k2: array[0..80] of char;
begin
  StrLCopy( k1, PResRecObj( key1 )^.testgroupID, 80 );
  StrLCat( k1, PResRecObj( key1 )^.testID, 80 );
  StrLCat( k1, PResRecObj( key1 )^.testCatID, 80 );

  StrLCopy( k2, PResRecObj( key2 )^.testgroupID, 80 );
  StrLCat( k2, PResRecObj( key2 )^.testID, 80 );
  StrLCat( k2, PResRecObj( key2 )^.testCatID, 80 );

  Compare := StrComp( k1, k2 );
end;



{----------------------------------------------------------------------------}
{ This function quickly tests to see if results can be loaded using
{ LoadResults.
{
{ Note: this function must return the same result as LoadResults.
{----------------------------------------------------------------------------}

function  TResults.CanLoad( isoRec: PDBRec ): boolean;
var
  isordDB      : PDBAssocFile;
begin
  CanLoad := FALSE;
  if isoRec <> nil then
  begin
    isordDB := New( PDBAssocFile, Init( isoRec, DBISOLATE_ISORDER_ASSOC,
      dbOpenNormal ) );
    if isordDB <> nil then
    begin
      CanLoad := isordDB^.dbc^.GetFirst( isordDB^.dbr );

      { Cleanup }
      MSDisposeObj( isordDB );
    end;
  end;
end;



{----------------------------------------------------------------------------}
{ This function loads the results for the specified isolate.
{
{ Note: this function must return the same result as CanLoad.
{----------------------------------------------------------------------------}

function  TResults.LoadResults( isoRec: PDBRec ): boolean;
var
  fi           : TFldInfo;
  isordDB      : PDBAssocFile;
  toDB         : PDBFile;
  todDB        : PDBAssocFile;
  tstDB        : PDBFile;
  tcatDB       : PDBFile;
  pTestGroupID : array [ 0..REStestgroupIDLen - 1 ] of char;
  pTGUD        : boolean;
  pTestID      : array [ 0..REStestIDLen - 1 ] of char;
  pTestName    : array [ 0..REStestNameLen - 1 ] of char;
  pTestCatID   : array [ 0..REStestCatIDLen - 1 ] of char;
  isorderSeq   : TSeqNum;
  testgroupSeq : TSeqNum;
  testSeq      : TSeqNum;
  drugSeq      : TSeqNum;
  tcatSeq      : TSeqNum;
  resultLen    : byte;
  pos          : integer;
  isOK         : boolean;
  gotResult    : boolean;
  resultStr    : array [ 0..RESmaxStrResult ] of char;
  resObj       : PResRecObj;
  flag         : word;

begin

  { Defaults }
  LoadResults := FALSE;
  resultsLoaded := FALSE;
  dbErrorNum := 0;

  { Clear any pending results in memory }
  FreeAll;

  {}
  flag := 0;

  { Cannot load from nothing. Note, this should have been tested before
    resultsLoaded or FreeAll was called. As is, it courupts the state of the
    object. }
  if isoRec = nil then
    Exit;

  {}
  isordDB := New( PDBAssocFile, Init( isoRec, DBISOLATE_ISORDER_ASSOC,
    dbOpenNormal ) );
  Assert( isordDB <> nil, 5 );

  {}
  isOK := isordDB^.dbc^.GetFirst( isordDB^.dbr );

  { At this point if isOK is TRUE, the first isorder has been found. }

  { If Ok, loop through the test orders and load TestGroupID, then loop thru
    the test order definitions and load TestRef, then using the TestRef, load
    the TestID, TestCatID, DrugRef, and TCatRef from TEST database. }
  if isOK then
  begin
    { Initialize }
    toDB := New( PDBFile, Init( DBTSTGRPFile, '', dbOpenNormal ) );
    Assert( toDB <> nil, 6 );

    tstDB := New( PDBFile, Init( DBTSTFile, '', dbOpenNormal ) );
    Assert( tstDB <> nil, 7 );

    tcatDB := New( PDBFile, Init( DBTSTCATFile, '', dbOpenNormal ) );
    Assert( tcatDB <> nil, 8 );

    toDB^.dbc^.SetSeqKeyNum;
    tstDB^.dbc^.SetSeqKeyNum;
    tcatDB^.dbc^.SetSeqKeyNum;

    { The first instance to load results is responsible for validating the
      field sizes. }
    if FieldSizesNeedValadation then
    begin
      FieldSizesNeedValadation := FALSE;
      toDB^.dbd^.FieldInfo( DBTSTGRPID, fi );
      Assert( REStestgroupIDLen = fi.fldSize, 9 );
      tstDB^.dbd^.FieldInfo( DBTSTID, fi );
      Assert( REStestIDLen = fi.fldSize, 10 );
      tstDB^.dbd^.FieldInfo( DBTSTName, fi );
      Assert( REStestNameLen = fi.fldSize, 11 );
      tstDB^.dbd^.FieldInfo( DBTSTTstCat, fi );
      Assert( REStestCatIDLen = fi.fldSize, 12 );
    end;

    { Loop }
    repeat
      if doYield then
        HandleEvents( 0 ); {- this will yield to the app. }
      {- get test results by value of isord seq }
      isorderSeq := isordDB^.dbr^.GetSeqValue;
      resDB^.dbr^.PutField( DBRESIsoOrdRef, @isorderSeq );
      gotResult := resDB^.dbc^.GetEQ( resDB^.dbr );
      if gotResult then
        resDB^.dbr^.GetField( DBRESBlock, resBuf, resFieldSize );

      isordDB^.dbr^.GetField( DBISOORDTstGrpRef, @testgroupSeq,
        SizeOf( TSeqNum ) );

      toDB^.dbc^.GetSeq( toDB^.dbr, testgroupSeq );
      toDB^.dbr^.GetField( DBTSTGRPID, @pTestGroupID, REStestgroupIDLen );
      toDB^.dbr^.GetField( DBTSTGRPUDFlag, @pTGUD, SizeOf( boolean ) );

      todDB := New( PDBAssocFile, Init( toDB^.dbr,
                            DBTESTGRP_TSTGPXRF_ASSOC, dbOpenNormal ) );
      Assert( todDB <> nil, 13 );

      if todDB^.dbc^.GetFirst( todDB^.dbr ) then
        repeat
          if doYield then
            HandleEvents( 0 ); {- this will yield to the app. }
          todDB^.dbr^.GetField( DBTSTGRPXRFTstRef, @testSeq,
            SizeOf( TSeqNum ) );
          todDB^.dbr^.GetField( DBTSTGRPXRFResBlockPos, @pos,
            SizeOf( integer ) );

          tstDB^.dbc^.GetSeq( tstDB^.dbr, testSeq );
          tstDB^.dbr^.GetField( DBTSTID, @pTestID, REStestIDLen );
          tstDB^.dbr^.GetField( DBTSTName, @pTestName, REStestNameLen );
          tstDB^.dbr^.GetField( DBTSTTstCat, @pTestCatID, REStestCatIDLen );
          tstDB^.dbr^.GetField( DBTSTDrugRef, @drugSeq, SizeOf( TSeqNum ) );
          tstDB^.dbr^.GetField( DBTSTTstCatRef, @tcatSeq, SizeOf( TSeqNum ) );

          {- Get length of result field defined for this test }
          tcatDB^.dbc^.GetSeq( tcatDB^.dbr, tcatSeq );
          tcatDB^.dbr^.GetField( DBTSTCATResLen, @resultLen, SizeOf( byte ) );

          {- Each result record read will have to be unpacked and each
            individual test must be put into the "in memory" list,
            matching it with it's appropriate test id.
          }
          StrCopy( resultStr, '' );
          flag := 0;
          if gotResult then
            UnPackResults( resultStr, pTestCatID, pos, resultLen, flag );

          {- add test information to result collection }
          resObj := New( PResRecObj, Init( pTestGroupID, pTGUD, pTestID,
            pTestCatID, pTestName, isorderSeq, testSeq, drugSeq, resultStr,
            pos, resultLen, flag ) );
          Insert( resObj );
        until not todDB^.dbc^.GetNext( todDB^.dbr );

      MSDisposeObj( todDB );
    until not isordDB^.dbc^.GetNext( isordDB^.dbr );

    { Load alt user defined test/results }
    isordDB^.dbr^.GetField(DBIsoOrdIsoRef, @isorderSeq, SizeOf(TSeqNum) );
    LoadAltResults( isorderseq );

    { Cleanup }
    MSDisposeObj( tcatDB );
    MSDisposeObj( tstDB );
    MSDisposeObj( toDB );
  end;

  { Cleanup }
  MSDisposeObj( isordDB );

  { Record outcome }
  isOK := isOK and ( dbErrorNum = 0 );
  resultsLoaded := isOK;
  LoadResults := isOK;

  { Handle ESBL functionality }
  ESBLObj := ESBLPointer;
  ESBLState := ( ESBLObj <> Nil ) and ( StrComp( ESBLObj^.Result, '' ) <> 0 );
  if Not ESBLState then
    RemoveESBLObj;
end;



{----------------------------------------------------------------------------}
{ This function saves the results to the results data file.
{----------------------------------------------------------------------------}

function TResults.SaveResults: boolean;
var
  isorderSeq: TSeqNum;
  gotResult: boolean;
  pResult: PResRecObj;
  j: integer;
begin

  { Defaults }
  SaveResults := FALSE;
  dbErrorNum := 0;

  { Return defaults if nothing to save. Note: why not call IsModified
    or better, let the caller decide to save or not, then if none loaded, save
    as "untitled."  Also, count = 0 is unsafe: what if 1 result was loaded and
    then it was deleted? See note with DeleteResults. }
  if ( not resultsLoaded ) or ( count = 0 ) then
    Exit;

  { Give ESBL a chance to include itself. }
  ReInsertESBLObj;

  { Save any Alt/Offline user tests first. }
  SaveAltResults;

  { For each test order: pack results into resBuf then put into the result
    record and store into the result file. Note: this loop assumes the results
    are sorted by order. }
  j := 0;
  pResult := At( 0 );
  while ( j < count ) and ( dbErrorNum = 0 ) do
  begin
    { Update order. Note: Order is not guaranteed, mingled IsoOrdSeqs will
      cause mangled results, retrieve then update. }
    isorderSeq := pResult^.isordSeq;
    resDB^.dbr^.PutField( DBRESIsoOrdRef, @isorderSeq );
    gotResult := resDB^.dbc^.GetEQ( resDB^.dbr );

    { Initialize. }
    if gotResult then
      resDB^.dbr^.GetField( DBRESBlock, resBuf, resFieldSize )
    else
      FillChar( resBuf^,resFieldSize,#0 );

    { Pack results for current order. Note: this is risky, if none are
      packed, the outer while will lock-up. }
    while ( j < count ) and ( isorderSeq = pResult^.isordSeq ) do
    begin
      {Ignore Addt'l tests, not associated to a GroupID}
      if StrComp( pResult^.testgroupID, NullTestGroupID ) <> 0 then
         PackResults( pResult );
      pResult^.modified := FALSE;
      Inc( j );
      if j < count then
        pResult := At( j );
    end;

    { Save order to data file }
    resDB^.dbr^.PutField( DBRESIsoOrdRef, @isorderSeq );
    gotResult := resDB^.dbc^.GetEQ( resDB^.dbr );
    resDB^.dbr^.PutField( DBRESBlock, resBuf );
    if gotResult then
      resDB^.dbc^.UpdateRec( resDB^.dbr )
    else
      resDB^.dbc^.InsertRec( resDB^.dbr );
    dbErrorNum := resDB^.dbc^.dbErrorNum;
  end;

  {Remove the ESBL Result, if needed}
  if Not ESBLState then RemoveESBLObj;

  SaveResults := ( dbErrorNum = 0 );
end;



{----------------------------------------------------------------------------}
{ This function deletes all results that are currently loaded. LoadResults
{ must be called after this routine if new results are to be dealt with.
{
{ Note: Why was DeleteAll not used for this? If AtDelete, Delete, and
{ DeleteAll should not be used, then overrides should be created that assert
{ false. If TResult is not a special kind of TCollection, then it should not
{ be derived from it, composition should have been used.
{----------------------------------------------------------------------------}

function TResults.DeleteResults: boolean;
var
  k: integer;
  pResult: PResRecObj;
begin
  DeleteResults := FALSE;
  dbErrorNum := 0;
  {- see if results have been previously loaded}
  if ( not resultsLoaded ) or ( count = 0 ) then
    Exit;

  {- delete all old results }
  k := 0;
  repeat
    pResult := At( k );
    { Assert: pResult <> nil }
    resDB^.dbr^.PutField( DBRESIsoOrdRef, @pResult^.isordSeq );
    if resDB^.dbc^.GetEQ( resDB^.dbr ) then
    begin
      resDB^.dbc^.DeleteRec( resDB^.dbr );
      if dbErrorNum = 0 then
        dbErrorNum := resDB^.dbc^.dbErrorNum;
    end;
  until not NextTestGroup( pResult^.testgroupID, k );

  FreeAll;     {- clear all results from memory collection }
  resultsLoaded := FALSE;
  DeleteResults := ( dbErrorNum = 0 );
end;



{----------------------------------------------------------------------------}
{ This function ...
{
{ Note: if one or all results are deleted, this function will report false.
{ See note in DeleteResults.
{----------------------------------------------------------------------------}

function TResults.IsModified: boolean;

  {--------------------------------------------------------------------------}
  {--------------------------------------------------------------------------}
  function IsMod( item: PResRecObj ): boolean; Far;
  begin
    IsMod := item^.modified;
  end;

begin { IsModified }

  { Default }
  IsModified := FALSE;

  { Return default if if results have not been loaded or if the number of
    them has been reduced to zero. Caution: what if one or all results were
    deleted? }
  if ( not resultsLoaded ) or ( count = 0 ) then
    Exit;

  { Report if any are modified }
  IsModified := ( FirstThat( @IsMod ) <> nil ) or ESBLModified;
end;



{----------------------------------------------------------------------------}
{----------------------------------------------------------------------------}

procedure TResults.ClearModify;

  {--------------------------------------------------------------------------}
  {--------------------------------------------------------------------------}
  procedure ClrMod( item: PResRecObj ); Far;
  begin
    item^.modified := FALSE;
  end;

begin
  ForEach( @ClrMod );
end;



{----------------------------------------------------------------------------}
{ This function returns a result for the particular test ID.
{----------------------------------------------------------------------------}

function TResults.GetResult( aTestID, aTestCatID, res: PChar;
  maxChars: integer ): boolean;

  {--------------------------------------------------------------------------}
  {--------------------------------------------------------------------------}
  function IsMatch( item: PResRecObj ): boolean; Far;
  begin
    IsMatch := ( StrComp( item^.testID, aTestID ) = 0 ) and
      ( StrComp( item^.testCatID, aTestCatID ) = 0 );
  end;

var
  pResult : PResRecObj;

begin { GetResult }

  { Default }
  GetResult := FALSE;
  res[0] := #0;

  { Try }
  pResult := FirstThat( @IsMatch );
  if pResult <> nil then
  begin
    { Limit size }
    if maxChars > RESmaxStrResult then
      maxChars := RESmaxStrResult;

    { Return result }
    StrMove( res, pResult^.result, maxChars );
    res[ maxChars ] := #0;
    GetResult := TRUE;
  end;
end;



{----------------------------------------------------------------------------}
{ This function sets a result for the test ID in resObj. If overwrite is
{ FALSE the result will not be changed if it already contains a value. If
{ overwrite is true then the result will always be overwritten.
{
{ Return value: a ture value indicates the result was set. A false value
{   indicates the result was not found or that it was not overwritten.
{----------------------------------------------------------------------------}

function TResults.SetResult( aTestID, aTestCatID, res: PChar;
  overwrite: boolean ): boolean;

  {--------------------------------------------------------------------------}
  {--------------------------------------------------------------------------}
  function IsMatch( item: PResRecObj ): boolean; Far;
  begin
    IsMatch := ( StrComp( item^.testID, aTestID ) = 0 ) and
      ( StrComp( item^.testCatID, aTestCatID ) = 0 );
  end;

var
  pResult : PResRecObj;

begin { SetResult }

  { Default }
  SetResult := FALSE;

  { Try }
  pResult := FirstThat( @IsMatch );
  if pResult <> nil then
    { Note: the "not overwrite and" is not needed }
    if overwrite or ( not overwrite and ( StrLen( pResult^.result ) = 0 ) ) then
    begin
      pResult^.SetResult( res );
      SetResult := TRUE;
    end;
end;



{----------------------------------------------------------------------------}
{ This function ...
{
{ Warning: A false return value may indicate the result was not found or that
{ the bit is clear. This function needs rethinking.
{----------------------------------------------------------------------------}

function TResults.GetFlag( aTestID, aTestCatID: PChar; flagPos: byte ):
  boolean;

  {--------------------------------------------------------------------------}
  {--------------------------------------------------------------------------}
  function IsMatch( item: PResRecObj ): boolean; Far;
  begin
    IsMatch := ( StrComp( item^.testID, aTestID ) = 0 ) and
               ( StrComp( item^.testCatID, aTestCatID ) = 0 );
  end;

var
  pResult : PResRecObj;

begin { GetFlag }

  pResult := FirstThat( @IsMatch );
  if pResult = nil then
    GetFlag := FALSE
  else
    GetFlag := ( pResult^.flag and ( 1 shl ( flagPos mod 8 ) ) ) <> 0;
end;



{----------------------------------------------------------------------------}
{----------------------------------------------------------------------------}

procedure TResults.SetFlag( aTestID, aTestCatID: PChar; flagPos: byte;
  aVal: boolean );

  {--------------------------------------------------------------------------}
  {--------------------------------------------------------------------------}
  function IsMatch( item: PResRecObj ): boolean; Far;
  begin
    IsMatch := ( StrComp( item^.testID, aTestID ) = 0 ) and
      ( StrComp( item^.testCatID, aTestCatID ) = 0 );
  end;

var
  pResult : PResRecObj;
  OrigFlag: Word;
begin { SetFlag }

  pResult := FirstThat( @IsMatch );
  if pResult <> nil then
  begin
    OrigFlag:=pResult^.flag;
    if aVal then
      pResult^.flag := pResult^.flag or ( 1 shl ( flagPos mod 8 ) )
    else
      pResult^.flag := pResult^.flag and not ( 1 shl ( flagPos mod 8 ) );

    if (OrigFlag<>pResult^.flag) then pResult^.Modified:=True;
  end;
end;



{----------------------------------------------------------------------------}
{ This function returns both results, MIC and NCCLS, for the particular
{ drug. It retunrns true if both are returned. If it returns false, the
{ values are undefined.
{----------------------------------------------------------------------------}

function TResults.GetDrugResults( aDrugSeq: TSeqNum; MICres, NCCLSres: PChar;
  maxChars: integer; var contraInd: boolean ): boolean;

  {--------------------------------------------------------------------------}
  { Returns true if the TCollection based index is valid.
  {--------------------------------------------------------------------------}
  function ValidIndex( Index: integer ): boolean;
  begin
    ValidIndex := ( Index >= 0 ) and ( Index < count );
  end;

  {--------------------------------------------------------------------------}
  {--------------------------------------------------------------------------}
  function IsMatch( item: PResRecObj ): boolean; Far;
  begin
    IsMatch := ( item^.drugSeq = aDrugSeq );
  end;

var
  pResult : PResRecObj;
  NextIndex: Integer;

begin { GetDrugResults }

  { Limit size }
  if maxChars > RESmaxStrResult then
    maxChars := RESmaxStrResult;

  { Defaults }
  MICres[ 0 ] := #0;
  NCCLSres[ 0 ] := #0;

  { Get results }
  pResult := FirstThat( @IsMatch );
  while pResult <> nil do
  begin
    if StrComp( pResult^.testCatID, 'MIC' ) = 0 then
    begin

      { As MIC }
      StrMove( MICres, pResult^.result, maxChars );
      MICres[ maxChars ] := #0;
    end
    else if StrComp( pResult^.testCatID, 'NCCLS' ) = 0 then
    begin

      { As NCCLS }
      StrMove( NCCLSres, pResult^.result, maxChars );
      NCCLSres[ maxChars ] := #0;
      contraInd := TestBit( pResult^.flag, REScontraIndicated );
    end;

    { Set up next iteration. Continue with next result if drug matches, else
      stop. }
    pResult := nil;
    NextIndex := IndexOf( pResult ) + 1;
    if ValidIndex( NextIndex ) then
    begin
      pResult := At( NextIndex );
      if pResult^.drugSeq <> aDrugSeq then
        pResult := nil;
    end;
  end;

  { Return true if both results were got. }
  GetDrugResults := ( MICres[ 0 ] <> #0 ) and ( NCCLSres[ 0 ] <> #0 );
end;



{----------------------------------------------------------------------------}
{ This function returns true if any non-null results exist.
{----------------------------------------------------------------------------}

function TResults.AnyResultsExist: boolean;

  {--------------------------------------------------------------------------}
  {--------------------------------------------------------------------------}
  function IsFound( item: PResRecObj ): boolean; Far;
  begin
    IsFound := StrComp( item^.result, '' ) <> 0;
  end;

begin
  AnyResultsExist := FirstThat( @IsFound ) <> nil;
end;



{----------------------------------------------------------------------------}
{ This function returns true if drug or biotype non-null results exist.
{----------------------------------------------------------------------------}

function TResults.IDResultsExist: boolean;

  {--------------------------------------------------------------------------}
  {--------------------------------------------------------------------------}
  function IsFound( item: PResRecObj ): boolean; Far;
  begin
    IsFound := ( StrComp( item^.result, '' ) <> 0 ) and
      ( ( StrComp( item^.testCatID, 'MIC' ) = 0 ) or
      ( StrComp( item^.testCatID, 'NCCLS' ) = 0 ) or
      ( StrComp( item^.testCatID, 'BIO' ) = 0 ) );
  end;

begin
  IDResultsExist := FirstThat( @IsFound ) <> nil;
end;



{----------------------------------------------------------------------------}
{ This function returns true if MIC non-null results exist.
{----------------------------------------------------------------------------}

function TResults.MICResultsExist: boolean;

  {--------------------------------------------------------------------------}
  {--------------------------------------------------------------------------}
  function IsFound( item: PResRecObj ): boolean; Far;
  begin
    IsFound := ( StrComp( item^.result, '' ) <> 0 ) and
      ( StrComp( item^.testCatID, 'MIC' ) = 0 );
  end;

begin
  MICResultsExist := FirstThat( @IsFound ) <> nil;
end;



{----------------------------------------------------------------------------}
{ This function returns the index of the first result with specified test
{ order ID. It returns -1 if none match.
{----------------------------------------------------------------------------}

function TResults.FindTestGroup( aTestGroupID: PChar ): integer;

  {--------------------------------------------------------------------------}
  {--------------------------------------------------------------------------}
  function Matches( item: PResRecObj ): boolean; Far;
  begin
    Matches := ( StrComp( item^.testgroupID, aTestGroupID ) = 0 );
  end;

var
  res   : PResRecObj;

begin { FindTestGroup }

  res := FirstThat( @Matches );
  if res <> nil then
    FindTestGroup := IndexOf( res )
  else
    FindTestGroup := -1;
end;



{----------------------------------------------------------------------------}
{ This function returns the next test order ID in result block. aTestGroupID
{ must contain the current test order ID (if aTestGroupID = '' then the first
{ test order ID will be returned). If no test order IDs exist after
{ aTestGroupID then FALSE will be returned.
{----------------------------------------------------------------------------}

function TResults.NextTestGroup( aTestGroupID: PChar; var index: integer ):
  boolean;
var
  k: integer;
  p: PResRecObj;
  found: boolean;
begin
  { Defaults }
  index := -1;
  NextTestGroup := FALSE;

  { Case 1 }
  if count = 0 then
  begin
    StrCopy( aTestGroupID, '' );
    Exit;
  end;

  { Case 2 }
  if StrComp( aTestGroupID, '' ) = 0 then
  begin
    p := At( 0 );
    StrLCopy( aTestGroupID, p^.testgroupID, REStestgroupIDLen-1 );
    index := 0;
    NextTestGroup:= true;
    Exit;
  end;

  { Case 3 }
  found := FALSE;
  k := FindTestGroup( aTestGroupID );  {- find current test order in list }
  if k <> -1 then
  begin
    while ( k < count ) and not found do
    begin
      p := At( k );
      found := ( StrComp( p^.testgroupID, aTestGroupID ) <> 0 ) and
               ( StrComp( p^.testgroupID, NullTestGroupID ) <> 0 );
      if found then
        Break;
      Inc( k );
    end;
    if found then
    begin
      StrLCopy( aTestGroupID, p^.testgroupID, REStestgroupIDLen-1 );
      NextTestGroup := TRUE;
    end;
  end;
  index := k;
end;



{----------------------------------------------------------------------------}
{ calculate the number of results associated with a test order. Return
{ the index of the first occurance and the count. First will equal -1 if
{ the test order is not found
{----------------------------------------------------------------------------}

function TResults.NumTests( aTestGroupID: PChar; var first: integer ):
  integer;
var
  k: integer;
  tgID: array [0..REStestgroupIDLen-1] of char;
begin
  NumTests := 0;
  StrLCopy( tgID, aTestGroupID, REStestgroupIDLen-1 );
  first := FindTestGroup( tgID );
  if ( first <> -1 ) then
  begin
    NextTestGroup( tgID, k );
    if k = -1 then
      NumTests := count - first
    else
      NumTests := k - first;
  end;
end;



{----------------------------------------------------------------------------}
{ This procedure converts a MIC byte value to a MIC string.
{
{ The undefined value ($FF) converts to a blank string.
{
{ Caution: invalad values are not detected and may display as greater than.
{----------------------------------------------------------------------------}

procedure TResults.ByteToMIC( mic: PChar; bVal: byte );
var
  pstr: array [ 0..RESmaxStrResult ] of char;
begin
  StrCopy( mic, '' );

  { Special case }
  if bVal = $FF then
    Exit;

  { Assert: not ( MB_LESS_THAN and MB_GREATER_THAN ) }

  { First part: Greater than/less than symbol }
  if bVal and MB_GREATER_THAN <> 0 then
    StrCat( mic, '>' )
  else if bVal and MB_LESS_THAN <> 0 then
    StrCat( mic, '<' )
  else
    StrCat( mic, ' ' );

  { Second part: get MIC index into master dil array. The index is obtained
    using the MB_INDEX mask. }
  StrPCopy( pstr, traysObj^.gMda[ bVal and MB_INDEX ] );
  TrimAll( pstr, pstr, ' ', RESmaxStrResult );
  StrLCat( mic, pstr, RESmaxStrResult );
end;



{----------------------------------------------------------------------------}
{ This procedure converts a MIC string to a byte value. It assumes the format
{ of the string is that produced by the ByteToMIC procedure.
{
{ Blank strings and strings containing values not found in the table are
{ converted to the undefined value ($FF).
{
{ Note, this procedure is sensitive to the format of the array and contains a
{ couple of assertions.
{
{ Why is this not a function?
{----------------------------------------------------------------------------}

procedure TResults.MICToByte( mic: PChar; var bVal: byte );
var
  tstr: string[ RESmaxStrResult ];
  mstr: string[ RESmaxStrResult ];
  found: boolean;
  k: integer;
begin

  { Blank string case }
  bVal := $FF;
  TrimAll( mic, mic, ' ', RESmaxStrResult ); { Warning: how can a PChar string have fixed length: RESmaxStrResult}
  if StrLen( mic ) = 0 then
    Exit;

  { Encode greater/less than bits and set up string for next step }
  tstr := StrPas( mic );
  if tstr[1] = '<' then
  begin
    bVal := MB_LESS_THAN;
    tstr[1] := ' ';
  end
  else if tstr[1] = '>' then
  begin
    bVal := MB_GREATER_THAN;
    tstr[1] := ' ';
  end
  else
  begin
    bVal := 0;
    tstr := ' ' + tstr;
  end;


  { For search, pad to correct length }
  while Length( tstr ) < Length( traysObj^.gMda[1] ) do
    tstr := tstr + ' ';

  { Search table for match, encode index if found. }
  found := FALSE;
  k := 0;
  while not found and ( k < MaxMDAEntries ) do
  begin
    mstr := traysObj^.gMda[k];
    if mstr = tstr then
    begin
      { Assert: (k and MB_MASK) = k }
      bVal := bVal or k;
      found := TRUE;
    end;
    Inc( k );
  end;

  { Assert: not ( MB_LESS_THAN and MB_GREATER_THAN ) }

  { If not found, encode byte as undefined }
  if not found then
    bVal := $FF;
end;



{----------------------------------------------------------------------------}
{----------------------------------------------------------------------------}

procedure TResults.UnPackResults( resultStr, pTestCatID: PChar; pos,
  resultLen: integer; var flag: word );
var
  num   : byte;
  ci    : boolean;
begin
  flag := 0;
  if StrComp( pTestCatID, 'MIC' ) = 0 then
  begin

    { Unpack as MIC byte }
    { Assert: 1 = SizeOf( Byte ) }
    Move( resBuf[pos], num, 1 );
    ByteToMIC( resultStr, num );
  end
  else if StrComp( pTestCatID, 'NCCLS' ) = 0 then
  begin

    { Unpack as NCCLS byte }
    ci := FALSE;
    { Assert: 1 = SizeOf( Byte ) }
    Move( resBuf[pos], num, 1 );
    InterpCodeToStr( num, resultStr, maxInterpStrLen, ci );
    if ci then
      SetBit( flag, REScontraIndicated );
  end
  else
  begin

    { Unpack as string }
    Move( resBuf[pos], resultStr^, resultLen );
    resultStr[resultLen] := #0;
  end;
end;



{----------------------------------------------------------------------------}
{ This procedure packs a result into resBuf.
{----------------------------------------------------------------------------}

procedure TResults.PackResults( p: PResRecObj );
var
  num: byte;
begin
  {Ignore tests not associated to an order}
  if StrComp( p^.testgroupID, NullTestGroupID ) = 0 then
    exit;

  { Pack }
  if StrComp( p^.testCatID, 'MIC' ) = 0 then
  begin

    { As MIC }
    MICToByte( p^.result, num );
    { Assert: 1 = SizeOf( Byte ) }
    Move( num, resBuf[p^.pos], 1 );
  end
  else if StrComp( p^.testCatID, 'NCCLS' ) = 0 then
  begin

    { As NCCLS }
    num := byte( InterpStrToCode( p^.result, TestBit( p^.flag,
      REScontraIndicated ) ) );
    { Assert: 1 = SizeOf( Byte ) }
    Move( num, resBuf[p^.pos], 1 );
  end
  else
  begin

    { As string }
    Pad( p^.result, p^.result, #0, p^.maxResLen );
    Move( p^.result, resBuf[p^.pos], p^.maxResLen );
  end;
end;



{----------------------------------------------------------------------------}
{----------------------------------------------------------------------------}

function TResults.ESBLPointer: PResRecObj;

  {--------------------------------------------------------------------------}
  {--------------------------------------------------------------------------}
  function isESBLTest( TestPointer: PResRecObj ): Boolean; far;
  begin
    isESBLTest := StrComp( TestPointer^.TestID, ESBLTest ) = 0;
  end;

begin
   ESBLPointer := FirstThat( @isESBLTest );
end;



{----------------------------------------------------------------------------}
{----------------------------------------------------------------------------}

function TResults.ESBLResultExists: boolean;
begin
  ESBLResultExists := ( ESBLObj <> Nil ) and
    ( StrComp( ESBLObj^.Result, '' ) <> 0 );
end;



{----------------------------------------------------------------------------}
{----------------------------------------------------------------------------}

function TResults.SetESBLResult( res: PChar; overwrite: boolean ): boolean;
begin
  if ESBLObj <> Nil then
  begin
    SetESBLResult := True;
    if overwrite then
      ESBLObj^.SetResult( res )
  end
  else
    SetESBLResult := False;
end;



{----------------------------------------------------------------------------}
{----------------------------------------------------------------------------}

function TResults.GetESBLResult( res: PChar; maxChars: integer ): boolean;
begin
  GetESBLResult := True;
  if ESBLObj <> Nil then
    ESBLObj^.GetResult( res, maxChars )
  else
  begin
    GetESBLResult := False;
    StrCopy( res, '' );
  end;
end;



{----------------------------------------------------------------------------}
{----------------------------------------------------------------------------}

Procedure TResults.ReInsertESBLObj;
begin
  if ( ESBLObj <> Nil ) and ( IndexOf( ESBLObj ) = -1 ) then
    Insert( ESBLObj );
end;



{----------------------------------------------------------------------------}
{----------------------------------------------------------------------------}

Procedure TResults.RemoveESBLObj;
begin
  if ( ESBLObj <> Nil ) and ( IndexOf( ESBLObj ) > -1 ) then
  begin
    ESBLObj^.SetResult( '' );
    Delete( ESBLObj );
  end;
end;



{----------------------------------------------------------------------------}
{----------------------------------------------------------------------------}
function  TResults.ESBLModified: Boolean;
begin
  ESBLModified:=(ESBLObj <> Nil) and (ESBLObj^.Modified);
end;



{----------------------------------------------------------------------------}
{----------------------------------------------------------------------------}

function TResults.ESBLChanged( Organism: Integer ): boolean;
var
  PrevESBLState: Boolean;
begin
  if ESBLObj <> Nil then
  begin
    PrevESBLState := ESBLState;
    ESBLState := ESBLCriterion( Organism, @Self );

    If ESBLState then
    begin
      If not PrevESBLState then
      begin
        ESBLObj^.SetResult( '?' );
        ReInsertESBLObj;
      end;
    end
    else
      RemoveESBLObj;

    ESBLChanged := PrevESBLState <> ESBLState;
  end
  else
    ESBLChanged := False;
end;



{----------------------------------------------------------------------------}
{----------------------------------------------------------------------------}

function TResults.BuildAddResObj( TstSeqRef, ISOSeqRef: TSeqNum ): PResRecObj;
const
  UserDefined = True;
  NullResStr = '';
  NullPos = -1;
  NullFlag = 0;
  SingleChar = 1;
var
  NewResRecObj: PResRecObj;
  testDB,
  drugDB: PDBFile;
  TestID,
  TestCat,
  TestName: array [ 0..64 ] of char;
  DrugSeq: TSeqNum;

begin
  {Get needed fields for the new AddDrug entry}
  testDB:= New( PDBFile, Init( DBTSTFile, '', dbOpenNormal ) );
  drugDB:= New( PDBFile, Init( DBDrugFile, '', dbOpenNormal ) );

  {Find the test}
  testDB^.dbr^.ClearRecord;
  testDB^.dbc^.GetSeq( testDB^.dbr, TstSeqRef );

  {Load needed data}
  testDB^.dbr^.GetFieldAsStr( DBTSTID, TestID, 64 );
  testDB^.dbr^.GetFieldAsStr( DBTSTTstCat, TestCat, 64 );
  testDB^.dbr^.GetField( DBTSTDrugRef, @DrugSeq, SizeOf( DrugSeq ) );
  drugDB^.dbr^.ClearRecord;
  drugDB^.dbc^.GetSeq( drugDB^.dbr, DrugSeq );
  drugDB^.dbr^.GetFieldAsStr( DBTSTName, TestName, 64 );
  Dispose( testDB,Done );
  Dispose( drugDB,Done );

  {Create the Additional result object and return it}
  NewResRecObj := New( PResRecObj, Init( NullTestGroupID, UserDefined, TestID,
    TestCat, TestName, ISOSeqRef, TstSeqRef, DrugSeq, NullResStr, NullPos,
    SingleChar, NullFlag ) );

  NewResRecObj^.Modified := True;

  BuildAddResObj := NewResRecObj;
end;



{----------------------------------------------------------------------------}
{----------------------------------------------------------------------------}

procedure TResults.LoadAltResults( ISOSeqRef: TSeqNum );
var
  AltResDB: pdbfile;
  TstSeq,
  CurrISO: TSeqNum;
  MoreAlts: boolean;
  ResStr: Array[ 0..64 ] of char;
  TheAltRes: PResRecObj;
begin
  AltResDB := New( PDBFile, Init( DBAltTstFile, '', dbOpenNormal ) );
  if AltResDB <> Nil then
  begin
    AltResDB^.dbr^.ClearRecord;
    AltResDB^.dbr^.PutField( DBAltTstIsoRef, @ISOSeqRef );

    MoreAlts := AltResDB^.dbc^.GetFirstContains( AltResDB^.dbr );
    While MoreAlts do
    begin
      AltResDB^.dbr^.GetField( DBAltTstIsoRef, @CurrISO, SizeOf( TSeqNum ) );
      AltResDB^.dbr^.GetField( DBAltTstTstRef, @TstSeq,  SizeOf( TSeqNum ) );
      AltResDB^.dbr^.GetField( DBAltTstRes,    @ResStr,  64 );
      TheAltRes := BuildAddResObj( TstSeq, ISOSeqRef );
      TheAltRes^.SetResult( ResStr );
      TheAltRes^.ClearModify;
      Insert( TheAltRes );
      MoreAlts := AltResDB^.dbc^.GetNextContains( AltResDB^.dbr );
    end;

    Dispose( AltResDB, Done );
  end;
end;



{----------------------------------------------------------------------------}
{ This procedure saves the "alt" results.
{
{ Caution: if the file cannot be opened, the results are not saved and no
{ indication of this is provided.
{----------------------------------------------------------------------------}

procedure TResults.SaveAltResults;
var
  AltResDB: pdbfile;

  {--------------------------------------------------------------------------}
  {--------------------------------------------------------------------------}
  procedure SaveItem( Res: PResRecObj ); far;
  var
    UpdateRes: boolean;
  begin
    {Ignore tests associated to an order}
    if StrComp( Res^.testgroupID, NullTestGroupID ) = 0 then with AltResDB^ do
    begin
      dbr^.ClearRecord;
      dbr^.PutField( DBAltTstIsoRef, @Res^.IsordSeq );
      dbr^.PutField( DBAltTstTstRef, @Res^._testSeq );
      UpdateRes := dbc^.GetEQ( dbr );
      dbr^.PutField( DBAltTstRes, @Res^.Result );
      if UpdateRes then
        AltResDB^.dbc^.UpdateRec( dbr )
      else
        AltResDB^.dbc^.InsertRec( dbr );
    end;
  end;

begin { SaveAltResults }

  AltResDB := New( PDBFile, Init( DBAltTstFile, '', dbOpenNormal ) );
  Assert( AltResDB <> Nil, 14 );
  ForEach( @SaveItem );
  Dispose( AltResDB, Done );
end;



{----------------------------------------------------------------------------}
{ Unit initialization
{----------------------------------------------------------------------------}

var
  desc: PDBReadDesc;
  fi: TFldInfo;

BEGIN
  FieldSizesNeedValadation := TRUE;
  desc := New( PDBReadDesc, Init( DBRESFile, '' ) );

  Assert( desc <> nil, 15 );
  desc^.FieldInfo(  DBRESBlock, fi );
  resFieldSize := fi.fldSize;
  MSDisposeObj( desc );
END.

