unit DMSDebug;

INTERFACE

IMPLEMENTATION

{$IFDEF DMSDEBUG}
uses
  Strings,
  WinDos,
  BBError,
  ObjMemory,
  PMD,
  MemCheck;

var
  exitsave  : pointer;

procedure ExitRoutine; far;
begin
  exitproc:= exitsave;
  memcheckreport;
end;

procedure InitCurDir;
var
  p1, p2  : array[0..100] of char;
  p       : PChar;
  s       : string;
begin
  StrPCopy(p1, paramStr(0));
  p:= StrRScan(p1, '\');
  if p <> nil then
  begin
    p^:= #0;
    SetCurDir(p1);
  end;
end;

BEGIN
  exitsave:= exitproc;
  exitproc:= @ExitRoutine;
  InitCurDir;
  InitBBError('DEBUG.LOG', false);
  InitObjMemory;
  InitPMD(dfStandard);
{$ENDIF}
END.
