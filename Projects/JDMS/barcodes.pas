{----------------------------------------------------------------------------}
{  Module Name  : BARCODES.PAS                                               }
{  Programmer   : EJ                                                         }
{  Date Created : 06/24/95                                                   }
{                                                                            }
{  Purpose -                                                                 }
{  This module provides the barcode printing functions and user              }
{  interface.                                                                }
{                                                                            }
{  Assumptions -                                                             }
{  None.                                                                     }
{                                                                            }
{  Revision History -                                                        }
{                                                                            }
{  Version  Date      Who    Did What                                        }
{  -------  --------  ------ ----------------------------------------------- }
{  1.00     06/24/95  EJ     Initial release.                                }
{                                                                            }
{----------------------------------------------------------------------------}

unit Barcodes;

INTERFACE

uses
  OWindows,
  ListLib,
  Trays,
  DBTypes,
  DBFile,
  ResLib,
  TPDate,
  WAGpib,
  Screens;

{$I BARCODES.INC}


procedure PrintBarcodes(aParent: PWindowsObject; aDlgName: PChar;
                        aSession, selectedIsoSeq: TSeqNum; aTray: PTraysObject;
                        aListObj: PListObject; dbIso, dbWap: PDBFile);

function  GetNextPanelID: integer;

procedure StampPanelID(PId: Word);
function  ValidResultSet(PId: Word; ReadDate: TimeDate): Boolean;

IMPLEMENTATION

uses
  Objects,
  ODialogs,
  OPrinter,
  WinTypes,
  WinProcs,
  Strings,
  DMString,
  Bits,
  MScan,
  APITools,
  DlgLib,
  CtlLib,
  DBIDs,
  DBLib,
  DMSErr,
  DMSDebug,
  WinDOS;

{$R BARCODES.RES}

const
  {Win.ini's Section and 'Generic/Text' Driver name}
  DMSSection   = 'DMS';
  DevSection   = 'Devices';
  GenericName  = 'GEN';
  CrLf         = #13#10;

  {File for keeping track of newly assigned PIDs and their dates}
  PIdDatesfname= 'PIdDates.dat';
  MaxWord      = $FFFF;

  {C. Itoh/ Printronix const}
  ESC          = #27;
  black        = 255;
  white        = 0;
  whitenarrow  = 3;
  whitewide    = 7;
  blacknarrow  = 2;
  blackwide    = 6;
  interchar    = 10;

  BarCodeLen          = 20;
  MaxLines            = 60;
  MaxBitMapSize       = 1000;
  wm_BarcodeError     = 373;

  CodeTable    : array [0..10] of integer
               = ($34, $121, $61, $160, $31, $130,
                  $70, $25, $124, $64, $94);


  appName  : PChar = 'DMS';
  profFile : PChar = 'DMS.INI';

Type
{ TBarCodePrinter is a simplistic representation of a BarCode printer, that   }
{ is capable of retrieving printer information for a given device name.       }
{ To print the actual barcode, it creates a TBarCode label and executes its   }
{ printpage procedure (calling TPrinter's Print method).                      }

  pBarCodePrinter=^TBarCodePrinter;
  TBarCodePrinter=
  object(TPrinter)
    constructor Init;

    procedure PrintDadeBarCode(aParent : pWindowsObject;
                               aTitle  : pChar;
                               aBarCodeStr,
                               aPatientStr, aSpecimenStr,
                               aIsolateStr, aPanelTypeStr: String);

    procedure PrintEpsonBarCode(aParent : pWindowsObject;
                                aTitle  : pChar;
                                aBarCodeStr,
                                aPatientStr, aSpecimenStr,
                                aIsolateStr, aPanelTypeStr: String);
    private
      AbortPrinting: Boolean;
  end;


Type
{ TBarCode provides logic to send passthrough data through any printer driver.}

  pBarCode=^TBarCode;
  TBarCode=
  object(TPrintOut)
    Constructor Init(aTitle: pChar;
                     aBarCodeStr,
                     aPatientStr, aSpecimenStr,
                     aIsolateStr, aPanelTypeStr: String);
    procedure Print(PrnDC: hDC); virtual;
  private
    BarCodeStr,
    PatientStr, SpecimenStr,
    IsolateStr, PanelTypeStr: String;

    procedure SendStr(aStr: String);
    procedure SendRaw(Data: Pointer; DataSize: Word);
    procedure PrintLabel; virtual;
  end;

{ TDadeBarCode label is a TBarCode that uses Blaster command sequences.       }

  pDadeBarCode=^TDadeBarCode;
  TDadeBarCode=
  object(TBarCode)
    procedure PrintLabel; virtual;
  private
    procedure ListLn(aStr: String);
  end;

{ TEpsonBarCode label is a TBarCode that uses Epson 850 ESC codes.}

  BarCodeBitMap=array [1..MaxBitMapSize] of char;
  pEpsonBarCode=^TEpsonBarCode;
  TEpsonBarCode=
  object(TBarCode)
    procedure PrintLabel; virtual;
  private
    CharCnt,
    highChar,
    lowChar     : Integer;
    BCodeBitMap : ^BarCodeBitMap;

    procedure LineFeed;
    procedure SetSpacing;

    procedure BuildBitMap;
    procedure PrintBitMap;
    procedure FirstLine;
    procedure SecondLine;
    procedure ThirdLine;
  end;

Type
  PBarcodeDlg = ^TBarcodeDlg;
  TBarcodeDlg = object(TCenterDlg)
    isoDB,
    wapDB              : PDBFile;
    isordDB            : PDBAssocFile;
    listObj            : PListObject;
    isoordSeq,
    sessRef,
    selSeq             : TSeqNum;
    optGrp             : PRadioGroup;
    cAS4               : PLCheckBox;
    BarCodePrinter     : pBarCodePrinter;
    printingInProgress : boolean;
    testCount          : integer;
    trayObj            : PTraysObject;
    testing,
    as4BCs,
    selectedOnly,
    alreadyPrinted,
    inSetUp,
    validID            : boolean;
    panelNum           : integer;
    panelID,
    familyStr,
    testgrp,
    specimen,
    isolate,
    patient            : string;
    barcode            : string[BarCodeLen];
    {}
    constructor Init(aParent: PWindowsObject; aName: PChar;
                     aSession, selectedIsoSeq: TSeqNum; aTray: PTraysObject;
                     aListObj: PListObject; dbIso, dbWap: PDBFile);
    destructor Done; virtual;
    procedure SetUpWindow; virtual;
    procedure Cancel(var msg: TMessage); virtual id_First + IDCANCEL;
    procedure AS4Barcodes(var msg: TMessage); virtual id_First + IDC_AS4_BARCODES;
    procedure PrintBarcodes(var msg: TMessage); virtual id_First + BTN_PRINTBC;
    procedure PrintPanelBarcodes;
    procedure PrintTestBarcodes;
    function  GetData: boolean;
    function  FirstPanel: boolean;
    function  NextPanel: boolean;
    procedure DadePrintIt; {Prints barcodes on the Dade barcode printer}
    procedure PrintIt;
    procedure BarcodeError(var msg: TMessage); virtual wm_First + wm_BarcodeError;
    procedure EnableButtons; virtual;
    function  IsDadeBarcodePtr : Boolean; {Detects the selection of a Dade Barcode Printer}
  end;


const
  MaxPanelID      = 1000;

var
  {temporary VAR}
  nextPanelID : integer;


procedure FormatPanelID(var panelID: string);
begin
  case Ord(panelID[0]) of
    0 : panelID := Concat('000', panelID);
    1 : panelID := Concat('00', panelID);
    2 : panelID := Concat('0', panelID);
  end;
end;

function GetNextPanelID: integer;
var
  s    : array [0..10] of char;
  code : integer;
begin
  GetPrivateProfileString(appName, 'WAPANELID', '1', s, 10, profFile);
  Val(s, nextPanelID, code);
  if nextPanelID = MaxPanelID then
    nextPanelID := 1;   {Reset to 1 when it reaches the Max- rab 4/27/93}
  GetNextPanelID := nextPanelID;    {moved down from above IF - rab 8/27/93}
  Inc(nextPanelID);
  Str(nextPanelID, s);
  WritePrivateProfileString(appName, 'WAPANELID', s, profFile);
end;

procedure FormatPanelType(var pTyp: string);
begin
  case Ord(pTyp[0]) of
    0 : pTyp := Concat('00', pTyp);
    1 : pTyp := Concat('0', pTyp);
  end;
end;

function GetNewPanelID(isordSeq: TSeqNum; wapDB: PDBFile;
                       var panelID: string; WantOldID: boolean;
                       var oldPanelIDOK: boolean): boolean;
var
  pstr           : array [0..5] of char;
  flag           : boolean;
  pID            : integer;
  i              : integer;
begin
  GetNewPanelID := FALSE;
  oldPanelIDOK := FALSE;
  repeat  {- "repeat once" loop allows this function to have one exit point }
    wapDB^.dbr^.ClearRecord;
    wapDB^.dbc^.SetCurKeyNum(DBWAPNL_IsoOrd_KEY);
    wapDB^.dbr^.PutField(DBWAPNLIsoOrdRef, @isordSeq);
    oldPanelIDOK := wapDB^.dbc^.GetEQ(wapDB^.dbr);
    wapDB^.dbc^.SetCurKeyNum(DBWAPNL_ID_KEY);

    if not WantOldID and oldPanelIDOK then
      Break;  {- to end of "repeat" loop }

    if oldPanelIDOK then
    begin
      wapDB^.dbr^.GetFieldAsStr(DBWAPNLID, pstr, SizeOf(pstr));
      panelID := StrPas(pstr);
    end
    else
    begin
      for i := 1 to MaxPanelID do  {- limit infinite loop condition }
      begin
        pID := GetNextPanelID;
        Str(pID, panelID);
        wapDB^.dbr^.PutField(DBWAPNLID, @pID);

        if wapDB^.dbc^.GetEQ(wapDB^.dbr) then
        begin
          if wapDB^.dbr^.GetField(DBWAPNLUsedFlag, @flag, SizeOf(flag)) and
             not flag then
          begin
            wapDB^.dbr^.PutField(DBWAPNLIsoOrdRef, @isordSeq);
            flag := TRUE;
            wapDB^.dbr^.PutField(DBWAPNLUsedFlag, @flag);
            flag := FALSE;
            wapDB^.dbr^.PutField(DBWAPNLBCPrinted, @flag);
            wapDB^.dbr^.PutField(DBWAPNLKnownToWA, @flag);
            GetNewPanelID := wapDB^.dbc^.UpdateRec(wapDB^.dbr);
            StampPanelID(PId);
            Break;  {- to end of "for" loop }
          end;
        end
        else
        begin
          wapDB^.dbr^.PutField(DBWAPNLIsoOrdRef, @isordSeq);
          flag := TRUE;
          wapDB^.dbr^.PutField(DBWAPNLUsedFlag, @flag);
          flag := FALSE;
          wapDB^.dbr^.PutField(DBWAPNLBCPrinted, @flag);
          wapDB^.dbr^.PutField(DBWAPNLKnownToWA, @flag);
          GetNewPanelID := wapDB^.dbc^.InsertRec(wapDB^.dbr) and
                           wapDB^.dbc^.GetEQ(wapDB^.dbr);
          StampPanelID(PId);
          Break;  {- to end of "for" loop }
        end;
      end;  {- for }
    end;
  until TRUE;  {- do once }
end;

{3:56pm 5/28/98}
Function ConvertClass
{===================================================================}
{  Change class value to convey OxMRSA or VaStS hold status to PDL  }
{===================================================================}
  (CurrentClass    : Integer;  {Unmodified class}
   PanelNum        : Integer;  {Current Panel}
   dbr             : PDBRec)   {Pointer to results}
  : Integer;
Const
  StrepClass           = 1;  {System defined strep}
  StaphClass           = 2;  {System defined staph}
  VaStSHoldClass       = 3;  {Indicates to PDL to hold strep based on Va/StS}
  VaOnlyHoldClass      = 5;  {Indicates to PDL to hold strep based on Va only}
  OxMultResHoldClass   = 4;  {Indicates to PDL to hold staph based on multiple resistance}
  PosID1ProbSet        = 5;  {Probability set for PID1}
  PosID2ProbSet        = 2;  {Probability set for PID2}
Var
  results              : PResults;              {Points to a result record}
  res                  : array[0..32] of char;  {Returned extra test values}
  IsPanelConv          : Boolean;               {Panel check}
  IsPanelGramPos       : boolean;               {Panel check}
  IsBetaHemNeg         : boolean;               {Result check}
  PanelNumStr          : string;                {String representation of panel number}
  PanelProbSet         : Integer;               {Probability set of the panel}
  VaStSHoldEnabled     : Boolean;               {Hold status}
  VaOnlyHoldEnabled    : Boolean;               {Hold status}
  OxMultResHoldEnabled : Boolean;               {Hold status}
Begin
  {Get the panel record from the database}
  results := New(PResults, Init);
  results^.traysObj^.LoadTrayByNum(PanelNum+1); {offset panel for tray database}
  results^.LoadResults(dbr);

  {Get some facts about the panel}
  IsPanelGramPos       := (results^.TraysObj^.TFSRec.PSet=PosID1ProbSet) or
                          (results^.TraysObj^.TFSRec.PSet=PosID2ProbSet);
  IsPanelConv          := (not results^.TraysObj^.Trayflo);
  VaStSHoldEnabled     := ScreenOn('VaStSHold');
  VaOnlyHoldEnabled    := ScreenOn('VaOnlyHold');
  OxMultResHoldEnabled := ScreenOn('OxMultResHold');

  {Set the default class value}
  ConvertClass := CurrentClass;

  {Determine if the class must change}
  If ( IsPanelConv and IsPanelGramPos ) then
  Begin
    if (CurrentClass=StrepClass) then
    Begin
      {Determine the beta-hemolysis result}
      IsBetaHemNeg := TRUE;
      If results^.GetResult('.HEM', 'HEM', res, 32) then
        If StrComp(res, '') <> 0 then
          If StrComp(res, '+') = 0 then
            IsBetaHemNeg := FALSE;
      If ( IsBetaHemNeg and VaOnlyHoldEnabled ) Then
        If ( VaStSHoldEnabled ) Then
          ConvertClass := VaStSHoldClass
        Else
          ConvertClass := VaOnlyHoldClass;
    End
    Else
      If ( CurrentClass=StaphClass ) and OxMultResHoldEnabled Then
        ConvertClass := OxMultResHoldClass;
  End;
  MSDisposeObj(results);
End;


{ Utility functions ----------------------------------------------------------}

{ Parses device info from a device entry in the Win.ini file.}
  procedure ParseDeviceInfo(aDeviceStr, aDriver, aPort: pChar);
  var Comma: Integer;
  begin
    {Device Info is in this form DeviceDriver,DevicePort}
    {Find the comma, and extract the Driver and Port strings}
    Comma:=0;
    While not (aDeviceStr[Comma] in [#0,',']) do inc(Comma);
    StrLCopy(aDriver,aDeviceStr,Comma);
    StrCopy(aPort,@aDeviceStr[Comma+1]);
  end;

{ Returns the first Device that uses the Generic/Text driver, or Nil.}
  procedure GenericDevice(Device, Driver, Port: pChar);
  var DeviceList,
      DeviceInfo : array [0..255] of char;
      GenericText: Boolean;
      NextDevice : Integer;
  begin
    {Get a list of installed devices}
    NextDevice:=0;
    GenericText:=False;
    GetProfileString(DevSection, NIL, NIL, DeviceList, 255);

    {Parse the list and inspect each device}
    While (StrLen(@DeviceList[NextDevice])>0) and (not GenericText) do
    begin
      StrCopy(Device,@DeviceList[NextDevice]);
      Inc(NextDevice, StrLen(Device)+1);
      if GetProfileString(DevSection, Device, NIL, DeviceInfo, 64) > 0 then
      begin
        ParseDeviceInfo(DeviceInfo, Driver, Port);
        GenericText:=StrPos(Driver,GenericName)<>Nil;
      end;
    end;

    {Return the first device that uses a Generic/Text driver, or ''}
    if not GenericText then StrCopy(Device,'');
  end;

{ Methods for TBarcodeDlg }

constructor TBarcodeDlg.Init(aParent: PWindowsObject; aName: PChar;
                             aSession, selectedIsoSeq: TSeqNum; aTray: PTraysObject;
                             aListObj: PListObject; dbIso, dbWap: PDBFile);
begin
  isordDB := nil;
  BarCodePrinter:= nil;
  if not inherited Init(aParent, aName) then
  begin
    Done;
    Fail;
  end;
  if (aTray = nil) or (aListObj = nil) or (dbIso = nil) or (dbWap = nil) then
  begin
    Done;
    Fail;
  end;
  sessRef := aSession;
  selSeq := selectedIsoSeq;
  trayObj := aTray;
  listObj := aListObj;
  isoDB := dbIso;
  wapDB := dbWap;
  printingInProgress := FALSE;
  validID := FALSE;
  alreadyPrinted := FALSE;
  testing := FALSE;
  as4BCs := FALSE;

  optGrp := New(PRadioGroup, Init(@self, IDC_PRINTNEWBARCODES, IDC_PRINTNEWBARCODES, FALSE));
  optGrp^.AddToGroup(IDC_PRINTALLBARCODES, IDC_PRINTALLBARCODES);
  optGrp^.AddToGroup(IDC_PRINTTESTBARCODES, IDC_PRINTTESTBARCODES);
  optGrp^.AddToGroup(IDC_PRINTBARCODE, IDC_PRINTBARCODE);
  cAS4 := New(PLCheckBox, InitResource(@self, IDC_AS4_BARCODES, FALSE));
end;

destructor TBarcodeDlg.Done;
begin
  MSDisposeObj(isordDB);
  MSDisposeObj(BarCodePrinter);

  inherited Done;
end;

function TBarcodeDlg.IsDadeBarcodePtr : Boolean;
{===================================================================}
{  Checks barcode printer selection                                 }
{===================================================================}
Const
  NCItohPrntnxPtr = 'CItohPrntnx';         {Printer name in DMS.INI}
  NDadeBarcodePtr = 'DadeBarcode';         {Printer name in DMS.INI}
  pkBarcodePtr    = 'BarcodePrinter';      {Printer name in DMS.INI}
var
  PrinterAssigned : array[0..20] of char;  {Returned printer selection}
begin
  {Default to not Dade barcode printer (CItoh/Printronix)}
  IsDadeBarcodePtr := FALSE;

  {Get the printer selection and check it}
  GetPrivateProfileString(DMSSection, pkBarcodePtr, 'NONE', PrinterAssigned, sizeof(PrinterAssigned)-1, profFile);
  If ( Strcomp(NDadeBarcodePtr,PrinterAssigned) = 0 ) Then
    IsDadeBarcodePtr := TRUE
end;

procedure TBarcodeDlg.SetUpWindow;
var
  hAS4 : HWnd;
  Device,
  Driver,
  Port : array [0..255] of char;

begin
  inherited SetUpWindow;
  inSetUp := TRUE;

  hAS4 := GetItemHandle(IDC_AS4_BARCODES);
  if hAS4 = 0 then
    MSDisposeObj(cAS4);

  if selSeq = 0 then
  begin
    optGrp^.SetSelected(IDC_PRINTNEWBARCODES);
    optGrp^.Enable(IDC_PRINTBARCODE, FALSE);
  end
  else
  begin
    optGrp^.SetSelected(IDC_PRINTBARCODE);
  end;

  { Get printer if available }
  GenericDevice(Device,Driver,Port);
  if StrComp(Device,'')=0 then
  begin                                            { Could not attach to printer }
    ShowError(parent, IDS_BC_PRINTER_ERR, nil, 0, 0, 0);
  end
  else
  begin
    BarCodePrinter := New(pBarCodePrinter, Init);
    BarCodePrinter^.SetDevice(Device,Driver,Port);
    ValidID:=True;
  end;
  inSetUp := FALSE;
  if validID then
    EnableButtons
  else
    EndDlg(IDCANCEL);
end;

procedure TBarcodeDlg.Cancel(var msg: TMessage);
var
  temp : array[0..64] of char;
  pstr : array[0..50] of char;
begin
  if printingInProgress then
  begin
    SR(IDS_BC_STOP_PROMPT, temp, sizeof(temp)-1);
    SR(IDS_CONFIRM, pstr, sizeof(pstr)-1);
    if YesNoMsg(HWindow, pstr, temp) then
      BarCodePrinter^.abortPrinting := TRUE;
  end
  else
    BarCodePrinter^.abortPrinting := TRUE;

  if BarCodePrinter^.abortPrinting then
    inherited Cancel(msg);
end;

procedure TBarcodeDlg.AS4Barcodes(var msg: TMessage);
begin
  if msg.lParamHi = BN_CLICKED then
  begin
    as4BCs := cAS4^.GetCheck = bf_Checked;
    if as4BCs then
      if optGrp^.SelectedVal = IDC_PRINTNEWBARCODES then
        optGrp^.SetSelected(IDC_PRINTALLBARCODES);
    EnableWindow(GetItemHandle(IDC_PRINTNEWBARCODES), not as4BCs);
  end;
  EnableButtons;
  DefWndProc(msg);
end;

procedure TBarcodeDlg.PrintBarcodes(var msg: TMessage);
begin
  alreadyPrinted := FALSE;
  selectedOnly := FALSE;
  testing := FALSE;
  BarCodePrinter^.abortPrinting := FALSE;
  PrintingInProgress := TRUE;
  EnableButtons;
  FocusCtl(HWindow, IDCANCEL);

  case optGrp^.SelectedVal of
    IDC_PRINTNEWBARCODES :
      begin
        PrintPanelBarcodes;
      end;
    IDC_PRINTALLBARCODES :
      begin
        alreadyPrinted := TRUE;
        PrintPanelBarcodes;
      end;
    IDC_PRINTTESTBARCODES :
      begin
        testing := TRUE;
        PrintTestBarcodes;
      end;
    IDC_PRINTBARCODE :
      begin
        selectedOnly := TRUE;
        alreadyPrinted := TRUE;
        PrintPanelBarcodes;
      end;
  end;
  printingInProgress := FALSE;
  EnableButtons;
  FocusCtl(HWindow, BTN_PRINTBC);
end;

procedure TBarcodeDlg.PrintPanelBarcodes;
{*****************************************************************************
  Print bar code
*****************************************************************************}
begin
  if FirstPanel then
  begin
    repeat
      if GetData then
        If IsDadeBarcodePtr Then
          DadePrintIt     {Print barcodes on a Dade barcode printer}
        Else
          PrintIt;        {Print barcodes on a CItoh/Printronix printer}
    until not NextPanel;
  end;
end;  {printbar}

procedure TBarcodeDlg.PrintTestBarcodes;
var
  size : byte;
begin
  testCount := 1;
  size := 3;
  FillChar(isolate[1], size, 'X');
  isolate[0] := Chr(size);
  size := 8;
  FillChar(specimen[1], size, 'X');
  specimen[0] := Chr(size);
  size := 12;
  FillChar(patient[1], size, 'X');
  patient[0] := Chr(size);
  size := 8;
  FillChar(testgrp[1], size, 'X');
  testgrp[0] := Chr(size);
  {** print a dummy barcode **}
  barCode := '*0000000*';

  repeat
    If IsDadeBarcodePtr Then
      DadePrintIt   {Print barcodes on a Dade barcode printer}
    Else
      PrintIt;      {Print barcodes on a CItoh/Printronix printer}
  until not NextPanel;
end;  {printbar}

function TBarcodeDlg.GetData: boolean;
var
  seq    : TSeqNum;
  dbr    : PDBRec;
  oldIDOK,
  udFlag : boolean;
  setFam : word;
  pstr   : array [0..40] of char;
  len,
  size   : byte;
  i,
  checknum,
  panelTyp,
  famNum : integer;
  ptypStr,
  check  : string;
begin
  {** set up the barcode string **}
  GetData := FALSE;
  if isoordSeq = 0 then
    Exit;
  isordDB^.dbr^.GetField(DBISOORDTstGrpRef, @seq, SizeOf(TSeqNum));
  if seq = 0 then
    Exit;
  dbr := listObj^.PeekSeq(DBTSTGRPFile, seq); { Read-only return value }
  if dbr = nil then
    Exit;
  dbr^.GetField(DBTSTGRPUDFlag, @udFlag, SizeOf(boolean));
  if udFlag then
    Exit;
  dbr^.GetFieldAsStr(DBTSTGRPID, pstr, 40);
  testgrp := StrPas(pstr);
  { Map the test group id into a known panel number }
  { Tray's panel list is 1-based,
    but the W/A panel list is 0-based,
    so subtract 1 from the function result }
  panelNum := trayObj^.MapTestGroupID(pstr)-1;

  isoDB^.dbr^.GetFieldAsStr(DBISOIso, pstr, 40);
  isolate := StrPas(pstr);
  isoDB^.dbr^.GetField(DBISOSetFamily, @setFam, sizeof(setFam));
  famNum := ExtractFamily(setFam);

  {Convert organism class to support the MRSA/VRE holds, JDMS v4.00, ghg 5/28/98}
  FamNum := ConvertClass
    ( FamNum,                  {CurrentClass   : Integer}
      PanelNum,                {PanelNum : Integer}
      isoDB^.dbr );            {dbr : PDBRec}

  Str(famNum, pstr);
  familyStr := StrPas(pstr);
  isoDB^.dbr^.GetField(DBISOSpecRef, @seq, SizeOf(TSeqNum));
  if seq = 0 then
    Exit;
  dbr := listObj^.PeekSeq(DBSPECFile, seq); { Read-only return value }
  if dbr = nil then
    Exit;
  dbr^.GetFieldAsStr(DBSPECSpecID, pstr, 40);
  specimen := StrPas(pstr);
  patient := '';
  dbr^.GetField(DBSPECPatRef, @seq, SizeOf(TSeqNum));
  if seq <> 0 then
  begin
    dbr := listObj^.PeekSeq(DBPATFile, seq); { Read-only return value }
    if dbr <> nil then
    begin
      dbr^.GetFieldAsStr(DBPATPatID, pstr, 40);
      patient := StrPas(pstr);
    end;
  end;
  len := Ord(isolate[0]);
  size := 3;
  FillChar(isolate[len+1], size-len, Chr(32));
  isolate[0] := Chr(size);
  len := Ord(specimen[0]);
  size := 8;
  FillChar(specimen[len+1], size-len, Chr(32));
  specimen[0] := Chr(size);
  len := Ord(patient[0]);
  size := 12;
  FillChar(patient[len+1], size-len, Chr(32));
  patient[0] := Chr(size);
  panelID := '0';
  if not (as4BCs or
          GetNewPanelID(isoordSeq, wapDB, panelID, alreadyPrinted, oldIDOK) or
          (alreadyPrinted and oldIDOK)) then
    Exit;
  FormatPanelID(panelID);
  Str(panelNum, pTypStr);
  FormatPanelType(pTypStr);
  checkNum := 0;
  for i := 1 to 2 do
    checkNum := checkNum + (Ord(ptypStr[i]) - Ord('0'));
  for i := 1 to 3 do
    checkNum := checkNum + (Ord(panelID[i]) - Ord('0'));
  checkNum := checkNum + famNum;
  Str(checkNum MOD 10, check);

  barcode := Concat('*', check, ptypStr, familyStr, panelID, '*');
  GetData := TRUE;
end;

function TBarcodeDlg.FirstPanel: boolean;
var
  isOk      : boolean;
begin
  isoordSeq := 0;
  isoDB^.dbr^.ClearRecord;
  if selectedOnly then
    isOk := (selSeq <> 0) and isoDB^.dbc^.GetSeq(isoDB^.dbr, selSeq)
  else if sessRef = 0 then
    isOK := isoDB^.dbc^.GetFirst(isoDB^.dbr)
  else
  begin
    isoDB^.dbr^.ClearRecord;
    isoDB^.dbr^.PutField(DBISOSess, @sessRef);
    isOK := isoDB^.dbc^.GetFirstContains(isoDB^.dbr);
  end;
  if isOK then
  begin
    MSDisposeObj(isordDB);
    isordDB := New(PDBAssocFile, Init(isoDB^.dbr, DBISOLATE_ISORDER_ASSOC, dbOpenNormal));
    if (isordDB <> nil) and
       isordDB^.dbc^.GetFirst(isordDB^.dbr) then
      isoordSeq := isordDB^.dbr^.GetSeqValue;
  end;
  FirstPanel := isoordSeq <> 0;
end;

function TBarcodeDlg.NextPanel: boolean;
var
  isOk    : boolean;
begin
  NextPanel := FALSE;
  if BarCodePrinter^.abortPrinting then
  begin
    printingInProgress := FALSE;
    Exit;
  end;
  if testing then
  begin
    if testCount < 3 then
    begin
      Inc(testCount);
      NextPanel := TRUE;
    end;
  end
  else
  begin
    isoordSeq := 0;
    isOK := isordDB^.dbc^.GetNext(isordDB^.dbr);
    repeat
      if isOK then
      begin
        isoordSeq := isordDB^.dbr^.GetSeqValue;
        Break;
      end;

      if selectedOnly then
        Break;

      repeat
        if sessRef = 0 then
          isOK := isoDB^.dbc^.GetNext(isoDB^.dbr)
        else
          isOK := isoDB^.dbc^.GetNextContains(isoDB^.dbr);
        if not isOK then
          Break;

        MSDisposeObj(isordDB);
        isordDB := New(PDBAssocFile, Init(isoDB^.dbr, DBISOLATE_ISORDER_ASSOC, dbOpenNormal));
        isOK := isordDB^.dbc^.GetFirst(isordDB^.dbr);
      until isOK;
    until not isOK;

    NextPanel := isoordSeq <> 0;
  end;
end;

procedure TBarcodeDlg.DadePrintIt;
begin
  HandleEvents(hWindow);

  if not BarCodePrinter^.AbortPrinting then
     BarCodePrinter^.PrintDadeBarCode
     (@Self,'Nihongo BarCode',BarCode,Patient,Specimen,Isolate,TestGrp);
end;

procedure TBarcodeDlg.PrintIt;
begin
  HandleEvents(hWindow);
  if not BarCodePrinter^.AbortPrinting then
     BarCodePrinter^.PrintEpsonBarCode
     (@Self,'Nihongo BarCode',BarCode,Patient,Specimen,Isolate,TestGrp);
end;

procedure TBarcodeDlg.BarcodeError(var msg: TMessage);
var
  style  : integer;
  s      : array [0..63] of char;
  wStr   : array [0..16] of char;
begin
  SR(IDS_WARNING, wStr, 16);
  SR(msg.wParam, s, 63);
  if inSetup then
    style := mb_OK
  else
    style := mb_RetryCancel;
  msg.result := MessageBox(hWindow, s, wStr, style);
end;

procedure TBarcodeDlg.EnableButtons;
begin
  EnableWindow(GetItemHandle(BTN_PRINTBC), validID and not printingInProgress);
end;


{ exported procedure }

procedure PrintBarcodes(aParent: PWindowsObject; aDlgName: PChar;
                        aSession, selectedIsoSeq: TSeqNum; aTray: PTraysObject;
                        aListObj: PListObject; dbIso, dbWap: PDBFile);
var
  d : PBarcodeDlg;
begin
  d := New(PBarcodeDlg, Init(aParent, aDlgName, aSession, selectedIsoSeq,
                             aTray, aListObj, dbIso, dbWap));
  if d <> nil then
    Application^.ExecDialog(d);
end;


{ TBarCodePrinter methods ----------------------------------------------------}

  constructor TBarCodePrinter.Init;
  begin
    {Create the object and associate it to a device}
    Inherited Init;
    AbortPrinting:=False;
  end;

  procedure TBarCodePrinter.PrintDadeBarCode(aParent : pWindowsObject;
                                             aTitle  : pChar;
                                             aBarCodeStr,
                                             aPatientStr, aSpecimenStr,
                                             aIsolateStr, aPanelTypeStr: String);
  var
    PrnDC: HDC;
    PageSize: TPoint;
    pBarCode: pDadeBarCode;
  begin
    {Prepare context, create and print the BarCode}
    PrnDC := GetDC;
    pBarCode:=New(pDadeBarCode,
              Init(aTitle,
                   aBarCodeStr,
                   aPatientStr, aSpecimenStr,
                   aIsolateStr, aPanelTypeStr));
    pBarCode^.Print(PrnDC);

    {Deallocate memory, device context}
    Dispose(pBarCode,Done);
    DeleteDC(PrnDC);
  end;


  procedure TBarCodePrinter.PrintEpsonBarCode(aParent : pWindowsObject;
                                              aTitle  : pChar;
                                              aBarCodeStr,
                                              aPatientStr, aSpecimenStr,
                                              aIsolateStr, aPanelTypeStr: String);
  var
    PrnDC: HDC;
    PageSize: TPoint;
    pBarCode: pEpsonBarCode;
  begin
    {Prepare context, create and print the BarCode}
    PrnDC := GetDC;
    pBarCode:=New(pEpsonBarCode,
              Init(aTitle,  aBarCodeStr,
                   aPatientStr,  aSpecimenStr,
                   aIsolateStr, aPanelTypeStr));
    pBarCode^.Print(PrnDC);

    {Deallocate memory, device context}
    Dispose(pBarCode,Done);
    DeleteDC(PrnDC);
  end;


{ TBarCode methods ----------------------------------------------------------}

  Constructor TBarCode.Init(aTitle: pChar;
                            aBarCodeStr,
                            aPatientStr, aSpecimenStr,
                            aIsolateStr, aPanelTypeStr: String);
  begin
    {Create the printout object and store the barcode strings}
    Inherited Init(aTitle);
    BarCodeStr    :=aBarCodeStr;
    PatientStr    :=aPatientStr;
    SpecimenStr   :=aSpecimenStr;
    IsolateStr    :=aIsolateStr;
    PanelTypeStr  :=aPanelTypeStr;
  end;

  procedure TBarCode.Print(PrnDC: hDC);
  begin
    DC:=PrnDC;
    Escape(DC, SetAbortProc, 0, nil, nil);
    Escape(DC, StartDoc, StrLen(Title),Title, nil);
    PrintLabel;
    Escape(DC, NewFrame, 0, nil, nil);
    Escape(DC, EndDoc, 0, nil, nil);
  end;

  procedure TBarCode.PrintLabel;
  begin
    Abstract;
  end;

  procedure TBarCode.SendStr(aStr: String);
  begin
    {Send a string of characters to the BarCode printer}
    SendRaw(@aStr[1],Length(aStr));
  end;

  procedure TBarCode.SendRaw(Data: Pointer; DataSize: Word);
  var pData: array [0..MaxBitMapSize+2] of byte;
  begin
    {Send a string of characters to the BarCode printer}
    Move(DataSize, pData, SizeOf(Word));
    Move(Data^, pData[2], DataSize);
    Escape(DC, WinTypes.PassThrough, 0, @pData, Nil);
  end;


{ TDadeBarCode --------------------------------------------------------------}

  procedure TDadeBarCode.ListLn(aStr: String);
  begin
    {Just to keep the code in printlabel as close}
    {to the existing code as possible}
    SendStr(aStr+CrLf);
  end;

  procedure TDadeBarCode.PrintLabel;
  begin
    {From Galvan's Barcode Blaster Code}

    {Init printer, ASCII MODE, Ufont bold24, pitch 200}
    ListLn('! 0 130 60 1');
    ListLn('PITCH 200');

    {Send strings with this font, at these locations}
    ListLn(Concat('U B24 (2,N,0) 020 010 ', PatientStr));
    ListLn(Concat('U B24 (2,N,0) 600 010 ', BarCodeStr));
    ListLn(Concat('U B24 (2,N,0) 020 040 ', SpecimenStr,'-',IsolateStr));
    ListLn(Concat('U B24 (2,N,0) 600 040 ', PanelTypeStr));

    {Send the actual Type 39 barcode, aspect ratio 2:6, height 56}
    ListLn(Concat('BARCODE CODE39(2:6)- 250 60 56 ', BarCodeStr));
    ListLn('END');
  end;


{ TEpsonBarCode -------------------------------------------------------------}

  procedure TEpsonBarCode.BuildBitMap;
  var
    ch : char;
    i, j, k, element, width, color : integer;
  begin
    {** initialize character counter **}
    charCnt := 0;

    {** Convert the characters of the barcode string **}
    for i := 1 to length(BarCodeStr) do
    begin
      {** get offset into code array **}
      ch := BarCodeStr[i];
      if (ch = '*') then
        element := 10
      else
        {** N.B. This will only work for numeric barcodes **}
        element := (Ord(ch) - Ord('0'));
      {** for each bit in character pattern..... **}
      for j := 8 downto 0 do
      begin
        if (j mod 2 = 0) then
          color := black
        else
          color := white;
        {** test if wide **}
        if (TestBit(CodeTable[element], j)) then
          if (color = white) then
            width := whitewide
          else
            width := blackwide
        else
          if (color = white) then
            width := whitenarrow
          else
            width := blacknarrow;
        {** print bar in correct color for correct # columns **}
        for k := 1 to width do
        begin
          charCnt := charCnt + 1;
          BCodeBitMap^[charCnt] := Chr(color);
        end;
      end;
      {** write inter character gap in white **}
      if (i < length(BarCodeStr)) then
        for k := 1 to interchar do
        begin
          charCnt := charCnt + 1;
          BCodeBitMap^[charCnt] := Chr(white);
        end;
    end;
  end;

  procedure TEpsonBarCode.LineFeed;
  begin
    SendStr( CrLf );
  end;

  procedure TEpsonBarCode.SetSpacing;
  begin
    {** set line spacing for graphics **};
    SendStr( ESC+'3'+Chr(24)+Chr(15) );
  end;

  procedure TEpsonBarCode.PrintBitMap;
  begin
    {** tell the printer how many graphic chars are coming **}
    SendStr( ESC+'Z'+Chr(lowChar)+Chr(highChar) );

    {** Write a line of barcode chars **}
    SendRaw(BCodeBitMap, CharCnt);
  end;

  procedure TEpsonBarCode.FirstLine;
  var  S : string;
  begin
    SetSpacing;
    s := Concat('         ', PatientStr, '           ');
    SendStr(S);
    PrintBitMap;
    s := Concat('      ', BarCodeStr);
    SendStr(S);
    LineFeed;
  end;

  procedure TEpsonBarCode.SecondLine;
  var  S : string;
  begin
    SetSpacing;
    S[0] := Chr(32);
    FillChar(S[1], 32, Chr(32));
    SendStr(S);
    PrintBitMap;
    LineFeed;
  end;

  procedure TEpsonBarCode.ThirdLine;
  var  S : string;
  begin
    SetSpacing;
    S := Concat('         ', SpecimenStr, '-', IsolateStr, '           ');
    SendStr(S);
    PrintBitMap;
    S := Concat('      ', PanelTypeStr);
    SendStr(S);
    LineFeed;
  end;

  procedure TEpsonBarCode.PrintLabel;
  begin
    {Allocate memory for the BarCodeBitMap}
    GetMem(BCodeBitMap,MaxBitMapSize);

    {From Nihongo's C. Itoh/Printronix Code}
    BuildBitMap;

    {** we have to send a high and low byte count of chars to be printed **}
    highChar := charCnt DIV 256;
    lowChar  := charCnt MOD 256;

    {** set form feed length for graphics **}
    SendStr( ESC+'@'+ESC+'x'+Chr(0)+ESC+'C'+Chr(0)+Chr(1) );

    FirstLine;
    SecondLine;
    ThirdLine;
    LineFeed;

    {** send extra space to make barcode 0.5 inches high **}
    SendStr( ESC+'J'+Chr(12) );

    {Free the memory used by the BarCodeBitMap}
    FreeMem(BCodeBitMap,MaxBitMapSize);
  end;


  procedure ResetPanelIDDates;
  Type PanelIdDates = array [1..MaxPanelId] of Date;
  var i: Integer;
      f: file of PanelIdDates;
      MaxJulians : PanelIdDates;
  begin
  {$I-}
    for i:=1 to MaxPanelId do
      {$IFDEF FourByteDates}
         MaxJulians[i]:=MaxLongInt;
      {$ELSE}
         MaxJulians[i]:=MaxWord;
      {$ENDIF}

    Assign(f, PIdDatesfname);
    Rewrite(f);
    Write(f, MaxJulians);
    Close(f);
  {$I+}
  end;

  procedure StampPanelID(PId: Word);
  Const ValidResultsWindow=10;
  var f: file of Date;
      Stamp: Date;
      found: array[0..fsPathName] of Char;
  begin
  {$I-}
    {Create the file if needed}
    FileSearch(found, PIdDatesfname,'');
    if found[0]=#0 then ResetPanelIDDates;

    {Stamp this PanelId with today's JulianDate, allow for
    {ValidResultsWindow days, in case host and WA dates were out of sync}
    Assign(f, PIdDatesfname);
    Reset(f);
    Seek(f,PId-1);
    Stamp:=Today-ValidResultsWindow;
    Write(f,Stamp);
    Close(f);
  {$I+}
  end;

  function ValidResultSet(PId: Word; ReadDate: TimeDate): Boolean;
  var f: file of Date;
      PIdDateStamp: Date;
      JulianReadDate: Date;
  begin
  {$I-}
    {When was this PID last assigned?}
    Assign(f,PIdDatesfname);
    Reset(f);
    Seek(f, PId-1);
    Read(f,PIdDateStamp);
    Close(f);

    {Is it reasonable to assume that these results correspond to this}
    {Iso/PID pair, and not to a previous panel run with the same PID?}
    JulianReadDate:=DMYtoDate(ReadDate.Day,ReadDate.Month,ReadDate.Year);
    ValidResultSet:=(IOResult=0) and (JulianReadDate>=PIdDateStamp);
  {$I+}
  end;


END.
