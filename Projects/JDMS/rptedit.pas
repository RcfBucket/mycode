{----------------------------------------------------------------------------}
{  Module Name  : RPTEDIT.PAS                                                }
{  Programmer   : RCF                                                        }
{  Date Created : 07/13/95                                                   }
{  Requirements :                                                            }
{                                                                            }
{  Purpose - This module is for the Report Format Editor program. It         }
{    provides report format editor windows.                                  }
{                                                                            }
{  Assumptions                                                               }
{    Data in the data file is valid                                          }
{                                                                            }
{  Limitations                                                               }
{    unknown                                                                 }
{                                                                            }
{  Referenced Documents                                                      }
{    none                                                                    }
{                                                                            }
{  Revision History - This project is under version control, use it to view  }
{    revision history.                                                       }
{                                                                            }
{----------------------------------------------------------------------------}

unit RptEdit;

INTERFACE

uses
  OWindows,
  WinTypes,
  RptShare,
  RptData,
  UserMsgs,
  BoxWin,
  PageWin;

{$I RPTGEN.INC}

type
  {--------------------------------------------------------------------------}
  { This object represents the report format edit window.
  {--------------------------------------------------------------------------}
  PRptEdit = ^TRptEdit;
  TRptEdit = object( TPageWin )

    {---- Interface }
    constructor Init( pParent: PWindowsObject; aZoom: Real; aSeq: Longint );

    {---- Implementation }
    private

    { New attributes }
    rptName: array[ 0..MaxRptNameLen ] of Char;
    rptVersion: array[ 0..16 ] of Char;
    rptSeq: Longint;

    { Redefined methods }
    procedure SetupWindow; virtual;
    procedure ClearAllBoxes; virtual;
    procedure ShowAllBoxes( aTitle: PChar ); virtual;
    function GetParentHWnd: HWnd; virtual;
    function InitBox( BoxRect: TRect; anOrigin: TPoint ): PBoxWin; virtual;
    procedure OnAdd( var msg: TMessage ); virtual CM_FIRST + CM_ADD;

    { New methods }
    function CanClear: Boolean; virtual;
    function SaveAllData( var aSeq: Longint; aName: PChar ): Boolean;
    procedure UMIsRptLoaded( var msg: TMessage ); virtual WM_FIRST +
      UM_ISRPTLOADED;
    procedure WMMDIActivate( var msg: TMessage ); virtual WM_FIRST +
      WM_MDIACTIVATE;
    procedure OnPageSetup( var msg: TMessage ); virtual CM_FIRST +
      CM_PAGESETUP;
    procedure OnSave( var msg: TMessage ); virtual CM_FIRST + CM_SAVE;
    procedure OnSaveAs( var msg: TMessage ); virtual CM_FIRST + CM_SAVEAS;
    procedure OnFileClose( var msg: TMessage ); virtual CM_FIRST +
      CM_FILECLOSE;

  end;

IMPLEMENTATION

uses
  ApiTools,
  CtlLib,
  DBErrors,
  DBFile,
  DBLib,
  DBTypes,
  DlgLib,
  ODialogs,
  PgSetup,
  PrnInfo,
  PrnObj,
  RptAlign,
  DBIDS,
  RptUtil,
  DMSErr,
  DMString,
  MScan,
  SectReg,
  Strings,
  StrsW,
  WinProcs;

                        {----------------------}
                        {     File Constants   }
                        {----------------------}



const
  {--------------------------------------------------------------------------}
  { Twips per inch
  {--------------------------------------------------------------------------}
  TPI = 1440;



                        {----------------------}
                        {     Save Dialog      }
                        {----------------------}



type
  {--------------------------------------------------------------------------}
  { This object represents the Save Report Format dialog box. This dialog
  { contains an edit box for the report format's name and Ok and Cancel
  { buttons.
  {--------------------------------------------------------------------------}
  PSaveDlg = ^TSaveDlg;
  TSaveDlg = object( TCenterDlg )
    SN: Longint;                                { Why not TSeqNum? }
    Name: PChar;
    pEditControl: PEdit;
    constructor Init( pParent: PWindowsObject; input_Name: PChar;
      input_SN: Longint );
    procedure SetupWindow; virtual;
    function CanClose: Boolean; virtual;
  end;



{----------------------------------------------------------------------------}
{ The constructor initializes the base class, creates control objects for
{ controls needing special handling, and initializes the attributes.
{----------------------------------------------------------------------------}

constructor TSaveDlg.Init( pParent: PWindowsObject; input_Name: PChar;
  input_SN: Longint );
const
  TEXT_LENGTH = 0;
  IDC_STATIC_NAME = 300; { must match resource file }
  BOLD = true;
var
  pC: PControl;
begin
  { Initialize base class }
  inherited Init( pParent, MakeIntResource( DLG_SAVE ) );

  { Create control objects }
  pC := New( PLStatic, InitResource( @Self, IDC_STATIC_NAME, TEXT_LENGTH,
    not BOLD ) );
  pEditControl := New( PEdit,
    InitResource( @Self, IDC_RPTNAME, MaxRptNameLen + 1 ) );

  { Initialize attributes }
  Name := input_Name;
  SN := input_SN;
end;



{----------------------------------------------------------------------------}
{ This procedure completes the initialization. The override is required to
{ set the name in the edit control.
{----------------------------------------------------------------------------}

procedure TSaveDlg.SetupWindow;
begin
  { Standard setup }
  inherited SetupWindow;

  { Set the name in the edit control }
  pEditControl^.SetText( name );
end;



{----------------------------------------------------------------------------}
{ This override replaces the closing condition and adds a side-affect.
{
{ Closing conditions: The name must not be blank and must not cause a
{ duplicate in the data file.
{
{ Side-affect: If the conditions are met, the name is transfered to a member
{ variable, otherwise, an error message is displayed.
{
{ Note: This override does not call the inherited CanClose as recommended.
{ It assumes it will always return true.
{----------------------------------------------------------------------------}

function TSaveDlg.CanClose: Boolean;

  {--------------------------------------------------------------------------}
  { Thid function returns true if the name and sequence number will not cause
  { a duplicate entry in the data file.
  {--------------------------------------------------------------------------}
  function isUnique( Name: PChar ): Boolean;
  var
    pDB: PDBFile;
  begin
    isUnique := true;
    pDB := New( PDBFile, Init( DBRPTFmtsFile, '', dbOpenNormal ) );
    if pDB = nil then
      ShowError( nil, IDS_CANTOPENFORMAT, nil, 0, MOD_RPTGEN, 3 )
    else
    begin
      { Search for name in data file }
      pDB^.dbr^.PutField( DBRPTFmtName, Name );
      pDB^.dbc^.GetEQ( pDB^.dbr );

      { If found, it's unique if SN matches. }
      if pDB^.dbc^.dbErrorNum = 0 then
        isUnique := SN = pDB^.dbr^.GetSeqValue;

      { Cleanup }
      MSDisposeObj( pDB );
    end;
  end;

var
  ControlText,
  TrialName: array[ 0..100 ] of Char;
  CloseOk: Boolean;

begin { CanClose }

  { Access trial name }
  pEditControl^.GetText( ControlText, SizeOf( ControlText ) - 1 );
  TrimAll( TrialName, ControlText, ' ', SizeOf( ControlText ) - 1 );

  { Condition 1: Name is not blank }
  CloseOk := StrLen( TrialName ) <> 0;
  if not CloseOk then
    ShowError( @self, IDS_RPTNAMEBLANK, nil, 0, MOD_RPTGEN, 0 );

  { Condition 2: Name is unique }
  if CloseOk then
  begin
    CloseOk := isUnique( TrialName );
    if not CloseOk then
      ShowError( @self, IDS_RPTEXISTS, nil, 0, MOD_RPTGEN, 0 );
  end;

  { Transfer if conditions were met }
  if CloseOk then
    StrCopy( Name, TrialName );

  { Return result }
  CanClose := CloseOk;
end;



                        {----------------------}
                        {     Edit Window      }
                        {----------------------}



{----------------------------------------------------------------------------}
{ The constructor performes step one initializations.
{----------------------------------------------------------------------------}

constructor TRptEdit.Init( pParent: PWindowsObject; aZoom: Real;
  aSeq: Longint );

const
  {--------------------------------------------------------------------------}
  { Define defaults: 8.5 x 11 inch, portrait, with 1/4-inch margins
  {--------------------------------------------------------------------------}
  DEFAULT_MARGIN = TPI div 4;
  DEFAULT_WIDTH = Trunc( 8.5 * TPI );
  DEFAULT_HEIGHT = Trunc( 11 * TPI );
  DEFAULT_RPP = 2;
  DEFAULT_SIZE: TPoint = ( x: DEFAULT_WIDTH; y: DEFAULT_HEIGHT );
  DEFAULT_MARGINS: TRect = ( left: DEFAULT_MARGIN; top: DEFAULT_MARGIN;
    right: DEFAULT_MARGIN; bottom: DEFAULT_MARGIN );

var
  tPage: TRptPage;
  k: Integer;
  db: PDBFile;
  pstr: array[ 0..100 ] of Char;

begin { Init }

  { Open wait cursor } { when is it turned off? }
  SetCursor( LoadCursor( 0, IDC_WAIT ) );

  { Define page parameters }
  with tPage do
  begin
    { Reset it }
    FillChar( tpage, SizeOf( tpage ), 0 );

    { Set printer dependent parameters }
    if prn^.status = PS_OK then
    begin
      { Set printer defaults }
      SelectedPaperSize( prn, paperIdx, size );
      PrintMargins( prn, margin );
      Orientation( prn, orient, k );
    end
    else
    begin
      { Set nominal defaults }
      size := DEFAULT_SIZE;
      margin := DEFAULT_MARGINS;
      orient := DMORIENT_PORTRAIT;
      paperIdx := DMPAPER_LETTER;
    end;

    { Set remaining parameters }
    rptsPerPage := DEFAULT_RPP;
  end;

  { Load attributes }
  rptSeq := aSeq;
  rptName[ 0 ] := #0;
  rptVersion[ 0 ] := #0;

  { If a record was specified, then read it from the data file (which may over
    write some of the defaults loaded above). }
  if rptSeq <> -1 then
  begin
    { Open data file and read report format record }
    db := New( PDBFile, Init( DBRPTFmtsFile, '', dbOpenNormal ) );
    if db = nil then
    begin
      ShowError( nil, IDS_CANTOPENFORMAT, nil, dbLastOpenError, MOD_RPTGEN,
        1 );
      Fail;
    end
    else
    begin
      { If it can be found, read its fields }
      db^.dbc^.GetSeq( db^.dbr, rptSeq );
      if db^.dbc^.dbErrorNum = 0 then
      begin
        { Found }
        with db^.dbr^ do
        begin
          GetField( DBRPTFmtName, @rptName, SizeOf( rptName ) - 1 );
          GetField( DBRPTFmtVersion, @rptVersion, Sizeof( rptVersion ) - 1 );
          GetField( DBRPTFmtSize, @tpage.size, SizeOf( tpage.size ) );
          GetField( DBRptFmtMargins, @tpage.margin, SizeOf( tpage.margin ) );
          GetField( DBRptFmtRptPerPage, @tpage.rptsPerPage,
            SizeOf( tpage.rptsPerPage ) );
          GetField( DBRptFmtOrient, @tpage.orient, SizeOf( tpage.orient ) );
          GetField( DBRptFmtPaperIdx, @tpage.paperIdx,
            SizeOf( tpage.paperIdx ) );
          GetField( DBRptFmtSpacing, @tpage.spacing,
            SizeOf( tpage.spacing ) );
        end;
      end
      else
      begin
        { Not found. (what should be done here?) }
      end;
      MSDisposeObj( db );
    end;
  end;

  { Initialize base object with proper title. }
  if StrLen( rptName ) = 0 then
    inherited Init( pParent, SR( IDS_UNTITLED, pstr, SizeOf( pstr ) - 1 ),
      tPage, aZoom )
  else
    inherited Init( pParent, rptName, tPage, aZoom );
end;



{----------------------------------------------------------------------------}
{ This procedure completes the initialization. The override is required to
{ load the sections.
{----------------------------------------------------------------------------}

procedure TRptEdit.SetupWindow;

  {--------------------------------------------------------------------------}
  { This procedure makes the box, adds it to the collection, and displays it
  { with the focus.
  {--------------------------------------------------------------------------}
  procedure DisplayIt( pBox: PBoxWin );
  begin
    application^.MakeWindow( pBox );
    boxes^.Insert( pBox );
    SetFocus( pBox^.HWindow );
    UpdateFocusChild;
  end;

var
  idx,
  sID,
  LTpPX,
  LTpPY: Integer;
  pBox: PBoxWin;
  rFont: TRptFont;
  Rect: TRect;
  Origin: TPoint;
  dc: HDC;
  pDF: PDBFile;
  SN: Longint;

begin { SetupWindow }

  { Standard setup }
  inherited SetupWindow;

  { If a report has been loaded then load its sections }
  if rptSeq <> -1 then
  begin
    {- for each section in the cross reference file, read the Section ID and
      then the section seq number. From that, initialize a section of the
      proper type (size/origin do not matter) and then call the sections'
      loadData function passing the sequence number }

    MakeDefaultRFont( rFont );  {- any old font }
    GetOrigin( Origin );
    SetRect( Rect, 0, 0, 10, 10 );  {- some arbitrary size }

    pDF := New( PDBFile, Init( DBRPTSectsFile, '', dbOpenNormal ) );
    if pDF = nil then
    begin
      ShowError( nil, IDS_CANTOPENXREF, nil, dbLastOpenError, MOD_RPTGEN, 1 );
    end
    else
    begin
      with pDF^ do
      begin
        { Access record }
        SN := rptSeq;
        dbr^.PutField( DBRptSectReportRef, @SN );
        dbc^.GetEQ( dbr );

        { Iterate through the xref file and load the sections found there }
        while ( dbc^.dbErrorNum = 0 ) and ( SN = rptSeq ) do
        begin
          { get section ID }
          dbr^.GetField( DBRptSectSectID, @sID, SizeOf( sID ) );

          { get section SN }
          dbr^.GetField( DBRptSectSectRef, @SN, SizeOf( SN ) );

          {}
          idx := sections^.IDXFromID( sID );
          if idx > 0 then
          begin
            pBox := sections^.sects[ idx ].InitFunc( @Self, Rect, Origin,
              GetZoom, page, rFont );
            DisplayIt( pBox );
            pBox^.LoadData( SN );
          end
          else
          begin
            ShowError( @self, IDS_NONREGSECT, nil, 0, MOD_RPTGEN, 0 );
          end;

          { Set up next iteration }
          dbc^.GetNext( dbr );
          dbr^.GetField( DBRptSectReportRef, @SN, SizeOf( SN ) );
        end;
      end;

      { Close x-ref file }
      MSDisposeObj( pDF );
    end;
  end;

  { Any modification after this point will need to set this flag }
  IsModified := false;
end;



{----------------------------------------------------------------------------}
{ This override replaces the parent with the grandparent so that ...
{ TRptEdit is a MDI window so return parent handle of main MDI frame window.
{----------------------------------------------------------------------------}

function TRptEdit.GetParentHWnd: HWnd;
begin
  GetParentHWnd := GetParent( GetParent( HWindow ) );
end;



{----------------------------------------------------------------------------}
{ This procedure responds to WM_MDIACTIVATE messages. It is needed to maintain
{ the correct menu when a window is activated or deactivated.
{----------------------------------------------------------------------------}

procedure TRptEdit.WMMDIActivate( var msg: TMessage );
var
  Activate: Boolean;
  NewMenu: Longint;
begin
  { The message's wParam indicates activation or deactivation. }
  Activate := Bool( msg.wParam );

  { On activation, enable the menu for this window, else, enable the default
    menu. }
  if Activate then
    NewMenu := MakeLong( hRptMenu, hRptWindowMenu )
  else
    NewMenu := MakeLong( hFileMenu, hFileWindowMenu );
  SendMessage( GetParent( HWindow ), WM_MDISETMENU, 0, NewMenu );
  DrawMenuBar( GetParent( GetParent( HWindow ) ) );

  { An application should return zero if it processes this message. }
  msg.result := 0;
end;



{----------------------------------------------------------------------------}
{ This function is called when a box needs to be created. BoxRect is the size
{ of the section in device units (pixels), and anOrigin is the page origin in
{ device pixels. This override executes the select section dialog, if the
{ user selects one, it is initialized.
{----------------------------------------------------------------------------}

function TRptEdit.InitBox( BoxRect: TRect; anOrigin: TPoint ): PBoxWin;
var
  tstr: array[ 0..100 ] of Char;
  rFont: TRptFont;
  sIdx: Integer;
  pd: PGetSection;
begin
  InitBox := nil;
  if sections^.TotalSections = 0 then
    ShowError( @self, IDS_NOSECTREG, nil, 0, MOD_RPTGEN, 0 )
  else
  begin
    pd := New( PGetSection, Init( @self, sIdx ) );
    if application^.ExecDialog( pd ) = IDOK then
    begin
      MakeDefaultRFont( rFont );
      {- call section initialization function for section type picked }
      InitBox := sections^.sects[ sIdx ].InitFunc( @self, BoxRect, anOrigin,
        GetZoom, page, rFont );
    end;
  end;
end;



{----------------------------------------------------------------------------}
{ This override adds a confirmation before clearing all boxes.
{----------------------------------------------------------------------------}

procedure TRptEdit.ClearAllBoxes;
var
  p1,
  p2: array[ 0..100 ] of Char;
begin
  if boxes^.count > 0 then
    if YesNoMsg( GetParentHWnd, SR( IDS_CONFIRM, p1, SizeOf( p1 ) - 1 ),
      SR( IDS_CONFIRMCLEAR, p2, SizeOf( p2 ) - 1 ) ) then
      inherited ClearAllBoxes;
end;



{----------------------------------------------------------------------------}
{ This procedure handles the Options | Page Setup command.
{----------------------------------------------------------------------------}

procedure TRptEdit.OnPageSetup( var msg: TMessage );
var
  pDialog: PPageSetupDlg;
begin
  pDialog := New( PPageSetupDlg, Init( @self, page ) );
  if application^.ExecDialog( pDialog ) = IDOK then
  begin
    ChangePageSize( page );
  end;
end;



{----------------------------------------------------------------------------}
{ This function saves a report format and all section data for that report.
{
{ Parameters:
{   aSeq  - This is the seq from which the report was loaded.  If it is -1
{     then this is a new report and must be added to the database.
{   aName - This is the user defined name for the report. It is assumed to
{     be unique
{----------------------------------------------------------------------------}

function TRptEdit.SaveAllData( var aSeq: Longint; aName: PChar ): Boolean;
var
  ret: Boolean;

  {--------------------------------------------------------------------------}
  {--------------------------------------------------------------------------}
  procedure PrepareSave( aSect: PBoxWin ); far;
  begin
    if ret then
      ret := aSect^.SavePrep;
  end;

var
  dbSect: PDBFile;


  {--------------------------------------------------------------------------}
  { This procedure tells a section to save it's data. If the section is
  { successful, the section is added to the cross reference file.
  {--------------------------------------------------------------------------}
  procedure SaveSection( aSect: PBoxWin ); far;
  var
    pstr  : array[ 0..200 ] of Char;
    sSeq  : Longint;
    sID   : Integer;
  begin
    { ignore if an error has occurred }
    if not ret then
      exit;

    { call the sections save section method and get it's seq number that it
      was saved under for the cross ref file }
    if aSect^.SaveData( sSeq ) then
    begin
      sID := aSect^.GetBoxType;  {- get the section type }

      {- then update the report cross reference file }
      with dbSect^ do
      begin
        dbr^.PutField( DBRptSectReportRef, @aSeq );  {- report seq num }
        dbr^.PutField( DBRptSectSectRef, @sSeq );  {- section seq num }
        dbr^.PutField( DBRptSectSectID, @sID );   {- section type (ID) }
        dbc^.InsertRec( dbr );
        if dbc^.dbErrorNum <> 0 then
        begin
          ShowError( @self, IDS_CANTINSERTXREF, nil, dbc^.dbErrorNum,
            MOD_RPTGEN, 0 );
          ret := false;
        end;
      end;
    end
    else
      ret := false;
  end;

  {--------------------------------------------------------------------------}
  { This procedure tells all sections to abort their save operation.
  {--------------------------------------------------------------------------}
  procedure SaveComplete( aSect: PBoxWin ); far;
  begin
    aSect^.SaveComplete;
  end;


  {--------------------------------------------------------------------------}
  { This procedure fills report format record with the proper data.
  {--------------------------------------------------------------------------}
  procedure FillRecord( rec: PDBRec );
  var
    p1: array[ 0..200 ] of Char;
  begin
    with rec^ do
    begin
      ClearData;
      PutField( DBRPTFmtName, aName );
      StrCopy( p1, RptGenVersion );
      PutFieldAsStr( DBRPTFmtVersion, p1 );
      PutField( DBRPTFmtSize, @page.size );
      PutField( DBRptFmtMargins, @page.margin );
      PutField( DBRptFmtRptPerPage, @page.rptsPerPage );
      PutField( DBRptFmtOrient, @page.orient );
      PutField( DBRptFmtPaperIdx, @page.paperIdx );
      PutField( DBRptFmtSpacing, @page.spacing );
    end;
  end;

var
  dbRpt: PDBFile;

  {--------------------------------------------------------------------------}
  { This procedure opens the report format database and the report/section
  { cross reference file.
  {--------------------------------------------------------------------------}
  function OpenFiles: Boolean;
  var
    Ok: Boolean;
  begin
    Ok := true;
    dbSect := nil;
    dbRpt := New( PDBFile, Init( DBRPTFmtsFile, '', dbOpenNormal ) );
    if dbRpt = nil then
    begin
      ShowError( @self, IDS_CANTOPENFORMAT, nil, dbLastOpenError, MOD_RPTGEN, 2 );
      Ok := false;
    end;

    if Ok then
    begin
      dbSect := New( PDBFile, Init( DBRPTSectsFile, '', dbOpenNormal ) );
      if dbSect = nil then
      begin
        ShowError( @self, IDS_CANTOPENXREF, nil, dbLastOpenError, MOD_RPTGEN, 2 );
        Ok := false;
        MSDisposeObj( dbRpt );
      end;
    end;
    OpenFiles := Ok;
  end;

  {--------------------------------------------------------------------------}
  { This procedure saves the data for the report configuration.
  {--------------------------------------------------------------------------}
  procedure StoreConfig;
  begin
    if aSeq = -1 then {- need to add record (new) }
    begin
      FillRecord( dbRpt^.dbr );
      if not dbRpt^.dbc^.InsertRec( dbRpt^.dbr ) then
      begin
        ShowError( @self, IDS_CANTSAVERPTFMT, nil, dbRpt^.dbc^.dbErrorNum,
          MOD_RPTGEN, 1 );
        ret := false;
      end
      else
        aSeq := dbRpt^.dbr^.GetSeqValue; {- return new seq number to caller }
    end
    else  {- modifying an existing report }
    begin
      if dbRpt^.dbc^.GetSeq( dbRpt^.dbr, aSeq ) then  {- re-find record in
        transaction }
      begin
        FillRecord( dbRpt^.dbr );
        if not dbRpt^.dbc^.UpdateRec( dbRpt^.dbr ) then
        begin
          ShowError( @self, IDS_CANTSAVERPTFMT, nil, dbRpt^.dbc^.dbErrorNum,
            MOD_RPTGEN, 2 );
          ret := false;
        end;
      end
      else
      begin
        ShowError( @self, IDS_CANTLOCRPTFMT, nil, dbRpt^.dbc^.dbErrorNum,
          MOD_RPTGEN, 0 );
        ret := false;
      end;
    end;
  end;

  {--------------------------------------------------------------------------}
  { This procedure ...
  {--------------------------------------------------------------------------}
  procedure ClearOld;
  var
    rSeq,
    sSeq: Longint;
    sID,
    sIdx: Integer;
  begin
    if aSeq = -1 then   {- no report has been saved, no need to delete }
      exit;

    {- clear cross ref file }
    with dbSect^ do
    begin
      rSeq := aSeq;
      dbSect^.dbr^.PutField( DBRptSectReportRef, @rSeq );
      while dbc^.GetEQ( dbSect^.dbr ) and ret do
      begin
        dbr^.GetField( DBRptSectSectRef, @sseq, SizeOf( sseq ) );
        dbr^.GetField( DBRptSectSectID, @sID, SizeOf( sID ) );

        {- tell section to delete all related data for sect ID }
        sIdx := sections^.IDXFromID( sID );
        if ( sIdx > 0 ) then
          sections^.sects[sIdx].DeleteFunc( ACTION_OPEN or ACTION_DELETE,
            rSeq, sSeq );
        dbc^.DeleteRec( dbSect^.dbr );  {- delete xref entry }
      end;
    end;
  end;

var
  curs: HCursor;
  k: Integer;

begin { SaveAllData }

  { Open wait cursor }
  curs := SetCursor( LoadCursor( 0, IDC_WAIT ) );

  { Open report format and cross ref file }
  ret := OpenFiles;

  { Tell all sections to prepare for saving }
  if ret then
    boxes^.ForEach( @PrepareSave );

  if ret then
  begin
    k := DBBeginTransaction;
    if k <> 0 then
    begin
      ShowError( @self, IDS_CANTBEGXACTION, nil, k, MOD_RPTGEN, 0 );
      ret := false;
    end;
  end;

  {- now save the report format config. At this point, the transaction has begun }
  if ret then
  begin
    StoreConfig;

    if ret then {- clear the old configuration }
      ClearOld;

    {- at this point, the report is saved and the cross ref file is prepared,
        now just tell all sections to write their data }
    if ret then
      boxes^.ForEach( @SaveSection );

    {- if all was successful, then end the transaction. Otherwise, abort it }
    if ret then
      DBEndTransaction
    else
      DBAbortTransaction;
  end;

  {- tell each section that saving is complete }
  boxes^.ForEach( @SaveComplete );

  {- tell all sections to close the "delete" files }
  for k := 1 to sections^.TotalSections do
    sections^.sects[ k ].DeleteFunc( ACTION_CLOSE, 0, 0 );

  { Close database objects } {should be CloseFiles? }
  MSDisposeObj( dbSect );
  MSDisposeObj( dbRpt );

  { Load return value }
  SaveAllData := ret;

  { Close wait cursor }
  SetCursor( curs );
end;



{----------------------------------------------------------------------------}
{ This procedure handles the File | Save As command.
{----------------------------------------------------------------------------}

procedure TRptEdit.OnSaveAs( var msg: TMessage );
var
  pSD: PDialog;
  p1: array[ 0..100 ] of Char;
begin
  { Prompt user to enter a name or cancel }
  StrCopy( p1, '' );
  pSD := New( PSaveDlg, Init( @self, p1, -1 ) );
  if application^.ExecDialog( pSD ) = IDOK then
  begin
    { Save the report format using the name }
    rptSeq := -1;
    if SaveAllData( rptSeq, p1 ) then
    begin
      { Name the window }
      SetWindowText( HWindow, p1 );
      StrCopy( rptName, p1 );

      { Clear modified flag }
      IsModified := false;
    end;
  end;
end;



{----------------------------------------------------------------------------}
{ This procedure handles the File | Close command.
{----------------------------------------------------------------------------}

procedure TRptEdit.OnFileClose( var msg: TMessage );
begin
  CloseWindow;
end;



{----------------------------------------------------------------------------}
{ This procedure handles the File | Save command.
{----------------------------------------------------------------------------}

procedure TRptEdit.OnSave( var msg: TMessage );
var
  pSaveDialog: PDialog;
begin
  pSaveDialog := New( PSaveDlg, Init( @self, rptName, rptSeq ) );
  if application^.ExecDialog( pSaveDialog ) = IDOK then
  begin
    if SaveAllData( rptSeq, rptName ) then
    begin
      { Handle possible rename }
      SetWindowText( HWindow, rptName );

      { Clear modified flag }
      IsModified := false;
    end;
  end;
end;



{----------------------------------------------------------------------------}
{ This override handles the Section | Add command.
{----------------------------------------------------------------------------}

procedure TRptEdit.OnAdd( var msg: TMessage );
var
  LogicalSRect: TLRect;
  DeviceSRect: TRect;
  s: PBoxWin;
  pt: TPoint;
  pstr: array[ 0..100 ] of Char;
  p1,
  p2: array[ 0..50 ] of Char;
  dc: HDC;
  LTpPX: Integer;
  LTpPY: Integer;

begin
  { Define default section rectangle that extends 3-inches by 2-inches from
    the origin. }
  with LogicalSRect  do
  begin
    left   := 0;
    top    := 0;
    right  := 3 * TPI;
    bottom := 2 * TPI;
  end;

  { Prompt user to accept, edit, or cancel the size and position using the Add
    Report Section dialog. }
  SR( IDS_NEWSECT, pstr, SizeOf( pstr ) - 1 );
  if GetAlignment( @self, SR( IDS_ADDSECT, p1, SizeOf( p1 ) - 1 ),
    SR( IDS_SECTION, p2, SizeOf( p2 ) - 1 ), pstr, LogicalSRect ) then
  begin
    { Convert logical coordinates to device units, taking into account the
      current zoom factor. }
    dc := GetDC( HWindow );
    SetLogicalTwips( dc, GetZoom );
    LTwipsPerPixel( dc, LTpPX, LTpPY );
    ReleaseDC( HWindow, dc );
    with DeviceSRect do
    begin
      left   := LogicalSRect.left   div LTpPX;
      right  := LogicalSRect.right  div LTpPX;
      top    := LogicalSRect.top    div LTpPY;
      bottom := LogicalSRect.bottom div LTpPY;
    end;

    { Translate rectangle to device origin }
    GetOrigin( pt );
    with DeviceSRect do
    begin
      Inc( left, pt.x );
      Inc( top, pt.y );
      Inc( right, left );
      Inc( bottom, top );
    end;

    { Add the section, prompting user for section type }
    CreateBox( DeviceSRect );
  end;
end;



{----------------------------------------------------------------------------}
{ This procedure handles 'is report loaded' messages. If the report is
{ loaded into this window, it returns a result of 1, else 0. The sender of
{ this message must place the sequence number of the message in question in
{ lParam.
{----------------------------------------------------------------------------}

procedure TRptEdit.UMIsRptLoaded( var msg: TMessage );
begin
  if msg.lParam = rptSeq then
    msg.result := 1
  else
    msg.result := 0;
end;



{----------------------------------------------------------------------------}
{ This override replaces the title that is passed into this function
{ (normally "Defined Boxes") with the title "Report Sections."
{----------------------------------------------------------------------------}

procedure TRptEdit.ShowAllBoxes( aTitle: PChar );
var
  Title: array[ 0..100 ] of Char;
begin
  inherited ShowAllBoxes( SR( IDS_RPTSECTIONS, Title, SizeOf( Title ) - 1 ) );
end;



{----------------------------------------------------------------------------}
{ This override returns true if it is Ok to clear the contents of the
{ window. If the report format is in a modified state, the user will need to
{ confirmation the clear with an option to save.
{----------------------------------------------------------------------------}

function TRptEdit.CanClear: Boolean;
var
  p1,
  p2: array[ 0..100 ] of Char;
  response: Integer;
  msg: TMessage;
begin
  CanClear := true;
  if IsModified then
  begin
    response := YesNoCancelMsg( hWindow, SR( IDS_CONFIRM, p1,
      SizeOf( p1 ) - 1 ), SR( IDS_CONFIRMSAVE, p2, SizeOf( p2 ) - 1 ) );
    if response = IDYES then
      OnSave( msg )
    else if not (response = IDNO) then
      CanClear := false;
  end;
end;

    END.