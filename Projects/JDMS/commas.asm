.MODEL large, Pascal

PUBLIC InsCommas

.CODE

;========================================================== InsCommas
;
; function InsCommas(dest, src: PChar; maxLen: Byte; comma, decPt: Char): PChar;
;
; returns numStr with commas inserted.
;--------------------------------------------------------------------
InsCommas  PROC FAR

; Parameters
dest            equ     dword ptr ss:[bp+16]
src             equ     dword ptr ss:[bp+12]
maxLen          equ     byte  ptr ss:[bp+10]
comma           equ     byte  ptr ss:[bp+8]
decPt           equ     byte  ptr ss:[bp+6]
pSize           equ     14
isneg           equ     byte ptr ss:[bp-2]

        push    bp
        mov     bp,sp
        sub     sp,2
        push    ds


; check for nil condition
        mov     ax, word ptr ss:[bp+12] ; if destStr = nil then bail
        or      ax, word ptr ss:[bp+14]
        jnz     @@1
        jmp     done
@@1:
        mov     ax, word ptr ss:[bp+16] ; if numStr = nil then bail
        or      ax, word ptr ss:[bp+18]
        jnz     @@2
        jmp     done
@@2:
        cmp     maxLen,0
        jnz     @@3
        jmp     done
@@3:

start:
        ; Get length of Numstr
        les     di, src                 ; get length of source string

        mov     isNeg, 0                ; see if there is a negative
        cmp     byte ptr es:[di], '-'
        jne     noNeg
        mov     isNeg, 1
noNeg:

        mov     al, 0
        mov     cx, 0ffffh
        cld                             ; direction ->
        repne scasb                     ; search for terminating null
        mov     ax, 0ffffh
        sub     ax, cx
        dec     ax
        mov     cx,ax                   ; CX = strlen(src)

        lds     si,src                  ; DS:SI => src
        xor     dx,dx                   ; Zero some vars
        xor     ah,ah
        xor     bx,bx

        mov     dh,cl                   ; dh = strlen(numStr)
        or      dh,dh
        jnz     @@4                     ; if strlen = 0 then done

        les     di, dest                ; return 0 length string
        mov     al,0
        stosb

@@4:
        mov     bl,al                   ; BL = Length(numstr)
        add     si,bx
        dec     si                      ; SI => end of numstr

; Calculate the length of the final string, counting number of commas

; see if there is a decimal point

        les     di,src           ; ES:DI => src[0]
        mov     cx,bx            ; CX = Length(src)
        mov     al,decPt         ; Look for a decimal point
        repne scasb

; If zero flag is set here then there is no decimal point
; else, CX+1 = number of digits past decimal point

        les     di,dest         ; ES:DI => dest[0]
        jnz     NoDec

        inc     cx              ; CX = # of bytes past dec point + "."
NoDec:
        mov     dl,cl           ; DL = # of bytes past dec point + "."

        mov     ax,bx           ; count the number of needed commas
        sub     ax,cx
        or      al,al           ; is there a whole part
        jz      NoWholePart
        dec     al
        sub     al, isNeg
        mov     cl,3
        idiv    cl              ; al = number of needed commas

NoWholePart:
        xor     ah,ah
        add     dh,al           ; DH = Final length of result string
        add     dh, isNeg

        cmp     dh,byte ptr maxLen
        jle     go
        les     di, dest
        cld
        mov     al,0
        stosb
        jmp     done

go:
        add     di,bx
        add     di,ax

; Copy numstr to result String

        std
        mov     al, 0
        stosb                   ; store terminating null

        or      dl,dl           ; are there numbers past decimal
        jz      noDec2
        mov     cl,dl
        rep movsb               ; move all past decimal

NoDec2:
        or      al,al           ; are there any whole numbers
        mov     ah, comma
        mov     cl,bl           ; AL = Length(numStr)
        sub     cl,dl
        jcxz    done
        xor     dl,dl

insLoop:
        lodsb                   ; AL = numstr[SI]
        cmp     al,'-'
        je      next
        cmp     dl,3            ; If DL = 3
        jne     next
        xchg    al,ah           ; put comma in AL
        stosb                   ; result[DI] = AL
        xchg    al,ah           ; put number back in AL
        xor     dl,dl
next:
        inc     dl              ; Inc(DL)
        stosb                   ; store number
        loop    insLoop         ; repeat until CX = 0
done:
        mov     ax, word ptr dest          ; setup function return
        mov     dx, word ptr dest+2

        pop     ds              ; restore some regs
        add     sp,2
        pop     bp
        ret     pSize
InsCommas       ENDP

        END