Unit DbifFlds;

Interface

Const
	DbifFilePatient =               1;
	DbifFileSpecimen =              51;
	DbifFileSource =                        101;
	DbifFileWard =                  106;
	DbifFileBattery =               151;
	DbifFileBatDef =                        201;
	DbifFileTest =                  251;
	DbifFileDrug =                  301;
	DbifFileTestCat =                       351;
	DbifFileTestProc =              401;
	DbifFileBatOrd =                        451;
	DbifFileResult =                        501;
	DbifFileOrg =                           551;
	DbifFileSession =               601;
{**UnusedSlot =                         651;}
	DbifFileBatProc =               701;
	DbifFileWAPanels =              751;
	DbifFileMPSSRuleSet =   801;
{**UnusedSlot =                         851;}
{**UnusedSlot =                         901;}
	DbifFileMPSSRules =             951;
{**UnusedSlot =                         1001;}
   DbifFileDrugSelNames =  1051;
   DbifFileDrugSelDrugs =       1101;

	DbifFldMnemonicAbbr = 1;
	DbifFldMnemonicDesc = 2;

	DbifFldPatientID = 1;
	DbifFldPatientLastName = 2;
	DbifFldPatientFirstName = 3;
	DbifFldPatientFreeText = 4;
	DbifFldPatientUpdDate = 5;
	DbifFldPatientUpdTime = 6;
	DbifFldPatientArchive = 7;

	DbifFldSpecimenID = 1;
	DbifFldSpecimenCollectDate = 2;
	DbifFldSpecimenPatientID = 3;
	DbifFldSpecimenSource = 4;
	DbifFldSpecimenWard = 5 ;
	DbifFldSpecimenFinalizedFlag = 6;
	DbifFldSpecimenFreeText = 7;
	DbifFldSpecimenUpdDate = 8;
	DbifFldSpecimenUpdTime = 9;
	DbifFldSpecimenArchive = 10;

	DbifFldSourceAbrev = 1;
	DbifFldSourceDesc = 2;
	DbifFldSourceType = 3;

	DbifFldWardAbrev = 1;

	DbifFldBatteryID = 1;
	DbifFldBatteryDesc = 2;
	DbifFldBatteryUDFlag = 3;
  DbifFldBatteryTSet = 4;

	DbifFldBatteryDefID = 1;
	DbifFldBatteryDefTestProc = 2;
	DbifFldBatteryDefTestID = 3;

	DbifFldTestID = 1;
	DbifFldTestName = 2;
	DbifFldTestDrug = 3;
	DbifFldTestCategory = 4;
	DbifFldTestUDFlag = 5;
  DbifFldTestResultLen = 6;

	DbifFldDrugID = 1;
	DbifFldDrugName = 2;
	DbifFldDrugUDFlag = 3;

	DbifFldTestCategoryID = 1;
	DbifFldTestCategoryDesc = 2;
  DbifFldTestCategoryResultLen = 3;

	DbifFldTestProcID = 1;
	DbifFldTestProcName = 2;
  DbifFldTestProcTSet = 3;

	DbifFldBatteryOrdSpecimenID = 1;
	DbifFldBatteryOrdCollectDate = 2;
	DbifFldBatteryOrdIsolateID = 3;
	DbifFldBatteryOrdDefID = 4;
	DbifFldBatteryOrdTransmitted = 5;
	DbifFldBatteryOrdQCFlag = 6;
	DbifFldBatteryOrdTestDate = 7;
	DbifFldBatteryOrdResultsFlag = 8;
	DbifFldBatteryOrdFreeText = 9;
	DbifFldBatteryOrdUpdDate = 10;
	DbifFldBatteryOrdUpdTime = 11;
  DbifFldBatteryOrdSession = 12;
  DbifFldBatteryOrdFamily = 13;
  DbifFldBatteryOrdOrganism = 14;

	DbifFldResultSpecimenID = 1;
	DbifFldResultCollectDate = 2;
	DbifFldResultIsolateID = 3;
	DbifFldResultDefID = 4;
	DbifFldResultTestProcID = 5;
	DbifFldResult = 6;

	DbifFldOrganismID = 1;
	DbifFldOrganismName = 2;
	DbifFldOrganismSet = 3;
	DbifFldOrganismClass = 4;

	DbifFldSessionID = 1;
	DbifFldSessionDesc = 2;

	DbifFldBatProcID = 1;
	DbifFldBatProcTestProc = 2;

	DbifFldWAPanelID = 1;
	DbifFldWAPanelSpecimenID = 2;
	DbifFldWAPanelCollectDate = 3;
	DbifFldWAPanelIsolateID = 4;
	DbifFldWAPanelBatteryID = 5;
	DbifFldWAPanelTestProc = 6;
   DbifFldWAPanelUsedFlag = 7;

	DbifFldMPSSRuleSetCode = 1;
	DbifFldMPSSRuleSetDesc = 2;

	DbifFldMPSSRuleRuleSet = 1;
	DbifFldMPSSRuleIndex = 2;
	DbifFldMPSSRuleAndOr = 3;
	DbifFldMPSSRuleFileName = 4;
	DbifFldMPSSRuleFieldName = 5;
	DbifFldMPSSRuleRelation = 6;
	DbifFldMPSSRuleValue = 7;

   DbifFldDrugSelNameID = 1;
   DbifFldDrugSelNameDesc = 2;

   DbifFldDrugSelDrugNameID = 1;
	 DbifFldDrugSelDrugDrugID = 2;

	 DBIFFldMnemFileDesc = 1;
	 DBIFFldMnemFileName = 2;

Implementation

End.
