unit FMConst;

INTERFACE


const
  backupDir                 = 'DMSBACK';
  restoreDir                = 'DMSRSTR';
  mergeDir                  = 'DMSMERGE';

  FullCatalog               = 'F';
  IncrCatalog               = 'I';
  ExtractCatalog            = 'E';
  ReportCatalog             = 'R';

  BackupInComplete          = 0;
  BackupComplete            = 1;

  max8dot3                  =  13;
  floppyBytes               =  1440000;
  OutWidth                  =  3;
  sem_FailCriticalErrors    =  $0001;
  sem_NoOpenFileErrorBox    =  $8000;
  pathNotFound              =  3;
  noMoreFiles               =  18;
  continueJob               =  0;
  cancelJob                 =  2;
  tryNewDisk                =  3;
  backupReportFiles         =  0;
  backupAllFiles            =  1;
  NoIncremental             =  5;

  fmErr_NoMatchSchema       =  19;
  fmErr_OpeningFile         =  20;
  fmErr_CreatingFile        =  21;
  fmErr_OpeningFileExclus   =  22;
  fmErr_DiskWrite           =  23;
  fmErr_HardDiskFull        =  24;
  fmErr_MPSSFailed          =  25;
  fmErr_CreatingFileDesc    =  26;
  fmErr_DeletingFile        =  27;
  fmErr_RenamingFile        =  28;
  fmErr_InsertingRecord     =  29;


{$I FMUTIL.INC}


IMPLEMENTATION

End.
