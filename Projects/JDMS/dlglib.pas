unit DlgLib;

INTERFACE
{---------------------------------------------------------------------------}
{ Common dialog objects for the DMS                                         }
{                                                                           }
{ This library provides dialog objects that should be used in the DMS. No   }
{ application should create a direct dialog descended from TDialog. Rather, }
{ the user should descend from TDMSDlg below.  If a centered dialog is      }
{ desired, then use the TCenterDlg below.                                   }
{                                                                           }
{ To use these dialog objects, you *MUST* obey the following rules:         }
{ All push buttons must have an ID value if 199 or *less*.  These dialogs   }
{ consider all child windows with an id of 199 or less as push buttons.     }
{                                                                           }
{ Date  Who    What                                                         }
{ ----- ------ -----------------------------------------------------------  }
{ 02/95 RCF    Initial release                                              }
{---------------------------------------------------------------------------}

uses
  CTL3D,
  wintypes,
  oDialogs,
  oWindows;

type
  PDMSDlg = ^TDMSDlg;
  TDMSDlg = object(TDialog)
    constructor Init(aParent: PWindowsObject; aName: PChar);
    destructor Done; virtual;
    procedure SetupWindow; virtual;
    procedure WMActivate(var msg: TMessage); virtual WM_FIRST + WM_ACTIVATE;
    procedure EnableButtons; virtual;
    procedure HandleFocus(on: boolean);
    private
    lastFoc   : HWnd;
    hndlFoc   : boolean;
    unhook    : boolean;
  end;

  TDMSDlgWindow = object(T3DDlgWindow)
    constructor Init(aParent: PWindowsObject; aName: PChar);
    destructor Done; virtual;
    procedure SetupWindow; virtual;
    procedure WMActivate(var msg: TMessage); virtual WM_FIRST + WM_ACTIVATE;
    procedure EnableButtons; virtual;
    procedure HandleFocus(on: boolean);
    private
    hndlFoc   : boolean;
    lastFoc   : HWnd;
    unhook    : boolean;
  end;

  PCenterDlg = ^TCenterDlg;
  TCenterDlg = object(TDMSDlg)
    procedure SetupWindow; virtual;
  end;

  PCenterDlgWindow = ^TCenterDlgWindow;
  TCenterDlgWindow = object(TDMSDlgWindow)
    procedure SetupWindow; virtual;
  end;

IMPLEMENTATION

uses
  Bits,
  APITools,
  DMSDebug,
  winprocs,
  Win31;

const
  {- the following key defines the key that will act as a tab key in all
     TDMSDlg's. This key will act in addition to the normal tab key. Also, if
     this key is 0 then no additional tab key will be defined }
  VK_USERTAB    = VK_RETURN;

  {- if the extendedCheck is true then the user defined tab key will only work
     when the key is used from the num-pad. For example, if VK_RETURN is the
     user defined tab key and extendedCheck is true, then ONLY the enter key
     on the numpad will act as an additional tab key.  If extendedCheck is
     false, then BOTH the regular enter and the numpad enter will act as a tab
     key }
  ExtendedCheck = true;

var
  msgHook       : HHook;
  msgInstance   : TFarProc;
  hookCount     : integer;

function MsgFilter(code: integer; wParam: word; lParam: longint): longint; export;
{- special msg filter hook to translate WM_KEYDOWN messages for dialogs. This
   routine xlates the User Tab key into a real tab key }
var
  pm  : PMsg;
begin
  pm:= PMsg(lParam);
  if (code < 0) or (pm^.message <> WM_KEYDOWN) or (pm^.wParam <> VK_USERTAB) then
  begin
    msgFilter:= CallNextHookEx(msgHook, code, wParam, lParam)
  end
  else
  begin
    if not extendedCheck or (extendedCheck and TestBit(pm^.lParam, 24)) then
      pm^.wParam:= VK_TAB;
    msgFilter:= CallNextHookEx(msgHook, code, wParam, lParam)
  end;
end;

procedure WMActivatePrim(HWindow: HWnd; active: boolean);
{- activate primitive for TDMSDlg and TDMSDlgWindow. Returns true if buttons on
   the dialog were enabled, otherwise it returns false }
var
  hw    : HWnd;
  id    : word;
begin
  if HWindow > 0 then
  begin
    hw:= GetWindow(HWindow, GW_CHILD); {- first child window }
    while hw <> 0 do
    begin
      id:= word(GetDlgCtrlID(hw));  {- get id of control }

      {- if ctl id within button range (1 - 200) }
      if (id > 0) and (id < 200) then
        EnableWindow(hw, active); {- enable/disable window }

      hw:= GetWindow(hw, GW_HWNDNEXT);  {- get next child window }
    end;
  end;
end;

procedure SetMsgHook;
begin
  if (msgInstance = nil) and (VK_USERTAB <> 0) then
  begin
    hookCount:= 0;
    msgInstance:= MakeProcInstance(@msgFilter, hInstance);
    msgHook:= SetWindowsHookEx(WH_MSGFILTER, THookProc(msgInstance), hInstance, GetCurrentTask);
  end;
  Inc(hookCount); {- increment hook count }
end;

procedure RemoveMsgHook;
begin
  Dec(hookCount);
  if (hookCount <= 0) and (VK_USERTAB <> 0) then
  begin
    UnhookWindowsHookEx(msgHook);
    FreeProcInstance(msgInstance);
    msgInstance:= nil;
    msgHook:= 0;
  end;
end;

{---------------------------------------------------------------[ TDMSDlg ]--}

constructor TDMSDlg.Init(aParent: PWindowsObject; aName: PChar);
begin
  inherited Init(aParent, aName);
  hndlFoc:= true;
  unhook := FALSE;
end;

procedure TDMSDlg.SetupWindow;
begin
  inherited SetupWindow;
  SetMsgHook;
  unhook := TRUE;
end;

procedure TDMSDlg.HandleFocus(on: boolean);
begin
  hndlFoc:= on;
end;

destructor TDMSDlg.Done;
begin
  if unhook then
    RemoveMsgHook;
  inherited Done;
end;

procedure TDMSDlg.WMActivate(var msg: TMessage);
var
  active  : boolean;
begin
  if hndlFoc and IsWindowVisible(HWindow) then
  begin
    active:= ((msg.wParam and (WA_ACTIVE or WA_CLICKACTIVE)) <> 0);

    {- if disabling controls save the current focused control }
    if not active then
      lastFoc:= GetFocus;   {- save the current focus }

    inherited WMActivate(msg);
    WMActivatePrim(HWindow, active);
    if active then
    begin
      EnableButtons;
      SendMessage(HWindow, WM_NEXTDLGCTL, lastFoc, 1);
      SendMessage(HWindow, WM_NEXTDLGCTL, 0, 0);
      SendMessage(HWindow, WM_NEXTDLGCTL, 1, 0);
    end;
  end
  else
  begin
    inherited WMActivate(msg);
    EnableButtons;
  end;
end;

procedure TDMSDlg.EnableButtons;
begin
  {- default does nothing }
end;

{----------------------------------------------------------[ TDMSDlgWindow ]--}

constructor TDMSDlgWindow.Init(aParent: PWindowsObject; aName: PChar);
begin
  inherited Init(aParent, aName);
  hndlFoc:= true;
  unhook := FALSE;
end;

procedure TDMSDlgWindow.SetupWindow;
begin
  inherited SetupWindow;
  SetMsgHook;
  unhook := TRUE;
end;

procedure TDMSDlgWindow.HandleFocus(on: boolean);
begin
  hndlFoc:= on;
end;

destructor TDMSDlgWindow.Done;
begin
  if unhook then
    RemoveMsgHook;
  inherited Done;
end;

procedure TDMSDlgWindow.WMActivate(var msg: TMessage);
var
  active  : boolean;
begin
  if hndlFoc and IsWindowVisible(HWindow) then
  begin
    active:= ((msg.wParam and (WA_ACTIVE or WA_CLICKACTIVE)) <> 0);

    {- if disabling controls save the current focused control }
    if not active then
      lastFoc:= GetFocus;   {- save the current focus }

    inherited WMActivate(msg);
    WMActivatePrim(HWindow, active);
    if active then
    begin
      EnableButtons;
      SendMessage(HWindow, WM_NEXTDLGCTL, lastFoc, 1);
      SendMessage(HWindow, WM_NEXTDLGCTL, 0, 0);
      SendMessage(HWindow, WM_NEXTDLGCTL, 1, 0);
    end;
  end
  else
  begin
    inherited WMActivate(msg);
    EnableButtons;
  end;
end;

procedure TDMSDlgWindow.EnableButtons;
begin
  {- default does nothing }
end;

{-----------------------------------------------------------[ TCenterDlg ]--}

procedure TCenterDlg.SetupWindow;
var
  x, y : integer;
  w, h : integer;
  r    : TRect;
begin
  inherited SetupWindow;
  GetWindowRect(HWindow, r);
  w:= r.right - r.left;
  h:= r.bottom - r.top;
  x:= (GetSystemMetrics(SM_CXSCREEN) div 2) - (w div 2);
  y:= (GetSystemMetrics(SM_CYSCREEN) div 2) - (h div 2);
  MoveWindow(HWindow, x, y, w, h, false);
end;

{------------------------------------------------------[ TCenterDlgWindow ]--}

procedure TCenterDlgWindow.SetupWindow;
var
  x, y : integer;
  w, h : integer;
  r    : TRect;
begin
  inherited SetupWindow;
  GetWindowRect(HWindow, r);
  w:= r.right - r.left;
  h:= r.bottom - r.top;
  x:= (GetSystemMetrics(SM_CXSCREEN) div 2) - (w div 2);
  y:= (GetSystemMetrics(SM_CYSCREEN) div 2) - (h div 2);
  MoveWindow(HWindow, x, y, w, h, false);
end;

BEGIN
  msgHook:= 0;
  msgInstance:= nil;
  hookCount:= 0;
END.

{- rcf }
