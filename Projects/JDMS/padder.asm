.MODEL large, pascal

PUBLIC Pad,
PUBLIC LeftPad
LOCALS

.CODE

;========================================================== PadCh
;
; function PadCh(dest, src: PChar; padWith: Char; padLen: Word): PChar;
;
; pad tstr with [padWith] to length [len] (truncating if necessary)
;--------------------------------------------------------------------
Pad   PROC FAR
; Parameters
dest            equ     dword ptr ss:[bp+14]
src             equ     dword ptr ss:[bp+10]
padWith         equ     byte ptr  ss:[bp+8]
padLen          equ     word ptr  ss:[bp+6]
@@pSize         equ     12

        push    bp
        mov     bp,sp           ; get pointer into stack
        push    ds              ; save Turbo's DS in DX

        cld                     ; direction ->

        les     di,src          ; get length of source string
        xor     ax,ax
        mov     cx,0ffffh
        repne scasb             ; search for terminating null
        mov     ax, 0ffffh      ;
        sub     ax,cx
        dec     ax
        mov     cx,ax           ; CX = len of source

        les     di,dest         ; point es:di to destination
        lds     si,src          ; DS:SI -> src
        mov     bx,ax           ; save length of source for later too

        jcxz    noCopy          ; if src length=0 then bail

        mov     dx,padLen
        cmp     dx,cx
        jae     copyIt
        mov     cx,dx
copyIt:
        rep     movsb           ; copy src to destination
noCopy:
        mov     ax,padLen       ; get pad length
        cmp     bx,ax           ; if string len = len to pad
        jae     done            ; if str len >= pad length we're done
                                ; we've just truncated.

; Here we actually pad the string. BX=StrLen, DI=beginning of result string

        mov     cx,ax           ; put pad len into CX
        sub     cx,bx           ; find # of bytes to pad
        mov     al,padWith      ; get pad ch.
        rep     stosb           ; store the byte CX times

done:
        mov     al,0
        stosb                   ; store terminating null

        mov     ax,word ptr dest
        mov     dx,word ptr dest+2

        pop     ds              ; restore regs
        pop     bp
        ret     @@psize
Pad     ENDP

;================================================================ LeftPadCh
;
; function LeftPadCh(src, dest: PChar; padWith: Char; padLen: Word): PChar;
;
; left pad tstr with [padWith] to length [len] (truncating if necessary)
;--------------------------------------------------------------------------
LeftPad         PROC FAR
; Parameters
@src            equ     dword ptr ss:[bp+14]
@dest           equ     dword ptr ss:[bp+10]
padWith         equ     byte  ptr ss:[bp+08]
padLen          equ     word  ptr ss:[bp+06]
@@pSize         equ     12

        push    bp
        mov     bp,sp           ; get pointer into stack
        push    ds              ; save Turbo's DS in DX

        cld                     ; direction ->

        les     di,src          ; get length of source string
        xor     ax,ax
        mov     cx,0ffffh
        repne scasb             ; search for terminating null
        mov     ax, 0ffffh      ;
        sub     ax,cx
        dec     ax
        mov     cx,ax           ; CX = strlen(src)
        mov     bx,ax           ; BX = strlen(src)

        lds     si,src          ; DS:SI -> src
        les     di,dest         ; es:di -> dest

        mov     ax,padLen       ; get pad length

        cmp     ax,bx           ; compare src length to padLen
        je      moveit          ; yes, move all of string
        jb      setlen

        mov     cx,ax           ; padLen -> CX
        mov     al,padWith      ; get char to pad with
        sub     cx,bx           ; get # of bytes to pad
        rep     stosb           ; fill with padWith
        mov     cx,bx           ; get original string length
        jmp     moveit          ; move rest of string
setlen:
        mov     cx,padlen       ; truncate to padLen bytes

moveit:                         ; move string to result
        rep     movsb

@@done:
        mov     al,0
        stosb                   ; store terminating null

        mov     ax,word ptr dest
        mov     dx,word ptr dest+2

        pop     ds
        pop     bp
        ret     @@psize
LeftPad         ENDP

        END
