{----------------------------------------------------------------------------}
{  Module Name  : PageWin.PAS                                                }
{  Programmer   : RCF                                                        }
{  Date Created : 07/13/95                                                   }
{  Requirements :                                                            }
{                                                                            }
{  Purpose - This is low-level module for the Report Format Editor program.  }
{    It provides a basic TWindow descendent that displays a zoomable page.   }
{    The scrolling of the window and the painting of the page is all handled }
{    here.                                                                   }
{                                                                            }
{    TPageWin uses Logical Twips mapping mode where 1440 units is equal to   }
{    one inch.                                                               }
{                                                                            }
{  Assumptions                                                               }
{    unknown                                                                 }
{                                                                            }
{  Limitations                                                               }
{    unknown                                                                 }
{                                                                            }
{  Referenced Documents                                                      }
{    none                                                                    }
{                                                                            }
{  Revision History - This project is under version control, use it to view  }
{    revision history.                                                       }
{                                                                            }
{----------------------------------------------------------------------------}

Unit PageWin;

INTERFACE

uses
  userMsgs,
  DMSDebug,
  DMSErr,
  Ctl3d,
  ApiTools,
  BoxWin,
  IntlLib,
  RptShare,
  RptData,
  RptUtil,
  RptShowAll,
  Strings,
  OWindows,
  ODialogs,
  Objects,
  MScan,
  Win31,
  DMString,
  WinProcs,
  WinTypes;

{$I RPTGEN.INC}


type
  {--------------------------------------------------------------------------}
  { This object provides a window with a graphic representation of a page.
  {--------------------------------------------------------------------------}
  PPageWin = ^TPageWin;
  TPageWin = object( T3DWindow )

    {---- Interface }
    Boxes: PCollection;          {- list of pointers to section boxes }
    edgeX: integer;
    edgeY: integer;
    page: TRptPage;
    IsModified: boolean;
    constructor Init( AParent: PWindowsObject; ATitle: PChar; APage: TRptPage;
      aZoom: real );
    destructor Done; virtual;
    function GetZoom: Real;
    procedure SetZoom( newZoom: Real );
    procedure GetOrigin( var pt: TPoint );
    procedure CreateBox( Coordinates: TRect );

    {---- Protected }
    procedure SetupWindow; virtual;
    function GetParentHWnd: HWnd; virtual;
    function GetClassName: PChar; virtual;
    procedure GetWindowClass( var AWndClass: TWndClass ); virtual;
    procedure Paint( dc: HDC; var ps: TPaintStruct ); virtual;
    function  InitBox( rect: TRect; anOrigin: TPoint ): PBoxWin; virtual;
    procedure ShowBoxStatus( aBox: PBoxWin ); virtual;
    procedure ChangePageSize( ANewPage: TRptPage ); virtual;
    procedure ClearAllBoxes; virtual;
    procedure FocusBox( dir: integer ); virtual;
    procedure ShowAllBoxes( title: PChar ); virtual;
    function CanClear: boolean; virtual;
    function CanClose: boolean; virtual;
    { Message handlers }
    procedure wmPaint( var msg: TMessage ); virtual WM_FIRST + WM_PAINT;
    procedure WMLButtonDown( var msg: TMessage ); virtual WM_FIRST +
      WM_LBUTTONDOWN;
    procedure WMRButtonDown( var msg: TMessage ); virtual WM_FIRST +
      WM_RBUTTONDOWN;
    procedure WMLButtonUp( var msg: TMessage ); virtual WM_FIRST +
      WM_LBUTTONUP;
    procedure WMMouseMove( var msg: TMessage ); virtual WM_FIRST +
      WM_MOUSEMOVE;
    procedure WMVScroll( var msg: TMessage ); virtual WM_FIRST + WM_VSCROLL;
    procedure WMHScroll( var msg: TMessage ); virtual WM_FIRST + WM_HSCROLL;
    procedure WMKeyDown( var msg: Tmessage ); virtual WM_FIRST + WM_KEYDOWN;
    procedure WMActivate( var msg: TMessage ); virtual WM_FIRST + WM_ACTIVATE;
    procedure WMSetFocus( var msg: TMessage ); virtual WM_FIRST + WM_SETFOCUS;
    procedure WMKillFocus( var msg: TMessage ); virtual WM_FIRST +
      WM_KILLFOCUS;
    { User defined messages }
    procedure UMRightMouse( var msg: TMessage ); virtual WM_FIRST +
      UM_RIGHTMOUSE;
    procedure UMSetZoom( var msg: Tmessage ); virtual WM_FIRST + UM_SETZOOM;
    { Section messages }
    procedure WMPrefChange( var msg: TMessage ); virtual WM_FIRST +
      UM_PREFERENCECHANGE;
    procedure UMBoxClose( var msg: TMessage ); virtual WM_FIRST +
      UM_BOXCLOSING;
    procedure UMBoxSizeMove( var msg: TMessage ); virtual WM_FIRST +
      UM_BOXSIZEMOVE;
    { Command handlers }
    procedure OnDefine( var msg: TMessage ); virtual CM_FIRST + CM_DEFINE;
    procedure OnAlign( var msg: TMessage ); virtual CM_FIRST + CM_ALIGN;
    procedure OnDelete( var msg: TMessage ); virtual CM_FIRST + CM_DELETE;
    procedure OnClear( var msg: TMessage ); virtual CM_FIRST + CM_CLEAR;
    procedure OnAdd( var msg: TMessage ); virtual CM_FIRST + CM_ADD;
    procedure OnShowAll( var msg: TMessage ); virtual CM_FIRST + CM_SHOWALL;

    {---- Implementation }
    private
    curBox: integer;
    focused: boolean;
    popMenu: HMenu;
    pTracker: PBoxTool;
    Zoom: real;
    ppiX,
    ppiY,
    scrollUnit: integer;  {- one scroll unit in LTwips }
    procedure SetupScroller;
    procedure DefineCurrentBox;
    procedure AlignCurrentBox;
    function GetActiveBox( var wndObj: PBoxWin ): HWnd;
    function NextBox( var wndObj: PBoxWin ): HWnd;
    function PrevBox( var wndObj: PBoxWin ): HWnd;
    function FirstBox( var wndObj: PBoxWin ): HWnd;
  end;


  {--------------------------------------------------------------------------}
  { This object is a subclassed Page Window for use in a dialog box.
  {--------------------------------------------------------------------------}
  PPageDlg = ^TPageDlg;
  TPageDlg = object( TPageWin )
    constructor Init( AParent: PWindowsObject; APage: TRptPage );
    procedure WMKeyDown( var msg: TMessage ); virtual WM_FIRST + WM_KEYDOWN;
    procedure WMChar( var msg: TMessage ); virtual WM_FIRST + WM_CHAR;
    procedure WMGetDlgCode( var msg: TMessage ); virtual WM_FIRST +
      WM_GETDLGCODE;
    procedure WMLButtonDown( var msg: TMessage ); virtual WM_FIRST +
      WM_LBUTTONDOWN;
    function InitBox( rect: TRect; AnOrigin: TPoint ): PBoxWin; virtual;
  end;

IMPLEMENTATION



                        {----------------------}
                        {   Local Functions    }
                        {----------------------}



{----------------------------------------------------------------------------}
{ This function retrieves the up/down state of the key at the time the input
{ message was generated.
{----------------------------------------------------------------------------}

function isDown( VirtKey: Integer ): boolean;
begin
  isDown := GetKeyState( VirtKey ) and $8000 <> 0;
end;


                        {----------------------}
                        {     Page Dialog      }
                        {----------------------}



{----------------------------------------------------------------------------}
{ The constructor calls the base class and then modifies the attributes and
{ style because ...
{----------------------------------------------------------------------------}

constructor TPageDlg.Init( aParent: PWindowsObject; aPage: TRptPage );
begin
  inherited Init( aParent, nil, aPage, 1.0 );
  with attr do
  begin
    x := 0;
    y := 40;
    w := 250;
    h := 85;
    style := WS_GROUP or WS_TABSTOP or WS_CHILD or WS_BORDER or
            WS_VISIBLE or WS_VSCROLL or WS_HSCROLL;
  end;
end;



{----------------------------------------------------------------------------}
{ This procedure responds when the left mouse button is depressed. It sends
{ a special message and then continues to processes the action as usual. The
{ special message is used to ...
{----------------------------------------------------------------------------}

procedure TPageDlg.WMLButtonDown( var msg: TMessage );
begin
  SendMessage( GetParent( HWindow ), WM_NEXTDLGCTL, HWindow, 1 );
  inherited WMLButtonDown( msg );
end;



{----------------------------------------------------------------------------}
{ This function is called when a box needs to be created.
{
{ Parameters:
{   rect - size of the section in device units (pixels)
{   anOrigin - page origin in device pixels.
{----------------------------------------------------------------------------}

function TPageDlg.InitBox( rect: TRect; AnOrigin: TPoint ): PBoxWin;
var
  rFont : TRptFont;
  pstr  : array[ 0..100 ] of char;
  p2    : array[ 0..25 ] of char;
begin
  MakeDefaultRFont( rFont );
  SR( IDS_DEFAULT, pstr, sizeof( pstr ) - 1 );
  Str( Boxes^.count, p2 );
  StrCat( pstr, p2 );
  InitBox := New( PBoxDlg,
    Init( @self, pstr, rect, anOrigin, GetZoom, page, rFont ) );
end;



{----------------------------------------------------------------------------}
{ This procedure responds to WM_GETDLGCODE messages. By responding, it can
{ take control of a particular type of input and process the input itself. In
{ this case, it takes control of all keyboard input so that ...
{----------------------------------------------------------------------------}

procedure TPageDlg.WMGetDlgCode( var msg: TMessage );
begin
  msg.result := DLGC_WANTMESSAGE {or DLGC_UNDEFPUSHBUTTON};
end;



{----------------------------------------------------------------------------}
{ This procedure responds to WM_CHAR messages. By responding, it can detect
{ the ^A character and map it to the CMAlign handler.
{----------------------------------------------------------------------------}

procedure TPageDlg.WMChar( var msg: TMessage );
begin
  if char( msg.wParam ) = ^A then
    OnAlign( msg )
  else
    DefWndProc( msg );
end;



{----------------------------------------------------------------------------}
{ This procedure responds to WM_KEYDOWN messages. Such messagees are sent
{ when a nonsystem key is pressed. This procedure handles Tab (with and
{ without SHIFT), DELETE, INSERT, RETURN, F8, and F5.
{----------------------------------------------------------------------------}

procedure TPageDlg.WMKeyDown( var msg: TMessage );
const
  CtlFocus_NEXT = 0;
  CtlFocus_PREVIOUS = 1;

  {--------------------------------------------------------------------------}
  { This procedure moves focus to the previous box, if any.
  {--------------------------------------------------------------------------}
  procedure FocusPrevious;
  begin
    if curBox > 0 then
      FocusBox( -1 )
    else
      PostMessage( GetParent( HWindow ), WM_NEXTDLGCTL, CtlFocus_PREVIOUS, 0 );
  end;

  {--------------------------------------------------------------------------}
  { This procedure moves focus to the next box, if any.
  {--------------------------------------------------------------------------}
  procedure FocusNext;
  begin
    if curBox < Boxes^.count - 1 then
      FocusBox( 1 )
    else
      PostMessage( GetParent( HWindow ), WM_NEXTDLGCTL, CtlFocus_NEXT, 0 );
  end;


  {--------------------------------------------------------------------------}
  { This procedure invalidates its entire client area, causing it to repaint
  { itself.
  {--------------------------------------------------------------------------}
  procedure Repaint( pBox: PBoxWin ); far;
  const
    ENTIRE_CLIENT = nil;
    ERASE_BACKGROUND = true;
  begin
    InvalidateRect( pBox^.HWindow, ENTIRE_CLIENT, not ERASE_BACKGROUND );
  end;

begin { WMKeyDown }

  case msg.wParam of

    VK_TAB:
      begin
        if isDown( VK_SHIFT ) then
          FocusPrevious
        else
          FocusNext;
        Boxes^.ForEach( @Repaint );
        msg.result := 0;
      end;

    VK_DELETE:
      OnDelete( msg );

    VK_INSERT:
      OnAdd( msg );

    VK_RETURN:
      DefineCurrentBox;

    VK_F8:
      AlignCurrentBox;

    VK_F5:
      begin
        FocusCtl( GetParentHWnd, IDC_ZOOM );
        SendDlgItemMessage( GetParentHWnd, IDC_ZOOM, CB_SHOWDROPDOWN,
          Word( true ), 0 );
      end;

  else
    inherited WMKeyDown( msg );
  end;

end;



                        {----------------------}
                        {      Page Win        }
                        {----------------------}



{----------------------------------------------------------------------------}
{ The constructor initializes the base object and then itself.
{----------------------------------------------------------------------------}

constructor TPageWin.Init( AParent: PWindowsObject; aTitle: PChar;
  aPage: TRptPage; aZoom: Real  );
var
  edge : Integer;
begin
  inherited Init( aParent, aTitle );

  popMenu := LoadMenu( HInstance, MakeIntResource( MENU_RIGHTPOP ) );

  attr.style := attr.style or WS_VSCROLL or WS_HSCROLL;

  scroller := New( PScroller, Init( @self, 0, 0, 0, 0 ) );
  scroller^.autoMode := False;
  scroller^.trackMode := False;

  zoom := aZoom;
  pTracker := nil;  {- box tool = nil }
  scrollUnit := 1440 div 4;  {- make scroll units 0.25 inches }

  {- setup page object with 1/8 inch edges (1440/4) }
  edgeX := 1440 div 8;
  edgeY := edgeX;

  page := aPage;
  Boxes := New( PCollection, Init( 10, 8 ) );
  curBox := -1;

  { Initialize modified flag. }
  IsModified := false;
end;



{----------------------------------------------------------------------------}
{ The destructor cleans up.
{----------------------------------------------------------------------------}

destructor TPageWin.Done;
begin

  {- since the list is just a collection of pointers to section boxes,
     just delete the reference (OWL will destroy the objects) }
  if Boxes <> nil then
    Boxes^.DeleteAll;
  MSDisposeObj( Boxes );

  MSDisposeObj( pTracker );
  inherited Done;
  DestroyMenu( popMenu );
end;



{----------------------------------------------------------------------------}
{ This override adds scroller setup.
{----------------------------------------------------------------------------}

procedure  TPageWin.SetupWindow;
var
  dc   : HDC;
begin
  inherited SetupWindow;

  { Compute ppi }
  dc := GetDC( HWindow );
  ppiX := GetDeviceCaps( dc, LOGPIXELSX );
  ppiY := GetDeviceCaps( dc, LOGPIXELSY );
  ReleaseDC( HWindow, dc );

  { Now that ppis are initialized, the scroller can be set up }
  SetupScroller;
end;



{----------------------------------------------------------------------------}
{ This function returns window handle of parent. The parent here should be
{ the one that get section status messages.  This method is provided instead
{ of GetParent() because of the differing parent nesting levels in this
{ program. i.e., RptEdit is a MDI with a ClientWnd as a parent and a main win
{ as parent of clientwnd. Section editors main win is their direct parent,
{ which is a dialog. By default, this routine returns the direct parent.
{----------------------------------------------------------------------------}

function TPageWin.GetParentHWnd: HWnd;
begin
  GetParentHWnd := GetParent( HWindow );
end;



{----------------------------------------------------------------------------}
{ This procedure returns the position of the origin of the page in device
{ pixels. This is where the left/top margin begin on the screen.
{----------------------------------------------------------------------------}

procedure TPageWin.GetOrigin( var pt: TPoint );
var
  dc  : HDC;
begin
  dc := GetDC( HWindow );
  SetLogicalTwips( dc, zoom );

  pt.x := edgeX + page.margin.left;
  pt.y := edgeY + page.margin.top;

  LpToDP( dc, pt, 1 );  {- convert to device pixels }

  {- account for the scroll position }
  Dec( pt.x, scroller^.XPos * scroller^.XUnit );
  Dec( pt.y, scroller^.YPos * scroller^.YUnit );

  ReleaseDC( HWindow, dc );
end;



{----------------------------------------------------------------------------}
{ This override changes the class name so that ...
{----------------------------------------------------------------------------}

function TPageWin.GetClassName: PChar;
begin
  GetClassName := 'RCFPageWin';
end;



{----------------------------------------------------------------------------}
{ This override modifies the background and icon.
{----------------------------------------------------------------------------}

procedure TPageWin.GetWindowClass( var AWndClass: TWndClass );
begin
  inherited GetWindowClass( AWndClass );
  aWndClass.hbrBackground := GetStockObject( LTGRAY_BRUSH );
  aWndClass.hicon := LoadIcon( HInstance, MakeIntResource( ICON_RPT ) );
end;



{----------------------------------------------------------------------------}
{ This common override redraws the page window.
{----------------------------------------------------------------------------}

procedure TPageWin.Paint( dc: HDC; var ps: TPaintStruct );
var
  sp,
  x,
  LTpPX,
  LTpPY,
  k,
  rptWidth: integer;
  o: HPen;
  r: TRect;
  hb,
  ob: HBrush;
  bdrPen,          { paper border }
  grPen,           { solid gray pen }
  dshGrPen: HPen;  { dashed gray pen }
  lbHatch: TLogBrush;  {- diagonal hatch brush }

begin

  bdrPen := CreatePen( PS_SOLID, 25, RGB( 0, 0, 0 ) );
  grPen := CreatePen( PS_SOLID, 1, RGB( 192, 192, 192 ) );
  dshGrPen := CreatePen( PS_DASH, 1, RGB( 192, 192, 192 ) );

  lbHatch.lbStyle := BS_HATCHED;
  lbHatch.lbColor := RGB( 192, 192, 192 );
  lbHatch.lbHatch := HS_BDIAGONAL;

  with page do
  begin
    LTwipsPerPixel( dc, LTpPX, LTpPY );

    {- draw page rectangle }
    Rectangle( dc, edgeX, edgeY, size.X + edgeX + LTpPX,
      size.Y + edgeY + LTpPX );

    {- draw thick border around right/bottom of page}
    o := Selectobject( dc, bdrPen );
    MoveTo( dc, edgeX + size.X + LTpPX, edgeY + 100 );
    LineTo( dc, edgeX + size.X + LTpPX, edgeY + size.Y + LTpPY );
    LineTo( dc, edgeX + 100, edgeY + size.Y + LTpPY );

    SelectObject( dc, grPen );

    {- draw margins }
    if margin.left > 0 then
    begin
      MoveTo( dc, edgeX + margin.left, edgeY + margin.top );
      LineTo( dc, edgeX + margin.left,
        edgeY + size.Y - margin.bottom + LTpPY );
    end;
    if margin.right > 0 then
    begin
      MoveTo( dc, edgeX + size.X - margin.right, edgeY + margin.top );
      LineTo( dc, edgeX + size.X - margin.right,
        edgeY + size.Y - margin.bottom + LTpPY);
    end;
    if margin.top > 0 then
    begin
      MoveTo( dc, edgeX + margin.left, edgeY + margin.top );
      LineTo(dc, edgeX + size.X - margin.right + LTpPX, edgeY + margin.top );
    end;
    if margin.bottom > 0 then
    begin
      MoveTo(dc, edgeX + margin.left, edgeY + size.Y - margin.bottom );
      LineTo(dc, edgeX + size.X - margin.right + LTpPX,
        edgeY + size.Y - margin.bottom);
    end;

    if rptsPerPage > 1 then
    begin
      {- calc usable size of first report on page }
      rptWidth := ( size.x - margin.left - margin.right -
        ( ( rptsPerPage - 1 ) * page.Spacing ) ) div rptsPerPage;

      sp := page.spacing div 2;

      SetRect( r, edgeX + margin.left + rptWidth + LTpPX,
        edgeY + margin.Top + LTpPY, edgeX + size.x - margin.right,
        edgeY + size.y - margin.bottom );

      hb := CreateBrushIndirect( lbHatch );
      ob := SelectObject( dc, hb );
      FillRect( dc, r, hb );
      DeleteObject( SelectObject( dc, ob ) );

      for k := 1 to rptsPerPage - 1 do
      begin
        x := edgeX + margin.left + k * rptWidth + ( page.spacing * ( k - 1 ) )
          + sp;
        if sp > 0 then
        begin
          SelectObject( dc, dshGrPen );
          MoveTo( dc, x - sp, edgeY + margin.top );
          LineTo( dc, x - sp, edgeY + size.Y - margin.bottom );
          MoveTo( dc, x + sp, edgeY + margin.top );
          LineTo( dc, x + sp, edgeY + size.Y - margin.bottom );
        end;

        SelectObject( dc, GrPen );
        MoveTo( dc, x, edgeY + margin.top );
        LineTo( dc, x, edgeY + size.Y - margin.bottom );
      end;
    end;
    SelectObject( dc, o );
  end;
  DeleteObject( bdrPen );
  DeleteObject( grPen );
  DeleteObject( dshGrPen );
end;



{----------------------------------------------------------------------------}
{ This procedure responds to WM_PAINT messages. It overrides the default
{ response so thay it can set the Logical Twips mapping mode in the device
{ context.
{----------------------------------------------------------------------------}

procedure TPageWin.wmPaint( var msg: TMessage );
var
  dc: HDC;
  ps: TPaintStruct;
begin
  BeginPaint( HWindow, ps );

  SetlogicalTwips( ps.HDC, zoom );

  if scroller <> nil then
    scroller^.BeginView( ps.HDC, ps );

  Paint( ps.HDC, ps );

  if Scroller <> nil then
    scroller^.EndView;

  EndPaint( HWindow, ps );
end;



{----------------------------------------------------------------------------}
{ This procedure sets up a scroller in "device" coordinates since this is
{ how it likes to work. It also takes into account the zoom factor. The
{ scroll units are 0.25 inches which is (1440 / 4) = 360 LTwips. To convert
{ to device coordinates; there are 1440 LTwips/inch and
{ GetDeviceCaps( dc, LOGPIXELS ) pixels/inch. Dividing, we get LTwips/Pixel,
{ which gives us the conversion factor to convert LTwips to device pixels.
{----------------------------------------------------------------------------}

procedure TPageWin.SetupScroller;
var
  LTpPX,             {- LTwips per Pixel (x direction) }
  LTpPY,             {- LTwips per Pixel (y direction) }
  AUnit,
  LTwidth,           {- LTwips width }
  LTHeight: Integer; {- LTwips height }
begin

  { Assert: ppiX and ppiY are initialized }

  LTpPX := 1440 div ppiX;   { Logical Twips per inch / Physical pixels per inch }
  LTpPY := 1440 div ppiY;

  AUnit := scrollUnit;
  {- now AUnit = scroll unit in LTwips, apply zoom factor to get actual scroll
     unit in LTwips }
  AUnit := Trunc( AUnit * zoom );

  {- tell scroller units (converting to device pixels) }
  scroller^.SetUnits( AUnit div LTpPX, AUnit div LTpPY );

  {- caclulate total page height in LTwips }
  LTWidth := Trunc( page.size.X * zoom );
  LTHeight := Trunc( page.size.Y * zoom );

  scroller^.SetRange( LTWidth div AUnit, LTHeight div AUnit );
end;



{----------------------------------------------------------------------------}
{ This function returns the current zoom factor.
{----------------------------------------------------------------------------}

function TPageWin.GetZoom: Real;
begin
  GetZoom := zoom;
end;



{----------------------------------------------------------------------------}
{ This procedure changes the zoom factor and invalidates the window so that
{ it will be repainted using the new zoom factor.
{----------------------------------------------------------------------------}

procedure TPageWin.SetZoom( newZoom: Real );
const
  ENTIRE_CLIENT = nil;
  ERASE_BACKGROUND = true;
var
  Origin: TPoint;

  {--------------------------------------------------------------------------}
  { This procedure passes the zoom change to the specified box.
  {--------------------------------------------------------------------------}
  procedure NotifyZoom( pBox: PBoxWin ); far;
  begin
    pBox^.ChangeZoom( Origin, Zoom );
  end;

begin { SetZoom }

  Zoom := newZoom;
  SetupScroller;
  GetOrigin( Origin );
  Boxes^.ForEach( @notifyZoom );
  InvalidateRect( HWindow, ENTIRE_CLIENT, ERASE_BACKGROUND );
end;



{----------------------------------------------------------------------------}
{ This procedure responds when the left mouse button is depressed in the
{ window. It starts a tracker (see WMLButtonUp and WMMouseMove).
{----------------------------------------------------------------------------}

procedure TPageWin.wmLButtonDown( var msg: TMessage );
var
  pt    : TPoint;
  mx    : TPoint;
  dc    : HDC;
  LTpPX : integer;
  LTpPY : integer;
begin
  if pTracker = nil then
  begin
    GetOrigin( pt );
    with page do
    begin
      dc := GetDC( HWindow );
      SetLogicalTwips( dc, zoom );
      LTwipsPerPixel( dc, LTpPX, LTpPY );

      mx.x := Trunc( ( ( size.X - ( margin.left + margin.right ) ) / LTpPX ) );
      mx.y := Trunc( ( ( size.Y - ( margin.top + margin.bottom ) ) / LTpPY ) );

      ReleaseDC( HWindow, dc );
    end;
    pTracker := New( PBoxTool, Init( @self, msg.lParamLo, msg.lParamHi, pt, mx,
      zoom, preferences.units ) );
  end;
end;



{----------------------------------------------------------------------------}
{ This procedure responds when the left mouse button is released. If the
{ tracker is active (see WMLButtonDown), it is terminated, its coordinates
{ are then passed to the create box procedure.
{----------------------------------------------------------------------------}

procedure TPageWin.WMLButtonUp( var msg: TMessage );
var
  r: TRect;
begin
  if pTracker <> nil then
  begin
    pTracker^.GetRect( r );
    pTracker^.Stop;
    if not IsRectEmpty( r ) then
    begin
      CreateBox( r );
    end;
    MSDisposeObj( pTracker );
  end;
end;



{----------------------------------------------------------------------------}
{ This procedure responds when the mouse moves. It updates the tracker if it
{ is active.
{----------------------------------------------------------------------------}

procedure TPageWin.wmMouseMove( var msg: TMessage );
begin
  if ( pTracker <> nil ) and not pTracker^.stopped then
    pTracker^.MouseMove( Integer( msg.lParamLo ), Integer( msg.lParamHi ) );
end;



{----------------------------------------------------------------------------}
{ This procedure creates a box with specified coordinates, in device pixels.
{----------------------------------------------------------------------------}

procedure TPageWin.CreateBox( Coordinates: TRect );
var
  pBox: PBoxWin;
  pt: TPoint;
begin
  GetOrigin( pt );  {- get current page origin }
  pBox := InitBox( Coordinates, pt );
  if pBox <> nil then
  begin
    Boxes^.Insert( pBox );
    curBox := Boxes^.IndexOf( pBox );
    application^.MakeWindow( pBox );
    SetFocus( pBox^.HWindow );
    UpdateFocusChild;
    IsModified := true;
  end;
end;



{----------------------------------------------------------------------------}
{ This virtual function initializes the box for the CreateBox procedure.
{ In this default implementation, it creates a box window with a default
{ title including a box number.
{
{ Parameters
{   rect - size of the box in device units (pixels)
{   anOrigin - page origin in device pixels
{----------------------------------------------------------------------------}

function TPageWin.InitBox( rect: TRect; AnOrigin: TPoint ): PBoxWin;
var
  rFont: TRptFont;
  Title: array[ 0..100 ] of char;
  Count: array[ 0..25 ] of char;
begin
  MakeDefaultRFont( rFont );
  SR( IDS_DEFAULT, Title, SizeOf( Title ) - 1 );
  Str( Boxes^.count, Count );
  StrCat( Title, Count );
  InitBox := New( PBoxWin,
    Init( @self, Title, rect, anOrigin, zoom, page, rFont ) );
end;



{----------------------------------------------------------------------------}
{ This procedure responds to WM_VSCROLL messages. It coordinates the scroll
{ bars and boxes during a scroll operation.
{----------------------------------------------------------------------------}

procedure TPageWin.WMVScroll( var msg: TMessage );
var
  Change: boolean;
  origin: TPoint;

  {--------------------------------------------------------------------------}
  { This procedure notifies the box that a change is about to take place
  { or that a change has taken place, depending on the change flag.
  {--------------------------------------------------------------------------}
  procedure FlagSections( p: PBoxWin ); far;
  begin
    p^.inChange := Change;
  end;

  {--------------------------------------------------------------------------}
  { This procedure notifies the box of the new origin.
  {--------------------------------------------------------------------------}
  procedure NotifySections( p: PBoxWin ); far;
  begin
    p^.ChangeOrigin( origin );
  end;

begin { WMVScroll }

  Change := true;
  Boxes^.ForEach( @FlagSections );
  inherited WMVScroll( msg );
  GetOrigin( origin );
  Change := false;
  Boxes^.ForEach( @FlagSections );
  Boxes^.ForEach( @NotifySections );
end;



{----------------------------------------------------------------------------}
{ This procedure responds to WM_HSCROLL messages. It coordinates the scroll
{ bars and boxes during a scroll operation.
{----------------------------------------------------------------------------}

procedure TPageWin.WMHScroll( var msg: TMessage );
var
  Change: boolean;
  origin: TPoint;

  {--------------------------------------------------------------------------}
  { This procedure notifies the box that a change is about to take place
  { or that a change has taken place, depending on the change flag.
  {--------------------------------------------------------------------------}
  procedure FlagSections( p: PBoxWin ); far;
  begin
    p^.inChange := Change;
  end;

  {--------------------------------------------------------------------------}
  { This procedure notifies the box of the new origin.
  {--------------------------------------------------------------------------}
  procedure NotifySections( p: PBoxWin ); far;
  begin
    p^.ChangeOrigin( origin );
  end;

begin { WMHScroll }

  Change := true;
  Boxes^.ForEach( @FlagSections );
  inherited WMHScroll( msg );
  GetOrigin( origin );
  Change := false;
  Boxes^.ForEach( @FlagSections );
  Boxes^.ForEach( @notifySections );
end;



{----------------------------------------------------------------------------}
{ This procedure responds application defined UM_BOXCLOSING messages. A box
{ will send this message just before it deletes itself. This procedure
{ removes it from the collection and adjusts the focus as needed.
{----------------------------------------------------------------------------}

procedure TPageWin.UMBoxClose( var msg: TMessage );

  {--------------------------------------------------------------------------}
  { This function returns true if the item is the one noted in the message.
  {--------------------------------------------------------------------------}
  function Matches( item: PBoxWin ): boolean; far;
  begin
    Matches := item^.HWindow = msg.wParam;
  end;

var
  pBox: PBoxWin;

begin { UMBoxClose }

  pBox := Boxes^.FirstThat( @matches );
  if pBox <> nil then
  begin
    Boxes^.Delete( pBox );  {- remove section from section list }
  end;
  if FirstBox( pBox ) > 0 then {- focus first box }
  begin
    SetFocus( pBox^.HWindow );
    UpdateFocusChild;
  end
  else
  begin
    ShowBoxStatus( nil );  {- no boxes, clear status line }
    focusChildHandle := 0;
    SetFocus( HWindow );
  end;
end;



{----------------------------------------------------------------------------}
{ This procedure responds to UM_BOXSIZEMOVE messages. It ...
{----------------------------------------------------------------------------}

procedure TPageWin.UMBoxSizeMove( var msg: TMessage );
var
  pBox: PBoxWin;

begin
  pBox := PBoxWin( msg.lParam );
  if pBox <> nil then
    curBox := Boxes^.IndexOf( pBox )
  else
    curBox := -1;
  ShowBoxStatus( pBox );
  UpdateFocusChild;
  IsModified := true;
end;



{----------------------------------------------------------------------------}
{ This procedure sends a status message to the parent for display in the
{ status bar. If aBox is nil the section status should be cleared.
{----------------------------------------------------------------------------}

procedure TPageWin.ShowBoxStatus( aBox: PBoxWin );
var
  num     : Real;
  pstr    : array[ 0..100 ] of char;
  p2      : array[ 0..100 ] of char;
  r       : TLRect;
  nums    : array[ 0..4 ] of array[ 0..25 ] of char;
  subst   : PErrSubst;
begin
  StrCopy( pstr, '' );
  if aBox <> nil then
  begin
    with aBox^ do
    begin
      SR(IDS_STATMSG, p2, sizeof( p2 ) - 1 );
      GetSize( r );

      LTwipsToUnits( r.left, num );
      IntlRealToStr( num, pstr, SizeOf( pstr ) - 1 );
      subst := New( PErrSubst, Init( pstr ) );

      LTwipsToUnits( r.top, num );
      IntlRealToStr( num, pstr, SizeOf( pstr ) - 1 );
      subst^.AddSubstStr( pstr );

      LTwipsToUnits( r.right, num );
      IntlRealToStr( num, pstr, SizeOf( pstr ) - 1 );
      subst^.AddSubstStr( pstr );

      LTwipsToUnits( r.bottom, num );
      IntlRealToStr( num, pstr, SizeOf( pstr ) - 1 );
      subst^.AddSubstStr( pstr );

      UnitsStr( true, pstr, SizeOf( pstr ) - 1 );
      subst^.AddSubstStr( pstr );

      WVSPrintf( pstr, p2, subst^.strs );

      MSDisposeObj( subst );
    end;
  end;

  {- pstr is NOT persistent here, receiver of message should
     make a local copy of the string passed!!!}
  SendMessage( GetParentHWnd, UM_BOXSTATUS, 0, longint( @pstr ) );
end;



{----------------------------------------------------------------------------}
{ This function returns the handle of the active box, if none are active or
{ none are defined, then it returns 0. The wndObj parameter is returned as
{ the PWindowsObject pointer to the returned box or nil if no boxes are
{ active.
{----------------------------------------------------------------------------}

function TPageWin.GetActiveBox( var wndObj: PBoxWin ): HWnd;
begin
  if ( Boxes^.Count = 0 ) or ( curBox = -1 ) then
  begin
    wndObj := nil;
    GetActiveBox := 0
  end
  else
  begin
    wndObj := Boxes^.At( curBox );
    GetActiveBox := wndObj^.HWindow;
  end;
end;



{----------------------------------------------------------------------------}
{ This function returns the first box window. It returns 0 if no boxes are
{ defined.
{----------------------------------------------------------------------------}

function TPageWin.FirstBox( var wndObj: PBoxWin ): HWnd;
begin
  if Boxes^.count = 0 then
  begin
    FirstBox := 0;
    wndObj := nil;
  end
  else
  begin
    wndObj := Boxes^.At( 0 );
    FirstBox := wndObj^.HWindow;
  end;
end;



{----------------------------------------------------------------------------}
{ This procedure returns the next box after the box passed (wndObj). If the
{ last box is specified in wndObj or there are not boxes then NextBox will
{ return 0. If wndObj = nil or no box is focused then the first box will be
{ returned.
{----------------------------------------------------------------------------}

function TPageWin.NextBox( var wndObj: PBoxWin ): HWnd;
var
  k: integer;
begin
  if ( Boxes^.count = 0 ) or ( curBox = ( Boxes^.count - 1 ) ) then
  begin
    wndObj := nil;
    NextBox := 0;
  end
  else if ( wndObj = nil ) or ( curBox = -1 ) then
    NextBox := FirstBox( wndObj )
  else
  begin
    k := curBox;
    Inc( k );
    wndObj := Boxes^.At( k );
    NextBox := wndObj^.HWindow;
  end;
end;



{----------------------------------------------------------------------------}
{ This function returns the previous box from the box passed (wndObj). If the
{ first box is specified in wndObj or there are not boxes then PrevBox will
{ return 0. If wndObj = nil or no box is focused then the first box will be
{ returned.
{----------------------------------------------------------------------------}

function TPageWin.PrevBox( var wndObj: PBoxWin ): HWnd;
var
  k  : integer;
begin
  if ( Boxes^.count = 0 ) or ( curBox = 0 ) then
  begin
    wndObj := nil;
    PrevBox := 0;
  end
  else if ( wndObj = nil ) or ( curBox = -1 ) then
    PrevBox := FirstBox( wndObj )
  else
  begin
    k := curBox;
    Dec( k );
    wndObj := Boxes^.At( k );
    PrevBox := wndObj^.HWindow;
  end;
end;



{----------------------------------------------------------------------------}
{ This procedure handles a preference changes message (from main window).
{----------------------------------------------------------------------------}

procedure TPageWin.WMPrefChange( var msg: TMessage );
var
  pBox: PBoxWin;
begin
  GetActiveBox( pBox );
  if pBox <> nil then
    ShowBoxStatus( pBox )
  else
    ShowBoxStatus( nil );
end;



{----------------------------------------------------------------------------}
{ This procedure changes the page size.
{----------------------------------------------------------------------------}

procedure TPageWin.ChangePageSize( aNewPage: TRptPage );
begin
  page := aNewPage;
  SetupScroller;
  InvalidateRect( HWindow, nil, true );
  IsModified := true;
end;



{----------------------------------------------------------------------------}
{ This procedure focus a box window.
{
{ Parameters:
{   dir - specifies what to focus.
{     -1  focus previous
{      0  focus current
{      1  focus next
{----------------------------------------------------------------------------}

procedure TPageWin.FocusBox( dir: integer );
var
  h: HWnd;
  w: PBoxWin;
begin
  h := GetActiveBox( w );
  if dir < 0 then
    h := PrevBox( w )
  else if dir > 0 then
    h := NextBox( w );
  if h > 0 then
  begin
    SetFocus( h );
    UpdateFocusChild;
  end;
end;



{----------------------------------------------------------------------------}
{ This procedure responds to WM_KEYDOWN messages. Such messagees are sent
{ when a nonsystem key is pressed. This procedure handles varoius keys.
{----------------------------------------------------------------------------}

procedure TPageWin.WMKeyDown( var msg: TMessage );
begin
  msg.result := 0;
  with scroller^ do
  begin
    case msg.wParam of

      VK_TAB:
        if isDown( VK_SHIFT ) then
          FocusBox( -1 )  {- previous box }
        else
          FocusBox( 1 );  {- next box }

      VK_NEXT:
        ScrollBy( 0, YPage );

      VK_PRIOR:
        ScrollBy( 0, -XPage );

      VK_DOWN:
        if isDown( VK_CONTROL ) then
          ScrollBy( 0, YPage )
        else
          ScrollBy( 0, YLine );

      VK_UP:
        if isDown( VK_CONTROL ) then
          ScrollBy( 0, -YPage )
        else
          ScrollBy( 0, -YLine );

      VK_LEFT:
        if isDown( VK_CONTROL ) then
          ScrollBy( -XPage, 0 )
        else
          ScrollBy( -XLine, 0 );

      VK_RIGHT:
        if isDown( VK_CONTROL ) then
          ScrollBy( XPage, 0 )
        else
          ScrollBy( XLine, 0 );

    else
      DefWndProc( msg );
    end;
  end;
end;



{----------------------------------------------------------------------------}
{ This procedure processes WM_ACTIVATE messages.
{----------------------------------------------------------------------------}

procedure TPageWin.WMActivate( var msg: TMessage );
begin
  inherited WMActivate( msg );
  FocusChild;
end;



{----------------------------------------------------------------------------}
{ This procedure processes WM_KILLFOCUS messages.
{----------------------------------------------------------------------------}

procedure TPageWin.WMKillFocus( var msg: TMessage );
begin
  DefWndProc( msg );
  focused := false;
end;



{----------------------------------------------------------------------------}
{ This procedure processes WM_SETFOCUS messages.
{----------------------------------------------------------------------------}

procedure TPageWin.WMSetFocus( var msg: TMessage );
begin
  focused := true;
  DefWndProc( msg );
  FocusChild;
end;



{----------------------------------------------------------------------------}
{ This procedure closes all box windows.
{----------------------------------------------------------------------------}

procedure TPageWin.ClearAllBoxes;
var
  pBox: PBoxWin;
begin
  while Boxes^.count > 0 do
  begin
    pBox := Boxes^.At( 0 );
    pBox^.CloseWindow;
  end;
  IsModified := true;
end;



{----------------------------------------------------------------------------}
{ This procedure responds when the right mouse button is depressed by opening
{ a context menu.
{----------------------------------------------------------------------------}

procedure TPageWin.WMRButtonDown( var msg: TMessage );
var
  h: hmenu;
begin
  SetFocus( HWindow );
  h := GetSubMenu( popMenu, 0 );
  ClientToScreen( HWindow, TPoint( msg.lParam ) );
  EnableMenuItem( h, CM_ALIGN, MF_GRAYED or MF_DISABLED or MF_BYCOMMAND );
  EnableMenuItem( h, CM_DEFINE, MF_GRAYED or MF_DISABLED or MF_BYCOMMAND );
  EnableMenuItem( h, CM_DELETE, MF_GRAYED or MF_DISABLED or MF_BYCOMMAND );
  EnableMenuItem( h, CM_ADD, MF_BYCOMMAND );
  TrackPopupMenu( h, TPM_LEFTALIGN or TPM_RIGHTBUTTON, msg.lParamLo,
    msg.lParamHi, 0, GetParentHWnd, nil );
end;



{----------------------------------------------------------------------------}
{ This procedure responds to application defined UM_RIGHTMOUSE messages by
{ opening a context menu.
{----------------------------------------------------------------------------}

procedure TPageWin.UMRightMouse( var msg: TMessage );
var
  h: hmenu;
begin
  h := GetSubMenu( popMenu, 0 );
  EnableMenuItem( h, CM_ALIGN, MF_BYCOMMAND );
  EnableMenuItem( h, CM_DEFINE, MF_BYCOMMAND );
  EnableMenuItem( h, CM_DELETE, MF_BYCOMMAND );
  TrackPopupMenu( h, TPM_LEFTALIGN or TPM_RIGHTBUTTON, msg.lParamLo,
    msg.lParamHi, 0, HWindow, nil );
end;



{----------------------------------------------------------------------------}
{ This procedure aligns the active box by calling the AlignBoxPrim procedure.
{ If none are active, it notifies the user.
{----------------------------------------------------------------------------}

procedure TPageWin.AlignCurrentBox;
var
  pBox: PBoxWin;
  pstr,
  p2: array[ 0..100 ] of char;
  r: TLRect;
begin
  pBox := nil;
  if GetActiveBox( pBox ) = 0 then
    if FirstBox( pBox ) = 0 then
      ErrorMsg( GetParentHWnd, SR( IDS_ERROR, p2, SizeOf( p2 ) - 1 ),
        SR( IDS_NOBOX, pstr, SizeOf( pstr ) - 1 ) );
  if pBox <> nil then
  begin
    pBox^.AlignBoxPrim;
    IsModified := true;
  end;
end;



{----------------------------------------------------------------------------}
{ This procedure defines the active box by calling the DefineBoxPrim
{ procedure. If none are active, it notifies the user.
{----------------------------------------------------------------------------}

procedure TPageWin.DefineCurrentBox;
var
  pBox: PBoxWin;
  NoBox,
  Error: array[ 0..100 ] of char;
begin
  pBox := nil;
  if GetActiveBox( pBox ) = 0 then
    if FirstBox( pBox ) = 0 then
      ErrorMsg( GetParentHWnd, SR( IDS_ERROR, Error, SizeOf( Error ) - 1 ),
        SR( IDS_NOBOX, NoBox, SizeOf( NoBox ) - 1 ) );
  if pBox <> nil then
  begin
    pBox^.DefineBoxPrim;
    IsModified := true;
  end;
end;



{----------------------------------------------------------------------------}
{ This procedure handles the add command.
{----------------------------------------------------------------------------}

procedure TPageWin.OnAdd( var msg: TMessage );
var
  r2: TRect;
begin
  SetRect( r2, 0, 0, 100, 100 );
  CreateBox( r2 );
end;



{----------------------------------------------------------------------------}
{ This procedure handles the define command.
{----------------------------------------------------------------------------}

procedure TPageWin.OnDefine( var msg: TMessage );
begin
  DefineCurrentBox;
end;



{----------------------------------------------------------------------------}
{ This procedure handles the align command.
{----------------------------------------------------------------------------}

procedure TPageWin.OnAlign( var msg: TMessage );
begin
  AlignCurrentBox;
end;



{----------------------------------------------------------------------------}
{ This procedure handles the delete command.
{----------------------------------------------------------------------------}

procedure TPageWin.OnDelete( var msg: TMessage );
var
  pBox: PBoxWin;
begin
  if GetActiveBox( pBox ) <> 0 then
  begin
    pBox^.DeleteBoxPrim;
    IsModified := true;
  end;
end;



{----------------------------------------------------------------------------}
{ This procedure handles the clear command.
{----------------------------------------------------------------------------}

procedure TPageWin.OnClear( var msg: TMessage );
begin
  ClearAllBoxes;
end;



{----------------------------------------------------------------------------}
{ This procedure handles set zoom messages.
{----------------------------------------------------------------------------}

procedure TPageWin.UMSetZoom( var msg: TMessage );
begin
  SetZoom( msg.lParam / 100.0 );
end;



{----------------------------------------------------------------------------}
{ This procedure handles Show All commands. It executes the virtual ShowAll
{ method for the contained page window.
{----------------------------------------------------------------------------}

procedure TPageWin.OnShowAll( var msg: TMessage );
var
  pstr: array[ 0..100 ] of char;
begin
  ShowAllBoxes( SR( IDS_DEFBOXES, pstr, SizeOf( pstr ) - 1 ) );
end;



{----------------------------------------------------------------------------}
{ This procedure brings up the show all dialog. If the user selects a box,
{ the box will receive the focus.
{----------------------------------------------------------------------------}

procedure TPageWin.ShowAllBoxes( title: PChar );
var
  pSADialog: PDialog;
  idx: integer;
  box: PBoxWin;
begin
  pSADialog := New( PShowAllDlg, Init( parent, title, Boxes, idx ) );
  if application^.ExecDialog( pSADialog ) = IDOK then
  begin
    if idx <> -1 then
    begin
      box := Boxes^.At( idx );
      SetFocus( box^.HWindow );
      UpdateFocusChild;
    end;
  end;
end;



{----------------------------------------------------------------------------}
{ This override of the CanClose function adds the CanClear functionality.
{----------------------------------------------------------------------------}

function TPageWin.CanClose: boolean;
begin
  CanClose := CanClear;
end;



{----------------------------------------------------------------------------}
{ This function returns true if it is Ok to clear the contents of the
{ window. In this default implementation, it always returns true. Override
{ using the IsModifies attribute to implement a save feature.
{----------------------------------------------------------------------------}

function TPageWin.CanClear: boolean;
begin
  CanClear := true;
end;


END.