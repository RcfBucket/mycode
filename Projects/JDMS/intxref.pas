Unit IntXref;

Interface

Uses
  WinTypes,
  Objects,
  ODialogs,
  OWindows,
  Strings,
  WinCrt,
  IntGlob,
  FileIO,
  BtrAPIW,
  WinProcs;

const
  id_List = 101;
  id_Add = 102;
  id_Delete = 103;
  id_Change = 104;
	id_EntrySave = 105;
  id_AltListSort = 106;
	id_Static = 107;
  id_ToMicroScan = 108;
  id_ToInterface = 109;
  id_Direction = 110;

  id_MnemEdit = 101;
  id_AltEdit = 102;
  id_Another = 103;
  id_Done = 104;
  id_Cancel = 105;
  str_ConflictingXref = 238;
  str_ConfirmXref=239;


  XRefMnemFile = 14;

  ExpandChr: array[0..1] of Char = Chr($2B) + #0;
  ShrinkChr: array[0..1] of Char = Chr($2D) + #0;
  NoAltsChr: array[0..1] of Char = Chr($20) + #0;

  TrueStr: array[0..2] of char = 'T';
	FalseStr: array[0..2] of char = 'F';

Type
  PXRefListDlg = ^TXRefListDlg;
  TXRefListDlg = object(TDialog)
    XRefName: array[0..30] of char;
    XRefNameCtl: PStatic;
    procedure EntrySave(var Msg: TMessage);virtual id_First + id_EntrySave;
    procedure FillList;virtual;
    procedure AddToLists(var Msg: TMessage);virtual id_First + id_Add;
		procedure DeleteFromList(var Msg: TMessage);virtual id_First + id_Delete;
    procedure ChangeItem(var Msg: TMessage);virtual id_First + id_Change;
    constructor Init(ParentWnd: PWindowsObject; AppName, TableTitle: PChar;MType:char);
    procedure SetupWindow;virtual;
		procedure DeleteSelection(Index: Integer);virtual;
		procedure SaveCfgRad;virtual;
		procedure SelectToMicroScan(var Msg: TMessage);virtual id_First + id_ToMicroScan;
    procedure SelectToInterface(var Msg: TMessage);virtual id_First + id_ToInterface;
  end;

  PXRefEntryDlg = ^TXRefEntryDlg;
  TXRefEntryDlg = object(TDialog)
    procedure AddAnotherItem(var Msg: TMessage);virtual id_First + id_Another;
    procedure SaveEntry(var Msg: TMessage);virtual id_First + id_Done;
    procedure Abort(var Msg: TMessage);virtual id_First + id_Cancel;
		procedure SaveCurrentEntry;virtual;
    procedure SetupWindow;virtual;
		procedure CreateNewItem;virtual;
		procedure AddToExistingItem(Direction: Boolean);virtual;
    function IsBlankFields: Boolean;virtual;
  end;

  PXRefChangeDlg = ^TXRefChangeDlg;
  TXRefChangeDlg = object(TDialog)
		OldText: array[0..MaxAltLen] of char;
    procedure SaveChanges(var Msg: TMessage);virtual id_First + id_Done;
    procedure AbortChange(var Msg: TMessage);virtual id_First + id_Cancel;
    procedure SetUpWindow;virtual;
  end;

	IntXRefCfgType = Record
										 ToMScanRad: Word;
                     ToIntRad: Word;
									 end;

var
  XRefListDlg: PXRefListDlg;
  XRefCfgBuff: IntXRefCfgType;
	ToMicroScan: Boolean;
  StatusCol: integer;
	MnemType: array[0..2] of char;

procedure IntXRef_ExecXRefEntry(ParentWnd: HWnd; ParentWndObj: PWindowsObject; MenuSel: Integer);


Implementation

{
********************
Non-Object Functions
********************
}

function FillListString(PrimText, SecText: PChar): PChar;
{
Set up ListText with text for display in list box.
}
var
  i, j: integer;
  ListText: array[0..MaxListLen] of Char;
  MnemEditText: array[0..MaxMnemLen] of Char;
  AltEditText: array[0..MaxAltLen] of Char;

begin
  if ToMicroScan then
  begin
    StrCopy(MnemEditText, PrimText);
    StrCopy(AltEditText, SecText);
  end
  else
  begin
    StrCopy(MnemEditText, SecText);
    StrCopy(AltEditText, PrimText);
  end;
  StrCopy(ListText, MnemEditText);
  for i := StrLen(MnemEditText)+MnemCol to AltCol-1 do
    ListText[i] := ' ';
  ListText[i+1] := #0;
  StrCat(ListText, AltEditText);
  FillListString := ListText;
end;

procedure SeperateMnemAlt(Index: integer; MnemEditText, AltEditText: PChar);
{
Get Mnemonic and Alternate from listbox selection.
}
var
  i, j: integer;
  ListText: array[0..MaxListLen] of char;

begin
  XRefListDlg^.SendDlgItemMsg(id_List, lb_GetText, Index, longint(@ListText));
	i := MnemCol;
	j := 0;
 	while (ListText[i] <> ' ') and
   	    (i <= MnemCol+MaxMnemLen-1) do
 	begin
   	MnemEditText[j] := ListText[i];
   	j := j + 1;
   	i := i + 1;
 	end;
 	MnemEditText[j] := #0;
	j := 0;
  i := AltCol;
	while (ListText[i] <> #0) and
				(i <= AltCol+MaxAltLen-1) do
	begin
	  AltEditText[j] := ListText[i];
	  j := j + 1;
    i := i + 1;
	end;
  AltEditText[j] := #0;
end;

{
******************
XRefListDlg Object
******************
}

constructor TXRefListDlg.Init(ParentWnd: PWindowsObject; AppName, TableTitle: PChar;MType: char);
var
	RadioPtr: PRadioButton;
  i, j: integer;

begin
  inherited Init(ParentWnd, AppName);
  j := 0;
	for i := 0 to StrLen(TableTitle) do
  begin
    if TableTitle[i] <> '&' then
    begin
  	  XRefName[j] := TableTitle[i];
      j := j + 1;
    end;
	end;
	XRefName[j] := #0;
	MnemType[0] := MType;
	MnemType[1] := #0;
  New(RadioPtr, InitResource(@Self, id_ToMicroScan));
  New(RadioPtr, InitResource(@Self, id_ToInterface));
end;

procedure TXRefListDlg.SetupWindow;
{ Fill list box with mnemonic and alternates for that particular
  mnemonic type.
}
var
  ListFont: HFont;
begin
  inherited SetupWindow;
  SendDlgItemMsg(id_Static, wm_SetText, 0, longint(@XRefName));
  ListFont := GetStockObject(SYSTEM_FIXED_FONT);
  SendDlgItemMsg(id_List, wm_SetFont, ListFont, 0);
	FillList;
end;

procedure TXRefListDlg.SelectToMicroScan(var Msg: TMessage);
begin
	ToMicroScan := True;
  SendDlgItemMsg(id_List, wm_SetRedraw, 0, 0);
  SendDlgItemMsg(id_List, lb_ResetContent, 0, 0);
  SendDlgItemMsg(id_AltListSort, lb_ResetContent, 0, 0);
	FillList;
  SendDlgItemMsg(id_List, wm_SetRedraw, 1, 0);
  InvalidateRect(GetDlgItem(HWindow, id_List), nil, True);
end;

procedure TXRefListDlg.SelectToInterface(var Msg: TMessage);
begin
	ToMicroScan := False;
  SendDlgItemMsg(id_List, wm_SetRedraw, 0, 0);
  SendDlgItemMsg(id_List, lb_ResetContent, 0, 0);
	FillList;
  SendDlgItemMsg(id_List, wm_SetRedraw, 1, 0);
  InvalidateRect(GetDlgItem(HWindow, id_List), nil, True);
end;

procedure TXRefListDlg.FillList;
{
Initialize list with primary items.
}
var
  PrimText: array[0..MaxAltLen] of Char;
  SecText: array[0..MaxAltLen] of Char;
  ListText: array[0..MaxListLen] of Char;
  ListFont: HFont;
	Index: integer;
	MnemPrimRec: TMnemPrimRec;
	AltPrimRec: TAltPrimRec;
	MnemPrimKey: TMnemPrimKey;
	AltPrimKey: TAltPrimKey;
	BtrvStat: integer;

begin
  if ToMicroScan then
  begin
    BtrvStat := CallDB(B_Get_Low, AltPrimFile, AltPrimRec.Start, AltPrimKey.Start, 0);
    while BtrvStat = 0 do
    begin
      if (StrComp(MnemType, AltPrimRec.MType) = 0) and
         (StrComp(TrueStr, AltPrimRec.IsPrim) = 0) then
      begin
        StrCopy(SecText, AltPrimRec.Alternate);
        StrCopy(PrimText, AltPrimRec.Mnemonic);
        StrCopy(ListText, FillListString(PrimText, SecText));
        Index := SendDlgItemMsg(id_AltListSort, lb_AddString, 0, longint(@SecText));
        SendDlgItemMsg(id_List, lb_InsertString, Index, longint(@ListText));
      end;
      BtrvStat := CallDB(B_Next, AltPrimFile, AltPrimRec.Start, AltPrimKey.Start, 0);
    end;
  end
  else
  begin
    BtrvStat := CallDB(B_Get_Low, MnemPrimFile, MnemPrimRec.Start, MnemPrimKey.Start, 0);
    while BtrvStat = 0 do
    begin
      if (StrComp(MnemType, MnemPrimRec.MType) = 0) and
         (StrComp(TrueStr, MnemPrimRec.IsPrim) = 0) then
      begin
        StrCopy(PrimText, MnemPrimRec.Alternate);
        StrCopy(SecText, MnemPrimRec.Mnemonic);
        StrCopy(ListText, FillListString(PrimText, SecText));
        SendDlgItemMsg(id_List, lb_AddString, 0, longint(@ListText));
      end;
      BtrvStat := CallDB(B_Next, MnemPrimFile, MnemPrimRec.Start, MnemPrimKey.Start, 0);
    end;
  end;
end;

procedure TXRefListDlg.ChangeItem(var Msg: TMessage);
var
  XRefChangeDlg: PXRefChangeDlg;
  TempMsg: array[0..40] of char;
  TempVerMsg: array[0..30] of char;

begin
  if SendDlgItemMsg(id_List, lb_GetCurSel, 0, 0) <> lb_Err then
  begin
    XRefChangeDlg := New(PXRefChangeDlg, Init(@Self, 'INT_XREF_CHANGE'));
    Application^.ExecDialog(XRefChangeDlg);
  end
  else
  begin
    LoadString(HInstance, str_IntBase+str_EntrySel, TempMsg, SizeOf(TempMsg));
    LoadString(HInstance, str_IntBase+str_IntMsg, TempVerMsg, SizeOf(TempVerMsg));
    MessageBox(HWindow, TempMsg, TempVerMsg, mb_ok);
  end;
end;

procedure TXRefListDlg.DeleteFromList(var Msg: TMessage);
const
  CRLF: array[0..2] of char = (#13, #10, #0);

var
  Index, i: Integer;
  Note: array[0..150] of char;
  LastCursor: HCursor;
	MnemPrimRec: TMnemPrimRec;
	AltPrimRec: TAltPrimRec;
	MnemPrimKey: TMnemPrimKey;
	AltPrimKey: TAltPrimKey;
	BtrvStat: integer;

  AltSearchKey: TAltSearchKey;
  TempMsg: array[0..60] of char;
  TempVerMsg: array[0..30] of char;

begin
  Index := SendDlgItemMsg(id_List, lb_GetCurSel, 0, 0);
  if Index <> lb_Err then { item selected }
  begin
    LoadString(HInstance, str_IntBase+str_OKDel, TempMsg, SizeOf(TempMsg));
    LoadString(HInstance, str_IntBase+str_IntMsg, TempVerMsg, SizeOf(TempVerMsg));
    if MessageBox(HWindow, TempMsg, TempVerMsg,
			mb_YesNo+mb_IconQuestion) = id_Yes then
				DeleteSelection(Index);
	end
	else { delete entire list }
  begin
    LoadString(HInstance, str_IntBase+str_DelSingle1, TempMsg, SizeOf(TempMsg));
    StrCopy(Note, TempMsg);
    StrCat(Note, #13#10);

    LoadString(HInstance, str_IntBase+str_DelSingle2, TempMsg, SizeOf(TempMsg));
    StrCat(Note, TempMsg);
    StrCat(Note, #13#10#13#10);

    LoadString(HInstance, str_IntBase+str_DelAll, TempMsg, SizeOf(TempMsg));
    StrCat(Note, TempMsg);
    LoadString(HInstance, str_IntBase+str_IntMsg, TempVerMsg, SizeOf(TempVerMsg));
    if MessageBox(HWindow, Note, TempVerMsg,
      mb_YesNo+mb_IconQuestion) = id_Yes then
    begin
      LastCursor := SetCursor(LoadCursor(0, idc_Wait));
      BtrvStat := CallDB(B_Get_Low, MnemPrimFile, MnemPrimRec.Start, MnemPrimKey.Start, 0);
			while BtrvStat = 0 do
			begin
        if StrComp(MnemType, MnemPrimRec.MType) = 0 then
          BtrvStat := CallDB(B_Delete, MnemPrimFile, MnemPrimRec.Start, MnemPrimKey.Start, 0);
        BtrvStat := CallDB(B_Next, MnemPrimFile, MnemPrimRec.Start, MnemPrimKey.Start, 0);
      end;
      BtrvStat := CallDB(B_Get_Low, AltPrimFile, AltPrimRec.Start, AltPrimKey.Start, 0);
			while BtrvStat = 0 do
			begin
        if StrComp(MnemType, AltPrimRec.MType) = 0 then
          BtrvStat := CallDB(B_Delete, AltPrimFile, AltPrimRec.Start, AltPrimKey.Start, 0);
        BtrvStat := CallDB(B_Next, AltPrimFile, AltPrimRec.Start, AltPrimKey.Start, 0);
			end;
      SendDlgItemMsg(id_List, lb_ResetContent, 0, 0);
      SendDlgItemMsg(id_AltListSort, lb_ResetContent, 0, 0);
      SetCursor(LastCursor);
    end;
  end;
end;

procedure TXRefListDlg.DeleteSelection(Index: Integer);
var
  SelIndex: integer;
  MnemEditText, OldMnemText: array[0..MaxMnemLen] of Char;
  AltEditText, PrimText, OldAltText: array[0..MaxAltLen] of Char;
	ListText: array[0..MaxListLen] of Char;
  AltPrimEntry, MnemPrimEntry: array[0..2] of char;
	MnemPrimRec: TMnemPrimRec;
	AltPrimRec: TAltPrimRec;
	MnemPrimKey: TMnemPrimKey;
	AltPrimKey: TAltPrimKey;
	MnemSearchKey: TMnemSearchKey;
  AltSearchKey: TAltSearchKey;
  BtrvStat: integer;

begin
  SeperateMnemAlt(Index, MnemEditText, AltEditText);
  StrCopy(MnemPrimKey.MType, MnemType);
  StrCopy(MnemPrimKey.Mnemonic, MnemEditText);
  StrCopy(MnemPrimKey.Alternate, AltEditText);
  BtrvStat := CallDB(B_Get_EQU, MnemPrimFile, MnemPrimRec.Start, MnemPrimKey.Start, 0);

  {fla**}
  if not ToMicroScan then
  BtrvStat := CallDB(B_Delete, MnemPrimFile, MnemPrimRec.Start, MnemPrimKey.Start, 0);

  StrCopy(AltPrimKey.MType, MnemType);
  StrCopy(AltPrimKey.Alternate, AltEditText);
  StrCopy(AltPrimKey.Mnemonic, MnemEditText);
  BtrvStat := CallDB(B_Get_EQU, AltPrimFile, AltPrimRec.Start, AltPrimKey.Start, 0);

  {fla**}
  if ToMicroScan then
  BtrvStat := CallDB(B_Delete, AltPrimFile, AltPrimRec.Start, AltPrimKey.Start, 0);

	SelIndex := Index;
  SendDlgItemMsg(id_List, lb_DeleteString, word(Index), 0);
  if ToMicroScan then
    SendDlgItemMsg(id_AltListSort, lb_DeleteString, word(Index), 0);
end;

procedure TXRefListDlg.AddToLists(var Msg: TMessage);
var
  XRefEntryDlg: PXRefEntryDlg;

begin
  XRefEntryDlg := New(PXRefEntryDlg, Init(@Self, 'INT_XREF_ENTRY'));
  Application^.ExecDialog(XRefEntryDlg);
end;

procedure TXRefListDlg.SaveCfgRad;
{ Save status of direction to respective mnem type in INI file.
}
var
  TStr: array[0..1] of Char;

begin
	if XRefCfgBuff.ToMScanRad = bf_Checked then
    StrCopy(TStr, 'Y')
	else
    StrCopy(TStr, 'N');
  WritePrivateProfileString('IntXRef', 'ToMScan', TStr, 'IntCstm.ini');
end;

procedure TXRefListDlg.EntrySave(var Msg: TMessage);
begin
  TransferData(tf_GetData);
  SaveCfgRad;
  EndDlg(0);
end;

{
*******************
XRefEntryDlg Object
*******************
}

procedure TXRefEntryDlg.SetupWindow;
{ If item is selected, initialize Add Entry Dialog.
}
var
  SelIndex: Integer;
  MnemEditText: array[0..MaxMnemLen] of Char;
  AltEditText: array[0..MaxAltLen] of Char;
  TempMsg: array[0..40] of char;

begin
  inherited SetupWindow;
  SendDlgItemMsg(id_MnemEdit, EM_LimitText, MaxMnemLen, 0);
  SendDlgItemMsg(id_AltEdit, EM_LimitText, MaxAltLen, 0);
  SelIndex := XRefListDlg^.SendDlgItemMsg(id_List, lb_GetCurSel, 0, 0);
	if SelIndex <> lb_Err then {Use existing mnemonic and add...}
  begin
    SeperateMnemAlt(SelIndex, MnemEditText, AltEditText);
    if ToMicroScan then {additional alternate.}
    begin
      SendDlgItemMsg(id_MnemEdit, wm_SetText, 0, longint(@MnemEditText));
      EnableWindow(GetDlgItem(HWindow, id_MnemEdit), False);
			SetFocus(GetItemHandle(id_AltEdit));
		end
    else {additional mnemonic.}
    begin
      SendDlgItemMsg(id_AltEdit, wm_SetText, 0, longint(@AltEditText));
      EnableWindow(GetDlgItem(HWindow, id_AltEdit), False);
			SetFocus(GetItemHandle(id_MnemEdit));
		end;
	end
	else
  begin
		SetFocus(GetItemHandle(id_MnemEdit));
	end;
end;

procedure TXRefEntryDlg.AddAnotherItem(var Msg: TMessage);
{ Save current mnemonic and clear entry in preperation for
  another menmonic.
}
var
  PrimEditId: integer;

begin
  if not IsBlankFields then
  begin
    SaveCurrentEntry;
    SendDlgItemMsg(id_AltEdit, WM_SetText, 0, 0);
    SendDlgItemMsg(id_MnemEdit, WM_SetText, 0, 0);
    if ToMicroScan then
      PrimEditId := id_MnemEdit
    else
			PrimEditId := id_AltEdit;
    EnableWindow(GetDlgItem(Hwindow, PrimEditId), True);
		SetFocus(GetItemHandle(id_MnemEdit));
    SendDlgItemMsg(id_MnemEdit, em_SetSel, 0, MakeLong(0, MaxMnemLen));
	end;
end;

procedure TXRefEntryDlg.CreateNewItem;
{ Add a new mnemonic to list and file. If there are multiple alternates,
  set status char to expand. First combo list box entry becomes the
  primary alternate. Add rest of alts to file.
}
var
  Index: integer;
  ListText: array[0..MaxListLen] of Char;
	MnemEditText: array[0..MaxMnemLen] of char;
  AltEditText: array[0..MaxAltLen] of char;
	MnemPrimRec: TMnemPrimRec;
	AltPrimRec: TAltPrimRec;
	MnemPrimKey: TMnemPrimKey;
	AltPrimKey: TAltPrimKey;
  BtrvStat: integer;

begin
  SendDlgItemMsg(id_MnemEdit, wm_GetText, MaxMnemLen+1, longint(@MnemEditText));
  SendDlgItemMsg(id_AltEdit, wm_GetText, MaxAltLen+1, longint(@AltEditText));
  StrCopy(MnemPrimKey.MType, MnemType);
  StrCopy(MnemPrimKey.Mnemonic, MnemEditText);
  StrCopy(MnemPrimKey.Alternate, AltEditText);
  StrCopy(MnemPrimRec.MType, MnemType);
  StrCopy(MnemPrimRec.Mnemonic, MnemEditText);
  StrCopy(MnemPrimRec.Alternate, AltEditText);
  StrCopy(MnemPrimRec.IsPrim, TrueStr);

  {fla**}
  if Not ToMicroScan then
  BtrvStat := CallDB(B_Insert, MnemPrimFile, MnemPrimRec.Start, MnemPrimKey.Start, 0);

  StrCopy(AltPrimKey.MType, MnemType);
  StrCopy(AltPrimKey.Alternate, AltEditText);
  StrCopy(AltPrimKey.Mnemonic, MnemEditText);
  StrCopy(AltPrimRec.MType, MnemType);
  StrCopy(AltPrimRec.Alternate, AltEditText);
  StrCopy(AltPrimRec.Mnemonic, MnemEditText);
  StrCopy(AltPrimRec.IsPrim, TrueStr);

  {fla**}
  if ToMicroScan then
  BtrvStat := CallDB(B_Insert, AltPrimFile, AltPrimRec.Start, AltPrimKey.Start, 0);

	if ToMicroScan then
	begin
    StrCopy(ListText, FillListString(MnemEditText, AltEditText));
    Index := XRefListDlg^.SendDlgItemMsg(id_AltListSort, lb_AddString, 0, longint(@AltEditText));
    XRefListDlg^.SendDlgItemMsg(id_List, lb_InsertString, Index, longint(@ListText));
	end
	else
  begin
    StrCopy(ListText, FillListString(AltEditText, MnemEditText));
    XRefListDlg^.SendDlgItemMsg(id_List, lb_AddString, 0, longint(@ListText));
 	end;
end;

procedure TXRefEntryDlg.AddToExistingItem(Direction: Boolean);
{ Add alts to an already existing mnemonic. Adjust status char if
  count is greater than one. Loop through all alts and either add the
  new alt or update an existing alts count.
}
var
	MnemEditText: array[0..MaxMnemLen] of char;
  AltEditText: array[0..MaxAltLen] of char;
  ListText: array[0..MaxListLen] of char;
	Index: integer;
	MnemPrimRec: TMnemPrimRec;
	AltPrimRec: TAltPrimRec;
	MnemPrimKey: TMnemPrimKey;
	AltPrimKey: TAltPrimKey;
  BtrvStat: integer;

begin
  SendDlgItemMsg(id_MnemEdit, wm_GetText, MaxMnemLen+1, longint(@MnemEditText));
  SendDlgItemMsg(id_AltEdit, wm_GetText, MaxAltLen+1, longint(@AltEditText));
  StrCopy(MnemPrimKey.MType, MnemType);
  StrCopy(MnemPrimKey.Mnemonic, MnemEditText);
  StrCopy(MnemPrimKey.Alternate, AltEditText);
  StrCopy(MnemPrimRec.MType, MnemType);
  StrCopy(MnemPrimRec.Mnemonic, MnemEditText);
  StrCopy(MnemPrimRec.Alternate, AltEditText);
  StrCopy(AltPrimKey.MType, MnemType);
  StrCopy(AltPrimKey.Alternate, AltEditText);
  StrCopy(AltPrimKey.Mnemonic, MnemEditText);
  StrCopy(AltPrimRec.MType, MnemType);
  StrCopy(AltPrimRec.Alternate, AltEditText);
  StrCopy(AltPrimRec.Mnemonic, MnemEditText);
	if Direction then
	begin
    StrCopy(MnemPrimRec.IsPrim, FalseStr);
    StrCopy(AltPrimRec.IsPrim, TrueStr);
	end
	else
	begin
    StrCopy(MnemPrimRec.IsPrim, TrueStr);
    StrCopy(AltPrimRec.IsPrim, FalseStr);
	end;
	if Direction = ToMicroScan then
		if ToMicroScan then
    begin
      StrCopy(ListText, FillListString(MnemEditText, AltEditText));
      Index := XRefListDlg^.SendDlgItemMsg(id_AltListSort, lb_AddString, 0, longint(@AltEditText));
      XRefListDlg^.SendDlgItemMsg(id_List, lb_InsertString, Index, longint(@ListText));
		end
		else
		begin
      StrCopy(ListText, FillListString(AltEditText, MnemEditText));
      XRefListDlg^.SendDlgItemMsg(id_List, lb_AddString, 0, longint(@ListText));
		end;
  BtrvStat := CallDB(B_Insert, MnemPrimFile, MnemPrimRec.Start, MnemPrimKey.Start, 0);
  BtrvStat := CallDB(B_Insert, AltPrimFile, AltPrimRec.Start, AltPrimKey.Start, 0);
end;

procedure TXRefEntryDlg.SaveCurrentEntry;
{ Add mnemonic entered in Entry Dialog. If mnemonic exist but not selected,
  add to combo box list alts that already exitst.
}
var
  PrimEditId, SecEditId: Integer;
  LastCursor: HCursor;
  PrimText, PTempText, SecText, STempText: array[0..MaxAltLen] of Char;
  PrimFound, SecFound, SameSec: Boolean;
  MnemPrimRec: TMnemPrimRec;
  AltPrimRec: TAltPrimRec;
  MnemSearchKey: TMnemSearchKey;
  AltSearchKey: TAltSearchKey;
  BtrvStat: integer;
  errmsg,errcaption: array[0..128] of char;
begin
  LastCursor := SetCursor(LoadCursor(0, idc_Wait));
  if ToMicroScan then
  begin
    PrimEditId := id_AltEdit;
    SecEditId := id_MnemEdit;
  end
  else
  begin
    PrimEditId := id_MnemEdit;
    SecEditId := id_AltEdit;
  end;

  StrCopy(PTempText, '');
  StrCopy(STempText, '');
  SendDlgItemMsg(PrimEditId, wm_GetText, MaxAltLen+1, longint(@PrimText));
  SendDlgItemMsg(SecEditId, wm_GetText, MaxAltLen+1, longint(@SecText));

  if ToMicroScan then
  begin
    StrCopy(AltSearchKey.MType, MnemType);
    StrCopy(AltSearchKey.Alternate, PrimText);
    BtrvStat := CallDB(B_Get_EQU, AltPrimFile, AltPrimRec.Start, AltSearchKey.Start, 1);
  end
  else
  begin
    StrCopy(MnemSearchKey.MType, MnemType);
    StrCopy(MnemSearchKey.Mnemonic, PrimText);
    BtrvStat := CallDB(B_Get_EQU, MnemPrimFile, MnemPrimRec.Start, MnemSearchKey.Start, 1);
  end;

  if BtrvStat = 0 then
  begin
    if ToMicroScan then
    begin
      StrCopy(PTempText, AltPrimRec.Alternate);
      StrCopy(STempText, AltPrimRec.Mnemonic);
    end
    else
    begin
      StrCopy(PTempText, MnemPrimRec.Mnemonic);
      StrCopy(STempText, MnemPrimRec.Alternate);
    end;
  end;

  PrimFound := False;
  if StrComp(PTempText, PrimText) = 0 then
     PrimFound := True;

  SameSec := False;
  if StrComp(STempText, SecText) = 0 then
     SameSec := True;

  {Let the user know that 1:m entries are not allowed}
  {(as oppossed to m:1, which are perfectly legal)}
  If (PrimFound) then
  begin
    LoadString(HInstance, str_IntBase+str_ConflictingXRef, errcaption, SizeOf(errcaption));
    LoadString(HInstance, str_IntBase+str_ConfirmXRef, errmsg, SizeOf(errmsg));
    StrCat(errcaption,' (');
    StrCat(errcaption,PrimText);
    StrCat(errcaption,')');
    messagebox(hwindow,errmsg,errcaption,mb_ok+mb_iconinformation);
    exit;
  end;

  if ToMicroScan then
  begin
    StrCopy(MnemSearchKey.MType, MnemType);
    StrCopy(MnemSearchKey.Mnemonic, SecText);
    BtrvStat := CallDB(B_Get_EQU, MnemPrimFile, MnemPrimRec.Start, MnemSearchKey.Start, 1);
  end
  else
  begin
    StrCopy(AltSearchKey.MType, MnemType);
    StrCopy(AltSearchKey.Alternate, SecText);
    BtrvStat := CallDB(B_Get_EQU, AltPrimFile, AltPrimRec.Start, AltSearchKey.Start, 1);
  end;

  if BtrvStat = 0 then
     if ToMicroScan then
        StrCopy(STempText, MnemPrimRec.Mnemonic)
     else
        StrCopy(STempText, AltPrimRec.Alternate);

  SecFound := False;
  if StrComp(STempText, SecText) = 0 then
     SecFound := True;

  if PrimFound then
  begin
    if (not SecFound) and (not SameSec) then
       AddToExistingItem(not ToMicroScan);
  end
  else
  begin
    if not SecFound then
       CreateNewItem
    else
       AddToExistingItem(ToMicroScan);
  end;

  SetCursor(LastCursor);
end;

procedure TXRefEntryDlg.SaveEntry(var Msg: TMessage);
begin
  if not IsBlankFields then
  begin
    SaveCurrentEntry;
    EndDlg(id_Done);
    XRefListDlg^.SendDlgItemMsg(id_List, lb_SetCurSel, word(-1), 0);
  end;
end;

procedure TXRefEntryDlg.Abort(var Msg: TMessage);
begin
  EndDlg(0);
  XRefListDlg^.SendDlgItemMsg(id_List, lb_SetCurSel, word(-1), 0);
end;

function TXRefEntryDlg.IsBlankFields: Boolean;
var
  MnemEditText: array[0..MaxMnemLen] of Char;
  AltEditText: array[0..MaxAltLen] of Char;
  TempMsg: array[0..60] of char;
  TempVerMsg: array[0..30] of char;

begin
  IsBlankFields := False;
  SendDlgItemMsg(id_AltEdit, wm_GetText, MaxAltLen + 1, longint(@AltEditText));
  SendDlgItemMsg(id_MnemEdit, wm_GetText, MaxMnemLen + 1, longint(@MnemEditText));
  if (StrComp(MnemEditText, '') = 0) and (StrComp(AltEditText, '') = 0) then
  begin
    LoadString(HInstance, str_IntBase+str_IsBlank, TempMsg, SizeOf(TempMsg));
    LoadString(HInstance, str_IntBase+str_IntMsg, TempVerMsg, SizeOf(TempVerMsg));
    MessageBox(HWindow, TempMsg, TempVerMsg, mb_ok);
    IsBlankFields := True;
  end;
end;

{
********************
XRefChangeDlg Object
********************
}

Procedure TXRefChangeDlg.SetupWindow;
{ Initialize Change Dialog with item selected. If a non-primary is
  selected get mnemonic from file.
}
var
  SelIndex: integer;
  MnemEditText: array[0..MaxMnemLen] of Char;
  AltEditText: array[0..MaxAltLen] of Char;

begin
  inherited SetupWindow;
  SendDlgItemMsg(id_MnemEdit, em_LimitText, MaxMnemLen, 0);
  SendDlgItemMsg(id_AltEdit, em_LimitText, MaxAltLen, 0);
  SelIndex := XRefListDlg^.SendDlgItemMsg(id_List, lb_GetCurSel, 0, 0);
  if SelIndex <> lb_Err then
  begin
    SeperateMnemAlt(SelIndex, MnemEditText, AltEditText);
    SendDlgItemMsg(id_MnemEdit, wm_SetText, 0, longint(@MnemEditText));
    SendDlgItemMsg(id_AltEdit, wm_SetText, 0, longint(@AltEditText));
    if ToMicroScan then
    begin
      EnableWindow(GetDlgItem(HWindow, id_MnemEdit), False);
      StrCopy(OldText, AltEditText);
    end
    else
    begin
      EnableWindow(GetDlgItem(HWindow, id_AltEdit), False);
      StrCopy(OldText, MnemEditText);
    end;
  end;
end;

Procedure TXRefChangeDlg.SaveChanges(var Msg: TMessage);
{ Save change to alternate. Get record, update key and alternate in
  record, update record. Get string from list box, update string,
  delete old string and insert changed string.
}
var
  Index, ListId: integer;
  AltEditText: array[0..MaxAltLen] of char;
  MnemEditText: array[0..MaxEditLen] of char;
  ListText: array[0..MaxListLen] of char;
  PrimText: array[0..MaxAltLen] of Char;
  SecText: array[0..MaxAltLen] of Char;
  BtrvStat: integer;
	MnemPrimRec: TMnemPrimRec;
	AltPrimRec: TAltPrimRec;
	MnemPrimKey: TMnemPrimKey;
  AltPrimKey: TAltPrimKey;
	TempIsPrim: array[0..2] of char;

begin
  SendDlgItemMsg(id_MnemEdit, wm_GetText, MaxEditLen+1, longint(@MnemEditText));
  SendDlgItemMsg(id_AltEdit, wm_GetText, MaxAltLen+1, longint(@AltEditText));
  if (StrComp(MnemEditText, '') <> 0) and
     (StrComp(AltEditText, '') <> 0) then
  begin
		if ToMicroScan then
    begin
      StrCopy(MnemPrimKey.MType, MnemType);
      StrCopy(MnemPrimKey.Mnemonic, MnemEditText);
      StrCopy(MnemPrimKey.Alternate, OldText);
      BtrvStat := CallDB(B_Get_EQU, MnemPrimFile, MnemPrimRec.Start, MnemPrimKey.Start, 0);
      StrCopy(TempIsPrim, MnemPrimRec.IsPrim);
      {fla**}
      {BtrvStat := CallDB(B_Delete, MnemPrimFile, MnemPrimRec.Start, MnemPrimKey.Start, 0);}
      StrCopy(MnemPrimKey.MType, MnemType);
      StrCopy(MnemPrimKey.Mnemonic, MnemEditText);
      StrCopy(MnemPrimKey.Alternate, AltEditText);
      StrCopy(MnemPrimRec.MType, MnemType);
      StrCopy(MnemPrimRec.Mnemonic, MnemEditText);
      StrCopy(MnemPrimRec.Alternate, AltEditText);
      StrCopy(MnemPrimRec.IsPrim, TempIsPrim);
      BtrvStat := CallDB(B_Insert, MnemPrimFile, MnemPrimRec.Start, MnemPrimKey.Start, 0);
      StrCopy(AltPrimKey.MType, MnemType);
      StrCopy(AltPrimKey.Alternate, OldText);
      StrCopy(AltPrimKey.Mnemonic, MnemEditText);
      BtrvStat := CallDB(B_Get_EQU, AltPrimFile, AltPrimRec.Start, AltPrimKey.Start, 0);
      StrCopy(TempIsPrim, AltPrimRec.IsPrim);
      BtrvStat := CallDB(B_Delete, AltPrimFile, AltPrimRec.Start, AltPrimKey.Start, 0);
      StrCopy(AltPrimKey.MType, MnemType);
      StrCopy(AltPrimKey.Alternate, AltEditText);
      StrCopy(AltPrimKey.Mnemonic, MnemEditText);
      StrCopy(AltPrimRec.MType, MnemType);
      StrCopy(AltPrimRec.Alternate, AltEditText);
      StrCopy(AltPrimRec.Mnemonic, MnemEditText);
      StrCopy(AltPrimRec.IsPrim, TempIsPrim);
      BtrvStat := CallDB(B_Insert, AltPrimFile, AltPrimRec.Start, AltPrimKey.Start, 0);
		end
		else
		begin
      StrCopy(MnemPrimKey.MType, MnemType);
      StrCopy(MnemPrimKey.Mnemonic, OldText);
      StrCopy(MnemPrimKey.Alternate, AltEditText);
      BtrvStat := CallDB(B_Get_EQU, MnemPrimFile, MnemPrimRec.Start, MnemPrimKey.Start, 0);
      StrCopy(TempIsPrim, MnemPrimRec.IsPrim);
      BtrvStat := CallDB(B_Delete, MnemPrimFile, MnemPrimRec.Start, MnemPrimKey.Start, 0);
      StrCopy(MnemPrimKey.MType, MnemType);
      StrCopy(MnemPrimKey.Mnemonic, MnemEditText);
      StrCopy(MnemPrimKey.Alternate, AltEditText);
      StrCopy(MnemPrimRec.MType, MnemType);
      StrCopy(MnemPrimRec.Mnemonic, MnemEditText);
      StrCopy(MnemPrimRec.Alternate, AltEditText);
      StrCopy(MnemPrimRec.IsPrim, TempIsPrim);
      BtrvStat := CallDB(B_Insert, MnemPrimFile, MnemPrimRec.Start, MnemPrimKey.Start, 0);
      StrCopy(AltPrimKey.MType, MnemType);
      StrCopy(AltPrimKey.Alternate, AltEditText);
      StrCopy(AltPrimKey.Mnemonic, OldText);
      BtrvStat := CallDB(B_Get_EQU, AltPrimFile, AltPrimRec.Start, AltPrimKey.Start, 0);
      StrCopy(TempIsPrim, AltPrimRec.IsPrim);
      {fla**}
      {BtrvStat := CallDB(B_Delete, AltPrimFile, AltPrimRec.Start, AltPrimKey.Start, 0);}
      StrCopy(AltPrimKey.MType, MnemType);
      StrCopy(AltPrimKey.Alternate, AltEditText);
      StrCopy(AltPrimKey.Mnemonic, MnemEditText);
      StrCopy(AltPrimRec.MType, MnemType);
      StrCopy(AltPrimRec.Alternate, AltEditText);
      StrCopy(AltPrimRec.Mnemonic, MnemEditText);
      StrCopy(AltPrimRec.IsPrim, TempIsPrim);
      BtrvStat := CallDB(B_Insert, AltPrimFile, AltPrimRec.Start, AltPrimKey.Start, 0);
		end;
    Index := XRefListDlg^.SendDlgItemMsg(id_List, lb_GetCurSel, 0, 0);
    XRefListDlg^.SendDlgItemMsg(id_List, lb_DeleteString, Index, 0);
    if ToMicroScan then
    begin
      XRefListDlg^.SendDlgItemMsg(id_AltListSort, lb_DeleteString, Index, 0);
      StrCopy(SecText, AltEditText);
      StrCopy(PrimText, MnemEditText);
      StrCopy(ListText, FillListString(PrimText, SecText));
      Index := XRefListDlg^.SendDlgItemMsg(id_AltListSort, lb_AddString, 0, longint(@SecText));
      XRefListDlg^.SendDlgItemMsg(id_List, lb_InsertString, Index, longint(@ListText));
    end
    else
    begin
      StrCopy(PrimText, AltEditText);
      StrCopy(SecText, MnemEditText);
      StrCopy(ListText, FillListString(PrimText, SecText));
      XRefListDlg^.SendDlgItemMsg(id_List, lb_AddString, 0, longint(@ListText));
    end;
    XRefListDlg^.SendDlgItemMsg(id_List, lb_SetCurSel, word(-1), 0);
    EndDlg(0);
  end;
end;

Procedure TXRefChangeDlg.AbortChange(var Msg: TMessage);
begin
  EndDlg(0);
  XRefListDlg^.SendDlgItemMsg(id_List, lb_SetCurSel, word(-1), 0);
end;

{
*******************************
Non Object Functions/Procedures
*******************************
}

Procedure IntXRef_ExecXRefEntry(ParentWnd: HWnd; ParentWndObj: PWindowsObject;
  MenuSel: Integer);
var
  MenuName: array[0..30] of char;
  MType: char;
  TStr: array[0..1] of char;

begin
  LoadString(HInstance, str_IntBase+str_PhyMnem+MenuSel-120, MenuName, SizeOf(MenuName));

	MType := XRefMnemTypes[MenuSel-120];
  GetPrivateProfileString('IntXRef', 'ToMScan', '', TStr, SizeOf(TStr), 'IntCstm.ini');
	if TStr[0] = 'Y' then
	begin
		XRefCfgBuff.ToMScanRad := bf_Checked;
		XRefCfgBuff.ToIntRad:= bf_UnChecked;
    ToMicroScan := True;
	end
	else
	begin
		XRefCfgBuff.ToMScanRad := bf_UnChecked;
		XRefCfgBuff.ToIntRad := bf_Checked;
		ToMicroScan := False;
  end;
	BtrvData[MnemPrimFile].Size.Len       := SizeOf(TMnemPrimRec);
  BtrvData[MnemPrimFile].FileName.Name  := 'MNEMPRIM.DAT  ';
	BtrvData[AltPrimFile].Size.Len       := SizeOf(TAltPrimRec);
	BtrvData[AltPrimFile].FileName.Name  := 'ALTPRIM.DAT   ';
  OpenCloseFiles(ParentWnd, B_Open, [MnemPrimFile, AltPrimFile]);
  XRefListDlg := New(PXRefListDlg, Init(ParentWndObj, 'INT_XREF', MenuName, MType));
  XRefListDlg^.TransferBuffer := @XRefCfgBuff;
  Application^.ExecDialog(XRefListDlg);
  OpenCloseFiles(ParentWnd, B_Close, [MnemPrimFile, AltPrimFile]);
end;

Begin
End.
