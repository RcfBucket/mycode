{----------------------------------------------------------------------------}
{  Module Name  : FILMAINT.PAS                                               }
{  Programmer   : DWC                                                        }
{  Date Created : 05/01/95                                                   }
{                                                                            }
{  Purpose -                                                                 }
{  This program is the main application for backup, restore, extract, merge  }
{  compress and purge.                                                       }
{                                                                            }
{  Assumptions -                                                             }
{  None.                                                                     }
{                                                                            }
{  Initialization -                                                          }
{  None.                                                                     }
{                                                                            }
{  Revision History -                                                        }
{                                                                            }
{  Version  Date      Who    Did What                                        }
{  -------  --------  ------ ----------------------------------------------- }
{  1.00     05/01/95  DWC    Initial release.                                }
{                                                                            }
{----------------------------------------------------------------------------}
program FilMaint;


uses
  OWindows,
  ODialogs,
  MTask,
  MScan,
  DMString,
  WinTypes,
  WinProcs,
  UserMsgs,
  Ctl3D,
  Strings,
  APITools,
  INILib,
  FMBackup,
  FMRstore,
  FMExtrct,
  FMMerge,
  FMCmprss,
  Threshld,
  Purge;

{$R FILMAINT.RES}
{$I FILMAINT.INC}
{$R FMUTIL.RES}
{$I FMUTIL.INC}
{$R FMLOGO.RES}
{$I DMSLOGO.INC}


type
  TFMMainApp = object(T3DApplication)
    procedure InitMainWindow; virtual;
  end;

  PFMMainWindow = ^TFMMainWindow;
  TFMMainWindow = object(T3DWindow)
    constructor Init(aParent: PWindow; aTitle: PChar);
    procedure   SetupWindow; virtual;
    procedure Paint(PaintDC: HDC; var PaintInfo: TPaintStruct); virtual;
    procedure WMDMSQuery(var msg: TMessage); virtual WM_FIRST + WM_MAINPROGQUERY;
    procedure GetWindowClass(var aWndClass: TWndClass); virtual;
    function GetClassName: PChar; virtual;

    procedure FullBackup(var Msg: TMessage); virtual CM_FIRST + CM_FULL;
    procedure IncrBackup(var Msg: TMessage); virtual CM_FIRST + CM_INCREMENTAL;
    procedure Restore(var Msg: TMessage); virtual CM_FIRST + CM_RESTORE;
    procedure Extract(var Msg: TMessage); virtual CM_FIRST + CM_EXTRACT;
    procedure Merge(var Msg: TMessage); virtual CM_FIRST + CM_MERGE;
    procedure Purge(var Msg: TMessage); virtual CM_FIRST + CM_PURGE;
    procedure Compress(var Msg: TMessage); virtual CM_FIRST + CM_COMPRESS;
    procedure fileMaintExit(var Msg: TMessage); virtual CM_FIRST + CM_EXIT;
    procedure fileStatistics(var Msg: TMessage); virtual CM_FIRST + CM_FMSTATISTICS;
  end;


var
  FMMainApp: TFMMainApp;


procedure TFMMainApp.InitMainWindow;
var
  pstr : array[0..80] of char;
begin
  if INIUse3D then
    Register3dApp(TRUE, TRUE, TRUE);
  MainWindow := New(PFMMainWindow, Init(nil, SR(IDS_FILMAINT, pstr, 80)));
  if MainWindow = nil then
  begin
    FatalError(SR(IDS_FMERROR, pstr, 10), SR(IDS_FMINIT, pstr, 80));
    Halt;
  end;
end;

constructor TFMMainWindow.Init(aParent: PWindow; aTitle: PChar);
begin
  inherited Init(aParent, aTitle);
  with Attr do
  begin
    Menu := LoadMenu(HInstance, MakeIntResource(MNU_FILMAINT));
    X := 0;
    Y := 0;
    H := 380;
    W := 640;
  end;
end;

procedure TFMMainWindow.SetupWindow;
var
  x, y : integer;
  w, h : integer;
  r    : TRect;
begin
  inherited SetupWindow;
  GetWindowRect(HWindow, r);
  w:= r.right - r.left;
  h:= r.bottom - r.top;
  x:= (GetSystemMetrics(SM_CXSCREEN) div 2) - (w div 2);
  y:= (GetSystemMetrics(SM_CYSCREEN) div 2) - (h div 2);
  MoveWindow(HWindow, x, y, w, h, false);
end;

procedure TFMMainWindow.Paint;
var
  Logo    : HBitMap;
  SaveDC  : HDC;
  BitInfo : TBitMap;
  r       : TRect;
  w       : integer;
  h       : integer;
begin
  inherited Paint(paintDC, paintInfo);
  {$IFDEF RELEASEVER}  {- releasable (real) version }
  Logo:= LoadBitmap(HInstance, MAKEINTRESOURCE(BMP_DADE));
  {$ELSE}  {- for investigational use only }
  Logo:= LoadBitmap(HInstance, MAKEINTRESOURCE(BMP_INV_USE_ONLY));
  {$ENDIF}
  GetWindowRect(hWindow, r);
  w:= r.right - r.left;
  h:= r.bottom - r.top;

  SaveDC:= CreateCompatibleDC(PaintDC);
  SelectObject(SaveDC, Logo);
  GetObject(logo, SizeOf(TBitMap), @bitInfo);
  BitBlt(PaintDC, (w - bitInfo.bmWidth) div 2,(h - bitInfo.bmHeight) div 2,
	BitInfo.bmWidth, bitInfo.bmHeight, SaveDC, 0, 0, SrcCopy);
  DeleteDC(SaveDC);
  DeleteObject(logo);
end;

procedure TFMMainWindow.WMDMSQuery(var msg: TMessage);
{- respond to the task module and tell it who I am }
begin
  StrLCopy(PChar(msg.lParam), application^.Name, msg.wParam);
  msg.result:= DMSMainProgRetCode;
(*  SetWindowLong(HWindow, DWL_MSGRESULT, msg.result);  {- set message result (for dialogs) }*)
end;

function TFMMainWindow.GetClassName: PChar;
begin
  GetClassName:= application^.name;
end;

procedure TFMMainWindow.GetWindowClass(var aWndClass: TWndClass);
begin
  inherited GetWindowClass(aWndClass);
  aWndClass.hIcon:= LoadIcon(HInstance, MakeIntResource(ICON_1));
end;



procedure TFMMainWindow.FullBackup(var Msg: TMessage);
begin
  FMFullBackup(@Self);
end;

procedure TFMMainWindow.IncrBackup(var Msg: TMessage);
begin
  FMIncrBackup(@Self);
end;

procedure TFMMainWindow.Restore(var Msg: TMessage);
begin
  FMRestore(@Self);
end;

procedure TFMMainWindow.Extract(var Msg: TMessage);
begin
  FMExtract(@Self);
end;

procedure TFMMainWindow.Merge(var Msg: TMessage);
begin
  FMMrge(@Self);
end;

procedure TFMMainWindow.Purge(var Msg: TMessage);
begin
  PurgeData(@Self);
end;

procedure TFMMainWindow.Compress(var Msg: TMessage);
var
  confirmMsg: array [0..255] of char;
  compressMsg: array [0..255] of char;
begin
  SR(IDS_CONFIRM, confirmMsg, 255);
  SR(IDS_COMPRESSMSG, compressMsg, 255);
  if YesNoMsg(hwindow, confirmMsg, compressMsg) then
  begin
     FMCompress(@Self);
  end;
end;

procedure TFMMainWindow.fileMaintExit(var Msg: TMessage);
begin
  CloseWindow;
end;

procedure TFMMainWindow.fileStatistics(var Msg: TMessage);
begin
  DatabaseStatistics(@Self);
end;

BEGIN
  if OKToRun(MOD_FILMAIN, false) then
  begin
    FMMainApp.Init(GetModuleName(MOD_FILMAIN));
    FMMainApp.Run;
    FMMainApp.Done;
  end;
END.
{- DWC }

