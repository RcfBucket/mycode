unit UIPat;

INTERFACE

uses
  DBLib,
  DBTypes,
  OWindows;

procedure UIPatientEntry(aParent: PWindowsObject; aSeq: TSeqNum);
{- call patient entry. aRec is a record to preload }

IMPLEMENTATION

uses
  UICommon,
  UserMsgs,
  APITools,
  CtlLib,
  DBFile,
  DBIDs,
  DMSErr,
  DMString,
  DlgLib,
  ListLib,
  MScan,
  ODialogs,
  Objects,
  PatReps,
  Strings,
  UIFlds,
  Win31,
  WinProcs,
  WinTypes;

{$R UIPAT.RES}
{$I UIPAT.INC}

type
  PUIPatient = ^TUIPatient;
  TUIPatient = object(TUICommonDlg)
    dbPat     : PDBFile;
    origPat   : PDBRec;
    tempRec   : PDBRec;
    ClearPat  : boolean;
    {}
    constructor Init(aParent: PWindowsObject; aSeq: TSeqNum);
    destructor Done; virtual;
    procedure SetupWindow; virtual;

    procedure WMLostFocus(var msg: TMessage);   virtual WM_FIRST + WM_LOSTFOCUS;
    procedure WMFldList(var msg: TMessage);     virtual WM_FIRST + WM_FLDLIST;

    {- field exit methods }
    procedure DoPatIDExit;

    {- misc stuff }
    function FillScreen: boolean;

    procedure SetModuleID; virtual;
    procedure ClearFields; virtual;
    procedure EnableButtons; virtual;
    function DataChanged: boolean; virtual;
    function CheckSave(clearAfter: boolean): boolean; virtual;
    function SaveData(clearAfter: boolean): boolean; virtual;
    procedure DeleteData; virtual;
    procedure NextPrev(doNext: boolean); virtual;
    procedure IDReview(var msg: TMessage); virtual ID_FIRST + IDC_REVIEW;
    procedure IDNext(var msg: TMessage);   virtual ID_FIRST + IDC_UINEXT;

    private
    pFld      : PUIFld;
  end;

constructor TUIPatient.Init(aParent: PWindowsObject; aSeq: TSeqNum);
var
  f   : PUIFld;
  k   : integer;
  fNum: integer;
  fi  : TFldInfo;
  p1,p2 : array[0..100] of char;
  subst : PErrSubst;
begin
  {- open all files being modified }
  dbPat:= New(PDBFile, Init(DBPatFile, '', dbOpenNormal));
  if dbPat = nil then
  begin {- cant open patient }
    subst:= New(PErrSubst, Init(DBPatFile));
    ShowError(nil, IDS_ERROROPEN, subst, 0, modID, 1);
    MSDisposeObj(subst);
    Halt;
  end;
  origPat:= New(PDBRec, Init(dbPat^.dbc));
  if origPat = nil then
  begin
    ShowError(nil, IDS_CANTALLOCREC, nil, 0, modID, 1);
    Halt;
  end;
  tempRec:= New(PDBRec, Init(dbPat^.dbc));
  if tempRec = nil then
  begin
    ShowError(nil, IDS_CANTALLOCREC, nil, 0, modID, 3);
    Halt;
  end;

  inherited Init(aParent, MakeIntResource(DLG_UIPATIENT), aSeq);
  ClearPat :=True;

  InitModeText(IDC_MODE);

  pFld:= LinkUIFld(DBPATPatID,    true,  false, 0, IDC_EDPATID,    IDC_LBLPATID,    dbPat^.dbd);
         LinkUIFld(DBPATLName,    false, false, 0, IDC_EDLAST,     IDC_LBLLAST,     dbPat^.dbd);
         LinkUIFld(DBPATFName,    false, false, 0, IDC_EDFirst,    IDC_LBLFIRST,    dbPat^.dbd);
         LinkUIFld(DBPATFreeText, false, false, 0, IDC_EDFREETEXT, IDC_LBLFREETEXT, dbPat^.dbd);

  {- now link all user defined fields }
  k:= IDC_EDUSER1;
  for fNum:= DBPatUDBase to dbPat^.dbd^.NumFields do
  begin
    dbPat^.dbd^.FieldInfo(fNum, fi);
    if ((fi.userFlag and DB_UD) <> 0) then
    begin
      LinkUIFld(fNum, false, false, 0, k, k+100, dbPat^.dbd);
      Inc(k);
    end;
  end;
end;

destructor TUIPatient.Done;
begin
  inherited Done;
  MSDisposeObj(origPat);
  MSDisposeObj(tempRec);
  MSDisposeObj(dbPat);
end;

procedure TUIPatient.SetupWindow;
begin
  inherited SetupWindow;

  if preLoad <> 0 then
  begin
    {- need to load a specimen record }
    if dbPat^.dbc^.GetSeq(tempRec, preLoad) then
    begin
      dbPat^.dbr^.CopyRecord(tempRec);
      if FillScreen then
        SetMode(MODE_EDIT);
    end;
  end;
end;

procedure TUIPatient.SetModuleID;
begin
  modID:= MOD_UIPATIENT;
end;

procedure TUIPatient.ClearFields;
begin
  inherited ClearFields;

  pFld^.FocusFld;   {- focus patient ID field }
  origPat^.ClearRecord;
  dbPat^.dbr^.ClearRecord;
end;

procedure TUIPatient.EnableButtons;
begin
  EnableWindow(GetItemHandle(IDC_UIPREV),    (mode = MODE_EDIT) and (preLoad = 0));
  EnableWindow(GetItemHandle(IDC_UINEXT),     preLoad = 0);
  EnableWindow(GetItemHandle(IDC_UICLEAR),    preLoad = 0);
  EnableWindow(GetItemHandle(IDC_UIDELETE),  (mode = MODE_EDIT) and (preLoad = 0));
  EnableWindow(GetItemHandle(IDC_REVIEW),     mode = MODE_EDIT);
  EnableWindow(GetItemHandle(IDC_UISAVE),     mode > MODE_NONE);
end;

function TUIPatient.FillScreen: boolean;
{- called when dbSpec has been loaded. Fills all controls and loads any relevant
   patient data. Assumes the data to be filled is in dbSpec^.dbr. Also, sets the
   orig recs to the value of the loaded record }

  procedure GetData(aFld: PUIFld); far;
  begin
    aFld^.XFerRecToCtl(dbPat^.dbr);
  end;

var
  seq   : TSeqNum;
begin
  if not origPat^.CopyRecord(dbPat^.dbr) then
  begin
    ShowError(@self, IDS_CANTCOPYREC, nil, origPat^.dbErrorNum, modID, 0);
    FillScreen:= false;
  end
  else
  begin
    flds^.ForEach(@GetData);
    FillScreen:= true;
  end;
end;

{---- Button methods }

procedure TUIPatient.IDReview(var msg: TMessage);
var
  pstr  : array[0..50] of char;
begin
  pFld^.XFerCtlToRec(tempRec);
  tempRec^.GetFieldAsStr(DBPatPatID, pstr, sizeof(pstr)-1);
  PatientReview(@self, pstr);
end;

procedure TUIPatient.IDNext(var msg: TMessage);
begin
  ClearPat:=False;
  NextPrev(True);
  ClearPat:=True;
end;

procedure TUIPatient.NextPrev(doNext: boolean);
var
  fld   : PUIFld;
  isOK  : boolean;
begin
  if not CheckSave(ClearPat) then exit;

  if (mode <> MODE_EDIT) or (origPat^.GetSeqValue = 0) then
  begin
    if dbPat^.dbc^.GetFirst(tempRec) then
    begin
      dbPat^.dbr^.CopyRecord(tempRec);
      if FillScreen then
        SetMode(MODE_EDIT);
    end
    else
      ShowError(@self, IDS_NOPATS, nil, dbPat^.dbc^.dbErrorNum, modID, 0);
  end
  else
  begin
    tempRec^.CopyRecord(origPat);
    if doNext then
      isOK:= dbPat^.dbc^.GetNext(tempRec)
    else
      isOK:= dbPat^.dbc^.GetPrev(tempRec);

    if not isOK then
    begin
      if dbPat^.dbc^.dbErrorNum = MOD_BTRV + 9 then
        ShowError(@self, IDS_DBNOMOREDATA, nil, dbPat^.dbc^.dbErrorNum, modID, 0)
      else
        ShowError(@self, IDS_DBERROR, nil, dbPat^.dbc^.dbErrorNum, modID, 1);
    end
    else
    begin
      dbPat^.dbr^.CopyRecord(tempRec);
      if FillScreen then
        SetMode(MODE_EDIT);
    end;
  end;

  pfld^.FocusFld;
end;

procedure TUIPatient.DeleteData;
var
  p1, p2  : array[0..100] of char;
  err     : integer;
begin
  if mode = MODE_EDIT then
  begin
    if YesNoMsg(HWindow, SR(IDS_CONFIRM, p1, 100), SR(IDS_CONFIRMDELPAT, p2, 100)) then
    begin
      err:= 0;
      WaitCursor(true);
      if dbPat^.dbc^.GetSeq(dbPat^.dbr, origPat^.GetSeqValue) then
      begin
        if not dbPat^.dbc^.DeleteRec(dbPat^.dbr) then
          err:= dbPat^.dbc^.dbErrorNum
        else
          ClearFields;
      end
      else
        err:= dbPat^.dbc^.dbErrorNum;

      if err <> 0 then
        ShowError(@self, IDS_CANTDEL, nil, err, modID, 0);
      WaitCursor(false);
    end;
  end;
end;

function TUIPatient.DataChanged: boolean;
{- compare the current data with the originally loaded data.  If any data has
   changed, then return true }

  function IsModified(aFld: PUIFld): boolean; far;
  begin
    IsModified:= aFld^.IsModified(origPat);
  end;

begin
  DataChanged:= flds^.FirstThat(@IsModified) <> nil;
end;

function TUIPatient.SaveData(clearAfter: boolean): boolean;
{- save the data for the specimen. return true if the data was saved. false otherwise}
var
  errNum  : integer;
  p1, p2  : array[0..200] of char;
  eNum    : word;
  seq     : TSeqNum;
  ret     : boolean;

  function IsInvalid(aFld: PUIFld): boolean; far;
  begin
    errNum:= aFld^.Format;
    IsInvalid:= errNum <> 0;
  end;

  function SaveIt: boolean;
  var
    sRet  : boolean;

    procedure XFerToRec(aFld: PUIFld); far;
    begin
       aFld^.XFerCtlToRec(dbPat^.dbr);
    end;

  begin
    sRet:= true;
    {- now transfer all fields to the record buffer }
    flds^.ForEach(@XFerToRec);  {- get specimen fields }

    if mode = MODE_NEW then
      dbPat^.dbc^.InsertRec(dbPat^.dbr)
    else
      dbPat^.dbc^.UpdateRec(dbPat^.dbr);

    {- special error handling for common errors }
    case dbPat^.dbc^.dbErrorNum of
      MOD_BTRV + 5: eNum:= IDS_DUPPAT;
      else
        eNum:= IDS_CANNOTSAVE;
    end;

    if dbPat^.dbc^.dbErrorNum <> 0 then
    begin
      ShowError(@self, eNum, nil, dbPat^.dbc^.dbErrorNum, modID, 0);
      pFld^.FocusFld;
      sret:= false;
    end
    else if clearAfter then
      ClearFields;
    SaveIt:= sret;
  end;
var
  fld : PUIFld;
begin
  WaitCursor(true);
  ret:= true;

  {- first run through all the fields and see if any are invalid }
  fld:= flds^.FirstThat(@IsInvalid);
  if fld <> nil then
  begin
    ShowUIFldError(@self, errNum, fld);
    ret:= false;
  end
  else
  begin
    {- now load current record and lock it. Then compare it to the original
      record that was loaded }
    if mode = MODE_EDIT then
    begin
      {- lock the original record }
      tempRec^.CopyRecord(origPat);
      errNum:= DBBeginTransaction;
      if errNum <> 0 then
      begin
        ret:= false;
        ShowError(@self, IDS_TRANSERROR, nil, errNum, modID, 0);
      end
      else
      begin
        if not dbPat^.dbc^.GetLock(tempRec) then
        begin
          errNum:= dbPat^.dbc^.dbErrorNum;

          case errNum of  {- checks for special errors }
            MOD_BTRV + 84:
              begin
                ShowError(@self, IDS_PATLOCKED, nil, errNum, modID, 0);
                ret:= false;
              end;
          end;
          if ret then
          begin
            {- record not found - should I re-create it ? }
            SR(IDS_CANTLOCATEORIGPAT, p2, 200);
            SR(IDS_TRYRECREATE, p1, 200);
            strcat(p2, #10#10);
            strcat(p2, p1);
            if YesNoMsg(HWindow, SR(IDS_WARNING, p1, 200), p2) then
            begin
              mode:= MODE_NEW;
              ret:= SaveIt;
            end
            else
              ret:= false;
          end;
        end
        else  {- record locked }
        begin
          {- at this point the original record is locked. Now see if someone else has
            modified it since we originally loaded it }
          if not origPat^.CompareRecord(tempRec) then
          begin
            if YesNoMsg(HWindow, SR(IDS_WARNING, p1, 200), SR(IDS_PATMODIFIED, p2, 200)) then
              ret:= SaveIt
            else
              ret:= false;
          end
          else  {- nobody else touched it, go ahead an save it }
            ret:= SaveIt;
          dbPat^.dbc^.UnlockRec(tempRec);
        end;
      end;
    end
    else
    begin
      errNum:= DBBeginTransaction;
      if errNum = 0 then
        ret:= SaveIt
      else
      begin
        ret:= false;
        ShowError(@self, IDS_TRANSERROR, nil, errNum, modID, 0);
      end;
    end;
  end;
  if not ret then
    DBAbortTransaction
  else
    DBEndTransaction;
  SaveData:= ret;
  WaitCursor(false);
end;

function TUIPatient.CheckSave(clearAfter: boolean): boolean;
{- Checks to see if any data needs to be saved. This is when any data on the screen
   has changed from the original record that was loaded. This function returns
   true if : 1. No saving is required, 2. The data changed and was saved.
             3. No mode has been established.
             4. The user opts to not save.
   false if: 1. The user attemps to save the data but an error occured.
             2. The user cancels }
var
  ret     : boolean;
  pstr    : array[0..200] of char;
  p2      : array[0..50] of char;
  k       : integer;
begin
  ret:= true;
  if mode <> MODE_NONE then
  begin
    {- first see if any data has changed }
    if DataChanged then
    begin
      k:= YesNoCancelMsg(HWindow, SR(IDS_CONFIRM, p2, 50), SR(IDS_CONFIRMSAVE, pstr, 200));
      if k = IDYES then
        ret:= SaveData(clearAfter)
      else
        ret:= k <> IDCANCEL;
    end;
  end;
  CheckSave:= ret;
end;

{---- field exit routines }

procedure TUIPatient.DoPatIDExit;
var
  id  : word;
begin
  {- if a mode has been established then no need for this processing }
  if (mode = MODE_NONE) then
  begin
    WaitCursor(true);
    {- if field is empty then no processing can occur. }
    if not (pFld^.IsEmpty) then
    begin
      {- attempt to find the patient id, if not found then NEW mode
          assumed, otherwise EDIT mode is assumed }

      pFld^.XferCtlToRec(tempRec);

      if listObj^.FindKey(@self, tempRec, true) then
      begin
        if not dbPat^.dbc^.GotoRecord(tempRec) then
        begin
          ShowError(@self, IDS_CANNOTPOS, nil, dbPat^.dbc^.dbErrorNum, modID, 0);
          SetMode(MODE_NEW);
          origPat^.ClearRecord;
        end
        else
        begin
          dbPat^.dbr^.CopyRecord(tempRec);
          if FillScreen then
            SetMode(MODE_EDIT);
        end;
      end
      else
      begin
        if listObj^.errorNum <> 0 then
          ShowError(@self, IDS_DBERROR, nil, listObj^.errorNum, modID, 2);
        SetMode(MODE_NEW);
        origPat^.ClearRecord;
      end;
    end;
    WaitCursor(false);
  end;
end;

procedure TUIPatient.WMFldList(var msg: TMessage);
{- called when user selects LIST in a field }
var
  id    : word;
  pstr  : array[0..100] of char;
  p2    : array[0..100] of char;
  ret   : longint;    {- return value to the UIFld that created this message }
  cont  : boolean;
begin
  ret:= -1;   {- tell UI field passing this message NOT to list (non-zero) }
  uiFldsProcessFocus:= false;   {- turn off field focus processing }

  id:= msg.wParam;
  if (id = IDC_EDPATID) then
  begin
    cont:= true;
    {- first see if any data has been entered. If so, prompt to save so the user
       wont lose data if they select from a list }
    if mode <> MODE_NONE then
      cont:= CheckSave(false);
    if cont then
    begin
      pFld^.XferCtlToRec(tempRec);
      if listObj^.SelectList(@self, tempRec) then
      begin
        dbPat^.dbc^.GotoRecord(tempRec); {- relocate for next/prev }
        dbPat^.dbr^.CopyRecord(tempRec);
        if FillScreen then
          SetMode(MODE_EDIT);
      end
      else if listObj^.errorNum <> 0 then
      begin
        if listObj^.errorNum = LISTERR_PARTIALKEYNOTFOUND then
          ShowError(@self, IDS_CANNOTLISTNOMATCH, nil, listObj^.errorNum, modID, 0)
        else
          ShowError(@self, IDS_CANNOTLISTBAD, nil, listObj^.errorNum, modID, 0);
      end;
    end;
  end
  else
    ret:= 0;    {- this routine is not handling the list. Let the UIfld handle it }

  uiFldsProcessFocus:= true;  {- turn focus processing back on }

  SetWindowLong(HWindow, DWL_MSGRESULT, ret);  {- set message result (for dialogs) }
end;

procedure TUIPatient.WMLostFocus(var msg: TMessage);
var
  fld   : PUIFld;
  err   : integer;
begin
  fld:= PUIFld(msg.lParam); {- get field pointer to fld losing focus }
  {- first see if the field is valid }
  err:= fld^.IsValid;
  if (err <> 0) and (err <> UIErrRequired) then  {- ignore required err on fld exit }
    ShowUIFldError(@self, err, fld)
  else
  begin
    case fld^.GetID of
      IDC_EDPATID:
        DoPatIDExit;
    end;
  end;
  SetWindowLong(HWindow, DWL_MSGRESULT, 0);  {- set message result (for dialogs) }
end;

{------[ Interfaced Procedures ]--}

procedure UIPatientEntry(aParent: PWindowsObject; aSeq: TSeqNum);
var
  d   : PDialog;
begin
  d:= New(PUIPatient, Init(aParent, aSeq));
  application^.ExecDialog(d);
end;


END.

{- rcf}
