.MODEL large, pascal

PUBLIC BlockCRC
LOCALS

.CODE
; This module calculates the CRC of a block of data.
; This routine uses a byte-wise algorythm to calculate the CRC
; For information on how this works see "Byte-wise CRC Calculations",
;    IEEE Micro, June 83
; All subscripts have been decremented by 1
; the value to xor with CRC in <dx> is n15 through n0
; the byte of memory is m0 to m7, xi = mi xor ri
;
; function BlockCRC( oldCRC     : integer;
;                    blockAddr  : pointer;
;                    blockLen   : integer ): integer;
;
;    Compute the CRC of the block pointed to by blockAddr which is blockLen
;    bytes in size, using OLDCRC as the starting value (for blocks larger
;	than 64K bytes.).  Return the new CRC value.
;

          public    BLOCKCRC

BLOCKCRC  proc far
oldCRC    equ  word ptr ss:[bp+12]
blockAddr equ  dword ptr ss:[bp+8]
blockLen  equ  word ptr  ss:[bp+6]
@@pSize   equ  8

		push	bp
		mov	bp,sp
		push	ds

          mov  dx,oldCRC           ; get initial CRC
          lds  si,blockAddr        ; get address of block
          mov  cx,blockLen         ; get number of bytes
          jcxz nobytes             ; return no change for null block
		jmp	crc1
nobytes:
          mov  ax, dx              ; just return the original CRC if blocksize=0
		jmp	done
crc1:
          lodsb                    ; get next byte
		xor	al,dl
          mov  bh,al               ; <bh> = x7 to x0
          add  al,al               ; <al> = x6 to x0 <pf> = n14 <cf> = x7
          pushf                    ; save <pf> and <cf>
          xor  bh,al               ; <bh> = n13 to n6
          xor  al,al               ; make xx7 = 0 and xx6 = 0
		popf
		jpe	crc2
          mov  al,011b             ; <al1>,<al0> = n14
crc2:
          jnc  crc3
          xor  al,010b             ; if x7 = 1 then xx8 = not xx8
crc3:
          mov  bl,al               ; <b1> = n15, <b0> = n14
          shr  al,1                ; n0 = n15, <a0> = n0
		ror	bx,1
		ror	bx,1
          or   bl,al               ; <bx> = n15 to n0
          xor  bl,dh               ; <bx> = r15 to r0
          mov  dx,bx               ; save CRC in dx
          loop crc1                ; do all bytes
          mov  ax,dx               ; return CRC in AX
done:
          pop  ds
		pop	bp
          ret  @@pSize
blockcrc	endp

END



