(****************************************************************************


uicommon.inc

produced by Borland Resource Workshop

Common UI stuff mod id = $2130 = 8240
*****************************************************************************)

const
  DLG_MOVEISO             = 8240;
  IDC_ISOLBL              = 200;
  IDC_SESSLBL             = 201;
  IDC_SPECLBL             = 202;
  IDC_TXTISO              = 203;
  IDC_TXTSESS             = 204;
  IDC_TXTSPEC             = 205;
  IDC_NEWSESS             = 206;

  DLG_FAMILY              = 8241;
  IDC_NOTEST              = 200;
  IDC_NEGATIVE            = 201;
  IDC_POSITIVE            = 202;
  IDC_FAMILYLIST          = 203;
  IDC_PANELSET            = 204;
  IDC_LBLPANELSET         = 205;
  IDC_ADDTESTGRP          = 206;
  IDC_FAMILYLBL           = 303;


  IDS_UIFLDINITERR        = 8240;
  IDS_SPECLOCKED          = 8241;
  IDS_CANNOTSAVE          = 8242;
  IDS_DBERROR             = 8243;
  IDS_DBNOMOREDATA        = 8244;
  IDS_TRANSERROR          = 8245;
  IDS_CANTDEL             = 8246;
  IDS_CANTALLOCREC        = 8247;
  IDS_CANTCOPYREC         = 8248;
  IDS_CANTLOCATEORIGSPEC  = 8249;
  IDS_TRYRECREATE         = 8250;
  IDS_SPECMODIFIED        = 8251;
  IDS_CANNOTLISTBAD       = 8253;
  IDS_CANNOTLISTNOMATCH   = 8254;
  IDS_ISOLOCKED           = 8255;
  IDS_CANTLOCATEORIGISO   = 8256;
  IDS_DUPSPEC             = 8257;
  IDS_NOSPECS             = 8258;
  IDS_CONFIRMSAVE         = 8259;
  IDS_PATSAVERROR         = 8260;
  IDS_CONFIRMDELSPEC      = 8261;
  IDS_ERRLODPAT           = 8263;
  IDS_DUPPAT              = 8264;
  IDS_PATMODIFIED         = 8265;
  IDS_CANTLOCATEORIGPAT   = 8266;
  IDS_PATLOCKED           = 8267;
  IDS_ERRLODSPEC          = 8268;
  IDS_CANNOTPOS           = 8269;
  IDS_SPECNOTFOUND        = 8270;
  IDS_CANTALLOCLISTREC    = 8271;
  IDS_ERROROPEN           = 8272;
  IDS_ISOMODIFIED         = 8273;
  IDS_DUPISO              = 8274;
  IDS_CANTINITRESULTS     = 8275;
  IDS_CONFIRMDELISO       = 8276;
  IDS_CANTINITLISTOBJ     = 8277;
  IDS_CANTEDITFAMILY      = 8278;
  IDS_NEWSPEC             = 8279;
  IDS_EDITSPEC            = 8280;
  IDS_NEWPAT              = 8281;
  IDS_EDITPAT             = 8282;
  IDS_NEWISO              = 8283;
  IDS_EDITISO             = 8284;
  IDS_ERRLODISO           = 8285;
	IDS_CANTOPENORDERDEF	=	8286;
	IDS_CANTOPENISOORD	=	8287;
	IDS_CANTCREATEISOORD	=	8288;
	IDS_CANTLOCORDDEF	=	8289;
	IDS_ERRUPDISOS	=	8290;
	IDS_AVAIL1	=	8262;
	IDC_INVALIDLISTOBJ	=	8252;
	IDS_NOISOINSESS	=	8291;
	IDS_SESSIONLOG	=	8292;
	DLG_ORG	=	8242;
	IDC_EDOrgnum	=	101;
	IDC_LBLOrgnum	=	102;
	IDC_Browse	=	103;
