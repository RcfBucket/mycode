Unit AutoLdr;

{$DEFINE WACONNECT}

Interface

{$IFDEF WACONNECT }
Uses
  WinTypes,
  WinProcs,
  WObjects,
	Strings,
  WLMScan,
  WLWAGpib,
  WLWUtil,
  WLDMStrg;

{$ELSE}
Uses
	WinTypes,
  WLmScan,
  WinProcs,
  WObjects,
  Strings,
  WLWUtil,
  WLDMStrg;

{$ENDIF}

Const
  id_FName             = 101;
  id_BlocksSent        = 102;

  FileList = 'WAFILES.LST';
  ParamsList = 'WAPARMS.LST';

  WaBase = 6100;

Type
  { Misc Types}
  FNameType            = String[12];

  PAutoLoadDlg = ^TAutoLoadDlg;
  TAutoLoadDlg = object(TDlgWindow)
    LastFName: FNameType;
    FileName: FNameType;
    Procedure NewBlockCount (Var Msg: TMessage); virtual wm_BlockCount;
    Procedure UpdateStatusWnd(BlocksXfered: Word);
    Constructor Init(AParent: PWindowsObject; AName: PChar);
    Procedure DoIt;
  End;

  Procedure ExecAutoLoad(TempWnd: HWnd; SelfPtr: PWindowsObject);

Implementation

{-----------------------------------------------------------------------}
{									}
{-----------------------------------------------------------------------}
Procedure TAutoLoadDlg.UpdateStatusWnd(BlocksXfered: Word);
Var
  TStr : Array [0..12] of Char;
Begin
  If FileName <> LastFName Then
  Begin
    StrPCopy(TStr,FileName);
    SetDlgItemText(HWindow, id_FName, TStr);
    LastFName:= FileName;
  End;
  Str(BlocksXfered,TStr);
  SetDlgItemText(HWindow, id_BlocksSent, TStr);
End;


Procedure TAutoLoadDlg.DoIt;
Var
  X: Word;
  FileLine: String;
  f: Text;
  LocalName: String;
  RemoteName: String;
  AOK: Boolean;
  Result: Integer;
  SendAlignment: Boolean;
  Wind: HWnd;
  PStr: Array [0..256] Of Char;
	PStr2: Array [0..256] Of Char;
  waCmd : PWAResetCmd;

Begin
	Wind := GetFocus;
  GetStr (WaBase + 28, PStr, 256);       { Update WAMain and WalkAway data files? }
  GetStr (WaBase + 01, PStr2, 256);      { Walkaway loader }
	IF MessageBox(HWindow,PStr,pStr2,mb_YesNo+mb_iconStop) = idNo THEN EXIT;
  GetStr (WaBase + 03, PStr, 80);       { Send alignment parameters file to Walkaway? }
	Result := MessageBox (GetFocus, PStr, PStr2, mb_YesNo Or mb_DefButton2);
  SetFocus (Wind);
  SendAlignment := (Result = idYes);
  If SendAlignment Then
  Begin
      GetStr (WaBase + 04, PStr, 150);  { WARNING! Sending this file will delete any existing parameters file! }
      GetStr (WaBase + 05, PStr2, 100); { Are you sure you want to send this file? }
			StrCat (PStr, Chr (13));
      StrCat (PStr, PStr2);
      GetStr (WaBase + 06, PStr2, 100); { Are you sure? }
      Result := MessageBox (GetFocus, PStr, PStr2, mb_IconExclamation Or mb_YesNo);
      SendAlignment := (Result = idYes);
  End;

  If Not FileExists (FileList) Then
  Begin
      GetStr (WaBase + 07, PStr, 150);  { File list not found! }
      GetStr (WaBase + 01, PStr2, 100); { Walkaway Loader }
      MessageBox(GetFocus, PStr, PStr2, mb_OK);
      Exit;
  End;

  If SendAlignment Then
  Begin
      If Not FileExists (ParamsList) Then
      Begin
          GetStr (WaBase + 08, PStr, 150);      { Params file list not found! }
          GetStr (WaBase + 01, PStr2, 100);     { Walkaway Loader }
          MessageBox(GetFocus, PStr, PStr2, mb_OK);
          Exit;
      End;
  End;

  SetFocus (Wind);
  Assign (f, FileList);
  Reset (f);
  AOK := (GetWaTypeFromROM <> ROMWaUnknown);
  While (AOK) And (Not Eof (f)) Do
  Begin
    GetNextLine (f, SlotsPerTower, LocalName, RemoteName);
   	FileName := LocalName;
   	AOK := SendAFile (LocalName, RemoteName, HWindow);
  End;

  Close (f);

  If (AOK) And (SendAlignment) Then
  Begin
      Assign (f, ParamsList);
      Reset (f);
      GetNextLine (f, SlotsPerTower, LocalName, RemoteName);
      FileName := LocalName;
      AOK := SendAFile (LocalName, RemoteName, HWindow);
      Close (f);
  End;

  If Not AOK Then
		FatalWaError (HWindow)
	ELSE
    BEGIN
      GetStr (WaBase + 27, PStr, 256);       { Done! }
      GetStr (WaBase + 01, PStr2, 256);      { Walkaway loader }
			waCmd := NEW(PWAResetCmd,Init(WAID));
			waCmd^.SetParams(0);
			waCmd^.SendWAMessage;
 			MessageBox(HWindow,PStr,PStr2,mb_OK);
			Dispose(waCmd,Done);
			WritePrivateProfileString(appName,'WAPANELID','1',profFile);
		END;
End;

Constructor TAutoLoadDlg.Init(AParent: PWindowsObject; AName: PChar);
Begin
  TDlgWindow.Init(AParent,AName);
  LastFName:= '';
End;

Procedure TAutoLoadDlg.NewBlockCount (Var Msg: TMessage);
Begin
    UpdateStatusWnd (Msg.wParam);
End;


Procedure ExecAutoLoad(TempWnd: HWnd; SelfPtr: PWindowsObject);
Var
    AutoLoadDlg : PAutoLoadDlg;
Begin
    AutoLoadDlg:= New(PAutoLoadDlg,Init(SelfPtr,'AUTO_LOAD'));
    Application^.MakeWindow(AutoLoadDlg);
    AutoLoadDlg^.DoIt;
    Dispose(AutoLoadDlg,Done);
End;

End.
