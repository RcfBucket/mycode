unit Status;

INTERFACE

uses
  ApiTools,
  WinTypes,
  WinProcs,
  WinCrt,
  OWindows,
  ODialogs,
  DlgLib,
  Strings;

type
  PGenStatDlg = ^TGenStatDlg;
  TGenStatDlg = object(TCenterDlg)
    cont    : boolean;
    {}
    constructor Init(aParent: PWindowsObject; aName: PChar);
    procedure SetText(text: PChar; id: integer);
    procedure Update; virtual;
    procedure Cancel(var msg: TMessage); virtual ID_FIRST + ID_CANCEL;
    function CanContinue: boolean;
    procedure SetContinue(on: boolean);
  end;

  PPctStatDlg = ^TPctStatDlg;
  TPctStatDlg = object(TGenStatDlg)
    pctColor    : TColorRef;
    textColor   : TColorRef;
    pctLow      : integer;
    pctHigh     : integer;
    pctCurrent  : integer;
    pctOld      : integer;
    displayPct  : boolean;
    percentBar  : integer;
    pctR        : TRect;
    {}
    constructor Init(AParent: PWindowsObject; AName: PChar; DrawTxt: boolean; pb: integer);
    procedure SetupWindow; virtual;
    procedure AddPctLevel(pctLevel: integer);
    procedure IncrPctLevel(pctLevel: integer);
    procedure CompPctLevel(cur: longint; total: longint);
    procedure ResetPct;
    private
    procedure WMPaint(var msg: TMessage); virtual $0088;
    procedure SetDefaults;
    procedure DrawPct;
    procedure DrawPercent;
    procedure DrawPctText;
  end;

procedure DrawInFrame(PaintDC: HDC; PaintR: TRect; back: boolean; width: integer);
procedure DrawOutFrame(PaintDC: HDC; PaintR: TRect; back: boolean; width: integer);

IMPLEMENTATION

const
  OutWidth = 3;

{-----------------------------------------------------------------------------}
{                       Miscellaneous Support Routines                        }
{-----------------------------------------------------------------------------}
{----------------------[DrawOutFrame]-----------------------------------------}
procedure DrawOutFrame(PaintDC: HDC; PaintR: TRect; back: boolean; width: integer);
var
  ThePen    : HPen;
  OldPen    : HPen;
  FillBrush : HBrush;
  OldBrush  : HBrush;
  count     : integer;
  CalcR     : TRect;
begin
  if Back then
  begin
    FillBrush:= CreateSolidBrush($00C0C0C0);
    OldBrush:= SelectObject(PaintDC, FillBrush);
    InflateRect(PaintR, -1 * (Width) + 1, -1 * (Width) + 1);
    FillRect(PaintDC, PaintR, FillBrush);
    InflateRect(PaintR, width-1, width-1);
    SelectObject(PaintDC, OldBrush);
    DeleteObject(FillBrush);
  end;

  CalcR:= PaintR;

  for count:= 0 to (Width-1) do
  begin
    PaintR:= CalcR;
    InflateRect(PaintR, -1 * (count), -1 * (count));

    ThePen:= CreatePen(PS_SOLID, 1, RGB(255,255,255));
    OldPen:= SelectObject(PaintDC, ThePen);
    MoveTo(PaintDC, PaintR.right, PaintR.top);
    LineTo(PaintDC, PaintR.left, PaintR.top);
    LineTo(PaintDC, PaintR.left, PaintR.bottom + 1);
    SelectObject(PaintDC, OldPen);
    DeleteObject(ThePen);

    ThePen:= CreatePen(PS_SOLID, 1, RGB(127,127,127));
    OldPen:= SelectObject(PaintDC, ThePen);
    MoveTo(PaintDC, PaintR.right, PaintR.top + 1);
    LineTo(PaintDC, PaintR.right, PaintR.bottom);
    LineTo(PaintDC, PaintR.left, PaintR.bottom);
    SelectObject(PaintDC, OldPen);
    DeleteObject(ThePen);
  end;
end;

{-----------------------[DrawInFrame]-----------------------------------------}
procedure DrawInFrame(PaintDC: HDC; PaintR: TRect; back: boolean; width: integer);
var
  ThePen     : HPen;
  OldPen     : HPen;
  FillBrush  : HBrush;
  OldBrush   : HBrush;
  count      : integer;
  CalcR      : TRect;
begin
  if Back then
  begin
    FillBrush:= CreateSolidBrush($00C0C0C0);
    OldBrush:= SelectObject(PaintDC, FillBrush);
    InflateRect(PaintR, -1 * (Width) + 1, -1 * (Width) + 1);
    FillRect(PaintDC, PaintR, FillBrush);
    InflateRect(PaintR, Width-1, Width-1);
    SelectObject(PaintDC, OldBrush);
    DeleteObject(FillBrush);
  end;

  CalcR:= PaintR;

  for count:= 0 to (Width-1) do
  begin
    PaintR:= CalcR;
    InflateRect(PaintR, -1 * (count), -1 * (count));

    ThePen:= CreatePen(PS_SOLID, 1, RGB(127,127,127));
    OldPen:= SelectObject(PaintDC, ThePen);
    MoveTo(PaintDC, PaintR.right, PaintR.top);
    LineTo(PaintDC, PaintR.left, PaintR.top);
    LineTo(PaintDC, PaintR.left, PaintR.bottom);
    SelectObject(PaintDC, OldPen);
    DeleteObject(ThePen);

    ThePen:= CreatePen(ps_Solid,1,RGB(255,255,255));
    OldPen:= SelectObject(PaintDC,ThePen);
    MoveTo(PaintDC,PaintR.right,PaintR.top+1);
    LineTo(PaintDC,PaintR.right,PaintR.bottom);
    LineTo(PaintDC,PaintR.left-1,PaintR.bottom);
    SelectObject(PaintDC,OldPen);
    DeleteObject(ThePen);
  end;
end;

{----------------------------[Init]-------------------------------------------}
{-----------------------------------------------------------------------------}
{                      TPctStatDlg Routines                                   }
{-----------------------------------------------------------------------------}
constructor TPctStatDlg.Init(AParent: PWindowsObject;
                             AName  : PChar;
                             DrawTxt: Boolean;
                             pb     :integer);
begin
  inherited Init(AParent,AName);
  DisplayPct:= DrawTxt;
  PercentBar:= pb;
end;

{----------------------------[SetupWindow]------------------------------------}
procedure TPctStatDlg.SetupWindow;
begin
  inherited SetupWindow;
  if percentBar <> 0 then
  begin
    SetDefaults;
    SendDlgItemMessage(HWindow, percentBar, WM_SETTEXT, 0, 0);
    DrawPct;
  end;
end;

{---------------------------[SetDefaults]-------------------------------------}
procedure TPctStatDlg.SetDefaults;
var
  DC     : HDC;
  Point  : TPoint;
  DlgR   : TRect;
  count  : integer;
begin
  pctLow:= 0;
  pctHigh:= 100;
  pctCurrent:= PctLow;
  pctOld:= -1;

  GetClientRect(HWindow,DlgR);
  Point.X:= DlgR.left; Point.Y:= DlgR.top;
  ClientToScreen(HWindow,Point);
  DlgR.left:= Point.X; DlgR.top:= Point.Y;
  Point.X:= DlgR.right; Point.Y:= DlgR.bottom;
  ClientToScreen(HWindow,Point);
  DlgR.right:= Point.X; DlgR.bottom:= Point.Y;

  GetWindowRect(GetDlgItem(HWindow, PercentBar), PctR);
  with PctR do
  begin
    top:= top - DlgR.top;
    bottom:= bottom - DlgR.top;
    left:= left - DlgR.left;
    right:= right - DlgR.left;
  end;
  PctColor:= RGB(64,64,64);
  TextColor:= RGB(0,0,128);
end;

{---------------------------[incrPctLevel]-----------------------------------}
procedure   TPctStatDlg.IncrPctLevel(PctLevel : integer);
begin
  PctCurrent:= PctLevel;
  if PctCurrent > PctHigh then
    PctCurrent:= PctHigh;
  if PctCurrent < PctLow then
    PctCurrent:= PctLow;
  Update;
  DrawPct;
end;

{---------------------------[compPctLevel]-----------------------------------}
procedure TPctStatDlg.CompPctLevel(cur: longint; total: longint);
begin
  if (total <= 0) or (cur = 0) then
    pctCurrent:= 0
  else
  begin
    PctCurrent:= integer(round((cur/total) * 100));
    if PctCurrent > PctHigh then
      PctCurrent:= PctHigh;
    if PctCurrent < PctLow then
      PctCurrent:= PctLow;
  end;
  Update;
  DrawPct;
end;

{---------------------------[AddPctLevel]-------------------------------------}
procedure TPctStatDlg.AddPctLevel(PctLevel: integer);
begin
  PctCurrent:=PctCurrent+PctLevel;
  if PctCurrent > PctHigh then
    PctCurrent:= PctHigh;
  if PctCurrent < PctLow then
    PctCurrent:= PctLow;
  Update;
  DrawPct;
end;

{---------------------------[ResetPct]----------------------------------------}
procedure TPctStatDlg.ResetPct;
begin
  PctCurrent:= 0;
  if PctCurrent > PctHigh then
    PctCurrent:= PctHigh;
  if PctCurrent < PctLow then
    PctCurrent:= PctLow;
  Update;
  DrawPct;
end;

{---------------------------[DrawPct]-----------------------------------------}
procedure TPctStatDlg.DrawPct;
begin
  if percentBar <> 0 then
  begin
    if PctOld <> PctCurrent then
    begin
      PctOld:= PctCurrent;
      DrawPercent;
      if DisplayPct then
        DrawPctText;
    end;
    if PctCurrent = PctLow then
      DrawPercent;
  end;
end;

{---------------------------[DrawPercent]-------------------------------------}
procedure TPctStatDlg.DrawPercent;
var
 InR      : TRect;
 OutR     : TRect;
 TempR    : TRect;
 PaintDC  : HDC;
 TheBrush : HBrush;
 OldBrush : HBrush;
 ThePen   : HPen;
 OldPen   : HPen;
 MemDC    : HDC;
 TheBits  : HBitmap;
 OldBits  : HBitmap;

begin
  TempR:= PctR;
  TempR.right:= TempR.right-TempR.left;
  TempR.left:= 0;
  TempR.bottom:= TempR.bottom-TempR.top;
  TempR.top:= 0;
  InR:= TempR;
  OutR:= TempR;
  InflateRect(InR, -1, -1);
  InflateRect(OutR, -1, -1);
  InR.bottom:= InR.bottom + 1;
  InR.right:= InR.right + 1;
  OutR.bottom:= OutR.bottom - 1;
  if (PctCurrent-PctLow) <> 0 then
    InR.left:=InR.left + integer(Trunc((InR.right-InR.left) *
    ((PctCurrent - PctLow) / (PctHigh - PctLow))));

  OutR.right:= InR.left + 1;
  PaintDC:= GetDC(HWindow);
  MemDC:= CreateCompatibleDC(PaintDC);
  TheBits:= CreateCompatibleBitmap(PaintDC, TempR.right, TempR.bottom);
  OldBits:= SelectObject(MemDC, TheBits);

  TheBrush:= GetStockObject(NULL_BRUSH);
  OldBrush:= SelectObject(MemDC, TheBrush);
  ThePen:= CreatePen(PS_SOLID, 1, GetSysColor(COLOR_WINDOWFRAME));
  OldPen:= SelectObject(MemDC, ThePen);
  Rectangle(MemDC, TempR.left, TempR.top, TempR.right, TempR.bottom);
  SelectObject(MemDC, OldBrush);
  DeleteObject(TheBrush);
  SelectObject(MemDC, OldPen);
  DeleteObject(ThePen);

  if (PctCurrent <> PctHigh) then
  begin
    TheBrush:= CreateSolidBrush($00C0C0C0);
    OldBrush:= SelectObject(MemDC,TheBrush);
    ThePen:= GetStockObject(Null_Pen);
    OldPen:= SelectObject(MemDC,ThePen);
    Rectangle(MemDC, InR.left, InR.top, InR.right, InR.bottom);
    InR.right:= InR.right - 2;
    InR.bottom:= InR.bottom - 2;
    InflateRect(InR, -2, -2);
    DrawInFrame(MemDC, InR, true, 1);
    InflateRect(InR, 2, 2);
    InR.right:= InR.right + 2;
    InR.left:= InR.left + 1;
    InR.bottom:= InR.bottom + 2;
    SelectObject(MemDC, OldBrush);
    DeleteObject(TheBrush);
    SelectObject(MemDC, OldPen);
    DeleteObject(ThePen);
  end;

  if PctCurrent <> PctLow then
  begin
    if OutR.right > (TempR.right - 2) then
      OutR.right:= TempR.right - 2;
    DrawOutFrame(MemDC, OutR, true, OutWidth);
  end;

  BitBlt(PaintDC, PctR.left, PctR.top, TempR.right, TempR.bottom, MemDC, 0, 0, SRCCOPY);
  SelectObject(MemDC, OldBits);
  DeleteObject(TheBits);
  ReleaseDC(HWindow, PaintDC);
  DeleteDC(MemDC);
end;

{---------------------------[DrawPctText]-------------------------------------}
procedure TPctStatDlg.DrawPctText;
var
  paintR   : TRect;
  buffer   : array[0..10] of char;
  extent   : longint;
  paintDC  : HDC;
begin
  PaintDC:= GetDC(HWindow);
  SetTextAlign(PaintDC,TA_TOP or TA_LEFT);
  SetBkMode(PaintDC, Transparent);
  SetTextColor(PaintDC, TextColor);
  Str(PctCurrent, buffer);
  StrCat(buffer, '%');
  extent:= GetTextExtent(PaintDC, buffer, StrLen(buffer));
  TextOut(PaintDC,
      PctR.left + ((PctR.right-PctR.left-Loword(Extent)) div 2),
      PctR.top  + ((PctR.bottom-PctR.top-Hiword(Extent)) div 2),
      buffer, StrLen(buffer));
  ReleaseDC(HWindow,PaintDC);
end;

{---------------------------[WMPaint]-----------------------------------------}
procedure TPctStatDlg.WMPaint(var Msg:TMessage);
begin
  if percentBar <> 0 then
  begin
    PctOld:= -1;
    DrawPct;
  end
  else
    DefWndProc(msg);
end;


{-----------------------------------------------------------------------------}
{                      TGenStatDlg Routines                                   }
{-----------------------------------------------------------------------------}
constructor TGenStatDlg.Init(aParent: PWindowsObject; aName: PChar);
begin
  inherited Init(aParent, aName);
  cont:= true;
  EnableKBHandler;
  HandleFocus(false);
end;

{---------------------------[SetText]-----------------------------------------}
procedure TGenStatDlg.SetText(text: PChar; id: integer);
begin
  if id <> 0 then {- set text of control }
    SendDlgItemMsg(id, WM_SETTEXT, 0, longint(text))
  else {- set caption of dialog }
    SendMessage(HWindow, WM_SETTEXT, 0, longint(text));
  Update;
end;

{---------------------------[Update]-----------------------------------------}
procedure TGenStatDlg.Update;
var
  msg : TMsg;
begin
  HandleEvents(HWindow);
(*  if parent <> nil then*)
(*  begin*)
(*    while PeekMessage(Msg, 0, 0, 0, PM_REMOVE) do*)
(*      if not IsDialogMessage(HWindow, msg) then*)
(*      begin*)
(*        TranslateMessage(Msg);*)
(*        DispatchMessage(Msg);*)
(*      end;*)
(*  end;*)
end;

{---------------------------[Cancel]------------------------------------------}
procedure TGenStatDlg.Cancel(var Msg: TMessage);
begin
  cont:= false;
end;

function TGenStatDlg.CanContinue: boolean;
begin
  HandleEvents(hWindow);
  CanContinue:= cont;
end;

procedure TGenStatDlg.SetContinue(on: boolean);
{- set the continue flag on or off }
begin
  cont:= on;
end;

END.
