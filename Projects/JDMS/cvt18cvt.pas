unit Cvt18Cvt;

{- this unit drives the conversion process for version 18.10 data }

INTERFACE

uses
  IntlLib,
  CvtMisc,
  OWindows;

procedure Cvt18ConvertData(aParent: PWindowsObject;
                           aMaps: PMapCollection;
                           aCollDate: TIntlDate);

IMPLEMENTATION

uses
  V3ResLib,
  StrSW,
  Bits,
  MScan,
  Strings,
  dbFile,
  dbids,
  dblib,
  dbtypes,
  Cvt18Col,
  Cvt18Dlg,
  Cvt18Msc,
  ApiTools,
  WinProcs,
  WinTypes,
  ODialogs,
  UserMsgs,
  DlgLib;

{$I CVT18.INC}

const
  BL   = 0;    {- shift constants - used to shift the result to bit 0 and 1 }
  OXI  = 2;    {- ie, extraTests shr OXI would put the oxi result in bit 0 & 1 }
  STS  = 4;
  GMS  = 6;
  TFG  = 8;

type
  PCvtDlg = ^TCvtDlg;
  TCvtDlg = object(TCenterDlg)
    collDate   : TIntlDate;
    maps       : PMapCollection;
    stopped    : boolean;
    working    : boolean;
    finalizing : boolean;
    listBox    : PListBox;
    mergeDir   : array[0..100] of char;
    restoreDir : array[0..100] of char;
    mDataFile  : file;
    specFile   : PDBFile;
    constructor Init(aParent: PWindowsObject; aMaps: PMapCollection; aCollDate: TIntlDate);
    procedure SetupWindow; virtual;
    procedure StartProcess(var msg: TMessage); virtual WM_FIRST + WM_FMPROCESS;
    procedure IDStop(var msg: TMessage); virtual ID_FIRST + IDC_STOP;
    procedure IDFinish(var msg: TMessage); virtual ID_FIRST + IDC_FINISH;
    procedure IDLog(var msg: TMessage); virtual ID_FIRST + IDC_LOG;
    procedure EnableButtons; virtual;
    function  CanClose: boolean; virtual;
    function  ConvertPatients: integer;
    function  ConvertSpecimens: integer;
    function  GetPatientSeqRef(mCurrPatID: PChar): TSeqNum;
    function  GetMnemSeqRef(mID, fName: PChar; tag: integer): TSeqNum;
    function  ConvertResults: integer;
    procedure GetSpecimenData(specID: PChar; var specRef: TSeqNum; var aCollDate: TIntlDate);
    procedure InitFiles;
    procedure CleanupFiles;
  end;


constructor TCvtDlg.Init(aParent: PWindowsObject; aMaps: PMapCollection;
                         aCollDate: TIntlDate);
begin
  inherited Init(aParent, MakeIntResource(DLG_CVT18CONVERT));
  specFile:= nil;
  listBox:= New(PListBox, InitResource(@self,IDC_GRID));
  StrCopy(restoreDir, curDMSDir);
  StrCat(restoreDir, OldDataDir);
  StrCopy(mergeDir, curDMSDir);
  StrCat(mergeDir, MergeDataDir);
  maps:= aMaps;
  collDate:= aCollDate;
  stopped:= false;
  working:= true;
  finalizing:= false;
  LoadPanelInfo;
  LoadTstGrpInfo(mergeDir);
  InitFiles;
end;

procedure TCvtDlg.SetupWindow;
begin
  inherited SetupWindow;
  PostMessage(hWindow, WM_FMPROCESS, 0, 0);
end;

procedure TCvtDlg.InitFiles;
var
  pstr  : array[0..100] of char;
begin
  StrCopy(pstr, restoreDir);
  StrCat(pstr, 'DM$DATA.DAT');
  Assign(mDataFile, pstr);
  Reset(mDataFile, 1);
  if IOResult <> 0 then
    CriticalError('Cannot open the V18.10 DM$DATA.DAT.');

  specFile:= New(PDBFile, Init('SPECIMEN', mergeDir, dbOpenNormal));
  if specFile = nil then
    CriticalError('Cannot open V3.02 SPECIMEN file.');

end;

procedure TCvtDlg.CleanupFiles;
begin
  Close(mDataFile);
  if IOResult <> 0 then {nothing};

  if specFile <> nil then
    Dispose(specFile, Done);
  specFile:= nil;
end;

procedure TCvtDlg.EnableButtons;
begin
  if stopped then
  begin
    EnableWindow(GetItemHandle(IDC_FINISH), false);
    EnableWindow(GetItemHandle(IDC_STOP), false);
    EnableWindow(GetItemHandle(IDC_LOG), false);
    EnableWindow(GetItemHandle(ID_OK), true);
  end
  else if finalizing then
  begin
    EnableWindow(GetItemHandle(IDC_FINISH), false);
    EnableWindow(GetItemHandle(IDC_STOP), false);
    EnableWindow(GetItemHandle(IDC_LOG), not working);
    EnableWindow(GetItemHandle(ID_OK), not working);
  end
  else if working then
  begin
    EnableWindow(GetItemHandle(IDC_FINISH), false);
    EnableWindow(GetItemHandle(IDC_STOP), true);
    EnableWindow(GetItemHandle(IDC_LOG), false);
    EnableWindow(GetItemHandle(ID_OK), false);
  end
  else
  begin
    EnableWindow(GetItemHandle(IDC_FINISH), true);
    EnableWindow(GetItemHandle(IDC_STOP), false);
    EnableWindow(GetItemHandle(IDC_LOG), true);
    EnableWindow(GetItemHandle(ID_OK), true);
  end;
end;

procedure TCvtDlg.StartProcess(var msg: TMessage);
var
  retVal : integer;
  pstr   : array[0..100] of char;
begin
  retVal:= continueJob;

  retVal:= ConvertPatients;

  if retVal = continueJob then
    retVal:= ConvertSpecimens;

  if retVal = continueJob then
    retVal:= ConvertResults;

  CleanupFiles;

  if retVal = continueJob then
  begin
    working:= false;
    EnableButtons;
    if not stopped then
      SetDlgItemText(hWindow, IDC_TEXT0, 'Conversion process COMPLETE.')
    else
      SetDlgItemText(hWindow, IDC_TEXT0, 'Conversion process CANCELLED.');
    MessageBeep(0);
  end
  else
  begin
    EndDlg(IDOK);
  end;
  FocusCtl(hWindow, IDC_FINISH);
end;

procedure TCvtDlg.IDStop(var msg: TMessage);
begin
  if not stopped then
  begin
    stopped:= YesNoMsg(hWindow, 'Confirm', 'Conversion interrupted by user.'#10#10+
                       'Are you sure want to cancel the conversion process?');
    if stopped then
    begin
      LogStr('* CONVERTION STOPPED BY USER *');
      LogLn;
    end;
  end;
end;

function TCvtDlg.CanClose: boolean;
begin
  if not stopped and working then
    CanClose:= false
  else if stopped then
    CanClose:= true
  else if finalizing then
    CanClose:= true
  else
    CanClose:= ConfirmExit(hWindow);
end;

procedure TCvtDlg.IDLog(var msg: TMessage);
begin
  ShowLogFile(@self);
end;

procedure TCvtDlg.IDFinish(var msg: TMessage);
var
  pstr  : array[0..100] of char;
begin
  finalizing:= true;
  working:= true;
  EnableButtons;
  listBox^.ClearList;
  SetDlgItemText(hWIndow, IDC_TEXT0, 'Finalizing Conversion.');

  listBox^.AddString('Copying database files.');
  HandleEvents(hWindow);
  {- copy merge database to Online database }
  StrCopy(pstr, curDMSDir);
  StrCat(pstr, MergeDataDir);

  if CopyFiles(pstr, curDMSDir, '*.DB*') <> 0 then
  begin
    LogStr('FATAL ERROR OCCURED COPYING MERGE FILES TO ONLINE DATABASE.'); LogLn;
    LogStr('--> A fatal error has occured. Your online database is not valid. You must');
    LogLn;
    LogStr('--> restore a previous backup to continue using the DMS.');
    LogLn;
    ErrorMsg(hWindow, 'FATAL ERROR',
    'A fatal error has occured. You online database is not valid. '#10+
    'You must restore a previous backup to continue using the DMS.');
    EndDlg(IDOK);
    Exit;
  end
  else
    FinalizeClean;

  working:= false;
  EnableButtons;
  LogStr('Conversion process completed successfully!'); LogLn;
  SetDlgItemText(hWIndow, IDC_TEXT0, 'Finalization COMPLETE.');
  listBox^.ClearList;
  MessageBeep(0);
  FocusCtl(hWindow, ID_OK);
  LogEnd;
end;

procedure CheckDBError(db: PDBFile; keyStr: PChar);
var
  pstr    : array[0..255] of char;
  fi      : TFileInfo;
begin
  if db^.dbc^.dbErrorNum <> 0 then
  begin
    db^.dbd^.FileInfo(fi);
    LogHex(db^.dbc^.dbErrorNum);
    if db^.dbc^.dbErrorNum = $6005 then  {- check duplicate }
    begin
      StrCopy(pstr, ' - Duplicate ID in ');
      StrCat(pstr, fi.desc);
      StrCat(pstr, ' - ID: ');
      StrCat(pstr, keyStr);
      StrCat(pstr, ' - record not converted.');
      LogStr(pstr);
      LogLn;
    end
    else
    begin
      StrCopy(pstr, ' - Cannot add record to ');
      StrCat(pstr, fi.desc);
      StrCat(pstr, ' - ID: ');
      StrCat(pstr, keyStr);
      CriticalError(pstr);
    end;
  end;
end;

function TCvtDlg.ConvertPatients: integer;
var
  retVal      : integer;
  mV18Rec     : TV18DataRec;
  mPatIdxFile : PDBFile;
  patFile     : PDBFile;
  tRecNum     : integer;
  seekNum     : longInt;
  mCurrPatID  : array[0..20] of char;
  mCurrPatLN  : array[0..20] of char;
  mCurrPatFN  : array[0..20] of char;

  procedure DoMappedFlds;
  var
    obj   : PMapObj;
    k     : integer;
    pstr  : array[0..100] of char;

    procedure DoDate(aFld: word; aV3Fld: integer);
    var
      d     : TIntlDate;
    begin
      d:= ConvertV18Date(aFld);
      patFile^.dbr^.PutField(aV3Fld, @d);
    end;

    procedure DoMnem(anID: PChar; tag, v3Fld: integer);
    var
      seq   : TSeqNum;
      fi    : TFldInfo;
      ai    : TAssocInfo;
      pstr  : array[0..10] of char;
    begin
      patFile^.dbd^.FieldInfo(v3Fld, fi);
      if fi.fldAssoc > 0 then
      begin
        patFile^.dbd^.AssocInfo(fi.fldAssoc, ai);
        GetV18Field(pstr, anID, 3);
        seq:= GetMnemSeqRef(pstr, ai.fName, tag);
        patFile^.dbr^.PutField(v3Fld, @seq);
      end;
    end;

  begin
    {- mapped collection indexs 0-10 are used for patient }
    for k:= 0 to 10 do
    begin
      obj:= maps^.At(k);
      if obj^.fld3Num > 0 then    {- field is mapped }
      begin
        case k of
          0:  {- Date of birth }
            DoDate(mV18Rec.pat_DOB, obj^.fld3Num);
          1:  {- Gender }
            if mv18Rec.pat_sex <> 0 then
            begin
              Str(mv18Rec.pat_sex, pstr);
              patFile^.dbr^.PutFieldAsStr(obj^.fld3Num, pstr);
            end;
          2:  {- Race }
            if ord(mV18Rec.pat_race) > 32 then
            begin
              pstr[0]:= mV18Rec.pat_race;
              pstr[1]:= #0;
              patFile^.dbr^.PutFieldAsStr(obj^.fld3Num, pstr);
            end;
          3:  {- Date In }
            DoDate(mV18Rec.pat_dateIn, obj^.fld3Num);
          4:  {- Date Out }
            DoDate(mV18Rec.pat_dateOut, obj^.fld3Num);
          5:  {- Physician }
            DoMnem(mV18Rec.pat_physID, 3, obj^.fld3Num);
          6:  {- Patient Ward }
            DoMnem(mV18Rec.pat_ward, 0, obj^.fld3Num);
          7:  {- Room Number }
            begin
              FillChar(pstr[0], 6, 0);
              Move(mV18rec.pat_roomNum, pstr[0], 5);
              patFile^.dbr^.PutFieldAsStr(obj^.fld3Num, pstr);
            end;
          8:  {- patient In/Out status }
            begin
              if mV18rec.pat_pFlags and 4 <> 4 then
              begin
                if mV18rec.pat_pFlags and 2 = 2 then
                  StrCopy(pstr, '1')  {- in Patient }
                else
                  StrCopy(pstr, '0'); {- Out Patient }
                patFile^.dbr^.PutFieldAsStr(obj^.fld3Num, pstr);
              end;
            end;
          9:  {- Institute }
            DoMnem(mV18Rec.pat_institute, 5, obj^.fld3Num);
          10: {- Service }
            DoMnem(mV18Rec.pat_service, 4, obj^.fld3Num);
        end;
      end;
    end;
  end;

begin
  retVal:= continueJob;

  AddNameToList('Patient Database', listBox);

  mPatIdxFile:=  New(PDBFile, Init('PAT18IDX', restoreDir, dbOpenNormal));
  if mPatIdxFile = nil then
    CriticalError('Cannot open the patient index file.');
  patFile:=  New(PDBFile, Init('PATIENT',  mergeDir, dbOpenNormal));
  if patFile = nil then
    CriticalError('Cannot open the patient file.');

  if mPatIdxFile^.dbc^.GetFirst(mPatIdxFile^.dbr) then
    repeat
      HandleEvents(hWindow);
      mPatIdxFile^.dbr^.GetField(2, @tRecNum, 2);
      seekNum:= longInt(tRecNum-1) * longInt(96);
      Seek(mDatafile, seekNum);
      BlockRead(mDataFile, mV18Rec, sizeof(mV18Rec));

      patFile^.dbr^.ClearRecord;

      GetV18Field(mCurrPatID, mV18Rec.pat_id, SizeOf(mV18Rec.pat_id));
      FormatDMSField(mCurrPatID, 12);

      GetV18Field(mCurrPatLN, mV18Rec.pat_lastName, SizeOf(mV18Rec.pat_lastName));
      GetV18Field(mCurrPatFN, mV18Rec.pat_firstName, SizeOf(mV18Rec.pat_firstName));
      DoMappedFlds;

      patFile^.dbr^.PutFieldAsStr(1, mCurrPatID);
      patFile^.dbr^.PutFieldAsStr(2, mCurrPatLN);
      patFile^.dbr^.PutFieldAsStr(3, mCurrPatFN);

      if not patFile^.dbc^.InsertRec(patFile^.dbr) then
        CheckDBError(patFile, mCurrPatID);

    until stopped or not mPatIdxFile^.dbc^.GetNext(mPatIdxFile^.dbr);

  if stopped then
    retVal:= cancelJob;
  MSDisposeObj(mPatIdxFile);
  MSDisposeObj(patFile);

  ConvertPatients:= retVal;
end;

function TCvtDlg.ConvertSpecimens: integer;
var
  retVal        : integer;
  mV18Rec       : TV18DataRec;
  mSpecIdxFile  : PDBFile;
  statFile      : PDBFile;
  tRecNum       : integer;
  seekNum       : longInt;
  mCurrPatID    : array[0..20] of char;
  mCurrSpecID   : array[0..20] of char;
  mCurrWard     : array[0..20] of char;
  mCurrSrc      : array[0..20] of char;
  mCurrFreeText : array[0..180] of char;
  mCurrCollDate : TIntlDate;
  patSeqRef     : TSeqNum;
  srcSeqRef     : TSeqNum;
  wardSeqRef    : TSeqNum;
  statSeqRef    : TSeqNum;
  pSeq          : TSeqNum;
  fSeq          : TSeqNum;
  tstr          : array[0..50] of char;
  tstr2         : array[0..100] of char;

  procedure DoMappedFlds;
  var
    obj   : PMapObj;
    k     : integer;
    pstr  : array[0..100] of char;

    procedure DoDate(aFld: word; aV3Fld: integer);
    var
      d     : TIntlDate;
    begin
      d:= ConvertV18Date(aFld);
      specFile^.dbr^.PutField(aV3Fld, @d);
    end;

    procedure DoTime(aFld: integer; aV3Fld: integer);
    var
      t     : TIntlTime;
    begin
      t:= ConvertV18Time(aFld);
      specFile^.dbr^.PutField(aV3Fld, @t);
    end;

    procedure DoMnem(anID: PChar; tag, v3Fld: integer);
    var
      seq   : TSeqNum;
      fi    : TFldInfo;
      ai    : TAssocInfo;
      pstr  : array[0..10] of char;
    begin
      specFile^.dbd^.FieldInfo(v3Fld, fi);
      if fi.fldAssoc > 0 then
      begin
        specFile^.dbd^.AssocInfo(fi.fldAssoc, ai);
        GetV18Field(pstr, anID, 3);
        seq:= GetMnemSeqRef(pstr, ai.fName, tag);
        specFile^.dbr^.PutField(v3Fld, @seq);
      end;
    end;

  begin
    {- mapped collection indexs 11-16 are used for specimen (hard-coded yeach!) }
    for k:= 11 to 16 do
    begin
      obj:= maps^.At(k);
      if obj^.fld3Num > 0 then    {- field is mapped }
      begin
        case k of
          11:  {- Collect Time }
            DoTime(mV18Rec.spec_collTime, obj^.fld3Num);
          12:  {- Order Date }
            DoDate(mV18Rec.spec_ordDate, obj^.fld3Num);
          13:  {- Order physician }
            DoMnem(mV18Rec.spec_ordPhys, 3, obj^.fld3Num);
          14:  {- Receive Date }
            DoDate(mV18Rec.spec_recvDate, obj^.fld3Num);
          15:  {- Receive Time }
            DoTime(mV18Rec.spec_recvTime, obj^.fld3Num);
          16:  {- Tech ID }
            DoMnem(mV18Rec.spec_techID, 9, obj^.fld3Num);
        end;
      end;
    end;
  end;

begin
  retVal:= continueJob;
  AddNameToList('Specimen Database', listBox);

  statFile:= New(PDBFile, Init('SPECSTAT', mergeDir, dbOpenNormal));
  if statFile = nil then
    CriticalError('Cannot open V3.02 Specimen Status file.');
  statFile^.dbr^.PutFieldAsStr(1, '1');  {- prelim }
  if statFile^.dbc^.GetEQ(statFile^.dbr) then
    pSeq:= statFile^.dbr^.GetSeqValue
  else
    pSeq:= 0;
  statFile^.dbr^.PutFieldAsStr(1, '2');  {- final }
  if statFile^.dbc^.GetEQ(statFile^.dbr) then
    fSeq:= statFile^.dbr^.GetSeqValue
  else
    fSeq:= 0;
  MSDisposeObj(statFile);

  mSpecIdxFile:= New(PDBFile, Init('SPC18IDX', restoreDir, dbOpenNormal));
  if mSpecIdxFile = nil then
    CriticalError('Cannot open the specimen index file.');

  if mSpecIdxFile^.dbc^.GetFirst(mSpecIdxFile^.dbr) then
    repeat
      HandleEvents(hWindow);
      mSpecIdxFile^.dbr^.GetField(2, @tRecNum, 2);
      seekNum:= (longInt(tRecNum-1) * longInt(96));
      Seek(mDatafile, seekNum);
      BlockRead(mDataFile, mV18Rec, sizeof(mV18Rec));

      { Patient SeqRef }
      GetV18Field(mCurrPatID, mV18Rec.spec_patID, SizeOf(mV18Rec.spec_patID));
      FormatDMSField(mCurrPatID, 12);

      patSeqRef:= GetPatientSeqRef(mCurrPatID);
      { Specimen Number }
      GetV18Field(mCurrSpecID, mV18Rec.spec_id, SizeOf(mV18Rec.spec_id));
      { Ward Mnemonic }
      GetV18Field(mCurrWard, mV18Rec.spec_ward, SizeOf(mV18Rec.spec_ward));
      wardSeqRef:= GetMnemSeqRef(mCurrWard, 'WARD', 0);
      { Source Mnemonic }
      GetV18Field(mCurrSrc, mV18Rec.spec_source, SizeOf(mV18Rec.spec_source));
      srcSeqRef:= GetMnemSeqRef(mCurrSrc, 'SOURCE', 1);
      { Collection Date }
      if mV18Rec.spec_CollDate = 0 then
        mCurrCollDate:= collDate
      else
        mCurrCollDate:= ConvertV18Date(mV18Rec.spec_CollDate);
      { Specimen Status }
      if TestBit(mV18Rec.spec_sFlags, 0) then
        statSeqRef:= fSeq
      else
        statSeqRef:= pSeq;
      { Specimen Free Text }
      ConvertV18FreeText(restoreDir, mCurrFreeText, mV18Rec.spec_freeText);

      specFile^.dbr^.ClearRecord;

      { Put data into V3.0 fields  and Insert into database }
      specFile^.dbr^.PutFieldAsStr(DBSPECSpecID, mCurrSpecID);
      specFile^.dbr^.PutField(DBSPECCollectDate, @mCurrCollDate);
      specFile^.dbr^.PutField(DBSPECPatRef, @patSeqRef);
      specFile^.dbr^.PutField(DBSPECSrc, @srcSeqRef);
      specFile^.dbr^.PutField(DBSPECWard, @wardSeqRef);
      specFile^.dbr^.PutField(DBSPECStat, @statSeqRef);
      specFile^.dbr^.PutFieldAsStr(DBSPECFreeText, mCurrFreeText);
      DoMappedFlds;
      if not specFile^.dbc^.InsertRec(specFile^.dbr) then
      begin
        StrCopy(tstr2, mCurrSpecID);
        StrCat(tstr2, ' ');
        StrCat(tstr2, IntlDateStr(mCurrCollDate, tstr, sizeof(tstr)-1));
        CheckDBError(specFile, tstr2);
      end;

    until stopped or not mSpecIdxFile^.dbc^.GetNext(mSpecIdxFile^.dbr);

  if stopped then
    retVal:= cancelJob;
  MSDisposeObj(mSpecIdxFile);
  ConvertSpecimens:= retVal;
end;

function TCvtDlg.GetPatientSeqRef(mCurrPatID: PChar): TSeqNum;
var
  patFile     : PDBFile;
Begin
  patFile:=  New(PDBFile, Init('PATIENT',  mergeDir, dbOpenNormal));
  if patFile = nil then
    CriticalError('Cannot open the patient file.');

  patFile^.dbr^.PutFieldAsStr(1, mCurrPatID);
  if patFile^.dbc^.GetEQ(patFile^.dbr) then
    GetPatientSeqRef:= patFile^.dbr^.GetSeqValue
  else
    GetPatientSeqRef:= 0;

  MSDisposeObj(patFile);
end;

function TCvtDlg.GetMnemSeqRef(mID, fName: PChar; tag: integer): TSeqNum;
var
  mnemFile     : PDBFile;
  aFile        : array[0..100] of char;
  mCustFile    : file;
  mMnemIdxFile : PDBFile;
  tRecNum      : integer;
  seekNum      : longInt;
  mV18CustRec  : TV18CustomRec;
  mMnem        : array[0..20] of char;
  mDesc        : array[0..20] of char;
  errorStr     : array[0..255] of char;
  pstr         : array[0..50] of char;
  b            : boolean;
begin
  mnemFile:=  New(PDBFile, Init(fName,  mergeDir, dbOpenNormal));
  if mnemFile = nil then
  begin
    StrCopy(errorStr, 'Cannot open the mnemonic file: ');
    StrCat(errorStr, mergeDir);
    StrCat(errorStr, fName);
    CriticalError(errorStr);
  end;
  mnemFile^.dbr^.PutFieldAsStr(1, mID);
  if mnemFile^.dbc^.GetEQ(mnemFile^.dbr) then
    GetMnemSeqRef:= mnemFile^.dbr^.GetSeqValue
  else
  begin
    StrCopy(aFile, restoreDir);
    StrCat(aFile, 'CUSTOM.FIL');
    Assign(mCustFile, aFile);
    Reset(mCustFile, 1);
    if IOResult <> 0 then
      CriticalError('Cannot open the V18.10 CUSTOM.FIL.');
    mMnemIdxFile:=  New(PDBFile, Init('MNM18IDX', restoreDir, dbOpenNormal));
    if mMnemIdxFile = nil then
    begin
      Str(dbLastOpenError, errorStr);
      StrCat(errorStr, '-');
      StrCat(errorStr, 'Cannot open the mnemonic file: ');
      StrCat(errorStr, restoreDir);
      StrCat(errorStr, 'MNM18IDX');
      CriticalError(errorStr);
    end;
    mMnemIdxFile^.dbr^.PutField(1, @tag);
    mMnemIdxFile^.dbr^.PutFieldAsStr(2, mID);
    if mMnemIdxFile^.dbc^.GetEQ(mMnemIdxFile^.dbr) then
    begin
      mMnemIdxFile^.dbr^.GetField(3, @tRecNum, 2);
      seekNum:= longInt(tRecNum-1) * longInt(32);
      Seek(mCustfile, seekNum);
      BlockRead(mCustFile, mV18CustRec, sizeof(mV18CustRec));

      GetV18Field(mDesc, mV18CustRec.description, SizeOf(mV18CustRec.description));

      mnemFile^.dbr^.ClearRecord;
      mnemFile^.dbr^.PutFieldAsStr(DBGSMNEMID, mID);
      mnemFile^.dbr^.PutFieldAsStr(DBGSMNEMDesc, mDesc);
      if StrIComp(fName, 'SOURCE') = 0 then
      begin
        b:= (mV18CustRec.flag = 1);
        mnemFile^.dbr^.PutField(DBSRCUrine, @b);
      end;

      if not mnemFile^.dbc^.InsertRec(mnemFile^.dbr) then
      begin
        StrCopy(pstr, 'File: ');
        StrCat(pstr, fName);
        StrCat(pstr, '  ID: ');
        StrCat(pstr, mID);
        CheckDBError(mnemFile, pstr)
      end
      else
        GetMnemSeqRef:= mnemFile^.dbr^.GetSeqValue;
    end
    else
      GetMnemSeqRef:= 0;
    Close(mCustFile);
    MSDisposeObj(mMnemIdxFile);
  end;
  MSDisposeObj(mnemFile);
end;

function TCvtDlg.ConvertResults: integer;
var
  retVal        : integer;
  retBool       : boolean;
  mV18Rec       : TV18DataRec;
  mV18AddDrgRec : TV18DataRec;
  mV18InterpRec : TV18DataRec;
  mResIdxFile   : PDBFile;
  resFile       : PDBFile;
  isoFile       : PDBFile;
  tRecNum       : integer;
  seekNum       : longInt;
  intrepRecNum  : longInt;
  addDrgrecNum  : longInt;
  convertType   : integer;
  panelNum      : byte;
  { Isolate data }
  isoID         : array[0..20] of char;
  iSpecSeqRef   : TSeqNum;
  iSet          : byte;
  iOrg          : integer;
  iTestDate     : TIntlDate;
  iOrder        : TSeqNum;
  iFTRec        : TRecPtr;
  iTnsFlg       : boolean;
  iSpecID       : array[0..20] of char;
  iSpecCDate    : TIntlDate;
  drugs         : PDrugList;
  isoRef        : TSeqNum;
  { additional drug collection }
  drugColl      : PTestColl;
  aDrugObj      : PTestResObj;
  colIndx       : integer;
  drugdups     : boolean;
  { Dup drug collection }
  dupOrdColl    : PPnlDrgColl;
  aDupObj       : PPnlXref;
  dupIndx       : integer;
  ci            : boolean;
  tstr          : array[0..50] of char;
  v3Result      : PV3Results;

  procedure GetV18Res(tgUD: boolean; testID, testCatID, aResStr: PChar; var contraI: boolean);
  var
    indx          : integer;
    foundRes      : boolean;
    isUrine       : boolean;
    pstr          : array[0..10] of char;
  begin
    StrCopy(aResStr, ''); {- clear result first }
    isUrine:= (mV18Rec.res_rFlags and (1 shl $D)) <> 0;
    if tgUD then
    begin
      indx:= 0;
      foundRes:= false;
      while ((indx < mV18AddDrgRec.drg_cnt) and (not foundRes)) do
      begin
        GetV18Field(pstr, mV18AddDrgRec.drg_drgType[indx].drug, 3);
        if (StrIComp(pstr, testID) = 0) then
        begin
          if StrComp(testCatId, 'MIC') = 0 then
            ByteToMIC(aResStr, mV18AddDrgRec.drg_drgType[indx].mic)
          else
            ByteToNCCLS(aResStr, mV18AddDrgRec.drg_drgType[indx].susc, tgUD, isUrine, contraI);
          foundRes:= true;
        end;
        Inc(indx);
      end;
    end
    else
    begin
      indx:= 1;
      foundRes:= false;
      while ((indx <= panels[panelNum].numTests) and (not foundRes)) do
      begin
        if (StrComp(panels[panelNum].testID[indx], testID) = 0) then
        begin
          if StrComp(testCatId, 'MIC') = 0 then
            ByteToMIC(aResStr, mV18Rec.res_MicLevels[indx-1])
          else
            ByteToNCCLS(aResStr, mV18InterpRec.int_Interp[82-indx], tgUD, isUrine, contraI);
          foundRes:= true;
        end;
        inc(indx);
      end;
    end;
  end;

  procedure BitTests(aRes: PChar; aFlags, aShift: integer);
  begin
    aFlags:= (aFlags shr aShift) and 3; {- put relevant bits in 0 and 1 }
    if aflags and 2 <> 0 then
    begin
      if aflags and 1 <> 0 then
        StrCopy(aRes, '+')
      else
        StrCopy(aRes, '-');
    end
    else
      StrCopy(aRes, '');
  end;

  procedure CreateResults(isoRef: TSeqNum);
  var
    indx     : integer;
    anItem   : PResRecObj;
    resStr   : array[0..25] of char;
  begin
    if isoFile^.dbc^.GetSeq(isoFile^.dbr, isoRef) then
    begin
      if v3Result^.LoadResults(isoFile^.dbr) then
      begin
        for indx:= 0 to v3Result^.Count-1 do
        begin
          anItem:= v3Result^.At(indx);
          {get version 18 data}
          if (StrComp(anItem^.testId,'.BIO') = 0) then
            UnpkBio(mV18Rec.res_biotype, resStr)
          else if (StrComp(anItem^.testId,'.BL') = 0) then
          begin
            BitTests(resStr, mV18Rec.res_xtraTests, BL);
          end
          else if (StrComp(anItem^.testId,'.OXI') = 0) then
          begin
            BitTests(resStr, mV18Rec.res_xtraTests, OXI);
          end
          else if (StrComp(anItem^.testId,'.StS') = 0) then
          begin
            BitTests(resStr, mV18Rec.res_xtraTests, STS);
          end
          else if (StrComp(anItem^.testId,'.GmS') = 0) then
          begin
            BitTests(resStr, mV18Rec.res_xtraTests, GMS);
          end
          else if (StrComp(anItem^.testId,'.TFG') = 0) then
          begin
            BitTests(resStr, mV18Rec.res_xtraTests, TFG);
          end
          else
          begin
            GetV18Res(anItem^.tgUD, anItem^.testID, anItem^.testCatID, resStr, ci);
            if strcomp(anItem^.testCatID, 'NCCLS') = 0 then
            begin
              v3Result^.SetFlag(anItem^.testID, anItem^.testCatID, REScontraIndicated, ci);
            end;
          end;
          v3Result^.SetResult(anItem^.testID, anItem^.testCatID, resStr, true);
        end;
        if not v3Result^.SaveResults then
        begin
          CriticalError('Cannot save results.');
        end;
      end;
    end;
  end;

begin
  retVal:= continueJob;

  if MaxAvail < SizeOf(drugList) then
    CriticalError('Cannot allocate enough memory convert terminated.');
  GetMem(drugs, SizeOf(drugList));

  AddNameToList('Isolate/Results Database', listBox);

  drugColl:= New(PTestColl, Init(100,50));
  dupOrdColl:= New(PPnlDrgColl, Init(100,50));

  mResIdxFile:=  New(PDBFile, Init('RES18IDX', restoreDir, dbOpenNormal));
  if mResIdxFile = nil then
    CriticalError('Cannot open the Results index file.');

  isoFile:=  New(PDBFile, Init('ISOLATE',  mergeDir, dbOpenNormal));
  if isoFile = nil then
    CriticalError('Cannot open V3.02 ISOLATE file.');

  v3Result:= New(PV3Results, Init);
  if v3Result = nil then
    CriticalError('Cannot create results object.');

  if mResIdxFile^.dbc^.GetFirst(mResIdxFile^.dbr) then
    repeat
      HandleEvents(hWindow);
      { Get the V18 results Record }
      mResIdxFile^.dbr^.GetField(2, @tRecNum, 2);
      seekNum:= longInt(tRecNum-1) * longInt(96);
      Seek(mDatafile, seekNum);
      BlockRead(mDataFile, mV18Rec, sizeof(mV18Rec));

      { Get V3.0 Specimen SeqRef }
      GetV18Field(iSpecID, mV18Rec.res_specID, SizeOf(mV18Rec.res_specID));
      GetV18Field(isoID, mV18Rec.res_isoNum, SizeOf(mV18Rec.res_isoNum));
      FormatDMSField(isoID, 3);

      GetSpecimenData(iSpecID, iSpecSeqRef, iSpecCDate);
      isoFile^.dbr^.ClearRecord;
      isoFile^.dbr^.PutFieldAsStr(1, isoID);
      isoFile^.dbr^.PutField(2, @iSpecSeqRef);
      if not isoFile^.dbc^.GetEQ(isoFile^.dbr) then
      begin
        {- clear data structures }
        FillChar(mv18InterpRec, sizeof(mV18InterpRec), 0);
        FillChar(mv18InterpRec.int_interp, sizeof(mv18InterpRec.int_interp), $66);
        FillChar(mV18AddDrgRec, sizeof(mV18AddDrgRec), 0);

        { Gather V 18 isolate and Results data }
        panelNum:= mV18Rec.res_trayType;
        iOrg:= mV18Rec.res_organism;
        iTestDate:= ConvertV18Date(mV18Rec.res_testDate);
        iSet:= 0;
        iOrder:= 0;
        iFTRec:= mV18Rec.res_freeText;
        (* Transmit flag *)
        iTnsFlg:= false;
        if TestBit(mV18Rec.res_sysFlag, 2) then
          iTnsFlg:= true;
        { Get Additional drugs }
        addDrgRecNum:= 0;
        move(mV18Rec.res_addDrugs, addDrgRecNum, 3);
        if addDrgRecNum <> 0 then
        begin
          seekNum:= (addDrgRecNum-1) * longInt(96);
          Seek(mDatafile, seekNum);
          BlockRead(mDatafile, mV18AddDrgRec, sizeof(mV18AddDrgRec));
          GetDrugNames(restoreDir, mV18AddDrgRec, drugs);
          colIndx:= drugColl^.CreateEntry(panelNum, drugs, drugdups);
          aDrugObj:= drugColl^.At(colIndx);
        end;
        { Get Interpretations }
        intrepRecNum:= 0;
        move(mV18Rec.res_interp, intrepRecNum, 3);
        if intrepRecNum <> 0 then
        begin
          seekNum:= (intrepRecNum-1) * longInt(96);
          Seek(mDatafile, seekNum);
          BlockRead(mDatafile, mV18InterpRec, sizeof(mV18InterpRec));
        end;
        convertType:= GetConversionType(panelNum, addDrgRecNum);
        HandleEvents(hWindow);
        case ConvertType of
          PanelOnly:
          if not stopped then
          begin
            if panels[panelNum].orderRef = 0 then
              retBool:= PromptForPanelOrder(@Self, @panels[panelNum]);
            HandleEvents(hWindow);
            if retBool and not stopped then
            begin
              isoRef:= Convert_Isolate(isoFile, restoreDir, mergeDir, isoID, iSpecSeqRef,
                              panels[panelNum].tstgrpSet, iOrg, iTestDate,
                              panels[panelNum].orderRef, iFTRec,
                              iTnsFlg, iSpecID, iSpecCDate, listBox);
              CreateResults(isoRef);
            end
            else
              retVal:= cancelJob;
          end;
          PanelANDDrugs:
          if not stopped then
          begin
            if drugdups then
            begin
              retBool:= PromptForDupsOrder(@Self, @panels[panelNum], aDrugObj, iSpecSeqRef);
              HandleEvents(hWindow);
              if retBool and not stopped then
              begin
                isoRef:= Convert_Isolate(isoFile, restoreDir, mergeDir,
                                          panels[panelNum].isolateID,
                                          iSpecSeqRef,panels[panelNum].tstgrpSet,
                                          iOrg, iTestDate,
                                          panels[panelNum].orderRef, iFTRec,
                                          iTnsFlg, iSpecID, iSpecCDate, listBox);
                CreateResults(isoRef);
                isoRef:= Convert_Isolate(isoFile, restoreDir, mergeDir, aDrugObj^.iso,
                                          iSpecSeqRef, iSet, iOrg, iTestDate,
                                          aDrugObj^.orderRef, iFTRec,
                                          iTnsFlg, iSpecID, iSpecCDate, listBox);
                CreateResults(isoRef);
              end
              else
                retVal:= cancelJob;
            end
            else
            begin
              dupIndx:= dupOrdColl^.AddXref(panelNum, colIndx);
              aDupObj:= dupOrdColl^.At(dupIndx);
              if aDupObj^.ordSeq = 0 then
                retBool:= PromptForNoDupsOrder(@Self, @panels[panelNum], aDrugObj, aDupObj);
              HandleEvents(hWindow);
              if retBool and not stopped then
              begin
                isoRef:= Convert_Isolate(isoFile, restoreDir, mergeDir, isoID, iSpecSeqRef,
                                panels[panelNum].tstgrpSet, iOrg, iTestDate, aDupObj^.ordSeq, iFTRec,
                                iTnsFlg, iSpecID, iSpecCDate, listBox);
                CreateResults(isoRef);
              end
              else
                retVal:= cancelJob;
            end;
          end;
          AddDrugsOnly:
          if not stopped then
          begin
            if ((aDrugObj^.tstgrpRef = 0) or (aDrugObj^.orderRef = 0)) then
              retBool:= promptForAddDrugOrder(@Self, aDrugObj);
            HandleEvents(hWindow);
            if retBool and not stopped then
            begin
              isoRef:= Convert_Isolate(isoFile, restoreDir, mergeDir, isoID, iSpecSeqRef,
                              iSet, iOrg, iTestDate, aDrugObj^.orderRef, iFTRec,
                              iTnsFlg, iSpecID, iSpecCDate, listBox);
              CreateResults(isoRef);
            end
            else
              retVal:= cancelJob;
          end;
          NoResults:
            if not stopped then
              Convert_Isolate(isoFile, restoreDir, mergeDir, isoID, iSpecSeqRef, iSet, iOrg, iTestDate,
                              iOrder, iFTRec, iTnsFlg, iSpecID, iSpecCDate, listBox);
        end;
      end
      else
      begin
        {- log duplicate isolate number }
        LogStr('Isolate already exists: ');
        LogStr('Spec: '); LogStr(iSpecID);
        LogStr(' ');
        IntlDateStr(iSpecCDate, tstr, sizeof(tstr)-1);
        LogStr(tstr);
        LogStr(' Isolate: ');
        LogStr(isoID);
        LogStr(' - Isolate not converted.');
        LogLn;
      end;
    until ((not mResIdxFile^.dbc^.GetNext(mResIdxFile^.dbr)) or (not retBool)) or
          (retVal = cancelJob) or stopped;

  if stopped then
    retVal:= cancelJob;
  MSDisposeObj(drugColl);
  MSDisposeObj(dupOrdColl);
  MSDisposeObj(mResIdxFile);
  MSDisposeObj(isoFile);
  MSDisposeObj(v3Result);

  ConvertResults:= retVal;
end;

procedure TCvtDlg.GetSpecimenData(specID: PChar; var specRef: TSeqNum; var aCollDate: TIntlDate);
var
  mV18Rec       : TV18DataRec;
  mSpecIdxFile  : PDBFile;
  tRecNum       : integer;
  seekNum       : longInt;
  aCDate        : TIntlDate;
begin
  mSpecIdxFile:= New(PDBFile, Init('SPC18IDX', restoreDir, dbOpenNormal));
  if mSpecIdxFile = nil then
    CriticalError('Cannot open the specimen index file.');
  mSpecIdxFile^.dbr^.PutFieldAsStr(1, specID);
  if mSpecIdxFile^.dbc^.GetEQ(mSpecIdxFile^.dbr) then
  begin
    mSpecIdxFile^.dbr^.GetField(2, @tRecNum, 2);
    seekNum:= (longInt(tRecNum-1) * longInt(96));
    Seek(mDatafile, seekNum);
    BlockRead(mDataFile, mV18Rec, sizeof(mV18Rec));
    { Collection Date }
    if mV18Rec.spec_CollDate = 0 then
      aCDate:= collDate
    else
      aCDate:= ConvertV18Date(mV18Rec.spec_CollDate);
    specFile^.dbr^.PutFieldAsStr(DBSPECSpecID, specID);
    specFile^.dbr^.PutField(DBSPECCollectDate, @aCDate);
    if specFile^.dbc^.GetEQ(specFile^.dbr) then
    begin
      specRef:= specFile^.dbr^.GetSeqValue;
      specFile^.dbr^.GetField(DBSPECCollectDate, @aCollDate, 4);
    end
    else
    begin
      specRef:= 0;
      aCollDate:= 0;
    end;
  end
  else
  begin
    specRef:= 0;
    aCollDate:= 0;
  end;
  MSDisposeObj(mSpecIdxFile);
end;

procedure Cvt18ConvertData(aParent: PWindowsObject; aMaps: PMapCollection;
                           aCollDate: TIntlDate);
begin
  aMaps^.LogUnmappedFields;
  aMaps^.LogMappedFields;
  application^.ExecDialog(New(PCvtDlg, Init(aParent, aMaps, aCollDate)));
end;

END.

