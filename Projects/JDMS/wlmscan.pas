UNIT WLMSCAN;

INTERFACE

Uses
   WLDMStrg,
   WLWaitLb,
   WObjects,
   WinProcs,
   WinTypes;

TYPE
    PCharString = Array[0..256] of Char;

   SessionTypes = (sessPat, sessSpc, sessBat, sessRes);

   PWaitCursor = ^TWaitCursor;
   TWaitCursor = object(TObject)
     oldCursor  : HCursor;
      {}
     constructor Init;
     destructor Done; virtual;
   End;

CONST
   LinesPerPage = 64;
   firstSessType = SessPat;
   lastSessType = SessRes;
   sessPrefix   : Array[SessionTypes] of String[2] =
                      ('SP', 'SS', 'SB', 'SR');
   sessFiles    : Array[SessionTypes] of String[8] =
                      ('PATIENT', 'SPECIMEN', 'BATORD', 'RESULT');

   {- Profile Constants for DMS.INI }

   profFile       = 'DMS.INI';
   appName        = 'DMS';    {- application name for WriteProfileString }

   sessKey        = 'CurrentSession';        {- current session key }

   WAExists       = 'WAExists';              {- W/A exists key }

  TwoWayStop    = 0;
  TwoWayStart   = 1;
   TwoWayInquire = 2;

{=======================================================================}
{       DMS System Constants from DMS 18.20                             }
{=======================================================================}
{**********  Miscellaneous constants  **********}
    { controls }
    MaxInputNameLen = 32;  { Maximum length for a generic label for a field }
    MaxInputPixLen  = 100; { Maximun pixel length for a generic input field }
    MaxDateLen      = 8;   { Maximum length for a date field }
    MaxTimeLen      = 8;   { Maximum length for a time field }
    MaxResStrLen    = 64;  { Maximum length of a resource string }
    MaxMiscStrLen   = 64;
    TestIDDrugLen   = 5;   { Used by BuildTestID }

    MaxSessionNameLen = 8;

{********** Miscellaneous WM_ based commands  **********}
    wm_SelectBat     = 373;
    wm_fldChanged    = 375;
    wm_ChangeTitle   = 376;
    wm_printBarcode  = 377;
      wm_FillInWAStatus = 378;
      wm_TwoWay = 379;

{ RESERVED ids for data input fields (100 - 199)}

    id_collDate = 102;  { use this id for collect date input or label fields }
    id_WADate   = 104;  { use this id for WA date input or label fields }
    id_WATime   = 112;  { use this id for WA time input or label fields }
    id_patId     = 120;  { use this id for patient id input or label fields }
    id_patFName  = 121;  { use this id for patient first name input or label fields }
    id_patLName  = 122;  { use this id for patient last name input or label fields }
    id_spcId     = 123;  { use this id for specimen id input or label fields }
    id_source    = 124;  { use this id for source input or label fields }
    id_batId     = 126;  { use this id for battery id input or label fields }
    id_isoId     = 127;  { use this id for isolate id input or label fields }
    id_biotype   = 131;  { use this id for biotype input or label fields }
    id_testId    = 133;
      id_result    = 134;
      id_Ward      = 135;

    id_AddCriteria = 170;  { id used to add criteria to a session }
    id_NotTransmit = 171;  { id used in selection search criteria for a
                             session when MFI is installed }
    id_PrintOnly   = 172;  { id used to select the 'print only' search
                             criteria }
    id_finalized   = 173;  { id used to select the 'finalized' search
                             criteria }
    id_Prelim      = 174;  { id used to select the 'preliminary' search
                             criteria }
    id_Summary     = 175;  { id for the field the sumarizes the search/
                             sort criteria for user verification }
    id_miscText    = 176;  { id  used for miscellaneous text in a window }
    id_dateTimer   = 177;  { id for the current date time message }
    id_listText    = 178;  { id for labels for DEE window list boxes }
    id_list        = 179;
    id_inputLabel  = 180;  { id for the one line input boxes in DEE Windows }
    id_therguide   = 181;  { patient therapy guide field }
    id_final       = 182;  { specimen final flag }
    id_xmit        = 183;  { battery mfi transmit flag }
    id_wapid       = 184;  { battery W/A panel id }
    id_testproc    = 185;  { test procedure id }
    id_testname    = 186;  { test name }

    id_Save   = 200;  { save the customize data (may be used in ANY dialog box
                       that has a save button)}
    id_Delete = 201;  { save the customize data (may be used in ANY dialog box
                        that has a delete button)}
    id_Print  = 202;  { save the customize data (may be used in ANY dialog box
                        that has a print button)}

    id_SourceID   = 300;
      id_PatientID  = 301;
    id_WardID         = 302;
    id_LastName   = 303;
      id_FirstName  = 304;

    { DMSSessionDlg Object Input Field Ids (310 - 319) }

    id_DlgSession  = 310;  { id for the session name control }
    id_DlgMarkedCB = 311;  { id for the 'marked items' check box }

    { RESERVED ids for the data entry and edit windows (400 - 499)}

    id_DEEList = 400;  { id for all DEE list boxes }
    id_DeeMsg = 401;
    id_ResultLB=402;   { id for the result list in the battery zoom window }
    id_Clear = 403;
    id_remove = 404;
    id_ZoomSpc = 405;
    id_zoomPat = 406;
    id_zoomBat = 407;
    id_ReadAllPanel = 408;
    id_WAOptions = 409;
    id_sourceDesc = 410;
    id_batteryDesc = 411;
    id_orgDesc     = 412;


 { RESERVED ids for the print windows (500 - 529)}

    id_longPat = 500;
    id_lprSum  = 501;
    id_lprRepr = 502;
    id_cpsr    = 503;
    id_antibio = 504;
    id_micrep  = 505;
    id_wardrep = 506;
    id_srcRep  = 507;
    id_testLog = 508;
    id_barcodes = 509;
    id_PrintAllBarcodes = 510;

 { RESERVED ids for the WA windows (530 - 599)}
    id_WAStat           = 530;
    id_WAErrs           = 531;
    id_1Minute          = 532;
    id_5Minute          = 533;
    id_15Minute         = 534;
    id_30Minute         = 535;
    id_doorOpen         = 536;
    id_panelId          = 537;
    id_wanum            = 538;
    id_WAInquiry        = 539;
    id_lastRead         = 540;
    id_alphaNap         = 541;
    id_PotHydro         = 542;
    id_SulfAcid         = 543;
    id_Peptidase        = 544;
    id_Dimethyl         = 545;
    id_FerricChlor      = 546;
    id_Kovacs           = 547;
    id_HNIDIndole       = 548;
    id_RapidIndole      = 549;
    id_WAProcessorSys   = 550;
    id_WAMeasureSys     = 551;
    id_WAMotionSys      = 552;
    id_WACommSys        = 553;
    id_WAFlourSys       = 554;
    id_WAFlags          = 555;
    id_WATower          = 556;
    id_WASlot           = 557;
    id_PrintBarCode     = 558;
    id_PrintTestBC      = 559;
    id_LSMap            = 560;
    id_IDNextPanel      = 570;
    id_SetAutoPurge     = 571;
    id_UserExcepts   = 572;
    id_reagents         = 573;
    id_steril           = 574;
    id_WAClock          = 575;
    id_WAClean          = 576;
    id_WAShield         = 577;
    id_WAMaintStat      = 578;
    id_WASterilStat     = 579;
    id_AccessWA         = 580;
    id_WAMaint      = 582;
    id_IDPanel      = 584;
    id_AbortPanel   = 585;
    id_WAQC      = 586;
    id_PanelDump   = 587;
    id_GPIBTimer   = 588;
    id_JamTimer         = 589;
    id_printUnread      = 590;
    id_ClearComm   = 591;   { bc 1/21/92 }
    id_SodiumHydro      = 592;
    id_NextRead         = 593;
    id_PanelInfo   = 594;
    id_WAState      = 595;
    id_AutoUpload       = 596;

 { RESERVED ids for the Custom Report windows (600 - 699)}

 { RESERVED ids for the Organism Identification windows (700 - 724)}

 { id_XXXXX for TrayEdit dialog }

 id_as4Well=800;   { Uses 800 - 895 }
 id_as4Label=900;   { Uses 900 - 995 }

{**********  ERR_ error codes  **********}
    err_DEE       =    0;
    err_DBIF      =  100;
    err_DMSEDOBJ  =  200;
    err_Btrieve   =  300;
    err_AS4       =  400;
    err_TrayLib   =  600;
    err_PDL       =  800;
    err_InterpLib = 1000;
    err_OrgGroup  = 1200;
    err_Session   = 1400;
    err_GPIB      = 1600;
    err_Biotype   = 1800;
    err_IDLIB     = 2000;
    err_resLib    = 2400;
    err_UserBat   = 2600;
    err_WA        = $8000;

VAR
  {- filled in at init }
  sessDeflt    : PChar; {- default session name }
  sessNone     : PChar; {- no session}
  screenHeight : Word;
  screenWidth  : Word;

FUNCTION BuildTestID(DrugAbbrev, Category: STRING): STRING;
FUNCTION TSTBIT(Num, Bit: LONGINT): BOOLEAN;
PROCEDURE SETBIT(Num: POINTER; Bit: LONGINT);
PROCEDURE CLRBIT(Num: POINTER; Bit: LONGINT);
FUNCTION TestBit(Num: POINTER;  Bit: LONGINT): BOOLEAN;
PROCEDURE StickBit(Num: POINTER; Bit: LONGINT);
PROCEDURE ClearBit(Num: POINTER; Bit: LONGINT);
Function CtrlPressed: Boolean;
Function ShiftPressed: Boolean;
PROCEDURE NCTherapy(InterpCode: INTEGER; VAR InterpText: STRING);
PROCEDURE MSDelay(msCount: INTEGER);

IMPLEMENTATION

TYPE
  Memory=ARRAY[0..32767] OF BYTE;
CONST
  BitMask: ARRAY[0..15] OF WORD = (1, 2, 4, 8, 16, 32, 64, 128,
                256, 512, 1024, 2048, 4096, 8192, 16384, 32768);

FUNCTION BuildTestID(DrugAbbrev, Category: STRING): STRING;
{========================================================================}
{ Given a drug abbrev from physical tray db, creates a DBIF test ID.     }
{========================================================================}
VAR tmpstr: STRING;
BEGIN
tmpstr := DrugAbbrev;
REPEAT tmpstr := tmpstr + ' ' UNTIL LENGTH(tmpstr) = TestIDDrugLen;
BuildTestID := tmpstr + Category;
END;

FUNCTION TSTBIT(Num, Bit: LONGINT): BOOLEAN;
BEGIN
TSTBIT := (Num AND BitMask[Bit]) <> 0;
END;

PROCEDURE SETBIT(Num: POINTER; Bit: LONGINT);
BEGIN
INTEGER(Num^) := INTEGER(Num^) OR BitMask[Bit];
END;

PROCEDURE CLRBIT(Num: POINTER; Bit: LONGINT);
BEGIN
INTEGER(Num^) := INTEGER(Num^) AND ($FFFF-BitMask[Bit]);
END;

FUNCTION TestBit(Num: POINTER;  Bit: LONGINT): BOOLEAN;
BEGIN
TestBit := (Memory(Num^)[Bit DIV 8] AND BitMask[Bit MOD 8]) <> 0;
END;

PROCEDURE StickBit(Num: POINTER; Bit: LONGINT);
BEGIN
Memory(Num^)[Bit DIV 8] := Memory(Num^)[Bit DIV 8] OR BitMask[Bit MOD 8];
END;

PROCEDURE ClearBit(Num: POINTER; Bit: LONGINT);
BEGIN
Memory(Num^)[Bit DIV 8] := Memory(Num^)[Bit DIV 8] AND ($FFFF-BitMask[Bit MOD 8]);
END;


{- used to detect whether or not a shift key is pressed}
Var
   keys : TKeyBoardState;

Function CtrlPressed: Boolean;
Begin
   GetKeyboardState(keys);
   CtrlPressed:= (Byte(keys[vk_Control]) and $80) > 0;
End;

Function ShiftPressed: Boolean;
Begin
   GetKeyboardState(keys);
   ShiftPressed:= (Byte(keys[vk_Shift]) and $80) > 0;
End;

Constructor TWaitCursor.Init;
Begin
   TObject.Init;
   oldCursor:= SetCursor(LoadCursor(0, idc_Wait));
   If GetSystemMetrics(sm_MousePresent) = 0 Then
      SetCursorPos(ScreenWidth Div 2, ScreenHeight Div 2);
   ShowCursor(True);
End;

Destructor TWaitCursor.Done;
Begin
   ShowCursor(False);
   SetCursor(oldCursor);
   TObject.Done;
End;

Procedure NCTherapy(InterpCode: Integer; Var InterpText: String);
Begin
   Case InterpCode Of
        0   :   InterpText:= 'N/A';
        1   :   InterpText:= 'TFG';
        2   :   InterpText:= 'MS';
        3   :   InterpText:= 'S';
        4   :   InterpText:= 'R';
        5   :   InterpText:= 'N/R';
        6   :   InterpText:= '';
        7   :   InterpText:= 'BLac';
        8   :   InterpText:= 'I';
        9   :   InterpText:= '';
       10   :   InterpText:= '';
       11   :   InterpText:= '';
       12   :   InterpText:= '';
       13   :   InterpText:= 'IB';
       14   :   InterpText:= '';
       15   :   InterpText:= '';
        Else   InterpText:= '****';   { Impossible! }
   End; {CASE}
End;

Procedure InitDefaultSessNames;
Begin
   GetMem(sessNone, 9);
   GetMem(sessDeflt, 9);
   GetStr(6125 {was 5201}, sessNone, 8);    {No Session Defined}
   GetStr(6126 {was 5202}, sessDeflt, 8);   {Default Session}
End;


PROCEDURE MSDelay(msCount: INTEGER);
BEGIN
   MSWait(msCount);       {- call wait in wait lib }
END;



BEGIN
   ScreenWidth:= GetSystemMetrics(sm_CXScreen);
   ScreenHeight:= GetSystemMetrics(sm_CYScreen);
   InitDefaultSessNames;
END.
