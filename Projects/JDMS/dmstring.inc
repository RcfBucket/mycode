(****************************************************************************


dmstring.inc

produced by Borland Resource Workshop


*****************************************************************************)

const
  IDS_ERROR                 =   1;
  IDS_CONFIRM               =   2;
  IDS_NO                    =   3;
  IDS_YES                   =   4;
  IDS_PROGHALT              =   5;
  IDS_PERCENT_S             =   6;
  IDS_FATALERROR            =   7;
  IDS_WARNING               =   8;
  IDS_NEW                   =   9;
  IDS_EDIT                  =  10;
  IDS_BIOTYPE               =  11;
  IDS_BETA_LACTAMASE        =  12;
  IDS_GENTAMICIN_SYNERGY    =  13;
  IDS_BETA_HEMOLYSIS        =  14;
  IDS_INDOLE                =  15;
  IDS_OXIDASE               =  16;
  IDS_STREPTOMYCIN_SYNERGY  =  17;
  IDS_THYMIDINE_FREE_GROWTH =  18;
  IDS_NORMAL_FLORA          =  19;
  IDS_NO_GROWTH             =  20;
  IDS_PRELIM_STATUS         =  21;
  IDS_FINAL_STATUS          =  22;
  IDS_INFORMATION           =  23;
  IDS_SESSIONSDISABLED      =  24;
  IDS_GRAMNEGATIVE          =  25;
  IDS_GRAMPOSITIVE          =  26;
  IDS_ANAEROBE              =  27;
  IDS_HNID                  =  28;
  IDS_YEAST                 =  29;
  IDS_UNKNOWN               =  30;
  IDS_FERMENTER             =  31;
  IDS_NONFERMENTER          =  32;
  IDS_STREP                 =  33;
  IDS_MICROLIST             =  34;
  IDS_COCCI                 =  35;
  IDS_CLOSTRIDIA            =  36;
  IDS_GRAMNEGBACILLI        =  37;
  IDS_GRAMPOSBACILLI        =  38;
  IDS_ALL                   =  39;
  IDS_PAGEPOFN              =  40;
  IDS_PRINTEDON             =  41;
  IDS_SAVE_DATA             =  42;
  IDS_OLD_BTRV_VERSION      =  43;
  IDS_PAGE                  =  44;
  IDS_CODE                  =  45;
  IDS_DESC                  =  46;
  IDS_FT_STR                =  47;
  IDS_FT_MNEM               =  48;
  IDS_FT_DATE               =  49;
  IDS_FT_TIME               =  50;
  IDS_FT_INT                =  51;
  IDS_FT_NUM                =  52;
  IDS_FT_INTERNAL           =  53;
  IDS_INVALID               =  54;
  IDS_ESBL                  =  55;

