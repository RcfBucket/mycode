unit Ctl3D;

{****************************************************************************}
{                                                                            }
{ Unit:    CTL3D                                                             }
{ Source:  Borland Pascal 7.0                                                }
{ Author:  Steve Hamer-Moss, CoCo Systems Ltd.                               }
{ Date:    January 1994                                                      }
{ Version: 2.04c                                                             }
{                                                                            }
{ Purpose:                                                                   }
{   BP7 interface unit for Microsoft's CTL3D dynamic link library. Allows    }
{   BP7 applications to use Microsoft-standard 3D dialog boxes and controls, }
{   even in parallel with BWCC(e.g., for use with common dialogs).           }
{                                                                            }
{                                                                            }
{ Revision history:                                                          }
{                                                                            }
{ Feb. 1993, v0.9: Initial release                                           }
{ July/Aug 1993, v2.01: Updated to work with v2.01 of Microsoft's CTL3D.DLL  }
{ Sep. 1993, v2.04: Updated to work with v2.04 of Microsoft's CTL3D.DLL, and }
{ and with CTL3DV2.DLL.                                                      }
{ Sep. 1993, v2.04: Updated to support 3D controls outside of dialog boxes,  }
{ and to support modeless dialogs                                            }
{ Jan. 1994, v2.04: Fixed bugs with TDlgWindow support                       }
{ Jan. 1994, v2.04c: Re-coded to add support for TPW 1.5, and corrected bug  }
{                    that prevented dynamic unloading/reloading of CTL3D     }
{ Dec 1994, reformatted (rcf)
{                                                                            }
{****************************************************************************}

{$C MOVEABLE PRELOAD DISCARDABLE}

INTERFACE

uses
  CommDlg,
  Strings,
{$IFDEF VER15}
  Wobjects,
{$ELSE}
  ODialogs,
  OWindows,
{$ENDIF}
  WinDOS,
  WinProcs,
  WinTypes;

const
  WM_DLGBORDER   = WM_USER + 3567;
  WM_DLGSUBCLASS = WM_USER + 3568;

type
  {========== Replacement 3D Window, Dialog and Control objects ==========}

  T3dApplication = object(TApplication)            {Main Application object}
    destructor Done; virtual;
  end;

  T3dDialog = object(TDialog)              {Main dialog object}
    procedure WMCtlColor(var Msg: TMessage); virtual WM_FIRST + WM_CTLCOLOR;
    procedure WMDlgBorder(var Msg: TMessage); virtual WM_FIRST + WM_DLGBORDER;
    procedure WMInitDialog(var Msg: TMessage); virtual WM_FIRST + WM_INITDIALOG;
    procedure WMNCActivate(var Msg: TMessage); virtual WM_FIRST + WM_NCACTIVATE;
    procedure WMNCPaint(var Msg: TMessage); virtual WM_FIRST + WM_NCPAINT;
    procedure WMSetText(var Msg: TMessage); virtual WM_FIRST + WM_SETTEXT;
  end;

  T3dDlgWindow = object(TDlgWindow)            {Dialog window object}
    procedure WMCtlColor(var Msg: TMessage); virtual WM_FIRST + WM_CTLCOLOR;
    procedure WMDlgBorder(var Msg: TMessage); virtual WM_FIRST + WM_DLGBORDER;
    procedure WMInitDialog(var Msg: TMessage); virtual WM_FIRST + WM_INITDIALOG;
    procedure WMNCActivate(var Msg: TMessage); virtual WM_FIRST + WM_NCACTIVATE;
    procedure WMNCPaint(var Msg: TMessage); virtual WM_FIRST + WM_NCPAINT;
    procedure WMSetText(var Msg: TMessage); virtual WM_FIRST + WM_SETTEXT;
    procedure WMSysColorChange(var Msg: TMessage); virtual WM_FIRST + WM_SYSCOLORCHANGE;
  end;

  T3dMDIWindow = object(TMDIWindow)            {MDI window object}
    procedure WMCtlColor(var Msg: TMessage); virtual WM_FIRST + WM_CTLCOLOR;
    procedure WMSysColorChange(var Msg: TMessage); virtual WM_FIRST + WM_SYSCOLORCHANGE;
  end;

  T3dWindow = object(TWindow)              {Window object}
    procedure WMCtlColor(var Msg: TMessage); virtual WM_FIRST + WM_CTLCOLOR;
    procedure WMSysColorChange(var Msg: TMessage); virtual WM_FIRST + WM_SYSCOLORCHANGE;
  end;

  P3DButton = ^T3DButton;
  T3DButton = object(TButton)            {Button control}
    procedure SetupWindow; virtual;
  end;

  P3DCheckBox = ^T3DCheckBox;
  T3DCheckBox = object(TCheckBox)        {Check box control}
    procedure SetupWindow; virtual;
  end;

  P3DComboBox = ^T3DComboBox;
  T3DComboBox = object(TComboBox)        {Combo box control}
    procedure SetupWindow; virtual;
  end;

  P3DEdit = ^T3DEdit;
  T3DEdit = object(TEdit)                {Edit control}
    procedure SetupWindow; virtual;
  end;

  P3DGroupBox = ^T3DGroupBox;
  T3DGroupBox = object(TGroupBox)        {Group box control}
    procedure SetupWindow; virtual;
  end;

  P3DListBox = ^T3DListBox;
  T3DListBox = object(TListBox)          {List box control}
    procedure SetupWindow; virtual;
  end;

  P3DRadioButton = ^T3DRadioButton;
  T3DRadioButton = object(TRadioButton)  {Radio button control}
    procedure SetupWindow; virtual;
  end;

  P3DScrollbar = ^T3DScrollbar;
  T3DScrollbar = object(TScrollbar)      {Scroll bar control}
    procedure SetupWindow; virtual;
  end;

  P3DStatic = ^T3DStatic;
  T3DStatic = object(TStatic)            {Static control}
    procedure SetupWindow; virtual;
  end;

{============= Replacement common dialog functions =============}

function ChooseColor3D(var CC: TChooseColor): Bool;
function ChooseFont3D(var CF: TChooseFont): Bool;
function FindText3D(var FR: TFindReplace): HWnd;
function GetOpenFilename3D(var OpenFile: TOpenFileName): Bool;
function GetSaveFilename3D(var OpenFile: TOpenFileName): Bool;
function PrintDlg3D(var PD: TPrintDlg): Bool;
function ReplaceText3D(var FR: TFindReplace): HWnd;

{===================== Global unit routines ====================}

function Ctl3dIsEnabled: boolean;
function Ctl3dIsAuto: boolean;
function Ctl3dModelessDlgProc(hWindow: hWnd; Msg,wParam: word; lParam: longint): Bool;
procedure Deregister3D;
function Register3dApp(Auto31,Borders3D,CommDlgs3D: boolean): boolean;
function Register3dDLL(Auto31,Borders3D,CommDlgs3D: boolean): boolean;

{- added to allow apps to register a late bound control (rcf 3/95) }
procedure Register3DControl(hw: HWnd);

IMPLEMENTATION

uses
  Win31,
  DMSDebug;

const
  Base_Version            =  $0009;
  ver_exCtl3D             =  $0201;

  {Messages sent by CTL3D...}
(*  dwl_MsgResult           =  0;  This is now in the WIN31 unit *)
  hInstance_Error         =  THandle(32);
  sem_FailCriticalErrors  =  $0001;
  sem_NoOpenFileErrorBox  =  $8000;

  Ctl3dDLLName            =  'CTL3D.DLL';
  Ctl3dV2DLLName          =  'CTL3DV2.DLL';

  Ctl3D_Buttons           =  $0001;
  Ctl3D_Listboxes         =  $0002;
  Ctl3D_Edits             =  $0004;
  Ctl3D_Combos            =  $0008;
  Ctl3D_StaticTexts       =  $0010;
  Ctl3D_StaticFrames      =  $0020;
  Ctl3D_All               =  $FFFF;

  Ctl3D_Border            =  1;
  Ctl3D_NoBorder          =  0;
  Ctl3D_NoSubclass        =  0;
  Ctl3D_Subclass          =  1;

                            {Names of CTL3D exported functions ...}
  Ctl3dAutoSubClassName   =  'CTL3DAUTOSUBCLASS';
  Ctl3dColorChangeName    =  'CTL3DCOLORCHANGE';
  Ctl3dCtlColorExName     =  'CTL3DCTLCOLOREX';
  Ctl3dEnabledName        =  'CTL3DENABLED';
  Ctl3dDlgFramePaintName  =  'CTL3DDLGFRAMEPAINT';
  Ctl3dGetVerName         =  'CTL3DGETVER';
  Ctl3dRegisterName       =  'CTL3DREGISTER';
  Ctl3dSubclassDlgName    =  'CTL3DSUBCLASSDLG';
  Ctl3dSubclassCtlName    =  'CTL3DSUBCLASSCTL';
  Ctl3dUnregisterName     =  'CTL3DUNREGISTER';


type  {ctl3d exported function templates ...}

  TCtl3dAutoSubclass  =  function(Instance: THandle): Bool;
  TCtl3dColorChange   =  function: Bool;
  TCtl3dCtlColorEx    =  function(Message, wParam: word; lParam: longint): HBrush;
  TCtl3dEnabled       =  function: Bool;
  TCtl3dDlgFramePaint =  function(HWindow: HWnd; Message, wParam: word; lParam: longint): longint;
  TCtl3dGetVer        =  function: word;
  TCtl3dRegister      =  function(Instance: THandle): Bool;
  TCtl3dSubclassCtl   =  function(HWindow: HWnd): Bool;
  TCtl3dSubclassDlg   =  function(HWindow: HWnd; GrBits: word): Bool;
  TCtl3dUnregister    =  function(Instance: THandle): Bool;
  TCommDlgHook        =  function(Wnd: HWnd; Msg, wParam: word; lParam: longint): word;
  THostType           = (AppHost, DLLHost);

var  {Variables to hold addresses of CTL3D exported functions ...}
  Ctl3dAutoSubclass   :  TCtl3dAutoSubclass;
  Ctl3dColorChange    :  TCtl3dColorChange;
  Ctl3dCtlColorEx     :  TCtl3dCtlColorEx;
  Ctl3dEnabled        :  TCtl3dEnabled;
  Ctl3dDlgFramePaint  :  TCtl3dDlgFramePaint;
  Ctl3dGetVer         :  TCtl3dGetVer;
  Ctl3dRegister       :  TCtl3dRegister;
  Ctl3dSubclassDlg    :  TCtl3dSubclassDlg;
  Ctl3dSubclassCtl    :  TCtl3dSubclassCtl;
  Ctl3dUnregister     :  TCtl3dUnregister;

var
  ver_Ctl3D    :  word;

  A31          :  boolean;    {true if Auto-subclassing}
  B3D          :  boolean;    {true if using 3D borders}
  CD3D         :  boolean;    {true if using 3D common dialogs}
  Ctl3dHandle  :  THandle;    {Handle to CTL3D DLL}
  HostType     :  THostType;  {Indicates registered host type for this unit}
  UseCtl3d     :  boolean;    {true if using 3D controls}

var
  UserHookProc :  TCommDlgHook;      {Address of user's hook proc,
                                   if any, for common dialogs}
  WinVersion   :  word;            {Windows version number}
  Win30        :  boolean;          {true if Windows 3.0}

{* --------------------------- Ctl3dIsEnabled ---------------------------- *}

function Ctl3dIsEnabled: boolean;
{ Returns true if unit has enabled use of 3D controls }
begin
  Ctl3dIsEnabled:= UseCtl3d;
end;

{* ----------------------------- Ctl3dIsAuto ----------------------------- *}

function Ctl3dIsAuto: boolean;
{ Returns true if unit has enabled use of 3D controls }
begin
  Ctl3dIsAuto:= UseCtl3d and A31;
end;

{* ----------------------------- HookProc3D ------------------------------ *}

function HookProc3D(HWindow: HWnd; Msg, wParam: word; lParam: longint): word; Export;
{ Common hook procedure for common dialogs. Implements 3D controls for all
  common dialogs when used via the replacement common dialog functions
  contained in this unit.

  If the user has also defined a hook procedure for use with the current
  common dialog function call, its address is now in the unit variable
  UserHookProc. The user hook is then called before this procedure does
  its 3D stuff. }
type
  TIntPtr =  ^integer;
begin
  {Call user hook procedure first}
  if @UserHookProc <> nil then
    HookProc3D:= UserHookProc(HWindow,Msg,wParam,lParam)
  else
    HookProc3D:= 0;

  if UseCtl3D and CD3D then {Now do the 3D stuff ...}
  begin
    case Msg of
      wm_InitDialog: {Initialise: Subclass the common dialog via CTL3D}
        if @Ctl3dSubclassDlg <> nil then
          Ctl3dSubclassDlg(HWindow,Ctl3D_All);

      wm_CtlColor:   {Colour setup: tell CTL3D to do graying}
        if @Ctl3dCtlColorEx <> nil then
          HookProc3D:= Ctl3dCtlColorEx(Msg,wParam,lParam);

      wm_DlgBorder:
        if B3D then  {Border: tell CTL3D to paint a 3D border}
          TIntPtr(lParam)^:= Ctl3D_Border
        else         {Border: tell CTL3D to use a modal border}
          TIntPtr(lParam)^:= Ctl3D_NoBorder;

      wm_NCActivate,
      wm_NCPaint,
      wm_SetText:
        if B3D and(@Ctl3dDlgFramePaint <> nil) then
        begin  {This will ensure CTL3D paints the controls correctly ...}
          SetWindowLong(HWindow, dwl_MsgResult,
                        Ctl3dDlgFramePaint(HWindow,Msg,wParam,lParam));
          HookProc3D:= 1;
        end;
    end; {case}
  end;
end;

{* ---------------------------- ChooseColor3D ---------------------------- *}

function ChooseColor3D(var CC: TChooseColor): Bool;
{ Replacement for common dialog function ChooseColor. Sets up local
  hook procedure for 3D painting(saving address of any user hook proc
  into UserHookProc) and then calls ChooseColor. }
begin
  if not(UseCtl3D and CD3D) then
    ChooseColor3D:= ChooseColor(CC)
  else
  begin
    if((CC.Flags and cc_EnableHook) = cc_EnableHook) then
      UserHookProc:= CC.lpfnHook
    else
    begin
{$IFDEF VER15}
      @UserHookProc:= nil;
{$ELSE}
      UserHookProc:= nil;
{$ENDIF}
      CC.Flags:= CC.Flags or cc_EnableHook;
    end;

    case HostType of
      AppHost:
        TFarProc(@CC.lpfnHook):= MakeProcInstance(TFarProc(@HookProc3D), hInstance);
      DLLHost:
        TFarProc(@CC.lpfnHook):= GetProcAddress(hInstance, 'HookProc3D');
    end;

    ChooseColor3D:= ChooseColor(CC);

    if HostType = AppHost then
      FreeProcInstance(TFarProc(@HookProc3D));
  end;
end;

{* ----------------------------- ChooseFont3D ---------------------------- *}

function ChooseFont3D(var CF: TChooseFont): Bool;
{ Replacement for common dialog function ChooseFont. Sets up local
  hook procedure for 3D painting(saving address of any user hook proc
  into UserHookProc) and then calls ChooseFont. }
begin
  if not(UseCtl3D and CD3D) then
    ChooseFont3D:= ChooseFont(CF)
  else
  begin
    if ((CF.Flags and cf_EnableHook) = cf_EnableHook) then
      UserHookProc:= CF.lpfnHook
    else
    begin
{$IFDEF VER15}
      @UserHookProc:= nil;
{$ELSE}
      UserHookProc:= nil;
{$ENDIF}
      CF.Flags:= CF.Flags or cf_EnableHook;
    end;

    case HostType of
      AppHost:
        TFarProc(@CF.lpfnHook):= MakeProcInstance(TFarProc(@HookProc3D),hInstance);
      DLLHost:
        TFarProc(@CF.lpfnHook):= GetProcAddress(hInstance,'HookProc3D');
    end;

    ChooseFont3D:= ChooseFont(CF);

    if HostType = AppHost then
      FreeProcInstance(TFarProc(@HookProc3D));
  end;
end;

{* ------------------------------ FindText3D ----------------------------- *}

function FindText3D(var FR: TFindReplace): HWnd;
{ Replacement for common dialog function FindText. Sets up local
  hook procedure for 3D painting(saving address of any user hook proc
  into UserHookProc) and then calls FindText. }
begin
  if not(UseCtl3D and CD3D) then
    FindText3D:= FindText(FR)
  else
  begin
    if((FR.Flags and fr_EnableHook) = fr_EnableHook) then
      UserHookProc:= FR.lpfnHook
    else
    begin
{$IFDEF VER15}
      @UserHookProc:= nil;
{$ELSE}
      UserHookProc:= nil;
{$ENDIF}
      FR.Flags:= FR.Flags or fr_EnableHook;
    end;

    case HostType of
      AppHost:
        TFarProc(@FR.lpfnHook):= MakeProcInstance(TFarProc(@HookProc3D),hInstance);
      DLLHost:
        TFarProc(@FR.lpfnHook):= GetProcAddress(hInstance,'HookProc3D');
    end;

    FindText3D:= FindText(FR);

    if HostType = AppHost then
      FreeProcInstance(TFarProc(@HookProc3D));
  end;
end;

{* -------------------------- GetOpenFilename3D --------------------------- *}

function GetOpenFilename3D(var OpenFile: TOpenFileName): Bool;
{
  Replacement for common dialog function GetOpenFileName. Sets up local
   hook procedure for 3D painting(saving address of any user hook proc
  into UserHookProc) and then calls GetOpenFileName.
}
begin
  if not(UseCtl3D and CD3D) then
     GetOpenFileName3D:= GetOpenFileName(OpenFile)
  else
  begin
    if((OpenFile.Flags and ofn_EnableHook) = ofn_EnableHook) then
      UserHookProc:= OpenFile.lpfnHook
    else
    begin
{$IFDEF VER15}
      @UserHookProc:= nil;
{$ELSE}
      UserHookProc:= nil;
{$ENDIF}
      OpenFile.Flags:= OpenFile.Flags or ofn_EnableHook;
    end;

    case HostType of
      AppHost:
        TFarProc(@OpenFile.lpfnHook):= MakeProcInstance(TFarProc(@HookProc3D),hInstance);
      DLLHost:
        TFarProc(@OpenFile.lpfnHook):= GetProcAddress(hInstance,'HookProc3D');
    end;

    GetOpenFileName3D:= GetOpenFileName(OpenFile);

    if HostType = AppHost then
      FreeProcInstance(TFarProc(@HookProc3D));
  end;
end;

{* -------------------------- GetSaveFilename3D -------------------------- *}

function GetSaveFilename3D(var OpenFile: TOpenFileName): Bool;
{ Replacement for common dialog function GetSaveFileName. Sets up local
  hook procedure for 3D painting(saving address of any user hook proc
  into UserHookProc) and then calls GetSaveFileName. }
begin
  if not(UseCtl3D and CD3D) then
    GetSaveFileName3D:= GetSaveFileName(OpenFile)
  else
  begin
    if((OpenFile.Flags and ofn_EnableHook) = ofn_EnableHook) then
      UserHookProc:= OpenFile.lpfnHook
    else
    begin
{$IFDEF VER15}
      @UserHookProc:= nil;
{$ELSE}
      UserHookProc:= nil;
{$ENDIF}
      OpenFile.Flags:= OpenFile.Flags or ofn_EnableHook;
    end;

    case HostType of
      AppHost:
        TFarProc(@OpenFile.lpfnHook):= MakeProcInstance(TFarProc(@HookProc3D),hInstance);
      DLLHost:
        TFarProc(@OpenFile.lpfnHook):= GetProcAddress(hInstance,'HookProc3D');
    end;

    GetSaveFileName3D:= GetSaveFileName(OpenFile);

    if HostType = AppHost then
      FreeProcInstance(TFarProc(@HookProc3D));
  end;
end;

{* ------------------------------ PrintDlg3d ----------------------------- *}

function PrintDlg3D(var PD: TPrintDlg): Bool;
{ Replacement for common dialog function PrintDlg. Sets up local
  hook procedure for 3D painting(saving address of any user hook proc
  into UserHookProc) and then calls PrintDlg. }
begin
  if not(UseCtl3D and CD3D) then
    PrintDlg3D:= PrintDlg(PD)
  else
  begin
    if ((PD.Flags and pd_EnablePrintHook) = pd_EnablePrintHook) then
{$IFDEF VER15}
      UserHookProc:= TCommDlgHook(@PD.lpfnPrintHook)
{$ELSE}
      UserHookProc:= TCommDlgHook(PD.lpfnPrintHook)
{$ENDIF}
    else
    begin
{$IFDEF VER15}
      @UserHookProc:= nil;
{$ELSE}
      UserHookProc:= nil;
{$ENDIF}
      PD.Flags:= PD.Flags or pd_EnablePrintHook;
    end;

    if ((PD.Flags and pd_EnableSetupHook) = pd_EnableSetupHook) then
{$IFDEF VER15}
      UserHookProc:= TCommDlgHook(@PD.lpfnSetupHook)
{$ELSE}
      UserHookProc:= TCommDlgHook(PD.lpfnSetupHook)
{$ENDIF}
    else
      PD.Flags:= PD.Flags or pd_EnableSetupHook;

    case HostType of
      AppHost:
        TFarProc(@PD.lpfnPrintHook):= MakeProcInstance(TFarProc(@HookProc3D),hInstance);
      DLLHost:
        TFarProc(@PD.lpfnPrintHook):= GetProcAddress(hInstance,'HookProc3D');
    end;

    PD.lpfnSetupHook:= PD.lpfnPrintHook;
    PrintDlg3D:= PrintDlg(PD);

    if HostType = AppHost then
      FreeProcInstance(TFarProc(@HookProc3D));
  end;
end;

{* ---------------------------- ReplaceText3d ---------------------------- *}

function ReplaceText3D(var FR: TFindReplace): HWnd;
{ Replacement for common dialog function ReplaceText. Sets up local
  hook procedure for 3D painting(saving address of any user hook proc
  into UserHookProc) and then calls ReplaceText. }
begin
  if not(UseCtl3D and CD3D) then
     ReplaceText3D:= ReplaceText(FR)
  else
  begin
    if ((FR.Flags and fr_EnableHook) = fr_EnableHook) then
      UserHookProc:= FR.lpfnHook
    else
    begin
{$IFDEF VER15}
      @UserHookProc:= nil;
{$ELSE}
      UserHookProc:= nil;
{$ENDIF}
      FR.Flags:= FR.Flags or fr_EnableHook;
    end;

    case HostType of
      AppHost:
        TFarProc(@FR.lpfnHook):= MakeProcInstance(TFarProc(@HookProc3D),hInstance);
      DLLHost:
        TFarProc(@FR.lpfnHook):= GetProcAddress(hInstance,'HookProc3D');
    end;

    ReplaceText3D:= ReplaceText(FR);

    if HostType = AppHost then
      FreeProcInstance(TFarProc(@HookProc3D));
  end;
end;

{* --------------------------- CloseDownCtl3d ---------------------------- *}

procedure CloseDownCtl3d;
{ Application cleanup code called from T3DApplication.Done. Frees the
  CTL3D library for this app. }
begin
  UseCtl3d:= false;
  B3D:= false;
  CD3D:= false;
  A31:= false;

  if Ctl3dHandle >= hInstance_Error then
  begin
    FreeLibrary(Ctl3dHandle);
    Ctl3dHandle:= 0;
  end;

  @Ctl3dAutoSubclass:= nil;
  @Ctl3dColorChange:= nil;
  @Ctl3dCtlColorEx:= nil;
  @Ctl3dEnabled:= nil;
  @Ctl3dDlgFramePaint:= nil;
  @Ctl3dGetVer:= nil;
  @Ctl3dRegister:= nil;
  @Ctl3dSubclassDlg:= nil;
  @Ctl3dSubclassCtl:= nil;
  @Ctl3dUnregister:= nil;
end;

{* ------------------------- Ctl3dModelessDlgProc ------------------------ *}

function Ctl3dModelessDlgProc(hWindow: hWnd; Msg,wParam: word; lParam: longint): Bool;
begin
  Ctl3dModelessDlgProc:= false;

  case Msg of
    WM_INITDIALOG:
      begin
        if Ctl3dIsEnabled and(not Ctl3dIsAuto) then
          if @Ctl3DSubclassDlg <> nil then
            Ctl3DSubclassDlg(hWindow,Ctl3d_All);
        Ctl3dModelessDlgProc:= true;
      end;

    WM_CTLCOLOR:
      if Ctl3dIsEnabled and(not Ctl3dIsAuto) and (@Ctl3dCtlColorEx <> nil) then
        Ctl3dModelessDlgProc:= Bool(Ctl3dCtlColorEx(Msg,wParam,lParam));

    WM_SETTEXT,
    WM_NCPAINT,
    WM_NCACTIVATE:
      if Ctl3dIsEnabled and(not Ctl3dIsAuto) and (@Ctl3dDlgFramePaint <> nil) then
      begin
        SetWindowLong(hWindow, DWL_MSGRESULT, Ctl3dDlgFramePaint(hWindow,Msg,wParam,lParam));
        Ctl3dModelessDlgProc:= true;
      end;
  end;
end;

{* ----------------------------- Deregister3D ---------------------------- *}

procedure Deregister3D;
begin
  if UseCtl3d and(@Ctl3dUnregister <> nil) then
  begin
    Ctl3dUnregister(GetCurrentTask);
    CloseDownCtl3d;
  end;
end;

{* ----------------------------- DLLExists ------------------------------- *}

function DLLExists(FN: PChar): boolean;
{
  General routine that looks for a DLL file. Used only when running
  under Windows 3.0(to prevent the 'Cannot find ...' system modal
  dialog box that occurs under Win 3.0 if the DLL is not present when
  calling LoadLibrary).

   Looks for named DLL in the following order:

  (1) The current directory,
  (2) The windows directory,
  (3) The windows system directory,
  (4) Anywhere in the DOS Path.

   Returns true if found.
}
const
  TempLen      =  255;
var
  Dest        :  array[0..fsPathName] of char;
  Temp        :  array[0..TempLen] of char;
  TExt        :  array[0..fsExtension] of char;
  TName       :  array[0..fsFileName] of char;
begin
  DLLExists:= false;
  Temp[0]:= #0;

  FileSearch(Dest,FN,Temp);

  if Dest[0] <> #0 then
    DLLExists:= true
  else
  begin
    GetWindowsDirectory(Temp, TempLen);
    FileSearch(Dest,FN,Temp);

    if Dest[0] <> #0 then
      DLLExists:= true
    else
    begin
      GetSystemDirectory(Temp,TempLen);
      FileSearch(Dest,FN,Temp);

      if Dest[0] <> #0 then
        DLLExists:= true
      else
      begin
        GetModuleFileName(hInstance,Temp,SizeOf(Temp));
        FileSplit(Temp,Temp,TName,TExt);
        if(StrLen(Temp) > 3) and(Temp[Pred(StrLen(Temp))] = '\') then
          Temp[Pred(StrLen(Temp))]:= #0;

        FileSearch(Dest,FN,Temp);

        if Dest[0] <> #0 then
          DLLExists:= true
        else
        begin
          FileSearch(Dest,FN,GetEnvVar('PATH'));
          if Dest[0] <> #0 then
            DLLExists:= true;
        end;
      end;
    end;
  end;
end;

{* ----------------------------- SetupFor3D ------------------------------ *}

function SetupFor3D(Auto31,Borders3D,CommDlgs3D: boolean): boolean;
{
  Sets up host
}
begin
  SetupFor3D:= true;            {Assume all will be OK}

  if UseCtl3d then                      {Already set up! Finish now}
    Exit
  else
    Ctl3dHandle:= 0;

  if Win30 then
  begin { Running under Win 3.0: look for CTL3D.DLL without causing user error}
    if DLLExists(Ctl3dV2DLLName) then
      Ctl3dHandle:= LoadLibrary(Ctl3dV2DLLName);

    if Ctl3dHandle < hInstance_Error then
      if DLLExists(Ctl3dDLLName) then
        Ctl3dHandle:= LoadLibrary(Ctl3dDLLName);
  end
  else
  begin {Running under Win 3.1: look for CTL3D.DLL without error message}
    SetErrorMode(SEM_NOOPENFILEERRORBOX or SEM_FAILCRITICALERRORS);
    Ctl3dHandle:= LoadLibrary(Ctl3dV2DLLName);

    if Ctl3dHandle < HINSTANCE_ERROR then
      Ctl3dHandle:= LoadLibrary(Ctl3dDLLName);

    SetErrorMode(0);
  end;

  if Ctl3dHandle >= hInstance_Error then
  begin  { Found CTL3D... }
    {get addresses of CTL3D exported functions}
    @Ctl3dAutoSubclass:= GetProcAddress(Ctl3dHandle,Ctl3dAutoSubClassName);
    @Ctl3dColorChange:= GetProcAddress(Ctl3dHandle,Ctl3dColorChangeName);
    @Ctl3dCtlColorEx:= GetProcAddress(Ctl3dHandle,Ctl3dCtlColorExName);
    @Ctl3dEnabled:= GetProcAddress(Ctl3dHandle,Ctl3dEnabledName);
    @Ctl3dDlgFramePaint:= GetProcAddress(Ctl3dHandle,Ctl3dDlgFramePaintName);
    @Ctl3dGetVer:= GetProcAddress(Ctl3dHandle,Ctl3dGetVerName);
    @Ctl3dRegister:= GetProcAddress(Ctl3dHandle,Ctl3dRegisterName);
    @Ctl3dSubclassDlg:= GetProcAddress(Ctl3dHandle,Ctl3dSubclassDlgName);
    @Ctl3dSubclassCtl:= GetProcAddress(Ctl3dHandle,Ctl3dSubclassCtlName);
    @Ctl3dUnregister:= GetProcAddress(Ctl3dHandle,Ctl3dUnregisterName);

    {Register this app instance with DLL}
    UseCtl3D:= Ctl3dRegister(GetCurrentTask);

    if UseCtl3D then  {Registration successful - ensure we have
                       a recent enough version of CTL3D, and
                       calling app wants to use 3D controls}
    begin
      ver_Ctl3D:= Ctl3dGetVer;
      UseCtl3d:= Ctl3dEnabled and (ver_Ctl3D >= Base_Version);
    end;

    if not UseCtl3D then    {Not all conditions met - tidy up}
      CloseDownCtl3d
    else
    begin
      B3D:= Borders3D;          {Save 3D borders state}
      CD3D:= CommDlgs3D;        {Save 3D common dialogs state}

      {CTL3D does not support auto-subclassing for Windows 3.0}
      if (not Win30) and Auto31 then
        A31:= Ctl3dAutoSubclass(GetCurrentTask)
      else
        A31:= false;
    end;
  end;

  SetupFor3D:= UseCtl3d;          {Return true if initialised OK}
end;

{* ---------------------------- Register3dApp ---------------------------- *}

function Register3dApp(Auto31,Borders3D,CommDlgs3D: boolean): boolean;
{ Initialises an application }
begin
  Register3dApp:= SetupFor3D(Auto31,Borders3D,CommDlgs3D);
  HostType:= AppHost;
end;

{* ---------------------------- Register3dApp ---------------------------- *}

function Register3dDLL(Auto31,Borders3D,CommDlgs3D: boolean): boolean;
{ Initialises a DLL }
begin
  Register3dDLL:= SetupFor3D(Auto31,Borders3D,CommDlgs3D);
  HostType:= DLLHost;
end;

{****************************************************************************}
{*                                                                          *}
{* Object:   T3dButton                                                      *}
{*                                                                          *}
{*                                                                          *}
{* Replacement object for TButton. Contains replacement SetupWindow method  *}
{* to add subclass button controls outside of dialog boxes.                 *}
{*                                                                          *}
{****************************************************************************}

{* ------------------------ T3dButton.SetupWindow ------------------------- *}

procedure T3dButton.SetupWindow;
begin
  TButton.SetupWindow;

  if @Ctl3DSubclassCtl <> nil then
    Ctl3dSubclassCtl(hWindow);
end;

{****************************************************************************}
{*                                                                          *}
{* Object:   T3dCheckBox                                                    *}
{*                                                                          *}
{*                                                                          *}
{* Replacement object for TCheckBox. Contains replacement SetupWindow method*}
{* to add subclass check box controls outside of dialog boxes.              *}
{*                                                                          *}
{****************************************************************************}

{* ----------------------- T3dCheckBox.SetupWindow ------------------------ *}

procedure T3dCheckBox.SetupWindow;
begin
  TCheckBox.SetupWindow;

  if @Ctl3DSubclassCtl <> nil then
    Ctl3dSubclassCtl(hWindow);
end;

{****************************************************************************}
{*                                                                          *}
{* Object:   T3dComboBox                                                    *}
{*                                                                          *}
{*                                                                          *}
{* Replacement object for TComboBox. Contains replacement SetupWindow method*}
{* to add subclass combo box controls outside of dialog boxes.              *}
{*                                                                          *}
{****************************************************************************}

{* ----------------------- T3dComboBox.SetupWindow ------------------------ *}

procedure T3dComboBox.SetupWindow;
begin
  TComboBox.SetupWindow;

  if @Ctl3DSubclassCtl <> nil then
    Ctl3dSubclassCtl(hWindow);
end;

{****************************************************************************}
{*                                                                          *}
{* Object:   T3dEdit                                                        *}
{*                                                                          *}
{*                                                                          *}
{* Replacement object for TEdit. Contains replacement SetupWindow method    *}
{* to add subclass edit controls outside of dialog boxes.                   *}
{*                                                                          *}
{****************************************************************************}

{* ------------------------- T3dEdit.SetupWindow -------------------------- *}

procedure T3dEdit.SetupWindow;
begin
  TEdit.SetupWindow;

  if @Ctl3DSubclassCtl <> nil then
    Ctl3dSubclassCtl(hWindow);
end;

{****************************************************************************}
{*                                                                          *}
{* Object:   T3dGroupBox                                                    *}
{*                                                                          *}
{*                                                                          *}
{* Replacement object for TGroupBox. Contains replacement SetupWindow method*}
{* to add subclass group box controls outside of dialog boxes.              *}
{*                                                                          *}
{****************************************************************************}

{* ----------------------- T3dGroupBox.SetupWindow ------------------------ *}

procedure T3dGroupBox.SetupWindow;
begin
  TGroupBox.SetupWindow;

  if @Ctl3DSubclassCtl <> nil then
    Ctl3dSubclassCtl(hWindow);
end;

{****************************************************************************}
{*                                                                          *}
{* Object:   T3dListBox                                                     *}
{*                                                                          *}
{*                                                                          *}
{* Replacement object for TListBox. Contains replacement SetupWindow method *}
{* to add subclass list box controls outside of dialog boxes.               *}
{*                                                                          *}
{****************************************************************************}

{* ------------------------ T3dListBox.SetupWindow ------------------------ *}

procedure T3dListBox.SetupWindow;
begin
  TListBox.SetupWindow;

  if @Ctl3DSubclassCtl <> nil then
    Ctl3dSubclassCtl(hWindow);
end;

{****************************************************************************}
{*                                                                          *}
{* Object:   T3dRadioButton                                                 *}
{*                                                                          *}
{*                                                                          *}
{* Replacement object for TRadioButton. Contains replacement SetupWindow    *}
{* method to add subclass radio button controls outside of dialog boxes.    *}
{*                                                                          *}
{****************************************************************************}

{* ---------------------- T3dRadioButton.SetupWindow ---------------------- *}

procedure T3dRadioButton.SetupWindow;
begin
  TRadioButton.SetupWindow;

  if @Ctl3DSubclassCtl <> nil then
    Ctl3dSubclassCtl(hWindow);
end;

{****************************************************************************}
{*                                                                          *}
{* Object:   T3dScrollbar                                                   *}
{*                                                                          *}
{*                                                                          *}
{* Replacement object for TScrollBar. Contains replacement SetupWindow      *}
{* method to add subclass scroll bar controls outside of dialog boxes.      *}
{*                                                                          *}
{****************************************************************************}

{* ----------------------- T3dScrollbar.SetupWindow ----------------------- *}

procedure T3dScrollbar.SetupWindow;
begin
  TScrollbar.SetupWindow;

  if @Ctl3DSubclassCtl <> nil then
    Ctl3dSubclassCtl(hWindow);
end;

{****************************************************************************}
{*                                                                          *}
{* Object:   T3dStatic                                                      *}
{*                                                                          *}
{*                                                                          *}
{* Replacement object for TStatic. Contains replacement SetupWindow method  *}
{* to add subclass static controls outside of dialog boxes.                 *}
{*                                                                          *}
{****************************************************************************}

{* ------------------------ T3dStatic.SetupWindow ------------------------- *}

procedure T3dStatic.SetupWindow;
begin
  TStatic.SetupWindow;

  if @Ctl3DSubclassCtl <> nil then
    Ctl3dSubclassCtl(hWindow);
end;

{****************************************************************************}
{*                                                                          *}
{* Object:   T3dApplication                                                 *}
{*                                                                          *}
{*                                                                          *}
{* Replacement object for TApplication. Contains replacement Init and Done  *}
{* methods to ensure proper set-up/close-down of 3D interface code.         *}
{*                                                                          *}
{****************************************************************************}

{* ------------------------- T3dApplication.Done -------------------------- *}

Destructor T3dApplication.Done;
{ Deregisters the application instance from CTL3D DLL and tidies up. }
begin
  Deregister3D;
  inherited Done;
end;

{****************************************************************************}
{*                                                                          *}
{* Object:   T3dDialog                                                      *}
{*                                                                          *}
{* Replacement object for TDialog. Controls CTL3D subclassing of individual *}
{* dialogs, and setup of the correct type of dialog border.                 *}
{*                                                                          *}
{****************************************************************************}

{* ------------------------ T3dDialog.WMCtlColor ------------------------- *}

procedure T3dDialog.WMCtlColor(var Msg: TMessage);
{ Ensures proper color setup for 3D dialog. }
var
  hB  :  hBrush;
begin
  if UseCtl3d and(not BWCCClassNames) and (@Ctl3dCtlColorEx <> nil) then
  begin
    with Msg do
      hB:= Ctl3dCtlColorEx(Message,wParam,lParam);

    if hB <> 0 then
      Msg.Result:= hB;
  end;
end;

{* ------------------------ T3dDialog.WMDlgBorder ------------------------ *}

procedure T3dDialog.WMDlgBorder(var Msg: TMessage);
{ Controls use of 3D border. }
type
  TIntPtr = ^integer;
begin
  if (not BWCCClassNames) and UseCtl3d then
  begin
    if B3D then
      TIntPtr(Msg.lParam)^:= Ctl3D_Border
    else
      TIntPtr(Msg.lParam)^:= Ctl3D_NoBorder;
  end;
end;

{* ------------------------ T3dDialog.WMInitDialog ------------------------ *}

procedure T3dDialog.WMInitDialog(var Msg: TMessage);
{ Causes CTL3D to subclass dialog. }
begin
  if(not BWCCClassNames) and UseCtl3d and (not A31) then
    Ctl3dSubclassDlg(hWindow,Ctl3D_All);

  TDialog.WMInitDialog(Msg)
end;

{* ------------------------ T3dDialog.WMNCActivate ------------------------ *}

procedure T3dDialog.WMNCActivate(var Msg: TMessage);
{ Ensures proper frame painting of 3D dialogs on mouse activation. }
begin
  if(not BWCCClassNames) and UseCtl3d and B3D and IsModal and(@Ctl3dDlgFramePaint <> nil) then
  begin
    with Msg do
    begin
      SetWindowLong(HWindow,dwl_MsgResult,
                    Ctl3dDlgFramePaint(HWindow,Message,wParam,lParam));
      Result:= 1;
    end;
  end
  else
    DefWndProc(Msg);
end;

{* ------------------------- T3dDialog.WMNCPaint -------------------------- *}

procedure T3dDialog.WMNCPaint(var Msg: TMessage);
{ ... again, part of proper 3D painting }
begin
  if(not BWCCClassNames) and UseCtl3d and B3D and IsModal and(@Ctl3dDlgFramePaint <> nil) then
  begin
    with Msg do
    begin
      SetWindowLong(HWindow,dwl_MsgResult, Ctl3dDlgFramePaint(HWindow, Message, wParam, lParam));
      Result:= 1;
    end;
  end
  else
    DefWndProc(Msg);
end;

{* ------------------------- T3dDialog.WMSetText -------------------------- *}

procedure T3dDialog.WMSetText(var Msg: TMessage);
{ ... again, part of proper 3D painting }
begin
  if(not BWCCClassNames) and UseCtl3d and B3D and IsModal and
    (@Ctl3dDlgFramePaint <> nil) then
  begin
    with msg do
    begin
      SetWindowLong(HWindow,dwl_MsgResult, Ctl3dDlgFramePaint(HWindow, Message, wParam, lParam));
      Result:= 1;
    end;
  end
  else
    DefWndProc(Msg);
end;

{****************************************************************************}
{*                                                                          *}
{* Object:   T3dDlgWindow                                                   *}
{*                                                                          *}
{* Replacement object for TDlgWindow.                                       *}
{*                                                                          *}
{****************************************************************************}

{* ----------------------- T3dDlgWindow.WMDlgBorder ----------------------- *}

procedure T3dDlgWindow.WMDlgBorder(var Msg: TMessage);
{ Controls use of 3D border. }
type
  TIntPtr = ^integer;
begin
  if(not BWCCClassNames) and UseCtl3d then
  begin
    if B3D then
      TIntPtr(Msg.lParam)^:= Ctl3D_Border
    else
      TIntPtr(Msg.lParam)^:= Ctl3D_NoBorder;
  end;
end;

{* ------------------------ T3dDlgWindow.WMCtlColor ----------------------- *}

procedure T3dDlgWindow.WMCtlColor(var Msg: TMessage);
var
  hB  :  hBrush;
begin
  if(@Ctl3dCtlColorEx <> nil) and UseCtl3d then
  begin
    with Msg do
      hB:= Ctl3dCtlColorEx(Message,wParam,lParam);

    if hB <> 0 then
      Msg.Result:= hB;
  end;
end;

{* ----------------------- T3dDlgWindow.WMInitDialog ---------------------- *}

procedure T3dDlgWindow.WMInitDialog(var Msg: TMessage);
{ Causes CTL3D to subclass dialog. }
begin
  if(not BWCCClassNames) and UseCtl3d then
    Ctl3dSubclassDlg(hWindow,Ctl3D_All);

  TDlgWindow.WMInitDialog(Msg);
end;

{* ----------------------- T3dDlgWindow.WMNCActivate ---------------------- *}

procedure T3dDlgWindow.WMNCActivate(var Msg: TMessage);
{ Ensures proper frame painting of 3D dialogs on mouse activation. }
begin
  if(not BWCCClassNames) and UseCtl3d and B3D and(@Ctl3dDlgFramePaint <> nil) then
  begin
    with Msg do
    begin
      SetWindowLong(HWindow,dwl_MsgResult, Ctl3dDlgFramePaint(HWindow, Message, wParam, lParam));
      Result:= 1
    end;
  end
  else
    DefWndProc(Msg);
end;

{* ------------------------ T3dDlgWindow.WMNCPaint ------------------------ *}

procedure T3dDlgWindow.WMNCPaint(var Msg: TMessage);
{ ... again, part of proper 3D painting }
begin
  if(not BWCCClassNames) and UseCtl3d and B3D and(@Ctl3dDlgFramePaint <> nil) then
  begin
    with Msg do
    begin
      SetWindowLong(HWindow,dwl_MsgResult, Ctl3dDlgFramePaint(HWindow, Message, wParam, lParam));
      Result:= 1
    end;
  end
  else
    DefWndProc(Msg)
end;

{* ------------------------ T3dDlgWindow.WMSetText ------------------------ *}

procedure T3dDlgWindow.WMSetText(var Msg: TMessage);
{ ... again, part of proper 3D painting }
begin
  if(not BWCCClassNames) and UseCtl3d and B3D and(@Ctl3dDlgFramePaint <> nil) then
  begin
    with Msg do
    begin
      SetWindowLong(HWindow,dwl_MsgResult,
                Ctl3dDlgFramePaint(HWindow,Message,wParam,lParam));
      Result:= 1;
    end;
  end
  else
    DefWndProc(Msg);
end;

{* --------------------- T3dDlgWindow.WMSysColorChange -------------------- *}

procedure T3dDlgWindow.WMSysColorChange(var Msg: TMessage);
{ Tell CTL3D of the new desktop color scheme }
begin
  if UseCtl3d then
    Ctl3dColorChange;
end;

{****************************************************************************}
{*                                                                          *}
{* Object:   T3dMDIWindow                                                   *}
{*                                                                          *}
{* Replacement object for TMDIWindow. Processes wm_SysColorChange to ensure *}
{* CTL3D is kept up to date on desktop color scheme.                        *}
{*                                                                          *}
{****************************************************************************}

{* ------------------------ T3dMDIWindow.WMCtlColor ----------------------- *}

procedure T3dMDIWindow.WMCtlColor(var Msg: TMessage);
var
  hB  :  hBrush;
begin
  if(@Ctl3dCtlColorEx <> nil) and UseCtl3d then
  begin
    with Msg do
      hB:= Ctl3dCtlColorEx(Message,wParam,lParam);

    if hB <> 0 then
      Msg.Result:= hB;
  end;
end;

{* -------------------- T3dMDIWindow.WMSysColorChange --------------------- *}

procedure T3dMDIWindow.WMSysColorChange(var Msg: TMessage);
{ Tell CTL3D of the new desktop color scheme }
begin
  if UseCtl3d then
    Ctl3dColorChange;
end;

{****************************************************************************}
{*                                                                          *}
{* Object:   T3dWindow                                                      *}
{*                                                                          *}
{* Replacement object for TWindow. Processes wm_SysColorChange to ensure    *}
{* CTL3D is kept up to date on desktop color scheme.                        *}
{*                                                                          *}
{****************************************************************************}

{* -------------------------- T3dWindow.WMCtlColor ------------------------ *}

procedure T3dWindow.WMCtlColor(var Msg: TMessage);
var
  hB :  hBrush;
begin
  if(@Ctl3dCtlColorEx <> nil) and UseCtl3d then
  begin
    with Msg do
      hB:= Ctl3dCtlColorEx(Message,wParam,lParam);
    if hB <> 0 then
      Msg.Result:= hB;
  end;
end;

{* ---------------------- T3dWindow.WMSysColorChange ---------------------- *}

procedure T3dWindow.WMSysColorChange(var Msg: TMessage);
{ Tell CTL3D of the new desktop color scheme }
begin
  if UseCtl3d then
    Ctl3dColorChange;
end;

procedure Register3DControl(hw: HWnd);
begin
  if @Ctl3DSubclassCtl <> nil then
    Ctl3dSubclassCtl(hw);
end;

{****************************************************************************}
{* Unit initialization. Get Windows version number, and note whether we are *}
{* running under Windows 3.0.                                               *}
{****************************************************************************}
BEGIN
  WinVersion:= loword(GetVersion);
  Win30:=(lo(WinVersion) = 3) and(hi(WinVersion) < 10);
  A31:= false;
  B3D:= false;
  CD3D:= true;
  Ctl3dHandle:= 0;
  HostType:= AppHost;
  UseCtl3d:= false;
END.
