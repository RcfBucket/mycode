(****************************************************************************


uipat.inc

produced by Borland Resource Workshop

Module ID = $2130 = 8496
*****************************************************************************)

const
  DLG_UIPATIENT       = 8496;

  IDC_REVIEW          = 108;

  IDC_EDLAST          = 201;
  IDC_EDFREETEXT      = 202;
  IDC_EDPATID         = 203;
  IDC_EDFIRST         = 204;
  IDC_MODE            = 210;

  IDC_LBLFIRST        = 300;
  IDC_LBLFREETEXT     = 301;
  IDC_LBLLAST         = 302;
  IDC_LBLPATID        = 303;

  IDC_EDUSER1         = 400;
  IDC_EDUSER2         = 401;
  IDC_EDUSER3         = 402;
  IDC_EDUSER4         = 403;
  IDC_EDUSER5         = 404;
  IDC_EDUSER6         = 405;
  IDC_EDUSER7         = 406;
  IDC_EDUSER8         = 407;
  IDC_EDUSER9         = 408;
  IDC_EDUSER10        = 409;
  IDC_LBLUSER1        = 500;
  IDC_LBLUSER2        = 501;
  IDC_LBLUSER3        = 502;
  IDC_LBLUSER4        = 503;
  IDC_LBLUSER5        = 504;
  IDC_LBLUSER6        = 505;
  IDC_LBLUSER7        = 506;
  IDC_LBLUSER8        = 507;
  IDC_LBLUSER9        = 508;
  IDC_LBLUSER10       = 509;

  IDS_CANTLOCATEORIG  = 8496;
  IDS_CONFIRMSAVE     = 8498;
	IDS_NOPATS	=	8499;
	IDS_CONFIRMDELPAT	=	8497;
