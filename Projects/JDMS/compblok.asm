CODE SEGMENT	BYTE PUBLIC

	ASSUME	CS:CODE

	PUBLIC	CompareBlock

Ptr1	Equ	DWord Ptr [BP+10]
Ptr2	Equ	DWord Ptr [BP+06]
CSize	Equ	Word Ptr [BP+4]
Psize	Equ	10			;10 bytes of parameters
;
;  Function CompareBlock (Ptr1, Ptr2: Pointer; CSize: Word): Boolean;
;   Returns True if the blocks at Ptr1^ and Ptr2^ are the same, else
;   returns False.  CSize is the number of bytes to compare.
;

CompareBlock Proc    Near
	push	bp
	mov	bp,sp
	push	ds
	lds	si,Ptr1
	les	di,Ptr2
	mov	cx,CSize
	cld
	rep	cmpsb
	jnz	NotSame
	mov	ax, 1
	jmp	CompareDone
NotSame:
	sub	ax, ax
CompareDone:
	pop	ds
	pop	bp
	ret	Psize
CompareBlock Endp


CODE	ENDS

	END
