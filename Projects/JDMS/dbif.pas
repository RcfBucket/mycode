Unit Dbif;

{- Modified version of version 2.30 kanji DBIF to work with v.3.00 convert }

Interface

Uses
(*  Strs,*)
	Strings,
  TPDate,
  WinTypes,
	WinProcs,
  BtrvIO,
  oDateLI{B};

Type
	DbifFileHandle = ^Byte;
	DbifRecordHandle = ^Byte;
	DbifFieldType = (
		DbifInvalid,
		DbifInteger,
		DbifNumber,
		DbifDate,
		DbifTime,
		DbifMnemonic,
		DbifString,
		DbifText,
    DbifAutoUser,
		DbifAutoDate,
		DbifAutoTime,
		DbifAutoArchive,
		DbifAutoSession);

	DbifFileModeType = (
		DbifExclusive,
		DbifShared);

Const
	DbifPrimaryIndex = 0;
	DbifSessionIndex = 1;

  {- copied from old MSCAN.PAS *v*}
  err_DBIF      =  100;
  err_Btrieve   =  300;
  profFile      = 'DMS.INI';
  appName       = 'DMS';    {- application name for WriteProfileString }

Var
	DbifCurrentSession  : String[8];
	DbifAutoArchiveChar : String[1];

Procedure RefreshDict;
Procedure DbifInit;
Procedure DbifDone; Far;
Function  DbifFileExists(FileName:String):Boolean;
Function  DbifOpenFile(FileName:String;Mode:DbifFileModeType):DbifFileHandle;
Function  DbifCreateFile(FileName:String;FileDesc:String;UDFlag:Boolean):DbifFileHandle;
Procedure DbifCreateLike(SrcFileName,DstFileName:String);
Function  DbifCreateTempFile:DbifFileHandle;
Procedure DbifCreateIndex(FileHandle:DbifFileHandle;FieldHandle:Word);
Procedure DbifDeleteFile(FileHandle:DbifFileHandle);
Procedure DbifCloseFile(FileHandle:DbifFileHandle);
Function  DbifGetRecordCount(FileHandle:DbifFileHandle):LongInt;
Function  DbifGetRecordLength(FileHandle:DbifFileHandle):Word;
Function  DbifNewRecord(FileHandle:DbifFileHandle):DbifRecordHandle;
Procedure DbifCopyRecord(SrcRecordHandle,DstRecordHandle:DbifRecordHandle);
Procedure DbifCopyField(SrcRecordHandle,SrcFieldHandle,DstRecordHandle,DstFieldHandle:Word);
Procedure DbifUseIndex(RecordHandle:DbifRecordHandle;IndexNumber:Word);
Procedure DbifGetRecord(RecordHandle:DbifRecordHandle);
Procedure DbifSearchRecordGE(RecordHandle:DbifRecordHandle);
Procedure DbifPutRecord(RecordHandle:DbifRecordHandle);
Procedure DbifDelRecord(RecordHandle:DbifRecordHandle);
Procedure DbifCloseRecord(RecordHandle:DbifRecordHandle);
Procedure DbifFirstRecord(RecordHandle:DbifRecordHandle);
Procedure DbifLastRecord(RecordHandle:DbifRecordHandle);
Procedure DbifNextRecord(RecordHandle:DbifRecordHandle);
Procedure DbifPrevRecord(RecordHandle:DbifRecordHandle);
Function  DbifFHofRH(RecordHandle:DBIFRecordHandle):DBIFFileHandle;
Function  DbifGetFileCount:Word;
Function  DbifGetFileName(FileNumber:Word):String;
Function  DbifGetFileDesc(FileNumber:Word):String;
Function  DbifGetFileUDFlag(FileNumber:Word):Boolean;
Function  DbifGetFieldCount(FileHandle:DbifFileHandle):Word;
Function  DbifGetFieldName(FileHandle:DbifFileHandle;FieldHandle:Word):String;
Function  DbifGetFieldType(FileHandle:DbifFileHandle;FieldHandle:Word):DbifFieldType;
Function  DbifGetFieldSize(FileHandle:DbifFileHandle;FieldHandle:Word):Word;
Function  DbifGetFieldKeyFlag(FileHandle:DbifFileHandle;FieldHandle:Word):Boolean;
Function  DbifGetFieldUDFlag(FileHandle:DbifFileHandle;FieldHandle:Word):Boolean;
(*Procedure DbifAddField(FileHandle:DbifFileHandle;FldName:String;*)
(*  FldType:DbifFieldType;FldSize:Word;KeyFlag,UDFlag:Boolean);*)
(*Procedure DbifDeleteField(FileHandle:DbifFileHandle;FldName:String);*)
Function  DbifGetFieldVal(RecordHandle:DbifRecordHandle;FieldHandle:Word):String;
Procedure DbifPutFieldVal(RecordHandle:DbifRecordHandle;FieldHandle:Word;FieldValue:String);
Procedure DbifGetField(RecordHandle:DbifRecordHandle;FieldHandle:Word;FieldValue:PChar);
Procedure DbifPutField(RecordHandle:DbifRecordHandle;FieldHandle:Word;FieldValue:PChar);
Function  DbifCompFieldVal(Val1,Val2:String;FieldType:DbifFieldType):Integer;
Procedure DbifSetError(ErrorCode:Word);
Function  DbifGetErrorCode:Word;
Function  DbifValidFileName(FileName:String):Boolean;

Const
	DbifMaxFiles 			= 100;
	DbifMaxRecBufs 		= 100;
	DbifMaxFileEntries 	= 75;
	DbifMaxFieldEntries 	= 20;
	DbifMaxRecordLength 	= 1018;

{This value was determined by subtracting 6 bytes (overhead) from
the page size.  A record must fully fit within a page.  The page size
could be increased in the AddField routine.  This would require
increasing the max page size in BtrvIO.  It would also require
reducing the number of files.  The number of files times the page
size must be less than 64K because we open files in accelerated mode
whenever possible.  Opening a file in accelerated mode "locks" a page
in memory.  This paragraph is for those who think they can have bigger
files by just increasing this constant.  Be sure you've checked out
all the repercussions!!!}

Type
	DbifDictRecType =
		Record
			FileName    : String[8];
			FieldNumber : Word;
			KeyFlag     : Boolean;
			UDFlag		: Boolean;
			FieldName   : String[24];
			FieldType   : DbifFieldType;
			FieldSize   : Word;
			FieldPos		: Word;
		End;
	DbifDirRecType =
		Record
			FileName : String[8];
			FileDesc : String[40];
			KeyLen	: Word;
			UDFlag   : Boolean;
			AltKey	: Word;
		End;
	DbifFieldEntriesType = Array[1..DbifMaxFileEntries] Of Word;
	DbifFileListType = Array[1..DbifMaxFileEntries] Of DbifDirRecType;
	DbifFieldListType = Array[1..DbifMaxFileEntries,1..DbifMaxFieldEntries] Of DbifDictRecType;
	DbifFileListPtr = ^DbifFileListType;
	DbifFieldListPtr = ^DbifFieldListType;
	DbifRecBufType =
		Record
			InUse : Boolean;
			FileHandle : DbifFileHandle;
         IndexNumber : Byte;
			Data : Array[0..DbifMaxRecordLength] Of Char;
		End;
	DbifOpenFileType =
		Record
			OpenFlag     : Boolean;
			OpenCount    : Word;
			PhysOpenFlag : Boolean;
			TempFlag     : Boolean;
			FileMode		 : DbifFileModeType;
			FileName     : String[8];
			DBLen        : Word;
			FileIndex	 : Word; {This is the first index into the DbifFieldList array}
			AutoFlag     : Boolean;
			BtrvDataBuf  :
				Record
					Case Word Of
						1 : (Start   : Word);
						2 : (CreateBuf :
							Record
								FileSpec :
									Record
										RecLen     : Word;
										PageSize   : Word;
										NoOfIdx    : Word;
										RecCount   : LongInt;
										FileFlags  : Word;
										Reserved1  : Word;
										Allocation : Word;
									End;
								KeySpecs : Array[1..31] Of
									Record
										KeyPos     : Word;
										KeyLen     : Word;
										KeyFlags   : Word;
										NotUsed2   : LongInt;
										ExtKeyType : Byte;
										NulVal     : Byte;
										Reserved2  : LongInt;
									End;
							End);
						3 : (IndexBuf :
							Record
								KeySpecs : Array[0..31] Of
									Record
										KeyPos     : Word;
										KeyLen     : Word;
										KeyFlags   : Word;
										NotUsed2   : LongInt;
										ExtKeyType : Byte;
										NulVal     : Byte;
										Reserved2  : LongInt;
									End;
							End);
				End;
				BtrvDBLen : Word;
				BtrvKeyBuf :
					Record
						Case Word Of
							1 : (Start : Word);
							2 : (Key : Array[0..DbifMaxRecordLength] Of Byte);
							3 : (FileName : Array[0..12] Of Char);
					End;
				BtrvPos :
					Record
						Case Word Of
							1 : (Start : Word);
							2 : (Block : Array[0..127] Of Byte);

					End;
		End;

Var
	DbifFileList			: DbifFileListPtr;
	DbifFieldList  		: DbifFieldListPtr;
	DbifTotFileEntries 	: Word;
	DbifTotFieldEntries 	: DbifFieldEntriesType;
	SavedExitProc  		: Pointer;
	DbifOpenFiles  		: Array[1..DbifMaxFiles] Of ^DbifOpenFileType;
	DbifRecBuf     		: Array[1..DbifMaxRecBufs] Of ^DbifRecBufType;
	DbifErrorCode  		: Word;

IMPLEMENTATION


{- copied from old DMString ------------------------------------------- *v* }
function GetStr(strNum: integer; strng: PChar; maxchars: word): integer;
var
  rtnval: integer;
  buffer: PChar;
begin
  GetMem(buffer, maxchars+1);
  rtnval:= LoadString(hinstance, strnum, buffer, maxchars);

  if rtnval > 0 then
    strcopy(strng, buffer);

  FreeMem(buffer, maxchars+1);
  getstr := rtnval;
end;

function GetPStr(strnum: integer; var strng: string): integer;
var
  rtnval: integer;
  buffer: array[0..255] of char;
begin
  rtnval:= loadstring(hinstance, strnum, buffer, 255);
  if rtnval >0 then
  begin
   strng:= strpas(buffer);
  end;
  getpstr:= rtnval;
end;

{- copied from old STRS.PAS }
Procedure TrimTrail(Var tstr: String);
{- return src with all trailing blanks removed }
Var
	k		: Integer;
Begin
	k:= Length(tstr);
	While (k >= 1) and (tstr[k] = ' ') Do
		Dec(k);
	tstr[0]:= Chr(k);
End;

Function Upper(StrToConv:String):String;

Var
	P : Array[0..255] Of Char;
Begin
	StrPCopy(P,StrToConv);
	AnsiUpper(P);
	Upper := StrPas(P);
End;

{-------------------------------------------------------------------- *v* --}

Function DbifValidFileName(FileName:String):Boolean;

Var
	Position 		: Array[0..128] of Byte;
	Data				: Array[0..128] of Char;
  KeyBuf			: Array[0..128] Of Char;
  DataLen			: Word;
  KeyLen			: Word;
	f 					: File;
	Waste 			: Word;

Begin
  DbifErrorCode := 0;
	DBIFValidFileName := FALSE;
	If Length(FileName) > 8 Then EXIT;
	If FileName <> 'VALID' Then
    BEGIN
    	FileName := Upper(FileName);
    	Assign(f,'VALID.DAT');
    	Rename(f,FileName+'.TMP');
    	If IOResult > 0 Then EXIT;
    	StrPCopy(KeyBuf,FileName+'.TMP');
      DataLen := 0;
    	If Btrv(0,Position,Data,Datalen,KeyBuf,0,0) > 0 Then
    		Begin
    			Rename(f,'VALID.DAT');
    			Waste := IOResult;
    			Exit;
        End;
    	Btrv(1,Position,Data,DataLen,KeyBuf,0,0);
    	Rename(f,'VALID.DAT');
			Waste := IOResult;
		END;
	DbifValidFileName := TRUE;
End;


Procedure DbifSetError(ErrorCode:Word);

Var
	ErrorDesc : String;

Begin
	DbifErrorCode := ErrorCode;
End;


Procedure RefreshDict;

Var
	DbifDictListFile : File;
	FileCounter : Word;
	FieldCounter : Word;

Begin
	{Calc all field positions}
	For FileCounter := 1 To DbifTotFileEntries Do
		Begin
			DbifFieldList^[FileCounter,1].FieldPos := 0;
			For FieldCounter := 2 To DbifTotFieldEntries[FileCounter] Do
				DbifFieldList^[FileCounter,FieldCounter].FieldPos :=
					DbifFieldList^[FileCounter,FieldCounter-1].FieldPos +
					DbifFieldList^[FileCounter,FieldCounter-1].FieldSize;
		End;
	{Calc all file key lengths}
	For FileCounter := 1 To DbifTotFileEntries Do
		Begin
			DbifFileList^[FileCounter].KeyLen := 0;
			For FieldCounter := 1 To DbifTotFieldEntries[FileCounter] Do
				If DbifFieldList^[FileCounter,FieldCounter].KeyFlag Then
					Inc(DbifFileList^[FileCounter].KeyLen,
						DbifFieldList^[FileCounter,FieldCounter].FieldSize);
		End;
	{Write new data dict files}
	Assign(DbifDictListFile,'DBIFDICT.DAT');
	ReWrite(DbifDictListFile,1);
	BlockWrite(DbifDictListFile,DbifTotFileEntries,2);
	For FileCounter := 1 To DbifTotFileEntries Do
		BlockWrite(DbifDictListFile,DbifTotFieldEntries[FileCounter],2);
	For FileCounter := 1 To DbifTotFileEntries Do
		BlockWrite(DbifDictListFile,DbifFileList^[FileCounter],SizeOf(DbifDirRecType));
	For FileCounter := 1 To DbifTotFileEntries Do
		For FieldCounter := 1 To DbifTotFieldEntries[FileCounter] Do
			BlockWrite(DbifDictListFile,DbifFieldList^[FileCounter,FieldCounter],SizeOf(DbifDictRecType));
	Close(DbifDictListFile);
End;


Procedure BtrvOpenFile(FileHandle:DbifFileHandle);

Var
	BtrvErrorCode : Word;
	FileToPhysClose : Word;
	CloseErrorCode : Word;

Begin
	{Exit if file is already physically open}
	If DbifOpenFiles[LongInt(FileHandle)]^.PhysOpenFlag Then
		Exit;
	Repeat
		{Setup Btrv FileName}
		StrPCopy(DbifOpenFiles[LongInt(FileHandle)]^.BtrvKeyBuf.FileName,DbifOpenFiles[LongInt(FileHandle)]^.FileName+'.DAT');
		{Setup DBLen}
		DbifOpenFiles[LongInt(FileHandle)]^.BtrvDBLen := 0;
		{Call Btrv to open the file}
		BtrvErrorCode := Btrv(0,
			DbifOpenFiles[LongInt(FileHandle)]^.BtrvPos.Start,
			DbifOpenFiles[LongInt(FileHandle)]^.BtrvDataBuf.Start,
			DbifOpenFiles[LongInt(FileHandle)]^.BtrvDBLen,
			DbifOpenFiles[LongInt(FileHandle)]^.BtrvKeyBuf.Start,
			255,
			0);
		If (BtrvErrorCode > 0) And (BtrvErrorCode <> 86) Then
			Begin
				DbifSetError(Err_Dbif+21);
				Exit;
			End;
		If BtrvErrorCode = 86 Then
			Begin
				{Find a file to physically close}
				{File must be logically and physically open}
				FileToPhysClose := 0;
				Repeat
					FileToPhysClose := Succ(FileToPhysClose);
				Until (DbifOpenFiles[FileToPhysClose]^.OpenFlag) And
						(DbifOpenFiles[FileToPhysClose]^.PhysOpenFlag);
				CloseErrorCode := Btrv(1,
					DbifOpenFiles[FileToPhysClose]^.BtrvPos.Start,
					DbifOpenFiles[FileToPhysClose]^.BtrvDataBuf.Start,
					DbifOpenFiles[FileToPhysClose]^.BtrvDBLen,
					DbifOpenFiles[FileToPhysClose]^.BtrvKeyBuf.Start,
					255,
					0);
				If CloseErrorCode > 0 Then
					Begin
						DbifSetError(Err_Dbif+22);
						Exit;
					End;
				DbifOpenFiles[FileToPhysClose]^.PhysOpenFlag := False;
			End;
	Until BtrvErrorCode = 0;
	{Mark the file physically open}
	DbifOpenFiles[LongInt(FileHandle)]^.PhysOpenFlag := True;
End;


Function DbifGetErrorCode;

Begin
	DbifGetErrorCode := DbifErrorCode;
	DbifErrorCode := 0;
End;


Procedure DbifDone;

Var
	i : Word;

Begin
	{Replace ExitProc so other units will exit properly}
	ExitProc := SavedExitProc;
	{Dispose of the rec bufs array}
	For i := 1 To DbifMaxRecBufs Do
		Begin
			If DbifRecBuf[i]^.InUse Then
				DbifCloseRecord(DbifRecordHandle(i));
			Dispose(DbifRecBuf[i]);
		End;
	{Dispose of the open files array}
	For i := 1 To DbifMaxFiles Do
		Begin
			If DbifOpenFiles[i]^.OpenFlag Then
				DbifCloseFile(DbifFileHandle(i));
			Dispose(DbifOpenFiles[i]);
		End;
	{Dispose of the dictionary list}
	Dispose(DbifFileList);
	Dispose(DbifFieldList);
End;

Procedure DbifInit;

Var
	i 						: Word;
	Waste 				: Word;
	DbifDictListFile 	: File;
	FileCounter 		: Word;
	FieldCounter 		: Word;
	TempValue 			: Array[0..255] Of Char;

Begin
	{Set up the exit procedure}
	SavedExitProc := ExitProc;
	ExitProc := @DbifDone;
	{Set up date format defaults}
(*  GetPrivateProfileString(AppName,'Date Order','Year Month Day',TempValue,255,ProfFile);*)
(*  If StrComp(TempValue,'Day Month Year') = 0 Then*)
(*    DateOrder := DMY;*)
(*  If StrComp(TempValue,'Month Day Year') = 0 Then*)
(*    DateOrder := MDY;*)
(*  If StrComp(TempValue,'Year Month Day') = 0 Then*)
(*    DateOrder := YMD;*)
{--- *v* --- force date order to Y/M/D --}
  DateOrder := YMD;

	GetPrivateProfileString(AppName,'Date Delimiter','Slash',TempValue,255,ProfFile);
	If StrComp(TempValue,'Slash') = 0 Then
		DateDelimiter := Slash;
	If StrComp(TempValue,'Comma') = 0 Then
		DateDelimiter := Comma;
	If StrComp(TempValue,'Hyphen') = 0 Then
		DateDelimiter := Hyphen;
	If StrComp(TempValue,'Period') = 0 Then
		DateDelimiter := Period;
	GetPrivateProfileString(AppName,'Date Leading Zeros','Yes',TempValue,255,ProfFile);
	If StrComp(TempValue,'Yes') = 0 Then
		DateLeadingZeros := True
	Else
		DateLeadingZeros := False;
	GetPrivateProfileString(AppName,'Date Four Digit Year','Yes',TempValue,255,ProfFile);
	If StrComp(TempValue,'Yes') = 0 Then
		DateFourDigitYear := True
	Else
		DateFourDigitYear := False;
	DbifAutoArchiveChar := 'Y';
(*   {Initialize the current session}*)
(*  GetPrivateProfileString(AppName,'CurrentSession',sessDeflt,TempValue,255,ProfFile);*)
(*   DbifCurrentSession := StrPas(TempValue);*)
   DbifCurrentSession := 'NONE';


	{Initialize the dictionary list}
	New(DbifFileList);
  FillChar(DBIFFileList^,SIZEOF(DBIFFileListtype),CHR(0));
	New(DbifFieldList);
	FillChar(DBIFFieldList^,SIZEOF(DBIFFieldListtype),CHR(0));
	{Read data dict files}
	Assign(DbifDictListFile,'DBIFDICT.DAT');
	Reset(DbifDictListFile,1);
	If IOResult > 0 Then
		DbifTotFileEntries := 0
	Else
		Begin
			BlockRead(DbifDictListFile,DbifTotFileEntries,2);
			For FileCounter := 1 To DbifTotFileEntries Do
				BlockRead(DbifDictListFile,DbifTotFieldEntries[FileCounter],2);
			For FileCounter := 1 To DbifTotFileEntries Do
				BlockRead(DbifDictListFile,DbifFileList^[FileCounter],SizeOf(DbifDirRecType));
			For FileCounter := 1 To DbifTotFileEntries Do
				For FieldCounter := 1 To DbifTotFieldEntries[FileCounter] Do
					BlockRead(DbifDictListFile,DbifFieldList^[FileCounter,FieldCounter],SizeOf(DbifDictRecType));
			Close(DbifDictListFile);
		End;
	{Set up the open files array}
	For i := 1 To DbifMaxFiles Do
		Begin
			New(DbifOpenFiles[i]);
			DbifOpenFiles[i]^.OpenFlag := False;
		End;
	{Set up the rec bufs array}
	For i := 1 To DbifMaxRecBufs Do
		Begin
			New(DbifRecBuf[i]);
			DbifRecBuf[i]^.InUse := False;
		End;
	{Set up error vars}
	DbifErrorCode := 0
End;


Function DbifFileExists;

Var
	FileIndex : Word;

Begin
   DbifErrorCode := 0;
	FileName := Upper(FileName);
	FileIndex := 1;
	While (DbifFileList^[FileIndex].FileName <> FileName) And (FileIndex < DbifTotFileEntries) Do
		Inc(FileIndex);
	DbifFileExists := DbifFileList^[FileIndex].FileName = FileName;
End;


Function DbifOpenFile;

Var
	FileCounter 	: Word;
	BtrvErrorCode 	: Word;
	DBLen 			: Word;
	FileIndex 		: Word;
	AutoFlag 		: Boolean;
	Found 			: Boolean;
	i 					: Word;

Begin
	DbifErrorCode := 0;
	If FileName = '' Then
		Begin
			DbifSetError(Err_Dbif+24);
			Exit;
		End;
	{Uppercase FileName}
	FileName := Upper(FileName);
	{Make sure file exists}
	FileIndex := 1;
	While (DbifFileList^[FileIndex].FileName <> FileName) And (FileIndex <= DbifTotFileEntries) Do
		Inc(FileIndex);
	If DbifFileList^[FileIndex].FileName <> FileName Then
		Begin
			DbifSetError(Err_Dbif+25);
			Exit;
		End;
	{Make sure file is not open in a conficting manner}
	{If this call is to open the file in exclusive mode
	the file must not be open at all}
	{If this call is to open the file in shared mode
	the file may be open in shared mode, but not in
	exclusive mode}
	For FileCounter := 1 To DbifMaxFiles Do
		If DbifOpenFiles[FileCounter]^.OpenFlag Then
			If DbifOpenFiles[FileCounter]^.FileName = FileName Then
				If DbifOpenFiles[FileCounter]^.FileMode = DbifExclusive Then
					Begin
						DbifSetError(Err_Dbif+3);
						Exit
					End
				Else
					If Mode = DbifExclusive Then
						Begin
							DbifSetError(Err_Dbif+4);
							Exit
						End;
	{Find a file handle}
	FileCounter := 0;
	Repeat
		FileCounter := Succ(FileCounter)
	Until ((Not DbifOpenFiles[FileCounter]^.OpenFlag) Or
		(FileCounter = DbifMaxFiles)) Or
		((DbifOpenFiles[FileCounter]^.FileName = FileName) And
		DbifOpenFiles[FileCounter]^.OpenFlag);
	If (DbifOpenFiles[FileCounter]^.FileName = FileName) And
		DbifOpenFiles[FileCounter]^.OpenFlag Then
		Begin
			Inc(DbifOpenFiles[FileCounter]^.OpenCount);
			DbifOpenFile := DbifFileHandle(FileCounter);
		End
	Else
		Begin
			{Calc file index}
			FileIndex := 1;
			While DbifFileList^[FileIndex].FileName <> FileName Do
				Inc(FileIndex);
			{Calc Key and DB lengths}
			DBLen := 0;
			For i := 1 To DbifTotFieldEntries[FileIndex] Do
				Inc(DBLen,DbifFieldList^[FileIndex,i].FieldSize);
			AutoFlag := False;
			For i := 1 To DbifTotFieldEntries[FileIndex] Do
				If (DbifFieldList^[FileIndex,i].FieldType = DbifAutoUser) Or
					(DbifFieldList^[FileIndex,i].FieldType = DbifAutoDate) Or
					(DbifFieldList^[FileIndex,i].FieldType = DbifAutoTime) Or
					(DbifFieldList^[FileIndex,i].FieldType = DbifAutoArchive) Or
					(DbifFieldList^[FileIndex,i].FieldType = DbifAutoSession) Then
						AutoFlag := True;
			{Open the file Dbif style}
			If DbifOpenFiles[FileCounter]^.OpenFlag Then
				Begin
					DbifSetError(Err_Dbif+1);
					Exit
				End
			Else
				Begin
					DbifOpenFiles[FileCounter]^.OpenFlag      := True;
					DbifOpenFiles[FileCounter]^.OpenCount     := 1;
					DbifOpenFiles[FileCounter]^.PhysOpenFlag  := False;
					DbifOpenFiles[FileCounter]^.TempFlag      := False;
					DbifOpenFiles[FileCounter]^.FileMode      := Mode;
					DbifOpenFiles[FileCounter]^.FileName      := FileName;
					DbifOpenFiles[FileCounter]^.DBLen         := DBLen;
					DbifOpenFiles[FileCounter]^.FileIndex		:= FileIndex;
					DbifOpenFiles[FileCounter]^.AutoFlag		:= AutoFlag;
				End;
			{Return file handle}
			DbifOpenFile := DbifFileHandle(FileCounter);
		End;
End;


Function DbifCreateFile;

Var
	f 					: File;
	FileHandle 		: LongInt;
	FileIndex 		: Word;
	FileHandleStr 	: String;

Begin
	DbifErrorCode := 0;
   {Uppercase FileName}
	FileName := Upper(FileName);
	{Validate FileName}
	If Not DbifValidFileName(FileName) Then
		Begin
			DbifSetError(Err_Dbif+24);
			Exit
		End;
	{Add the file to the file list}
	Inc(DbifTotFileEntries);
	DbifFileList^[DbifTotFileEntries].FileName := FileName;
	DbifFileList^[DbifTotFileEntries].FileDesc := FileDesc;
	DbifFileList^[DbifTotFileEntries].UDFlag := UDFlag;
	DbifFileList^[DbifTotFileEntries].AltKey := 0;
	DbifTotFieldEntries[DbifTotFileEntries] := 0;
	FileIndex := DbifTotFileEntries;
	RefreshDict;
	{Get a file handle}
	FileHandle := 0;
	Repeat
		FileHandle := FileHandle + 1;
	Until (Not DbifOpenFiles[LongInt(FileHandle)]^.OpenFlag) Or (FileHandle = DbifMaxFiles);
	If DbifOpenFiles[LongInt(FileHandle)]^.OpenFlag Then
		Begin
			DbifSetError(Err_Dbif+1);
			Exit
		End;
	DbifOpenFiles[LongInt(FileHandle)]^.OpenFlag			:= True;
	DbifOpenFiles[LongInt(FileHandle)]^.OpenCount		:= 1;
	DbifOpenFiles[LongInt(FileHandle)]^.PhysOpenFlag	:= False;
	DbifOpenFiles[LongInt(FileHandle)]^.TempFlag			:= False;
	DbifOpenFiles[LongInt(FileHandle)]^.FileMode			:= DbifExclusive;
	DbifOpenFiles[LongInt(FileHandle)]^.FileName			:= FileName;
	DbifOpenFiles[LongInt(FileHandle)]^.DBLen				:= 0;
	DbifOpenFiles[LongInt(FileHandle)]^.FileIndex		:= FileIndex;
	DbifCreateFile := DbifFileHandle(FileHandle)
End;


Procedure DbifCreateLike;

Var
	f 					: File;
	SrcFileHandle 	: LongInt;
   DstFileHandle 	: LongInt;
	SrcFileIndex 	: Word;
   DstFileIndex 	: Word;
	FileHandleStr 	: String;
   FieldCounter 	: Word;
   TempFlag 		: Boolean;
   DstFHStr 		: String[10];

Begin
	DbifErrorCode := 0;
   TempFlag := False;
   {Uppercase FileName}
	SrcFileName := Upper(SrcFileName);
   DstFileName := Upper(DstFileName);
   {Get Src and Dst file handles.}
	SrcFileHandle := 0;
	Repeat
		Inc(SrcFileHandle);
	Until (Not DbifOpenFiles[SrcFileHandle]^.OpenFlag) Or (SrcFileHandle = DbifMaxFiles);
   DbifOpenFiles[SrcFileHandle]^.OpenFlag := True;
   DstFileHandle := 0;
 	Repeat
		Inc(DstFileHandle);
	Until (Not DbifOpenFiles[LongInt(DstFileHandle)]^.OpenFlag) Or (DstFileHandle = DbifMaxFiles);
   DbifOpenFiles[DstFileHandle]^.OpenFlag := True;
	If DstFileName = 'TEMP' Then
		Begin
        	TempFlag := True;
         Str(LongInt(DstFileHandle),DstFHStr);
			DstFileName := 'TEMP'+DstFHStr;
      End;
   {Find the source file and set the SrcFileIndex}
   SrcFileIndex := 1;
   While DbifFileList^[SrcFileIndex].FileName <> SrcFileName Do
   	Inc(SrcFileIndex);
	{Add the file to the file list}
	Inc(DbifTotFileEntries);
	DstFileIndex := DbifTotFileEntries;
	DbifFileList^[DstFileIndex].FileName := DstFileName;
	DbifFileList^[DstFileIndex].FileDesc := DbifFileList^[SrcFileIndex].FileDesc;
	DbifFileList^[DstFileIndex].UDFlag   := DbifFileList^[SrcFileIndex].UDFlag;
	DbifFileList^[DstFileIndex].AltKey   := DbifFileList^[SrcFileIndex].AltKey;
	{Transfer the field info from Src file to Dst file}
	DbifTotFieldEntries[DstFileIndex] := DbifTotFieldEntries[SrcFileIndex];
   For FieldCounter := 1 To DbifTotFieldEntries[SrcFileIndex] Do
   	DbifFieldList^[DstFileIndex,FieldCounter] := DbifFieldList^[SrcFileIndex,FieldCounter];
	RefreshDict;
   {Open the Src file.}
	StrPCopy(DbifOpenFiles[SrcFileHandle]^.BtrvKeyBuf.FileName,SrcFileName+'.DAT');
	DbifOpenFiles[SrcFileHandle]^.BtrvDBLen := 0;
	Btrv(0,
		DbifOpenFiles[SrcFileHandle]^.BtrvPos.Start,
		DbifOpenFiles[SrcFileHandle]^.BtrvDataBuf.Start,
      DbifOpenFiles[SrcFileHandle]^.BtrvDBLen,
		DbifOpenFiles[SrcFileHandle]^.BtrvKeyBuf.Start,
		255,
		0);
   {Stat the Src file.}
   DbifOpenFiles[SrcFileHandle]^.BtrvDBLen := 512;
   Btrv(15,
		DbifOpenFiles[SrcFileHandle]^.BtrvPos.Start,
		DbifOpenFiles[SrcFileHandle]^.BtrvDataBuf.Start,
      DbifOpenFiles[SrcFileHandle]^.BtrvDBLen,
		DbifOpenFiles[SrcFileHandle]^.BtrvKeyBuf.Start,
		255,
		0);
   {Copy the data buffer from Src file to Dst file.}
	Move(DbifOpenFiles[SrcFileHandle]^.BtrvDataBuf.Start,
   	DbifOpenFiles[DstFileHandle]^.BtrvDataBuf.Start,
      SizeOf(DbifOpenFiles[SrcFileHandle]^.BtrvDataBuf));
   DbifOpenFiles[DstFileHandle]^.BtrvDBLen :=
   	DbifOpenFiles[SrcFileHandle]^.BtrvDBLen;
   {Close the Src file.}
	Btrv(1,
		DbifOpenFiles[SrcFileHandle]^.BtrvPos.Start,
		DbifOpenFiles[SrcFileHandle]^.BtrvDataBuf.Start,
		DbifOpenFiles[SrcFileHandle]^.BtrvDBLen,
		DbifOpenFiles[SrcFileHandle]^.BtrvKeyBuf.Start,
		255,
		0);
   DbifOpenFiles[SrcFileHandle]^.OpenFlag := False;
   {Create the Dst file.}
	StrPCopy(DbifOpenFiles[DstFileHandle]^.BtrvKeyBuf.FileName,DstFileName+'.DAT');
	Btrv(14,
		DbifOpenFiles[DstFileHandle]^.BtrvPos.Start,
		DbifOpenFiles[DstFileHandle]^.BtrvDataBuf.Start,
		DbifOpenFiles[DstFileHandle]^.BtrvDBLen,
		DbifOpenFiles[DstFileHandle]^.BtrvKeyBuf.Start,
		255,
		0);
   {Close the Dst file.}
  	Btrv(1,
		DbifOpenFiles[DstFileHandle]^.BtrvPos.Start,
		DbifOpenFiles[DstFileHandle]^.BtrvDataBuf.Start,
		DbifOpenFiles[DstFileHandle]^.BtrvDBLen,
		DbifOpenFiles[DstFileHandle]^.BtrvKeyBuf.Start,
		255,
		0);
	DbifOpenFiles[DstFileHandle]^.OpenFlag := False;
End;


Function DbifCreateTempFile;

Var
	f 					: File;
	FileHandle 		: LongInt;
	FileHandleStr 	: String;
	FileIndex 		: Word;

Begin
	DbifErrorCode := 0;
	FileHandle := 0;
	Repeat
		FileHandle := FileHandle + 1;
	Until (Not DbifOpenFiles[LongInt(FileHandle)]^.OpenFlag) Or (FileHandle = DbifMaxFiles);
	If DbifOpenFiles[LongInt(FileHandle)]^.OpenFlag Then
		Begin
			DbifSetError(Err_Dbif+1);
			Exit
		End;
	Str(FileHandle,FileHandleStr);
	Inc(DbifTotFileEntries);
	DbifFileList^[DbifTotFileEntries].FileName := 'TEMP'+FileHandleStr;
	DbifFileList^[DbifTotFileEntries].FileDesc := 'Temporary File';
	DbifFileList^[DbifTotFileEntries].UDFlag := False;
	DbifFileList^[DbifTotFileEntries].AltKey := 0;
	DbifTotFieldEntries[DbifTotFileEntries] := 0;
	FileIndex := DbifTotFileEntries;
	DbifOpenFiles[LongInt(FileHandle)]^.OpenCount		:= 1; {MJS}
        DbifOpenFiles[LongInt(FileHandle)]^.OpenFlag			:= True;
	DbifOpenFiles[LongInt(FileHandle)]^.PhysOpenFlag	:= False;
	DbifOpenFiles[LongInt(FileHandle)]^.FileMode			:= DbifExclusive;
	DbifOpenFiles[LongInt(FileHandle)]^.DBLen				:= 0;
	DbifOpenFiles[LongInt(FileHandle)]^.TempFlag := True;
	DbifOpenFiles[LongInt(FileHandle)]^.FileName := 'TEMP' + FileHandleStr;
	DbifOpenFiles[LongInt(FileHandle)]^.FileIndex := FileIndex;
	DbifCreateTempFile := DbifFileHandle(FileHandle)
End;

Procedure DbifCreateIndex;

Var
	FieldCounter		 : Word;
	NoOfKeys           : Word;
	KeyNo              : Word;
	BtrvErrorCode      : Word;

Begin
	DbifErrorCode := 0;
	DbifFileList^[DbifTotFileEntries].AltKey := FieldHandle;
	RefreshDict;
	BtrvOpenFile(FileHandle);
	{Load up 0th element with the supplemental field info}
	DbifOpenFiles[LongInt(FileHandle)]^.BtrvDataBuf.IndexBuf.KeySpecs[0].KeyPos :=
		DbifFieldList^[DbifOpenFiles[LongInt(FileHandle)]^.FileIndex,FieldHandle].FieldPos+1;
	DbifOpenFiles[LongInt(FileHandle)]^.BtrvDataBuf.IndexBuf.KeySpecs[0].KeyLen :=
		DbifFieldList^[DbifOpenFiles[LongInt(FileHandle)]^.FileIndex,FieldHandle].FieldSize;
	DbifOpenFiles[LongInt(FileHandle)]^.BtrvDataBuf.IndexBuf.KeySpecs[0].KeyFlags := 256+16+2;
	DbifOpenFiles[LongInt(FileHandle)]^.BtrvDataBuf.IndexBuf.KeySpecs[0].NotUsed2 := 0;
	Case DbifFieldList^[DbifOpenFiles[LongInt(FileHandle)]^.FileIndex,FieldHandle].FieldType Of
		DbifInteger  	 : DbifOpenFiles[LongInt(FileHandle)]^.BtrvDataBuf.IndexBuf.KeySpecs[0].ExtKeyType := 1;
		DbifMnemonic 	 : DbifOpenFiles[LongInt(FileHandle)]^.BtrvDataBuf.IndexBuf.KeySPecs[0].ExtKeyType := 10;
		DbifString   	 : DbifOpenFiles[LongInt(FileHandle)]^.BtrvDataBuf.IndexBuf.KeySpecs[0].ExtKeyType := 10;
		DbifText     	 : DbifOpenFiles[LongInt(FileHandle)]^.BtrvDataBuf.IndexBuf.KeySpecs[0].ExtKeyType := 11;
		DbifDate     	 : DbifOpenFiles[LongInt(FileHandle)]^.BtrvDataBuf.IndexBuf.KeySpecs[0].ExtKeyType := 3;
		DbifTime     	 : DbifOpenFiles[LongInt(FileHandle)]^.BtrvDataBuf.IndexBuf.KeySpecs[0].ExtKeyType := 4;
		DbifAutoUser 	 : DbifOpenFiles[LongInt(FileHandle)]^.BtrvDataBuf.IndexBuf.KeySpecs[0].ExtKeyType := 10;
		DbifAutoDate 	 : DbifOpenFiles[LongInt(FileHandle)]^.BtrvDataBuf.IndexBuf.KeySpecs[0].ExtKeyType := 3;
		DbifAutoTime 	 : DbifOpenFiles[LongInt(FileHandle)]^.BtrvDataBuf.IndexBuf.KeySpecs[0].ExtKeyType := 4;
		DbifAutoArchive : DbifOpenFiles[LongInt(FileHandle)]^.BtrvDataBuf.IndexBuf.KeySpecs[0].ExtKeyType := 10;
		DbifAutoSession : DbifOpenFiles[LongInt(FileHandle)]^.BtrvDataBuf.IndexBuf.KeySpecs[0].ExtKeyType := 10;
	End;
	DbifOpenFiles[LongInt(FileHandle)]^.BtrvDataBuf.IndexBuf.KeySpecs[0].NulVal := 0;
	DbifOpenFiles[LongInt(FileHandle)]^.BtrvDataBuf.IndexBuf.KeySpecs[0].Reserved2 := 0;
	{Calc NoOfKeys}
	NoOfKeys := 0;
	For FieldCounter := 1 To DbifTotFieldEntries[DbifOpenFiles[LongInt(FileHandle)]^.FileIndex] Do
		If DbifFieldList^[DbifOpenFiles[LongInt(FileHandle)]^.FileIndex,FieldCounter].KeyFlag Then
			Inc(NoOfKeys);
	{Setup key specifications}
	KeyNo := 1;
	For FieldCounter := 1 To DbifTotFieldEntries[DbifOpenFiles[LongInt(FileHandle)]^.FileIndex] Do
		Begin
			If DbifFieldList^[DbifOpenFiles[LongInt(FileHandle)]^.FileIndex,FieldCounter].KeyFlag Then
				Begin
					DbifOpenFiles[LongInt(FileHandle)]^.BtrvDataBuf.IndexBuf.KeySpecs[KeyNo].KeyPos :=
						DbifFieldList^[DbifOpenFiles[LongInt(FileHandle)]^.FileIndex,FieldCounter].FieldPos+1;
					DbifOpenFiles[LongInt(FileHandle)]^.BtrvDataBuf.IndexBuf.KeySpecs[KeyNo].KeyLen :=
						DbifFieldList^[DbifOpenFiles[LongInt(FileHandle)]^.FileIndex,FieldCounter].FieldSize;
					If KeyNo = NoOfKeys {Last or Only Segment} Then
						DbifOpenFiles[LongInt(FileHandle)]^.BtrvDataBuf.IndexBuf.KeySpecs[KeyNo].KeyFlags := 256+2
					Else
						DbifOpenFiles[LongInt(FileHandle)]^.BtrvDataBuf.IndexBuf.KeySpecs[KeyNo].KeyFlags := 256+16+2;
					DbifOpenFiles[LongInt(FileHandle)]^.BtrvDataBuf.IndexBuf.KeySpecs[KeyNo].NotUsed2 := 0;
					Case DbifFieldList^[DbifOpenFiles[LongInt(FileHandle)]^.FileIndex,FieldCounter].FieldType Of
						DbifInteger  	 : DbifOpenFiles[LongInt(FileHandle)]^.BtrvDataBuf.IndexBuf.KeySpecs[KeyNo].ExtKeyType := 1;
						DbifMnemonic 	 : DbifOpenFiles[LongInt(FileHandle)]^.BtrvDataBuf.IndexBuf.KeySPecs[KeyNo].ExtKeyType := 10;
						DbifString   	 : DbifOpenFiles[LongInt(FileHandle)]^.BtrvDataBuf.IndexBuf.KeySpecs[KeyNo].ExtKeyType := 10;
						DbifText     	 : DbifOpenFiles[LongInt(FileHandle)]^.BtrvDataBuf.IndexBuf.KeySpecs[KeyNo].ExtKeyType := 11;
						DbifDate     	 : DbifOpenFiles[LongInt(FileHandle)]^.BtrvDataBuf.IndexBuf.KeySpecs[KeyNo].ExtKeyType := 3;
						DbifTime     	 : DbifOpenFiles[LongInt(FileHandle)]^.BtrvDataBuf.IndexBuf.KeySpecs[KeyNo].ExtKeyType := 4;
						DbifAutoUser 	 : DbifOpenFiles[LongInt(FileHandle)]^.BtrvDataBuf.IndexBuf.KeySpecs[KeyNo].ExtKeyType := 10;
						DbifAutoDate 	 : DbifOpenFiles[LongInt(FileHandle)]^.BtrvDataBuf.IndexBuf.KeySpecs[KeyNo].ExtKeyType := 3;
						DbifAutoTime 	 : DbifOpenFiles[LongInt(FileHandle)]^.BtrvDataBuf.IndexBuf.KeySpecs[KeyNo].ExtKeyType := 4;
						DbifAutoArchive : DbifOpenFiles[LongInt(FileHandle)]^.BtrvDataBuf.IndexBuf.KeySpecs[KeyNo].ExtKeyType := 10;
						DbifAutoSession : DbifOpenFiles[LongInt(FileHandle)]^.BtrvDataBuf.IndexBuf.KeySpecs[KeyNo].ExtKeyType := 10;
					End;
					DbifOpenFiles[LongInt(FileHandle)]^.BtrvDataBuf.IndexBuf.KeySpecs[KeyNo].NulVal := 0;
					DbifOpenFiles[LongInt(FileHandle)]^.BtrvDataBuf.IndexBuf.KeySpecs[KeyNo].Reserved2 := 0;
					KeyNo := Succ(KeyNo);
				End;
		End;
	{Setup DBLen};
	DbifOpenFiles[LongInt(FileHandle)]^.BtrvDBLen := 16 + (16 * NoOfKeys);
	{Call Btrieve to create the index}
	BtrvErrorCode := Btrv(31,
		DbifOpenFiles[LongInt(FileHandle)]^.BtrvPos.Start,
		DbifOpenFiles[LongInt(FileHandle)]^.BtrvDataBuf.Start,
		DbifOpenFiles[LongInt(FileHandle)]^.BtrvDBLen,
		DbifOpenFiles[LongInt(FileHandle)]^.BtrvKeyBuf.Start,
		255,
		0);
End;


Procedure DbifDeleteFile;

Var
	BtrvErrorCode 	: Word;
	F 					: File;
	Waste 			: Word;

Begin
	DbifErrorCode := 0;
   {Validate the filehandle}
	If (LongInt(FileHandle) < 1) Or (LongInt(FileHandle) > DbifMaxFiles) Then
		Begin
			DbifSetError(Err_Dbif+8);
			Exit
		End;
	{Check to make sure the file is open}
	If Not DbifOpenFiles[LongInt(FileHandle)]^.OpenFlag Then
		Begin
			DbifSetError(Err_Dbif+9);
			Exit
		End;
	{Physically close the file if physically open}
	If DbifOpenFiles[LongInt(FileHandle)]^.PhysOpenFlag Then
		Begin
			BtrvErrorCode := Btrv(1,
				DbifOpenFiles[LongInt(FileHandle)]^.BtrvPos.Start,
				DbifOpenFiles[LongInt(FileHandle)]^.BtrvDataBuf.Start,
				DbifOpenFiles[LongInt(FileHandle)]^.BtrvDBLen,
				DbifOpenFiles[LongInt(FileHandle)]^.BtrvKeyBuf.Start,
				255,
				0);
			If BtrvErrorCode > 0 Then
				Begin
					DbifSetError(Err_Btrieve+BtrvErrorCode);
					Exit
				End
		End;
	{Close the file}
	DbifOpenFiles[LongInt(FileHandle)]^.OpenFlag := False;
	{Erase file from new data dict}
	If DbifOpenFiles[LongInt(FileHandle)]^.FileIndex = DbifTotFileEntries Then
		Dec(DbifTotFileEntries)
	Else
		Begin
			DbifFileList^[DbifOpenFiles[LongInt(FileHandle)]^.FileIndex] := DbifFileList^[DbifTotFileEntries];
			DbifFieldList^[DbifOpenFiles[LongInt(FileHandle)]^.FileIndex] := DbifFieldList^[DbifTotFileEntries];
			DbifTotFieldEntries[DbifOpenFiles[LongInt(FileHandle)]^.FileIndex] := DbifTotFieldEntries[DbifTotFileEntries];
			Dec(DbifTotFileEntries)
		End;
	RefreshDict;
	{Physically delete the file}
	Assign(F,DbifOpenFiles[LongInt(FileHandle)]^.FileName+'.DAT');
	Erase(F);
	Waste := IOResult;
End;


Procedure DbifCloseFile;

Var
	BtrvErrorCode : Word;
	F             : File;
	Waste			  : Word;

Begin
	DbifErrorCode := 0;
	{Make sure file handle is valid}
	If (LongInt(FileHandle) < 1) Or (LongInt(FileHandle) > DbifMaxFiles) Then
		Begin
			DbifSetError(Err_Dbif+8);
			Exit
		End;
	{Check to make sure the file is open}
	If Not DbifOpenFiles[LongInt(FileHandle)]^.OpenFlag Then
		Begin
			DbifSetError(Err_Dbif+9);
			Exit
		End;
	{Physically close the file if physically open}
	{Close the file}
	Dec(DbifOpenFiles[LongInt(FileHandle)]^.OpenCount);
	If DbifOpenFiles[LongInt(FileHandle)]^.OpenCount = 0 Then
		Begin
			If DbifOpenFiles[LongInt(FileHandle)]^.PhysOpenFlag Then
				Begin
					BtrvErrorCode := Btrv(1,
						DbifOpenFiles[LongInt(FileHandle)]^.BtrvPos.Start,
						DbifOpenFiles[LongInt(FileHandle)]^.BtrvDataBuf.Start,
						DbifOpenFiles[LongInt(FileHandle)]^.BtrvDBLen,
						DbifOpenFiles[LongInt(FileHandle)]^.BtrvKeyBuf.Start,
						255,
						0);
					If BtrvErrorCode > 0 Then
						Begin
							DbifSetError(Err_Btrieve+BtrvErrorCode);
							Exit
						End
				End;
			DbifOpenFiles[LongInt(FileHandle)]^.OpenFlag := False;
		End;
	{Delete all datadict references and the file if tempflag is set}
	If DbifOpenFiles[LongInt(FileHandle)]^.TempFlag Then
		Begin
			{Erase file from new data dict}
			If DbifOpenFiles[LongInt(FileHandle)]^.FileIndex = DbifTotFileEntries Then
				Dec(DbifTotFileEntries)
			Else
				Begin
					DbifFileList^[DbifOpenFiles[LongInt(FileHandle)]^.FileIndex] := DbifFileList^[DbifTotFileEntries];
					DbifFieldList^[DbifOpenFiles[LongInt(FileHandle)]^.FileIndex] := DbifFieldList^[DbifTotFileEntries];
					DbifTotFieldEntries[DbifOpenFiles[LongInt(FileHandle)]^.FileIndex] := DbifTotFieldEntries[DbifTotFileEntries];
					Dec(DbifTotFileEntries)
				End;
			RefreshDict;
			{Physically delete the file}
			Assign(F,DbifOpenFiles[LongInt(FileHandle)]^.FileName+'.DAT');
			Erase(F);
			Waste := IOResult;
		End;
End;


Function DbifGetRecordCount(FileHandle:DbifFileHandle):LongInt;

Var
	BtrvErrorCode 	: Word;
	DBLen 			: Word;
	Found 			: Boolean;

Begin
	DbifErrorCode := 0;
	{Validate file handle}
	If (LongInt(FileHandle) < 1) Or (LongInt(FileHandle) > DbifMaxFiles) Then
		Begin
			DbifSetError(Err_Dbif+8);
			Exit
		End;
	{Check to make sure the file is open}
	If Not DbifOpenFiles[LongInt(FileHandle)]^.OpenFlag Then
		Begin
			DbifSetError(Err_Dbif+9);
			Exit
		End;
	BtrvOpenFile(FileHandle);
	DbifOpenFiles[LongInt(FileHandle)]^.BtrvDBLen := DbifMaxRecordLength;
	BtrvErrorCode := Btrv(15,
		DbifOpenFiles[LongInt(FileHandle)]^.BtrvPos.Start,
		DbifOpenFiles[LongInt(FileHandle)]^.BtrvDataBuf.Start,
		DbifOpenFiles[LongInt(FileHandle)]^.BtrvDBLen,
		DbifOpenFiles[LongInt(FileHandle)]^.BtrvKeyBuf.Start,
		255,
		0);
	If BtrvErrorCode > 0 Then
		Begin
			DbifSetError(Err_Btrieve+BtrvErrorCode);
			Exit;
		End;
	DbifGetRecordCount := DbifOpenFiles[LongInt(FileHandle)]^.BtrvDataBuf.CreateBuf.FileSpec.RecCount;
End;


Function DbifGetRecordLength;

Begin
	DbifErrorCode := 0;
   {Validate file handle}
	If (LongInt(FileHandle) < 1) Or (LongInt(FileHandle) > DbifMaxFiles) Then
		Begin
			DbifSetError(Err_Dbif+8);
			Exit
		End;
	{Check to make sure the file is open}
	If Not DbifOpenFiles[LongInt(FileHandle)]^.OpenFlag Then
		Begin
			DbifSetError(Err_Dbif+9);
			Exit
		End;
	DbifGetRecordLength := DbifOpenFiles[LongInt(FileHandle)]^.DBLen;
End;


Function DbifNewRecord;

Var
	RecordCounter : 0..DbifMaxRecBufs;

Begin
	DbifErrorCode := 0;
   {Make sure file handle is valid}
	If (LongInt(FileHandle) < 1) Or (LongInt(FileHandle) > DbifMaxFiles) Then
		Begin
			DbifSetError(Err_Dbif+8);
			Exit
		End;
	{Check to make sure the file is open}
	If Not DbifOpenFiles[LongInt(FileHandle)]^.OpenFlag Then
		Begin
			DbifSetError(Err_Dbif+9);
			Exit
		End;
	{Find an available record buffer and mark it in use}
	RecordCounter := 0;
	Repeat
		RecordCounter := Succ(RecordCounter)
	Until (Not DbifRecBuf[RecordCounter]^.InUse) Or (RecordCounter = DbifMaxRecBufs);
	If DbifRecBuf[RecordCounter]^.InUse Then
		Begin
			DbifSetError(Err_Dbif+5);
			Exit
		End;
	DbifRecBuf[RecordCounter]^.InUse := True;
	DbifRecBuf[RecordCounter]^.FileHandle := FileHandle;
   DbifRecBuf[RecordCounter]^.IndexNumber := DbifPrimaryIndex;
	FillChar(DbifRecBuf[RecordCounter]^.Data,SizeOf(DbifRecBuf[RecordCounter]^.Data),0);
	DbifNewRecord := DbifRecordHandle(RecordCounter)
End;


Procedure DbifCopyRecord;

Begin
	Move(DbifRecBuf[LongInt(SrcRecordHandle)]^.Data,DbifRecBuf[LongInt(DstRecordHandle)]^.Data,DbifMaxRecordLength);
End;


Procedure DbifCopyField;

Var
	SrcFileHandle 	: DbifFileHandle;
	DstFileHandle 	: DbifFileHandle;
	SrcFileIndex 	: Word;
	DstFileIndex 	: Word;

Begin
	DbifErrorCode := 0;
   SrcFileHandle := DbifRecBuf[SrcRecordHandle]^.FileHandle;
	DstFileHandle := DbifRecBuf[DstRecordHandle]^.FileHandle;
	SrcFileIndex  := DbifOpenFiles[LongInt(SrcFileHandle)]^.FileIndex;
	DstFileIndex  := DbifOpenFiles[LongInt(DstFileHandle)]^.FileIndex;
	Move(DbifRecBuf[SrcRecordHandle]^.Data[DbifFieldList^[SrcFileIndex,SrcFieldHandle].FieldPos],
		DbifRecBuf[DstRecordHandle]^.Data[DbifFieldList^[DstFileIndex,DstFieldHandle].FieldPos],
		DbifFieldList^[DstFileIndex,DstFieldHandle].FieldSize);
End;


Procedure DbifUseIndex;

Begin
	DbifRecBuf[LongInt(RecordHandle)]^.IndexNumber := IndexNumber;
End;


Procedure DbifGetRecord;

Var
	FileIndex 		: Word;
	FileHandle 		: DbifFileHandle;
	FieldHandle 	: Word;
	BtrvErrorCode 	: Word;

Begin
	DbifErrorCode := 0;
	{Validate RecordHandle}
	If Not DbifRecBuf[LongInt(RecordHandle)]^.InUse Then
		Begin
			DbifSetError(Err_Dbif+6);
			Exit
		End;
	{Validate FileHandle}
	FileHandle := DbifRecBuf[LongInt(RecordHandle)]^.FileHandle;
	If Not DbifOpenFiles[LongInt(FileHandle)]^.OpenFlag Then
		Begin
			DbifSetError(Err_Dbif+9);
			Exit
		End;
	{Make sure the file is physically open}
	BtrvOpenFile(FileHandle);
	{Set up BtrvKeyBuf}
	If DbifRecBuf[LongInt(RecordHandle)]^.IndexNumber = 0 Then
		Move(DbifRecBuf[LongInt(RecordHandle)]^.Data[0],
			DbifOpenFiles[LongInt(FileHandle)]^.BtrvKeyBuf.Key[0],
			DbifFileList^[DbifOpenFiles[LongInt(FileHandle)]^.FileIndex].KeyLen)
	Else
		Begin
			FileIndex := DbifOpenFiles[LongInt(FileHandle)]^.FileIndex;
			FieldHandle := DbifFileList^[DbifOpenFiles[LongInt(FileHandle)]^.FileIndex].AltKey;
			Move(DbifRecBuf[LongInt(RecordHandle)]^.Data[DbifFieldList^[FileIndex,FieldHandle].FieldPos],
				DbifOpenFiles[LongInt(FileHandle)]^.BtrvKeyBuf.Key[0],
				DbifFieldList^[FileIndex,FieldHandle].FieldSize);
			Move(DbifRecBuf[LongInt(RecordHandle)]^.Data[0],
				DbifOpenFiles[LongInt(FileHandle)]^.BtrvKeyBuf.Key[DbifFieldList^[FileIndex,FieldHandle].FieldSize],
				DbifFileList^[FileIndex].KeyLen)
		End;
	{Call GetEQ}
	BtrvErrorCode := Btrv(5,
		DbifOpenFiles[LongInt(FileHandle)]^.BtrvPos.Start,
		DbifRecBuf[LongInt(RecordHandle)]^.Data,
		DbifOpenFiles[LongInt(FileHandle)]^.DBLen,
		DbifOpenFiles[LongInt(FileHandle)]^.BtrvKeyBuf.Start,
		255,
		DbifRecBuf[LongInt(RecordHandle)]^.IndexNumber);
	If (BtrvErrorCode > 0) And (BtrvErrorCode <> 4) Then
		Begin
			DbifSetError(Err_Btrieve+BtrvErrorCode);
			Exit
		End;
	If BtrvErrorCode = 4 Then
		Begin
			DbifSetError(Err_Dbif+11);
			Exit
		End;
End;


Procedure DbifSearchRecordGE;

Var
	FileIndex 		: Word;
	FileHandle 		: DbifFileHandle;
	FieldHandle 	: Word;
	BtrvErrorCode 	: Word;

Begin
	DbifErrorCode := 0;
	{Validate RecordHandle}
	If Not DbifRecBuf[LongInt(RecordHandle)]^.InUse Then
		Begin
			DbifSetError(Err_Dbif+6);
			Exit
		End;
	{Validate FileHandle}
	FileHandle := DbifRecBuf[LongInt(RecordHandle)]^.FileHandle;
	If Not DbifOpenFiles[LongInt(FileHandle)]^.OpenFlag Then
		Begin
			DbifSetError(Err_Dbif+9);
			Exit
		End;
	{Make sure the file is physically open}
	BtrvOpenFile(FileHandle);
	{Set up BtrvKeyBuf}
	If DbifRecBuf[LongInt(RecordHandle)]^.IndexNumber = 0 Then
		Move(DbifRecBuf[LongInt(RecordHandle)]^.Data[0],
			DbifOpenFiles[LongInt(FileHandle)]^.BtrvKeyBuf.Key[0],
			DbifFileList^[DbifOpenFiles[LongInt(FileHandle)]^.FileIndex].KeyLen)
	Else
		Begin
			FileIndex := DbifOpenFiles[LongInt(FileHandle)]^.FileIndex;
			FieldHandle := DbifFileList^[DbifOpenFiles[LongInt(FileHandle)]^.FileIndex].AltKey;
			Move(DbifRecBuf[LongInt(RecordHandle)]^.Data[DbifFieldList^[FileIndex,FieldHandle].FieldPos],
				DbifOpenFiles[LongInt(FileHandle)]^.BtrvKeyBuf.Key[0],
				DbifFieldList^[FileIndex,FieldHandle].FieldSize);
			Move(DbifRecBuf[LongInt(RecordHandle)]^.Data[0],
				DbifOpenFiles[LongInt(FileHandle)]^.BtrvKeyBuf.Key[DbifFieldList^[FileIndex,FieldHandle].FieldSize],
				DbifFileList^[FileIndex].KeyLen)
		End;
	{Call GetGE}
	BtrvErrorCode := Btrv(9,
		DbifOpenFiles[LongInt(FileHandle)]^.BtrvPos.Start,
		DbifRecBuf[LongInt(RecordHandle)]^.Data,
		DbifOpenFiles[LongInt(FileHandle)]^.DBLen,
		DbifOpenFiles[LongInt(FileHandle)]^.BtrvKeyBuf.Start,
		255,
		DbifRecBuf[LongInt(RecordHandle)]^.IndexNumber);
	If (BtrvErrorCode > 0) And (BtrvErrorCode <> 9) Then
		Begin
			DbifSetError(Err_Btrieve+BtrvErrorCode);
			Exit
		End;
	If BtrvErrorCode = 9 Then
		Begin
			DbifSetError(Err_Dbif+11);
			Exit
		End;
End;


Procedure DbifPutRecord;

Const
	CurrentUser = 'SYSTEM';

Var
	FileIndex 		: Word;
	FileHandle 		: DbifFileHandle;
	FieldHandle 	: Word;
	FieldCounter 	: Word;
	BtrvErrorCode 	: Word;
	BtrvOpCode 		: Word;

Begin
	DbifErrorCode := 0;
	{Validate RecordHandle}
	If Not DbifRecBuf[LongInt(RecordHandle)]^.InUse Then
		Begin
			DbifSetError(Err_Dbif+6);
			Exit
		End;
	{Validate FileHandle}
	FileHandle := DbifRecBuf[LongInt(RecordHandle)]^.FileHandle;
	If Not DbifOpenFiles[LongInt(FileHandle)]^.OpenFlag Then
		Begin
			DbifSetError(Err_Dbif+9);
			Exit
		End;
	{Handle Auto fields}
	If DbifOpenFiles[LongInt(FileHandle)]^.AutoFlag Then
		For FieldCounter := 1 To DbifTotFieldEntries[DbifOpenFiles[LongInt(FileHandle)]^.FileIndex] Do
			Begin
				If DbifFieldList^[DbifOpenFiles[LongInt(FileHandle)]^.FileIndex,FieldCounter].FieldType = DbifAutoUser Then
					DbifPutFieldVal(RecordHandle,FieldCounter,CurrentUser)
				Else If DbifFieldList^[DbifOpenFiles[LongInt(FileHandle)]^.FileIndex,FieldCounter].FieldType = DbifAutoDate Then
					DbifPutFieldVal(RecordHandle,FieldCounter,CurrentDate)
				Else If DbifFieldList^[DbifOpenFiles[LongInt(FileHandle)]^.FileIndex,FieldCounter].FieldType = DbifAutoTime Then
					DbifPutFieldVal(RecordHandle,FieldCounter,CurrentTime)
				Else If DbifFieldList^[DbifOpenFiles[LongInt(FileHandle)]^.FileIndex,FieldCounter].FieldType = DbifAutoArchive Then
					DbifPutFieldVal(RecordHandle,FieldCounter,DbifAutoArchiveChar)
        Else If DbifFieldList^[DbifOpenFiles[LongInt(FileHandle)]^.FileIndex,FieldCounter].FieldType = DbifAutoSession Then
          DbifPutFieldVal(RecordHandle,FieldCounter,DbifCurrentSession);
			End;
	{Make sure the file is physically open}
	BtrvOpenFile(FileHandle);
	{Set up BtrvKeyBuf}
	If DbifRecBuf[LongInt(RecordHandle)]^.IndexNumber = 0 Then
		Move(DbifRecBuf[LongInt(RecordHandle)]^.Data[0],
			DbifOpenFiles[LongInt(FileHandle)]^.BtrvKeyBuf.Key[0],
			DbifFileList^[DbifOpenFiles[LongInt(FileHandle)]^.FileIndex].KeyLen)
	Else
		Begin
			FileIndex := DbifOpenFiles[LongInt(FileHandle)]^.FileIndex;
			FieldHandle := DbifFileList^[DbifOpenFiles[LongInt(FileHandle)]^.FileIndex].AltKey;
			Move(DbifRecBuf[LongInt(RecordHandle)]^.Data[DbifFieldList^[FileIndex,FieldHandle].FieldPos],
				DbifOpenFiles[LongInt(FileHandle)]^.BtrvKeyBuf.Key[0],
				DbifFieldList^[FileIndex,FieldHandle].FieldSize);
			Move(DbifRecBuf[LongInt(RecordHandle)]^.Data[0],
				DbifOpenFiles[LongInt(FileHandle)]^.BtrvKeyBuf.Key[DbifFieldList^[FileIndex,FieldHandle].FieldSize],
				DbifFileList^[FileIndex].KeyLen)
		End;
	{Call GetEQ}
	BtrvErrorCode := Btrv(5,
		DbifOpenFiles[LongInt(FileHandle)]^.BtrvPos.Start,
		DbifOpenFiles[LongInt(FileHandle)]^.BtrvDataBuf.Start,
		DbifOpenFiles[LongInt(FileHandle)]^.DBLen,
		DbifOpenFiles[LongInt(FileHandle)]^.BtrvKeyBuf.Start,
		255,
		DbifRecBuf[LongInt(RecordHandle)]^.IndexNumber);
	If (BtrvErrorCode > 0) And (BtrvErrorCode <> 4) Then
		Begin
			DbifSetError(Err_Btrieve+BtrvErrorCode);
			Exit
		End;
	{If Exists Call Update otherwise call Insert}
	If BtrvErrorCode = 4 Then
		BtrvOpCode := 2  {Insert}
	Else
		BtrvOpCode := 3; {Update}
	BtrvErrorCode := Btrv(BtrvOpCode,
		DbifOpenFiles[LongInt(FileHandle)]^.BtrvPos.Start,
		DbifRecBuf[LongInt(RecordHandle)]^.Data,
		DbifOpenFiles[LongInt(FileHandle)]^.DBLen,
		DbifOpenFiles[LongInt(FileHandle)]^.BtrvKeyBuf.Start,
		255,
		DbifRecBuf[LongInt(RecordHandle)]^.IndexNumber);
	If BtrvErrorCode > 0 Then
		Begin
			DbifSetError(Err_Btrieve+BtrvErrorCode);
			Exit
		End;
End;


Procedure DbifDelRecord;

Var
	FileIndex 		: Word;
	FileHandle 		: DbifFileHandle;
	FieldHandle 	: Word;
	BtrvErrorCode 	: Word;

Begin
	DbifErrorCode := 0;
	{Validate RecordHandle}
	If Not DbifRecBuf[LongInt(RecordHandle)]^.InUse Then
		Begin
			DbifSetError(Err_Dbif+6);
			Exit
		End;
	{Validate FileHandle}
	FileHandle := DbifRecBuf[LongInt(RecordHandle)]^.FileHandle;
	If Not DbifOpenFiles[LongInt(FileHandle)]^.OpenFlag Then
		Begin
			DbifSetError(Err_Dbif+9);
			Exit
		End;
	{Make sure the file is physically open}
	BtrvOpenFile(FileHandle);
	{Set up BtrvKeyBuf}
	If DbifRecBuf[LongInt(RecordHandle)]^.IndexNumber = 0 Then
		Move(DbifRecBuf[LongInt(RecordHandle)]^.Data[0],
			DbifOpenFiles[LongInt(FileHandle)]^.BtrvKeyBuf.Key[0],
			DbifFileList^[DbifOpenFiles[LongInt(FileHandle)]^.FileIndex].KeyLen)
	Else
		Begin
			FileIndex := DbifOpenFiles[LongInt(FileHandle)]^.FileIndex;
			FieldHandle := DbifFileList^[DbifOpenFiles[LongInt(FileHandle)]^.FileIndex].AltKey;
			Move(DbifRecBuf[LongInt(RecordHandle)]^.Data[DbifFieldList^[FileIndex,FieldHandle].FieldPos],
				DbifOpenFiles[LongInt(FileHandle)]^.BtrvKeyBuf.Key[0],
				DbifFieldList^[FileIndex,FieldHandle].FieldSize);
			Move(DbifRecBuf[LongInt(RecordHandle)]^.Data[0],
				DbifOpenFiles[LongInt(FileHandle)]^.BtrvKeyBuf.Key[DbifFieldList^[FileIndex,FieldHandle].FieldSize],
				DbifFileList^[FileIndex].KeyLen)
		End;
	{Call GetEQ}
	BtrvErrorCode := Btrv(5,
		DbifOpenFiles[LongInt(FileHandle)]^.BtrvPos.Start,
		DbifOpenFiles[LongInt(FileHandle)]^.BtrvDataBuf.Start,
		DbifOpenFiles[LongInt(FileHandle)]^.DBLen,
		DbifOpenFiles[LongInt(FileHandle)]^.BtrvKeyBuf.Start,
		255,
		DbifRecBuf[LongInt(RecordHandle)]^.IndexNumber);
	If (BtrvErrorCode > 0) And (BtrvErrorCode <> 4) Then
		Begin
			DbifSetError(Err_Btrieve+BtrvErrorCode);
			Exit
		End;
	{Return error if record does not exist}
	If BtrvErrorCode = 4 Then
		Begin
			DbifSetError(Err_Dbif+11);
			Exit
		End;
	BtrvErrorCode := Btrv(4,
		DbifOpenFiles[LongInt(FileHandle)]^.BtrvPos.Start,
		DbifRecBuf[LongInt(RecordHandle)]^.Data,
		DbifOpenFiles[LongInt(FileHandle)]^.DBLen,
		DbifOpenFiles[LongInt(FileHandle)]^.BtrvKeyBuf.Start,
		255,
		DbifRecBuf[LongInt(RecordHandle)]^.IndexNumber);
	If BtrvErrorCode > 0 Then
		Begin
			DbifSetError(Err_Btrieve+BtrvErrorCode);
			Exit
		End;
End;


Procedure DbifCloseRecord;

Begin
	DbifErrorCode := 0;
	{Make sure record is open}
	If DbifRecBuf[LongInt(RecordHandle)]^.InUse Then
		DbifRecBuf[LongInt(RecordHandle)]^.InUse := False
	Else
		Begin
			DbifSetError(Err_Dbif+6);
			Exit
		End;
End;


Procedure DbifFirstRecord;

Var
	FileHandle 		: DbifFileHandle;
	BtrvErrorCode 	: Word;

Begin
	DbifErrorCode := 0;
	{Validate RecordHandle}
	If Not DbifRecBuf[LongInt(RecordHandle)]^.InUse Then
		Begin
			DbifSetError(Err_Dbif+6);
			Exit
		End;
	{Validate FileHandle}
	FileHandle := DbifRecBuf[LongInt(RecordHandle)]^.FileHandle;
	If Not DbifOpenFiles[LongInt(FileHandle)]^.OpenFlag Then
		Begin
			DbifSetError(Err_Dbif+9);
			Exit
		End;
	{Make sure the file is physically open}
	BtrvOpenFile(FileHandle);
	{Call GetFirst}
	BtrvErrorCode := Btrv(12,
		DbifOpenFiles[LongInt(FileHandle)]^.BtrvPos.Start,
		DbifRecBuf[LongInt(RecordHandle)]^.Data,
		DbifOpenFiles[LongInt(FileHandle)]^.DBLen,
		DbifOpenFiles[LongInt(FileHandle)]^.BtrvKeyBuf.Start,
		255,
		DbifRecBuf[LongInt(RecordHandle)]^.IndexNumber);
	If (BtrvErrorCode > 0) And (BtrvErrorCode <> 9) Then
		Begin
			DbifSetError(Err_Btrieve+BtrvErrorCode);
			Exit
		End;
	If BtrvErrorCode = 9 Then
		Begin
			DbifSetError(Err_Dbif+12);
			Exit
		End;
End;


Procedure DbifLastRecord;

Var
	FileHandle 		: DbifFileHandle;
	BtrvErrorCode 	: Word;

Begin
	DbifErrorCode := 0;
	{Validate RecordHandle}
	If Not DbifRecBuf[LongInt(RecordHandle)]^.InUse Then
		Begin
			DbifSetError(Err_Dbif+6);
			Exit
		End;
	{Validate FileHandle}
	FileHandle := DbifRecBuf[LongInt(RecordHandle)]^.FileHandle;
	If Not DbifOpenFiles[LongInt(FileHandle)]^.OpenFlag Then
		Begin
			DbifSetError(Err_Dbif+9);
			Exit
		End;
	{Make sure the file is physically open}
	BtrvOpenFile(FileHandle);
	{Call GetLast}
	BtrvErrorCode := Btrv(13,
		DbifOpenFiles[LongInt(FileHandle)]^.BtrvPos.Start,
		DbifRecBuf[LongInt(RecordHandle)]^.Data,
		DbifOpenFiles[LongInt(FileHandle)]^.DBLen,
		DbifOpenFiles[LongInt(FileHandle)]^.BtrvKeyBuf.Start,
		255,
		DbifRecBuf[LongInt(RecordHandle)]^.IndexNumber);
	If (BtrvErrorCode > 0) And (BtrvErrorCode <> 9) Then
		Begin
			DbifSetError(Err_Btrieve+BtrvErrorCode);
			Exit
		End;
	If BtrvErrorCode = 9 Then
		Begin
			DbifSetError(Err_Dbif+12);
			Exit
		End;
End;


Procedure DbifNextRecord;

Var
	FileIndex 		: Word;
	FileHandle 		: DbifFileHandle;
	FieldHandle 	: Word;
	BtrvErrorCode 	: Word;

Begin
	DbifErrorCode := 0;
	{Validate RecordHandle}
	If Not DbifRecBuf[LongInt(RecordHandle)]^.InUse Then
		Begin
			DbifSetError(Err_Dbif+6);
			Exit
		End;
	{Validate FileHandle}
	FileHandle := DbifRecBuf[LongInt(RecordHandle)]^.FileHandle;
	If Not DbifOpenFiles[LongInt(FileHandle)]^.OpenFlag Then
		Begin
			DbifSetError(Err_Dbif+9);
			Exit
		End;
	{Make sure the file is physically open}
	BtrvOpenFile(FileHandle);
	{Set up BtrvKeyBuf}
	If DbifRecBuf[LongInt(RecordHandle)]^.IndexNumber = 0 Then
		Move(DbifRecBuf[LongInt(RecordHandle)]^.Data[0],
			DbifOpenFiles[LongInt(FileHandle)]^.BtrvKeyBuf.Key[0],
			DbifFileList^[DbifOpenFiles[LongInt(FileHandle)]^.FileIndex].KeyLen)
	Else
		Begin
			FileIndex := DbifOpenFiles[LongInt(FileHandle)]^.FileIndex;
			FieldHandle := DbifFileList^[DbifOpenFiles[LongInt(FileHandle)]^.FileIndex].AltKey;
			Move(DbifRecBuf[LongInt(RecordHandle)]^.Data[DbifFieldList^[FileIndex,FieldHandle].FieldPos],
				DbifOpenFiles[LongInt(FileHandle)]^.BtrvKeyBuf.Key[0],
				DbifFieldList^[FileIndex,FieldHandle].FieldSize);
			Move(DbifRecBuf[LongInt(RecordHandle)]^.Data[0],
				DbifOpenFiles[LongInt(FileHandle)]^.BtrvKeyBuf.Key[DbifFieldList^[FileIndex,FieldHandle].FieldSize],
				DbifFileList^[FileIndex].KeyLen)
		End;
	{Call GetEQ}
	BtrvErrorCode := Btrv(5,
		DbifOpenFiles[LongInt(FileHandle)]^.BtrvPos.Start,
		DbifOpenFiles[LongInt(FileHandle)]^.BtrvDataBuf.Start,
		DbifOpenFiles[LongInt(FileHandle)]^.DBLen,
		DbifOpenFiles[LongInt(FileHandle)]^.BtrvKeyBuf.Start,
		255,
		DbifRecBuf[LongInt(RecordHandle)]^.IndexNumber);
	If (BtrvErrorCode > 0) And (BtrvErrorCode <> 4) Then
		Begin
			DbifSetError(Err_Btrieve+BtrvErrorCode);
			Exit
		End;
	{If it doesn't exist call GetGE(9) otherwise call GetNext(6)}
	If BtrvErrorCode = 4 Then
		BtrvErrorCode := Btrv(9,
			DbifOpenFiles[LongInt(FileHandle)]^.BtrvPos.Start,
			DbifRecBuf[LongInt(RecordHandle)]^.Data,
			DbifOpenFiles[LongInt(FileHandle)]^.DBLen,
			DbifOpenFiles[LongInt(FileHandle)]^.BtrvKeyBuf.Start,
			255,
			DbifRecBuf[LongInt(RecordHandle)]^.IndexNumber)
	Else
		BtrvErrorCode := Btrv(6,
			DbifOpenFiles[LongInt(FileHandle)]^.BtrvPos.Start,
			DbifRecBuf[LongInt(RecordHandle)]^.Data,
			DbifOpenFiles[LongInt(FileHandle)]^.DBLen,
			DbifOpenFiles[LongInt(FileHandle)]^.BtrvKeyBuf.Start,
			255,
			DbifRecBuf[LongInt(RecordHandle)]^.IndexNumber);
	If (BtrvErrorCode > 0) And (BtrvErrorCode <> 9) Then
		Begin
			DbifSetError(Err_Btrieve+BtrvErrorCode);
			Exit
		End;
	If BtrvErrorCode = 9 Then
		Begin
			DbifSetError(Err_Dbif+12);
			Exit
		End;
End;


Procedure DbifPrevRecord;

Var
	FileIndex 		: Word;
	FileHandle 		: DbifFileHandle;
	FieldHandle 	: Word;
	BtrvErrorCode 	: Word;

Begin
	DbifErrorCode := 0;
	{Validate RecordHandle}
	If Not DbifRecBuf[LongInt(RecordHandle)]^.InUse Then
		Begin
			DbifSetError(Err_Dbif+6);
			Exit
		End;
	{Validate FileHandle}
	FileHandle := DbifRecBuf[LongInt(RecordHandle)]^.FileHandle;
	If Not DbifOpenFiles[LongInt(FileHandle)]^.OpenFlag Then
		Begin
			DbifSetError(Err_Dbif+9);
			Exit
		End;
	{Make sure the file is physically open}
	BtrvOpenFile(FileHandle);
	{Set up BtrvKeyBuf}
	If DbifRecBuf[LongInt(RecordHandle)]^.IndexNumber = 0 Then
		Move(DbifRecBuf[LongInt(RecordHandle)]^.Data[0],
			DbifOpenFiles[LongInt(FileHandle)]^.BtrvKeyBuf.Key[0],
			DbifFileList^[DbifOpenFiles[LongInt(FileHandle)]^.FileIndex].KeyLen)
	Else
		Begin
			FileIndex := DbifOpenFiles[LongInt(FileHandle)]^.FileIndex;
			FieldHandle := DbifFileList^[DbifOpenFiles[LongInt(FileHandle)]^.FileIndex].AltKey;
			Move(DbifRecBuf[LongInt(RecordHandle)]^.Data[DbifFieldList^[FileIndex,FieldHandle].FieldPos],
				DbifOpenFiles[LongInt(FileHandle)]^.BtrvKeyBuf.Key[0],
				DbifFieldList^[FileIndex,FieldHandle].FieldSize);
			Move(DbifRecBuf[LongInt(RecordHandle)]^.Data[0],
				DbifOpenFiles[LongInt(FileHandle)]^.BtrvKeyBuf.Key[DbifFieldList^[FileIndex,FieldHandle].FieldSize],
				DbifFileList^[FileIndex].KeyLen)
		End;
	{Call GetEQ}
	BtrvErrorCode := Btrv(5,
		DbifOpenFiles[LongInt(FileHandle)]^.BtrvPos.Start,
		DbifOpenFiles[LongInt(FileHandle)]^.BtrvDataBuf.Start,
		DbifOpenFiles[LongInt(FileHandle)]^.DBLen,
		DbifOpenFiles[LongInt(FileHandle)]^.BtrvKeyBuf.Start,
		255,
		DbifRecBuf[LongInt(RecordHandle)]^.IndexNumber);
	If (BtrvErrorCode > 0) And (BtrvErrorCode <> 4) Then
		Begin
			DbifSetError(Err_Btrieve+BtrvErrorCode);
			Exit
		End;
	{If Exists Call Update otherwise call Insert}
	If BtrvErrorCode = 4 Then
		Begin
			DbifSetError(Err_Dbif+11);
			Exit
		End;
	BtrvErrorCode := Btrv(7,
		DbifOpenFiles[LongInt(FileHandle)]^.BtrvPos.Start,
		DbifRecBuf[LongInt(RecordHandle)]^.Data,
		DbifOpenFiles[LongInt(FileHandle)]^.DBLen,
		DbifOpenFiles[LongInt(FileHandle)]^.BtrvKeyBuf.Start,
		255,
		DbifRecBuf[LongInt(RecordHandle)]^.IndexNumber);
	If (BtrvErrorCode > 0) And (BtrvErrorCode <> 9) Then
		Begin
			DbifSetError(Err_Btrieve+BtrvErrorCode);
			Exit
		End;
	If BtrvErrorCode = 9Then
		Begin
			DbifSetError(Err_Dbif+12);
			Exit
		End;
End;


Function DbifFHofRH;

Begin
	DbifFHOfRH:= DbifRecBuf[LongInt(RecordHandle)]^.FileHandle;
End;


Function DbifGetFileCount;

Begin
	DbifGetFileCount := DbifTotFileEntries;
End;


Function DbifGetFileName;

Begin
	DbifGetFileName := DbifFileList^[FileNumber].FileName;
End;


Function DbifGetFileDesc;

Begin
	DbifGetFileDesc := DbifFileList^[FileNumber].FileDesc;
End;


Function DbifGetFileUDFlag;

Begin
	DbifGetFileUDFlag := DbifFileList^[FileNumber].UDFlag;
End;


Function DbifGetFieldCount;

Begin
	DbifErrorCode := 0;
	{Make sure file handle is valid}
	If (LongInt(FileHandle) < 1) Or (LongInt(FileHandle) > DbifMaxFiles) Then
		Begin
			DbifSetError(Err_Dbif+8);
			Exit
		End;
	{Check to make sure the file is open}
	If Not DbifOpenFiles[LongInt(FileHandle)]^.OpenFlag Then
		Begin
			DbifSetError(Err_Dbif+9);
			Exit
		End;
	{Set result to FieldCount}
	DbifGetFieldCount := DbifTotFieldEntries[DbifOpenFiles[LongInt(FileHandle)]^.FileIndex];
End;


Function DbifGetFieldName;

Begin
	DbifErrorCode := 0;
	{Make sure file handle is valid}
	If (LongInt(FileHandle) < 1) Or (LongInt(FileHandle) > DbifMaxFiles) Then
		Begin
			DbifSetError(Err_Dbif+8);
			Exit
		End;
	{Check to make sure the file is open}
	If Not DbifOpenFiles[LongInt(FileHandle)]^.OpenFlag Then
		Begin
			DbifSetError(Err_Dbif+9);
			Exit
		End;
	{Check to make sure FieldHandle is valid}
	If (FieldHandle < 1) Or (FieldHandle > DbifTotFieldEntries[DbifOpenFiles[LongInt(FileHandle)]^.FileIndex]) Then
		Begin
			DbifSetError(Err_Dbif+7);
			Exit
		End;
	DbifGetFieldName := DbifFieldList^[DbifOpenFiles[LongInt(FileHandle)]^.FileIndex,FieldHandle].FieldName;
End;


Function DbifGetFieldType;

Begin
	DbifErrorCode := 0;
	{Make sure file handle is valid}
	If (LongInt(FileHandle) < 1) Or (LongInt(FileHandle) > DbifMaxFiles) Then
		Begin
			DbifSetError(Err_Dbif+8);
			Exit
		End;
	{Check to make sure the file is open}
	If Not DbifOpenFiles[LongInt(FileHandle)]^.OpenFlag Then
		Begin
			DbifSetError(Err_Dbif+9);
			Exit
		End;
	{Check to make sure FieldHandle is valid}
	If (FieldHandle < 1) Or (FieldHandle > DbifTotFieldEntries[DbifOpenFiles[LongInt(FileHandle)]^.FileIndex]) Then
		Begin
			DbifSetError(Err_Dbif+7);
			Exit
		End;
	DbifGetFieldType := DbifFieldList^[DbifOpenFiles[LongInt(FileHandle)]^.FileIndex,FieldHandle].FieldType;
End;


Function DbifGetFieldSize;

Begin
	DbifErrorCode := 0;
	{Make sure file handle is valid}
	If (LongInt(FileHandle) < 1) Or (LongInt(FileHandle) > DbifMaxFiles) Then
		Begin
			DbifSetError(Err_Dbif+8);
			Exit
		End;
	{Check to make sure the file is open}
	If Not DbifOpenFiles[LongInt(FileHandle)]^.OpenFlag Then
		Begin
			DbifSetError(Err_Dbif+9);
			Exit
		End;
	{Check to make sure FieldHandle is valid}
	If (FieldHandle < 1) Or (FieldHandle > DbifTotFieldEntries[DbifOpenFiles[LongInt(FileHandle)]^.FileIndex]) Then
		Begin
			DbifSetError(Err_Dbif+7);
			Exit
		End;
	DbifGetFieldSize := DbifFieldList^[DbifOpenFiles[LongInt(FileHandle)]^.FileIndex,FieldHandle].FieldSize;
End;


Function DbifGetFieldKeyFlag;

Begin
	DbifErrorCode := 0;
	{Make sure file handle is valid}
	If (LongInt(FileHandle) < 1) Or (LongInt(FileHandle) > DbifMaxFiles) Then
		Begin
			DbifSetError(Err_Dbif+8);
			Exit
		End;
	{Check to make sure the file is open}
	If Not DbifOpenFiles[LongInt(FileHandle)]^.OpenFlag Then
		Begin
			DbifSetError(Err_Dbif+9);
			Exit
		End;
	{Check to make sure FieldHandle is valid}
	If (FieldHandle < 1) Or (FieldHandle > DbifTotFieldEntries[DbifOpenFiles[LongInt(FileHandle)]^.FileIndex]) Then
		Begin
			DbifSetError(Err_Dbif+7);
			Exit
		End;
	DbifGetFieldKeyFlag := DbifFieldList^[DbifOpenFiles[LongInt(FileHandle)]^.FileIndex,FieldHandle].KeyFlag;
End;


Function DbifGetFieldUDFlag;

Begin
	DbifErrorCode := 0;
	{Make sure file handle is valid}
	If (LongInt(FileHandle) < 1) Or (LongInt(FileHandle) > DbifMaxFiles) Then
		Begin
			DbifSetError(Err_Dbif+8);
			Exit
		End;
	{Check to make sure the file is open}
	If Not DbifOpenFiles[LongInt(FileHandle)]^.OpenFlag Then
		Begin
			DbifSetError(Err_Dbif+9);
			Exit
		End;
	{Check to make sure FieldHandle is valid}
	If (FieldHandle < 1) Or (FieldHandle > DbifTotFieldEntries[DbifOpenFiles[LongInt(FileHandle)]^.FileIndex]) Then
		Begin
			DbifSetError(Err_Dbif+7);
			Exit
		End;
	DbifGetFieldUDFlag := DbifFieldList^[DbifOpenFiles[LongInt(FileHandle)]^.FileIndex,FieldHandle].UDFlag;
End;


(*Procedure DbifAddField;*)

(*Var*)
(*  FieldCounter       : Word;*)
(*  Waste              : Word;*)
(*  TempFileHandle     : Word;*)
(*  RecLen             : Word;*)
(*  NoOfKeys           : Word;*)
(*  FieldPos           : Word;*)
(*  KeyNo              : Word;*)
(*  LastFieldNo        : Word;*)
(*  BtrvErrorCode      : Word;*)
(*  CloseErrorCode     : Word;*)
(*  MnemonicFileHandle : DbifFileHandle;*)
(*  f                  : File;*)
(*  MnemText : ARRAY[0..32] OF CHAR;*)
(*  mnFileObj : PMnemFileObj;*)
(*  mnFileName : String;*)

(*Begin*)
(*  DbifErrorCode := 0;*)
(*  {Make sure file handle is valid}*)
(*  If (LongInt(FileHandle) < 1) Or (LongInt(FileHandle) > DbifMaxFiles) Then*)
(*    Begin*)
(*      DbifSetError(Err_Dbif+8);*)
(*      Exit*)
(*    End;*)
(*  {Check to make sure the file is open}*)
(*  If Not DbifOpenFiles[LongInt(FileHandle)]^.OpenFlag Then*)
(*    Begin*)
(*      DbifSetError(Err_Dbif+9);*)
(*      Exit*)
(*    End;*)
(*  {Make sure file is open in Exclusive mode}*)
(*  If DbifOpenFiles[LongInt(FileHandle)]^.FileMode <> DbifExclusive Then*)
(*    Begin*)
(*      DbifSetError(Err_Dbif+23);*)
(*      Exit*)
(*    End;*)

(*  { Get the real mnemonic file name from MNEMFILE.DAT }*)
(*  IF FldType = DbifMnemonic THEN*)
(*    BEGIN*)
(*      mnFileObj := New(PMnemFileObj,Init);*)
(*      mnFileobj^.GetFileName(FldName,mnFileName);*)
(*      Dispose(mnFileObj,Done);*)
(*    END;*)

(*  {Validate fldname as a valid filename if Mnemonic}*)
(*  If (FldType = DbifMnemonic) And (Not DbifValidFileName(mnFileName)) Then*)
(*    Begin*)
(*      DbifSetError(Err_Dbif+24);*)
(*      Exit*)
(*    End;*)
(*  {Close the file is physically open}*)
(*  If DbifOpenFiles[LongInt(FileHandle)]^.PhysOpenFlag Then*)
(*    Begin*)
(*      CloseErrorCode := Btrv(1,*)
(*        DbifOpenFiles[LongInt(FileHandle)]^.BtrvPos.Start,*)
(*        DbifOpenFiles[LongInt(FileHandle)]^.BtrvDataBuf.Start,*)
(*        DbifOpenFiles[LongInt(FileHandle)]^.BtrvDBLen,*)
(*        DbifOpenFiles[LongInt(FileHandle)]^.BtrvKeyBuf.Start,*)
(*        255,*)
(*        0);*)
(*      If CloseErrorCode > 0 Then*)
(*        Begin*)
(*          DbifSetError(Err_Dbif+22);*)
(*          Exit*)
(*        End;*)
(*      DbifOpenFiles[LongInt(FileHandle)]^.PhysOpenFlag := False;*)
(*    End;*)
(*  {Make sure field does not already exist}*)
(*  FieldCounter := 1;*)
(*  While (FieldCounter <= DbifTotFieldEntries[DbifOpenFiles[LongInt(FileHandle)]^.FileIndex]) And*)
(*    (Upper(DbifFieldList^[DbifOpenFiles[LongInt(FileHandle)]^.FileIndex,FieldCounter].FieldName) <> Upper(FldName)) Do*)
(*      Inc(FieldCounter);*)
(*  If FieldCounter <= DbifTotFieldEntries[DbifOpenFiles[LongInt(FileHandle)]^.FileIndex] Then*)
(*    Begin*)
(*      DbifSetError(Err_Dbif + 2);*)
(*      Exit;*)
(*    End;*)
(*  {Find last field number for this file}*)
(*  LastFieldNo := DbifTotFieldEntries[DbifOpenFiles[LongInt(FileHandle)]^.FileIndex];*)
(*  {Add field to dictionary}*)
(*  Inc(DbifTotFieldEntries[DbifOpenFiles[LongInt(FileHandle)]^.FileIndex]);*)
(*  DbifFieldList^[DbifOpenFiles[LongInt(FileHandle)]^.FileIndex,*)
(*    DbifTotFieldEntries[DbifOpenFiles[LongInt(FileHandle)]^.FileIndex]].FileName :=*)
(*    DbifOpenFiles[LongInt(FileHandle)]^.FileName;*)
(*  DbifFieldList^[DbifOpenFiles[LongInt(FileHandle)]^.FileIndex,*)
(*    DbifTotFieldEntries[DbifOpenFiles[LongInt(FileHandle)]^.FileIndex]].FieldNumber :=*)
(*    Succ(LastFieldNo);*)
(*  DbifFieldList^[DbifOpenFiles[LongInt(FileHandle)]^.FileIndex,*)
(*    DbifTotFieldEntries[DbifOpenFiles[LongInt(FileHandle)]^.FileIndex]].KeyFlag :=*)
(*    KeyFlag;*)
(*  DbifFieldList^[DbifOpenFiles[LongInt(FileHandle)]^.FileIndex,*)
(*    DbifTotFieldEntries[DbifOpenFiles[LongInt(FileHandle)]^.FileIndex]].UDFlag :=*)
(*    UDFlag;*)
(*  DbifFieldList^[DbifOpenFiles[LongInt(FileHandle)]^.FileIndex,*)
(*    DbifTotFieldEntries[DbifOpenFiles[LongInt(FileHandle)]^.FileIndex]].FieldName :=*)
(*    FldName;*)
(*  DbifFieldList^[DbifOpenFiles[LongInt(FileHandle)]^.FileIndex,*)
(*    DbifTotFieldEntries[DbifOpenFiles[LongInt(FileHandle)]^.FileIndex]].FieldType :=*)
(*    FldType;*)
(*  DbifFieldList^[DbifOpenFiles[LongInt(FileHandle)]^.FileIndex,*)
(*    DbifTotFieldEntries[DbifOpenFiles[LongInt(FileHandle)]^.FileIndex]].FieldSize :=*)
(*    FldSize;*)
(*  RefreshDict;*)
(*  {Open a temp file}*)
(*  TempFileHandle := 0;*)
(*  Repeat*)
(*    TempFileHandle := Succ(TempFileHandle);*)
(*  Until (Not DbifOpenFiles[TempFileHandle]^.OpenFlag) Or (TempFileHandle = DbifMaxFiles);*)
(*  If DbifOpenFiles[TempFileHandle]^.OpenFlag Then*)
(*    Begin*)
(*      DbifSetError(Err_Dbif+1);*)
(*      Exit*)
(*    End*)
(*  Else*)
(*    Begin*)
(*      DbifOpenFiles[TempFileHandle]^.OpenFlag     := True;*)
(*      DbifOpenFiles[TempFileHandle]^.PhysOpenFlag := False;*)
(*      DbifOpenFiles[TempFileHandle]^.TempFlag     := True;*)
(*      DbifOpenFiles[TempFileHandle]^.FileMode     := DbifExclusive;*)
(*      DbifOpenFiles[TempFileHandle]^.FileName     := 'TEMP0'*)
(*    End;*)
(*  {Calculate the new record length}*)
(*  RecLen := 0;*)
(*  For FieldCounter := 1 To DbifTotFieldEntries[DbifOpenFiles[LongInt(FileHandle)]^.FileIndex] Do*)
(*    Inc(RecLen,DbifFieldList^[DbifOpenFiles[LongInt(FileHandle)]^.FileIndex,FieldCounter].FieldSize);*)
(*  DbifOpenFiles[LongInt(FileHandle)]^.DBLen := RecLen;*)
(*  DbifOpenFiles[TempFileHandle]^.BtrvDataBuf.CreateBuf.FileSpec.RecLen := RecLen;*)
(*  {Set page size to 1024. It may be a good idea to calculate an*)
(*  optimum value here when we get a chance to optimize Dbif}*)
(*  DbifOpenFiles[TempFileHandle]^.BtrvDataBuf.CreateBuf.FileSpec.PageSize := 1024;*)
(*  {Set the number of keys to 1}*)
(*  DbifOpenFiles[TempFileHandle]^.BtrvDataBuf.CreateBuf.FileSpec.NoOfIdx := 1;*)
(*  {Set NotUsed1, FileFlags, Reserved1, and Allocation to 0}*)
(*  DbifOpenFiles[TempFileHandle]^.BtrvDataBuf.CreateBuf.FileSpec.RecCount := 0;*)
(*  DbifOpenFiles[TempFileHandle]^.BtrvDataBuf.CreateBuf.FileSpec.FileFlags := 0;*)
(*  DbifOpenFiles[TempFileHandle]^.BtrvDataBuf.CreateBuf.FileSpec.Reserved1 := 0;*)
(*  DbifOpenFiles[TempFileHandle]^.BtrvDataBuf.CreateBuf.FileSpec.Allocation := 0;*)
(*  NoOfKeys := 0;*)
(*  For FieldCounter := 1 To DbifTotFieldEntries[DbifOpenFiles[LongInt(FileHandle)]^.FileIndex] Do*)
(*    If DbifFieldList^[DbifOpenFiles[LongInt(FileHandle)]^.FileIndex,FieldCounter].KeyFlag Then*)
(*      Inc(NoOfKeys);*)
(*  {Setup key specifications}*)
(*  FieldPos := 1;*)
(*  KeyNo := 1;*)
(*  For FieldCounter := 1 To DbifTotFieldEntries[DbifOpenFiles[LongInt(FileHandle)]^.FileIndex] Do*)
(*    Begin*)
(*      If DbifFieldList^[DbifOpenFiles[LongInt(FileHandle)]^.FileIndex,FieldCounter].KeyFlag Then*)
(*        Begin*)
(*          DbifOpenFiles[TempFileHandle]^.BtrvDataBuf.CreateBuf.KeySpecs[KeyNo].KeyPos := FieldPos;*)
(*          DbifOpenFiles[TempFileHandle]^.BtrvDataBuf.CreateBuf.KeySpecs[KeyNo].KeyLen :=*)
(*            DbifFieldList^[DbifOpenFiles[LongInt(FileHandle)]^.FileIndex,FieldCounter].FieldSize;*)
(*          If KeyNo = NoOfKeys Then {Last or Only Segment}*)
(*            DbifOpenFiles[TempFileHandle]^.BtrvDataBuf.CreateBuf.KeySpecs[KeyNo].KeyFlags := 256*)
(*          Else*)
(*            DbifOpenFiles[TempFileHandle]^.BtrvDataBuf.CreateBuf.KeySpecs[KeyNo].KeyFlags := 256+16;*)
(*          DbifOpenFiles[TempFileHandle]^.BtrvDataBuf.CreateBuf.KeySpecs[KeyNo].NotUsed2 := 0;*)
(*          If DbifFieldList^[DbifOpenFiles[LongInt(FileHandle)]^.FileIndex,FieldCounter].FieldType = DbifNumber Then*)
(*            Begin*)
(*              DbifSetError(Err_Dbif+20);*)
(*              Exit*)
(*            End;*)
(*          Case DbifFieldList^[DbifOpenFiles[LongInt(FileHandle)]^.FileIndex,FieldCounter].FieldType Of*)
(*            DbifInteger    : DbifOpenFiles[TempFileHandle]^.BtrvDataBuf.CreateBuf.KeySpecs[KeyNo].ExtKeyType := 1;*)
(*            DbifMnemonic   : DbifOpenFiles[TempFileHandle]^.BtrvDataBuf.CreateBuf.KeySPecs[KeyNo].ExtKeyType := 10;*)
(*            DbifString     : DbifOpenFiles[TempFileHandle]^.BtrvDataBuf.CreateBuf.KeySpecs[KeyNo].ExtKeyType := 10;*)
(*            DbifText       : DbifOpenFiles[TempFileHandle]^.BtrvDataBuf.CreateBuf.KeySpecs[KeyNo].ExtKeyType := 11;*)
(*            DbifDate       : DbifOpenFiles[TempFileHandle]^.BtrvDataBuf.CreateBuf.KeySpecs[KeyNo].ExtKeyType := 3;*)
(*            DbifTime       : DbifOpenFiles[TempFileHandle]^.BtrvDataBuf.CreateBuf.KeySpecs[KeyNo].ExtKeyType := 4;*)
(*            DbifAutoUser   : DbifOpenFiles[TempFileHandle]^.BtrvDataBuf.CreateBuf.KeySpecs[KeyNo].ExtKeyType := 10;*)
(*            DbifAutoDate   : DbifOpenFiles[TempFileHandle]^.BtrvDataBuf.CreateBuf.KeySpecs[KeyNo].ExtKeyType := 3;*)
(*            DbifAutoTime   : DbifOpenFiles[TempFileHandle]^.BtrvDataBuf.CreateBuf.KeySpecs[KeyNo].ExtKeyType := 4;*)
(*            DbifAutoArchive : DbifOpenFiles[TempFileHandle]^.BtrvDataBuf.CreateBuf.KeySpecs[KeyNo].ExtKeyType := 10;*)
(*            DbifAutoSession : DbifOpenFiles[TempFileHandle]^.BtrvDataBuf.CreateBuf.KeySpecs[KeyNo].ExtKeyType := 10;*)
(*          End;*)
(*          DbifOpenFiles[TempFileHandle]^.BtrvDataBuf.CreateBuf.KeySpecs[KeyNo].NulVal := 0;*)
(*          DbifOpenFiles[TempFileHandle]^.BtrvDataBuf.CreateBuf.KeySpecs[KeyNo].Reserved2 := 0;*)
(*          KeyNo := Succ(KeyNo);*)
(*        End;*)
(*      FieldPos := FieldPos + DbifFieldList^[DbifOpenFiles[LongInt(FileHandle)]^.FileIndex,FieldCounter].FieldSize;*)
(*    End;*)
(*  {Setup DBLen};*)
(*  DbifOpenFiles[TempFileHandle]^.BtrvDBLen := 16 + (16 * NoOfKeys);*)
(*  {Setup BtrvFileName}*)
(*  StrPCopy(DbifOpenFiles[TempFileHandle]^.BtrvKeyBuf.FileName,DbifOpenFiles[TempFileHandle]^.FileName+'.DAT');*)
(*  {Call Btrieve to create the file}*)
(*  BtrvErrorCode := Btrv(14,*)
(*    DbifOpenFiles[TempFileHandle]^.BtrvPos.Start,*)
(*    DbifOpenFiles[TempFileHandle]^.BtrvDataBuf.Start,*)
(*    DbifOpenFiles[TempFileHandle]^.BtrvDBLen,*)
(*    DbifOpenFiles[TempFileHandle]^.BtrvKeyBuf.Start,*)
(*    255,*)
(*    0);*)
(*  {Check for Btrieve error}*)
(*  If BtrvErrorCode > 0 Then*)
(*    Begin*)
(*      DbifSetError(Err_Btrieve+BtrvErrorCode);*)
(*      Exit*)
(*    End;*)
(*  {Physically close both files}*)
(*  If DbifOpenFiles[LongInt(FileHandle)]^.PhysOpenFlag Then*)
(*    Begin*)
(*      CloseErrorCode := Btrv(1,*)
(*        DbifOpenFiles[LongInt(FileHandle)]^.BtrvPos.Start,*)
(*        DbifOpenFiles[LongInt(FileHandle)]^.BtrvDataBuf.Start,*)
(*        DbifOpenFiles[LongInt(FileHandle)]^.BtrvDBLen,*)
(*        DbifOpenFiles[LongInt(FileHandle)]^.BtrvKeyBuf.Start,*)
(*        255,*)
(*        0);*)
(*      If CloseErrorCode > 0 Then*)
(*        Begin*)
(*          DbifSetError(Err_Dbif+22);*)
(*          Exit*)
(*        End;*)
(*      DbifOpenFiles[LongInt(FileHandle)]^.PhysOpenFlag := False;*)
(*    End;*)
(*  If DbifOpenFiles[TempFileHandle]^.PhysOpenFlag Then*)
(*    Begin*)
(*      CloseErrorCode := Btrv(1,*)
(*        DbifOpenFiles[TempFileHandle]^.BtrvPos.Start,*)
(*        DbifOpenFiles[TempFileHandle]^.BtrvDataBuf.Start,*)
(*        DbifOpenFiles[TempFileHandle]^.BtrvDBLen,*)
(*        DbifOpenFiles[TempFileHandle]^.BtrvKeyBuf.Start,*)
(*        255,*)
(*        0);*)
(*      If CloseErrorCode > 0 Then*)
(*        Begin*)
(*          DbifSetError(Err_Dbif+22);*)
(*          Exit*)
(*        End;*)
(*      DbifOpenFiles[TempFileHandle]^.PhysOpenFlag := False;*)
(*    End;*)
(*  {Erase old file}*)
(*  Assign(f,DbifOpenFiles[LongInt(FileHandle)]^.FileName+'.DAT');*)
(*  Erase(f);*)
(*  Waste := IOResult;*)
(*  {Rename temp file to real filename}*)
(*  Assign(f,'TEMP0.DAT');*)
(*  Rename(f,DbifOpenFiles[LongInt(FileHandle)]^.FileName+'.DAT');*)
(*  Waste := IOResult;*)
(*  {Close temp file}*)
(*  DbifOpenFiles[TempFileHandle]^.OpenFlag := False;*)
(*  {If Mnemonic and related file does not already exist, create the related file}*)
(*  If (FldType = DbifMnemonic) And (Not DbifFileExists(mnFileName)) Then*)
(*    Begin*)
(*      GetStr(25105,mnemText,32);*)
(*      MnemonicFileHandle := DbifCreateFile(mnFileName,mnFileName+mnemtext,UDFlag);*)
(*      DbifAddField(MnemonicFileHandle,*)
(*        mnFileName+' Code',*)
(*        DbifString,*)
(*        FldSize,*)
(*        True,*)
(*        UDFlag);*)
(*      DbifAddField(MnemonicFileHandle,*)
(*        mnFileName+' Desc',*)
(*        DbifString,*)
(*        41,*)
(*        False,*)
(*        UDFlag);*)
(*      DbifCloseFile(MnemonicFileHandle);*)
(*    End;*)
(*End;*)


(*Procedure DbifDeleteField;*)

(*Var*)
(*  FieldCounter       : Word;*)
(*  FieldNo        : Word;*)
(*  Waste           : Word;*)
(*  TempFileHandle     : Word;*)
(*  RecLen             : Word;*)
(*  NoOfKeys           : Word;*)
(*  FieldPos           : Word;*)
(*  KeyNo              : Word;*)
(*  BtrvErrorCode      : Word;*)
(*  f                  : File;*)

(*Begin*)
(*  DbifErrorCode := 0;*)
(*  {Make sure file handle is valid}*)
(*  If (LongInt(FileHandle) < 1) Or (LongInt(FileHandle) > DbifMaxFiles) Then*)
(*    Begin*)
(*      DbifSetError(Err_Dbif+8);*)
(*      Exit*)
(*    End;*)
(*  {Check to make sure the file is open}*)
(*  If Not DbifOpenFiles[LongInt(FileHandle)]^.OpenFlag Then*)
(*    Begin*)
(*      DbifSetError(Err_Dbif+9);*)
(*      Exit*)
(*    End;*)
(*  {Make sure file is open in Exclusive mode}*)
(*  If DbifOpenFiles[LongInt(FileHandle)]^.FileMode <> DbifExclusive Then*)
(*    Begin*)
(*      DbifSetError(Err_Dbif+23);*)
(*      Exit*)
(*    End;*)
(*  {Close the file if physically open}*)
(*  If DbifOpenFiles[LongInt(FileHandle)]^.PhysOpenFlag Then*)
(*    Begin*)
(*      BtrvErrorCode := Btrv(1,*)
(*        DbifOpenFiles[LongInt(FileHandle)]^.BtrvPos.Start,*)
(*        DbifOpenFiles[LongInt(FileHandle)]^.BtrvDataBuf.Start,*)
(*        DbifOpenFiles[LongInt(FileHandle)]^.BtrvDBLen,*)
(*        DbifOpenFiles[LongInt(FileHandle)]^.BtrvKeyBuf.Start,*)
(*        255,*)
(*        0);*)
(*      If BtrvErrorCode > 0 Then*)
(*        Begin*)
(*          DbifSetError(Err_Dbif+22);*)
(*          Exit*)
(*        End;*)
(*      DbifOpenFiles[LongInt(FileHandle)]^.PhysOpenFlag := False;*)
(*    End;*)
(*  {Make sure field does not already exist}*)
(*  FieldNo := 1;*)
(*  While (FieldNo <= DbifTotFieldEntries[DbifOpenFiles[LongInt(FileHandle)]^.FileIndex]) And*)
(*    (Upper(DbifFieldList^[DbifOpenFiles[LongInt(FileHandle)]^.FileIndex,FieldNo].FieldName) <> Upper(FldName)) Do*)
(*      Inc(FieldNo);*)
(*  If FieldNo > DbifTotFieldEntries[DbifOpenFiles[LongInt(FileHandle)]^.FileIndex] Then*)
(*    Begin*)
(*      DbifSetError(Err_Dbif + 13);*)
(*      Exit;*)
(*    End;*)
(*  {Delete field from dictionary}*)
(*  For FieldCounter := FieldNo+1 To DbifTotFieldEntries[DbifOpenFiles[LongInt(FileHandle)]^.FileIndex] Do*)
(*    Begin*)
(*      DbifFieldList^[DbifOpenFiles[LongInt(FileHandle)]^.FileIndex,FieldCounter-1] :=*)
(*        DbifFieldList^[DbifOpenFiles[LongInt(FileHandle)]^.FileIndex,FieldCounter];*)
(*      DbifFieldList^[DbifOpenFiles[LongInt(FileHandle)]^.FileIndex,FieldCounter-1].FieldNumber := FieldCounter-1;*)
(*    End;*)
(*  Dec(DbifTotFieldEntries[DbifOpenFiles[LongInt(FileHandle)]^.FileIndex]);*)
(*  RefreshDict;*)
(*  {Open a temp file}*)
(*  TempFileHandle := 0;*)
(*  Repeat*)
(*    TempFileHandle := Succ(TempFileHandle);*)
(*  Until (Not DbifOpenFiles[TempFileHandle]^.OpenFlag) Or (TempFileHandle = DbifMaxFiles);*)
(*  If DbifOpenFiles[TempFileHandle]^.OpenFlag Then*)
(*    Begin*)
(*      DbifSetError(Err_Dbif+1);*)
(*      Exit*)
(*    End*)
(*  Else*)
(*    Begin*)
(*      DbifOpenFiles[TempFileHandle]^.OpenFlag     := True;*)
(*      DbifOpenFiles[TempFileHandle]^.PhysOpenFlag := False;*)
(*      DbifOpenFiles[TempFileHandle]^.TempFlag     := True;*)
(*      DbifOpenFiles[TempFileHandle]^.FileMode     := DbifExclusive;*)
(*      DbifOpenFiles[TempFileHandle]^.FileName     := 'TEMP0'*)
(*    End;*)
(*  {Calculate the new record length}*)
(*  RecLen := 0;*)
(*  For FieldCounter := 1 To DbifTotFieldEntries[DbifOpenFiles[LongInt(FileHandle)]^.FileIndex] Do*)
(*    Inc(RecLen,DbifFieldList^[DbifOpenFiles[LongInt(FileHandle)]^.FileIndex,FieldCounter].FieldSize);*)
(*  DbifOpenFiles[LongInt(FileHandle)]^.DBLen := RecLen;*)
(*  DbifOpenFiles[TempFileHandle]^.BtrvDataBuf.CreateBuf.FileSpec.RecLen := RecLen;*)
(*  {Set page size to 1024. It may be a good idea to calculate an*)
(*  optimum value here when we get a chance to optimize Dbif}*)
(*  DbifOpenFiles[TempFileHandle]^.BtrvDataBuf.CreateBuf.FileSpec.PageSize := 1024;*)
(*  {Set the number of keys to 1}*)
(*  DbifOpenFiles[TempFileHandle]^.BtrvDataBuf.CreateBuf.FileSpec.NoOfIdx := 1;*)
(*  {Set NotUsed1, FileFlags, Reserved1, and Allocation to 0}*)
(*  DbifOpenFiles[TempFileHandle]^.BtrvDataBuf.CreateBuf.FileSpec.RecCount := 0;*)
(*  DbifOpenFiles[TempFileHandle]^.BtrvDataBuf.CreateBuf.FileSpec.FileFlags := 0;*)
(*  DbifOpenFiles[TempFileHandle]^.BtrvDataBuf.CreateBuf.FileSpec.Reserved1 := 0;*)
(*  DbifOpenFiles[TempFileHandle]^.BtrvDataBuf.CreateBuf.FileSpec.Allocation := 0;*)
(*  NoOfKeys := 0;*)
(*  For FieldCounter := 1 To DbifTotFieldEntries[DbifOpenFiles[LongInt(FileHandle)]^.FileIndex] Do*)
(*    If DbifFieldList^[DbifOpenFiles[LongInt(FileHandle)]^.FileIndex,FieldCounter].KeyFlag Then*)
(*      Inc(NoOfKeys);*)
(*  {Setup key specifications}*)
(*  FieldPos := 1;*)
(*  KeyNo := 1;*)
(*  For FieldCounter := 1 To DbifTotFieldEntries[DbifOpenFiles[LongInt(FileHandle)]^.FileIndex] Do*)
(*    Begin*)
(*      If DbifFieldList^[DbifOpenFiles[LongInt(FileHandle)]^.FileIndex,FieldCounter].KeyFlag Then*)
(*        Begin*)
(*          DbifOpenFiles[TempFileHandle]^.BtrvDataBuf.CreateBuf.KeySpecs[KeyNo].KeyPos := FieldPos;*)
(*          DbifOpenFiles[TempFileHandle]^.BtrvDataBuf.CreateBuf.KeySpecs[KeyNo].KeyLen :=*)
(*            DbifFieldList^[DbifOpenFiles[LongInt(FileHandle)]^.FileIndex,FieldCounter].FieldSize;*)
(*          If KeyNo = NoOfKeys Then {Last or Only Segment}*)
(*            DbifOpenFiles[TempFileHandle]^.BtrvDataBuf.CreateBuf.KeySpecs[KeyNo].KeyFlags := 256*)
(*          Else*)
(*            DbifOpenFiles[TempFileHandle]^.BtrvDataBuf.CreateBuf.KeySpecs[KeyNo].KeyFlags := 256+16;*)
(*          DbifOpenFiles[TempFileHandle]^.BtrvDataBuf.CreateBuf.KeySpecs[KeyNo].NotUsed2 := 0;*)
(*          Case DbifFieldList^[DbifOpenFiles[LongInt(FileHandle)]^.FileIndex,FieldCounter].FieldType Of*)
(*            DbifInteger  : DbifOpenFiles[TempFileHandle]^.BtrvDataBuf.CreateBuf.KeySpecs[KeyNo].ExtKeyType := 1;*)
(*            DbifNumber   :*)
(*              Begin*)
(*                DbifSetError(Err_Dbif+20);*)
(*                Exit*)
(*              End;*)
(*            DbifMnemonic : DbifOpenFiles[TempFileHandle]^.BtrvDataBuf.CreateBuf.KeySPecs[KeyNo].ExtKeyType := 10;*)
(*            DbifString   : DbifOpenFiles[TempFileHandle]^.BtrvDataBuf.CreateBuf.KeySpecs[KeyNo].ExtKeyType := 10;*)
(*            DbifText     : DbifOpenFiles[TempFileHandle]^.BtrvDataBuf.CreateBuf.KeySpecs[KeyNo].ExtKeyType := 11;*)
(*            DbifDate     : DbifOpenFiles[TempFileHandle]^.BtrvDataBuf.CreateBuf.KeySpecs[KeyNo].ExtKeyType := 3;*)
(*            DbifTime     : DbifOpenFiles[TempFileHandle]^.BtrvDataBuf.CreateBuf.KeySpecs[KeyNo].ExtKeyType := 4;*)
(*            DbifAutoUser : DbifOpenFiles[TempFileHandle]^.BtrvDataBuf.CreateBuf.KeySpecs[KeyNo].ExtKeyType := 10;*)
(*            DbifAutoDate : DbifOpenFiles[TempFileHandle]^.BtrvDataBuf.CreateBuf.KeySpecs[KeyNo].ExtKeyType := 3;*)
(*            DbifAutoTime : DbifOpenFiles[TempFileHandle]^.BtrvDataBuf.CreateBuf.KeySpecs[KeyNo].ExtKeyType := 4;*)
(*          End;*)
(*          DbifOpenFiles[TempFileHandle]^.BtrvDataBuf.CreateBuf.KeySpecs[KeyNo].NulVal := 0;*)
(*          DbifOpenFiles[TempFileHandle]^.BtrvDataBuf.CreateBuf.KeySpecs[KeyNo].Reserved2 := 0;*)
(*          KeyNo := Succ(KeyNo);*)
(*        End;*)
(*      FieldPos := FieldPos + DbifFieldList^[DbifOpenFiles[LongInt(FileHandle)]^.FileIndex,FieldCounter].FieldSize;*)
(*    End;*)
(*  {Setup DBLen};*)
(*  DbifOpenFiles[TempFileHandle]^.BtrvDBLen := 16 + (16 * NoOfKeys);*)
(*  {Setup BtrvFileName}*)
(*  StrPCopy(DbifOpenFiles[TempFileHandle]^.BtrvKeyBuf.FileName,DbifOpenFiles[TempFileHandle]^.FileName+'.DAT');*)
(*  {Call Btrieve to create the file}*)
(*  BtrvErrorCode := Btrv(14,*)
(*    DbifOpenFiles[TempFileHandle]^.BtrvPos.Start,*)
(*    DbifOpenFiles[TempFileHandle]^.BtrvDataBuf.Start,*)
(*    DbifOpenFiles[TempFileHandle]^.BtrvDBLen,*)
(*    DbifOpenFiles[TempFileHandle]^.BtrvKeyBuf.Start,*)
(*    255,*)
(*    0);*)
(*  {Check for Btrieve error}*)
(*  If BtrvErrorCode > 0 Then*)
(*    Begin*)
(*      DbifSetError(Err_Btrieve+BtrvErrorCode);*)
(*      Exit*)
(*    End;*)
(*  {Physically close both files}*)
(*  If DbifOpenFiles[LongInt(FileHandle)]^.PhysOpenFlag Then*)
(*    Begin*)
(*      BtrvErrorCode := Btrv(1,*)
(*        DbifOpenFiles[LongInt(FileHandle)]^.BtrvPos.Start,*)
(*        DbifOpenFiles[LongInt(FileHandle)]^.BtrvDataBuf.Start,*)
(*        DbifOpenFiles[LongInt(FileHandle)]^.BtrvDBLen,*)
(*        DbifOpenFiles[LongInt(FileHandle)]^.BtrvKeyBuf.Start,*)
(*        255,*)
(*        0);*)
(*      If BtrvErrorCode > 0 Then*)
(*        Begin*)
(*          DbifSetError(Err_Dbif+22);*)
(*          Exit*)
(*        End;*)
(*      DbifOpenFiles[LongInt(FileHandle)]^.PhysOpenFlag := False;*)
(*    End;*)
(*  If DbifOpenFiles[TempFileHandle]^.PhysOpenFlag Then*)
(*    Begin*)
(*      BtrvErrorCode := Btrv(1,*)
(*        DbifOpenFiles[TempFileHandle]^.BtrvPos.Start,*)
(*        DbifOpenFiles[TempFileHandle]^.BtrvDataBuf.Start,*)
(*        DbifOpenFiles[TempFileHandle]^.BtrvDBLen,*)
(*        DbifOpenFiles[TempFileHandle]^.BtrvKeyBuf.Start,*)
(*        255,*)
(*        0);*)
(*      If BtrvErrorCode > 0 Then*)
(*        Begin*)
(*          DbifSetError(Err_Dbif+22);*)
(*          Exit*)
(*        End;*)
(*      DbifOpenFiles[TempFileHandle]^.PhysOpenFlag := False;*)
(*    End;*)
(*  {Erase old file}*)
(*  Assign(f,DbifOpenFiles[LongInt(FileHandle)]^.FileName+'.DAT');*)
(*  Erase(f);*)
(*  Waste := IOResult;*)
(*  {Rename temp file to real filename}*)
(*  Assign(f,'TEMP0.DAT');*)
(*  Rename(f,DbifOpenFiles[LongInt(FileHandle)]^.FileName+'.DAT');*)
(*  Waste := IOResult;*)
(*  {Close temp file}*)
(*  DbifOpenFiles[TempFileHandle]^.OpenFlag := False;*)
(*End;*)


Function DbifGetFieldVal;

Var
	FieldBuf  :
		Record
			Case Integer Of
				1 : (AsString  : String);
				2 : (AsText		: Array[0..255] Of Char);
				3 : (AsLongInt : LongInt);
				4 : (AsReal    : Real);
				5 : (AsDate :
					Record
						Day : Byte;
						Month : Byte;
						Year : Word
					End;);
				6 : (AsTime :
					Record
						HundSec : Byte;
						Second : Byte;
						Minute : Byte;
						Hour : Byte;
					End;)
		End;
	FileHandle 	: DbifFileHandle;
	FieldValue 	: String;
	YearStr 		: String[10];
	MonthStr 	: String[10];
	DayStr 		: String[10];
	HourStr 		: String[10];
	MinuteStr 	: String[10];
	SecondStr 	: String[10];
	Delimiter 	: Char;

Begin
	DbifErrorCode := 0;
	{Validate RecordHandle}
	If Not DbifRecBuf[LongInt(RecordHandle)]^.InUse Then
		Begin
			DbifSetError(Err_Dbif+6);
			Exit
		End;
	{Validate FileHandle}
	FileHandle := DbifRecBuf[LongInt(RecordHandle)]^.FileHandle;
	If Not DbifOpenFiles[LongInt(FileHandle)]^.OpenFlag Then
		Begin
			DbifSetError(Err_Dbif+9);
			Exit
		End;
	{Validate FieldHandle}
	If (FieldHandle < 1) Or (FieldHandle > DbifTotFieldEntries[DbifOpenFiles[LongInt(FileHandle)]^.FileIndex]) Then
		Begin
			DbifSetError(Err_Dbif+7);
			Exit
		End;
	Move(DbifRecBuf[LongInt(RecordHandle)]^.Data[DbifFieldList^[DbifOpenFiles[LongInt(FileHandle)]^.FileIndex,
		FieldHandle].FieldPos],
		FieldBuf,
		DbifFieldList^[DbifOpenFiles[LongInt(FileHandle)]^.FileIndex,FieldHandle].FieldSize);
	Case DbifFieldList^[DbifOpenFiles[LongInt(FileHandle)]^.FileIndex,FieldHandle].FieldType Of
		DbifInvalid  : FieldValue := '';
		DbifInteger  : Str(FieldBuf.AsLongInt,FieldValue);
		DbifNumber   : Begin
								Str(FieldBuf.AsReal:0:10,FieldValue);
								While FieldValue[Length(FieldValue)] = '0' Do
									FieldValue := Copy(FieldValue,1,Length(FieldValue)-1);
								If FieldValue[Length(FieldValue)] = '.' Then
									FieldValue := FieldValue + '0';
							End;
		DbifDate,
		DbifAutoDate : Begin
								If FieldBuf.AsDate.Year = 0 Then
									FieldValue := ''
								Else
									Begin
										Case DateDelimiter Of
											Slash : Delimiter := '/';
											Comma : Delimiter := ',';
											Hyphen : Delimiter := '-';
											Period : Delimiter := '.';
										End;
										If Not DateFourDigitYear Then
											FieldBuf.AsDate.Year := FieldBuf.AsDate.Year Mod 100;
										Str(FieldBuf.AsDate.Year,YearStr);
										Str(FieldBuf.AsDate.Month,MonthStr);
										If (Length(MonthStr) = 1) And (DateLeadingZeros) Then
											MonthStr := '0' + MonthStr;
										Str(FieldBuf.AsDate.Day,DayStr);
										If (Length(DayStr) = 1) And (DateLeadingZeros) Then
											DayStr := '0' + DayStr;
										Case DateOrder Of
											DMY : FieldValue := DayStr+Delimiter+MonthStr+Delimiter+YearStr;
											MDY : FieldValue := MonthStr+Delimiter+DayStr+Delimiter+YearStr;
											YMD : FieldValue := YearStr+Delimiter+MonthStr+Delimiter+DayStr;
										End;
									End
							End;
		DbifTime,
		DbifAutoTime : Begin
								Str(FieldBuf.AsTime.Hour,HourStr);
								If Length(HourStr) = 1 Then
									HourStr := '0' + HourStr;
								Str(FieldBuf.AsTime.Minute,MinuteStr);
								If Length(MinuteStr) = 1 Then
									MinuteStr := '0' + MinuteStr;
								Str(FieldBuf.AsTime.Second,SecondStr);
								If Length(SecondStr) = 1 Then
									SecondStr := '0' + SecondStr;
								FieldValue := HourStr+':'+MinuteStr+':'+SecondStr
							End;
		DbifMnemonic    : FieldValue := FieldBuf.AsString;
		DbifString      : FieldValue := FieldBuf.AsString;
		DbifText        : FieldValue := StrPas(FieldBuf.AsText);
		DbifAutoUser    : FieldValue := FieldBuf.AsString;
		DbifAutoArchive : FieldValue := FieldBuf.AsString;
		DbifAutoSession : FieldValue := FieldBuf.AsString;
	End;
	DbifGetFieldVal := FieldValue;
End;


Procedure DbifPutFieldVal;

Var
	TodayStr 	: String;
	TempStr 		: String;
	TempInt 		: Integer;
	Code 			: Integer;
	Waste 		: Integer;
	Year 			: Integer;
	Month 		: Integer;
	Day 			: Integer;
	Hour 			: Integer;
	Minute 		: Integer;
	Second 		: Integer;
	FileHandle 	: DbifFileHandle;
	FieldBuf  	:
		Record
			Case Integer Of
				1 : (AsString : String);
				2 : (AsText : Array[0..255] Of Char);
				3 : (AsLongInt : LongInt);
				4 : (AsReal : Real);
				5 : (AsDate :
					Record
						Day : Byte;
						Month : Byte;
						Year : Word;
					End;);
				6 : (AsTime :
					Record
						HundSec : Byte;
						Second : Byte;
						Minute : Byte;
						Hour : Byte;
					End;)
		End;

Begin
	DbifErrorCode := 0;
	{Validate RecordHandle}
	If Not DbifRecBuf[LongInt(RecordHandle)]^.InUse Then
		Begin
			DbifSetError(Err_Dbif+6);
			Exit
		End;
	{Validate FileHandle}
	FileHandle := DbifRecBuf[LongInt(RecordHandle)]^.FileHandle;
	If Not DbifOpenFiles[LongInt(FileHandle)]^.OpenFlag Then
		Begin
			DbifSetError(Err_Dbif+9);
			Exit
		End;
	{Validate FieldHandle}
	If (FieldHandle < 1) Or (FieldHandle > DbifTotFieldEntries[DbifOpenFiles[LongInt(FileHandle)]^.FileIndex]) Then
		Begin
			DbifSetError(Err_Dbif+7);
			Exit
		End;
	Case DbifFieldList^[DbifOpenFiles[LongInt(FileHandle)]^.FileIndex,FieldHandle].FieldType Of
		DbifInteger  : Begin
								If FieldValue = '' Then
									FieldValue := '0';
								Val(FieldValue,FieldBuf.AsLongInt,Code);
								If Code <> 0 Then
									Begin
										DbifSetError(Err_Dbif+14);
										Exit
									End
							End;
		DbifNumber   : Begin
								If FieldValue = '' Then
									FieldValue := '0.0';
								Val(FieldValue,FieldBuf.AsReal,Code);
								If Code <> 0 Then
									Begin
										DbifSetError(Err_Dbif+15);
										Exit
									End
							End;
		DbifDate,
		DbifAutoDate : Begin
								If FieldValue = '' Then
									Begin
										FieldBuf.AsDate.Year := 0;
										FieldBuf.AsDate.Month := 0;
										FieldBuf.AsDate.Day := 0;
									End
								Else
									Begin
										TrimTrail(FieldValue);
										FieldValue := Upper(FieldValue);
										GetPStr(5313,TodayStr);
										TodayStr := Copy(TodayStr,1,Pos(' ',TodayStr)-1);
										If Copy(FieldValue,1,Length(TodayStr)) = TodayStr Then
											Begin
												If Length(FieldValue) = Length(TodayStr) Then
													TempInt := 0
												Else
													Begin
														Val(Copy(FieldValue,Length(TodayStr)+1,255),TempInt,Code);
														If Code <> 0 Then
															Begin
															DbifSetError(Err_Dbif+19);
															Exit
														End;
													End;
												DateToDMY(IncDate(Today,TempInt,0,0),Day,Month,Year);
												FieldBuf.AsDate.Year := Year;
												FieldBuf.AsDate.Month := Month;
												FieldBuf.AsDate.Day := Day
											End
										Else If Copy(FieldValue,1,1) = 'T' Then
											Begin
												If FieldValue = 'T' Then
													TempInt := 0
												Else
													Begin
														Val(Copy(FieldValue,2,255),TempInt,Code);
														If Code <> 0 Then
															Begin
																DbifSetError(Err_Dbif+19);
																Exit
															End;
													End;
												DateToDMY(IncDate(Today,TempInt,0,0),Day,Month,Year);
												FieldBuf.AsDate.Year := Year;
												FieldBuf.AsDate.Month := Month;
												FieldBuf.AsDate.Day := Day
											End
										Else If ValidDate(FieldValue) Then
											Begin
												BreakDate(FieldValue,Year,Month,Day);
												FieldBuf.AsDate.Year := Year;
												FieldBuf.AsDate.Month := Month;
												FieldBuf.AsDate.Day := Day
											End
										Else
											Begin
												DbifSetError(Err_Dbif+19);
												Exit
											End;
									End;
							End;
		DbifTime,
		DbifAutoTime : Begin
								If (FieldValue = 'N') Or
									(FieldValue = 'n') Or
									(FieldValue = 'T') Or
									(FieldValue = 't') Then
                           FieldValue := CurrentTime;
								If FieldValue = '' Then
									Begin
										FieldBuf.AsTime.Hour := 0;
										FieldBuf.AsTime.Minute := 0;
										FieldBuf.AsTime.Second := 0;
									End
								Else If ValidTime(FieldValue) Then
									Begin
										BreakTime(FieldValue,Hour,Minute,Second);
										FieldBuf.AsTime.Hour := Hour;
										FieldBuf.AsTime.Minute := Minute;
										FieldBuf.AsTime.Second := Second
									End
								Else
									Begin
										DbifSetError(Err_Dbif+19);
										Exit
									End;
							End;
		DbifMnemonic : Begin
								{Make sure value is not too long}
								If Length(FieldValue) >= DbifFieldList^[DbifOpenFiles[LongInt(FileHandle)]^.FileIndex,FieldHandle].FieldSize Then
									Begin
										DbifSetError(Err_Dbif+16);
										Exit
									End;
								FieldBuf.AsString := FieldValue;
							End;
		DbifAutoArchive,
		DbifAutoSession,
		DbifString	 : Begin
								If Length(FieldValue) >= DbifFieldList^[DbifOpenFiles[LongInt(FileHandle)]^.FileIndex,FieldHandle].FieldSize Then
									Begin
										DbifSetError(Err_Dbif+17);
										Exit
									End
								Else
									FieldBuf.AsString := FieldValue
							End;
		DbifText     : Begin
								If Length(FieldValue) >= DbifFieldList^[DbifOpenFiles[LongInt(FileHandle)]^.FileIndex,FieldHandle].FieldSize Then
									Begin
										DbifSetError(Err_Dbif+18);
										Exit
									End
								Else
									StrPCopy(FieldBuf.AsText,FieldValue);
							End;
		DbifAutoUser : Begin
								If Length(FieldValue) >= DbifFieldList^[DbifOpenFiles[LongInt(FileHandle)]^.FileIndex,FieldHandle].FieldSize Then
									Begin
										DbifSetError(Err_Dbif+17);
										Exit
									End
								Else
									FieldBuf.AsString := FieldValue
							End;
	End;
	Move(FieldBuf,
		DbifRecBuf[LongInt(RecordHandle)]^.Data[DbifFieldList^[DbifOpenFiles[LongInt(FileHandle)]^.FileIndex,FieldHandle].FieldPos],
		DbifFieldList^[DbifOpenFiles[LongInt(FileHandle)]^.FileIndex,FieldHandle].FieldSize);
End;


Procedure DbifGetField;

Var
	FileHandle : DbifFileHandle;

Begin
	DbifErrorCode := 0;
	FileHandle := DbifRecBuf[LongInt(RecordHandle)]^.FileHandle;
	If DbifFieldList^[DbifOpenFiles[LongInt(FileHandle)]^.FileIndex,FieldHandle].FieldType <> DbifText Then
		StrPCopy(FieldValue,DbifGetFieldVal(RecordHandle,FieldHandle))
	Else
		Move(
			DbifRecBuf[LongInt(RecordHandle)]^.Data[DbifFieldList^[DbifOpenFiles[LongInt(FileHandle)]^.FileIndex,FieldHandle].FieldPos],
			FieldValue^,
			DbifFieldList^[DbifOpenFiles[LongInt(FileHandle)]^.FileIndex,FieldHandle].FieldSize);
End;


Procedure DbifPutField;

Var
	FileHandle : DbifFileHandle;

Begin
	DbifErrorCode := 0;
	FileHandle := DbifRecBuf[LongInt(RecordHandle)]^.FileHandle;
	If DbifFieldList^[DbifOpenFiles[LongInt(FileHandle)]^.FileIndex,FieldHandle].FieldType <> DbifText Then
		DbifPutFieldVal(RecordHandle,FieldHandle,StrPas(FieldValue))
	Else
		Move(
			FieldValue^,
			DbifRecBuf[LongInt(RecordHandle)]^.Data[DbifFieldList^[DbifOpenFiles[LongInt(FileHandle)]^.FileIndex,FieldHandle].FieldPos],
			DbifFieldList^[DbifOpenFiles[LongInt(FileHandle)]^.FileIndex,FieldHandle].FieldSize);
End;

Function DbifCompFieldVal(Val1,Val2:String;FieldType:DbifFieldType):Integer;

Var
	Int1 		: LongInt;
	Int2 		: LongInt;
	Real1 	: Real;
	Real2 	: Real;
	Y1 		: Integer;
	M1 		: Integer;
	D1 		: Integer;
	Y2 		: Integer;
	M2 		: Integer;
	D2 		: Integer;
	TempStr 	: String;
	Code 		: Integer;

Begin
	DbifErrorCode := 0;
	If FieldType = DbifInteger Then
		Begin
			Val(Val1,Int1,Code);
			Val(Val2,Int2,Code);
			If Int1 = Int2 Then
				DbifCompFieldVal := 0
			Else If Int1 < Int2 Then
				DbifCompFieldVal := -1
			Else
				DbifCompFieldVal := 1;
			Exit;
		End;
	If FieldType = DbifNumber Then
		Begin
			Val(Val1,Real1,Code);
			Val(Val2,Real2,Code);
			If Real1 = Real2 Then
				DbifCompFieldVal := 0
			Else If Real1 < Real2 Then
				DbifCompFieldVal := -1
			Else DbifCompFieldVal := 1;
			Exit;
		End;
	If (FieldType = DbifDate) Or
		(FieldType = DbifAutoDate) Then
		Begin
			BreakDate(Val1,Y1,M1,D1);
			Str(Y1,TempStr);
			Val1 := TempStr;
			Str(M1,TempStr);
			If Length(TempStr) = 1 Then
				TempStr := '0'+TempStr;
			Val1 := Val1+TempStr;
			Str(D1,TempStr);
			If Length(TempStr) = 1 Then
				TempStr := '0'+TempStr;
			Val1 := Val1+TempStr;
			BreakDate(Val2,Y2,M2,D2);
			Str(Y2,TempStr);
			Val2 := TempStr;
			Str(M2,TempStr);
			If Length(TempStr) = 1 Then
				TempStr := '0'+TempStr;
			Val2 := Val2+TempStr;
			Str(D2,TempStr);
			If Length(TempStr) = 1 Then
				TempStr := '0'+TempStr;
			Val2 := Val2+TempStr;
		End;
	If Val1 = Val2 Then
		DbifCompFieldVal := 0
	Else If Val1 < Val2 Then
		DbifCompFieldVal := -1
	Else DbifCompFieldVal := 1;
End;

Begin
(*  DbifInit;*)
End.
