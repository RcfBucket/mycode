Unit IntIO;


Interface

Uses
  IntGlob,
  IntASTM,
  WinCrt,
	WinProcs;


function IntIO_OpenXFer: boolean;
procedure IntIO_PutSingleRecord;
procedure IntIO_PutEndXFer;
procedure	IntIO_GetFileData;
procedure	IntIO_CommCleanup;

var
	OkToSend: boolean;


Implementation

const
	UseFileXFerMode = True;
	UseOtherMode = False;


var
	XFerFile     : Text;
	UseXFerFile  : boolean;

function IntIO_OpenXFer: boolean;
var
	Opened: boolean;

begin
  Opened := True;
	IntIO_OpenXFer := Opened;
end;

procedure IntIO_PutSingleRecord;
begin
	OkToSend := WriteRecord(SingleRecord);
end;

procedure IntIO_PutEndXFer;
begin
	Terminate;
end;

procedure	IntIO_GetFileData;
begin
	GetFileData;
end;

procedure	IntIO_CommCleanup;
begin
	CommCleanup;
end;

Begin
End.
