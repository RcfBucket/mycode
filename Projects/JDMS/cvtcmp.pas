unit CvtCmp;

{- this module is used to compare V2 user procedures and batteries to
   version 3 test groups and batteries }

INTERFACE

uses
  Objects,
  DBFile,
  DBTypes;

procedure BuildV2TestProcList(aBatID, aProcID: PChar; aProcList: PCollection; aLog: boolean);

function  CompOrderAndProcList(anOrder: TSeqNum; aProcList: PCollection): boolean;
function  CompGroupAndProcList(aGrpSeq: TSeqNum; aProcList: PCollection): boolean;
function  CompOrderAndBattery(anOrder: TSeqNum; aBatID: PChar): boolean;
function  CompOrderAndPanel(anOrder: TSeqNum; aPanelAbbr: PChar): boolean;

function  CompOrderAndProc(ordID, aBatID, procID: PChar): boolean;
function  CompGroupAndProc(aGrpID, aBatID, aProc: PChar): boolean;

function  CreateIsoOrders(dbIso: PDBFile): boolean;

type
  {- This object is used as a collection item for collections passed to the
     compare routines in this module }
  PTestObj = ^TTestObj;
  TTestObj = object(TObject)
    testID    : array[0..8] of char;
    testName  : array[0..31] of char;
    testDrug  : array[0..6] of char;
    testCat   : array[0..6] of char;
    testUD    : boolean;
    resLen    : integer;
    v3testID  : array[0..8] of char;
    {}
    private
    seen      : boolean;
  end;

IMPLEMENTATION

uses
  WinProcs,
  WinTypes,
  Strings,
  MScan,
  CvtMisc,
  DBIF,
  DBIFFlds,
  DB2DBIF,
  V3ResLib,
  DBIDs,
  DBLib;

procedure BadError(num: integer; abort: boolean);
{- show a critical error and possibly abort }
var
  errStr : array[0..32] of char;
begin
  Str(num + 5000, errStr);
  StrCat(errStr, ' Error occurred in CVTCMP');
  CriticalError(errStr);
end;


function CreateIsoOrders(dbIso: PDBFile): boolean;
{- create isolate order records.
    open the order xref file.
    for each tst order in the order
      create an Iso Order
}
var
  isoOrd    : PDBFile;
  orderDef  : PDBFile;
  ordSeq    : TSeqNum;
  isoSeq    : TSeqNum;
  tstOrdSeq : TSeqNum;
  sret      : boolean;
begin
  sret:= true;

  dbIso^.dbr^.GetField(DBISOOrdRef, @ordSeq, SizeOf(TSeqNum));
  isoSeq:= dbIso^.dbr^.GetSeqValue;

  {- open order definition file }
  orderDef:= OpenV30File(DBOrdXrfFile);
  if orderDef = nil then
  begin
    sret:= false;
    BadError(11, True);
  end
  else
  begin
    {- open isolate orders file }
    isoOrd:= OpenV30File(DBIsoOrdFile);
    if isoOrd = nil then
    begin
      BadError(12, True);
      sret:= false;
    end;
  end;

  {- now lets get on with it }
  if sret then
  begin
    orderDef^.dbr^.PutField(DBOrdXrfOrdRef, @ordSeq);
    {- find the first order definition record }
    if orderDef^.dbc^.GetFirstContains(orderDef^.dbr) then
    begin
      repeat
        {- get the test order from the order definition }
        orderDef^.dbr^.GetField(DBOrdXrfTstGrpRef, @tstOrdSeq, sizeOf(tstOrdSeq));

        {- now I have the test order reference and the isolate seq num.
          Time to create the Isolate order }
        isoOrd^.dbr^.ClearRecord;
        isoOrd^.dbr^.PutField(DBIsoOrdIsoRef, @isoSeq);
        isoOrd^.dbr^.PutField(DBIsoOrdTstGrpRef, @tstOrdSeq);
        if not isoOrd^.dbc^.InsertRec(isoOrd^.dbr) then
        begin
          BadError(13, true);
          sret:= false;
        end;
        orderDef^.dbr^.ClearField(DBOrdXrfTstGrpRef);
      until not sret or not orderDef^.dbc^.GetNextContains(orderDef^.dbr);
    end
    else
    begin
      BadError(14, True);
      sret:= false;
    end;
  end;

  MSDisposeObj(isoOrd);
  MSDisposeObj(orderDef);
  CreateIsoOrders := sret;
end;


function CreateDummyIso(dbIso: PDBFile; anOrder: TSeqNum): boolean;
{- create isolate record and isolate order records. }
var
  sret      : boolean;
begin
  sret:= true;

  dbIso^.dbr^.ClearRecord;
  {- specimen seq is 0 }
  dbIso^.dbr^.PutFieldAsStr(DBIsoIso, '001');
  if dbIso^.dbc^.GetEQ(dbIso^.dbr) then
    dbIso^.dbc^.DeleteRec(dbIso^.dbr);

  if anOrder = 0 then
    sret := FALSE
  else
  begin
    dbIso^.dbr^.PutField(DBISOOrdRef, @anOrder);
    dbIso^.dbc^.InsertRec(dbIso^.dbr);  {- add record }
    if dbIso^.dbc^.dbErrorNum <> 0 then
      BadError(15, True);

    dbIso^.dbc^.GetEQ(dbIso^.dbr);
    CreateIsoOrders(dbIso);
  end;
  CreateDummyIso := sret;
end;


procedure BuildV2TestProcList(aBatID, aProcID: PChar; aProcList: PCollection; aLog: boolean);
{- for a given battery id, this routine loads aProcList collection with all of
   the test ID's and categorys for the test procedure within the battery definition.
  Input:
    aBatID    - This is battery id containing the desired user procedure.
    aProcList - A valid collection. All existing items will be purged from this
                collection.
  Input:
    aProcList - This is a collection of TTestObj's for all of the tests in the
                user proc for the speified battery.
}
var
  tstr      : string;
  batDefFH  : DBIFFileHandle;
  batDefRH  : DBIFRecordHandle;
  tstDefFH  : DBIFFileHandle;
  tstDefRH  : DBIFRecordHandle;
  tob       : PTestObj;
  isOK      : boolean;
  k, c      : integer;
  p         : PChar;

  function Matches(rh: DBIFRecordHandle; fld: PChar; fldNum: integer): boolean;
  {- test value in a DBIF record with a value in a PDB2Field. Return True if
    values are equal. rh specifies a DBIF record handle, fld is a value
    to test against and fldNum is the field in rh to test against fld }
  var
    tstr  : string[132];
  begin
    tstr := StrPas(fld);
    Matches:= (tstr = DBIFGetFieldVal(rh, fldNum));
  end;

  procedure AddTest;
  var
    found : PTestObj;

    function IsMatch(item: PTestObj): boolean; Far;
    begin
      IsMatch:= (StrComp(item^.testDrug, tob^.testDrug) = 0) and
                (StrComp(item^.testCat, tob^.testCat) = 0);
    end;

  begin
    found:= aProcList^.FirstThat(@IsMatch);
    if found = nil then
      aProcList^.Insert(tob)
    else
    begin
      if aLog then
      begin
        LogStr('Duplicate test drug found in Battery: ');
        LogStr(aBatID);
        LogStr(', Proc: USER, Test: ');
        LogStr(tob^.testID);
        LogStr(', Category: ');
        LogStr(tob^.testCat);
        LogStr(', Drug: ');
        LogStr(tob^.testDrug);
        LogLn;
      end;
      MSDisposeObj(tob);
    end;
  end;

begin
  aProcList^.FreeAll;

  ChDir(dbifDir);

  {- Open BATTERY definition file }
  batDefFH:= DBIFOpenFile('BATDEF', DBIFShared);
  if DBIFGetErrorCode <> 0 then BadError(1, True);
  batDefRH:= DBIFNewRecord(batDefFH);
  if DBIFGetErrorCode <> 0 then BadError(2, True);

  {- Open TEST definition file }
  tstDefFH:= DBIFOpenFile('TEST', DBIFShared);
  if DBIFGetErrorCode <> 0 then BadError(3, True);
  tstDefRH:= DBIFNewRecord(tstDefFH);
  if DBIFGetErrorCode <> 0 then BadError(4, True);

  {- find battery ID in battery definition file }
  tstr := StrPas(aBatID);
  DBIFPutFieldVal(batDefRH, DBIFFldBatteryDefID, tstr);
  tstr := StrPas(aProcID);
  DBIFPutFieldVal(batDefRH, DbifFldBatteryDefTestProc, tstr);
  DBIFSearchRecordGE(batDefRH);
  isok := DBIFGetErrorCode = 0;
  {- loop thru and load the test proc definitions for the battery. }
  while isOK and
        Matches(batDefRH, aBatID, DBIFFldBatteryDefID) and
        ((StrLen(aProcID) = 0) or
         Matches(batDefRH, aProcID, DbifFldBatteryDefTestProc)) do
  begin
    tob := New(PTestObj, Init);

    {- get test ID }
    tstr:= DBIFGetFieldVal(batDefRH, DBIFFldBatteryDefTestID);
    StrPCopy(tob^.testID, tstr);
    StrPCopy(tob^.v3testID, tstr);

    {- get test name (description) and category and drug id for test }
    DBIFPutFieldVal(tstDefRH, DBIFFldTestID, tstr);
    DBIFGetRecord(tstDefRH);

    if DBIFGetErrorCode <> 0 then BadError(5, True);
    {- Get Full test name }
    tstr:= DBIFGetFieldVal(tstDefRH, DBIFFldTestName);
    StrPCopy(tob^.testName, tstr);

    {- Get drug defined for test }
    tstr:= DBIFGetFieldVal(tstDefRH, DBIFFldTestDrug);
    StrPCopy(tob^.testDrug, tstr);

    {- Get test category }
    tstr:= DBIFGetFieldVal(tstDefRH, DBIFFldTestCategory);
    if (tstr = '') then
    begin
      tstr := 'BIO';
      StrCopy(tob^.testDrug, '');
    end;
    StrPCopy(tob^.testCat, tstr);

    {- Get UD flag for test }
    tstr:= DBIFGetFieldVal(tstDefRH, DbifFldTestUDFlag);
    if tstr = 'N' then
    begin
      tob^.testUD := FALSE;
      if (StrComp(tob^.testCat, 'NCCLS') = 0) or
         (StrComp(tob^.testCat, 'MIC') = 0) then
      begin
        p := StrEnd(tob^.v3testID);
        Dec(p);
        if (p^ = '1') or (p^ = '0') then
          repeat
            p^ := #0;
            Dec(p);
          until (p^ <> ' ') ;
      end;
    end
    else
      tob^.testUD := TRUE;

    {- Get length of result field defined for this test }
    tstr:= DBIFGetFieldVal(tstDefRH, DBIFFldTestResultLen);
    Val(tstr, k, c);
    if (c = 0) then
      tob^.resLen:= k;

    {- add user test information to collection }
    AddTest;
    DBIFNextRecord(batDefRH);
    isok:= DBIFGetErrorCode = 0;
  end;

  DbifCloseRecord(batDefRH);
  DbifCloseFile(batDefFH);
  DbifCloseRecord(tstDefRH);
  DbifCloseFile(tstDefFH);

  ChDir(workDir);
end;

function CompOrderAndProcList(anOrder: TSeqNum; aProcList: PCollection): boolean;
{- this routine looks at the given order and compares it with the tests in a
   user procedure. If the entire order is compatible with the USER proc then
   the function returns true.
  Input:
    anOrder   - This is the seq number of the order to compare
    aProcList - This is a collection of TTestObj's for all of the tests in the
                user proc.
}
var
  dbIso     : PDBFile;
  results   : PV3Results;
  resObj    : PResRecObj;

  procedure ClearSeen(item: PTestObj); far;
  begin
    item^.seen := FALSE;
  end;

  function IsNotSeen(item: PTestObj): boolean; far;
  begin
    IsNotSeen := not item^.seen;
  end;

  function IsNotFound(item: PResRecObj): boolean; far;
  var
    found : PTestObj;

    function IsMatch(item2: PTestObj): boolean; far;
    begin
      IsMatch:= not item2^.seen and
                (StrComp(item2^.v3TestID, item^.testID) = 0) and
                (StrComp(item2^.testCat, item^.testCatID) = 0);
    end;

  begin
    found:= aProcList^.FirstThat(@IsMatch);
    if found <> nil then
      found^.seen := TRUE;
    IsNotFound := (found = nil);
  end;

begin
  CompOrderAndProcList:= FALSE;

  dbIso := OpenV30File(DBISOFile);
  if dbIso = nil then
    BadError(21, True);

  results := New(PV3Results, Init);
  if results = nil then
    BadError(22, True);

  if CreateDummyIso(dbIso, anOrder) and
     results^.LoadResults(dbIso^.dbr) and
     (results^.Count = aProcList^.Count) then
  begin
    aProcList^.ForEach(@ClearSeen);
    if (results^.FirstThat(@IsNotFound) = nil) and
       (aProcList^.FirstThat(@IsNotSeen) = nil) then
      CompOrderAndProcList:= TRUE;
  end;

  dbIso^.dbc^.DeleteRec(dbIso^.dbr);

  MSDisposeObj(results);
  MSDisposeObj(dbIso);
end;

function CompGroupAndProcList(aGrpSeq: TSeqNum; aProcList: PCollection): boolean;
{- this routine looks at a test group and compares it with the tests in a user
   procedure. If the entire group is compatible with the USER procedure, then
   the function returns true.
  Input:
    anOrder   - This is the seq number of the order to compare
    aProcList - This is a collection of TTestObj's for all of the tests in the
                user proc.
}
var
  db        : PDBFile;
  tstDB     : PDBFile;
  testSeq   : TSeqNum;
  pTestID   : array [0..8] of char;
  pTestCat  : array [0..6] of char;
  found     : PTestObj;
  ret       : boolean;

  function IsMatch(item: PTestObj): boolean; far;
  begin
    IsMatch:= not item^.seen and
              (StrComp(item^.v3TestID, pTestID) = 0) and
              (StrComp(item^.testCat, pTestCat) = 0);
  end;

  procedure ClearSeen(item: PTestObj); far;
  begin
    item^.seen := FALSE;
  end;

  function IsNotSeen(item: PTestObj): boolean; far;
  begin
    IsNotSeen := not item^.seen;
  end;

begin
  ret := FALSE;
  CompGroupAndProcList:= FALSE;
  db := OpenV30File(DBTSTGRPXRFFile);
  if db = nil then
    BadError(31, True);

  tstDB := OpenV30File(DBTSTFile);
  if tstDB = nil then
    BadError(32, True);

  db^.dbr^.PutField(DBTSTGRPXRFTstGrpRef, @aGrpSeq);
  if db^.dbc^.GetFirstContains(db^.dbr) then
  begin
    ret := TRUE;
    aProcList^.ForEach(@ClearSeen);
    repeat
      db^.dbr^.GetField(DBTSTGRPXRFTstRef, @testSeq, SizeOf(TSeqNum));
      tstDB^.dbc^.GetSeq(tstDB^.dbr, testSeq);
      tstDB^.dbr^.GetField(DBTSTID, @pTestID, REStestIDLen);
      tstDB^.dbr^.GetField(DBTSTTstCat, @pTestCat, REStestCatIDLen);

      found:= aProcList^.FirstThat(@IsMatch);
      if found = nil then
      begin
        ret := FALSE;
        Break;
      end
      else
        found^.seen := TRUE;
    until not db^.dbc^.GetNextContains(db^.dbr);
    if ret and (aProcList^.FirstThat(@IsNotSeen) <> nil) then
      ret := FALSE;
  end;
  MSDisposeObj(db);
  MSDisposeObj(tstDB);
  CompGroupAndProcList:= ret;
end;

function CompOrderAndBattery(anOrder: TSeqNum; aBatID: PChar): boolean;
{- this routine looks at an order and compares it with all the tests in a
   battery. If the entire order is compatible with the battery, then
   the function returns true.
  Input:
    anOrder   - This is the seq number of the order to compare
    aBatID    - This is the version 2 battery id
}
var
  coll  : PCollection;
begin
  {- build test list for user proc and see if it is compatible  }
  coll:= New(PCollection, Init(25, 25));
  BuildV2TestProcList(aBatID, '', coll, FALSE);
  CompOrderAndBattery:= CompOrderAndProcList(anOrder, coll);
  MSDisposeObj(coll);
end;

function CompOrderAndPanel(anOrder: TSeqNum; aPanelAbbr: PChar): boolean;
{- this routine looks at an order and compares it with a panel abbr. That is
   it confirms that the ONLY thing (group) the order contains is the panel
   specified.
  Input:
    anOrder   - This is the seq number of the order to compare
    aPanelAbbr- This is a V3 panel abbreviation (ie, NC1J, NC2J etc)
}
var
  db    : PDBFile;
  seq   : TSeqNum;
  count : integer;
  pstr  : array[0..100] of char;
begin
  CompOrderAndPanel:= FALSE;
  db:= OpenV30File(DBORDXRFFile);
  if db = nil then
    BadError(41, True);

  count:= 0;
  db^.dbr^.PutField(DBORDXRFOrdRef, @seq);
  if db^.dbc^.GetFirstContains(db^.dbr) then
  begin
    db^.dbr^.GetField(DBORDXRFTstGrpRef, @seq, sizeof(seq));
    if not db^.dbc^.GetNextContains(db^.dbr) then
      count:= 1
    else
      count:= 2;
  end;
  MSDisposeObj(db);

  if count = 1 then
  begin
    db:= OpenV30File(DBTSTGRPFile);
    if db = nil then
      BadError(42, True);
    if db^.dbc^.GetSeq(db^.dbr, seq) then
    begin
      db^.dbr^.GetFieldAsStr(DBTSTGRPID, pstr, sizeof(pstr)-1);
      CompOrderAndPanel:= StrComp(pstr, aPanelAbbr) = 0;
    end;
    MSDisposeObj(db);
  end;
end;

function CompOrderAndProc(ordID, aBatID, procID: PChar): boolean;
{- returns true if order contains 1 test group and that test group is the same
   as the proc. }
var
  db    : PDBFile;
  seq   : TSeqNum;
  count : integer;
  pstr  : array[0..100] of char;
  coll  : PCollection;
begin
  CompOrderAndProc:= FALSE;
  seq:= OrderExists(ordID, nil);
  if StrComp(procID, 'USER') <> 0 then
  begin
    if seq <> 0 then
    begin
      db:= OpenV30File(DBORDXRFFile);
      if db = nil then
        BadError(51, True);

      count:= 0;
      db^.dbr^.PutField(DBORDXRFOrdRef, @seq);
      if db^.dbc^.GetFirstContains(db^.dbr) then
      begin
        db^.dbr^.GetField(DBORDXRFTstGrpRef, @seq, sizeof(seq));
        if not db^.dbc^.GetNextContains(db^.dbr) then
          count:= 1
        else
          count:= 2;
      end;
      MSDisposeObj(db);

      if count = 1 then
      begin
        db:= OpenV30File(DBTSTGRPFile);
        if db = nil then
          BadError(52, True);
        if db^.dbc^.GetSeq(db^.dbr, seq) then
        begin
          db^.dbr^.GetFieldAsStr(DBTSTGRPID, pstr, sizeof(pstr)-1);
          CompOrderAndProc:= StrComp(pstr, procID) = 0;
        end;
        MSDisposeObj(db);
      end;
    end;
  end
  else
  begin
    {- build test list for user proc and see if it is compatible  }
    coll:= New(PCollection, Init(25, 25));
    BuildV2TestProcList(aBatID, procID, coll, FALSE);
    if seq > 0 then
      CompOrderAndProc:= CompOrderAndProcList(seq, coll);
    MSDisposeObj(coll);
  end;
end;

function CompGroupAndProc(aGrpID, aBatID, aProc: PChar): boolean;
{- look at the contents of the group and return true if they are compatible
   with the test proc for the battery }
var
  coll    : PCollection;
  seq     : TSeqNum;
begin
  CompGroupAndProc:= false;
  if StrComp(aProc, 'USER') = 0 then
  begin
    coll:= New(PCollection, Init(25,25));
    seq:= GroupExists(aGrpID, nil);
    if seq > 0 then
    begin
      BuildV2TestProcList(aBatID, aProc, coll, FALSE);
      CompGroupAndProc:= CompGroupAndProcList(seq, coll);
    end;
    MSDisposeObj(coll);
  end
  else
  begin
    CompGroupAndProc:= StrComp(aGrpID, aProc) = 0;
  end;
end;


END.

