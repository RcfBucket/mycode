UNIT wlHWConf;
INTERFACE
  USES WObjects, WinTypes, WinProcs, Strings,
  WLmScan, WLDMStrg;

{$R wlHWCONF.RES}

CONST
  NoPort         = 0;
	GPIBPort       = 1;
	LPT1Port       = 2;
	COM1Port       = 3;
	COM2Port       = 4;
	COM3Port       = 5;

	id_Com1AS4     = 106;
	id_Com1Report  = 107;
	id_Com1TwoWay  = 108;
	id_Com1None    = 109;

	id_Com2AS4     = 110;
	id_Com2Report  = 111;
	id_Com2TwoWay  = 112;
	id_Com2None    = 113 ;

	id_Com3AS4     = 114;
	id_Com3Report  = 115;
	id_Com3TwoWay  = 116;
	id_Com3None    = 117;

  id_LPT1Barcode = 118;
	id_LPT1Report  = 119;
	id_LPT1None    = 120;

	id_GPIBWA      = 121;
	id_GPIBNone    = 122;

	AS4     = 1;
	WA      = 2;
	TwoWay  = 3;
	Report  = 4;
  Barcode = 5;

  pkReport='REPORTPRINTER';
  pkBarcode='BARCODEPRINTER';
  pkWalkAway='WAEXISTS';
	pkTwoWay='TWOWAYPORT';
	pkAS4='AS4PORT';

TYPE
  PConfigDialog = ^TConfigDialog;
  TConfigDialog = OBJECT(TDialog)
    AS4Port, TwoWayPort, BCPrinterPort, RepPrinterPort, WAPort : INTEGER;
		Restart: BOOLEAN;
   	CONSTRUCTOR Init(AParent : PWindowsObject; ATitle: PChar);
		DESTRUCTOR Done; VIRTUAL;
    FUNCTION CanClose : BOOLEAN; VIRTUAL;
	  PROCEDURE SetupWindow; virtual;
		FUNCTION GetPortAssignment(KeyName: PChar) : INTEGER; VIRTUAL;
    PROCEDURE SetPortAssignment(KeyName: PChar; KeyPort : INTEGER); VIRTUAL;
		FUNCTION ValidatePortAssignments : BOOLEAN; VIRTUAL;
    FUNCTION SetPort(inst : INTEGER) : BOOLEAN; VIRTUAL;
	END;

PROCEDURE Start(AParent : PWindowsObject);
FUNCTION HWEXISTS(ProfileKey: PCHAR): BOOLEAN;

IMPLEMENTATION
VAR
  Wndw : PConfigDialog;
  running : BOOLEAN;

FUNCTION HWEXISTS(ProfileKey: PCHAR): BOOLEAN;
{===================================================================}
{                                                                   }
{===================================================================}
VAR portAssigned : ARRAY[0..16] OF CHAR;
BEGIN
	GetPrivateProfileString(appName, ProfileKey, 'NONE', portAssigned, 16, profFile);
  HWEXISTS := StrComp(portAssigned,'NONE') <> 0;
END;

CONSTRUCTOR TConfigDialog.Init(AParent : PWindowsObject; ATitle: PChar);
{===================================================================}
{                                                                   }
{===================================================================}
BEGIN
  TDialog.Init(AParent, ATitle);
  Restart := FALSE;
END;

DESTRUCTOR TConfigDialog.Done;
{===================================================================}
{                                                                   }
{===================================================================}
BEGIN
  running := FALSE;
  IF Restart THEN HALT;
  TDialog.Done;
END;

FUNCTION TConfigDialog.CanClose : BOOLEAN;
{===================================================================}
{                                                                   }
{===================================================================}
VAR temp1 : ARRAY[0..128] OF CHAR;
    temp2 : ARRAY[0..16] OF CHAR;
BEGIN
  CanClose := FALSE;
	IF TDialog.CanClose THEN
    IF ValidatePortAssignments THEN
  		BEGIN
       	GetStr(5001,temp1,128);
        GetStr(6001,temp2,16);
    	  IF MessageBox(HWindow, temp1,temp2, mb_OKCancel) = id_Cancel THEN EXIT;
	  		Restart := TRUE;
        CanClose := TRUE;
			END;
END;

FUNCTION TConfigDialog.GetPortAssignment(KeyName: PChar) : INTEGER;
{===================================================================}
{                                                                   }
{===================================================================}
VAR PortAssigned : ARRAY[0..16] OF CHAR;
BEGIN
  GetPortAssignment := 0;  { default to none }
	GetPrivateProfileString(appName, KeyName, 'NONE', PortAssigned, 16, profFile);
	IF StrComp(PortAssigned,'NONE') = 0 THEN
		GetPortAssignment := 0
	ELSE
		IF StrComp(PortAssigned,'GPIB') = 0 THEN
	  	GetPortAssignment := 1
	  ELSE
			IF StrComp(PortAssigned,'LPT1') = 0 THEN
		    GetPortAssignment := 2
	    ELSE
				IF StrComp(PortAssigned,'COM1') = 0 THEN
		      GetPortAssignment := 3
	      ELSE
					IF StrComp(PortAssigned,'COM2') = 0 THEN
		        GetPortAssignment := 4
  				ELSE
	  				IF StrComp(PortAssigned,'COM3') = 0 THEN
		          GetPortAssignment := 5;
END;

FUNCTION TConfigDialog.SetPort(inst : INTEGER) : BOOLEAN;
BEGIN
  SetPort := FALSE;
	CASE inst OF
		AS4     : BEGIN
							  IF SendDlgItemMsg(id_Com1AS4, bm_GetCheck, 0, 0) <> 0 THEN
								  AS4Port := COM1Port;
								IF SendDlgItemMsg(id_Com2AS4, bm_GetCheck, 0, 0) <> 0 THEN
                  IF AS4Port = noPort THEN
										AS4Port := COM2Port
                  ELSE SetPort := TRUE;
								IF SendDlgItemMsg(id_Com3AS4, bm_GetCheck, 0, 0) <> 0 THEN
 									IF AS4Port = noPort THEN
										AS4Port := COM3Port
                  ELSE SetPort := TRUE;
							END;
		WA      : BEGIN
								IF SendDlgItemMsg(id_GPIBWA, bm_GetCheck, 0, 0) <> 0 THEN
									WAPort := GPIBPort;
							END;
		TwoWay  : BEGIN
								IF SendDlgItemMsg(id_Com1TwoWay, bm_GetCheck, 0, 0) <> 0 THEN
								  TwoWayPort := COM1Port;
								IF SendDlgItemMsg(id_Com2TwoWay, bm_GetCheck, 0, 0) <> 0 THEN
                  IF TwoWayPort = noPort THEN
										TwoWayPort := COM2Port
                  ELSE SetPort := TRUE;
							END;
		Report  : BEGIN
								IF SendDlgItemMsg(id_Com1Report, bm_GetCheck, 0, 0) <> 0 THEN
									RepPrinterPort := COM1Port;
								IF SendDlgItemMsg(id_Com2Report, bm_GetCheck, 0, 0) <> 0 THEN
                  IF RepPrinterPort = noPort THEN
										RepPrinterPort := COM2Port
                  ELSE SetPort := TRUE;
								IF SendDlgItemMsg(id_Com3Report, bm_GetCheck, 0, 0) <> 0 THEN
									IF RepPrinterPort = noPort THEN
  									RepPrinterPort := COM3Port
									ELSE SetPort := TRUE;
								IF SendDlgItemMsg(id_LPT1Report, bm_GetCheck, 0, 0) <> 0 THEN
									IF RepPrinterPort = noPort THEN
	  								RepPrinterPort := LPT1Port
									ELSE SetPort := TRUE;
							END;
		Barcode : BEGIN
							  IF SendDlgItemMsg(id_LPT1Barcode, bm_GetCheck, 0, 0) <> 0 THEN
								  BCPrinterPort := LPT1Port;
  						END;
  END;
END;

FUNCTION TConfigDialog.ValidatePortAssignments : BOOLEAN;
{===================================================================}
{                                                                   }
{===================================================================}
VAR Com1Used, Com2Used, Com3Used, LPT1Used, conflict : BOOLEAN;
    pStr : ARRAY[0..128] OF CHAR;
BEGIN
	ValidatePortAssignments := TRUE;

	Com1Used := FALSE;
	Com2Used := FALSE;
	Com3Used := FALSE;
	LPT1Used := FALSE;
	Conflict := FALSE;

	AS4Port := noPort;
	WAPort := noPort;
	TwoWayPort := noPort;
	RepPrinterPort := noPort;
  BCPrinterPort := noPort;

	conflict := SetPort(AS4);
  IF NOT conflict THEN
		conflict := SetPort(WA);
	IF NOT conflict THEN
		conflict := SetPort(TwoWay);
	IF NOT conflict THEN
		conflict := SetPort(Report);
	IF NOT conflict THEN
		conflict := SetPort(Barcode);

	IF NOT conflict THEN
	  CASE AS4Port OF
		  Com1Port : Com1Used := TRUE;
		  Com2Port : Com2Used := TRUE;
		  Com3Port : Com3Used := TRUE;
	  END;

	IF NOT conflict THEN
	  CASE RepPrinterPort OF
		  Com1Port : IF Com1Used THEN conflict := TRUE
			  				 ELSE Com1Used := TRUE;
		  Com2Port : IF Com2Used THEN conflict := TRUE
			  				 ELSE Com2Used := TRUE;
		  Com3Port : IF Com3Used THEN conflict := TRUE
			  				 ELSE Com3Used := TRUE;
		  LPT1Port : IF LPT1Used THEN conflict := TRUE
			  				 ELSE LPT1Used := TRUE;
	  END;

	IF NOT conflict THEN
		BEGIN
    	CASE TwoWayPort OF
		    Com1Port : IF Com1Used THEN conflict := TRUE
				    			 ELSE Com1Used := TRUE;
    		Com2Port : IF Com2Used THEN conflict := TRUE
		    					 ELSE Com2Used := TRUE;
			END;

    	IF NOT conflict THEN
		    BEGIN
					IF BCPrinterPort = LPT1Port THEN
					  IF LPT1Used THEN conflict := TRUE
    				ELSE LPT1Used := TRUE;
				END;

			IF NOT conflict THEN
				BEGIN
					SetPortAssignment(pkAS4,AS4Port);
					SetPortAssignment(pkWalkAway,WAPort);
					SetPortAssignment(pkTwoWay,TwoWayPort);
					SetPortAssignment(pkReport,RepPrinterPort);
					SetPortAssignment(pkBarcode,BCPrinterPort);
					EXIT;
				END;
		END;

  GetStr(5002,pStr,128);
	MessageBox(HWindow, pStr,'', mb_OK);
	ValidatePortAssignments := FALSE;

END;

PROCEDURE TConfigDialog.SetPortAssignment(KeyName: PChar; KeyPort : INTEGER);
{===================================================================}
{                                                                   }
{===================================================================}
VAR PortAssigned : ARRAY[0..16] OF CHAR;
BEGIN
	CASE KeyPort OF
		NoPort   :  WritePrivateProfileString(appName, KeyName, 'NONE', profFile);
		GPIBPort :  WritePrivateProfileString(appName, KeyName, 'GPIB', profFile);
		LPT1Port :  WritePrivateProfileString(appName, KeyName, 'LPT1', profFile);
		COM1Port :  WritePrivateProfileString(appName, KeyName, 'COM1', profFile);
		COM2Port :  WritePrivateProfileString(appName, KeyName, 'COM2', profFile);
		COM3Port :  WritePrivateProfileString(appName, KeyName, 'COM3', profFile);
	END;

	IF StrComp(KeyName,pkAS4) = 0 THEN
		AS4Port := keyPort
	ELSE
		IF StrComp(KeyName,pkWalkAway) = 0 THEN
	  	WAPort := keyPort
	  ELSE
			IF StrComp(KeyName,pkReport) = 0 THEN
		    RepPrinterPort := keyPort
	    ELSE
				IF StrComp(KeyName,pkBarcode) = 0 THEN
					BCPrinterPort := keyPort
	      ELSE
					IF StrComp(KeyName,pkTwoWay) = 0 THEN
		        TwoWayPort := keyPort;
END;

PROCEDURE TConfigDialog.SetupWindow;
{===================================================================}
{                                                                   }
{===================================================================}
BEGIN

  AS4Port := GetPortAssignment(pkAS4);
  TwoWayPort := GetPortAssignment(pkTwoWay);
  BCPrinterPort := GetPortAssignment(pkBarcode);
  RepPrinterPort := GetPortAssignment(pkReport);
	WAPort := GetPortAssignment(pkWalkAway);

  { cant run the interface on COM3 because it uses windows drivers }
	SendDlgItemMsg(id_Com3TwoWay, bm_SetCheck, 0, 0);
	EnableWindow(GetItemHandle(id_Com3TwoWay),FALSE);
	EnableWindow(GetItemHandle(id_Com3Report),FALSE);

  SendDlgItemMsg(id_LPT1None, bm_SetCheck, 1, 0);
  SendDlgItemMsg(id_Com1None, bm_SetCheck, 1, 0);
  SendDlgItemMsg(id_Com2None, bm_SetCheck, 1, 0);
  SendDlgItemMsg(id_Com3None, bm_SetCheck, 1, 0);
  SendDlgItemMsg(id_GPIBNone, bm_SetCheck, 1, 0);

  SendDlgItemMsg(id_Com1AS4, bm_SetCheck, 0, 0);
  SendDlgItemMsg(id_Com2AS4, bm_SetCheck, 0, 0);
	SendDlgItemMsg(id_Com3AS4, bm_SetCheck, 0, 0);
	IF AS4Port <> noPort THEN
    CASE AS4Port OF
  		COM1Port : BEGIN
					  			 SendDlgItemMsg(id_Com1AS4, bm_SetCheck, 1, 0);
									 SendDlgItemMsg(id_Com1None, bm_SetCheck, 0, 0);
								 END;
			COM2Port : BEGIN
									 SendDlgItemMsg(id_Com2AS4, bm_SetCheck, 1, 0);
									 SendDlgItemMsg(id_Com2None, bm_SetCheck, 0, 0);
								 END;
			COM3Port : BEGIN
									 SendDlgItemMsg(id_Com3AS4, bm_SetCheck, 1, 0);
									 SendDlgItemMsg(id_Com3None, bm_SetCheck, 0, 0);
								 END;
		END;

  SendDlgItemMsg(id_Com1Report, bm_SetCheck, 0, 0);
	SendDlgItemMsg(id_Com2Report, bm_SetCheck, 0, 0);
  SendDlgItemMsg(id_Com3Report, bm_SetCheck, 0, 0);
	SendDlgItemMsg(id_LPT1Report, bm_SetCheck, 0, 0);
  IF repPrinterPort <> noPort THEN
    CASE RepPrinterPort OF
			COM1Port : BEGIN
									 SendDlgItemMsg(id_Com1Report, bm_SetCheck, 1, 0);
									 SendDlgItemMsg(id_Com1None, bm_SetCheck, 0, 0);
								 END;
			COM2Port : BEGIN
									 SendDlgItemMsg(id_Com2Report, bm_SetCheck, 1, 0);
									 SendDlgItemMsg(id_Com2None, bm_SetCheck, 0, 0);
								 END;
			COM3Port : BEGIN
									 SendDlgItemMsg(id_Com3Report, bm_SetCheck, 1, 0);
									 SendDlgItemMsg(id_Com3None, bm_SetCheck, 0, 0);
								 END;
			LPT1Port : BEGIN
									 SendDlgItemMsg(id_LPT1Report, bm_SetCheck, 1, 0);
									 SendDlgItemMsg(id_LPT1None, bm_SetCheck, 0, 0);
								 END;
    END;

  SendDlgItemMsg(id_LPT1Barcode, bm_SetCheck, 0, 0);
  IF RepPrinterPort <> LPT1Port THEN
		IF BCPrinterPort = LPT1Port THEN
      BEGIN
				SendDlgItemMsg(id_LPT1Barcode, bm_SetCheck, 1, 0);
				SendDlgItemMsg(id_LPT1None, bm_SetCheck, 0, 0);
			END;

  SendDlgItemMsg(id_Com1TwoWay, bm_SetCheck, 0, 0);
  SendDlgItemMsg(id_Com2TwoWay, bm_SetCheck, 0, 0);
	CASE TwoWayPort OF
		COM1Port : BEGIN
								 SendDlgItemMsg(id_Com1TwoWay, bm_SetCheck, 1, 0);
								 SendDlgItemMsg(id_Com1None, bm_SetCheck, 0, 0);
							 END;
		COM2Port : BEGIN
								 SendDlgItemMsg(id_Com2TwoWay, bm_SetCheck, 1, 0);
								 SendDlgItemMsg(id_Com2None, bm_SetCheck, 0, 0);
							 END;
  END;

  SendDlgItemMsg(id_GPIBWA, bm_SetCheck, 0, 0);
	IF WAPort = GPIBPort THEN
    BEGIN
			SendDlgItemMsg(id_GPIBWA, bm_SetCheck, 1, 0);
			SendDlgItemMsg(id_GPIBNone, bm_SetCheck, 0, 0);
		END;

END;

PROCEDURE Start(AParent : PWindowsObject);
{===================================================================}
{                                                                   }
{===================================================================}
BEGIN
  IF NOT running THEN
    BEGIN
      running := TRUE;
      Wndw := New(PConfigDialog, Init(AParent, 'HWCONFIGDLG'));
      WITH AParent^ DO
        Application^.ExecDialog(wndw);
    END;
END;

{===================================================================}
{                                                                   }
{===================================================================}
BEGIN
  Wndw := NIL;
  running := FALSE;
END.


