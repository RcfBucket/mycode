unit UICommon;

INTERFACE

uses
  MScan,
  DBLib,
  UIFlds,
  DMSErr,
  IntlLib,
  DBTypes,
  DBFile,
  DlgLib,
  ListLib,
  UserMsgs,
  ResLib,
  Objects,
  ODialogs,
  OWindows,
  WinTypes,
  WinProcs,
  Validate;

const
  MODE_NONE       = 0;
  MODE_NEW        = 1;
  MODE_EDIT       = 2;

  {- a UI dialog if using any of the following buttons must define them with
     the following IDs }
  IDC_UIDELETE    = 150;
  IDC_UINEXT      = 151;
  IDC_UIPREV      = 152;
  IDC_UISAVE      = 153;
  IDC_UICLEAR     = 154;

  RecallSpecKey   = VK_F5;    {- key used to recall last specimen }
  PrintKey        = VK_F6;    {- print key. Currently used in QE }

type
  PUICommonDlg = ^TUICommonDlg;
  TUICommonDlg = Object(TCenterDlg)
    flds      : PCollection;  {- collection of all user interface fields }
    listObj   : PListObject;
    preLoad   : TSeqNum;
    mode      : integer;
    modeTxt   : PStatic;      {- current mode text }
    modID     : word;
    waitCount : integer;
    origCurs  : HCursor;
    baseTitle : PChar;
    lastSpec  : array[0..15] of char;
    lastCD    : array[0..20] of char;
    {}
    constructor Init(aParent: PWindowsObject; aName: PChar; aSeq: TSeqNum);
    destructor Done; virtual;
    procedure DoFail(sID: word; errNum: integer; aSubst: PErrSubst);
    procedure SetupWindow; virtual;
    function GetID: integer; virtual;
    function CanClose: boolean; virtual;
    procedure Cancel(var msg: TMessage); virtual ID_FIRST + ID_CANCEL;
    function LinkUIFld(aFldNum: integer; isReq, isStatic: boolean; fldType: integer;
                       aResID, aLblID: word; dbd: PDBReadDesc): PUIFld;
    function FindField(anID: word): PUIFld;
    procedure InitModeText(anID: word);
    procedure WaitCursor(on: boolean);
    procedure SetMode(aMode: integer); virtual;
    function LockIt(db: PDBFile; aRec: PDBRec; msgID, msg2ID: word; var aMode: integer): boolean;

    {- methods that should be overwritten }
    procedure SetModuleID; virtual;
    function SaveData(clearAfter: boolean): boolean; virtual;
    function ExitAfterSave: boolean; virtual;
    procedure NextPrev(doNext: boolean); virtual;
    procedure DeleteData; virtual;
    procedure ClearFields; virtual;
    function  CheckSave(clearAfter: boolean): boolean; virtual;
    function DataChanged: boolean; virtual;
    procedure RecallLastSpecimen; virtual;
    procedure PrintData; virtual;
    procedure SetLastSpecimen(specID, specCD: PChar);

    {- button methods }
    procedure IDDelete(var msg: TMessage); virtual ID_FIRST + IDC_UIDELETE;
    procedure IDPrev(var msg: TMessage);   virtual ID_FIRST + IDC_UIPREV;
    procedure IDNext(var msg: TMessage);   virtual ID_FIRST + IDC_UINEXT;
    procedure IDSave(var msg: TMessage);   virtual ID_FIRST + IDC_UISAVE;
    procedure IDClear(var msg: TMessage);  virtual ID_FIRST + IDC_UICLEAR;

    procedure WMCtlColor(var msg: TMessage); virtual WM_FIRST + WM_CTLCOLOR;
    procedure WMUserKey(var msg: TMessage); virtual WM_FIRST + WM_USERKEY;
  end;

  PMoveIsoDlg = ^TMoveIsoDlg;
  TMoveIsoDlg = object(TCenterDlg)
    sess      : PUIFld;
    iFld      : PUIFld;
    sTxtFld   : PUIFld;
    iso       : PDBFile;
    listObj   : PListObject;
    {}
    constructor Init(aParent: PWindowsObject; aIso: PDBFile; aListObj: PListObject);
    procedure SetupWindow; virtual;
    function CanClose: boolean; virtual;
  end;

{$I UICOMMON.INC}
{$R UICOMMON.RES}

function SelectFamily(aParent: PWindowsObject; ordrSeq: TSeqNum;
                      var aSetFamily: word; var aTest: integer): boolean;
function CreateOrders(aParent: PWindowsObject; dbIso: PDBFile;
                      var isoOrd: PDBFile; results: PResults): boolean;
procedure PutAddTestInfo(results: PResults; aSetFamily: word; aTest: integer);
function CreateResults(dbIso: PDBFile; results: PResults;
                       aSetFamily: word; extraTst: integer): boolean;
function UpdateSpecIsos(iso: PDBFile; aIsoRec, aSpcRec: PDBRec): integer;

IMPLEMENTATION

uses
  CtlLib,
  APITools,
  DBIDS,
  DMString,
  Strings,
  Win31;

type
  PGroupInfo = ^TGroupInfo;
  TGroupInfo = object(TObject)
    seq     : TSeqNum;
    layout  : array[0..3] of char;
  end;

  PFamilyDlg = ^TFamilyDlg;
  TFamilyDlg = object(TCenterDlg)   {- dialog to edit family/add tests }
    rg      : PRadioGroup;
    tset    : byte;
    tclass  : byte;
    setVal  : ^word;
    list    : PComboBox;
    pnlSet  : PStatic;
    grp     : PGroupBox;
    test    : ^integer;
    isRapid : boolean;
    {}
    constructor Init(aParent: PWindowsObject; var setClass: word; var aTest: integer; aRapid: boolean);
    procedure SetupWindow; virtual;
    destructor Done; virtual;
    function CanClose: boolean; virtual;
    procedure IDList(var msg: TMessage); virtual ID_FIRST + IDC_FAMILYLIST;
  end;


constructor TUICommonDlg.Init(aParent: PWindowsObject; aName: PChar; aSeq: TSeqNum);
begin
  inherited Init(aParent, aName);

  preLoad:= aSeq;
  mode:= MODE_NONE;
  baseTitle:= nil;
  waitCount:= 0;
  strcopy(lastSpec, '');
  strcopy(lastCD, '');

  modeTxt:= nil;
  SetModuleID;
  flds:= New(PCollection, Init(25, 10));
  listObj:= New(PListObject, Init);
  if listObj = nil then
  begin
    DoFail(IDS_CANTINITLISTOBJ, 0, nil);
    fail;
  end;
end;

destructor TUICommonDlg.Done;
begin
  inherited Done;
  MSStrDispose(baseTitle);
  MSDisposeObj(flds);
  MSDisposeObj(listObj);
end;

procedure TUICommonDLg.DoFail(sID: word; errNum: integer; aSubst: PErrSubst);
begin
  ShowError(nil, sID, aSubst, errNum, modID, 0);
  Done;
end;

procedure TUICommonDlg.SetupWindow;

  procedure EnableIt(aFld: PUIFld); far;
  {- for all linked User defined fields, re-enable them since they are (should be)
     disabled and invisible in the resource }
  begin
    if aFld^.IsUD then
    begin
      aFld^.Show(true);
      aFld^.Enable(true);
    end;
  end;

var
  pstr  : array[0..200] of char;
begin
  inherited SetupWindow;

  GetWindowText(HWindow, pstr, sizeof(pstr)-1);
  baseTitle:= StrNew(pstr);

  flds^.ForEach(@EnableIt);
  ClearFields;
  SetMode(MODE_NONE);
end;

function TUICommonDlg.CanClose: boolean;
begin
  CanClose:= CheckSave(false);
end;

procedure TUICommonDlg.Cancel(var msg: TMessage);
begin
  if CanClose then
    inherited Cancel(msg)
  else
    msg.result:= 0;
end;

function TUICommonDlg.GetID: integer;
{- return module ID }
begin
  GetID:= modID;
end;

function TUICommonDlg.LinkUIFld(aFldNum: integer; isReq, isStatic: boolean; fldType: integer;
                           aResID, aLblID: word; dbd: PDBReadDesc): PUIFld;
var
  fld : PUIFld;
begin
  if isStatic then
    fld:= New(PUIStaticFld, Init(@self, dbd, listObj, aFldNum, aResID, aLblID))
  else if fldType = UICheckType then
    fld:= New(PUICheckFld, Init(@self, dbd, aFldNum, aResID, true))
  else
    fld:= New(PUIFld, Init(@self, dbd, listObj, isReq, aFldNum, aResID, aLblID));
  if fld = nil then
  begin
    ShowError(nil, IDS_UIFLDINITERR, nil, 0, modID, 0);
    Halt;
  end;
  flds^.Insert(fld); {- add field to field list collection }
  LinkUIFld:= fld;
end;

function TUICommonDlg.FindField(anID: word): PUIFld;
{- Find a field that matches the ID specified. If the field is not found then
   nil will be returned }

  function Matches(aFld: PUIFld): boolean; far;
  begin
    Matches:= aFld^.GetID = anID;
  end;

begin
  FindField:= flds^.FirstThat(@Matches);
end;

procedure TUICommonDlg.ClearFields;
{- clear all fields on screen }

  procedure ClearIt(aFld: PUIFld); far;
  begin
    aFld^.Clear;
  end;

begin
  SetMode(MODE_NONE);
  flds^.ForEach(@ClearIt);
end;

procedure TUICommonDlg.SetModuleID;
{- this method must be overwritten to set the MODID member to the appropriate
   value }
begin
  ABSTRACT;
end;

procedure TUICommonDlg.WaitCursor(on: boolean);
{- display a wait cursor }
begin
  if not on then
  begin
    if waitCount > 0 then
    begin
      Dec(waitCount);
      if waitCount = 0 then
        SetCursor(origCurs);
    end;
  end
  else
  begin
    if waitCount = 0 then
      origCurs:= SetCursor(LoadCursor(0, IDC_WAIT));
    Inc(waitCount);
  end;
end;

procedure TUICommonDlg.SetMode(aMode: integer);
{- set the current mode. If a mode is negative, the mode will not be changed,
   but the mode text will be updated }
var
  pstr  : array[0..50] of char;
begin
  if aMode >= MODE_NONE then
    mode:= aMode;
  if modeTxt <> nil then
    case mode of
      MODE_NEW  : modeTxt^.SetText(SR(IDS_NEW, pstr, 50));
      MODE_EDIT : modeTxt^.SetText(SR(IDS_EDIT, pstr, 50));
      else
      begin
        modeTxt^.SetText('');
      end;
    end;
  EnableButtons;
end;

procedure TUICommonDlg.InitModeText(anID: word);
{- initialize the mode static control }
begin
  modeTxt:= New(PStatic, InitResource(@self, anID, 0));
end;

procedure TUICommonDlg.WMCtlColor(var msg: TMessage);
begin
  if (modeTxt <> nil) and (msg.lParamLo = modeTxt^.HWindow) then
  begin
    SetTextColor(msg.wParam, RGB($FF,0,0));
    msg.result:= GetStockObject(WHITE_BRUSH);
    SetWindowLong(HWindow, DWL_MSGRESULT, msg.result);  {- set message result (for dialogs) }
  end
  else
    DefWndProc(msg);
end;

procedure TUICommonDlg.WMUserKey(var msg: TMessage);
begin
  if msg.wParam = RecallSpecKey then
    RecallLastSpecimen
  else if msg.wParam = PrintKey then
    PrintData;
end;

procedure TUICommonDlg.NextPrev(doNext: boolean);
begin
  ABSTRACT;
end;

function TUICommonDlg.SaveData(clearAfter: boolean): boolean;
begin
  SaveData:= true;
end;

function TUICommonDlg.ExitAfterSave: boolean;
{- return true if dialog should exit after save button is pressed. This is
   usually true when the dialog is used as a Zoom  dialog }
begin
  ExitAfterSave:= (preLoad > 0);
end;

procedure TUICommonDlg.DeleteData;
begin
end;

function TUICommonDlg.DataChanged: boolean;
begin
end;

function TUICommonDlg.CheckSave(clearAfter: boolean): boolean;
{- Checks to see if any data needs to be saved. This is when any data on the screen
   has changed from the original record that was loaded. This function returns
   true if : 1. No saving is required, 2. The data changed and was saved.
             3. No mode has been established.
             4. The user opts to not save.
   false if: 1. The user attemps to save the data but an error occured.
             2. The user cancels }
begin
  CheckSave:= true;
end;

procedure TUICommonDlg.RecallLastSpecimen;
{- this routine is called when the user presses the recall last specimen key.
   Descendents (who care) can overwrite this method to process the request by
   reloading the last specimen that was saved. Descendents that do process this
   request should maintain the lastSpec and lastCD data members in this object.
   To do this they can call SetLastSpec() when a user saves specimen data}
begin
end;

procedure TUICommonDlg.PrintData;
{- this routine is called when the user presses the print key.
   Descendents (who care) can overwrite this method to process the request
   and printing whatever their hearts desire }
begin
end;

procedure TUICommonDlg.SetLastSpecimen(specID, specCD: PChar);
begin
  if specID = nil then
    StrCopy(lastSpec, '')
  else
    StrLCopy(lastSpec, specID, sizeof(lastSpec)-1);
  if specCD = nil then
    StrCopy(lastCD, '')
  else
    StrLCopy(lastCD, specCD, sizeof(lastCD)-1);
end;

procedure TUICommonDlg.IDClear(var msg: TMessage);
begin
  if CheckSave(true) then
    ClearFields;
end;

procedure TUICommonDlg.IDSave(var msg: TMessage);
begin
  if SaveData(true) then
    if ExitAfterSave then
      EndDlg(ID_OK);
end;

procedure TUICommonDlg.IDNext(var msg: TMessage);
{- go to the next specimen. If there is no currently loaded specimen, then go
   to the first specimen }
begin
  NextPrev(true);
end;

procedure TUICommonDlg.IDPrev(var msg: TMessage);
begin
  NextPrev(false);
end;

procedure TUICommonDlg.IDDelete(var msg: TMessage);
begin
  DeleteData;
end;

function TUICommonDlg.LockIt(db: PDBFile; aRec: PDBRec; msgID, msg2ID: word; var aMode: integer): boolean;
var
  p1, p2  : array[0..200] of char;
  errNum  : integer;
  ret     : boolean;
begin
  ret:= true;
  if not db^.dbc^.GetLock(aRec) then
  begin
    errNum:= db^.dbc^.dbErrorNum;
    case errNum of  {- checks for special errors }
      MOD_BTRV + 84:
        begin
          ShowError(@self, msgID, nil, errNum, modID, 0);
          ret:= false;
        end;
    end;
    if ret then
    begin
      {- record not found - should I re-create it ? }
      SR(msg2ID, p2, 200);
      SR(IDS_TRYRECREATE, p1, 200);
      strcat(p2, #10#10);
      strcat(p2, p1);
      if YesNoMsg(HWindow, SR(IDS_WARNING, p1, 200), p2) then
      begin
        aMode:= MODE_NEW;
        ret:= true;
      end
      else
        ret:= false;
    end;
  end;
  LockIt:= ret;
end;

constructor TMoveIsoDlg.Init(aParent: PWindowsObject; aIso: PDBFile; aListObj: PListObject);
var
  c   : PLStatic;
begin
  inherited Init(aParent, MakeIntResource(DLG_MOVEISO));
  iso:= aIso;
  listObj:= aListObj;
  iFld:= New(PUIStaticFld, Init(@self, iso^.dbd, nil, DBIsoIso, IDC_TXTISO, IDC_ISOLBL));
  sTxtFld:= New(PUIStaticFld, Init(@self, iso^.dbd, listObj, DBIsoSess, IDC_TXTSESS, IDC_SESSLBL));
  sess:= New(PUIFld, Init(@self, iso^.dbd, listObj, false, DBIsoSess, IDC_NEWSESS, 0));
  c:= New(PLStatic, InitResource(@self, IDC_SPECLBL, 0, false));
end;

procedure TMoveIsoDlg.SetupWindow;
var
  seq : TSeqNum;
  rec : PDBRec;
  pstr: array[0..50] of char;
  fi  : TFldInfo;
begin
  inherited SetupWindow;
  iFld^.XFerRecToCtl(iso^.dbr);
  sTxtFld^.XFerRecToCtl(iso^.dbr);
  sess^.XFerRecToCtl(iso^.dbr);
  iso^.dbr^.GetField(DBIsoSpecRef, @seq, sizeof(seq));
  rec:= listObj^.PeekSeq(DBSpecFile, seq);
  if rec <> nil then
  begin
    rec^.GetFieldAsStr(DBSpecSpecID, pstr, sizeof(pstr)-1);
    SendDlgItemMsg(IDC_TXTSPEC, WM_SETTEXT, 0, longint(@pstr));
    rec^.desc^.FieldInfo(DBSpecSpecID, fi);
    SendDlgItemMsg(IDC_SPECLBL, WM_SETTEXT, 0, longint(@fi.fldName));
  end
  else
    SendDlgItemMsg(IDC_SPECLBL, WM_SETTEXT, 0,0);
end;

function TMoveIsoDlg.CanClose;
var
  k   : integer;
begin
  k:= sess^.Format;
  if k <> 0 then
  begin
    ShowUIFldError(@self, k, sess);
    CanClose:= false;
  end
  else
  begin
    CanClose:= true;
    sess^.XFerCtlToRec(iso^.dbr);
  end;
end;

{-------------------------------------------------------------[ TFamilyDlg ]--}

constructor TFamilyDlg.Init(aParent: PWindowsObject; var setClass: word; var aTest: integer; aRapid: boolean);
var
  c   : PControl;
begin
  if not (ExtractSet(setClass) in [1, 2, 20, 5, 21, 3]) then
  begin
    ShowError(aParent, IDS_CANTEDITFAMILY, nil, 0, MOD_UIISO, 0);
    fail;
  end;

  inherited Init(aParent, MakeIntResource(DLG_FAMILY));
  setVal:= @setClass;
  aTest:= 0;  {- assume "no test" }
  test:= @aTest;
  isRapid:= aRapid;

  c:= New(PLStatic, InitResource(@self, IDC_LBLPANELSET, 0, false));
  c:= New(PLStatic, InitResource(@self, IDC_FAMILYLBL, 0, false));
  pnlSet:= New(PStatic, InitResource(@self, IDC_PANELSET, 0));
  grp:= New(PGroupBox, InitResource(@self, IDC_ADDTESTGRP));

  list:= New(PComboBox, InitResource(@self, IDC_FAMILYLIST, 0));
  rg:= New(PRadioGroup, Init(@self, IDC_NOTEST, 0, false));
  rg^.AddToGroup(IDC_NEGATIVE, 1);
  rg^.AddToGroup(IDC_POSITIVE, 2);
end;

destructor TFamilyDlg.Done;
begin
  inherited Done;
  MSDisposeObj(rg);
end;

procedure TFamilyDlg.SetupWindow;
var
  k     : integer;
  pstr  : array[0..100] of char;
  p1    : array[0..100] of char;
  tmp   : word;
begin
  inherited SetupWindow;

  tset:= ExtractSet(setVal^);
  tClass:= ExtractFamily(setVal^);

  ExtractSetAsStr(setVal^, pstr, sizeof(pstr)-1);
  pnlSet^.SetText(pstr);
  ExtractFamilyAsStr(setVal^, pstr, sizeof(pstr)-1);
  tmp:= setVal^;

  case tset of
    1, 20:
      begin
        grp^.SetCaption(SR(IDS_OXIDASE, p1, sizeof(p1)-1));
        EnableWindow(list^.HWindow, false);
        EnableWindow(GetDlgItem(HWindow, IDC_FAMILYLBL), false);
      end;
    5,2,21:
      begin
        pnlSet^.SetText(SR(IDS_GRAMPOSITIVE, p1, sizeof(p1)-1));
        grp^.SetCaption(SR(IDS_BETA_HEMOLYSIS, p1, sizeof(p1)-1));
        for k:= 1 to 2 do
        begin
          tmp:= (tmp and $FF00) + k;
          list^.AddString(ExtractFamilyAsStr(tmp, pstr, sizeof(pstr)-1));
        end;
      end;
    3:
      begin
        pnlSet^.SetCaption(SR(IDS_ANAEROBE, p1, sizeof(p1)-1));
        grp^.SetCaption(SR(IDS_INDOLE, p1, sizeof(p1)-1));
        for k:= 1 to 4 do
        begin
          tmp:= (tmp and $FF00) + k;
          list^.AddString(ExtractFamilyAsStr(tmp, pstr, sizeof(pstr)-1));
        end;
      end;
  end;
  if tClass <> 0 then
    list^.SetSelIndex(tClass - 1)
  else
    list^.SetSelIndex(0);

  rg^.SetSelected(test^);
  if tSet in [2, 5, 21] then
  begin
    if (isRapid) then
    begin
      rg^.EnableGroup(false);
      rg^.SetSelected(-1);
      test^:= 0;
    end
    else
    begin
      rg^.EnableGroup(list^.GetSelIndex = 0);
      rg^.Enable(0, false);
      test^:= 1;  {- default to negative }
      if list^.GetSelIndex = 0 then
        rg^.SetSelected(test^)
      else
        rg^.SetSelected(-1);
    end;
    EnableWindow(grp^.HWindow, rg^.IsGroupEnabled);
  end;
end;

procedure TFamilyDlg.IDList(var msg: TMessage);
var
  aSet  : byte;
  k     : integer;
begin
  if msg.lParamHi = CBN_SELCHANGE then
  begin
    aSet:= ExtractSet(setVal^);
    if (aSet = 2) or (aSet = 5) or (aSet = 21) then
    begin
      if (isRapid) then
      begin
        rg^.EnableGroup(false);
        EnableWindow(grp^.HWindow, false);
      end
      else
      begin
        rg^.EnableGroup(list^.GetSelIndex = 0);
        EnableWindow(grp^.HWindow, rg^.IsGroupEnabled);
        if rg^.IsGroupEnabled then
        begin
          rg^.Enable(0, false); {- disable no test for Beta Hemolysis }
          rg^.SetSelected(test^)
        end
        else
          rg^.SetSelected(-1);
      end;
    end;
  end;
  DefWndProc(msg);
end;

function TFamilyDlg.CanClose: boolean;
begin
  {Get the current SetClass}
  if list^.GetSelIndex >= 0 then
    setVal^:= (setVal^ and $FF00) + (list^.GetSelIndex + 1)
  else
    setVal^:= setVal^ and $FF00;

  {Evaluate the test flags}
  tset:= ExtractSet(setVal^);
  tClass:= ExtractFamily(setVal^);
  test^:= integer(rg^.SelectedVal);
  if test^ < 0 then
    test^:= 0;
  if (TSet in [2, 5, 21]) and (TClass = 2) then  {Strip off BetaHemolysis result for Micrococacea}
    test^:= 0;

  CanClose:= true;
end;

procedure ScanOrder(ordrSeq: TSeqNum; oInfo: PCollection);
{- scan and gather info on the test order }
var
  dbo     : PDBFile;
  dbg     : PDBFile;
  grpSeq  : TSeqNum;
  found   : boolean;
  grpRec  : PGroupInfo;
begin
  oInfo^.FreeAll;

  dbo:= New(PDBFile, Init(DBORDXRFFile, '', dbOpenNormal));
  if dbo = nil then
    exit;
  dbg:= New(PDBFile, Init(DBTSTGRPFile, '', dbOpenNormal));
  if dbg = nil then
  begin
    MSDisposeObj(dbo);
    exit;
  end;

  dbo^.dbr^.PutField(DBORDXRFOrdRef, @ordrSeq);
  if dbo^.dbc^.GetFirstContains(dbo^.dbr) then
  begin
    repeat
      dbo^.dbr^.GetField(DBORDXRFTstGrpRef, @grpSeq, sizeof(grpSeq));
      if dbg^.dbc^.GetSeq(dbg^.dbr, grpSeq) then
      begin
        grpRec:= New(PGroupInfo, Init);
        grpRec^.seq:= grpSeq;
        oInfo^.Insert(grpRec);
        dbg^.dbr^.GetFieldAsStr(DBTSTGRPLayout, grpRec^.layout, sizeof(grpRec^.layout)-1);
      end;
    until not dbo^.dbc^.GetNextContains(dbo^.dbr);
  end;

  MSDisposeObj(dbo);
  MSDisposeObj(dbg);
end;

function SelectFamily(aParent: PWindowsObject; ordrSeq: TSeqNum;
                      var aSetFamily: word; var aTest: integer): boolean;
{- prompt for family and extra test information }
var
  d       : PDialog;
  tSet    : integer;
  promptForTest     : boolean;
  promptForFamily   : boolean;
  oInfo   : PCollection;
  rapids  : boolean;

  function IDExist: boolean;
  {- see if an ID panel exists in the order }
    function Matches(item: PGroupInfo): boolean; far;
    begin
      Matches:= (item^.layout[1] = 'I');
    end;

  begin
    IDExist:= oInfo^.FirstThat(@Matches) <> nil;
  end;

  function RapidExist: boolean;
  {- see if a rapid ID panel exists in the order }

    function Matches(item: PGroupInfo): boolean; far;
    begin
      Matches:= (item^.layout[0] = 'R');
    end;

  begin
    RapidExist:= oInfo^.FirstThat(@Matches) <> nil;
  end;

  function ComboExist: boolean;
  {- see if a combo panel exists in the order }
    function Matches(item: PGroupInfo): boolean; far;
    begin
      Matches:= (item^.layout[1] = 'C');
    end;

  begin
    ComboExist:= oInfo^.FirstThat(@Matches) <> nil;
  end;

begin
  oInfo:= New(PCollection, Init(10, 10));
  {- get order information (panels in the order) }
  ScanOrder(ordrSeq, oInfo);

  promptForTest:= false;  {- prompt for OXI, BetaH, IND }
  promptForFamily:= false;

  rapids:= RapidExist;

  {- first thing is to find out if prompting is even neccessary }
  tSet:= ExtractSet(aSetFamily);
  if tSet in [1, 20] then   {- neg panels ordered }
  begin
    if (IDExist or ComboExist) and not RapidExist then
      promptForTest:= true;
  end
  else if tSet in [2, 5, 21] then  {- pos }
  begin
    {- someday, if Rapid Pos Mic are supported, we must prompt for family }
    if (IDExist or ComboExist or not RapidExist) then
        promptForFamily:= true;
    promptForTest:= RapidExist and promptForFamily;
  end
  else if tSet = 3 then  {- anarobe }
  begin
    if IDExist or ComboExist then
      promptForFamily:= true;
    promptForTest:= promptForFamily;
  end;

  if promptForTest or promptForFamily then
  begin
    d:= New(PFamilyDlg, Init(aParent, aSetFamily, aTest, rapids));
    SelectFamily:= application^.ExecDialog(d) = IDOK;
  end
  else
    SelectFamily:= true;

  MSDisposeObj(oInfo);
end;

function CreateOrders(aParent: PWindowsObject; dbIso: PDBFile;
                      var isoOrd: PDBFile; results: PResults): boolean;
{- create isolate orders and blank results record.
{ if an order was made -
    open the order xref file.
    for each tst order in the order
      create an Iso Order
  else
    Dont do anything.
  NOTE: this routine opens, but does NOT close the Isolate order file. This is
        because it is opened in a transaction and cannot be closed inside a
        transaction. The file will be closed at the end of the main procedure.
}
var
  orderDef  : PDBFile;
  isoSeq    : TSeqNum;
  tstOrdSeq : TSeqNum;
  orderSeq  : TSeqNum;
  sret      : boolean;
  tSeq      : TSeqNum;
begin
  sret:= true;
  dbIso^.dbr^.GetField(DBIsoOrdRef, @orderSeq, sizeof(orderSeq));
  if orderSeq <> 0 then
  begin
    isoSeq:= dbIso^.dbr^.GetSeqValue;
    {- open order definition file }
    orderDef:= New(PDBFile, Init(DBOrdXrfFile, '', dbOpenNormal));
    if orderDef = nil then
    begin
      sret:= false;
      ShowError(aParent, IDS_CANTOPENORDERDEF, nil, dbLastOpenError, MOD_UICOMMON, 0);
    end
    else
    begin
      {- open isolate orders file }
      isoOrd:= New(PDBFile, Init(DBIsoOrdFile, '', dbOpenNormal));
      if isoOrd = nil then
      begin
        ShowError(aParent, IDS_CANTOPENISOORD, nil, dbLastOpenError, MOD_UICOMMON, 0);
        sret:= false;
      end;
    end;

    {- now lets get on with it }
    if sret then
    begin
      orderDef^.dbr^.PutField(DBOrdXrfOrdRef, @orderSeq);
      {- find the first order definition record }
      if orderDef^.dbc^.GetFirstContains(orderDef^.dbr) then
      begin
        repeat
          {- get the test order from the order definition }
          orderDef^.dbr^.GetField(DBOrdXrfTstGrpRef, @tstOrdSeq, sizeOf(tstOrdSeq));

          {- now I have the test order reference and the isolate seq num.
            Time to create the Isolate order }
          isoOrd^.dbr^.ClearRecord;
          isoOrd^.dbr^.PutField(DBIsoOrdIsoRef, @isoSeq);
          isoOrd^.dbr^.PutField(DBIsoOrdTstGrpRef, @tstOrdSeq);
          if not isoOrd^.dbc^.InsertRec(isoOrd^.dbr) then
          begin
            ShowError(aParent, IDS_CANTCREATEISOORD, nil, isoOrd^.dbc^.dbErrorNum, MOD_UICOMMON, 0);
            sret:= false;
          end;
          orderDef^.dbr^.ClearField(DBOrdXrfTstGrpRef);
        until not sret or not orderDef^.dbc^.GetNextContains(orderDef^.dbr);
      end
      else
      begin
        ShowError(aParent, IDS_CANTLOCORDDEF, nil, orderDef^.dbc^.dbErrorNum, MOD_UICOMMON, 0);
        sret:= false;
      end;
    end;

    {- go ahead and close orderDef since no modications have been done to it }
    MSDisposeObj(orderDef);
  end;
  CreateOrders:= sret;
end;

procedure PutAddTestInfo(results: PResults; aSetFamily: word; aTest: integer);
{- puts the test into the results list. aTest depends on the current setFamily
   aTest = 0 for no test, 1 for negative, or 2 for positive. These values will
   be converted to the proper string format for results then depending on the
   setFamily aTest will be stored as Oxidase, Indole or BetaHem }
var
  pstr  : array[0..30] of char;
begin
  if aTest = 1 then
    StrCopy(pstr, '-')
  else if aTest = 2 then
    StrCopy(pstr, '+')
  else
    StrCopy(pstr, '');

  case ExtractSet(aSetFamily) of
    1 :       {- gram negs - need oxidase }
      results^.SetResult('.OXI', 'OXI', pstr, true);
    2, 5, 21: {- gram pos - need beta hemo (maybe) }
      results^.SetResult('.HEM', 'HEM', pstr, true);
    3:        {- anaerobe - need indole }
      results^.SetResult('.IND', 'IND', pstr, true);
  end;
end;

function CreateResults(dbIso: PDBFile; results: PResults;
                       aSetFamily: word; extraTst: integer): boolean;
{- create blank results and insert any additional test (if any) }
{- this should only be called when creating an new isolate.
{  if no order was made then skip this step }
var
  sret      : boolean;
  orderSeq  : TSeqNum;
begin
  dbIso^.dbr^.GetField(DBIsoOrdRef, @orderSeq, sizeof(orderSeq));
  if orderSeq <> 0 then
  begin
    sret:= results^.LoadResults(dbIso^.dbr);
    if sret then
    begin
      PutAddTestInfo(results, aSetFamily, extraTst);
      results^.SaveResults;
    end;
  end
  else
    sRet:= true;
  CreateResults:= sret;
end;

{-----------------------------------------------------------------------------}

function UpdateSpecIsos(iso: PDBFile; aIsoRec, aSpcRec: PDBRec): integer;
{- use this function to update isolate records that belong to a specimen.
   This function will ensure that the duplicate (redundant) data in the isolate
   file (specID and collect date) are updated. This function should be wrapped in
   a transaction!

   sSpcRec      - Specimen record containing specimen info
   iso         - A dbFile object for isolates.
   aIsoRec      - If nil, this function will use the record (dbr) in Iso, otherwise
                  aIsoRec will be used for the data transfers to the database
   Returns -1 if parameters passed are in error
   0 if function was successful, >0 for any database error.

   Note: This routine changes the current key number.
}
var
  ret       : integer;
  seq       : TSeqNum;
  isoRec    : PDBRec;
  id        : array[0..50] of char;
  cd        : TIntlDate;
begin
  ret:= 0;
  if (aSpcRec = nil) or (iso = nil) then
    ret:= -1
  else
  begin
    if aIsoRec = nil then
      isoRec:= iso^.dbr
    else
      isoRec:= aIsoRec;

    seq:= aSpcRec^.GetSeqValue;
    aSpcRec^.GetField(DBSpecSpecID, @id, sizeof(id)-1);
    aSpcRec^.GetField(DBSpecCollectDate, @cd, sizeof(cd));

    iso^.dbc^.SetCurKeyNum(DBISO_ID_KEY);
    isoRec^.ClearRecord;
    isoRec^.PutField(DBIsoSpecRef, @seq);
    if iso^.dbc^.GetFirstContains(isoRec) then
    begin
      repeat
        isoRec^.PutField(DBIsoSpecID, @id);
        isoRec^.PutField(DBIsoCollectDate, @cd);
        if not iso^.dbc^.UpdateRec(isoRec) then
          ret:= iso^.dbc^.dbErrorNum;
      until (ret <> 0) or not iso^.dbc^.GetNextContains(isoRec);
    end;
  end;

  UpdateSpecIsos:= ret;
end;


END.

{- rcf }
