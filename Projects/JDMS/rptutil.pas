unit RptUtil;

{.$DEFINE KANJI}

{- Report Generator Utilities }
{
  Defines specified in this unit -
  KANJI - If defined, the unit will be compiled to set up fonts appropriate for
          JWindows. If not defined, this unit will create default fonts for
          English windows.

}

INTERFACE

uses
  SpinEdit,
  GridLib,
  DBLib,
  RptShare,
  RptData,
  WinTypes,
  ODialogs,
  OWindows,
  Objects;

type
  PBoxTool = ^TBoxTool;
  TBoxTool = object(TObject)
    parent    : PWindowsObject;
    dc        : HDC;
    x1, x2    : integer;
    y1, y2    : integer;
    pen       : HPen;
    lppx, lppy: integer;
    units     : TRptUnits;
    oldCaption: PChar;
    capSize   : integer;
    origin    : TPoint;
    max       : TPoint;
    zoom      : real;
    stopped   : boolean;
    {}
    constructor Init(AParent: PWindowsObject; startX, startY: integer;
                     AOrigin, AMax: TPoint; AZoom: real; AUnits: TRptUnits);
    destructor Done; virtual;
    procedure GetRect(var r: TRect);
    procedure MouseMove(x, y: integer);
    procedure DisplaySize(r: TRect);
    procedure Stop;
  end;

  PCheckGrid = ^TCheckGrid;
  TCheckGrid = object(TGrid)
    checkCol  : integer;
    {}
    constructor InitResource(aParent: PWindowsObject; anID: integer; aCols: integer;
                             bold: boolean; aCheckCol: integer);
    procedure ToggleCheck; virtual;
    procedure SetCheck(aRow: integer; checked: boolean);
    function GetCheck(aRow: integer): boolean;
    procedure WMChar(var msg: TMessage); virtual WM_FIRST + WM_CHAR;
    procedure WMDblClk(var msg: TMessage); virtual WM_FIRST + WM_LBUTTONDBLCLK;
  end;

procedure LTwipsToUnits(lTwips: longint; var units: real);
procedure UnitsToLTwips(units: real; var lTwips: longint);
procedure PixelsToUnits(px, ppi: integer; var units: real);
procedure UnitsStr(abbrev: boolean; pstr: PChar; maxLen: integer);
function  GetAFont(HWindow: HWnd; var rptFont: TRptFont): boolean;
procedure MakeDefaultRFont(var rFont: TRptFont);
procedure FillLineTypes(DlgHwnd: HWnd; AList: PComboBox);
function GetWidth(AParent: PWindowsObject; prompt: PChar; var width: longint; max: longint): boolean;
function GetSectOpt(AParent: PWindowsObject; var opt: TSectOpt): boolean;
procedure FillIsoFlds(db: PDBReadDesc; flds, sel: PGrid; var fldList: TRptFldList);
function ExtractWidth(parent: PWindowsObject; ctl: PSpinreal; errIdx: integer;
                      var aVal: longint; aMin, aMax: longint): boolean;
function ExtractNum(parent: PWindowsObject; ctl: PSpinInt; errIdx: integer;
                    var aVal: integer; aMin, aMax: integer): boolean;

IMPLEMENTATION

uses
  DMSDebug,
  DBTypes,
  DBErrors,
  MScan,
  ApiTools,
  ctllib,
  modWin,
  DlgLib,
  strsw,
  DMSErr,
  strings,
  prninfo,
  IntlLib,
  CommDlg,
  WinProcs;

{$I RPTGEN.INC}

procedure FillLineTypes(dlgHwnd: HWnd; AList: PComboBox);
{- fill a combo box with available line types. Itemdata will
   have the line style or -1 for none}
var
  pstr  : array[0..100] of char;
  k, i  : integer;
begin
  SR(IDS_LineNone, pstr, 100);

  i:= aList^.AddString(pstr);
  SendDlgItemMessage(DlgHWnd, aList^.GetID, CB_SETITEMDATA, i, -1);

  for k:= IDS_FirstLineType to IDS_LastLineType do
  begin
    SR(k, pstr, 100);
    i:= AList^.AddString(pstr);
    SendDlgItemMessage(DlgHWnd, aList^.GetID, CB_SETITEMDATA, i, k - IDS_FirstLineType);
  end;
end;

procedure UnitsToLTwips(units: real; var lTwips: longint);
{- convert a number to Twips based on the currently selected
   units (in, mm, cm) }
begin
  case preferences.units of
    rptInch : lTwips:= Round(units * 1440.0);
    rptCm : lTwips:= Round(units / 2.54 * 1440.0);
    rptMM : lTwips:= Round(units / 25.4 * 1440.0);
  end;
end;

procedure LTwipsToUnits(lTwips: longint; var units: real);
{- convert given LTwips to the currently selected units }
begin
  units:= lTwips / 1440.0;  {- units in inches }
  case preferences.units of
    rptMM: units:= units * 25.4;
    rptCM: units:= units * 2.54;
  end;
  units:= Round(units * 100) / 100.0;
end;

procedure PixelsToUnits(px, ppi: integer; var units: real);
{- convert given pixels to the currently selected units. ppi should
   be the pixels per inch conversion factor for the pixels to convert }
begin
  units:= px / ppi; {- units in inches }

  case preferences.units of
    rptMM: units:= units * 25.4;
    rptCM: units:= units * 2.54;
  end;
end;

procedure UnitsStr(abbrev: boolean; pstr: PChar; maxLen: integer);
{- return a string for the selected unit type. if abbrev is true
   then return unit abbreviation, otherwise return full name }
var
  idx  : integer;
begin
  case preferences.units of
    rptInch:
      if abbrev then
        idx:= ids_in
      else
        idx:= ids_inches;
    rptMM:
      if abbrev then
        idx:= ids_MM
      else
        idx:= ids_millimeters;
    rptCM:
      if abbrev then
        idx:= ids_CM
      else
        idx:= ids_centimeters;
    else
      idx:= ids_in;
  end;
  SR(idx, pstr, maxlen);
end;

function GetAFont(HWindow: HWnd; var rptFont: TRptFont): boolean;
{- prompt the user for a font. This function uses the Common Dialog interface.
   If the user selects a font, the function returns true and the selected font
   is converted to a TRptFont }
var
  cf  : TChooseFont;
  tlf : TLogFont;
  dc  : HDC;
  lppy: integer;
begin
  dc:= GetDC(0);
  lppy:= GetDeviceCaps(dc, LOGPIXELSY);
  ReleaseDC(0, dc);

  tlf:= rptFont.lf;
  tlf.lfHeight:= -Trunc(rptFont.ptSize / 10.0 * lppy / 72.0);

  FillChar(cf, sizeof(cf), 0);
  with cf do
  begin
    lStructSize:= sizeof(TChooseFont);
    hWndOwner:= HWindow;
    hdc:= 0;
    lpLogFont:= @tlf;
    rgbColors:= rptFont.color;
    flags:= CF_SCREENFONTS or
            CF_INITTOLOGFONTSTRUCT or
            CF_SCALABLEONLY;
  end;
  if ChooseFont(cf) then
  begin
    GetAFont:= true;
    rptFont.ptSize:= cf.iPointSize;
    rptFont.color:= cf.rgbColors;
    rptFont.lf:= tlf;
    rptFont.lf.lfHeight:= -Trunc(rptFont.ptSize / 10 * 20);  {convert to LTWips }
  end
  else
    GetAFont:= false;
end;

{----------------------------------------------------[ TBoxTool] --}

constructor TBoxTool.init(AParent: PWindowsObject; startX, startY: integer;
                          AOrigin, AMax: TPoint; AZoom: real; AUnits: TRptUnits);
var
  r : TRect;
begin
  inherited Init;
  x1:= startX;
  y1:= startY;
  x2:= startX;
  y2:= startY;
  units:= AUnits;
  parent:= AParent;
  origin:= AOrigin;
  zoom:= AZoom;
  max:= AMax;

  SetCapture(parent^.HWindow);
  dc:= GetDC(parent^.HWindow);
  lppx:= GetDeviceCaps(dc, LOGPIXELSX);
  lppy:= GetDeviceCaps(dc, LOGPIXELSY);

  pen:= CreatePen(PS_DOT, 0, RGB(0,0,0));
  SelectObject(dc, pen);
  SetROP2(dc, r2_Not);

  MoveTo(dc, x1, y1);
  SelectObject(dc, GetStockObject(Null_Brush));
  Rectangle(dc, x1, y1, x2, y2);

  capSize:= GetWindowTextLength(parent^.HWindow);
  if capSize > 0 then
  begin
    Inc(capSize);
    GetMem(oldCaption, capSize);
    GetWindowText(parent^.HWindow, oldCaption, capSize);
  end
  else
    oldCaption:= nil;
  GetRect(r);
  DisplaySize(r);
  stopped:= false;
end;

destructor TBoxTool.done;
var
  r  : TRect;
begin
  stopped:= true;
  GetRect(r);
  DrawFocusRect(dc, r);
  ReleaseDC(parent^.HWindow, dc);
  ReleaseCapture;
  DeleteObject(pen);
  if oldCaption = nil then
    SetWindowText(parent^.HWindow, '')
  else
  begin
    SetWindowText(parent^.HWindow, oldCaption);
    MSFreeMem(oldCaption, capSize);
  end;
  inherited Done;
end;

procedure TBoxTool.Stop;
begin
  ReleaseCapture;
  stopped:= true;
end;

procedure TBoxTool.DisplaySize(r: TRect);
var
  pstr : array[0..200] of char;
  p2   : array[0..200] of char;
  num  : real;
  subst: PErrSubst;

  function MakeNum: real;
  {- convert ANum to inches or mm }
  begin
    if units = rptMM then
      MakeNum:= num * 25.4 / zoom
    else if units = rptCM then
      MakeNum:= num * 2.54 / zoom
    else
      MakeNum:= num / zoom;
  end;

begin
  SR(IDS_STATMSG, p2, sizeof(p2)-1);

  { Left }
  num:= (r.left - origin.x) / lppx;
  IntlrealToStr(MakeNum, pstr, sizeof(pstr)-1);
  subst:= New(PErrSubst, Init(pstr));

  { Top }
  num:= (r.top - origin.y) / lppy;
  IntlrealToStr(MakeNum, pstr, sizeof(pstr)-1);
  subst^.AddSubstStr(pstr);

  { Width }
  num:= (r.right - r.left) / lppx;
  IntlrealToStr(MakeNum, pstr, sizeof(pstr)-1);
  subst^.AddSubstStr(pstr);

  { Height }
  num:= (r.bottom - r.top) / lppy;
  IntlrealToStr(MakeNum, pstr, sizeof(pstr)-1);
  subst^.AddSubstStr(pstr);

  { units (mm, cm, in) }
  UnitsStr(true, pstr, 100);
  subst^.AddSubstStr(pstr);

  WVSPrintf(pstr, p2, subst^.strs);

  MSDisposeObj(subst);

  SetWindowText(parent^.HWindow, pstr);
end;

procedure TBoxTool.GetRect(Var r: TRect);
{- return the current rectangle }
begin
  if x2 < x1 then
  begin
    r.left:= x2; r.right:= x1
  end
  else
  begin
    r.left:= x1; r.right:= x2;
  end;
  if y2 < y1 then
  begin
    r.top:= y2; r.bottom:= y1
  end
  else
  begin
    r.top:= y1; r.bottom:= y2;
  end;
end;

procedure TBoxTool.MouseMove(x, y: integer);
var
  r  : TRect;
begin
  {- ugly as it may be, the following formulae are used to limit
     the box being drawn to max.x and max.y (passed in at init).
     Basically, Abs(x - x1) is the current magnitude of the box,
     and (x - x1) / Abs(x - x1) is the direction.
     If the magnitude is greater than the max size then
     subtract (or add) the difference into the current position.
  }
  if abs(x - x1) > max.x then
    Inc(x, (max.x - Abs(x-x1))*((x-x1) div Abs(x-x1)) );

  if abs(y - y1) > max.y then
    Inc(y, (max.y - Abs(y-y1))*((y-y1) div Abs(y-y1)) );

  GetRect(r);
  DrawFocusRect(dc, r);
  x2:= x;
  y2:= y;
  GetRect(r);
  DrawFocusRect(dc, r);
  DisplaySize(r);
end;

{$IFDEF KANJI}

{- create a font suitable for JWindows }
procedure MakeDefaultRFont(var rFont: TRptFont);
var
  dc : HDC;
  f  : HFont;
begin
  FillChar(rFont, sizeof(rfont), 0);
  rFont.ptSize:= 100;  {- ten times point size }
  rFont.lf.lfWeight:= FW_NORMAL;
  rFont.lf.lfPitchAndFamily:= FF_MODERN;
  rFont.lf.lfcharSet:= SHIFTJIS_CHARSET;
  rFont.lf.lfHeight:= -Trunc(rFont.ptSize / 10 * 20);  { convert to LTWips }
  rFont.color:= RGB(0,0,0); {- black }

  {- get the face name windows assigns to the font }
  dc:= GetDC(0);
  f:= CreateFontIndirect(rFont.lf);
  SelectObject(dc, f);
  GetTextFace(dc, lf_FaceSize, rFont.lf.lfFaceName);
  ReleaseDC(0, dc);
  DeleteObject(f);
end;

{$ELSE}

{- create a default font for section windows }
procedure MakeDefaultRFont(var rFont: TRptFont);
var
  dc : HDC;
  f  : HFont;
begin
  FillChar(rFont, sizeof(rfont), 0);
  rFont.lf.lfPitchAndFamily:= FF_SWISS;
  rFont.ptSize:= 100;  {- ten times point size}
  rFont.lf.lfWeight:= FW_NORMAL;
  rFont.lf.lfHeight:= -Trunc(rFont.ptSize / 10 * 20);  {convert to LTWips }
  strcopy(rFont.lf.lfFaceName, 'Arial');
  rFont.color:= RGB(0,0,0); {- black }

  {- get the face name windows assigns to the font }
  dc:= GetDC(0);
  f:= CreateFontIndirect(rFont.lf);
  SelectObject(dc, f);
  GetTextFace(dc, lf_FaceSize, rFont.lf.lfFaceName);
  ReleaseDC(0, dc);
  DeleteObject(f);
end;

{$ENDIF}

{------------------------------------------------------------[ TCheckGrid ]--}

constructor TCheckGrid.InitResource(aParent: PWindowsObject; anID: integer; aCols: integer;
                                    bold: boolean; aCheckCol: integer);
begin
  inherited InitResource(aParent, anID, aCols, bold);
  if (aCheckCol >= 0) and (aCheckCol < aCols) then
    checkCol:= aCheckCol
  else
    checkCol:= -1;
end;

procedure TCheckGrid.ToggleCheck;
var
  pstr  : array[0..20] of char;
  k     : integer;
begin
  if checkCol >= 0 then
  begin
    k:= GetSelIndex;
    if k >= 0 then
    begin
      GetData(checkCol, k, pstr, 20);
      if (strlen(pstr) = 0) then
        strcopy(pstr, 'X')
      else
        pstr[0]:= #0;
      SetData(checkCol, k, pstr);
    end;
  end;
end;

function TCheckGrid.GetCheck(aRow: integer): boolean;
var
  pstr  : array[0..10] of char;
begin
  if checkCol <> -1 then
  begin
    GetData(checkCol, aRow, pstr, sizeof(pstr)-1);
    GetCheck:= StrComp(pstr, 'X') = 0;
  end
  else
    GetCheck:= false;
end;

procedure TCheckGrid.SetCheck(aRow: integer; checked: boolean);
var
  pstr  : array[0..10] of char;
begin
  if checkCol <> -1 then
  begin
    if checked then
      StrCopy(pstr, 'X')
    else
      StrCopy(pstr, '');
    SetData(checkCol, aRow, pstr);
  end;
end;

procedure TCheckGrid.WMDblClk(var msg: TMessage);
begin
  ToggleCheck;
  DefWndProc(msg);
end;

procedure TCheckGrid.WMChar(var msg: TMessage);
begin
  if GetSelIndex <> -1 then
    ToggleCheck;
  DefWndProc(msg);
end;

{---------------------------------[ TWidthDlg ]--}

type
  PWidthDlg = ^TWidthDlg;
  TWidthDlg = object(TCenterDlg)
    prompt  : PChar;
    ed      : PSpinreal;
    width   : ^longint; {- width in LTwips }
    max     : longint;  {- max width (in LTwips) }
    constructor Init(AParent: PWindowsObject; APrompt: PChar; var AWidth: longint; aMax: longint);
    procedure SetupWindow; virtual;
    function CanClose: boolean; virtual;
  end;

constructor TWidthDlg.Init(AParent: PWindowsObject; APrompt: PChar; var aWidth: longint; aMax: longint);
var
  mreal : real;
  c     : PControl;
begin
  inherited Init(AParent, MakeIntResource(DLG_WIDTH));
  LTwipsToUnits(aMax, mreal);
  ed:= New(PSpinreal, InitResource(@self, IDC_WIDTH, 0,0, 0, mreal, 0.1));
  c:= New(PLStatic, InitResource(@self, 300, 0, false));
  c:= New(PLStatic, InitResource(@self, 400, 0, false));
  width:= @AWidth;
  prompt:= APrompt;
  max:= aMax;
end;

procedure TWidthDlg.SetupWindow;
var
  pstr  : array[0..100] of char;
  num   : real;
begin
  inherited SetupWindow;
  SendDlgItemMsg(100, WM_SETTEXT, 0, longint(prompt));

  UnitsStr(false, pstr, 100);
  SendDlgItemMsg(300, WM_SETTEXT, 0, longint(@pstr));

  LTwipsToUnits(width^, num);
  ed^.SetNum(num);
end;

function TWidthDlg.Canclose: boolean;
var
  pstr  : array[0..100] of char;
  num   : real;
  code  : integer;
  temp  : longint;
begin
  CanClose:= false;
  if ed^.FldIsBlank then
  begin
    ShowError(@self, IDS_WIDTHREQ, nil, 0, MOD_RPTGEN, 0);
    FocusCtl(HWindow, ed^.GetID);
  end
  else if not ed^.NumIsValid or (ed^.GetDouble < 0) then
  begin
    ShowError(@self, IDS_INVWIDTH, nil, 0, MOD_RPTGEN, 0);
    FocusCtl(HWindow, ed^.GetID);
  end
  else
  begin
    num:= ed^.GetDouble;
    UnitsToLTwips(num, temp);
    if temp > max then
    begin
      ShowError(@self, IDS_WIDTHTOOLARGE, nil, 0, MOD_RPTGEN, 0);
      FocusCtl(HWindow, ed^.GetID);
    end
    else
    begin
      CanClose:= true;
      width^:= temp;
    end;
  end;
end;

function GetWidth(AParent: PWindowsObject; prompt: PChar; var width: longint; max: longint): boolean;
{- get a width from the user.
   aMax  - the maximum width (in LTwips) }
var
  p   : PWidthDlg;
begin
  p:= New(PWidthDlg, Init(AParent, prompt, width, max));
  GetWidth:= application^.ExecDialog(p) = IDOK;
end;

type
  PSectOptDlg = ^TSectOptDlg;
  TSectOptDlg = object(TDMSDlg)
    border: PComboBox;
    opt   : PSectOpt;
    constructor Init(AParent: PWindowsObject; var AnOpt: TSectOpt);
    procedure SetupWindow; virtual;
    procedure OK(var msg: TMessage); virtual id_First + id_OK;
  end;

{----------------------[ Section Options ]--}

constructor TSectOptDlg.Init(AParent: PWindowsObject; var AnOpt: TSectOpt);
var
  c  : PControl;
begin
  inherited Init(AParent, MakeIntResource(DLG_SECTOPT));
  border:= New(PComboBox, InitResource(@self, IDC_BORDERSTYLE, 0));
  c:= New(PLRadioButton, InitResource(@self, IDC_OPAQUE, false));
  c:= New(PLRadioButton, InitResource(@self, IDC_XPARENT, false));
  c:= New(PLStatic, InitResource(@self, 400, 0, false));

  opt:= @anOpt;
end;

procedure TSectOptDlg.SetupWindow;
var
  p2, pstr  : array[0..100] of char;
  k         : integer;
  j         : longint;
begin
  inherited SetupWindow;

  SendDlgItemMsg(IDC_OPAQUE, BM_SETCHECK, bf_Unchecked, 0);
  SendDlgItemMsg(IDC_XPARENT, BM_SETCHECK, bf_Unchecked, 0);

  if opt^.bkMode = OPAQUE then
    SendDlgItemMsg(IDC_OPAQUE, BM_SETCHECK, bf_checked, 0)
  else
    SendDlgItemMsg(IDC_XPARENT, BM_SETCHECK, bf_checked, 0);

  FillLineTypes(HWindow, border);
  border^.SetSelIndex(0);

  for k:= 0 to border^.GetCount - 1 do
  begin
    j:= SendDlgItemMsg(border^.GetID, CB_GETITEMDATA, k, 0);
    if integer(j) = opt^.border then
      border^.SetSelIndex(k);
  end;
end;

procedure TSectOptDlg.OK(var msg : TMessage);
begin
  if SendDlgItemMsg(IDC_OPAQUE, BM_GETCHECK, 0, 0) = bf_checked then
    opt^.bkMode:= OPAQUE
  else
    opt^.bkMode:= TRANSPARENT;
  opt^.border:= integer(SendDlgItemMsg(IDC_BORDERSTYLE, CB_GETITEMDATA,
                        border^.GetSelIndex, 0));
  inherited OK(msg);
end;

function GetSectOpt(AParent: PWindowsObject; var opt: TSectOpt): boolean;
var
  p : PDialog;
begin
  p:= New(PSectOptDlg, Init(AParent, opt));
  GetSectOpt:= Application^.ExecDialog(p) = IDOK;
end;

procedure FillIsoFlds(db: PDBReadDesc; flds, sel: PGrid; var fldList: TRptFldList);
{- fill grids with isolate fields and selected isolate fields. Grids are
   filled with the field name in column 0, and item data equal to the
   fields number.  Also, assumes that flds is a two column grid, the
   first col is hidden and contains the field number (converted to char for sorting),
   the second holds the field name.
   flds    - grid to contain unselected isolate fields
   sel     - grid to contain selected isolate fields
   fldList - list of selected isolate fields }
var
  k,j : integer;
  fld : integer;
  pstr: array[0..100] of char;
  num : real;
  fi  : TFldInfo;

  function FindFld(AFld: integer): boolean;
  {- return true if field is selected in current configuration }
  var
    i     : integer;
    found : boolean;
  begin
    found:= false;
    i:= 1;
    while not found and (i <= fldList[0]) do
    begin
      found:= fldList[i] = AFld;
      Inc(i);
    end;
    FindFld:= found;
  end;

begin
  {- item data will hold field number }
  flds^.SetRedraw(false);
  sel^.SetRedraw(false);

  flds^.ClearList;
  sel^.ClearList;

  for k:= 1 to fldList[0] do
  begin
    fld:= fldList[k];
    db^.FieldInfo(fld, fi);
    j:= sel^.AddString('');
    sel^.SetData(0, j, fi.fldName);
    sel^.SetItemData(j, fld);
  end;

  for k:= 1 to db^.NumFields do  {- iterate thru fields }
  begin
    if not FindFld(k) then
    begin
      db^.FieldInfo(k, fi);
      if fi.userFlag and DB_HIDDEN = 0 then {- do not show hidden fields }
      begin
        pstr[0]:= Chr(ord('@') + k);  {- set a sort order key for field }
        pstr[1]:= #0;
        j:= flds^.AddString(pstr);
        flds^.SetData(0, j, fi.fldName);
        flds^.SetItemData(j, k);  {- item data equals the field number }
      end;
    end;
  end;

  flds^.SetRedraw(true);
  sel^.SetRedraw(true);
end;

function ExtractWidth(parent: PWindowsObject; ctl: PSpinreal; errIdx: integer;
                      var aVal: longint; aMin, aMax: longint): boolean;
{- errIdx is an index to a string that has 3 "%s" format chars. The first
   will be replaced with the minimum, the second with the max and the third with
   the currently selected units. }
var
  err   : boolean;
  nstr  : array[0..50] of char;
  num   : real;
  max   : real;
  ret   : boolean;
  subst : PErrSubst;
begin
  ret:= false;
  num:= ctl^.GetDouble;
  LTwipsToUnits(aMax, max);
  if ctl^.FldIsBlank or not ctl^.NumIsValid or
     (num < aMin) or (num > max) then
  begin
    LTwipsToUnits(aMin, num);
    IntlrealToStr(num, nstr, 20);
    subst:= New(PErrSubst, Init(nStr));

    LTwipsToUnits(aMax, num);
    IntlrealToStr(num, nstr, 20);
    subst^.AddSubstStr(nstr);

    UnitsStr(false, nstr, 20);
    subst^.AddSubstStr(nstr);
    ret:= false;
    ShowError(parent, errIdx, subst, 0, MOD_RPTGEN, 0);
    FocusCtl(parent^.HWindow, ctl^.GetID);
    MSDisposeObj(subst);
  end
  else
  begin
    UnitsToLTwips(num, aVal);
    ret:= true;
  end;
  ExtractWidth:= ret;
end;

function ExtractNum(parent: PWindowsObject; ctl: PSpinInt; errIdx: integer;
                    var aVal: integer; aMin, aMax: integer): boolean;
{- errIdx is an index to a string that has 2 "%s" format chars. The first
   will be replaced with the minimum, the second with the max. }
var
  err   : boolean;
  nstr  : array[0..50] of char;
  num   : longint;
  ret   : boolean;
  subst : PErrSubst;
begin
  ret:= false;
  num:= ctl^.GetLong;
  if ctl^.FldIsBlank or not ctl^.NumIsValid or
     (num < aMin) or (num > aMax) then
  begin
    IntlIntToStr(aMin, nstr, 20);
    subst:= New(PErrSubst, Init(nstr));

    IntlIntToStr(aMax, nstr, 20);
    subst^.AddSubstStr(nstr);

    ShowError(parent, errIdx, subst, 0, MOD_RPTGEN, 0);

    ret:= false;
    FocusCtl(parent^.hWindow, ctl^.GetID);
  end
  else
  begin
    aVal:= num;
    ret:= true;
  end;
  ExtractNum:= ret;
end;

END.

{- rcf }
