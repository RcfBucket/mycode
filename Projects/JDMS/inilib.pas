unit IniLib;

{----------------------------------------------------------------------------}
{  Module Name  : INILIB.PAS                                                 }
{  Programmer   : everybody                                                  }
{  Date Created : 03-95                                                      }
{                                                                            }
{  Purpose - This unit defines the name for the DMS's INI file. It also      }
{    provides functions for accessing some of the keys in the file.          }
{                                                                            }
{  Assumptions -                                                             }
{  None                                                                      }
{                                                                            }
{  Initialization -                                                          }
{                                                                            }
{  Revision History -                                                        }
{                                                                            }
{  Version  Date      Who    Did What                                        }
{  -------  --------  ------ ----------------------------------------------- }
{  1.00     03-95     rcf    Started the module                              }
{  PVCS     PVCS      SM     Added the INIFileName constant                  }
{                                                                            }
{----------------------------------------------------------------------------}

INTERFACE

{---- These functions access some of the keys in the INI file. The functions
      are not intended for global use, but were put here to help manage the
      layout of the INI file. Since then, the INIFileName constant was added
      and it is now recommended that new functions use the constant and be put
      in the implementation section of the unit that requires it.
}
function INIUse3D: boolean;
function INIShowSeconds: boolean;
function INISpecimenListSort: integer;
function INIListOnPartialID: boolean;
function INIGridStretchFull: boolean;
function INIGetLastSession(lastSess: PChar; maxLen: integer): PChar;
procedure INISetLastSession(lastSess: PChar);
function ReadWarningThreshold: byte;
function ReadPreventionThreshold: byte;
procedure WriteWarningThreshold(threshold: byte);
procedure WritePreventionThreshold(threshold: byte);
procedure ReadFileMaintExe(exeStr: PChar; maxLen: integer);
function  INIGetAutoUploadWA: boolean;
procedure INISetAutoUploadWA(flag: boolean);
procedure INIGetBackupID(Id: PChar);
procedure INISetBackupID(Id: PChar);
procedure INIGetExtractID(Id: PChar);
procedure INISetExtractID(Id: PChar);
function INIAvgColumnFluff: real;
function INIGridHeaderPad: integer;
function INIDefPointSize: integer;
function INIWAEPIPointSize: integer;
function INIScreenPointSize: integer;
function INIGridStatusCutoff: longint;
function INIGridStatusBlockSize: longint;
function INIVerifyBackup: boolean;


{---- This is the master definition for the name of the program's INI file. No
      other occurrences of 'DMS.INI' should exist.
}
const
  INIFileName = 'DMS.INI';

IMPLEMENTATION {-------------------------------------------------------------}

uses
  Strings,
  WinProcs,
  WinTypes;

function INIUse3D: boolean;
{- return TRUE if an application is to use 3D windows/dialogs. Default=TRUE }
var
  pstr    : array[0..100] of char;
begin
  GetPrivateProfileString('General', 'Use3D', 'YES', pstr, 100, INIFileName);
  INIUse3D:= StrIComp(pstr, 'YES') = 0;
end;

function INISpecimenListSort: integer;
{- returns the default specimen sort order. Default is Spec/CollectDate }
var
  pstr  : array[0..100] of char;
  num   : integer;
  code  : integer;
begin
  GetPrivateProfileString('General', 'SpecimenListSort', '1', pstr, 100, INIFileName);
  Val(pstr, num, code);
  if (code <> 0) or (num <> 2) then
    INISpecimenListSort:= 1
  else
    INISpecimenListSort:= 2;
end;

function INIShowSeconds: boolean;
{- return true if TimeToString functions should include seconds }
var
  pstr  : array[0..100] of char;
begin
  GetPrivateProfileString('General', 'ShowSeconds', 'NO', pstr, 100, INIFileName);
  INIShowSeconds:= StrIComp(pstr, 'YES') = 0;
end;

function INIListOnPartialID: boolean;
{ return TRUE if an application is to pull up a selection grid when a partial ID is searched for }
var
  pstr    : array[0..100] of char;
begin
  GetPrivateProfileString('General', 'PartialIDList', 'NO', pstr, 100, INIFileName);
  INIlistOnPartialID:= (StrIComp(pstr, 'YES') = 0);
end;

function INIGridStretchFull: boolean;
{ return TRUE if columns will be stretched to edge of grid                 }
{ return FALSE if columns will be stretched to edge of vertical scroll bar }
var
  pstr    : array[0..100] of char;
begin
  GetPrivateProfileString('General', 'GridStretchFull', 'YES', pstr, 100, INIFileName);
  INIGridStretchFull:= (StrIComp(pstr, 'YES') = 0);
end;

function ReadWarningThreshold: byte;
var
  tempArray  : array [0..255] of char;
  tempVal    : byte;
  code       : integer;
begin
  GetPrivateProfileString('DMS','Warning Threshold','90',tempArray,255,INIFileName);
  Val(tempArray, tempVal, code);
  if code > 0 then
    ReadWarningThreshold := 90
  else
    ReadWarningThreshold := tempVal;
end;

function ReadPreventionThreshold: byte;
var
  tempArray  : array [0..255] of char;
  tempVal    : byte;
  code       : integer;
begin
  GetPrivateProfileString('DMS', 'Prevention Threshold','91', tempArray, 255, INIFileName);
  Val(tempArray, tempVal, code);
  if code > 0 then
    ReadPreventionThreshold := 90
  else
    ReadPreventionThreshold := tempVal;
end;

procedure WriteWarningThreshold(threshold: byte);
var
  tempArray  : array [0..255] of char;
begin
  Str(threshold, tempArray);
  WritePrivateProfileString('DMS', 'Warning Threshold', tempArray, INIFileName);
end;

procedure WritePreventionThreshold(threshold: byte);
var
  tempArray  : array [0..255] of char;
begin
  Str(threshold,tempArray);
  WritePrivateProfileString('DMS','Prevention Threshold',tempArray,INIFileName);
end;

function INIGetLastSession(lastSess: PChar; maxLen: integer): PChar;
{- this function retrieves the last session used from the INI file. If no session
   has been used, then it returns an empty string for the NONE session otherwise
   it returns the ID of the last used session }
begin
  GetPrivateProfileString('DMS', 'Last Session', '', lastSess, maxLen, INIFileName);
  INIGetLastSession:= lastSess;
end;

procedure INISetLastSession(lastSess: PChar);
{- sets the last session in use in the INI file. The last sess parameter should
   be the ID of the session that was used last }
var
  pstr  : array[0..100] of char;
begin
  if lastSess = nil then
    StrCopy(pstr, '')
  else
    StrLCopy(pstr, lastSess, 100);
  WritePrivateProfileString('DMS', 'Last Session', pstr, INIFileName);
end;

procedure ReadFileMaintExe(exeStr: PChar; maxLen: integer);
{- this function returns the directory path and name(including extension) }
{  of the File Maintenance application.                                   }
begin
  GetPrivateProfileString('DMS', 'File Maintenance', 'c:\dms\FilMaint.exe', exeStr, maxLen, INIFileName);
end;

function  INIGetAutoUploadWA: boolean;
{- return TRUE if DMSWA application is to start auto upload of panel data. Default=FALSE }
var
  pstr    : array[0..100] of char;
begin
  GetPrivateProfileString('DMS', 'AutoUploadWA', 'NO', pstr, 100, INIFileName);
  INIGetAutoUploadWA := StrIComp(pstr, 'YES') = 0;
end;

procedure INISetAutoUploadWA(flag: boolean);
begin
  if flag then
    WritePrivateProfileString('DMS', 'AutoUploadWA', 'YES', INIFileName)
  else
    WritePrivateProfileString('DMS', 'AutoUploadWA', 'NO', INIFileName);
end;

procedure INIGetBackupID(id: PChar);
{- this function retrieves the last backup from the INI file. -}
begin
  GetPrivateProfileString('DMS', 'FullBackup', '0', id, 100, INIFileName);
end;

procedure INISetBackupID(id: PChar);
{- sets the backup ID in the INI file. The last backup parameter should
   be the ID of next backup that will be used for the next full backup -}
var
  pstr  : array[0..100] of char;
begin
  if id = nil then
    StrCopy(pstr, '0')
  else
    StrLCopy(pstr, id, 100);
  WritePrivateProfileString('DMS', 'FullBackup', pstr, INIFileName);
end;

procedure INIGetExtractID(id: PChar);
{- this function retrieves the last extract from the INI file. -}
begin
  GetPrivateProfileString('DMS', 'Extract', '0', id, 100, INIFileName);
end;

procedure INISetExtractID(id: PChar);
{- sets the extract ID in the INI file. The last extract parameter should
   be the ID of next extract that will be used for the next extract -}
var
  pstr  : array[0..100] of char;
begin
  if id = nil then
    StrCopy(pstr, '0')
  else
    StrLCopy(pstr, id, 100);
  WritePrivateProfileString('DMS', 'Extract', pstr, INIFileName);
end;

Function INIGetBackupVersion: Integer;
{- this function retrieves the backup version from the INI file. -}
{ allows detection of whether data conversion must be performed on the backup }
Var
  StrBackupVersion : Array[0..10] of char;
  NumBackupVersion : Integer;
  code             : Integer;
begin
  INIGetBackupVersion := 0;
  GetPrivateProfileString('DMS', 'BackupVersion', '0', StrBackupVersion, 100, INIFileName);
  Val(StrBackupVersion,NumBackupVersion,code);
  INIGetBackupVersion := NumBackupVersion;
end;

function INIAvgColumnFluff: real;
var
  pstr  : array[0..50] of char;
  num   : real;
  code  : integer;
begin
  GetPrivateProfileString('DMS', 'AvgColFluff', '0.33', pstr, sizeof(pstr)-1, INIFileName);
  Val(pstr, num, code);
  if (code <> 0) or (num < 0.0) or (num > 3.0) then
    num:= 0.33;
  INIAvgColumnFluff:= num + 1.0;  {- make ret val a multiplier }
end;

function INIGridHeaderPad: integer;
{- return the amount of pixels to add to the grid headers for padding.
   Header size will be the vertical size of the font plus this value }
var
  pstr  : array[0..50] of char;
  num   : integer;
  code  : integer;
begin
  GetPrivateProfileString('DMS', 'GridHeadPad', '5', pstr, sizeof(pstr)-1, INIFileName);
  Val(pstr, num, code);
  if (code <> 0) or (num < 0) or (num > 10) then
    num:= 5;
  INIGridHeaderPad:= num;
end;

function INIDefPointSize: integer;
{- return the default point size used in reports }
var
  pstr  : array[0..50] of char;
  num   : integer;
  code  : integer;
begin
  GetPrivateProfileString('General', 'DefPointSize', '10', pstr, sizeof(pstr)-1, INIFileName);
  Val(pstr, num, code);
  if (code <> 0) or (num <= 0) or (num > 72) then
    num:= 10;
  INIDefPointSize:= num;
end;

function INIWAEPIPointSize: integer;
{- return the default point size used in CPSR }
var
  pstr  : array[0..50] of char;
  num   : integer;
  code  : integer;
begin
  {$IFDEF KANJI}
  GetPrivateProfileString('General', 'WAEPIPointSize', '10', pstr, sizeof(pstr)-1, INIFileName);
  {$ELSE}
  GetPrivateProfileString('General', 'WAEPIPointSize', '8', pstr, sizeof(pstr)-1, INIFileName);
  {$ENDIF}
  Val(pstr, num, code);
  if (code <> 0) or (num <= 0) or (num > 72) then
    num:= 10;
  INIWAEPIPointSize:= num;
end;

function INIScreenPointSize: integer;
{- return the default point size used on screens }
var
  pstr  : array[0..50] of char;
  num   : integer;
  code  : integer;
begin
  GetPrivateProfileString('General', 'ScreenPointSize', '10', pstr, sizeof(pstr)-1, INIFileName);
  Val(pstr, num, code);
  if (code <> 0) or (num <= 0) or (num > 14) then
    num:= 10;
  INIScreenPointSize:= num;
end;

function INIGridStatusCutoff: longint;
{ If a file has more records than the returned value, than a status bar will be }
{ displayed during the filling of a grid for the file.                          }
var
  pstr  : array[0..50] of char;
  num   : integer;
  code  : integer;
begin
  GetPrivateProfileString('General', 'GridStatusCutoff', '100', pstr, sizeof(pstr)-1, INIFileName);
  Val(pstr, num, code);
  if (code <> 0) then
    num:= 100;
  INIGridStatusCutoff:= num;
end;

function INIGridStatusBlockSize: longint;
{ When are being filled, the returned value represents the number of records read }
{ between updates to the status bar.                                              }
var
  pstr  : array[0..50] of char;
  num   : integer;
  code  : integer;
begin
  GetPrivateProfileString('General', 'GridStatusBlockSize', '10', pstr, sizeof(pstr)-1, INIFileName);
  Val(pstr, num, code);
  if (code <> 0) then
    num:= 10
  else if (num = 0) then
    num:= 1; {prevent division by zero }
  INIGridStatusBlockSize:= num;
end;

function INIVerifyBackup: boolean;
{- return TRUE if application is to verify backup and restore. Default=TRUE }
var
  pstr    : array[0..100] of char;
begin
  GetPrivateProfileString('DMS', 'VerifyBackup', 'YES', pstr, 100, INIFileName);
  INIVerifyBackup:= StrIComp(pstr, 'YES') = 0;
end;

END.
