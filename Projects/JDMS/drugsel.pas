{----------------------------------------------------------------------------}
{  Module Name  : DrugSel.PAS                                                }
{  Programmer   : RCF                                                        }
{  Date Created : 07/13/95                                                   }
{  Requirements :                                                            }
{                                                                            }
{  Purpose - This unit provides a function for getting a set of drugs from   }
{    the user.                                                               }
{                                                                            }
{  Assumptions -                                                             }
{                                                                            }
{  Referenced Documents                                                      }
{    a. Software Specification for Nihongo Data Management System v4.00      }
{                                                                            }
{  Revision History - This project is under version control, use it to view  }
{    revision history.                                                       }
{                                                                            }
{----------------------------------------------------------------------------}

unit DrugSel;

INTERFACE

uses
  OWindows,
  DBTypes;

const
  DrugsAcross = 11;

type
  PDrugArray = ^TDrugArray;
  TDrugArray = array [ 1..DrugsAcross ] of TSeqNum;


function GetDrugArray( pParent: PWindowsObject; aDrugArray: PDrugArray ):
  boolean;


IMPLEMENTATION

uses
  APITools,
  CtlLib,
  DMString,
  DBIDs,
  DMSErr,
  DBFile,
  DlgLib,
  GridLib,
  MScan,
  ODialogs,
  Screens,
  Strings,
  Trays,
  UIFlds,
  WinTypes,
  WinProcs,
  Win31;

{$I DRUGSEL.INC}
{$R DRUGSEL.RES}

const
  {--------------------------------------------------------------------------}
  { This constant sets the length of the list box line buffer.
  {--------------------------------------------------------------------------}
  LBL_LEN  = 80;



                        {----------------------}
                        {     Save Dialog      }
                        {----------------------}



type
  {--------------------------------------------------------------------------}
  { This object represents the dialog box used to save a set of drugs.
  {--------------------------------------------------------------------------}
  PSaveSelDlg = ^TSaveSelDlg;
  TSaveSelDlg = object( TCenterDlg )

    { Interface }
    constructor Init( AParent: PWindowsObject; aDrugArray: PDrugArray;
      dbSeln: PDBFile );
    destructor Done; virtual;

    { Implementation }
    private
    pIDEdit: PUIFld;
    drugArray: PDrugArray;
    pSelectionDB: PDBFile;      { pointer to external object }
    descEdit: PUIFld;
    procedure SetupWindow; virtual;
    function  CanClose: boolean; virtual;
  end;



{----------------------------------------------------------------------------}
{ The constructor performes step one of the initialization.
{----------------------------------------------------------------------------}

constructor TSaveSelDlg.Init( aParent: PWindowsObject;
  aDrugArray: PDrugArray; dbSeln: PDBFile );
begin
  inherited Init( AParent, MakeIntResource( DLG_SAVE_SELECTION ) );
  drugArray := ADrugArray;
  pSelectionDB := dbSeln;
  pIDEdit := New( PUIFullEditFld, Init( @Self, pSelectionDB^.dbd, nil, TRUE,
    DBDRUGSELNID, IDC_CODE, IDC_LBL1 ) );
  descEdit := New( PUIFullEditFld, Init( @Self, pSelectionDB^.dbd, nil, TRUE,
    DBDRUGSELNDesc, IDC_DESC, IDC_LBL2 ) );
end;



{----------------------------------------------------------------------------}
{ The destructor performes object cleanup.
{----------------------------------------------------------------------------}

destructor TSaveSelDlg.Done;
begin
  MSDisposeObj( descEdit );
  MSDisposeObj( pIDEdit );
  inherited Done;
end;



{----------------------------------------------------------------------------}
{ This override performes step two of the initialization.
{----------------------------------------------------------------------------}

procedure TSaveSelDlg.SetupWindow;
begin
  inherited SetupWindow;
  pIDEdit^.XferRecToCtl( pSelectionDB^.dbr );
  descEdit^.XferRecToCtl( pSelectionDB^.dbr );
end;



{----------------------------------------------------------------------------}
{ This function determines if it is Ok to close the dialog box by
{ validating information. Errors are reported to the user.
{----------------------------------------------------------------------------}

function TSaveSelDlg.CanClose: boolean;
var
  selxDB: PDBAssocFile;
  selID,
  tempID: array [ 0..8 ] of char;
  selnSeqNum: TSeqNum;
  idx: Word;
  err: integer;
begin
  CanClose := FALSE;
  { Get the user input }
  err := pIDEdit^.Format;
  if err <> 0 then
  begin
    ShowUIFldError( @Self, err, pIDEdit );
    Exit;
  end;
  err := descEdit^.Format;
  if err <> 0 then
  begin
    ShowUIFldError( @Self, err, descEdit );
    Exit;
  end;

  pSelectionDB^.dbr^.GetFieldAsStr( DBDRUGSELNID, tempID, 8 );
  pIDEdit^.XferCtlToRec( pSelectionDB^.dbr );
  pSelectionDB^.dbr^.GetFieldAsStr( DBDRUGSELNID, selID, 8 );
  if StrIComp( tempID, selID ) <> 0 then
  begin
    { Verify ID does not exist }
    if pSelectionDB^.dbc^.GetEQ( pSelectionDB^.dbr ) then
    begin
      pSelectionDB^.dbr^.PutFieldAsStr( DBDRUGSELNID, tempID );
      pSelectionDB^.dbc^.GetEQ( pSelectionDB^.dbr );
      ShowError( @Self, IDS_IN_USE, nil, 0, 0, 0 );
      FocusCtl( HWindow, IDC_CODE );
      Exit;
    end;
  end
  else
    pSelectionDB^.dbc^.DeleteRec( pSelectionDB^.dbr );

  { Save the selection name }
  descEdit^.XferCtlToRec( pSelectionDB^.dbr );
  pSelectionDB^.dbc^.InsertRec( pSelectionDB^.dbr );
  selnSeqNum := pSelectionDB^.dbr^.GetSeqValue;

  { Save the drugs }
  selxDB := New( PDBAssocFile,
    Init( pSelectionDB^.dbr, DBDRUGSELN_DRUGSELX_ASSOC, dbOpenNormal ) );
  if selxDB = nil then
  begin
    ShowError( @Self, IDS_INITERR1, nil, 0, MOD_DRUGSEL, 2 );
    Exit;
  end;
  selxDB^.dbr^.PutField( DBDRUGSELXRFSelRef, @selnSeqNum );
  for idx := 1 to DrugsAcross do
  begin
    if drugArray^[ idx ] = 0 then
      Break;
    selxDB^.dbr^.PutField( DBDRUGSELXRFDrugRef, @drugArray^[ idx ] );
    selxDB^.dbc^.InsertRec( selxDB^.dbr );
  end;
  MSDisposeObj( selxDB );
  CanClose := TRUE;
end;



                        {----------------------}
                        {     Edit Dialog      }
                        {----------------------}



type
  {--------------------------------------------------------------------------}
  { This object represents the dialog box used to edit a set of drugs.
  {--------------------------------------------------------------------------}
  PEditSelDlg = ^TEditSelDlg;
  TEditSelDlg = object( TCenterDlg )

    { Interface }
    constructor Init( aParent: PWindowsObject; bEdit: boolean;
      aDrugArray: PDrugArray; dbSeln: PDBFile );
    destructor Done; virtual;


    { Implementation }
    private
    TrayDBObj: PTraysObject;
    PrevSels: array[ 1..DrugsAcross ] of Integer;
    drugDB: PDBFile;
    ListBoxLine: array [ 0..LBL_LEN ] of char;
    pSelectionDB: PDBFile;         { pointer to external object }
    bEditing: boolean;
    drugArray: PDrugArray;
    ItemsInList: Integer;
    MultiDrugSelect: Boolean;
    procedure SetupWindow; virtual;
    procedure InitListBoxSel; virtual;
    procedure List( var msg: TMessage ); virtual id_First + IDC_LIST;
    procedure Save( var msg: TMessage ); virtual id_First + IDC_SAVE;
    procedure Clear( var msg: TMessage ); virtual id_First + IDC_CLEAR;
    function  CanClose: boolean; virtual;
    procedure EnableButtons; virtual;
    procedure SortIntArray( var intArray: Array of Integer );
    procedure WhoWasFlipped( var Idx: Integer; var OnOff: boolean );
    procedure SelectMultiDrugs;
  end;



{----------------------------------------------------------------------------}
{ The constructor performes step one of the initialization.
{----------------------------------------------------------------------------}

constructor TEditSelDlg.Init( aParent: PWindowsObject; bEdit: boolean;
                             aDrugArray: PDrugArray; dbSeln: PDBFile );
var
  c: PControl;
begin
  inherited Init( aParent, MakeIntResource( DLG_EDIT_SELECTION ) );
  bEditing := bEdit;
  drugArray := aDrugArray;
  pSelectionDB := dbSeln;
  drugDB := nil;
  c := New( PLStatic, InitResource( @Self, IDC_LBL1, 0, FALSE ) );
  TrayDBObj := New( PTraysObject, Init );

  {If We are not consolidating drugs, provide}
  {Master/Slaves multiselect as a convenience}
  MultiDrugSelect:=Not ScreenOn(FilterMultiDrugs);
end;



{----------------------------------------------------------------------------}
{ The destructor performes object cleanup.
{----------------------------------------------------------------------------}

destructor TEditSelDlg.Done;
begin
  MSDisposeObj( drugDB );
  MSDisposeObj( TrayDBObj );
  inherited Done;
end;



{----------------------------------------------------------------------------}
{ This override performes step two of the initialization.
{----------------------------------------------------------------------------}

procedure TEditSelDlg.SetupWindow;
var
  r: TRect;
  idx: integer;
  drugID: array [ 0..10 ] of char;
begin
  inherited SetupWindow;
  SetDlgItemText( hWindow, IDC_COUNT, '0' );
  GetClientRect( GetItemHandle( IDC_LIST ), r );
  SendDlgItemMsg( IDC_LIST, LB_SETCOLUMNWIDTH,
                 Word( ( r.right - r.left ) div 8 ), 0 );
  drugDB := New( PDBFIle, Init( DBDRUGFile, '', dbOpenNormal ) );
  if ( drugDB <> nil ) and drugDB^.dbc^.SetCurKeyNum( DBDRUG_Sort_KEY ) then
  begin
    if drugDB^.dbc^.GetFirst( drugDB^.dbr ) then
    repeat
      drugDB^.dbr^.GetFieldAsStr( DBDRUGAbbr, drugID, 10 );
      idx := SendDlgItemMsg( IDC_LIST, LB_ADDSTRING, 0,
        longint( @drugID[ 0 ] ) );
      SendDlgItemMsg( IDC_LIST, LB_SETITEMDATA, idx,
        drugDB^.dbr^.GetSeqValue );
    until not drugDB^.dbc^.GetNext( drugDB^.dbr );
  end;
  if bEditing then
    InitListBoxSel;

  {Store currently selected indexes, sorted for future comparisons}
  FillChar( PrevSels, SizeOf( PrevSels ), 255 );
  SendDlgItemMsg( IDC_LIST, LB_GETSELITEMS, Word( DrugsAcross ),
    longint( @PrevSels ) );
  SortIntArray( PrevSels );

  EnableButtons;
end;



{----------------------------------------------------------------------------}
{ This procedure selects the drugs in the array
{----------------------------------------------------------------------------}

procedure TEditSelDlg.InitListBoxSel;
var
  idx, sel: integer;
  drugID: array [ 0..10 ] of char;
begin
  for idx := 1 to DrugsAcross do
  begin
    if drugArray^[ idx ] = 0 then
      Break;
    if drugDB^.dbc^.GetSeq( drugDB^.dbr, drugArray^[ idx ] ) then
    begin
      sel := SendDlgItemMsg( IDC_LIST, LB_FINDSTRINGEXACT, Word( -1 ),
        longint( drugDB^.dbr^.GetFieldAsStr( DBDRUGAbbr, drugID, 10 ) ) );
      if sel <> LB_ERR then
        SendDlgItemMsg( IDC_LIST, LB_SETSEL, 1, MakeLong( sel, 0 ) );
    end;
  end;
  sel := SendDlgItemMsg( IDC_LIST, LB_GETSELCOUNT, 0, 0 );
  Str( sel, drugID );
  SetDlgItemText( hWindow, IDC_COUNT, drugID );
  ItemsInList := SendDlgItemMsg( IDC_LIST, LB_GETCOUNT, 0, 0 );
end;



{----------------------------------------------------------------------------}
{ This procedure sorts the array. It uses the classic bubble sort for an
{ array of integers with unknown bounds. O(n) is negligeable, qsort isn't
{ worth the trouble for small arrays.
{----------------------------------------------------------------------------}

procedure TEditSelDlg.SortIntArray( var intArray: Array of Integer );
var i, j, temp: integer;
begin
  for i := Low( intArray ) to High( intArray )-1 do
      for j := i + 1 to High( intArray ) do
          if intArray[ i ]<intArray[ j ] then
          begin
            temp := intArray[ i ];
            intArray[ i ] := intArray[ j ];
            intArray[ j ] := temp;
          end;
end;



{----------------------------------------------------------------------------}
{ This procedure compares NewSels vs. PrevSels to determine if a selection
{ has been toggled
{----------------------------------------------------------------------------}

procedure TEditSelDlg.WhoWasFlipped( var Idx: Integer; var OnOff: boolean );
var NewSels: array[ 1..DrugsAcross ] of Integer;
    Flipped: Integer;
    I: Integer;
begin
  {Get current selections, sort them}
  FillChar( NewSels, SizeOf( NewSels ), 255 );
  SendDlgItemMsg( IDC_LIST, LB_GETSELITEMS, Word( DrugsAcross ),
    longint( @NewSels ) );
  SortIntArray( NewSels );

  {What is different in our new selections?}
  i := Low( PrevSels );
  while ( i <= High( PrevSels ) ) and ( PrevSels[ i ] = NewSels[ i ] ) do
    inc( i );

  {If there is something different, what is it?, was it flipped on or off?}
  if i<=High( PrevSels )then
  begin
    OnOff := ( PrevSels[ i ] < NewSels[ i ] ) or ( PrevSels[ i ] = 255 );
    if OnOff
       then Idx := NewSels[ i ]
       else Idx := PrevSels[ i ];
  end
  else Idx := -1;

  {Store the newsels as prevsels}
  Move( NewSels, PrevSels, SizeOf( PrevSels ) );
end;

{----------------------------------------------------------------------------}
{ This procedure adds master drug functionality so that selecting a master
{ automatically selects its slaves.
{----------------------------------------------------------------------------}

procedure TEditSelDlg.SelectMultiDrugs;
var Idx,
    CurrSel,
    Slaves,
    SelCount: Integer;
    isSelected: Boolean;
    DrugAbr,
    CurrAbr,
    MappedAbr: array[ 0..15 ] of char;
begin
   {Was something flipped on?}
   WhoWasFlipped( CurrSel, isSelected );

   if ( CurrSel <> -1 ) then
   begin
     {Was it a drug that acts as master for other drugs?}
     SendDlgItemMsg( IDC_LIST, LB_GetText, CurrSel, LongInt( @DrugAbr ) );
     SelCount := SendDlgItemMsg( IDC_LIST, LB_GETSELCOUNT, 0, 0 );
     Slaves := TrayDBObj^.DrugMapCount( DrugAbr );

     {Can we select the mapped drugs without exceeding the allowable max  }
     {sels? If so, look for drugs that map to this master and select them }
     if  (MultiDrugSelect and isSelected) and
         ( Slaves>0 ) and ( SelCount+Slaves <= DrugsAcross ) then
     begin
       Idx:=0;
       While SendDlgItemMsg( IDC_LIST, LB_GetText, Idx, LongInt( @CurrAbr ) ) <> LB_Err do
       begin
         if TrayDBObj^.MapDrgAbbr( CurrAbr, MappedAbr ) then
           if StrComp( MappedAbr, DrugAbr ) = 0 then
              SendDlgItemMsg( IDC_LIST, LB_SetSel, 1, MakeLong( Idx,0 ) );
         Inc(Idx);
       end;
     end;

     {Was it a drug that acts as slave for other drug?}
     if (not MultiDrugSelect) and (TrayDBObj^.MapDrgAbbr(DrugAbr, MappedAbr)) then
     begin
       Idx:=0;
       StrCopy(CurrAbr,'');
       While (StrComp(CurrAbr,MappedAbr)<>0) do
       begin
         SendDlgItemMsg( IDC_LIST, LB_GetText, Idx, LongInt( @CurrAbr ) );
         Inc(Idx);
       end;
       SendDlgItemMsg( IDC_LIST, LB_SetSel, 1, MakeLong( Idx-1,0 ) );
       SendDlgItemMsg( IDC_LIST, LB_SetSel, 0, MakeLong( CurrSel,0 ) );
       SendDlgItemMsg( IDC_LIST, LB_GETSELITEMS, Word( DrugsAcross ), longint( @PrevSels ) );
       SortIntArray( PrevSels );
     end;
   end;
end;


{----------------------------------------------------------------------------}
{ This procedure handles commands from the list control so that selections
{ changes can be validated.
{----------------------------------------------------------------------------}

procedure TEditSelDlg.List( var msg: TMessage );
var
  selCount: integer;
  selCountStr: array [ 0..10 ] of char;
begin
  if msg.lParamHi = LBN_SELCHANGE then
  begin
    {Was a master drug selected?}
    {Select its mapped drugs as well}
    DefWndProc( msg );
    SelectMultiDrugs;

    selCount := SendDlgItemMsg( IDC_LIST, LB_GETSELCOUNT, 0, 0 );
    Str( selCount, selCountStr );
    SetDlgItemText( hWindow, IDC_COUNT, selCountStr );
    if selCount > DrugsAcross then
      ShowError( @Self, IDS_TOO_MANY_SEL, nil, pSelectionDB^.dbc^.dbErrorNum,
        MOD_DRUGSEL, 0 );
    EnableButtons;
  end
  else if msg.lParamHI = LBN_DBLCLK then
  begin
    (* display complete drug info ?? *)
  end;
  DefWndProc( msg );
end;



{----------------------------------------------------------------------------}
{ This procedure handles commands from the Save button.
{----------------------------------------------------------------------------}

procedure TEditSelDlg.Save( var msg: TMessage );
var
  pDialog: PSaveSelDlg;
begin
  if not CanClose then
    Exit;

  pDialog := New( PSaveSelDlg, Init( @Self, drugArray, pSelectionDB ) );
  if pDialog <> nil then
    if Application^.ExecDialog( pDialog ) = IDOK then
      if not bEditing then
        bEditing := TRUE;
end;



{----------------------------------------------------------------------------}
{ This procedure handles messages from the Clear button. It clears any
{ selected drugs and resets the counter.
{----------------------------------------------------------------------------}

procedure TEditSelDlg.Clear( var msg: TMessage );
var
  totalEntries: Word;
begin
  {Store currently selected indexes, sorted for future comparisons}
  FillChar( PrevSels, SizeOf( PrevSels ), 255 );

  SetDlgItemText( hWindow, IDC_COUNT, '0' );
  totalEntries := SendDlgItemMsg( IDC_LIST, LB_GETCOUNT, 0, 0 );
  SendDlgItemMsg( IDC_LIST, LB_SELITEMRANGE, 0,
    longint( totalEntries * 65536 ) );
  EnableButtons;
  DefWndProc( msg );
end;



{----------------------------------------------------------------------------}
{ This function determines if it is Ok to close the dialog box by
{ validating the selection list. Errors are reported to the user.
{----------------------------------------------------------------------------}

function TEditSelDlg.CanClose: boolean;
var
  idx: Word;
  selCount: integer;
  selItems: array[ 1..DrugsAcross ] of integer;
begin
  selCount := SendDlgItemMsg( IDC_LIST, LB_GETSELCOUNT, Word( 0 ),
    longint( 0 ) );
  if selCount > DrugsAcross then
  begin
    ShowError( @Self, IDS_TOO_MANY_SEL, nil, pSelectionDB^.dbc^.dbErrorNum,
      MOD_DRUGSEL, 0 );
    CanClose := FALSE;
  end
  else
  begin
    SendDlgItemMsg( IDC_LIST, LB_GETSELITEMS, Word( DrugsAcross ),
      longint( @selItems ) );
    for idx := 1 to DrugsAcross do
      drugArray^[ idx ] := 0;
    for idx := 1 to selCount do
      drugArray^[ idx ] := SendDlgItemMsg( IDC_LIST, LB_GETITEMDATA,
        Word( selItems[ idx ] ), 0 );
    CanClose := TRUE
  end;
end;



{----------------------------------------------------------------------------}
{ This procedure enables the buttons.
{----------------------------------------------------------------------------}

procedure TEditSelDlg.EnableButtons;
var
  bEnable: boolean;
  lastFoc: HWnd;
begin
  lastFoc := GetFocus;   {- save the current focus }
  bEnable := ( SendDlgItemMsg( IDC_LIST, LB_GETSELCOUNT, 0, 0 ) > 0 );
  EnableWindow( GetItemHandle( IDOK ), bEnable );
  EnableWindow( GetItemHandle( IDC_SAVE ), bEnable );
  EnableWindow( GetItemHandle( IDC_CLEAR ), bEnable );
  if not IsWindowEnabled( lastFoc ) then
    FocusCtl( HWindow, IDC_LIST );
end;



                        {----------------------}
                        {     Main Dialog      }
                        {----------------------}



type
  {--------------------------------------------------------------------------}
  { This object represents the main dialog for selecting a set of drugs.
  {--------------------------------------------------------------------------}
  PDrugSelDlg = ^TDrugSelDlg;
  TDrugSelDlg = object( TCenterDlg )

    { Interface }
    constructor Init( pParent: PWindowsObject; aDrugArray: PDrugArray );
    destructor Done; virtual;


    { Implementation }
    private
    drugArray: PDrugArray;
    grid: PGrid;
    pSelectionDB: PDBFile;
    ListBoxLine: array [ 0..LBL_LEN ] of char;
    procedure SetupWindow; virtual;
    procedure FillListBox; virtual;
    procedure List( var msg: TMessage ); virtual id_First + IDC_LIST;
    procedure Add( var msg: TMessage ); virtual id_First + IDC_ADD;
    procedure Edit( var msg: TMessage ); virtual id_First + IDC_EDIT;
    procedure Delete( var msg: TMessage ); virtual id_First + IDC_DELETE;
    function  CanClose: boolean; virtual;
    procedure EnableButtons; virtual;
    procedure DrawItem( var msg: TMessage ); virtual wm_First + wm_DrawItem;
    procedure MeasureItem( var msg: TMessage );
      virtual wm_First + wm_MeasureItem;
    procedure CharToItem( var msg: TMessage );
      virtual wm_First + wm_CharToItem;
    function  LoadSelection: boolean;
  end;



{----------------------------------------------------------------------------}
{ The constructor performes step one of the initialization.
{----------------------------------------------------------------------------}

constructor TDrugSelDlg.Init( pParent: PWindowsObject;
  aDrugArray: PDrugArray );
var
  drugIdx: Word;
  c: PControl;
begin
  inherited Init( pParent, MakeIntResource( DLG_DRUG_SELECTION ) );
  pSelectionDB := nil;
  drugArray := ADrugArray;
  for drugIdx := 1 to DrugsAcross do
    drugArray^[ drugIdx ] := 0;
  grid := New( PGrid, InitResource( @Self, IDC_LIST, 2, FALSE ) );
  if grid = nil then
  begin
    Done;
    Fail;
  end;
  pSelectionDB := New( PDBFile, Init( DBDRUGSELNFile, '', dbOpenNormal ) );
  if pSelectionDB = nil then
  begin
    Done;
    Fail;
  end;
end;



{----------------------------------------------------------------------------}
{ The destructor performes object cleanup.
{----------------------------------------------------------------------------}

destructor TDrugSelDlg.Done;
begin
  MSDisposeObj( pSelectionDB );
  inherited Done;
end;



{----------------------------------------------------------------------------}
{ This override performes step two of the initialization.
{----------------------------------------------------------------------------}

procedure TDrugSelDlg.SetupWindow;
begin
  inherited SetupWindow;
  SR( IDS_CODE, ListBoxLine, LBL_LEN );
  grid^.SetHeader( 0, ListBoxLine );
  SR( IDS_DESC, ListBoxLine, LBL_LEN );
  grid^.SetHeader( 1, ListBoxLine );
  FillListBox;
  grid^.OptColumnWidth( 0 );
  grid^.OptColumnWidth( 1 );
  grid^.StretchColumn( 1 );
end;



{----------------------------------------------------------------------------}
{ This procedure reloads the list using the contents of the drug selection
{ data file.
{
{ Note: GetNext was GetNextContains but sometimes the list would not fill.
{ It's hoped that GetNextContains was not fufilling a purpose.
{----------------------------------------------------------------------------}

procedure TDrugSelDlg.FillListBox;
var
  k: integer;
  sel: integer;
begin
  grid^.SetRedraw( FALSE );
  sel := grid^.GetSelIndex;
  if sel = -1 then
    sel := 0;
  grid^.ClearList;
  pSelectionDB^.dbr^.ClearRecord;
  if pSelectionDB^.dbc^.GetFirst( pSelectionDB^.dbr ) then
  begin
    repeat
      pSelectionDB^.dbr^.GetFieldAsStr( DBDRUGSELNID, ListBoxLine, LBL_LEN );
      k := grid^.AddString( ListBoxLine );
      grid^.SetData( 0, k, ListBoxLine );
      pSelectionDB^.dbr^.GetFieldAsStr( DBDRUGSELNDesc, ListBoxLine, LBL_LEN );
      grid^.SetData( 1, k, ListBoxLine );
      grid^.SetItemData( k, pSelectionDB^.dbr^.GetSeqValue );
    until not pSelectionDB^.dbc^.GetNext( pSelectionDB^.dbr );
  end;
  grid^.SetRedraw( TRUE );
  grid^.SetSelIndex( sel );
  EnableButtons;
end;



{----------------------------------------------------------------------------}
{ This procedure handles messages sent to the list control. It enables the
{ buttons when the selection state changes. This is needed to implement the
{ feature where ...
{----------------------------------------------------------------------------}

procedure TDrugSelDlg.List( var msg: TMessage );
begin
  if msg.lParamHi = LBN_SELCHANGE then
    EnableButtons
  else if msg.lParamHI = LBN_DBLCLK then
    ; { available for possible action }
  DefWndProc( msg );
end;



{----------------------------------------------------------------------------}
{ This procedure handles the message from the Add button. When the user
{ selects the button, the edit dialog is displayed with an empty set.
{----------------------------------------------------------------------------}

procedure TDrugSelDlg.Add( var msg: TMessage );
var
  d: PEditSelDlg;
begin
  pSelectionDB^.dbr^.ClearRecord;
  d := New( PEditSelDlg, Init( @Self, FALSE, drugArray, pSelectionDB ) );
  if Application^.ExecDialog( d ) = IDOK then
    EndDlg( IDOK )
  else
    FillListBox;
end;



{----------------------------------------------------------------------------}
{ This procedure handles messages from the Edit button. It opens an edit
{ dialog box with the selected drug set.
{----------------------------------------------------------------------------}

procedure TDrugSelDlg.Edit( var msg: TMessage );
var
  d: PEditSelDlg;
  sel: integer;
begin
  sel := grid^.GetSelIndex;
  if sel = lb_Err then
  begin
    MessageBeep( MB_ICONEXCLAMATION );
    Exit;
  end;
  if not pSelectionDB^.dbc^.GetSeq( pSelectionDB^.dbr, grid^.GetItemData( sel ) ) then
  begin
    MessageBeep( MB_ICONEXCLAMATION );
    Exit;
  end;
  LoadSelection;
  d := New( PEditSelDlg, Init( @Self, TRUE, drugArray, pSelectionDB ) );
  if Application^.ExecDialog( d ) = IDOK then
    EndDlg( IDOK )
  else
    FillListBox;
end;



{----------------------------------------------------------------------------}
{ This procedure handles messages from the Delete button. It deletes the
{ selected drug set.
{----------------------------------------------------------------------------}

procedure TDrugSelDlg.Delete( var msg: TMessage );
var
  sel: integer;
  pstr: array [ 0..40 ] of char;
begin
  sel := grid^.GetSelIndex;
  if sel = lb_Err then
  begin
    MessageBeep( MB_ICONEXCLAMATION );
    Exit;
  end;
  SR( IDS_DELETE, ListBoxLine, LBL_LEN );
  StrLCat( ListBoxLine, #10#10, LBL_LEN );
  grid^.GetData( 1, sel, pstr, 40 );
  StrLCat( ListBoxLine, pstr, LBL_LEN );
  GetWindowText( hWindow, pstr, 40 );
  if YesNoMsg( HWindow, pstr, ListBoxLine ) then
  begin
    if pSelectionDB^.dbc^.GetSeq( pSelectionDB^.dbr,
      grid^.GetItemData( sel ) ) then
      pSelectionDB^.dbc^.DeleteRec( pSelectionDB^.dbr );
    if pSelectionDB^.dbc^.dbErrorNum <> 0 then
      ShowError( @Self, IDS_CANNOT_DELETE, nil, pSelectionDB^.dbc^.dbErrorNum,
        MOD_DRUGSEL, 0 );
    FillListBox;
  end;
end;



{----------------------------------------------------------------------------}
{ This function determines if it is Ok to close the dialog box by
{ validating the selection list. Errors are reported to the user. If valid,
{ the selections are loaded.
{----------------------------------------------------------------------------}

function TDrugSelDlg.CanClose: boolean;
var
  sel: integer;
begin
  CanClose := FALSE;
  { Get the user input }
  sel := grid^.GetSelIndex;
  if sel = lb_Err then
  begin
    MessageBeep( MB_ICONEXCLAMATION );
    Exit;
  end;
  { Load the selection }
  if not pSelectionDB^.dbc^.GetSeq( pSelectionDB^.dbr,
    grid^.GetItemData( sel ) ) then
  begin
    MessageBeep( MB_ICONEXCLAMATION );
    Exit;
  end;
  CanClose := LoadSelection;
end;



{----------------------------------------------------------------------------}
{ This procedure enables the buttons.
{----------------------------------------------------------------------------}

procedure TDrugSelDlg.EnableButtons;
var
  bEnable: boolean;
begin
  bEnable := ( grid^.GetSelIndex <> lb_Err );
  EnableWindow( GetItemHandle( IDOK ), bEnable );
  EnableWindow( GetItemHandle( IDC_EDIT ), bEnable );
  EnableWindow( GetItemHandle( IDC_DELETE ), bEnable );
end;



{----------------------------------------------------------------------------}
{ This procedure passes DrawItem commands to the grid.
{----------------------------------------------------------------------------}

procedure TDrugSelDlg.DrawItem( var msg: TMessage );
begin
  grid^.DrawItem( msg );
end;



{----------------------------------------------------------------------------}
{ This procedure passes MeasureItem commands to the grid.
{----------------------------------------------------------------------------}

procedure TDrugSelDlg.MeasureItem( var msg: TMessage );
begin
  grid^.MeasureItem( msg );
end;



{----------------------------------------------------------------------------}
{ This procedure passes CharToItem commands to the grid.
{----------------------------------------------------------------------------}

procedure TDrugSelDlg.CharToItem( var msg: TMessage );
begin
  grid^.CharToItem( msg );
end;



{----------------------------------------------------------------------------}
{ This function loads a selection into the drug array.
{----------------------------------------------------------------------------}

function TDrugSelDlg.LoadSelection: boolean;
var
  selxDB: PDBAssocFile;
  idx: Word;
  finished: boolean;
begin
  LoadSelection := FALSE;
  selxDB := New( PDBAssocFile, Init( pSelectionDB^.dbr,
    DBDRUGSELN_DRUGSELX_ASSOC, dbOpenNormal ) );
  if selxDB = nil then
  begin
    ShowError( @Self, IDS_INITERR1, nil, 0, MOD_MPSS, 26 );
    Exit;
  end;
  for idx := 1 to DrugsAcross do
    drugArray^[ idx ] := 0;
  idx := 1;
  finished := not selxDB^.dbc^.GetFirst( selxDB^.dbr );
  while not finished do
  begin
    selxDB^.dbr^.GetField( DBDRUGSELXRFDrugRef, @( drugArray^[ idx ] ),
      SizeOf( TSeqNum ) );
    Inc( idx );
    finished := not selxDB^.dbc^.GetNext( selxDB^.dbr );
  end;
  MSDisposeObj( selxDB );
  LoadSelection := TRUE;
end;



                        {----------------------}
                        { EXPORTED PROCEDURES  }
                        {----------------------}



{----------------------------------------------------------------------------}
{ This procedure uses the dialog boxes defined above to prompt the user
{ for a set of drugs.
{----------------------------------------------------------------------------}

function GetDrugArray( pParent: PWindowsObject; aDrugArray: PDrugArray ):
  boolean;
var
  pDialog: PDrugSelDlg;
begin
  pDialog := New( PDrugSelDlg, Init( pParent, aDrugArray ) );
  if pDialog = nil then
    GetDrugArray := FALSE
  else
    GetDrugArray := ( Application^.ExecDialog( pDialog ) = IDOK );
end;

END.
