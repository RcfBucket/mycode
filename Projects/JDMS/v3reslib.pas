{----------------------------------------------------------------------------}
{  Module Name  : V3RESLIB.PAS                                               }
{  Programmer   : EJ                                                         }
{  Date Created : 01/30/95                                                   }
{                                                                            }
{  Purpose -                                                                 }
{  This module is used to access and maintain the Nihongo DMS result         }
{  database file (RESULT.DBD).                                               }
{                                                                            }
{  Assumptions -                                                             }
{  Each isolate order defined will have ONE result record that will          }
{  contain all test results for the related test order packed into           }
{  that result record.  However, isolates may have more than one             }
{  isolate order and therefore more than one result record.                  }
{  The PResults object will contain ALL results for a specified              }
{  isolate.                                                                  }
{                                                                            }
{  Initialization -                                                          }
{  Query result field size from database.                                    }
{                                                                            }
{  Revision History -                                                        }
{                                                                            }
{  Version  Date      Who    Did What                                        }
{  -------  --------  ------ ----------------------------------------------- }
{  1.00     01/30/95  EJ     Initial release                                 }
{                                                                            }
{----------------------------------------------------------------------------}

(* Note: Currently calling HandleEvents at all times. May consider adding a flag
         to indicate if it should be called *)

unit V3ResLib;

{- Format of in memory records (using LoadResults) are as follows:

  TESTGROUPID--TESTID--TESTCATID--TESTREF--DRUGREF--RESULT
  ..
  ..

  The result list in memory is sorted by TESTGROUPID-TESTID-TESTCATID.
}

{- Error codes that cause program termination (BadError)

  MOD_RESLIB +                (MOD_RESLIB is defined in MScan )

  1 - Cannot open RESULT.DBI
  2 - Cannot open RESULT
  3 - Cannot open ISOLATE
  4 - Cannot open ISORDER
  5 - Cannot open TESTGRP
  6 - Cannot open TSTGPXRF
  7 - Cannot open TEST
  8 - Cannot open TSTCAT
  9 - Cannot create Trays object
  10 - Cannot allocate memory
  11 - Invalid result buffer position value
  12 - Field sizes do not match
}

INTERFACE

uses
  DBTypes,
  DBFile,
  DBLib,
  Trays,
  Objects;

const
  RESmaxStrResult    = 20;   {- maximum length of an unpacked result }
  REStestgroupIDLen  = 9;    {- maximum lengths of fields (from DBLib) }
  REStestIDLen       = 8;
  REStestNameLen     = 33;
  REStestCatIDLen    = 6;

  {- Result Flags - Used in SetFlag and GetFlag }
  REScontraIndicated = 0;

type
  PResRecObj = ^TResRecObj;
  TResRecObj = object(TObject)
    testgroupID : array [0..REStestgroupIDLen-1] of char;   { read-only }    { formerly procID }
    tgUD        : boolean;                                  { read-only }
    testID      : array [0..REStestIDLen-1] of char;        { read-only }
    testName    : array [0..REStestNameLen-1] of char;      { read-only }
    testCatID   : array [0..REStestCatIDLen-1] of char;     { read-only }
    modified    : boolean;                                  { read-only }
    isordSeq    : TSeqNum;                                  { read-only }
    testSeq     : TSeqNum;                                  { read-only }
    drugSeq     : TSeqNum;                                  { read-only }
    result      : array [0..RESmaxStrResult] of char;       { read-only }
    flag        : word;                                     { read-only }
    {}
    constructor Init(aTestGroupID: PChar; aTGUD: boolean;
                     aTestID, aTestCatID, aTestName: PChar;
                     aIsordSeq, aTestSeq, aDrugSeq: TSeqNum;
                     aResult: PChar; aPosition: integer; aMaxResLen: byte;
                     aFlag: word);
    destructor Done; virtual;
    procedure SetResult(aResult: PChar);
    function  GetResult(aResult: PChar; maxLen: integer): PChar;
    procedure ClearModify;
    {}
    private
    maxResLen   : byte;
    pos         : integer;
  end;

  PV3Results = ^TV3Results;
  TV3Results = object(TSortedCollection)
    traysObj      : PTraysObject;
    dbErrorNum    : integer;
    {}
    constructor Init;
    destructor Done; virtual;
    function  Compare(key1, key2: pointer): integer; virtual;
    function  LoadResults(isoRec: PDBRec): boolean; virtual;
    function  SaveResults: boolean; virtual;
    function  DeleteResults: boolean; virtual;
    function  IsModified: boolean; virtual;
    procedure ClearModify; virtual;
    function  GetResult(aTestID, aTestCatID, res: PChar;
                        maxChars: integer): boolean;
    function  SetResult(aTestID, aTestCatID, res: PChar;
                        overwrite: boolean): boolean;
    function  GetFlag(aTestID, aTestCatID: PChar; flagPos: byte): boolean;
    procedure SetFlag(aTestID, aTestCatID: PChar; flagPos: byte; aVal: boolean);
    function  GetDrugResults(aDrugSeq: TSeqNum; MICres, NCCLSres: PChar;
                             maxChars: integer; var contraInd: boolean): boolean;
    function  AnyResultsExist: boolean;
    function  IDResultsExist: boolean;
    function  MICResultsExist: boolean;
    function  FindTestGroup(aTestGroupID: PChar): integer;
    function  NextTestGroup(aTestGroupID: PChar; var index: integer): boolean;
    function  NumTests(aTestGroupID: PChar; var first: integer): integer;
    procedure ByteToMIC(mic: PChar; bVal: byte);
    procedure MICToByte(mic: PChar; var bVal: byte);
    {}
    private
    resultsLoaded : boolean;
    resBuf        : PChar;    {- buffer to hold packed results }
    resDB         : PDBFile;
    {}
    procedure UnPackResults(resultStr, pTestCatID: PChar;
                            pos, resultLen: integer; var flag: word);
    procedure PackResults(p: PResRecObj);
  end;


IMPLEMENTATION

uses
  ApiTools,
  Bits,
  IntrpMap,
  DBIDs,
  MScan,
  DMString,
  DMSErr,
  CvtMisc,
  StrsW,
  Strings;

const
  greaterThan         = $40;
  lessThan            = $20;
  Err_OpenRESULT_Desc = 1;
  Err_OpenRESULT      = 2;
  Err_OpenISOLATE     = 3;
  Err_OpenISORDER     = 4;
  Err_OpenTESTGRP     = 5;
  Err_OpenTSTGPXRF    = 6;
  Err_OpenTEST        = 7;
  Err_OpenTSTCAT      = 8;
  Err_TraysObject     = 9;
  Err_AllocMem        = 10;
  Err_BadPosition     = 11;
  Err_DBFieldSize     = 12;

var
  firstTime     : boolean;
  resFieldSize  : word;


procedure BadError(num, loc: integer; abort: boolean);
{- show a critical error and possibly abort }
begin
  ShowError(nil, IDS_PROGHALT, nil, num + MOD_RESLIB, MOD_RESLIB, 0);
  if abort then
    Halt(1);
end;


{----------------------------------------------------------------------}

constructor TResRecObj.Init(aTestGroupID: PChar; aTGUD: boolean;
                            aTestID, aTestCatID, aTestName: PChar;
                            aIsordSeq, aTestSeq, aDrugSeq: TSeqNum;
                            aResult: PChar; aPosition: integer; aMaxResLen: byte;
                            aFlag: word);
begin
  inherited Init;

  maxResLen := aMaxResLen;
  pos := aPosition;
  if pos >= resFieldSize then
    BadError(Err_BadPosition, 0, TRUE);

  StrLCopy(result, aResult, RESmaxStrResult);
  StrLCopy(testgroupID, aTestGroupID, REStestgroupIDLen-1);
  tgUD := aTGUD;
  StrLCopy(testID, aTestID, REStestIDLen-1);
  StrLCopy(testName, aTestName, REStestNameLen-1);
  StrLCopy(testCatID, aTestCatID, REStestCatIDLen-1);
  modified := FALSE;
  isordSeq := aIsordSeq;
  testSeq := aTestSeq;
  drugSeq := aDrugSeq;
  flag := aFlag;
end;

destructor TResRecObj.Done;
begin
  inherited Done;
end;

procedure TResRecObj.SetResult(aResult: PChar);
begin
  if StrLComp(aResult, result, RESmaxStrResult) <> 0 then
  begin
    StrLCopy(result, aResult, RESmaxStrResult);
    modified := TRUE;
    flag := 0;
  end;
end;

function TResRecObj.GetResult(aResult: PChar; maxLen: integer): PChar;
begin
  if aResult <> nil then
    StrLCopy(aResult, result, maxLen);
  GetResult := aResult;
end;

procedure TResRecObj.ClearModify;
begin
  modified := FALSE;
end;


{--------------------------------------------[ TV3Results ]-----------------}

constructor TV3Results.Init;
begin
  inherited Init(60, 60);

  resultsLoaded := FALSE;
  dbErrorNum := 0;
  GetMem(resBuf, resFieldSize);
  if resBuf = nil then
    BadError(Err_AllocMem, 0, TRUE);
  traysObj := New(PTraysObject, Init);
  if traysObj = nil then
    BadError(Err_TraysObject, 0, TRUE);
  resDB := OpenV30File(DBRESFile);
  if resDB = nil then
    BadError(Err_OpenRESULT, dbLastOpenError, TRUE);

(*         duplicates := TRUE; ?!?!  Should be FALSE, I think. *)
end;

destructor TV3Results.Done;
begin
  MSDisposeObj(resDB);
  MSDisposeObj(traysObj);
  MSFreeMem(resBuf, resFieldSize);
  inherited Done;
end;

function TV3Results.Compare(key1, key2: pointer): integer;
{- sort results list by : TESTGROUPID--TESTID--TESTCATID }
var
  k1, k2  : array[0..80] of char;
begin
  StrLCopy(k1, PResRecObj(key1)^.testgroupID, 80);
  StrLCat(k1, PResRecObj(key1)^.testID, 80);
  StrLCat(k1, PResRecObj(key1)^.testCatID, 80);

  StrLCopy(k2, PResRecObj(key2)^.testgroupID, 80);
  StrLCat(k2, PResRecObj(key2)^.testID, 80);
  StrLCat(k2, PResRecObj(key2)^.testCatID, 80);

  Compare := StrComp(k1, k2);
end;

function  TV3Results.LoadResults(isoRec: PDBRec): boolean;
var
  fi           : TFldInfo;
  isordDB      : PDBFile;
  toDB         : PDBFile;
  todDB        : PDBFile;
  tstDB        : PDBFile;
  tcatDB       : PDBFile;
  pTestGroupID : array [0..REStestgroupIDLen-1] of char;
  pTGUD        : boolean;
  pTestID      : array [0..REStestIDLen-1] of char;
  pTestName    : array [0..REStestNameLen-1] of char;
  pTestCatID   : array [0..REStestCatIDLen-1] of char;
  isorderSeq   : TSeqNum;
  testgroupSeq : TSeqNum;
  testSeq      : TSeqNum;
  drugSeq      : TSeqNum;
  tcatSeq      : TSeqNum;
  resultLen    : byte;
  pos          : integer;
  isOK         : boolean;
  gotResult    : boolean;
  resultStr    : array [0..RESmaxStrResult] of char;
  resObj       : PResRecObj;
  flag         : word;
begin
  LoadResults := FALSE;
  resultsLoaded := FALSE;
  dbErrorNum := 0;
  FreeAll; {- clear any pending results in memory }

  if isoRec = nil then
    Exit;

  isordDB := OpenV30File(DBISOORDFile);
  if isordDB = nil then
    BadError(Err_OpenISORDER, dbLastOpenError, TRUE);

  isorderSeq := isoRec^.GetSeqValue;
  isordDB^.dbr^.PutField(DBISOORDIsoRef, @isorderSeq);
  isOK := isordDB^.dbc^.GetFirstContains(isordDB^.dbr);

  {- At this point if isOK is TRUE, the first isorder has been found.

    loop through the test orders and load TestGroupID,
    then loop thru the test order definitions and load TestRef,
    then using the TestRef, load the TestID, TestCatID,
    DrugRef, and TCatRef from TEST database.
  }

  if isOK then
  begin
    toDB := OpenV30File(DBTSTGRPFile);
    if toDB = nil then
      BadError(Err_OpenTESTGRP, dbLastOpenError, TRUE);

    tstDB := OpenV30File(DBTSTFile);
    if tstDB = nil then
      BadError(Err_OpenTEST, dbLastOpenError, TRUE);

    tcatDB := OpenV30File(DBTSTCATFile);
    if tcatDB = nil then
      BadError(Err_OpenTSTCAT, dbLastOpenError, TRUE);

    toDB^.dbc^.SetSeqKeyNum;
    tstDB^.dbc^.SetSeqKeyNum;
    tcatDB^.dbc^.SetSeqKeyNum;

    {- calculate field sizes }
    if firstTime then
    begin
      firstTime := FALSE;
      toDB^.dbd^.FieldInfo(DBTSTGRPID, fi);
      if REStestgroupIDLen <> fi.fldSize then
        BadError(Err_DBFieldSize, 1, TRUE);
      tstDB^.dbd^.FieldInfo(DBTSTID, fi);
      if REStestIDLen <> fi.fldSize then
        BadError(Err_DBFieldSize, 2, TRUE);
      tstDB^.dbd^.FieldInfo(DBTSTName, fi);
      if REStestNameLen <> fi.fldSize then
        BadError(Err_DBFieldSize, 3, TRUE);
      tstDB^.dbd^.FieldInfo(DBTSTTstCat, fi);
      if REStestCatIDLen <> fi.fldSize then
        BadError(Err_DBFieldSize, 4, TRUE);
    end;

    todDB := OpenV30File(DBTSTGRPXRFFile);
    if todDB = nil then
      BadError(Err_OpenTSTGPXRF, dbLastOpenError, TRUE);
    repeat
      {- get test results by value of isord seq }
      isorderSeq := isordDB^.dbr^.GetSeqValue;
      resDB^.dbr^.PutField(DBRESIsoOrdRef, @isorderSeq);
      gotResult := resDB^.dbc^.GetEQ(resDB^.dbr);
      if gotResult then
        resDB^.dbr^.GetField(DBRESBlock, resBuf, resFieldSize);

      isordDB^.dbr^.GetField(DBISOORDTstGrpRef, @testgroupSeq, SizeOf(TSeqNum));

      toDB^.dbc^.GetSeq(toDB^.dbr, testgroupSeq);
      toDB^.dbr^.GetField(DBTSTGRPID, @pTestGroupID, REStestgroupIDLen);
      toDB^.dbr^.GetField(DBTSTGRPUDFlag, @pTGUD, SizeOf(boolean));

      todDB^.dbr^.ClearRecord;
      todDB^.dbr^.PutField(DBTSTGRPXRFTstGrpRef, @testgroupSeq);
      if todDB^.dbc^.GetFirstContains(todDB^.dbr) then
        repeat
          todDB^.dbr^.GetField(DBTSTGRPXRFTstRef, @testSeq, SizeOf(TSeqNum));
          todDB^.dbr^.GetField(DBTSTGRPXRFResBlockPos, @pos, SizeOf(integer));

          tstDB^.dbc^.GetSeq(tstDB^.dbr, testSeq);
          tstDB^.dbr^.GetField(DBTSTID, @pTestID, REStestIDLen);
          tstDB^.dbr^.GetField(DBTSTName, @pTestName, REStestNameLen);
          tstDB^.dbr^.GetField(DBTSTTstCat, @pTestCatID, REStestCatIDLen);
          tstDB^.dbr^.GetField(DBTSTDrugRef, @drugSeq, SizeOf(TSeqNum));
          tstDB^.dbr^.GetField(DBTSTTstCatRef, @tcatSeq, SizeOf(TSeqNum));

          {- Get length of result field defined for this test }
          tcatDB^.dbc^.GetSeq(tcatDB^.dbr, tcatSeq);
          tcatDB^.dbr^.GetField(DBTSTCATResLen, @resultLen, SizeOf(byte));

          {- Each result record read will have to be unpacked and each
            individual test must be put into the "in memory" list,
            matching it with it's appropriate test id.
          }
          StrCopy(resultStr, '');
          flag:= 0;
          if gotResult then
            UnPackResults(resultStr, pTestCatID, pos, resultLen, flag);

          {- add test information to result collection }
          resObj := New(PResRecObj, Init(pTestGroupID, pTGUD,
                                         pTestID, pTestCatID, pTestName,
                                         isorderSeq, testSeq, drugSeq,
                                         resultStr, pos, resultLen, flag));
          Insert(resObj);
        until not todDB^.dbc^.GetNextContains(todDB^.dbr);

    until not isordDB^.dbc^.GetNextContains(isordDB^.dbr);

    MSDisposeObj(todDB);
    MSDisposeObj(tcatDB);
    MSDisposeObj(tstDB);
    MSDisposeObj(toDB);
  end;

  MSDisposeObj(isordDB);

  isOK := isOK and (dbErrorNum = 0);
  resultsLoaded := isOK;
  LoadResults := isOK;
end;

function TV3Results.SaveResults: boolean;
{- save results to database }
var
  isorderSeq   : TSeqNum;
  gotResult    : boolean;
  res          : PResRecObj;
  j            : integer;
begin
  SaveResults := FALSE;
  dbErrorNum := 0;
  {- see if results have been previously loaded }
  if (not resultsLoaded) or (count = 0) then
    Exit;

  {- for each test order: pack results into resBuf then put into
    the result record and store into the result file }

  j := 0;
  res := At(0);
  isorderSeq := res^.isordSeq;
  while (j < count) and (dbErrorNum = 0) do
  begin
    while (j < count) and (isorderSeq = res^.isordSeq) do
    begin
      PackResults(res);
      res^.modified := FALSE;
      Inc(j);
      if j < count then
        res := At(j);
    end;

    resDB^.dbr^.PutField(DBRESIsoOrdRef, @isorderSeq);
    gotResult := resDB^.dbc^.GetEQ(resDB^.dbr);
    resDB^.dbr^.PutField(DBRESBlock, resBuf);
    if gotResult then
      resDB^.dbc^.UpdateRec(resDB^.dbr)
    else
      resDB^.dbc^.InsertRec(resDB^.dbr);

    dbErrorNum := resDB^.dbc^.dbErrorNum;
    isorderSeq := res^.isordSeq;
  end;
  SaveResults := (dbErrorNum = 0);
end;

function TV3Results.DeleteResults: boolean;
{- delete all results that are currently loaded. LoadResults must be
   called after this routine if new results are to be dealt with }
var
  k     : integer;
  res   : PResRecObj;
begin
  DeleteResults := FALSE;
  dbErrorNum := 0;
  {- see if results have been previously loaded}
  if (not resultsLoaded) or (count = 0) then
    Exit;

  {- delete all old results }
  k := 0;
  repeat
    res := At(k);
    resDB^.dbr^.PutField(DBRESIsoOrdRef, @res^.isordSeq);
    if resDB^.dbc^.GetEQ(resDB^.dbr) then
    begin
      resDB^.dbc^.DeleteRec(resDB^.dbr);
      if dbErrorNum = 0 then
        dbErrorNum := resDB^.dbc^.dbErrorNum;
    end;
  until not NextTestGroup(res^.testgroupID, k);

  FreeAll;     {- clear all results from memory collection }
  resultsLoaded := FALSE;
  DeleteResults := (dbErrorNum = 0);
end;

function TV3Results.IsModified: boolean;

  function IsMod(item: PResRecObj): boolean; Far;
  begin
    IsMod := item^.modified;
  end;

begin
  IsModified := FALSE;

  {- see if results have been previously loaded}
  if (not resultsLoaded) or (count = 0) then
    Exit;

  IsModified := (FirstThat(@IsMod) <> nil);
end;

procedure TV3Results.ClearModify;

  procedure ClrMod(item: PResRecObj); Far;
  begin
    item^.modified := FALSE;
  end;

begin
  ForEach(@ClrMod);
end;

function TV3Results.GetResult(aTestID, aTestCatID, res: PChar;
                            maxChars: integer): boolean;
{- return a result for the particular test ID }

  function IsMatch(item: PResRecObj): boolean; Far;
  begin
    IsMatch := (StrComp(item^.testID, aTestID) = 0) and
               (StrComp(item^.testCatID, aTestCatID) = 0);
  end;

var
  found : PResRecObj;
begin
  GetResult := FALSE;
  res[0] := #0;
  found := FirstThat(@IsMatch);
  if found <> nil then
  begin
    if maxChars > RESmaxStrResult then
      maxChars := RESmaxStrResult;
    StrMove(res, found^.result, maxChars);
    res[maxChars] := #0;
    GetResult := TRUE;
  end;
end;

function TV3Results.SetResult(aTestID, aTestCatID, res: PChar;
                            overwrite: boolean): boolean;
{- set a result for the test ID in resObj. If overwrite is FALSE the result
  will not be changed if it already contains a value. If overwrite is
  true then the result will always be overwritten }

  function IsMatch(item: PResRecObj): boolean; Far;
  begin
    IsMatch := (StrComp(item^.testID, aTestID) = 0) and
               (StrComp(item^.testCatID, aTestCatID) = 0);
  end;

var
  found : PResRecObj;
begin
  SetResult := FALSE;
  found := FirstThat(@IsMatch);
  if found <> nil then
    if overwrite or (not overwrite and (StrLen(found^.result) = 0)) then
    begin
      found^.SetResult(res);
      SetResult := TRUE;
    end;
end;

function TV3Results.GetFlag(aTestID, aTestCatID: PChar; flagPos: byte): boolean;
var
  found : PResRecObj;

  function IsMatch(item: PResRecObj): boolean; Far;
  begin
    IsMatch := (StrComp(item^.testID, aTestID) = 0) and
               (StrComp(item^.testCatID, aTestCatID) = 0);
  end;

begin
  found := FirstThat(@IsMatch);
  if found = nil then
    GetFlag := FALSE
  else
    GetFlag := (found^.flag and (1 shl (flagPos mod 8))) <> 0;
end;

procedure TV3Results.SetFlag(aTestID, aTestCatID: PChar; flagPos: byte; aVal: boolean);
var
  found : PResRecObj;

  function IsMatch(item: PResRecObj): boolean; Far;
  begin
    IsMatch := (StrComp(item^.testID, aTestID) = 0) and
               (StrComp(item^.testCatID, aTestCatID) = 0);
  end;

begin
  found := FirstThat(@IsMatch);
  if found <> nil then
  begin
    if aVal then
      found^.flag := found^.flag or (1 shl (flagPos mod 8))
    else
      found^.flag := found^.flag and not (1 shl (flagPos mod 8));
  end;
end;

function TV3Results.GetDrugResults(aDrugSeq: TSeqNum; MICres, NCCLSres: PChar;
                                 maxChars: integer; var contraInd: boolean): boolean;
{- return results for the particular drug }

  function IsMatch(item: PResRecObj): boolean; Far;
  begin
    IsMatch := (item^.drugSeq = aDrugSeq);
  end;

var
  found : PResRecObj;
begin
  if maxChars > RESmaxStrResult then
    maxChars := RESmaxStrResult;
  MICres[0] := #0;
  NCCLSres[0] := #0;
  found := FirstThat(@IsMatch);
  while found <> nil do
  begin
    if StrComp(found^.testCatID, 'MIC') = 0 then
    begin
      StrMove(MICres, found^.result, maxChars);
      MICres[maxChars] := #0;
    end
    else if StrComp(found^.testCatID, 'NCCLS') = 0 then
    begin
      StrMove(NCCLSres, found^.result, maxChars);
      NCCLSres[maxChars] := #0;
      contraInd := TestBit(found^.flag, REScontraIndicated);
    end;
    found := At(IndexOf(found) + 1);
    if found^.drugSeq <> aDrugSeq then
      found := nil;
  end;
  GetDrugResults := (MICres[0] <> #0) and (NCCLSres[0] <> #0);
end;

function TV3Results.AnyResultsExist: boolean;
{- return true if ANY non-null results exist }
  function IsFound(item: PResRecObj): boolean; Far;
  begin
    IsFound := StrComp(item^.result, '') <> 0;
  end;

begin
  AnyResultsExist := FirstThat(@IsFound) <> nil;
end;

function TV3Results.IDResultsExist: boolean;
{- return true if drug or biotype non-null results exist }
  function IsFound(item: PResRecObj): boolean; Far;
  begin
    IsFound := (StrComp(item^.result, '') <> 0) and
               ((StrComp(item^.testCatID, 'MIC') = 0) or
                (StrComp(item^.testCatID, 'NCCLS') = 0) or
                (StrComp(item^.testCatID, 'BIO') = 0));
  end;

begin
  IDResultsExist := FirstThat(@IsFound) <> nil;
end;

function TV3Results.MICResultsExist: boolean;
{- return true if MIC non-null results exist }
  function IsFound(item: PResRecObj): boolean; Far;
  begin
    IsFound := (StrComp(item^.result, '') <> 0) and
               (StrComp(item^.testCatID, 'MIC') = 0);
  end;

begin
  MICResultsExist := FirstThat(@IsFound) <> nil;
end;

function TV3Results.FindTestGroup(aTestGroupID: PChar): integer;
{- return index of first result with specified test order ID.
  Return -1 if no match. }

  function Matches(item: PResRecObj): boolean; Far;
  begin
    Matches := (StrComp(item^.testgroupID, aTestGroupID) = 0);
  end;

var
  res   : PResRecObj;
begin
  res := FirstThat(@Matches);
  if res <> nil then
    FindTestGroup := IndexOf(res)
  else
    FindTestGroup := -1;
end;

function TV3Results.NextTestGroup(aTestGroupID: PChar; var index: integer): boolean;
{- return the next test order ID in result block. aTestGroupID must
  contain the current test order ID (if aTestGroupID = '' then the
  first test order ID will be returned). If no test order IDs
  exist after aTestGroupID then FALSE will be returned. }
var
  k     : integer;
  p     : PResRecObj;
  found : boolean;
begin
  index := -1;
  NextTestGroup := FALSE;
  if count = 0 then
  begin
    StrCopy(aTestGroupID, '');
    Exit;
  end;

  if StrComp(aTestGroupID, '') = 0 then
  begin
    p := At(0);
    StrLCopy(aTestGroupID, p^.testgroupID, REStestgroupIDLen-1);
    index := 0;
    NextTestGroup:= true;
    Exit;
  end;

  found := FALSE;
  k := FindTestGroup(aTestGroupID);  {- find current test order in list }
  if k <> -1 then
  begin
    while (k < count) and not found do
    begin
      p := At(k);
      found := StrComp(p^.testgroupID, aTestGroupID) <> 0;
      if found then
        Break;
      Inc(k);
    end;
    if found then
    begin
      StrLCopy(aTestGroupID, p^.testgroupID, REStestgroupIDLen-1);
      NextTestGroup := TRUE;
    end;
  end;
  index := k;
end;

function TV3Results.NumTests(aTestGroupID: PChar; var first: integer): integer;
{- calculate the number of results associated with a test order. Return
  the index of the first occurance and the count. First will equal -1 if
  the test order is not found }
var
  k     : integer;
  tgID  : array [0..REStestgroupIDLen-1] of char;
begin
  NumTests := 0;
  StrLCopy(tgID, aTestGroupID, REStestgroupIDLen-1);
  first := FindTestGroup(tgID);
  if (first <> -1) then
  begin
    NextTestGroup(tgID, k);
    if k = -1 then
      NumTests := count - first
    else
      NumTests := k - first;
  end;
end;

procedure TV3Results.ByteToMIC(mic: PChar; bVal: byte);
{- convert byte value to MIC string }
var
  pstr  : array [0..RESmaxStrResult] of char;
begin
  StrCopy(mic, '');
  if bVal = $FF then
    Exit;
  if bVal and greaterThan <> 0 then
    StrCat(mic, '>')
  else if bVal and LessThan <> 0 then
    StrCat(mic, '<')
  else
    StrCat(mic, ' ');
  {- get MIC index into master dil array. Mask off greater/less than bits
    using $1F }
  StrPCopy(pstr, traysObj^.gMda[bVal and $1F]);
  TrimAll(pstr, pstr, ' ', RESmaxStrResult);
  StrLCat(mic, pstr, RESmaxStrResult);
end;

procedure TV3Results.MICToByte(mic: PChar; var bVal: byte);
{- convert an MIC value (ie, >0.5) to a byte value }
var
  tstr  : string[RESmaxStrResult];
  mstr  : string[RESmaxStrResult];
  found : boolean;
  k     : integer;
begin
  bVal := $FF;
  TrimAll(mic, mic, ' ', RESmaxStrResult);
  if StrLen(mic) = 0 then
    Exit;

  tstr := StrPas(mic);
  if tstr[1] = '<' then
  begin
    bVal := lessThan;
    tstr[1] := ' ';
  end
  else if tstr[1] = '>' then
  begin
    bVal := greaterThan;
    tstr[1] := ' ';
  end
  else
  begin
    bVal := 0;
    tstr := ' ' + tstr;
  end;
  while Length(tstr) < Length(traysObj^.gMda[1]) do
    tstr := tstr + ' ';

  found := FALSE;
  k := 0;
  while not found and (k < MaxMDAEntries) do
  begin
    mstr := traysObj^.gMda[k];
    if mstr = tstr then
    begin
      bVal := bVal or k;
      found := TRUE;
    end;
    Inc(k);
  end;
  if not found then
    bVal := $FF;
end;

procedure TV3Results.UnPackResults(resultStr, pTestCatID: PChar;
                                 pos, resultLen: integer; var flag: word);
var
  num   : byte;
  ci    : boolean;
begin
  flag := 0;
  if StrComp(pTestCatID, 'MIC') = 0 then
  begin
    Move(resBuf[pos], num, 1);
    ByteToMIC(resultStr, num);
  end
  else if StrComp(pTestCatID, 'NCCLS') = 0 then
  begin
    ci := FALSE;
    Move(resBuf[pos], num, 1);
    InterpCodeToStr(num, resultStr, maxInterpStrLen, ci);
    if ci then
      SetBit(flag, REScontraIndicated);
  end
  else
  begin
    Move(resBuf[pos], resultStr^, resultLen);
    resultStr[resultLen] := #0;
  end;
end;

procedure TV3Results.PackResults(p: PResRecObj);
{- Pack a result into resBuf }
var
  num      : byte;
begin
  if StrComp(p^.testCatID, 'MIC') = 0 then
  begin
    MICToByte(p^.result, num);
    Move(num, resBuf[p^.pos], 1);
  end
  else if StrComp(p^.testCatID, 'NCCLS') = 0 then
  begin
    num := byte(InterpStrToCode(p^.result, TestBit(p^.flag, REScontraIndicated)));
    Move(num, resBuf[p^.pos], 1);
  end
  else
  begin
    Pad(p^.result, p^.result, #0, p^.maxResLen);
    Move(p^.result, resBuf[p^.pos], p^.maxResLen);
  end;
end;


var
  desc    : PDBReadDesc;
  fi      : TFldInfo;

BEGIN
  firstTime := TRUE;
  desc := New(PDBReadDesc, Init(DBRESFile, ''));
  if desc = nil then
  begin
    resFieldSize := 0;
    BadError(Err_OpenRESULT_Desc, dbLastOpenError, TRUE);
  end
  else
  begin
    desc^.FieldInfo(DBRESBlock, fi);
    resFieldSize := fi.fldSize;
  end;

  MSDisposeObj(desc);
END.

