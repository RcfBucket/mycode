unit DMSMain;

{----------------------------------------------------------------------------}
{  Module Name  : DMSMain.PAS                                                }
{  Programmer   : MJS - so far                                               }
{  Date Created : 03/95                                                      }
{                                                                            }
{  Purpose - This module provides the top-level logic for the DMS program.   }
{    It handles initialization, menu command dispatching, and shut down.     }
{                                                                            }
{  Assumptions - File names of external programs                             }
{                                                                            }
{  Initialization -                                                          }
{  None                                                                      }
{                                                                            }
{  Revision History -                                                        }
{                                                                            }
{  Version  Date      Who    Did What                                        }
{  -------  --------  ------ ----------------------------------------------- }
{  1.00     03/95     MJS    Created unit.                                   }
{  Now under revision control. Use PVCS to view revision history             }
{                                                                            }
{----------------------------------------------------------------------------}

INTERFACE

procedure Start;

IMPLEMENTATION

uses
  MTASK,
  AS4QC,
  CtlLib,
  APITools,
  UserMsgs,
  DMSErr,
  ChkSpace,
  CTL3D,
  Custom,
  DLGLib,
  DMString,
  DMSDebug,
  EpiCnfg,
  ESBL,
  GSDef,
  HWConfig,
  IniLib,
  IntlLib,
  IntrpMap,
  MScan,
  MoveIso,
  Objects,
  oDialogs,
  OWindows,
  PatReps,
  Final,
  Screens,
  Sessions,
  Strings,
  Threshld,
  UIIDMIC,
  UIIso,
  UISpec,
  UISpcLog,
  UIPat,
  UIGSRes,
  UIQEOrd,
  WinDOS,
  Win31,
  WinProcs,
  WinTypes;

{$R DMSMAIN.RES}
{$I DMSMAIN.INC}
{$R DMSLOGO.RES}
{$I DMSLOGO.INC}

type

  {==========================================================================}
  { This object provides the structure for the DMS application. It is based
  { on an ObjectWindows 3D application. It adds an application defined main
  { window.
  {==========================================================================}

  TDMApplication = object(T3DApplication)
    procedure InitMainWindow; virtual;
  end;



  {==========================================================================}
  { This object provides the structure for the application's main window. It
  { is based on an ObjectWindows 3D window. It adds message processing and
  { other functionality required by the program.
  {==========================================================================}

  PDMSMain = ^TDMSMain;
  TDMSMain = object(T3DWindow)
    twoWayRunning : boolean;
    waRunning     : boolean;
    {}
    constructor Init(aParent: PWindowsObject; aTitle: PChar);
    destructor Done; virtual;
    procedure SetupWindow;  virtual;
    function  CanClose: boolean; virtual;
    procedure GetWindowClass(var aWndClass: TWndClass); virtual;
    function GetClassName: PChar; virtual;
    procedure Paint(PaintDC: HDC; var PaintInfo: TPaintStruct); virtual;
    procedure WMWinINI(var msg: TMessage); virtual WM_FIRST + WM_WININICHANGE;
    procedure ChangeTitle(var msg : TMessage); virtual WM_FIRST + WM_CHANGETITLE;
    procedure Inquiry(var msg: TMessage); virtual WM_FIRST + WM_INQUIRE;
    procedure WMDMSQuery(var msg: TMessage); virtual WM_FIRST + WM_MAINPROGQUERY;

    {File Options}
    procedure FileStatistics(var msg: TMessage); virtual CM_FIRST + MNU_FILESTATISTICS;
    procedure ProgramExit(var msg: TMessage); virtual CM_FIRST + MNU_PROGRAMEXIT;

    {Data Entry Options}
    procedure SpecimenLogin(var msg: TMessage); virtual CM_FIRST + MNU_SPECIMENLOGIN;
    procedure GramStainResults(var msg: TMessage); virtual CM_FIRST + MNU_GRAMSTAINRESULTS;
    procedure IsolateEntry(var msg: TMessage); virtual CM_FIRST + MNU_ISOLATE;
    procedure OrderEntry(var msg: TMessage); virtual CM_FIRST + MNU_ORDERENTRY;
    procedure SpecimenEntry(var msg: TMessage); virtual CM_FIRST + MNU_SPECIMENENTRY;
    procedure PatientEntry(var msg: TMessage); virtual CM_FIRST + MNU_PATIENTENTRY;
    procedure IDMICEntry(var msg: TMessage); virtual CM_FIRST + MNU_IDMICENTRY;

    {Reports Options}
    procedure PatientReports(var msg: TMessage); virtual CM_FIRST + MNU_REPORTS;

    {Customization Options}
    procedure CustMnemonics(var msg: TMessage); virtual CM_FIRST + MNU_CUSTMNEMONICS;
    procedure CustWindows(var msg: TMessage); virtual CM_FIRST + MNU_CUSTWINDOWS;
    procedure CustHWConfig(var msg: TMessage); virtual CM_FIRST + MNU_CUSTHWCONFIG;
    procedure CustDrugs(var msg: TMessage); virtual CM_FIRST + MNU_CUSTDRUGS;
    procedure CustTestDef(var msg: TMessage); virtual CM_FIRST + MNU_CUSTTESTDEF;
    procedure CustTestGroup(var msg: TMessage); virtual CM_FIRST + MNU_CUSTTESTGROUP;
    procedure CustOrder(var msg: TMessage); virtual CM_FIRST + MNU_CUSTORDERS;
    procedure CustOrganism(var msg: TMessage); virtual CM_FIRST + MNU_CUSTORGANISM;
    procedure CustGramStainDef(var msg: TMessage); virtual CM_FIRST + MNU_CUSTGRAMSTAINDEF;
    procedure CustGramStainVals(var msg: TMessage); virtual CM_FIRST + MNU_CUSTGRAMSTAINVALS;
    procedure CustIntMapping(var msg: TMessage); virtual CM_FIRST + MNU_CUSTINTMAPPING;
    procedure CustSessions(var msg: TMessage); virtual CM_FIRST + MNU_CUSTSESSIONS;
    procedure CustThresholds(var msg: TMessage); virtual CM_FIRST + MNU_CUSTTHRESHOLDS;

    procedure ToggleScreen(ScreenStr: PChar; Screen_MNU: Integer);
    procedure CustESBL(var msg: TMessage);  virtual CM_First + MNU_ESBL;
    procedure CustESBLOrgMapping(var msg: TMessage);  virtual CM_First + MNU_ESBLOrgs;
    procedure CustMRS(var msg: TMessage);  virtual CM_First + MNU_MRS;
    procedure CustE157(var msg: TMessage);  virtual CM_First + MNU_E157;
    procedure CustVIE(var msg: TMessage);   virtual CM_First + MNU_VIE;
    procedure CustVRE(var msg: TMessage);   virtual CM_First + MNU_VRE;
    procedure CustIB(var msg: TMessage);  virtual CM_First + MNU_IB;
    procedure CustAutoResolve(var msg: TMessage); virtual CM_First + MNU_AutoResolve;
    procedure CustOxMultResHold(var msg: TMessage); virtual CM_First + MNU_OxMultResHold;
    procedure CustVaStSHold(var msg: TMessage); virtual CM_First + MNU_VaStSHold;
    procedure CustVaOnlyHold(var msg: TMessage); virtual CM_First + MNU_VaOnlyHold;
    procedure CustMultiDrugs(var msg: TMessage);  virtual CM_First + MNU_MultiDrugs;
    procedure CustXtras(var msg: TMessage);  virtual CM_First + MNU_Xtras;
    procedure GrayOutAutoResolve;
    procedure GrayOutPDLHolds;
    procedure SetScreens;

    procedure CustReportsEpi(var msg: TMessage); virtual CM_FIRST + MNU_CUS_REP_EPI;

    {Utility Options}
    procedure FinalSpecimens(var msg: TMessage); virtual CM_FIRST + MNU_FINALSPECIMENS;
    procedure MoveIsos(var msg: TMessage); virtual CM_FIRST + MNU_MOVEISOLATES;
    procedure AS4QCDiag(var msg: TMessage); virtual CM_FIRST + MNU_AS4QCDIAG;
    procedure WalkAway(var msg: TMessage); virtual CM_FIRST + MNU_WALKAWAY;
    procedure TwoWay(var msg: TMessage); virtual CM_FIRST + MNU_TWOWAYINT;

    {Help Options}
    procedure HelpIndex(var msg: TMessage); virtual CM_FIRST + MNU_HELPINDEX;
    procedure HelpUsing(var msg: TMessage); virtual CM_FIRST + MNU_HELPUSING;
    procedure HelpAbout(var msg: TMessage); virtual CM_FIRST + MNU_ABOUT;
  end;



{============================================================================}
{ This constant ...
{============================================================================}

const
  MaxTitleLen   = 256;



{============================================================================}
{ This variable ...
{============================================================================}

var
  DMSApp      : TDMApplication;



{============================================================================}
{  This local procedure builds the program title string. It reads the DMS    }
{  title string and CONCATs the name of the current session.                 }
{                                                                            }
{  Assumptions                                                               }
{    none                                                                    }
{  Errors returned                                                           }
{    none                                                                    }
{  Returns  : none                                                           }
{  InParms  : a PChar to place the title in.                                 }
{  OutParms : the title + current session name.                              }
{============================================================================}

procedure MakeTitle(title: PChar);
var
  tempStr : array[0..100] of char;
begin
  if title <> nil then
  begin
    SR(IDS_TITLE, title, MaxTitleLen);
    if GetCurrentSession(tempStr, sizeof(tempStr)-1) = 0 then
      SR(IDS_SESSIONSDISABLED, tempStr, sizeof(tempStr)-1);
    StrCat(title, tempStr);
  end;
end;



{============================================================================}
{ This procedure ...
{============================================================================}

procedure TDMSMain.ChangeTitle(var msg : TMessage);
var
  temp : array [0..127] of char;
begin
  MakeTitle(temp);
  SetWindowText(HWindow, temp);
end;



{============================================================================}
{ This procedure ...
{============================================================================}

function TDMSMain.GetClassName: PChar;
begin
  GetClassName:= application^.name;
end;



{============================================================================}
{ This procedure ...
{============================================================================}

procedure TDMSMain.GetWindowClass(var aWndClass: TWndClass);
begin
  inherited GetWindowClass(aWndClass);
  aWndClass.hIcon:= LoadIcon(hInstance, MAKEINTRESOURCE(ICN_DADE));
end;



{============================================================================}
{ This procedure ...
{============================================================================}

constructor TDMSMain.Init(aParent: PWindowsObject; aTitle: PChar);
begin
  if not inherited Init(aParent, aTitle) then
  begin
    Done;
    fail;
  end;

  attr.menu:= LoadMenu(hInstance, MAKEINTRESOURCE(MNU_MAIN));
  attr.X:= CW_USEDEFAULT;
  attr.Y:= CW_USEDEFAULT;
  attr.w:= GetSystemMetrics(SM_CXFULLSCREEN);
  attr.h:= GetSystemMetrics(SM_CYFULLSCREEN);

  twoWayRunning := false;
  waRunning:= false;
end;



{============================================================================}
{ This destructor ...
{============================================================================}

destructor TDMSMain.Done;
begin
  PostMessage(HWND_BROADCAST, WM_INQUIRE, ProgramStopped, MakeLong(InqMsgDMS, InqMsgAll));
  inherited Done;
end;



{============================================================================}
{ This procedure ...
{============================================================================}

procedure TDMSMain.SetupWindow;
begin
  inherited SetupWindow;
  ShowWindow(hWindow, SW_SHOWMAXIMIZED);
  if not HWExists(pkTwoWay) then
    EnableMenuItem(Attr.Menu, MNU_TWOWAYINT, MF_GRAYED);
  if not HWExists(pkAS4) then
    EnableMenuItem(Attr.Menu, MNU_AS4QCDIAG, MF_GRAYED);
  if not HWExists(pkWalkAway) then
  begin
    EnableMenuItem(Attr.Menu, MNU_WALKAWAY, MF_GRAYED);
    EnableMenuItem(Attr.Menu, MNU_AutoResolve, MF_GRAYED);
    EnableAutoResolve(False);
  end;

  {- tell other apps the the DMS has just started. }
  PostMessage(HWND_BROADCAST, WM_INQUIRE, ProgramStarted, MakeLong(InqMsgDMS, InqMsgAll));

  {- broadcast and inquire to see who is running }
  PostMessage(HWND_BROADCAST, WM_INQUIRE, ProgramInquire, MakeLong(InqMsgDMS, InqMsgAll));

  SetScreens;
end;



{============================================================================}
{  Displays the exit prompt and closes the window if the user selects 'yes'. }
{  Assumptions                                                               }
{    none                                                                    }
{  Errors returned                                                           }
{    none                                                                    }
{  Returns  : true if the user wishes to exit the DMS, false otherwise.      }
{  InParms  : none                                                           }
{  OutParms : none                                                           }
{============================================================================}

function TDMSMain.CanClose: boolean;
var
  p1, p2  : array[0..100] of char;
begin
  CanClose:= false;
  if inherited CanClose then
  begin
    SR(IDS_CONFIRM, p1, sizeof(p1)-1);
    SR(IDS_EXITMSG, p2, sizeof(p2)-1);
    CanClose:= YesNoMsg(hwindow, p1, p2);
  end;
end;



{============================================================================}
{ This procedure ... file menu items
{============================================================================}

procedure TDMSMain.FileStatistics(var msg: TMessage);
begin
  DatabaseStatistics(@Self);
end;



{============================================================================}
{ This procedure ...
{============================================================================}

procedure TDMSMain.ProgramExit(var msg: TMessage);
begin
  CloseWindow;
end;



{============================================================================}
{ This procedure ...data entry menu items
{============================================================================}

procedure TDMSMain.SpecimenLogin(var msg: TMessage);
begin
  if EnoughDiskSpace(@Self) then
    UISpecimenLogin(@Self, 0);
end;



{============================================================================}
{ This procedure ...
{============================================================================}

procedure TDMSMain.GramStainResults(var msg: TMessage);
begin
  if EnoughDiskSpace(@Self) then
    UIGSResults(@Self, 0);
end;



{============================================================================}
{ This procedure ...
{============================================================================}

procedure TDMSMain.IsolateEntry(var msg: TMessage);
begin
  if EnoughDiskSpace(@Self) then
    UIIsoEntry(@Self, 0, 0);
end;



{============================================================================}
{ This procedure ...
{============================================================================}

procedure TDMSMain.OrderEntry(var msg: TMessage);
begin
  if EnoughDiskSpace(@Self) then
    UIQuickOrdEntry(@Self);
end;



{============================================================================}
{ This procedure ...
{============================================================================}

procedure TDMSMain.SpecimenEntry(var msg: TMessage);
begin
  if EnoughDiskSpace(@Self) then
    UISpecimenEntry(@Self, 0);
end;



{============================================================================}
{ This procedure ...
{============================================================================}

procedure TDMSMain.PatientEntry(var msg: TMessage);
begin
  if EnoughDiskSpace(@Self) then
    UIPatientEntry(@Self, 0);
end;



{============================================================================}
{ This procedure ...
{============================================================================}

procedure TDMSMain.IDMICEntry(var msg: TMessage);
begin
  if EnoughDiskSpace(@Self) then
    UIIDMICEntry(@Self, 0, false);
end;



{============================================================================}
{ This procedure ...
{============================================================================}

procedure RunProgram(aParent: PWindowsObject; aName: PChar);
var
  tw    : TWaitCursor;
  ret   : word;
begin
  tw.Init;
  ret:= WinExec(aName, SW_SHOWNORMAL);
  tw.Done;
  if ret < 32 then
  begin
    ShowError(aParent, IDS_ERREXEC, nil, ret, MOD_DMSMAIN, 0);
  end;
end;



{============================================================================}
{ This procedure ... report options
{============================================================================}

procedure TDMSMain.PatientReports(var msg: TMessage);
begin
  RunProgram(@self, 'PRTMAIN.EXE');
end;



{============================================================================}
{ This procedure ... customization options
{============================================================================}

procedure TDMSMain.CustMnemonics(var msg: TMessage);
begin
  EditMnemonicFiles(@Self);
end;



{============================================================================}
{ This procedure ...
{============================================================================}

procedure TDMSMain.CustWindows(var msg: TMessage);
var
  dirStr : array[0..100] of char;
begin
  GetWindowsDirectory(dirStr, sizeof(dirStr)-1);
  StrLCat(dirStr, '\CONTROL.EXE', sizeof(dirStr)-1);
  RunProgram(@self, dirStr);
end;



{============================================================================}
{ This procedure ...
{============================================================================}

procedure TDMSMain.CustHWConfig(var msg: TMessage);
begin
  ConfigureHW(@Self);
end;



{============================================================================}
{ This procedure ...
{============================================================================}

procedure TDMSMain.CustDrugs(var msg: TMessage);
begin
  DefineDrugs(@self);
end;



{============================================================================}
{ This procedure ...
{============================================================================}

procedure TDMSMain.CustTestDef(var msg: TMessage);
begin
  DefineTests(@self);
end;



{============================================================================}
{ This procedure ...
{============================================================================}

procedure TDMSMain.CustTestGroup(var msg: TMessage);
begin
  DefineTestGroups(@self);
end;



{============================================================================}
{ This procedure ...
{============================================================================}

procedure TDMSMain.CustOrder(var msg: TMessage);
begin
  DefineOrders(@self);
end;



{============================================================================}
{ This procedure ...
{============================================================================}

procedure TDMSMain.CustOrganism(var msg: TMessage);
begin
  DefineOrganisms(@self);
end;



{============================================================================}
{ This procedure ...
{============================================================================}

procedure TDMSMain.CustGramStainDef(var msg: TMessage);
begin
  DefineGSDef(@Self);
end;



{============================================================================}
{ This procedure ...
{============================================================================}

procedure TDMSMain.CustGramStainVals(var msg: TMessage);
begin
  DefineGSVals(@Self);
end;



{============================================================================}
{ This procedure ...
{============================================================================}

procedure TDMSMain.CustIntMapping(var msg: TMessage);
begin
  DialogEditInterpMap(@Self);
end;



{============================================================================}
{ This procedure ...
{============================================================================}

procedure TDMSMain.CustSessions(var msg: TMessage);
begin
  DefineSessions(@Self);
end;



{============================================================================}
{ This procedure ...
{============================================================================}

procedure TDMSMain.CustThresholds(var msg: TMessage);
begin
  DBOptions(@Self);
end;


{============================================================================}
{ These procedures handle the customization of ESBL/MRS/E157/VIE/VRE screens
{============================================================================}
procedure TDMSMain.ToggleScreen(ScreenStr: PChar; Screen_MNU: Integer);
var MenuChecked: Boolean;
begin
  MenuChecked:=(GetMenuState(Attr.Menu, Screen_MNU, MF_BYCOMMAND) and MF_Checked)<>0;
  if MenuChecked
     then  CheckMenuItem(Attr.Menu, Screen_MNU, MF_UnChecked)
     else  CheckMenuItem(Attr.Menu, Screen_MNU, MF_Checked);
  EnableScreen(ScreenStr, Not MenuChecked);
end;

procedure TDMSMain.CustESBL(var msg: TMessage);
var GrayOutOrgMapping: Boolean;
begin
  ToggleScreen(ESBLScreen, MNU_ESBL);

  GrayOutAutoResolve;
  GrayOutOrgMapping :=(GetMenuState(Attr.Menu, MNU_ESBL, MF_BYCOMMAND) and MF_Checked)=0;

  if GrayOutOrgMapping
     then EnableMenuItem(Attr.Menu, MNU_ESBLOrgs,MF_GRAYED)
     else EnableMenuItem(Attr.Menu, MNU_ESBLOrgs,MF_ENABLED);
end;

procedure TDMSMain.CustESBLOrgMapping(var msg: TMessage);
begin
  ToggleScreen(ESBLOrgs, MNU_ESBLOrgs);
end;

procedure TDMSMain.CustMRS(var msg: TMessage);
begin
  ToggleScreen(MRSScreen, MNU_MRS);
end;

procedure TDMSMain.CustE157(var msg: TMessage);
begin
  ToggleScreen(E157Screen, MNU_E157);
end;

procedure TDMSMain.CustVIE(var msg: TMessage);
begin
  ToggleScreen(VIEScreen, MNU_VIE);
end;

procedure TDMSMain.CustVRE(var msg: TMessage);
begin
  ToggleScreen(VREScreen, MNU_VRE);
end;

procedure TDMSMain.CustIB(var msg: TMessage);
begin
  ToggleScreen(IBInterps, MNU_IB);
end;

procedure TDMSMain.CustAutoResolve(var msg: TMessage);
begin
  ToggleScreen(AutoResolveKey, MNU_AutoResolve);
end;

procedure TDMSMain.CustVaOnlyHold(var msg: TMessage);
begin
  ToggleScreen(VaOnlyHold, MNU_VaOnlyHold);
  If ScreenOn(VaOnlyHold) Then
    EnableMenuItem(Attr.Menu, MNU_VaStSHold, MF_ENABLED)
  Else
  Begin
    EnableMenuItem(Attr.Menu, MNU_VaStSHold, MF_GRAYED);
    CheckMenuItem(Attr.Menu, MNU_VaStSHold, MF_UnChecked);
  End;
end;

procedure TDMSMain.CustVaStSHold(var msg: TMessage);
begin
  ToggleScreen(VaStSHold, MNU_VaStSHold);
end;

procedure TDMSMain.CustOxMultResHold(var msg: TMessage);
begin
  ToggleScreen(OxMultResHold, MNU_OxMultResHold);
end;

procedure TDMSMain.CustMultiDrugs(var msg: TMessage);
begin
  ToggleScreen(FilterMultiDrugs, MNU_MultiDrugs);
end;

procedure TDMSMain.CustXtras(var msg: TMessage);
begin
  ToggleScreen(PrintXtrasKey, MNU_Xtras);
end;

procedure TDMSMain.GrayOutAutoResolve;
begin
  If (GetMenuState(Attr.Menu, MNU_ESBL, MF_BYCOMMAND) and MF_Checked)=0
     then EnableMenuItem(Attr.Menu, MNU_AutoResolve, MF_GRAYED)
     else if HWExists(pkWalkAway) then
          EnableMenuItem(Attr.Menu, MNU_AutoResolve, MF_ENABLED);
end;

procedure TDMSMain.GrayOutPDLHolds;
begin
  {check for the presence of a WalkAway}
  If HWExists(pkWalkAway) then
  Begin
    {Set the Va/StS hold settings}
    EnableMenuItem(Attr.Menu, MNU_VaOnlyHold, MF_ENABLED);
    If ScreenOn(VaOnlyHold) Then
    Begin
      CheckMenuItem(Attr.Menu, MNU_VaOnlyHold, MF_Checked);

      {Now handle the StS component}
      EnableMenuItem(Attr.Menu, MNU_VaStSHold, MF_ENABLED);
      If ScreenOn(VaStSHold) Then
        CheckMenuItem(Attr.Menu, MNU_VaStSHold, MF_Checked)
      Else
        CheckMenuItem(Attr.Menu, MNU_VaStSHold, MF_UnChecked);
    End
    Else
    Begin
      CheckMenuItem(Attr.Menu, MNU_VaOnlyHold, MF_UnChecked);

      {Without the Va only hold, the StS component mut be disabled}
      If ScreenOn(VaStSHold) then CheckMenuItem(Attr.Menu, MNU_VaStSHold, MF_UnChecked);
      EnableMenuItem(Attr.Menu, MNU_VaStSHold, MF_GRAYED);
    End;

    {Set the Ox hold settings}
    EnableMenuItem(Attr.Menu, MNU_OxMultResHold, MF_ENABLED);
    If ScreenOn(OxMultResHold) Then
      CheckMenuItem(Attr.Menu, MNU_OxMultResHold, MF_Checked)
    Else
      CheckMenuItem(Attr.Menu, MNU_OxMultResHold, MF_UnChecked);
  End
  Else
  Begin
    {Without a WalkAway all of this must be disabled}
    If ScreenOn(VaOnlyHold) then CheckMenuItem(Attr.Menu, MNU_VaOnlyHold, MF_UnChecked);
    EnableMenuItem(Attr.Menu, MNU_VaOnlyHold, MF_GRAYED);
    If ScreenOn(VaStSHold) then CheckMenuItem(Attr.Menu, MNU_VaStSHold, MF_UnChecked);
    EnableMenuItem(Attr.Menu, MNU_VaStSHold, MF_GRAYED);
    If ScreenOn(OxMultResHold) then CheckMenuItem(Attr.Menu, MNU_OxMultResHold, MF_UnChecked);
    EnableMenuItem(Attr.Menu, MNU_OxMultResHold, MF_GRAYED);
  End;
end;

{============================================================================}
{ This procedure sets the cust menus for all screens from their ini settings
{============================================================================}
procedure TDMSMain.SetScreens;
begin
  if ScreenOn(ESBLOrgs)
     then CheckMenuItem(Attr.Menu, MNU_ESBLOrgs, MF_Checked);

  if ScreenOn(ESBLScreen)
     then CheckMenuItem(Attr.Menu, MNU_ESBL, MF_Checked)
     else EnableMenuItem(Attr.Menu, MNU_ESBLOrgs, MF_GRAYED);

  if ScreenOn(MRSScreen)       then CheckMenuItem(Attr.Menu, MNU_MRS, MF_Checked);
  if ScreenOn(E157Screen)      then CheckMenuItem(Attr.Menu, MNU_E157, MF_Checked);
  if ScreenOn(VIEScreen)       then CheckMenuItem(Attr.Menu, MNU_VIE, MF_Checked);
  if ScreenOn(VREScreen)       then CheckMenuItem(Attr.Menu, MNU_VRE, MF_Checked);
  if ScreenOn(IBInterps)       then CheckMenuItem(Attr.Menu, MNU_IB, MF_Checked);
  if ScreenOn(AutoResolveKey)  then CheckMenuItem(Attr.Menu, MNU_AUTORESOLVE, MF_Checked);
  if ScreenOn(FilterMultiDrugs)then CheckMenuItem(Attr.Menu, MNU_MultiDrugs, MF_Checked);
  if ScreenOn(PrintXtrasKey)   then CheckMenuItem(Attr.Menu, MNU_Xtras, MF_Checked);

  GrayOutAutoResolve;
  GrayOutPDLHolds; {Enable/disable holds based on DMS.INI values and presence of WalkAway}
end;

{============================================================================}
{ This function maps the Customize/Reports/Epidemiology menu item to the
{ Configure Epidemiology Reports dialog box.
{============================================================================}

procedure TDMSMain.CustReportsEpi(var msg: TMessage);
begin
  CnfgEpiReportsDlg( @self );
end;


{============================================================================}
{ This procedure ...
{============================================================================}

procedure TDMSMain.Inquiry(var msg: TMessage);
{- respond to WM_INQUIRY messages. }
begin
  {- if message was not sent by self and it was sent to everybody or
     specifically to the DMS }
  if (msg.lParamLo <> InqMsgDMS) and ((msg.lParamHI = InqMsgAll) or
     (msg.lParamHI = InqMsgDMS)) then
  begin
    case msg.wParam of
      ProgramFocus :  {- someone asked to focus the DMS }
        begin
          ShowOwnedPopups(hWindow, true);
          SetFocus(GetLastActivePopup(hWindow));
        end;
      ProgramInquire :
        begin
          {- lParamLo is the sender of this inquiry }
          PostMessage(HWND_BROADCAST, WM_INQUIRE, ProgramStarted, MakeLong(InqMsgDMS, msg.lParamLo));
        end;
      ProgramStopped :
        if msg.lParamLo = InqMsgTwoWay then
        begin
          CheckMenuItem(attr.menu, MNU_TWOWAYINT, MF_BYCOMMAND or MF_UNCHECKED);
          twoWayRunning:= false;
        end
        else if msg.lParamLo = InqMsgWA then
        begin
          CheckMenuItem(attr.menu, MNU_WALKAWAY, MF_BYCOMMAND or MF_UNCHECKED);
          waRunning:= false;
        end;
      ProgramStarted :
        if msg.lParamLo = InqMsgTwoWay then
        begin
          CheckMenuItem(attr.menu, MNU_TWOWAYINT, MF_BYCOMMAND or MF_CHECKED);
          twoWayRunning:= true;
        end
        else if msg.lParamLo = InqMsgWA then
        begin
          CheckMenuItem(attr.menu, MNU_WALKAWAY, MF_BYCOMMAND or MF_CHECKED);
          waRunning:= true;
        end;
    end; { case }
  end;
end;



{============================================================================}
{ This procedure ...
{============================================================================}

procedure TDMSMain.WMDMSQuery(var msg: TMessage);
{- respond to the task module and tell it who I am }
begin
  StrLCopy(PChar(msg.lParam), application^.Name, msg.wParam);
  msg.result:= DMSMainProgRetCode;
end;



{============================================================================}
{ This procedure ...
{============================================================================}

procedure TDMSMain.FinalSpecimens(var msg: TMessage);
begin
  FinalizeSpecimens(@self);
end;



{============================================================================}
{ This procedure ...
{============================================================================}

procedure TDMSMain.WalkAway(var msg: TMessage);
begin
  if WARunning then
    PostMessage(HWND_BROADCAST, WM_INQUIRE, ProgramFocus, MakeLong(InqMsgDMS, InqMsgWA))
  else
  begin
    RunProgram(@self, 'DMSWA.EXE');
  end;
end;



{============================================================================}
{ This procedure ...
{============================================================================}

procedure TDMSMain.TwoWay(var msg: TMessage);
begin
  if twoWayRunning then
    PostMessage(HWND_BROADCAST, WM_INQUIRE, ProgramFocus, MakeLong(InqMsgDMS, InqMsgTwoWay))
  else
  begin
    RunProgram(@self, 'DMSINT.EXE');
  end;
end;



{============================================================================}
{ This procedure ...
{============================================================================}

procedure TDMSMain.MoveIsos(var msg: TMessage);
begin
  MoveIsolates(@self);
end;



{============================================================================}
{ This procedure ...
{============================================================================}

procedure TDMSMain.AS4QCDiag(var msg: TMessage);
begin
  DoQCDiag(@self);
end;



{============================================================================}
{ This procedure ... help options
{============================================================================}

procedure TDMSMain.HelpIndex(var msg: TMessage);
var
  helpF: array[0..32] of char;
begin
  StrCopy(helpF, 'JDMSHLP.HLP');
  WinHelp(hwindow, helpF, HELP_INDEX, 0);
end;



{============================================================================}
{ This procedure ...
{============================================================================}

procedure TDMSMain.HelpUsing(var msg: TMessage);
var
  HelpF: array[0..32] of char;
begin
  StrCopy(helpF, 'JDMSHLP.HLP');
  WinHelp(hwindow, helpF, Help_HelpOnHelp, 0);
end;



{============================================================================}
{ This type ...
{============================================================================}

type
  PAboutDlg = ^TAboutDlg;
  TAboutDlg = object(TCenterDlg)
    constructor Init(aParent: PWindowsObject);
    procedure SetUpWindow; virtual;
  end;



{============================================================================}
{ This procedure ...
{============================================================================}

constructor TAboutDlg.Init(aParent: PWindowsObject);
var c   : PLStatic;
begin
  inherited Init(aParent, MakeIntResource(DLG_ABOUT));
  c:= New(PLStatic, InitResource(@self, IDC_COPYRIGHT, 0, false));
  c:= New(PLStatic, InitResource(@self, IDC_MSCAN, 0, false));
end;

procedure TAboutDlg.SetUpWindow;
const
  pkWaterReady        = 'IsWaterReady';       {Feature name}
var
  VersionText: array [0..255] of char;
  WaterBlankStatus    : array[0..20] of char; {Feature status}
begin
  Inherited SetUpWindow;
  SendDlgItemMsg(IDC_Version, wm_GetText, 255, LongInt(@VersionText));
  GetPrivateProfileString(appName, pkWaterReady, 'NONE', WaterBlankStatus, sizeof(WaterBlankStatus), profFile);
  If ( StrComp(WaterBlankStatus,'Enabled') = 0 ) Then
    StrCat(VersionText, 'W');
  {$IFNDEF RELEASEVER}
  {- Internal version }
    StrCat(VersionText, ', Internal rev 113');
  {$ENDIF}
  SendDlgItemMsg(IDC_Version, wm_SetText,   0, LongInt(@VersionText));
end;


{============================================================================}
{ This procedure ...
{============================================================================}

procedure TDMSMain.HelpAbout(var msg: TMessage);
begin
  Application^.ExecDialog(New(PAboutDlg, Init(@Self)));
end;



{============================================================================}
{ This procedure ...
{============================================================================}

procedure TDMSMain.Paint(PaintDC: HDC; var PaintInfo: TPaintStruct);
var
  Logo    : HBitMap;
  SaveDC  : HDC;
  BitInfo : TBitMap;
  r       : TRect;
  w       : integer;
  h       : integer;
begin
  inherited Paint(paintDC, paintInfo);
  {$IFDEF RELEASEVER}  {- releasable (real) version }
  Logo:= LoadBitmap(HInstance, MAKEINTRESOURCE(BMP_DADE));
  {$ELSE}  {- for investigational use only }
  Logo:= LoadBitmap(HInstance, MAKEINTRESOURCE(BMP_INV_USE_ONLY));
  {$ENDIF}
  GetWindowRect(hWindow, r);
  w:= r.right - r.left;
  h:= r.bottom - r.top;

  SaveDC:= CreateCompatibleDC(PaintDC);
  SelectObject(SaveDC, Logo);
  GetObject(logo, SizeOf(TBitMap), @bitInfo);
  BitBlt(PaintDC, (w - bitInfo.bmWidth) div 2,(h - bitInfo.bmHeight) div 2,
	BitInfo.bmWidth, bitInfo.bmHeight, SaveDC, 0, 0, SrcCopy);
  DeleteDC(SaveDC);
  DeleteObject(logo);
end;



{============================================================================}
{ This procedure ...
{============================================================================}

procedure TDMSMain.WMWinIni(var msg: TMessage);
var
  temp : array[0..64] of char;
begin
  IntlInitFromWinIni;
  MakeTitle(temp);
  SetWindowText(hwindow, temp);
end;



{============================================================================}
{ This procedure ...
{============================================================================}

procedure TDMApplication.InitMainWindow;
var
  temp: array[0..MaxTitleLen] of char;
begin
  MakeTitle(temp);
  if INIUse3D then
    Register3DApp(true, true, true);
  MainWindow := New(PDMSMain, Init(nil, temp));
end;



{============================================================================}
{ This procedure controls program execution at the highest level.            }
{============================================================================}

procedure Start;
begin
  if OKToRun(MOD_DMSMAIN, false) then
  begin
    DMSApp.Init(GetModuleName(MOD_DMSMAIN));
    DMSApp.Run;
    DMSApp.Done;
  end;
end;

END.
