{----------------------------------------------------------------------------}
{  Module Name  : EngPrint.PAS                                               }
{  Programmer   : RCF                                                        }
{  Date Created : 07/13/95                                                   }
{  Requirements : ..., S3.11 [ref. a], S3.28 [ref. b]                        }
{                                                                            }
{  Purpose - This unit provides a print engine for generating Long Patient   }
{    Reports (aka Custom Reports or Long Format Reports).                    }
{                                                                            }
{    The report has the same information as the Short Format Report [ref b]. }
{    It's used to report results to the physician. It's format is            }
{    customizable.                                                           }
{                                                                            }
{    The isolate database and its associated databases are the source of     }
{    input for the report. This input is reduced by the Multiple Parameter   }
{    Search System (MPSS) (with or withour user defined rules) and local     }
{    rules.                                                                  }
{                                                                            }
{  Assumptions -                                                             }
{                                                                            }
{  Special                                                                   }
{    To maintain the set of unit error numbers, whenever an Assert or        }
{    DoneOnError function is added, search for Assert and DoneOnError and    }
{    renumber them with unique values.                                       }
{                                                                            }
{  Referenced Documents                                                      }
{    a. Software Specification for Nihongo Data Management System v4.00      }
{    b. S3.28 Design Document, Revision 2                                    }
{                                                                            }
{  Revision History - This project is under version control, use it to view  }
{    revision history.                                                       }
{                                                                            }
{----------------------------------------------------------------------------}

unit EngPrint;

INTERFACE

uses
  DBTypes,
  OWindows,
  EngDef;

procedure PrintCustomReport( pParent: PWindowsObject; Format: TSeqNum;
  useMPSS, Preview: boolean );

IMPLEMENTATION

uses
  ApiTools,
  CtlLib,
  DlgLib,
  DBFile,
  DMString,
  DBIDS,
  DBLib,
  DMSErr,
  GridLib,
  Gather,
  INILib,
  IntlLib,
  ListLib,
  MScan,
  ModWin,
  ODialogs,
  Objects,
  PrnObj,
  PrnInfo,
  RptShare,
  Screens,
  Sort1,
  Sort_Par,
  Sort_Dlg,
  SpecMPSS,
  Status,
  Strings,
  WinProcs,
  WinTypes;

{$R ENGPRINT.RES}
{$I ENGPRINT.INC}



                        {----------------------}
                        {  Helper Functions    }
                        {----------------------}



{--------------------------------------------------------------------------}
{ This procedure handles assertions for this unit. If an assertion is
{ invalid, it calls show error with a number and halts. Otherwise, it does
{ nothing.
{
{ Inputs:
{   Valid - result of assertion, true if valid, false if invalid.
{   UnitErrorNumber - a number that uniquely identifies the assertion
{     within this unit.
{--------------------------------------------------------------------------}

procedure Assert( Valid: boolean; UnitErrorNumber: integer );
begin
  if not Valid then
  begin
    ShowError( nil, 0, nil, MOD_ENGPRINT + 1, MOD_ENGPRINT,
      UnitErrorNumber );
    Halt;
  end;
end;



                        {----------------------}
                        {    Abort Dialog      }
                        {----------------------}



type
  {--------------------------------------------------------------------------}
  { This object represents a custom abort dialog box for use with a printer
  { object. Note: the IDs of the controls in this dialog must be the same as
  { those in the generic abort dialog used in PrnObj. This is neccessary
  { becase the PrnObj module sends text to a specific control ID (for page
  { number)
  {--------------------------------------------------------------------------}
  PEngAbortDlg = ^TEngAbortDlg;
  TEngAbortDlg = object( TPctStatDlg )
    { Interface }
    constructor Init( aParent: PWindowsObject; aName, aTitle, aDevice,
      aPort: PChar );

    { Implementation }
    private
    title,
    device,
    port: PChar;
    procedure Cancel( var msg: TMessage ); virtual ID_FIRST + ID_CANCEL;
    procedure SetupWindow; virtual;
  end;



{----------------------------------------------------------------------------}
{ The constructor starts the initialization of the dialog box object. At this
{ point, the window has not been created so any additional initialization
{ is defered to SetupWindow.
{----------------------------------------------------------------------------}

constructor TEngAbortDlg.Init( aParent: PWindowsObject; aName, aTitle,
  aDevice, aPort: PChar );
var
  c: PControl;
begin
  {---- Perform base class initialization }
  inherited Init( aParent, aName, true, IDC_PERCENT );

  {---- Create objects for controls needing special handling }
  c := New( PLStatic, InitResource( @self, IDC_DEVICE, 0, false ) );
  c := New( PLStatic, InitResource( @self, IDC_PORT, 0, false ) );

  {---- Store attributes }
  title := aTitle;              { note: it might be safer to copy the title }
  device := aDevice;            { note: it might be safer to copy }
  port := aPort;                { note: it might be safer to copy }

  { Note on above: just format the control labels here and toss the
    parameters. }
end;



{----------------------------------------------------------------------------}
{ This procedure is called after the window has been created so that it may
{ be set up. This override replaces the default text in some of the controls
{ and resets the percentage to zero.
{----------------------------------------------------------------------------}

procedure TEngAbortDlg.SetupWindow;
var
  pstr: array[ 0..200 ] of char;
begin
  {---- Base class set up }
  inherited SetupWindow;

  {---- Replace text in applicable controls }
  SetDlgItemText( HWindow, IDC_TITLE, title );
  StrCopy( pstr, 'Printer - ' );
  StrCat( pstr, device );
  SetDlgItemText( HWindow, IDC_DEVICE, pstr );
  StrCopy( pstr, 'Port - ' );
  StrCat( pstr, port );
  SetDlgItemText( HWindow, IDC_PORT, pstr );

  {---- Reset percentage }
  CompPctLevel( 0, 0 );
end;



{----------------------------------------------------------------------------}
{ This command handler is called when the Cancel button is selected. This
{ override adds a confirmation step to the end of the base class processing
{ of the command.
{----------------------------------------------------------------------------}

procedure TEngAbortDlg.Cancel( var msg: TMessage );
var
  p1, p2: array[ 0..100 ] of char;
begin
  inherited Cancel( msg );
  if YesNoMsg( HWindow, SR( IDS_CONFIRM, p1, sizeof( p1 ) - 1 ),
    SR( IDS_CONFIRMCANCEL, p2, sizeof( p2 ) - 1 ) ) then
    SetUserAbort( true ); {- tell print module that user has aborted }
end;



                        {----------------------}
                        {    Print Engine      }
                        {----------------------}



type
  {--------------------------------------------------------------------------}
  { This object provides a print engine for generating long patient reports.
  { This functionality could have been included in the printout object but
  { object windows doen not provide a way to preview a printout. So, this
  { object was created to seperate report generation from the printout so
  { that a preview window could be created.
  {--------------------------------------------------------------------------}
  PPrintEngine = ^TPrintEngine;
  TPrintEngine = object( TObject )

    { Interface }
    Complete: boolean;  {- READ ONLY, true if print engine is complete }
    ReportDef: PRptDef;
    constructor Init( aParent: PWindowsObject; Format: TSeqNum; aUseMPSS,
      allowYield: boolean; SortParameters: SortEngineParameters );
    destructor Done; virtual;
    procedure PaintPage( dc: HDC; minMargins: TRect );
    function NextPage: boolean;
    procedure AssignAndSetupAbortDialog( pDialog: PEngAbortDlg );

    { Implementation }
    private
    pSorter: PSpecimenSortEngine;
    doYield: boolean;
    pWC: PWaitCursor;
    abortDlg: PEngAbortDlg; { copy of pointer to an abort dialog box }
    pSpecs: array[ 1..MaxRptsPerPage ] of PGatherSpec;
    numLoaded: integer; { number of specimens currently loaded into gather
      cache }
    curSpec: integer; { current specimen that has been passed to Section
      engines }
    mpssObj: PSpecMPSS;
    dbSpec: PDBFile;
    parent: PWindowsObject;
    listObj: PListObject;
    listRec: PMnemListRec;
    isEOF: boolean;         { set to true when at the end of the isolate file}
    procedure LoadSpecimens;
  end;



{----------------------------------------------------------------------------}
{ The constructor initializes a print object and attempts to find the first
{ record to print. If unsuccessful or no data exists, then this object will
{ fail. If successful, the print engine is prepared to print the first page
{ of the report. Successive pages can be loaded with the NextPage method.
{----------------------------------------------------------------------------}

constructor TPrintEngine.Init( aParent: PWindowsObject; Format: TSeqNum;
  aUseMPSS, allowYield: boolean; SortParameters: SortEngineParameters );

  {--------------------------------------------------------------------------}
  { This function shows an error and calls the destructor if the test value
  { is true. It passes the test value through to facilitate calling Fail.
  {--------------------------------------------------------------------------}
  function DoneOnError( Error: boolean; MessageID: Word; ErrorNumber: longint;
    UnitErrorNumber: integer ): boolean;
  begin
    if Error then
    begin
      ShowError( aParent, MessageID, nil, ErrorNumber, MOD_ENGPRINT,
        UnitErrorNumber );
      Done;
    end;
    DoneOnError := Error;
  end;

  {--------------------------------------------------------------------------}
  { This procedure sets the data of a given section to the current spec.
  {--------------------------------------------------------------------------}
  procedure SetData( sect: PSectEngineBase ); far;
  begin
    sect^.SetPrintData( pSpecs[ curSpec ] );
  end;

var
  p1,
  p2: array[ 0..200 ] of char;
  k: integer;
  SeqNum: TSeqNum;
  GotOne: boolean;

begin { Init }

  { Perform base class initialization }
  inherited Init;

  { Set pointers for safe dispose in case construction fails }
  pSorter := nil;
  pWC := nil;
  abortDlg := nil;
  ReportDef := nil;
  dbSpec := nil;
  listRec := nil;
  listObj := nil;
  for k := 1 to MaxRptsPerPage do
    pSpecs[ k ] := nil;

  { Bracket construction in a wait cursor }
  pWC := New( PWaitCursor, Init );

  { Set attributes }
  doYield := allowYield;
  parent := aParent;
  Complete := false;
  curSpec := 0;
  numLoaded := 0;
  isEOF := false;

  { Open the report definition and load it }
  ReportDef := New( PRptDef, Init( Format ) );
  if DoneOnError( ReportDef = nil, IDS_CANTLOADRPTDEF, engLastDefInitError, 100 )
    then
    Fail;

  { Construct other objects }
  listObj := New( PListObject, Init );
  if DoneOnError( listObj = nil, IDS_CANTINITLISTOBJ, 0, 101 ) then
    Fail;
  listRec := New( PMnemListRec, Init );
  if DoneOnError( listRec = nil, IDS_CANTINITLISTREC, 0, 102 ) then
    Fail;
  dbSpec := New( PDBFile, Init( DBSpecFile, '', dbOpenNormal ) );
  if DoneOnError( dbSpec = nil, IDS_CANTOPENSPEC, dbLastOpenError, 103 ) then
    Fail;
  mpssObj := New( PSpecMPSS, Init( parent, dbSpec ) );
  if DoneOnError( mpssObj = nil, IDS_CANTINITMPSS, 0, 104 ) then
    Fail;

  { Create cache of gather objects }
  for k := 1 to ReportDef^.pageDef.rptsPerPage do
  begin
    pSpecs[ k ] := New( PGatherSpec,
                  Init( listObj, listRec, true, doYield, false, ScreenOn(PrintXtrasKey)  ) );
    if DoneOnError( pSpecs[ k ] = nil, IDS_CANTINITGATHER, 0, 105  ) then
      Fail;
  end;

  { Handle MPSS option }
  if aUseMPSS then
  begin
    if not mpssObj^.LoadRules( parent, ReportDef^.rptName ) then
    begin
      if not YesNoMsg( parent^.HWindow, SR( IDS_CONFIRM, p1, sizeof( p1 ) - 1 ),
        SR( IDS_NOMPSSSEL, p2, sizeof( p2 ) - 1 ) ) then
      begin
        Done;
        fail;
      end;
    end;
  end;

  { At this point, everything is initialized. Now try to find the first
    specimen. }
  if INISpecimenListSort = 1 then
    dbSpec^.dbc^.SetCurKeyNum( DBSPEC_ID_KEY )
  else
    dbSpec^.dbc^.SetCurKeyNum( DBSPEC_Date_KEY );
  if DoneOnError( not mpssObj^.GetFirstSpecimen, IDS_NODATA, 0, 106 ) then
    Fail;

  { Assert: GetFirstSpecimen returned true }


  { Get first record, sorting first if enabled. }
  if SortParameters.NumberOfLevels > 0 then
  begin
    pSorter := New( PSpecimenSortEngine, Init( SortParameters ) );
    Assert( pSorter <> nil, 99 );
  end;
  if pSorter <> nil then
  begin
    repeat
      {use sequence number as sort index }
      SeqNum := dbSpec^.dbr^.GetSeqValue;
      Assert( SeqNum <> -1, 99 );

      {input sort element}
      pSorter^.Input( SeqNum );

    until not mpssObj^.GetNextSpecimen;
    pSorter^.Sort;
    GotOne := pSorter^.Output( SeqNum );
    Assert( GotOne, 99 );
    dbSpec^.dbc^.GetSeq( dbSpec^.dbr, SeqNum );
  end;


  { Assert: the isolate file is pointing to the first valid isolate. }

  { Fill the gather cache with specimen data }
  LoadSpecimens;
  curSpec := 1;
  ReportDef^.sects^.ForEach( @SetData );

  { Remove the wait cursor }
  MSDisposeObj( pWC );
end;



{----------------------------------------------------------------------------}
{ The destructor performes cleanup
{----------------------------------------------------------------------------}

destructor TPrintEngine.Done;
var
  k: integer;
begin

  { Comment: normally, the base class is not destroyed first. It this Ok? }

  inherited Done;

  for k := 1 to ReportDef^.pageDef.rptsPerPage do
    MSDisposeObj( pSpecs[ k ] );
  MSDisposeObj( ReportDef );
  MSDisposeObj( dbSpec );
  MSDisposeObj( mpssObj );
  MSDisposeObj( listObj );
  MSDisposeObj( listRec );
  MSDisposeObj( pWC );
end;



{----------------------------------------------------------------------------}
{ This procedure associates an externally allocated abort dialog box with
{ this engine. The box must be newly created, not used elsewhere, and not
{ destroyed until after this engine is destroyed.
{----------------------------------------------------------------------------}

procedure TPrintEngine.AssignAndSetupAbortDialog( pDialog: PEngAbortDlg );
begin
  abortDlg := pDialog;
  abortDlg^.CompPctLevel( mpssObj^.GetCurrentSpec, mpssObj^.GetTotalSpecs );
end;



{----------------------------------------------------------------------------}
{ This procedure loads (i.e., gathers) the data for up to MaxRptsPerPage or
{ specimens data. Assumes dbIso is sitting on a valid isolate record. Also,
{ only fills the gather cache from curSpec + 1 to maxRptsPerPage.
{----------------------------------------------------------------------------}

procedure TPrintEngine.LoadSpecimens;

  {--------------------------------------------------------------------------}
  { This function gets the next specimen in the sorted series. It returns
  { false if it could not get a specimen, indicating the end of the series.
  {--------------------------------------------------------------------------}
  function GetSortedNext: boolean;
  var
    GotOne: boolean;
    SeqNum: TSeqNum;
  begin
    GotOne := pSorter^.Output( SeqNum );
    if GotOne then
      dbSpec^.dbc^.GetSeq( dbSpec^.dbr, SeqNum );
    GetSortedNext := GotOne;
  end;

var
  k: integer;
  seq: TSeqNum;
  lastGathSpec: TSeqNum;  { seq of last gathered specimen }
begin
  {- remove any old loaded specimens from the gather cache }
  for k := numLoaded + 1 to ReportDef^.pageDef.rptsPerPage do
    pSpecs[ k ]^.Clear;

  {- if not already at the end of the file }
  if not isEOF then
  begin
    while ( numLoaded < ReportDef^.pageDef.rptsPerPage ) and not isEOF do
    begin
      lastGathSpec := dbSpec^.dbr^.GetSeqValue;
      k := pSpecs[ numLoaded + 1 ]^.LoadRecord( lastGathSpec );
      if k <> 0 then
        ShowError( parent, IDS_LOADERR, nil, k, MOD_ENGPRINT, 0 )
      else
      begin
        Inc( numLoaded );
        if pSorter = nil then
          isEOF := not mpssObj^.GetNextSpecimen
        else
          isEOF := not GetSortedNext;
      end;
      if abortDlg <> nil then
        abortDlg^.CompPctLevel( mpssObj^.GetCurrentSpec,
          mpssObj^.GetTotalSpecs );
    end;
  end
  else if abortDlg <> nil then
    abortDlg^.CompPctLevel( 1, 1 ); {- force 100% }
end;



{----------------------------------------------------------------------------}
{ This procedure paints a page to the device context. It assumes that the
{ sections have had their positions pushed using the first gathered specimen
{ in the gather cache (pSpecs[ 1 ]).
{----------------------------------------------------------------------------}

procedure TPrintEngine.PaintPage( dc: HDC; minMargins: TRect );
var
  curMarg: TRect;
  count,
  rptWidth,
  LTpPX,
  LTpPY,
  halfSpace: integer;

oldPen: HPen;
k,x: integer;

  {--------------------------------------------------------------------------}
  {--------------------------------------------------------------------------}
  procedure PrintIt( sect: PSectEngineBase ); far;
  begin
    sect^.PrintData( dc, curMarg );
  end;

  {--------------------------------------------------------------------------}
  { This function returns true if the section is not done printing its data.
  {--------------------------------------------------------------------------}
  function IsntDone( sect: PSectEngineBase ): boolean; far;
  begin
    IsntDone := not sect^.IsPrintComplete;
  end;

  {--------------------------------------------------------------------------}
  {--------------------------------------------------------------------------}
  procedure SetData( sect: PSectEngineBase ); far;
  begin
    sect^.SetPrintData( pSpecs[ curSpec ] );
  end;

  {--------------------------------------------------------------------------}
  {--------------------------------------------------------------------------}
  procedure RestorePosition( sect: PSectEngineBase ); far;
  begin
    sect^.PopPosition;
  end;

  {--------------------------------------------------------------------------}
  {--------------------------------------------------------------------------}
  procedure SavePosition( sect: PSectEngineBase ); far;
  begin
    sect^.PushPosition;
  end;

  {--------------------------------------------------------------------------}
  {--------------------------------------------------------------------------}
  procedure IncMargins;
  begin
    Inc( curMarg.left, rptWidth );
    if count < ReportDef^.pageDef.rptsPerPage then
      Inc( curMarg.left, ReportDef^.pageDef.spacing );
    curMarg.right := curMarg.left + rptWidth + LTpPX;
  end;

begin { PaintPage }

  LTwipsPerPixel( dc, LTpPX, LTpPY );
  count := 1;

  {- calculate width of an individual report on the paper }
  with ReportDef^.pageDef do
  begin
    rptWidth := ( size.x - margin.left - margin.right -
      ( ( rptsPerPage - 1 ) * spacing ) ) div rptsPerPage;
    halfSpace := spacing div 2;
  end;

  curMarg := ReportDef^.pageDef.margin;

  {- adjust the margins by the minimum margins to get accurate margin distance
     on DC }
  Dec( curMarg.top, minMargins.top );
  Dec( curMarg.left, minMargins.left );
  Dec( curMarg.right, minMargins.right );
  Dec( curMarg.bottom, minMargins.bottom );

  curMarg.right := curMarg.left + rptWidth;

  curSpec := 1;
  ReportDef^.sects^.ForEach( @SetData );
  ReportDef^.sects^.ForEach( @RestorePosition );
  ReportDef^.sects^.ForEach( @SavePosition );

  {Need to figure out why section positions are not being saved properly!}
  repeat
    {- tell the section to print for the current specimen }
    ReportDef^.sects^.ForEach( @PrintIt );

    {- see if all sections completed the printing of their data}
    if ReportDef^.sects^.FirstThat( @IsntDone ) = nil then
    begin {- all sects are complete }
      Inc( curSpec );
      if curSpec <= numLoaded then
        ReportDef^.sects^.ForEach( @SetData );
    end;
    IncMargins;
    Inc( count );
  until ( count > ReportDef^.pageDef.rptsPerPage ) or ( curSpec > numLoaded );

  {- at this point, the page is painted and curSpec points the the next gathered
     specimen to be printed from the cache (or if it is greater than numLoaded
     then all gathered specimens in the cache have been printed.
     If curSpec <= numLoaded then more specimens in the cache need to be printed.
     The position of the sections for the gathered specimen (curSpec) is also
     pushed into the sections stack. This information can be used by NextPage
     to pack and refill the gather cache }
end;



{----------------------------------------------------------------------------}
{ this function loads the engine with the data for the next page. It returns
{ true, when there is still data pending
{----------------------------------------------------------------------------}

function TPrintEngine.NextPage: boolean;

  procedure SavePosition( sect: PSectEngineBase ); far;
  begin
    sect^.PushPosition;
  end;

  procedure ClearStack( sect: PSectEngineBase ); far;
  begin
    sect^.ClearPosStack;
  end;

  procedure PackCache;
  var
    k: integer;
    pHoldSpec: PGatherSpec;
  begin
    {- first save the position of the unfinished specimen  }
    ReportDef^.sects^.ForEach( @SavePosition );

    for k := curSpec to numLoaded do
    begin
      pHoldSpec := pSpecs[ k - curSpec + 1 ];
      pSpecs[ k - curSpec + 1 ] := pSpecs[ k ];
      pSpecs[ k ] := pHoldSpec;
    end;
    {- adjust the size of the cache (accounting for already printed
       specimens) }
    numLoaded := numLoaded - curSpec + 1;
  end;

begin { NextPage }

  pWC := nil;
  if not doYield then
    pWC := New( PWaitCursor, Init );
  if not isEOF then
  begin
    {- first, pack the gather cache moving unprinted specimen to the beginning
      of the cache }
    if curSpec <= numLoaded then
    begin
      PackCache;
    end
    else
    begin
      {- all specimens in the cache have been printed }
      ReportDef^.sects^.ForEach( @ClearStack );
      numLoaded := 0;
      curSpec := 0;
    end;

    LoadSpecimens;

    if numLoaded > 0 then
      curSpec := 1
    else
      curSpec := 0;
    NextPage := not isEOF or ( numLoaded > 0 );
  end
  else if curSpec <= numLoaded then
  begin
    PackCache;
    NextPage := true;
    curSpec := 1;
  end
  else
    NextPage := ( false );
  MSDisposeObj( pWC );
end;



                        {----------------------}
                        {    Preview Window    }
                        {----------------------}


type
  {--------------------------------------------------------------------------}
  { This object represents a Print Engine Preview window.
  {--------------------------------------------------------------------------}
  PPreviewWindow = ^TPreviewWindow;
  TPreviewWindow = object( TModalWin )

    { Interface }
    pEngine: PPrintEngine;
    zoom: real;
    ppiX: integer;
    ppiY: integer;
    page: integer;
    working: integer;
    constructor Init( aParent: PWindowsObject; input_pEngine: PPrintEngine;
      input_MustDisposeEngine: boolean );
    destructor Done; virtual;
    procedure SetupWindow; virtual;
    procedure ShowPage;
    procedure SetupScroller;
    procedure WMPaint( var msg: TMessage ); virtual WM_FIRST + WM_PAINT;
    procedure WMKeyDown( var msg: TMessage ); virtual WM_FIRST + WM_KEYDOWN;
    function GetClassName: PChar; virtual;
    procedure GetWindowClass( var AWndClass: TWndClass ); virtual;
    procedure CMCloseUp( var msg: TMessage ); virtual CM_FIRST + CM_CLOSE;
    procedure CMZoom( var msg: TMessage ); virtual CM_FIRST + CM_ZOOM;
    procedure CMNext( var msg: TMessage ); virtual CM_FIRST + CM_NEXT;

    { Implementation }
    private
    MustDisposeEngine: boolean;
  end;



{----------------------------------------------------------------------------}
{ The constructor creates a report preview window.
{
{ Inputs
{   aParent - a pointer to the parent window
{   inpit_pEngine - a pointer to the print engine
{   input_MustDestroyEngine - If true, this window will dispose the engine
{     when done, otherwise, the engine will be left alone.
{----------------------------------------------------------------------------}

constructor TPreviewWindow.Init( aParent: PWindowsObject;
  input_pEngine: PPrintEngine; input_MustDisposeEngine: boolean );
begin
  inherited Init( aParent, input_pEngine^.ReportDef^.rptname, aParent^.HWindow );
  working := 0;
  attr.menu := LoadMenu( HInstance, MakeIntResource( MNU_PREVIEW ) );
  attr.style := attr.style or WS_HSCROLL or WS_VSCROLL;
  attr.x := CW_USEDEFAULT;
  attr.y := CW_USEDEFAULT;
  attr.w := GetSystemMetrics( SM_CXFULLSCREEN );
  attr.h := GetSystemMetrics( SM_CYFULLSCREEN );
  pEngine := input_pEngine;
  MustDisposeEngine := input_MustDisposeEngine;
  scroller := New( PScroller, Init( @self, 0,0, 0,0 ) );
  scroller^.autoMode := False;
  scroller^.trackMode := False;
  zoom := 1.0;
  page := 1;
end;



{----------------------------------------------------------------------------}
{ The destructor performes cleanup. It also disposes of the engine if such
{ action was requested.
{----------------------------------------------------------------------------}

destructor TPreviewWindow.Done;
begin
  inherited Done;
  if MustDisposeEngine then
    MSDisposeObj( pEngine );
end;



{----------------------------------------------------------------------------}
{ This override ...
{----------------------------------------------------------------------------}

procedure TPreviewWindow.SetupWindow;
var
  dc: HDC;
begin
  inherited SetupWindow;

  dc := GetDC( HWindow );
  ppiX := GetDeviceCaps( dc, LOGPIXELSX );
  ppiY := GetDeviceCaps( dc, LOGPIXELSY );
  ReleaseDC( HWindow, dc );

  SetupScroller;
  ShowPage;
end;



{----------------------------------------------------------------------------}
{ This procedure ...
{----------------------------------------------------------------------------}

procedure TPreviewWindow.ShowPage;
var
  pstr: array[ 0..200 ] of char;
  p2: array[ 0..20 ] of char;
begin
  StrCopy( pstr, pEngine^.ReportDef^.rptName );
  StrCat( pstr, ' --- ' );
  StrCat( pstr, SR( dmstring.IDS_PAGE, p2, sizeof( p2 )-1 ) );
  StrCat( pstr, ' ' );
  IntlIntToStr( page, p2, sizeof( p2 )-1 );
  StrCat( pstr, p2 );
  SetCaption( pstr );
end;



{----------------------------------------------------------------------------}
{ This procedure sets up a scroller in 'device' coordinates since this is
{ how it likes to work. Also, take into account the zoom factor. The scroll
{ units are 0.25 inches, which is (1440 / 4) = 360 LTwips. To convert to
{ device coordinates; there are 1440 LTwips/inch and GetDeviceCaps( dc,
{ LOGPIXELS) pixels/inch - dividing we get LTwips/Pixel, which gives us the
{ conversion factor to convert LTwips to device pixels.
{----------------------------------------------------------------------------}

procedure TPreviewWindow.SetupScroller;
var
  LTpPX: Integer;  {- LTwips per Pixel (x direction) }
  LTpPY: Integer;  {- LTwips per Pixel (y direction) }
  AUnit: Integer;
  LTwidth: Integer;  {- LTwips width }
  LTHeight: Integer; {- LTwips height }
begin
  LTpPX := 1440 div ppiX; { Logical Twips per inch / Physical pixels per inch}
  LTpPY := 1440 div ppiY;

  AUnit := 1440;  {- scroll 1 inch at a time }
  {- now AUnit = scroll unit in LTwips, apply zoom factor to get actual scroll
     unit in LTwips }
  AUnit := Trunc( AUnit * zoom );

  {- tell scroller units ( converting to device pixels) }
  scroller^.SetUnits( AUnit div LTpPX, AUnit div LTpPY );

  {- caclulate total page height in LTwips }
  with pEngine^.ReportDef^.pageDef do
  begin
    LTWidth := Trunc( size.X * zoom );
    LTHeight := Trunc( size.Y * zoom );
  end;

  scroller^.SetRange( LTWidth div AUnit, LTHeight div AUnit );
end;



{----------------------------------------------------------------------------}
{ This command handler override sets Logical Twips mapping mode in the device
{ context.
{----------------------------------------------------------------------------}

procedure TPreviewWindow.WMPaint( var msg: TMessage );
var
  dc: HDC;
  ps: TPaintStruct;
  LTpPX: integer;  {- LTwips per Pixel (x direction) }
  LTpPY: integer;  {- LTwips per Pixel (y direction) }
  oldPen: HPen;
  rect: TRect;
begin
  dc := BeginPaint( HWindow, ps );

  SetlogicalTwips( dc, zoom );

  if scroller <> nil then
    scroller^.BeginView( dc, ps );

  LTwipsPerPixel( dc, LTpPX, LTpPY );
  {- draw a rectangle indicating the paper }
  with pEngine^.ReportDef^.pageDef do
  begin
    Rectangle( dc, 0,0, size.x + LTpPX, size.y + LTpPY );

    oldPen := SelectObject( dc, CreatePen( PS_DASH, 0,
      RGB( 192, 192, 192 ) ) );
    Rectangle( dc, margin.left, margin.top, size.x - margin.right,
      size.y - margin.bottom );
    DeleteObject( SelectObject( dc, oldPen ) );
  end;

  SetRectEmpty( rect ); {- the screen has zero minimum margins }
  pEngine^.PaintPage( dc, rect );

  if Scroller <> nil then
    scroller^.EndView;

  EndPaint( HWindow, ps );
end;



{----------------------------------------------------------------------------}
{ This procedure handles key-down messages for the window. It provides
{ user controls for scrolling and next page.
{----------------------------------------------------------------------------}

procedure TPreviewWindow.WMKeyDown( var msg: TMessage );

  {--------------------------------------------------------------------------}
  { This function reads the state of the control keys and depending on the
  { state, it returns either the scroller's y-page or y-line amount.
  {--------------------------------------------------------------------------}
  function YPageOrLineAmount: integer;
  begin
    if GetKeyState( VK_CONTROL ) and $8000 > 0 then
      YPageOrLineAmount := scroller^.YPage
    else
      YPageOrLineAmount := scroller^.YLine;
  end;

  {--------------------------------------------------------------------------}
  { This function reads the state of the control keys and depending on the
  { state, it returns either the scroller's x-page or x-line amount.
  {--------------------------------------------------------------------------}
  function XPageOrLineAmount: integer;
  begin
    if GetKeyState( VK_CONTROL ) and $8000 > 0 then
      XPageOrLineAmount := scroller^.XPage
    else
      XPageOrLineAmount := scroller^.XLine;
  end;

begin { WMKeyDown }

  msg.result := 0;
  case msg.wParam of

    VK_NEXT:
      CMNext( msg );

    VK_DOWN:
      scroller^.ScrollBy( 0, YPageOrLineAmount );

    VK_UP:
      scroller^.ScrollBy( 0, -YPageOrLineAmount );

    VK_LEFT:
      scroller^.ScrollBy( -XPageOrLineAmount, 0 );

    VK_RIGHT:
      scroller^.ScrollBy( XPageOrLineAmount, 0 );

    else
      DefWndProc( msg );
  end;

end;



{----------------------------------------------------------------------------}
{ This function overrides the default class name so that ...
{----------------------------------------------------------------------------}

function TPreviewWindow.GetClassName: PChar;
begin
  GetClassName := 'RCFRptPreview';
end;



{----------------------------------------------------------------------------}
{ This override modifies the background brush so that ...
{----------------------------------------------------------------------------}

procedure TPreviewWindow.GetWindowClass( var AWndClass: TWndClass );
begin
  inherited GetWindowClass( AWndClass );
  aWndClass.hbrBackground := GetStockObject( LTGRAY_BRUSH );
end;



{----------------------------------------------------------------------------}
{ This procedure handles a close command.
{----------------------------------------------------------------------------}

procedure TPreviewWindow.CMCloseUp( var msg: TMessage );
begin
  CloseWindow;
end;



{----------------------------------------------------------------------------}
{ This procedure handles a zoom command.
{----------------------------------------------------------------------------}

procedure TPreviewWindow.CMZoom( var msg: TMessage );
var
  dlg: PZoomDlg;
begin
  dlg := New( PZoomDlg, Init( @self, MakeIntResource( DLG_ZOOM ), IDC_LIST,
    zoom ) );
  if application^.ExecDialog( dlg ) = IDOK then
  begin
    SetupScroller;
    InvalidateRect( HWindow, nil, true );
  end;
end;



{----------------------------------------------------------------------------}
{ This procedure handles a next page command.
{----------------------------------------------------------------------------}

procedure TPreviewWindow.CMNext( var msg: TMessage );
var
  p1,
  p2: array[ 0..100 ] of char;
begin
  if working > 0 then
    exit;
  Inc( working );
  if not pEngine^.NextPage then
    ErrorMsg( hWindow, SR( IDS_ERROR, p1, sizeof( p1 )-1 ),
      SR( IDS_NOMOREDATA, p2, sizeof( p2 )-1 ) )
  else
  begin
    Inc( page );
    ShowPage;
  end;
  InvalidateRect( HWindow, nil, true );
  HandleEvents( hWindow );
  Dec( working );
end;



                        {----------------------}
                        {   Custom Printer     }
                        {----------------------}



type
  {--------------------------------------------------------------------------}
  { This object represents the printer. It is used as a target for printout
  { objects. This derived class replaces the default printer abort dialog box
  { with a customized one.
  {--------------------------------------------------------------------------}
  PCustomPrinter = ^TCustomPrinter;
  TCustomPrinter = object( TPrinter )
    AbortDialog: PEngAbortDlg; {- pointer to the abort dialog box }
    function InitAbortDialog( pParent: PWindowsObject; title: PChar ): PDialog;
      virtual;
  end;



{----------------------------------------------------------------------------}
{ This override creates a customized abort dialog box for display during
{ printing.
{----------------------------------------------------------------------------}

function TCustomPrinter.InitAbortDialog( pParent: PWindowsObject; Title: PChar ):
  PDialog;
begin
  AbortDialog := New( PEngAbortDlg, Init( pParent,
    MakeIntResource( DLG_ABORTWIN ), Title, Device, Port ) );
  InitAbortDialog := AbortDialog;
end;



                        {----------------------}
                        {    Report Printout   }
                        {----------------------}



type
  {--------------------------------------------------------------------------}
  { This object represents a printout of the report. Once constructed, it can
  { be printed by passing it to a printer object's Print method.
  {--------------------------------------------------------------------------}
  PReportPrintout = ^TReportPrintout;
  TReportPrintout = object( TPrintout )
    pEngine: PPrintEngine;
    isDone: boolean;
    MinimumMargins: TRect;
    pPrinter: PCustomPrinter;  {- printer object used to print the printout }
    constructor Init( input_pEngine: PPrintEngine;
      input_pPrinter: PCustomPrinter );
    function HasNextPage( page: word ): boolean; virtual;
    procedure PrintPage( page: word; var rect: TRect; flags: word ); virtual;
    function GetDialogInfo( var pages: integer ): boolean; virtual;
    procedure SetPrintParams( aDC: HDC; aSize: TPoint ); virtual;
    procedure BeginPrinting; virtual;
  end;



{----------------------------------------------------------------------------}
{ The constructor initializes the printout for use with a specified printer
{ and engine.
{----------------------------------------------------------------------------}

constructor TReportPrintout.Init( input_pEngine: PPrintEngine;
  input_pPrinter: PCustomPrinter );
begin
  inherited Init( input_pEngine^.ReportDef^.rptName );
  pPrinter := input_pPrinter;
  pEngine := input_pEngine;
  isDone := false;
end;



{----------------------------------------------------------------------------}
{ This override replaces the base class functionality. It returns false to
{ disables page selection. The number of pages cannot be easilly determined,
{ so Pages is set to 0 so that printing will continue until HasNextPage
{ returns false.
{----------------------------------------------------------------------------}

function TReportPrintout.GetDialogInfo( var pages: integer ): boolean;
begin
  pages := 0;
  GetDialogInfo := false;
end;



{----------------------------------------------------------------------------}
{ This procedure gives the print engine control of the printer's abort
{ dialog box.
{----------------------------------------------------------------------------}

procedure TReportPrintout.BeginPrinting;
begin
  pEngine^.AssignAndSetupAbortDialog( pPrinter^.AbortDialog );
end;



{----------------------------------------------------------------------------}
{ This override is required because GetDialogInfo passed the responsibility
{ for determining the number of pages to this function. This function
{ determines if there is another page to print by asking the print engine.
{----------------------------------------------------------------------------}

function TReportPrintout.HasNextPage( page: word ): boolean;
begin
  HasNextPage := pEngine^.NextPage;
end;



{----------------------------------------------------------------------------}
{ This override is required to print the current page. It transfers the
{ responsibility to the print engine.
{----------------------------------------------------------------------------}

procedure TReportPrintout.PrintPage( page: word; var rect: TRect;
  flags: word );
begin
  SetLogicalTwips( DC, 1.0 );
  pEngine^.PaintPage( DC, MinimumMargins );
end;



{----------------------------------------------------------------------------}
{ This override applies the printer's minimum margins to the printout.
{----------------------------------------------------------------------------}

procedure TReportPrintout.SetPrintParams( aDC: HDC; aSize: TPoint );
var
  ok: boolean;
begin
  {---- Calling the base class is required }
  inherited SetPrintParams( aDC, aSize );

  {---- Get the printers minimum margins for use with the PrintPage method. }
  ok := PrintMargins( pPrinter, MinimumMargins );
  Assert( ok, 1 );
end;



                        {----------------------}
                        {  EXPORT function     }
                        {----------------------}



{----------------------------------------------------------------------------}
{ This procedure completes the processing of a "print long patient reports"
{ command. It is called after a format and other options have been selected.
{ The outcome of this procedure depends on user interactions and the content
{ of the database.
{
{ Input
{   aParent - pointer to parent window
{   aRptSeq - sequence number of the report format record inthe xxx data file.
{   useMPSS - true if the user would like to use the MPSS
{   preview - true for a preview window; false for a hard copy.
{----------------------------------------------------------------------------}

procedure PrintCustomReport( pParent: PWindowsObject; Format: TSeqNum;
  useMPSS, Preview: boolean );
const
  DISPOSE_ENGINE = true;
var
  pPreview: PPreviewWindow;
  pReport: PReportPrintout;
  pEngine: PPrintEngine;
  pPrinter: PCustomPrinter;
  hardCopy: boolean;
  SortParameters: SortEngineParameters;
  pKeyInformation: PGroup1KeyInformation;
  Action: integer;
  Title: array[ 0..63 ] of char;
begin

  { Get report title }
  SR( IDS_LPR_TITLE, Title, SizeOf( Title ) - 1 );

  { Load default sort parameters and permit user to view/edit }
  GetGroup1SortParameters( SortParameters );
  pKeyInformation := New( PGroup1KeyInformation, Init );
  Action := SortParametersDialog( pParent, Title, pKeyInformation,
    false, SortParameters );

  { Continue? }
  if Action = id_Next then
  begin
    {---- Construct engine }
    hardCopy := not Preview;
    pEngine := New( PPrintEngine,
      Init( pParent, Format, useMPSS, hardCopy, SortParameters ) );
    if pEngine = nil then
    begin
      EnableWindow( pParent^.hWindow, true );
      SetFocus( pParent^.hWindow );
      Exit;
    end;

    {---- Update sorting options }
    SetGroup1SortParameters( SortParameters );

    {---- Use engine to generate preview or hard copy }
    if Preview then
    begin
      pPreview := New( PPreviewWindow,
        Init( pParent, pEngine, DISPOSE_ENGINE ) );
      EnableWindow( pParent^.hWindow, true );
      { Note: MakeWindow returns before the window closes, thus it (not this
        procedure) is responsible for disposing the window, which in turn
        will dispose the engine. Also note that any instructions at the bottom
        of this procedure will execute before the window is closed, moreover,
        as control returns to the main, the user may attempt to do other
        things such as reenter this procedure. A modal preview would have been
        safer. }
      application^.MakeWindow( pPreview );
    end
    else
    begin
      pPrinter := New( PCustomPrinter, Init );
      pReport := New( PReportPrintout, Init( pEngine, pPrinter ) );
      pPrinter^.Print( pParent, pReport );
      MSDisposeObj( pReport );
      MSDisposeObj( pPrinter );
      EnableWindow( pParent^.hWindow, true );
      MSDisposeObj( pEngine );
    end;

    {---- ? }
    SetFocus( pParent^.hWindow );
  end;
end;

END.