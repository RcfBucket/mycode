unit Interps;
{-----------------------------------------------------------------------}
{  A TherLib Module.  Contains TherInit, TherRecRead, and TherClose.    }
{                                                                       }
{  REVISION HISTORY STARTS SUDDENLY ON February 7, 1987.                }
{  7-Feb-87                                                             }
{  Changed the TherpXXXX routines to return values even when            }
{  compiler suppression is detected, because MKINTRP uses the           }
{  values.  (CURT)                                                      }
{  17-Feb-87                                                            }
{  Broke DRUGS into four modules: TherDrug, CalcTher, TherpIni,         }
{  and TherCore.  (Curt)                                                }
{  07-Dec-87                                                            }
{  Broke DrugLib into two libraries:  DrugLib, an interface to the      }
{  dosages.dat file, and TherLib, an interface to the therapy.dat       }
{  file.  Because of major differences in the new file formats,         }
{  the code went though many changes.  (Bob)                            }
{-----------------------------------------------------------------------}
{  A TherLib module.                                                    }
{                                                                       }
{  REVISION HISTORY STARTS SUDDENLY ON February 7, 1987.                }
{  7-Feb-87                                                             }
{  Changed the TherpXXXX routines to return values even when            }
{  compiler suppression is detected, because MKINTRP uses the           }
{  values.  (CURT)                                                      }
{  17-Feb-87                                                            }
{  Broke DRUGS into four modules: TherDrug, CalcTher, TherpIni,         }
{       and TherCore.  (CURT)                                           }
{  22-Mar-87                                                            }
{       Added support for GUIDE.SRC, the therapy guide debugging        }
{  program.  (CURT)                                                     }
{  07-Dec-87                                                            }
{  Broke DrugLib into two libraries:  DrugLib, an interface to the      }
{  dosages.dat file, and TherLib, an interface to the therapy.dat       }
{  file.  Because of major differences in the new file formats,         }
{  the code went though many changes.  (Bob)                            }
{  04-28-89                                                             }
{       Changed drug exception to look for PS when P wasn't found on    }
{  the panel.  (Bob)                                                    }
{  06-07-89                                                             }
{       Filled interp record with $66's so that therapies which are not }
{  found in the system will report as N/R and not N/A.  (Bob)           }
{  10-16-89                                                             }
{       Made changes to support contraindication apart from the therapy }
{  data file. (Bob)                                                     }
{-----------------------------------------------------------------------}
{-----------------------------------------------------------------------}
{ THE DRUGS MODULE.                                                     }
{ 10-Mar-87                                                             }
{       Added a new module -- TherpAge.  (Curt)                         }
{  documented and Mendefied it.  (Bob)                                  }
{                                                                       }
{-----------------------------------------------------------------------}
{-----------------------------------------------------------------------}
{ Interface to the organism group data file used by PrCust and the drug }
{ suppression code.                                                     }
{ Change Log :                                                          }
{    Written by Bob M.  24-Feb-89   for new suppression feature.        }
{    Changed by Bob M.  17-Jan-90   added OrgInGroup.                   }
{-----------------------------------------------------------------------}
{- 02/95     RCF  reformatted according to JDMS V3.0 standards.         }
{- 07/95     RCF  changed the TIntep object's parameter requirements    }
{-----------------------------------------------------------------------}

INTERFACE

uses
  OrgGroup,
  ResLib,
  DBFile,
  DBTypes,
  DBIds,
  ESBL,
  Objects,
  Screens,
  Trays,
  WinProcs,
  WinApi;

const
  UrineFlag     = $2000;
  gtBit         = $20;
  ltBit         = $40;
  nrBit         = $80;
  AbrTxtLength  = 3;
  ExcTxtLength  = 7;
  GrpTxtLength  = 3;
  MicTxtLength  = 3;
  ThrTxtLength  = 3;
  TryTxtLength  = 2;
  NegGroup      = 1;
  PosGroup      = 2;
  AnaGroup      = 3;
  drgRecType    = 5;
  tryRecType    = 2;
  {Absolute Drug Numbers}
  czu           = 29;
  oxy           = 62;
  pen           = 64;
  penSt         = 69;
  pen2          = 89;
  {Mdi values to test at}
  czuMdi        = 16;
  oxyMdi2       =  7;
  oxyMdi1       =  6;
  penMdi        =  3;
  {Extra Test bits}
  bLPlus1       =  0;
  bLPlus2       =  1;
  oxRes1        =  2;
  oxRes2        =  3;
  strepR        =  4;
  gentR         =  6;
  TfgNeg1       =  8;
  TfgNeg2       =  9;
  EmptyExcept   =  0;
  MaxAge        =  3;
  MaxCmdLength  = 15;
  MaxExc        = 16;  { Double the number of exceptions per drug, supports 97/98 NCCLS, ghg v4.00, 8/1/98 }
  MaxMdi        = 31;
  MicPerRec     = 32;
  offset        =  2;
  ciOffset      = 15;
  MaxRoute      =  4;  { Maximum number of routes. }
  MaxMdaEntries = 31;  { Maximum number of entries in an Master Dilution array.}
  {Organism Group Numbers - used as constants to pass to org group routines }
  gStaph        = $1;   { Staphlococcus organism group, ghg v4.00, 8/1/98 }
  gStrep        = $2;   { Streptococcus organism group, ghg v4.00, 8/1/98 }
  gEntro        = $3;   { Enterococcus organism group, ghg v4.00, 8/1/98 }
  gStPnm        = $4;   { Streptococcus pneumonia organism group, ghg v4.00, 8/1/98 }
  gNFerm        = $5;   { Non fermenter organism group, ghg v4.00, 8/1/98 }
  gPsudo        = $6;   { Psudomona organism group, ghg v4.00, 8/1/98s }
  gLMono        = $7;   { L. monocytogones organism group, ghg v4.00, 8/1/98 }
  gPsAer        = $8;   { Psudomonas aeruginosa organism group, ghg v4.00, 8/1/98 }
  gCitro        = $9;   { Citrobacter organism group, ghg v4.00, 8/1/98 }
  gPAndP        = $0A;  { Proteus and providencia organism group, ghg v4.00, 8/1/98 }
  gAAnit        = $0B;  {  }
  gMmPm         = $0C;  {  }
  gSalSg        = $0D;  { Salmonella/Shigella organism group, ghg v4.00, 8/1/98 }
  gVStrp        = $0E;  { Viridans streptococcus organism group, ghg v4.00, 8/1/98 }
  gBStrp        = $0F;  { Beta streptococcus organism group, ghg v4.00, 8/1/98 }
  gDAmal        = $10;  {  }
  gSerta        = $11;  {  }
  gCns          = $12;  {  }
  gBLacI        = $13;  {  }
  gSalmo        = $14;  { Salmonella organism group, ghg v4.00, 8/1/98 }
  gXanth        = $15;  { Xanthamonas organism group, ghg v4.00, 8/1/98 }
  gSyngy        = $16;  { Synergy organism group, ghg v4.00, 8/1/98}
  gAcine        = $17;  { Acinetobacter organism group, ghg v4.00, 8/1/98 }
  gCarmn        = $18;  { Orgs that should not report Carumonam }
  gBhStp        = $19;  { Beta-Hemolysis=Neg group from v22.XX, ghg v4.00, 8/1/98 }
  gESBL         = $1A;  { ESBL organism group }
  gPALSt        = $1B;  { Pediococcus/Aerococcus/Leuconostoc organism group, ghg v4.00, 8/1/98 }
  gGASSt        = $1C;  {  }
  gVChol        = $1D;  { V cholera organism group, ghg v4.00, 8/1/98 }
  gSaure        = $1E;  { Staph aureus organism group, ghg v4.00, 8/1/98 }
  gHaemo        = $1F;  { Haemophilus organism group, ghg v4.00, 8/1/98 }
  gMiscO        = $20;  { Miscellaneous other organism group, ghg v4.00, 8/1/98 }
  gNgono        = $21;  { N. gonohreae organism group, ghg v4.00, 8/1/98 }
  gOFast        = $22;  { Other fastidious organism group, ghg v4.00, 8/1/98 }

type
  InterpTable  = array[0..MaxRoute] of byte;
  { BLacRec is an array large enough to hold bit map of MaxDrugs  }
  { bits.  The file contains one record for each tray.    }
  BLacRec      = array[1..10] of byte;
  BLacMapType  = array[0..15] of byte;
  AbrStrType   = string[AbrTxtLength];
  ExcStrType   = string[ExcTxtLength];
  GrpStrType   = string[GrpTxtLength];
  MicStrType   = string[MicTxtLength];
  ThrStrType   = string[ThrTxtLength];
  TryStrType   = string[TryTxtLength];
  AdnType      = byte;
  DrnType      = integer;
  ExcType      = array[0..1] of integer;  { Double the number of exceptions per drug, supports 97/98 NCCLS, ghg v4.00, 8/1/98 }
  IdType       = byte;
  ILineType    = string[254];
  SupType      = byte;
  TherType     = byte;
  abrTable     = array[1..MaxDrugs] of AbrStrType;
  OrgRecType = record
    grpNum  : byte;
  end;
  (*GenRec       = string[261];*)
  GenRec       = array[0..261] of byte; { Supports ExcType change, ghg, v4.00}
  MicTRec = record
    id      : idType;
    micTxt  : array[1..MicPerRec] of MicStrType;
    filler  : array[1..133] of byte;    { Supports ExcType change, ghg, v4.00}
  end;
  TrayRec = record
    id      : idType;
    sup     : SupType;
    grp     : byte;
    drgCnt  : byte;
    drugDrn : array[1..MaxDrugs] of DrnType;
    adnTbl  : array[1..44] of byte;
    filler  : array[1..64] of byte;     { Supports ExcType change, ghg, v4.00}
  end;
  TherField  = array[0..MaxMdi] of TherType;
  DrugRec = record
    id      : idType;
    adn     : AdnType;
    sup     : SupType;
    ncOn    : boolean;
    msOn    : boolean;
    rtCnt   : byte;
    exc     : array[1..MaxExc] of ExcType;
    next    : array[1..MaxExc] of DrnType;
    ncTher  : TherField;
    msTher  : array[1..MaxRoute] of TherField;
  end;
  TrayDrugPairs = record
    trayNum  : byte;
    drugNum  : byte;
  end;

  MicTblType = array[0..MaxMdi] of MicStrType;

  InterpResults = record
    TrayType  : integer;
    MICLevels : MICs;
    Organism  : integer;
    RFlags    : integer;
    XtraTests : integer;
  end;

  PInterpsObject = ^TInterpsObject;
  TInterpsObject = object(TObject)
    orgfile         : file of OrgRecType;
    orgRec          : OrgRecType;
    therfile        : file of GenRec;
    bLacfile        : file of BLacRec;
    bLacBuffer      : BLacRec;
    doBLac          : boolean;
    msBLacMapTable  : BLacMapType;
    ncBLacMapTable  : BLacMapType;
    interpRes       : InterpResults;
    trayBuff        : TrayRec;
    drugBuff        : DrugRec;
    testBLac        : boolean;
    orgGroup        : integer;
    lastOrgGroup    : integer;
    exception       : ExcType;
    traySup         : SupType;
    ageGroup        : integer;
    ogOffset        : integer;
    anyCI           : boolean;
    blap            : GenRec;
    orgList         : array[1..10] of byte;
    TraysObject     : PTraysObject;
    OrgGroupObject  : POrgGrpObj;
    constructor Init(ATraysObject: PTraysObject);
    destructor done; virtual;
    procedure InterpAbort(ErrNum: integer);
    function GetOrgGrp(orgnum: integer): integer;
    function TstForBLac(rdn: integer): boolean;
    procedure BLacReadRec(trayNum: integer);
    function TherRecRead(drn: integer; recAddr: pointer; recType: byte): boolean;
    function TherRecWrite(drn: integer; recAddr: pointer; recType: byte): boolean;
    function BT(bitOffset: integer): boolean;
    function DT(adn, mdi: byte): boolean;
    function OT(orgGrp: integer): boolean;
    procedure SB(offset: byte);
    procedure BuildException;
    procedure GetDrugRec(rdn: integer);
    function GetTher(rdn: integer; var iTable: InterpTable; getNc, getMs: boolean): integer;
    procedure TherpIso(age: integer; var newIso: InterpResults);
    function TherpAge(dob, testDate: word; pFlags: integer): integer;
  end;

  PInterp = ^TInterp;
  TInterp = object(TObject)
    interpsObj: PInterpsObject;
    resList   : PResults;
    interpRes : InterpResults;
    numTests  : integer;
    {}
    constructor Init(anOrgID: integer; aResults: PResults);
    destructor Done; virtual;
    procedure SetupInterps(tstID, result: PChar);
    procedure CalcInterps(isUrineSrc: boolean);
    procedure CalcESBLInterps(isUrineSrc: boolean);
    procedure SetExtraTestBits(testID, testCat: PChar; existsBit: integer); virtual;
    procedure EditInterps(Abbr: PChar; iTable: InterpTable; urineSrc: boolean);
    procedure LoadESBLInfo(var ESBLResult: ESBLRes; var ESBLInfo: ESBLRecord);
    procedure ESBLInterps(ESBLResult: ESBLRes;  ESBLInfo: ESBLRecord;
                          RDN: Integer; var iTable: InterpTable; urineSrc: boolean);
  end;

IMPLEMENTATION

uses
  Bits,
  StrsW,
  MScan,
  IntrpMap,
  APITools,
  DMString,
  Strings,
  DMSErr,
  DMSDebug;

constructor TInterpsObject.Init(ATraysObject: PTraysObject);
{----------------------------------------------------------------------}
{ TherpInit has to be called before any other TherLib routine.  It     }
{ opens the Therapy.dat file and Initializes the format variable.      }
{ The function result returns True if the file opened correctly.       }
{----------------------------------------------------------------------}
var
  tData : string[30];
  i     : integer;
begin
  inherited Init;
  doBLac:= ScreenOn(IBInterps);

  if doBLac then
  begin
    Assign(bLacFile, bLacfname);
    Reset(bLacFile);
    Read(bLacFile, bLacBuffer);


    tData:=GetBLacMappings;  { get NCCLS interp mapping values }
    for i:= 1 to sizeof(BLacMapType) do
      tData[i]:= CHR(ORD(tData[i]) - ORD('A'));
    MOVE(tData[1], ncBLacMapTable, sizeof(BLacMapType));

    tData:=GetBLacMappings; { get MicroScan BLac interp mapping values }
    for i:= 1 to sizeof(BLacMapType) do
      tData[i]:= CHR(ORD(tData[i]) - ORD('A'));
    MOVE(tData[1], msBLacMapTable, sizeof(BLacMapType));
  end;

  {$IFOPT I+}
    {$DEFINE chki}
  {$ENDIF}
  {$I-}

  Assign(therFile, 'THERAPY.DAT');
  Reset(TherFile);
  if (IOResult <> 0) then
    InterpAbort(1);
  Assign(orgFile, 'ORGFILE.DAT');
  Reset(orgFile);
  if (IOResult <> 0) then
    InterpAbort(2);
  OrgGroupObject:=nil;
  OrgGroupObject:= New(POrgGrpObj, Init);
  if OrgGroupObject = nil then
  begin
    Done;
    Fail;
  end;
  TraysObject:= ATraysObject;
end;

destructor TInterpsObject.Done;
{----------------------------------------------------------------------}
{ TherpClose closes the therapy data file opened by TherpInit.         }
{ The function result returns true if the close was successful.        }
{----------------------------------------------------------------------}
begin
  inherited Done;
  if doBLac then Close(bLacFile);
  if OrgGroupObject <> nil then
    Dispose(OrgGroupObject, done);
  Close(therFile);
  if IOResult <> 0 then;
  Close(orgFile);
  if IOResult <> 0 then;

  {$IFDEF chki}
    {$I+}
    {$UNDEF chki}
  {$ENDIF}
end;

procedure TInterpsObject.InterpAbort(ErrNum: integer);
{-----------------------------------------------------------------------}
{ purpose :  Error out when some condition has been met.                }
{ input   :  errnum  - The error message to be displayed.               }
{-----------------------------------------------------------------------}
begin
  Halt;
end;

function TInterpsObject.GetOrgGrp(orgNum: integer): integer;
{----------------------------------------------------------------------}
{ GetOrgGrp takes the orgNum and returns CI info.                      }
{----------------------------------------------------------------------}
begin
  Seek(orgFile, (orgNum - 1));
  Read(orgFile, orgRec);
  GetOrgGrp:= orgRec.grpNum;
end;

function TInterpsObject.TstForBLac(rdn: integer): boolean;
{----------------------------------------------------------------------}
{ TstForBLac returns true if drug is a BetaLactam. Assumes that doBlac }
{ is true, the  bLacfile is open (TherpInit) and the record for this   }
{ tray has been read into bLacBuffer (BLacReadRec).                    }
{----------------------------------------------------------------------}
begin
  TstForBLac:= TestBit(bLacBuffer, rdn)
end;

procedure TInterpsObject.BLacReadRec(trayNum: integer);
{----------------------------------------------------------------------}
{ Read record from BLac file.  Assumes that doBLac is true, and the    }
{ bLacfile is open (TherpInit).                                        }
{----------------------------------------------------------------------}
begin
  Seek(bLacFile, trayNum);
  Read(bLacFile, bLacBuffer);
end;

function TInterpsObject.TherRecRead(drn: integer; recAddr: pointer; recType: byte): boolean;
{-----------------------------------------------------------------------}
{  TherRecRead reads a record from the therapy data file, indexed by    }
{  the data record number.  The expected type of the record is compared }
{  for equality.  True is returned if the recType matches, and there    }
{  are no other errors.                                                 }
{-----------------------------------------------------------------------}
begin
  TherRecRead:= false;
  if drn <= 0 then
    exit;
  Seek(therFile, drn);
  if IOResult <> 0 then
    exit;
  Read(therFile, GenRec(recAddr^));
  if IOResult <> 0 then
    exit;
  if (byte(recAddr^) <> recType) then
    exit;
  TherRecRead:= TRUE;
end;

function TInterpsObject.TherRecWrite(drn: integer; recAddr: pointer; recType: byte): boolean;
{-----------------------------------------------------------------------}
{  TherRecRead writes a record to the therapy data file, indexed by     }
{  the data record number.  The expected type of the record is compared }
{  for equality.  True is returned if the recType matches, and there    }
{  are no other errors.                                                 }
{- added 11/94 rcf                                                      }
{-----------------------------------------------------------------------}
begin
  TherRecWrite:= false;
  if (byte(recAddr^) <> recType) then
    exit;
  if drn <= 0 then
    exit;
  Seek(therFile, drn);
  if IOResult = 0 then
  begin
    write(therFile, GenRec(recAddr^));
    TherRecWrite:= IOResult = 0;
  end;
end;

function TInterpsObject.BT(bitOffset: integer): boolean;
{-----------------------------------------------------------------------}
{  BT, or BitTest, tests a bit in the results record.                   }
{-----------------------------------------------------------------------}
begin
  BT:= TestBit(interpRes.xtraTests, bitOffset);
end;

function TInterpsObject.DT(adn, mdi: byte): boolean;
{-----------------------------------------------------------------------}
{  DT, or DrugTest, tests if the result mdi for the given adn was       }
{  greater then the mdi passed in.                                      }
{-----------------------------------------------------------------------}
var
  isoMdi, rdn : byte;
begin
  DT:= false;
  rdn:= 1;
  while (trayBuff.adnTbl[rdn] <> adn) and (rdn <= TrayBuff.drgCnt) do
    Inc(rdn);
  if (rdn > TrayBuff.drgCnt) and (adn = Pen) then
  begin
    rdn:= 1;
    while (trayBuff.adnTbl[rdn] <> PenSt) and (rdn <= TrayBuff.drgCnt) do
      Inc(rdn);
    if (rdn > TrayBuff.drgCnt) then
    begin
      rdn:= 1;
      while (trayBuff.adnTbl[rdn] <> Pen2) and (rdn <= TrayBuff.drgCnt) do
        Inc(rdn);
    end;
  end;
  if (rdn > TrayBuff.drgCnt) then
    exit;
  isoMdi:= interpRes.micLevels[rdn];
  DT:= TraysObject^.micGt(isoMdi, mdi);
end;

function TInterpsObject.OT(orgGrp: integer): boolean;
{-----------------------------------------------------------------------}
{  OT, or OrganismTest, looks up if the organism number is in the org   }
{  group passed in.                                                     }
{-----------------------------------------------------------------------}
begin
  OT:= orgGroup = orgGrp;
end;

procedure TInterpsObject.SB(offset: byte);
{-----------------------------------------------------------------------}
{  SB calls StickBit to set a bit in the exception variable.            }
{-----------------------------------------------------------------------}
begin
  SetBit(exception, offset);
end;

procedure TInterpsObject.BuildException;
{-----------------------------------------------------------------------}
{  BuildException builds an exception value to be used to determine if  }
{  an exception mask is valid. All possible exceptions for a given tray }
{  type are tested, and a corrisponding bit is set for true results.    }
{-----------------------------------------------------------------------}
var
  d       : integer;
  orgNum  : integer;
  miscOrg : boolean;
  i       : integer;
  oxyMdi  : integer;
begin
  exception[0]:= 0;  { Supports ExcType change, ghg, v4.00}
  exception[1]:= 0;
  {this scum line of code is here to handle the fact }
  {that panel 11 does not have a dilution of 2 for Ox}
(*  if interpRes.trayType = 11 then oxyMdi:= oxyMdi1 else *)
  oxyMdi:= oxyMdi2;

  orgNum := interpRes.organism;
  ogOffset:= (((traysObject^.tfsRec.trayNum) + ciOffset) * (MaxDrugs + 1)) + 1;
  d:= GetOrgGrp(ogOffset);

  anyCI:= (d <> 0) and (OrgGroupObject^.OrgInGroup(orgNum, 0));

  testBLac:= ((doBLac) and (OrgGroupObject^.OrgInGroup(orgNum, gBLacI)));

  if (trayBuff.grp mod 10) = NegGroup then
    with OrgGroupObject^ do
    begin
      if OrgInGroup(orgNum, gNFerm)    then SB($01);
      if OrgInGroup(orgNum, gPsudo)    then SB($02);
      if OrgInGroup(orgNum, gPsAer)    then SB($03);
      if DT(czu, czuMdi)               then SB($04);
      if OrgInGroup(orgNum, gCitro)    then SB($05);
      if OrgInGroup(orgNum, gPAndP)    then SB($06);
      if OrgInGroup(orgNum, gMiscO)    then SB($07);
      if OrgInGroup(orgNum, gAAnit)    then SB($08);
      if OrgInGroup(orgNum, gMmPm )    then SB($09);
      if OrgInGroup(orgNum, gSalSg)    then SB($0A);
      if OrgInGroup(orgNum, gDAmal)    then SB($0B);
      if OrgInGroup(orgNum, gSerta)    then SB($0C);
      if OrgInGroup(orgNum, gCarmn)    then SB($0D);
      if OrgInGroup(orgNum, gVChol)    then SB($10); { 97/98 NCCLS tables, v4.00, ghg 8/1/98 }
      if OrgInGroup(orgNum, gHaemo)    then SB($11); { 97/98 NCCLS tables, v4.00, ghg 8/1/98 }
      if OrgInGroup(orgNum, gNgono)    then SB($12); { 97/98 NCCLS tables, v4.00, ghg 8/1/98 }
      if OrgInGroup(orgNum, gOFast)    then SB($13); { 97/98 NCCLS tables, v4.00, ghg 8/1/98 }
    end;

  if (trayBuff.grp mod 10) = PosGroup then with OrgGroupObject^ do
  begin
    if OrgInGroup(orgNum, gStaph)      then SB($01);
    if OrgInGroup(orgNum, gEntro)      then SB($02);
    if OrgInGroup(orgNum, gStrep)      then SB($03);
    if OrgInGroup(orgNum, gStPnm)      then SB($04);
    if DT(oxy, oxyMdi)                 then SB($05);
    if DT(pen, penMdi)                 then SB($06);
    if OrgInGroup(orgNum, gLMono)      then SB($07);
    if BT(blPlus1) and BT(blPlus2)     then SB($08);
    if not BT(TfgNeg1) and BT(TfgNeg2) then SB($09);
    if not BT(strepR)                  then SB($0A);
    if not BT(gentR)                   then SB($0B);
    if (BT(oxRes1) and BT(oxRes2))     then SB($0C);
    if OrgInGroup(orgNum, gVStrp)      then SB($0D);
    if OrgInGroup(orgNum, gBStrp)      then SB($0E);
    if OrgInGroup(orgNum, gCns  )      then SB($0F);
    if OrgInGroup(orgNum, gSaure)      then SB($10); { 97/98 NCCLS tables, v4.00, ghg 8/1/98 }
    if OrgInGroup(orgNum, gHaemo)      then SB($11); { 97/98 NCCLS tables, v4.00, ghg 8/1/98 }
    if OrgInGroup(orgNum, gNgono)      then SB($12); { 97/98 NCCLS tables, v4.00, ghg 8/1/98 }
    if OrgInGroup(orgNum, gOFast)      then SB($13); { 97/98 NCCLS tables, v4.00, ghg 8/1/98 }
  end;
end;

procedure TInterpsObject.GetDrugRec(rdn: integer);
{-----------------------------------------------------------------------}
{  GetDrugRec gets the head drug record drn for a given rdn from the    }
{  tray record, tests the exception conditions in the exception list    }
{  until 'AND'ing the exception with the exception mask result in zero. }
{  if the corresponding next field is equal to zero then no exceptions  }
{  were true and the right record is therefore already in memory, but   }
{  if this value is not zero then it is the drn for the needed drug     }
{  record which must be read in.                                        }
{-----------------------------------------------------------------------}
var
  bitMap : array[1..10] of byte;
  drn, i : integer;
begin
  {test if drug is contra-indicated before going on}
  if anyCI then
  begin
    drn:= GetOrgGrp((ogOffset + rdn));
    if drn <> 0 then
    begin
      drn:= ((MaxTray + ciOffset + drn) * (MaxDrugs + 1));
      for i:= 1 to 10 do
        bitMap[i]:= GetOrgGrp((drn + i));
      for i:= 1 to gMiscO do
        if TestBit(bitMap, i) and OrgGroupObject^.orgInGroup(interpRes.organism, i) then
        begin
          FillChar(drugBuff, sizeof(drugbuff), CHR($FF));
          exit;
        end;
    end;
  end;
  drn:= trayBuff.drugDrn[rdn];
  if not TherRecRead(drn, Addr(drugBuff), drgRecType) then
    exit;
  i:= 1;
  while ((drugBuff.exc[i][0] and exception[0]) <> 0) or   { Supports ExcType change, ghg, v4.00}
        ((drugBuff.exc[i][1] and exception[1]) <> 0) do
    Inc(i);
  drn:= drugBuff.next[i];
  if drn <> 0 then
    if not TherRecRead(drn, Addr(drugBuff), drgRecType) then
      exit;
end;

function TInterpsObject.GetTher(rdn: integer; var iTable: InterpTable;
                                getNc, getMs: boolean): integer;
{----------------------------------------------------------------------}
{ GetTher gets the interpretation for a given mdi.  TherpIso must be   }
{ called before the first call to GetTher.                             }
{ Input:  rdn   -  The relative drug number for this panel.            }
{         getNc -  Should be true to report NCCLS therapy.             }
{         getMs -  Should be true to report MicroScan therapy.         }
{ Output: iTable  -  A array of interpretation nibble pairs,           }
{         where the zeroth byte is the nccls blood and                 }
{         urine interps, and bytes 1 - MaxRoute are                    }
{         MicroScan blood (high order nibble) and                      }
{         urine (low order) interps for each route.                    }
{  The function result returns the number of routes.                   }
{----------------------------------------------------------------------}
var
  i, j,  k : integer;
  mdi      : byte;
begin
  FillChar(drugBuff, sizeof(drugbuff), CHR(0));
  FillChar(drugBuff.ncther, 160, CHR($66));
  GetDrugRec(rdn);
  { flag compiler suppression }
  iTable[0]:= $FF;
  if (drugBuff.sup and $80 = $80) then
    exit;

  if ((interpRes.rFlags and urineFlag) = 0) and (traySup and 8 = 8) then
    traySup:= traySup or 2;

  mdi:= interpRes.micLevels[rdn];
  if mdi and nrBit = nrBit then
    mdi:= 0
  else if mdi and gtBit = gtBit then
    mdi:= 31
  else
    mdi:= mdi and $1F;  { Reset all high bits }

  for i:= 0 to MaxRoute do
    iTable[i]:= $66;
  if getNc then
    iTable[0]:= drugBuff.ncTher[mdi];

  if getMs then
    for i:= 1 to MaxRoute do
      iTable[i]:= drugBuff.msTher[i][mdi];
  if (traySup and 1 = 1) then  { force to blank }
    for i:= 0 to MaxRoute do
      iTable[i]:= iTable[i] and $0F or $60;
  if (traySup and 2 = 2) then  { force to blank }
    for i:= 0 to MaxRoute do
      iTable[i]:= iTable[i] and $F0 or $06;

  if testBLac then
  begin
    if TstForBLac(rdn) then
    begin
     { do nccls mapping }
      j:= iTable[0] and $0F;
      k:= (iTable[0] shr 4);
      j:= ncBLacMapTable[j];
      k:= ncBLacMapTable[k];
      iTable[0]:= (k shl 4) + j;

      { do ms mapping }
      for i:= 1 to MaxRoute do
      begin
        j:= iTable[i] and $0F;
        k:= (iTable[i] shr 4);
        j:= msBLacMapTable[j];
        k:= msBLacMapTable[k];
        iTable[i]:= (k shl 4) + j;
      end; { loop on route count }
    end;  { if TstForBLac }
  end; { if testBLac }

  if drugBuff.rtCnt = 0 then
    GetTher:= 1
  else
    GetTher:= drugBuff.rtCnt;
end;

procedure TInterpsObject.TherpIso(age: integer; var newIso: InterpResults);
{----------------------------------------------------------------------}
{ TherpIso sets up for processing a new isolate record.  It Resets     }
{ various variables, calculates the tray index record number from the  }
{ tray number and the age group of the patient, and reads in that      }
{ for future calls.                                                    }
{ Input: age     -  The age group of the patient from TherpAge.        }
{        newIso  -  The isolate record to process.                     }
{ Output:  <none>  -                                                   }
{----------------------------------------------------------------------}
var
  drn: integer;
begin
  FillChar(trayBuff, sizeof(traybuff), CHR(0));
  interpRes:= newIso;
  ageGroup:= age;
  if doBLac then
    BLacReadRec(interpRes.trayType);
  drn:= ((interpRes.trayType-1) * (MaxAge + 1) + ageGroup + offset + 1);
  if not TherRecRead(drn, Addr(trayBuff), tryRecType) then
    {nothing};
  traySup:= trayBuff.sup;
  BuildException;
end;

function TInterpsObject.TherpAge(dob, testDate: word; pFlags: integer): integer;
{----------------------------------------------------------------------}
{ TherpAge calculates the interpretation age group.                    }
{ Input: dob     - The date of birth of the patient.                   }
{        testdate- The date of the test.                               }
{        pFlags  - The pediatric flag.                                 }
{ The function result returns the patient age group.                   }
{----------------------------------------------------------------------}
var
  daysOld : word;
  ageGrp  : integer;
begin
  ageGrp:= 0;
  therpAge:= 0;          { default age group is adult }
    { if dob or test date missing or ped flag not set, assume adult }
  if (dob = 0) or ((pFlags and 1) = 0) or (testDate = 0) then
    exit;
  daysOld:= testDate - dob;
  if daysOld < 8 then
    ageGrp:= 1    { 0 to 7 days }
  else if daysOld < 29 then
    ageGrp:= 2    { 8 to 28 days }
  else
    ageGrp:= 3;    { 29 days and up }
  therpAge:= ageGrp;
end;


{*****************************************************************************}

constructor TInterp.Init(anOrgID: integer; aResults: PResults);
{- this object assumes that the results passed in have had its tray object
   properly loaded with the tray in question }
var
  i       : integer;
  tmpstr  : string;
begin
  inherited Init;
  resList:= aResults;
  numTests:= resList^.TraysObj^.DrugCnt;
  interpsObj := nil;
  InterpsObj:= New(PInterpsObject, Init(resList^.TraysObj));
  if InterpsObj = nil then
  begin
    Done;
    Fail;
  end;
  with InterpRes do
  begin
    TrayType:= resList^.TraysObj^.tfsRec.trayNum;
    organism:= anOrgID;
  end;
end;

destructor TInterp.Done;
begin
  inherited Done;
  if InterpsObj <> nil then
    Dispose(interpsObj, Done);
end;

procedure TInterp.SetupInterps(TstID, Result: PChar);
var
  i   : integer;
  rdn : integer;
  rslt, dil : array[0..25] of char;
begin
  TrimAll(rslt, result, ' ', sizeof(rslt)-1);
  for i:= 1 to NumTests do
  begin
    if StrComp(resList^.traysObj^.dilList[i].drugname, tstId) = 0 then
    begin
      InterpRes.MICLevels[i]:= 0;
      for rdn:= 0 to resList^.traysObj^.dilList[i].DilCnt do
      begin
        TrimAll(dil, resList^.traysObj^.dilList[i].DrugDils[rdn], ' ', sizeof(dil)-1);
        if StrComp(dil, rslt) = 0 then
          InterpRes.MICLevels[i]:= resList^.traysObj^.GetMDI(i, rdn, false);
      end;
    end;
  end;
end;

procedure TInterp.SetExtraTestBits(testID, testCat: PChar; existsBit: integer);
var
  resVal  : array[0..30] of char;
begin
  if ResList^.GetResult(testID, testCat, resVal, sizeof(resVal)-1) then
  begin
    SetBit(InterpRes.xtraTests, existsBit + 1);
    { if the test was positive then set the bit only if the test was TFG, BLAC or OXI.
      if the test was sts or gms then a positive means susceptible, or no growth, so
      dont set the extra test bit. }
    if (StrComp(resVal, '+') = 0) then
    begin
      if (StrComp(testID, '.TFG') = 0) or
         (StrComp(testID, '.BL') = 0) or
         (StrComp(testID, '.OXI') = 0) then
        SetBit(InterpRes.xtraTests, existsBit)
      { For sts and gms a '-' means resistant or growth exists so set the extra test bit.}
      else if (StrComp(resVal, '-') = 0) then
      begin
        if (StrComp(testID, '.Sts') = 0) or (StrComp(testID, '.Gms') = 0) then
          SetBit(InterpRes.xtraTests, existsBit);
      end;
    end;
  end;
end;

procedure TInterp.CalcInterps(isUrineSrc: boolean);
var
  i         : integer;
  iTable    : InterpTable;
  ESBLResult: ESBLRes;
  ESBLInfo  : ESBLRecord;

begin
  with InterpRes do
  begin
    rFlags:= 0;
    if isUrineSrc then
      rFlags:= rFlags or UrineFlag;

    XtraTests := 0;
  end;

  SetExtraTestBits('.BL', 'BL', 0);
  SetExtraTestBits('.OXI', 'OXI', 2);
  SetExtraTestBits('.Sts', 'Sts', 4);
  SetExtraTestBits('.Gms', 'Gms', 6);
  SetExtraTestBits('.TFG', 'TFG', 8);

  LoadESBLInfo(ESBLResult, ESBLInfo);

  InterpsObj^.TherpIso(0, InterpRes);
  for i:= 1 to NumTests do
  begin
    InterpsObj^.GetTher(i, iTable, true, false);
    EditInterps(resList^.traysObj^.dilList[i].drugname, iTable, isUrineSrc);
    ESBLInterps(ESBLResult, ESBLInfo, i, iTable, isUrineSrc);
  end;
end;

procedure TInterp.CalcESBLInterps(isUrineSrc: boolean);
var
  RDN       : integer;
  iTable    : InterpTable;
  ESBLResult: ESBLRes;
  ESBLInfo  : ESBLRecord;

begin
  with InterpRes do
  begin
    rFlags:= 0;
    if isUrineSrc then
      rFlags:= rFlags or UrineFlag;

    XtraTests := 0;
  end;

  SetExtraTestBits('.BL', 'BL', 0);
  SetExtraTestBits('.OXI', 'OXI', 2);
  SetExtraTestBits('.Sts', 'Sts', 4);
  SetExtraTestBits('.Gms', 'Gms', 6);
  SetExtraTestBits('.TFG', 'TFG', 8);

  LoadESBLInfo(ESBLResult, ESBLInfo);

  InterpsObj^.TherpIso(0, InterpRes);
  for RDN:= 1 to NumTests do
  if ESBLInfo.Mdis[RDN]<>ESBLNA then
  begin
    InterpsObj^.GetTher(RDN, iTable, true, false);
    EditInterps(resList^.traysObj^.dilList[RDN].drugname, iTable, isUrineSrc);
    ESBLInterps(ESBLResult, ESBLInfo, RDN, iTable, isUrineSrc);
  end;
end;

procedure TInterp.EditInterps(abbr: PChar; iTable: InterpTable; urineSrc: boolean);
var
  res   : array[0..16] of char;
  ci    : boolean;
begin
  if urineSrc then
    InterpCodeToStr(iTable[0] and $0F, res, sizeof(res)-1, ci)
  else
    InterpCodeToStr(iTable[0] shr 4, res, sizeof(res)-1, ci);

  resList^.SetResult(abbr, 'NCCLS', res, true);
  {- Interp should tell reslib whether or not the drug was contraindicated }
  resList^.SetFlag(abbr, 'NCCLS', REScontraIndicated, ci);
end;

procedure TInterp.LoadESBLInfo(var ESBLResult: ESBLRes; var ESBLInfo: ESBLRecord);
var  ESBLStr : array [0..RESmaxStrResult] of char;
begin
  ResList^.ESBLChanged(InterpRes.Organism);
  ResList^.GetESBLResult(ESBLStr,RESmaxStrResult);
  if StrComp(ESBLStr,'+')=0
     then ESBLResult:=rESBL
     else if StrComp(ESBLStr,'?')=0
          then ESBLResult:=rEBL
          else ESBLResult:=rESBLNA;

  ESBLMics(ResList^.TraysObj^.tfsRec.trayNum, ESBLInfo);
end;

procedure TInterp.ESBLInterps(ESBLResult: ESBLRes; ESBLInfo: ESBLRecord;
                              RDN: Integer; var iTable: InterpTable; urineSrc: boolean);
var TheIntrp: Integer;
    CodeStr : array[0..16] of char;
    Contra  : boolean;

begin
  If (iTable[0]<>$FF) and (ESBLResult <> rESBLNA) then
  If (ESBLinfo.Mdis[RDN]<>ESBLNA) then
  begin
    If ESBLMicRule(InterpRes.MICLevels[RDN], ESBLinfo.Mdis[RDN]) then
       If ESBLResult=rESBL
          then TheIntrp:=ESBLIntrpCode
          else TheIntrp:=EBLIntrpCode
    else TheIntrp:=RStarIntrpCode;

    if urineSrc
      then iTable[0]:= (iTable[0] and $0F) or (TheIntrp shl 4)
      else iTable[0]:= (iTable[0] and $F0) or TheIntrp;

    InterpCodeToStr(TheIntrp, CodeStr, sizeof(CodeStr)-1, Contra);

    With ResList^ do
    begin
      SetResult(TraysObj^.dilList[RDN].drugname, 'NCCLS', CodeStr, true);
      SetFlag(TraysObj^.dilList[RDN].drugname, 'NCCLS', REScontraIndicated, Contra);
    end;

  end; {if..}
end;


END.


