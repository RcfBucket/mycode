unit StatBar;

INTERFACE

uses
  objects,
  strings,
  OWindows,
  WinProcs,
  WinTypes;

const
  SB_ALIGNLEFT   = 0;
  SB_ALIGNCENTER = 1;
  SB_ALIGNRIGHT  = 2;

  SB_BORSTYLE    = 0;
  SB_WORDSTYLE   = 1;

type
  PStatusBar = ^TStatusBar;
  TStatusBar = object(TWindow)
    constructor Init(aParent: PWindowsObject; aStyle: integer);
    destructor Done; virtual;
    procedure SetupWindow; virtual;
    procedure AddSegment(txt: PChar; aWidth, anAlign: integer);
    procedure RemoveSegment;
    procedure ChangeSegmentText(segment: integer; txt: PChar);
    procedure ChangeSegmentStyle(segment: integer; aWidth, anAlign: integer);
    procedure ChangeBarStyle(newStyle: integer);
    procedure ParentResized(parentR: TRect);

    procedure Paint(dc: HDC; var ps: TPaintStruct); virtual;
    procedure GetWindowClass(var AWndClass: TWndClass); virtual;
    function GetClassName: PChar; virtual;
    procedure WMSetFont(var msg: TMessage); virtual WM_FIRST + WM_SETFONT;
    function GetStyle: integer;
    procedure NCHitTest(var msg: Tmessage); virtual WM_FIRST + WM_NCHITTEST;
    private
    segments  : PCollection;
    wtPen     : HPen;
    dkGrPen   : HPen;
    style     : integer;  {- 0 = Borland style, 1 = Word style }
    cy        : integer;
    redraw    : Boolean;
    font      : HFont;
    procedure PaintWordStyle(dc: HDC; r: TRect);
    procedure PaintBorStyle(dc: HDC; r: TRect);
  end;

IMPLEMENTATION

uses
  DMSDebug;

type
  PSegment = ^TSegment;
  TSegment = object(TObject)
    buf      : PChar;
    width    : integer;
    alignment: integer;
    constructor Init(AStr: PChar; aWidth, anAlign: integer);
    destructor Done; virtual;
    procedure ChangeText(ANewStr: PChar);
    procedure ChangeWidth(aWidth: integer);
    procedure ChangeAlignment(anAlign: integer);
  end;

{--------------------------------------------[ TSegment ]--}

constructor TSegment.Init(AStr: PChar; aWidth, anAlign: integer);
begin
  inherited Init;
  buf:= nil;
  ChangeText(AStr);
  ChangeWidth(aWidth);
  ChangeAlignment(anAlign);
end;

destructor TSegment.Done;
begin
  if buf <> nil then
  begin
    StrDispose(buf);
    buf:= nil;
  end;
  inherited Done;
end;

procedure TSegment.ChangeAlignment(anAlign: integer);
begin
  case anAlign of
    SB_AlignLeft  : alignment:= DT_LEFT;
    SB_AlignCenter: alignment:= DT_CENTER;
    SB_AlignRight : alignment:= DT_RIGHT;
    else
      alignment:= DT_LEFT;
  end;
end;

procedure TSegment.ChangeWidth(aWidth: integer);
begin
  if aWidth < 10 then
    width:= 10
  else
    width:= aWidth;
end;

procedure TSegment.ChangeText(ANewStr: PChar);
begin
  if buf <> nil then
    StrDispose(buf);
  if (ANewStr <> nil) and (strlen(ANewStr) > 0) then
    buf:= StrNew(ANewStr)
  else
  begin
    GetMem(buf, 1);
    buf^:= #0;
  end;
end;

{--------------------------------------------[ TStatusBar ]--}

constructor TStatusBar.Init(aParent: PWindowsObject; aStyle: integer);
begin
  inherited Init(aParent, nil);
  redraw:= false;
  attr.style:= WS_CHILD or WS_VISIBLE or WS_BORDER or WS_GROUP;
  attr.x:= 0; attr.y:= 0;
  attr.h:= 24; attr.w:= 10;
  SetFlags(wb_MDIChild, False);
  segments:= New(PCollection, Init(3, 3));
  segments^.Insert(New(PSegment, Init('', 0, 0)));
  wtPen:= GetStockObject(WHITE_PEN);
  dkGrPen:= CreatePen(PS_SOLID, 0, RGB(128,128,128));
  style:= aStyle;
  cy:= 0;
  font:= 0;
end;

procedure TStatusBar.SetupWindow;
var
  dc    : HDC;
  tm    : TTextMetric;
  r     : TRect;
begin
  inherited SetupWindow;
  dc:= GetDC(HWindow);
  GetTextMetrics(dc, tm);
  ReleaseDC(HWindow, dc);
  cy:= tm.tmHeight + 8;  {- add offset for border }
  GetClientRect(GetParent(HWindow), r);
  MoveWindow(HWindow, r.left - 1, r.bottom - cy, r.right+2, cy, true);
  redraw:= true;
end;

destructor TStatusBar.Done;
begin
  if segments <> nil then
  begin
    Dispose(segments, Done);
    segments:= nil;
  end;
  DeleteObject(dkGrPen);
  inherited done;
end;

procedure TStatusBar.ParentResized(parentR: TRect);
{- this method must be called whenever the parent is resized to ensure
   that the status bar is updated properly. In the parent, capture WM_SIZE
   messages and pass the new parent client rect to this method }
begin
  MoveWindow(HWindow, parentR.left - 1, parentR.bottom - cy + 1, parentR.right+2, cy, true);
end;

procedure TStatusBar.AddSegment(txt: PChar; aWidth, anAlign: integer);
{- add a new segment to the status bar (added to the right)
   Txt is text for segment
   aWidth is width of segment
   anAlign is 0 for left justify, 1 for center, 2 for right }
begin
  segments^.Insert(New(PSegment, Init(txt, aWidth, anAlign)));
  if redraw then
    InvalidateRect(HWindow, nil, true);
end;

procedure TStatusBar.RemoveSegment;
{- remove the last segment in the status bar. If only one segment
   exists, no action will occur }
begin
  if segments^.count > 1 then
  begin
    segments^.AtFree(segments^.count-1);
    if redraw then
      InvalidateRect(HWindow, nil, true);
  end;
end;

procedure TStatusBar.ChangeSegmentStyle(segment: integer; aWidth, anAlign: integer);
{- change attributes of a segment. To ignore a parameter pass in -1.
   i.e., to just change alignment, pass -1 for aWidth }
var
  s     : PSegment;
begin
  if (segment >= 0) and (segment < segments^.count) then
  begin
    s:= segments^.At(segment);
    if aWidth >= 0 then
      s^.ChangeWidth(aWidth);
    if anAlign >= 0 then
      s^.ChangeAlignment(anAlign);
    if redraw then
      InvalidateRect(HWindow, nil, true);
  end;
end;

procedure TStatusBar.ChangeBarStyle(newStyle: integer);
{- change status bar display style }
begin
  if (newStyle >= 0) and (newStyle <= 1) then
  begin
    if newStyle <> style then
    begin
      style:= newStyle;
      if redraw then
        InvalidateRect(HWindow, nil, true);
    end;
  end;
end;

procedure TStatusBar.ChangeSegmentText(segment: integer; txt: PChar);
var
  s : PSegment;
begin
  if (segment >= 0) and (segment < segments^.count) then
  begin
    s:= segments^.At(segment);
    s^.ChangeText(txt);
    if redraw then
      InvalidateRect(HWindow, nil, true);
  end;
end;

function TStatusBar.GetClassName: PChar;
begin
  GetClassName:= 'RCFSBar';
end;

procedure TStatusBar.GetWindowClass(var AWndClass: TWndClass);
begin
  inherited GetWindowClass(AWndClass);
  AWndClass.hbrBackground:= GetStockObject(LTGRAY_BRUSH);
end;

procedure TStatusBar.PaintWordStyle(dc: HDC; r: TRect);
var
  k     : integer;
  s     : PSegment;
  ir    : TRect;
begin
  SelectObject(dc, wtPen);
  MoveTo(dc, r.left, r.top);
  LineTo(dc, r.right, r.top);
  Dec(r.right, 3);
  SetBkColor(dc, RGB(192,192,192));
  for k:= segments^.count - 1 downto 0 do
  begin
    s:= segments^.At(k);
    r.left:= r.right;
    if k = 0 then
      r.left:= 3
    else
      Dec(r.left, s^.width);
    SelectObject(dc, dkGrPen);
      MoveTo(dc, r.left+3, r.bottom-2);
      LineTo(dc, r.left+3, r.top+2);  { left white border }
      LineTo(dc, r.right-3, r.top+2);  { across top }
    SelectObject(dc, wtPen);
      LineTo(dc, r.right-3, r.bottom-2); { right side }
      LineTo(dc, r.left+3, r.bottom-2);  { bottom }

    ir:= r;
    InflateRect(ir, -6, 0);
    DrawText(dc, s^.buf, strlen(s^.buf), ir, DT_VCENTER or DT_SINGLELINE or s^.alignment);

    Dec(r.right, s^.width);
  end;
end;

procedure TStatusBar.PaintBorStyle(dc: HDC; r: TRect);
var
  k     : integer;
  s     : PSegment;
  ir    : TRect;
begin
  SetBkColor(dc, RGB(192,192,192));
  for k:= segments^.count - 1 downto 0 do
  begin
    s:= segments^.At(k);
    r.left:= r.right;
    if k = 0 then
      r.left:= 0
    else
      Dec(r.left, s^.width);

    SelectObject(dc, wtPen);
    MoveTo(dc, r.left, r.bottom-3);
    LineTo(dc, r.left, r.top);  { left white border }
    LineTo(dc, r.right-1, r.top);  { across top }
    MoveTo(dc, r.right-3, r.top+3);
    LineTo(dc, r.right-3, r.bottom-3); { right side }
    LineTo(dc, r.left+1, r.bottom-3);  { bottom }
    SelectObject(dc, dkGrPen);
    MoveTo(dc, r.left+2, r.bottom-4);
    LineTo(dc, r.left+2, r.top+2);   { left }
    LineTo(dc, r.right-3, r.top+2);  { top }
    MoveTo(dc, r.right-1, r.top+1);
    LineTo(dc, r.right-1, r.bottom-1); { right }
    LineTo(dc, r.left, r.bottom-1);    { bottom }

    ir:= r;
    InflateRect(ir, -5, 0);
    Inc(ir.right);
    DrawText(dc, s^.buf, strlen(s^.buf), ir, DT_VCENTER or DT_SINGLELINE or s^.alignment);

    Dec(r.right, s^.width);
  end;
end;

procedure TStatusBar.Paint(dc: HDC; var ps: TPaintStruct);
var
  r        : TRect;
begin
  GetClientRect(HWindow, r);
  if font <> 0 then
    SelectObject(dc, font);
  case style of
    0  : PaintBorStyle(dc, r);
    1  : PaintWordStyle(dc, r);
    else
      PaintWordStyle(dc, r);
  end;
end;

function TStatusBar.GetStyle: integer;
begin
  GetStyle:= style;
end;

procedure TStatusBar.wmSetFont(var msg: TMessage);
var
  dc   : HDC;
  tm   : TTextMetric;
  r    : TRect;
begin
  font:= HFont(msg.wParam);
  dc:= GetDC(HWindow);
  SelectObject(dc, font);
  GetTextMetrics(dc, tm);
  ReleaseDC(HWindow, dc);
  cy:= tm.tmHeight + 8;  {- add offset for border }
  GetClientRect(GetParent(HWindow), r);
  MoveWindow(HWindow, r.left - 1, r.bottom - cy, r.right+2, cy, true);

  DefWndProc(msg);
end;

procedure TStatusBar.NCHitTest(var msg: Tmessage);
begin
  msg.result:= HTTRANSPARENT;
end;



END.

{- rcf}
