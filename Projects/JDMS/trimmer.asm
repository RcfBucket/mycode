.MODEL large, pascal

PUBLIC TrimTrail
PUBLIC TrimLead
LOCALS __

.CODE
;========================================================== TrimTrailCh
;
; function TrimTrail(dest, src: PChar; trimCh: Char; maxLen: integer): PChar;
;
; Trim trailing [ch] from tstr.
;-----------------------------------------------------------------------
;
;     2       4        2        2       4       4
;   | BP | RetAddr | maxLen | trimCh | src   | dest   | <----- Stack Usage
;   `-------------------------------------------------'
;                  ^=+6     ^=+8     ^=+10   ^=+14
;-----------------------------------------------------------------------
TrimTrail          PROC FAR
; Parameters
__dest          equ dword ptr ss:[bp+14]
__src           equ dword ptr ss:[bp+10]
__trimCh        equ byte  ptr ss:[bp+8]
__maxLen        equ word  ptr ss:[bp+6]
__pSize         equ 12

        push    bp              ; save stack
        mov     bp,sp
        push    ds

        cld                     ; direction ->

        les     di,__src        ; get length of source string
        xor     ax,ax           ; look for null
        mov     cx,0ffffh
        repne scasb             ; search for terminating null
        mov     ax, 0ffffh      ;
        sub     ax,cx
        dec     ax
        mov     cx,ax           ; CX = strlen(src)

        mov     bx,__maxLen     ; BX = maxLen
        cmp     cx,bx
        jle     __dontAdjust
        mov     cx, __maxLen

__dontAdjust:
        mov     bx,cx           ; BX = max len of result (dest) string

        lds     si,__src        ; DS:SI -> src
        les     di,__dest       ; ES:DI -> dest

        rep     movsb           ; copy rest of string to result

        mov     al, 0           ; store terminating null in dest
        stosb

        cmp     bx, 0           ; was length zero
        je      __done

        dec     di              ; point DI to last char in string
        dec     di
        mov     cx,bx
        mov     al,__trimCh     ; al = trim character
        std                     ; direction <--
        repe    scasb
        jz      trimmedAll
        inc     cx
trimmedAll:

        les     di,__dest
        add     di,cx

        mov     al,0
        stosb

__done:
        mov     ax, word ptr __Dest       ; setup function return
        mov     dx, word ptr __Dest+2

        pop     ds              ; restore some regs
        pop     bp
        ret     __pSize
TrimTrail       ENDP



;========================================================== TrimLeadCh
;
; function TrimLead(dest, src: PChar; trimCh: Char; maxLen: integer): PChar;
;
;        Trim leading [ch] from tstr.
;--------------------------------------------------------------------
;     2       4         2        2      4       4
;   | BP | RetAddr | maxLen | trimCh | src   | dest   | <----- Stack Usage
;   `-------------------------------------------------'
;                  ^=+6     ^=+8     ^=+10   ^=+14
;--------------------------------------------------------------------
TrimLead        PROC FAR
; Parameters
__dest          equ dword ptr ss:[bp+14]
__src           equ dword ptr ss:[bp+10]
__trimCh        equ byte ptr  ss:[bp+8]
__maxLen        equ word ptr  ss:[bp+6]
__pSize         equ 12

        push    bp
        mov     bp,sp           ; get pointer into stack
        push    ds              ; save Turbo's DS in DX

        cld

        les     di,__src        ; get length of source string
        xor     ax,ax           ; look for null
        mov     cx,0ffffh
        repne scasb             ; search for terminating null
        mov     ax,0ffffh
        sub     ax,cx
        dec     ax
        mov     cx,ax           ; CX = strlen(src)
        mov     bx,ax

        les     di,__src        ; es:di = src
        mov     al,__trimCh
        repe scasb

        les     di,__dest       ; es:di => destination string

        jz      __done          ;

        lds     si,__src
        inc     cx
        sub     bx,cx
        add     si,bx

        mov     cx,__maxLen

__doLoop:
        lodsb
        cmp     al,0
        je      __done
        stosb
        loop __doLoop

__done:
        mov     al,0
        stosb

        mov     ax, word ptr __Dest       ; setup function return
        mov     dx, word ptr __Dest+2

        pop     ds              ; restore some regs
        pop     bp
        ret     __pSize
TrimLead        ENDP

;-------------------------------------------------------------------
        END
