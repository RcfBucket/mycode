(****************************************************************************


uispclog.inc

produced by Borland Resource Workshop

Module ID = $2160 = 8544
*****************************************************************************)

const
  DLG_UISPECLOG                   = 8544;
  DLG_CLEAR                       = 8545;

  IDC_EDSPECID                    = 201;
  IDC_EDCOLLECT                   = 202;
  IDC_EDSOURCE                    = 203;
  IDC_EDWARD                      = 204;
  IDC_EDFREETEXT                  = 205;
  IDC_EDPATID                     = 206;
  IDC_EDLAST                      = 207;
  IDC_EDFIRST                     = 208;
  IDC_LBLSPECID                   = 209;
  IDC_LBLCOLLECT                  = 210;
  IDC_LBLSOURCE                   = 211;
  IDC_LBLWARD                     = 212;
  IDC_LBLPATID                    = 214;
  IDC_LBLLAST                     = 215;
  IDC_LBLFIRST                    = 216;
  IDC_SPECFREE                    = 217;

  IDC_EDSPECUSER1                 = 400;
  IDC_EDSPECUSER2                 = 401;
  IDC_EDSPECUSER3                 = 402;
  IDC_EDSPECUSER4                 = 403;
  IDC_LBLSPECUSER1                = 500;
  IDC_LBLSPECUSER2                = 501;
  IDC_LBLSPECUSER3                = 502;
  IDC_LBLSPECUSER4                = 503;

  IDC_EDPATUSER1                  = 600;
  IDC_EDPATUSER2                  = 601;
  IDC_EDPATUSER3                  = 602;
  IDC_EDPATUSER4                  = 603;
  IDC_LBLPATUSER1                 = 700;
  IDC_LBLPATUSER2                 = 701;
  IDC_LBLPATUSER3                 = 702;
  IDC_LBLPATUSER4                 = 703;

  IDC_MODEPAT                     = 300;
  IDC_MODESPEC                    = 301;

  IDC_GS                          = 101;
  IDC_ISOLATE                     = 102;
  IDC_PATIENT                     = 103;
  IDC_SPECIMEN                    = 104;
  IDC_CLEARPAT                    = 105;
  IDC_CLEARALL                    = 106;
