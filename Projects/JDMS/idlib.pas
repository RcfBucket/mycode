unit IDLib;

{$N+}
{$R-}

{- added calls to RunError when files cannot be opened
   Run time errors :
   110   Cannot open prob file
   111   Cannot Reset prob file
   112   Cannot close prob file
   113   Cannot open org file
   114   Cannot close org file
}

INTERFACE

uses
  DynARR,
  MScan,
  Bits,
  Objects,
  ApiTools,
  DMString,
  DMSErr,
  DMSDebug,
  winprocs;

const
  MaxOrgNum        = 999;    { Max number of unique pre-defined organisms }
  MaxSpclChars     = 200;    { Max number of organism special characteristics descriptions }
  MaxSpclCharsLen  = 20;     { Max length of organism special characteristics descriptions }
  MaxFtn           = 4;      { Max number of organism footnotes per organism }
  MaxMap           = 8;      {  }
  MaxSpc           = 3;      { Max number of organism special characteristics per organism }
  MaxName          = 16;     { characters in org name }
  AtypHigh         = 0.85;   { prob table entry must be >= this number }
  AtypsListed      = 4;
  CutoffDef        = 85.0;   { Default column-1 cutoff value }
  MaxBioLen        = 100;
  MaxOrgList       = 5;      { Max number of orgs that can be in an org list }
  MaxAddTests      = 3;      { Max number of additional tests reported per organism }
  AddTestLen       = 4;      { Length of additional test text }
  MaxAddTestClmns  = 100;    { Number of additional test columns in an add test table }

type
  ProbLine = TDynamicSinglearray;  { 0..MaxBioLen }
  TMatrix = record
    IndivOrg    : integer;
    SpeciesOrg  : integer;
    GroupOrg    : integer;
    CutOff      : single;
    Adjust      : single;
    pTable      : array[0..2] of ProbLine;
    HoldResult  : single;
  end;

  Pchar4 = ^TChar4;
  TChar4 = array[1..4] of char;

  PBioNames = ^TBioNames;
  TBioNames = record
    Buff: TChar4;
    Next: PBioNames;
  end;

  TChemDesc = object
    firstChem   : PBioNames;
    procedure Init;
    procedure Add(var rec: TChar4);
    function Ptr(Index: integer): Pchar4;
    procedure done;
  end;

  ProbDesc = record                 { ORG.PRB record    }
    PrbOrg    : integer;            { PRB table number  }
    NamOrg    : integer;            { Name table number }
    GroupOrg  : integer;            { Name table number for group ID }
    SC        : byte;               { Set/Class         }
    Occur     : byte;               { Fudge factor      }
    Cutoff    : byte;               { Cutoff Freq code  }
    Probs     : TDynamicbytearray;  { 0..MaxBioLen }  { Prob entries }
  end;

  PProbobject = ^TProbobject;
  TProbobject = object
    InitFlag      : boolean;
    ProbName      : string;
    ProbSC        : integer;
    NumProbRecs   : integer;
    ProbLineIndex : array[0..MaxBioLen] OF byte;
    BioType       : string;
    BioMask       : string;      { Skipped tests bitmap }
    BioStrLen     : integer;
    BioLen        : integer;     { The length of the biotype in bits -1 }
    MaxBiot       : integer;     { number of bytes for bitmap }
    ProbBuff      : ProbDesc;    { PRB file buffer }
    ProbFile      : TEXT;        { PRB source file to be read }
    PrevProb      : string[255]; { PRB file buffer - previous line }
    BioChems      : TChemDesc;   { Name for each bit in biotype }
    Matrix        : TDynamicrecordarray;
    function Init(pProbName: string): boolean;
    function ReadProbTable: boolean;
    procedure ScanDataLine(ch: char);
    procedure ScanHeaderLine;
    function RealCutoff(Num: integer): single;
    function NewSC(NewprobSC: integer): boolean;
    function OKBiotype(var BioStr: string): boolean;
    procedure done;
  end;

  TIDList = object    { ID output list }
    NumOrgs     : integer;              { How many were found }
    Slots       : TDynamicintegerarray; { 1..MaxListed }
    ReportOrgs  : TDynamicintegerarray; { 1..MaxListed }
    Freqs       : TDynamicsinglearray;  { 1..MaxListed }
    Probs       : TDynamicsinglearray;  { 1..MaxListed }
    procedure Init;
    procedure Assign(var Source: TIDList);
    procedure CompressGroups;
    procedure CompressSpecies;
    procedure DelEntry(Entry: integer);
    procedure ReSortList;
    procedure GenProbs;
    procedure SetPosNeg(sc: integer; ProbObj: PProbobject; var TestPos, TestNeg, AtypPos, AtypNeg: string);
    procedure InsertOrg(Org: integer);
    procedure Clear;
    procedure done;
  end;

  AtypList = array[1..AtypsListed] of string[4];

  PIdentify = ^TIdentify;
  TIdentify = object(TObject)
    IDList        : TIDList;
    GroupIDList   : TIDList;
    Atypicals     : array[1..AtypsListed] OF string[4];
    AtypicalPos   : String;    { Biochems for this org that are >= 85% }
    AtypicalNeg   : String;    { Biochems for this org that are <= 15% }
    Compress      : boolean;
    TryGroupLevel : boolean;
    InvalidBiotype: boolean;
    TestPos       : string;
    TestNeg       : string;
    constructor Init(ProbName, MICName: string; pCompress, pTryGroup: boolean);
    destructor done; virtual;
    procedure Abort(ErrNum: integer);
    function Identify(BioStr, BioMaskStr: string; ProbSC: integer): boolean;
    procedure MICRefinements(MICBioStr, MICBioMaskStr: string);
    procedure GetAtypicals(Index: integer; var Count: integer);
  end;

  Tchar2 = array[1..2] of char;
  TOrgNames = record
    Name    : string[MaxName];            { organism name }
    NamOrg  : integer;                    { org name number }
    Used    : boolean;                    { true= this org was used as a reference }
    SC      : byte;                       { Organism SetClass }
    OrgFtn  : array[1..MaxFtn] of Tchar2; { Organism footnotes }
    OrgSpc  : array[1..MaxSpc] of byte;   { Special characteristics }
  end;

  TSpCDesc = record                       { SPCHARS.TXT (SPecial Characteristics record) }
    Desc   : string[MaxSpclCharsLen];     { Special characteristics text }
  end;

  NameDesc = record                       { ORGNAM.TXT record }
    OrgNum  : integer;
    SC      : byte;
    Map     : array[1..MaxMap] of byte;
    OrgFtn  : array[1..MaxFtn] of Tchar2;
    OrgSpc  : array[1..MaxSpc] of byte;
    OrgName : array[1..MaxName] of char;
  end;

  TAddTestList = record
    Title    : Array[1..MaxAddTests] of String[AddTestLen];
    Prob     : Array[1..MaxOrgList,1..MaxAddTests] of String[AddTestLen];
  End;

  TAddTestRecord = Record
    OrgNumber     : Integer;
    AddTest       : Array [1..MaxAddTestClmns] of Integer;
  End;

  TAddTestTable = Record
    SetClass      : Integer;
    NumberOfTests : Integer;
    Title         : Array [1..MaxAddTestClmns] of String [AddTestLen];
    OrgRec        : Array [1..MaxOrgList] of TAddTestRecord;
  End;

  TOrgList = array[1..MaxOrgList] of integer;
  TOrgobject = object
    InitFlag  : boolean;
    OrgFile   : Text;
    OrgAdtFile: Text;
    OrgSC     : byte;
    OrgBuff   : NameDesc;
    OrgNames  : TDynamicrecordarray;  { Org names indexed by slot }
    NumNames  : integer;              { Number of org names loaded from ORG.NAM }
    SpclChars : TDynamicRecordArray;  { Special characteristics indexed by item }
    TheTable  : TAddTestTable;
    function  Init: boolean;
    function  NewSC(NewOrgSC: integer): boolean;
    procedure ReadOrg;
    procedure LoadSpclChars;          { Get the special characteristics from SPCHAR.TXT }
    procedure ReportAddTests         { Return OrgAdt detail }
      (IDTooLow:Boolean;
       NumOrgsListed:Integer;
       CurrentOrgIdx:Integer;
       Var OrgList:TOrgList;
       Var AddTestList:TAddTestList);
    procedure ReportSpclChars         { Return OrgSpC detail }
      (OrgNumber:Integer;
       OutputFieldLen:Integer;
       Var SpclCharsText:String);
    Procedure ReportFootnotes         { Return footnote detail }
      (NumOrgsListed:Integer;
       CurrentOrgIdx:Integer;
       OrgList:TOrgList;
       BioType:String;
       Var FootNoteList:String);
    procedure ReadNameTable;
    procedure FindOrgInNameTable(TheOrgNumber :Integer);
    procedure done;
  end;

var
  OrgObject   : TOrgobject;
  ChemProbs   : TProbobject;
  MICProbs    : TProbobject;
  MaxProbRecs : word;    { Max number of PRB table entries }
  MaxListed   : word;    { Number of orgs listed in the ID list }

procedure SetBioBit(var BioStr: string; BitNum: integer);
procedure ClearBioBit(var BioStr: string; BitNum: integer);
function TestBioBit(var BioStr: string; BitNum: integer): boolean;
Function AtypicalBiochemResult(TestIndex:Integer;Biotype,AtypPos,AtypNeg:String):Boolean;

IMPLEMENTATION

const
  OrgName       = 'ORGNAM.TXT'; { organism name file name }
  OrgAdtName    = 'ORGADT.TXT'; { organism name file name }
  OrgPrbName    = 'ORGPRB.TXT'; { organism name file name }
  RapidNeg2     = 201;          { PRB set/class for Rapid Neg table }
  RapidNeg3     = 221;          { PRB set/class for Rapid Neg 3 table }
  IndTstPos     = 35;           { position of indole test from the left side of the table }
  IndInvPos     = 44;           { position of indole invalid from the left side of table }

var
  LN10    : single;
  saveExit: Pointer;

function SVAL(var Line: string; var index: integer): integer;
{-----------------------------------------------}
{ Return the value of the digits in LINE,       }
{ starting at INDEX and stopping at the first   }
{ non-digit.  INDEX is returned pointing to the }
{ delimitter.                                   }
{-----------------------------------------------}
var
  NegFlag: boolean;
  TempVal: integer;
begin
  tempVal:= 0;
  NegFlag:= false;
  while (Index <= LENGTH(Line)) and (Line[Index] = ' ') do
    Inc(Index);
  if (Index <= LENGTH(Line)) and (Line[Index] = '-') then
  begin
    NegFlag:= true;
    Inc(Index);
  end;
  while (Index <= LENGTH(Line)) and (Line[Index] <= '9') and
        (Line[Index] >= '0') do
  begin
    TempVal:= TempVal * 10 + Ord(Line[Index]) - Ord('0');
    Inc(Index);
  end;
  if NegFlag then
    TempVal:= -TempVal;
  SVAL:= TempVal;
end;

procedure SetBioBit(var bioStr: string; BitNum: integer);
begin
  SetBit(bioStr[Length(bioStr) - (BitNum div 3)], BitNum mod 3);
end;

procedure ClearBioBit(var BioStr: string; BitNum: integer);
begin
  ClearBit(BioStr[LENGTH(BioStr)-(BitNum div 3)], BitNum mod 3);
end;

function TestBioBit(var BioStr: string; BitNum: integer): boolean;
begin
  TestBioBit:= TestBit(bioStr[LENGTH(BioStr)-(BitNum div 3)], BitNum mod 3);
end;

Function AtypicalBiochemResult(
{************************************************************************}
{* Figures out which biochems have atypical results.                    *}
{************************************************************************}
  TestIndex : Integer;        { Biochemical test of interest }
  Biotype   : String;         { Biotype being evaluated }
  AtypPos   : String;         { List of possible positive atypical tests }
  AtypNeg   : String):        { List of possible negative atypical tests }
  Boolean;
Begin
  { Compare the test to the biochems that could indicate an atypical status }
  AtypicalBiochemResult :=
    (TestBioBit(BioType,TestIndex)       { Biochem is positive } and
     TestBioBit(AtypNeg,TestIndex))      { Typically the biochem is negative }
    or
    ((Not TestBioBit(BioType,TestIndex)) { Biochem is negative } and
     TestBioBit(AtypPos,TestIndex));     { Typically the biochem is positive }
End;

procedure TChemDesc.Init;
begin
  FirstChem:= nil;
end;

procedure TChemDesc.Add(var Rec: TChar4);
var
  i   : integer;
  p   : PBioNames;
  temp: TBioNames;
begin
  p:= FirstChem;
  if FirstChem = nil then
  begin { Make fake pred for first entry }
    p:= Addr(temp);
    p^.Next:= nil;
  end;
  while p^.Next <> nil do
    p:= p^.Next;
  GetMem(p^.Next, SizeOf(TBioNames));
  p^.Next^.Buff:= Rec;
  p^.Next^.Next:= nil;
  if FirstChem = nil then
    FirstChem:= p^.Next;
end;

function TChemDesc.Ptr(Index: integer): Pchar4;
var
  p: PBioNames;
begin
  p:= FirstChem;
  while (Index > 0) and (p <> nil) do
  begin
    p:= p^.Next;
    Index:= Index - 1 ;
  end;
  if p = nil then
    Ptr:= nil
  else
    Ptr:= Addr(p^.Buff);
end;

procedure TChemDesc.Done;
var
  p: PBioNames;
begin
  while (FirstChem <> nil) do
  begin
    p:= FirstChem^.Next;
    FreeMem(FirstChem, SizeOf(TBioNames));
    FirstChem:= p;
  end;
end;

function TProbobject.RealCutoff(Num: integer): single;
{ Return the cutoff as a 4-byte REAL.    }
begin
  RealCutoff:= EXP(-(Num div 10) * LN10) * (Num mod 10);
end;

function TProbobject.Init(pProbName: string): boolean;
{ Read probability table and convert to REAL. }
var
  Num   : integer;
  Code  : integer;
  Index : integer;
begin
  Init:= false;
  if not InitFlag then
  begin
    ProbName:= pProbName;
    ProbSC:= 0;
    with Matrix do
      Init(MaxProbRecs, SizeOf(TMatrix));
    BioChems.Init;

    {$IFOPT I+}
      {$DEFINE chki}
    {$ENDIF}
    {$I-}

    Assign(ProbFile, ProbName);       { Prob table input file }
    Reset(ProbFile);
    if IOResult <> 0 then
      RunError(110);

    {$IFDEF chki}
      {$I+}
      {$UNDEF chki}
    {$ENDIF}

    InitFlag:= true;
  end;
  Init:= true;
end;

function TProbobject.NewSC(NewProbSC: integer): boolean;
var
  i: integer;
begin
  NewSC:= false;
  if NewProbSC <> ProbSC then
  begin
    if ProbSC <> 0 then
    begin
      ProbBuff.Probs.Done;
      for i:= 0 to NumProbRecs do
        with TMatrix(Matrix.P^[i]^) do
        begin
          if pTable[0].P <> nil then ptable[0].Done;
          if pTable[1].P <> nil then ptable[1].Done;
          if pTable[2].P <> nil then ptable[2].Done;
        end;
    end;
    ProbSC:= NewProbSC;
    if not ReadProbTable then
      exit;
  end;
  NewSC:= true;
end;

function TProbobject.ReadProbTable: boolean;
{ Read a line from the prob file and convert the text into the binary format.}
var
  ch      : char;
  DataLine: boolean;
  Idx     : integer;
  Result  : single;
  Prob    : integer;
label  900;
begin
  ReadProbTable:= false;

  {$IFOPT I+}
    {$DEFINE chki}
  {$ENDIF}
  {$I-}

  Reset(ProbFile);
  if IOResult <> 0 then
    RunError(111);
  BioLen:= 0;
  repeat              { Scan prb table till first match }
    DataLine:= false;
    repeat
      Read(ProbFile, ch);
      if ch = '*' then { Comment line }
      begin
        ReadLn(ProbFile);
      end { ch = '*' }
      else if ch = '>' then { Biochem names }
      begin
        ScanHeaderLine;
      end { ch = '>' }
      else
      begin   { Probabilities }
        DataLine:= true;
        if BioLen = 0 then
          exit;
        if BioLen > MaxBioLen then
          RunError(0);
        ProbBuff.Probs.Init(BioLen);
        ScanDataLine(ch);
      end;
    until (DataLine) or EOF(ProbFile);
  until (ProbBuff.SC = ProbSC) or EOF(ProbFile);
  if ProbBuff.SC <> ProbSC then
    exit;

  NumProbRecs:= 0;     { Read in prob table }
  repeat
    with TMatrix(Matrix.P^[NumProbRecs]^) do
    begin
      CutOff:= RealCutoff(ProbBuff.Cutoff);
      Adjust:= ProbBuff.Occur / 100.0;
      IndivOrg:= ProbBuff.PrbOrg;
      SpeciesOrg:= ProbBuff.NamOrg;
      GroupOrg:= ProbBuff.GroupOrg;
      ptable[0].Init(BioLen);
      ptable[1].Init(BioLen);
      ptable[2].Init(BioLen);
      for Prob:= 0 to BioLen do
      begin
        case ProbBuff.Probs.P^[Prob] of
          101: Result:= 0.0001;
          105: Result:= 0.005;
          109: Result:= 0.999;
          111: Result:= 0.001;
          115: Result:= 0.015;
          else Result:= ProbBuff.Probs.P^[Prob] / 100.0;
        end; { CASE }
        pTable[2].P^[BioLen-Prob]:= 1.0;
        pTable[1].P^[BioLen-Prob]:= Result;
        pTable[0].P^[BioLen-Prob]:= 1.0 - Result;
      end; { for Prob }
    end; { with Matrix }
    Inc(NumProbRecs);
    if NumProbRecs > MaxProbRecs then
      exit;
    if EOF(ProbFile) then
      goto 900;
    DataLine:= false;
    repeat
      Read(ProbFile, ch);
      if (ch = '*') or (ch = '>') then
        ReadLn(ProbFile)
      else
      begin
        ScanDataLine(ch);
        DataLine:= true;
      end;
    until DataLine or EOF(ProbFile);
  until (ProbBuff.SC <> ProbSC) or EOF(ProbFile);
900:
  Dec(NumProbRecs);
  ReadProbTable:= true;

  {$IFDEF chki}
    {$I+}
    {$UNDEF chki}
  {$ENDIF}
end;  { ReadProb }

procedure TProbobject.ScanDataLine(ch: char);
var
  FirstNum  : integer;
  Prob      : integer;
begin
  with ProbBuff do
  begin
    FirstNum:= 0;
    while (ch < '0') or (ch > '9') do
    begin
      Read(ProbFile, ch);
      if EOLN(ProbFile) or EOF(ProbFile) then
        RunError(0);
    end;
    repeat
      FirstNum:= (FirstNum * 10) + Ord(ch) - Ord('0');
      Read(ProbFile, ch);
    until (ch < '0') or (ch > '9') or EOLN(ProbFile) or EOF(ProbFile);
    PrbOrg:= FirstNum;
    Read(ProbFile, NamOrg);
    Read(ProbFile, GroupOrg);
    Read(ProbFile, SC);
    Read(ProbFile, Occur);
    Read(ProbFile, Cutoff);
    for Prob:= 0 to BioLen do
      Read(ProbFile, ProbBuff.Probs.P^[Prob]);
    ReadLn(ProbFile);
  end;
end;  { ScanDataLine }

procedure TProbobject.ScanHeaderLine;
var
  Idx     : integer;
  BioSize : integer;
  ch      : char;
  BiochemBuff: TChar4;
begin
  BioChems.Done;  { Clear prev list and init }
  BioChems.Init;
  repeat
    Read(ProbFile, ch);
  until EOLN(ProbFile) or EOF(ProbFile) or (ch > ' ');
  BioLen:= 0;
  while not (EOLN(ProbFile) or EOF(ProbFile)) do
  begin
    for Idx:= 1 to 4 do      { Copy a name }
    begin
      BiochemBuff[Idx]:= ch;
      if EOLN(ProbFile) or EOF(ProbFile) then
        ch:= ' '
      else
        Read(Probfile, ch);
    end;
    Inc(BioLen);
    BioChems.Add(BiochemBuff);
  end;
  BioSize:= ((BioLen + 2) div 3) * 3;
  BioLen:= BioSize - 1;
  BioStrLen:= BioSize div 3;
  MaxBiot:= ((BioSize+7) div 8) - 1;
  ReadLn(ProbFile);
end;

function TProbobject.OKBiotype(var BioStr: string): boolean;
var
  i : integer;
begin
  OKBiotype:= false;
  repeat
    i:= POS('-', BioStr);
    if i <> 0 then
      SYSTEM.Delete(BioStr, i, 1);
  until (i = 0);
  if Length(BioStr) <> BioStrLen then
    exit;
  for i:= 1 to LENGTH(BioStr) do
    if (BioStr[i] < '0') or (BioStr[i] > '7') then
      exit;
  OKBiotype:= true;
end;

procedure TProbobject.Done;
var
  i: integer;
begin
  if not InitFlag then
    exit;
  if ProbSC <> 0 then
  begin
    ProbBuff.Probs.Done;
    for i:= 0 to NumProbRecs do with TMatrix(Matrix.P^[i]^) do
    begin
      if pTable[0].P <> nil then ptable[0].Done;
      if pTable[1].P <> nil then ptable[1].Done;
      if pTable[2].P <> nil then ptable[2].Done;
    end;
  end;
  Matrix.Done;
  Biochems.Done;
  CLOSE(ProbFile);
  InitFlag:= false;
  if IOResult <> 0 then
    RunError(112);
end;

{*******************************************************************************
   TOrgObject.LoadSpclChars - 6/23/98 9:43am
   Creates a table and fills it with special characteristics
********************************************************************************}
Procedure TOrgObject.LoadSpclChars;
Const
  SpclCharsName = 'SPCHAR.TXT';   { Organism special characteristics file name }
Var
  SpclCharsFile      : Text;      { Pointer to SPCHAR.TXT }
  SpclCharsText      : String;    { Line of SPCHAR.TXT }
  SpclCharsCount     : Integer;   { Index into special chars table ref'd in ORGNAM.TXT, TOrgObject.TOrgNames.OrgSpc }
  EndOfSpclCharsText : Integer;   { Length of line of special characteristics text }
Begin
    { Create the table for special characteristics }
    With SpclChars Do
      Init(MaxSpclChars, SizeOf(TSpCDesc));

    { Open the special charateristics data file }
    Assign(SpclCharsFile,SpclCharsName);
    Reset(SpclCharsFile);

    { Load the special characteristics into memory }
    SpclCharsCount := 0;
    While ( not EOF(SpclCharsFile) ) Do
    Begin
      Readln(SpclCharsFile,SpclCharsText);
      If ( SpclCharsText[1] <> '*' ) Then
      Begin
        Inc(SpclCharsCount);
        TSpCDesc(SpclChars.P^[SpclCharsCount]^).Desc := '';
        EndOfSpclCharsText := Pos('/',SpclCharsText)-1;
        If ( EndOfSpclCharsText<0 ) Then
          EndOfSpclCharsText := 0;
        If ( EndOfSpclCharsText>MaxSpclCharsLen ) Then
          EndOfSpclCharsText := MaxSpclCharsLen;
        Move(SpclCharsText[1],TSpCDesc(SpclChars.P^[SpclCharsCount]^).Desc[1],EndOfSpclCharsText);
        TSpCDesc(SpclChars.P^[SpclCharsCount]^).Desc[0] := Chr(EndOfSpclCharsText);
      End;
    End;

    { We're done so close the special characteristics data file }
    Close(SpclCharsFile);
End; {TOrgObject.LoadSpclChars}

{*******************************************************************************
   TOrgObject.ReportSpclChars - 6/23/98 1:21pm

********************************************************************************}
Procedure TOrgObject.ReportSpclChars(
  OrgNumber         : Integer;  { Organism of interest }
  OutputFieldLen    : Integer;  { Length of returned detail string }
  Var SpclCharsText : String);  { Return the text detail here }
Var
  OrgSpcCount       : Integer;  { Indexes the special charateristics array }
Begin
  { Define an initial condition }
  SpclCharsText := '';

  { Get the texts and concat them together }
  For OrgSpcCount := 1 To MaxSpc Do
    SpclCharsText :=
      Concat(SpclCharsText,
      TSpCDesc(SpclChars.P^[
        ord(TOrgNames(OrgNames.P^[ OrgNumber ]^).OrgSpc[OrgSpcCount])
      ]^).Desc);

  If ( Ord(SpclCharsText[0]) > OutputFieldLen ) Then
    SpclCharsText[0] := Chr(OutputFieldLen);
End; {TOrgObject.ReportSpclChars}

{*******************************************************************************
   TOrgObject.ReportAddTests - 6/23/98 2:34pm

********************************************************************************}
Procedure TOrgObject.ReportAddTests(
  IDTooLow          : Boolean;         { Indicates additional tests are needed }
  NumOrgsListed     : Integer;         { Number of orgs returned from identify }
  CurrentOrgIdx     : Integer;         { Pointer to org being evaluated }
  Var OrgList       : TOrgList;        { List of org numbers returned from identify }
  Var AddTestList   : TAddTestList);   { The returned additional tests for org being evaluated }

Var
  OrgSetClass : Integer;
  j           : Integer;
  i           : Integer;

  Function ExtractStr(
    Var DataLine  : String;
    Var DataIndex : Integer;
    ReturnStrLen  : Integer)
    :String;
  Var
    i         : Integer;
    StartPtr  : Integer;
    EndPtr    : Integer;
    ReturnStr : String;
  Begin
    {Init the return string}
    FillChar(ReturnStr,ReturnStrLen,' ');
    ReturnStr[0] := Chr(ReturnStrLen);

    While (DataLine[DataIndex] = ' ') and (DataIndex < Ord(DataLine[0])) do
      Inc(DataIndex);
    StartPtr := DataIndex;
    While (DataLine[DataIndex] <> ' ') and (DataIndex < Ord(DataLine[0])) do
      Inc(DataIndex);
    EndPtr := (DataIndex-1);
    If StartPtr<=EndPtr then begin
      If (EndPtr-StartPtr+1) > ReturnStrLen then
        EndPtr := StartPtr + (ReturnStrLen-1);
      For i := StartPtr to EndPtr do
        ReturnStr[i-StartPtr+1] := DataLine[i];
      For i := (EndPtr-StartPtr+2) to ReturnStrLen do
        ReturnStr[i] := ' ';
      ReturnStr[0] := chr(ReturnStrLen);
    End;
    ExtractStr := ReturnStr;
  End;

  Function ExtractInt(
    Var DataLine  : String;
    Var DataIndex : Integer;
    DataWidth     : Integer)
    :Integer;
  Var
    DataStr     : String;
    ReturnData  : Integer;
    i           : Integer;
  Begin
    DataStr := ExtractStr(DataLine,DataIndex,DataWidth);
    ReturnData := 0;
    For i := 1 to DataWidth do
      If (Ord(DataStr[i]) >= Ord('0')) and (Ord(DataStr[i]) <= Ord('9')) then
        ReturnData := ReturnData*10 + Ord(DataStr[i])-Ord('0');
    ExtractInt := ReturnData;
  End;

  Function GetTable(
    SetClass     : Integer;
    Var TheTable : TAddTestTable;
    Var OrgList  : TOrgList;
    NumOrgsListed: Integer)
    :Boolean;
  Var
    TableFound    : Boolean;
    DataLine      : String;
    DataIndex     : Integer;
    FoundSetClass : Integer;
    FoundOrg      : Integer;
    i             : Integer;
    j             : Integer;
  Begin
{$I-}
    GetTable := False;
    Reset(OrgAdtFile);
    TableFound := False;
    While Not (EOF(OrgAdtFile) or TableFound or (IOResult<>0)) do begin
      DataLine := ' ';
      While (Not EOF(OrgAdtFile)) and (DataLine[1]<>'>') do
        ReadLn(OrgAdtFile,DataLine);
      If DataLine[1]='>' then begin
        DataIndex := 2;
        FoundSetClass := ExtractInt(DataLine,DataIndex,AddTestLen);
        If SetClass = FoundSetClass then begin
          GetTable := True;
          TheTable.SetClass := FoundSetClass;
          TheTable.NumberOfTests := ExtractInt(DataLine,DataIndex,AddTestLen);
          For i := 1 to TheTable.NumberOfTests do
            TheTable.Title[i] := ExtractStr(DataLine,DataIndex,AddTestLen);
          DataLine := ' ';
          While (Not EOF(OrgAdtFile)) and ((DataLine[1]<>'*') and (DataLine[1]<>'>')) do begin
            DataIndex := 1;
            ReadLn(OrgAdtFile,DataLine);
            FoundOrg := ExtractInt(DataLine,DataIndex,AddTestLen);
            For i := 1 to NumOrgsListed do
              If FoundOrg = OrgList[i] then begin
                TheTable.OrgRec[i].OrgNumber := FoundOrg;
                For j := 1 to TheTable.NumberOfTests do
                  TheTable.OrgRec[i].AddTest[j] := ExtractInt(DataLine,DataIndex,AddTestLen);
              End;
          End;
        End;
      End;
    End;
{$I+}
  End;

  Procedure SelectAddTests(
    NumOrgsListed    : Integer;
    Var AddTestList  : TAddTestList;
    Var TheTable     : TAddTestTable);
  Const
    NotApplFlag      = 128;
    Pg               = 'Pg  ';
    MaxComparisons   = 3;
    N_Flag           = 129;
    PO_Flag          = 133;
    PE_Flag          = 134;
    N_Text           = 'N   ';
    PO_Text          = 'PO  ';
    PE_Text          = 'PE  ';
    NA_Text          = '    ';
  Var
    AddTestDiff      : Array [1..MaxComparisons,1..MaxAddTestClmns] of Integer;
    AddTestSelected  : Array [1..MaxAddTestClmns] of Boolean;
    CompIdx          : Integer;
    NumOrgsToEval    : Integer;
    LastDiff         : Integer;
    LastTest         : Integer;
    TestFound        : Boolean;
    I                : Integer;
    J                : Integer;
    K                : Integer;
    M                : Integer;
  Begin
    {First set all differences to zero}
    For i := 1 to MaxComparisons do
      For j := 1 to MaxAddTestClmns do
        AddTestDiff[i,j] := 0;
    For j := 1 to MaxAddTestClmns do
      AddTestSelected[j] := False;

    CompIdx := 1;
    NumOrgsToEval := NumOrgsListed;
    If NumOrgsToEval > MaxComparisons then
      NumOrgsToEval := MaxComparisons;
    For i := 1 to (NumOrgsToEval-1) do
      For j := (i+1) to NumOrgsToEval do
      Begin
        For k := 1 to TheTable.NumberOfTests do
        Begin
          If Not ((TheTable.OrgRec[i].AddTest[k] = NotApplFlag) or
                  (TheTable.OrgRec[j].AddTest[k] = NotApplFlag)) then
          Begin
            If (TheTable.OrgRec[i].AddTest[k] > NotApplFlag) then
            Begin
              If (TheTable.OrgRec[i].AddTest[k] <> TheTable.OrgRec[j].AddTest[k]) then
              Begin
                If (Pos(TheTable.Title[k],Pg)<>0) then
                  AddTestDiff[CompIdx,k] := 90
                Else
                  AddTestDiff[CompIdx,k] := 65;
              End;
            End
            Else
            Begin
              AddTestDiff[CompIdx,k] := TheTable.OrgRec[i].AddTest[k]-TheTable.OrgRec[j].AddTest[k];
              If Abs(AddTestDiff[CompIdx,k]) < 65 then
                AddTestDiff[CompIdx,k] := 0;
            End;
          End;
        End;
        CompIdx := CompIdx + 1;
      End;

    For i := 1 to (CompIdx-1) do
    Begin
      LastDiff := 0;
      LastTest := 0;
      TestFound := False;
      For j := 1 to TheTable.NumberOfTests do
        If ((Abs(AddTestDiff[i,j]) >= 65) and
            (AddTestSelected[j] = False) and
            (TestFound = False)) or
           ((Abs(AddTestDiff[i,j]) > LastDiff) and
            (AddTestSelected[j] = False) and
            (TestFound = True)) then
        Begin
          AddTestSelected[j] := True;
          LastDiff := Abs(AddTestDiff[i,j]);
          AddTestList.Title[i] := TheTable.Title[j];
          For k:= 1 to NumOrgsToEval do
          Begin
            Case TheTable.OrgRec[k].AddTest[j] of
              N_Flag      : AddTestList.Prob[k,i] := N_Text;
              PO_Flag     : AddTestList.Prob[k,i] := PO_Text;
              PE_Flag     : AddTestList.Prob[k,i] := PE_Text;
              NotApplFlag : AddTestList.Prob[k,i] := NA_Text;
              Else
                Str(TheTable.OrgRec[k].AddTest[j],AddTestList.Prob[k,i]);
            End;
            For m := (Ord(AddTestList.Prob[k,i][0])+1) to AddTestLen do
              AddTestList.Prob[k,i][m] := ' ';
            AddTestList.Prob[k,i][0] := Chr(AddTestLen);
          End;
          If TestFound then
            AddTestSelected[LastTest] := False;
          TestFound := True;
          LastTest := j;
        End;
    End;
  End;

{ReportAddTests}
Begin
  { Define default return values for titles and all add tests }
  For i := 1 To MaxAddTests Do
  Begin
    FillChar(AddTestList.Title[i][1],AddTestLen,' ');
    AddTestList.Title[i][0] := Chr(AddTestLen);
  End;
  For i := 1 to MaxOrgList do
    For j := 1 to MaxAddTests do
    Begin
      FillChar(AddTestList.Prob[i,j][1],AddTestLen,' ');
      AddTestList.Prob[i,j][0] := Chr(AddTestLen);
    End;

  { Only look for additional tests if there are organisms defined }
  If (NumOrgsListed>0) and IDTooLow Then
  Begin
    { What table are we looking for, it is determined by the set class }
    OrgSetClass := Ord(TOrgNames(OrgNames.P^[ OrgList[1] ]^).SC);
    If ( GetTable(OrgSetClass,TheTable,OrgList,NumOrgsListed) ) Then
    Begin
      SelectAddTests(NumOrgsListed,AddTestList,TheTable);
    End;
  End;
End;

{*******************************************************************************
   TOrgObject.ReportFootnotes - 6/23/98 2:34pm

********************************************************************************}
Procedure TOrgObject.ReportFootnotes(
  NumOrgsListed     : Integer;         { Number of orgs returned from identify }
  CurrentOrgIdx     : Integer;         { Pointer to org being evaluated }
  OrgList           : TOrgList;        { List of org numbers returned from identify }
  BioType           : String;          { CurrentBiotype }
  Var FootNoteList  : String);         { The returned footnotes for org being evaluated }
Const
  GramNeg         = 1;                 { organism set }
  GramPos         = 2;                 { organism set }
  Anaerobe        = 3;                 { organism set }
  HNID            = 4;                 { organism set }
  Yeast           = 6;                 { organism set }
  Misc            = 8;                 { organism set }
  Fermenter       = 1;                 { organism class }
  NonFermenter    = 2;                 { organism class }
  Column1FootNote = ' ';               { Matches column 1 footnote }
  Column2FootNote = '2';               { Matches column 2 footnote }
  NonFermOxNeg    = '00000000';        { Specific biotype for footnote E }
  NonFermOxPos    = '00000002';        { Specific biotype for footnote E }
Type
  TOrgFootNotes = record               { List of info for }
    OrgSet   : Integer;
    OrgClass : Integer;
    FootNote : Array [1..MaxFtn] of char;
    Column   : Array [1..MaxFtn] of char;
  End;
Var
  OrgsFootNotes : Array [1..MaxOrgList] of TOrgFootNotes;
  MatchC1       : Boolean;
  MatchC2       : Boolean;
  Matches       : Integer;
  I             : Integer;
  J             : Integer;
  OrgListCount  : Integer;
  FootNoteCount : Integer;
  ThisFootNote  : Char;
  ThisColumn    : Char;
Begin
  FootNoteList := '';

  If ( NumOrgsListed>0 ) Then Begin
    For OrgListCount := 1 To NumOrgsListed Do Begin
      OrgsFootNotes[OrgListCount].OrgSet :=
        Ord(TOrgNames(OrgNames.P^[ OrgList[OrgListCount] ]^).SC) div 10;
      OrgsFootNotes[OrgListCount].OrgClass :=
        Ord(TOrgNames(OrgNames.P^[ OrgList[OrgListCount] ]^).SC) mod 10;
      For FootNoteCount := 1 To MaxFtn Do Begin
        OrgsFootNotes[OrgListCount].FootNote[FootNoteCount] :=
          TOrgNames(OrgNames.P^[ OrgList[OrgListCount] ]^).OrgFtn[FootNoteCount][1];
        OrgsFootNotes[OrgListCount].Column[FootNoteCount] :=
          TOrgNames(OrgNames.P^[ OrgList[OrgListCount] ]^).OrgFtn[FootNoteCount][2];
      End;
    End;

    For FootNoteCount := 1 To MaxFtn Do Begin
      ThisFootNote := OrgsFootNotes[CurrentOrgIdx].FootNote[FootNoteCount];
      ThisColumn   := OrgsFootNotes[CurrentOrgIdx].Column[FootNoteCount];
      Case ( OrgsFootNotes[CurrentOrgIdx].OrgSet ) Of
        GramNeg  : Begin
          Case ( ThisFootNote ) Of
            { Type 1 } 'F','G' : Begin
              Matches := 0;
              For i := 1 To NumOrgsListed Do
                For j := 1 To MaxFtn Do
                  If ( ThisFootNote = OrgsFootNotes[i].FootNote[j] ) Then
                    Inc(Matches);
              If ( Matches >= 2 ) Then
                FootNoteList := Concat(FootNoteList,ThisFootNote,' ');
            End;
            { Type 2 } 'A','B','C','P' : Begin
              MatchC1 := False;
              MatchC2 := False;
              For i := 1 To NumOrgsListed Do
                For j := 1 To MaxFtn Do Begin
                  MatchC1 := MatchC1 or
                             (( ThisFootNote = OrgsFootNotes[i].FootNote[j] ) and
                              ( OrgsFootNotes[i].Column[j] = Column1FootNote ));
                  MatchC2 := MatchC2 or
                             (( ThisFootNote = OrgsFootNotes[i].FootNote[j] ) and
                              ( OrgsFootNotes[i].Column[j] = Column2FootNote ));
                End;
              If ( MatchC1 and MatchC2 ) Then
                FootNoteList := Concat(FootNoteList,ThisFootNote,' ');
            End;
            { Hardcoded } 'E' : Begin
              If ( OrgsFootNotes[CurrentOrgIdx].OrgClass = NonFermenter ) and
                 (( Pos(NonFermOxNeg,BioType)>0 ) or ( Pos(NonFermOxPos,BioType)>0 ))
              Then
                FootNoteList := Concat(FootNoteList,ThisFootNote,' ');
            End;
            { Type 0: 'D','E','H','I','J','K','L','M','N','O','Q','R','S' } Else
              FootNoteList := Concat(FootNoteList,ThisFootNote,' ');
          End;
        End;

        GramPos  : Begin
          Case ( ThisFootNote ) Of
            '~' :; { type 0 footnotes have not yet been defined }
            { Type 0: 'A','B','C','D' } Else
              FootNoteList := Concat(FootNoteList,ThisFootNote,' ');
          End;
        End;

        Anaerobe : Begin
          Case ( ThisFootNote ) Of
            '~' :; { type 0 footnotes have not yet been defined }
            { Type 0: 'A','B','C','D','E','F' } Else
              FootNoteList := Concat(FootNoteList,ThisFootNote,' ');
          End;
        End;

        HNID     : Begin
          Case ( ThisFootNote ) Of
            '~' :; { type 0 footnotes have not yet been defined }
            { Type 0: 'A','B','D' } Else
              FootNoteList := Concat(FootNoteList,ThisFootNote,' ');
          End;
        End;

        Yeast    : Begin
          Case ( ThisFootNote ) Of
            '~' :; { type 0 footnotes have not yet been defined }
            { Type 0: 'A','B','C','E','F','G','H','I','J' } Else
              FootNoteList := Concat(FootNoteList,ThisFootNote,' ');
          End;
        End;
      End;
    End;
  End;
  While ( Ord(FootNoteList[0])<(2*MaxFtn) ) Do
    FootNoteList := Concat(FootNoteList,' ');
End; {TOrgObject}

function TOrgobject.Init: boolean;
var
  OldFileMode        : integer;
begin
  Init:= false;
  if not InitFlag then
  begin
    OrgSC:= 0;
    NumNames:= 0;        { Number of org names loaded from name table }
    with OrgNames do     { Create the table for org names }
      Init(MaxOrgNum, SizeOf(TOrgNames));
    with TOrgNames(OrgNames.P^[0]^) do
    begin
      FillChar(Name[1], MaxName, '*');
      Name[0]:= CHR(MaxName);
    end;

    LoadSpclChars;  { Make a list of special characteristics }

    OldFileMode:= SYSTEM.FileMode;
    SYSTEM.FileMode:= 0;           { Read-only }
    {$IFOPT I+}
      {$DEFINE chki}
    {$ENDIF}
    {$I-}
    Assign(OrgFile, OrgName);   { Org name table }
    Reset(OrgFile);
    if IOResult <> 0 then
      RunError(113);

    {Open the additional test table}
    Assign(OrgAdtFile, OrgAdtName);
    Reset(OrgAdtFile);

    SYSTEM.FileMode:= OldFileMode;
    if IOResult <> 0 then
      RunError(113);
    {$IFDEF chki}
      {$I+}
      {$UNDEF chki}
    {$ENDIF}
    InitFlag:= true;
  end;
  Init:= true;
end;

function TOrgobject.NewSC(NewOrgSC: integer): boolean;
var
  i : integer;
begin
  NewSC:= false;
  if NewOrgSC <> OrgSC then
  begin
    if OrgSC <> 0 then
      NumNames:= 0;
    OrgSC:= NewOrgSC;
    ReadNameTable;
  end;
  NewSC:= true;
end;

procedure TOrgobject.ReadOrg;
{ Read a line from the org names text file and return the values in the
  namedesc variable. Only return the values BATCHID needs. }
Const
  StartOfFootnotes = 15;
  LengthOfFootnote = 2;
var
  Line  : string[255];
  Index : integer;
  Junk  : integer;
  i     : integer;
  j     : integer;
begin
  if EOF(OrgFile) then
    exit;
  ReadLn(OrgFile, Line);
  If ( Line[1]<>'*' ) Then
  Begin
    Index:= 1;
    OrgBuff.OrgNum:= SVAL(Line, Index);
    OrgBuff.SC:= byte(SVAL(Line, Index));
    Junk:= SVAL(Line, Index);
    Junk:= SVAL(Line, Index);      { Throw away maps }
    for i:= 1 to MaxFtn do  {Pick up the organism footnotes}
      For j := 1 To LengthOfFootnote Do
        OrgBuff.OrgFtn[i][j] :=Line[StartOfFootnotes+((i-1)*2)+(j-1)];
    Inc(Index, 2 * MaxFtn);     { Skip the Tchar2's }
    for i:= 1 to MaxSpc do  {Pick up the organism special characteristics}
      OrgBuff.OrgSpc[i] := Byte(SVAL(Line, Index));
(*    Inc(Index);*)
    while (Index <= LENGTH(Line)) and (Line[Index] = ' ') do
      Inc(Index);
    Junk:= 1;
    while (Index <= LENGTH(Line)) and (Junk <= MaxName) do
    begin
      OrgBuff.OrgName[Junk]:= Line[Index];
      Inc(Junk);
      Inc(Index);
    end;
  End;
end;  { ReadOrg }

procedure TOrgobject.ReadNameTable;
{ Read in all the organism names for the set/class we're working on.
  Make Xref into list.   }
var
  Slot  : integer;  { slot in the organism names storage array }
begin
  Slot:= NumNames + 1;
  Reset(OrgFile);
  while not EOF(OrgFile) do
  begin
    ReadOrg;
    if (OrgBuff.SC = OrgSC) or (((OrgSC mod 10) = 0) and
       ((OrgBuff.SC div 10) = (OrgSC div 10))) then
    begin
      with TOrgNames(OrgNames.P^[OrgBuff.OrgNum]^) do
      begin
        Move(OrgBuff.OrgName[1], Name[1], MaxName);
        Name[0]:= CHR(MaxName);
        NamOrg:= OrgBuff.OrgNum;
        SC:= OrgBuff.SC;
        Move(OrgBuff.OrgFtn,OrgFtn,SizeOf(OrgFtn));
        Move(OrgBuff.OrgSpc,OrgSpc,SizeOf(OrgSpc));
      end;
      Inc(Slot);
    end;
  end;
  NumNames:= Slot - 1;
end;  { ReadNameTable }

procedure TOrgobject.FindOrgInNameTable(TheOrgNumber :Integer);
{ Read a specific organism from the ORGNAM.TXT file }
begin
  Reset(OrgFile);
  while not EOF(OrgFile) and (OrgBuff.OrgNum <> TheOrgNumber) do
    ReadOrg;
end;  { ReadNameTable }

procedure TOrgobject.Done;
begin
  OrgNames.Done;
  SpclChars.Done;
  Close(OrgFile);
  Close(OrgAdtFile);
  InitFlag:= false;
  if IOResult <> 0 then
    RunError(114);
end;

procedure TIDList.Init;
begin
  NumOrgs:= 0;
  Slots.Init(MaxListed);
  ReportOrgs.Init(MaxListed);
  Freqs.Init(MaxListed);
  Probs.Init(MaxListed);
end;

procedure TIDList.InsertOrg(Org: integer);
{  Save the organism in the ID list array }
var
  Indx    : integer;
  Freq    : single;
  Found   : boolean;    { true when item has been inserted }
  ListSize: byte;
begin
  Found:= false;
  if NumOrgs < MaxListed then
    Inc(NumOrgs);
  ListSize:= NumOrgs;
  Freq:= TMatrix(ChemProbs.Matrix.P^[Org]^).HoldResult
         * TMatrix(ChemProbs.Matrix.P^[Org]^).Adjust;
  Indx:= 1;
  while (Indx <= ListSize) and not Found do
  begin
    if (Freq > Freqs.P^[Indx]) or (ListSize = 1) then
    begin       { Insert it into the list here }
      Found:= true;
      if (ListSize > 1) and (Indx < MaxListed) then
      begin
        { notE these moves MUST be move rights }
        Move(Slots.P^[Indx], Slots.P^[Indx+1],
            (ListSize-Indx) * SizeOf(Slots.P^[1]));
        Move(ReportOrgs.P^[Indx], ReportOrgs.P^[Indx+1],
            (ListSize-Indx) * SizeOf(ReportOrgs.P^[1]));
        Move(Freqs.P^[Indx], Freqs.P^[Indx+1],
            (ListSize-Indx) * SizeOf(Freqs.P^[1]));
      end;
      Freqs.P^[Indx]:= Freq; { Get the likelyhood     }
      Slots.P^[Indx]:= Org;  { Save PRB slot number   }
      ReportOrgs.P^[Indx]:= TMatrix(ChemProbs.Matrix.P^[Org]^).SpeciesOrg;
    end;
    Inc(Indx);
  end;
end;  { InsertOrg }

procedure TIDList.SetPosNeg(sc : integer; ProbObj: PProbobject;
                            var TestPos, TestNeg, AtypPos, AtypNeg: string);
var
  Indx, Prob  : integer;
begin
  for Indx:= 1 to NumOrgs do
    with ProbObj^ do
    begin
      for Prob:= 0 to BioLen do with TMatrix(Matrix.P^[Slots.P^[Indx]]^) do
      begin
        if (sc = RapidNeg2) or (sc = RapidNeg3) then
          if pTable[1].P^[Prob] >= 0.75 then
            SetBioBit(TestPos, Prob)
          else
            SetBioBit(TestNeg, Prob)
        else if pTable[1].P^[Prob] >= 0.50 then
          SetBioBit(TestPos, Prob)
        else
          SetBioBit(TestNeg, Prob);
        If ( Indx = 1 ) Then Begin { Atypicals only apply to the first org }
          if pTable[1].P^[Prob] >= 0.85 then
            SetBioBit(AtypPos, Prob)
          else
            if pTable[1].P^[Prob] <= 0.15 then
              SetBioBit(AtypNeg, Prob);
        End;
      end;
    end;
end;

procedure TIDList.GenProbs;
{ Run through the org list and calculate the normalized probabilities for printing }
var
  Indx    : integer;
  RawSum  : single;
begin
  RawSum:= 0.0;
  for Indx:= 1 to NumOrgs do   { Add up the raw probs }
    RawSum:= RawSum + Freqs.P^[Indx];
  for Indx:= NumOrgs downto 1 do  { Compute the normalized probs }
  begin
    Probs.P^[Indx]:= (Freqs.P^[Indx] / RawSum) * 100.0;
    if Probs.P^[Indx] >= 99.990 then
      Probs.P^[Indx]:= 99.990; { 100% is impossible }
    if Probs.P^[Indx] < 0.10 then
      Dec(NumOrgs);   { don't print below .1% }
  end;
end;  { GenProbs }

procedure TIDList.DelEntry(Entry: integer);
{ Delete an entry out of the IDList. }
var
  NewEntry: integer;
begin
  if Entry <= NumOrgs then
  begin
    if Entry < NumOrgs then
    begin
      NewEntry:= Entry + 1;
      Move(Slots.P^[NewEntry], Slots.P^[Entry],
            ((NumOrgs-NewEntry)+1) * SizeOf(Slots.P^[1]));
      Move(ReportOrgs.P^[NewEntry], ReportOrgs.P^[Entry],
            ((NumOrgs-NewEntry)+1) * SizeOf(ReportOrgs.P^[1]));
      Move(Freqs.P^[NewEntry], Freqs.P^[Entry],
            ((NumOrgs-NewEntry)+1) * SizeOf(Freqs.P^[1]));
    end;
    Dec(NumOrgs);
  end;
end;  { DelEntry }

procedure TIDList.ReSortList;
{ Re-sort the IDList. }
var
  High      : single;
  HighIdx   : integer;
  TempOrg   : integer;
  TempGroup : integer;
  Top       : integer;
  Idx       : integer;
begin
  Top:= 1;
  while Top < NumOrgs do
  begin
    HighIdx:= Top;
    High:= Freqs.P^[HighIdx];
    Idx:= Top + 1;
    while Idx <= NumOrgs do { Find largest }
    begin
      if Freqs.P^[Idx] > High then
      begin
        HighIdx:= Idx;
        High:= Freqs.P^[HighIdx];
      end;
      Inc(idx);
    end;
    if HighIdx > Top then     { Exchange TOP with HIGHEST }
    begin
      TempOrg:= Slots.P^[HighIdx];
      TempGroup:= ReportOrgs.P^[HighIdx];
      Slots.P^[HighIdx]:= Slots.P^[Top];
      ReportOrgs.P^[HighIdx]:= ReportOrgs.P^[Top];
      Freqs.P^[HighIdx]:= Freqs.P^[Top];
      Slots.P^[Top]:= TempOrg;
      ReportOrgs.P^[Top]:= TempGroup;
      Freqs.P^[Top]:= High;
    end;
    Inc(Top);       { Bump top up one }
  end; { while Top < NumOrgs }
end;  { ResortList }

procedure TIDList.CompressSpecies;
{ Run through the IDlist and compress all org groups into single entries and
  then resort it. }
var
  Idx1, Idx2  : integer;
  Species1,
  Species2    : integer;
begin
  Idx1:= 1;
  while Idx1 < NumOrgs do
  begin
    Idx2:= Idx1 + 1;
    while Idx2 <= NumOrgs do with ChemProbs do
    begin
      Species1:= TMatrix(Matrix.P^[Slots.P^[Idx1]]^).SpeciesOrg;
      Species2:= TMatrix(Matrix.P^[Slots.P^[Idx2]]^).SpeciesOrg;
      if Species1 = Species2 then
      begin
        Freqs.P^[Idx1]:= Freqs.P^[Idx1] + Freqs.P^[Idx2];
        DelEntry(Idx2);
      end
      else
        Inc(Idx2);
    end; { while Idx2 }
    Inc(Idx1);
  end; { while Idx1 }
  ReSortList;
  for Idx1:= 1 to NumOrgs do
  begin
    ReportOrgs.P^[Idx1]:= TMatrix(ChemProbs.Matrix.P^[Slots.P^[Idx1]]^).SpeciesOrg;
  end;
end;  { CompressSpecies }

procedure TIDList.CompressGroups;
{ Run through the IDlist and compress all org groups into single entries and
  then resort it. }
var
  Idx1, Idx2    : integer;
  Group1, Group2: integer;
begin
  Idx1:= 1;
  while Idx1 < NumOrgs do
  begin
    Idx2:= Idx1 + 1;
    while Idx2 <= NumOrgs do with ChemProbs do
    begin
      Group1:= TMatrix(Matrix.P^[Slots.P^[Idx1]]^).GroupOrg;
      Group2:= TMatrix(Matrix.P^[Slots.P^[Idx2]]^).GroupOrg;
      if Group1 = Group2 then
      begin
        Freqs.P^[Idx1]:= Freqs.P^[Idx1] + Freqs.P^[Idx2];
        DelEntry(Idx2);
      end
      else
        Inc(Idx2);
    end; { while Idx2 }
    Inc(Idx1);
  end; { while Idx1 }
  ReSortList;
  for Idx1:= 1 to NumOrgs do
  begin
    ReportOrgs.P^[Idx1]:= TMatrix(ChemProbs.Matrix.P^[Slots.P^[Idx1]]^).GroupOrg;
  end;
end;  { CompressGroups }

procedure TIDList.Assign(var Source: TIDList);
var
  Indx: integer;
begin
  NumOrgs:= Source.NumOrgs;
  for Indx:= 1 to Source.NumOrgs do
  begin
    Slots.P^[Indx]:= Source.Slots.P^[Indx];
    ReportOrgs.P^[Indx]:= Source.ReportOrgs.P^[Indx];
    Freqs.P^[Indx]:= Source.Freqs.P^[Indx];
    Probs.P^[Indx]:= Source.Probs.P^[Indx];
  end;
end;

procedure TIDList.Clear;
var
  Indx: integer;
begin
  NumOrgs:= 0;     { Reset the IDlist for the next ID }
  for Indx:= 1 to MaxListed do
    Freqs.P^[Indx]:= 0.0;
end;

procedure TIDList.Done;
begin
  Slots.Done;
  ReportOrgs.Done;
  Freqs.Done;
  Probs.Done;
end;

procedure TIdentify.Abort(ErrNum: integer);
begin
  ShowError(nil, IDS_PROGHALT, nil, MOD_IDLIB + errNum, MOD_IDLIB, 0);
  Halt;
end;

constructor TIdentify.Init(ProbName, MICName: string; pCompress, pTryGroup: boolean);
{ Initialize global Variables and read command line parameters. }
var
  Indx    : integer;
  Temp    : integer;
  Code    : integer;
  TempStr : string[80];
begin
  inherited Init;
  Compress:= pCompress;
  TryGroupLevel:= pTryGroup;
  if TryGroupLevel then
    Compress:= true;
  if not Orgobject.InitFlag then
    Abort(1);
  IDList.Init;
  GroupIDList.Init;
  if not ChemProbs.InitFlag then
    Abort(2);
  with MICProbs do
  begin
    if MICName ='' then
    begin
      BioStrLen:= 0;
      Biolen:= -1;
      MaxBiot:= -1;
      InitFlag:= false;
      NumProbRecs:= -1;
    end
    else
    begin   { Load table for antibiogramme refinements }
      if not MICProbs.InitFlag then
        Abort(3);
    end;
  end;
end;

procedure TIdentify.GetAtypicals(Index: integer; var Count: integer);
{ Look through the biotype and prob tables and find the tests that are atypical.
  Atypical is defined as wrong tests where the prob table value is >= 85 or
  <= 15 percent. }
var
  Bit: integer;   { what bit in the biotype we're looking at }
  Org: integer;
  Test: single;
begin
  Count:= 0;         { Start with no atyps found }
  Org:= IDlist.Slots.P^[Index];      { Get slot number in prob table. }
  for Bit:= 0 to ChemProbs.BioLen do     { for each bit in the biotype }
  begin
    if TestBioBit(ChemProbs.Biotype, Bit) then
    begin
      Test:= TMatrix(ChemProbs.Matrix.P^[Org]^).pTable[0].P^[Bit];
    end
    else
    begin
      Test:= TMatrix(ChemProbs.Matrix.P^[Org]^).pTable[1].P^[Bit];
    end;
    if Test >= AtypHigh then    { Test wrong and atypical }
    begin       { Add the test at Bit to the Atypicals list  }
      Inc(Count);
      if Count <= AtypsListed then  { Save the text for this test }
      begin
        Move(ChemProbs.BioChems.Ptr(Bit)^, Atypicals[Count][1], 4);
        Atypicals[Count][0]:= CHR(4);
      end;         { Otherwise just count it and don't save text }
    end; { if Test >= AtypHigh }
  end; { for }
end;  { GetAtypicals }

procedure TIdentify.MICRefinements(MICBioStr, MICBioMaskStr: string);
var
  Prob      : integer;  { index into the biotype - i.e. bit number }
  Indx      : integer;
  CurrResult: single;
  MICSlot   : integer;
  MICIndx   : integer;
  ThisOrg   : integer;
  ChemSlot  : integer;
begin
  if not MICProbs.NewSC(241) then exit;
  with MICProbs do
  begin
    if (MICBioStr <> '') and (IDList.NumOrgs > 1) and (IDList.Probs.P^[1] <= 85.0) then
    begin       { Antibiogramme Refinements }
      Biotype:= MICBioStr;
      if not OKBiotype(Biotype) then
      begin
        InvalidBiotype:= true;
        exit;
      end;
      if MICBioMaskStr = '' then
        while Length(MICBioMaskSTr) < BioStrLen do
          MICBioMaskStr:= CONCAT(MICBioMaskStr, '0');
      BioMask:= MICBioMaskStr;
      if not OKBiotype(BioMask) then
      begin
        InvalidBiotype:= true;
        exit;
      end;
      for Indx:= 1 to IDList.NumOrgs do
      begin
        ChemSlot:= IDList.Slots.P^[Indx];
        CurrResult:= TMatrix(ChemProbs.Matrix.P^[ChemSlot]^).HoldResult;
        ThisOrg:= TMatrix(ChemProbs.Matrix.P^[ChemSlot]^).IndivOrg;
        MICIndx:= 0;   MICSlot:= -1;
        repeat
          if TMatrix(Matrix.P^[MICIndx]^).IndivOrg = ThisOrg then
            MICSlot:= MICIndx;
          Inc(MICIndx);
        until (MICSlot >= 0) or (MICIndx >= NumProbRecs);
        for Prob:= 0 to BioLen do
        begin
          if TestBioBit(BioMask, Prob) then
            ProbLineIndex[Prob]:= 2
          else if TestBioBit(Biotype, Prob) then
            ProbLineIndex[Prob]:= 1
          else
            ProbLineIndex[Prob]:= 0;
        end;
        Prob:= BioLen;
        if (MICSlot >= 0) then
        begin
          with TMatrix(Matrix.P^[MICSlot]^) do
          begin
            while (Prob >= 0) and (CurrResult > CutOff) do
            begin       { not last bit not below cutoff }
              CurrResult:= CurrResult * pTable[ProbLineIndex[Prob]].P^[Prob];
              Dec(Prob);
            end; { while Prob }
          end;
        end;
        IDList.Freqs.P^[Indx]:= CurrResult * TMatrix(ChemProbs.Matrix.P^[ChemSlot]^).Adjust;
        TMatrix(ChemProbs.Matrix.P^[ChemSlot]^).HoldResult:= CurrResult;
      end; { for Indx }
      IDList.ReSortList;
      IDList.GenProbs;
    end; { Antibiogramme refinements }
  end;
end;  { MICRefinements }

function TIdentify.Identify(BioStr, BioMaskStr: string; ProbSC: integer) : boolean;
var
  Prob          : integer;  { index into the biotype - i.e. bit number }
  Org           : integer;
  Indx          : integer;
  CurrResult    : single;
  MICBioStr     : string;
  MICBioMaskStr : string;
  ThisIndoleInv : boolean;  { true if This biotype has indole INVALID }
  IndoleTest    : integer;  { ChemProbs offset of indole test from right }
  IndoleInv     : integer;  { ChemProbs offset of indole-invalid from right }
  OrgSC         : integer;
begin
  Identify:= false;
  InvalidBiotype:= false;
  IDList.Clear;
  GroupIDlist.Clear;
  if not ChemProbs.NewSC(ProbSC) then
    exit;
  MICBioStr:= '';
  MICBioMaskStr:= '';
  Indx:= POS('.', BioStr);
  if Indx <> 0 then
  begin
    MICBioStr:= COPY(BioStr, Indx+1, LENGTH(BioStr)-Indx);
    BioStr:= COPY(BioStr, 1, Indx-1);
  end;
  Indx:= POS('.', BioMaskStr);
  if Indx <> 0 then
  begin
    MICBioMaskStr:= COPY(BioMaskStr, Indx+1, LENGTH(BioMaskStr)-Indx);
    BioMaskStr:= COPY(BioMaskStr, 1, Indx-1);
  end;
  with ChemProbs do
  begin
    Biotype:= BioStr;
    if not OKBiotype(Biotype) then
    begin
      InvalidBiotype:= true;
      Identify:= true;
      exit;
    end;
    if BioMaskStr = '' then
      while Length(BioMaskStr) < BioStrLen do
        BioMaskStr:= CONCAT(BioMaskStr, '0');
    BioMask:= BioMaskStr;
    if not OKBiotype(BioMask) then
    begin
      InvalidBiotype:= true;
      Identify:= true;
      exit;
    end;
    TestPos:= '';
    while Length(TestPos) < BioStrLen do
      TestPos:= CONCAT(TestPos, '0');
    TestNeg:= TestPos;
    AtypicalPos:= TestPos;
    AtypicalNeg:= TestPos;
  end;
  with ChemProbs do
  begin
    if (ProbSC = RapidNeg2) or (ProbSC = RapidNeg3) then
    begin
      IndoleTest:= BioLen - IndTstPos;   { Offsets from the right side of the }
      IndoleInv:= BioLen - IndInvPos;    { Prob table. }
      ThisIndoleInv:= TestBioBit(Biotype, IndoleInv);
      if ThisIndoleInv then
      begin   { No indole -> mask indole test }
        SetBioBit(BioMask, IndoleTest);
      end
      else
      begin     { Indole -> Remove Indole mask }
        ClearBioBit(BioMask, IndoleTest);
      end; { if ThisIndoleInv }
    end; { if ProbSC = RapidNeg2 or RapidNeg3 }
  end;
  case ProbSC of          { Read in the org names from ORG.NAM }
    51  : OrgSC:= 21;
    52  : OrgSC:= 22;
    201,
    221 : OrgSC:= 10;
    211 : OrgSC:= 21;
    212 : OrgSC:= 22;
    else  OrgSC:= ProbSC;
  end;
  if not Orgobject.NewSC(OrgSC) then
    exit;
  { Set prob table indexes based on biotype, in prep for main loop }
  with ChemProbs do for Prob:= 0 to BioLen do
  begin
    if TestBioBit(BioMask, Prob) then
      ProbLineIndex[Prob]:= 2
    else if TestBioBit(Biotype, Prob) then
      ProbLineIndex[Prob]:= 1
    else
      ProbLineIndex[Prob]:= 0;
  end;
  with ChemProbs do
    for Org:= 0 to NumProbRecs do
      with TMatrix(Matrix.P^[Org]^) do
      begin
        Prob:= BioLen;   { Starting bit position }
        CurrResult:= 1.0;
        while (Prob >= 0) and (CurrResult > CutOff) do
        begin
          CurrResult:= CurrResult * pTable[ProbLineIndex[Prob]].P^[Prob];
          Dec(Prob);
        end; { while Prob }
        HoldResult:= CurrResult;
      end; { for Org }

  with ChemProbs do
  begin
    for Org:= 0 to NumProbRecs do
    with TMatrix(Matrix.P^[Org]^) do
    begin
      if HoldResult <> 0.0 then
        if HoldResult >= CutOff then
          IDList.InsertOrg(Org);
    end;  { for Org }
  end;

  IDList.GenProbs;
  if MICProbs.InitFlag then
  begin
    MICRefinements(MICBioStr, MICBioMaskStr);
    if InvalidBiotype then
    begin
      Identify:= true;
      exit;
    end;
  end;
  IDList.SetPosNeg(probSC,@ChemProbs, TestPos, TestNeg, AtypicalPos, AtypicalNeg);
  if Compress then
  begin       { Compress into species names }
    IDList.CompressSpecies;
    IDList.GenProbs;    { Calculate normalized probs }
  end;
  if TryGroupLevel then
  begin
    if (IDList.NumOrgs > 1) and (IDList.Probs.P^[1] <= 85.0) then
    begin { Compress into group names }
      GroupIDList.Assign(IDList);
      GroupIDList.CompressGroups;
      GroupIDList.GenProbs;
      if (GroupIDList.NumOrgs > 0) and (GroupIDList.Probs.P^[1] <= 85.0) then
        GroupIDList.NumOrgs:= 0;
    end;
  end;
  Identify:= true;
end;  { Identify }

destructor TIdentify.Done;
{ Close the open files.				 }
begin
  inherited Done;
  IDList.Done;
  GroupIDList.Done;
end;

function InitIDLIB: boolean;
begin
  InitIDLIB:= false;
  if not Orgobject.Init then
    exit;
  if not ChemProbs.Init(OrgPrbName) then
    exit;
  if not MICProbs.Init('') then
    exit;
  InitIDLIB:= true;
end;

{----------------------------------------------------------------------------}
{  Performs exit functions for the module and replaces the exitProc with the }
{  saved exit procedure.                                                     }
{----------------------------------------------------------------------------}

procedure CloseIDLIB;  far;
begin
  Orgobject.Done;
  ChemProbs.Done;
  MICProbs.Done;
  exitProc := saveExit;
end;

BEGIN
  ChemProbs.InitFlag:= false;
  MICProbs.InitFlag:= false;
  Orgobject.InitFlag:= false;
  LN10:= LN(10);
  MaxProbRecs:= 350;   { Max number of PRB table entries }
  MaxListed:= 5;     { Number of orgs listed in the ID list }
  InitIDLIB;
  saveExit := exitProc;
  exitProc := @CloseIDLIB;
END.
