unit EngDef;

{- REPORT ENGINE DEFINITIONS }

{- This module provides an object to load a custom report defintion into memory.

   The report defintion object is made up of the report name, page definition
   and section defintions.

   The section defintions are the section engines for each section type. The
   section engine descends from the common base section engine object
   TSectEngineBase. The engine is capable of printing itself on a device context.

   The report defintion that is provided contains a collection of the section
   engine objects for each section defined in the report.
}

INTERFACE

uses
  Objects,
  Gather,
  RptShare,
  MScan,
  DBTypes,
  WinProcs,
  WinTypes;

type
  {- Section Position object. These objects describe the positioning used by
     sections when they are printing. They contain a pointer to the data that
     is being printed as well as where in the data the section is currently located. }
  PSectEnginePos = ^TSectEnginePos;
  TSectEnginePos = object(TObject)
    data        : PGatherSpec;
    dataPrinted : boolean;
    posArray    : array[0..30] of longint; {- generic position array - depends on object }
                                           {- This array must have at least
                                              MaxIsoPerSect+1 elements (see rptshare.pas) }

    {}
    constructor Init;
    function CopyFrom(aPos: PSectEnginePos): boolean; virtual;
  end;


  {- BASE section engine object used by all section types }
  PSectEngineBase = ^TSectEngineBase;
  TSectEngineBase = object(TObject)
    constructor Init(aSectSeq: TSeqNum);
    destructor Done; virtual;
    procedure SetPrintData(aGatherSpec: PGatherSpec); virtual;
    function IsPrintComplete: boolean; virtual;
    procedure PrintData(DC: HDC; margins: TRect); virtual;
    function PushPosition: boolean; virtual;
    procedure PopPosition; virtual;
    procedure ClearPosStack; virtual;
    private
    sectRect    : TLRect;   {- origin and extent of section }
    opts        : TSectOpt;
    rfont       : TRptFont;
    intMargin   : integer;  {- internal margin used for drawing text. This val
                               is only valid after calling PrintData (in the
                               base object) }
    borderPen   : HPen;
    logFont     : TLogFont;   {- the current font attributes }
    font        : HFont;      {- handle to the current font }
    position    : PSectEnginePos;
    pushedPos   : PSectEnginePos;   {- pushed position (1 item stack) }
    LTpPX       : integer;    {- logical twips per pixel. Only valid after calling}
    LTpPY       : integer;    {  PrintData method (in the base object)}
    rLimit      : integer;  {- right limit for section }
    bLimit      : integer;  {- bottom limit for section }
    lLimit      : integer;  {- left limit for section }
    tLimit      : integer;  {- top limit for section }
    procedure PrepareForPrinting; virtual;
    function CreatePosition: PSectEnginePos; virtual;
    procedure PrintText(dc: HDC; buf: PChar; r: TRect; dtOpts: word);
  end;

  {- object to hold a report format definition }
  PRptDef = ^TRptDef;
  TRptDef = object(TObject)
    rptName : array[0..MaxRptNameLen] of char;
    sects   : PCollection;   {- collection of section definitions (engines) }
    pageDef : TRptPage;
    {}
    constructor Init(aRptSeq: TSeqNum);
    destructor Done; virtual;
    function LoadSections(aRptSeq: TSeqNum): integer;
    function LoadReportDef(aRptSeq: TSeqNum): integer;
  end;

const
  {- report definition errors reported in [engLastDefInitError] }

  {- cannot allocate the section collection in the report defintion object }
  Eng_CantAllocSectCollection     = MOD_RPTENG + 1;

  {- cannot locate the report format record (seq not found) }
  Eng_CantLoadRptFmt              = MOD_RPTENG + 2;

  Eng_CantLoadSection             = MOD_RPTENG + 3;

  {- error occured allocating collection to hold section fields information }
  Eng_CantAllocFldsCollection     = MOD_RPTENG + 4;

  {- following errors occur when section engines cannot open their definition files}
  Eng_CantOpenDemoSectFile        = MOD_RPTENG + 5;
  Eng_CantOpenDemoSectFldsFile    = MOD_RPTENG + 6;
  Eng_CantOpenGSSectFile          = MOD_RPTENG + 7;
  Eng_CantOpenGSSectFldsFile      = MOD_RPTENG + 8;
  Eng_CantOpenIsoSumSectFile      = MOD_RPTENG + 9;
  Eng_CantOpenIsoCompSectFile     = MOD_RPTENG + 10;
  Eng_CantOpenIsoFullSectFile     = MOD_RPTENG + 11;

  {- following occur when section engines cannot find their definition record }
  Eng_CantFindDemoSect            = MOD_RPTENG + 20;
  Eng_CantFindGSSect              = MOD_RPTENG + 21;
  Eng_CantFindIsoSumSect          = MOD_RPTENG + 22;
  Eng_CantFindIsoCompSect         = MOD_RPTENG + 23;
  Eng_CantFindIsoFullSect         = MOD_RPTENG + 23;

  {- no sections are defined in the report format }
  Eng_NoSectsDefined              = MOD_RPTENG + 30;

  {- The id in the section definition of the Rpt xref file is unknown }
  Eng_UnknownSectionID            = MOD_RPTENG + 31;

var
  engLastDefInitError : integer;    {- contains an initialization error immediately }
                                    {- following a call to init a TRptDef. }
                                    {- if zero, then initialization was successful }
IMPLEMENTATION

uses
  APITools,
  DBLib,
  DBFile,
  DBIDs,
  StrsW,
  Strings;

const
  ExtraPointSize    = 1;    {- extra points to add to font point size to allow
                               for descenders and stuff}

type
  {- DEMOGRAPHICS section print engine }
  PDemoSectEng = ^TDemoSectEng;
  TDemoSectEng = object(TSectEngineBase)
    flds    : PCollection;    {- collection of section field descriptions }
    {}
    constructor Init(aSectSeq: TSeqNum);
    destructor Done; virtual;
    procedure PrintData(DC: HDC; margins: TRect); virtual;
  end;

  {- GRAM STAIN section print engine }
  PGSSectEng = ^TGSSectEng;
  TGSSectEng = object(TSectEngineBase)
    flds    : PCollection;    {- collection of section field descriptions }
    {}
    constructor Init(aSectSeq: TSeqNum);
    destructor Done; virtual;
    procedure PrintData(DC: HDC; margins: TRect); virtual;
  end;

  {- ISOLATE SUMMARY section print engine }
  PIsoSumSectEng = ^TIsoSumSectEng;
  TIsoSumSectEng = object(TSectEngineBase)
    separators      : integer;  {- border style -1=none, or PS_xxx constant }
    headers         : boolean;  {- column headers on/off }
    underlineHeader : boolean;  {- underline column headers }
    isoFlds         : TRptFldList; {- selected iso field numbers }
    fldWidth        : array[1..MaxFldsPerRecord] of longint; {- column widths in LTwips }
    {}
    constructor Init(aSectSeq: TSeqNum);
    procedure PrintData(DC: HDC; margins: TRect); virtual;
  end;

  {- ISOLATE COMPRESSED DETAIL section print engine }
  PIsoCompSectEng = ^TIsoCompSectEng;
  TIsoCompSectEng = object(TSectEngineBase)
    separators      : integer;  {- border style -1=none, or PS_xxx constant }
    headers         : boolean;  {- column headers on/off }
    underlineHeader : boolean;  {- underline column headers }
    interps         : boolean;  {- show interps }
    MICs            : boolean;  {- show MICs }
    isoPerSect      : integer;  {- number of isos per section }
    isoWidth        : longint;  {- width of isolate column }
    micWidth        : longint;
    micOrder        : integer;  {- 0=mic first, 1 = interp first }
    micSpace        : longint;  {- spacing between mic and interp sub cols }
    interpWidth     : longint;
    drugDisp        : integer;  {- drug display type (0=name, 1-abbrev, 2=alternate }
    drugWidth       : longint;
    drugList        : PCollection;
    {}
    constructor Init(aSectSeq: TSeqNum);
    destructor Done; virtual;
    procedure SetPrintData(aGatherSpec: PGatherSpec); virtual;
    procedure PrintData(DC: HDC; margins: TRect); virtual;
    procedure BuildDrugList;
    procedure PopPosition; virtual;
    function GetCurIso: integer;
    function GetCurDrug: integer;
    procedure SetCurIso(newIso: integer);
    procedure SetCurDrug(newDrug: integer);
  end;

  {- ISOLATE FULL DETAIL section print engine }
  PIsoFullSectEng = ^TIsoFullSectEng;
  TIsoFullSectEng = object(TSectEngineBase)
    isoPerSect      : integer;     {- number of isos per section }
    separators      : integer;     {- border style -1=none, or PS_xxx constant }
    isoWidth        : longint;     {- width of isolate column }
    detail          : TDetailList; {- isolate detail columns }
    isoFlds         : TRptFldList; {- selected iso field numbers }
    lblWidth        : longint;     {- width of isolate field label }
    valWidth        : longint;     {- width of isolate field value }
    subSpacing      : longint;     {- spacing between isolate sub columns }
    {}
    constructor Init(aSectSeq: TSeqNum);
    procedure PrintData(DC: HDC; margins: TRect); virtual;
    function GetCurIso: integer;
    function GetCurDrug(column: integer): integer;
    procedure SetCurIso(newIso: integer);
    procedure SetCurDrug(column, newDrug: integer);
  end;

var
  sectEngInitError    : integer;

{----[ Miscellaneous routines ]----}

procedure CheckFont(var logFont: TLogFont; dc: HDC; var font: HFont; anOpt: TFldOpts);
{- compare the current font with the passed font. If different, then select
    a new font. }
var
  curIsBold   : boolean;
  curIsUnd    : boolean;
  tFont       : HFont;
begin
  curIsBold:= logFont.lfWeight = FW_BOLD;
  curIsUnd:= logFont.lfUnderline <> 0;
  if (curIsBold <> anOpt.bold) or (curIsUnd <> anOpt.underline) then
  begin {- need to change the font }
    if anOpt.bold then
      logFont.lfWeight:= FW_BOLD
    else
      logFont.lfWeight:= FW_NORMAL;
    if anOpt.underLine then
      logFont.lfUnderline:= 1
    else
      logFont.lfUnderline:= 0;

    tFont:= CreateFontIndirect(logFont);
    DeleteObject(SelectObject(dc, tFont));
    font:= tFont;
  end;
end;

constructor TSectEnginePos.Init;
begin
  inherited Init;
  FillChar(posArray, sizeof(posArray), 0);
end;

function TSectEnginePos.CopyFrom(aPos: PSectEnginePos): boolean;
begin
  if (aPos = nil) then
    CopyFrom:= false
  else
  begin
    CopyFrom:= true;
    data:= aPos^.data;
    posArray:= aPos^.posArray;
    dataPrinted:= aPos^.dataPrinted;
  end;
end;

{-------------------------------------------------------[ TSectEngineBase ]--}

{ This is the base section engine object for report sections. Any report sections
  should descend from this object and override all the methods. }

constructor TSectEngineBase.Init(aSectSeq: TSeqNum);
{- initialize the section engine.
   aSectSeq - This is the section sequence number as it was saved in the report
              generator. Each report has numerous sections.  The report engine
              will go through each section in the report cross reference file
              (RPTSECTS) and for each section defined there, it will instantiate
              a TSectEngineBase derivative based on the section type found in
              the RPTSECTS record }
begin
  inherited Init;
  sectEngInitError:= 0;
  position:= nil;
  pushedPos:= nil;

  {- set some defaults (descendents should read these from file)}
  sectRect.left:= 0; sectRect.right:= 1440;
  sectRect.top:= 0;  sectRect.bottom:= 1440;

  opts.bkMode:= TRANSPARENT;
  opts.border:= -1;   {- no border }

  intMargin:= 0;  {- assume no internal margin to start (wait until PrintData call)}
  FillChar(rFont, sizeof(rFont), 0);
  borderPen:= GetStockObject(NULL_PEN);
  font:= 0;
  FillChar(logFont, sizeof(logFont), 0);
  position:= CreatePosition;
end;

destructor TSectEngineBase.Done;
begin
  inherited Done;
  if opts.border <> -1 then
  begin
    DeleteObject(borderPen);
    opts.border:= -1;
  end;
  if font <> 0 then
  begin
    DeleteObject(font);
    font:= 0;
  end;
  MSDisposeObj(position);
  MSDisposeObj(pushedPos);
end;

procedure TSectEngineBase.PrepareForPrinting;
{- set up pens and fonts for printing. Should be called ONLY after definition
   is loaded and the opts and font fields have valid data. }
begin
  if opts.border <> -1 then
    borderPen:= CreatePen(opts.border, 0, RGB(0,0,0));

  logFont:= rFont.lf;
  font:= CreateFontIndirect(logFont);
end;

procedure TSectEngineBase.SetPrintData(aGatherSpec: PGatherSpec);
{- This function sets the data to be printed.  This function must be called
   before any calls to print the data.  Each section should know how to extract
   the information that it represents from the gather object.
   This function also pushes the current position onto the position stack. }
begin
  position^.data:= aGatherSpec;
  position^.dataPrinted:= false;
  FillChar(position^.posArray, sizeof(position^.posArray), 0);
end;

procedure TSectEngineBase.PrintData(DC: HDC; margins: TRect);
{- this function should be called to print the data that was set in SetPrintData.
   The section will print all data from the gather object according to how the
   section was defined in the report generator. Different sections will act
   differently when this function is called.  For example, when a demographic
   type section is called to Print it's Data, it will always print whatever has
   been defined in the section definition.  However, isolate type sections, those
   that repeat, will print whatever they can in their defined section, then for
   subsequent calls to PrintData, they will continue where they left off until
   they have printed everything.
   The default behaviour of the base section engine is to print the border around
   the section (if any).

   DC    - This is the DC to print to }
var
  oldPen    : HPen;
  x         : integer;
begin
  LTwipsPerPixel(dc, LTpPX, LTpPY);
  intMargin:= LTpPX * 2;    {- internal margin for text in rectangles  }

  position^.dataPrinted:= true;
  SetBkMode(dc, TRANSPARENT);

  rLimit:= margins.left + sectRect.left + sectRect.right;
  bLimit:= margins.top + sectRect.top + sectRect.bottom;
  lLimit:= margins.left + sectRect.left;
  tLimit:= margins.top + sectRect.top;

  if opts.border <> -1 then
  begin
    oldPen:= SelectObject(dc, borderPen);

    {- calculate right limit, clip if neccessary }
    x:= margins.left + sectRect.left + sectRect.right + LTpPX;
(*    if x > margins.right then*)
(*    begin*)
(*      {- draw a "clipped" rectangle }*)
(*      x:= margins.right;*)
(*      MoveTo(dc, margins.left + sectRect.left + sectRect.right + LTpPX,*)
(*                 margins.top + sectRect.top);*)
(*      LineTo(dc, margins.left + sectRect.left,*)
(*                 margins.top + sectRect.top);*)
(*      LineTo(dc, margins.left + sectRect.left,*)
(*                 margins.top + sectRect.top + sectRect.bottom + LTpPY);*)
(*      LineTo(dc, margins.left + sectRect.left + sectRect.right + LTpPX,*)
(*                 margins.top + sectRect.top + sectRect.bottom + LTpPY);*)
(*    end*)
(*    else*)
    Rectangle(dc, margins.left + sectRect.left,
                  margins.top + sectRect.top,
                  x,
                  margins.top + sectRect.top + sectRect.bottom + LTpPY);
  end;
  if opts.border <> -1 then
    SelectObject(dc, oldPen);
end;

function TSectEngineBase.IsPrintComplete: boolean;
{- this function returns true when a section has successfully printed all of
   its data from the gathered specimen.  For example, if an isolate section
   was asked to print, and the section definition only had room for 10 isolates,
   and there were 20 isolates, then after the first call to PrintData there
   would still be 10 isolates that needed to be printed. After a second call to
   PrintData, all isolate data would be printed and this function can then return
   true. }
begin
  IsPrintComplete:= position^.dataPrinted;
end;

function TSectEngineBase.CreatePosition: PSectEnginePos;
{- this function is called to create the sections position object. It is called
   from Init. Descendents should override this method to create specialized
   position objects depending on their needs }
begin
  CreatePosition:= New(PSectEnginePos, Init);
end;

procedure TSectEngineBase.PrintText(dc: HDC; buf: PChar; r: TRect; dtOpts: word);
{- this method is used to print text. It differs from draw text in that it
   adjusts the size of the print rectangle by the internal margin (intMarg).
   This keeps the text from bumping up against the egde.
   intMarg must have been set to the proper value. By default it is the first
   thing set in PrintData.
   buf    - is the string to print.
   r      - is the print rectangle (in LTwips). This param is modified locally so
            make sure it is not passed as a Var param.
   dtOpts - is the DT_xxx drawing options for the drawtext function  }
begin
  { Adjust the size of the rect }
  Inc(r.left, intMargin);
  Dec(r.right, intMargin);
  DrawText(dc, buf, StrLen(buf), r, dtOpts);
end;

function TSectEngineBase.PushPosition: boolean;
{- This function pushes the current position information onto a 1 item stack.
   Any subsequent pushes will remove the last pushed position.
   Returns true if the position was pushed properly }
var
  aPos  : PSectEnginePos;
begin
  MSDisposeObj(pushedPos);

  pushedPos:= CreatePosition;
  if not pushedPos^.CopyFrom(position) then
    MSDisposeObj(pushedPos);
  PushPosition:= pushedPos <> nil;
end;

procedure TSectEngineBase.PopPosition;
{- retore a pushed position }
begin
  if pushedPos <> nil then
  begin
    position^.CopyFrom(pushedPos);  {- restore pushed position }
    MSDisposeObj(pushedPos);
  end;
end;

procedure TSectEngineBase.ClearPosStack;
begin
  if pushedPos <> nil then
  begin
    MSDisposeObj(pushedPos);
  end;
end;

{======================== DEMOGRAPHICS SECTION ENGINE ========================}

type
  {- This is the record type used to store the demographic field definitions }
  {- This object will be collected by the Demographic section engine }
  PDemoFldDef = ^TDemoFldDef;
  TDemoFldDef = object(TObject)
    fldType     : integer;  {- 0 = free text, 1 = field label, 2 = field value }
    buf         : PChar;    {- free text -or- field label (depends on type) }
                            {- This field may be NIL if no value is defined }
    fileName    : array[0..9] of char;
    fldNum      : integer;  {- field number of database record }
    org         : TPoint;   {- field origin }
    ext         : TPoint;   {- field extent }
    opts        : TFldOpts; {- field display options }
    constructor Init(dbr: PDBRec);
    destructor Done; virtual;
  end;

constructor TDemoFldDef.Init(dbr: PDBRec);
{- this object assumes that the record passed in contains record data from
   the demographic fields file (DBRPTDemoFldsFile) }
var
  pstr    : PChar;
  size    : word;
begin
  inherited Init;
  dbr^.GetField(DBRptDemoFldsFldType, @fldType, sizeof(fldType));

  {- extract free text buffer }
  if fldType = 0 then
  begin
    size:= dbr^.desc^.FieldSizeForStr(DBRptDemoFldsBuf);
    GetMem(pstr, size);
    dbr^.GetFieldAsStr(DBRptDemoFldsBuf, pstr, size-1);
    buf:= StrNew(pstr);
    MSFreeMem(pstr, size);
  end
  else
    buf:= nil;

  dbr^.GetFieldAsStr(DBRptDemoFldsFileName, fileName, sizeof(fileName)-1);
  dbr^.GetField(DBRptDemoFldsFldNum, @fldNum, sizeof(fldNum));
  dbr^.GetField(DBRptDemoFldsOrg, @org, sizeof(org));
  dbr^.GetField(DBRptDemoFldsExt, @ext, sizeof(ext));
  dbr^.GetField(DBRptDemoFldsOpts, @opts, sizeof(opts));
end;

destructor TDemoFldDef.Done;
begin
  inherited Done;
  MSStrDispose(buf);
end;

{----------------------------------------------------------[ TDemoSectEng ]--}

constructor TDemoSectEng.Init(aSectSeq: TSeqNum);
var
  dbDemo  : PDBFile;
  dbFlds  : PDBFile;
  seq     : TSeqNum;
begin
  inherited Init(aSectSeq);
  dbDemo:= nil;
  dbFlds:= nil;
  flds:= nil;

  flds:= New(PCollection, Init(20, 20));
  if flds = nil then
    sectEngInitError:= Eng_CantAllocFldsCollection;

  if sectEngInitError = 0 then
  begin
    dbDemo:= New(PDBFile, Init(DBRPTDemoSectsFile, '', dbOpenNormal));
    if dbDemo = nil then
      sectEngInitError:= Eng_CantOpenDemoSectFile;
  end;

  if sectEngInitError = 0 then
  begin
    dbFlds:= New(PDBFile, Init(DBRPTDemoFldsFile, '', dbOpenNormal));
    if dbFlds = nil then
      sectEngInitError:= Eng_CantOpenDemoSectFldsFile;
  end;

  if sectEngInitError = 0 then
  begin
    {- read the demographic definition from the database }
    if dbDemo^.dbc^.GetSeq(dbDemo^.dbr, aSectSeq) then
    begin
      {- got the section definition (in dbDemo^.dbr), now read section def and
         all demographic field definitions for the section }

      dbDemo^.dbr^.GetField(DBRptDemoSectFont, @rFont, sizeof(rFont));
      dbDemo^.dbr^.GetField(DBRptDemoSectOptions, @opts, sizeof(opts));
      dbDemo^.dbr^.GetField(DBRptDemoSectSize, @sectRect, sizeof(sectRect));

      seq:= dbDemo^.dbr^.GetSeqValue;
      dbFlds^.dbr^.PutField(DBRptDemoFldsSectRef, @seq);
      if dbFlds^.dbc^.GetFirstContains(dbFlds^.dbr) then
      begin
        repeat
          flds^.Insert(New(PDemoFldDef, Init(dbFlds^.dbr)));
        until not dbFlds^.dbc^.GetNextContains(dbFlds^.dbr);
      end;
    end
    else
      sectEngInitError:= Eng_CantFindDemoSect;
  end;

  MSDisposeObj(dbDemo);
  MSDisposeObj(dbFlds);

  if sectEngInitError <> 0 then
  begin
    Done;
    fail;
  end
  else
    PrepareForPrinting;
end;

destructor TDemoSectEng.Done;
begin
  inherited Done;
  MSDisposeObj(flds);
end;

procedure TDemoSectEng.PrintData(DC: HDC; margins: TRect);
{- print the demographics data from the gathered specimen (set in SetPrintData) }
var
  p1, p2    : array[0..100] of char;
  y, cy     : integer;
  tm        : TTextMetric;
  oldFont   : HFont;

  procedure PrintIt(fld: PDemoFldDef); far;
  {- print a specific field }
  var
    r     : TRect;
    pstr  : PChar;
    gObj  : PGatherObj;
    size  : integer;
  begin
    CheckFont(logFont, dc, font, fld^.opts);

    r.left:= margins.left + sectRect.left + fld^.org.x;
    {- if the field is to the left or above the section it will be moved into
       the section }
    if r.left < lLimit then
      r.left:= lLimit;
    r.top:= margins.top + sectRect.top + fld^.org.y;
    if r.top < tLimit then
      r.top:= tLimit;
    r.right:= r.left + fld^.ext.x;
    r.bottom:= r.top + fld^.ext.y;

    {- see if field needs to be cliped }
    if r.left > rLimit then
      exit
    else if r.right > rLimit then
      r.right:= rLimit;

    if r.top > bLimit then
      exit
    else if r.bottom > bLimit then
      r.bottom:= bLimit;

    pstr:= nil;
    size:= 0;

(*    {- if field borders were ever to be used, the following line would display them }*)
(*    Rectangle(dc, r.left - LTpPX, r.top - LTpPY, r.right, r.bottom);*)

    if fld^.fldType = 0 then  {- if field is freetext then the text is in fld^.buf }
    begin
      if (fld^.buf <> nil) and (StrLen(fld^.buf) > 0) then
      begin
        size:= StrLen(fld^.buf) + 1;
        GetMem(pstr, size);
        StrLCopy(pstr, fld^.buf, size-1);
      end;
    end
    else
    begin {- otherwise, the field data must be extracted }
      {- is it a specimen or patient field type ?}
      if StrIComp(fld^.fileName, DBSpecFile) = 0 then
        gObj:= position^.data
      else
        gObj:= position^.data^.patInfo;

      {- is it a field label or a field value (1 or 2) ? }
      if fld^.fldType = 1 then
        size:= gObj^.FieldNameStrLen(fld^.fldNum)
      else
        size:= gObj^.FieldStrLen(fld^.fldNum);

      {- is it worth displaying ? }
      if size > 0 then
      begin
        GetMem(pstr, size);
        StrCopy(pstr, '');
        if fld^.fldType = 1 then
          gObj^.GetFieldName(fld^.fldNum, pstr, size-1)
        else
          gObj^.GetField(fld^.fldNum, pstr, size-1);
      end;
    end;
    if size > 0 then
    begin
      PrintText(dc, pstr, r, fld^.opts.just or DT_WORDBREAK or DT_NOPREFIX);
      MSFreeMem(pstr, size);
    end;
  end;

begin
  inherited PrintData(dc, margins);
  if position^.data <> nil then
  begin
    oldFont:= SelectObject(dc, font);
    flds^.ForEach(@PrintIt);
    SelectObject(dc, oldFont);
  end;
end;

{========================= GRAM STAIN SECTION ENGINE ========================}

type
  {- This is the record type used to store the gram stain field definitions }
  {- This object will be collected by the gram stain section engine }
  PGSFldDef = ^TGSFldDef;
  TGSFldDef = object(TObject)
    fldType     : integer;  {- 0 = free text, 1 = label, 2 = abbrev, 3 = value }
    buf         : PChar;    {- free text buffer }
    slotNum     : integer;  {- gs slot number }
    org         : TPoint;   {- field origin }
    ext         : TPoint;   {- field extent }
    opts        : TFldOpts; {- field display options }
    constructor Init(dbr: PDBRec);
    destructor Done; virtual;
  end;

{------------------------------------------------------------[ TGSFldDef ]--}

constructor TGSFldDef.Init(dbr: PDBRec);
{- this object assumes that the record passed in contains record data from
   the demographic fields file (DBRPTDemoFldsFile) }
var
  pstr    : PChar;
  size    : word;
begin
  inherited Init;
  dbr^.GetField(DBRptGSFldsFldType, @fldType, sizeof(fldType));

  {- extract free text }
  if fldType = 0 then
  begin
    size:= dbr^.desc^.FieldSizeForStr(DBRptGSFldsBuf);
    GetMem(pstr, size);
    dbr^.GetFieldAsStr(DBRptGSFldsBuf, pstr, size-1);
    buf:= StrNew(pstr);
    MSFreeMem(pstr, size);
  end
  else
    buf:= nil;

  dbr^.GetField(DBRptGSFldsSlotNum, @slotNum, sizeof(slotNum));
  dbr^.GetField(DBRptGSFldsOrg, @org, sizeof(org));
  dbr^.GetField(DBRptGSFldsExt, @ext, sizeof(ext));
  dbr^.GetField(DBRptGSFldsOpts, @opts, sizeof(opts));
end;

destructor TGSFldDef.Done;
begin
  inherited Done;
  MSStrDispose(buf);
end;

{----------------------------------------------------------[ TGSSectEng ]--}

constructor TGSSectEng.Init(aSectSeq: TSeqNum);
var
  dbGS    : PDBFile;
  dbGSFlds: PDBFile;
  seq     : TSeqNum;
begin
  inherited Init(aSectSeq);
  dbGS:= nil;
  dbGSFlds:= nil;
  flds:= nil;

  flds:= New(PCollection, Init(20, 20));
  if flds = nil then
    sectEngInitError:= Eng_CantAllocFldsCollection;

  if sectEngInitError = 0 then
  begin
    dbGS:= New(PDBFile, Init(DBRPTGSSectsFile, '', dbOpenNormal));
    if dbGS = nil then
      sectEngInitError:= Eng_CantOpenGSSectFile;
  end;

  if sectEngInitError = 0 then
  begin
    dbGSFlds:= New(PDBFile, Init(DBRPTGSFldsFile, '', dbOpenNormal));
    if dbGSFlds = nil then
      sectEngInitError:= Eng_CantOpenGSSectFldsFile;
  end;

  if sectEngInitError = 0 then
  begin
    {- read the gram stain report definition from the database }
    if dbGS^.dbc^.GetSeq(dbGS^.dbr, aSectSeq) then
    begin
      {- got the section definition (in dbGS^.dbr), now read section def and
         all gram stain field definitions for the section }

      dbGS^.dbr^.GetField(DBRptGSSectFont, @rFont, sizeof(rFont));
      dbGS^.dbr^.GetField(DBRptGSSectOptions, @opts, sizeof(opts));
      dbGS^.dbr^.GetField(DBRptGSSectSize, @sectRect, sizeof(sectRect));

      seq:= dbGS^.dbr^.GetSeqValue;
      dbGSFlds^.dbr^.PutField(DBRptGSFldsSectRef, @seq);
      if dbGSFlds^.dbc^.GetFirstContains(dbGSFlds^.dbr) then
      begin
        repeat
          flds^.Insert(New(PGSFldDef, Init(dbGSFlds^.dbr)));
        until not dbGSFlds^.dbc^.GetNextContains(dbGSFlds^.dbr);
      end;
    end
    else
      sectEngInitError:= Eng_CantFindGSSect;
  end;

  MSDisposeObj(dbGS);
  MSDisposeObj(dbGSFlds);

  if sectEngInitError <> 0 then
  begin
    Done;
    fail;
  end
  else
    PrepareForPrinting;
end;

destructor TGSSectEng.Done;
begin
  inherited Done;
  MSDisposeObj(flds);
end;

procedure TGSSectEng.PrintData(DC: HDC; margins: TRect);
{- print the gram stain data from the gathered specimen (set in SetPrintData) }
var
  p1, p2    : array[0..100] of char;
  y, cy     : integer;
  tm        : TTextMetric;
  oldFont   : HFont;

  procedure PrintIt(fld: PGSFldDef); far;
  {- print a specific field }
  var
    r     : TRect;
    pstr  : array[0..200] of char;
  begin
    CheckFont(logFont, dc, font, fld^.opts);

    r.left:= margins.left + sectRect.left + fld^.org.x;
    {- if the field is to the left or above the section it will be moved into
       the section }
    if r.left < lLimit then
      r.left:= lLimit;
    r.top:= margins.top + sectRect.top + fld^.org.y;
    if r.top < tLimit then
      r.top:= tLimit;
    r.right:= r.left + fld^.ext.x;
    r.bottom:= r.top + fld^.ext.y;

    {- see if field needs to be cliped }
    if r.left > rLimit then
      exit
    else if r.right > rLimit then
      r.right:= rLimit;

    if r.top > bLimit then
      exit
    else if r.bottom > bLimit then
      r.bottom:= bLimit;

(*    {- if field borders were ever to be used, the following line would display them }*)
(*    Rectangle(dc, r.left - LTpPX, r.top - LTpPY, r.right, r.bottom);*)

    strcopy(pstr, '');
    if fld^.fldType = 0 then  {- if field is freetext then the text is in fld^.buf }
    begin
      if (fld^.buf <> nil) and (StrLen(fld^.buf) > 0) then
        StrLCopy(pstr, fld^.buf, sizeof(pstr)-1)
    end
    else
    begin {- otherwise, the field data must be extracted }
      {- is it a field label or a field value (1 or 2) ? }
      if position^.data^.gsInfo <> nil then
      begin
        with position^.data^.gsInfo^ do
        begin
          case fld^.fldType of
            1   :     {- label }
              GetFieldName(fld^.slotNum, pstr, sizeof(pstr)-1);
            2   :     {- abbreviation }
              GetFieldAbbr(fld^.slotNum, pstr, sizeof(pstr)-1);
            3   :     {- value }
              GetField(fld^.slotNum, pstr, sizeof(pstr)-1);
          end;
        end;
      end;
    end;
    if strlen(pstr) > 0 then
      PrintText(dc, pstr, r, DT_WORDBREAK + DT_NOPREFIX);
  end;

begin
  inherited PrintData(dc, margins);
  if position^.data <> nil then
  begin
    oldFont:= SelectObject(dc, font);
    flds^.ForEach(@PrintIt);
    SelectObject(dc, oldFont);
  end;
end;

{=========================== ISOLATE SUMMARY SECTION ========================}

{--------------------------------------------------------[ TIsoSumSectEng ]--}

constructor TIsoSumSectEng.Init(aSectSeq: TSeqNum);
var
  db      : PDBFile;
  seq     : TSeqNum;
begin
  inherited Init(aSectSeq);
  db:= nil;

  if sectEngInitError = 0 then
  begin
    db:= New(PDBFile, Init(DBRptIsoSumFile, '', dbOpenNormal));
    if db = nil then
      sectEngInitError:= Eng_CantOpenIsoSumSectFile;
  end;

  if sectEngInitError = 0 then
  begin
    {- read the definition from the database }
    if db^.dbc^.GetSeq(db^.dbr, aSectSeq) then
    begin
      {- got the section definition (in db^.dbr), now read section def }
      db^.dbr^.GetField(DBRptIsoSumFont, @rFont, sizeof(rFont));
      db^.dbr^.GetField(DBRptIsoSumOptions, @opts, sizeof(opts));
      db^.dbr^.GetField(DBRptIsoSumSize, @sectRect, sizeof(sectRect));

      db^.dbr^.GetField(DBRptIsoSumSeparators, @separators, sizeof(separators));
      db^.dbr^.GetField(DBRptIsoSumHeaders, @headers, sizeof(headers));
      db^.dbr^.GetField(DBRptIsoSumULHeader, @underlineHeader, sizeof(underlineHeader));
      db^.dbr^.GetField(DBRptIsoSumIsoFlds, @isoFlds, sizeof(isoFlds));
      db^.dbr^.GetField(DBRptIsoSumFldWidth, @fldWidth, sizeof(fldWidth));
    end
    else
      sectEngInitError:= Eng_CantFindIsoSumSect;
  end;

  MSDisposeObj(db);

  if sectEngInitError <> 0 then
  begin
    Done;
    fail;
  end
  else
    PrepareForPrinting;
end;

procedure TIsoSumSectEng.PrintData(DC: HDC; margins: TRect);
{- print the demographics data from the gathered specimen (set in SetPrintData) }
var
  p1, p2    : array[0..100] of char;
  oldFont   : HFont;
  cy        : integer;
  tm        : TTextMetric;
  fldCount  : integer;    {- number of selected isolate fields }

  procedure DrawSummaryHeader;
  var
    r     : TRect;
    k     : integer;
    j     : integer;
    oPen  : HPen;
  begin
    r.left:= margins.left + sectRect.left;
    r.top:= margins.top + sectRect.top;
    r.right:= 0;
    r.bottom:= r.top + cy;

    if headers then
    begin
      for k:= 1 to fldCount do
      begin
        {- check for cliping inside of section }
        if r.left > rLimit then
          break;
        with position^.data^.isoInfo^ do
        begin
          GetFieldName(isoFlds[k], p1, sizeof(p1)-1);
          r.right:= r.left + fldWidth[k];
          if r.right > rLimit then
            r.right:= rLimit;
          PrintText(dc, p1, r, DT_SINGLELINE or DT_NOPREFIX);
          if underLineHeader and (k = fldCount) then
          begin
            MoveTo(dc, margins.left + sectRect.left, r.bottom);
            LineTo(dc, r.right, r.bottom);
          end;
          r.left:= r.right + 1;
        end;
      end;
    end;

    if separators >= 0 then
    begin
      if position^.data^.isoInfo^.GetNumIsolates = 0 then
        exit;

      oPen:= SelectObject(dc, CreatePen(separators, 0, RGB(0,0,0)));
      j:= lLimit;
      r.top:= tLimit;
      r.bottom:= bLimit;

      if opts.border = -1 then  {- if no border was specified, then draw a line }
      begin                     {  to the left of the first column. Otherwise, }
        MoveTo(dc, j, r.top);   {  it's unneccessary to do so }
        LineTo(dc, j, r.bottom);
      end;

      for k:= 1 to fldCount do
      begin
        if j > rLimit then
          break;
        Inc(j, fldWidth[k]);
        MoveTo(dc, j, r.top);
        LineTo(dc, j, r.bottom);
      end;
      DeleteObject(SelectObject(dc, oPen));
    end;
  end;

  procedure PrintIsolates;
  var
    y     : integer;
    idx   : integer;
    fld   : integer;
    r     : TRect;
  begin
    r.left:= lLimit;
    r.top:= tLimit;
    if headers then   {- account for header line }
      Inc(r.top, cy);
    r.right:= 0;
    r.bottom:= r.top + cy;

    with position^.data^.isoInfo^ do
    begin
      for idx:= 0 to GetNumIsolates - 1 do
      begin
        SetIsoIndex(idx); {- set up iso index of desired isolate }
        for fld:= 1 to fldCount do
        begin
          if r.left > margins.left + sectRect.left + sectRect.right then
            break;
          GetField(isoFlds[fld], p1, sizeof(p1)-1);
          r.right:= r.left + fldWidth[fld];
          if r.right > rLimit then
            r.right:= rLimit;
          PrintText(dc, p1, r, DT_SINGLELINE or DT_NOPREFIX);
          r.left:= r.right + 1;
        end;
        Inc(r.top, cy);
        r.bottom:= r.top + cy;
        r.left:= lLimit;
        if r.top > margins.top + sectRect.top + sectRect.bottom then
          break;  {- past the section rectangle boundaries }
        if r.bottom > bLimit then
          r.bottom:= bLimit;
      end;
    end;
  end;

begin
  inherited PrintData(dc, margins);
  if position^.data <> nil then
  begin
    oldFont:= SelectObject(dc, font);
    GetTextMetrics(dc, tm);
    cy:= PointsToLTwips((rFont.ptSize div 10)+ ExtraPointSize);
    fldCount:= isoFlds[0];    {- field count is in zero'th item }

    DrawSummaryHeader;
    PrintIsolates;

    SelectObject(dc, oldFont);
  end;
end;

{======================= ISOLATE COMPRESSED SECTION =========================}

{  This section makes use of the position array as follows:
   posArray[0] - indicates the vertical position in the data (drug)
   posArray[1] - indicates the horizontal position in the data (isolate)

   Vertically the data to be printed is controlled by drugList. DrugList contains
   all drugs for all isolates in the proper sort order.

                  .--Horizontal position
                  V
                  001 | 002 | 003 | 004 ...
                -----------------------
  vertical->| 0 |
  (drugs)   | 1 |
            | 2 |
            |...|

  The section first prints vertically until all drugs for the current set of
  isolates are printed. Then, the current isolate index is bumped up to the
  next set of isolates (if any).
}

type
  {- this object is used by the compressed detail section to collect ALL drug
     information for ALL isolates on a specimen.  }
  PIsoCompObj = ^TIsoCompObj;
  TIsoCompObj = object(TObject)
    drugSeq   : TSeqNum;
    sortIdx   : longint;
    isoIdx    : integer; {- index of first iso in gather object containing this
                            drug. This is used so drug info can be retrieved
                            at any time no matter what isolate is current in
                            the section. }
    sortIdxStr: array[0..25] of char; {- used by collection for sorting }
    constructor Init(aDrugSeq, aSortIdx: longint; aFirstIsoIdx: integer);
  end;

  PIsoCompDrugList = ^TIsoCompDrugList;
  TIsoCompDrugList = object(TSortedCollection)
    function KeyOf(item: Pointer): Pointer; virtual;
    function Compare(Key1, Key2: Pointer): integer; virtual;
  end;

constructor TIsoCompObj.Init(aDrugSeq, aSortIdx: longint; aFirstIsoIdx: integer);
var
  pstr  : array[0..25] of char;
begin
  inherited Init;
  drugSeq:= aDrugSeq;
  sortIdx:= aSortIdx;
  isoIdx:= aFirstIsoIdx;
  Str(sortIdx, pstr);
  LeftPad(sortIdxStr, pstr, '0', sizeof(sortIdxStr)-1);
end;

function TIsoCompDrugList.KeyOf(item: Pointer): pointer;
begin
  keyOf:= @PIsoCompObj(item)^.sortIdxStr;
end;

function TIsoCompDrugList.Compare(Key1, Key2: pointer): integer;
begin
  Compare:= StrComp(PChar(key1), PChar(key2));
end;

{--------------------------------------------------------[ TIsoCompSectEng ]--}

constructor TIsoCompSectEng.Init(aSectSeq: TSeqNum);
var
  db      : PDBFile;
  seq     : TSeqNum;
begin
  inherited Init(aSectSeq);
  db:= nil;
  drugList:= nil;

  if sectEngInitError = 0 then
  begin
    db:= New(PDBFile, Init(DBRptIsoCmpFile, '', dbOpenNormal));
    if db = nil then
      sectEngInitError:= Eng_CantOpenIsoCompSectFile;
  end;

  if sectEngInitError = 0 then
  begin
    {- read the definition from the database }
    if db^.dbc^.GetSeq(db^.dbr, aSectSeq) then
    begin
      {- got the section definition (in db^.dbr), now read section def }
      db^.dbr^.GetField(DBRptIsoCompFont, @rFont, sizeof(rFont));
      db^.dbr^.GetField(DBRptIsoCompOptions, @opts, sizeof(opts));
      db^.dbr^.GetField(DBRptIsoCompSize, @sectRect, sizeof(sectRect));

      db^.dbr^.GetField(DBRptIsoCompSep,        @separators, sizeof(separators));
      db^.dbr^.GetField(DBRptIsoCompHeaders,    @headers, sizeof(headers));
      db^.dbr^.GetField(DBRptIsoCompULHeader,   @underlineHeader, sizeof(underlineHeader));
      db^.dbr^.GetField(DBRptIsoCompInterps,    @interps, sizeof(interps));
      db^.dbr^.GetField(DBRptIsoCompMICs,       @MICs, sizeof(MICs));
      db^.dbr^.GetField(DBRptIsoCompIsoPerSect, @isoPerSect, sizeof(isoPerSect));
      db^.dbr^.GetField(DBRptIsoCompIsoWidth,   @isoWidth, sizeof(isoWidth));
      db^.dbr^.GetField(DBRptIsoCompMicWidth,   @micWidth, sizeof(micWidth));
      db^.dbr^.GetField(DBRptIsoCompInterpWidth,@interpWidth, sizeof(interpWidth));
      db^.dbr^.GetField(DBRptIsoCompMicOrder,   @micOrder, sizeof(micOrder));
      db^.dbr^.GetField(DBRptIsoCompMicSpace,   @micSpace, sizeof(micSpace));
      db^.dbr^.GetField(DBRptIsoCompDrugDisp,   @drugDisp, sizeof(drugDisp));
      db^.dbr^.GetField(DBRptIsoCompDrugWidth,  @drugWidth, sizeof(drugWidth));
      if not interps then
        interpWidth:= 0;
      if not MICs then
        micWidth:= 0;
    end
    else
      sectEngInitError:= Eng_CantFindIsoCompSect;
  end;

  drugList:= New(PIsoCompDrugList, Init(50, 50));

  MSDisposeObj(db);

  if sectEngInitError <> 0 then
  begin
    Done;
    fail;
  end
  else
    PrepareForPrinting;
end;

destructor TIsoCompSectEng.Done;
begin
  inherited Done;
  MSDisposeObj(drugList);
end;

procedure TIsoCompSectEng.BuildDrugList;
{- from the current data, build a collection of all drugs for all isolates on
   the specimen, in drug sort idx order }
var
  isoIdx    : integer;
  res       : PGatherRes;
  seq       : TSeqNum;
  sort      : longint;
  micIdx    : integer;

  function Matches(obj: PIsoCompObj): boolean; far;
  {- assumes seq to match is in [seq] }
  begin
    Matches:= obj^.drugSeq = seq;
  end;

begin
  drugList^.FreeAll;
  with position^.data^.isoInfo^ do
  begin
    for isoIdx:= 0 to GetNumIsolates - 1 do
    begin
      SetIsoIndex(isoIdx);
      res:= GetResults;

      {- insert all drugs for isolate in the list. }
      for micIdx:= 0 to res^.NumMICResults - 1 do
      begin
        if not res^.GetMICSuppressed(micIdx) then  {- make sure to suppress when needed}
        begin
          seq:= res^.GetMICDrugSeq(micIdx);
          {- make sure the drug seq has not already been collected. If so, there
            is no need to re-add it to the list }
          if drugList^.FirstThat(@Matches) = nil then
          begin
            sort:= res^.GetMICDrugSortIdx(micIdx);
            drugList^.Insert(New(PIsoCompObj, Init(seq, sort, isoIdx)));
          end;
        end;
      end;
    end;
  end;
end;

procedure TIsoCompSectEng.PopPosition;
begin
  inherited PopPosition;
  BuildDrugList;
end;

function TIsoCompSectEng.GetCurIso: integer;
{- return the current iso index waiting to be printed in the gather object }
begin
  GetCurIso:= position^.posArray[1];
end;

function TIsoCompSectEng.GetCurDrug: integer;
{- return the current drug index waiting to be printed (vertical positioning) }
begin
  GetCurDrug:= position^.posArray[0];
end;

procedure TIsoCompSectEng.SetCurIso(newIso: integer);
{- set the current isolate index }
begin
  position^.posArray[1]:= newIso;
end;

procedure TIsoCompSectEng.SetCurDrug(newDrug: integer);
{- set the current drug index }
begin
  position^.posArray[0]:= newDrug;
end;

procedure TIsoCompSectEng.SetPrintData(aGatherSpec: PGatherSpec);
begin
  inherited SetPrintData(aGatherSpec);
  BuildDrugList;
end;

procedure TIsoCompSectEng.PrintData(DC: HDC; margins: TRect);
{- print the demographics data from the gathered specimen (set in SetPrintData) }
var
  p1, p2    : array[0..100] of char;  {- these two MUST be the same size !}
  oldFont   : HFont;
  cy        : integer;
  tm        : TTextMetric;
  firstDrug : integer;  {- starting drug for this print only }
  firstIso  : integer;  {- starting iso for this print only }

  procedure PrintHeader;
  {- draw the header for the section }
  var
    curIso    : integer;
    r         : TRect;
    isos      : integer;
    res       : PGatherRes;
  begin
    if headers then
    begin
      r.left:= lLimit + drugWidth;
      r.top:= tLimit;
      r.right:= r.left + drugWidth;
      r.bottom:= r.top + cy;
      isos:= 0;
      curIso:= firstIso;
      with position^.data^.isoInfo^ do
      begin
        while (isos < isoPerSect) and (curIso < GetNumIsolates) do
        begin
          SetIsoIndex(curIso);
          res:= GetResults;
          if res^.NumMICResults > 0 then
          begin
            Inc(isos);
            GetField(DBIsoIso, p1, sizeof(p1)-1);
            r.right:= r.left + isoWidth;
            if r.right > rLimit then
              r.right:= rLimit;
            PrintText(dc, p1, r, DT_CENTER or DT_SINGLELINE or DT_NOPREFIX);
            r.left:= r.right+1;

            {- see if clipping needs to occur }
            if r.left > rLimit then
              break;
          end;
          Inc(curIso);
        end;
        if underLineHeader and (isos > 0) then
        begin
          r.left:= lLimit;
          r.right:= r.left + drugWidth + (isoWidth * isos);
          if r.right > rLimit then
            r.right:= rLimit;
          MoveTo(dc, r.left, r.bottom);
          LineTo(dc, r.right, r.bottom);
        end;
      end;
    end;
  end;

  procedure PrintDrugs;
  {- print drugs down the left of the section and update the current
     vertical position (posArray[0]) for the section }
  var
    r         : TRect;
    drugIdx   : integer;
    obj       : PIsoCompObj;
    isoIdx    : integer;
    res       : PGatherRes;
    k         : integer;
    oPen      : HPen;
  begin
    drugIdx:= GetCurDrug;

    if drugIdx >= drugList^.count then
      exit;

    {- set up print rectangle }
    r.left:= lLimit;
    r.top:= tLimit;
    if headers then   {- account for header line }
      Inc(r.top, cy);
    r.right:= r.left + drugWidth;
    r.bottom:= r.top + cy;

    if separators >= 0 then
    begin
      oPen:= SelectObject(dc, CreatePen(separators, 0, RGB(0,0,0)));
      begin
        MoveTo(dc, r.left, tLimit);
        LineTo(dc, r.left, bLimit);
      end;
      MoveTo(dc, r.right, tLimit);
      LineTo(dc, r.right, bLimit);
      DeleteObject(SelectObject(dc, oPen));
    end;

    {- scan through the drug list printing the drug names. }
    repeat
      with position^.data^ do
      begin
        obj:= drugList^.At(drugIdx);
        isoInfo^.SetIsoIndex(obj^.isoIdx);  {- use the iso index of first iso that
                                         contains the drug just to get a res obj
                                         that contains the drug. It's just needed
                                         here to get the name anyway }
        res:= isoInfo^.GetResults;
        {- find the index into the gather object for the drug seq number }
        k:= res^.FindDrugIndex(obj^.drugSeq);
        if k >= 0 then
        begin
          {- drug display type (0=name, 1-abbrev, 2=alternate }
          case drugDisp of
            0   : res^.GetMICDrugName(k, p1, sizeof(p1)-1);
            2   : res^.GetMICDrugAlt(k, p1, sizeof(p1)-1);
            else
              res^.GetMICDrugAbbr(k, p1, sizeof(p1)-1);
          end;
          PrintText(dc, p1, r, DT_SINGLELINE or DT_NOPREFIX);
          Inc(r.top, cy);
          r.bottom:= r.top + cy;
        end;
        Inc(drugIdx);
      end;
    until (drugIdx >= drugList^.count) or (r.bottom > bLimit);

    {- adjust posArray to the next drug that still will need to be printed. }
    SetCurDrug(drugIdx);
  end;

  procedure PrintIsos;
  var
    r         : TRect;
    idx       : integer;
    drugSeq   : TSeqNum;
    curIso    : integer;
    res       : PGatherRes;
    resIdx    : integer;
    left      : integer;
    top       : integer;
    right     : integer;
    count     : integer;
    oPen      : HPen;

    procedure PrintRes(s1, s2: PChar; dt1, dt2: word; w1, w2: integer);
    {- print result (s1 first using dt1 as DT_xx flag then printing s2 using
       dt2 as DT_xx flag) }
    begin
      r.left:= left;
      r.right:= r.left + w1;
      if r.right > right then
        r.right:= right;
      if w1 > 0 then
        PrintText(dc, s1, r, dt1 or DT_SINGLELINE or DT_NOPREFIX);

      if w1 > 0 then
        r.left:= r.right + micSpace;

      if w2 > 0 then
      begin
        r.right:= r.left + w2;
        if (r.left < right) then
        begin
          if r.right > right then
            r.right:= right;
          PrintText(dc, s2, r, dt2 or DT_SINGLELINE or DT_NOPREFIX);
        end;
      end;
    end;

  begin
    left:= lLimit + drugWidth;
    r.left:= left;
    top:= tLimit;
    if headers then   {- account for header line }
      Inc(top, cy);
    r.top:= top;
    r.right:= 0;
    r.bottom:= r.top + cy;
    right:= rLimit; {- rightmost limit }
    count:= 0;
    with position^.data^.isoInfo^ do
    begin
      curIso:= firstIso; {- first iso needing printing }
      {- repeat for number of isolates specified per section }
      while (count < isoPerSect) and (curIso < GetNumIsolates) and
            (r.left < right) do
      begin
        SetIsoIndex(curIso);   {- set cur iso in gather object }
        res:= GetResults;      {- get results for current isolate }
        if res^.NumMICResults > 0 then  {- only print isos with drugs }
        begin
          idx:= firstDrug;  {- start at position in drug list that has not been printed yet}
          {- go through drugs on current isolate }
          repeat  {- for each isolate }
            drugSeq:= PIsoCompObj(drugList^.At(idx))^.drugSeq; {- get the drug seq for the current slot}

            {- find the gatherRes index for the current drug }
            resIdx:= res^.FindDrugIndex(drugSeq);
            if resIdx >= 0 then
            begin
              res^.GetMICResultMapped(resIdx, p1, p2, sizeof(p1));
              if micOrder = 0 then  {- mic first then interp }
                PrintRes(p1, p2, DT_RIGHT, DT_LEFT, micWidth, interpWidth)
              else {- interp then mic }
                PrintRes(p2, p1, DT_LEFT, DT_RIGHT, interpWidth, micWidth);
            end;
            Inc(r.top, cy);
            r.bottom:= r.top + cy;
            Inc(idx);
          until (idx >= drugList^.count) or (r.bottom > bLimit);
          if separators >= 0 then
          begin
            oPen:= SelectObject(dc, CreatePen(separators, 0, RGB(0,0,0)));
            MoveTo(dc, left + isoWidth, margins.top + sectRect.top);
            LineTo(dc, left + isoWidth, margins.top + sectRect.top + sectRect.bottom);
            DeleteObject(SelectObject(dc, oPen));
          end;
          Inc(count);
          Inc(left, isoWidth);
          r.top:= top;
          r.bottom:= r.top + cy;
        end;
        Inc(curIso);
      end;

      {- if all results were printed vertically, then set the current iso
          to the next set for section }
      if GetCurDrug >= drugList^.count then
      begin
        SetCurIso(curIso);  {- flag ready for the next set of isos }
        SetCurDrug(0);      {- reset current drug for next isolate set }
      end;
    end;
  end;

begin
  inherited PrintData(dc, margins);
  if position^.data <> nil then
  begin
    firstDrug:= GetCurDrug;
    firstIso:= GetCurIso;
    if firstDrug < drugList^.count then
    begin
      oldFont:= SelectObject(dc, font);

      cy:= PointsToLTwips((rFont.ptSize div 10) + ExtraPointSize);

      PrintDrugs; { must be called before printIsos }
      PrintIsos;
      PrintHeader;

      SelectObject(dc, oldFont);
    end;
  end;

  if (sectRect.bottom < cy) or (headers and (sectRect.bottom < cy*2)) then
    position^.dataPrinted:= true  {- no room to print data }
  else
    position^.dataPrinted:= (GetCurIso >= position^.data^.isoInfo^.GetNumIsolates) or
                            (drugList^.count = 0);
end;

{======================= ISOLATE FULL DETAIL SECTION =========================}

{  This section makes use of the position array as follows:
   posArray[0..maxIsosPerSect-1]
     - indicates the vertical position in the data for each isolate
       that exist in the current section.
       There are MaxIsoPerSect in a section so posArray reserves
       position 0 through MaxIsoPerSect-1 as position indicators
       for each isolate in the section.
   posArray[30]
     - indicates the horizontal position in the data (isolate)

  The section first prints vertically until all drugs for the current set of
  isolates are printed. Then, the current isolate index is bumped up to the
  next set of isolates (if any).
}

{--------------------------------------------------------[ TIsoFullSectEng ]--}

constructor TIsoFullSectEng.Init(aSectSeq: TSeqNum);
var
  db      : PDBFile;
  seq     : TSeqNum;
begin
  inherited Init(aSectSeq);
  db:= nil;

  if sectEngInitError = 0 then
  begin
    db:= New(PDBFile, Init(DBRptIsoFulFile, '', dbOpenNormal));
    if db = nil then
      sectEngInitError:= Eng_CantOpenIsoFullSectFile;
  end;

  if sectEngInitError = 0 then
  begin
    {- read the definition from the database }
    if db^.dbc^.GetSeq(db^.dbr, aSectSeq) then
    begin
      {- got the section definition (in db^.dbr), now read section def }
      db^.dbr^.GetField(DBRptIsoFullFont, @rFont, sizeof(rFont));
      db^.dbr^.GetField(DBRptIsoFullOptions, @opts, sizeof(opts));
      db^.dbr^.GetField(DBRptIsoFullSize, @sectRect, sizeof(sectRect));

      db^.dbr^.GetField(DBRptIsoFullIsoPerSect, @isoPerSect, sizeof(isoPerSect));
      db^.dbr^.GetField(DBRptIsoFullSeparators, @separators, sizeof(separators));
      db^.dbr^.GetField(DBRptIsoFullIsoWidth,   @isoWidth, sizeof(isoWidth));
      db^.dbr^.GetField(DBRptIsoFullDetail,     @detail, sizeof(detail));
      db^.dbr^.GetField(DBRptIsoFullIsoFlds,    @isoFlds, sizeof(isoFlds));
      db^.dbr^.GetField(DBRptIsoFullLblWidth,   @lblWidth, sizeof(lblWidth));
      db^.dbr^.GetField(DBRptIsoFullValWidth,   @valWidth, sizeof(valWidth));
      db^.dbr^.GetField(DBRptIsoFullSubSpace,   @subSpacing, sizeof(subSpacing));
    end
    else
      sectEngInitError:= Eng_CantFindIsoFullSect;
  end;

  MSDisposeObj(db);

  if sectEngInitError <> 0 then
  begin
    Done;
    fail;
  end
  else
    PrepareForPrinting;
end;

function TIsoFullSectEng.GetCurIso: integer;
{- return the current iso index waiting to be printed in the gather object }
begin
  GetCurIso:= position^.posArray[30];
end;

function TIsoFullSectEng.GetCurDrug(column: integer): integer;
{- return the current drug index waiting to be printed (vertical positioning) }
begin
  GetCurDrug:= position^.posArray[column];
end;

procedure TIsoFullSectEng.SetCurIso(newIso: integer);
{- set the current isolate index }
begin
  position^.posArray[30]:= newIso;
end;

procedure TIsoFullSectEng.SetCurDrug(column, newDrug: integer);
{- set the current drug index }
begin
  position^.posArray[column]:= newDrug;
end;

procedure TIsoFullSectEng.PrintData(DC: HDC; margins: TRect);
{- print the demographics data from the gathered specimen (set in SetPrintData) }
var
  p1, p2    : array[0..100] of char;  {- these two MUST be the same size !}
  oldFont   : HFont;
  cy        : integer;
  firstDrug : integer;  {- starting drug for this print only }
  firstIso  : integer;  {- starting iso for this print only }

  procedure PrintHeader(var r: TRect; iso: integer);
  {- assumes r.left and r.top have been set appropriately }
  var
    res     : PGatherRes;
    k       : integer;
    left    : integer;
    right   : integer;
  begin
    {- no need to print anything if widths are zero }
    if (lblWidth = 0) and (valWidth = 0) then
      exit;

    right:= margins.left + sectRect.left + sectRect.right;
    r.bottom:= r.top + cy;
    left:= r.left;
    with position^.data^.isoInfo^ do
    begin
      for k:= 1 to isoFlds[0] do   {- for each select isolate field }
      begin
        r.left:= left;
        if lblWidth > 0 then
        begin
          GetFieldName(isoFlds[k], p1, sizeof(p1)-1);
          r.right:= r.left + lblWidth;
          if r.right > right then
            r.right:= right;
          PrintText(dc, p1, r, DT_SINGLELINE or DT_NOPREFIX);
          Inc(r.left, lblWidth);
        end;

        if left > right then
          continue;

        if valWidth > 0 then
        begin
          GetField(isoFlds[k], p1, sizeof(p1)-1);
          r.right:= r.left + valWidth;
          if r.right > right then
            r.right:= right;
          PrintText(dc, p1, r, DT_SINGLELINE or DT_NOPREFIX);
        end;
        Inc(r.top, cy);
        r.bottom:= r.top + cy;
      end;
      {- draw line under isolate fields }
      if (lblWidth <> 0) or (valWidth <> 0) then
      begin
        MoveTo(dc, left, r.top);
        LineTo(dc, left + isoWidth, r.top);
      end;
    end;
  end;

  procedure PrintIsos;
  var
    r       : TRect;
    curIso  : integer;
    curDrug : integer;
    left    : integer;  {- section left limit }
    bottom  : integer;  {- section bottom limit }
    right   : integer;  {- section right limit }
    res     : PGatherRes;
    k       : integer;
    dtOpt   : word;
    column  : integer;
    oPen    : HPen;
  begin
    left:= margins.left + sectRect.left;
    r.left:= left;
    if (separators >= 0) and (opts.border = -1) then
    begin
      oPen:= SelectObject(dc, CreatePen(separators, 0, RGB(0,0,0)));
      MoveTo(dc, left, margins.top + sectRect.top);
      LineTo(dc, left, margins.top + sectRect.top + sectRect.bottom);
      DeleteObject(SelectObject(dc, oPen));
    end;
    bottom:= margins.top + sectRect.top + sectRect.bottom;
    right:= margins.left + sectRect.left + sectRect.right;
    column:= 0;
    curIso:= firstIso;
    with position^.data^.isoInfo^ do
    begin
      while (column < isoPerSect) and (curIso < GetNumIsolates) and
            (r.left < right) do
      begin
        SetIsoIndex(curIso);
        res:= GetResults;
        if res^.NumMICResults > 0 then
        begin
          curDrug:= GetCurDrug(column);
          r.left:= left;
          r.top:= margins.top + sectRect.top;
          PrintHeader(r, curIso);
          r.left:= left;
          if curDrug < res^.NumMICResults then
          begin
            repeat  {- for drugs on this isolate }
              if not res^.GetMICSuppressed(curDrug) then
              begin
                {- print sub column information }
                for k:= 1 to 5 do
                begin
                  {- Detail Types: 0-drug abbr, 1-drug name, 2-Alt drug abbr, 3-MIC, 4- Interp }
                  if detail[k].visible then
                  begin
                    r.right:= r.left + detail[k].width;
                    if r.right > right then
                      r.right:= right;
                    strcopy(p1, '');
                    dtOpt:= DT_SINGLELINE or DT_NOPREFIX;
                    case detail[k].detailType of
                      0   : res^.GetMICDrugAbbr(curDrug, p1, sizeof(p1)-1);
                      1   : res^.GetMICDrugName(curDrug, p1, sizeof(p1)-1);
                      2   : res^.GetMICDrugAlt(curDrug, p1, sizeof(p1)-1);
                      3   : begin  {- MIC }
                              res^.GetMICResultMapped(curDrug, p1, p2, sizeof(p1)-1);
                              dtOpt:= dtOpt or DT_RIGHT;
                            end;
                      4   : res^.GetMICResultMapped(curDrug, p2, p1, sizeof(p1)-1); {- NCCLS }
                    end;
                    PrintText(dc, p1, r, dtOpt);
                    r.left:= r.right + subSpacing;
                    if r.left >= right then
                      break;
                  end;
                end;
                Inc(r.top, cy);
                r.bottom:= r.top + cy;
                r.left:= left;
              end;
              Inc(curDrug);
            until (r.bottom > bottom) or (curDrug >= res^.NumMICResults);
            SetCurDrug(column, curDrug);
          end;
          Inc(column);
          Inc(left, isoWidth);
          if separators >= 0 then
          begin
            if left < right then
            begin
              oPen:= SelectObject(dc, CreatePen(separators, 0, RGB(0,0,0)));
              MoveTo(dc, left, margins.top + sectRect.top);
              LineTo(dc, left, margins.top + sectRect.top + sectRect.bottom);
              DeleteObject(SelectObject(dc, oPen));
            end;
          end;
        end;
        Inc(curIso);
      end;
    end;
  end;

  procedure CheckComplete;
  {- see if the section is done printing }
  var
    drugsDone   : boolean;  {- drugs printed for current iso set }
    column      : integer;
    curIso      : integer;
    res         : PGatherRes;
  begin
    curIso:= firstIso;
    column:= 0;
    drugsDone:= true;
    with position^.data^.isoInfo^ do
    begin
      while (column < isoPerSect) and (curIso < GetNumIsolates) and (drugsDone) do
      begin
        SetIsoIndex(curIso);
        res:= GetResults;
        if res^.NumMICResults > 0 then
        begin
          if GetCurDrug(column) < res^.NumMICResults then
            drugsDone:= false;
          Inc(column);
        end;
        Inc(curIso);
      end;
      if drugsDone then   {- all drugs printed for current isolate set }
      begin
        if curIso < GetNumIsolates then {- more iso sets to print }
        begin
          SetCurIso(curIso);
          {- reset all drug positions for the new iso set }
          for column:= 0 to MaxIsoPerSect - 1 do
            SetCurDrug(column, 0);
          position^.dataPrinted:= false;
        end
        else
          position^.dataPrinted:= true; {- all data has been printed }
      end
      else
        position^.dataPrinted:= false;
    end;
  end;

begin
  inherited PrintData(dc, margins);
  if position^.data <> nil then
  begin
    firstIso:= GetCurIso;
    firstDrug:= GetCurDrug(firstIso);

    oldFont:= SelectObject(dc, font);
    cy:= PointsToLTwips((rFont.ptSize div 10) + ExtraPointSize);

    PrintIsos;

    SelectObject(dc, oldFont);
  end;

  CheckComplete;
end;

{---------------------------------------------------------------[ TRptDef ]--}

constructor TRptDef.Init(aRptSeq: TSeqNum);
{- initialize a report definition object. This object loads the specified
   report format and all section definitions for that report format.
   If this object fails to initialize, the error code will be in engLastDefInitError }
begin
  inherited Init;
  engLastDefInitError:= 0;
  sects:= nil;
  FillChar(pageDef, sizeof(pageDef), 0);
  StrCopy(rptName, '');

  sects:= New(PCollection, Init(20, 20));
  if sects = nil then
    engLastDefInitError:= Eng_CantAllocSectCollection;

  if engLastDefInitError = 0 then
    engLastDefInitError:= LoadReportDef(aRptSeq);

  if engLastDefInitError <> 0 then
  begin
    Done;
    fail;
  end;
end;

destructor TRptDef.Done;
begin
  inherited Done;
  MSDisposeObj(sects);
end;

function TRptDef.LoadReportDef(aRptSeq: TSeqNum): integer;
{- loads a report definition into [rptDef] and loads all section data into
   [sects] }
var
  db    : PDBFile;
  ret   : integer;
begin
  ret:= 0;
  db:= New(PDBFile, Init(DBRPTFmtsFile, '', dbOpenNormal));
  if db <> nil then
  begin
    {- first read the definition from the report format file }
    if db^.dbc^.GetSeq(db^.dbr, aRptSeq) then
    begin
      {- fill the [rptDef] structure }
      db^.dbr^.GetFieldAsStr(DBRPTFmtName, rptName, sizeof(rptName)-1);
      db^.dbr^.GetField(DBRptFmtSize, @pageDef.size, sizeof(pageDef.size));
      db^.dbr^.GetField(DBRptFmtMargins, @pageDef.margin, sizeof(pageDef.margin));
      db^.dbr^.GetField(DBRptFmtRptPerPage, @pageDef.rptsPerPage, sizeof(pageDef.rptsPerPage));
      db^.dbr^.GetField(DBRptFmtOrient, @pageDef.orient, sizeof(pageDef.orient));
      db^.dbr^.GetField(DBRptFmtPaperIdx, @pageDef.paperIdx, sizeof(pageDef.paperIdx));
      db^.dbr^.GetField(DBRptFmtSpacing, @pageDef.spacing, sizeof(pageDef.spacing));

      {- now load all sections for this report format }
      ret:= LoadSections(aRptSeq);
    end
    else
      ret:= eng_CantLoadRptFmt;
    MSDisposeObj(db);
  end
  else
    ret:= dbLastOpenError;

  LoadReportDef:= ret;
end;

function TRptDef.LoadSections(aRptSeq: TSeqNum): integer;
{- load the report sections into the [sects] collections and fill the rptDef
   field }
var
  seq   : TSeqNum;
  row   : longint;
  db    : PDBFile;
  sectID: integer;
  ret   : integer;

  function AddSection(aSect: PSectEngineBase): integer;
  begin
    if aSect = nil then
      AddSection:= sectEngInitError
    else
    begin
      sects^.Insert(aSect);
      AddSection:= 0;
    end;
  end;

begin
  ret:= 0;
  {- load the report section definitions from the RptSect cross ref file }
  db:= New(PDBFile, Init(DBRPTSectsFile, '', dbOpenNormal));
  if db = nil then
    ret:= dbLastOpenError
  else
  begin
    db^.dbr^.PutField(DBRptSectReportRef, @aRptSeq);
    if db^.dbc^.GetFirstContains(db^.dbr) then
    begin
      {- scan through the sections and load each definition. Store the definition
          in the sects collection }
      repeat
        db^.dbr^.GetField(DBRptSectSectID, @sectID, sizeof(sectID));
        db^.dbr^.GetField(DBRptSectSectRef, @seq, sizeof(seq));
        case sectID of
          SectDemoFreeID:
            ret:= AddSection(New(PDemoSectEng, Init(seq)));
          SectGSID:
            ret:= AddSection(New(PGSSectEng, Init(seq)));
          SectIsoSumID:
            ret:= AddSection(New(PIsoSumSectEng, Init(seq)));
          SectIsoCompID:
            ret:= AddSection(New(PIsoCompSectEng, Init(seq)));
          SectIsoFullID:
            ret:= AddSection(New(PIsoFullSectEng, Init(seq)));
          else
            ret:= Eng_UnknownSectionID;
        end;
      until not db^.dbc^.GetNextContains(db^.dbr);
    end
    else
      ret:= Eng_NoSectsDefined;
    MSDisposeObj(db);
  end;
  LoadSections:= ret;
end;


BEGIN
  engLastDefInitError:= 0;
END.

{- rcf }
