(****************************************************************************


pnlview.inc

produced by Borland Resource Workshop


*****************************************************************************)

const

  DLG_BIOTYPEEDIT     = $71A0;
  DLG_AS4SAVE         = $71A1;
  DLG_SEARCH          = $71A2;
  DLG_MICEDIT         = $71A3;
  DLG_WAPANEL_VIEW    = $71A4;
  DLG_MANUALCALL      = $71A5;

	BTN_SEARCHOK	=	51;
	BTN_SEARCHCLEAR	=	52;
	BTN_READ	=	51;
	BTN_NEXT	=	52;
	BTN_BIOTYPE	=	53;
	BTN_VIEW	=	54;
	BTN_SAVE	=	55;
	BTN_PRINT	=	56;
	BTN_SEARCH	=	57;
	BTN_RESOLVE	=	58;
	BTN_ABORT_PNL	=	59;

  IDC_SEARCHSPECLBL   = 200;
  IDC_SEARCHSPECEDIT  = 201;
  IDC_SEARCHCDLBL     = 202;
  IDC_SEARCHCDEDIT    = 203;
  IDC_SEARCHISOLBL    = 204;
  IDC_SEARCHISOEDIT   = 205;
  IDC_SEARCHTGLBL     = 206;
  IDC_SEARCHTGEDIT    = 207;

  IDC_DRUGNAME        = 200;
  IDC_DIL_LIST        = 201;

	IDC_SAVEID	=	301;
	IDC_SAVEMIC	=	302;
	IDC_SAVE_STATUS	=	303;

  IDC_BIOTYPE         = 400;
  IDC_BIOCHEMLIST     = 401;
  IDC_ORGNUMLIST      = 402;
  IDC_ORGLIST         = 403;
  IDC_ORGPROBLIST     = 404;
  IDC_FERMENTER       = 405;
  IDC_NONFERMENTER    = 406;
  IDC_STREP           = 407;
  IDC_MICRO           = 408;

  IDC_MICLIST         = 410;
  IDC_SPECNUM         = 411;
  IDC_ISONUM          = 412;
  IDC_PANELTYPE       = 413;
  IDC_STOREDORG       = 414;
  IDC_ORGANISM        = 415;
  IDC_HIDDENSTATUS    = 416;
  IDC_WAPNL_STATUS    = 417;

  IDC_PROMPTTEST      = 401;
  IDC_MANUALCALLGROUP = 402;
  IDC_NEGATIVE        = 403;
  IDC_POSITIVE        = 404;

  IDC_LBL1            = 501;
  IDC_LBL2            = 502;
  IDC_LBL3            = 503;
  IDC_LBL4            = 504;
  IDC_LBL5            = 505;
  IDC_LBL6            = 506;
  IDC_LBL7            = 507;
  IDC_LBL8            = 508;
  IDC_LBL9            = 509;

  IDS_DATASTORED      = $71A1;
  IDS_UNDETBIOT       = $71A2;
  IDS_VRBIOT          = $71A3;
  IDS_INVBIOT         = $71A4;
  IDS_TEST_REQUIRED   = $71A5;
  IDS_ISO_NOT_FOUND   = $71A7;
  IDS_SPEC_NOT_FOUND  = $71A8;
  IDS_CANNOT_EDIT     = $71A9;
  IDS_TEST_GROUP      = $71AA;

	IDS_PANEL_NOT_FOUND	=	29094;
	DLG_ORGENTRY	=	29094;
	IDC_ORGID	=	201;
	DIALOG_1	=	1;
	IDC_Footnotes	=	60;
	IDC_SpclChars	=	61;
	IDC_SpclCharText	=	63;
	IDC_FootNoteText	=	62;
	IDC_AddTest1	=	409;
	IDC_AddTest2	=	410;
	IDC_AddTest3	=	411;
	IDC_TitleAddTest1	=	412;
	IDC_TitleAddTest2	=	413;
	IDC_TitleAddTest3	=	414;
	IDC_AddTestTitle	=	415;
