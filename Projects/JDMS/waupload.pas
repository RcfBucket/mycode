{----------------------------------------------------------------------------}
{  Module Name  : WAUPLOAD.PAS                                               }
{  Programmer   : EJ                                                         }
{  Date Created : 06/28/95                                                   }
{                                                                            }
{  Purpose -                                                                 }
{  This module provides automatic upload functionality for the               }
{  WA INTERFACE.                                                             }
{                                                                            }
{  Assumptions -                                                             }
{  None.                                                                     }
{                                                                            }
{  Revision History -                                                        }
{                                                                            }
{  Version  Date      Who    Did What                                        }
{  -------  --------  ------ ----------------------------------------------- }
{  1.00     06/28/95  EJ     Initial release.                                }
{                                                                            }
{----------------------------------------------------------------------------}

unit WAUpload;

INTERFACE

procedure UploadResults;


IMPLEMENTATION

uses
  Objects,
  Strings,
  Bits,
  MScan,
  DBIDs,
  DBFile,
  DBTypes,
  ESBL,
  ResLib,
  Strsw,
  ListLib,
  Biotype,
  IDLib,
  Screens,
  Trays,
  Pnlview,
  WAUtil,
  WAGPIB,
  DMSDebug,
  WinAPI,
  WinProcs;

{$I WAMAIN.INC}


type
  PWAObject  = ^TWAObject;
  TWAObject = object(TObject)
    specDB        : PDBFile;
    isoDB         : PDBFile;
    isordDB       : PDBFile;
    tstGrp        : PDBFile;
    orgDB         : PDBFile;
    wapDB         : PDBFile;
    waexlistDB    : PDBFile;
    resList       : PResults;
    listObj       : PListObject;
    exceptions    : PExceptions;
    biotypeObj    : PWABiotypeObject;
    isoOrgRef     : TSeqNum;
    tstGrpID      : array[0..32] of char;
    setFamily     : word;
    orgNum        : integer;
    panelID       : integer;
    waSlot        : integer;
    panelStatus   : byte;
    panelType     : byte;
    flags         : integer;
    flagsObj      : PPanelFlags;
    micData       : PMIArray;
    posFlags      : TPanelBitmap;
    virtualTests  : TPanelBitmap;
    MICsLoaded    : boolean;
    IDReady       : boolean;
    panelInWA     : boolean;
    wellFile      : Text;
    numXTests     : integer;
    recordLocked  : boolean;
    {}
    constructor Init;
    destructor Done; virtual;
    procedure InitVars;
    function  DataIsAvailable: boolean;
    function  InitPanelData: boolean;
    function  GetPanelData: integer;
    function  AutoStorePanelData: boolean;
    function  PanelMICsInquiry: integer;
    function  VirtualTestsInquiry: integer;
    function  PanelResultsInquiry: integer;
    function  GetPanelUsedFlag: boolean;
    procedure ClearWAExList;
    procedure SetExceptions;
    procedure SetException(exceptionNum: integer);
    procedure SetWAFlag(flgNum: integer);
    function  ProblemsOccurred: boolean;
    function  EvaluateStatus: boolean;
    function  EvaluateFlags: boolean;
    procedure CheckExtraTests;
    procedure ClearPanelID;
    function  LockIt: boolean;
    procedure PreScreenResults(Organism: Integer);
  end;

constructor TWAObject.Init;
begin
  inherited Init;
  specDB     := nil;
  isoDB      := nil;
  isordDB    := nil;
  tstGrp     := nil;
  orgDB      := nil;
  wapDB      := nil;
  waexlistDB := nil;
  resList    := nil;
  listObj    := nil;
  exceptions := nil;
  biotypeObj := nil;
  flagsObj   := nil;
  recordLocked := FALSE;

  specDB := New(PDBFile, Init(DBSPECFile, '', DBOpenNormal));
  isoDB := New(PDBFile, Init(DBISOFile, '', DBOpenNormal));
  isordDB := New(PDBFile, Init(DBISOORDFile, '', DBOpenNormal));
  tstGrp := New(PDBFile, Init(DBTstGrpFile, '', DBOpenNormal));
  orgDB := New(PDBFile, Init(DBOrgFile, '', DBOpenNormal));
  wapDB := New(PDBFile, Init(DBWAPNLFile, '', DBOpenNormal));
  waexlistDB := New(PDBFile, Init(DBWAEXFile, '', dbOpenNormal));
  resList := New(PResults, Init);
  listObj := New(PListObject, Init);
  exceptions := New(PExceptions, Init);

  {$IFOPT I+}
    {$DEFINE chki}
  {$ENDIF}
  {$I-}
  if IOResult <> 0 then ; { clear it }
  Assign(wellFile, 'DataList.TXT');
  Reset(wellFile);
  if (IOResult <> 0) or
     (specDB = nil) or (isoDB = nil) or (isordDB = nil) or
     (tstGrp = nil) or (orgDB = nil) or (wapDB = nil) or
     (waexlistDB = nil) or (resList = nil) or (listObj = nil) then
  begin
    Done;
    Fail;
  end;
  {$IFDEF chki}
    {$I+}
    {$UNDEF chki}
  {$ENDIF}
end;

destructor TWAObject.Done;
begin
  {$IFOPT I+}
    {$DEFINE chki}
  {$ENDIF}
  {$I-}
  Close(wellFile);
  if IOResult <> 0 then ; { clear it }
  {$IFDEF chki}
    {$I+}
    {$UNDEF chki}
  {$ENDIF}

  MSDisposeObj(flagsObj);
  MSDisposeObj(biotypeObj);
  MSDisposeObj(exceptions);
  MSDisposeObj(listObj);
  MSDisposeObj(resList);
  MSDisposeObj(waexlistDB);
  MSDisposeObj(wapDB);
  MSDisposeObj(orgDB);
  MSDisposeObj(tstGrp);
  MSDisposeObj(isordDB);
  MSDisposeObj(isoDB);
  MSDisposeObj(specDB);
  inherited Done;
end;

procedure TWAObject.InitVars;
begin
  if recordLocked then
  begin
    isoDB^.dbc^.UnlockRec(isoDB^.dbr);
    recordLocked := FALSE;
  end;
  panelID := 0;
  IDReady := FALSE;
  MICsLoaded := FALSE;
  panelInWA := FALSE;
  orgNum := -1;
  numXTests := 0;
end;

function TWAObject.DataIsAvailable: boolean;
var
  WASTICmd : PWASTICmd;
  WADVICmd : PWADVICmd;
  slot     : byte;
begin
  DataIsAvailable := FALSE;
  WASTICmd := New(PWASTICmd, Init(WAID));
  if WASTICmd^.SendWAMessage then
  begin
    if TestBit(WASTICmd^.recvMsg^.stiBits, DataAvailable) then
    begin
      WADVICmd := New(PWADVICmd, Init(WAID));
      if WADVICmd^.SendWAMessage then
      begin
        if WADVICmd^.WAStatNum = 0 then
        begin
          WADVICmd^.GetParams(panelID, slot, panelStatus, panelType, flags);
          waSlot := slot;
          paneltype := panelType + 1;  { 1-based for trays methods }
          if InitPanelData then
            DataIsAvailable := TRUE;
        end;
      end;
      MSDisposeObj(WADVICmd);
    end;
  end;
  MSDisposeObj(WASTICmd);
end;

function TWAObject.InitPanelData: boolean;
var
  seq      : TSeqNum;
  orgPChar : array[0..15] of char;
  code     : integer;
begin
  InitPanelData := FALSE;
  tstGrpID[0] := #0;
  repeat { once }
    if not wapDB^.dbr^.PutField(DBWAPNLID, @panelID) or
       not wapDB^.dbc^.GetEQ(wapDB^.dbr) or
       not wapDB^.dbr^.GetField(DBWAPNLIsoOrdRef, @seq, SizeOf(TSeqNum)) or
       not isordDB^.dbc^.GetSeq(isordDB^.dbr, seq) or
       not isordDB^.dbr^.GetField(DBISOORDIsoRef, @seq, SizeOf(TSeqNum)) or
       not isoDB^.dbc^.GetSeq(isoDB^.dbr, seq) or
       not isoDB^.dbr^.GetField(DBISOSpecRef, @seq, SizeOf(TSeqNum)) or
       not specDB^.dbc^.GetSeq(specDB^.dbr, seq) or
       not isoDB^.dbr^.GetField(DBISOOrgRef, @isoOrgRef, sizeof(isoOrgRef)) or
       not isoDB^.dbr^.GetField(DBISOSetFamily, @setfamily, sizeof(setFamily)) or
       not isordDB^.dbr^.GetField(DBISOORDTstGrpRef, @seq, SizeOf(TSeqNum)) or
       not tstGrp^.dbc^.GetSeq(tstGrp^.dbr, seq) then
    begin
      SetException(IDS_READ_INIT_FAILED);
      Break;
    end;
    tstGrp^.dbr^.GetFieldAsStr(DBTSTGrpID, tstGrpID, 32);
    resList^.traysObj^.LoadTray(tstGrpID);
    resList^.LoadResults(isoDB^.dbr);
    if (isoOrgRef <> 0) then
      if orgDB^.dbc^.GetSeq(orgDB^.dbr, isoOrgRef) then
      begin
        orgDB^.dbr^.GetFieldAsStr(DBOrgOrg, orgPChar, 15);
        Val(orgPChar, orgNum, code);
      end;
    MSDisposeObj(biotypeObj);
    biotypeObj := New(PWABiotypeObject, Init(@resList^.traysObj^.tfsRec));
    if biotypeObj = nil then
    begin
      SetException(IDS_READ_INIT_FAILED);
      Break;
    end;
    MSDisposeObj(flagsObj);
    flagsObj := New(PPanelFlags, Init(flags));
    if not LockIt then
    begin
      SetException(IDS_READ_LOCK_FAILED);
      Break;
    end;
    recordLocked := TRUE;
    InitPanelData := TRUE;
  until TRUE;
end;

procedure TWAObject.PreScreenResults(Organism: Integer);
var i          : integer;
    dName      : array[0..63] of char;
    tempResList: PResults;
    E157Applies: Boolean;
    ESBLApplies: Boolean;
    BioStr   : array[0..32] of char;

begin
  exceptions^.PossibleE157:=false;
  exceptions^.PossibleESBL:=false;
  if ScreenOn(E157Screen) or ScreenOn(ESBLScreen) then
  begin
    TempResList:=New(PResults,Init);
    TempResList^.LoadResults(isoDB^.dbr);
    TempResList^.TraysObj^.LoadTrayByNum(ResList^.TraysObj^.tfsrec.traynum);

    for i := 1 to TempResList^.TraysObj^.DrugCnt do
        with TempResList^.TraysObj^.dilList[i] do
        begin
          StrLCopy(dName, drugName, sizeof(dName)-1);
          TrimTrail(dName, dName, ' ', sizeof(dName)-1);
          TempResList^.SetResult(dname, 'MIC', drugDils[MICData[i]], True);
        end;

    if IDReady
       then StrPCopy(bioStr, biotypeObj^.biotype)
       else StrCopy(bioStr, '');

    TempResList^.SetResult('.BIO', 'BIO', BioStr, true);

    E157Applies:=E157Criterion(Organism,ExtractSet(SetFamily),TempResList);
    ESBLApplies:=not E157Applies and ESBLCriterion(Organism,TempResList);

    exceptions^.PossibleE157:=E157Applies;
    exceptions^.PossibleESBL:=ESBLApplies and (not AutoResolveOn);
    MSDisposeObj(TempResList);
  end;
end;

function TWAObject.GetPanelData: integer;
{**************************************************************************}
{*  This method assumes that a PSI/DVI command was issued to the W/A for  *}
{*  this panel and the status and flags are read.                         *}
{**************************************************************************}
var
  ret      : integer;
begin
  ret := 9999;
  exceptions^.ClearAllExceptions;
  ClearWAExList;
  FillChar(micData, SizeOf(micData), #0);
  FillChar(posFlags, SizeOf(TPanelBitmap), #0);
  FillChar(virtualTests, SizeOf(TPanelBitmap), #0);

  if not ProblemsOccurred then
  begin
    ret := 0;
    case panelStatus of
      MICsAvailable:
        begin
          ret := PanelMICsInquiry;
          if ret = 0 then
            ret := VirtualTestsInquiry;
        end;
      reagentsAdded:
        begin
          ret := 9999;
          { wait for process to be complete }
        end;
      processComplete:
        begin
          { if there are MICs or extra tests on this panel, get em }
          ret := PanelMICsInquiry;
          if ret = 0 then
            ret := VirtualTestsInquiry;
          if ret = 0 then
            ret := PanelResultsInquiry;

          if (OrgNum<0)
             then exceptions^.NeedOrg :=
                  (resList^.traysObj^.tfsRec.idRec = -1)
             else PreScreenResults(OrgNum);
        end;
      end;
    if ret = 0 then
      CheckExtraTests;
    SetExceptions;
  end
  else if panelStatus = PanelMissing then
  begin
    exceptions^.AbortedMissing := TRUE;
    SetExceptions;
  end;
  GetPanelData := ret;
end;

function TWAObject.AutoStorePanelData: boolean;
var
  storeID,
  storeMICs,
  storeETs,
  canceled,
  IDOnly,
  MICOnly  : boolean;
  bioStr   : array[0..32] of char;

  function OKToStoreMICs: boolean;
  begin
    OKToStoreMICs := FALSE;

    if panelStatus <> processComplete then
      Exit;

    if not (IDOnly or MICsLoaded) then
      Exit;

    with exceptions^ do
      if NeedSts or NeedGms or NeedTFG or NeedBLac or  NeedOxac or
         NeedOxid or NeedOrg or PossibleE157 or PossibleESBL then
        Exit;

    with flagsObj^ do
      if SkippedWells or DelayedRead or ISFGrowth or
         BadSterileWell or InvSterileWell then
        Exit;

    OKToStoreMICs := TRUE;  { this will get reset later if not in batch mode }
  end;

  function OKToStoreExtraTests: boolean;
  begin
    OKToStoreExtraTests := FALSE;

    if panelStatus <> processComplete then
      Exit;

    if (orgNum = -1) or resList^.traysObj^.TrayFlo then
      Exit;

    if numXTests = 0 then
      Exit;

    with exceptions^ do
      if NeedSts or NeedOxac or NeedBLac or NeedGmS or
         NeedTFG or NeedOrg or PossibleE157 or PossibleESBL then
        Exit;

    OKToStoreExtraTests := TRUE;
  end;

  function OKToStoreID: boolean;
  begin
    OKToStoreID := FALSE;

    if panelStatus <> processComplete then
      Exit;

    if not (MICOnly or IDReady) then
      Exit;

    with exceptions^ do
      if NeedOxid or NoID or IDTooLow or NeedIndole or
         UndefinedBiotype or NeedOrg or PossibleE157 or PossibleESBL then
        Exit;

    with flagsObj^ do
      if SkippedWells or DelayedRead or ISFGrowth or
         BadDCB or BadOFB or BadNPC or BadBNAC or
         BadSterileWell or InvSterileWell or NoDispense then
        Exit;

    OKToStoreID := TRUE;
  end;

begin
  AutoStorePanelData := FALSE;  { assume something went wrong }

  IDOnly:=resList^.TraysObj^.DrugCnt=0;
  MICOnly:=resList^.traysObj^.tfsRec.idRec=-1;

  storeMICs := OKToStoreMICs;
  storeETs := OKToStoreExtraTests;
  storeID := OKToStoreID;

  { Do not store ANYTHING unless both ID and MIC are ready }
  { Holds would cause prelim results to be stored for MICs!}
  if not (storeID and storeMICS) then
    Exit;

  canceled := FALSE;

  if IDReady then
    StrPCopy(bioStr, biotypeObj^.biotype)
  else
    StrCopy(bioStr, '');

  if SaveIsolateTests(nil, FALSE, FALSE,
                      storeID and not MICOnly,
                      storeMICs and not IDOnly,
                      storeETs, canceled,
                      wellFile, orgDB, isoOrgRef, bioStr,
                      PMICData(@micData), isoDB, specDB, resList, @posFlags) then
  begin
    AutoStorePanelData := TRUE;
    if ({combo panels} storeID and storeMICs) or
       ({ID only panels} storeID and IDOnly) or
       ({MIC only panels} StoreMICs and MIConly) then
      if not canceled and (panelStatus = processComplete) then
        ClearPanelID;
  end
  else
  begin
    SetException(IDS_SAVE_FAILED);
  end;
end;

function TWAObject.PanelMICsInquiry: integer;
var
  WACmd     : PWAPMICmd;
  pId       : integer;
  slot      : byte;
  count     : byte;
begin
  PanelMICsInquiry := 0;
  WACmd := New(PWAPMICmd, Init(WAID));
  WACmd^.SetParams(panelID);
  if WACmd^.SendWAMessage then
  begin
    if WACmd^.WAStatNum = 0 then
    begin
      WACmd^.GetParams(pId, slot, count, micData);
      { the MICs are stored after the PRI call because the org is needed for
        calculating some of the interpretations }
      MICsLoaded := TRUE;
    end
    else
      PanelMICsInquiry := WACmd^.WAStatNum;
  end
  else
    PanelMICsInquiry := WACmd^.WAErrNum;
  MSDisposeObj(WACmd);
end;

function TWAObject.VirtualTestsInquiry: integer;
var
  WACmd     : PWAPEICmd;
begin
  VirtualTestsInquiry := 0;
  FillChar(virtualTests, SizeOf(TPanelBitmap), #0);
  if not flagsObj^.VirtualTests then
    Exit;
  WACmd := New(PWAPEICmd, Init(WAID));
  WACmd^.SetParams(panelID);
  if WACmd^.SendWAMessage then
  begin
    if waCmd^.WAStatNum = 0 then
      Move(WACmd^.recvMsg^.priBits[1], virtualTests[0], SizeOf(PRIArray))
    else
      VirtualTestsInquiry := WACmd^.WAStatNum;
  end
  else
    VirtualTestsInquiry := WACmd^.WAErrNum;
  MSDisposeObj(WACmd);
end;

function TWAObject.PanelResultsInquiry: integer;
{**************************************************************************}
{ This procedure assumes a PMICmd has been executed                        }
{**************************************************************************}
Const
  RNID3_IDRec  = 20;        {Identifies BDesc record number for RNID3 panels}
var
  WACmd       : PWAPRICmd;
  pId         : integer;
  slot        : byte;
  class       : byte;
  bits        : PRIArray;
  gdBiotype   : boolean;
  aSet        : byte;
  pstr        : array [0..9] of char;
begin

  {Get the PDL var Did_Indole value for this panel}
  BioTypeObj^.IndWasDispd := False;
  If (resList^.traysObj^.tfsRec.IDRec = RNID3_IDRec) then
    BioTypeObj^.IndWasDispd := IndoleDispensedFlag(PanelID);

  PanelResultsInquiry := 0;
  WACmd := New(PWAPRICmd, Init(WAID));
  WACmd^.SetParams(panelID);
  if WACmd^.SendWAMessage then
  begin
    if WACmd^.WAStatNum = 0 then
    begin
      WACmd^.GetParams(pId, slot, class, bits);
      Move(bits, posFlags, 12);
      { if this is an MIC only panel then exit }
      if resList^.traysObj^.tfsRec.idRec = -1 then
      begin
        MSDisposeObj(WACmd);
        Exit;
      end;

      aSet := ExtractSet(setFamily);
      setFamily := MakeSetFamily(aSet, class);
      isoDB^.dbr^.PutField(DBISOSetFamily, @setFamily);

      gdBiotype := GetBiotype(nil, FALSE, class, resList,
                              @posFlags, nil, @virtualTests,
                              PMICData(@micData), biotypeObj);
      if gdBiotype then
      begin
        { determine the organism and store it }
        if not biotypeObj^.GetTopID(orgNum, nil, nil, nil) then
        begin
          if not biotypeObj^.Identify^.InvalidBiotype then
            exceptions^.UndefinedBiotype := TRUE;
        end
        else
        begin
          IDReady := TRUE;
          if biotypeObj^.Identify^.IDList.NumOrgs <= 0 then
          begin
            exceptions^.NoID := TRUE;
          end
          else
          begin
            with biotypeObj^.Identify^.IDList do
              if (PROBS.P^[1] <= 85) then
                exceptions^.IDTooLow := TRUE
              else
              begin
                Str(orgNum, pstr);
                orgDB^.dbr^.ClearRecord;
                orgDB^.dbr^.PutFieldAsStr(DBORGOrg, pstr);
                if orgDB^.dbc^.GetEQ(orgDB^.dbr) then
                  isoOrgRef := orgDB^.dbr^.GetSeqValue;
              end;
          end;
        end
      end
      else
      begin
        if (biotypeObj^.oxidaseBitNum <> -1) and
           TestBioBit(biotypeObj^.neededResults, biotypeObj^.oxidaseBitNum) then
          exceptions^.NeedOxid := TRUE; { Needs Oxidase result }
        if (biotypeObj^.indoleBitNum <> -1) and
           TestBioBit(biotypeObj^.neededResults, biotypeObj^.indoleBitNum) then
          exceptions^.NeedIndole := TRUE;
      end;
    end
    else
      PanelResultsInquiry := WACmd^.WAStatNum;
  end
  else
    PanelResultsInquiry := WACmd^.WAErrNum;
  MSDisposeObj(WACmd);
end;

function TWAObject.GetPanelUsedFlag: boolean;
var
  flg    : boolean;
begin
  flg := FALSE;
  if wapDB <> nil then
    wapDB^.dbr^.GetField(DBWAPNLUsedFlag, @flg, SizeOf(flg));
  GetPanelUsedFlag := flg;
end;

procedure TWAObject.ClearWAExList;
var
  seq  : TSeqNum;
begin
  waexlistDB^.dbr^.ClearRecord;
  seq := isordDB^.dbr^.GetSeqValue;
  waexlistDB^.dbr^.PutField(DBWAEXIsoOrdRef, @seq);
  while waexlistDB^.dbc^.GetEQ(waexlistDB^.dbr) do
  begin
    waexlistDB^.dbc^.DeleteRec(waexlistDB^.dbr);
    if waexlistDB^.dbc^.dbErrorNum <> 0 then
      Break;
  end;
end;

procedure TWAObject.SetExceptions;
begin
  if exceptions^.ExceptionsExist then
  begin
    if exceptions^.NeedStS then
      SetException(IDS_EXC_NEEDS_STS);
    if exceptions^.NeedGmS then
      SetException(IDS_EXC_NEEDS_GMS);
    if exceptions^.NeedTFG then
      SetException(IDS_EXC_NEEDS_TFG);
    if exceptions^.NeedBLac then
      SetException(IDS_EXC_NEEDS_BLAC);
    if exceptions^.NeedOxac then
      SetException(IDS_EXC_NEEDS_OXAC);
    if exceptions^.NeedOxid then
      SetException(IDS_EXC_NEEDS_OXID);
    if exceptions^.NeedOrg then
      SetException(IDS_EXC_NEEDS_ORG);
    if exceptions^.NoID then
      SetException(IDS_EXC_NO_ID);
    if exceptions^.IDTooLow then
      SetException(IDS_EXC_ID_TOO_LOW);
    if exceptions^.NeedIndole then
      SetException(IDS_EXC_NEEDS_INDOLE);
    if exceptions^.UndefinedBiotype then
      SetException(IDS_UNDEF_BIOTYPE);
    if exceptions^.AbortedMissing then
      SetException(IDS_EXC_ABORTED_MISSING);
    if exceptions^.Hours42 then
      SetException(IDS_EXC_HOURS_42);
    if exceptions^.PossibleE157 then
      SetException(IDS_EXC_PossibleE157);
    if exceptions^.PossibleESBL then
      SetException(IDS_EXC_PossibleESBL);
  end;
end;

procedure TWAObject.SetException(exceptionNum : integer);
var
  tower : integer;
  slot  : byte;
begin
  if GetPanelUsedFlag then
  begin
    GetTowerSlot(waSlot, slot, tower);
    waexlistDB^.dbr^.PutField(DBWAEXPanelID, @panelID);
    waexlistDB^.dbr^.PutField(DBWAEXTower, @tower);
    tower := slot;  { for some reason slot is a byte in GetTowerSlot and an int in the DB }
    waexlistDB^.dbr^.PutField(DBWAEXSlot, @tower);
    waexlistDB^.dbr^.PutFieldAsStr(DBWAEXPanelName, tstGrpID);
    waexlistDB^.dbr^.PutField(DBWAEXStrNum, @exceptionNum);
    waexlistDB^.dbr^.ClearField(DBWAEXString);
    waexlistDB^.dbc^.InsertRec(waexlistDB^.dbr);
  end;
end;

procedure TWAObject.SetWAFlag(flgNum: integer);
begin
  if flgNum <> IDS_FLG_IDS_AVAIL_RAPID then
    SetException(flgNum);
end;

function TWAObject.ProblemsOccurred: boolean;
begin
  ProblemsOccurred := FALSE;
  if EvaluateStatus then
    ProblemsOccurred := TRUE;
  if EvaluateFlags then
    ProblemsOccurred := TRUE;
end;

function TWAObject.EvaluateStatus: boolean;
var
  statNum    : integer;
begin
  if (panelStatus = 0) or
     (panelStatus = MICsAvailable) or
     (panelStatus = ReagentsAdded) or
     (panelStatus = ProcessComplete) or
     ((panelStatus = LampFailure) and resList^.traysObj^.TrayFlo) then
    EvaluateStatus := FALSE
  else
  begin
    EvaluateStatus := TRUE;
    if (panelStatus = PanelUnrecognized) or
       (panelStatus = AbortedPanel) or
       (panelStatus = PanelMissing) or
       (panelStatus = DuplicatePanelId) or
       (panelStatus = LampFailure) then
    begin
      GetStatNum(panelStatus, statNum, flagsObj);
      SetException(statNum);
    end;
  end;
end;

function TWAObject.EvaluateFlags: boolean;
Const
  StaphFamily = 2;
Var
  AFam      : Integer;
  EFlags    : boolean;
begin
  EFlags := FALSE;
  if flagsObj^.flagVal <> 0 then
  begin
    if flagsObj^.powerFailure then SetWaFlag(IDS_FLG_POWER_FAIL);
    if flagsObj^.ISFGrowth then SetWaFlag(IDS_FLG_ISF_GROWTH);
    if flagsObj^.BadSterileWell then SetWaFlag(IDS_FLG_BAD_STERILE_WELL);
    if flagsObj^.SkippedWells then SetWaFlag(IDS_FLG_SKIP_WELL);
    if flagsObj^.VerifyPanelId then SetWaFlag(IDS_FLG_VERIFY_ID);
    panelInWA := flagsObj^.panelInWA;
    if not panelInWA then SetWaFlag(IDS_FLG_PANEL_NOT_IN_WA);
    if flagsObj^.slowGrower then
      if panelStatus <> processComplete then
      begin
        exceptions^.Hours42 := TRUE;
        if panelStatus <> MICsAvailable then
          SetWaFlag(IDS_FLG_SLOW_GROWER);
      end;
    if flagsObj^.possibleMRSASE then
      if panelStatus <> processComplete then
      Begin
        aFam := ExtractFamily(setFamily);
        If AFam = StaphFamily then
          SetWaFlag(IDS_FLG_MRSA_SE)
        Else
          SetWaFlag(IDS_FLG_VRE_Hold);
      End;
    if flagsObj^.DelayedRead then SetWaFlag(IDS_FLG_DELAYED_READ);
    if flagsObj^.InvSterileWell then SetWaFlag(IDS_FLG_INV_STERILE_WELL);
    if flagsObj^.IDAvailOnRapid then SetWaFlag(IDS_FLG_IDS_AVAIL_RAPID);
    if flagsObj^.NoDispense then SetWaFlag(IDS_FLG_NO_DISPENSE);

    {** if rapid anaerobe panel or rapid yeast panels **}
    if (StrComp(tstGrpID, 'RAID') = 0) or
       (StrComp(tstGrpID, 'RYID') = 0) then
    begin
      if flagsObj^.BadDCB then
      begin
        flagsObj^.BadNPC := TRUE;
        SetWaFlag(IDS_FLG_BAD_NPC);
        EFlags := TRUE;
      end
      else if flagsObj^.BadOFB then
      begin
        flagsObj^.BadBNAC := TRUE;
        SetWaFlag(IDS_FLG_BAD_BNAC);
        EFlags := TRUE;
      end
    end
    else
    begin
      if flagsObj^.BadDCB then SetWaFlag(IDS_FLG_BAD_DCB);
      if flagsObj^.BadOFB then SetWaFlag(IDS_FLG_BAD_OFB);
    end;
  end;
  EvaluateFlags := EFlags;
end;

procedure TWAObject.CheckExtraTests;
var
  testID    : array [0..32] of char;
  xtraTbl   : XtraType;
  i         : integer;
begin
  if (orgNum <> -1) and not resList^.traysObj^.TrayFlo then
  begin
    resList^.traysObj^.GetXtra(xtraTbl, numXTests);
    for i := 1 to numXTests do
    begin
      if GetETResult(i, testID, xtraTbl, resList, orgNum,
                     wellFile, @posFlags, PMICData(@micData)) = 3 then
      begin
        if StrComp(testID, '.StS') = 0 then
          exceptions^.NeedStS := TRUE
        else if StrComp(testID, '.BL') = 0 then
          exceptions^.NeedBLac := TRUE
        else if StrComp(testID, '.GmS') =  0 then
          exceptions^.NeedGmS := TRUE
        else if StrComp(testID, '.TFG') = 0 then
          exceptions^.NeedTFG := TRUE;
      end;
    end;  {for i := 1 to numxtests}
  end;  {if not stored results to follow}
end;

procedure TWAObject.ClearPanelID;
var
  flg  : boolean;
begin
  if panelID <> 0 then
  begin
    { Clear the used flag in wapDB for isolate order
      Cannot delete it because may need it back to re-read from WA }
    flg := FALSE;
    wapDB^.dbr^.PutField(DBWAPNLUsedFlag, @flg);
    wapDB^.dbc^.UpdateRec(wapDB^.dbr);
  end;
end;

function TWAObject.LockIt: boolean;
begin
  LockIt := isoDB^.dbc^.GetLock(isoDB^.dbr);
end;


procedure UploadResults;
var
  waObj   : PWAObject;
  ret     : integer;
begin
  waObj := New(PWAObject, Init);
  if waObj = nil then
    Exit;

  waObj^.InitVars;
  while waObj^.DataIsAvailable do
  begin
    ret := waObj^.GetPanelData;
    if ret = 0 then { no errors from the W/A or GPIB }
      waObj^.AutoStorePanelData;
    waObj^.InitVars;
(*  HandleEvents(0); *)
  end;
  MSDisposeObj(waObj);
end;


END.
