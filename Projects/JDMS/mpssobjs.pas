{----------------------------------------------------------------------------}
{  Module Name  : MPSSOBJS.PAS                                               }
{  Programmer   : EJ                                                         }
{  Date Created : 04/04/95                                                   }
{                                                                            }
{  Purpose -                                                                 }
{  This module is used to define objects used internally by MPSS sub-system. }
{                                                                            }
{  Assumptions -                                                             }
{  None.                                                                     }
{                                                                            }
{  Revision History -                                                        }
{                                                                            }
{  Version  Date      Who    Did What                                        }
{  -------  --------  ------ ----------------------------------------------- }
{  1.00     04/04/95  EJ     Initial release                                 }
{                                                                            }
{----------------------------------------------------------------------------}

unit MPSSObjs;

INTERFACE

uses
  DBTypes,
  DBFile;

const
  MaxMPSSRules  = 100;

type
  FNString  = array [0..8] of char;

  TMPSSRel  = (MPSSAnd, MPSSOr, MPSSAndOrSpecial);
  TMPSSOp   = (MPSSEQ,MPSSNE,MPSSLT,MPSSGT,MPSSLE,MPSSGE,MPSSIN);

  PRules = ^TRules;
  TRules =
    array [0..MaxMPSSRules] of
      record
        AndOr        : TMPSSRel;
        LParen       : boolean;
        RParen       : boolean;
        FH           : PDBFile;    { EOL signified by FH=nil }
        FileDesc     : array [0..50] of char;
        FieldName    : array [0..32] of char;
        FieldNo      : word;
        FieldType    : TDBFldTypes;     {- type of field }
        TestRef      : TSeqNum;
        Operator     : TMPSSOp;
        Value        : array [0..255] of char;
        case integer of
        1:        {- used for initialization }
           (NumericValue  : array[0..9] of byte);
        2:        {- dbInteger,  2 bytes }
                  {- dbLongInt,  4 bytes }
                  {- dbWord,     unsigned 2 bytes }
                  {- dbBoolean,  1 byte boolean value }
                  {- dbByte,     1 byte }
                  {- dbShortInt, 1 byte unsigned }
                  {- dbChar,     1 byte (char) }
                  {- dbDate,     4 byte date }
                  {- dbTime,     4 byte time }
                  {- dbSeqRef,   sequence reference }
                  {- dbAutoInc   auto increment field }
           (lValue   : longint);
        3:        {- dbSingle,   4 byte real }
           (sValue   : single);
        4:        {- dbDouble,   8 byte real }
           (dValue   : double);
                  {- else no other internal format is needed for the following types }
                  {- dbString,   user specified length, length byte at beginning of string }
                  {- dbCode,     same as String but case insensitive }
                  {- dbZString,  zero terminated string of user defined length }
                  {- dbZCode,    same as ZString but case insensitive }
                  {- dbBlock,    user defined block of data }
      end;

  PFileEntry = ^TFileEntry;
  TFileEntry =
      record
        FileName     : FNString;
        FH           : PDBFile;    { EOL signified by FH=nil }
        FileDesc     : array [0..50] of char;
        Fresh        : boolean;
        LinkedBy     : word;
        LinkAssoc    : TAssocInfo;
      end;

  PFiles = ^TFiles;
  TFiles = array [1..10] of TFileEntry;

  PFields = ^TFields;
  TFields =
    array [1..250] of
      record
        FH           : PDBFile;    { EOL signified by FH=nil }
        FileDesc     : array [0..50] of char;
        FieldName    : array [0..32] of char;
        FieldNo      : word;
        FieldType    : TDBFldTypes;     {- type of field }
        OpsType      : integer;
        TestRef      : TSeqNum;
      end;

IMPLEMENTATION

END.
