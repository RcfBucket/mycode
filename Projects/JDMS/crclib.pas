unit CRCLib;

INTERFACE

uses
  WinDOS;

function GetDirCRC(crcDir: PChar; var CRC: word): boolean;
function GetFileCRC(fileName: PChar; var CRC: word): boolean;

IMPLEMENTATION

uses
  Strings;

{** uses BLOCKCRC.ASM **}
{$L BlockCRC}

const
  BufSize  = 65520;

type
  BufType = array[1..BufSize] of char;

function BlockCRC(oldCRC: word; p: pointer; blockLen: word): word; far; external;

function GetFileCRC(fileName: PChar; var CRC: word): boolean;
{- Compute a CRC for the specified file and returns whether the
   operation was successful or not.
   You must set CRC to 0 if you want a CRC *just* for the specified file.
   If you want to do a cumulative CRC for a bunch of files, pass in 0 for the
   first file, then pass the CRC from the previous file to the next file etc...}
var
  f         : file;
  OldAttr   : word;
  Buffer    : pointer;
  NumRead   : word;
begin
  GetFileCRC:= False;
  Assign(f, FileName);

  GetFAttr(f, OldAttr); {** save old file attribute so it can be reset **}
  if (DosError <> 0) then
    exit;

  SetFAttr(f, 0);             {** strip out file attributes **}
  if (DosError <> 0) then
    exit;

  {$I-}
  Reset(f, 1);
  {$I+}
  if (IOResult <> 0) then
  begin
    SetFAttr(f, OldAttr);     {** reset old file attribute before exiting **}
    exit;
  end;

  GetMem(buffer, bufSize);
  repeat
    {$I-}
    {** get next block to compute **}
    BlockRead(f, Buffer^, BufSize, NumRead);
    {$I+}
    if (IOResult <> 0) then
    begin
      Close(f);
      FreeMem(buffer, bufSize);
      exit;
    end
    else
      CRC:= BlockCRC(CRC, Buffer, NumRead);
  until (NumRead < BufSize);
  FreeMem(buffer, BufSize);
  Close(f);
  GetFileCRC:= True;
end;


{--------[GetDirCRC]--------}
function GetDirCRC(crcDir: PChar; var CRC: word): boolean;
var
  fileName  : array[0..100] of char;
  filePath  : array[0..100] of char;
  dirInfo   : TSearchRec;
  funStat   : boolean;

begin
  funstat := true;
  CRC := 0;

  StrCopy(filePath, crcDir);
  StrCat(filePath, '*.*');
  FindFirst(filePath, faReadOnly or faArchive, dirInfo);
  if DosError = 0 then
  Begin
    while (DosError = 0) and funStat do
    begin
      StrCopy(fileName,crcDir);
      StrCat(fileName, dirInfo.Name);
      funStat := GetFileCRC(fileName, CRC);
      FindNext(dirInfo);
    end;
  end
  else
    funStat := false;

  GetDirCRC := funStat;
end;

END.

