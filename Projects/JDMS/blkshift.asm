	NAME	BLKSH
;
;	Block shift left/right routine.
;

	ASSUME	CS:CODE,DS:DATA

DATA	SEGMENT	PUBLIC
DATA	ENDS

CODE	SEGMENT	PUBLIC

	public	blkshift

;
;	Procedure BlkShift( VAR Target : ByteArray; Size : Integer;
;						   Count : Integer );
;
;	This implementation limits COUNT to 0-7.
;
;	Shift SIZE bytes at TARGET^ by COUNT bits.  Positive counts
;	indicate left shifts, negative count indicate right shifts.
;	Shifting 0 bits has no effect.  Target[n] is considered to be
;	to the right of Target[n+1].
;
target  equ dword ptr [bp+10]
bsize equ word  ptr [bp+8]
count equ word  ptr [bp+6]
parmsz  equ 8

blkshift  proc  far
	push	bp		; address stack
	mov	bp,sp

	mov	cx,count	; get bit count
	les	di,target	; get target
	mov	dx,bsize	; get byte count
	cmp	cx,0
	jz	bs1		; out if shift 0
	js	bs2		; negative - shift right
			; positive - left shift
	sub	bl,bl
	and	cx,7		; max shift of 7
bsp1:
	mov	al,es:byte ptr [di]
	sub	ah,ah		; load zero filled byte into AX
	shl	ax,cl		; shift the byte
	or	al,bl		; add shift out of previous byte
	mov	bl,ah		; save carry out of this byte
	mov	es:byte ptr [di],al
	inc	di
	dec	dx		; dec byte count
	jnz	bsp1
	jmp	bs1		; exit if all bytes done
bs2:
			; negative - right shift
	sub	bl,bl
	neg	cx		; form actual shift count
	and	cx,7
	add	di,dx		; address of last byte
	dec	di
bsn1:
	mov	ah,es:byte ptr [di]
	sub	al,al		; get zero filled byte in AX
	shr	ax,cl
	or	ah,bl
	mov	bl,al
	mov	es:byte ptr [di],ah
	dec	di
	dec	dx		; dec byte count
	jnz	bsn1		; loop through all
bs1:
	pop	bp
	ret	parmsz		; cleanup & exit
blkshift	endp

CODE	ENDS

	END
