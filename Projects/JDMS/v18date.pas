unit v18Date;
{$A-}
INTERFACE

var
  ddlm   : byte;
  didx   : array [0..2] of byte;
  dbgn   : byte;
  dend   : byte;
  dprev  : byte;
  dthis  : byte;
  dnext  : byte;
  dday   : byte;
  dmon   : byte;
  dqtr   : byte;
  dyr    : byte;
  init87 : word;

function DateSet(dateFmt: String): boolean;
procedure DateUnpack(DateIn: Word; VAR DateOut: String; FourDig: Boolean);

IMPLEMENTATION


{$L unjulian}
{$L dateset}
function DateSet(DateFmt: string): boolean; external;


{$L dateunpk}
Procedure DateUnpk(DateIn: Word; VAR DateOut: String; FourDig: Boolean); near;
external;

Procedure DateUnpack(DateIn: Word; VAR DateOut: String; FourDig: Boolean);
begin
  DateUnpk(dateIn, dateOut, fourDig);
end;

END.
