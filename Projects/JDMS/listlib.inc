(****************************************************************************


listlib.inc

produced by Borland Resource Workshop


*****************************************************************************)

const
  DLG_LIST  =                     10496;
  DLG_EDIT_MNEM =                 10497;
  DLG_EDIT_DRUG =                 10498;
  DLG_DRUGOPTIONS =               10499;
  DLG_EDIT_TEST =                 10500;
  DLG_LISTSESSIONS  =             10501;
  DLG_EDITTESTGROUP =             10502;
  DLG_EDITORDER =                 10503;
  DLG_STATUS  =                   10504;
  DLG_LISTORGANISMS =             10505;
  DLG_EDITORGANISM  =             10506;

  IDS_DEL_CUR_REC =               10496;
  IDS_UIFLDINITERR  =             10497;
  IDS_CANNOTSAVE  =               10498;
  IDS_DUPREC  =                   10499;
  IDS_CLOSE =                     10500;
  IDS_DBOPFAILED  =               10501;
  IDS_MNEMNOASSOC =               10502;
  IDS_MEMFAILURE  =               10503;
  IDS_LISTOPFAILED  =             10504;
  IDS_TOOMANYRECS =               10505;
  IDS_CANTDELETEREC =             10506;
  IDS_CANTEDITREC =               10507;
  IDS_DRUGOPTIONS =               10508;
  IDS_SESSIONSDISABLED  =         10509;
  IDS_LISTRECFAILED =             10510;
  IDS_TESTSREQUIRED =             10511;
  IDS_DUPLICATETESTINGROUP =      10512;
  IDS_TESTGROUPSREQUIRED =        10513;
  IDS_DUPLICATETESTGROUPINORDER = 10514;
  IDS_TESTGROUPTOOBIG =           10515;
  IDS_INCOMPATIBLETESTGROUP =     10516;
  IDS_DUPLICATETESTINORDER  =     10517;
  IDS_IDMICCONFLICT =             10518;
  IDS_CANCELFILL  =               10519;
  IDS_LIST  =                     10520;
  IDS_FAMILY  =                   10521;
  IDS_GRIDSORTFAILED  =           10522;
  IDS_INVALIDORGNUM =             10523;
  IDS_CONFIRMSAVE =               10524;
  IDS_CANTDELCURSESS =            10525;

  BTN_ADD =                         101;
  BTN_DELETE  =                     102;
  BTN_DISABLE =                     103;
  BTN_EDIT  =                       104;
  BTN_OPTIONS =                     105;
  BTN_PRINT =                       106;
  BTN_SAVE  =                       107;
  BTN_SELCUR  =                     108;
  BTN_SORTDOWN  =                   109;
  BTN_SORTRESET =                   110;
  BTN_SORTUP  =                     111;
  BTN_SUPPALL =                     112;
  BTN_SUPPINVERT  =                 113;
  BTN_SUPPNONE  =                   114;
  BTN_SUPPTOGGLE  =                 115;

  CHK_URINE_SOURCE  =               201;
  EDT_CATEGORY  =                   202;
  EDT_DESC  =                       203;
  EDT_DESC2 =                       204;
  EDT_DRUG  =                       205;
  EDT_ID  =                         206;
  GRD_LIST  =                       207;
  GRP_SORT  =                       208;
  GRP_SUPPRESS  =                   209;
  LBL_CURSESSION  =                 210;
  TXT_CATEGORY  =                   211;
  TXT_CURSESSION  =                 212;
  TXT_DESC  =                       213;
  TXT_DESC2 =                       214;
  TXT_DRUG  =                       215;
  TXT_ID  =                         216;
  TXT_MODE =                        217;
  TXT_NUMTESTS =                    218;
  LBL_NUMTESTS =                    219;
  BAR_PROGRESS  =                   220;
  CHK_UDONLY  =                     221;
  CMB_SETS  =                       222;
  LBL_SETS  =                       223;
  GRP_SORTBY  =                     224;
  RDO_UNSORTED  =                   225;
  RDO_ID  =                         226;
  RDO_NAME  =                       227;
  RDO_SET =                         228;
  GRP_NAMEDISPLAYED =               229;
  RDO_SHORTNAME =                   230;
  RDO_LONGNAME  =                   231;
  TXT_SET =                         232;
  TXT_FAMILY  =                     233;
  CMB_SET =                         234;
  CMB_FAMILY  =                     235;
