{----------------------------------------------------------------------------}
{  Module Name  : DBMKLIB.PAS                                                }
{  Programmer   : EJ                                                         }
{  Date Created : 11/01/94                                                   }
{                                                                            }
{  Purpose -                                                                 }
{  This module provides a function that reads a DBMAKE text file and         }
{  returns an error code, or if no error, returns a collection of            }
{  DBReadWriteDesc objects.                                                  }
{                                                                            }
{  Assumptions -                                                             }
{  The caller will dispose of the collection of DBReadWriteDesc objects.     }
{                                                                            }
{  Initialization -                                                          }
{  None.                                                                     }
{                                                                            }
{  Revision History -                                                        }
{                                                                            }
{  Version  Date      Who    Did What                                        }
{  -------  --------  ------ ----------------------------------------------- }
{  1.00     11/01/94  EJ     Initial release                                 }
{                                                                            }
{----------------------------------------------------------------------------}
{$I-}

unit DBMkLib;

INTERFACE

uses
  Objects,
  DBLib,
  DBNDesc;

const
  dbMkErr_InvalidFldUserFlag      = -1;
  dbMkErr_InvalidFieldType        = -2;
  dbMkErr_InvalidFieldSize        = -3;
  dbMkErr_UniqueDupNotSpecified   = -4;
  dbMkErr_InvalidKeyUserFlag      = -5;
  dbMkErr_InvalidKeyNumber        = -6;
  dbMkErr_InvalidFieldNumber      = -7;
  dbMkErr_AscDescNotSpecified     = -8;
  dbMkErr_InvalidFileUserFlag     = -9;
  dbMkErr_AutoSeqNoSpecified      = -10;
  dbMkErr_InvalidCommand          = -11;
  dbMkErr_EndFileExpected         = -12;
  dbMkErr_DBCollectionNotDefined  = -13;
  dbMkErr_CannotOpenFile          = -14;
  dbMkErr_InvalidMakeFile         = -15;
  dbMkErr_CannotCreateFile        = -16;
  dbMkErr_InvalidDataType         = -17;
  dbMkErr_CannotFindDesc          = -18;
  dbMkErr_InvalidFieldValue       = -19;
  dbMkErr_DBNameDescNotDefined    = -20;
  dbMkErr_InvalidFileDesc         = -21;
  dbMkErr_InvalidFieldDec         = -22;

type

  TMkStatus = record
    errCode : integer;  {- positive value indicates a DB error,
                          negative value indicates a DBMKLIB error}
    errLine : integer;  {- the line in the text file where the error occured }
  end;

function ReadDBMakeFile(sfileName, ffileName, pfileName, subdir: PChar;
                        oFlag : char; pDBObjs: PCollection;
                        outf : PText; var status: TMkStatus ): Boolean;
function DBMkLibErrStr(errCode: integer; eStr: PChar; maxLen: integer): PChar;
function WriteDBMakeFile(fileName: PChar; pDB: PDBReadWriteDesc; overwrite, usePath: Boolean): integer;

IMPLEMENTATION

uses
  MScan,
  WinProcs,
  Strings,
  StrsW,
  DBErrors,
  DBTypes,
  DMSDebug;

const
  dbFile               = 'FILE';
  dbAddFile            = 'APPENDFILE';
  dbEndFile            = 'ENDFILE';
  dbAddField           = 'ADDFIELD';
  dbAddKey             = 'ADDKEY';
  dbAddKeySeg          = 'ADDKEYSEG';
  dbAddAssoc           = 'ADDASSOC';
  dbAddDelAssoc        = 'ADDDELASSOC';

type
  TDBMkObj = object(TObject)
    constructor Init(AFileName: PChar; AColl: PCollection;
                     ADesc : PDBNameDescriptor);
    destructor Done; virtual;
    procedure Run(var status: TMkStatus);

    private
    fil     : Text;
    filName : array[0..150] of char;
    stat    : TMkStatus;
    d       : PDBReadWriteDesc;
    dbs     : PCollection;
    line    : integer;
    pd      : PDBNameDescriptor;

    function  IsOK: Boolean;
    procedure Error(eNum: integer);
    procedure ReadALine(tstr: PChar);
    procedure DoAddField(tPChar: PChar; var fldNum :integer);
    procedure DoAddKey(tPChar: PChar; var keyNum : integer);
    procedure DoAddKeySeg(tPChar: PChar);
    procedure DoAddAssoc(tPChar: PChar; var assocNum : integer);
    procedure DoAddDelAssoc(tPChar: PChar);
    procedure DoFile(tPChar: PChar);
    procedure DoAddFile(tPChar: PChar);
    procedure DoFileBlock(fileFlag : integer);
  end;

{----------------------------------------------------------------------------}
{  Initializes the DBMkObj, validates the collection object, and             }
{  opens the DBMAKE text file.                                               }
{                                                                            }
{  Errors returned                                                           }
{    dbMkErr_DBCollectionNotDefined                                          }
{    dbMkErr_CannotOpenFile                                                  }
{                                                                            }
{  InParms : AFileName is the name of the DBMAKE text file.                  }
{            AColl is a collection object that will contain a                }
{            collection of DBReadWriteDesc objects.                          }
{----------------------------------------------------------------------------}
constructor TDBMkObj.Init(AFileName: PChar; AColl: PCollection;
                          ADesc : PDBNameDescriptor);
begin
  FillChar(stat, sizeof(stat), 0);
  dbs := nil;
  d := nil;
  StrCopy(filName, '');
  line := 0;
  pd :=  ADesc;

  if AColl = nil then
    Error(dbMkErr_DBCollectionNotDefined)
  else
  begin
    StrLCopy(filName, AFileName, 100);
    dbs := AColl;
    dbs^.FreeAll;

    Assign(fil, AFileName);
    Reset(fil);
    if IOResult <> 0 then
      Error(dbMkErr_CannotOpenFile);
  end;
end;

{----------------------------------------------------------------------------}
{  Performs final checks and closes the DBMAKE text file before the          }
{  object is disposed.  If there has been an error, frees the                }
{  collection of DBReadWriteDesc objects.                                    }
{                                                                            }
{  Assumptions                                                               }
{  The collection will be freed by the caller if there was no error.         }
{----------------------------------------------------------------------------}
destructor TDBMkObj.Done;
begin
  if not IsOK and (dbs <> nil) then
    dbs^.FreeAll;
  Close(fil);
  if IOResult <> 0 then {nothing};
end;

{----------------------------------------------------------------------------}
{  Read the DBMAKE text file and process the FILE blocks.                    }
{                                                                            }
{  Errors returned                                                           }
{    dbMkErr_InvalidMakeFile                                                 }
{                                                                            }
{  OutParms: status is an error status struct containing an error            }
{            number and the line number where it was detected.               }
{----------------------------------------------------------------------------}
procedure TDBMkObj.Run(var status: TMkStatus);
var
  tStr      : array[0..255] of char;
  upStr     : array[0..255] of char;
  upPChar   : PChar;
  pPos      : PChar;
  count     : integer;
begin
  count := 0;
  while IsOK and not eof(fil) do
  begin
    { read until a FILE command is found }
    ReadALine(tstr);
    StrCopy(upStr,tStr);
    upPChar := ansiUpper(upStr);
    If (strLComp('//', upPChar, 2 ) <> 0) then  { not a comment }
    begin
      pPos := StrPos(upPChar, dbFile);
      if  ((pPos <> nil) and ((pPos - upPChar) = 0)) then
      begin
        DoFile(tstr);
        if IsOK then
          Inc(count);
      end
      else
      begin
        pPos :=StrPos(upPChar, dbAddFile);
        if  ((pPos <> nil) and ((pPos - upPChar) = 0)) then
          DoAddFile(tstr);
      end;
    end;
  end;
  if IsOK and (count = 0) then
  begin
    line := 0;
    Error(dbMkErr_InvalidMakeFile);
  end;
  status := stat;
end;

{----------------------------------------------------------------------------}
{  Test the error code portion of the status struct.                         }
{                                                                            }
{  Returns : TRUE if errCode is equal to 0, else FALSE.                      }
{----------------------------------------------------------------------------}
function TDBMkObj.IsOK: Boolean;
begin
  IsOK := stat.errCode = 0;
end;

{----------------------------------------------------------------------------}
{  Sets the error status struct.                                             }
{                                                                            }
{  InParms : eNum is the error number.                                       }
{----------------------------------------------------------------------------}
procedure TDBMkObj.Error(eNum: integer);
begin
  stat.errCode := eNum;
  stat.errLine := line;
end;

{----------------------------------------------------------------------------}
{  Read a line of text from the DBMAKE input file.                           }
{                                                                            }
{  OutParms: tstr contains the text that is read.                            }
{----------------------------------------------------------------------------}
procedure TDBMkObj.ReadALine(tstr: PChar);
var
   readStr : array[0..255] of char;

begin
  Readln(fil, readstr);
  strCopy(tstr,readStr);
  Inc(line);
  tstr := TrimAll(tstr, tstr, ' ', 255);
end;

{----------------------------------------------------------------------------}
{  Adds a field to the database description.                                 }
{                                                                            }
{  Assumptions                                                               }
{  The text in tstr has the format:                                          }
{    ADDFIELD  fldName, userFlag, fldType [, size]                           }
{                                                                            }
{  Errors returned                                                           }
{    dbMkErr_InvalidFldUserFlag                                              }
{    dbMkErr_InvalidFieldType                                                }
{    dbMkErr_InvalidFieldSize                                                }
{                                                                            }
{  InParms : tstr contains the field definition.                             }
{----------------------------------------------------------------------------}
procedure TDBMkObj.DoAddField(tPChar: PChar; var fldNum : integer);
var
  temp    : array[0..100] of char;
  w       : array[0..255] of char;
  code    : integer;
  afield  : TFldInfo;
  p       : PChar;

begin
  FillChar(afield, sizeof(afield), 0);
  StrStrip(w, tPChar, ' ', 255); { remove FILE command }
  TrimAll(tPChar, tPChar, ' ', 255);

  StrStrip(w, tPChar, ',', 255);
  TrimAll(tPChar, tPChar, ' ', 255);
  TrimAll(w, w, ' ', 255);
  if StrLen(w) > maxFldNameLen then
  begin
    Error(dbMkErr_InvalidFieldValue);
    exit;
  end;

  p := pd^.GetFieldDescription(w, fldNum);
  if (p = nil) then
  begin
    Error(dbMkErr_InvalidFieldDec);
    exit;
  end;
  StrCopy(afield.fldName, p);

  StrStrip(w, tPChar, ',', 255); {- w = userFlag }
  TrimAll(tPChar, tPChar, ' ', 255);
  TrimAll(w, w, ' ', 255);

  if StrPos(w,'0x') <> nil then
  begin
    StrStrip(temp, w, 'x', 100);
    afield.userFlag := word(HVal(w, code));
  end
  else if StrPos(w,'$') <> nil then
  begin
    StrStrip(temp, w, '$', 100);
    afield.userFlag := word(HVal(w, code));
  end
  else
    Val(w, afield.userFlag, code);
  if code <> 0 then
  begin
    Error(dbMkErr_InvalidFldUserFlag);
    exit;
  end;

  StrStrip(w, tPChar, ',', 255);
  TrimAll(tPChar, tPChar, ' ', 255);
  TrimAll(w, w, ' ', 255);
  afield.fldType := DBStrType(w);
  if afield.fldType = dbInvalid then
  begin
    Error(dbMkErr_InvalidFieldType);
    exit;
  end;

  if afield.fldType in [dbString, dbCode, dbZString, dbZCode, dbBlock] then
  begin
    StrStrip(w, tPChar, ',', 255); { w = size }
    Val(w, afield.fldSize, code);
    if (afield.fldSize = 0) or (code <> 0) then
    begin
      Error(dbMkErr_InvalidFieldSize);
      exit;
    end;
  end
  else
    afield.fldSize := 0;

  code := d^.AddField(afield);
  if d^.dbErrorNum <> 0 then
    Error(d^.dbErrorNum);
end;

{----------------------------------------------------------------------------}
{  Adds a key to the database description.                                   }
{                                                                            }
{  Assumptions                                                               }
{  The text in tstr has the format:                                          }
{    ADDKEY    keyName, Unique, userFlag                                     }
{                                                                            }
{  Errors returned                                                           }
{    dbMkErr_UniqueDupNotSpecified                                           }
{    dbMkErr_InvalidKeyUserFlag                                              }
{                                                                            }
{  InParms : tstr contains the key definition.                               }
{----------------------------------------------------------------------------}
procedure TDBMkObj.DoAddKey(tPChar: PChar; var keyNum : integer);
var
  pstr    : array[0..100] of char;
  code    : integer;
  w       : array[0..255] of char;
  upStr   : array[0..80] of char;
  pUpStr  : PChar;
  akey    : TKeyInfo;
begin
  FillChar(akey, sizeof(akey), 0);
  StrStrip(w, tPChar, ' ', 255); { remove FILE command }
  TrimAll(tPChar, tPChar, ' ', 255);

  StrStrip(w, tPChar, ',', 255);
  TrimAll(tPChar, tPChar, ' ', 255);
  TrimAll(w, w, ' ', 255);
  if StrLen(w) > maxKeyNameLen then
  begin
    Error(dbMkErr_InvalidFieldValue);
    exit;
  end;
  StrCopy(akey.keyName, w);

  {************************}
  {* add Key to dbids.pas *}
  {************************}
  pd^.InsertDBKey(w, keyNum);

  StrStrip(w, tPChar, ',', 255); {- w = unique/duplicate }
  TrimAll(tPChar, tPChar, ' ', 255);
  TrimAll(w, w, ' ', 255);

  strCopy(upStr,w);
  pUpStr := ansiUpper(upStr);

  if StrComp('UNIQUE',upStr) = 0 then
    akey.unique := TRUE
  else if StrComp('DUPLICATES',upStr) = 0 then
    akey.unique := FALSE
  else
  begin
    Error(dbMkErr_UniqueDupNotSpecified);
    exit;
  end;

  StrStrip(w, tPChar, ',', 255); {- w = user flag }
  TrimAll(tPChar, tPChar, ' ', 255);
  TrimAll(w, w, ' ', 255);
  if StrPos(w,'0x') <> nil then
  begin
    StrStrip(pstr, w, 'x', 255);
    akey.keyFlag := word(HVal(w, code));
  end
  else if StrPos(w,'$') <> nil then
  begin
    StrStrip(pstr, w, '$', 255);
    akey.keyFlag := word(HVal(w, code));
  end
  else
    Val(w, akey.keyFlag, code);
  if code <> 0 then
  begin
    Error(dbMkErr_InvalidKeyUserFlag);
    exit;
  end;

  code := d^.AddKey(akey);
  if d^.dbErrorNum <> 0 then
    Error(d^.dbErrorNum);
end;

{----------------------------------------------------------------------------}
{  Adds a key segment to the database description.                           }
{                                                                            }
{  Assumptions                                                               }
{  The text in tstr has the format:                                          }
{    ADDKEYSEG KeyNum, fldNum, Descending | Ascending                        }
{                                                                            }
{  Errors returned                                                           }
{    dbMkErr_InvalidKeyNumber                                                }
{    dbMkErr_InvalidFieldNumber                                              }
{    dbMkErr_AscDescNotSpecified                                             }
{                                                                            }
{  InParms : tstr contains the key segment definition.                       }
{----------------------------------------------------------------------------}
procedure TDBMkObj.DoAddKeySeg(tPChar: PChar);
var
  knum    : integer;
  code    : integer;
  w       : array[0..255] of char;
  aseg    : TSegmentInfo;
  p       : PChar;
  upStr   : array[0..255] of char;
  pUpStr  : PChar;

begin
  FillChar(aseg, sizeof(aseg), 0);
  StrStrip(w, tPChar, ' ', 255); { remove FILE command }
  TrimAll(tPChar, tPChar, ' ', 255);

  StrStrip(w, tPChar, ',', 255); {- w = keyNum }
  TrimAll(tPChar, tPChar, ' ', 255);
  Val(w, knum, code);
  if code <> 0 then
  begin
    Error(dbMkErr_InvalidKeyNumber);
    exit;
  end;

  StrStrip(w, tPChar, ',', 255); {- w = fldNum }
  TrimAll(tPChar, tPChar, ' ', 255);
  TrimAll(w, w, ' ', 255);
  p := pd^.GetFieldNumber(w);
  Val(p, aseg.fldNum, code);
  if code <> 0 then
  begin
    Error(dbMkErr_InvalidFieldNumber);
    exit;
  end;

  StrStrip(w, tPChar, ',', 255); {- w = fldNum }
  TrimAll(tPChar, tPChar, ' ', 255);
  TrimAll(w, w, ' ', 255);
  strCopy(upStr,w);
  pUpStr := ansiUpper(upStr);

  if StrComp('ASCENDING',upStr) = 0 then
    aseg.descend := FALSE
  else if StrComp('DESCENDING',upStr) = 0 then
    aseg.descend := TRUE
  else
  begin
    Error(dbMkErr_AscDescNotSpecified);
    exit;
  end;

  d^.AddKeySeg(kNum, aseg);
  if d^.dbErrorNum <> 0 then
    Error(d^.dbErrorNum);
end;

{----------------------------------------------------------------------------}
{  Adds an association to the database description.                          }
{                                                                            }
{  Assumptions                                                               }
{  The text in tstr has the format:                                          }
{    ADDASSOC Filename, KeyNum, Count, Fld1[, Fld2[, Fld3[, Fld4]]][, Const] }
{                                                                            }
{  Errors returned                                                           }
{    dbMkErr_InvalidKeyNumber                                                }
{    dbMkErr_InvalidField                                                    }
{    dbMkErr_InvalidFieldNumber                                              }
{                                                                            }
{  InParms : tstr contains the association definition.                       }
{----------------------------------------------------------------------------}
procedure TDBMkObj.DoAddAssoc(tPChar: PChar; var assocNum : integer);
var
  i       : integer;
  code    : integer;
  w       : array[0..255] of char;
  aassoc  : TAssocInfo;
  p       : PChar;
begin
  FillChar(aassoc, sizeof(aassoc), 0);
  StrStrip(w, tPChar, ' ', 255); { remove AddAssoc command }
  TrimAll(tPChar, tPChar, ' ', 255);

  StrStrip(w, tPChar, ',', 255);
  TrimAll(tPChar, tPChar, ' ', 255);
  TrimAll(w, w, ' ', 255);
  if StrLen(w) > maxFileNameLen then
  begin
    Error(dbMkErr_InvalidFieldValue);
    exit;
  end;
  StrCopy(aassoc.fName, w);

  { Make DB association constants }
  pd^.MakeAssociation(aassoc.fName, assocNum);

  StrStrip(w, tPChar, ',', 255); {- w = keyNum }
  TrimAll(tPChar, tPChar, ' ', 255);
  Val(w, aassoc.keyNum, code);
  if code <> 0 then
  begin
    Error(dbMkErr_InvalidKeyNumber);
    exit;
  end;

  StrStrip(w, tPChar, ',', 255); {- w = Count }
  TrimAll(tPChar, tPChar, ' ', 255);
  Val(w, aassoc.numFields, code);
  if code <> 0 then
  begin
    Error(dbMkErr_InvalidFieldValue);
    exit;
  end;
  if aassoc.numFields > maxAssocFlds then
  begin
    Error(dbMkErr_InvalidFieldValue);
    exit;
  end;

  for i := 1 to aassoc.numFields do
  begin
    StrStrip(w, tPChar, ',', 255); {- w = fldNum }
    TrimAll(tPChar, tPChar, ' ', 255);
    TrimAll(w, w, ' ', 255);
    p := pd^.GetFieldNumber(w);
    Val(p, aassoc.fldNum[i], code);
    if code <> 0 then
    begin
      Error(dbMkErr_InvalidFieldNumber);
      exit;
    end;
  end;

  {- constant makes up rest of line }
  if StrLen(tPChar) <> 0 then
    begin
    if StrLen(tPChar) > maxConstLen then
    begin
      Error(dbMkErr_InvalidFieldValue);
      exit;
    end;
    StrCopy(aassoc.constFldValue, tPChar);
    end;

  d^.AddAssoc(aassoc);
  if d^.dbErrorNum <> 0 then
    Error(d^.dbErrorNum);
end;

{----------------------------------------------------------------------------}
{  Adds a delete association to the database description.                    }
{                                                                            }
{  Assumptions                                                               }
{  The text in tstr has the format:                                          }
{    ADDDELASSOC Filename, delAction, U | D[, fldName]                       }
{                                                                            }
{  Errors returned                                                           }
{                                                                            }
{  InParms : tstr contains the delete association definition.                }
{----------------------------------------------------------------------------}
procedure TDBMkObj.DoAddDelAssoc(tPChar: PChar);
var
  i       : integer;
  code    : integer;
  w       : array[0..255] of char;
  adel    : TDelAssocInfo;
  p       : PChar;
  upStr   : array[0..255] of char;
  pUpStr  : PChar;

begin
  FillChar(adel, sizeof(adel), 0);
  StrStrip(w, tPChar, ' ', 255); { remove AddAssoc command }
  TrimAll(tPChar, tPChar, ' ', 255);

  StrStrip(w, tPChar, ',', 255);
  TrimAll(tPChar, tPChar, ' ', 255);
  TrimAll(w, w, ' ', 255);
  if StrLen(w) > maxFileNameLen then
  begin
    Error(dbMkErr_InvalidFieldValue);
    exit;
  end;
  StrCopy(adel.fName, w);

  StrStrip(w, tPChar, ',', 255);
  TrimAll(tPChar, tPChar, ' ', 255);
  TrimAll(w, w, ' ', 255);
  strCopy(upStr,w);
  pUpStr := ansiUpper(upStr);

  if StrComp('DISALLOW',w) = 0 then
    adel.delAction := DISALLOW_DELETE
  else if strComp('DELETE',w) = 0 then
    adel.delAction := DELETE_ASSOCIATED
  else
  begin
    Error(dbMkErr_InvalidFieldValue);
    exit;
  end;

  StrStrip(w, tPChar, ',', 255);
  TrimAll(tPChar, tPChar, ' ', 255);
  TrimAll(w, w, ' ', 255);
  strCopy(upStr,w);
  pUpStr := ansiUpper(upStr);

  if StrComp('D',upStr) = 0 then
    adel.bDownline := TRUE
  else if StrComp('U',upStr) = 0 then
    adel.bDownline := FALSE
  else
  begin
    Error(dbMkErr_InvalidFieldValue);
    exit;
  end;

  {- field name makes up rest of line }
  if StrLen(tPChar) <> 0 then
    begin
    if StrLen(tPChar) > maxFldNameLen then
    begin
      Error(dbMkErr_InvalidFieldValue);
      exit;
    end;
    { Lookup  the field associated with the file }
    p := pd^.GetDelAssoc(tPChar);
    if (p = nil) then
    begin
      Error(dbMkErr_InvalidFieldDec);
      exit;
    end;
    StrCopy(adel.assocFldName, p);
    end;

  d^.AddDelAssoc(adel);
  if d^.dbErrorNum <> 0 then
    Error(d^.dbErrorNum);
end;

{----------------------------------------------------------------------------}
{  Begins a database file block definition.                                  }
{                                                                            }
{  Assumptions                                                               }
{  The text in tstr has the format:                                          }
{    FILE filename, description, userFlag, [NO]AUTOSEQ, dataType [, path]    }
{                                                                            }
{  Errors returned                                                           }
{    dbMkErr_InvalidFileUserFlag                                             }
{    dbMkErr_AutoSeqNoSpecified                                              }
{    dbMkErr_InvalidDataType                                                 }
{                                                                            }
{  InParms : tstr contains the file statement.                               }
{----------------------------------------------------------------------------}
procedure TDBMkObj.DoFile(tPChar: PChar);
var
  save     : array[0..255] of char;
  temp     : array[0..255] of char;
  w        : array[0..255] of char;
  k        : word;
  code     : integer;
  name     : array[0..255] of char;
  desc     : array[0..255] of char;
  pstr     : array[0..255] of char;
  fldNum   : integer;
  tmpPStr  : PChar;
  fileFlag : integer;
  pUpStr   : PChar;
  tstr     : string;
begin
  fldNum := 0;
  fileFlag := 0;

  StrStrip(temp, tPChar, ' ', 255); { remove FILE command }
  TrimAll(tPChar, tPChar, ' ', 255);

  StrCopy(save,tPChar);

  StrStrip(w, tPChar, ',', 255); { w = fileName }
  TrimAll(tPChar, tPChar, ' ', 255);
  TrimAll(w, w, ' ', 255);
  StrCopy(name, w);

  StrStrip(w, tPChar, ',', 255); { w = description}
  TrimAll(tPChar, tPChar, ' ', 255);

  StrStrip(w, tPChar, ',', 255); { w = userFlag }
  TrimAll(tPChar, tPChar, ' ', 255);

  StrStrip(w, tPChar, ',', 255); { w = AutoSeq keyword }
  TrimAll(tPChar, tPChar, ' ', 255);

  StrStrip(w, tPChar, ',', 255); { w = dataType }
  TrimAll(tPChar, tPChar, ' ', 255);

  {- file path makes up rest of line }
  If StrLen(tPChar) = 0 then { tstr = path }
  begin
    GetDir(0, tstr);
    StrPCopy(pstr, tstr);
  end
  else
    StrCopy(pstr, tPChar);
  if pstr[StrLen(pstr)-1] <> '\' then
    StrCat(pstr, '\');

  d := New(PDBReadWriteDesc, Init(name, pstr));
  code := DBDeleteDatabase(d);
  if (code <> 0) and (code <> dbErr_CantDeleteDefFile) and
     (code <> dbErr_CantDeleteDataFile) then
  begin
    Error(code);
    exit;
  end;

  d := New(PDBReadWriteDesc, Init(name, pstr));

  StrCopy(tPChar,save);

  StrStrip(w, tPChar, ',', 255); { w = fileName }
  TrimAll(tPChar, tPChar, ' ', 255);

  StrStrip(w, tPChar, ',', 255); { w = description}
  TrimAll(tPChar, tPChar, ' ', 255);
  TrimAll(w, w, ' ', 255);
  StrCopy(pstr, w);

  { Look up File Description }
  StrCopy(desc, w);
  pd^.SetBDFileName(desc);
  tmpPStr := pd^.GetFileDescription(desc, name);
  if (tmpPStr = nil) then
  begin
    Error(dbMkErr_InvalidFileDesc);
    exit;
  end;

  d^.SetFileDesc(tmpPStr);

  StrStrip(w, tPChar, ',', 255); { w = userFlag }
  TrimAll(tPChar, tPChar, ' ', 255);
  TrimAll(w, w, ' ', 255);
  if StrPos(w,'0x') <> nil then
  begin
    StrStrip(temp, w, 'x', 255);
    k := word(HVal(w, code));
  end
  else if StrPos(w,'$') <> nil then
  begin
    StrStrip(temp, w, '$', 255);
    k := word(HVal(w, code));
  end
  else
    Val(w, k, code);
  if code = 0 then
  begin
    d^.SetFileFlag(k);
    fileFlag := integer(k);
  end
  else
  begin
    Error(dbMkErr_InvalidFileUserFlag);
    exit;
  end;

  StrStrip(w, tPChar, ',', 255); { w = AutoSeq keyword }
  TrimAll(tPChar, tPChar, ' ', 255);
  TrimAll(w, w, ' ', 255);
  pUpStr := ansiUpper(w);

  if StrComp('AUTOSEQ',w) = 0 then
    d^.SetAutoSeq(TRUE)
  else if StrComp('NOAUTOSEQ',w) = 0 then
    d^.SetAutoSeq(FALSE)
  else
  begin
    Error(dbMkErr_AutoSeqNoSpecified);  {- autoseq or noautoseq not specified }
    exit;
  end;

  StrStrip(w, tPChar, ',', 255); { w = dataType }
  TrimAll(tPChar, tPChar, ' ', 255);
  TrimAll(w, w, ' ', 255);
  Val(w, k, code);
  if code = 0 then
    d^.SetDataType(k)
  else
  begin
    Error(dbMkErr_InvalidDataType);
    exit;
  end;

  { now read the rest of the FILE block }
  DoFileBlock(fileFlag);
  if IsOK then
  begin
    {- d has been defined at this point}
    dbs^.Insert(d);
    pd^.InsertBlank;
  end
  else
    MSDisposeObj(d);
end;

{----------------------------------------------------------------------------}
{  Begins a database file block definition addition.                         }
{                                                                            }
{  Assumptions                                                               }
{  The text in tstr has the format:                                          }
{    APPENDFILE filename[, path]                                             }
{                                                                            }
{  Errors returned                                                           }
{                                                                            }
{  InParms : tstr contains the addfile statement.                            }
{----------------------------------------------------------------------------}
procedure TDBMkObj.DoAddFile(tPChar: PChar);
var
  w    : array[0..255] of char;
  k    : word;
  code : integer;
  name : array[0..255] of char;
  pstr : array[0..255] of char;
  tstr : string;

  function Matches(d: PDBReadWriteDesc): boolean; far;
  var
    path   : array[0..maxPathLen] of char;
    afile  : TFileInfo;
  begin
    Matches := FALSE;
    d^.FileInfo(afile);
    d^.FilePath(path, maxPathLen);
    if (StrIComp(name, afile.fName) = 0) and
       (StrIComp(pstr, path) = 0) then
      Matches := TRUE;
  end;

begin
  StrStrip(w, tPChar, ' ', 255); { remove FILE command }
  TrimAll(tPChar, tPChar, ' ', 255);

  StrStrip(w, tPChar, ',', 255); { w = fileName }
  TrimAll(tPChar, tPChar, ' ', 255);
  TrimAll(w, w, ' ', 255);
  StrCopy(name, w);

  {Save the Association file Name. Need it to build association constants }
  pd^.SetBDFileName(name);

  {- file path makes up rest of line }
  If StrLen(tPChar) = 0 then { tstr = path }
  begin
    GetDir(0, tstr);
    StrPCopy(pstr, tstr);
  end
  else
    StrCopy(pstr, tPChar);
  if pstr[StrLen(pstr)-1] <> '\' then
    StrCat(pstr, '\');

  d := dbs^.FirstThat(@Matches);
  if d = nil then
    Error(dbMkErr_CannotFindDesc)
  else
    { now read the rest of the FILE block }
    DoFileBlock(-1);
end;

{----------------------------------------------------------------------------}
{  Processes a database file block definition.                               }
{                                                                            }
{  Errors returned                                                           }
{    dbMkErr_InvalidCommand                                                  }
{    dbMkErr_EndFileExpected                                                 }
{----------------------------------------------------------------------------}
procedure TDBMkObj.DoFileBlock(fileFlag : integer);
var
  tPChar    : array[0..255] of char;
  upStr     : array[0..255] of char;
  pUpStr    : PChar;
  endF      : Boolean;
  fldNum    : integer;
  assocNum  : integer;
  keyNum    : integer;
  firstPass : Boolean;
  tstr      : string;
begin
  fldNum := 0;
  assocNum := 0;
  keyNum := 0;
  endF := FALSE;
  firstPass := true;
  while IsOK and not Eof(fil) and not endF do
  begin
    ReadALine(tPChar);
    strCopy(upStr,tPChar);
    pUpStr := ansiUpper(upStr);
    tstr := StrPas(tPChar);

    If (strLComp('//', pUpStr, 2 ) <> 0)  then  { not a comment }
    begin
      if StrPos(pUpStr,dbAddField) <> nil then
      begin
        inc(fldNum);
        DoAddField(tPChar, fldNum);
      end
      else if StrPos(pUpStr,dbAddKeySeg) <> nil then
        DoAddKeySeg(tPChar)
      else if StrPos(pUpStr,dbAddKey) <> nil then
      begin
        if firstPass then
        begin
          pd^.InsertUDField(fileFlag, fldNum);
          pd^.InsertBlank;
        end;
        firstPass := False;
        inc(keyNum);
        DoAddKey(tPChar, keyNum);
      end
      else if StrPos(pUpStr,dbAddAssoc) <> nil then
      begin
        inc(assocNum);
        DoAddAssoc(tPChar, assocNum);
      end
      else if StrPos(pUpStr,dbAddDelAssoc) <> nil then
        DoAddDelAssoc(tPChar)
      else if StrPos(pUpStr,dbEndFile) <> nil then
      begin
        endF := TRUE;
        d^.StoreDef;
      end
      else
        Error(dbMkErr_InvalidCommand);
    end;
  end;
  if IsOK and not endF then
    Error(dbMkErr_EndFileExpected);
end;

{----------------------------------------------------------------------------}
{  Reads the text file associated with fileName and parses the FILE          }
{  sections into a collection of PDBReadWrite objects.  If an error          }
{  occurs, the collection will be returned empty, and status will be         }
{  filled in with error information.  The data files will not be             }
{  created.  The caller must iterate through the collection and              }
{  create the Btrieve files.                                                 }
{                                                                            }
{  Returns : TRUE if no error.                                               }
{  InParms : fileName is the name of the DBMAKE text file.                   }
{            pdbObjs is the collection of valid file description objects.    }
{  OutParms: status is a struct containing any error and the line            }
{            number where in the text file it occured.                       }
{----------------------------------------------------------------------------}
function ReadDBMakeFile(sfileName, ffileName, pfileName, subdir: PChar;
                        oFlag : char; pDBObjs: PCollection;
                        outf : PText; var status: TMkStatus): Boolean;
var
  dbmk     : TDBMkObj;
  pDBDesc  : PDBNameDescriptor;
begin
  pDBDesc := New(PDBNameDescriptor,Init(ffileName,subdir, pfileName,
                 outf, oFlag));
  if pDBDesc = nil then
  begin
     status.errCode := dbMkErr_DBNameDescNotDefined;
     status.errLine := 0;
  end
  else
  begin
    dbMk.Init(sfileName, pdbObjs, pDBDesc);
    dbMk.Run(status);
    dbMk.Done;

    pDBDesc^.InsertSpecialConst;
    pDBDesc^.MakePas;
    MSDisposeObj(pDBDesc);
  end;

  ReadDBMakeFile := status.errCode = 0;
end;

{----------------------------------------------------------------------------}
{  Returns a string for the given errCode.                                   }
{                                                                            }
{  Returns : a pointer to the error string.                                  }
{  InParms : errCode is error number.                                        }
{            maxLen is max length of eStr.                                   }
{  OutParms: eStr contains the error text.                                   }
{----------------------------------------------------------------------------}
function DBMkLibErrStr(errCode: integer; eStr: PChar; maxLen: integer): PChar;
var
  msg : array[0..255] of char;
begin
  if errCode > 0 then
    DBErrorStr(errCode, eStr, maxLen)
  else if errCode < 0 then
  begin
    case errCode of
      dbMkErr_InvalidFldUserFlag      : StrCopy(msg,'Invalid field User Flag');
      dbMkErr_InvalidFieldType        : StrCopy(msg,'Invalid field type');
      dbMkErr_InvalidFieldSize        : StrCopy(msg,'Invalid field size');
      dbMkErr_UniqueDupNotSpecified   : StrCopy(msg,'UNIQUE or DUPLICATES not specified for key');
      dbMkErr_InvalidKeyUserFlag      : StrCopy(msg,'Invalid key User Flag');
      dbMkErr_InvalidKeyNumber        : StrCopy(msg,'Invalid key number');
      dbMkErr_InvalidFieldNumber      : StrCopy(msg,'Invalid field number');
      dbMkErr_AscDescNotSpecified     : StrCopy(msg,'ASCENDING or DESCENDING not specified');
      dbMkErr_InvalidFileUserFlag     : StrCopy(msg,'Invalid file User Flag');
      dbMkErr_AutoSeqNoSpecified      : StrCopy(msg,'AUTOSEQ or NOAUTOSEQ not specified');
      dbMkErr_InvalidCommand          : StrCopy(msg,'Invalid command');
      dbMkErr_EndFileExpected         : StrCopy(msg,'ENDFILE command expected');
      dbMkErr_DBCollectionNotDefined  : StrCopy(msg,'A valid collection not passed to RUN method');
      dbMkErr_CannotOpenFile          : StrCopy(msg,'Cannot open definition file');
      dbMkErr_InvalidMakeFile         : StrCopy(msg,'Invalid MAKE file.');
      dbMkErr_CannotCreateFile        : StrCopy(msg,'Cannot create definition file');
      dbMkErr_InvalidDataType         : StrCopy(msg,'Invalid file datatype');
      dbMkErr_CannotFindDesc          : StrCopy(msg,'Cannot find file description object');
      dbMkErr_InvalidFieldValue       : StrCopy(msg,'Invalid field value');
      dbMkErr_DBNameDescNotDefined    : StrCopy(msg,'An invalid Field Name Descriptor');
      dbMkErr_InvalidFileDesc         : StrCopy(msg,'Invalid file descriptor const');
      dbMkErr_InvalidFieldDec         : StrCopy(msg,'Invalid field descriptor const');
      else
        StrCopy(msg,'Unknown Error');
    end;
    StrLCopy(eStr, msg, maxLen);
  end
  else
    StrLCopy(eStr, 'No error', maxLen);
  DBMkLibErrStr := eStr;
end;

{----------------------------------------------------------------------------}
{  Write the definition of the database object to a file.                    }
{                                                                            }
{  Errors returned                                                           }
{    dbMkErr_CannotCreateFile                                                }
{    dbMkErr_CannotOpenFile                                                  }
{                                                                            }
{  Returns : an error number.                                                }
{  InParms : fileName is the name of the text file to write to.              }
{            pDB is the object to query.                                     }
{            overwrite flag indicates to replace or append to text file.     }
{            usePath flag indicates to write path or not.                    }
{----------------------------------------------------------------------------}
function WriteDBMakeFile(fileName: PChar; pdb: PDBReadWriteDesc;
           overwrite, usePath: Boolean): integer;
var
  fil  : Text;
  ret  : integer;
  j, k : integer;
  pstr : array[0..255] of char;
  afile: TFileInfo;
  fi   : TFldInfo;
  ki   : TKeyInfo;
  ai   : TAssocInfo;
  di   : TDelAssocInfo;
  base : integer;
  a    : PDBReadDesc;
begin
  ret := 0;
  if pdb <> nil then
  begin
    Assign(fil, fileName);
    if overwrite then
    begin
      Rewrite(fil);
      if IOResult <> 0 then
        ret := dbMkErr_CannotCreateFile;
    end
    else
    begin
      Append(fil);
      if IOResult <> 0 then
        ret := dbMkErr_CannotOpenFile;
    end;
    if ret = 0 then
    begin
      if not overwrite then
        Writeln(fil);

(*    base := pdb^.DataType * 100; *)
      base := 0;
      Writeln(fil, '// Exported Definition //');

      { FILE filename, description, userFlag, autoSeq, dataType [, path] }
      pdb^.FileInfo(afile);
      Write(fil, dbFile, ' ');
      Write(fil, StrPas(afile.fName), ', ');
      Write(fil, StrPas(afile.desc), ', ');
      Write(fil, '$', StrPas(HexStr(afile.userFlag,
                             sizeof(afile.userFlag), pstr, 255)), ', ');
      if not afile.autoSeq then
        Write(fil, 'NO');
      Write(fil, 'AUTOSEQ, ');
      Write(fil, pdb^.DataType);

      if usePath then
      begin
        pdb^.FilePath(pstr, 255);
        Write(fil, ', ', pstr);
      end;
      Writeln(fil);

      { ADDFIELD  fldName, userFlag, fldType[, size] }
      for j := 1 to pdb^.NumFields do
      begin
        pdb^.FieldInfo(j + base, fi);
        Write(fil, '  ', dbAddField, ' ');
        Write(fil, fi.fldName, ', ');
        Write(fil, '$', StrPas(HexStr(fi.userFlag,
                               sizeof(fi.userFlag), pstr, 255)), ', ');
        Write(fil, DBTypeStr(fi.fldType, pstr, 255));
        if fi.fldType in [dbString, dbCode, dbBlock, dbZString, dbZCode] then
          Write(fil, ', ', fi.fldsize);
        Writeln(fil);
      end;

      { ADDKEY keyName, Unique, userFlag }
      for j := 1 to pdb^.NumKeys do
      begin
        if j = 1 then Writeln(fil);
        pdb^.KeyInfo(j + base, ki);
        Write(fil, '  ', dbAddKey, ' ');
        Write(fil, ki.keyName, ', ');
        if ki.unique then
          Write(fil, 'unique, ')
        else
          Write(fil, 'duplicates, ');
        Write(fil, ki.keyFlag);
        Writeln(fil);

        { ADDKEYSEG KeyNum, fldNum, Descending }
        for k := 1 to ki.NumSegs do
        begin
          Write(fil, '    ', dbAddKeySeg, ' ', j + base, ', ', ki.seglist[k].fldNum + base, ', ');
          if ki.seglist[k].descend then
            Write(fil, 'descending')
          else
            Write(fil, 'ascending');
          Writeln(fil);
        end;
      end;

      { ADDASSOC Filename, KeyNum, Count, Fld1[, Fld2[, Fld3[, Fld4]]][, Const] }
      for j := 1 to pdb^.NumAssocs do
      begin
        if j = 1 then Writeln(fil);
        pdb^.AssocInfo(j + base, ai);
        Write(fil, '  ', dbAddAssoc, ' ');
        Write(fil, ai.fName, ', ');
        if ai.keyNum = 0 then
          Write(fil, '0, ')
        else
        begin
          a := New(PDBReadDesc, Init(ai.fName, pdb^.FilePath(pstr, 255)));
          if a <> nil then
          begin
            Write(fil, a^.DataType * 100 + ai.keyNum, ', ');
            MSDisposeObj(a);
          end
          else
            Write(fil, ai.keyNum, ', ');
        end;
        Write(fil, ai.numFields);
        for k := 1 to ai.numFields do
          if ai.fldNum[k] = 0 then
            Write(fil, ', 0')
          else
            Write(fil, ', ', ai.fldNum[k]+base);
        if StrLen(ai.constFldValue) > 0 then
          Write(fil, ', ', ai.constFldValue);
        Writeln(fil);
      end;

      { ADDDELASSOC  Filename, delAction, U | D[, fldName] }
      for j := 1 to pdb^.NumDelAssocs do
      begin
        if j = 1 then Writeln(fil);
        pdb^.DelAssocInfo(j + base, di);
        Write(fil, '  ', dbAddDelAssoc, ' ');
        Write(fil, di.fName, ', ');
        case di.delAction of
          DISALLOW_DELETE:
            Write(fil, 'DISALLOW, ');
          DELETE_ASSOCIATED:
            Write(fil, 'DELETE, ');
          else
            Write(fil, '????, ');
        end;
        if di.bDownline then
          Write(fil, 'D')
        else
          Write(fil, 'U');
        if StrLen(di.assocFldName) > 0 then
          Write(fil, ', ', StrPas(di.assocFldName));
        Writeln(fil);
      end;

      Writeln(fil, dbEndFile);
      Writeln(fil, '//');
      Writeln(fil, '// End of Exported Definition.');

      Close(fil);
      If IOResult <> 0 then {nothing};
    end;
  end;
  WriteDBMakeFile := ret;
end;

END.

