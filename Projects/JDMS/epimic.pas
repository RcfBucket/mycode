{----------------------------------------------------------------------------}
{  Module Name  : EPIMIC.PAS                                                 }
{  Programmer   : EJ                                                         }
{  Date Created : 07/04/95                                                   }
{  Requirements : ..., S3.23 [ref. c], S3.28 [ref. d]                        }
{                                                                            }
{  Purpose - This unit provides a procedure for generating an Antimicrobial  }
{    Minimal Inhibitory Concentration (MIC) report. The Antimicrobial MIC    }
{    report reflects the cumulative percentages of MIC values that occured   }
{    in various organism/drug combinations. This report is for epidemiology. }
{                                                                            }
{    The isolate data file [ref. b] and its associated data files are the    }
{    primary source of input for the report. This input is reduced by the    }
{    Multiple Parameter Search System (MPSS) (with or withour user defined   }
{    rules), local rules, and further reduced by the Eliminate Duplicates    }
{    Filter (EDFilter).                                                      }
{                                                                            }
{    The format of the report is hard coded. The major sort order is user    }
{    controlled.                                                             }
{                                                                            }
{  Assumptions                                                               }
{    a. A mono-spaced font is used for the report.                           }
{    b. An initial list size for 100 paragraphs and a grow by of 100 will    }
{       be satisfactory to all users.                                        }
{                                                                            }
{  Limitations                                                               }
{    a. Number of isolates is limited by EDFilter and other things.          }
{    b. Number of organisms is limited by TCollection.                       }
{    c. Number of organisms is limited by the sort engine when sorting is    }
{       enabled.                                                             }
{                                                                            }
{  Special                                                                   }
{    To maintain the set of unit error numbers, whenever the Assert or       }
{    DoneOnError functions are added, search for Assert and DoneOnError and  }
{    renumber them with unique values.                                       }
{                                                                            }
{  Referenced Documents                                                      }
{    a. Software Specification for Nihongo Data Management System v4.00      }
{    b. Database Design Document, Revision 0                                 }
{    c. S3.23 Design Document, Revision 2                                    }
{    d. S3.28 Design Document, Revision 2                                    }
{                                                                            }
{  Revision History - This project is under version control, use it to view  }
{    revision history.                                                       }
{                                                                            }
{----------------------------------------------------------------------------}

unit EpiMIC;

INTERFACE

uses
  OWindows;

procedure EpiAntimicrobicMIC( pParent: PWindowsObject; useMPSS: boolean );


IMPLEMENTATION

uses
  APITools,
  Bits,
  DBIDs,
  DBLib,
  DBFile,
  DBTypes,
  DMSErr,
  EpiFiles,
  EDFilter,
  ListLib,
  MScan,
  MPSS,
  Objects,
  PrnPrev,
  Report,
  ResLib,
  Trays,
  Screens,
  Sort_Eng,
  Sort_Dlg,
  Sort_Par,
  Strings,
  StrsW;

{$I PRTEPI.INC}



                        {----------------------}
                        {  Helper Functions    }
                        {----------------------}



{--------------------------------------------------------------------------}
{ This procedure handles assertions for this unit. If an assertion is
{ false, it calls ShowError with the number and other information.
{ Otherwise, it does nothing.
{--------------------------------------------------------------------------}

procedure Assert( Valid: boolean; UnitErrorNumber: integer );
begin
  if not Valid then
  begin
    ShowError( nil, IDS_EPI_ERR, nil, MOD_EPIMIC+1, MOD_EPIMIC,
      UnitErrorNumber );
    Halt;
  end;
end;



{----------------------------------------------------------------------------}
{ This procedure concats the specified number of blanks onto the end of
{ the newStr
{----------------------------------------------------------------------------}

procedure ConcatBlanks( howMany: integer; newStr: PChar; Length: integer );
begin
  if howMany <= 0 then
    Exit;

  Inc( howMany, StrLen( newStr ) );
  if howMany >= Length then
    howMany := Length;
  Pad( newStr, newStr, ' ', howMany );
end;



{----------------------------------------------------------------------------}
{ This procedure loads a buffer with the name of the organism as listed in
{ the organism database. If the name is too long, it is clipped. If the
{ organism is not listed, up to 5 asterisks are returned.
{
{ Input
{   Organism - the sequence number of an organism record in the organism
{     data file.
{   pBuffer - pointer to a memory buffer to receive the name
{   MaxLen - the maximum number of characters to copy, must be size of
{     buffer - 1 or less.
{ Output
{   pBuffer^ - contains the organism's short name or a fill if not found.
{
{ Return value: pBuffer
{----------------------------------------------------------------------------}

function OrganismShortNameLCopy( Organism: TSeqNum; pBuffer: PChar;
  MaxLen: Word ): boolean;
var
  List: TListObject;
  ListRec: TMnemListRec;
begin
  { Construct list and list record objects }
  List.Init;
  ListRec.Init;

  { Copy name or default }
  if List.FindMnemSeq( DBORGFile, @ListRec, organism ) then
    StrLCopy( pBuffer, ListRec.desc, MaxLen )
  else
    StrLCopy( pBuffer, '*****', MaxLen );

  { Cleanup. }
  ListRec.Done;
  List.Done;
end;



var
  {--------------------------------------------------------------------------}
  { Local variables for DrugNameCat function. Must be initializes before
  { calling DrugNameCat and must be destroyed when DrugNameCat is longer
  { needed.
  {
  { History: Originally, the functionality of DrugNameCat was mixed into the
  { printing code. It was extracted and all its variables were internal to
  { the function. Then, the variables were extracted for a possible speed
  { improvement, but this reintroduced complexity. Look at
  { EpiAntimicrobicMIC.
  {
  { This unit should focus on the Antimicrobial MIC report. These variables
  { have nothing to do with the report. The next step is to move DrugNameCat
  { and its variables into an external object. If such object were created,
  { it may also serve to replace the DrugIDLCat function in the EpiSus unit.
  {--------------------------------------------------------------------------}
  DrugNames: TListObject;
  Mnemonics: TMnemListRec;
  pTray: PTraysObject;
  FilterMult: boolean;



{----------------------------------------------------------------------------}
{ This procedure loads a buffer with the name of the drug as listed in
{ the drug database. If the name is too long, it is clipped. If the
{ organism is not listed, up to 5 asterisks are returned.
{
{ The name is modified if screening is on.
{----------------------------------------------------------------------------}

procedure DrugNameCat( str: PChar; Drug: TSeqNum );
var
  MappedAbrv: array [ 0..15 ] of char;

begin
  Assert( pTray <> nil, 1 );
  { Assert: DrugNames and Mnemonics are also initialized }

  { Copy name or default }
  if DrugNames.FindMnemSeq( DBDRUGFile, @Mnemonics, Drug ) then
  begin
    If FilterMult then
    begin
      pTray^.MapDrgAbbr( Mnemonics.ID, MappedAbrv );
      StrCat( str, MappedAbrv );
    end
    else StrCat( Str, Mnemonics.ID );
  end
  else StrCat( Str, '*****' );
end;



{----------------------------------------------------------------------------}
{ This function copies a longint to a buffer. If the buffer is too small,
{ the function returns false and nothing is copied. The buffer must be at
{ least SizeOf( Longint ) for the function to succeede.
{
{ Input
{   value - the value to copy
{   pBuffer - pointer to a memory buffer to receive the value
{   SizeOfBuffer - the size of the memory buffer in bytes
{ Output
{   pBuffer^ - if successful, a copy of the value, otherwise unchanged,
{
{ Return value: a true value indicates the operation was successful
{----------------------------------------------------------------------------}

function LongintCopy( value: longint; pBuffer: pointer;
  SizeOfBuffer: Word ): boolean;
type
  PLongint = ^Longint;
var
  CanCopy: boolean;
begin
  CanCopy := SizeOfBuffer >= SizeOf( Longint );
  if CanCopy then
    PLongint( pBuffer )^ := value;
  LongintCopy := CanCopy;
end;



                        {----------------------}
                        {    KeyInformation    }
                        {----------------------}



type
  {--------------------------------------------------------------------------}
  { This type enumerates the set of keys that can be used with the SIR sort
  { engine. If this set is changed in any way, the code in this file must be
  { updated.
  {--------------------------------------------------------------------------}
  KeyNumbers =
  (
    KN_ORGANISM_SNAME,
    KN_TOTAL
  );


const
  {--------------------------------------------------------------------------}
  { The following constants specifies the numner of keys in the set above.
  { These values must be updated if the set size changes.
  {--------------------------------------------------------------------------}
  NUMBER_OF_KEYS = 2;
  MAXIMUM_KEY_NUMBER = Ord( KN_TOTAL );


type
  {--------------------------------------------------------------------------}
  { This object provides names for sort keys in group 2.
  {--------------------------------------------------------------------------}
  PGroup2KeyInformation = ^TGroup2KeyInformation;
  TGroup2KeyInformation = object( TKeyInformation )
    function GetKeyName( KeyNumber: integer; KeyName: PChar; Size: integer ):
      PChar; virtual;
    function NumberOfKeys: integer; virtual;
  end;



{----------------------------------------------------------------------------}
{ This function gets the name of a key given its number.
{----------------------------------------------------------------------------}

function TGroup2KeyInformation.GetKeyName( KeyNumber: integer;
  KeyName: PChar; Size: integer ): PChar;
begin
  case KeyNumbers( KeyNumber ) of
    KN_ORGANISM_SNAME :
      GetKeyName := SR( IDS_ORGANISM_SNAME, KeyName, Size );
    KN_TOTAL :
      GetKeyName := SR( IDS_TOTAL, KeyName, Size );
  else
    Assert( false, 2 );
  end;
end;



{----------------------------------------------------------------------------}
{ This function returns the number of keys in group 3.
{----------------------------------------------------------------------------}

function TGroup2KeyInformation.NumberOfKeys: integer;
begin
  NumberOfKeys := NUMBER_OF_KEYS;
end;



                        {----------------------}
                        {      constants       }
                        {----------------------}



const
  {--------------------------------------------------------------------------}
  { Set the width of the report. Units are in character columns. The report
  { width must be acceptable to the TReport.Init constructor.
  {--------------------------------------------------------------------------}
  REPORT_WIDTH = 120;

  {--------------------------------------------------------------------------}
  { Column data
  {--------------------------------------------------------------------------}
  FirstCol      = 27;
  MaxMICIndex   = 17;
  MICArray: array [ 1..MaxMICIndex ] of PChar =
  (
    '.03', '.06', '.12', '.25', '0.5', '1', '2', '4', '6', '8',
    '16', '25', '32', '64', '100', '128', '256'
  );



                        {----------------------}
                        {      TDrugLine       }
                        {----------------------}



type
  {--------------------------------------------------------------------------}
  { This object represents a line of drug MIC information.
  {--------------------------------------------------------------------------}
  PDrugLine = ^TDrugLine;
  TDrugLine = object( TObject )

    {Interface}
    micCounts: array[ 1..MaxMICIndex ] of longint;
    constructor Init( input_Drug: TSeqNum; input_Isolates: integer;
      input_gtIDx: word );
    procedure Print( prnInfo: PPrintInfo );
    function GetDrug: TSeqNum;

    {Implementation}
    private
    Drug: TseqNum;
    Isolates: longint;
    gtIdx: word;
  end;



{----------------------------------------------------------------------------}
{ The constructor initializes the line. The micCounts array will need updating
{ before the line is ready to print.
{----------------------------------------------------------------------------}

constructor TDrugLine.Init( input_Drug: TSeqNum; input_Isolates: integer;
  input_gtIDx: word );
var
  i: integer;
begin

  { Store inputs }
  Drug := input_Drug;
  Isolates := input_Isolates;
  gtIdx := input_gtIdx;

  { Zero MIC counters }
  for i := 1 To MaxMICIndex do
    micCounts[ i ] := 0;
end;



{----------------------------------------------------------------------------}
{ This function returns the drug reference in the data.
{----------------------------------------------------------------------------}

function TDrugLine.GetDrug: TSeqNum;
begin
  GetDrug := Drug;
end;



{----------------------------------------------------------------------------}
{ This procedure writes the drug line to the printout object.
{----------------------------------------------------------------------------}

procedure TDrugLine.Print( prnInfo: PPrintInfo );
var
  micIndex: word;
  accumCount: longint;
  percentVal: integer;
  mic90: word;
  lbl: array [ 0..80 ] of char;
  printStr: array [ 0..REPORT_WIDTH ] of char;
begin

  prnInfo^.body^.AddRow( r_Normal, 0 );

  {---- Format line }

  { pad }
  StrCopy( printStr, '  ' );

  { drug name }
  DrugNameCat( printStr, Drug );

  { isolate count }
  Str( Isolates, lbl );
  ConcatBlanks( FirstCol - StrLen( printStr ) - StrLen( lbl ) - 1,
    printStr, REPORT_WIDTH );
  StrLCat( printStr, lbl, REPORT_WIDTH );

  { start MIC array }
  StrLCat( printStr, '|', REPORT_WIDTH );

  { MIC levels }
  percentVal := 0;
  mic90 := 0;
  accumCount := 0;
  for micIndex := 1 To MaxMICIndex do
  begin

    { MIC level }
    Inc( accumCount, micCounts[ micIndex ] );
    if ( accumCount = 0 ) and ( gtIdx = micIndex ) then
      StrLCat( printStr, '   0', REPORT_WIDTH )
    else if micCounts[ micIndex ] = 0 then
      StrLCat( printStr, '    ', REPORT_WIDTH )
    else
    begin
      if Isolates <> 0 then
        percentVal := ( accumCount * 100 ) div Isolates;
      Str( percentVal, lbl );
      ConcatBlanks( 4 - StrLen( lbl ), printStr, REPORT_WIDTH );
      StrLCat( printStr, lbl, REPORT_WIDTH );
      if ( percentVal >= 90 ) and ( mic90 = 0 ) then
        mic90 := micIndex;
    end;
  end;

  { end MIC array }
  StrLCat( printStr, '|', REPORT_WIDTH );

  { MIC 90% }
  if mic90 = 0 then
    StrLCat( printStr, '  --', REPORT_WIDTH )
  else
  begin
    ConcatBlanks( 4 - StrLen( MICArray[ mic90 ] ), printStr, REPORT_WIDTH );
    StrLCat( printStr, MICArray[ mic90 ], REPORT_WIDTH );
  end;

  {---- Write line }
  prnInfo^.body^.AddCell( printStr, LT500, c_Stretch, 0 );
  printStr[ 0 ] := #0;
end;



                        {----------------------}
                        {    TMICParagraph     }
                        {----------------------}



type
  {--------------------------------------------------------------------------}
  { This object represents a paragraph of the report. It holds the paragrpah
  { data and provides a method to format it into a paragraph of text.
  { Based on TObject so that it can be inserted into a collection.
  {--------------------------------------------------------------------------}
  PMICParagraph = ^TMICParagraph;
  TMICParagraph = object( TObject )

    { Interface }
    constructor Init( newOrganism: TSeqNum; input_Isolates: longint );
    destructor Done; virtual;
    function GetOrg: TSeqNum;
    procedure AddLine( Line: PDrugLine );
    procedure Print( prnInfo: PPrintInfo );

    { Implementation }
    private
    organism: TSeqNum;       { the organism record's sequence number }
    Isolates: longint;       { number of tested isolates with this organism }
    pLines: PCollection;     { lines of body of paragraph }
  end;



{----------------------------------------------------------------------------}
{ The constructor initializes the object with an organism and tested count
{ and initializes the list of lines. Use AddLine to add lines to the
{ paragraph.
{----------------------------------------------------------------------------}

constructor TMICParagraph.Init( newOrganism: TSeqNum;
  input_Isolates: longint );
var
  i: integer;
begin
  pLines := New( PCollection, Init( 4, 4 ) );
  organism := newOrganism;
  Isolates := input_Isolates;
end;



{----------------------------------------------------------------------------}
{ The destructor cleans up.
{----------------------------------------------------------------------------}

destructor TMICParagraph.Done;
begin
  MSDisposeObj( pLines );
  inherited Done;
end;



{----------------------------------------------------------------------------}
{ This function returns the paragraph's organism.
{----------------------------------------------------------------------------}

function TMICParagraph.GetOrg: TSeqNum;
begin
  GetOrg := organism;
end;



{----------------------------------------------------------------------------}
{ This procedure adds a line to the body of the paragraph.
{----------------------------------------------------------------------------}

procedure TMICParagraph.AddLine( Line: PDrugLine );
begin
  pLines^.Insert( Line );
end;



{----------------------------------------------------------------------------}
{ This procedure formats the paragraph and writes it to the printout object.
{----------------------------------------------------------------------------}

procedure TMICParagraph.Print( prnInfo: PPrintInfo );
var
  printStr: array [ 0..REPORT_WIDTH ] of char;

  {--------------------------------------------------------------------------}
  { This procedure writes the paragraph header to the prontout object.
  {--------------------------------------------------------------------------}
  procedure PrintHeader;
  var
    buffer: array [ 0..FirstCol ] of char;
    count: longint;
    lbl: array [ 0..80 ] of char;
  begin

    {---- Line 1 }

    { Add start row }
    prnInfo^.body^.AddRow( r_StartGroup, 0 );

    {Write organism name}
    OrganismShortNameLCopy( organism, buffer, FirstCol );
    prnInfo^.body^.AddCell( buffer, LT500, c_Stretch, 0 );

    {---- Line 2 }

    { Add row }
    prnInfo^.body^.AddRow( r_Normal, 0 );

    { Convert isolates tested to string }
    Str( Isolates, lbl );

    { Read "Total" string. }
    SR( IDS_TOTAL_LBL, printStr, FirstCol );

    { Format line }
    ConcatBlanks( FirstCol - StrLen( printStr ) - StrLen( lbl ) - 1,
      printStr, REPORT_WIDTH );
    StrLCat( printStr, lbl, REPORT_WIDTH );
    prnInfo^.body^.AddCell( printStr, LT500, c_Stretch, 0 );

    {---- Line 3 - blank }
    prnInfo^.body^.AddRow( r_Normal, 0 );
  end;

  {--------------------------------------------------------------------------}
  { This procedure writes the paragraph's footer to the prontout object.
  {--------------------------------------------------------------------------}
  procedure PrintFooter;
  begin

    { Blank line }
    prnInfo^.body^.AddRow( r_EndGroup, 0 );
  end;

  {--------------------------------------------------------------------------}
  { This procedure writes a line to the printout object.
  {--------------------------------------------------------------------------}
  procedure PrintLine( p: PDrugLine ); far;
  begin
    p^.Print( prnInfo );
  end;

begin { Print }

  PrintHeader;
  pLines^.ForEach( @PrintLine );
  PrintFooter;

end;



                        {----------------------}
                        {    MIC SortEngine    }
                        {----------------------}



type
  {--------------------------------------------------------------------------}
  { This object provides a sort engine for the reports in this unit. See base
  { class description. This class defines a number of keys and methods to
  { access them
  {--------------------------------------------------------------------------}
  PMICSortEngine = ^MICSortEngine;
  MICSortEngine = object( SortEngine )
    constructor Init( input_Parameters: SortEngineParameters );
    procedure Input( index: integer; input_pParagraph: PMICParagraph );
    function GetItem( KeyNumber: integer; pItem: Pointer;
      MaxSize: Word ): boolean; virtual;
    function DataTypeOf( KeyNumber: integer ): SEKeyType; virtual;

    private
    pParagraph: PMICParagraph;
  end;




{----------------------------------------------------------------------------}
{ This constructor initializes the base class (see SortEngine) and clears
{ the record pointers to prevent misuse.
{----------------------------------------------------------------------------}

constructor MICSortEngine.Init( input_Parameters: SortEngineParameters );
const
  {--------------------------------------------------------------------------}
  { Looking at ref. b, the longest expected field is the organism's long name
  { at 65 bytes, thus, the following buffer size should hold any data set.
  {--------------------------------------------------------------------------}
  BUFFER_SIZE = SE_MAXLEVELS * 65;

begin
  inherited Init( input_Parameters, BUFFER_SIZE );
  pParagraph := nil;
end;



{----------------------------------------------------------------------------}
{ This function indicates the data type for each defined key.
{----------------------------------------------------------------------------}

function MICSortEngine.DataTypeOf( KeyNumber: integer ): SEKeyType;
begin
  case KeyNumbers( KeyNumber ) of
    KN_ORGANISM_SNAME :
      DataTypeOf := SEKT_ZSTRING;
    KN_TOTAL :
      DataTypeOf := SEKT_LONGINT;
  else
    Assert( false, 3 );
  end;
end;



{----------------------------------------------------------------------------}
{ See base class description. This override sets up record pointers
{ needed by GetItem and uses the specimen record's sequence number for the
{ sort index.
{----------------------------------------------------------------------------}

procedure MICSortEngine.Input( index: integer;
  input_pParagraph: PMICParagraph );
begin
  pParagraph := input_pParagraph;
  inherited Input( index );
  pParagraph := nil;
end;



{----------------------------------------------------------------------------}
{ See base class description. This override provides access to key
{ fields associated with a SIR report.
{----------------------------------------------------------------------------}

function MICSortEngine.GetItem( KeyNumber: integer; pItem: Pointer;
  MaxSize: Word ): boolean;
var
  SeqNum: TSeqNum;
begin

  Assert( MaxSize > 0, 4 );
  Assert( pParagraph <> nil, 5 );

  case KeyNumbers( KeyNumber ) of

    KN_ORGANISM_SNAME :
      GetItem := OrganismShortNameLCopy( pParagraph^.organism, pItem,
        MaxSize - 1 );

    KN_TOTAL :
      GetItem := LongintCopy( pParagraph^.Isolates, pItem,
        MaxSize );

  else
    GetItem := false;
  end;

end;



                        {----------------------}
                        {      TEpiReport      }
                        {----------------------}



type
  {--------------------------------------------------------------------------}
  { This object generates the report.
  {--------------------------------------------------------------------------}
  PEpiMICReport = ^TEpiMICReport;
  TEpiMICReport = object( TReport )

    { Interface }
    constructor Init( aParent: PWindowsObject; Title: PChar; useMPSS: boolean;
      SortParameters: SortEngineParameters );
    destructor Done; virtual;
    procedure Generate;

    { Implementation }
    private
    mpssObj: PMPSS;
    isoDB,
    epi1DB,
    epi2DB,
    epi3DB: PDBFile;
    pSorter: PMICSortEngine;   { nil if sorting not requested }
    procedure InitHeadings; virtual;
    procedure PrintReportBody;
    procedure BuildTempFiles;
  end;



{----------------------------------------------------------------------------}
{ This constructor places the object in a defined initial state. It also has
{ the user select MPSS rules, if enabled. If construction is successful, the
{ next step should be a call to InitHeadings.
{----------------------------------------------------------------------------}

constructor TEpiMICReport.Init( aParent: PWindowsObject; Title: PChar;
  useMPSS: boolean; SortParameters: SortEngineParameters );

  {--------------------------------------------------------------------------}
  { This function shows an error and calls the destructor if the test value
  { is true. It passes the test value through to facilitate calling Fail.
  {--------------------------------------------------------------------------}
  function DoneOnError( Error: boolean; UnitErrorNumber: integer ): boolean;
  begin
    if Error then
    begin
      ShowError( aParent, IDS_EPI_ERR, nil, MOD_EPIMIC+1, MOD_EPIMIC,
        UnitErrorNumber );
      Done;
    end;
    DoneOnError := Error;
  end;

var
  code: integer;

begin { Init }

  {---- Place object in a fail-safe state. }
  mpssObj := nil;
  isoDB := nil;
  epi1DB := nil;
  epi2DB := nil;
  epi3DB := nil;
  pSorter := nil;

  {---- Construct a report with title, column width, and no maximum lines. }
  if not inherited Init( aParent, Title, REPORT_WIDTH, 0 ) then
    Fail;

  {---- Arm the base class abort flag during the following initialization.
        If none fail, it will be disarmed at the end of this constructor. }
  abortPrinting := TRUE;

  {---- Optionally create sort engine }
  if SortParameters.NumberOfLevels > 0 then
  begin
    pSorter := New( PMICSortEngine, Init( SortParameters ) );
    if DoneOnError( pSorter = nil, 50 ) then
      Fail;
  end;

  {---- Create an isolate database object. }
  isoDB := New( PDBFile, Init( DBISOFile, '', dbOpenNormal ) );
  if DoneOnError( isoDB = nil, 51 ) then
    Fail;

  {---- Create a Multiple Parameter Search (MPSS) object driven by the
        isolate database. }
  mpssObj := New( PMPSS, Init( aParent, isoDB, IncludeUndefDates ) );
  if DoneOnError( mpssObj = nil, 52 ) then
    Fail;

  {---- If MPSS rules were specified, permit the user to select some or
        cancel. }
  if useMPSS and not mpssObj^.MPSSLoadRules( aParent, Title ) then
  begin
    Done;
    Fail;
  end;

  {---- Create a temporary ... database. Then, create an object
        for the database. }
  if DoneOnError( not CreateEPITMP1, 53 ) then
    Fail;
  epi1DB := New( PDBFile, Init( 'EPITMP1', '', dbOpenExclusive ) );
  if DoneOnError( epi1DB = nil, 54 ) then
    Fail;

  {---- Create a temporary ... database. Then, create an object
        for the database. }
  if DoneOnError( not CreateEPITMP2, 55 ) then
    Fail;
  epi2DB := New( PDBFile, Init( 'EPITMP2', '', dbOpenExclusive ) );
  if DoneOnError( epi2DB = nil, 56 ) then
    Fail;

  {---- Create a temporary epi. organism database. Then, create an
        object for the database. }
  if DoneOnError( not CreateEPITMP3, 57 ) then
    Fail;
  epi3DB := New( PDBFile, Init( 'EPITMP3', '', dbOpenExclusive ) );
  if DoneOnError( epi3DB = nil, 58 ) then
    Fail;

  {---- Construction complete, disarm the abort control. }
  abortPrinting := FALSE;

end;



{----------------------------------------------------------------------------}
{ This destructor disposes memory allocated to the object.
{----------------------------------------------------------------------------}

destructor TEpiMICReport.Done;
begin
  MSDisposeObj( pSorter );
  MSDisposeObj( epi1DB );
  MSDisposeObj( epi2DB );
  MSDisposeObj( epi3DB );
  MSDisposeObj( mpssObj );
  MSDisposeObj( isoDB );
  inherited Done;
end;



{----------------------------------------------------------------------------}
{ This function sets up the report headings. It should be called just after
{ the constructor. The next step should be a call AllowCancel (see base class)
{ and then call BuildTempFiles.
{----------------------------------------------------------------------------}

procedure TEpiMICReport.InitHeadings;
var
  micIndex: word;
  lbl: array [ 0..80 ] of char;

begin

  inherited InitHeadings;

  printStr^ := #0;
  ConcatDashes( printStrLen, printStr );
  prnInfo^.header^.AddRow( r_Normal, 0 );
  prnInfo^.header^.AddCell( printStr, LT500, c_Stretch, 0 );

  SR( IDS_ORG_LBL, printStr, printStrLen );
  SR( IDS_ISO_LBL, lbl, 80 );
  ConcatBlanks( FirstCol - StrLen( printStr ) - StrLen( lbl ) - 1, printStr );
  StrLCat( printStr, lbl, printStrLen );
  StrLCat( printStr, '|', printStrLen );
  SR( IDS_PERCENT_LBL, lbl, 80 );
  ConcatBlanks( ( MaxMICIndex * 4 ) - StrLen( lbl ), lbl );
  StrLCat( printStr, lbl, printStrLen );
  StrLCat( printStr, '|', printStrLen );
  SR( IDS_MIC_LBL, lbl, 80 );
  ConcatBlanks( 4 - StrLen( lbl ), printStr );
  StrLCat( printStr, lbl, printStrLen );
  prnInfo^.header^.AddRow( r_Normal, 0 );
  prnInfo^.header^.AddCell( printStr, LT500, c_Stretch, 0 );

  StrCopy( printStr, '  ' );
  SR( IDS_DRUG_LBL, lbl, 80 );
  StrLCat( printStr, lbl, printStrLen );
  SR( IDS_TESTED_LBL, lbl, 80 );
  ConcatBlanks( FirstCol - StrLen( printStr ) - StrLen( lbl ) - 1, printStr );
  StrLCat( printStr, lbl, printStrLen );
  StrLCat( printStr, '|', printStrLen );
  for micIndex := 1 To MaxMICIndex do
  begin
    ConcatBlanks( 4 - StrLen( MICArray[ MICIndex ]), printStr );
    StrLCat( printStr, MICArray[ micIndex ], printStrLen );
  end;
  StrLCat( printStr, '| 90%', printStrLen );
  prnInfo^.header^.AddRow( r_Normal, 0 );
  prnInfo^.header^.AddCell( printStr, LT500, c_Stretch, 0 );

  printStr^ := #0;
  ConcatDashes( printStrLen, printStr );
  prnInfo^.header^.AddRow( r_Normal, 0 );
  prnInfo^.header^.AddCell( printStr, LT500, c_Stretch, 0 );

  printStr^ := #0;
  prnInfo^.header^.AddRow( r_Normal, 0 );
end;



{----------------------------------------------------------------------------}
{ This function generates the body of the report. It should be called after
{ BuildTempFiles. The destructor should be called next.
{
{Org*      Drug*       MIC Index*     Count
{Org*      Drug*       Count
{Org*      Count
{----------------------------------------------------------------------------}

procedure TEpiMICReport.PrintReportBody;

  {--------------------------------------------------------------------------}
  { This function returns the organism's total count stored in the temp.
  { epi. organism database.
  {--------------------------------------------------------------------------}
  function OrganismTotalCount( Organism: TSeqNum ): longint;
  var
    count: longint;
  begin

    {---- Find the current organism in the temp. epi. organism database and
          get the count, otherwise, count is zero. }
    epi3DB^.dbr^.PutField( DBEPI3Org, @Organism );
    if epi3DB^.dbc^.GetEQ( epi3DB^.dbr ) then
    begin
      epi3DB^.dbr^.GetField( DBEPI3Count, @count, SizeOf( count ) );
      OrganismTotalCount := count;
    end
    else
      OrganismTotalCount := 0;
  end;

  {--------------------------------------------------------------------------}
  { Get the number of isolates tested with this organism/drug
  {--------------------------------------------------------------------------}
  procedure ReadDrugInfo( Organism, Drug: TSeqNum; var count: longint;
    var gtIdx: word );
  begin
    count := 0;
    epi2DB^.dbr^.PutField( DBEPI2Org, @Organism );
    epi2DB^.dbr^.PutField( DBEPI2Drug, @Drug );
    if epi2DB^.dbc^.GetEQ( epi2DB^.dbr ) then
    begin
      epi2DB^.dbr^.GetField( DBEPI2Count, @count, SizeOf( count ) );
      epi2DB^.dbr^.GetField( DBEPI2GTIdx, @gtIdx, SizeOf( gtIdx ) );
    end;
  end;

  {--------------------------------------------------------------------------}
  { This procedure directs a paragraph to print itself.
  {--------------------------------------------------------------------------}
  procedure PrintParagraph( p: PMICParagraph ); far;
  begin
    p^.Print( prnInfo );
  end;

  {--------------------------------------------------------------------------}
  { This procedure sets the text and zeros the percentage for the abort
  { dialog.
  {--------------------------------------------------------------------------}
  procedure InitDlg( var percent: integer );
  var
    lbl   : array [ 0..80 ] of char;
  begin
    SR( IDS_FORMATING_RPT, lbl, 80 );
    cancelDlg^.SetText( lbl, 0 );
    percent := 0;
    cancelDlg^.SetText( '0%', IDC_LBL2 )
  end;

  {--------------------------------------------------------------------------}
  { This procedure updates the percentages in the abort dialog.
  {--------------------------------------------------------------------------}
  procedure UpdateDlg( var percent: integer; newPercent: integer );
  var
    lbl   : array [ 0..80 ] of char;
  begin
    if newPercent <> percent then
    begin
      percent := newPercent;
      Str( percent, lbl );
      StrCat( lbl, '%' );
      cancelDlg^.SetText( lbl, IDC_LBL2 );
    end;
    List( '' );  { uses side effect of List to update abort dialog }
  end;

var
  gtIdx,
  micIndex: word;
  orgSeq,
  currOrg,
  drugSeq,
  currDrug: TSeqNum;
  PercentProcessed,
  newPercentProcessed: integer;
  i,
  count,
  NumberOfRecords,
  RecordsProcessed: longint;
  ParagraphList: TCollection;
  pParagraph: PMICParagraph;
  pLine: PDrugLine;

begin { PrintReportBody }

  {---- Access initial record in the organism/drug MIC data file. Exit
        function if no records exist. }
  if not epi1DB^.dbc^.GetFirst( epi1DB^.dbr ) then
    Exit;


  {---- Transition the abort dialog box from the creating state to the
        formatting state. }
  InitDlg( PercentProcessed );
  NumberOfRecords := epi1DB^.dbc^.NumRecords;
  RecordsProcessed := 0;

  {---- Initialize the current organism and drug from the initial record.
        Then, process all records in the database. Check for a pending
        user abort after processing each record. }
  epi1DB^.dbr^.GetField( DBEPI1Org, @currOrg, SizeOf( TSeqNum ) );
  epi1DB^.dbr^.GetField( DBEPI1Drug, @currDrug, SizeOf( TSeqNum ) );
  ParagraphList.Init( 100, 100 );
  pParagraph := New( PMICParagraph, Init( currOrg,
    OrganismTotalCount( currOrg ) ) );
  ReadDrugInfo( currOrg, currDrug, count, gtIdx );
  pLine := New( PDrugLine, Init( currDrug, count, gtIdx ) );
  repeat

    {---- Access next record}
    epi1DB^.dbr^.GetField( DBEPI1Org, @orgSeq, SizeOf( TSeqNum ) );
    epi1DB^.dbr^.GetField( DBEPI1Drug, @drugSeq, SizeOf( TSeqNum ) );
    if ( drugSeq <> pLine^.GetDrug ) or ( orgSeq <> pParagraph^.GetOrg ) then
    begin
      {Add line and start a new one}
      pParagraph^.AddLine( pLine );
      ReadDrugInfo( orgSeq, drugSeq, count, gtIdx );
      pLine := New( PDrugLine, Init( drugSeq, count, gtIdx ) );
    end;
    if orgSeq <> pParagraph^.GetOrg then
    begin
      {Add paragraph and start a new one}
      ParagraphList.Insert( pParagraph );
      pParagraph := New( PMICParagraph, Init( orgSeq,
        OrganismTotalCount( orgSeq ) ) );
    end;

    {---- Add data to line }
    epi1DB^.dbr^.GetField( DBEPI1MICIdx, @MICIndex, SizeOf( MICIndex ) );
    epi1DB^.dbr^.GetField( DBEPI1Count, @count, SizeOf( count ) );
    pLine^.micCounts[ micIndex ] := count;

    {---- Compute progress and update dialog box when outdated. }
    Inc( RecordsProcessed );
    newPercentProcessed := ( RecordsProcessed * 100 ) div NumberOfRecords;
    UpdateDlg( PercentProcessed, newPercentProcessed );

  until ( not epi1DB^.dbc^.GetNext( epi1DB^.dbr ) ) or userAbort;

  {---- Add last line to paragraph and paragraph to list }
  pParagraph^.AddLine( pLine );
  ParagraphList.Insert( pParagraph );

  {---- Print paragraphs }
  if not userAbort then
  begin
    if pSorter = nil then
      { Unsorted printing }
      ParagraphList.ForEach( @PrintParagraph )
    else
    begin
      { Sorted Printing }
      for i := 0 to ParagraphList.Count - 1 do
        pSorter^.Input( i, ParagraphList.At( i ) );
      pSorter^.Sort;
      while pSorter^.Output( i )  do
        PrintParagraph( ParagraphList.At( i ) );
    end
  end;

  {---- Cleanup }
  ParagraphList.Done;
end;



{----------------------------------------------------------------------------}
{ This private function compiles the databases needed by PrintReport. This
{ procedure should be called after the call to AllowCancel. The PrintReport
{ procedure should be called next.
{----------------------------------------------------------------------------}

procedure TEpiMICReport.BuildTempFiles;

  {--------------------------------------------------------------------------}
  { This procedure sets the text and zeros the percentage for the abort
  { dialog.
  {--------------------------------------------------------------------------}
  procedure InitDialog( var percent: integer; Title: PChar );
  begin
    cancelDlg^.SetText( Title, 0 );
    percent := 0;
    cancelDlg^.SetText( '0%', IDC_LBL2 )
  end;

  {--------------------------------------------------------------------------}
  { This procedure updates the percentages in the abort dialog.
  {--------------------------------------------------------------------------}
  procedure UpdateDialog( var percent: integer; newPercent: integer );
  var
    lbl   : array [ 0..80 ] of char;
  begin
    if newPercent <> percent then
    begin
      percent := newPercent;
      Str( percent, lbl );
      StrCat( lbl, '%' );
      cancelDlg^.SetText( lbl, IDC_LBL2 );
    end;
    List( '' );  { uses side effect of List to update abort dialog }
  end;

  {--------------------------------------------------------------------------}
  { This procedure performes a final update to the abort dialog.
  {
  { Does this need to set IDC_LBL2 to "100%" ?
  {--------------------------------------------------------------------------}
  procedure FinalDialogUpdate;
  begin
    List( '' );
  end;

type
  {--------------------------------------------------------------------------}
  { This type holds applicable fields from an isolate record.
  {--------------------------------------------------------------------------}
  TIsolate = record
    orgSeq: TSeqNum;
    QCFlag: boolean;
  end;


  {--------------------------------------------------------------------------}
  { This procedure loads an isolate variable from an isolate record.
  {--------------------------------------------------------------------------}
  procedure LoadIsolate( IsolateRec: PDBRec; var isolate: TIsolate );

    {------------------------------------------------------------------------}
    { This function reads the QC flag from the isolate record.
    {------------------------------------------------------------------------}
    function getQCFlag: boolean;
    var
      QCFlag: boolean;
    begin
      IsolateRec^.GetField( DBISOQCFlag, @QCFlag, SizeOf( QCFlag ) );
      getQCFlag := QCFlag;
    end;

    {------------------------------------------------------------------------}
    { This function reads the organism reference from the isolate record.
    {------------------------------------------------------------------------}
    function getOrgSeq: TSeqNum;
    var
      OrgSeq: TSeqNum;
    begin
      IsolateRec^.GetField( DBISOOrgRef, @OrgSeq, SizeOf( OrgSeq ) );
      getOrgSeq := OrgSeq;
    end;

  begin { LoadIsolate }
    with isolate do
    begin
      QCFlag := getQCFlag;
      OrgSeq := getOrgSeq;
    end
  end;

  {--------------------------------------------------------------------------}
  { This function qualifies an isolate variable for the report. It rejects
  { isolates that are invalid for this report or have nothing to contribute
  { to the report. Currently, it rejects records with one or more of the
  { following conditions:
  {
  {   a.  Its QC flag is set (true)
  {   b.  Its organism reference is null (0)
  {
  {--------------------------------------------------------------------------}
  function isQualifiedIsolate( isolate: TIsolate ): boolean;

    {------------------------------------------------------------------------}
    { This function rejects null references.
    {------------------------------------------------------------------------}
    function valid( num: TSeqNum ): boolean;
    begin
      valid := num <> 0;
    end;

  begin { isQualifiedIsolate }
    with isolate do
    begin
      isQualifiedIsolate := not QCFlag and valid( orgSeq );
    end
  end;


  {--------------------------------------------------------------------------}
  { This function qualifies the isolate's results for use in the report.
  { Currently, the simple ability to load results qualifies the results.
  {--------------------------------------------------------------------------}
  function QualifyResults( IsolateRec: PDBRec; results: PResults ):
    boolean;
  begin
    QualifyResults := results^.CanLoad( IsolateRec );
  end;

  {--------------------------------------------------------------------------}
  { This function loads the results for the isolate.
  {
  { Note: this function assumes the results qualified using QualifyResults.
  {--------------------------------------------------------------------------}
  procedure LoadResults( IsolateRec: PDBRec; results: PResults );
  var
    ok: boolean;
  begin
    ok := results^.LoadResults( IsolateRec );
    Assert( ok, 6 );
  end;

  {--------------------------------------------------------------------------}
  { This procedure adds data from the isolate and its results to the report
  { by updating the temp files.
  {--------------------------------------------------------------------------}
  procedure AddToReport( isolate: TIsolate; results: PResults );

    {------------------------------------------------------------------------}
    { This procedure increments the count for the given organism in the
    { temporary epi. organism database, adding a record if necessary.
    {------------------------------------------------------------------------}
    procedure TallyOrg( OrgSeq: TSeqNum );
    var
      count: longint;
    begin
      { Find/create the organism's record in the temporary epi. organism
        database and increment/set the count field. }
      epi3DB^.dbr^.PutField( DBEPI3Org, @OrgSeq );
      if epi3DB^.dbc^.GetEQ( epi3DB^.dbr ) then
      begin
        { Found record - increment count }
        epi3DB^.dbr^.GetField( DBEPI3Count, @count, SizeOf( count ) );
        Inc( count );
        epi3DB^.dbr^.PutField( DBEPI3Count, @count );
        epi3DB^.dbc^.UpdateRec( epi3DB^.dbr );
      end
      else
      begin
        { Create record - initialize count }
        count := 1;
        epi3DB^.dbr^.PutField( DBEPI3Count, @count );
        epi3DB^.dbc^.InsertRec( epi3DB^.dbr );
      end;
    end;

    {------------------------------------------------------------------------}
    { This procedure begins a series of organism/drug tallies for a given
    { organism. This procedure goes with the TallyOrgDrug procedure.
    {------------------------------------------------------------------------}
    procedure BeginOrgDrugTally( OrgSeq: TSeqNum );
    begin
      epi2DB^.dbr^.PutField( DBEPI2Org, @OrgSeq );
    end;

    {------------------------------------------------------------------------}
    { This procedure creates/updates the highest value and the count of MIC
    { indexes for an organism/drug pair.
    {
    { Assert: the organism was specified with the BeginOrgDrugTally procedure.
    {------------------------------------------------------------------------}
    procedure TallyOrgDrug( drugSeq: TSeqNum; micIndex: word;
      gtFlag: boolean );
    var
      count: longint;
      tInt: integer;
    begin
      epi2DB^.dbr^.PutField( DBEPI2Drug, @drugSeq );
      if epi2DB^.dbc^.GetEQ( epi2DB^.dbr ) then
      begin
        if gtFlag then
        begin
          epi2DB^.dbr^.GetField( DBEPI2GTIdx, @tInt, SizeOf( tInt ) );
          if tInt < micIndex then
            epi2DB^.dbr^.PutField( DBEPI2GTIdx, @MICIndex );
        end;
        epi2DB^.dbr^.GetField( DBEPI2Count, @count, SizeOf( count ) );
        Inc( count );
        epi2DB^.dbr^.PutField( DBEPI2Count, @count );
        epi2DB^.dbc^.UpdateRec( epi2DB^.dbr );
      end
      else
      begin
        if gtFlag then
          epi2DB^.dbr^.PutField( DBEPI2GTIdx, @MICIndex )
        else
        begin
          tInt := 0;
          epi2DB^.dbr^.PutField( DBEPI2GTIdx, @tInt );
        end;
        count := 1;
        epi2DB^.dbr^.PutField( DBEPI2Count, @count );
        epi2DB^.dbc^.InsertRec( epi2DB^.dbr );
      end;
    end;

    {------------------------------------------------------------------------}
    { This procedure begins a series of organism/drug tallies for a given
    { organism. This procedure goes with the TallyOrgDrugMIC procedure.
    {------------------------------------------------------------------------}
    procedure BeginOrgDrugMICTally( OrgSeq: TSeqNum );
    begin
      epi1DB^.dbr^.PutField( DBEPI1Org, @OrgSeq );
    end;

    {------------------------------------------------------------------------}
    { This procedure creates/increments the count for an organism/drug/MIC if
    { the greater than flag is false, if true and no record exists, it creates
    { a one with a count of zero.
    {
    { Assert: the organism was specified with the BeginOrgDrugMICTally
    { procedure.
    {------------------------------------------------------------------------}
    procedure TallyOrgDrugMIC( drugSeq: TSeqNum; MICIndex: word;
      gtFlag: boolean );
    var
      count: longint;
    begin
      epi1DB^.dbr^.PutField( DBEPI1Drug, @drugSeq );
      epi1DB^.dbr^.PutField( DBEPI1MICIdx, @MICIndex );
      if epi1DB^.dbc^.GetEQ( epi1DB^.dbr ) then
      begin

        {---- Found record }
        if not gtFlag then
        begin
          epi1DB^.dbr^.GetField( DBEPI1Count, @count, SizeOf( count ) );
          Inc( count );
          epi1DB^.dbr^.PutField( DBEPI1Count, @count );
          epi1DB^.dbc^.UpdateRec( epi1DB^.dbr );
        end;
      end
      else
      begin

        {---- Create Record }
        if gtFlag then
          count := 0
        else
          count := 1;
        epi1DB^.dbr^.PutField( DBEPI1Count, @count );
        epi1DB^.dbc^.InsertRec( epi1DB^.dbr );
      end;
    end;

  var
    count  : longint;
    resCntr: word;
    resRec : PResRecObj;
    found  : boolean;
    drugSeq: TSeqNum;
    MIC    : array [ 0..RESmaxStrResult ] of char;
    gtFlag : boolean;
    micIndex: word;

  begin { addToReport }

    TallyOrg( isolate.OrgSeq );

    { Tally org/drug and drug/MIC }
    BeginOrgDrugTally( isolate.orgSeq );
    BeginOrgDrugMICTally( isolate.orgSeq );
    for resCntr := 0 to results^.Count - 1 do
    begin

      resRec := results^.At( resCntr );
      if StrComp( resRec^.testCatID, 'MIC' ) <> 0 then
        Continue;

      if results^.GetFlag( resRec^.testID, 'NCCLS', REScontraIndicated ) then
        Continue;

      found := FALSE;
      drugSeq := resRec^.drugSeq;
      StrCopy( MIC, resRec^.result );
      gtFlag := ( MIC[ 0 ] = '>' );
      if gtFlag or ( MIC[ 0 ] = '<' ) or ( MIC[ 0 ] = ' ' ) then
        StrCopy( MIC, @MIC[ 1 ] );

      micIndex := 0;
      for count := 1 to MaxMICIndex do
        if StrComp( MICArray[ count ], MIC ) = 0 then
        begin
          found := TRUE;
          micIndex := count;
        end;

      if found and ( micIndex <> 0 ) then
      begin
        TallyOrgDrug( drugSeq, MICIndex, gtFlag );
        TallyOrgDrugMIC( drugSeq, MICIndex, gtFlag );
      end;
    end;
  end;

  {--------------------------------------------------------------------------}
  { This procedure adds the specified isolate to the report using the
  { addToReport procedure. The purpose of this procedure is to gather the
  { information needed by addToReport.
  {
  { Assumptions: The isolate is qualified for the report.
  {--------------------------------------------------------------------------}
  procedure compile( IsoSeq: TSeqNum );
  var
    isolate: TIsolate;
    ResultList: TResults;          { List for isolate results }
    pIsoDB: PDBFile;
    pIsolate: PDBRec;
    ok    : boolean;
  begin
    { Setup. }
    ResultList.Init;

    { Access the isolate record. }
    pIsoDB := New( PDBFile, Init( DBISOFile, '', dbOpenNormal ) );
    Assert( pIsoDB <> nil, 7 );
    ok := pIsoDB^.dbc^.GetSeq( pIsoDB^.dbr, IsoSeq );
    Assert( ok, 8 );
    pIsolate := pIsoDB^.dbr;

    { Add to report, no need to re-qualify }
    LoadIsolate( pIsolate, isolate );
    LoadResults( pIsolate, @ResultList );
    AddToReport( isolate, @ResultList );

    { Cleanup. Note, MSDisposeObj not required, item goes out of scope }
    Dispose( pIsoDB, Done );
    ResultList.Done;
  end;

var
  IsoSeq : TSeqNum;
  results: PResults;          { List for isolate results }
  isolate: TIsolate;
  percent: integer;
  pEDFilter: PEDFilterObj;
  PassTitle: array [ 0..63 ] of char;

begin { BuildTempFiles }

  { This function reduces the data then sends it to addToReport. }

  {---- Step 1: Select isolates. }
  if mpssObj^.MPSSGetFirst( isoDB ) then
  begin
    InitDialog( percent, SR( IDS_SELECTING, PassTitle,
      SizeOf( PassTitle ) - 1 ) );
    results := New( PResults, Init );
    Assert( results <> nil, 9 );
    pEDFilter := New( PEDFilterObj, Init );
    Assert( pEDFilter <> nil, 10 );
    repeat
      { Process isolate record: load just enough to qualify it, if ok,
        load its results and qualify them, if ok, add isolate to list. }
      LoadIsolate( isoDB^.dbr, isolate );
      if isQualifiedIsolate( isolate ) and
        QualifyResults( isoDB^.dbr, results ) then
        begin

          { Select isolate }
          IsoSeq := isoDB^.dbr^.GetSeqValue;
          Assert( IsoSeq <> -1, 11 );
          pEDFilter^.Input( IsoSeq );

        end;
      UpdateDialog( percent, mpssObj^.MPSSGetPercent );
    until ( not mpssObj^.MPSSGetNext( isoDB ) ) or userAbort;
    FinalDialogUpdate;
    MSDisposeObj( results );

    { If ok, process data in the filter to eliminate duplicates, then
      extract data from filter, using it to compile the report. }
    if not userAbort then
    begin

      {---- Step 2: Eliminate duplicates }
      pEDFilter^.Process;

      {---- Step 3: Compile selected isolates }
      InitDialog( percent, SR( IDS_COMPILING, PassTitle,
        SizeOf( PassTitle ) - 1 ) );
      while pEDFilter^.Output( IsoSeq ) and not userAbort do
      begin

        { Compile isolate }
        compile( IsoSeq );
        UpdateDialog( percent, pEDFilter^.PercentOutput );
      end;
      FinalDialogUpdate;
    end;

    {---- Cleanup. Note, MSDisposeObj not required, item goes out of scope }
    Dispose( pEDFilter, Done );
  end;

end;



{----------------------------------------------------------------------------}
{ After the object has been constructed, this public procedure steps through
{ the printing proces.
{----------------------------------------------------------------------------}

procedure TEpiMICReport.Generate;
begin
  InitHeadings;
  AllowCancel;
  BuildTempFiles;
  if not userAbort then
    PrintReportBody;
end;



                        {----------------------}
                        {   EXPORT function    }
                        {----------------------}


{----------------------------------------------------------------------------}
{ This command-level procedure steps the user through the process of
{ generating an antimicrobial MIC report.
{
{ Inputs:
{   pParent - pointer to the parent window
{   useMPSS - true if the user wants to select MPSS rules
{
{----------------------------------------------------------------------------}

procedure EpiAntimicrobicMIC( pParent: PWindowsObject; useMPSS: boolean );

const
  NO_BACK = false;

  {--------------------------------------------------------------------------}
  { This constant defines the INI file section name for group 2 sort
  { parameters. It must be unique within the file.
  {--------------------------------------------------------------------------}
  SectionName = 'Reports - G2 Sort Parameters';

  {--------------------------------------------------------------------------}
  { This function loads the report title.
  {--------------------------------------------------------------------------}
  function LoadTitle( Buffer: PChar; SizeOfBuffer: Word ): PChar;
  begin
    LoadTitle := SR( IDS_EPI_MIC_TITLE, Buffer, SizeofBuffer );
  end;

var
  pReport: PEpiMICReport;
  pWCur: PWaitCursor;
  SortParameters: SortEngineParameters;
  KeyInformation: TGroup2KeyInformation;
  Title: array[ 0..64 ] of char;

begin

  {---- Load default inputs }
  SortParameters.LoadFromINI( SectionName, MAXIMUM_KEY_NUMBER );
  KeyInformation.Init;
  LoadTitle( Title, SizeOf( Title ) );

  {---- Set up DrugNameCat variables }
  DrugNames.Init;
  Mnemonics.Init;
  FilterMult := ScreenOn( FilterMultiDrugs );
  pTray := New( PTraysObject, Init );

  {---- Let user view/edit sort parmeters and chose to continue or cancel. If
        user decided to continue, generate report and update default sort
        parameters. }
  if SortParametersDialog( pParent, Title, @KeyInformation, NO_BACK,
    SortParameters ) = id_Next then
  begin

    {---- Construct the generator within a wait style cursor. }
    pWCur :=  New( PWaitCursor, Init );
    pReport := New( PEpiMICReport, Init( pParent, Title, useMPSS,
      SortParameters ) );
    MSDisposeObj( pWCur );

    {---- If successful, use it to generate the report then dispose it
          (MSDisposeObj not needed). At this point, update the sort
          parameters in the INI file. }
    if pReport <> nil then
    begin
      pReport^.Generate;
      SortParameters.StoreToINI( SectionName );
      Dispose( pReport, Done );
    end;

  end;

  { Cleanup DrugNameCat variables. Note: MSDisposeObj is used because pTray
    does not go out of scope. }
  MSDisposeObj( pTray );
  Mnemonics.Done;
  DrugNames.Done;
end;



{----------------------------------------------------------------------------}
{ Unit initialization. Needed to insure DrugNameCat varaibles are not used
{ before they are initialized. pTray is set to nil here and tested for nil in
{ DrugNameCat. It's assumes if this variable has been initialized, then the
{ others have been too.
{----------------------------------------------------------------------------}

BEGIN
  pTray := nil;
END.