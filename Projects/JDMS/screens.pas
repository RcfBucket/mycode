Unit Screens;
{----------------------------------------------------------------------------}
{  Module Name  : Screens.pas                                                }
{  Programmer   : FL                                                         }
{  Date Created : 04/98                                                      }
{                                                                            }
{  Purpose     -  Logic to screen for VIE/VRE/MRS/Ecoli 0157.               }
{                 ESBL Logic resides within its own unit ESBL.pas            }
{                                                                            }
{  Assumptions -                                                             }
{  None.                                                                     }
{                                                                            }
{  Revision History -                                                        }
{                                                                            }
{  Version  Date      Who    Did What                                        }
{  -------  --------  ------ ----------------------------------------------- }
{  1.00     05/98     FL     Initial release.                                }
{                                                                            }
{----------------------------------------------------------------------------}

Interface

Uses
  BioType,
  Objects,
  OWindows,
  ResLib,
  WinAPI,
  WinProcs,
  WinTypes;

Const
  ESBLScreen     ='ESBLScreening';
  VIEScreen      ='VIEScreening';
  VREScreen      ='VREScreening';
  MRSScreen      ='MRSScreening';
  E157Screen     ='EcoliScreening';
  AutoResolveKey ='AutoResolve';
  VaOnlyHold     ='VaOnlyHold';
  VaStSHold      ='VaStSHold';
  OxMultResHold  ='OxMultResHold';
  PrintXtrasKey  ='PrintXtras';

  IBInterps      ='IBInterps';
  IBMappings     ='IBMappings';
  BLacfname      ='BLactam.dat';
  FilterMultiDrugs='FilterMultiDrugs';

  ScreensSection ='Screens';
  VIESection     ='VIE';
  VRESection     ='VRE';
  MRSSection     ='MRS';
  E157Section    ='E157';
  ESBLSection    ='ESBL';

Type
  PScreens = ^TScreens;
  TScreens =
  Object(TObject)
    E157Found,
    ESBLConfPos,
    ESBLConfNeg,
    ESBLSuspected,
    VIEFound,
    VREFound,
    MRSFound,
    OrganismMapped: Boolean;

    constructor Init;
    procedure Copy(xScreens: PScreens);
    procedure ClearScreens;
    function ScreensFound: boolean;
  End;


  function  ScreenOn(ScreenPrivateKey: PChar): Boolean;
  procedure EnableScreen(ScreenPrivateKey: PChar; ScreenSetting: Boolean);

  function  AutoResolveOn: Boolean;
  procedure EnableAutoResolve(AutoResolveSetting: Boolean);

  function VIECriterion (Organism: Integer; Results: PResults): Boolean;
  function VRECriterion (Organism: Integer; Results: PResults): Boolean;
  function MRSCriterion (Organism: Integer; Results: PResults): Boolean;
  function E157Criterion(Organism: Integer; SetFamily: Word; Results: PResults): Boolean;
  function ESBLMappable (Organism : Integer;
                     var MappedOrg: Integer;
                         Results  : PResults): Boolean;

  function ScreenApplies(ScreenName: PChar; Organism: Integer): boolean;

  function SearchOrgMaps(ScreenName: PChar;
                         Organism  : Integer;
                     var MappedOrg : Integer;
                         Direct    : Boolean): Boolean;

  procedure ScreenIsolate(aParent : PWindowsobject;
                          Organism: Integer;
                          var MappedOrg: Integer;
                          xSet         : byte;
                          Results      : PResults;
                          xScreens     : PScreens;
                          xSilent      : boolean;
                          xUseESBLRes  :  boolean);

  function GetBLacMappings: String;

Implementation

Uses
  DlgLib,
  ESBL,
  MScan,
  Strings,
  StrSW,
  Trays;

{$R Screens.res}
{$I Screens.inc}

Const
  sOn            ='Enabled';
  sOff           ='Disabled';
  ScreensProfiles='ScrnMaps.ini';

Type
  PScreensDlg = ^TScreensDlg;
  TScreensDlg = object(TCenterDlg)
    ScreenResults: PScreens;
    UserResults  : PScreens;

    constructor Init(aParent     : PWindowsobject;
                     xScreens: PScreens);
    destructor done; virtual;

    procedure SetupWindow; virtual;
    function  CanClose: boolean; virtual;
    procedure ReDrawDialog;
    procedure WMCommand(var msg: TMessage); virtual WM_FIRST + WM_Command;
  end;


  {---------------------------------------------------------[ TScreens ]--}
  constructor TScreens.Init;
  begin
    ClearScreens;
  end;

  Procedure TScreens.Copy(xScreens: PScreens);
  begin
    VIEFound:=xScreens^.VIEFound;
    VREFound:=xScreens^.VREFound;
    MRSFound:=xScreens^.MRSFound;
    ESBLConfPos:=xScreens^.ESBLConfPos;
    ESBLConfNeg:=xScreens^.ESBLConfNeg;
    ESBLSuspected:=xScreens^.ESBLSuspected;
    E157Found:=xScreens^.E157Found;
    OrganismMapped:=xScreens^.OrganismMapped;
  end;

  procedure TScreens.ClearScreens;
  begin
    VIEFound:=false;
    VREFound:=false;
    MRSFound:=false;
    ESBLConfPos:=false;
    ESBLConfNeg:=false;
    ESBLSuspected:=false;
    E157Found:=false;
    OrganismMapped:=false;
  end;

  function TScreens.ScreensFound: boolean;
  begin
    ScreensFound:= VIEFound or VREFound or MRSFound or
                   ESBLConfPos or ESBLConfNeg or ESBLSuspected or E157Found;
  end;


  {---------------------------------------------------------[ TScreensDlg ]--}
  constructor TScreensDlg.Init(aParent : PWindowsobject; xScreens: PScreens);
  begin
    inherited Init(aParent, MakeIntResource(DLG_Screens));
    ScreenResults:=xScreens;
    UserResults:=New(PScreens,Init);
    UserResults^.Copy(ScreenResults);
  end;

  procedure TScreensDlg.ReDrawDialog;
  var ESBLAvail: boolean;
  begin
    ESBLAvail:=ScreenResults^.ESBLSuspected and not UserResults^.E157Found;
    EnableWindow(GetItemHandle(IDC_ESBLGroup), ESBLAvail);
    EnableWindow(GetItemHandle(IDC_ESBLPos), ESBLAvail);
    EnableWindow(GetItemHandle(IDC_ESBLNeg), ESBLAvail);
    EnableWindow(GetItemHandle(IDC_EBL), ESBLAvail);
  end;

  procedure TScreensDlg.SetupWindow;
  begin
    inherited SetupWindow;

    {Set some defaults}
    With ScreenResults^ do
    begin
      SendDlgItemMsg(IDC_EBL, BM_SETCHECK, 1,0);

      if (E157Found) then
          SendDlgItemMsg(IDC_E157, BM_SETCHECK, 1,0)
      else
      begin
        EnableWindow(GetItemHandle(IDC_E157Group), FALSE);
        EnableWindow(GetItemHandle(IDC_E157), FALSE);
      end;
    end;

    {Grey out those controls that should not be available}
    ReDrawDialog;
  end;

  procedure TScreensDlg.WMCommand(var msg: TMessage);
  begin
    inherited WMCommand(msg);
    CanClose;
  end;

  function TScreensDlg.CanClose: boolean;
  begin
    With UserResults^ do
    begin
      E157Found:=SendDlgItemMsg(IDC_E157, BM_GETCHECK, 0,0) <> 0;

      if ScreenResults^.ESBLSuspected then
      begin
        ESBLConfPos  :=not E157Found and (SendDlgItemMsg(IDC_ESBLPos, BM_GETCHECK, 0,0) <> 0);
        ESBLConfNeg  :=not E157Found and (SendDlgItemMsg(IDC_ESBLNeg, BM_GETCHECK, 0,0) <> 0);
        ESBLSuspected:=not E157Found and (SendDlgItemMsg(IDC_EBL, BM_GETCHECK, 0,0) <> 0);
      end;

      ReDrawDialog;
    end;
    CanClose:=inherited CanClose;
  end;

  destructor TScreensDlg.done;
  begin
    ScreenResults^.Copy(UserResults);
    MSDisposeObj(UserResults);

    Inherited Done;
  end;


  {---------------------------------------------------------[ Screen Resolution Routines ]--}
  procedure ResolveScreens(aParent: PWindowsobject; xScreens: PScreens);
  var ScreensDlg: PScreensDlg;
  begin
    if xScreens^.ScreensFound then
    begin
      ScreensDlg:=New(PScreensDlg,Init(aParent,xScreens));
      Application^.ExecDialog(ScreensDlg);
    end;
  end;

  procedure ScreenIsolate(aParent : PWindowsobject;
                          Organism: Integer;
                          var MappedOrg: Integer;
                          xSet         : byte;
                          Results      : PResults;
                          xScreens     : PScreens;
                          xSilent      : boolean;
                          xUseESBLRes  : boolean);
  var UserPrompt: boolean;
      E157Mapped: boolean;
      ESBLResult: array [0..4] of char;
  begin
    With xScreens^ do
    begin
      ClearScreens;
      MappedOrg:=Organism;

      {Reverse Mapped organisms to regular orgs}
      If SearchOrgMaps(E157Section, Organism, MappedOrg, False) or
         SearchOrgMaps(ESBLSection, Organism, MappedOrg, False) or
         SearchOrgMaps(MRSSection , Organism, MappedOrg, False) or
         SearchOrgMaps(VIESection , Organism, MappedOrg, False) or
         SearchOrgMaps(VRESection , Organism, MappedOrg, False) then;

      {Inspect the Isolate for a possible E.coli O:157 H:7}
      E157Found  := E157Criterion(MappedOrg,xSet,Results);
      E157Mapped := SearchOrgMaps(E157Section, Organism, MappedOrg, False);

      {Inspect the Isolate for a possible ESBL}
      ESBLSuspected := (not E157Mapped) and ESBLCriterion(MappedOrg,Results);

      {See if it's necessary to prompt}
      UserPrompt:= (E157Found and not E157Mapped) or
                   (ESBLSuspected and not (xSilent or xUseESBLRes));

      {If needed, pop up a window for the user to decide}
      if UserPrompt then ResolveScreens(aParent,xScreens);

      {Set the ESBL Result}
      if ESBLConfPos then Results^.SetESBLResult('+',True)
         else if ESBLConfNeg then Results^.SetESBLResult('-',True)
              else if (ESBLSuspected and not E157Found) then
                       {Attempt to use an existing result}
                       if xUseESBLRes and (not UserPrompt) then
                       begin
                         {Default to EBL? if the user left it blank}
                         Results^.GetESBLResult(ESBLResult,4);
                         Results^.SetESBLResult('?',StrComp(ESBLResult,'')=0);
                       end
                       else Results^.SetESBLResult('?',True)
                   else Results^.SetESBLResult('',True);

      {Organism remapping logic: E157, ESBL, VIE, VRE, MRS}
      if E157Found
         then SearchOrgMaps(E157Section, MappedOrg, MappedOrg, True)
         else if (Results^.ESBLResultExists) then
              begin
                ESBLMappable(MappedOrg, MappedOrg, Results);
              end
              else if VIECriterion(MappedOrg,Results) then
                   begin
                     VIEFound:=True;
                     SearchOrgMaps(VIESection, MappedOrg, MappedOrg, True);
                   end
                   else if VRECriterion(MappedOrg,Results) then
                        begin
                          VREFound:=True;
                          SearchOrgMaps(VRESection, MappedOrg, MappedOrg, True)
                        end
                        else if MRSCriterion(Organism,Results) then
                             begin
                               MRSFound:=True;
                               SearchOrgMaps(MRSSection, MappedOrg, MappedOrg, True);
                             end;

      {Don't care for E157Found when the organism has already been mapped}
      if E157Mapped then E157Found:=False;

      {Has the organism been remapped by any Screen?}
      OrganismMapped:=Organism<>MappedOrg;
    end;{with xScreens^ do}
  end;


  {---------------------------------------------------------[ Customization Routines ]--}

  function ScreenOn(ScreenPrivateKey: PChar): Boolean;
  var ScreenSetting: array [0..15] of char;
  begin
    GetPrivateProfileString(ScreensSection, ScreenPrivateKey, sOff, ScreenSetting, 15, profFile);
    ScreenOn:=StrIComp(ScreenSetting, sOn)=0;
  end;

  procedure EnableScreen(ScreenPrivateKey: PChar; ScreenSetting: Boolean);
  begin
    if ScreenSetting
       then WritePrivateProfileString(ScreensSection, ScreenPrivateKey, sOn, profFile)
       else WritePrivateProfileString(ScreensSection, ScreenPrivateKey, sOff, profFile);
  end;

  function AutoResolveOn: Boolean;
  begin
    AutoResolveOn:=ScreenOn(ESBLScreen) and ScreenOn(AutoResolveKey);
  end;

  procedure EnableAutoResolve(AutoResolveSetting: Boolean);
  begin
    EnableScreen(AutoResolveKey,AutoResolveSetting);
  end;


  {---------------------------------------------------------[ OrgMapping Routines ]--}

  function ESBLMappable (Organism : Integer;
                     var MappedOrg: Integer;
                         Results  : PResults): Boolean;
  var ESBLResult: array[0..4] of char;

  begin
     ESBLMappable:=
       SearchOrgMaps(ESBLSection, Organism, MappedOrg,
                    (Results^.ESBLResultExists) and
                    (Results^.GetESBLResult(ESBLResult,4)) and
                    (ESBLResult[0] in ['?', '+']) and
                    (ScreenOn(ESBLOrgs)));
  end;

  function SearchOrgMaps(ScreenName: PChar;
                         Organism  : Integer;
                     var MappedOrg : Integer;
                         Direct    : Boolean): Boolean;
  var OrgStr, MappedOrgStr: array[0..4] of char;
      MappingSection      : array[0..64] of char;
      IniOrg,
      Err: Integer;
  begin
    If Direct then
       StrCopy(MappingSection, ScreenName)
    else
    begin
      StrCopy(MappingSection,'r');
      StrCat(MappingSection, ScreenName);
     end;

    Str(Organism,OrgStr);
    GetPrivateProfileString(MappingSection, OrgStr, sOff, MappedOrgStr, 4, ScreensProfiles);
    Val(MappedOrgStr, IniOrg, Err);
    if Err=0 then MappedOrg:=IniOrg;
    SearchOrgMaps:=Err=0;
  end;

  function ScreenApplies(ScreenName: PChar; Organism: Integer): boolean;
  var MappedOrg: Integer;
  begin
    ScreenApplies:=SearchOrgMaps(ScreenName,Organism,MappedOrg,True) or
                   SearchOrgMaps(ScreenName,Organism,MappedOrg,False);
  end;

  function VIECriterion(Organism: Integer; Results: PResults): Boolean;
  var TestResult: array [0..15] of char;
  begin
    VIECriterion := ScreenOn(VIEScreen) and
                    ScreenApplies(VIESection, Organism) and
                    Results^.GetResult('Vcm', 'NCCLS', TestResult, 15) and
                    (StrComp(TestResult,'I')=0);
  end;

  function VRECriterion(Organism: Integer; Results: PResults): Boolean;
  var TestResult: array [0..15] of char;
  begin
    VRECriterion := ScreenOn(VREScreen) and
                    ScreenApplies(VRESection, Organism) and
                    Results^.GetResult('Vcm', 'NCCLS', TestResult, 15) and
                    (StrComp(TestResult,'R')=0);
  end;

  function MRSCriterion(Organism: Integer; Results: PResults): Boolean;
  var TestResult: array [0..15] of char;
  begin
    MRSCriterion := ScreenOn(MRSScreen) and
                    ScreenApplies(MRSSection, Organism) and
                    Results^.GetResult('Mpc', 'NCCLS', TestResult, 15) and
                    (StrComp(TestResult,'R')=0);
  end;

  function SORNegative(var BioObj: PBioTypeObject): boolean;
  var Bit, Value, Well, Group: Integer;
      ResultStr : array [0..255] of char;
      ttt       : array [0..255] of char;
      ThisRes   : array [0..5] of char;
  begin
    SORNegative:=false;

    for Bit:=0 to BioObj^.MaxBitNum do
        if BioObj^.BitInfo(Bit, Value, Well, Group, ResultStr) then
           if StrComp('Sor',ResultStr)=0 then
              SORNegative:=Value=0;
  end;

  function E157Criterion(Organism: Integer; SetFamily: Word; Results: PResults): Boolean;
  var BioObj : PBiotypeObject;
      OrgSet : integer;
      err    : integer;
      BioRes : PResRecObj;

      function IsBioType(Res: PResRecObj): Boolean; far;
      begin
        IsBioType:=StrComp(Res^.TestCatID, 'BIO')=0;
      end;

  begin
    E157Criterion:=False;
    if ScreenOn(E157Screen) and  ScreenApplies(E157Section, Organism) then
    begin
      BioRes:=Results^.FirstThat(@IsBioType);

      if (BioRes<>Nil) and
         (Results^.TraysObj^.MapTestGroupID(BioRes^.TestGroupID) > 0) then
      begin
        OrgSet:=ExtractSet(Setfamily);
        Results^.TraysObj^.LoadTray(BioRes^.TestGroupID);

        if (Results^.TraysObj^.tfsRec.pset=1) then
        begin
          BioObj := New(PBiotypeObject, Init(@Results^.TraysObj^.tfsRec));
          BioObj^.SetBioType(OrgSet,BioRes^.Result);

          If ((BioObj^.MaxBitNum+2) div 3) <> StrLen(BioRes^.Result)
             {biotype is not the right format, prompt to allow the user to decide}
              then  E157Criterion:= True
             {biotype is right format, check sorbitol}
             else E157Criterion:=SORNegative(BioObj);

          MSDisposeObj(BioObj);
        end;
      end;
    end;
  end;

  function GetBLacMappings: String;
  var IBInterps: array [0..15] of char;
  begin
    GetPrivateProfileString(ScreensSection, IBMappings, '', IBInterps, 15, profFile);
    GetBlacMappings:=IBInterps;
  end;

End.
