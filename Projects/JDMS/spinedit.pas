unit SpinEdit;

{--------------------------------------------------------------------------}
{ SpinEdit control.                                                        }
{                                                                          }
{ This control provides all of the functionality of the normal TEdit       }
{ control with the added feature of a built in spin button.                }
{ The user can use the spin button with the mouse or press up/down to spin }
{ with the keyboard.                                                       }
{                                                                          }
{ Two added virtual methods are automatically called when the spinner is   }
{ pressed.                                                                 }
{                                                                          }
{ Date  Who    Did What                                                    }
{ ----- ------ ----------------------------------------------------------- }
{ 11/94 RCF    Created the control.                                        }
{                                                                          }
{--------------------------------------------------------------------------}

INTERFACE

{$R SPINEDIT.RES}

uses
  Win31,
  Strings,
  WinProcs,
  WinTypes,
  OWindows,
  ODialogs;

type
  PSpinEdit = ^TSpinEdit;
  TSpinEdit = object(TEdit)
    constructor Init(aParent: PWindowsObject; anId: integer; aTitle: PChar;
                     x,y,w,h, aTextLen: integer; multiline: boolean; aSpinWidth: integer);
    constructor InitResource(AParent: PWindowsObject; resourceID, aTextLen, aSpinWidth: integer);
    procedure SetupWindow; virtual;
    procedure SpinUp; virtual;
    procedure SpinDown; virtual;
    procedure SetSpinDelay(ms: integer);
    procedure SetInitialDelay(ms: integer);
    procedure WMKillFocus(var msg: TMessage); virtual WM_FIRST or WM_KILLFOCUS;
    procedure WMKeyDown(var msg: TMessage); virtual WM_FIRST or WM_KEYDOWN;
    function  FldIsBlank: boolean; virtual;

    private
    spinWidth  : integer; {- width of spin control }
    um, lm     : integer; {- upper and lower midpoints }
    buttonPress: integer; {- 0=no press, -1=down pressed, 1=up pressed }
    sDelay     : integer; {- spin repeat delay }
    initDelay  : integer; {- initial repeat delay }
    spinning   : boolean; {- T=spin down, F=spin up }
    gotCapture : boolean;
    {}
    procedure Initialize(aSpinWidth: integer);
    procedure DrawIt;
    procedure CalcMidpoint(var r: TRect);
    procedure StartTimer;
    procedure EndTimer;
    procedure WMNCCalcSize(var msg: TMessage); virtual WM_FIRST + WM_NCCALCSIZE;
    procedure WMEraseBkGnd(var msg: TMessage); virtual WM_FIRST + WM_ERASEBKGND;
    procedure WMNCLButtonDown(var msg: TMessage);  virtual WM_FIRST + WM_NCLBUTTONDOWN;
    procedure WMNCLButtonDblClk(var msg: TMessage);  virtual WM_FIRST + WM_NCLBUTTONDBLCLK;
    procedure WMNCHitTest(var msg: TMessage);  virtual WM_FIRST + WM_NCHITTEST;
    procedure WMTimer(var msg: TMessage); virtual WM_FIRST + WM_TIMER;
    procedure WMLButtonUp(var msg: TMessage);  virtual WM_FIRST + WM_LBUTTONUP;
  end;

  PSpinReal = ^TSpinReal;
  TSpinReal = object(TSpinEdit)
    min, max  : double;
    incr      : double;
    fact      : double;
    constructor Init(aParent: PWindowsObject; anId: integer; aTitle: PChar;
                     x,y,w,h, aTextLen: integer; multiline: boolean; aSpinWidth: integer;
                     aMin, aMax, anInc: double);
    constructor InitResource(AParent: PWindowsObject; resourceID, aTextLen, aSpinWidth: integer;
                     aMin, aMax, anInc: double);
    procedure WMChar(var msg: TMessage); virtual WM_FIRST + WM_CHAR;
    procedure SpinUp; virtual;
    procedure SpinDown; virtual;
    function GetDouble: double; virtual;
    function GetLong: longint; virtual;
    function NumIsValid: boolean; virtual;
    function NumInRange: boolean; virtual;
    procedure SetNum(num: double); virtual;
    procedure WMKillFocus(var msg: TMessage); virtual WM_FIRST or WM_KILLFOCUS;
    private
    function ValidChar(ch: char): boolean; virtual;
    procedure IncDec(dy: double);
  end;

  PSpinInt = ^TSpinInt;
  TSpinInt = object(TSpinReal)
    constructor Init(aParent: PWindowsObject; anId: integer; aTitle: PChar;
                     x,y,w,h, aTextLen: integer; multiline: boolean; aSpinWidth: integer;
                     aMin, aMax, anInc: longint);
    constructor InitResource(AParent: PWindowsObject; resourceID, aTextLen, aSpinWidth: integer;
                     aMin, aMax, anInc: longint);
    procedure SetNum(num: double); virtual;
    private
    function ValidChar(ch: char): boolean; virtual;
  end;

IMPLEMENTATION

uses
  Ctl3D,
  IntlLib,
  StrsW,
  DMSDebug;

{$I SPINEDIT.INC}

var
  hbUp  : HBitmap;  {- up and down bitmaps }
  hbDown: HBitmap;

constructor TSpinEdit.Init(aParent: PWindowsObject; anId: integer; aTitle: PChar;
                          x,y,w,h, aTextLen: integer; multiline: boolean; aSpinWidth: integer);
begin
  inherited Init(aParent, anID, aTitle, x,y,w,h, aTextLen, multiline);
  Initialize(aSpinWidth);
end;

constructor TSpinEdit.InitResource(AParent: PWindowsObject; resourceID, aTextLen, aSpinWidth: integer);
begin
  inherited InitResource(AParent, resourceID, aTextLen);
  Initialize(aSpinWidth);
end;

procedure TSpinEdit.Initialize(aSpinWidth: integer);
begin
  spinning:= false;
  sDelay:= 50;
  initDelay:= 600;
  buttonPress:= 0;
  spinWidth:= aSpinWidth;
  gotCapture:= false;
  if spinWidth < 12 then
    if spinWidth < 0 then
      spinWidth:= 0
    else
      spinWidth:= 12;
end;

procedure TSpinEdit.SetupWindow;
var
  r  : TRect;
begin
  inherited SetupWindow;
  GetWindowRect(HWindow, r);
  r.right:= r.right - r.left;
  r.bottom:= r.bottom - r.top;
  if spinWidth > 0 then
  begin
    SetWindowPos(HWindow, HWND_TOP, r.left, r.top, r.right+1, r.bottom, SWP_NOZORDER or SWP_NOMOVE);
    SetWindowPos(HWindow, HWND_TOP, r.left, r.top, r.right, r.bottom, SWP_NOZORDER or SWP_NOMOVE);
  end;
end;

procedure TSpinEdit.SpinUp;
begin
  {- default does nothing }
end;

procedure TSpinEdit.SpinDown;
begin
  {- default does nothing }
end;

procedure TSpinEdit.SetSpinDelay(ms: integer);
begin
  if ms > 0 then
    sDelay:= ms;
end;

procedure TSpinEdit.SetInitialDelay(ms: integer);
begin
  if ms > 0 then
    initDelay:= ms;
end;

procedure TSpinEdit.WMNCCalcSize(var msg: TMessage);
var
  lpnCSP : PNCCalcSize_Params;
  k      : integer;
begin
  lpnCSP:= PNCCalcSize_Params(msg.lParam);
  DefWndProc(msg);
  if spinWidth > 0 then
    Dec(lpnCSP^.rgrc[0].right, spinWidth);
end;

procedure TSpinEdit.WMNCHitTest(var msg: TMessage);
var
  pt   : TPoint;
  r    : TRect;
begin
  pt:= TPoint(msg.lParam);
  GetWindowRect(HWindow, r);
  r.left:= r.right - spinWidth;
  if (spinWidth > 0) and PtInRect(r, pt) then
    msg.result:= HTBORDER
  else
    DefWndProc(msg);
end;

procedure TSpinEdit.WMTimer(var msg: TMessage);
begin
  {- kill initial delay }
  if not spinning then
    KillTimer(HWindow, 0);

  if buttonPress = 1 then
    SpinUp
  else if buttonPress = -1 then
    SpinDown;

  {- set the repeat timer delay }
  if not spinning then
  begin
    SetTimer(HWindow, 0, sDelay, nil);
    spinning:= true;
  end;
end;

procedure TSpinEdit.StartTimer;
begin
  spinning:= false;
  SetTimer(HWindow, 0, initDelay, nil);
end;

procedure TSpinEdit.EndTimer;
begin
  KillTimer(HWindow, 0);
  if gotCapture then
  begin
    ReleaseCapture;
    gotCapture:= false;
  end;
  buttonPress:= 0;
  DrawIt;
  spinning:= false;
end;

procedure TSpinEdit.WMKillFocus(var msg: TMessage);
begin
  EndTimer;
  inherited WMKillFocus(msg);
end;

procedure TSpinEdit.WMLButtonUp(var msg: TMessage);
begin
  EndTimer;
  DefWndProc(msg);
end;

procedure TSpinEdit.WMNCLButtonDblClk(var msg: TMessage);
begin
  if spinWidth > 0 then
    WMNCLButtonDown(msg);
  DefWndProc(msg);
end;

procedure TSpinEdit.WMNCLButtonDown(var msg: TMessage);
var
  pt   : TPoint;
  r    : TRect;
begin
  if spinWidth > 0 then
  begin
    {- if control does not have focus, then focus and bail. If focus has already
       been attained, then proceed with spin logic }
    if GetFocus <> HWindow then
    begin
      SendMessage(parent^.HWindow, WM_NEXTDLGCTL, HWindow, 1);
      SetFocus(HWindow);
      exit;
    end;
    pt:= TPoint(msg.lParam);
    GetWindowRect(HWindow, r);
    r.left:= r.right - spinWidth;
    if PtInRect(r, pt) then
    begin
      CalcMidpoint(r);
      ScreenToClient(HWindow, pt);
      if pt.y <= um then
      begin
        buttonPress:= 1;
        DrawIt;
      end
      else if pt.y >= lm then
      begin
        buttonPress:= -1;
        DrawIt;
      end;
      SetCapture(HWindow);
      gotCapture:= true;
      StartTimer;
      if buttonPress = 1 then
        SpinUp
      else if buttonPress = -1 then
        SpinDown;
    end;
  end;
  DefWndProc(msg);
end;

procedure TSpinEdit.CalcMidPoint(var r: TRect);
{- calculate mid point and normalize WindowRect [r] to Client rect }
var
  m  : integer;
  pt : TPoint;
begin
  r.left:= r.right - spinWidth;
  pt.x:= r.left;
  pt.y:= r.top;
  ScreenToClient(HWindow, pt);

  r.left:= pt.x;
  r.top:= pt.y;
  pt.x:= r.right;
  pt.y:= r.bottom;
  ScreenToClient(HWindow, pt);

  r.right:= pt.x;
  r.bottom:= pt.y;
  r.left:= r.right - spinWidth;
  Dec(r.right);
  Dec(r.bottom);

  m:= r.bottom - r.top;
  if odd(m) then
  begin
    um:= (m div 2);
    lm:= um + 1;
  end
  else
  begin
    lm:= m div 2;
    um:= lm;
  end;
end;

procedure TSpinEdit.DrawIt;
{- draw spin button. }
var
  dc    : HDC;
  r     : TRect;
  op    : HPen;
  sz    : TPoint;
  mdc   : HDC;
begin
  if spinWidth <= 0 then exit;  {- spin button hidden }
  dc:= GetDC(HWindow);
  GetWindowRect(HWindow, r);
{  Dec(r.right);
  Dec(r.bottom);}

  {- normalize rect to client relative units }
  CalcMidPoint(r);

  FillRect(dc, r, GetStockObject(LTGRAY_BRUSH));

  MoveTo(dc, r.left, r.top);
  LineTo(dc, r.right, r.top);
  if not Ctl3dIsEnabled then
  begin
    LineTo(dc, r.right, r.bottom);
    LineTo(dc, r.left - 1, r.bottom);
  end;

  {- draw mid point divider }
  MoveTo(dc, r.left, um);
  LineTo(dc, r.right, um);
  MoveTo(dc, r.left, lm);
  LineTo(dc, r.right, lm);

  {- draw 3d effects (White portion) }
  op:= SelectObject(dc, GetStockObject(WHITE_PEN));
  if (buttonPress <> 1) or (buttonPress = 0) then
  begin {- upper }
    MoveTo(dc, r.left, um-1);
    LineTo(dc, r.left, r.top+1);
    LineTo(dc, r.right-1, r.top+1);
  end;
  if (buttonPress <> -1) or (buttonPress = 0) then
  begin {- lower }
    MoveTo(dc, r.left, r.bottom-1);
    LineTo(dc, r.left, lm+1);
    LineTo(dc, r.right-1, lm+1);
  end;
  SelectObject(dc, op);

  {- draw 3D effects (Gray portion) }
  op:= SelectObject(dc, CreatePen(PS_SOLID, 0, RGB(128,128,128)));
  {- upper button }
  if (buttonPress <> 1) or (buttonPress = 0) then
  begin
    MoveTo(dc, r.right-1, r.top+1);
    LineTo(dc, r.right-1, um-1);
    LineTo(dc, r.left, um-1);
  end;
  if (buttonPress <> -1) or (buttonPress = 0) then
  begin  {- lower button }
    MoveTo(dc, r.right-1, lm+1);
    LineTo(dc, r.right-1, r.bottom-1);
    LineTo(dc, r.left, r.bottom-1);
  end;
  DeleteObject(SelectObject(dc, op));

  sz.x:= 7;
  sz.y:= 4;

  mdc:= CreateCompatibleDC(dc);
  SelectObject(mdc, hbUp);


  StretchBlt(dc, r.left+2, r.top+4, r.right-r.left - 4, um-r.top-7, mdc, 0,0, sz.x,sz.y, SRCCOPY);

  SelectObject(mdc, hbDown);
  StretchBlt(dc, r.left+2, lm+4, r.right - r.left - 4, r.bottom - lm - 7, mdc, 0,0, sz.x,sz.y, SRCCOPY);

  DeleteDC(mDC);
  ReleaseDC(HWindow, dc);
end;

procedure TSpinEdit.WMKeyDown(var msg: TMessage);
begin
  if spinning then
    EndTimer;
  if (GetKeyState(VK_SHIFT) and $8000 = 0) and
     (GetKeyState(VK_CONTROL) and $8000 = 0) then
  begin
    if msg.wParam = VK_UP then
    begin
      SpinUp;
      msg.result:= 0;
    end
    else if msg.wParam = VK_DOWN then
    begin
      SpinDown;
      msg.result:= 0;
    end
    else
      inherited WMKeyDown(msg);
  end
  else
    inherited WMKeyDown(msg);
end;

procedure TSpinEdit.WMEraseBkGnd(var msg: TMessage);
var
  r  : TRect;
  dc : HDC;
begin
  DefWndProc(msg);
  if spinWidth > 0 then
  begin
    buttonPress:= 0;
    DrawIt;
  end;
end;

function TSpinEdit.FldIsBlank: boolean;
{- return true if field is blank }
var
  pstr : PChar;
  len  : integer;
begin
  len:= SendMessage(HWindow, EM_LINELENGTH, 0, 0);
  FldIsBlank:= len = 0;
end;

{------------------------------------[ TSpinReal ]--}

constructor TSpinReal.Init(aParent: PWindowsObject; anId: integer; aTitle: PChar;
                     x,y,w,h, aTextLen: integer; multiline: boolean; aSpinWidth: integer;
                     aMin, aMax, anInc: double);
begin
  inherited Init(aParent, anID, aTitle, x,y,w,h, aTextLen, multiline, aSpinWidth);
  max:= aMax;
  min:= aMin;
  incr:= anInc;
  fact:= 0;
end;

constructor TSpinReal.InitResource(AParent: PWindowsObject; resourceID, aTextLen, aSpinWidth: integer;
                                   aMin, aMax, anInc: double);
var
  temp  : double;
begin
  inherited InitResource(AParent, resourceID, aTextLen, aSpinWidth);
  max:= aMax;
  min:= aMin;
  incr:= anInc;
  temp:= Frac(anInc);

end;

procedure TSpinReal.IncDec(dy: double);
var
  k         : double;
  code      : integer;
  ch        : char;
begin
  k:= GetDouble + dy;
  if min <> max then
  begin
    if k < min then
      k:= min
    else if k > max then
      k:= max;
  end;
  SetNum(k);
end;

procedure TSpinReal.SpinUp;
begin
  IncDec(incr);
end;

function TSpinReal.NumIsValid: boolean;
{- return true if the text in the control is a valid number. A blank is considered
   valid }
var
  p1    : array[0..100] of char;
  err   : boolean;
begin
  GetText(p1, 100);
  IntlStrToReal(p1, err);
  NumIsValid:= not err;
end;

function TSpinReal.NumInRange: boolean;
var
  p1    : array[0..100] of char;
  err   : boolean;
  num   : double;
begin
  if (min <> max) then
  begin
    GetText(p1, 100);
    num:= IntlStrToReal(p1, err);
    if not err then
      NumInRange:= (num >= min) and (num <= max)
    else
      NumInRange:= false;
  end
  else
    NumInRange:= true;
end;

procedure TSpinReal.SetNum(num: double);
var
  pstr : array[0..100] of char;
begin
  IntlRealToStr(num, pstr, 100);
  SetText(pstr);
  SetSelection(0, strlen(pstr));
end;

function TSpinReal.GetLong: longint;
begin
  GetLong:= Trunc(GetDouble);
end;

function TSpinReal.GetDouble: double;
var
  p1  : array[0..100] of char;
  k   : double;
  err : boolean;
begin
  GetText(p1, 100);
  k:= IntlStrToReal(p1, err);
  if err then
    k:= 0;
  GetDouble:= k;
end;

procedure TSpinReal.SpinDown;
begin
  IncDec(-1*incr);
end;

function TSpinReal.ValidChar(ch: char): boolean;
begin
  ValidChar:= ((ch >= '0') and (ch <= '9')) or
              (ch = '-') or
              (ch = char(VK_BACK)) or
              (ch = IntlThousandSep) or
              (ch = IntlDecimalChar);
end;

procedure TSpinReal.WMChar(var msg: TMessage);
begin
  if not ValidChar(char(msg.wParam)) then
    msg.wParam:= 0
  else
    inherited WMChar(msg);
end;

procedure TSpinReal.WMKillFocus(var msg: TMessage);
begin
  inherited WMKillFocus(msg);
  if NumIsValid then
    SetNum(GetDouble);  {- format number }
end;

{---------------------[ TSpinInt ]--}

constructor TSpinInt.Init(aParent: PWindowsObject; anId: integer; aTitle: PChar;
                     x,y,w,h, aTextLen: integer; multiline: boolean; aSpinWidth: integer;
                     aMin, aMax, anInc: longint);
begin
  inherited Init(aParent, anID, aTitle, x,y,w,h, aTextLen, multiline, aSpinWidth,
                 aMin, aMax, anInc);
end;

constructor TSpinInt.InitResource(AParent: PWindowsObject; resourceID, aTextLen, aSpinWidth: integer;
                                  aMin, aMax, anInc: longint);
begin
  inherited InitResource(AParent, resourceID, aTextLen, aSpinWidth, aMin, aMax, anInc);
end;

(*function TSpinInt.GetLong: double;*)
(*var*)
(*  p1    : array[0..100] of char;*)
(*  err   : boolean;*)
(*  k     : longint;*)
(*begin*)
(*  GetText(p1, 100);*)
(*  k:= IntlStrToInt(p1, err);*)
(*  if err then*)
(*    k:= 0;*)
(*  GetLong:= k;*)
(*end;*)

(*function TSpinInt.GetDouble: double;*)
(*var*)
(*  p1    : array[0..100] of char;*)
(*  err   : boolean;*)
(*  k     : longint;*)
(*begin*)
(*  GetText(p1, 100);*)
(*  k:= IntlStrToReal(p1, err);*)
(*  if err then*)
(*    k:= 0;*)
(*  GetDouble:= k;*)
(*end;*)

procedure TSpinInt.SetNum(num: double);
var
  pstr : array[0..100] of char;
  iNum : longint;
begin
  if (num < (-MaxLongint-1)) then
    num:= -MaxLongint-1
  else if num > MaxLongInt then
    num:= MaxLongint;
  iNum:= Trunc(num);
  IntlIntToStr(inum, pstr, 100);
  SetText(pstr);
  SetSelection(0, strlen(pstr));
end;

function TSpinInt.ValidChar(ch: char): boolean;
begin
  ValidChar:= ((ch >= '0') and (ch <= '9')) or
              (ch = '-') or
              (ch = char(VK_BACK)) or
              (ch = IntlThousandSep);
end;

var
  saveExit  : Pointer;

procedure SpinExit; far;
begin
  exitProc:= saveExit;
  DeleteObject(hbUp);
  DeleteObject(hbDown);
end;


BEGIN
  saveExit:= exitProc;
  exitProc:= @SpinExit;
  hbUp:= LoadBitmap(HInstance, MakeIntResource(BMP_UP));
  hbDown:= LoadBitmap(HInstance, MakeIntResource(BMP_DOWN));
END.

{- rcf }
