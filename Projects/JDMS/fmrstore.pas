{----------------------------------------------------------------------------}
{  Module Name  : FMRSTORE.PAS                                               }
{  Programmer   : DWC                                                        }
{  Date Created : 05/11/95                                                   }
{                                                                            }
{  Purpose -                                                                 }
{  This module allows the user to retore data.                               }
{                                                                            }
{  Assumptions -                                                             }
{  None.                                                                     }
{                                                                            }
{  Initialization -                                                          }
{  None.                                                                     }
{                                                                            }
{  Revision History -                                                        }
{                                                                            }
{  Version  Date      Who    Did What                                        }
{  -------  --------  ------ ----------------------------------------------- }
{  1.00     05/11/95  DWC     Initial release.                               }
{                                                                            }
{----------------------------------------------------------------------------}

unit FMRSTORE;

INTERFACE

uses
  OWindows;

procedure FMRestore(aParent: PWindowsObject);
procedure FMReportRestore(aParent: PWindowsObject);

IMPLEMENTATION

uses
  Mscan,
  Objects,
  fmconst,
  fmtypes,
  fmglobal,
  fmerror,
  ApiTools,
  DBTypes,
  DBLib,
  DBFile,
  WinDOS,
  WinProcs,
  WinTypes,
  WinCrt,
  userMsgs,
  DMString,
  INILib,
  Strings;

type
  PDlgFRestore = ^TDlgFRestore;
  TDlgFRestore = object(TDlgFMStatus)
    {}
    procedure ProcessDlg(var msg: TMessage); virtual WM_FIRST + WM_FMPROCESS;
  end;

  PDlgRRestore = ^TDlgRRestore;
  TDlgRRestore = object(TDlgFMStatus)
    {}
    procedure ProcessDlg(var msg: TMessage); virtual WM_FIRST + WM_FMPROCESS;
  end;

{----------------------[RestoreDataBaseFiles]-------------------------------}
{---------------------------------------------------------------------------}
{                                                                           }
{---------------------------------------------------------------------------}
function RestoreDataBaseFiles(dlg      : PDlgFMStatus;
                              dbDir,
                              workDir  : PChar;
                              drive    : byte;
                              var SetID: Integer): integer;
var
  diskId          : integer;
  retVal          : integer;
  setRec          : TBackupRec;
  lastDisk        : boolean;
  lastFileName    : array[0..max8dot3] of char;
  backupId        : integer;
  backupIncr      : integer;
  backupType      : char;
  numDisks        : integer;
  aTitle          : array[0..255] of char;
  msgStr          : array[0..255] of char;
  msgStr1         : array[0..50] of char;
  msgStr2         : array[0..50] of char;
  tempStr         : array[0..10] of char;
  srstr           : array[0..25] of char;
  pStr            : array[0..50] of char;
  freeBytes       : longint;

begin
  retVal := continueJob;

  dlg^.SetText(SR(IDS_PERDISK,  pStr, 50),IDC_PB1_TEXT);
  dlg^.SetText(SR(IDS_PERTOTAL, pStr, 50),IDC_PB2_TEXT);
  dlg^.ResetPct(1);
  dlg^.ResetPct(2);
  totalCt := 0;
  totalSpace := 0;
  diskSizeCt := 0;
  SetID := -1;
  diskSpace := floppyBytes;


  SR(IDS_FMRESTORE, aTitle, 255);
  SR(IDS_FULLBACKUPSET, msgStr2, 255);
  retVal := getSetInfo(dlg, setRec, msgStr2, aTitle, FullCatalog , 0);

  if (retVal = continueJob) and dlg^.CanContinue then
  begin
    backupType := setRec.BType;
    backupId   := setRec.BIdent;
    backupIncr := setRec.numIncr;
    numDisks   := setRec.numDisks;
    totalSpace := setRec.numBytes + setRec.numIncrBytes;
    SetID      := backupId;
    strCopy(lastFileName,'');

    {Check the drive to make sure there is enough free disk space to perform }
    {this operation.                                                         }
    freeBytes := DiskFree(drive);
    if totalSpace < freeBytes then
    begin
      StrCopy(msgStr, SR(IDS_FMDISK1OF, srstr, 25));
      StrCat(msgStr,' ');
      str(numDisks,tempStr);
      StrCat(msgStr,tempStr);
      dlg^.SetText(msgStr, IDC_TEXT2);
      {- Restore Full Backup -}
      lastDisk := false;
      diskId := 1;
      while  (not lastDisk) and (retVal = continueJob) and dlg^.CanContinue do
      begin
        diskSizeCt := 0;
        dlg^.ResetPct(1);
        retVal := restoreFiles(dlg, dbDir, lastFileName);
        if (retVal = continueJob) and dlg^.CanContinue then
        begin
          inc(diskId);
          if diskId <= numDisks then
            retVal := InsertRestoreDisk(dlg,diskId,numDisks,backupType,backupId,msgStr2, aTitle)
          else
            lastDisk := true;
        end;
      end;

      if (backupIncr = 1) and (retVal = continueJob) and dlg^.CanContinue then
      begin
        {- Restore Incremetal Backup -}
        SetID:=-1;
        CleanDir(workDir,'*.*');
        SR(IDS_INCRBACKUPSET, msgStr, 255);
        retVal := getSetInfo(dlg, setRec, msgStr, aTitle, IncrCatalog, backupId);
        if (retVal = continueJob) and dlg^.CanContinue then
        begin
          dlg^.SetText(SR(IDS_RESTOREINCR, msgStr, 255),IDC_TEXT1);
          numDisks := setRec.numDisks;
          StrCopy(msgStr1,SR(IDS_FMDISK1OF, srstr, 25));
          StrCat(msgStr1,' ');
          str(numDisks,tempStr);
          StrCat(msgStr1,tempStr);
          dlg^.SetText(msgStr1, IDC_TEXT2);
          strCopy(lastFileName,'');
          diskId := 1;
          lastDisk := false;
          while  (not lastDisk) and (retVal = continueJob) and dlg^.CanContinue do
          begin
            diskSizeCt := 0;
            restoreFiles(dlg, workDir, lastFileName);
            inc(diskId);
            if diskId <= numDisks then
              InsertRestoreDisk(dlg, diskId, numDisks, 'I', backupId, msgStr, aTitle)
            else
              lastDisk := true;
          end;
          restoreData(dlg, dbDir, workDir);
        end;
      end;
    end
    else
    begin
      {-  insufficient disk space to perform this operation. -}
      SR(IDS_INFORMATION, aTitle, 255);
      SR(IDS_INSUFFDISKSPACE, msgStr1, 255);
      InfoMsg(dlg^.HWindow, aTitle, msgStr1);
      retVal := cancelJob;
    end;
  end;

  RestoreDataBaseFiles := retVal;
end;

{----------------------[RestoreReportDBFiles]-------------------------------}
{---------------------------------------------------------------------------}
{                                                                           }
{---------------------------------------------------------------------------}
function RestoreReportDBFiles(dlg:PDlgFMStatus; dbDir :PChar): integer;
var
  diskId          : integer;
  retVal          : integer;
  setRec          : TBackupRec;
  lastDisk        : boolean;
  lastFileName    : array[0..max8dot3] of char;
  backupId        : integer;
  backupIncr      : integer;
  backupType      : char;
  numDisks        : integer;
  aTitle          : array[0..255] of char;
  msgStr          : array[0..255] of char;
  msgStr2         : array[0..50] of char;
  tempStr         : array[0..10] of char;
  srStr           : array[0..25] of char;
  pStr            : array[0..50] of char;

begin
  retVal := continueJob;

  dlg^.SetText(SR(IDS_PERDISK,  pStr, 50),IDC_PB1_TEXT);
  dlg^.SetText(SR(IDS_PERTOTAL, pStr, 50),IDC_PB2_TEXT);
  dlg^.ResetPct(1);
  dlg^.ResetPct(2);
  totalCt := 0;
  totalSpace := 0;
  diskSizeCt := 0;
  diskSpace := floppyBytes;

  SR(IDS_FMRESTORE, aTitle, 255);
  SR(IDS_REPORTBACKUPSET, msgStr2, 255);
  retVal := getSetInfo(dlg, setRec, msgStr2, aTitle, ReportCatalog, 0);
  totalSpace := setRec.numBytes;

  if (retVal = continueJob) and dlg^.CanContinue then
  begin
    backupType := setRec.BType;
    backupId   := setRec.BIdent;
    backupIncr := setRec.numIncr;
    numDisks   := setRec.numDisks;
    strCopy(lastFileName,'');

    StrCopy(msgStr,SR(IDS_FMDISK1OF, srStr, 25));
    StrCat(msgStr,' ');
    str(numDisks,tempStr);
    StrCat(msgStr,tempStr);
    dlg^.SetText(msgStr, IDC_TEXT2);
    lastDisk := false;
    diskId := 1;
    while  (not lastDisk) and (retVal = continueJob) and dlg^.CanContinue do
    begin
      diskSizeCt := 0;
      retVal := restoreFiles(dlg, dbDir, lastFileName);
      if (retVal = continueJob) and dlg^.CanContinue then
      begin
        inc(diskId);
        if diskId <= numDisks then
          retVal := InsertRestoreDisk(dlg,diskId,numDisks,backupType,backupId,msgStr2, aTitle)
        else
          lastDisk := true;
      end;
    end;

  end;
  RestoreReportDBFiles := retVal;
end;

{----------------------[DeleteReportFiles]----------------------------------}
{---------------------------------------------------------------------------}
{                                                                           }
{---------------------------------------------------------------------------}
procedure DeleteReportFiles(fNList : PCollection; aDir : PChar);
var
  i         : integer;
  retVal    : integer;
  fileEntry : PArchFile;
  dbFile    : array[0..255] of char;
  aFile     : file;

Begin
  {$I-}
  for i := 0 to fNList^.count-1 do
  begin
    fileEntry := fNList^.At(i);

    StrCopy(dbFile, adir);
    StrCat(dbFile, fileEntry^.fileName);
    StrCat(dbFile, '.dbi');
    Assign(aFile, dbFile);
    Erase(aFile);
    retVal := IOResult;

    StrCopy(dbFile, adir);
    StrCat(dbFile, fileEntry^.fileName);
    StrCat(dbFile, '.dbd');
    Assign(aFile, dbFile);
    Erase(aFile);
    retVal := IOResult;
  end;
  {$I+}
end;

{----------------------[TDlgFRestore.ProcessDlg]----------------------------}
{---------------------------------------------------------------------------}
{                                                                           }
{---------------------------------------------------------------------------}
procedure TDlgFRestore.ProcessDlg(var msg: TMessage);
var
  estDiskNum   : integer;
  diskNum      : integer;
  dbDir        : array[0..maxPathLen] of char;
  workDir      : array[0..maxPathLen] of char;
  saveDir      : array[0..maxPathLen] of char;
  RestoreRec   : TBackupRec;
  aTitle       : array [0..255] of char;
  updateMsg    : array [0..255] of char;
  RestoreId    : array [0..15] of char;
  retVal       : integer;
  saveVal      : integer;
  fileNameList : PCollection;
  drive        : byte;
  freeBytes    : longint;
  neededBytes  : longint;
  verifyRestore: Boolean;
  vSet         : Boolean;
  DiskSetId    : integer;

begin
  verifyRestore := INIVerifyBackup;
  fileNameList := New(PCollection, Init(20, 5));
  retVal := continueJob;
  diskNum := 0;
  StrCopy(fmErr_Msg,'');

  SetText('    ',0);
  SetText('    ',IDC_TEXT1);
  SetText('    ',IDC_TEXT2);
  SetText('    ',IDC_PB1_TEXT);
  SetText('    ',IDC_PB1);
  SetText('    ',IDC_PB2_TEXT);
  SetText('    ',IDC_PB2);

  SR(IDS_RESTOREMAINMSG, updateMsg, 255);
  SetText(updateMsg,0);

  GetCurrentDir(dbDir, drive);
  GetWorkingDir(dbDir, saveDir, restoreDir);
  GetWorkingDir(dbDir, workDir, backupDir);

  {Check the drive to make sure there is enough free disk space to perform }
  {this operation.                                                         }
  freeBytes := DiskFree(drive);
  neededBytes := ComputeTotalBytes(@Self, dbDir, estDiskNum);
  if neededBytes < freeBytes then
  begin
    SetText(SR(IDS_SAVINGFILES, updateMsg, 255),IDC_TEXT2);
    {- Save the Current database to a temporary subdirectory. -}
    CleanDir(workDir,'*.*');
    CleanDir(saveDir,'*.*');
    if (retVal = continueJob) and (CanContinue) then
    begin
      ShowWindow(GetDlgItem(HWindow, IDCANCEL), SW_HIDE);
      EnableWindow(GetDlgItem(HWindow, IDCANCEL), False);
      retVal := BuildArchiveFiles(@Self, fileNameList, dbDir, fileDescExt,backupAllFiles);
      retval := CopyFiles(@Self, fileNameList, dbDir, saveDir, fileDescExt,
                          diskNum, RestoreRec, false, false, false, nil);
      retVal := CopyFiles(@Self, fileNameList, dbDir, saveDir, datFileExt,
                          diskNum, RestoreRec, false, false, false, nil);

      {- Remove the Current database. -}
      SR(IDS_PREPARINGDISK, updateMsg, 255);
      SetText(updateMsg,IDC_TEXT2);
      if (retVal = continueJob) and CanContinue then
      begin
        CleanDir(dbDir,'*.DB*');

        ShowWindow(GetDlgItem(HWindow, IDCANCEL), SW_SHOW);
        EnableWindow(GetDlgItem(HWindow, IDCANCEL), True);
        SR(IDS_RESTOREFULL, updateMsg, 255);
        SetText(updateMsg,IDC_TEXT1);
        if VerifyRestore then
        begin
          GetVerify(vSet);
          SetVerify(True);
        end;
        retVal := RestoreDataBaseFiles(@Self, dbDir, workDir, drive, DiskSetId);

        if VerifyRestore then
          SetVerify(vSet);
        ShowWindow(GetDlgItem(HWindow, IDC_PB1_TEXT), SW_HIDE);
        ShowWindow(GetDlgItem(HWindow, IDC_PB1), SW_HIDE);
        ShowWindow(GetDlgItem(HWindow, IDC_PB2_TEXT), SW_HIDE);
        ShowWindow(GetDlgItem(HWindow, IDC_PB2), SW_HIDE);
        if (retVal = continueJob) and (CanContinue) then
        begin
          {- Delete temporary database -}
          SR(IDS_DELETETEMPORARY, updateMsg, 255);
          SetText(updateMsg,IDC_TEXT1);
          SR(IDS_CleanDir, updateMsg, 255);
          SetText(updateMsg,IDC_TEXT2);
          CleanDir(saveDir,'*.DB*');
          CleanDir(workDir,'*.DB*');

          {This DiskSet is now the most recent full backup}
          if DiskSetId>=0 then
          begin
            str(DiskSetId,RestoreId);
            INISetBackupID(RestoreId);
          end;
        end
        else
        begin
          {- Restore failed must restore online database. -}
          ShowWindow(GetDlgItem(HWindow, IDCANCEL), SW_HIDE);
          SR(IDS_RESTORETEMPORARY, updateMsg, 255);
          SetText(updateMsg,IDC_TEXT1);
          SR(IDS_RESTOREOLD, updateMsg, 255);
          SetText(updateMsg,IDC_TEXT2);
          {- reset variables so that functions will work -}
          saveVal := retVal;
          retVal := continueJob;
          SetContinue(true);
          BuildArchiveFiles(@Self, fileNameList, saveDir,fileDescExt,backupAllFiles);
          CopyFiles(@Self, fileNameList, saveDir,  dbDir, fileDescExt,diskNum, RestoreRec, false, false, false, nil);
          CopyFiles(@Self, fileNameList, saveDir,  dbDir, datFileExt, diskNum, RestoreRec, false, false, false, nil);
          CleanDir(saveDir,'*.DB*');
          CleanDir(workDir,'*.DB*');
          ShowWindow(GetDlgItem(HWindow, IDCANCEL), SW_SHOW);
          retVal := saveVal;
        end;
      end
      else
      begin
        CleanDir(saveDir,'*.DB*');
        CleanDir(workDir,'*.DB*');
        retVal:= cancelJob;
      end;
    end;

    if (retVal <> continueJob) and (retVal <> cancelJob) then
    begin
      SR(IDS_RESTORETITLE, updateMsg, 255);
      FMShowError(@Self, updateMsg, retVal, fmErr_Msg);
    end;
  end
  else
  begin
    {-  insufficient disk space to perform this operation. -}
    SR(IDS_INFORMATION, aTitle, 255);
    SR(IDS_INSUFFDISKSPACE, updateMsg, 255);
    InfoMsg(self.HWindow, aTitle, updateMsg);
  end;

  MSDisposeObj(fileNameList);
  EndDlg(IDCancel);
end;


{----------------------[FMRestore]------------------------------------------}
{---------------------------------------------------------------------------}
{                                                                           }
{---------------------------------------------------------------------------}
procedure FMRestore(aParent: PWindowsObject);
var
  dlgRestore   : PDlgFRestore;

begin
  dlgRestore:= New(PDlgFRestore, Init(aParent, true));
  application^.ExecDialog(dlgRestore);;
end;

{----------------------[TDlgRRestore.ProcessDlg]----------------------------}
{---------------------------------------------------------------------------}
{                                                                           }
{---------------------------------------------------------------------------}
procedure TDlgRRestore.ProcessDlg(var msg: TMessage);
var
  diskNum      : integer;
  dbDir        : array[0..maxPathLen] of char;
  workDir      : array[0..maxPathLen] of char;
  saveDir      : array[0..maxPathLen] of char;
  RestoreRec   : TBackupRec;
  updateMsg    : array [0..255] of char;
  retVal       : integer;
  saveVal      : integer;
  fileNameList : PCollection;
  drive        : byte;
  verifyRestore: Boolean;
  vSet         : Boolean;

begin
  verifyRestore := INIVerifyBackup;
  fileNameList := New(PCollection, Init(20, 5));
  retVal := continueJob;
  diskNum := 0;
  StrCopy(fmErr_Msg,'');

  SetText('    ',0);
  SetText('    ',IDC_TEXT1);
  SetText('    ',IDC_TEXT2);
  SetText('    ',IDC_PB1_TEXT);
  SetText('    ',IDC_PB1);
  SetText('    ',IDC_PB2_TEXT);
  SetText('    ',IDC_PB2);

  SR(IDS_RESTOREMAINMSG, updateMsg, 255);
  SetText(updateMsg,0);

  GetCurrentDir(dbDir, drive);
  Update;
  GetWorkingDir(dbDir, saveDir, restoreDir);
  GetWorkingDir(dbDir, workDir, backupDir);
  Update;
  SR(IDS_SAVINGREPORTFILES, updateMsg, 255);
  SetText(updateMsg,IDC_TEXT1);
  {- Save the Current database to a temporary subdirectory. -}
  CleanDir(workDir,'*.*');
  CleanDir(saveDir,'*.*');
  if (retVal = continueJob) and (CanContinue) then
  begin
    ShowWindow(GetDlgItem(HWindow, IDCANCEL), SW_HIDE);
    retVal := BuildArchiveFiles(@Self, fileNameList, dbDir, fileDescExt, backupReportFiles);
    retval := CopyFiles(@Self, fileNameList, dbDir, saveDir, fileDescExt,
                        diskNum, RestoreRec, false, false, false, nil);
    retVal := CopyFiles(@Self, fileNameList, dbDir, saveDir, datFileExt,
                        diskNum, RestoreRec, false, false, false, nil);

    {- Remove the Current database. -}
    SR(IDS_PREPARINGDISK, updateMsg, 255);
    SetText(updateMsg,IDC_TEXT1);
    if (retVal = continueJob) and (CanContinue) then
    begin
      { from active database area }
      DeleteReportFiles(fileNameList, dbDir);
      ShowWindow(GetDlgItem(HWindow, IDCANCEL), SW_SHOW);

      SR(IDS_RESTOREREPORT, updateMsg, 255);
      SetText(updateMsg,IDC_TEXT1);
      ResetPct(1);
      if VerifyRestore then
      begin
        GetVerify(vSet);
        SetVerify(True);
      end;
      retVal := RestoreReportDBFiles(@Self, dbDir);
      if VerifyRestore then
        SetVerify(vSet);
      ShowWindow(GetDlgItem(HWindow, IDC_PB1_TEXT), SW_HIDE);
      ShowWindow(GetDlgItem(HWindow, IDC_PB1), SW_HIDE);
      ShowWindow(GetDlgItem(HWindow, IDC_PB2_TEXT), SW_HIDE);
      ShowWindow(GetDlgItem(HWindow, IDC_PB2), SW_HIDE);
      if (retVal = continueJob) and (CanContinue) then
      begin
        {- Delete temporary database -}
        SR(IDS_DELETETEMPORARY, updateMsg, 255);
        SetText(updateMsg,IDC_TEXT1);
        SR(IDS_CleanDir, updateMsg, 255);
        SetText(updateMsg,IDC_TEXT2);
        CleanDir(saveDir,'*.DB*');
      end
      else
      begin
        {- Restore failed must restore online database. -}
        ShowWindow(GetDlgItem(HWindow, IDCANCEL), SW_HIDE);
        SR(IDS_RESTORETEMPORARY, updateMsg, 255);
        SetText(updateMsg,IDC_TEXT1);
        SR(IDS_RESTOREOLD, updateMsg, 255);
        SetText(updateMsg,IDC_TEXT2);
        {- reset variables so that functions will work -}
        saveVal := retVal;
        retVal := continueJob;
        SetContinue(true);
        BuildArchiveFiles(@Self, fileNameList, saveDir,fileDescExt, backupReportFiles);
        CopyFiles(@Self, fileNameList, saveDir,  dbDir, fileDescExt,diskNum, RestoreRec, false, false, false, nil);
        CopyFiles(@Self, fileNameList, saveDir,  dbDir, datFileExt, diskNum, RestoreRec, false, false, false, nil);
        ShowWindow(GetDlgItem(HWindow, IDCANCEL), SW_SHOW);
        retVal := saveVal;
        CleanDir(saveDir,'*.DB*');
      end;
    end;
  end;

  if (retVal <> continueJob) and (retVal <> cancelJob) then
  begin
    SR(IDS_RESTORETITLE, updateMsg, 255);
    FMShowError(@Self, updateMsg, retVal, fmErr_Msg);
  end;

  MSDisposeObj(fileNameList);
  EndDlg(IDCancel);
end;

{----------------------[FMReportRestore]------------------------------------}
{---------------------------------------------------------------------------}
{                                                                           }
{---------------------------------------------------------------------------}
procedure FMReportRestore(aParent: PWindowsObject);
var
  dlgRestore   : PDlgRRestore;

begin
  dlgRestore:= New(PDlgRRestore, Init(aParent, true));
  application^.ExecDialog(dlgRestore);
end;

End.
