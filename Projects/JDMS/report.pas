{----------------------------------------------------------------------------}
{  Module Name  : Report.PAS                                                 }
{  Programmer   : EJ                                                         }
{  Date Created : 05/24/95                                                   }
{                                                                            }
{  Purpose -                                                                 }
{  This module provides the common objects and functions to preview          }
{  and print reports.                                                        }
{                                                                            }
{  Assumptions -                                                             }
{  None.                                                                     }
{                                                                            }
{  Revision History -                                                        }
{                                                                            }
{  Version  Date      Who    Did What                                        }
{  -------  --------  ------ ----------------------------------------------- }
{  1.00     05/24/95  EJ     Initial release.                                }
{                                                                            }
{----------------------------------------------------------------------------}

(*!!!
  NOTE: The report object in this module is currently only used by the W/A
         and EPI. In the future, this object should be scrapped and the
         prnprev lib should be used directly. No new descendents should be
         created from TReport.

         rcf  02/13/96 3:25pm
*)

unit Report;

INTERFACE

uses
  OWindows,
  Objects,
  WinTypes,
  DlgLib,
  Status,
  PrnPrev;

{$I REPORT.INC}

type
  PCancelDlg = ^TCancelDlg;
  TCancelDlg = object(TGenStatDlg)
    constructor Init(aParent: PWindowsObject; aName: PChar);
    destructor Done; virtual;
    procedure SetupWindow; virtual;
    procedure Cancel(var msg: TMessage); virtual id_First + IDCANCEL;
  end;

  PReport = ^TReport;
  TReport = object(TObject)
    parent        : PWindowsObject;
    msgParent     : PWindowsObject;
    userAbort     : boolean;
    abortPrinting : boolean;
    printStrLen   : integer;
    printStr      : PChar;
    prnInfo       : PPrintInfo;
    cancelDlg     : PCancelDlg;
    {}
    constructor Init(aParent: PWindowsObject; aRptTitle: PChar;
                     maxCol, aMaxLines: word);
    destructor Done; virtual;
    procedure InitHeadings; virtual;
    procedure AllowCancel;
    procedure List(aStr: PChar);
    procedure ListLn(aStr: PChar);
    procedure NewPage;
    procedure PrintTheLine;
    procedure PrintCenter;
    procedure FormFeed;
    procedure CRLineFeed;
    procedure ConcatBlanks(howMany: integer; newStr: PChar);
    procedure ConcatDashes(howMany: integer; newStr: PChar);
    procedure ConcatColon(maxSize: integer; newStr: PChar);
    {}
    private
    RptTitle      : array [0..50] of char;
    maxLines      : word;
    currentLine   : word;
    bNewPage      : boolean;
    bCenter       : boolean;
  end;


IMPLEMENTATION

uses
  WinProcs,
  Strings,
  INILib,
  StrsW,
  APITools,
  MScan,
  DMSErr,
  DMSDebug;

{$R REPORT.RES}


{---------------------------------------------[ TCancelDlg ]--}
constructor TCancelDlg.Init(aParent: PWindowsObject; aName: PChar);
begin
  inherited Init(aParent, aName);
end;

destructor TCancelDlg.Done;
begin
  EnableWindow(parent^.HWindow, TRUE);
  SetFocus(parent^.HWindow);
  inherited Done;
end;

procedure TCancelDlg.SetupWindow;
begin
  inherited SetupWindow;
  EnableWindow(parent^.HWindow, FALSE);
end;

procedure TCancelDlg.Cancel(var msg: TMessage);
var
  tempMsg : array [0..40] of char;
begin
  SR(IDS_STOP_RPT, tempMsg, 40);
  if YesNoMsg(HWindow, nil, tempMsg) then
  begin
    inherited Cancel(msg);
    EnableWindow(GetItemHandle(IDCANCEL), FALSE);
  end;
end;

{---------------------------------------------[ TReport ]--}
constructor TReport.Init(aParent: PWindowsObject; aRptTitle: PChar;
                         maxCol, aMaxLines: word);
var
  lf   : TLogFont;
begin
  userAbort := FALSE;
  abortPrinting := FALSE;
  parent := aParent;
  msgParent := aParent;
  cancelDlg := nil;

  if aRptTitle <> nil then
    StrLCopy(RptTitle, aRptTitle, 50)
  else
    RptTitle[0] := #0;

  printStrLen := maxCol;
  maxLines := aMaxLines;
  currentLine := 0;
  bNewPage := FALSE;
  bCenter := FALSE;

  GetMem(printStr, printStrLen+1);
  if printStr = nil then
  begin
    ShowError(aParent, IDS_REPORT_ERR, nil, MOD_REPORT+3, MOD_REPORT, 0);
    abortPrinting := TRUE;
    Done;
    Fail;
  end;
  printStr^ := #0;

  MakeScreenFont(lf, FALSE, TRUE);

  {- the following line uses the WAEPI point size INI entry, because only the
     w/a and epi reports are using this object. See note at top }
  prnInfo := New(PPrintInfo, Init(lf, INIWAEPIPointSize));
  if prnInfo = nil then
  begin
    ShowError(aParent, IDS_REPORT_ERR, nil, MOD_REPORT+1, MOD_REPORT, 0);
    abortPrinting := TRUE;
    Done;
    Fail;
  end;
end;

destructor TReport.Done;
var
  ppDlg        : PPrnPrevDlg;
begin
  if cancelDlg <> nil then
  begin
    msgParent := parent;
    MSDisposeObj(cancelDlg);
  end;
  if not abortPrinting then
  begin
    if userAbort then
    begin
      SR(IDS_USER_ABORT, printStr, 40);
      prnInfo^.body^.AddRow(r_Normal, 0);
      prnInfo^.body^.AddRow(r_Normal, 0);
      prnInfo^.body^.AddCell(printStr, LT600, c_Normal, 0);
    end;
    ppDlg := New(PPrnPrevDlg, Init(parent, prnInfo, RptTitle));
    Application^.ExecDialog(ppDlg);
  end;
  MSDisposeObj(prnInfo);
  MSFreeMem(printStr, printStrLen+1);
  inherited Done;
end;

procedure TReport.InitHeadings;
var
  pageStr : array [0..40] of char;
begin
  prnInfo^.header^.AddRow(r_Normal, 0);
  prnInfo^.header^.AddCell('^d  ^t', LT200, c_Date or c_Time, 0);
  prnInfo^.header^.AddCell(RptTitle, LT400, c_PageCenter or c_Center or c_Bold, 0);
  SR(IDS_PAGE, pageStr, 40);
  prnInfo^.header^.AddCell(pageStr, LT150, c_PageRight or c_Right or c_PageNum or c_NumPages, 0);
  prnInfo^.header^.AddRow(r_Normal, 0);
end;

procedure TReport.AllowCancel;
begin
  cancelDlg := New(PCancelDlg, Init(parent, MakeIntResource(DLG_CANCEL)));
  if cancelDlg <> nil then
  begin
    Application^.MakeWindow(cancelDlg);
    cancelDlg^.SetText(RptTitle, IDC_LBL);
    msgParent := cancelDlg;
  end
  else
    ShowError(msgParent, IDS_REPORT_ERR, nil, MOD_REPORT+2, MOD_REPORT, 0);
end;

procedure TReport.List(aStr: PChar);
begin
  if userAbort or abortPrinting then
    Exit;

  StrLCat(printStr, aStr, printStrLen);

  if cancelDlg <> nil then
  begin
    cancelDlg^.Update;
    if not cancelDlg^.CanContinue then
      userAbort := TRUE;
  end;
end;

procedure TReport.ListLn(aStr: PChar);
var
  rflags : longint;
  cflags : longint;
begin
  if userAbort or abortPrinting then
  begin
    printStr^ := #0;
    Exit;
  end;

  List(aStr);
  rflags := r_Normal;
  cflags := c_Stretch;

  Inc(currentLine);
  if ((maxLines <> 0) and (currentLine > maxLines)) or bNewPage then
  begin
    currentLine := 0;
    bNewPage := FALSE;
    rflags := rflags or r_PageBreak;
  end;

  if bCenter then
    cflags := cflags or c_Center;

  prnInfo^.body^.AddRow(rflags, 0);
  prnInfo^.body^.AddCell(printStr, LT600, cflags, 0);
  printStr^ := #0;
end;

procedure TReport.NewPage;
begin
{$IFDEF DMSDEBUG}
  if printStr ^ <> #0 then
    InfoMsg(msgParent^.hWindow, 'DEBUG', 'NewPage called when data in printStr');
{$ENDIF}
  bNewPage := TRUE;
end;

procedure TReport.PrintTheLine;
{***********************************************************************}
{* Send the printStr to the printer and CR/LF.                         *}
{***********************************************************************}
begin
  ListLn('');
end;

procedure TReport.PrintCenter;
{***********************************************************************}
{* Prints the string centered on the line, the CR/LF.                  *}
{***********************************************************************}
var
  tbool : boolean;
begin
  tbool := bCenter;
  bCenter := TRUE;
  ListLn('');
  bCenter := tbool;
end;

procedure TReport.FormFeed;
{************************************************************************}
{*    Tells the printer to formfeed.                                    *}
{************************************************************************}
var pageStr : string;
begin
{$IFDEF DMSDEBUG}
  if printStr ^ <> #0 then
    InfoMsg(msgParent^.hWindow, 'DEBUG', 'FormFeed called when data in printStr');
{$ENDIF}
  ListLn('');
  if currentLine <> 0 then
    NewPage;
end;

procedure TReport.CRLineFeed;
{************************************************************************}
{*  Send a CR and LF to the printer.   Used for printing blank lines.   *}
{************************************************************************}
begin
{$IFDEF DMSDEBUG}
  if printStr ^ <> #0 then
    InfoMsg(msgParent^.hWindow, 'DEBUG', 'CRLineFeed called when data in printStr');
{$ENDIF}
  ListLn('');
end;

procedure TReport.ConcatBlanks(howMany: integer; newStr: PChar);
{************************************************************************}
{*  Concats the specified number of blanks onto the end of the newStr   *}
{************************************************************************}
begin
  if howMany <= 0 then
    Exit;

  Inc(howMany, StrLen(newStr));
  if howMany >= printStrLen then
    howMany := printStrLen;
  Pad(newStr, newStr, ' ', howMany);
end;

procedure TReport.ConcatDashes(howMany: integer; newStr: PChar);
{************************************************************************}
{*  Concats the specified number of blanks onto the end of the printStr *}
{************************************************************************}
begin
  if howMany <= 0 then
    Exit;

  Inc(howMany, StrLen(newStr));
  if howMany >= printStrLen then
    howMany := printStrLen;
  Pad(newStr, newStr, '-', howMany);
end;

procedure TReport.ConcatColon(maxSize: integer; newStr: PChar);
{************************************************************************}
{*  Concats a colon to newStr at maxsize-1 position                     *}
{************************************************************************}
begin
  if StrLen(newStr) > maxSize then
    Exit;
{$IFDEF DMSDEBUG}
  if maxSize > printStrLen then
    InfoMsg(msgParent^.hWindow, 'DEBUG', 'ConcatColon called with maxSize > printStrLen');
{$ENDIF}
  FillChar(newStr[StrLen(newStr)], maxSize-StrLen(newStr), ' ');
  newStr[maxSize-2] := ':';
  newStr[maxSize] := #0;
end;


END.
