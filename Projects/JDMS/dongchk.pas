unit DongChk;

INTERFACE

function DongleOk: boolean;

IMPLEMENTATION

uses
  SSTPWF,     { Sentinel linkable driver        }
  WinDos;

function DongleSeed(var seedString: string ): word;
const
  MonthStr: array[1..12] of string[30] = ('January', 'February', 'March',
                                          'April', 'May', 'June',
                                          'July', 'August', 'September',
                                          'October', 'November', 'December');

var
  aMonth,
  aDay,
  aYear,
  aDayOfWeek : word;

begin
  GetDate(aYear, aMonth, aDay, aDayOfWeek);
  seedString := MonthStr[aMonth];
  DongleSeed := aMonth;
end;

function DongleOk: boolean;
const
  MonthVal: array[1..12] of word = (54383, 61073, 65262, 54448, 65529, 65513,
                                    65501, 60338, 19946, 65514, 64309, 16363);

var
  aMonth : word;
  aMonthStr: string;
  result   : word;
  test: string;
  success : boolean;
  count : word;
begin
  aMonth:= DongleSeed(aMonthStr);

  success:= False;
  count:= 1;

	while (not success) and (count <= 3) do
	begin
		case count of
      1 : test:= '55';
      2 : test:= '56';
      3 : test:= '57';
    end;

    result:= SSQuery(test, Length(test));
    result:= SSQuery(aMonthStr, Length(aMonthStr));
    if ((result xor $5a5a) = (MonthVal[aMonth] xor $5a5a)) then
      success:= True;
    Inc(count);
	end;

	if success then
    DongleOK:= True
	else
    DongleOK:= False;

    {$IFDEF IGNOREDONGLE}
        DongleOk:=True;
    {$ENDIF}
end;

END.
