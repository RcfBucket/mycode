{----------------------------------------------------------------------------}
{  Module Name  : TRAYS.PAS                                                  }
{  Programmer   : EJ                                                         }
{  Date Created : 02/08/95                                                   }
{                                                                            }
{  Purpose -                                                                 }
{  This module provides the objects required to access and manipulate        }
{  tray information.                                                         }
{                                                                            }
{  Assumptions -                                                             }
{  None.                                                                     }
{                                                                            }
{  Revision History -                                                        }
{                                                                            }
{  Version  Date      Who    Did What                                        }
{  -------  --------  ------ ----------------------------------------------- }
{  1.00     02/08/94  EJ     Initial conversion from version 2.0.            }
{                                                                            }
{----------------------------------------------------------------------------}
unit Trays;


INTERFACE

uses
  Objects;

const
  MICLength      = 4;
  MaxWell        = 300;
  MaxDilLen      = 16;
  MaxMdaEntries  = 31;  { Maximum number of entries in an Master Dilution array.}
  MaxExtra       = 8;   { Max extra tests on a panel }
  MaxDrugs       = 75;  { Maximum absolute drugs in system  }
  MaxTray        = 75;  { Max number of tray types    }
  MaxSystemDrugs = 99;  { Max absolute drugs in the system }
(*MaxDil         = 31;   *)
  MaxMICLen      = 36;

type
  AbbrStr      = string[3];
  MicStr       = string[MicLength];
  TrayStr      = string[16];
  MastDilarray = array[0..MaxMdaEntries] of string[MICLength];
  DrugDilType  = array[0..MaxDilLen] of array[0..MICLength] of char;
  XtraType     = array[1..MaxExtra] of byte;
  MICs         = array[1..MaxMICLen] of byte;

  PPanelBitmap = ^TPanelBitmap;
  TPanelBitmap = array[0..11] of byte;
  PMICData = ^TMICData;
  TMICData = array[1..MaxMICLen] of byte;

  PTFSpecRec = ^TFSpecRec;
  TFSpecRec = record
    trayNum: integer;
    tName  : array[0..32] of char;   (* could size be set to 16 ?? *)
    tSet,
    pSet,
    idRec  : integer
  end;

  DrugRec = record {1+4=5}
    dilCnt  : byte;
    drugNum : byte;
  end;

  MapPair  = record
    iDrgNum  : byte;
    oDrgNum  : byte;
  end;

  AMapPair = record
    iDrgAbr  : AbbrStr;
    oDrgAbr  : AbbrStr;
  end;

  MapTable   = array [0..20] of MapPair;
  AMapTable  = array [1..20] of AMapPair;
  AbrTable   = array [1..MaxSystemDrugs] of AbbrStr;

(*
  AdnTblType = array [1..MaxDrugs] of byte;
  dorderType = array [1..MaxSystemDrugs] of byte;
  RtCntType  = array [1..MaxDrugs] of byte;
  MdiMapTbl  = array [1..MICLength, 0..31] of byte;

  MicStrType   = string[7];
  MicAryType   = array[1..7] of char;
  NameStr      = string[16];
  MdaTxtType   = array[0..39] of MicStrType;
  TherMda      = array[0..MaxMdaEntries] of string[7]; { DMS 15 }

  IsoDilType = array[1..MaxMICLen] of record
    Drug : string[4];
    Dil  : string[4];
  end;

  dosageType  = record
    case integer of
      0:  ( {1 + 1 + 1 + 1 + 4 + 21 + 1 + 1 + 1 + 132 + 28 = 192}
          { record + (tray.number * 4) + ageGroup + maxTray + 1 }
        routeCnt : byte;
        rtMap    : byte;
        micGroup : byte;
        footNote : char;
        abbr     : AbbrStr;
        name     : string[20];
        altered  : byte;
        futNote2 : char;
        filler1  : byte;
        dosage   : array[1..4] of string[32];
        costCode : array[1..4] of string[6]);
      1:  ( {1 + 1 + 1 + 75 + 75 + 39 = 192}
          { record trayNumber + 3 }
        id       : byte;
        trayNum  : byte;
        drugCnt  : byte;
        adnTbl   : AdnTblType;
        rtCnt    : RtCntType;
        filler   : array[1..39] of byte);
      2:  ( { 1 + 128 + 1 + 14 + 42 + 6 = 192 }
          { record 0 }
        id2      : byte;
        mdiMTbl  : MdiMapTbl;
        adnCnt   : byte;
        filler2  : array[1..20] of byte;
        mapTbl   : MapTable);
      3:  ( { 1 + 60 + 99 + 32 = 192}
          { record 3 }
        id3      : byte;
        tmStamp  : array[1..60] of char;
        drugOrder: dorderType;
        filler3  : array[1..32] of byte);
      4:  ( { 1 + (20 * 7 = 140) + 2 = 192 }
          { record 1 and 2 }
        id4      : byte;
        micText  : array[0..19] of MicAryType;
        filler4  : array[1..51] of byte);
  end;
*)

  TrayRec = record
    case integer of
      0 : ( {1 + 1 + 17 + 1 + 1 + 2 + 1 + 8 + 300 + 150 + 1 + 1 = 484}
          { record (traynum + 2) }
        number  : byte;
        drugCnt : byte;
        name    : TrayStr;
        probNum : byte;
        group   : byte;
        idRec   : integer;
        xtraCnt : byte;
        xtraTbl : XtraType;
        mic     : array[1..MaxWell] of byte;
        drug    : array[1..MaxDrugs] of DrugRec;
        sup     : byte;
        flag    : byte);
      1 : ( {1 + 1 + 17 + (5*32=160) + (2*21=42) + 1 + 1 + 261 = 484}
          { record 0 }
        aNumber : byte;
        trayCnt : byte;
        aName   : TrayStr;
        mda     : MastDilarray;
        mapTbl  : MapTable;
        fstCust : byte;
        custCnt : byte;
        filler  : array[1..261] of byte);
      2 : ( {1 + 1 + 17 + 60 + 1 + 160 + 244 = 484}
      { record 1 }
        bNumber  : byte;
        bDrgCnt  : byte;
        bName    : TrayStr;
        tmStamp  : array[1..60] of char;
        aMapCnt  : byte;
        aMapTbl  : AMapTable;
        filler1  : array[1..244] of byte);
      3 : ( {1 + 1 + 17 + (99*4 = 396) + 69 = 484}
      { record 2 }
        cNumber  : byte;
        cDrgCnt  : byte;
        cName    : TrayStr;
        abrTbl   : AbrTable;
        filler2  : array[1..69] of byte);
  end;

  PMapBat = ^TMapBat;
  TMapBat = object(TObject)
    mapBatFile : Text;
    error      : boolean;
    batIDList  : array[1..MaxTray] of PChar;
    {}
    constructor Init;
    destructor Done; virtual;
  end;

  TDilList = array[1..MaxMICLen] Of Record
    DrugName: array[0..31] of char;
    DilCnt: Integer;
    DrugDils: DrugDilType;
  end;

  PTraysObject = ^TTraysObject;
  TTraysObject = object(TObject)
    gMda        : MastDilarray;        { Read-Only }
    gTrayMax    : integer;             { Read-Only }
    abrTbl      : AbrTable;            { Read-Only }
    lastAbr     : integer;             { Read-Only }
    tfsrec      : TFSpecRec;           { Read-Only }
    dilList     : TDilList;            { Read-Only }
    mapBat      : PmapBat;             { Read-Only }
    {}
    constructor Init;
    destructor Done; virtual;
    procedure LoadTray(trayName:PChar);
    procedure LoadTrayByNum(tNum:integer);
    procedure GetDrugRDN(testID: PChar; var RDN: integer);
    procedure GetMicText(mic: byte; var strg: MicStr);
    function  GetWell(step, rdn: integer): integer;
    function  GetMdi(rdn, step: integer;  nr: boolean): byte;
    procedure InitAbbrTable;
    procedure AdnToAbbrConvert(adn: integer; var abr: string);
    function  AbbrToAdnConvert(abr: PChar): integer;
    function  TrayObsolete: boolean;
    function  MicGT(first, second: byte): boolean;
    function  TrayFlo: boolean;
    procedure TrayAbort(ErrNum: integer);
    function  DrugCnt: integer;
    function  MapTestGroupID(BatID: PChar): integer;
    function  GetXtraWellNumber(var f: Text; testID: PChar;
                                var wellNumber: integer) : boolean;
    procedure GetXtra(var xtraTbl: XtraType;  var XtraCnt: integer);
    function  RdnToAdn(rdn:integer): integer;
    function  MapDrgAbbr(iAbr: PChar; oAbr: PChar): boolean;
    function  DrugMapCount(iAbr: PChar): Integer;
    {}
    private
    gTrayFile   : file of Trayrec;
    gTray       : TrayRec;
    DrgMaps     : TrayRec;
(*  gInitMap    : boolean;
    gInitAdnMap : boolean;
    aMapCnt     : byte;
    aMapTbl     : AMapTable;
    adnMapCnt   : byte;
    adnMapTbl   : MapTable;  *)
    {}
    procedure GetDils(rdn: integer;  var drgAbr: string;
                      var drugDils: DrugDilType;  var dilCnt: integer);
    procedure LoadSTray(trayNum:integer);
(*    function  MapAdn(adn: integer): integer;
    function  AdnMT(var lMapTbl: MapTable): integer;
    procedure MicUnpack(trayNum: integer;  var micLvls: mics;
                        var micList: IsoDilType;  var micCnt: integer);
    procedure GetMda(var table: MastDilarray);
    function  GetDsi(trayNum, rdn, mdi: integer; var nr: boolean): byte;
    function  NRMic (x: byte): boolean;
    function  MicLT(first, second: byte): boolean;
    function  MicG  (x, y: byte): boolean;
    function  MicL  (x, y: byte): boolean;
    function  MicGE (x, y: byte): boolean;
    function  MicLE (x, y: byte): boolean;
    function  MicEQ (x, y: byte): boolean;
    function  MicNE (x, y: byte): boolean;
    function  LMic  (x: byte): boolean;
    function  GMic  (x: byte): boolean;
    function  GetTGrp(trayNum: integer): byte;
    function  GetProbNum(trayNum: integer): byte;
    function  TrayHNID(trayNum: integer): boolean;
    function  TrayBkpt(trayNum: integer): boolean;
    function  GetIdRec(trayNum: integer): integer;
    function  GetTSup(trayNum: integer): byte;
    procedure lpLoadAdnMap; *)
    {}
  end;

IMPLEMENTATION

uses
  Strings,
  StrSW,
  DMString,
  MScan,
  DMSErr,
  DMSDebug,
  WinProcs,
  WinTypes;

const
  err_TrayRangeInLoadTray         = 1;
  err_TrayRangeInLoadSTray        = 2;
  err_FileSeekTRAYREC_DAT         = 3;
  err_FileReadTRAYREC_DAT         = 4;
  err_InvalidTrayNum              = 5;
  err_FileResetTRAYREC_DAT        = 6;
  err_LoadingBatteryIDList        = 7;
  err_BatteryIDNotFound           = 8;


function class(x, y: byte): integer;
var
  relation: integer;
  iX, iY  : byte;
begin
  iX:= x and $1F;
  iY:= y and $1F;
  if iX = iY then
    relation:= 1
  else if iX > iY then
    relation:= 2
  else
    relation:= 3;
  class:= ((x shr 5) * 16 + (y shr 5)) * 16 + relation;
end;

constructor TMapBat.Init;
var
  BatID     : string;
  batIndex  : integer;
  delimiter : char;
  oldFMode  : integer;
begin
  inherited Init;
  {$IFOPT I+}
    {$DEFINE chki}
  {$ENDIF}
  {$I-}

  error:= TRUE;
  for batIndex:= 1 to MaxTray do
    batIDList[batIndex]:= nil;
  Assign(mapBatFile, 'MAPBATID.TXT');
  oldFMode:= integer(fileMode);
  FileMode:= 0;
  Reset(mapBatFile);
  fileMode:= oldFMode;
  if IOResult <> 0 then
    Exit;
  while not EOF(mapBatFile) do
  begin
    ReadLn(mapBatFile, batIndex, delimiter, batID);
    if IOResult <> 0 then
      Exit;
    if (batIndex < 1) or (batIndex > MaxTray) then
      Exit;
    GetMem(batIDList[batIndex], Length(BatID)+1);
    StrPCopy(batIDList[batIndex], BatID);
  end;
  Close(mapBatFile);
  if IOResult <> 0 then Exit;
  error:= FALSE;
  {$IFDEF chki}
    {$I+}
    {$UNDEF chki}
  {$ENDIF}
end;

destructor TMapBat.Done;
var
  batID    : string;
  batIndex : integer;
  delimiter: char;
  oldFMode : integer;
begin
  inherited Done;
  {$IFOPT I+}
    {$DEFINE chki}
  {$ENDIF}
  {$I-}

  oldFMode:= fileMode;
  fileMode:= 0;
  Reset(mapBatFile);
  fileMode:= oldFMode;
  if IOResult <> 0 then Exit;
  while not EOF(mapBatFile) do
  begin
    ReadLn(mapBatFile, batIndex, delimiter, BatID);
    if IOResult <> 0 then Exit;
    if (batIndex < 1) or (batIndex > MaxTray) then Exit;
    MSFreeMEM(batIDList[batIndex], Length(BatID)+1);
  end;
  Close(mapBatFile);
  {$IFDEF chki}
    {$I+}
    {$UNDEF chki}
  {$ENDIF}
end;

constructor TTraysObject.Init;
{-----------------------------------------------------------------------}
{ INIT opens the tray record file and initializes variables for         }
{ the running of TrayLib routines.  This must be loaded in your root.   }
{                                                                       }
{ input  :  <none>                                                      }
{ output :  <none>                                                      }
{ The function returns the number of tray records or a  -1 on error.    }
{-----------------------------------------------------------------------}
var
  i, j, k  : integer;
  filName  : array [0..30] of char;
  oldFMode : integer;
begin
  inherited Init;
  {$IFOPT I+}
    {$DEFINE chki}
  {$ENDIF}
  {$I-}

  oldFMode:= fileMode;
  Assign(gTrayFile, 'TRAYREC.DAT');
  fileMode:= 0;
  Reset(gTrayFile);
  fileMode:= oldFMode;
  if IOResult <> 0 then
    TrayAbort(err_FileResetTRAYREC_DAT);
  InitAbbrTable;
  gTrayMax:= MaxTray;
  gTray.number:= 0;
  LoadSTray(MaxTray+1);
  gTrayMax:= gTray.trayCnt;
  gMda:= gTray.mda;
  mapBat:= New(PmapBat, Init);
  if mapBat^.error then
    TrayAbort(err_LoadingBatteryIDList);

  Seek(gTrayFile,1);
  Read(gTrayFile,DrgMaps);

  {$IFDEF chki}
    {$I+}
    {$UNDEF chki}
  {$ENDIF}
end;

destructor TTraysObject.Done;
var
  i : integer;
begin
  inherited Done;
  Close(gTrayFile);
  if IoResult <> 0 then {nothing};
  MSDisposeObj(mapBat);
end;

procedure TTraysObject.LoadSTray(trayNum: integer);
{-----------------------------------------------------------------------}
{ purpose :  Load into the global tray record variable, the record      }
{            whose drn corrisponds with trayNum                         }
{ input   :  trayNum  - The tray id number and record number - 1        }
{-----------------------------------------------------------------------}
var
  recnum: integer;
begin
  {$IFOPT I+}
    {$DEFINE chki}
  {$ENDIF}
  {$I-}

  if (TrayNum < 1) or (TrayNum > (MaxTray + 3)) then
    TrayAbort(err_TrayRangeInLoadSTray);
  if (TrayNum > MaxTray) then
    recNum:= TrayNum - (MaxTray + 1)
  else
    recNum:= TrayNum + 2;
  Seek(gTrayFile, recnum);
  if IOResult <> 0 then
    TrayAbort(err_FileSeekTRAYREC_DAT);
  Read(gTrayFile, gTray);
  if IOResult <> 0 then
    TrayAbort(err_FileReadTRAYREC_DAT);
  if ((trayNum <> -1) and (trayNum <> gTray.number)) then
    TrayAbort(err_InvalidTrayNum);

  tfsRec.trayNum := trayNum;
  with tfsRec do
  begin
    pSet:= gTray.probNum;
    tSet:= gTray.group;
    idRec:= gTray.idRec;
    StrPCopy(tName, gTray.name);
  end;
  {$IFDEF chki}
    {$I+}
    {$UNDEF chki}
  {$ENDIF}
end;

procedure TTraysObject.LoadTray(trayName:PChar);
{-----------------------------------------------------------------------}
{ purpose :  Load into the global tray record variable, the record      }
{            whose drn corrisponds with trayName                         }
{ input   :  trayNum  - The tray id number and record number - 1        }
{-----------------------------------------------------------------------}
var
  tNum,
  i    : integer;
  tStr : string[63];
begin
  tNum := MapTestGroupID(trayName);
  if tNum <> tfsRec.trayNum then
  begin
    if (tNum < 1) or (tNum > gTrayMax) then
      TrayAbort(err_TrayRangeInLoadTray);
    LoadSTray(tNum);
    for i := 1 to DrugCnt do
    begin
      GetDils(i, tStr,
              dilList[i].drugDils, dilList[i].dilCnt);
      StrPCopy(dilList[i].drugname, tStr);
      TrimAll(dilList[i].drugname, dilList[i].drugname, ' ', 63);
    end;
  end;
end;

procedure TTraysObject.LoadTrayByNum(tNum:integer);
{-----------------------------------------------------------------------}
{ purpose :  Load into the global tray record variable, the record      }
{            whose drn corrisponds with trayNum                         }
{ input   :  trayNum  - The tray id number and record number - 1        }
{-----------------------------------------------------------------------}
var
  i    : integer;
  tStr : string[20];
begin
  if tNum <> tfsRec.trayNum then
  begin
    if (tNum < 1) or (tNum > gTrayMax) then
      TrayAbort(err_TrayRangeInLoadTray);
    LoadSTray(tNum);
    for i := 1 to DrugCnt do
    begin
      GetDils(i, tStr,
              dilList[i].drugDils, dilList[i].dilCnt);
      StrPCopy(dilList[i].drugname, tStr);
      TrimAll(dilList[i].drugname, dilList[i].drugname, ' ', 63);
    end;
  end;
end;

procedure TTraysObject.GetXtra(var xtraTbl: XtraType;
                               var XtraCnt: integer);
{----------------------------------------------------------------------}
{ GetXtra returns a list of valid extra test for a tray.               }
{                                                                      }
{ output :  XtraTbl  -  A list of valid extra tests.                   }
{           XtraCnt  -  The number of extra tests for this tray.       }
{  4 = StS,  5 = Oxi,  7 = Bl,  8 = GmS,  9 = TFG.                     }
{----------------------------------------------------------------------}
begin
  XtraCnt:= gTray.XtraCnt;
  XtraTbl:= gTray.XtraTbl;
end;

function  TTraysObject.RdnToAdn(rdn:integer): integer;
begin
  if Rdn in [1..DrugCnt]
     then RdnToAdn:=gTray.Drug[rdn].drugnum
     else RdnToAdn:=-1;
end;

function TTraysObject.GetXtraWellNumber(var f : Text; testID : PChar;
                                        var wellNumber : integer) : boolean;
{----------------------------------------------------------------------}
{ Returns the zero-based well number of the given extra test in        }
{ 'wellNumber'.  Returns FALSE if the test id is not found.            }
{----------------------------------------------------------------------}
var
  i         : integer;
  okFine    : boolean;
  testIDPos : POINTER;
  readStr   : string[128];
  temp      : array[0..127] of char;
begin
  {$IFOPT I+}
    {$DEFINE chki}
  {$ENDIF}
  {$I-}

  GetXtraWellNumber:= FALSE;
  wellNumber:= 0;
  Reset(f);
  okFine:= TRUE;
  i:= 1;
  { find the panel in the file }
  while (i <= (9*tfsRec.trayNum)) and (OKFine) do
  begin
    ReadLn(f,readStr);
    if IOResult <> 0 then okFine:= FALSE;
    i:= i + 1;
  end;
  { pass by the panel name line }
  if okFine then
  begin
    ReadLn(f,readStr);
    if IOResult <> 0 then okFine:= FALSE;
  end;
  { Find the extra test id in the next 8 lines of the file }
  if okFine then
  begin
    i:= 0;
    { find the right row }
    while (i <= 7) and (okFine) do
    begin
      ReadLn(f,readStr);
      if IOResult <> 0 then
        okFine:= FALSE;
      if okFine then
      begin
        StrPCopy(temp,readStr);
        testIDPos:= StrPos(temp,testID);
        if testIDPos <> nil then
        begin
          wellNumber:= wellNumber + (((OFS(testIDPos^))-(OFS(temp[0]))) div 9 );
          GetXtraWellNumber:= TRUE;
          Exit;
        end;
      end;
      i:= i + 1;
      wellNumber:= wellNumber + 12;
    end;
  end;
  {$IFDEF chki}
    {$I+}
    {$UNDEF chki}
  {$ENDIF}
end;

procedure TTraysObject.GetDrugRDN(testID: PChar; var RDN: integer);
{- given a battery id and a test ID return a tray number and a relative
  drug number. RDN of -1 will be returned if error }
var
  tstr  : string[8];
  abbr  : string[20];
  k     : integer;
  dils  : DrugDilType;
  dilCnt: integer;
  found : boolean;
  Trimmed: array [0..63] of char;
begin
  RDN:= -1;
  k:= 1;
  found:= FALSE;
  TrimAll(Trimmed, testID, ' ', 63);
  while (k <= DrugCnt) and not found do
  begin
    found:= StrComp(dilList[k].drugname,Trimmed)=0;
    if not found then
      Inc(k);
  end;
  if found then
    RDN:= k;
end;

procedure TTraysObject.GetMicText(mic: byte; var strg: MicStr);
{-----------------------------------------------------------------------}
{ GetMicText builds mic string depending upon mdi and flags.            }
{ Input   :  mic  - The mdi and flags of some dilution step             }
{ Output  :  strg  - The printable string for the given mic             }
{-----------------------------------------------------------------------}
begin
  if ((mic and $80) <> 0) then
     strg:= gMda[0]
  else
  begin
    strg:= gMda[(mic and $1F)];
    if ((mic and $20) <> 0) then
      strg[1]:= '>'
    else if ((mic and $40) <> 0) then
      strg[1]:= '<';
  end;
end;

function TTraysObject.GetWell(step, rdn: integer): integer;
{-----------------------------------------------------------------------}
{ Purpose :  Find the well number for a given drug and step value       }
{ Input   :  step  - The dilution step for a given drug                 }
{    rdn  - The drug number for a drug on a given tray                  }
{ Output  :  GetWell  - The well number for that drug/step combo        }
{-----------------------------------------------------------------------}
var
  accum, i  : integer;
begin
  accum:= 0;
  rdn:= rdn - 1;
  for i:= 1 to rdn do
    accum:= accum + gTray.drug[i].dilCnt + 2;
  GetWell:= accum + step + 1;
end;

procedure TTraysObject.GetDils(rdn  : integer; var drgAbr: string;
                               var drugDils: DrugDilType; var dilCnt  : integer);
{----------------------------------------------------------------------}
{ GetDils returns all of the dilution values for a drug, name of drug, }
{ and the number of dilution values.                                   }
{    **** This procedure must be called in the root program.  ****     }
{                                                                      }
{ input  :  rdn      - Ther relative drug number for this tray.        }
{ output :  drgAbr   - Abbr. of this drug in the system.               }
{           drugDils - string representations of the dilution values   }
{                      for each dil. step.  (array of string[4]).      }
{           dilCnt   - Number of dilutions.  Return 0 on error.        }
{----------------------------------------------------------------------}
var
  i, x  : integer;
  tStr  : string[4];
begin
  dilCnt:= 0;
  if (rdn > gTray.drugCnt) then Exit;
  dilCnt:= gTray.drug[rdn].dilCnt + 1;
  AdnToAbbrConvert(gTray.drug[rdn].drugNum, drgAbr);
  x:= GetWell(0, rdn);
  for i:= 0 to (dilCnt+1) do
  begin
    GetMicText(gTray.mic[x], tStr);
    StrpCopy(drugDils[i], tStr);
    x:= x + 1;
  end;
end;

function TTraysObject.GetMdi(rdn, step: integer; nr  : boolean): byte;
{----------------------------------------------------------------------}
{ GetMdi returns the Master Dilution array index for the dilution      }
{ referenced by the trayNum, rdn, and step input.                      }
{                                                                      }
{ input  :  trayNum  -  The number of the tray.                        }
{           rdn      -  The relative drug number for this tray.        }
{           step     -  The number of the dilution in the drug's       }
{                       dilution sequence.                             }
{           nr       -  true suppresses reporting of the MIC.          }
{ output :  <none>  -                                                  }
{ The function returns the dilution's mda index or a 96 on error.      }
{----------------------------------------------------------------------}
var
  i, result  : byte;
begin
  result:= $61;      { set to return error }
  if (rdn >= 1) and (rdn < (gTray.drugCnt + 2)) and (step >= 0)
     and (step < gTray.drug[rdn].dilCnt + 2) then
  begin
    i:= GetWell(step, rdn);
    result:= gTray.mic[i];
  end;
  if nr then
    result:= result or $80;    { set N/R bit }
  GetMdi:= result;
end;

procedure TTraysObject.InitAbbrTable;
{-----------------------------------------------------------------------}
{  InitAbbrTable loads the abrTable from TrayRec.dat.                   }
{-----------------------------------------------------------------------}
begin
  LoadSTray((MaxTray + 3));
  lastAbr:= gTray.cDrgCnt;
  abrTbl:= gTray.abrTbl;
end;

procedure TTraysObject.AdnToAbbrConvert(adn: integer; var abr: string);
{----------------------------------------------------------------------}
{ AdnToAbbrConvert loads the abr string from the abrTable.             }
{    **** This procedure must be called in the root program.  ****     }
{                                                                      }
{ input  :  adn  -  The absolute drug number of this drug.             }
{ output :  abr  -  The drugs abbreviation string.                     }
{----------------------------------------------------------------------}
begin
  if (adn > lastAbr) or (adn < 1 ) then
    abr:= 'XXX'
  else
    abr:= abrTbl[adn];
end;

function TTraysObject.AbbrToAdnConvert(abr: PChar): integer;
{----------------------------------------------------------------------}
{ AbbrToAdnConvert searches the abrTable for the abr string.           }
{ input  :  abr  -  The drugs abbreviation string.                     }
{ The function returns the absolute drug number for this abr.          }
{----------------------------------------------------------------------}
var
  i     : integer;
  tstr  : AbbrStr;
begin
  AbbrToAdnConvert:= 0;
  tstr:= StrPas(abr);
  for i:= (Length(tstr) + 1) to 3 do  {- pad the abbr }
    tstr[i]:= ' ';
  tstr[0]:= #3;  {- set length to 3 }
  i:= 1;
  while abrTbl[i] <> tstr do
  begin
    Inc(i);
    if i > lastAbr then
      exit;
  end;
  AbbrToAdnConvert:= i;
end;

function TTraysObject.TrayObsolete: boolean;
{----------------------------------------------------------------------}
{ TrayObsolete returns the state of the obsolete flag for this tray.   }
{                                                                      }
{ input  :  trayNum  -  The number of the tray.                        }
{ output :  <none>  -                                                  }
{ function returns true if the obso bit for this tray has been set.    }
{----------------------------------------------------------------------}
begin
  TrayObsolete:= (gTray.flag and $01) <> 0;
end;

function TTraysObject.MicGT(first, second: byte): boolean;
{----------------------------------------------------------------------}
{ MicGt returns true if the 1st mic has a greater value than the 2nd.  }
{                                                                      }
{ input  :  first  -  Mic value one.                                   }
{           second -  Mic value two.                                   }
{ output :  MicGt  -  function returns true if the first mic value     }
{                     is greater than the second mic value.            }
{----------------------------------------------------------------------}
var
  code  : integer;
begin
  code:= class(first, second);
  MicGT:= (code = $002) or  {  x,  y, x>y }
          (code = $102) or  { >x,  y, x>y }
          (code = $101) or  { >x,  y, x=y }
          (code = $022) or  {  x, <y, x>y }
          (code = $122) or  { >x, <y, x>y }
          (code = $121);    { >x, <y, x=y }
end;

function TTraysObject.TrayFlo: boolean;
{----------------------------------------------------------------------}
{ TrayFlo returns the state of the fluorogenic flag for this tray.     }
{                                                                      }
{ output :  <none>  -                                                  }
{ function returns true if the Flo bit for this tray has been set.     }
{----------------------------------------------------------------------}
begin
  TrayFlo:= (gTray.flag and $04) <> 0;
end;

procedure TTraysObject.TrayAbort(ErrNum: integer);
{-----------------------------------------------------------------------}
{ purpose :  error out when some condition has been met.                }
{ input   :  errnum  - The error message to be displayed.               }
{-----------------------------------------------------------------------}
begin
  ShowError(nil, IDS_PROGHALT, nil, ErrNum + MOD_TRAYLIB, MOD_TRAYLIB, 0);
  Halt;
end;

function TTraysObject.DrugCnt: integer;
{----------------------------------------------------------------------}
{ DrugCnt returns the number of drugs for the given panel.             }
{                                                                      }
{ input  :  trayNum  -  The number of the tray.                        }
{ output :  <none>  -                                                  }
{ function returns the number of drugs on this tray or -1 on error.    }
{----------------------------------------------------------------------}
begin
  DrugCnt:= 0;
  DrugCnt:= gTray.drugCnt;
end;

function TTraysObject.MapTestGroupID(BatID: PChar): integer;
var
  i : integer;
begin
  MapTestGroupID := 0;
  if not mapBat^.error then
  begin
    for i:= 1 to MaxTray do
    begin
      if mapBat^.batIDList[i] <> nil then
        if StrIComp(mapBat^.batIDList[i], BatID) = 0 then
        begin
          mapTestGroupID:= i;
          Exit;
        end;
    end;
  end;
end;

(*
function TTraysObject.GetDsi(trayNum, rdn, mdi: integer; var nr: boolean): byte;
{----------------------------------------------------------------------}
{ GetDsi returns the dilution step number for the dilution referenced  }
{ by the traynum, drugnum, and master dilution index.                  }
{                                                                      }
{ input  :  trayNum  -  The number of the tray.                        }
{           rdn      -  The relative drug number for this tray.        }
{           mdi      -  The dilution's index in the mda.               }
{ output :  nr       -  true to suppress reporting for the MIC.        }
{           GetDsi   -  function returns the dilution step.            }
{----------------------------------------------------------------------}
var
  i, j     : integer;
  lastStep : integer;
  lMdi     : byte;
  lMdi2    : byte;
begin
  GetDsi:= 0;
  if trayNum <> gTray.number then
    LoadTray(trayNum);
  if (rdn < 1) or (rdn > gTray.drugCnt) then Exit;
  lMdi:= mdi and $1F;
  lMdi2:= mdi and $7F;
  lastStep:= gTray.drug[rdn].dilCnt;
  i:= 0;
  j:= GetWell(0, rdn);
  if lMdi <> 0 then
  begin
    while (lMdi2 <> gTray.mic[j]) and (i <= lastStep) do
    begin
      j:= j + 1;
      i:= i + 1;
    end;
  end;
  nr:= (mdi and $80) <> 0;
  if (mdi and $20) <> 0 then
    GetDsi:= i + 1
  else
    GetDsi:= i;
end; { getdsi }

procedure TTraysObject.GetMda(var table: MastDilarray);
{----------------------------------------------------------------------}
{ GetMda loads the mda table into some other local copy.               }
{                                                                      }
{ output :  table  -  Copy of the mda table.                           }
{----------------------------------------------------------------------}
begin
  table:= gMda;
end;

function TTraysObject.MicLT(first, second: byte): boolean;
{----------------------------------------------------------------------}
{ MicLt returns true if the 1st mic has a smaller value than the 2nd.  }
{                                                                      }
{ input  :  first  -  Mic value one.                                   }
{           second -  Mic value two.                                   }
{ output :  MicGt  -  function returns true if the first mic value     }
{                     is smaller than the second mic value.            }
{----------------------------------------------------------------------}
var
  code  : integer;
begin
  code:= class(first, second);
  MicLT:= (code = $003) or  {  x,  y, x<y }
          (code = $203) or  { <x,  y, x<y }
          (code = $013) or  {  x, >y, x<y }
          (code = $011) or  {  x, >y, x=y }
          (code = $213) or  { <x, >y, x<y }
          (code = $211);    { <x, >y, x=y }
end;

function TTraysObject.MicG  (x, y: byte): boolean;
{----------------------------------------------------------------------}
{ MicG compares two mic's and returns a true if the first mdi is       }
{ greater than the second.                                             }
{ Input:  x  -  The first mic                                          }
{         y  -  The second mic                                         }
{ Output:  <none>                                                      }
{ The function returns a true if x is greater than y                   }
{----------------------------------------------------------------------}
begin
  MicG := (x and $1F) > (y and $1F);
end;

function TTraysObject.MicL  (x, y: byte): boolean;
{----------------------------------------------------------------------}
{ MicL compares two mic's and returns a true if the first mdi is       }
{ less than the second.                                                }
{ Input:  x  -  The first mic                                          }
{         y  -  The second mic                                         }
{ Output:  <none>                                                      }
{ The function returns a true if x is less than y                      }
{----------------------------------------------------------------------}
begin
  MicL := (x and $1F) < (y and $1F);
end;

function TTraysObject.MicGE (x, y: byte): boolean;
{----------------------------------------------------------------------}
{ MicGE compares two mic's and returns a true if the first mdi is      }
{ greater than or equal to the second.                                 }
{ Input:  x  -  The first mic                                          }
{         y  -  The second mic                                         }
{ Output:  <none>                                                      }
{ The function returns a true if x is equal to or greater than y       }
{----------------------------------------------------------------------}
begin
  MicGE:= (x and $1F) >= (y and $1F);
end;

{}  function TTraysObject.MicLE (x, y: byte): boolean;
{----------------------------------------------------------------------}
{ MicLE compares two mic's and returns a true if the first mdi is   }
{ less than or equal to the second.          }
{ Input:  x  -  The first mic        }
{    y  -  The second mic        }
{ Output:  <none>              }
{ The function returns a true if x is less than or equal to y    }
{----------------------------------------------------------------------}
begin
  MicLE:= (x and $1F) <= (y and $1F);
end;

function TTraysObject.MicEQ (x, y: byte): boolean;
{----------------------------------------------------------------------}
{ MicEQ compares two mic's and returns a true if the first mdi is      }
{ equal to the second.                                                 }
{ Input:  x  -  The first mic                                          }
{         y  -  The second mic                                         }
{ Output:  <none>                                                      }
{ The function returns a true if x is equal to y                       }
{----------------------------------------------------------------------}
begin
  MicEQ:= (x and $1F) = (y and $1F);
end;

function TTraysObject.MicNE (x, y: byte): boolean;
{----------------------------------------------------------------------}
{ MicNE compares two mic's and returns a true if the first mdi is      }
{ not equal to the second.                                             }
{ Input:  x  -  The first mic                                          }
{         y  -  The second mic                                         }
{ Output:  <none>                                                      }
{ The function returns a true if x is greater than y                   }
{----------------------------------------------------------------------}
begin
  MicNE:= (x and $1F) <> (y and $1F);
end;

{}  function TTraysObject.NRMic (x: byte): boolean;
{----------------------------------------------------------------------}
{ NRMic test if the NR bit is set in the passed in mic      }
{ Input:  x  -  The mic to be tested        }
{ Output:  <none>              }
{ The function returns a true if the nr bit is set in x    }
{----------------------------------------------------------------------}
begin
  NRMic:= (x and $80) <> 0;
end;

function TTraysObject.LMic  (x: byte): boolean;
{----------------------------------------------------------------------}
{ LMic test if the less than bit is set in the passed in mic           }
{ Input:  x  -  The mic to be tested                                   }
{ Output:  <none>                                                      }
{ The function returns a true if the less than bit is set in x         }
{----------------------------------------------------------------------}
begin
  LMic := (x and $40) <> 0;
end;

function TTraysObject.GMic  (x: byte): boolean;
{----------------------------------------------------------------------}
{ GMic test if the greater than bit is set in the passed in mic        }
{ Input:  x  -  The mic to be tested                                   }
{ Output:  <none>                                                      }
{ The function returns a true if the greater than bit is set in x      }
{----------------------------------------------------------------------}
begin
  gMic := (x and $20) <> 0;
end;

function TTraysObject.GetTGrp(trayNum: integer): byte;
{----------------------------------------------------------------------}
{ GetTGrp returns the tray set group number for a tray.                }
{                                                                      }
{ input  :  trayNum  -  The number of the tray.                        }
{ output :  <none>   -                                                 }
{ The function returns the tray set group number for this tray:        }
{ 1 = Neg, 2 = Pos, 3 = Ana, 4 = HNID, 6 = yeast, 8 = none.            }
{----------------------------------------------------------------------}
begin
  if trayNum <> gTray.number then
    LoadTray(trayNum);
  GetTGrp:= gTray.group;
end;

function TTraysObject.GetProbNum(trayNum: integer): byte;
{----------------------------------------------------------------------}
{ GetProbNum returns the probability number for this tray.             }
{                                                                      }
{ input  :  trayNum  -  The number of the tray.                        }
{ output :  <none>  -                                                  }
{ The function returns the probability number for this tray.           }
{ 1 = Neg,  2 = Pos Frozen,  3 = Ana,  4 = HNID,  5 = Pos Dried,       }
{ 6 = Yeast,  8 = none,  20 = Rapid Neg,  21 = Rapid Pos.              }
{----------------------------------------------------------------------}
begin
  if trayNum <> gTray.number then
    LoadTray(trayNum);
  GetProbNum:= gTray.probNum;
end;

{}  function TTraysObject.TrayXF4(trayNum: integer): boolean;
{----------------------------------------------------------------------}
{ TrayXF4 returns the state of the ** custom flag ** for this tray.  }
{                  }
{ input  :  trayNum  -  The number of the tray.      }
{ output :  <none>  -            }
{ function returns true if the cust bit for this tray has been set.  }
{----------------------------------------------------------------------}
begin
  if trayNum <> gTray.number then LoadTray(trayNum);
  TrayXF4:= (gTray.flag and $10) <> 0;
end;

function TTraysObject.TrayHNID(trayNum: integer): boolean;
{----------------------------------------------------------------------}
{ TrayHNID returns the state of the HNID flag for this tray.           }
{                                                                      }
{ input  :  trayNum  -  The number of the tray.                        }
{ output :  <none>  -                                                  }
{ function returns true if the HNID bit for this tray has been set.    }
{----------------------------------------------------------------------}
begin
  if trayNum <> gTray.number then
    LoadTray(trayNum);
  TrayHNID:= (gTray.flag and $08) <> 0;
end;

function TTraysObject.TrayBkpt(trayNum: integer): boolean;
{----------------------------------------------------------------------}
{ TrayBkpt returns the state of the Break point flag for this tray.    }
{                                                                      }
{ input  :  trayNum  -  The number of the tray.                        }
{ output :  <none>  -                                                  }
{ function returns true if the Bkpt bit for this tray has been set.    }
{----------------------------------------------------------------------}
begin
  if trayNum <> gTray.number then
    LoadTray(trayNum);
  TrayBkpt:= (gTray.flag and $02) <> 0;
end;

function TTraysObject.GetIdRec(trayNum: integer): integer;
{----------------------------------------------------------------------}
{ GetIdRec returns the Bdesc file record number for this tray.         }
{                                                                      }
{ input  :  trayNum  -  The number of the tray.                        }
{ output :  <none>  -                                                  }
{ The function returns the BDesc record number  for this tray.         }
{----------------------------------------------------------------------}
begin
  if trayNum <> gTray.number then
    LoadTray(trayNum);
  GetIdRec:= gTray.idRec;
end;

function TTraysObject.GetTSup(trayNum: integer): byte;
{----------------------------------------------------------------------}
{ GetTSup returns the therapy suppression for a tray.                  }
{                                                                      }
{ input  :  trayNum  -  The number of the tray.                        }
{ output :  <none>  -                                                  }
{ The function returns the therapy suppression byte for this tray.     }
{ 0 = No predefined source suppression, 2 = Urine source suppression   }
{ 8 = Blood suppression based on source level.                         }
{----------------------------------------------------------------------}
begin
  if trayNum <> gTray.number then
    LoadTray(trayNum);
  GetTSup:= gTray.sup;
end;

{}  function TTraysObject.TrayXF5(trayNum: integer): boolean;
{----------------------------------------------------------------------}
{ TrayXF5 returns the state of the phony flag for this tray.    }
{                  }
{ input  :  trayNum  -  The number of the tray.      }
{ output :  <none>  -            }
{ function returns true if the XF5 bit for this tray has been set.  }
{----------------------------------------------------------------------}
begin
  if trayNum <> gTray.number then LoadTray(trayNum);
  TrayXF5:= (gTray.flag and $20) <> 0;
end;

{}  function TTraysObject.TrayXF6(trayNum: integer): boolean;
{----------------------------------------------------------------------}
{ TrayXF6 returns the state of the XF6 flag for this tray (undefined).  }
{                  }
{ input  :  trayNum  -  The number of the tray.      }
{ output :  <none>  -            }
{ function returns true if the XF6 bit for this tray has been set.  }
{----------------------------------------------------------------------}
begin
  if trayNum <> gTray.number then LoadTray(trayNum);
  TrayXF6:= (gTray.flag and $40) <> 0;
end;

{}  function TTraysObject.TrayXF7(trayNum: integer): boolean;
{----------------------------------------------------------------------}
{ TrayXF7 returns the state of the XF7 flag for this tray (undefined).  }
{                  }
{ input  :  trayNum  -  The number of the tray.      }
{ output :  <none>  -            }
{ function returns true if the XF7 bit for this tray has been set.  }
{----------------------------------------------------------------------}
begin
  if trayNum <> gTray.number then LoadTray(trayNum);
  TrayXF7:= (gTray.flag and $80) <> 0;
end;


{}  procedure TTraysObject.MapTray(var mTbl: PanNumTbl);
{ MapTray maps from external to internal tray number.      }
{                  }
{ input  :  <none>  -            }
{ output :  mTab  -  Table of tray mappings.      }
{----------------------------------------------------------------------}
var
  miscBuf  : string[30];
  i  : integer;
begin
  GetStrP(strBase+45, miscBuf);
  GetMiscStr(45, miscBuf);
  for i:= 1 to (SIZEOF(PanNumTbl)) do
    mTbl[i]:= Ord(miscBuf[i]) - Ord(' ');
end;

{}  procedure TTraysObject.LdMapTray(var mTbl: PanNumTbl);
{----------------------------------------------------------------------}
{ LdMapTray loads a panel number table into the misc.fil.    }
{                  }
{ input :  mTab  -  Table of tray mappings.      }
{----------------------------------------------------------------------}
var
  miscBuf  : string[30];
  i   : integer;
begin
  FillCh(ADDR(miscBuf), SIZEOF(miscBuf), ' ');
  miscBuf[0]:= Chr(30);
  for i:= 1 to (SIZEOF(PanNumTbl)) do
    miscBuf[i]:= Chr(mTbl[i] + Ord(' '));
  PutMiscStr(45, miscBuf);
end;

procedure TTraysObject.lpLoadAdnMap;
begin
  LoadSTray((MaxTray + 1));
  gInitAdnMap:= TRUE;
  adnMapTbl:= gTray.MapTbl;
  adnMapCnt:= adnMapTbl[0].iDrgNum;
end;

*)

function TTraysObject.MapDrgAbbr(iAbr: PChar; oAbr: PChar): boolean;
{----------------------------------------------------------------------}
{ MapDrgAbbr searches the drug abbreviation map table for the passed   }
{ in abr and returns the corrisponding abr if one is found.            }
{    **** This procedure must be called in the root program.  ****     }
{                                                                      }
{ input  :  iAbr  -  The abbreviation to be mapped.                    }
{ output :  oAbr  -  The resulted abbreviation.                        }
{----------------------------------------------------------------------}
var
  i         : integer;
  iAbbrStr  : String;
begin
  iAbbrStr:=copy(StrPas(iAbr)+'   ',1,3);
  StrCopy(oAbr,iAbr);

  i:=1;
  While (i<=DrgMaps.aMapCnt) and (DrgMaps.aMapTbl[i].iDrgAbr <> iAbbrStr) do inc(i);
  if i<=DrgMaps.aMapCnt then StrPCopy(oAbr,DrgMaps.aMapTbl[i].oDrgAbr);

  TrimAll(oAbr,oAbr,#32,4);
  MapDrgAbbr:=StrComp(iAbr,oAbr)<>0;
end;

function TTraysObject.DrugMapCount(iAbr: PChar): Integer;
{----------------------------------------------------------------------}
{ input  :  iAbr  -  The drug abbreviation.                            }
{ output :  The number of  drugs that map to iAbr                      }
{----------------------------------------------------------------------}
var
  i, count  : integer;
  iAbbrStr  : String;
begin
  iAbbrStr:=copy(StrPas(iAbr)+'   ',1,3);

  count:=0;
  for i:=1 to DrgMaps.aMapCnt do
      if (DrgMaps.aMapTbl[i].oDrgAbr = iAbbrStr) then inc(count);

  DrugMapCount:=count;
end;

(*
function TTraysObject.MapAdn(adn: integer): integer;
{----------------------------------------------------------------------}
{ MapAdn searches the adn map table for the passed-in adn and returns  }
{ the corrisponding adn if one is found.                               }
{    **** This procedure must be called in the root program.  ****     }
{                                                                      }
{ input  :  adn  -  The adn to be mapped.                              }
{ output :  <none>  -                                                  }
{ The function returns the adn that this adn maps to (or itself).      }
{----------------------------------------------------------------------}
var
  i  : integer;
begin
  MapAdn:= adn;
  if not gInitAdnMap then
    lpLoadAdnMap;
  for i:= 1 to adnMapCnt do
    if adnMapTbl[i].iDrgNum = adn then
    begin
      MapAdn:= adnMapTbl[i].oDrgNum;
      Exit;
    end;
end;

function TTraysObject.AdnMT(var lMapTbl: MapTable): integer;
{----------------------------------------------------------------------}
{ AdnMT returns a copy of the adn map table.                           }
{    **** This procedure must be called in the root program.  ****     }
{                                                                      }
{ input  :  <none>  -                                                  }
{ output :  lMapTbl - A copy of the adn map table.                     }
{ The function returns number of table entries.                        }
{----------------------------------------------------------------------}
var
  i  : integer;
begin
  AdnMT:= 0;
  if not gInitAdnMap then
    lpLoadAdnMap;
  lMapTbl:= adnMapTbl;
  AdnMT:= adnMapCnt;
end;

procedure TTraysObject.MicUnpack(trayNum: integer; var micLvls: mics;
                                 var micList: IsoDilType; var micCnt: integer);
{----------------------------------------------------------------------}
{ MicUnpack returns the dilution text strings for the given mic values.}
{    **** This procedure must be called in the root program.  ****     }
{                                                                      }
{ input  :  trayNum  -  The number of the tray.                        }
{           micLvls  -  An array of mic values from results type,      }
{                       one mic value per drug on this tray.           }
{ output :  micList  -  List of name/dilution string pairs.            }
{           micCnt   -  Number of drugs on this tray.                  }
{----------------------------------------------------------------------}
var
  i  : integer;
begin
  if trayNum <> gTray.number then
    LoadTray(trayNum);
  micCnt:= gTray.drugCnt;
  if micCnt = 0 then Exit;
  for i:= 1 to micCnt do
  begin
    AdnToAbbrConvert(gTray.drug[i].drugNum, micList[i].drug);
    GetMicText(micLvls[i], micList[i].dil);
  end;
end;

{}  procedure TTraysObject.GetCust(var custCnt, fstCust: integer);
{----------------------------------------------------------------------}
{ GetCust returns the number of custom panels and the number of the  }
{ first custom panel.  The custom panels are assumed to be contiguous.  }
{                  }
{ input  :  <none>  -            }
{ output :  custCnt  - The number of custom panels.      }
{    fstCust  - The panel number of the first custom panel.  }
{----------------------------------------------------------------------}
var  i  : integer;
begin
  LoadSTray((MaxTray + 1));
  custCnt:= gTray.custCnt;
  fstCust:= gTray.fstCust;
end;

{}  procedure TTraysObject.GetTStmp(var timeStamp: string);
{----------------------------------------------------------------------}
{ GetTStmp returns the timeStamp stored in TrayRec.dat.    }
{                  }
{ Input:  <none>  -            }
{ Output:  timeStamp - The time and date in a string.    }
{----------------------------------------------------------------------}
begin
  LoadSTray((MaxTray + 2));
  Move(ADDR(gTray.tmStamp), ADDR(timeStamp[1]), SIZEOF(gTray.tmStamp));
  timeStamp[0]:= Chr(SIZEOF(gTray.tmStamp));
end;
*)

END.
