{$R-}
Unit IntlLib;

{- International data conversion routines.

   packed date formats are assumed to be 4 bytes stored as follows
     day (1 byte), month (1 byte) / year (2 bytes)

   packed time formats are assumed to be 4 bytes stored as follows
     hSec (1 byte), sec (1 byte), min (1 byte), hour (1 byte)

   Date strings are returned in the International SHORT date format. If
   the format is undefined in WIN.INI then mm/dd/yyyy is assumed.

   Time strings are returned as defined in the international time format
   section. If the format is undefined in WIN.INI then hh:mm:ss am/pm is
   assumed.

   Number strings are returned according to the International settings.
}

INTERFACE

uses
  WinDos,
  Strings,
  WinProcs,
  WinTypes;

type
  TIntlDate = longint;  {- type used to hold dates }
  TIntlTime = longint;  {- type used to hold times }

{- date stuff }
procedure IntlInitFromWinIni;

function  IntlCurrentDate: TIntlDate;
function  IntlDateStr(ADate: TIntlDate; dstr: PChar; maxlen: integer): PChar;
procedure IntlExtractDate(ADate: TIntlDate; var y, m, d: word);
function  IntlPackDate(y, m, d: word): TIntlDate;
function  IntlStrToDate(dStr: PChar; var err: boolean): TIntlDate;
procedure IntlDateMask(pstr: PChar; maxLen: integer);

{- time stuff }
function IntlCurrentTime: TIntlTime;
function IntlTimeStr(ATime: TIntlTime; includeSecs: boolean;
                    tstr: PChar; maxlen: integer): PChar;
procedure IntlExtractTime(ATime: TIntlTime; var h, m, s, s100: word);
function IntlPackTime(h, m, s, s100: word): TIntlTime;
function IntlStrToTime(tstr: PChar; var err: boolean): TIntlTime;

{- numeric stuff }
function IntlRealToStr(d: double; pstr: PChar; maxLen: integer): PChar;
function IntlIntToStr(i: longint; pstr: PChar; maxLen: integer): PChar;

function IntlStrToReal(nstr: PChar; var err: boolean): double;
function IntlStrToInt(nstr: PChar; var err: boolean): longint;

function IntlThousandSep: char;
function IntlDecimalChar: char;

IMPLEMENTATION

uses
  StrsW,
  DMSDebug;

type
  TBTDateRec = record  {- btrieve date format }
    case integer of
    1: (day  : byte;
        month: byte;
        year : word;
       );
    2: (asLong: TIntlDate;)
  end;

  TBTTimeRec = record {- btrieve time format }
    case integer of
    1: (hSec   : byte;
        sec    : byte;
        min    : byte;
        hour   : byte;
       );
    2: (asLong: TIntlTime;);
  end;

  IntlDate = record
    dateMask   : array[0..25] of char;  {- date mask string }
    dateSep    : char;     {- date seperator }
    dLen       : integer;  {- 1 or 2 byte day (1 or 01) }
    mLen       : integer;  {- 1 or 2 byte month (1 or 01) }
    yLen       : integer;  {- 2 or 4 byte year (1994 or 94) }
    dateOrd    : array[1..3] of char;
  end;

  IntlTime = record
    timeSep    : array[0..1] of char;
    hourLZero  : boolean;
    time24     : boolean;
    s1159      : array[0..10] of char;
    s2359      : array[0..10] of char;
  end;

  IntlNumber = record
    lZero     : boolean;  {- leading zeros for numbers between -1.0 and 1.0 }
    digits    : integer;  {- number of digits after decimal point }
    decimal   : char;     {- decimal character }
    thousand  : char;     {- thoundands seperator }
  end;

var
  iDateFmt   : IntlDate;
  iTimeFmt   : IntlTime;
  iNumFmt    : IntlNumber;

function IntlPackDate(y, m, d: word): TIntlDate;
{- pack date elements into a TIntlDate }
var
  dt  : TBTDateRec;
begin
  dt.month:= m;
  dt.day:= d;
  dt.year:= y;
  IntlPackDate:= dt.asLong;
end;

procedure IntlExtractDate(ADate: TIntlDate; var y, m, d: word);
{- extract elements of a TIntlDate }
var
  dt  : TBTDateRec;
begin
  dt.asLong:= ADate;
  m:= dt.month;
  d:= dt.day;
  y:= dt.year;
end;

procedure IntlDateMask(pstr: PChar; maxLen: integer);
begin
  StrLCopy(pstr, iDateFmt.dateMask, maxLen);
end;

function IntlCurrentDate: TIntlDate;
{- return current date }
var
  m, d, dow : word;
  dt  : TBTDateRec;
begin
  GetDate(dt.Year, m, d, dow);
  dt.month:= byte(m);
  dt.day:= byte(d);
  IntlCurrentDate:= dt.asLong;
end;

function CountMask(mask: PChar; maskChar: char): integer;
{- count number of mask chars in a mask }
var
  i  : PChar;
  j  : integer;
begin
  i:= StrScan(mask, maskChar);
  j:= Ord(i <> nil);
  if i <> nil then
  begin
    while i[j] = maskChar do
      inc(j);
  end;
  CountMask:= j;
end;

function IntlDateStr(aDate: TIntlDate; dstr: PChar; maxlen: integer): PChar;
{- convert a date to a string based on windows current
   international settings. Format is returned in international short date format.
   Assumes iDateFmt.mLen, iDateFmt.dLen, iDateFmt.yLen and iDateFmt.dateMask
   are valid (see IntlInitFromWinIni)}
var
  dt    : TBTDateRec;
  k     : integer;
  rstr  : array[0..30] of char;
  cnt   : integer;

  procedure AddOne(num: word; len: integer);
  var
    pstr  : array[0..30] of char;
    j     : integer;
    p     : array[0..1] of char;  {- needed to make strcat work }
  begin
    str(num:len, pstr);
    if (len > 1) then
    begin
      j:= 0;
      while (j < strlen(pstr)) and (pstr[j] = ' ') do
      begin
        pstr[j]:= '0';
        Inc(j);
      end;
    end;
    strcat(rstr, pstr);
    if cnt < 3 then
    begin
      p[0]:= iDateFmt.dateSep;
      p[1]:= #0;
      strcat(rstr, p);
    end;
  end;

begin
  dt.asLong:= ADate;
  strcopy(rstr, '');
  if dt.asLong <> 0 then
  begin
    k:= 0;
    cnt:= 1;
    while iDateFmt.dateMask[k] <> #0 do
    begin
      case iDateFmt.dateMask[k] of
        'D': begin
               AddOne(dt.day, iDateFmt.dLen);
               Inc(k, iDateFmt.dLen);
               Inc(cnt);
             end;
        'M': begin
               AddOne(dt.month, iDateFmt.mLen);
               Inc(k, iDateFmt.mLen);
               Inc(cnt);
             end;
        'Y': begin
               if iDateFmt.yLen < 4 then
                 dt.year:= dt.year mod 100;
               AddOne(dt.year, iDateFmt.yLen);
               Inc(k, iDateFmt.yLen);
               Inc(cnt);
             end;
        else
          Inc(k);
      end;
    end;
  end;
  strLCopy(dStr, rstr, maxlen);
  IntlDateStr:= dstr;
end;

function BreakDateStr(dStr: PChar; var year,month,day: word): boolean;
{- extract the day month and year from a date string. return true if successful }
var
  p   : PChar;
  tstr: array[0..30] of char;
  pstr: array[0..30] of char;
  ok  : boolean;
  code: integer;
  k   : integer;
  num : integer;
begin
  ok:= true;

  StrLCopy(pstr, dstr, 30);
  k:= 1;
  p:= pstr;
  while ok and (k <= 3) do
  begin
    Val(p, num, code);
    if code <> 0 then
    begin
      StrLCopy(tstr, p, code-1);
      Inc(p, code);
      Val(tstr, num, code);
      ok:= (code = 0) and (num >= 0);
    end
    else
      p:= StrEnd(p);
    if ok then
      case iDateFmt.dateOrd[k] of
        'M' : month:= num;
        'D' : day:= num;
        'Y' : if num < 50 then
                year:= num + 2000
              else if num < 100 then
                year:= num + 1900
              else
                year:= num;
        else
          ok:= false;
      end;
    if (k < 3) and (strlen(p) = 0) then
      ok:= false;
    Inc(k);
  end;
  BreakDateStr:= ok;
end;

function IntlStrToDate(dStr: PChar; var err: boolean): TIntlDate;
var
  m,y,d  : word;
begin
  err:= false;
  if strlen(dStr) = 0 then
    IntlStrToDate:= 0
  else if BreakDateStr(dstr, y,m,d) then
    IntlStrToDate:= IntlPackDate(y,m,d)
  else
  begin
    err:= true;
    IntlStrToDate:= 0;
  end;
end;

{-------------------------------------------------[ Time ]--}

function IntlCurrentTime: TIntlTime;
{- return current time }
var
  h, m, s, s100: word;
  dt  : TBTTimeRec;
begin
  GetTime(h, m, s, s100);
  dt.hour:= byte(h);
  dt.min:= byte(m);
  dt.sec:= byte(s);
  dt.hSec:= byte(s100);
  IntlCurrentTime:= dt.asLong;
end;

function IntlTimeStr(ATime: TIntlTime; includeSecs: boolean;
                    tstr: PChar; maxlen: integer): PChar;
var
  dt    : TBTTimeRec;
  rstr  : array[0..50] of char;
  pstr  : array[0..20] of char;
  ampm  : array[0..10] of char;
  ch    : array[0..1] of char;
  n     : word;
begin
  dt.asLong:= ATime;
  strcopy(rstr, '');

  if aTime <> 0 then
  begin
    strcopy(ampm, ' ');

    {- hours }
    if not iTimeFmt.time24 then
    begin {- convert hours to 12 hour }
      if dt.hour > 11 then
        strcat(ampm, iTimeFmt.s2359)
      else
        strcat(ampm, iTimeFmt.s1159);
      if dt.hour > 12 then
        Dec(dt.hour, 12)
      else if dt.hour = 0 then
        dt.hour:= 12;
    end
    else
      strcat(ampm, iTimeFmt.s2359);

    if iTimeFmt.hourLZero then
    begin
      str(dt.hour:2, pstr);
      if pstr[0] = ' ' then
        pstr[0]:= '0';
    end
    else
      str(dt.hour, pstr);

    strcopy(rstr, pstr);  {- rstr = hour element }
    strcat(rstr, iTimeFmt.timeSep);

    {- minutes }
    str(dt.min:2, pstr);
    if pstr[0] = ' ' then
      pstr[0]:= '0';
    strcat(rstr, pstr);

    if includeSecs then
    begin
      strcat(rstr, iTimeFmt.timeSep);
      Str(dt.sec:2, pstr);
      if pstr[0] = ' ' then
        pstr[0]:= '0';
      strcat(rstr, pstr);
    end;

    strcat(rstr, ampm);
  end;
  strLCopy(tstr, rstr, maxLen);
  IntlTimeStr:= tstr;
end;

procedure IntlExtractTime(ATime: TIntlTime; var h, m, s, s100: word);
{- extract time elements from a TIntlTime }
var
  dt  : TBTTimeRec;
begin
  dt.asLong:= ATime;
  h:= dt.hour;
  m:= dt.min;
  s:= dt.sec;
  s100:= dt.hSec;
end;

function IntlPackTime(h, m, s, s100: word): TIntlTime;
var
  dt  : TBTTimeRec;
begin
  dt.hour:= h;
  dt.min:= m;
  dt.sec:= s;
  dt.hSec:= s100;
  IntlPackTime:= dt.asLong;
end;

function BreakTimeStr(tstr: PChar; var h, m, s: word): boolean;
{- extract the hour,min and seconds (if specified) from a time string.
   return true if successful }
var
  p   : PChar;
  nstr: array[0..50] of char;
  pstr: array[0..50] of char;
  ok  : boolean;
  code: integer;
  k   : integer;
  num : integer;
begin
  ok:= true;

  s:= 0;
  h:= 0;
  m:= 0;

  StrLCopy(pstr, tstr, 50);
  p:= StrScan(pstr, ' ');
  if p <> nil then
    p^:= #0;

  k:= 1;
  p:= pstr;
  while ok and (k <= 3) and (strlen(p) > 0) do
  begin
    Val(p, num, code);
    if code <> 0 then
    begin
      StrLCopy(nstr, p, code-1);
      Inc(p, code);
      Val(nstr, num, code);
      ok:= (code = 0) and (num >= 0);
    end
    else
      p:= StrEnd(p);
    if ok then
      case k of
        1 : h:= num;
        2 : m:= num;
        3 : s:= num;
        else
          ok:= false;
      end;
    Inc(k);
  end;
  if k <= 2 then
    ok:= false;
  BreakTimeStr:= ok;
end;

function IntlStrToTime(tstr: PChar; var err: boolean): TIntlTime;
{- expects tstr to be a 24 hour format }
var
  h,m,s : word;
  p     : PChar;
  pstr  : array[0..50] of char;
  pstr2 : array[0..50] of char;

  function FindAMPM: PChar;
  {- find am/pm string (if any). assumes time string in tstr }
  var
    k   : integer;
    pc  : PChar;
  begin
    k:= 0;
    pc:= nil;
    while (pc = nil) and (k <= strlen(tstr)) do
    begin
      if ((tstr[k] >= '0') and (tstr[k] <= '9')) or (tstr[k] = iTimeFmt.timeSep[0]) then
        Inc(k)
      else
        pc:= @tstr[k];
    end;
    FindAMPM:= pc;
  end;

begin
  err:= false;
  if strlen(tstr) = 0 then
    IntlStrToTime:= 0
  else
  begin
    strcopy(pstr, '');
    if not iTimeFmt.time24 then
    begin {- if 12 hour format then check the am/pm string (if any) }
      p:= FindAMPM;
      if p <> nil then
      begin
        StrCopy(pstr, p);   {- copy am pm portion into pstr }
        p^:= #0;
      end;
    end;
    if BreakTimeStr(tstr, h, m, s) then
    begin
      if strlen(pstr) > 0 then
      begin
        TrimAll(pstr, pstr, ' ', 50);
        StrCopy(pstr2, iTimeFmt.s2359); {- intl PM string }
        AnsiUpper(pstr);
        AnsiUpper(pstr2);
        p:= StrPos(pstr2, pstr);
        if (p <> nil) then  {- if am pm string specified then }
        begin
          if h <> 12 then
            Inc(h, 12);
        end
        else if h = 12 then
          h:= 0;
      end;
      if h <= 23 then
        IntlStrToTime:= IntlPackTime(h, m, s, 0)
      else
      begin
        IntlStrToTime:= 0;
        err:= true;
      end;
    end
    else
    begin
      IntlStrToTime:= 0;
      err:= true;
    end;
  end;
end;

{--------------------------[ Numeric conversion ]--}

procedure IntlNumStr(nstr, dstr: PChar; maxLen: integer);
var
  p     : PChar;
  buf   : array[0..100] of char;
begin
  p:= StrScan(nstr, '.');
  if p <> nil then
    p^:= iNumFmt.decimal;
  InsCommas(buf, nstr, 100, inumFmt.thousand, inumFmt.decimal);
  p:= @buf[0];
  if not iNumFmt.lZero then
  begin
    if (buf[0] = '0') and (buf[1] = '.') then
(*    if strlComp(buf, '0.', 2) = 0 then*)
      p:= @buf[1]
(*    else if strlcomp(buf, '-0.', 3) = 0 then*)
    else if (buf[0] = '-') and (buf[1] = '0') and (buf[2] = '.') then
    begin
      buf[1]:= '-';
      p:= @buf[1];
    end;
  end;
  StrLCopy(dstr, p, maxLen);
end;

function IntlRealToStr(d: double; pstr: PChar; maxLen: integer): PChar;
var
  buf   : array[0..100] of char;
begin
  Str(d:0:iNumFmt.digits, buf);
  IntlNumStr(buf, pstr, maxLen);
  IntlRealToStr:= pstr;
end;

function IntlIntToStr(i: longint; pstr: PChar; maxLen: integer): PChar;
var
  buf   : array[0..100] of char;
begin
  Str(i, buf);
  IntlNumStr(buf, pstr, maxLen);
  IntlIntToStr:= pstr;
end;

function IntlStrToReal(nstr: PChar; var err: boolean): double;
var
  pstr, p : PChar;
  sz      : integer;
  num     : double;
  code    : integer;
begin
  err:= false;
  sz:= StrLen(nstr) + 1;
  GetMem(pstr, sz);

  StripRange(pstr, nstr, sz-1, iNumFmt.thousand, iNumFmt.thousand);
  p:= StrScan(pstr, iNumFmt.decimal);
  if p <> nil then
    p^:= '.';
  if strlen(pstr) > 0 then
  begin
    Val(pstr, num, code);
    err:= code <> 0;
  end
  else
    num:= 0;

  if not err then
    IntlStrToReal:= num
  else
    IntlStrToReal:= 0;
  FreeMem(pstr, sz);
end;

function IntlStrToInt(nstr: PChar; var err: boolean): longint;
var
  pstr, p : PChar;
  sz      : integer;
  num     : longint;
  code    : integer;
begin
  err:= false;
  sz:= StrLen(nstr) + 1;
  GetMem(pstr, sz);

  StripRange(pstr, nstr, sz-1, iNumFmt.thousand, iNumFmt.thousand);
  p:= StrScan(pstr, iNumFmt.decimal);
  if p <> nil then
    p^:= #0;
  if strlen(pstr) > 0 then
  begin
    Val(pstr, num, code);
    err:= code <> 0;
  end
  else
    num:= 0;
  if not err then
    IntlStrToInt:= num
  else
    IntlStrToInt:= 0;
  FreeMem(pstr, sz);
end;

function IntlThousandSep: char;
begin
  IntlThousandSep:= iNumFmt.thousand;
end;

function IntlDecimalChar: char;
begin
  IntlDecimalChar:= iNumFmt.decimal;
end;


{--------------------------[ IntlInitFromWinINI ]--}

procedure IntlInitFromWinIni;
{- get information from WIN.INI on international settings.
   An application should trap WM_WININICHANGE and call this
   routine to keep date formats up to date }
var
  k   : integer;
  n   : integer;
  ok  : boolean;
  pstr: array[0..50] of char;
  cnt : integer;
begin
  {- DATE INITIALIZATION }
  k:= GetProfileString('Intl', 'sShortDate', 'MM/DD/YYYY', iDateFmt.dateMask, sizeof(iDateFmt.dateMask)-1);
  AnsiUpper(iDateFmt.dateMask);

  {- find date seperator (first char that is not a M, D, or Y }
  k:= 0;
  iDateFmt.dateSep:= #0;
  while (iDateFmt.dateSep = #0) and (iDateFmt.dateMask[k] <> #0) do
  begin
    if (iDateFmt.dateMask[k] <> 'M') and (iDateFmt.dateMask[k] <> 'D') and (iDateFmt.dateMask[k] <> 'Y') then
      iDateFmt.dateSep:= iDateFmt.dateMask[k];
    Inc(k);
  end;
  ok:= iDateFmt.dateSep <> #0;

  {- validate date mask }
  if ok then
  begin
    ok:= true;
    {- find lengths of date portions }
    k:= 0;
    cnt:= 1;
    while (iDateFmt.dateMask[k] <> #0) and (cnt <= 3) do
    begin
      if iDateFmt.dateMask[k] <> iDateFmt.dateSep then
      begin
         n:= CountMask(iDateFmt.dateMask, iDateFmt.dateMask[k]);
         if iDateFmt.dateMask[k] = 'M' then
         begin
           iDateFmt.dateOrd[cnt]:= 'M';
           if (n > 2) then
             ok:= false
           else
             iDateFmt.mLen:= n
         end
         else if iDateFmt.dateMask[k] = 'D' then
         begin
           iDateFmt.dateOrd[cnt]:= 'D';
           if n > 2 then
             ok:= false
           else
             iDateFmt.dLen:= n;
         end
         else if iDateFmt.dateMask[k] = 'Y' then
         begin
           iDateFmt.dateOrd[cnt]:= 'Y';
           if (n = 2) or (n = 4) then
             iDateFmt.yLen:= n
           else
             ok:= false;
         end
         else
           ok:= false;  {- error occurred }
         Inc(k, n);
         if cnt < 3 then
           Inc(cnt);
      end
      else
      begin
        if CountMask(iDateFmt.dateMask, iDateFmt.dateSep) > 1 then
        begin
          {- error condition }
          ok:= false;
        end
        else
          Inc(k);
      end;
    end;
  end;

  if not ok then  {if error reset to defaults }
  begin
    strcopy(iDateFmt.dateMask, 'MM/DD/YYYY');
    iDateFmt.dateSep:= '/';
    iDateFmt.mLen:= 2;
    iDateFmt.dLen:= 2;
    iDateFmt.yLen:= 4;
  end;

  {- TIME INITIALIZATION }
  GetProfileString('Intl', 'iTime', '1', pstr, 100);
  iTimeFmt.time24:= (pstr[0] = '1');

  GetProfileString('Intl', 'sTime', ':', pstr, 100);
  iTimeFmt.timeSep[0]:= pstr[0];
  iTimeFmt.timeSep[1]:= #0;

  GetProfileString('Intl', 's1159', '', iTimeFmt.s1159, 100);
  GetProfileString('Intl', 's2359', '', iTimeFmt.s2359, 100);
  GetProfileString('Intl', 'iTLZero', '0', pstr, 100);
  iTimeFmt.hourLZero:= pstr[0] = '1';

  {- NUMERIC INITIALIZATION }
  GetProfileString('Intl', 'sThousand', ',', pstr, 100);
  iNumFmt.thousand:= pstr[0];
  GetProfileString('Intl', 'sDecimal', '.', pstr, 100);
  iNumFmt.decimal:= pstr[0];
  GetProfileString('Intl', 'iDigits', '2', pstr, 100);
  Val(pstr, iNumFmt.digits, k);
  if k <> 0 then
    iNumFmt.digits:= 2;
  GetProfileString('Intl', 'iLZero', '1', pstr, 100);
  iNumFmt.lZero:= (pstr[0] = '1');
end;


BEGIN
  IntlInitFromWinIni;
END.

{- rcf}
