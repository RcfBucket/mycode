unit PrtMnem;

{- this unit is used to print the contents of a mnemonic file }

INTERFACE

uses
  OWindows,
  PrnPrev;

procedure PrintMnemFile(aParent: PWindowsObject; fileName_s: PChar);

IMPLEMENTATION

uses
  APITools,
  Strings,
  DMString,
  WinTypes,
  WinProcs,
  MScan,
  ListLib,
  DBLib,
  INILib,
  Status,
  DBTypes,
  DMSErr,
  DBFile;

{$R PRTMNEM.RES}
{$I PRTMNEM.INC}

function FillMnemPrintInfo(aParent: PWindowsObject; db: PDBFile; pInfo: PPrintInfo): boolean;
{- fills print info with data from fil }
type
  TColInfo = record
    width   : integer;
    assoc   : array[0..8] of char;
  end;
var
  fi        : TFldInfo;
  p1, p2    : array[0..200] of char;
  fld       : integer;
  k         : integer;
  info      : array[1..maxFldsPerRecord] of TColInfo;
  sw        : PPctStatDlg;
  tw        : TWaitCursor;
  tot, cur  : longint;
  strSize   : integer;
  ai        : TAssocInfo;
  listObj   : PListObject;
  listRec   : PMnemListRec;
  seq       : TSeqNum;
begin
  FillMnemPrintInfo:= true;

  listObj:= New(PListObject, Init);
  if listObj = nil then
  begin
    FillMnemPrintInfo:= false;
    exit;
  end;

  listRec:= New(PMnemListRec, Init);
  if listRec = nil then
  begin
    FillMnemPrintInfo:= false;
    MSDisposeObj(listObj);
    exit;
  end;

  cur:= 0;
  tot:= db^.dbc^.NumRecords;
  sw:= nil;
  if tot > 50 then
  begin
    db^.dbd^.FileDesc(p1, sizeof(p1)-1);
    sw:= New(PPctStatDlg, Init(aParent, MakeIntResource(DLG_BUILDING), true, IDC_PERCENT));
    application^.MakeWindow(sw);
    sw^.SetText(p1, IDC_TEXT);
    sw^.CompPctLevel(cur, tot);
  end
  else
    tw.Init;

  FillChar(info, sizeof(info), 0);
  with pInfo^.header^ do
  begin
    AddRow(r_BorderTop, 0);
    db^.dbd^.FileDesc(p1, sizeof(p1)-1);
    SR(IDS_LIST, p2, sizeof(p2)-1);
    StrCat(p1, ' ');
    StrCat(p1, p2);
    AddCell(p1, 0, c_Stretch or c_Bold, -2);
    AddCell(SR(IDS_PRINTEDON, p1, SizeOf(p1)-1), LT200, c_PageRight or c_Right or c_Date, 0);
    AddRow(0, 0);
    AddRow(r_BorderBottom, 0);
    for fld:= 1 to db^.dbd^.NumFields do
    begin
      db^.dbd^.FieldInfo(fld, fi);
      if fi.userFlag and DB_HIDDEN = 0 then
      begin
        k:= db^.dbd^.FieldSizeForStr(fld);
        if fi.fldType = dbSeqRef then      {- SEQREF FIELD TYPE }
        begin
          if (fi.userFlag and DB_Mnemonic <> 0) and (fi.fldAssoc > 0) then
          begin
            db^.dbd^.AssocInfo(fi.fldAssoc, ai);
            StrCopy(info[fld].assoc, ai.fname);
            k:= listObj^.GetMnemDescLen(ai.fName) + 1;
          end;
        end;
        if StrLen(fi.fldName) > k then
          k:= StrLen(fi.fldName);
        info[fld].width:= pInfo^.GetAvgCharWidth(k, 0);
        AddCell(fi.fldName, info[fld].width, c_Bold, 0);
      end;
    end;
  end;

  with pInfo^.footer^ do
  begin
    AddRow(r_CenterRow or r_BorderTop, 0);
    AddCell(SR(IDS_PAGEPOFN, p1, SizeOf(p1)-1), LT400, c_Center or c_PageNum or c_NumPages, 0);
  end;

  {- now fill in body of report }
  with pInfo^.body^ do
  begin
    if db^.dbc^.GetFirst(db^.dbr) then
    begin
      repeat
        Inc(cur);
        if (sw <> nil) and ((cur mod 10 = 0) or (cur >= tot)) then
          sw^.CompPctLevel(cur, tot);
        AddRow(0, 0);
        for fld:= 1 to db^.dbd^.NumFields do
        begin
          db^.dbd^.FieldInfo(fld, fi);
          if fi.userFlag and DB_HIDDEN = 0 then
          begin
            if StrLen(info[fld].assoc) > 0 then
            begin
              db^.dbr^.GetField(fld, @seq, sizeof(seq));
              if seq > 0 then
              begin
                if listObj^.FindMnemSeq(info[fld].assoc, listRec, seq) then
                  StrLCopy(p1, listRec^.desc, sizeof(p1)-1)
                else
                  StrCopy(p1, '****');
              end
              else
                StrCopy(p1, '');
            end
            else
              db^.dbr^.GetFieldAsStr(fld, p1, sizeof(p1)-1);

            AddCell(p1, info[fld].width, c_Normal, 0);
          end;
        end;
      until not db^.dbc^.GetNext(db^.dbr) or ((sw <> nil) and not sw^.CanContinue);
      if sw <> nil then
      begin
        sw^.CompPctLevel(1, 1);
        FillMnemPrintInfo:= sw^.CanContinue;
      end;
    end
    else
    begin
      AddRow(0, 0);
      AddRow(0, 0);
      AddCell(SR(IDS_NODATA, p1, sizeof(p1)-1), 0, c_Stretch, 0);
    end;
  end;

  if sw <> nil then
    MSDisposeObj(sw)
  else
    tw.Done;

  MSDisposeObj(listObj);
end;

procedure PrintMnemFile(aParent: PWindowsObject; fileName_s: PChar);
{- this function prints one or more mnemonic files.
   aParent    : pointer to a parent window.
   fileName_s : FileName(s) to print. This parameter specifies a file name OR
                several file names.
                If only one file name is specified, the standard PrintPrev
                dialog will display. If more than one file is specified, then
                the files are printed directly (no preview).
                To specify more than one file name, separate the file names
                in the string with a character 9 (#9). }
var
  pc      : PChar;
  db      : PDBFile;
  subst   : PErrSubst;
  pInfo   : PPrintInfo;
  lf      : TLogFont;
  p1, p2  : array[0..100] of char;
  temp    : array[0..1] of char;
  k       : integer;

  function PrintIt(fName: PChar; doPrev: boolean): boolean;
  begin
    db:= New(PDBFile, Init(fname, '', dbOpenNormal));
    if db = nil then
    begin
      subst:= New(PErrSubst, Init(fileName_s));
      ShowError(aParent, IDS_CANTOPENMNEM, subst, dbLastOpenError, MOD_PRTMNEM, 0);
      MSDisposeObj(subst);
    end
    else
    begin
      pInfo:= New(PPrintInfo, Init(lf, INIDefPointSize));
      if FillMnemPrintInfo(aParent, db, pInfo) then
      begin
        SR(IDS_PRINT, p2, sizeof(p2)-1);
        StrCat(p2, ' -- ');
        db^.dbd^.FileDesc(p1, sizeof(p1)-1);
        StrCat(p2, p1);
        if doPrev then
        begin
          application^.ExecDialog(New(PPrnPrevDlg, Init(aParent, pInfo, p2)));
          PrintIt:= true;
        end
        else
          PrintIt:= PrintPrim(aParent, pInfo, p2, false);
      end
      else
        PrintIt:= false;
      MSDisposeObj(db);
      MSDisposeObj(pInfo);
    end;
  end;

begin
  if (fileName_s = nil) or (StrLen(fileName_s) = 0) then
    exit;

  MakeScreenFont(lf, false, false);

  pc:= StrScan(fileName_s, #9);
  if pc <> nil then
  begin
    k:= 0;
    StrCopy(p1, '');
    temp[1]:= #0;
    while k <= strlen(fileName_s) do
    begin
      if (k >= strLen(fileName_s)) or (fileName_s[k] = #9) then
      begin
        if not PrintIt(p1, false) then
          break;
        StrCopy(p1, '');  {- clear file name }
      end
      else
      begin
        temp[0]:= fileName_s[k];
        StrCat(p1, temp);
      end;
      Inc(k);
    end;
  end
  else   {- only one file specified }
  begin
    PrintIt(fileName_s, true);
  end;
end;

END.

{- rcf }
