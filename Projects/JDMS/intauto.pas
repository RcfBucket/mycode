Unit IntAuto;



Interface

Uses
  IntGlob,
  WinCrt,
  Objects,
  ODialogs,
  OWindows,
  WinTypes,
  Strings,
	WinProcs;


const
  id_TransTime  = 101;
	id_SaveAuto   = 118;
	id_AutoRanges = 119;
  id_EnableAuto = 120;



Type
	PAutoUploadDlg = ^TAutoUploadDlg;
	TAutoUploadDlg = object(TDialog)
		procedure SetupWindow;virtual;
		function GetFreqID(FreqMin: PChar): integer; virtual;
		procedure SaveAuto(var Msg: TMessage);virtual id_First + id_SaveAuto;
  end;

procedure IntAuto_AutoUpload(ParentWnd: HWnd; WndObjPtr: PWindowsObject);

procedure IntAuto_ReadAutoRanges;
procedure IntAuto_SaveAutoRanges;

Implementation


Uses
	IntAstm;

{
********************
AutoUploadDlg Object
********************
}

procedure TAutoUploadDlg.SetupWindow;
var
	TransTime: array[0..5] of char;
  SFreqID: array[0..4] of char;
  FreqID, Result: integer;
  tstr: array[0..1] of char;

begin
  GetPrivateProfileString('Auto', 'TransmitTime', '0', TransTime, SizeOf(TransTime), 'IntCstm.ini');
  SendDlgItemMsg(id_TransTime, wm_SetText, 0, longint(@TransTime));
  GetPrivateProfileString('Auto', 'Frequency', '1440', SFreqID, SizeOf(SFreqID), 'IntCstm.ini');
  FreqID := GetFreqID(SFreqID);
  SendDlgItemMsg(FreqID, bm_SetCheck, 1, 0);
  GetPrivateProfileString('Auto', 'Enabled', 'N', tstr, SizeOf(tstr), 'IntCstm.ini');
  if StrComp(tstr, 'Y') = 0 then
    SendDlgItemMsg(id_EnableAuto, bm_SetCheck, 1, 0)
	else
    SendDlgItemMsg(id_EnableAuto, bm_SetCheck, 0, 0);
end;

function TAutoUpLoadDlg.GetFreqID(FreqMin: PChar): integer;
var
	TempID: integer;

begin
  if StrComp(FreqMin, '1440') = 0 then
  	TempID := 102;
  if StrComp(FreqMin, '720') = 0 then
  	TempID := 103;
  if StrComp(FreqMin, '480') = 0 then
		TempID := 104;
  if StrComp(FreqMin, '360') = 0 then
  	TempID := 105;
  if StrComp(FreqMin, '240') = 0 then
  	TempID := 106;
  if StrComp(FreqMin, '180') = 0 then
  	TempID := 107;
  if StrComp(FreqMin, '120') = 0 then
  	TempID := 108;
  if StrComp(FreqMin, '90') = 0 then
  	TempID := 109;
  if StrComp(FreqMin, '60') = 0 then
  	TempID := 110;
  if StrComp(FreqMin, '45') = 0 then
  	TempID := 111;
  if StrComp(FreqMin, '30') = 0 then
  	TempID := 112;
  if StrComp(FreqMin, '20') = 0 then
  	TempID := 113;
  if StrComp(FreqMin, '15') = 0 then
  	TempID := 114;
  if StrComp(FreqMin, '10') = 0 then
  	TempID := 115;
  if StrComp(FreqMin, '5') = 0 then
		TempID := 116;
	GetFreqId := TempID;
end;

procedure TAutoUploadDlg.SaveAuto(var Msg: TMessage);
var
	TransTime: array[0..5] of char;
  SFreqID: array[0..4] of char;
  FreqID, RadID: integer;
  tstr: array[0..1] of char;

begin
  SendDlgItemMsg(id_TransTime, wm_GetText, SizeOf(TransTime), longint(@TransTime));
  WritePrivateProfileString('Auto', 'TransmitTime', TransTime, 'IntCstm.ini');
	RadID := bf_Unchecked;
	FreqID := 102;
	while (RadID <> bf_Checked) and (FreqID <= 116) do
  begin
    if SendDlgItemMsg(FreqID, bm_GetCheck, 0, 0) <> 0 then
			RadID := bf_Checked;
    Inc(FreqID);
	end;
	Dec(FreqID);
	case FreqID of
    102: StrCopy(SFreqID, '1440');
    103: StrCopy(SFreqID, '720');
    104: StrCopy(SFreqID, '480');
    105: StrCopy(SFreqID, '360');
    106: StrCopy(SFreqID, '240');
    107: StrCopy(SFreqID, '180');
    108: StrCopy(SFreqID, '120');
    109: StrCopy(SFreqID, '90');
    110: StrCopy(SFreqID, '60');
    111: StrCopy(SFreqID, '45');
    112: StrCopy(SFreqID, '30');
    113: StrCopy(SFreqID, '20');
    114: StrCopy(SFreqID, '15');
    115: StrCopy(SFreqID, '10');
    116: StrCopy(SFreqID, '5');
  end;
  WritePrivateProfileString('Auto', 'Frequency', SFreqID, 'IntCstm.ini');
  if SendDlgItemMsg(id_EnableAuto, bm_GetCheck, 0, 0) <> 0 then
  begin
    StrCopy(tstr, 'Y');
		AutoUploadOn := True;
	end
	else
  begin
    StrCopy(tstr, 'N');
		AutoUploadOn := False;
	end;
  InvalidateRect(GlobHWnd, nil, True);
  WritePrivateProfileString('Auto', 'Enabled', tstr, 'IntCstm.ini');
  Restart;
	EndDlg(0);
end;


{
*********************
Non-Object Procedures
*********************
}

procedure IntAuto_ReadAutoRanges;
var
	CTransFlag: array[0..1] of char;

begin
	with Ranges do
  begin
    GetPrivateProfileString('Auto', 'StartRange', '', StartRange, SizeOf(StartRange), 'IntCstm.ini');
    GetPrivateProfileString('Auto', 'EndRange', '', EndRange, SizeOf(EndRange), 'IntCstm.ini');
    GetPrivateProfileString('Auto', 'RequestType', '', RequestType, SizeOf(RequestType), 'IntCstm.ini');
    GetPrivateProfileString('Auto', 'TransmitFlag', '', CTransFlag, SizeOf(CTransFlag), 'IntCstm.ini');
    ReTransmit:= (StrComp(CTransFlag, 'T') = 0);
  end;
end;

procedure IntAuto_SaveAutoRanges;
var
	CTransFlag: array[0..1] of char;

begin
	with Ranges do
  begin
    WritePrivateProfileString('Auto', 'StartRange', StartRange, 'IntCstm.ini');
    WritePrivateProfileString('Auto', 'EndRange', EndRange, 'IntCstm.ini');
    WritePrivateProfileString('Auto', 'RequestType', RequestType, 'IntCstm.ini');
    if ReTransmit then
      StrCopy(CTransFlag, 'T')
    else
      StrCopy(CTransFlag, 'N');
    WritePrivateProfileString('Auto', 'TransmitFlag', CTransFlag, 'IntCstm.ini');
	end;
end;

procedure IntAuto_AutoUpload(ParentWnd: HWnd; WndObjPtr: PWindowsObject);
var
	AutoUploadDlg: PAutoUploadDlg;

begin
  AutoUploadDlg:= New(PAutoUploadDlg, Init(WndObjPtr, 'INT_AUTO_UPLOAD'));
	Application^.ExecDialog(AutoUploadDlg);
end;

Begin
End.
