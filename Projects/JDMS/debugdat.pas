Unit DebugDat;

Interface

Uses
  WinDos,
  WinProcs,
	Strings;


var
	Debugfptr: longint;

function DebugDat_GetDebugData(CharsToGet: integer): PChar;


Implementation

Uses
	intastm;

function DebugDat_GetDebugData(CharsToGet: integer): PChar;
var
	DataDesc: array[0..255] of char;
	i: integer;
  CharRead: char;

begin
  i := 0;
	while (not Eof(LogDbgFile)) and (i <= CharsToGet-1) do
  begin
    Read(LogDbgFile, CharRead);
		DataDesc[i] := CharRead;
		Inc(i);
  end;
	DataDesc[CharsToGet] := #0;
	DebugDat_GetDebugData := DataDesc;
end;

Begin
End.
