unit MoveIso;

INTERFACE

uses
  OWindows;

procedure MoveIsolates(aParent: PWindowsObject);

IMPLEMENTATION

uses
  DlgLib,
  DMString,
  APITools,
  MPSS,
  Status,
  MSCAN,
  UIFlds,
  DBLib,
  DBIds,
  DBTypes,
  ListLib,
  WinTypes,
  Strings,
  DBFile;

{$I MOVEISO.INC}
{$R MOVEISO.RES}

type
  PMoveIsoDlg = ^TMoveIsoDlg;
  TMoveIsoDlg = object(TCenterDlg)
    sess      : PUIFld;
    iso       : PDBFile;
    listObj   : PListObject;
    {}
    constructor Init(aParent: PWindowsObject; aIso: PDBFile; aListObj: PListObject);
    procedure SetupWindow; virtual;
    function CanClose: boolean; virtual;
  end;

{-----------------------------------------------------------[ TMoveIsoDlg ]--}

constructor TMoveIsoDlg.Init(aParent: PWindowsObject; aIso: PDBFile; aListObj: PListObject);
begin
  inherited Init(aParent, MakeIntResource(DLG_MOVEISO));
  iso:= aIso;
  listObj:= aListObj;
  sess:= New(PUIFld, Init(@self, iso^.dbd, listObj, false, DBIsoSess, IDC_NEWSESS, 0));
end;

procedure TMoveIsoDlg.SetupWindow;
var
  pstr: array[0..50] of char;
begin
  inherited SetupWindow;
  StrCopy(pstr, '');
  iso^.dbr^.PutFieldAsStr(DBISOSess, pstr);
  sess^.XFerRecToCtl(iso^.dbr);
end;

function TMoveIsoDlg.CanClose;
var
  k   : integer;
begin
  k:= sess^.Format;
  if k <> 0 then
  begin
    ShowUIFldError(@self, k, sess);
    CanClose:= false;
  end
  else
  begin
    CanClose:= true;
    sess^.XFerCtlToRec(iso^.dbr);
  end;
end;

{---------------}

function GetNewSession(aParent: PWindowsObject; dbIso: PDBFile): TSeqNum;
{- return a session seq or -1 if cancelled }
var
  sessDlg : PMoveIsoDLg;
  sessRef : TSeqNum;
  listObj : PListObject;
begin
  GetNewSession:= -1;
  listObj:= New(PListObject, Init);
  if listObj <> nil then
  begin
    sessDlg:= new(PMoveIsoDlg, Init(aParent, dbIso, listObj));
    if sessDlg <> nil then
    begin
      if application^.ExecDialog(sessDlg) = IDOK then
      begin
         dbIso^.dbr^.GetField(DBIsoSess, @sessRef, sizeof(TSeqNum));
         GetNewSession:= sessRef;
      end;
    end;
    Dispose(listObj, Done);
  end;
end;

procedure MoveIsolates(aParent: PWindowsObject);
var
  mpss    : PMPSS;
  dbIso   : PDBFile;
  p1      : array[0..100] of char;
  p2      : array[0..200] of char;
  sessRef : TSeqNum;
  sw      : PPctStatDlg;
  wait    : PWaitCursor;
  isOK    : boolean;
  k       : integer;
begin
  dbIso:= New(PDBFile, Init(DBIsoFile, '', dbOpenNormal));
  if dbIso <> nil then
  begin
    mpss:= new(PMPSS, Init(aParent,dbIso,SkipUndefDates));
    if mpss <> nil then
    begin
      if mpss^.MPSSLoadRules(aParent, SR(IDS_MOVEISO, p1, sizeof(p1)-1)) then
      begin
        sessRef:= GetNewSession(aParent, dbIso);
        isOK:= sessRef <> -1;
        if isOK and (sessRef = 0) then
        begin
          isOK:= YesNoMsg(aParent^.hWindow, SR(IDS_CONFIRM, p1, sizeof(p1)),
                          SR(IDS_CONFIRMMOVE, p2, sizeof(p2)-1));
        end;
        if isOK then
        begin
          sw:= nil;
          wait:= nil;
          if mpss^.totalRecords > 50 then
          begin
            sw:= New(PPCtStatDlg, Init(aParent, MakeIntResource(DLG_MOVING), true, IDC_PERCENT));
            application^.MakeWindow(sw);
            sw^.CompPctLevel(0, 0);
          end
          else
            wait:= New(PWaitCursor, Init);

          if mpss^.MPSSGetFirst(dbIso) then
          repeat
            if (sw <> nil) and ((mpss^.mCurrentRecord mod 10 = 0) or
              (mpss^.mCurrentRecord = mpss^.totalRecords)) then
              sw^.CompPctLevel(mpss^.mCurrentRecord, mpss^.totalRecords);

            dbIso^.dbr^.PutField(DBIsoSess, @sessRef);
            dbIso^.dbc^.UpdateRec(dbIso^.dbr);
            if sw <> nil then
              sw^.Update;
          until not mpss^.MPSSGetNext(dbIso);
          if sw <> nil then
          begin
            for k:= mpss^.mCurrentRecord to mpss^.TotalRecords do
              sw^.CompPctLevel(k, mpss^.totalRecords);
            Dispose(sw, Done);
          end;
          if wait <> nil then
            Dispose(wait, Done);
        end;
      end;
      Dispose(mpss, Done);
    end
    else
      Infomsg(aParent^.hwindow, '', SR(IDS_NOMPSSOBJ, p1, sizeof(p1)-1));
    Dispose(dbIso, Done);
  end
  else
    Infomsg(aParent^.hwindow, '', SR(IDS_NOISOFILE, p1, sizeof(p1)-1));
end;

END.
