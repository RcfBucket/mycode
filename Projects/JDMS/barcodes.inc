(****************************************************************************


barcodes.inc

produced by Borland Resource Workshop


*****************************************************************************)

const

	DLG_WA_BARCODES	=	$70B0;
	DLG_BARCODES	=	$70B1;

	BTN_PRINTBC	=	101;

	IDC_PRINTNEWBARCODES	=	201;
	IDC_PRINTALLBARCODES	=	202;
	IDC_PRINTTESTBARCODES	=	203;
	IDC_PRINTBARCODE	=	204;
	IDC_AS4_BARCODES	=	205;

	IDS_BC_PRINTER_ERR	=	$70B1;
	IDS_BC_STOP_PROMPT	=	$70B2;
	IDS_BC_COMM_ERROR	=	$70B3;
	IDS_BC_PAPER_ERROR	=	$70B4;
	IDS_BC_IO_ERROR	=	$70B5;
	IDS_BC_BREAK_DETECTED	=	$70B6;
	IDS_BC_PTR_TIMEOUT	=	$70B7;
	IDS_BC_ERR	=	$70B8;
