unit AS4QC;
{----------------------------------------------------------------------------}
{  Module Name  : AS4QC.PAS                                                  }
{  Programmer   : MJ                                                         }
{  Date Created : 05/95                                                      }
{                                                                            }
{  Purpose - This module implements the QC dialog box and contains the       }
{            routines for printing the QC Diagnostic Report.                 }
{                                                                            }
{  Assumptions -                                                             }
{              None.                                                                     }
{                                                                            }
{  Revision History -                                                        }
{                                                                            }
{  Version  Date      Who    Did What                                        }
{  -------  --------  ------ ----------------------------------------------- }
{  1.00     05/95     MJ     Initial release.                                }
{                                                                            }
{----------------------------------------------------------------------------}

INTERFACE

uses
  OWindows;

procedure DoQCDiag(aParent: PWindowsObject);

IMPLEMENTATION

uses
  autoscan,
  WinTypes,
  PrnPrev,
  DMSDebug,
  DlgLib,
  MScan,
  IniLib,
  APITools,
  Bits,
  IntlLib,
  Strings,
  WinProcs;

{$I AS4RDR.INC}
{$I AS4QC.inc}
{$R AS4QC.res}

type
  PQCDlg = ^TQcDlg;
  TQCDlg = object(TCenterDlg)
    constructor Init(aParent    : PWindowsObject;
                     as4Obj     : PAutoScanObject;
                     ATitle     : Pchar;
                     CloseOnExit: boolean);
    procedure OK(var msg: TMessage); virtual ID_FIRST + ID_OK;
    procedure QCPrint(var msg: TMessage); virtual ID_FIRST + BTN_QCPRINT;
    procedure Cancel(var msg: TMessage); virtual ID_FIRST + ID_CANCEL;
    destructor done;  virtual;
    {}
    private
    autoSCANObject    : PautoSCANObject;
    AS4ObjSupplied    : boolean;
    TestMask          : byte;
    closeDrawerOnExit : boolean;
    procedure SetupWindow;  virtual;
  end;


constructor TQCDlg.Init(aParent    : PWindowsObject;
                        as4Obj     : PAutoScanObject;
                        aTitle     : Pchar;
                        closeOnExit: boolean);
{=======================================================================}
{                                                                       }
{=======================================================================}
var
  pStr : array[0..127] of char;
  wc   : PWaitCursor;
begin
  wc := New(PWaitCursor, Init);
  if not inherited Init(aParent, aTitle) then
  begin
    MSDisposeObj(wc);
    Done;
    Fail;
  end;
  if as4Obj = NIL then
  begin
    autoSCANObject := New(PautoSCANObject, Init);
    if autoscanObject = nil then
    begin
      MSDisposeObj(wc);
      Done;
      Fail;
    end;
    autoScanObject^.SetHWindow(hWindow);
    as4ObjSupplied := false;
  end
  else
  begin
    autoScanObject := as4Obj;
    as4ObjSupplied := true;
  end;
  TestMask := 0;
  closeDrawerOnExit:= closeOnExit;
  if not autoScanObject^.AS4Connected then
  begin
    MSDisposeObj(wc);
    InfoMsg(hWindow,'', SR(IDS_AS4NOTREADY, pStr, sizeof(pStr)-1));
    Done;
    Fail;
  end;
  MSDisposeObj(wc);
end;

procedure TQCDlg.Cancel;
var pStr : array[0..127] of char;
begin
  if CloseDrawerOnExit then
  begin
    {- endAS4 dialog is in SESSRDR.RES }
    InfoMsg(hWindow,'', SR(IDS_CLOSEDRAWER, pStr, 127));
    autoScanObject^.CloseDrawer;
  end;
  inherited Cancel(msg);
end;

destructor TQCDlg.Done;
{=======================================================================}
{                                                                       }
{=======================================================================}
begin
  if not as4ObjSupplied then
    MSDisposeObj(autoSCANObject);
  inherited Done;
end;

procedure TQCDlg.SetupWindow;
{=======================================================================}
{                                                                       }
{=======================================================================}
begin
  inherited SetupWindow;
  EnableWindow(GetDlgItem(hWindow, BTN_QCPRINT), false);
end;

procedure TQCDlg.QCPrint(var msg: TMessage);
{=======================================================================}
{                                                                       }
{=======================================================================}
var
  printInfo : PPrintInfo;
  printDlg  : PPrnPrevDlg;
  printFont : TLogFont;
  pstr,
  pStr1     : PChar;
  i         : integer;
  qcDate    : TIntlDate;
  qcTime    : TIntlTime;

  procedure PrintResult(resID, lblID: word);
  var
    i: integer;
  begin
  with printInfo^ do
    begin
      body^.AddRow(0,0);
      GetDlgItemText(self.hWindow, lblID, pstr, 127);
      body^.AddCell(pStr, LT400, c_left, 12);

      GetDlgItemText(self.hWindow, resID, pstr, 127);
      body^.AddCell(pStr, LT100, c_left, 12);

      body^.AddCell('__________________________________',LT300, c_left, 12);
      body^.AddRow(0,0);
    end;
  end;

begin
  GetMem(pStr, 128);
  GetMem(pStr1, 128);

  MakeScreenFont(printFont, false, false);
  printInfo := new(PPrintInfo, Init(printFont, INIDefPointSize));

  {print the report title}
  SR(IDS_AS4QCTITLE, pStr1, 127);
  printInfo^.header^.AddRow(0, 0);
  printInfo^.header^.AddCell(pStr1, LT800, c_bold or c_center, 16);
  printInfo^.header^.AddRow(0, 0);

  SR(IDS_QCDIAG, pStr, 127);
  qcDate := IntlCurrentDate;
  IntlDateStr(qcDate, pStr1, 127);
  StrLCat(pStr, '  ',127);
  StrLCat(pStr, pStr1, 127);
  qcTime := IntlCurrentTime;
  IntlTimeStr(qcTime, false, pStr1, 127);
  StrLCat(pStr, '  ', 127);
  StrLCat(pStr, pStr1, 127);
  printInfo^.header^.AddRow(0,0);
  printInfo^.header^.AddCell(pStr, LT800, c_bold or c_center, 14);

  for i := 1 to 4 do
    printInfo^.body^.AddRow(0,0);

  printInfo^.body^.AddRow(r_borderBottom,0);
  SR(IDS_TESTPERFORMED, pStr, 127);
  printInfo^.body^.AddCell(pStr, LT400, c_left, 12);
  SR(IDS_RESULT, pStr, 127);
  printInfo^.body^.AddCell(pStr, LT100, c_left, 12);
  SR(IDS_CORRECTMEASURE, pStr, 127);
  printInfo^.body^.AddCell(pStr, LT300, c_left, 12);

  printInfo^.body^.AddRow(0,0);
  printInfo^.body^.AddRow(0,0);

  PrintResult(IDC_PROMCHKSUM, IDC_PROMCHKSUMLBL);
  PrintResult(IDC_RAMMEMORY, IDC_RAMMEMORYLBL);
  PrintResult(IDC_POWERSUPPLYVOLTAGE, IDC_POWERSUPPLYVOLTAGELBL);
  PrintResult(IDC_LIGHTSOURCE, IDC_LIGHTSOURCELBL);
  PrintResult(IDC_ADPHOTOBOARD, IDC_ADPHOTOBOARDLBL);
  PrintResult(IDC_COLORWHEEL, IDC_COLORWHEELLBL);
  PrintResult(IDC_DRAWERMOTION, IDC_DRAWERMOTIONLBL);

  printInfo^.body^.AddRow(0,0);
  SR(IDS_FAN, pStr, 127);
  printInfo^.body^.AddCell(pStr, LT500, c_left, 12);
  printInfo^.body^.AddCell('__________________________________', LT300, c_left, 12);
  printInfo^.body^.AddRow(0,0);

  printInfo^.body^.AddRow(0,0);
  SR(IDS_DIFFUSERPLATE, pStr, 127);
  printInfo^.body^.AddCell(pStr, LT500, c_left, 12);
  printInfo^.body^.AddCell('__________________________________', LT300, c_left, 12);
  printInfo^.body^.AddRow(0,0);

  printInfo^.body^.AddRow(0,0);
  SR(IDS_PHOTODIODESHIELD, pStr, 127);
  printInfo^.body^.AddCell(pStr, LT500, c_left, 12);
  printInfo^.body^.AddCell('__________________________________', LT300, c_left, 12);


  for i := 1 to 10 do
    printInfo^.body^.AddRow(0,0);
  SR(IDS_TECH, pStr, 127);
  printInfo^.body^.AddCell(pStr, LT100, c_left or c_normal, 12);
  printInfo^.body^.AddCell('_______________________________', LT600, c_left, 12);

  SR(IDS_QCRPTTITLE, pStr, 127);
  printDlg := new(PPrnPrevDlg, Init(@self, printInfo, pStr));
  if printDlg <> nil then
  begin
    application^.ExecDialog(printDlg);
    MSDisposeObj(printInfo);
  end;
  MSFreeMem(pStr, 128);
  MSFreeMem(pStr1, 128);
end;

procedure TQCDlg.OK(var msg: TMessage);
{=======================================================================}
{                                                                       }
{=======================================================================}
var
  pStr : array[0..32] of char;
  wait : TWaitCursor;

  procedure FlagResult(maskBit: byte; ID: word);
  begin
    if TestBit(testMask, maskBit) then
      SetDlgItemText(hWindow, ID, SR(IDS_PASS, pStr, sizeof(pStr)-1))
    else
      SetDlgItemText(hWindow, ID, SR(IDS_FAIL, pStr, sizeof(pStr)-1));
  end;

begin
  wait.Init;
  autoSCANObject^.DoSelfTest(TestMask);
  wait.Done;
  if autoSCANObject^.abort then
  begin
    InfoMsg(hWindow, '', SR(IDS_QCABORTED, pStr, sizeof(pStr)-1));
    exit;
  end;
  autoSCANObject^.testTime := IntlCurrentTime;
  autoSCANObject^.testDate := IntlCurrentDate;
  autoSCANObject^.SaveCal(false);
  FlagResult(0, IDC_PROMCHKSUM);
  FlagResult(1, IDC_RAMMEMORY);
  FlagResult(2, IDC_POWERSUPPLYVOLTAGE);
  FlagResult(3, IDC_LIGHTSOURCE);
  FlagResult(4, IDC_ADPHOTOBOARD);
  FlagResult(5, IDC_COLORWHEEL);
  FlagResult(6, IDC_DRAWERMOTION);
  EnableWindow(GetDlgItem(hWindow, BTN_QCPRINT), true);
end;

procedure DoQCDIAG(aParent: PWindowsObject);
begin
  Application^.ExecDialog(new(PQCDlg, init(aParent, nil, 'QCDIAG', true)));
end;

END.
