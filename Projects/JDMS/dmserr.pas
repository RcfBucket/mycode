unit DMSErr;
{----------------------------------------------------------------------------}
{  Module Name  : DMSERR.PAS                                                 }
{  Programmer   : rcf                                                        }
{  Date Created : 02/95                                                      }
{                                                                            }
{  Purpose -                                                                 }
{  This module is used to display error messages. Error messages can be      }
{  displayed indicating error number, module ID and location ID.  This       }
{  provides the ability to more easily track where errors originated.        }
{                                                                            }
{  Assumptions -                                                             }
{  None                                                                      }
{                                                                            }
{  Initialization -                                                          }
{  An application should call the 'RegisterErrorTitle' procedure contained   }
{  herein to set the title that will be used in the error message box. This  }
{  title is used to identify what application is creating the error.         }
{                                                                            }
{  Revision History -                                                        }
{                                                                            }
{  Version  Date      Who    Did What                                        }
{  -------  --------  ------ ----------------------------------------------- }
{  1.00     02/95     rcf    Initial release.                                }
{  1.01     02/95     rcf    Added substitution parameters.                  }
{----------------------------------------------------------------------------}

INTERFACE

uses
  Objects,
  OWindows,
  WinTypes;

const
  MaxSubstStrs = 10;   {- maximum number of subst strs allowed in TErrSubst }

type
  PErrSubst = ^TErrSubst;
  TErrSubst = object(TObject)
    strs    : array[0..MaxSubstStrs-1] of PChar;
    numStrs : integer;
    {}
    constructor Init(aSubstStr: PChar);
    destructor Done; virtual;
    function AddSubstStr(aSubstStr: PChar): boolean;
    function TotalSubstLen: word;
  end;


procedure RegisterErrorTitle(aTitle: PChar);
procedure ShowError(aParent : PWindowsObject; {- parent window object (or nil)}
                    MsgID   : word;           {- string resource ID for message}
                    subst   : PErrSubst;      {- substitution strings for message}
                    errNum  : longint;        {- module ID (need to account for words and negatives) }
                    modID   : word;           {- unique error id for module }
                    location: integer);       {- unique error number }
procedure ShowErrorStr(hw      : HWND;        {- window handle (or nil)}
                       pstr    : PChar;       {- string message}
                       errNum  : longint;     {- unique error id for module }
                       modID   : word;        {- module ID (need to account for words and negatives) }
                       location: integer;     {- location number }
                       len     : word);       {- max length of msg }

IMPLEMENTATION

uses
  WinProcs,
  StrsW,
  ApiTools,
  DMString,
  MScan,
  Strings,
  DMSDebug;

var
  errTitle    : array[0..50] of char;


procedure RegisterErrorTitle(aTitle: PChar);
{- this procedure sets the title that will be used in the error message box.
   The title should be set so that the application from which errors emanate
   is apparent when the error box is displayed (i.e., 'DMS - Error', or 'Report
   Editor - Error'. The default title comes from the common strings lib and is
   'Error' }
begin
  if aTitle = nil then  {- use the default title it nil passed in }
    SR(IDS_ERROR, errTitle, sizeof(errTitle)-1)    {- get global 'Error' string }
  else
    StrLCopy(errTitle, aTitle, sizeof(errTitle)-1);
end;

procedure ShowError(aParent : PWindowsObject; {- parent window object (or nil)}
                    MsgID   : word;           {- string resource ID for message}
                    subst   : PErrSubst;      {- substitution strings for message}
                    errNum  : longint;        {- module ID (need to account for words and negatives) }
                    modID   : word;           {- unique error id for module }
                    location: integer);       {- unique error number }
{- Display an error message.
   in  - aParent  : A parent window object. If this object is not nil, a standard
                    error message box will be displayed. If aParent = nil then
                    a System Modal error box will be displayed. If aParent is
                    nil the message passed should only be one (1) line long
                    (no character 10's (#10) in the message).
         errNum   : This is the error number that caused the error being displayed.
                    If zero, no error number will be displayed.
         modID    : This is the module id of the module calling this procedure.
                    Typically this would be the error base number for the module.
                    If this number is zero, the module number will not be shown
                    in the message box.
         location : This is a location code (optional) for the error. The caller
                    can use this to identify where in the module the error originated.
                    This is useful if the same error can occur in several places
                    in a module.
         msgID    : This is a string resource ID for the message you wish to
                    display.  You can embed the newline character \n in the
                    string for force new lines.  Also, you can embed string
                    substitution characters (%s) into the string.  If subst strs
                    are called for you must fill the subst structure prior to
                    calling ShowError.
         subst    : A substitution object.  If the message has substitution strings
                    the you can pass this object in with the strings to substitute.
                    This parameter can be nil. }
var
  pstr  : PChar;
  tstr  : PChar;
  p     : PChar;
  p2    : array[0..50] of char;
  len   : word;
begin
  tstr:= nil;
  len:= 512;    {- default string length }
  if subst <> nil then
  begin
    Inc(len, subst^.TotalSubstLen);
    GetMem(tstr, len);
  end;

  GetMem(pstr, len);  {- allocate buffer for message string }

  {- if subst strings are specified then perform substitution }
  if subst <> nil then
  begin
    SR(msgID, tstr, len);
    WVSPrintf(pstr, tstr, subst^.strs);
    MSFreeMem(tstr, len);
  end
  else
    SR(msgID, pstr, len); {- get message from string resource }

  if aParent = nil then
  begin
    p:= StrScan(pstr, #10);  {- aParent = nil so a system modal box will be }
    if p <> nil then          {  displayed, only allowing one line of text }
      p^:= #0;
  end;

  {- if an error code is specified, then add some blank lines }
  if (errNum <> 0) or (modID <> 0) or (location <> 0) then
    StrCat(pstr, #10#10);

  {- append error number and module id and location id information. }
  if errNum <> 0 then
  begin
    SR(IDS_ERROR, p2, 50);   {- get global 'Error' string }
    StrCat(pstr, p2);
    StrCat(pstr, ' ');
    StrCat(pstr, HexStr(errNum, sizeof(modID), p2, 100));
    StrCat(pstr, '    ');
  end;

  {- append module ID }
  if modID <> 0 then
  begin
    StrCat(pstr, 'Mod ');
    StrCat(pstr, HexStr(modID, sizeof(modID), p2, 100));
    StrCat(pstr, '    ');
  end;

  {- append location code }
  if location <> 0 then
  begin
    StrCat(pstr, 'Loc ');
    Str(location, p2);
    StrCat(pstr, p2);
  end;

  if aParent = nil then
    FatalError(errTitle, pstr)
  else
    ErrorMsg(aParent^.HWindow, errTitle, pstr);

  MSFreeMem(pstr, len);
end;


procedure ShowErrorStr(hw      : HWND;       {- window handle (or nil)}
                       pstr    : PChar;      {- string message}
                       errNum  : longint;    {- unique error id for module }
                       modID   : word;       {- module ID (need to account for words and negatives) }
                       location: integer;    {- location number }
                       len     : word);      {- max length of msg }
var
  p     : PChar;
  p2    : array[0..50] of char;
begin
  if hw = 0 then
  begin
    p := StrScan(pstr, #10);  {- aParent = nil so a system modal box will be }
    if p <> nil then          {  displayed, only allowing one line of text }
      p^:= #0;
  end;

  {- if an error code is specified, then add some blank lines }
  if (errNum <> 0) or (modID <> 0) or (location <> 0) then
    StrLCat(pstr, #10#10, len);

  {- append error number and module id and location id information. }
  if errNum <> 0 then
  begin
    SR(IDS_ERROR, p2, 50);   {- get global 'Error' string }
    StrLCat(pstr, p2, len);
    StrLCat(pstr, ' ', len);
    StrLCat(pstr, HexStr(errNum, sizeof(modID), p2, 100), len);
    StrLCat(pstr, '    ', len);
  end;

  {- append module ID }
  if modID <> 0 then
  begin
    StrLCat(pstr, 'Mod ', len);
    StrLCat(pstr, HexStr(modID, sizeof(modID), p2, 100), len);
    StrLCat(pstr, '    ', len);
  end;

  {- append location code }
  if location <> 0 then
  begin
    StrLCat(pstr, 'Loc ', len);
    Str(location, p2);
    StrLCat(pstr, p2, len);
  end;

  if hw = 0 then
    FatalError(errTitle, pstr)
  else
    ErrorMsg(hw, errTitle, pstr);
end;

{----------------------------------------------------------[ TErrSubst ]--}

constructor TErrSubst.Init(aSubstStr: PChar);
begin
  inherited Init;
  numStrs:= 0;
  FillChar(strs, sizeof(strs), 0);
  AddSubstStr(aSubstStr);
end;

destructor TErrSubst.Done;
var
  k   : integer;
begin
  for k:= 0 to numStrs - 1 do
    MSStrDispose(strs[k]);
  inherited Done;
end;

function TErrSubst.AddSubstStr(aSubstStr: PChar): boolean;
{- add a string to the substitution list. returns false if no more strings
   may be added }
begin
  if numStrs < MaxSubstStrs then
  begin
    if (aSubstStr = nil) or (strlen(aSubstStr) = 0) then
    begin
      GetMem(strs[numStrs], 1);
      strs[numStrs][0]:= #0;
    end
    else
      strs[numStrs]:= StrNew(aSubstStr);
    Inc(numStrs);
    AddSubstStr:= true;
  end
  else
    AddSubstStr:= false;
end;

function TErrSubst.TotalSubstLen: word;
{- return the length of all the subst strings combined }
var
  k   : integer;
  tot : word;
begin
  tot:= 0;
  for k:= 0 to numStrs - 1 do
    Inc(tot, strlen(strs[k]));
  TotalSubstLen:= 0;
end;

{---------------------------------------------------[ Initialization ]--}

var
  exitSave  : pointer;

procedure DMSErrorExit; far;
begin
  exitProc:= exitSave;
end;

BEGIN
  exitSave:= exitProc;
  exitProc:= @DMSErrorExit;
  RegisterErrorTitle(nil);
END.

{- rcf}
