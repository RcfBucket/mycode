unit UserMsgs;

INTERFACE

uses
  wintypes;

const
  (******* Used by UI Flds ********)

  WM_LOSTFOCUS = WM_USER + 200;
  {- User defined message sent to parent dialogs when a UI fld loses it's focus
     This message is NOT sent if the ctl gaining focus is a button (ID < 200)

     WParam   = resource ID of the field gaining focus
     LParam   = Pointer to the field object that lost focus (PUIFld)
  }

  WM_FLDLIST = WM_USER + 202;
  {- user defined message sent to parent when the list button is pressed. If
     the return value from this message is -1 the listing process is disabled.
     if the return value is 0 then listing will proceed normally for the field
     in question.

     WParam   = the resource ID of the window (control) attempting to list.
     LParam   = a pointer to the field object that is listing.  (PUIFld)
  }

  WM_MNEMSELECTED = WM_USER + 203;
  {- This message is sent to the dialog when a user selects a mnemonic from a
     list.
     wParam   = the resource ID of the control.
     lParam   = a pointer to the field (PUIFld)
  }

  WM_AUTOCLEARED = WM_USER + 204;
  {- This message is sent to the dialog when a user first types something and
     the control clears it (based on its clearflag) This is currently only sent
     by mnemonic reference fields.
     wParam   = the resource ID of the control.
     lParam   = a pointer to the field (PUIFld)
  }

  WM_USERKEY = WM_USER + 205;
  {- This message is sent to the parent dialog of a UIFLD when a function key
     is pressed that is not needed by the field. This gives the dialog a change
     to respond to key down messages.
     wParam   = same as WM_KEYDOWN
     lParam   = same as WM_KEYDOWN
  }

  (******* Used by UIGSRes ********)
  WM_SPIN       = WM_USER + 210;
  {- sent by the results spin edit control.
     wParam  : 0 = Spin Up, 1 = Spin Down
     lParam  : unused }

  WM_RESCHANGE  = WM_USER + 211;
  {- sent by the results spin edit control.
     wParam  : 0 = free text data in lParam,
               1 = lParam has a pointer to a ListRec containing the mnemonic
                   information (if nil then no mnemonic selected)
     lParam  : see wParam }

  WM_RIGHTCLICK = WM_USER + 212;
  {- send by grid to dialog.
     wParam  : 0 (unused)
     lParam  : row that was right clicked }

  WM_RESGRIDGOTFOCUS = WM_USER + 220;
  {- called when a result grid (in ID/MIC entry) gets input focus.
     wParam  : ID of the grid recieving focus.
     lParam  : Pointer to the PWindowsObject of the grid itself that is getting focus }

  (******* Used by Report Generator ********)

  {- box window is closing (sent to parent) from a box window }
  {  wParam = HWindow of section }
  {- lParam = Pointer to PWindowsObject of closing window }
  UM_BOXCLOSING   = WM_USER + 300;

  {- This message is send when a box window get focus, changes size or moves. }
  {- wParam   = HWindow of box that changed or receieved focus }
  {- lParam   = pointer to the box window object }
  UM_BOXSIZEMOVE  = WM_USER + 301;

  {- box status display, sent to top level windows to display box status msg }
  {- wParam   = not used (0) }
  {- lParam   = pointer to a null terminated string for display.
                (if nil, then clear status) }
  UM_BOXSTATUS    = WM_USER + 302;

  {- Preference changes. Main window sends to all children, which in turn
     send to their children and so on. It indicates that preferences have
     changed (ie, WIN.INI changes, display units, etc }
  {- wParam   = not used (0) }
  {- lParam   = not used (0) }
  UM_PREFERENCECHANGE = WM_USER + 303;

  {- right mouse clicked in a section send to parent of section }
  {- wParam   = HWindow of section }
  {- lParamLo = mouse.x position }
  {- lParamHi = mouse.y position }
  UM_RIGHTMOUSE = WM_USER + 304;

  {- sent to page windows to indicate a new zoom factor }
  {- wParam   = 0 }
  {- lParam   = zoom factor * 100 }
  UM_SETZOOM = WM_USER + 305;

  {- Sent to report edit windows to see if a specified report is already loaded}
  {- WParam   = 0 }
  {- LParam   = sequence number for report to check }
  {- Message result should be 0 if report is not loaded, otherwise non-zero }
  UM_ISRPTLOADED = WM_USER + 306;


  (****** Used in MScan *******)
  {- this window message is sent by SendChangeTitleMsg.       }
  {- it is using the module id for DMSMAIN.  DMSMAIN does not }
  {- this number for anything but its base reference.         }
  {- wParam = not used }
  {- lParam = not used }
  WM_CHANGETITLE = WM_USER + 310;

  (****** Used in ListLib *******)
  {- this window message is sent by SetupWindow.       }
  {- wParam = not used }
  {- lParam = not used }
  WM_FILLMYGRID = WM_USER + 320;

  (****** Used in JamCheck *******)
  {- this window message is sent by SetWAStatus.       }
  {- wParam = not used }
  {- lParam = ptr to status buffer }
  WM_WASTATUS = WM_USER + 350;

  (****** Used in AS4RDR *******)
  WM_GETQC = WM_USER + 500;

  (****** Used in DMSMAIN/TWOWAY/WA *******)
  WM_INQUIRE = WM_USER + 501;
  { This message is used to provide minimal inter process comminication between
    running applications. It's purpose is to be able to tell when programs
    start and stop as well as to tell an application to bring itself into focus.

    wParam - One of the following constants. These are information passed by
             or passed to a program to request/send inquiry information.
    ProgramStopped - A program sends this message to indicate that it has stopped.
    ProgramStarted - A program sends this message when it starts up.
    ProgramInquire - This message is broadcast to inquire about a program.
                     Programs receiving this message should respond by sending
                     back a ProgramStopped message to the program that passed
                     the original inquiry.
    ProgramFocus   - Sent to programs to tell them to bring themselves to the
                    foreground. }
  ProgramStopped  = 0;
  ProgramStarted  = 1;
  ProgramInquire  = 2;
  ProgramFocus    = 3;

  {- The lParam is used to indicate who sent the message and to indicate who
     should receive it. lParamLo is used to indicate who sent the message,
     lParamHi is used to tell who the message is indicated for.
     lParamLo - One of the following constants. This param indicates who sent
                the inquire message. (InqMsgAll is not valid here!)
     lParamHi - One of the following constants. This param indicates who the
                inquire message was intended for. If InqMsgAll is indicated the
                all apps should respond to the message (except the app sending
                the message) }
  InqMsgAll       = 0;  {- sent to/by to all receivers }
  InqMsgDMS       = 1;  {- sent to/by the DMSMain }
  InqMsgTwoWay    = 2;  {- sent to/by the TwoWay interface }
  InqMsgWA        = 3;  {- sent to/by the WA main module }

  (****** Used in MTask *******)
  {- this window message is sent to main windows to query if they are a DMS program. }
  {- wParam = Maximum size of the buffer pointed to by lParam. }
  {- lParam = This is a pointer to a buffer to which the receiving task (if it
              is a DMS main program) must copy it's application name. This is
              the same name that was used in it's application.init() call. It
              can be retrieved using app.Name.

   - If an application is a DMS main program, and responds to this message, it
     should set the return value to the constant [DMSMainProgRetCode]. This
     constant is found in MTASK.PAS }
  WM_MAINPROGQUERY = WM_USER + 502;

  {- sent to File maintenance }
  {- wParam   = 0 }
  {- lParam   = 0 }
  WM_FMPROCESS = WM_USER + 256;


IMPLEMENTATION

END.

{- rcf }
