unit PrnInfo;

{- printer specific information routines. }

INTERFACE

uses
  wintypes,
  winprocs,
  prnObj,
  objects;

const
  {- borland did not include these in WinTypes.pas }
  DMPAPER_ENV_DL              = 27;
  DMPAPER_ENV_C5              = 28;
  DMPAPER_ENV_C3              = 29;
  DMPAPER_ENV_C4              = 30;
  DMPAPER_ENV_C6              = 31;
  DMPAPER_ENV_C65             = 32;
  DMPAPER_ENV_B4              = 33;
  DMPAPER_ENV_B5              = 34;
  DMPAPER_ENV_B6              = 35;
  DMPAPER_ENV_ITALY           = 36;
  DMPAPER_ENV_MONARCH         = 37;
  DMPAPER_ENV_PERSONAL        = 38;
  DMPAPER_FANFOLD_US          = 39;
  DMPAPER_FANFOLD_STD_GERMAN  = 40;
  DMPAPER_FANFOLD_LGL_GERMAN  = 41;

  first_DMPaper               = DMPAPER_LETTER;
  last_DMPaper                = DMPAPER_FANFOLD_LGL_GERMAN;

type
  {- object to hold paper information }
  PPaperInfo = ^TPaperInfo;
  TPaperInfo = object(TObject)
    name     : PChar;
    size     : TPoint;
    idx      : integer;
    constructor Init(AName: PChar; ASize: TPoint; AnIdx: integer);
    destructor Done; virtual;
  end;

function PaperInfo(APrn: PPrinter; info: PCollection): boolean;
{- returns paper information for a selected printer or for all supported
   papers }

function SelectedPaperSize(APrn: PPrinter; var dmPaperIdx: integer; var size: TPoint): boolean;
{- returns the currently selected paper size in the printer driver in LTwips }

function PrintMargins(APrn: PPrinter; var margins: TRect): boolean;
{- return the minimum margins for the printer }

function Orientation(APrn: PPrinter; var orient, relation: integer): boolean;
{- return current printer orientation, DMORIENT_PORTRAIT or DMORIENT_LANDSCAPE
   and relationship of portrait to landscape }

function MaxMinPrintExtents(APrn: PPrinter; var max, min: TPoint): boolean;
{- return maximum and minimum extents for paper sizes }

procedure MergePapers(dest, src: PCollection);
{- merge paper definitions from src into dest }

IMPLEMENTATION

uses
  strings,
  win31,
  print,
  MScan,
  DMSDebug;

type
  Pwordarray = ^Twordarray;
  Twordarray = array[0..100] of word;  {- of arbitrary size (doesnt matter) }

  PPointarray = ^TPointarray;
  TPointarray = array[0..100] of TPoint; {- of arbitrary size (doesnt matter) }

(*
  { re-declare this because WinProcs delcares it incorrectly }
  TDeviceCapabilities = function(DeviceName, Port: PChar; Index: word;
                                 Output: PChar; DevMode: PChar): Longint;
*)

const
  paperSizes : array[first_DMPaper..last_DMPaper] of TPoint =
  ( (x:Round(8.5 * 1440); y:Round(11  * 1440)), { dmPaper_Letter }
    (x:Round(8.5 * 1440); y:Round(11  * 1440)), { dmPaper_LetterSmall }
    (x:Round(11  * 1440); y:Round(17  * 1440)), { dmpaper_Tabloid }
    (x:Round(17  * 1440); y:Round(11  * 1440)), { dmPaper_dmpaper_Ledger }
    (x:Round(8.5 * 1440); y:Round(14  * 1440)), { dmpaper_Legal }
    (x:Round(5.5 * 1440); y:Round(8.5 * 1440)), { dmpaper_Statement }
    (x:Round(7.25* 1440); y:Round(10.5* 1440)), { dmpaper_Executive }
    (x:Round(297/25.4*1440); y:Round(420/25.4*1440)), { dmpaper_A3 }
    (x:Round(210/25.4*1440); y:Round(297/25.4*1440)), { dmpaper_A4 }
    (x:Round(210/25.4*1440); y:Round(297/25.4*1440)), { dmpaper_A4Small }
    (x:Round(148/25.4*1440); y:Round(210/25.4*1440)), { dmpaper_A5 }
    (x:Round(250/25.4*1440); y:Round(354/25.4*1440)), { dmpaper_B4 }
    (x:Round(182/25.4*1440); y:Round(257/25.4*1440)), { dmpaper_B5 }
    (x:Round(8.5 *1440); y:Round(13*1440)), { dmpaper_Folio }
    (x:Round(215/25.4*1440); y:Round(275/25.4*1440)), { dmpaper_Quarto }
    (x:Round(10*1440); y:Round(14*1440)), { dmpaper_10X14 }
    (x:Round(11*1440); y:Round(17*1440)), { dmpaper_11X17 }
    (x:Round(8.5*1440); y:Round(11*1440)), { dmpaper_Note }
    (x:Round(3.875*1440); y:Round(8.875*1440)), { dmpaper_Env_9 }
    (x:Round(4.125*1440); y:Round(9.5*1440)), { dmpaper_Env_10 }
    (x:Round(4.5*1440); y:Round(10.375*1440)), { dmpaper_Env_11 }
    (x:Round(4.5*1440); y:Round(11*1440)), { dmpaper_Env_12 }
    (x:Round(5*1440); y:Round(11.5*1440)), { dmpaper_Env_14 }
    (x:Round(-1); y:Round(-1)), { dmpaper_CSheet (unsupported) }
    (x:Round(-1); y:Round(-1)), { dmpaper_DSheet (unsupported) }
    (x:Round(-1); y:Round(-1)), { dmpaper_ESheet (unsupported) }
    (x:Round(110/25.4*1440); y:Round(220/25.4*1440)), { DMPAPER_ENV_DL }
    (x:Round(162/25.4*1440); y:Round(229/25.4*1440)), { DMPAPER_ENV_C5 }
    (x:Round(324/25.4*1440); y:Round(458/25.4*1440)), { DMPAPER_ENV_C3 }
    (x:Round(229/25.4*1440); y:Round(324/25.4*1440)), { DMPAPER_ENV_C4 }
    (x:Round(114/25.4*1440); y:Round(162/25.4*1440)), { DMPAPER_ENV_C6 }
    (x:Round(114/25.4*1440); y:Round(229/25.4*1440)), { DMPAPER_ENV_C65 }
    (x:Round(250/25.4*1440); y:Round(353/25.4*1440)), { DMPAPER_ENV_B4 }
    (x:Round(176/25.4*1440); y:Round(250/25.4*1440)), { DMPAPER_ENV_B5 }
    (x:Round(176/25.4*1440); y:Round(125/25.4*1440)), { DMPAPER_ENV_B6 }
    (x:Round(110/25.4*1440); y:Round(230/25.4*1440)), { DMPAPER_ENV_ITALY }
    (x:Round(3.875*1440); y:Round(7.5*1440)), { DMPAPER_ENV_MONARCH}
    (x:Round(3.625*1440); y:Round(6.5*1440)), { DMPAPER_ENV_PERSONAL }
    (x:Round(14.875*1440); y:Round(11*1440)), { DMPAPER_FANFOLD_US }
    (x:Round(8.5*1440); y:Round(12*1440)), { DMPAPER_FANFOLD_STD_GERMAN}
    (x:Round(8.5*1440); y:Round(13*1440)) { DMPAPER_FANFOLD_LGL_GERMAN }
    );

procedure PaperNameIdx(idx: integer; AName: PChar; maxLen: integer);
{- return a paper name based on its index value. idx is one of the
   standard paper constants found in WinTypes (dmPaper_...) }
var
  pstr  : array[0..200] of char;
begin
  case idx of
    dmpaper_Letter:       strcopy(pstr, 'Letter 8 1/2 x 11 in.');
    dmpaper_LetterSmall:  strcopy(pstr, 'Letter Small 8 1/2 x 11 in.');
    dmpaper_Tabloid:      strcopy(pstr, 'Tabloid 11 x 17 in.');
    dmpaper_Ledger:       strcopy(pstr, 'Ledger 17 x 11 in.');
    dmpaper_Legal:        strcopy(pstr, 'Legal 8 1/2 x 14 in.');
    dmpaper_Statement:    strcopy(pstr, 'Statement 5 1/2 x 8 1/2 in.');
    dmpaper_Executive:    strcopy(pstr, 'Executive 7 1/4 x 10 1/2 in.');
    dmpaper_A3:           strcopy(pstr, 'A3 297 x 420 mm');
    dmpaper_A4:           strcopy(pstr, 'A4 210 x 297 mm');
    dmpaper_A4Small:      strcopy(pstr, 'A4 Small 210 x 297 mm');
    dmpaper_A5:           strcopy(pstr, 'A5 148 x 210 mm');
    dmpaper_B4:           strcopy(pstr, 'B4 250 x 354 mm');
    dmpaper_B5:           strcopy(pstr, 'B5 182 x 257 mm');
    dmpaper_Folio:        strcopy(pstr, 'Folio 8 1/2 x 13 in.');
    dmpaper_Quarto:       strcopy(pstr, 'Quarto 215 x 275 mm');
    dmpaper_10X14:        strcopy(pstr, '10x14 in.');
    dmpaper_11X17:        strcopy(pstr, '11x17 in.');
    dmpaper_Note:         strcopy(pstr, 'Note 8 1/2 x 11 in.');
    dmpaper_Env_9:        strcopy(pstr, 'Envelope #9 3 7/8 x 8 7/8 in.');
    dmpaper_Env_10:       strcopy(pstr, 'Envelope #10 4 1/8 x 9 1/2 in.');
    dmpaper_Env_11:       strcopy(pstr, 'Envelope #11 4 1/2 x 10 3/8 in.');
    dmpaper_Env_12:       strcopy(pstr, 'Envelope #12 4 1/2 x 11 in.');
    dmpaper_Env_14:       strcopy(pstr, 'Envelope #14 5 x 11 1/2 in.');
    dmpaper_CSheet:       strcopy(pstr, 'C size sheet (unsupported)');
    dmpaper_DSheet:       strcopy(pstr, 'D size sheet (unsupported)');
    dmpaper_ESheet:       strcopy(pstr, 'E size sheet (unsupported)');
    DMPAPER_ENV_DL:       strcopy(pstr, 'Envelope DL, 110 x 220 mm');
    DMPAPER_ENV_C5:       strcopy(pstr, 'Envelope C5, 162 x 229 mm');
    DMPAPER_ENV_C3:       strcopy(pstr, 'Envelope C3, 324 x 458 mm');
    DMPAPER_ENV_C4:       strcopy(pstr, 'Envelope C4, 229 x 324 mm');
    DMPAPER_ENV_C6:       strcopy(pstr, 'Envelope C6, 114 x 162 mm');
    DMPAPER_ENV_C65:      strcopy(pstr, 'Envelope C65, 114 x 229 mm');
    DMPAPER_ENV_B4:       strcopy(pstr, 'Envelope B4, 250 x 353 mm');
    DMPAPER_ENV_B5:       strcopy(pstr, 'Envelope B5, 176 x 250 mm');
    DMPAPER_ENV_B6:       strcopy(pstr, 'Envelope B6, 176 x 125 mm');
    DMPAPER_ENV_ITALY:    strcopy(pstr, 'Envelope, 110 x 230 mm');
    DMPAPER_ENV_MONARCH:  strcopy(pstr, 'Envelope Monarch, 3 7/8 x 7 1/2 in.');
    DMPAPER_ENV_PERSONAL: strcopy(pstr, 'Envelope, 3 5/8 x 6 1/2 in.');
    DMPAPER_FANFOLD_US:   strcopy(pstr, 'U.S. Std Fanfold, 14 7/8 x 11 in.');
    DMPAPER_FANFOLD_STD_GERMAN: strcopy(pstr, 'German Std Fanfold, 8 1/2 x 12 in.');
    DMPAPER_FANFOLD_LGL_GERMAN: strcopy(pstr, 'German Legal Fanfold, 8 1/2 x 13 in.');
    dmpaper_User:         strcopy(pstr, 'User defined...');
    else                  strcopy(pstr, 'Unknown paper size');
  end;
  StrLCopy(AName, pstr, maxLen);
end;

procedure MergePapers(dest, src: PCollection);
{- merge all papers in src that dont already exist into dest }
var
  k   : integer;
  item: PPaperInfo;
  idx : integer;

  function Matches(p: PPaperInfo): Boolean; far;
  begin
    Matches:= (idx = p^.idx);
  end;

begin
  for k:= 0 to src^.count - 1 do
  begin
    item:= src^.At(k);
    idx:= item^.idx;
    if dest^.FirstThat(@Matches) = nil then
      dest^.Insert(New(PPaperInfo, Init(item^.name, item^.size, item^.idx)));
  end;
end;

function PaperInfo(APrn: PPrinter; info: PCollection): Boolean;
{- return a collection paper information for the current printer.
   if APrn = nil, then all available paper information will be
   returned. The collection must be passed in pre-initialized,
   and when returned will contain pointers to TPaperInfo structures.
   All previous information in info will be destroyed.
   Returns false if paper info cannot be returned.
NOTE: ALL SIZES returned are in LTwips! }
var
  k     : integer;
  ret   : Boolean;
  dvc   : TDevCaps;
  pInfo : PPaperInfo;
  pstr  : array[0..200] of char;

  nCount: integer;
  pnames: PChar;
  sizes : PPointarray;
  sCount: integer;
  papers: Pwordarray;
  pCount: integer;

begin
  ret:= true;
  info^.FreeAll;   {- clear any previous information }
  if APrn = nil then
  begin  {- return all paper information }
    for k:= first_DMPaper to last_DMPaper do
    begin
      PaperNameIdx(k, pstr, 200);
      pInfo:= New(PPaperInfo, Init(pstr, paperSizes[k], k));
      info^.Insert(pInfo);
    end;
  end
  else
  if aPrn^.status = ps_OK then begin
    sCount:= 0;
    nCount:= 0;
    pCount:= 0;

    @dvc:= GetProcAddress(aprn^.deviceModule, 'DeviceCapabilities');
    if @dvc <> nil then
    begin
      k:= dvc(aprn^.device, aprn^.port, DC_PAPERNAMES, nil, nil);
      if k <> -1 then
      begin
        nCount:= k;
        GetMem(pnames, 64*nCount);
        k:= dvc(aprn^.device, aprn^.port, DC_PAPERNAMES, pnames, nil);

        {- get paper sizes }
        k:= dvc(aprn^.device, aprn^.port, DC_PAPERSIZE, nil, nil);
        if k <> -1 then
        begin
          sCount:= k;
          GetMem(sizes, sCount * sizeof(TPoint));
          k:= dvc(aprn^.device, aprn^.port, DC_PAPERSIZE, PChar(sizes), nil);
        end;

        {- get paper size indicies }
        k:= dvc(aprn^.device, aprn^.port, DC_PAPERS, nil, nil);
        if k <> -1 then
        begin
          pCount:= k;
          GetMem(papers, pCount * sizeof(word));
          k:= dvc(aprn^.device, aprn^.port, DC_PAPERS, PChar(papers), nil);
        end;

        {- if counts returned are not all the same then something
           went wrong! }
        if (nCount = sCount) and (sCount = pCount) then
        begin
          for k:= 0 to Pred(nCount) do
          begin
            sizes^[k].x:= Round(sizes^[k].x/10.0/25.4 * 1440);
            sizes^[k].y:= Round(sizes^[k].y/10.0/25.4 * 1440);
            pInfo:= New(PPaperInfo, Init(
                      PChar(@pnames[k*64]),
                      sizes^[k],
                      integer(papers^[k])
                    ));
            info^.Insert(pInfo);
          end;
        end
        else
          ret:= false;

        {- deallocate memory used by buffers }
        MSFreeMem(pnames, 64*nCount);
        MSFreeMem(sizes, sCount * sizeof(TPoint));
        MSFreeMem(papers, pCount * sizeof(word));
      end;
    end
    else
      ret:= false;
  end
  else
    ret:= false;  {- no printer conncected }
  PaperInfo:= ret;
end;

function PrintOffset(APrn: PPrinter; var pOffset: TPoint): boolean;
{- returns the minimum margin for a printer. }
var
  dc  : HDC;
  k   : integer;
begin
  if aPrn^.status = ps_OK then
  begin
    dc:= Aprn^.GetDC;
    Escape(dc, GETPRINTINGOFFSET, sizeof(tpoint), nil, @pOffset);

    k:= GetDeviceCaps(dc, LOGPIXELSX);
    pOffset.x:= Round(pOffset.x / k * 1440.0);  {- convert to LTwips }

    k:= GetDeviceCaps(dc, LOGPIXELSY);
    pOffset.y:= Round(pOffset.y / k * 1440.0);  {- convert to LTwips }

    DeleteDC(dc);
  end
  else
    FillChar(pOffset, sizeof(pOffset), 0);
end;

function PrintMargins(APrn: PPrinter; var margins: TRect): Boolean;
{- return minimum margins for a printer. The rect structure will
   hold the left, right, top and bottom margins. returns false if printer is
   invalid }
var
  ofset : TPoint;
  sz    : TPoint;
  res   : TPoint;
  dc    : HDC;
  num   : Real;
  idx   : integer;
begin
  FillChar(margins, sizeof(margins), 0);
  if aPrn^.status = ps_OK then
  begin
    PrintOffset(Aprn, ofset);  {- get upper left print offset }
    SelectedPaperSize(aprn, idx, sz);  {- get paper size }
    dc:= APrn^.GetDC;

    {- caclulate printable width in LTwips }
    num:= GetDeviceCaps(dc, HORZRES) / GetDeviceCaps(dc, LOGPIXELSX);
    num:= num * 1440;
    res.x:= Round(num);
    num:= GetDeviceCaps(dc, VERTRES) / GetDeviceCaps(dc, LOGPIXELSY);
    num:= num * 1440;
    res.y:= Round(num);

    margins.left:= ofset.x;
    margins.top:= ofset.y;
    margins.right:= sz.x - margins.left - res.x;
    margins.bottom:= sz.y - margins.top - res.y;
    DeleteDC(dc);
    PrintMargins:= true;
  end
  else
    PrintMargins:= false;
end;

function SelectedPaperSize(APrn: PPrinter; var dmPaperIdx: integer; var size: TPoint): boolean;
{- returns the currently selected paper size in the printer driver in LTwips }
var
  dc  : HDC;
  k   : integer;
begin
  if aPrn^.status = ps_OK then
  begin
    dc:= aprn^.GetDC;
    Escape(dc, GETPHYSPAGESIZE, sizeof(tpoint), nil, @size);

    k:= GetDeviceCaps(dc, LOGPIXELSX);
    size.x:= Round(size.x / k * 1440.0);

    k:= GetDeviceCaps(dc, LOGPIXELSY);
    size.y:= Round(size.y/k*1440.0);

    dmPaperIdx:= APrn^.DevSettings^.dmPaperSize;

    DeleteDC(dc);
  end
  else
  begin
    dmPaperIdx:= -1;
    longint(size):= 0;
    SelectedPaperSize:= false;
  end;
end;

function Orientation(APrn: PPrinter; var orient, relation: integer): boolean;
{- return current printer orientation, DMORIENT_PORTRAIT or DMORIENT_LANDSCAPE
   and relationshiop of portrait to landscape.
   Releation is 90 or 270 degress (see help on DeviceCapabilities (DC_ORIENTATION) }
var
  dvc  : TDevCaps;
begin
  if aPrn^.status = ps_OK then
  begin
    orient:= APrn^.DevSettings^.dmOrientation;
    @dvc:= GetProcAddress(aprn^.DeviceModule, 'DeviceCapabilities');
    if @dvc <> nil then
    begin
      relation:= integer(dvc(aprn^.device, aprn^.port, DC_ORIENTATION, nil, nil));
    end;
    Orientation:= true;
  end
  else
  begin
    orient:= -1;
    relation:= -1;
    Orientation:= false;
  end;
end;

function MaxMinPrintExtents(APrn: PPrinter; var max, min: TPoint): Boolean;
{- return maximum and minimum extents.
   Returns a TPOINT structures containing the maximum and minimum paper
   size that the dmPaperLength and dmPaperWidth members of the printer
   driver's TDEVMODE structure can specify.
   Values returned are in tenths of a millimeter. }
var
  dvc   : TDevCaps;
  k     : Longint;
begin
  MaxMinPrintExtents:= false;
  longint(max):= 0;
  longint(min):= 0;
  if aPrn^.status = ps_OK then
  begin
    @dvc:= GetProcAddress(aprn^.DeviceModule, 'DeviceCapabilities');
    if @dvc <> nil then
    begin
      {- DC_MAX/MINEXTENT return values in tenths of a millimeter }
      k:= dvc(aprn^.device, aprn^.port, DC_MAXEXTENT, nil, nil);
      Move(k, max, sizeof(max));

      k:= dvc(aprn^.device, aprn^.port, DC_MINEXTENT, nil, nil);
      Move(k, min, sizeof(max));
      MaxMinPrintExtents:= true;
    end;
  end
  else
    MaxMinPrintExtents:= false;
end;

{-------------------------------------------------------------[ TPaperInfo ]--}

constructor TPaperInfo.Init(AName: PChar; ASize: TPoint; AnIdx: integer);
begin
  inherited Init;
  if strlen(AName) = 0 then
    name:= StrNew(' ')
  else
    name:= StrNew(AName);
  size:= ASize;
  idx:= anIdx;
end;

destructor TPaperInfo.Done;
begin
  MSStrDispose(name);
  inherited Done;
end;


END.

{- rcf }
