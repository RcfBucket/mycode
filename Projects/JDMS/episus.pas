{----------------------------------------------------------------------------}
{  Module Name  : EPISUS.PAS                                                 }
{  Programmer   : EJ                                                         }
{  Date Created : 07/04/95                                                   }
{  Requirements : ..., S3.23 [ref. c], S3.28 [ref. d]                        }
{                                                                            }
{  Purpose - This unit provides a command-level procedure for generating an  }
{    Antimicrobial Susceptibility Report or an Antimicrobial Susceptibility, }
{    Intermediate, and Restant (SIR) Report. The reports are for             }
{    epidemiology.                                                           }
{                                                                            }
{    The Antimicrobial Susceptibility report provides a cross reference of   }
{    drugs and organisms showing the susceptibility percentages and the      }
{    number of tests run on drug/organism combinations. The Antimicrobial    }
{    SIR report is the same as the Antimicrobial Susceptibility Report but   }
{    it includes Intermediate and Resistant information.                     }
{                                                                            }
{    The isolate database and its associated databases are the source of     }
{    input for the report. This input is reduced by the Multiple Parameter   }
{    Search System (MPSS) (with or withour user defined rules), local rules, }
{    and further reduced by the Eliminate Duplicates Filter (EDFilter).      }
{                                                                            }
{  Example:                                                                  }
{                                                                            }
{                                                                            }
{  98/04/14 15:14           Antimicrobial ... Report           Page 1 of 1   }
{                                                                            }
{  Organism      Total   Ap   Pip  Cf   C/S  Cft  Cz   Caz  Czn  Cpd         }
{  -----------------------------------------------------------------------   }
{  E coli              s 50%  50%            0%   0%                         }
{                    2   2    2              2    1                          }
{                                                                            }
{  Aer sobria          s 0%   100%      100% 100% 0%              100%       }
{                    2   2    2              2    1               1          }
{                                                                            }
{  <----section 1----><---------------------Section 2-------------------->   }
{                                                                            }
{                                                                            }
{  Assumptions -                                                             }
{    a. GetDrugArray returns true only if one or more drugs were stored.     }
{    b. DrugsAcross is such that it will not overflow the report (see        }
{       the DRUG_WIDTH constant below).                                      }
{    c. A mono-spaced font is used for the report.                           }
{    d. Number of isolates per drug is less than 2,000,000 and will fit in   }
{       DRUG_WIDTH as an integer percent and as a count.                     }
{    e. Number of isolates processed is also limited by EDFilter.            }
{                                                                            }
{  Special                                                                   }
{    To maintain the set of unit error numbers, whenever the Assert or       }
{    DoneOnError functions are added, search for Assert and DoneOnError and  }
{    renumber them with unique values.                                       }
{                                                                            }
{  Referenced Documents                                                      }
{    a. Software Specification for Nihongo Data Management System v4.00      }
{    b. Database Design Document, Revision 0                                 }
{    c. S3.23 Design Document, Revision 2                                    }
{    d. S3.28 Design Document, Revision 2                                    }
{                                                                            }
{  Revision History - This project is under version control, use it to view  }
{    revision history.                                                       }
{                                                                            }
{----------------------------------------------------------------------------}

unit EpiSus;

INTERFACE

uses
  OWindows;

procedure EpiAntimicrobicSus( pParent: PWindowsObject; useMPSS,
  includeIR: boolean );


IMPLEMENTATION

uses
  APITools,
  Bits,
  DMSErr,
  DrugSel,
  DBIDs,
  DBLib,
  DBFile,
  DBTypes,
  EDFilter,
  EpiFiles,
  ListLib,
  MPSS,
  MScan,
  Objects,
  PrnPrev,
  ResLib,
  Report,
  Screens,
  Sort_Dlg,
  Sort_Eng,
  Sort_Par,
  Strings,
  StrsW,
  Trays;

{$I PRTEPI.INC}
{$I REPORT.INC}



                        {----------------------}
                        {  Helper Functions    }
                        {----------------------}



{--------------------------------------------------------------------------}
{ This procedure handles assertions for this unit. If an assertion is
{ false, it calls show error with a number and halts. Otherwise, it does
{ nothing.
{--------------------------------------------------------------------------}

procedure Assert( Valid: boolean; UnitErrorNumber: integer );
begin
  if not Valid then
  begin
    ShowError( nil, IDS_EPI_ERR, nil, MOD_EPISUS+1, MOD_EPISUS,
      UnitErrorNumber );
    Halt;
  end;
end;



{--------------------------------------------------------------------------}
{ This function loads a buffer with the name of the organism as listed in
{ the organism data file [ref. b]. If the name is too long, it is clipped.
{ If the organism is not listed, up to 5 asterisks are returned.
{
{ Input
{   Organism - the sequence number of an organism record in the organism
{     data file.
{   pBuffer - pointer to a memory buffer to receive the name
{   MaxLen - the maximum number of characters to copy, must be size of
{     buffer - 1 or less.
{ Output
{   pBuffer^ - contains the organism's short name or a fill if not found.
{
{ Return value: pBuffer
{--------------------------------------------------------------------------}

function OrganismShortNameLCopy( Organism: TSeqNum; pBuffer: PChar;
  MaxLen: Word ): PChar;
var
  List: TListObject;
  ListRec: TMnemListRec;
begin
  {---- Construct list and list record objects }
  List.Init;
  ListRec.Init;

  {---- Copy characters }
  if List.FindMnemSeq( DBORGFile, @ListRec, organism ) then
    StrLCopy( pBuffer, ListRec.desc, MaxLen )
  else
    StrLCopy( pBuffer, '*****', MaxLen );

  {---- Cleanup and return pointer }
  ListRec.Done;
  List.Done;
  OrganismShortNameLCopy := pBuffer;
end;



{--------------------------------------------------------------------------}
{ This function copies a longint to a buffer. If the buffer is too small,
{ the function returns false and nothing is copied. The buffer must be at
{ least SizeOf( Longint ) for the function to succeede.
{
{ Input
{   value - the value to copy
{   pBuffer - pointer to a memory buffer to receive the value
{   SizeOfBuffer - the size of the memory buffer in bytes
{ Output
{   pBuffer^ - if successful, a copy of the value, otherwise unchanged,
{
{ Return value: a true value indicates the operation was successful
{--------------------------------------------------------------------------}

function LongintCopy( value: longint; pBuffer: pointer;
  SizeOfBuffer: Word ): boolean;
type
  PLongint = ^Longint;
var
  ok: boolean;
begin
  ok := SizeOfBuffer >= SizeOf( Longint );
  if ok then
    PLongint( pBuffer )^ := value;
  LongintCopy := ok;
end;



{----------------------------------------------------------------------------}
{ This function LCats the ID ( aka primary abbreviation) of the drug as
{ listed in the drug data file [ref. b] to the buffer. If the drug is not
{ listed, up to 5 asterisks are returned.
{
{ Input
{   Drug - the sequence number of a drug record in the drug data file.
{   pBuffer - pointer to a memory buffer to receive the ID
{   MaxLen - the maximum number of characters to copy, must be size of
{     buffer - 1 or less.
{ Output
{   pBuffer^ - the ID or asterisks.
{
{ Return value: pBuffer
{----------------------------------------------------------------------------}

function DrugIDLCat( Drug: TSeqNum; pBuffer: PChar; MaxLen: Word ): PChar;
var
  List: TListObject;
  ListRec: TMnemListRec;
begin
  {---- Construct list and list record objects }
  List.Init;
  ListRec.Init;

  {---- Copy characters }
  if List.FindMnemSeq( DBDRUGFile, @ListRec, Drug ) then
    StrLCat( pBuffer, ListRec.ID, MaxLen )
  else
    StrLCat( pBuffer, '*****', MaxLen );
  DrugIDLCat := pBuffer;

  {---- Cleanup and return pointer }
  ListRec.Done;
  List.Done;
  DrugIDLCat := pBuffer;
end;



                        {----------------------}
                        {      NCCLS Logic     }
                        {----------------------}



type
  {--------------------------------------------------------------------------}
  { This is the return type for the EvalNCCLS function.
  {--------------------------------------------------------------------------}
  NCCLSEval =
  (
    NCCLS_OTHER,        { do not report }
    NCCLS_S,            { report as Susceptible }
    NCCLS_I,            { report as Intermediate }
    NCCLS_R             { report as Resistant }
  );



{----------------------------------------------------------------------------}
{ This function determines, for the purpose of this report, which
{ interpretations go with which category in the report.
{----------------------------------------------------------------------------}

function EvalNCCLS( intrp: PChar ): NCCLSEval;

  {--------------------------------------------------------------------------}
  { This function returns true if a given string matches the interpretation.
  {--------------------------------------------------------------------------}
  function is( i: PChar ): boolean;
  begin
     is := StrComp( intrp, i ) = 0;
  end;

begin { EvalNCCLS }

  if is( 'S' ) or is( 'IB' ) or is('MS') then
    EvalNCCLS := NCCLS_S
  else if is( 'R' ) or is( 'BLac' ) or is('ESBL') or is('EBL?') or is('R*') or is('MRSA') then
    EvalNCCLS := NCCLS_R
  else if is( 'I' ) then
    EvalNCCLS := NCCLS_I
  else
    EvalNCCLS := NCCLS_OTHER;

end;



                        {----------------------}
                        {    KeyInformation    }
                        {----------------------}



type
  {--------------------------------------------------------------------------}
  { This type enumerates the set of keys that can be used with the SIR sort
  { engine. If this set is changed in any way, the code in this file must be
  { updated.
  {--------------------------------------------------------------------------}
  KeyNumbers =
  (
    KN_ORGANISM_SNAME,
    KN_TOTAL
  );


const
  {--------------------------------------------------------------------------}
  { The following constants specifies the numner of keys in the set above.
  { These values must be updated if the set size changes.
  {--------------------------------------------------------------------------}
  NUMBER_OF_KEYS = 2;
  MAXIMUM_KEY_NUMBER = Ord( KN_TOTAL );


type
  {--------------------------------------------------------------------------}
  { This object provides names for sort keys in group 3.
  {--------------------------------------------------------------------------}
  PGroup3KeyInformation = ^TGroup3KeyInformation;
  TGroup3KeyInformation = object( TKeyInformation )
    function GetKeyName( KeyNumber: integer; KeyName: PChar; Size: integer ):
      PChar; virtual;
    function NumberOfKeys: integer; virtual;
  end;



{----------------------------------------------------------------------------}
{ This function gets the name of a key given its number.
{----------------------------------------------------------------------------}

function TGroup3KeyInformation.GetKeyName( KeyNumber: integer;
  KeyName: PChar; Size: integer ): PChar;
begin
  case KeyNumbers( KeyNumber ) of
    KN_ORGANISM_SNAME :
      GetKeyName := SR( IDS_ORGANISM_SNAME, KeyName, Size );
    KN_TOTAL :
      GetKeyName := SR( IDS_TOTAL, KeyName, Size );
  else
    Assert( false, 1 );
  end;
end;



{----------------------------------------------------------------------------}
{ This function returns the number of keys in group 3.
{----------------------------------------------------------------------------}

function TGroup3KeyInformation.NumberOfKeys: integer;
begin
  NumberOfKeys := NUMBER_OF_KEYS;
end;



                        {----------------------}
                        {    TSIRParagraph     }
                        {----------------------}



const
  {--------------------------------------------------------------------------}
  { Set the width of the report and allocate some to section 1 and the
  { rest to section 2. All units are in character columns. The report
  { width must be acceptable to the TReport.Init constructor.
  {--------------------------------------------------------------------------}
  REPORT_WIDTH = 120;
  S1_WIDTH = 32;
  S2_WIDTH = REPORT_WIDTH - S1_WIDTH;

  {--------------------------------------------------------------------------}
  { Drugs in section 2 are each given the following width, in character
  { columns. This value must be small enough to fit "DrugsAcross" number of
  { drugs but at least 5 to fit '100% '.
  {--------------------------------------------------------------------------}
  DRUG_WIDTH = 5;

  {--------------------------------------------------------------------------}
  { Interpretation code field width, in character columns. The value of this
  { constant must be at least 3 to fit a single character code with a blank
  { on each side.
  {--------------------------------------------------------------------------}
  INTER_WIDTH = 3;

type
  {--------------------------------------------------------------------------}
  { Organism/durg information required to generate a report.
  {--------------------------------------------------------------------------}
  ODInfoElement = ( ODI_T, ODI_S, ODI_I, ODI_R );
  ODInfo = record
  case integer of
  0: (
       Isolates,        { number of isolates with this organism and drug. }
       SCount,          { of them, number that were sus. to the drug. }
       ICount,          { of them, number that were int. }
       RCount: longint; { of them, number that were resistant to the drug. }
     );
  1: (
       element: array [ ODI_T..ODI_R ] of longint;
     )
  end;


  {--------------------------------------------------------------------------}
  { This object represents a paragraph of the report. It holds the paragrpah
  { data and provides a method to format it into a paragraph of text.
  {--------------------------------------------------------------------------}
  PSIRParagraph = ^TSIRParagraph;
  TSIRParagraph = object( TObject )
    Organism: TSeqNum;        { the organism record's sequence number }
    Isolates: longint;        { number of tested isolates with this organism }
    info: array [ 1..DrugsAcross ] of ODInfo;
    constructor Init( newOrganism: TSeqNum; TotalCount: longint );
    function GetOrg: TSeqNum;
    procedure AddDrug( drugColumn: word; DrugInfo: ODInfo );
    procedure Print(
      IREnable: boolean; printStr: PChar; prnInfo: PPrintInfo );
  end;



{--------------------------------------------------------------------------}
{ The constructor initializes the object with an organism and tested count
{ and clears the drug columns.
{--------------------------------------------------------------------------}

constructor TSIRParagraph.Init( newOrganism: TSeqNum; TotalCount: longint );
var
  i: integer;
begin
  organism := newOrganism;
  Isolates := TotalCount;
  for i := 1 to DrugsAcross do
  begin
    with info[ i ] do
    begin
      Isolates := 0;
      SCount := 0;
      ICount := 0;
      RCount := 0;
    end;
  end;
end;



{--------------------------------------------------------------------------}
{ This function returns the organism reference in the data.
{--------------------------------------------------------------------------}

function TSIRParagraph.GetOrg: TSeqNum;
begin
  GetOrg := organism;
end;



{--------------------------------------------------------------------------}
{ This procedure places information into the specified drug column. If
{ data was added to the column since the last reset, the function
{ will halt the program.
{--------------------------------------------------------------------------}

procedure TSIRParagraph.AddDrug( drugColumn: word; DrugInfo: ODInfo );
begin
  info[ drugColumn ] := DrugInfo;
end;



{--------------------------------------------------------------------------}
{ This procedure prints the organism data. It prints a group consisting
{ of 3 lines. The first line is blank, it's used to separate groups on
{ the report.
{--------------------------------------------------------------------------}

procedure TSIRParagraph.Print( IREnable: boolean; printStr: PChar;
  prnInfo: PPrintInfo );
var
  S1Buff: array[ 0..S1_WIDTH ] of char;
  S2Buff: array[ 0..S2_WIDTH ] of char;

  {------------------------------------------------------------------------}
  { This function formats a blank section 1 buffer and returns a pointer to
  { the buffer.
  {------------------------------------------------------------------------}
  function S1Blank: PChar;
  begin
    S1Buff[ 0 ] := #0;
    Pad( S1Buff, S1Buff, ' ', S1_WIDTH );
    S1Blank := S1Buff;
    { Assert: StrLen( S1Buff ) = S1_WIDTH }
  end;

  {------------------------------------------------------------------------}
  { This function formats the organism's name into the section 1 buffer
  { and returns a pointer to the buffer.
  {------------------------------------------------------------------------}
  function S1OrgName: PChar;
  begin
    OrganismShortNameLCopy( organism, S1Buff, S1_WIDTH );
    Pad( S1Buff, S1Buff, ' ', S1_WIDTH );
    S1OrgName := S1Buff;
    { Assert: StrLen( S1Buff ) = S1_WIDTH }
  end;

  {------------------------------------------------------------------------}
  { This function formats the organism's total count into a section 1
  { buffer and returns a pointer to the buffer.
  {------------------------------------------------------------------------}
  function S1Total: PChar;
  var
    totalText: array[ 0..15 ] of char;
  begin
    S1Buff[ 0 ] := #0;
    Str( Isolates, totalText );
    Pad( S1Buff, S1Buff, ' ', S1_WIDTH - StrLen( totalText ) );
    StrLCat( S1Buff, totalText, S1_WIDTH );
    S1Total := S1Buff;
    { Assert: StrLen( S1Buff ) = S1_WIDTH }
  end;

  {------------------------------------------------------------------------}
  { This procedure copies the interpretation field into the buffer. The
  { field is blanked if the show flag is false. The procedure copies a
  { space, the parameter name, and one or more spaces into the buffer. The
  { name is clipped if needed. The buffer will be INTER_WIDTH in length.
  {------------------------------------------------------------------------}
  procedure CopyIntrField( buffer: PChar; parameter: ODInfoElement;
    show: boolean );
  const
    {--------------------------------------------------------------------------}
    { This is the text to be printed in the INTER_WIDTH field for each element
    { type.
    {--------------------------------------------------------------------------}
    ODInfoElementName: array [ ODI_T..ODI_R ] of array[ 0..3 ] of
      char = ('','Sus','Int','Res');
  begin
    { Assert: INTER_WIDTH >= 3 }
    StrCopy( buffer, ' ' );
    if show then
      StrLCat( buffer, ODInfoElementName[ parameter ], INTER_WIDTH-1 );
    Pad( buffer, buffer, ' ', INTER_WIDTH );
    { Assert: StrLen( buffer ) = INTER_WIDTH }
  end;

  {------------------------------------------------------------------------}
  { This function formats section 2 as percents of the specified parameter
  { into S2Buff. The percent is rounded to the nearest whole number.
  {------------------------------------------------------------------------}
  function S2Percent( parameter: ODInfoElement ): PChar;
  var
    percentVal: integer;
    percentText: array[ 0..15 ] of char;
    i: integer;
  begin
    CopyIntrField( S2Buff, parameter, IREnable );
    for i := 1 to DrugsAcross do with info[ i ] do
    begin
      if Isolates > 0 then
      begin
        percentVal := (( element[ parameter ] * 100 * 10 )
          div Isolates + 5) div 10;
        Str( percentVal, percentText );
        StrCat( percentText, '%');
        Pad( percentText, percentText, ' ', DRUG_WIDTH );
        StrLCat( S2Buff, percentText, S2_WIDTH );
      end
      else
        Pad( S2Buff, S2Buff, ' ', StrLen( S2Buff ) + DRUG_WIDTH );
    end;
    S2Percent := S2Buff;
  end;

  {------------------------------------------------------------------------}
  { This function formats section 2 as counts of the specified parameter
  { into S2Buff.
  {------------------------------------------------------------------------}
  function S2Count( parameter:ODInfoElement ): PChar;
  var
     countText: array[ 0..15 ] of char;
     i: integer;
  begin
    CopyIntrField( S2Buff, parameter, false );
    for i := 1 to DrugsAcross do
    begin
      if info[ i ].Isolates > 0 then
      begin
        Str( info[ i ].element[ parameter ], countText );
        Pad( countText, countText, ' ', DRUG_WIDTH );
        StrLCat( S2Buff, countText, S2_WIDTH );
      end
      else
        Pad( S2Buff, S2Buff, ' ', StrLen( S2Buff ) + DRUG_WIDTH );
    end;
    S2Count := S2Buff;
  end;

  {------------------------------------------------------------------------}
  { This procedure combines the sections into a line and adds them to the
  { printing.
  {------------------------------------------------------------------------}
  procedure PrintLine( section1, section2: PChar; aflags: longint );
  begin
    StrCat( StrCopy( printStr, section1 ), section2 );
    prnInfo^.body^.AddRow( aflags, 0);
    prnInfo^.body^.AddCell( printStr, LT500, c_Stretch, 0 );
  end;

begin { Print }

  PrintLine( '', '', r_StartGroup );
  PrintLine( S1OrgName, S2Percent( ODI_S ), r_Normal );
  if IREnable then
  begin
    PrintLine( S1Blank, S2Count(   ODI_S ), r_Normal );
    PrintLine( S1Blank, S2Percent( ODI_I ), r_Normal );
    PrintLine( S1Blank, S2Count(   ODI_I ), r_Normal );
    PrintLine( S1Blank, S2Percent( ODI_R ), r_Normal );
    PrintLine( S1Blank, S2Count(   ODI_R ), r_Normal );
  end;
  PrintLine( S1Total, S2Count( ODI_T ), r_EndGroup );

end;




                        {----------------------}
                        {    SIR SortEngine    }
                        {----------------------}



type
  {--------------------------------------------------------------------------}
  { This object provides a sort engine for the reports in this unit. See base
  { class description. This class defines a number of specimen keys and
  { methods to access them
  {--------------------------------------------------------------------------}
  PSIRSortEngine = ^SIRSortEngine;
  SIRSortEngine = object( SortEngine )
    constructor Init( input_Parameters: SortEngineParameters );
    procedure Input( index: integer; input_pParagraph: PSIRParagraph );
    function GetItem( KeyNumber: integer; pItem: Pointer;
      MaxSize: Word ): boolean; virtual;
    function DataTypeOf( KeyNumber: integer ): SEKeyType; virtual;

    private
    pParagraph: PSIRParagraph;
  end;



{----------------------------------------------------------------------------}
{ This constructor initializes the base class (see SortEngine) and clears
{ the record pointers to prevent misuse.
{----------------------------------------------------------------------------}

constructor SIRSortEngine.Init( input_Parameters: SortEngineParameters );
const
  {--------------------------------------------------------------------------}
  { Looking at ref. b, the longest expected field is the organism's long name
  { at 65 bytes, thus, the following buffer size should hold any data set.
  {--------------------------------------------------------------------------}
  BUFFER_SIZE = SE_MAXLEVELS * 65;

begin
  inherited Init( input_Parameters, BUFFER_SIZE );
  pParagraph := nil;
end;



{----------------------------------------------------------------------------}
{ This function indicates the data type for each defined key.
{----------------------------------------------------------------------------}

function SIRSortEngine.DataTypeOf( KeyNumber: integer ): SEKeyType;
begin
  case KeyNumbers( KeyNumber ) of
    KN_ORGANISM_SNAME :
      DataTypeOf := SEKT_ZSTRING;
    KN_TOTAL :
      DataTypeOf := SEKT_LONGINT;
  else
    Assert( false, 2 );
  end;
end;



{----------------------------------------------------------------------------}
{ See base class description. This override sets up record pointers
{ needed by GetItem and uses the specimen record's sequence number for the
{ sort index.
{----------------------------------------------------------------------------}

procedure SIRSortEngine.Input( index: integer;
  input_pParagraph: PSIRParagraph );
begin
  pParagraph := input_pParagraph;
  inherited Input( index );
  pParagraph := nil;
end;



{----------------------------------------------------------------------------}
{ See base class description. This override provides access to key
{ fields associated with a SIR report.
{----------------------------------------------------------------------------}

function SIRSortEngine.GetItem( KeyNumber: integer; pItem: Pointer;
  MaxSize: Word ): boolean;
begin

  {---- Validate inputs and set default return value }
  Assert( MaxSize > 0, 3 );
  Assert( pParagraph <> nil, 4 );
  GetItem := false;

  {---- Get item based on key number }
  case KeyNumbers( KeyNumber ) of

    KN_ORGANISM_SNAME :
    begin
      OrganismShortNameLCopy( pParagraph^.organism, pItem, MaxSize - 1 );
      GetItem := true;
    end;

    KN_TOTAL :
      GetItem := LongintCopy( pParagraph^.Isolates, pItem, MaxSize );
  end;

end;



                        {----------------------}
                        {     TEpiSusReport    }
                        {----------------------}



type
  {--------------------------------------------------------------------------}
  { This is the epidemiology susceptibility and SIR report generator object.
  {--------------------------------------------------------------------------}
  PEpiSusReport = ^TEpiSusReport;
  TEpiSusReport = object(TReport)

    { Interface }
    constructor Init( aParent: PWindowsObject; Title: PChar;
      aDrugs: TDrugArray; useMPSS, includeIR: boolean;
      SortParameters: SortEngineParameters );
    procedure Generate;
    destructor Done; virtual;

    { Implementation }
    private
    mpssObj: PMPSS;            { Multiple Parameter Search (MPSS) }
    isoDB,                     { Isolate database }
    epi4DB,                    { Temp. epi. organism/drug database }
    epi3DB: PDBFile;           { Temp. epi. organism database }
    drugsSelected: TDrugArray; { List of drugs selected for report }
    pctDone: integer;          { percent done for progress indicator }
    IREnable: boolean;         { Enable I and R data reporting }
    pSorter: PSIRSortEngine;   { nil if sorting not requested }
    procedure BuildTempFiles;
    procedure InitHeadings; virtual;
    procedure PrintReportBody;

  end;



{----------------------------------------------------------------------------}
{ This constructor places the object in a defined initial state. It also has
{ the user select MPSS rules, if enabled. If construction is successful, the
{ next step should be a call to InitHeadings.
{----------------------------------------------------------------------------}

constructor TEpiSusReport.Init( aParent: PWindowsObject; Title: PChar;
  aDrugs: TDrugArray; useMPSS, includeIR: boolean;
  SortParameters: SortEngineParameters );

  {--------------------------------------------------------------------------}
  { This function shows an error and calls the destructor if the test value
  { is true. It passes the test value through to facilitate calling Fail.
  {--------------------------------------------------------------------------}
  function DoneOnError( Error: boolean; UnitErrorNumber: integer ): boolean;
  begin
    if Error then
    begin
      ShowError( aParent, IDS_EPI_ERR, nil, MOD_EPISUS+1, MOD_EPISUS,
        UnitErrorNumber );
      Done;
    end;
    DoneOnError := Error;
  end;

var
  idx: word;
  code: integer;

begin { Init }

  {---- Place object in a fail-safe state and load attributes. }
  mpssObj := nil;
  isoDB := nil;
  epi4DB := nil;
  epi3DB := nil;
  pSorter := nil;

  { Set attributes }
  drugsSelected := aDrugs;
  IREnable := includeIR;
  Parent := aParent;

  {---- Construct a report with title, column width, and no maximum lines. }
  if not inherited Init( aParent, Title, REPORT_WIDTH, 0 ) then
    Fail;

  {---- Arm the base class abort flag during the following initialization.
        If none fail, it will be disarmed at the end of this constructor. }
  abortPrinting := TRUE;

  {---- Optionally create sort engine }
  if SortParameters.NumberOfLevels > 0 then
  begin
    pSorter := New( PSIRSortEngine, Init( SortParameters ) );
    if DoneOnError( pSorter = nil, 50 ) then
      Fail;
  end;

  {---- Create an isolate database object. }
  isoDB := New( PDBFile, Init( DBISOFile, '', dbOpenNormal ) );
  if DoneOnError( isoDB = nil, 51 ) then
    Fail;

  {---- Create a Multiple Parameter Search (MPSS) object driven by the
        isolate database. }
  mpssObj := New(PMPSS, Init( aParent, isoDB, IncludeUndefDates ) );
  if DoneOnError( mpssObj = nil, 52 ) then
    Fail;

  {---- If MPSS rules were specified, permit the user to select some or
        cancel. }
  if useMPSS and not mpssObj^.MPSSLoadRules( aParent, title ) then
  begin
    Done;
    Fail;
  end;

  {---- Create a temporary epi. organism/drug database. Then, create an object
        for the database. }
  if DoneOnError( not CreateEPITMP4, 53 ) then
    Fail;
  epi4DB := New( PDBFile, Init( 'EPITMP4', '', dbOpenExclusive ) );
  if DoneOnError( epi4DB = nil, 54 ) then
    Fail;

  {---- Create a temporary epi. organism database. Then, create an
        object for the database. }
  if DoneOnError( not CreateEPITMP3, 55 ) then
    Fail;
  epi3DB := New( PDBFile, Init( 'EPITMP3', '', dbOpenExclusive ) );
  if DoneOnError( epi3DB = nil, 56 ) then
    Fail;

  {---- Construction complete, disarm the abort control. }
  abortPrinting := FALSE;

end;



{----------------------------------------------------------------------------}
{ This destructor disposes memory allocated to the object.
{----------------------------------------------------------------------------}

destructor TEpiSusReport.Done;
begin
  MSDisposeObj( pSorter );
  MSDisposeObj( epi3DB );
  MSDisposeObj( epi4DB );
  MSDisposeObj( mpssObj );
  MSDisposeObj( isoDB );
  inherited Done;
end;



{----------------------------------------------------------------------------}
{ This function sets up the report headings. It should be called after the
{ constructor. The next step should be a call AllowCancel (see base class)
{ and then call BuildTempFiles.
{----------------------------------------------------------------------------}

procedure TEpiSusReport.InitHeadings;
var
  PadLength,
  idx: word;
  lbl: array [ 0..80 ] of char;

begin { InitHeadings }

  inherited InitHeadings;

  {---- Initialize section 1 of heading with the word for "Organism" left
        justified and the  word for "Total" right justified. }
  SR( IDS_ORGANISM_LBL, printStr, S1_WIDTH );
  SR( IDS_TOTAL_LBL, lbl, S1_WIDTH );
  Pad( printStr, printStr, ' ', S1_WIDTH - StrLen( lbl ) );
  StrLCat( printStr, lbl, printStrLen );

  {---- Initialize section 2 of heading with the interpretation code field
        followed by the drug fields. Drugs that would exceed the right margin
        are omitted without warning. Fields for unknown drug codes are filled
        with asterisks. }
  PadLength := S1_WIDTH + INTER_WIDTH;
  Pad( printStr, printStr, ' ', PadLength );
  for idx := 1 to DrugsAcross do
  begin
    if drugsSelected[ idx ] = 0 then
      Break;
    Inc( PadLength, DRUG_WIDTH );
    DrugIDLCat( drugsSelected[ idx ], printStr, printStrLen );
    Pad( printStr, printStr, ' ', PadLength );
  end;

  {---- Build header object. }
  prnInfo^.header^.AddRow( r_Normal {flags}, 0 {wrapMarg} );
  prnInfo^.header^.AddCell( printStr, LT500 {width},
    c_Stretch or c_BorderBottom {flags}, 0 {PtSize} );
  printStr^ := #0;

end;



{----------------------------------------------------------------------------}
{ This function generates the body of the report. It should be called after
{ BuildTempFiles. The destructor should be called next.
{----------------------------------------------------------------------------}

procedure TEpiSusReport.PrintReportBody;

  {------------------------------------------------------------------------}
  { This procedure directs a paragraph to print itself.
  {------------------------------------------------------------------------}
  procedure PrintParagraph( p: PSIRParagraph ); far;
  begin
    p^.Print( IREnable, printStr, prnInfo );
  end;

  {------------------------------------------------------------------------}
  { This function returns the organism's total count stored in the temp.
  { epi. organism database.
  {------------------------------------------------------------------------}
  function OrganismTotalCount( Organism: TSeqNum ): longint;
  var
    count: longint;
  begin

    {---- Find the current organism in the temp. epi. organism database and
          get the count, otherwise, count is zero. }
    epi3DB^.dbr^.PutField( DBEPI3Org, @Organism );
    if epi3DB^.dbc^.GetEQ( epi3DB^.dbr ) then
    begin
      epi3DB^.dbr^.GetField( DBEPI3Count, @count, SizeOf( count ) );
      OrganismTotalCount := count;
    end
    else
      OrganismTotalCount := 0;
  end;

var
  initialOrg,
  orgSeq,
  drugSeq: TSeqNum;
  drugColumn: word;
  tPct: integer;
  i,
  count,
  total,
  rCnt: longint;
  pParagraph: PSIRParagraph;
  info: ODInfo;
  lbl: array [ 0..80 ] of char;
  ParagraphList: TCollection;

begin { PrintReportBody }

  { Assert: same-organism records in the temp. epi. organism/drug database are
            contiguous and only one record exists per organism/drug pair. }

  {---- Access initial record in the temp. epi. organism/drug database. Exit
        function if no records exist. }
  if not epi4DB^.dbc^.GetFirst( epi4DB^.dbr ) then
    Exit;

  {---- Transition the abort dialog box from the creating state to the
        formatting state. }
  SR( IDS_FORMATING_RPT, lbl, 80 );
  cancelDlg^.SetText( lbl, 0 );
  pctDone := 0;
  cancelDlg^.SetText( '0%', IDC_LBL2 );
  total := epi4DB^.dbc^.NumRecords;
  rCnt := 0;

  {---- Initialize the current organism from the initial record and reset its
        data. Then, process all records in the database. Check for a pending
        user abort after processing each record. }
  epi4DB^.dbr^.GetField( DBEPI4Org, @initialOrg, SizeOf( TSeqNum ) );
  pParagraph := New( PSIRParagraph, Init( initialOrg,
    OrganismTotalCount( initialOrg ) ) );
  ParagraphList.Init( 100, 100 );
  repeat

    { Step 1: Compute progress and update dialog box when required. }
    tPct:= pctDone;
    Inc(rCnt);
    pctDone := (rCnt * 100) div total;
    if tPct <> pctDone then
    begin
      Str(pctDone, lbl);
      StrCat(lbl, '%');
      cancelDlg^.SetText(lbl, IDC_LBL2);
    end;
    List('');  { uses side effect of List to update userAbort }

    { Step 2: Read the organism and drug fields. If the organism changed,
              print/reset organism information and update current organism. }
    epi4DB^.dbr^.GetField(DBEPI4Org, @orgSeq, SizeOf(TSeqNum));
    epi4DB^.dbr^.GetField(DBEPI4Drug, @drugSeq, SizeOf(TSeqNum));
    if orgSeq <> pParagraph^.GetOrg then
    begin
      ParagraphList.Insert( pParagraph );
      pParagraph := New( PSIRParagraph, Init( orgSeq,
        OrganismTotalCount( orgSeq ) ) );
    end;

    { Step 3: Find the (left-most occurrence of the) drug in the report that
              matches the current drug. If none match, go to the next record.}
    drugColumn := 0;
    for count := 1 to DrugsAcross do
    begin
      if drugsSelected[ count ] = 0 then
        Break;
      if drugsSelected[ count ] = drugSeq then
        drugColumn := count;
    end;
    if drugColumn = 0 then
      Continue;

    { Step 4: Add the drug information to the organism data. }
    with epi4DB^.dbr^ do
    begin
      GetField( DBEPI4Count,  @info.Isolates, SizeOf(info.Isolates) );
      GetField( DBEPI4SusCnt, @info.SCount, SizeOf(info.SCount) );
      GetField( DBEPI4IntCnt, @info.ICount, SizeOf(info.ICount) );
      GetField( DBEPI4ResCnt, @info.RCount, SizeOf(info.RCount) );
    end;
    pParagraph^.AddDrug( drugColumn, info );

  until ( not epi4DB^.dbc^.GetNext( epi4DB^.dbr ) ) or userAbort;
  ParagraphList.Insert( pParagraph );

  {---- If user has not aborted, insert final organism information and print
        paragraphs. }
  if not userAbort then
  begin
    if pSorter <> nil then
    begin
      {---- Sorted Printing }
      for i := 0 to ParagraphList.Count - 1 do
        pSorter^.Input( i, ParagraphList.At( i ) );
      pSorter^.Sort;
      while pSorter^.Output( i )  do
        PrintParagraph( ParagraphList.At( i ) );
    end
    else
      {---- Unsorted printing }
      ParagraphList.ForEach( @PrintParagraph )
  end;

  {---- Cleanup }
  ParagraphList.Done;
end;



{----------------------------------------------------------------------------}
{ This private function compiles the databases needed by PrintReport. This
{ procedure should be called after the call to AllowCancel. The PrintReport
{ procedure should be called next.
{----------------------------------------------------------------------------}

procedure TEpiSusReport.BuildTempFiles;

type

  {--------------------------------------------------------------------------}
  { This type holds applicable fields from an isolate record.
  {--------------------------------------------------------------------------}
  TIsolate = record
    Organism: TSeqNum;
    QCFlag: boolean;
  end;


  {--------------------------------------------------------------------------}
  { This function qualifies an isolate variable for the report. It rejects
  { isolates that are invalid for this report or have nothing to contribute
  { to the report. Currently, it rejects records with one or more of the
  { following conditions:
  {
  {   a.  Its QC flag is set (true)
  {   b.  Its organism reference is null (0)
  {
  {--------------------------------------------------------------------------}
  function isQualifiedIsolate( isolate: TIsolate ): boolean;

    {------------------------------------------------------------------------}
    { This function rejects null references.
    {------------------------------------------------------------------------}
    function valid( num: TSeqNum ): boolean;
    begin
      valid := num <> 0;
    end;

  begin { isQualifiedIsolate }
    with isolate do
    begin
      isQualifiedIsolate := not QCFlag and valid( Organism );
    end
  end;


  {--------------------------------------------------------------------------}
  { This procedure loads an isolate variable from an isolate record.
  {--------------------------------------------------------------------------}
  procedure LoadIsolate( IsolateRec: PDBRec; var isolate: TIsolate );

    {------------------------------------------------------------------------}
    { This function reads the QC flag from the isolate record.
    {------------------------------------------------------------------------}
    function getQCFlag: boolean;
    var
      QCFlag: boolean;
    begin
      IsolateRec^.GetField( DBISOQCFlag, @QCFlag, SizeOf( QCFlag ) );
      getQCFlag := QCFlag;
    end;

    {------------------------------------------------------------------------}
    { This function reads the organism reference from the isolate record.
    {------------------------------------------------------------------------}
    function getOrgSeq: TSeqNum;
    var
      OrgSeq: TSeqNum;
    begin
      IsolateRec^.GetField( DBISOOrgRef, @OrgSeq, SizeOf( OrgSeq ) );
      getOrgSeq := OrgSeq;
    end;

  begin { LoadIsolate }
    with isolate do
    begin
      QCFlag := getQCFlag;
      Organism := getOrgSeq;
    end
  end;

  {--------------------------------------------------------------------------}
  { This function qualifies the isolate's results for use in the report.
  { Currently, the simple ability to load results qualifies the results.
  {--------------------------------------------------------------------------}
  function QualifyResults( IsolateRec: PDBRec; results: PResults )
    : boolean;
  begin
    QualifyResults := results^.CanLoad( IsolateRec );
  end;

  {--------------------------------------------------------------------------}
  { This function loads the results for the isolate and qualifies them for
  { use in the report. Currently, the simple ability to load results
  { qualifies the results.
  {
  { Note: this function assumes the results qualified using QualifyResults.
  {--------------------------------------------------------------------------}
  procedure LoadResults( IsolateRec: PDBRec; results: PResults );
  var
    ok: boolean;
  begin
    ok := results^.LoadResults( IsolateRec );
    Assert( ok, 5 );
  end;

  {--------------------------------------------------------------------------}
  { This procedure adds data from the isolate and its results to the report
  { by updating the temp files.
  {--------------------------------------------------------------------------}
  procedure addToReport( isolate: TIsolate; results: PResults );

    {------------------------------------------------------------------------}
    { This procedure increments the count for the given organism in the
    { temporary epi. organism database, adding a record if necessary.
    {------------------------------------------------------------------------}
    procedure TallyOrg( OrgSeq: TSeqNum );
    var
      count: longint;
    begin
      { Step 5: Find/create the organism's record in the temporary epi.
                organism database and increment/set the count field. }
      epi3DB^.dbr^.PutField( DBEPI3Org, @OrgSeq );
      if epi3DB^.dbc^.GetEQ( epi3DB^.dbr ) then
      begin
        { Found record - increment count }
        epi3DB^.dbr^.GetField( DBEPI3Count, @count, SizeOf( count ) );
        Inc( count );
        epi3DB^.dbr^.PutField( DBEPI3Count, @count );
        epi3DB^.dbc^.UpdateRec( epi3DB^.dbr );
      end
      else
      begin
        { Create record - initialize count }
        count := 1;
        epi3DB^.dbr^.PutField( DBEPI3Count, @count );
        epi3DB^.dbc^.InsertRec( epi3DB^.dbr );
      end;
    end;


    {------------------------------------------------------------------------}
    { Set the organism field in the temp. epi. organism/drug database record.
    {------------------------------------------------------------------------}
    procedure BeginOrgDrugTally( OrgSeq: TSeqNum );
    begin
      epi4DB^.dbr^.PutField( DBEPI4Org, @OrgSeq );
    end;

    {------------------------------------------------------------------------}
    { This procedure increments the SIR and total counts for an organism/drug
    { pair in the temporary epi. organism/drug database.
    {
    { Assert: the organism was specified with the BeginOrgDrugTally procedure.
    {------------------------------------------------------------------------}
    procedure TallyOrgDrug( DrugSeq: TSeqNum; Eval: NCCLSEval );
    var
      count: longint;
    begin

      {---- Find/create the organism/drug record in the database and
            increment/set the count fields.

            Assert: The DBEPI4Org field is set to the organism.
      }
      epi4DB^.dbr^.PutField( DBEPI4Drug, @drugSeq );
      if epi4DB^.dbc^.GetEQ( epi4DB^.dbr ) then
      begin
        { Found record - increment applicable counts }
        epi4DB^.dbr^.GetField( DBEPI4Count, @count, SizeOf( count ) );
        Inc(count);
        epi4DB^.dbr^.PutField( DBEPI4Count, @count );
        with epi4DB^.dbr^ do case Eval of

        NCCLS_S:
          begin
            GetField( DBEPI4SusCnt, @count, SizeOf( count ) );
            Inc(count);
            PutField( DBEPI4SusCnt, @count );
          end;

        NCCLS_I:
          begin
            GetField( DBEPI4IntCnt, @count, SizeOf( count ) );
            Inc( count );
            PutField( DBEPI4IntCnt, @count );
          end;

        NCCLS_R:
          begin
            GetField( DBEPI4ResCnt, @count, SizeOf( count ) );
            Inc( count );
            PutField( DBEPI4ResCnt, @count );
          end;

        end;
        epi4DB^.dbc^.UpdateRec( epi4DB^.dbr );
      end
      else
      begin
        { Create record - initialize all counts as applicable }
        count := 0;
        epi4DB^.dbr^.PutField( DBEPI4SusCnt, @count );
        epi4DB^.dbr^.PutField( DBEPI4IntCnt, @count );
        epi4DB^.dbr^.PutField( DBEPI4ResCnt, @count );
        count := 1;
        epi4DB^.dbr^.PutField( DBEPI4Count, @count );
        with epi4DB^.dbr^ do case Eval of
          NCCLS_S: PutField( DBEPI4SusCnt, @count );
          NCCLS_I: PutField( DBEPI4IntCnt, @count );
          NCCLS_R: PutField( DBEPI4ResCnt, @count );
        end;
        epi4DB^.dbc^.InsertRec( epi4DB^.dbr );
      end;

    end;

    {------------------------------------------------------------------------}
    { This function accesses an isolate order test result record
    {------------------------------------------------------------------------}
    function LoadRecord( recordNumber: word; results: PResults ): PResRecObj;
    begin
      LoadRecord := results^.At( recordNumber );
    end;

    {------------------------------------------------------------------------}
    { This function qualifies the result for the report, and if qualified,
    { returns its NCCLS evaluation as defined in the NCCLSEval function.
    {   a.   Skip the record if it is not NCCLS.
    {   b.   Skip the record if its NCCLS result type is not needed.
    {   c.   Skip the record if the REScontraIndicated bit is set.
    {   d.   Skip the record if the drug will not be in the report.
    {------------------------------------------------------------------------}
    function isQualifiedGetEval( resRec: PResRecObj;
                                 var Eval: NCCLSEval; FilterMult: Boolean;
                                 var TrayDBObj: PTraysObject;
                                 var TestDB: PDBFile) :
      boolean;

      {----------------------------------------------------------------------}
      { This function tests the result see if it is NCCLS
      {----------------------------------------------------------------------}
      function isNCCLS( pRecord: PResRecObj ): boolean;
      begin
        isNCCLS := StrComp( pRecord^.testCatID, 'NCCLS' ) = 0;
      end;

      {--------------------------------------------------------------------------}
      { This procedure maps a result object to its 'master' drug, at the very    }
      { DrugRef Level.  TrayDBOj performs the abbreviation mapping.  TestDB is   }
      { used to resolve the target drug's DrugRef.                               }
      {--------------------------------------------------------------------------}
      procedure MapResToMaster;
      var pStr: array [0..15] of char;
      begin
        if TrayDBObj^.MapDrgAbbr(ResRec^.TestID,pStr) then
        begin
          TestDB^.dbr^.PutField(DBTSTID, @pStr);
          TestDB^.dbr^.PutField(DBTSTTstCat, @ResRec^.TestCatID);
          TestDB^.dbc^.GetEQ(TestDB^.dbr);
          TestDB^.dbr^.GetField(DBTSTDrugRef, @ResRec^.drugSeq, SizeOf(ResRec^.drugSeq));
          StrCopy(ResRec^.TestID,pStr);
        end;
      end;

      {----------------------------------------------------------------------}
      { This function returns true if the drug is in the report.
      {----------------------------------------------------------------------}
      function drugListed( drugSeq: TSeqNum ): boolean;
      var
        drugIdx: integer;
        count: longint;
      begin
        drugIdx := 0;
        for count := 1 to DrugsAcross do
        begin
          if drugsSelected[ count ] = 0 then
            Break;
          if drugsSelected[ count ] = drugSeq then
            drugIdx := count;
        end;
        drugListed := drugIdx <> 0;
      end;

    begin { isQualifiedGetEval }
      isQualifiedGetEval:=False;
      if isNCCLS( resRec ) then
      begin
        Eval := EvalNCCLS( resRec^.result );
        if ( Eval <> NCCLS_OTHER ) and not TestBit( resRec^.flag, REScontraIndicated )then
        begin
          {Remap this drug at the DrugSeq level}
          If FilterMult then MapResToMaster;
          isQualifiedGetEval:=drugListed( resRec^.drugSeq );
        end
      end;
    end;

  var
    resCntr: word;
    resRec: PResRecObj;
    Eval: NCCLSEval;

    FilterMult : Boolean;
    IncludeDrug: Boolean;
    TrayDBObj  : PTraysObject;
    TestDB     : PDBFile;

  begin { addToReport }

    {---- This is for drug consolidation }
    FilterMult:=ScreenOn(FilterMultiDrugs);
    TrayDBObj:=New(PTraysObject,Init);
    TestDB:= New(PDBFile, Init(DBTSTFile, '', dbOpenNormal));

    {---- Update organism count }
    TallyOrg( isolate.Organism );

    {---- Update organism/drug count. Begin by setting the drug, then loop
          through each isolate order test result - tallying those who
          qualify. }
    BeginOrgDrugTally( isolate.Organism );
    for resCntr := 0 to results^.Count - 1 do
    begin
      resRec := LoadRecord( resCntr, results );
      if isQualifiedGetEval( resRec, Eval, FilterMult, TrayDBObj, TestDB ) then
        TallyOrgDrug( resRec^.drugSeq, Eval );
    end;

    {---- Cleanup }
    MSDisposeObj(TrayDBObj);
    MSDisposeObj(TestDB);
  end;

  {--------------------------------------------------------------------------}
  { This procedure sets the text and zeros the percentage for the abort
  { dialog.
  {--------------------------------------------------------------------------}
  procedure InitDialog( var percent: integer; Title: PChar );
  begin
    cancelDlg^.SetText( Title, 0 );
    percent := 0;
    cancelDlg^.SetText( '0%', IDC_LBL2 )
  end;

  {--------------------------------------------------------------------------}
  { This procedure updates the percentages in the abort dialog.
  {--------------------------------------------------------------------------}
  procedure UpdateDialog( var percent: integer; newPercent: integer );
  var
    lbl: array [ 0..80 ] of char;
  begin
    if newPercent <> percent then
    begin
      percent:= newPercent;
      Str( percent, lbl );
      StrCat( lbl, '%' );
      cancelDlg^.SetText( lbl, IDC_LBL2 );
    end;
    List( '' );  { uses side effect of List to update abort dialog }
  end;

  {--------------------------------------------------------------------------}
  { This procedure performes a final update to the abort dialog.
  {
  { Does this need to set IDC_LBL2 to "100%" ?
  {--------------------------------------------------------------------------}
  procedure FinalDialogUpdate;
  begin
    List( '' );
  end;

  {--------------------------------------------------------------------------}
  { This procedure adds the specified isolate to the report using the
  { addToReport procedure. The purpose of this procedure is to gather the
  { information needed by addToReport.
  {--------------------------------------------------------------------------}
  procedure compile( IsoSeq: TSeqNum );
  var
    isolate: TIsolate;
    results: PResults;          { List for isolate results }
    pIsoDB: PDBFile;
    pIsolate: PDBRec;
    ok: boolean;
  begin
    results := New( PResults, Init );
    Assert( results <> nil, 6 );

    {---- Access the isolate record. }
    pIsoDB := New( PDBFile, Init( DBISOFile, '', dbOpenNormal ) );
    Assert( pIsoDB <> nil, 7 );
    ok := pIsoDB^.dbc^.GetSeq( pIsoDB^.dbr, IsoSeq );
    Assert( ok, 8 );
    pIsolate := pIsoDB^.dbr;

    LoadIsolate( pIsolate, isolate );
    LoadResults( pIsolate, results );
    addToReport( isolate, results );

    MSDisposeObj( pIsoDB );
    MSDisposeObj( results );
  end;

var
  IsoSeq: TSeqNum;
  isolate: TIsolate;
  percent: integer;
  results: PResults;          { List for isolate results }
  pEDFilter: PEDFilterObj;
  PassTitle: array [0..63] of char;

begin { BuildTempFiles }

  {---- Step 1: Select isolates. If there is at least one MPSS-valid record in
        the isolate database, Initialize the abort dialog to the "creating
        report" mode, process each record, then send a final update to the
        dialog. After processing each record, check for a pending user abort
        and update the percentage. }
  if mpssObj^.MPSSGetFirst( isoDB ) then
  begin
    InitDialog( percent, SR( IDS_SELECTING, PassTitle,
      SizeOf( PassTitle ) - 1 ) );
    results := New( PResults, Init );
    Assert( results <> nil, 9 );
    pEDFilter := New( PEDFilterObj, Init );
    Assert( pEDFilter <> nil, 10 );
    repeat
      { Process isolate record: load just enough to qualify it, if ok,
        qualify its results, if ok, select it (put isolate into filter). }
      LoadIsolate( isoDB^.dbr, isolate );
      if isQualifiedIsolate( isolate ) and
        QualifyResults( isoDB^.dbr, results ) then
        begin

          { Select isolate }
          IsoSeq := isoDB^.dbr^.GetSeqValue;
          Assert( IsoSeq <> -1, 11 );
          pEDFilter^.Input( IsoSeq );

        end;
      UpdateDialog( percent, mpssObj^.MPSSGetPercent );
    until ( not mpssObj^.MPSSGetNext( isoDB ) ) or userAbort;
    FinalDialogUpdate;
    MSDisposeObj( results );

    { If ok, process data in the filter to eliminate duplicates, then
      extract data from filter and use it to compile the report. }
    if not userAbort then
    begin

      {---- Step 2: Eliminate duplicates }
      pEDFilter^.Process;

      {---- Step 3: Compile selected isolates }
      InitDialog( percent, SR( IDS_COMPILING, PassTitle,
      SizeOf( PassTitle ) - 1 ) );
      while pEDFilter^.Output( IsoSeq ) and not userAbort do
      begin

        { Compile isolate }
        compile( IsoSeq );
        UpdateDialog( percent, pEDFilter^.PercentOutput );

      end;
      FinalDialogUpdate;
    end;

    { Cleanup }
    Dispose( pEDFilter, Done );
  end;
end;



{----------------------------------------------------------------------------}
{ After the object has been constructed, this public procedure steps through
{ the printing proces.
{----------------------------------------------------------------------------}

procedure TEpiSusReport.Generate;
begin
  InitHeadings;
  AllowCancel;
  BuildTempFiles;
  if not userAbort then
    PrintReportBody;
end;



                        {----------------------}
                        {   EXPORT function    }
                        {----------------------}



{----------------------------------------------------------------------------}
{ This command-level procedure steps the user through the process of
{ generating an antimicrobial susceptibility report or an antimicrobial SIR
{ report.
{
{ pParent - pointer to the parent window
{ useMPSS - true if the user is to select MPSS rules
{ includeIR - true will generate a SIR report, false, a Susceptibility report
{
{----------------------------------------------------------------------------}

procedure EpiAntimicrobicSus( pParent: PWindowsObject; useMPSS, includeIR:
  boolean );

  {--------------------------------------------------------------------------}
  { This function loads a report title based on the type of report
  { (Susceptibility or SIR).
  {--------------------------------------------------------------------------}
  function LoadTitle( Buffer: PChar; SizeOfBuffer: word ): PChar;
  begin
    if includeIR then
      LoadTitle := SR( IDS_EPI_SIR_TITLE, Buffer, SizeofBuffer )
    else
      LoadTitle := SR( IDS_EPI_SUS_TITLE, Buffer, SizeofBuffer );
  end;

const
  {--------------------------------------------------------------------------}
  { This constant defines the INI file section name for group 3 sort
  { parameters.
  {--------------------------------------------------------------------------}
  SectionName = 'Reports - G3 Sort Parameters';

var
  pReport: PEpiSusReport;
  pWCur: PWaitCursor;
  drugs: TDrugArray;        { Drugs to be included in the report }
  SortParameters: SortEngineParameters;
  pKeyInformation: PGroup3KeyInformation;
  Step,
  Action: integer;
  Title: array[ 0..64 ] of char;

begin { EpiAntimicrobicSus }

  {---- Load default inputs. }
  SortParameters.LoadFromINI( SectionName, MAXIMUM_KEY_NUMBER );
  pKeyInformation := New( PGroup3KeyInformation, Init );
  LoadTitle( Title, SizeOf( Title ) );

  {---- Step user through input dialogs until complete or canceled. }
  Step := 1;
  repeat
    case Step of
      1 : { Prompt user to enter a list of drugs. }
        if GetDrugArray( pParent, @drugs ) then
          Inc( step )
        else
          step := 0;

      2 : { Prompt user to select sort parameters. }
        case SortParametersDialog( pParent, Title, pKeyInformation,
            true, SortParameters ) of
          id_Next :
            Inc( step );
          id_Back :
            Dec( step );
        else
            step := 0;
        end;

    else
      Assert( false, 12 );
    end;
  until not ( step in [ 1..2 ] );

  {---- If user decided to continue, generate report and update default sort
        parameters. }
  if step = 3 then
  begin
    {---- Construct the generator within a wait style cursor. }
    pWCur :=  New( PWaitCursor, Init );
    pReport := New( PEpiSusReport, Init( pParent, Title, drugs, useMPSS,
      includeIR, SortParameters ) );
    MSDisposeObj( pWCur );

    {---- If successful, use it to generate the report then dispose it. At
          this point, update the sort parameters. }
    if pReport <> nil then
    begin
      pReport^.Generate;
      SortParameters.StoreToINI( SectionName );
      MSDisposeObj( pReport );
    end;
  end;

  {---- Cleanup }
  Dispose( pKeyInformation, Done );
end;

END.
