unit autoSCAN;
{----------------------------------------------------------------------------}
{  Module Name  : AUTOSCAN.PAS                                               }
{  Programmer   : MJ                                                         }
{  Date Created : 05/95                                                      }
{                                                                            }
{  Purpose - This module is the instrument interface to the autoscan-4.  It  }
{            contains the implementation for the calibration of the AS4,     }
{            the reading of panels and interpretation of results.            }
{                                                                            }
{  Assumptions -                                                             }
{              None.                                                         }
{                                                                            }
{  Revision History -                                                        }
{                                                                            }
{  Version  Date      Who    Did What                                        }
{  -------  --------  ------ ----------------------------------------------- }
{  1.00     05/95     MJ     Initial release.                                }
{                                                                            }
{----------------------------------------------------------------------------}

INTERFACE

uses
  PDLObj,
  DMString,
  IntlLib,
  APITools,
  WinTypes,
  WinProcs,
  WinDos,
  Strings,
  Objects,
  DMSErr,
  MSCAN,
  WaitLib,
  DMSDebug,
  OWindows;

{$I AUTOSCAN.INC }
{$R AUTOSCAN.RES }

{$I AS4RDR.INC }

const
  MAXRow              = 7;    { Number of rows, cols for AS4, W/A panels }
  MAXCol              = 11;
  MAXPortNum          = 7;    { Highest as4 port number for saving calibration data}
  portnum             = 0;
  oneDay              = 1440; { Minutes in one day}
  oneHour             = 60;   { Minutes in one hour}
  linkTO              = 320;  { Increased from 10 to 400 for v4.00, ghg 10/7/98 }
  cmdTO               = 5;
  readTO              = 300;
  delMSecs            = 2;
  filterStrBase       = 2562;
  sdata                = 0;
  interuptEnable       = 1;
  interuptIdentify     = 2;
  lineCtrl             = 3;
  modemCtrl            = 4;
  lineStatus           = 5;
  modemStatus          = 6;
  reqToSendDTR         = 3;
  reqToSend            = 2;
  clearAll             = 0;
  ReadWriteOn          = 128;
  loopBack             = 16;
  cancelOp             = 24;
  dataAvail            = 1;
  uploadDACS           = '4';
  Calibrate            = '2';
  readPanel            = '1';
  SelfTest             = 'T';
  drawerClose          = 'F';
  drawerOpen           = 'G';
  retErrors            = 'N';
  chkCalByte           = 'V';

type
  FileSpec = record
    waveLength: integer;
    calValue  : integer;
    calRange  : integer;
  end;

  Column      = array[0..MAXRow] of integer;
  Image       = array[0..MAXTRWell] of integer;
  Picture     = array[0..MAXFilter] of image;

  PautoSCANObject = ^TautoSCANObject;
  TautoSCANObject = object(TObject)
    portBase     : word; { (COM1=1016) (COM2=760) (COM3/PS2=12832) (COM3/Gateway=1000)}
    inControl    : boolean;
    as4Connected : boolean;
    filterDesc   : array[0..9] of FILESPEC;
    DACValues    : picture;
    rawData      : TRawData;
    hReaderWindow: HWnd;
    ProcessKeys  : Boolean;
    abort        : boolean;
    calDate      : TIntlDate;
    calTime      : TIntlTime;
    testDate     : TIntlDate;
    testTime     : TIntlTime;
    calChanged   : boolean;
    calFile      : file of image;
    calBuff      : image;         { One filter's calibration values }
    AS4Text      : array[0..127] of char;
    {}
    constructor Init;
    destructor Done; virtual;
    procedure SetHWindow(wndw : HWnd);
    procedure POut(index, data: byte);
    function  PInp(Index: byte): byte;
    function  InData: integer;
    procedure Cancel;
    function  RangeError(value, flt: integer): boolean;
    function  OutOfControl: boolean;
    function  GetTray(source: integer): boolean;
    function  TimeLeft: boolean;
    function  WaitCts: boolean;
    function  Wait: boolean;
    function  Sent(ch: integer): boolean;
    function  SendCommand(comm: char): boolean;
    procedure RawSend(data: integer);
    procedure SendToAS4(data: integer);
    procedure GetDacs;
    procedure SetDac;
    procedure DoCalibrate;
    procedure ReadTray;
    function  GetByte: integer;
    function  LoadCal: boolean;
    function  InitCalLimits: boolean;
    function  SaveCal(allData: boolean): boolean;
    procedure DoSelfTest(var mask: byte);
    function  TimeSince(aDate: TIntlDate; aTime: TIntlTime): longint;
    function  CalibrationDue: boolean;
    function  DailyQCDue: boolean;
    function  As4Ready: boolean;
    function  RecalWanted(promptField: integer): boolean;
    function  GetAS4Text(strNum: integer): PChar;
    procedure CloseDrawer;
    function OpenDrawer: boolean;
    procedure ClearDateTime;
    procedure InitializePort;
    procedure GetCommPort;
    function  InitCalibration: boolean;
    function  TransmitterRegsReady: boolean;
    function  FlushBuffers: boolean;
    function  DataReady: boolean;
    function  ClearToSend: boolean;
    function  RequestToSend(doWait: boolean): boolean;
    function  UserAbort: Boolean;
    procedure IgnoreKeys(xState: boolean);
  end;

IMPLEMENTATION

uses
  Bits,
  DateLib;


constructor TautoSCANObject.Init;
var
  pStr  : array[0..63] of char;
begin
  inherited Init;
  IgnoreKeys(True);
  ClearDateTime;
  GetCommPort;
  InitializePort;
  if not InitCalibration then
  begin
    ErrorMsg(hReaderWindow, '', SR(IDS_ERRORCALFILECNVT, pStr, sizeof(pStr)-1));
    Done;
    Fail;
  end;
  as4Connected := OpenDrawer; {- always ensure that the drawer is opened }
end;

function TautoSCANObject.InitCalibration: boolean;
{-----------------------------------------------------------------------}
{ Purpose : initialize the serial port                                  }
{           9600 baud, 1 stop, no parity, no interrupts                 }
{-----------------------------------------------------------------------}
begin
  InitCalibration := false;
  FillChar(dacvalues, SizeOf(dacvalues), 0);
  if LoadCal then
    if InitCalLimits then
      InitCalibration := true;
end;

procedure TautoSCANObject.GetCommPort;
var
  commAddr : integer;
  code     : integer;
  s        : array[0..16] of char;
begin
  GetPrivateProfileString('DMS', 'AS4PORT', 'COM1', s, sizeof(s)-1, profFile);
  if StrIComp(s, 'COM2') = 0 then
  begin
    GetPrivateProfileString('DMS', 'COM2ADDR', '760', s, sizeof(s)-1, profFile);
    Val(s, commAddr, code);
    portBase:= commAddr;
  end
  else if StrComp(s, 'COM3') = 0 then
  begin
    GetPrivateProfileString('DMS', 'COM3ADDR', '12832', s, sizeof(s)-1, profFile);
    Val(s, commAddr, code);
    portBase := commAddr;
  end
  else
  begin
    { default to COM1 }
    GetPrivateProfileString('DMS', 'COM1ADDR', '1016', s, sizeof(s)-1, profFile);
    Val(s, commAddr, code);
    portBase := commAddr;
  end;
end;

function TAutoSCanObject.RequestToSend(doWait: boolean): boolean;
begin
  POut(modemCtrl, reqToSend);
  if doWait then
    MSWait(DelMSecs);
end;

procedure TautoSCANObject.InitializePort;
{-----------------------------------------------------------------------}
{ Purpose : initialize the serial port                                  }
{           9600 baud, 1 stop, no parity, no interrupts                 }
{-----------------------------------------------------------------------}
var i : integer;
begin
  POut(interuptEnable, 0);       {disable interupts}
  POut(interuptIdentify, 0);
  POut(lineCtrl, ReadWriteOn);   {enable the ReadWrite bit}
  POut(sdata, 12);
  POut(interuptEnable, 0);       {disable interupts}
  POut(lineCtrl, 7);
  POut(modemCtrl, loopBack);     {check the transmit-data and receive-data paths}
  i:= InData;
  POut(modemCtrl, reqToSendDTR); {Set the ReqToSend bit and the Data Terminal Ready bit}
end;

procedure TautoSCANObject.ClearDateTime;
begin
  calTime  := IntlPackTime(0, 0, 0, 0);
  calDate  := IntlPackDate(0, 0, 0);
  testTime := IntlPackTime(0, 0, 0, 0);
  testDate := IntlPackDate(0, 0, 0);
end;

function TautoSCANObject.GetAS4Text(strNum: integer): PChar;
begin
  SR(strNum, AS4Text, sizeof(as4Text)-1);
  GetAS4Text:= AS4Text;
end;

procedure TautoScanObject.SetHWindow(wndw: HWnd);
begin
  hReaderWindow:= wndw;
end;

procedure TautoScanObject.IgnoreKeys(xState : boolean);
begin
  ProcessKeys:=not xState;
end;

procedure TautoSCANObject.POut(index, data : byte );
{-----------------------------------------------}
{ Send DATA to port at BASE+INDEX               }
{-----------------------------------------------}
begin
  Port[portbase + index]:= data;
end;

function TautoSCANObject.PInp( index : byte ) : byte;
{-----------------------------------------------}
{ Get data from BASE+INDEX                      }
{-----------------------------------------------}
begin
  PInp:= Port[portbase + index];
end;

function TautoSCANObject.InData : integer;
{-----------------------------------------------}
{ Return the contents of the reciever buffer    }
{-----------------------------------------------}
begin
  inData:= PInp(sdata) and $FF;
end;

destructor TautoSCANObject.Done;
begin
  inherited Done;
  POut(interuptEnable, 1);  { enable interrupts for this port }
end;

function TAutoScanObject.TransmitterRegsReady: boolean;
const
  TransHoldReg = 5;
  TransShiftReg = 6;
var
  lineResult : byte;
begin
  TransmitterRegsReady := false;
  lineResult := PInp(lineStatus);
  { the transmitter regs are ready when both the holding register and
    shift register are empty (bit 6 high) and the serial port controller
    is ready to accept a new character (bit 5 high)}
  TransmitterRegsReady := TestBit(lineResult, TransHoldReg) and
                          TestBit(lineResult, TransShiftReg);
end;

procedure TautoSCANObject.Cancel;
{-------------------------------------------------------}
{ Cancel an operation in progress on the AS4            }
{-------------------------------------------------------}
begin
  RequestToSend(true);
  POut(sdata, cancelOp);
  Repeat
  Until (PInp(lineStatus) AND $60)<>0;
{*  while not TransmitterRegsReady do;*}
  POut(modemCtrl, clearAll);
end;

function TautoSCANObject.RangeError( value, flt : integer ): boolean;
{-----------------------------------------------}
{ Return TRUE If the passed value is out of     }
{ range for the specified filter                }
{-----------------------------------------------}
begin
  with filterdesc[flt] do
    RangeError:= ABS(value - calvalue) > calrange;
end;

function TautoSCANObject.OutOfControl : boolean;
{-----------------------------------------------}
{ Return TRUE If any wells are out of cal.      }
{-----------------------------------------------}
var
  flt,
  well  : integer;
begin
  outOfControl:= true;
  for flt:= 0 to (MAXFilter-WaterBlankFilter) do  {MaxFilter reduced to account for WATER}
    for well:= 0 to MAXTRWell do
      if RangeError(rawdata[1, flt, well], flt) then
        exit;
  OutOfControl:= false
end;

(*
function TautoSCANObject.Rawread : integer;
{--------------------------------------------------------------}
{  PURPOSE:  to get data from as4 without handshaking          }
{--------------------------------------------------------------}
var
  x: integer;
begin
  Pout(modemCtrl, reqToSend);             { Set Request to send high}
  MSWait(delMSecs);
  repeat
  until ((PInp(lineStatus) AND $01) = $01) or abort;

  x:= PInp(sdata) and $FF;
  rawread:=  x;
  Pout(modemCtrl, clearAll);
end;
*)

function TAutoScanObject.DataReady: boolean;
var
  lineResult : byte;
begin
  lineResult := PInp(lineStatus);
  DataReady := TestBit(lineResult, dataAvail);
end;

function TautoScanObject.GetByte: integer;
{-----------------------------------------------------------------------}
{ Purpose: Get a byte from the AS-4 - WGE 10/31/92                      }
{-----------------------------------------------------------------------}
begin
  GetByte := 0;
  RequestToSend(false);

  while {(not DataReady)} TimeLeft and ((PInp(lineStatus) and 1) <> 1)  do;

  if not abort then
  begin
    GetByte:= inData;
    POut(modemCtrl, clearAll);
    MSWait(delMSecs);
  end;
end;

function TAutoScanObject.FlushBuffers: boolean;
var
  well : integer;
begin
  FlushBuffers := false;
  SetTo(readTo);
  Pout(modemCtrl, clearAll);
  repeat
  until not TimeLeft or (GetByte = 17);   {- wait for DC1 }
  MSWait(delMSecs);
  if abort then
    exit;
  {This loop flushes the buffer under Windows with Turbo Pascal.
   Simple read of data line}
  repeat
    well:= Pinp(sdata);
  until {not DataReady;}(Pinp(lineStatus) and 1) = 0;
  FlushBuffers := not abort;
end;

function TautoSCANObject.GetTray(source: integer): boolean;
{-----------------------------------------------------------------------}
{ Purpose : get the data For an entire tray from AS4.  Puts data into   }
{           the RAWDATA Array.  Uses source to determine If the data is }
{           run data or calibration data.  0 = run; 1 = calibration     }
{-----------------------------------------------------------------------}
var
  filter    : integer;
  column    : integer;
  row       : integer;
  temp      : integer;
begin
  GetTray := false;
  for filter:= 0 to (MAXFilter-WaterBlankFilter) do {MaxFilter reduced to account for WATER}
  begin
    if not FlushBuffers then
      exit;
    for column:= 0 to MAXCol do
    begin
      for row:= 0 to MAXRow do
      begin
        temp:= GetByte shl 4;         {first byte read * 16 plus  }

        if abort then
          exit;
        temp:= temp + (GetByte shr 4);   {second byte read / 16 }
        if abort then
          exit;
        rawdata[source, filter, (MAXRow+1)*column+(MAXRow-row)]:= temp;
      end;
    end;
  end;
  GetTray := true;
end;

function TautoSCANObject.TimeLeft: boolean;
{---------------------------------------------------------------}
begin
  if not Abort then
    if UserAbort or TimeOut then
    begin
      Cancel;
      SetDlgItemText(hReaderWindow, IDC_STATUS, GetAS4Text(IDS_PROCESSTO));
      Abort:= true;
    end;
  TimeLeft := not abort;
end;


function TautoSCANObject.ClearToSend: boolean;
const
  ctsBit = 4;
var
  lineResult : byte;
begin
  lineResult := PInp(modemStatus);
  ClearToSend := TestBit(lineResult, ctsBit);
end;

function TautoSCANObject.UserAbort: boolean;
var Msg    : TMsg;
    Escaped: Boolean;
begin
  Escaped:=False;
  if ProcessKeys then
     if PeekMessage(Msg, 0, wm_KeyDown, wm_KeyDown, PM_REMOVE or PM_NOYIELD) then
        if (Msg.wParam=vk_Escape)
           then Escaped:=True
           else DispatchMessage(Msg);
  UserAbort:=Escaped;
end;

function TautoSCANObject.WaitCts: boolean;
{-----------------------------------------------------------------------}
{ Purpose : Wait For cts to go active   ESCapable                       }
{-----------------------------------------------------------------------}
begin
  WaitCTS := false;
  SetTo(linkTo);
  repeat
{  until not TimeLeft or ClearToSend;}
  until not TimeLeft or ClearToSend;
  WaitCts := not abort;
end;

function TautoSCANObject.Wait: boolean;
{-----------------------------------------------------------------------}
{ Purpose : Wait For a character from AS4                               }
{-----------------------------------------------------------------------}
begin
  Wait := false;
  RequestToSend(true);
  {Wait For char or timeOut}
  repeat
  until not TimeLeft or ((PInp(lineStatus) and 1)<>0) or Abort;
  POut(modemCtrl, clearAll);    {Clear Request to Send and DTR}
  Wait := not abort;
end;

function TautoSCANObject.Sent( ch : integer ) : boolean;
{-----------------------------------------------}
{ Send a byte and return true If echoed         }
{-----------------------------------------------}
begin
  Sent := false;
  SetTo(cmdTo);

  While TimeLeft and ( (Pinp(sdata)=-1) or DataReady ) do;

  if not Abort then
  begin
    repeat
    until not TimeLeft or ClearToSend;
    if not Abort then
    begin
      POut(sdata, ch);
      if not Wait then;
      Sent:=(inData = ch);
    end;
  end;
end;

function TautoSCANObject.SendCommand( comm : char ): boolean;
{-----------------------------------------------------------------------}
{ Purpose : send a command to the AS4  ESCapable                        }
{-----------------------------------------------------------------------}
begin
  Abort:= false;
  RequestToSend(false);
  POut(modemCtrl, clearAll);
  RequestToSend(false);
  POut(modemCtrl, clearAll);
  POut(sdata, cancelOp);    { make sure AS4 will listen!    (CAN)   }
  repeat
    repeat
    until Sent(27) or Abort;
  until not TimeLeft or Sent(Ord(comm)) or Abort;
  SendCommand := not abort;
end;

procedure TautoSCANObject.RawSend(data : integer);
{--------------------------------------------------------------}
{  PURPOSE:  to send data to  as4 without handshaking          }
{--------------------------------------------------------------}
begin
  repeat
{  until (ClearToSend) or abort;}
  until (PInp(modemStatus) and $10 = $10) or (abort);
  if not abort then
  begin
    repeat
{   until (abort) or (not TransmitterRegsReady);}
    until (PInp(lineStatus) and $60 = $60) or (abort);
    if not abort then
    begin
      Pout(sdata, data);
      repeat
{      until (abort) or (not ClearToSend);}
      until (PInp(modemStatus) and $10 = 0) or (abort);
    end;
  end;
end;

procedure TautoSCANObject.SendToAS4( data : integer);
{-------------------------------------------------------------}
{  PURPOSE:  to send DATA to as4                              }
{-------------------------------------------------------------}
var
  x        : integer;
  sendDone : boolean;
begin
  repeat   { clear out leftover garbage }
{  until (Pinp(sdata) <> -1) and (not DataReady);}
  until (Pinp(sdata) <> -1) and ((PInp(lineStatus) and 1) = 0);

  RawSend(data);
  if not abort then
  begin
    x:= GetByte;
    if x <> data then { status:= SR(IDS_COMMERROR, AS4Text, sizeof(AS4Text)-1); }
      abort:=  true;
  end;
end;

procedure TautoSCANObject.GetDacs;
{-----------------------------------------------}
{ Get the DAC values from the AS4               }
{-----------------------------------------------}
var
  flt,
  block,
  col,
  row,
  well : integer;
begin
  FillChar(dacvalues, SizeOf(dacvalues), 0);
  if SendCommand(uploadDACs) then
  begin
    for block:= 1 to 7 do
    begin
      if block mod 2 = 1 then
      begin
        SetTo(readTo);
        Pout(modemCtrl, clearAll);
        repeat
        until not TimeLeft or (GetByte = 17);   {- wait for DC1 }
        MSWait(delMSecs);

        if abort then
          exit;
      end;
      for col:= 0 to MAXcol do
        for row:= 0 to MAXrow do
        begin
          dacvalues[block -1, (col*(MAXRow+1))+(MAXRow - row)]:= GetByte;
          if abort then
            exit;
        end;
    end;
  end;
  Cancel;  {cancel rest of transmission, it was garbage anyway.}
end;

procedure TautoSCANObject.SetDac;
{-----------------------------------------------------------------------}
{ Purpose : calibrate the AS4 and get water blank data                  }
{-----------------------------------------------------------------------}
var
  flt,
  row,
  col,
  well          : integer;
begin
  abort:= false;
  if SendCommand(Calibrate) then
    if WaitCts then
      if GetTray(cal) then {Read calibrate values into rawdata Array}
        if SendCommand(retErrors) then
          if Wait then
            if inData<>127 then
            begin
              SetDlgItemText(hReaderWindow, IDC_STATUS, GetAS4Text(IDS_PROCESSABORT));
              Abort := true;
            end
            else
              GetDacs;
end;

procedure TautoSCANObject.DoCalibrate;
{-------------------------------------------------------}
{ Do the daily instrument calibration                   }
{-------------------------------------------------------}
var
  h,
  m,
  s,
  hs,
  y,
  d,
  dow   : WORD;
  pStr  : array[0..63] of char;
  pStr1 : array[0..63] of char;
begin
  IgnoreKeys(False);
    FillChar(dacvalues, SizeOf(dacvalues), CHR(0));
    SetDlgItemText(hReaderWindow, IDC_STATUS, GetAS4Text(IDS_CALPRESSREAD));
    SetDac;
  IgnoreKeys(True);
  if (abort) then
  begin
    SetDlgItemText(hReaderWindow, IDC_STATUS, GetAS4Text(IDS_CALABORTED))
  end
  else
  begin
    calTime   := IntlCurrentTime;
    calDate   := IntlCurrentDate;
    calchanged:= true;
    inControl := true;

    if OutOfControl then
    begin
      inControl:= false;
      SetDlgItemText(hReaderWindow, IDC_STATUS, GetAS4Text(IDS_INSTOUTCTRL));
    end;

    {else}
    begin
      if SaveCal(true) then
      begin
        if InControl then
           SetDlgItemText(hReaderWindow, IDC_STATUS, GetAS4Text(IDS_INSTINCTRL));
      end
      else
      begin
        ErrorMsg(hReaderWindow, SR(IDS_NOSAVECALVALS, pStr, sizeof(pStr)-1),
                                SR(IDS_ERROR, pStr1, sizeof(pStr1)-1));
        inControl:= false;
      end;
    end;
  end;
end;

procedure TautoSCANObject.ReadTray;
{-----------------------------------------------------------------------}
{ Purpose : establish link and get data For a tray from AS4             }
{-----------------------------------------------------------------------}
begin
  abort := false;
  if SendCommand(readPanel) then
    if WaitCts then
    begin
      GetTray(run);
      if not Abort then
        if SendCommand(retErrors) then
          if Wait then
            if InData<>127 then
            begin
              SetDlgItemText(HReaderWindow, IDC_STATUS, GetAS4Text(IDS_PROCESSABORT));
              abort := true;
            end;
    end;
end;

function TautoSCANObject.LoadCal: boolean;
{---------------------------------------}
{ Load the old calibrate values         }
{---------------------------------------}
var
  row,
  col,
  flt : integer;
begin
  LoadCal:= true;

  {$IFOPT I+}
    {$DEFINE chki}
  {$ENDIF}
  {$I-}

  FillChar(calbuff, SizeOf(calbuff), Chr(0));
  Assign(calfile, 'CALFNAME');
  Reset(calfile);
  if IoResult <> 0 then { cal file does not exist, create one }
  begin
    Rewrite (calfile);
    if IoResult <> 0 then
      LoadCal:= false
    else
    begin
      ClearDateTime;
      incontrol:= false;
      calchanged:= true;
      for flt:= 0 to (maxPortNum +1) * ((maxfilter-WaterBlankFilter) +2)-1 do {MaxFilter reduced to account for WATER}
      begin
        { init calibration file For all possible ports}
        Write(calfile, calbuff);
        if IoResult <> 0 then
        begin
          LoadCal:= false;
          flt := (maxPortNum +1) * ((maxfilter - WaterBlankFilter) +2)-1;  { get out of the FOR loop }
          {MaxFilter reduced to account for WATER}
        end;
      end;
      Close(calfile);
    end;
  end
  else  { calfile exists }
  begin
    calchanged:= false;
    Seek(calfile, portnum * 8);
    if IoResult <> 0 then
      LoadCal:= false
    else
    begin
      Read(calfile, calbuff);
      if IoResult <> 0 then
        LoadCal:= false
      else
      begin
        calTime := IntlPackTime(calbuff[0], calBuff[1], 0, 0);
        calDate := IntlPackDate(calbuff[2], calbuff[3], calbuff[4]);
        testTime := IntlPackTime(calbuff[5], calBuff[6], 0, 0);
        testDate := IntlPackDate(calbuff[7], calbuff[8], calbuff[9]);
        if calbuff[10] <> 0 then
          incontrol:= true
        else
          incontrol:= false;
        for flt:= 0 to (maxfilter-WaterBlankFilter) do {MaxFilter reduced to account for WATER}
        begin
          Read(calfile, calbuff);
          if IoResult <> 0 then
          begin
            LoadCal:= false;
            flt := (maxFilter-WaterBlankFilter) {MaxFilter reduced to account for WATER}
          end
          else;
          Move(calbuff, rawdata[1, flt], SizeOf(image));
        end;
      end;
    end;
    Close(calfile)
  end;
  if IOResult = 0 then;
  {$IFDEF chki}
    {$I+}
    {$UNDEF chki}
  {$ENDIF}
end;

function TautoSCANObject.InitCalLimits: boolean;
var
  i     : integer;
  value : integer;
  indx  : integer;
  ptemp : PChar;
  temp  : array[0..10] of char;
begin
  InitCalLimits:= true;
  pTemp:= addr(temp);
  for i:= 0 to 9 do
  begin
    with filterDesc[i] do
    begin
      StrLCopy(pTemp, GetAS4Text(3*i+filterStrBase), 10);
      Val(pTemp, value, indx);
      if indx <> 0 then             {Error converting string to Integer}
        InitCalLimits:= false;
      waveLength:= value;
      StrLCopy(pTemp, GetAS4Text(3*i+filterStrBase+1), 10);
      val(pTemp, value, indx);
      if indx <> 0 then         {Error converting string to Integer}
        InitCalLimits:= false;
      calValue:= value;
      StrLCopy(pTemp, GetAS4Text(3*i+filterStrBase+2), 10);
      Val(pTemp, value, indx);
      if indx <> 0 then         {Error converting string to Integer}
        InitCalLimits:= false;
      calRange:= value;
    end;
  end;
end;

function TautoSCANObject.SaveCal(allData:boolean): boolean;
{---------------------------------------}
{ Save the calibrate values For later   }
{---------------------------------------}
var
  flt,
  row,
  col        : integer;
  dummy      : word;
  fileExists : boolean;

begin
  SaveCal:= true;
  {$IFOPT I+}
    {$DEFINE chki}
  {$ENDIF}
  {$I-}

  FileExists := true;
  Assign(calfile, 'CALFNAME');
  FillChar(calbuff, SizeOf(calbuff), 0);
  Reset(calfile);
  if IoResult <> 0 then
  begin   { Can't open calibration file, so create it}
    Rewrite(calfile);
    if IoResult <> 0 then { Can't create calibration file, so give up}
    begin
      SaveCal:= false;
      fileExists := false;
    end
    else {init calibration file For all possible ports}
      for flt:= 0 to maxPortNum *((maxfilter-WaterBlankFilter)+2) do {MaxFilter reduced to account for WATER}
      begin
        Write(calfile, calbuff);
        if IoResult <> 0 then
        begin
          SaveCal:= false;
          fileExists := false;
          flt := maxPortNum *((maxfilter-WaterBlankFilter)+2);  { I/O error so get out of FOR loop }
          {MaxFilter reduced to account for WATER}
        end;
      end;
  end;
  if fileExists then
  begin
    IntlExtractTime(calTime, word(calbuff[0]), word(calBuff[1]), dummy, dummy);
    IntlExtractDate(calDate, word(calbuff[2]), word(calbuff[3]), word(calbuff[4]));
    IntlExtractTime(testTime, word(calbuff[5]), word(calBuff[6]), dummy, dummy);
    IntlExtractDate(testDate, word(calbuff[7]), word(calbuff[8]), word(calbuff[9]));
    if incontrol then
      calbuff[10]:=-1
    else
      calbuff[10]:= 0;
    Seek(calfile, portnum * 8);
    if IOResult <> 0 then
      SaveCal := false
    else
    begin
      Write(calfile, calbuff);
      if IoResult <> 0 then
        SaveCal:= false
      else
        if allData then
          for flt:= 0 to (maxfilter-WaterBlankFilter) do {MaxFilter reduced to account for WATER}
          begin
            Move(rawdata[1, flt], calbuff, SizeOf(calbuff));
            Write(calfile, calbuff);
            if IoResult <> 0 then
            begin
              SaveCal := false;
              flt := (maxFilter-WaterBlankFilter);  { IO error so get out of the loop }
                                                    {MaxFilter reduced to account for WATER}
            end;
          end;
    end;
    Close(calfile);
  end;
  if IoResult <> 0 then {- clear ioresult};
  {$IFDEF chki}
    {$I+}
    {$UNDEF chki}
  {$ENDIF}
end;

procedure TautoSCANObject.DoSelfTest(var mask: byte);
begin
  abort:= false;
  if SendCommand(selfTest) then {start self test}
  begin
    SetTO(readTO);
    if Wait then
      mask:= inData;
  end;
end;

procedure TautoSCANObject.CloseDrawer;
begin
  if not SendCommand(drawerClose) then;
end;

function TautoSCANObject.OpenDrawer: boolean;
begin
  OpenDrawer := SendCommand(drawerOpen);
end;

function TautoSCANObject.TimeSince(aDate: TIntlDate; aTime: TIntlTime) : longint;
{-----------------------------------------------}
{ Return the time in minutes since WHEN         }
{-----------------------------------------------}
var
  curDate     : TIntlDate;
  curTime     : TIntlTime;
  elapsedSecs : TSecs;
begin
  curDate := IntlCurrentDate;
  curTime := IntlCurrentTime;
  elapsedSecs := DateTimeDiffInSecs(aDate, aTime, curDate, curTime);
  TimeSince := elapsedSecs div 60;
end;

function TautoSCANObject.CalibrationDue: boolean;
{-------------------------------------------------}
{ Return TRUE If date/time is older than 1 hour   }
{-------------------------------------------------}
begin
  if calDate = 0 then {new blank cal file}
    CalibrationDue := true
  else
    CalibrationDue:= (TimeSince(calDate, calTime) > oneHour);
end;

function TautoSCANObject.DailyQCDue: boolean;
{---------------------------------------------------}
{ Return TRUE If date/time is older than 24 hours   }
{---------------------------------------------------}
begin
  if testDate = 0 then {new blank cal file}
    DailyQCDue := true
  else
    DailyQCDue:= (TimeSince(testDate, testTime) > oneDay);
end;

function TautoSCANObject.AS4Ready: boolean;
{---------------------------------------------------}
{ Ask the AS-4 If it has been turned off            }
{---------------------------------------------------}
begin
  AS4Ready := false;
  if SendCommand(chkCalByte) then
  begin
    if not Wait then;
    AS4Ready:= (inData=ORD('Y'));
  end;
end;

function TautoSCANObject.RecalWanted(promptfield : integer) : boolean;
{-----------------------------------------------}
{ Ask the user If we should recalibrate now.    }
{-----------------------------------------------}
begin
  if MessageBox(HReaderWindow, GetAS4Text(IDS_RECALAS4), ' ',
                 mb_YesNo + mb_IconQuestion ) = idYes then
    RecalWanted:= true
  else
    RecalWanted:= false;
end;

END.
