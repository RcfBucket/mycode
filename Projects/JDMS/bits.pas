unit Bits;
{----------------------------------------------------------------------------}
{  Module Name  : BITS.PAS                                                   }
{  Programmer   : rcf                                                        }
{  Date Created : 02/95                                                      }
{                                                                            }
{  Purpose -                                                                 }
{  This module provides bit manipulation routines.                           }
{                                                                            }
{  Assumptions -                                                             }
{  None                                                                      }
{                                                                            }
{  Initialization -                                                          }
{  None                                                                      }
{                                                                            }
{  Revision History -                                                        }
{                                                                            }
{  Version  Date      Who    Did What                                        }
{  -------  --------  ------ ----------------------------------------------- }
{  1.00     02/95     rcf    Initial release.                                }
{----------------------------------------------------------------------------}

INTERFACE

procedure SetBit(var p; bit: Word);
procedure ClearBit(var p; bit: Word);
function TestBit(var p; bit: Word): Boolean;
procedure ToggleBit(var p; bit: Word);

IMPLEMENTATION

uses
  DMSDebug;

{$R-}  {- need this becase of forced typecasting below }

type
  TArray = array[0..100] of byte;  {- array of any size (just for typecasting) }

procedure SetBit(var p; bit: Word);
{- set a bit in memory }
begin
  TArray(p)[bit div 8]:= TArray(p)[bit div 8] or (1 shl (bit mod 8));
end;

procedure ClearBit(var p; bit: Word);
{- clear a bit in memory }
begin
  TArray(p)[bit div 8]:= TArray(p)[bit div 8] and not (1 shl (bit mod 8));
end;

function TestBit(var p; bit: Word): boolean;
{- return true if a particular bit is set }
begin
  TestBit:= (TArray(p)[bit div 8] and (1 shl (bit mod 8))) <> 0;
end;

procedure ToggleBit(var p; bit: Word);
begin
  if TestBit(p, bit) then
    ClearBit(p, bit)
  else
    SetBit(p, bit);
end;

END.

{- rcf }
