{----------------------------------------------------------------------------}
{  Module Name  : WACPSR.PAS                                                 }
{  Programmer   : EJ                                                         }
{  Date Created : 05/24/95                                                   }
{                                                                            }
{  Purpose -                                                                 }
{  This module provides the user with CPSR functions available in            }
{  the WA INTERFACE.                                                         }
{                                                                            }
{  Assumptions -                                                             }
{  None.                                                                     }
{                                                                            }
{  Revision History -                                                        }
{                                                                            }
{  Version  Date      Who    Did What                                        }
{  -------  --------  ------ ----------------------------------------------- }
{  1.00     05/24/95  EJ     Initial release.                                }
{                                                                            }
{----------------------------------------------------------------------------}

unit WACPSR;

INTERFACE

uses
  OWindows,
  DBFile;

procedure PrintCPSR(aParent: PWindowsObject; dbIso, dbPat, dbSpec, dbWap: PDBFile);


IMPLEMENTATION

uses
  Strings,
  MScan,
  INILib,
  APITools,
  ListLib,
  DBIDs,
  DBLib,
  DBTypes,
  ResLib,
  Report,
  PrnPrev,
  Biotype,
  PnlView,
  Trays,
  IDLib,
  Bits,
  WAMain,
  WAUtil,
  WAGPIB,
  WaitLib,
  DMSErr,
  DMSDebug,
  BarCodes,
  Screens;


type
  PCPSRObj = ^TCPSRObj;
  TCPSRObj = object(TReport)
    isoDB        : PDBFile;
    isordDB      : PDBAssocFile;
    tgrpDB       : PDBFile;
    wapDB        : PDBFile;
    patDB        : PDBFile;
    specDB       : PDBFile;
    resList      : PResults;
    listObj      : PListObject;
    mnmListRec   : PMnemListRec;
    waStat       : integer;
    waErr        : integer;
    reportData   : boolean;
    panelID      : integer;
    tstGrpID     : array [0..32] of char;
    fieldVal     : array [0..255] of char;
    WASCICmd     : PWASCICmd;
    WAPSICmd     : PWAPSICmd;
    WAPEICmd     : PWAPEICmd;
    WAPMICmd     : PWAPMICmd;
    WAPRICmd     : PWAPRICmd;
    waFlags      : PPanelFlags;
    bioObj       : PWABiotypeObject;
    trayBits     : TPanelBitmap;
    MICs         : PMIArray;
    virtualTests : TPanelBitmap;
    exceptions   : PExceptions;
    orgNum       : integer;
    ValidResults : Boolean;
    {}
    constructor Init(aParent: PWindowsObject; dbIso, dbPat, dbSpec, dbWap: PDBFile);
    destructor Done; virtual;
    function  PrintReport: boolean;
    function  PrintPage: boolean;
    function  GetPanelID: boolean;
    function  SetUpWACmds: boolean;
    procedure GetTheSpecimenAndPatient;
    procedure PrintDemogSection; virtual;
    procedure PrintWASection; virtual;
    procedure PrintDBSection; virtual;
    procedure PrintAFlag(strID: integer);
    procedure PrintWAFlags; virtual;
    function  PrintBiotype(fromWA: boolean): boolean;
    procedure PrintBiochems(fromWA: boolean; biotypeExists: boolean);
    procedure PrintAnException(strNum: integer);
    procedure PrintExceptions;
(*  procedure PrintAutoExceptions; *)
    procedure PrintIdentification(fromWA: boolean; biotypeExists: boolean);
    procedure PrintMICs(fromWA: boolean);
    procedure PrintExtraTests(fromWA: boolean);
    function  GetVirtualTests: boolean;
    function  CheckStatus: boolean;
    function  micNeedsOrg: Boolean;
    function  OnWAExList(pId, WASlot, pExCode: Integer): Boolean;
  end;


{-------------------------------------------------------[ TCPSRObj ]--}
constructor TCPSRObj.Init(aParent: PWindowsObject; dbIso, dbPat, dbSpec, dbWap: PDBFile);
var
  temp : array [0..64] of char;
begin
  isoDB := dbIso;
  patDB := dbPat;
  specDB := dbSpec;
  wapDB := dbWap;

  tgrpDB := nil;
  resList := nil;
  isordDB := nil;
  listObj := nil;
  mnmListRec := nil;
  exceptions := nil;

  SR(IDS_CPSR, temp, 64);

  if not inherited Init(aParent, temp, 121, 0) then
  begin
    Done;
    Fail;
  end;

  WAPSICMd := New(PWAPSICmd, Init(WAID));
  WASCICmd := New(PWASCICmd, Init(WAID));
  WAPEICmd := New(PWAPEICmd, Init(WAID));
  WAPMICmd := New(PWAPMICmd, Init(WAID));
  WAPRICmd := New(PWAPRICmd, Init(WAID));

  tgrpDB := New(PDBFile, Init(DBTSTGRPFile, '', dbOpenNormal));
  if tgrpDB = nil then
  begin
    ShowError(aParent, IDS_WACPSR_ERR, nil, dbLastOpenError, MOD_WACPSR, 2);
    Done;
    Fail;
  end;

  resList := New(PResults, Init);
  if resList = nil then
  begin
    ShowError(aParent, IDS_WACPSR_ERR, nil, MOD_WACPSR+2, MOD_WACPSR, 0);
    Done;
    Fail;
  end;

  listObj:= New(PListObject, Init);
  if listObj = nil then
  begin
    ShowError(aParent, IDS_WACPSR_ERR, nil, MOD_WACPSR+3, MOD_WACPSR, 0);
    Done;
    Fail;
  end;

  mnmListRec := New(PMnemListRec, Init);
  if mnmListRec = nil then
  begin
    ShowError(aParent, IDS_WACPSR_ERR, nil, MOD_WACPSR+4, MOD_WACPSR, 0);
    Done;
    Fail;
  end;

  exceptions := New(PExceptions, Init);
end;

destructor TCPSRObj.Done;
begin
  MSDisposeObj(exceptions);
  MSDisposeObj(mnmListRec);
  MSDisposeObj(listObj);
  MSDisposeObj(isordDB);
  MSDisposeObj(resList);
  MSDisposeObj(tgrpDB);

  MSDisposeObj(WAPRICmd);
  MSDisposeObj(WAPMICmd);
  MSDisposeObj(WAPEICmd);
  MSDisposeObj(WASCICmd);
  MSDisposeObj(WAPSICmd);

  inherited Done;
end;

{----------------------------------------------------------------------------}
{  Mainline for printing a CPSR report.  Isolate key has been set.           }
{----------------------------------------------------------------------------}
function TCPSRObj.PrintReport: boolean;
var
  okFine  : boolean;
  udFlag  : boolean;
  seq     : TSeqNum;
  cnt     : integer;
  pstr    : array [0..9] of char;
begin
  okFine := TRUE;
  cnt := 0;
  if isoDB^.dbc^.GetFirstContains(isoDB^.dbr) then
  begin
    repeat
      resList^.LoadResults(isoDB^.dbr);
      isordDB := New(PDBAssocFile, Init(isoDB^.dbr,
                                        DBISOLATE_ISORDER_ASSOC, dbOpenNormal));
      if isordDB = nil then
      begin
        okFine := FALSE;
        ShowError(msgParent, IDS_WACPSR_ERR, nil, dbLastOpenError, MOD_WACPSR, 6);
        Break;
      end;

      if isordDB^.dbc^.GetFirst(isordDB^.dbr) then
      begin
        repeat
          Inc(cnt);
          Str(cnt, pstr);
          cancelDlg^.SetText(pstr, IDC_LBL2);
          List('');
          isordDB^.dbr^.GetField(DBISOORDTstGrpRef, @seq, SizeOf(TSeqNum));
          tgrpDB^.dbc^.GetSeq(tgrpDB^.dbr, seq);
          tgrpDB^.dbr^.GetField(DBTSTGRPUDFlag, @udFlag, SizeOf(boolean));
          if not udFlag then
            if not PrintPage then
              okFine := FALSE;
        until (not isordDB^.dbc^.GetNext(isordDB^.dbr)) or (not okFine) or (userAbort);
      end;
      MSDisposeObj(isordDB);
    until (not isoDB^.dbc^.GetNextContains(isoDB^.dbr)) or (not okFine) or (userAbort);
    cancelDlg^.SetText('', IDC_LBL2);
  end;
end;

{----------------------------------------------------------------------------}
{  Mainline for printing a CPSR page for this isolate order.                 }
{----------------------------------------------------------------------------}
function TCPSRObj.PrintPage: boolean;
begin
  PrintPage := FALSE;
  waFlags := nil;

  { Initial Setup for each page }
  exceptions^.ClearAllExceptions;
  tgrpDB^.dbr^.GetFieldAsStr(DBTSTGRPID, tstGrpID, SizeOf(tstGrpID)-1);
  resList^.traysObj^.LoadTray(tstGrpID);
  bioObj := New(PWABiotypeObject, Init(@resList^.traysObj^.tfsrec));
  if bioObj = nil then
    Exit;

  PrintPage := TRUE;
  { get the WA information on this panel - EXIT if errors}
  if GetPanelID then
  begin
    if SetUpWACmds then
    begin
      if waStat = 0 then
      begin
        { get the DB objects loaded }
        GetTheSpecimenAndPatient;

        { print the page }
        prnInfo^.body^.AddRow(r_StartGroup, 0);
        PrintDemogSection;
        PrintWASection;
        PrintDBSection;
        prnInfo^.body^.AddRow(r_EndGroup, 0);
      end;
    end;
  end;

  { clean up for each page }
  MSDisposeObj(waFlags);  { waFlags may have been created in SetUpWACmds }
  MSDisposeObj(bioObj);
end;

function TCPSRObj.GetPanelID: boolean;
var
  seq    : TSeqNum;
begin
  panelID := 0;

  wapDB^.dbc^.SetCurKeyNum(DBWAPNL_IsoOrd_KEY);
  wapDB^.dbr^.ClearRecord;
  seq := isordDB^.dbr^.GetSeqValue;
  wapDB^.dbr^.PutField(DBWAPNLIsoOrdRef, @seq);
  if wapDB^.dbc^.GetEQ(wapDB^.dbr) then
  begin
    wapDB^.dbr^.GetField(DBWAPNLID, @panelID, SizeOf(panelID));
  end;

  wapDB^.dbc^.SetCurKeyNum(DBWAPNL_ID_KEY);
  GetPanelID := (panelID <> 0);
end;

function TCPSRObj.SetUpWACmds: boolean;
Const
  RNID3_IDRec  = 20;        {Identifies BDesc record number for RNID3 panels}
var
  xpId     : Integer;
  xStatus  : byte;
  xpType   : byte;
  xslot    : byte;
  xflgs    : integer;
  xLastRead: TimeDate;

begin
  SetUpWACmds := FALSE;
  reportData := FALSE;
  waStat := 0;
  waErr := 0;
  if panelID <> 0 then
  repeat
    WAPSICmd^.SetParams(panelID);
    if not WAPSICmd^.SendWAMessage then
    begin
      waErr := WAPSICmd^.waErrNum;
      ValidResults:=False;
      Break;
    end
    else
    begin
      WAPsiCmd^.GetParams(xpId, xslot, xStatus, xpType, xflgs, xLastRead);
      ValidResults:=ValidResultSet(xpId, xLastRead);
    end;

    if WAPSICmd^.WAStatNum = 0 then
    begin
      { NOTE: waFlags will be disposed of by calling routine (PrintPage) }
      waFlags := New(PPanelFlags, Init(WAPSICmd^.recvMsg^.psiFlags));
      if waFlags^.panelInWA then
      begin
        WASCICmd^.SetParams(WAPSICmd^.recvMsg^.psiSlot);
        if not WASCICmd^.SendWAMessage then
        begin
          waErr := WASCICmd^.waErrNum;
          Break;
        end;
      end
      else
      begin
        WASCICmd^.WAStatNum := 0;
        WASCICmd^.recvMsg^.PSILastRd.sec  := 0;
        WASCICmd^.recvMsg^.PSILastRd.min  := 0;
        WASCICmd^.recvMsg^.PSILastRd.hour := 0;
        WASCICmd^.recvMsg^.PSILastRd.day  := 0;
        WASCICmd^.recvMsg^.PSILastRd.month:= 0;
        WASCICmd^.recvMsg^.PSILastRd.year := 0;
      end;
      if WASCICmd^.WAStatNum = 0 then
      begin
        WAPMICmd^.SetParams(panelID);
        if not WAPMICmd^.SendWAMessage then
        begin
          waErr := WAPMICmd^.waErrNum;
          Break;
        end;
        if WAPMICmd^.WAStatNum = 0 then
        begin
          {Get the PDL var Did_Indole value for this panel}
          BioObj^.IndWasDispd := False;
          If (resList^.traysObj^.tfsRec.IDRec = RNID3_IDRec) then
            BioObj^.IndWasDispd := IndoleDispensedFlag(PanelID);

          WAPRICmd^.SetParams(panelId);
          if not WAPRICmd^.SendWAMessage then
          begin
            waErr := WAPRICmd^.waErrNum;
            Break;
          end;
          if WAPRICmd^.WAStatNum = 0 then
            reportData := TRUE
          else
            waStat := WAPRICmd^.WAStatNum;
        end
        else
          waStat := WAPMICmd^.WaStatNum;
      end
      else
        waStat := WASCICmd^.WAStatNum;
    end
    else if WAPSICmd^.WAStatNum <> $46A then
      waStat := WAPSICmd^.WAStatNum;
  until TRUE;

  if reportData then
    Move(WAPMICmd^.recvMsg^.pmiMICs[1], MICs[1], MaxPMI);
  if waErr = 0 then
    SetUpWACmds := TRUE
  else
    ShowError(msgParent, IDS_WA_ERR, nil, waErr, MOD_WACPSR, 0);
  if waStat <> 0 then
    ShowError(msgParent, IDS_WA_ERR, nil, waStat, MOD_WACPSR, 0);
end;

procedure TCPSRObj.GetTheSpecimenAndPatient;
var
  seq    : TSeqNum;
begin
  specDB^.dbr^.ClearRecord;
  patDB^.dbr^.ClearRecord;

  isoDB^.dbr^.GetField(DBISOSpecRef, @seq, SizeOf(TSeqNum));
  if (seq <> 0) and specDB^.dbc^.GetSeq(specDB^.dbr, seq) then
  begin
    specDB^.dbr^.GetField(DBSPECPatRef, @seq, SizeOf(TSeqNum));
    if (seq <> 0) and patDB^.dbc^.GetSeq(patDB^.dbr, seq) then
      ;
  end;
end;

procedure TCPSRObj.PrintDemogSection;
{************************************************************************}
{*  Prints the isolate, specimen and patient demographics, the WA ID,   *}
{*  and the tower, slot, first and next read times, status, exceptions, *}
{*  and auto-resolved exceptions for this panel.                        *}
{************************************************************************}
const
  labelWidth = 15;
  columnOne  = 38;
  columnTwo  = 73;
var
  s       : array [0..8] of char;
  seq     : TSeqNum;
  statNum : integer;
begin
  { no section header for this section }

{ First Line }

  { specimen number/collect date }
  specDB^.dbd^.FieldName(DBSPECSpecID, fieldVal, labelWidth);
  ConcatColon(labelWidth, fieldVal);
  StrLCopy(printStr, fieldVal, printStrLen);
  specDB^.dbr^.GetFieldAsStr(DBSPECSpecID, fieldVal, (columnOne-StrLen(printStr)));
  StrLCat(printStr, fieldVal, printStrLen);
  ConcatBlanks(2, printStr);
  specDB^.dbr^.GetFieldAsStr(DBSPECCollectDate, fieldVal, (columnOne-StrLen(printStr)));
  StrLCat(printStr, fieldVal, printStrLen);
  ConcatBlanks(columnOne-StrLen(printStr), printStr);

  { Order }
  isoDB^.dbd^.FieldName(DBISOOrdRef, fieldVal, labelWidth);
  ConcatColon(labelWidth, fieldVal);
  StrLCat(printStr, fieldVal, printStrLen);
  isoDB^.dbr^.GetField(DBISOOrdRef, @seq, SizeOf(TSeqNum));
  if listObj^.FindMnemSeq(DBORDFile, mnmListRec, seq) then
    StrLCopy(fieldVal, mnmListRec^.desc, (columnTwo-StrLen(printStr)-1))
  else
    fieldVal[0] := #0;
  StrLCat(printStr, fieldVal, printStrLen);
  ConcatBlanks(columnTwo-StrLen(printStr), printStr);

  { WA Panel ID }
(*SR(IDS_PANELID, fieldVal, printStrLen); { Panel ID}      *)
  wapDB^.dbd^.FieldName(DBWAPNLID, fieldVal, labelWidth); { Panel ID}
  ConcatColon(labelWidth, fieldVal);
  StrLCat(printStr, fieldVal, printStrLen);
  Str(panelID, s);
  StrLCat(printStr, s, printStrLen);
  PrintTheLine;

{ Second Line }

  { isolate number }
  isoDB^.dbd^.FieldName(DBISOIso, fieldVal, labelWidth);
  ConcatColon(labelWidth, fieldVal);
  StrLCopy(printStr, fieldVal, printStrLen);
  isoDB^.dbr^.GetFieldAsStr(DBISOIso, fieldVal, (columnOne-StrLen(printStr)-1));
  StrLCat(printStr, fieldVal, printStrLen);
  ConcatBlanks(columnOne-StrLen(printStr), printStr);

  { panel name }
  SR(IDS_PANELTYPE, fieldVal, printStrLen); { Panel Type }
  ConcatColon(labelWidth, fieldVal);
  StrLCat(printStr, fieldVal, printStrLen);
  StrLCopy(fieldVal, tstGrpID, (columnTwo-StrLen(printStr)-1));
  StrLCat(printStr, fieldVal, printStrLen);
  ConcatBlanks(columnTwo-StrLen(printStr), printStr);

  { Tower }
  SR(IDS_TOWER, fieldVal, labelWidth); { Tower }
  ConcatColon(labelWidth, fieldVal);
  StrLCat(printStr, fieldVal, printStrLen);
  if reportData and ValidResults then
  begin
    Str((WAPSICmd^.recvMsg^.psiSlot div WA[WAID].slotsPerTower)+1, s);
    StrLCat(printStr, s, printStrLen);
  end;
  PrintTheLine;

{ Third Line }

  { Source }
  specDB^.dbd^.FieldName(DBSPECSrc, fieldVal, labelWidth);
  ConcatColon(labelWidth, fieldVal);
  StrLCopy(printStr, fieldVal, printStrLen);
  specDB^.dbr^.GetField(DBSPECSrc, @seq, SizeOf(TSeqNum));
  if listObj^.FindMnemSeq(DBSRCFile, mnmListRec, seq) then
    StrLCopy(fieldVal, mnmListRec^.desc, (columnOne-StrLen(printStr)-1))
  else
    fieldVal[0] := #0;
  StrLCat(printStr, fieldVal, printStrLen);
  ConcatBlanks(columnOne-StrLen(printStr), printStr);

  { Ward }
  specDB^.dbd^.FieldName(DBSPECWard, fieldVal, labelWidth);
  ConcatColon(labelWidth, fieldVal);
  StrLCat(printStr, fieldVal, printStrLen);
  specDB^.dbr^.GetField(DBSPECWard, @seq, SizeOf(TSeqNum));
  if listObj^.FindMnemSeq(DBWARDFile, mnmListRec, seq) then
    StrLCopy(fieldVal, mnmListRec^.desc, (columnTwo-StrLen(printStr)-1))
  else
    fieldVal[0] := #0;
  StrLCat(printStr, fieldVal, printStrLen);
  ConcatBlanks(columnTwo-StrLen(printStr), printStr);

  { Slot }
  SR(IDS_SLOT, fieldVal, labelWidth); { Slot }
  ConcatColon(labelWidth, fieldVal);
  StrLCat(printStr, fieldVal, printStrLen);
  if reportData and ValidResults then
  begin
    Str((WAPSICmd^.recvMsg^.psiSlot mod WA[WAID].slotsPerTower)+1, s);
    StrCopy(fieldVal, s);
    StrLCat(printStr, fieldVal, printStrLen);
  end;
  PrintTheLine;

{ Fourth Line }

  { Patient ID }
  patDB^.dbd^.FieldName(DBPATPatID, fieldVal, labelWidth);
  ConcatColon(labelWidth, fieldVal);
  StrLCopy(printStr, fieldVal, printStrLen);
  patDB^.dbr^.GetFieldAsStr(DBPATPatID, fieldVal, (columnOne-StrLen(printStr)-1));
  StrLCat(printStr, fieldVal, printStrLen);
  ConcatBlanks(columnOne-StrLen(printStr), printStr);

  { Test Date }
  isoDB^.dbd^.FieldName(DBISOTstDate, fieldVal, labelWidth);
  ConcatColon(labelWidth, fieldVal);
  StrLCat(printStr, fieldVal, printStrLen);
  isoDB^.dbr^.GetFieldAsStr(DBISOTstDate, fieldVal, (columnTwo-StrLen(printStr)-1));
  StrLCat(printStr, fieldVal, printStrLen);
  ConcatBlanks(columnTwo-StrLen(printStr), printStr);

  { Last Read Date and Time }
  SR(IDS_LASTREAD, fieldVal, labelWidth);
  ConcatColon(labelWidth, fieldVal);
  StrLCat(printStr, fieldVal, printStrLen);
  if reportData and ValidResults then
  begin
    FormatReadTime(WAPSICmd^.recvMsg^.PSILastRd, fieldVal, TRUE);
    StrLCat(printStr, fieldVal, printStrLen);
  end;
  PrintTheLine;

{ Fifth Line }

  { patient Name }
  SR(IDS_PATIENT_NAME, fieldVal, labelWidth);
  ConcatColon(labelWidth, fieldVal);
  StrLCopy(printStr, fieldVal, printStrLen);
  patDB^.dbr^.GetFieldAsStr(DBPATLName, fieldVal, (columnOne-StrLen(printStr)-1));
  StrLCat(printStr, fieldVal, printStrLen);
  StrLCat(printStr, ' ', printStrLen);
  patDB^.dbr^.GetFieldAsStr(DBPATFName, fieldVal, (columnOne-StrLen(printStr)-1));
  StrLCat(printStr, fieldVal, printStrLen);
  ConcatBlanks(columnOne-StrLen(printStr), printStr);

  { WA ID }
  SR(IDS_WALKAWAY, fieldVal, labelWidth); { Walkaway }
  ConcatColon(labelWidth, fieldVal);
  StrLCat(printStr, fieldVal, printStrLen);
  Str(WAID, s);
  StrLCat(printStr, s, printStrLen);
  ConcatBlanks(columnTwo-StrLen(printStr), printStr);

  { Next Read Time }
  SR(IDS_NEXTREAD, fieldVal, labelWidth);
  ConcatColon(labelWidth, fieldVal);
  StrLCat(printStr, fieldVal, printStrLen);
  if reportData and ValidResults then
  begin
    FormatReadTime(WASCICmd^.recvMsg^.PSILastRd, fieldVal, TRUE);
    StrLCat(printStr, fieldVal, printStrLen);
  end;
  PrintTheLine;

  CRLineFeed;
end;

procedure TCPSRObj.PrintWASection;
{************************************************************************}
{*  Prints the biochemicals from the WA and the calculated biotype, the *}
{*  list of organims, the MICs from the WA and the extra tests.         *}
{************************************************************************}
var
  biotypeExists : boolean;
  s             : array [0..8] of char;
  statNum       : integer;
begin
  { print the section header }
  SR(IDS_WA_HDR, printStr, printStrLen); { DATA OBTAINED DIRECTLY FROM WALKAWAY }
  PrintCenter;

  { Print the WA status, flags, exceptions and auto-resolved exceptions }
  SR(IDS_STS_HDR, printStr, printStrLen); { Status and Panel Flags : }
  PrintTheLine;

  if reportData and ValidResults then
  begin
    ConcatBlanks(3, printStr);
    GetStatNum(WAPSICmd^.recvMsg^.psiStat, statNum, waFlags);
    SR(statNum, fieldVal, printStrLen);
    StrLCat(printStr, fieldVal, printStrLen);
    PrintTheLine;
    if CheckStatus then
    begin
      reportData := FALSE;
    end
    else
    begin
      PrintWAFlags;
     {CRLineFeed;
      PrintAutoExceptions;}
    end;
  end;

  biotypeExists := PrintBiotype(TRUE) and ValidResults;
  exceptions^.AbortedMissing:=WAPSICmd^.recvMsg^.psiStat=PanelMissing;

  PrintBiochems(TRUE, biotypeExists);
  CRLineFeed;
  PrintIdentification(TRUE, biotypeExists);
  CRLineFeed;
  PrintMICs(TRUE);
  CRLineFeed;
  PrintExtraTests(TRUE);
  CRLineFeed;
  PrintExceptions;
  CRLineFeed;
end;

procedure TCPSRObj.PrintDBSection;
{************************************************************************}
{*  Prints the stored biotype, biochemicals, organism, MICs, and extra  *}
{*  tests.                                                              *}
{************************************************************************}
var
  biotypeExists : boolean;
begin
  { print the section header }
  SR(IDS_DB_HDR, printStr, printStrLen); { DATA STORED IN DMS DATABASE }
  PrintCenter;
  biotypeExists := PrintBiotype(FALSE);
  PrintBiochems(FALSE, biotypeExists);
  CRLineFeed;
  PrintIdentification(FALSE, biotypeExists);
  CRLineFeed;
  PrintMICs(FALSE);
  CRLineFeed;
  PrintExtraTests(FALSE);
end;


procedure TCPSRObj.PrintAFlag(strID: integer);
{************************************************************************}
{*  Prints a panel flag.                                                *}
{************************************************************************}
begin
  SR(strID, fieldVal, printStrLen);
  if (StrLen(printStr) + StrLen(fieldVal)) > printStrLen then
  begin
    PrintTheLine;
    ConcatBlanks(3, printStr);
  end;
  StrLCat(printStr, fieldVal, printStrLen);
  ConcatBlanks(2, printStr);
end;

procedure TCPSRObj.PrintWAFlags;
{************************************************************************}
{*  Prints the panel flags from the WA.                                 *}
{************************************************************************}
Const
  StaphFamily = 2;
  RapNegID3SetClass = 221;
Var
  SetFamily : Word;
  AFam      : Integer;
begin
  if waFlags^.flagVal <> 0 then
  begin
    ConcatBlanks(3, printStr);
    if waFlags^.powerFailure then PrintAFlag(IDS_FLG_POWER_FAIL);
    if waFlags^.ISFGrowth then PrintAFlag(IDS_FLG_ISF_GROWTH);
    if waFlags^.BadSterileWell then PrintAFlag(IDS_FLG_BAD_STERILE_WELL);
    if waFlags^.SkippedWells then PrintAFlag(IDS_FLG_SKIP_WELL);
    if waFlags^.VerifyPanelId then PrintAFlag(IDS_FLG_VERIFY_ID);
    if waFlags^.panelInWA then PrintAFlag(IDS_FLG_PANEL_IN_WA);

    {Let WAUpload take care of setting these as exceptions}
    {if waFlags^.slowGrower then
      if WAPSICmd^.recvMsg^.psiStat <> processComplete then
        if WAPSICmd^.recvMsg^.psiStat <> MICsAvailable then
          PrintAFlag(IDS_FLG_SLOW_GROWER);
    if waFlags^.possibleMRSASE then
      if WAPSICmd^.recvMsg^.psiStat <> processComplete then
      Begin
        isoDB^.dbr^.GetField(DBISOSetFamily, @setFamily, sizeof(setFamily));
        aFam := ExtractFamily(setFamily);
        If AFam = StaphFamily then
          PrintAFlag(IDS_FLG_MRSA_SE)
        Else
          PrintAFlag(IDS_FLG_VRE_Hold);
      End;}

    {then, we'll check them with OnWAExList, their}
    {WAFlags might have been cleared, and we'll miss them}
    if OnWAExList(PanelId, WAPSICmd^.recvMsg^.psiSlot, IDS_FLG_SLOW_GROWER) then
      PrintAFlag(IDS_FLG_SLOW_GROWER);
    if OnWAExList(PanelId, WAPSICmd^.recvMsg^.psiSlot, IDS_FLG_MRSA_SE) then
      PrintAFlag(IDS_FLG_MRSA_SE);
    if OnWAExList(PanelId, WAPSICmd^.recvMsg^.psiSlot, IDS_FLG_VRE_Hold) then
      PrintAFlag(IDS_FLG_VRE_Hold);

    if waFlags^.DelayedRead then PrintAFlag(IDS_FLG_DELAYED_READ);
    if waFlags^.InvSterileWell then PrintAFlag(IDS_FLG_INV_STERILE_WELL);
    if waFlags^.IDAvailOnRapid then PrintAFlag(IDS_FLG_IDS_AVAIL_RAPID);
    if waFlags^.NoDispense then
    Begin
      PrintAFlag(IDS_FLG_NO_DISPENSE);
      isoDB^.dbr^.GetField(DBISOSetFamily, @setFamily, sizeof(setFamily));
      If ((ExtractSet(SetFamily)*10) + ExtractFamily(SetFamily)) = RapNegID3SetClass then
        Exceptions^.NeedIndole := TRUE;
    End;

    {** if rapid anaerobe panel or rapid yeast panels **}
    if (StrComp(tstGrpID, 'RAID') = 0) or
       (StrComp(tstGrpID, 'RYID') = 0) then
    begin
      if (waFlags^.BadDCB) then
      begin
        waFlags^.BadNPC := TRUE;
        PrintAFlag(IDS_FLG_BAD_NPC);
      end
      else if (waFlags^.BadOFB) then
      begin
        waFlags^.BadBNAC := TRUE;
        PrintAFlag(IDS_FLG_BAD_BNAC);
      end
    end
    else
    begin
      if waFlags^.BadDCB then PrintAFlag(IDS_FLG_BAD_DCB);
      if waFlags^.BadOFB then PrintAFlag(IDS_FLG_BAD_OFB);
    end;
  end;
  PrintTheLine;
end;

function TCPSRObj.micNeedsOrg: Boolean;
var ValidSlot  : Boolean;
    ShouldCare : Boolean;
    DataReady  : Boolean;
    userOrg    : Integer;
    Flag       : Integer;
    Seq        : TSeqNum;
begin
  With WAPSICmd^.recvMsg^ do
  begin
    ValidSlot  := Not ( (psiStat=PanelUnrecognized) or (psiStat=AbortedPanel) );

    if ValidSlot then
    begin
      userOrg := 0;
      isoDB^.dbr^.GetField(DBISOOrgRef, @Seq, SizeOf(TSeqNum));
      if listObj^.FindMnemSeq(DBORGFile, mnmListRec, Seq) then
         Val(mnmListRec^.ID, userOrg, Flag);
    end;

    ShouldCare := ValidSlot and (userOrg=0);
    DataReady  := (psiStat=processComplete) or (psiStat=MICsAvailable);
    micNeedsOrg:= ShouldCare and DataReady;
  end;
end;


function TCPSRObj.OnWAExList(pId, WASlot, pExCode: Integer): Boolean;
var  WAExDB   : PDBFile;
     pTower   : Integer;
     pSlot    : Integer;
     IsoOrdSeq: TSeqNum;

     ExPId    : Integer;
     ExTower  : Integer;
     ExSlot   : Integer;
     WAExCode : Integer;
     ExFound  : Boolean;
     Stop     : Boolean;
begin
  {Resolve the proper Tower/Slot}
  Stop:=False;
  ExFound:=False;
  pTower:=(WASlot div WA[WAID].slotsPerTower)+1;
  pSlot :=(WASlot mod WA[WAID].slotsPerTower)+1;

  {Use the ISOOrdSeq, to avoid chances of collision }
  IsoOrdSeq:=IsordDB^.dbr^.GetSeqValue;

  {Access the WAException list DB}
  WAExDB := New(PDBFile, Init(DBWAEXFile, '', dbOpenNormal));
  WAExDB^.dbc^.SetCurKeyNum(DBWAEX_Slot_KEY);
  WAExDB^.dbr^.ClearRecord;
  WAExDB^.dbr^.PutField(dbWAExTower     , @pTower);
  WAExDB^.dbr^.PutField(dbWAExSlot      , @pSlot);
  WAExDB^.dbr^.PutField(dbWAExIsoOrdRef , @IsoOrdSeq);

  {Is this pid/slot/tower/isoord/exceptioncode combination in the WAExDB?}
  if WAExDB^.dbc^.GetGE(WAExDB^.dbr) then
  While not (ExFound or Stop) do
  begin
    WAExDB^.dbr^.GetField(dbWAEXPanelID, @ExPId  , SizeOf(ExPId  ));
    WAExDB^.dbr^.GetField(dbWAExTower  , @ExTower, SizeOf(ExTower));
    WAExDB^.dbr^.GetField(dbWAExSlot   , @ExSlot , SizeOf(ExSlot ));
    WAExDB^.dbr^.GetField(DBWAEXStrNum , @WAExCode,SizeOf(WAExCode));

    ExFound:=(ExPId=pId) and (ExTower=pTower) and
             (ExSlot=pSlot) and (WAExCode=pExCode);

    Stop:= ( (ExPId<>pId) or (ExTower<>pTower) or (ExSlot<>pSlot) ) or
           not (WAExDB^.dbc^.GetNext(WAExDB^.dbr));
  end;

  {Release mem/lock, return result}
  MSDisposeObj(WAExDB);
  OnWAExList:=ExFound;
end;

function TCPSRObj.PrintBiotype(fromWA: boolean): boolean;
{************************************************************************}
{* Print the biotype from the WA or stored in DB.                       *}
{* If from WA, then this routine assumes a PRI and a PMI have already   *}
{* been called.  If not fromWA then this routine expects the results    *}
{* object to be initialized and loaded.                                 *}
{************************************************************************}
var
  gdBiotype  : boolean;
  res        : array[0..20] of char;
  oxi, ind,
  betaHem    : integer;  {00-no test, 10-neg, 11-pos}
  setFamily  : word;
  aFam       : integer;
begin
  PrintBiotype := FALSE;
  SR(IDS_BIOTYPE_HDR, printStr, printStrLen); { BIOTYPE : }
  if resList^.traysObj^.tfsrec.idRec = -1 then
  begin
    exceptions^.NeedOrg := micNeedsOrg;
    PrintTheLine;
    Exit;
  end;

  ConcatBlanks(3, printStr);
  if fromWA and reportData and ValidResults then
  begin
    if (WAPSICmd^.recvMsg^.psiStat = processComplete) or
       ((WAPSICmd^.recvMsg^.psiStat = LampFailure) and
        resList^.traysObj^.TrayFlo) then
    begin
      Move(WAPRICmd^.recvMsg^.priBits[1], trayBits[0], 12);
      if GetVirtualTests then
      begin
        oxi := 0;                      (* use GetBiotype w/ no prompt ??? *)
        betaHem := 0;
        ind := 0;
        if resList^.GetResult('.OXI', 'OXI', res, 20) and
           (StrLen(res) > 0) then
        begin
          SetBit(oxi, 1); {set result exists bit}
          if StrComp(res, '+') = 0 then
            SetBit(oxi, 0); {set result bit to pos}
        end;
        if resList^.GetResult('.IND', 'IND', res, 20) and
           (StrLen(res) > 0) then
        begin
          SetBit(ind, 1); {set result exists bit}
          if StrComp(res, '+') = 0 then
            SetBit(ind, 0); {set result bit to pos}
        end;
        if resList^.GetResult('.HEM', 'HEM', res, 20) and
           (StrLen(res) > 0) then
        begin
          SetBit(betaHem, 1); {set result exists bit}
          if StrComp(res, '+') = 0 then
            SetBit(betaHem, 0); {set result bit to pos}
        end;

        gdBiotype := bioObj^.ComputeBiotype(WAPRICmd^.recvMsg^.priClass,
                                           @trayBits, nil,
                                           PMICData(@MICs),
                                           oxi, ind, betaHem,
                                           @virtualTests);
        PrintBiotype := TRUE;
        if gdBiotype then
        begin
          StrPCopy(fieldVal, bioObj^.biotype);
          StrLCat(printStr, fieldVal, printStrLen);

          if bioObj^.GetTopID(orgNum, nil, nil, nil) then
             if (bioObj^.Identify^.IDList.NumOrgs <= 0) then
                 exceptions^.NoID := TRUE;
        end
        else
        begin
          if (bioObj^.oxidaseBitNum <> -1) and
             TestBioBit(bioObj^.neededResults, bioObj^.oxidaseBitNum) then
            exceptions^.NeedOxid := TRUE; { Needs Oxidase result }
          if (bioObj^.indoleBitNum <> -1) and
             TestBioBit(bioObj^.neededResults, bioObj^.indoleBitNum) then
            exceptions^.NeedIndole := TRUE;
        end;
      end;
    end;
  end
  else if not fromWA then
  begin
    resList^.GetResult('.BIO', 'BIO', fieldVal, 20);
    if StrLen(fieldVal) > 0 then
    begin
      StrLCat(printStr, fieldVal, printStrLen);
      { Set up the biotype object for printing the biochemicals }
      isoDB^.dbr^.GetField(DBISOSetFamily, @setFamily, sizeof(setFamily));
      aFam := ExtractFamily(setFamily);
      bioObj^.SetBioClass(aFam);
      bioObj^.SetBiotype(aFam, fieldVal);
      PrintBiotype := TRUE;
    end;
  end;

  PrintTheLine;
end;

procedure TCPSRObj.PrintBiochems(fromWA: boolean; biotypeExists: boolean);
{************************************************************************}
{* Print the biochems from the WA or stored in DB.                      *}
{************************************************************************}
const
  MaxColWidth = 7;  { Column format is "*BBBB+ " }
var
  line1, line2, line3 : PChar;
  i, j                : integer;
  value, well, group  : integer;
begin
  If FromWA then
    SR(IDS_BIOCHEM_WA_HDR, printStr, printStrLen)  { BIOCHEMICALS : (Biochecmicals ...}
  Else
    SR(IDS_BIOCHEM_DB_HDR, printStr, printStrLen); { BIOCHEMICALS : (Biochecmicals ...}
  PrintTheLine;

  if fromWA and not reportData then
    Exit;
  if resList^.traysObj^.tfsrec.idRec = -1 then
    Exit;
  if not biotypeExists then
    Exit;

  GetMem(line1, printStrLen+1);
  GetMem(line2, printStrLen+1);
  GetMem(line3, printStrLen+1);
  StrCopy(line1, ' ');
  StrCopy(line2, ' ');
  StrCopy(line3, ' ');
  j := 0;
  for i := bioObj^.MaxBitNum downto 0 do
  begin
    Inc(j);
    if bioObj^.BitInfo(i, value, well, group, addr(fieldVal[1])) then
    begin
      If FromWA {WalkAway data not stored data} and
         AtypicalBiochemResult(i,BioObj^.BioType,bioObj^.Identify^.AtypicalPos,bioObj^.Identify^.AtypicalNeg) and
         (bioObj^.Identify^.IDList.NumOrgs > 0) {at least one organism was found} then
        FieldVal[0] := '*'
      Else
        FieldVal[0] := ' ';
      ConcatBlanks(MaxColWidth-StrLen(fieldVal), fieldVal);
      case value of
        -1 :
          begin
            if bioObj^.tfsRec^.pSet = 1 then
              resList^.GetResult('.OXI', 'OXI', @fieldVal[5], 20)
            else if bioObj^.tfsRec^.pSet = 3 then
              resList^.GetResult('.IND', 'IND', @fieldVal[5], 20)
            else
              fieldVal[5] := ' ';
          end;
         0 : fieldVal[5] := '-';
         1 : fieldVal[5] := '+';
      end;
    end
    else
                        {1234567}
                        {*BBBB+ }
      StrCopy(fieldVal, '       ');
    case j mod 3 of
      0 : StrLCat(line3, fieldVal, printStrLen);
      1 : StrLCat(line1, fieldVal, printStrLen);
      2 : StrLCat(line2, fieldVal, printStrLen);
    end;
  end;
  StrLCopy(printStr, line1, printStrLen);
  PrintTheLine;
  StrLCopy(printStr, line2, printStrLen);
  PrintTheLine;
  StrLCopy(printStr, line3, printStrLen);
  PrintTheLine;
  MSFreeMem(line1, printStrLen+1);
  MSFreeMem(line2, printStrLen+1);
  MSFreeMem(line3, printStrLen+1);
end;

procedure TCPSRObj.PrintAnException(strNum: integer);
begin
  ConcatBlanks(2, printStr);
  SR(strNum, fieldVal, printStrLen);
  if StrLen(printStr) + StrLen(fieldVal) > printStrLen then
  begin
    PrintTheLine;
    printStr[0] := CHR(0);
    ConcatBlanks(3, printStr);
  end;
  StrLCat(printStr, fieldVal, printStrLen);
end;

procedure TCPSRObj.PrintExceptions;
{************************************************************************}
{*  Prints the panel exceptions.                                        *}
{************************************************************************}
begin
  SR(IDS_EXCEPTIONS_HDR, printStr, printStrLen); { EXCEPTIONS }
  If ValidResults then
  begin
    if exceptions^.NeedSts then
      PrintAnException(IDS_EXC_NEEDS_STS);
    if exceptions^.NeedGms then
      PrintAnException(IDS_EXC_NEEDS_GMS);
    if exceptions^.NeedTfg then
      PrintAnException(IDS_EXC_NEEDS_TFG);
    if exceptions^.NeedBLac then
      PrintAnException(IDS_EXC_NEEDS_BLAC);
    if exceptions^.NeedOxac then
      PrintAnException(IDS_EXC_NEEDS_OXAC);
    if exceptions^.NeedOxid then
      PrintAnException(IDS_EXC_NEEDS_OXID);
    if exceptions^.NeedOrg then
      PrintAnException(IDS_EXC_NEEDS_ORG);
    if exceptions^.NoID then
      PrintAnException(IDS_EXC_NO_ID);
    if exceptions^.IDTooLow then
      PrintAnException(IDS_EXC_ID_TOO_LOW);
    if exceptions^.AbortedMissing then
      PrintAnException(IDS_Exc_Aborted_Missing);
    if OnWAExList(PanelId, WAPSICmd^.recvMsg^.psiSlot, IDS_SAVE_FAILED) then
      PrintAnException(IDS_SAVE_FAILED);
    if OnWAExList(PanelId, WAPSICmd^.recvMsg^.psiSlot, IDS_EXC_PossibleESBL) then
      PrintAnException(IDS_EXC_PossibleESBL);
    if OnWAExList(PanelId, WAPSICmd^.recvMsg^.psiSlot, IDS_EXC_PossibleE157) then
      PrintAnException(IDS_EXC_PossibleE157);
  end;
  PrintTheLine;
end;

(*
procedure TCPSRObj.PrintAutoExceptions;
{************************************************************************}
{*  Prints the panel auto-resolved exceptions.                          *}
{************************************************************************}
begin
  GetStr(StrBase+12, printStr, printStrLen); { AUTO-RESOLVED EXCEPTIONS }
  PrintTheLine;
{** fill in **}
end;
*)

procedure TCPSRObj.PrintIdentification(fromWA: boolean; biotypeExists: boolean);
{************************************************************************}
{* Print the organism list from the WA or stored organism from the DB.  *}
{* If from WA then this routine assumes the bioObj has been initialized *}
{* and the biotype already calculated, and also, a WAPRICommand has     *}
{* been issued.                                                         *}
{************************************************************************}
const
  columnOne         = 32;
  AddTestTextOffset = 44;  { Offset of additional test text into the title line }
  GramPosStrep      = 21;  { Set classes for comparison }
  GramPosStaph      = 22;  { Set classes for comparison }
var
  i         : integer;
  j         : integer;
  IDLineStr : string;
  seq       : TSeqNum;
  OrgList   : TOrgList;
  TmpAdtFtnSpc    : string;   { Formatted additional tests, footnotes, special chars }
  AddTestList     : TAddTestList; { Additional tests for separation if identification }
  TheFootnotes    : string;   { Organism footnotes }
  TheSpecialChars : string;   { Organism special characteristics }
  OrgSetClass     : Integer;  { SetClass of stored organism }
  IsoSetClass     : Integer;  { SetClass of stored isolate }
  SetFamily       : Word;     {}
  TmpStr          : array [0..120] of char;
begin
  if fromWA and reportData then
  begin
    SR(IDS_IDENTIFICATION_HDR, printStr, printStrLen); { IDENTIFICATION : }
    if resList^.traysObj^.tfsrec.idRec = -1 then
    begin
      orgNum := 0;
      isoDB^.dbr^.GetField(DBISOOrgRef, @seq, SizeOf(TSeqNum));
      if listObj^.FindMnemSeq(DBORGFile, mnmListRec, seq) then
        Val(mnmListRec^.ID, orgNum, i);    { needed for extra test results }
      PrintTheLine;
      Exit;
    end;
    if (not biotypeExists) or (exceptions^.NeedIndole or
        exceptions^.needOxid or exceptions^.undefinedBiotype) then
    begin
      PrintTheLine;
      Exit;
    end;

    ConcatBlanks(3, printStr);
    if not bioObj^.GetTopID(i, nil, nil, nil) then
    begin
      if bioObj^.Identify^.InvalidBiotype then
      begin
        SR(IDS_INVALID_BIOTYPE, fieldVal, printStrLen); {Invalid Biotype}
      end
      else
      begin
        SR(IDS_UNDEF_BIOTYPE, fieldVal, printStrLen); {'Undefined Biotype'}
      end;
      StrLCat(printStr, fieldVal, printStrLen);
      PrintTheLine;
    end
    else if bioObj^.Identify^.IDList.NumOrgs <= 0 then
    begin
      StrLCat(printStr, '<<', printStrLen);
      SR(IDS_RARE_BIOTYPE, fieldVal, printStrLen); { Very Rare Biotype }
      StrLCat(printStr, fieldVal, printStrLen);
      StrLCat(printStr, '>>', printStrLen);
      PrintTheLine;
    end
    else
    begin
      PrintTheLine;
      with bioObj^.Identify^.IDList do
      begin
        orgNum := ReportOrgs.P^[1];
        if (PROBS.P^[1] <= 85) then
          exceptions^.IDTooLow := TRUE;
        { Get the additional tests }
        For j := 1 To NumOrgs Do
          OrgList[j] := ReportOrgs.P^[j];
        OrgObject.ReportAddTests(Exceptions^.IDTooLow,NumOrgs,1,OrgList,AddTestList);
        SR(IDS_ORG_LIST_HDR1, printStr, printStrLen); { Organism Name ...}
        { insert the additional test titles into the title line }
        TmpAdtFtnSpc := Concat(AddTestList.Title[1],' ',
                               AddTestList.Title[2],' ',
                               AddTestList.Title[3],'  ',chr(0));
        PrintStr := StrLCat(PrintStr,Addr(TmpAdtFtnSpc[1]),printStrLen);
        SR(IDS_ORG_LIST_HDR2, TmpStr, printStrLen); { Footnotes...}
        PrintStr := StrLCat(PrintStr,TmpStr,printStrLen);
        PrintTheLine;
        SR(IDS_ORG_LIST_HDR3, printStr, printStrLen); { ------------- ... }
        PrintTheLine;
        For i := 1 to NumOrgs do
        begin
          printStr[0] := CHR(0);
          ConcatBlanks(3, printStr);
          Str(i, IDLineStr);
          StrPCopy(fieldVal, IDLineStr);
          StrLCat(printStr, fieldVal, printStrLen);
          ConcatBlanks(2, printStr);
          StrPCopy(fieldVal, TOrgNames(OrgObject.OrgNames.P^[ ReportOrgs.P^[i] ]^).Name);
          StrLCat(printStr, fieldVal, printStrLen);
          ConcatBlanks(columnOne-(StrLen(printStr)), printStr);
          Str(Probs.P^[i]:4:1, IDLineStr);
          if IDLineStr = '100.0' then
            IDLineStr := '99.9';
          StrPCopy(fieldVal, IDLineStr);
          StrLCat(printStr, fieldVal, printStrLen);
          OrgObject.ReportFootNotes(NumOrgs,i,OrgList,BioObj^.BioType,TheFootnotes);
          OrgObject.ReportSpclChars(ReportOrgs.P^[i],30,TheSpecialChars);
          TmpAdtFtnSpc := '      ';
          For j := 1 to 3 do
            TmpAdtFtnSpc := concat(TmpAdtFtnSpc,AddTestList.Prob[i,j],' ');
          TmpAdtFtnSpc :=  concat('  ',TmpAdtFtnSpc,' ',TheFootnotes,'   ',TheSpecialChars,chr(0));
          StrLCat(printStr,addr(TmpAdtFtnSpc[1]),printStrLen);
          PrintTheLine;
        end;
      end;
    end;
  end
  else if not fromWA then { get from the database }
  begin
    SR(IDS_ORG_HDR, printStr, printStrLen); { ORGANISM : }
    StrLCat(printStr, ' ', printStrLen);
    {Get the name of the stored organism}
    isoDB^.dbr^.GetField(DBISOOrgRef, @seq, SizeOf(TSeqNum));
    if listObj^.FindMnemSeq(DBORGFile, mnmListRec, seq) then
    Begin
      StrLCat(printStr, mnmListRec^.desc, printStrLen);
      {Verify organism is appropriate for isolate family}
      Val(mnmListRec^.ID, orgNum, i);
      OrgSetClass := Ord(TOrgNames(OrgObject.OrgNames.P^[ OrgNum ]^).SC);
      If OrgSetClass = 0 then
      Begin
        OrgObject.FindOrgInNameTable(OrgNum);
        OrgSetClass := Ord(OrgObject.OrgBuff.SC);
      End;
      isoDB^.dbr^.GetField(DBISOSetFamily, @setFamily, sizeof(setFamily));
      IsoSetClass := (ExtractSet(SetFamily)*10) + ExtractFamily(SetFamily);
      if (resList^.traysObj^.tfsrec.idRec <> (-1)) and                      {Panel is not MIC only}
         ((IsoSetClass = GramPosStaph) or (IsoSetClass = GramPosStrep)) and {Isolate is gram pos}
         (IsoSetClass <> OrgSetClass) then                                  {SC of stored isolate does not match SC of Org}
      Begin
        SR(IDS_ERR_Stored_Org_Family, TmpStr, PrintStrLen); { Stored organism does not match iso...}
        StrLCat(PrintStr, ' ', PrintStrLen);
        StrLCat(PrintStr, TmpStr, PrintStrLen);
      End;
    End;
    PrintTheLine;
  end;
end;

procedure TCPSRObj.PrintMICs(fromWA: boolean);
{************************************************************************}
{* Print the MICs from the WA or stored in DB. THis routine assumes a   *}
{* PMI command has alreay been issued.                            *}
{************************************************************************}
const
  MaxColWidth = 6;
var
  MICLine, DilLine, NCCLSLine : PChar;
  drugDils     : DrugDilType;
  dilCnt       : integer;
  i            : integer;
  Abbr         : string;
  sortedRes    : PGatherTGRes;
begin
  GetMem(MICLine, printStrLen+1);
  GetMem(DilLine, printStrLen+1);
  GetMem(NCCLSLine, printStrLen+1);
  StrCopy(MICLine, '   ');
  StrCopy(DilLine, '   ');
  StrCopy(NCCLSLine, '   ');
  SR(IDS_MICS_HDR, printStr, printStrLen); { MICS : (Antimicrobics... }
  PrintTheLine;

  if fromWA and reportData then
  begin
    if (WAPSICmd^.recvMsg^.psiStat = processComplete) or
       (WAPSICmd^.recvMsg^.psiStat = MICsAvailable) or
       ((WAPSICmd^.recvMsg^.psiStat = LampFailure) and
        resList^.traysObj^.TrayFlo) then
      if ValidResults then
      with resList^.traysObj^ do
        for i := 1 to DrugCnt do
        begin
          StrCopy(fieldVal, dilList[i].drugname);
          ConcatBlanks((MaxColWidth-StrLen(fieldVal)), fieldVal);
          StrLCat(MICLine, fieldVal, printStrLen);
          StrCopy(fieldVal, dilList[i].drugDils[MICs[i]]);
          ConcatBlanks((MaxColWidth-StrLen(fieldVal)), fieldVal);
          StrLCat(DilLine, fieldVal, printStrLen);
          if ((i mod 16) = 0) or (StrLen(MICLine) >= printStrLen) or
             (i = DrugCnt) then
          begin
            StrLCopy(printStr, MICLine, printStrLen);
            PrintTheLine;
            StrLCopy(printStr, DilLine, printStrLen);
            PrintTheLine;
            if not (i = DrugCnt) then
              CRLineFeed;
            StrCopy(MICLine, '   ');
            StrCopy(DilLine, '   ');
          end;
        end;
  end
  else if not fromWA then
  begin
    sortedRes := New(PGatherTGRes, Init(resList, tstGrpID, listObj));
    if sortedRes = nil then
    begin
      ShowError(msgParent, IDS_WACPSR_ERR, nil, MOD_WACPSR+6, MOD_WACPSR, 0);
      Exit;
    end;

    for i := 0 to sortedRes^.NumMICResults - 1 do
    begin
      sortedRes^.GetMICDrugAbbr(i, fieldVal, MaxColWidth);
      ConcatBlanks((MaxColWidth-StrLen(fieldVal)), fieldVal);
      StrLCat(MICLine, fieldVal, printStrLen);

      fieldVal[0] := #0;
      printStr^ := #0;
      sortedRes^.GetMICResult(i, fieldVal, printStr, MaxColWidth);
      ConcatBlanks((MaxColWidth-StrLen(fieldVal)), fieldVal);
      ConcatBlanks((MaxColWidth-StrLen(printStr)), printStr);
      StrLCat(DilLine, fieldVal, printStrLen);
      StrLCat(NCCLSLine, printStr, printStrLen);
      if (((i+1) mod 16) = 0) or (StrLen(MICLine) >= printStrLen) or
         (i = (sortedRes^.NumMICResults - 1)) then
      begin
        StrLCopy(printStr, MICLine, printStrLen);
        PrintTheLine;
        StrLCopy(printStr, DilLine, printStrLen);
        PrintTheLine;
        StrLCopy(printStr, NCCLSLine, printStrLen);
        PrintTheLine;
        CRLineFeed;
        StrCopy(MICLine, '   ');
        StrCopy(DilLine, '   ');
        StrCopy(NCCLSLine, '   ');
      end;
    end;
    MSDisposeObj(sortedRes);
  end;
  MSFreeMem(MICLine, printStrLen+1);
  MSFreeMem(DilLine, printStrLen+1);
  MSFreeMem(NCCLSLine, printStrLen+1);
end;

procedure TCPSRObj.PrintExtraTests(fromWA: boolean);
{************************************************************************}
{* Print the extra tests from the WA or stored in DB.                   *}
{* Meaning of the numbers in trayTable returned by GetXTra.             *}
{*  4 = StS,  5 = Oxi,  7 = Bl,  8 = GmS,  9 = TFG.                     *}
{************************************************************************}
var
  i, wellNum, extraResult : integer;
  xTraCnt     : integer;
  xTraTable   : XTraType;
  testID      : array [0..5] of char;
  ESBLResult  : array [0..4] of char;
  PrintESBL   : boolean;
  f           : Text;
  LocalBits   : TPanelBitmap;
begin
  SR(IDS_EXTRA_TESTS_HDR, printStr, printStrLen); { EXTRA TESTS : }
  ConcatBlanks(3, printStr);
  resList^.traysObj^.GetXTra(xTraTable, xTraCnt);

  PrintESBL:= (not fromWA) and
              ResList^.GetESBLResult(ESBLResult,4) and
              (StrComp(ESBLResult,'')<>0);

  {Do not print extra test info from the WA until the panel completes}
  if fromWA and (WAPSICmd^.recvMsg^.psiStat <> processComplete) then
     xTraCnt:=0;

  if (xTraCnt = 0) and (not PrintESBL) then
  begin
    SR(IDS_NONE, fieldVal, printStrLen); { None }
    StrLCat(printStr, fieldVal, printStrLen);
    PrintTheLine;
    Exit;
  end;

{$I-}
  Assign(f, 'DataList.TXT');
  Reset(f);
  if IOResult <> 0 then
  begin
    ShowError(msgParent, IDS_WACPSR_ERR, nil, MOD_WACPSR+5, MOD_WACPSR, 0);
    Exit;
  end;

  Move(WAPRICmd^.recvMsg^.priBits, LocalBits, 12);
  for i := 1 to xTraCnt do
  begin
    case xTraTable[i] of
      4 : StrCopy(testID, '.StS');
      5 : StrCopy(testID, '.OXI');
      7 : StrCopy(testID, '.BL');
      8 : StrCopy(testID, '.GmS');
      9 : StrCopy(testID, '.TFG');
      else
        StrCopy(testID, '????');
    end;
    if fromWA and reportData then
    begin
      if ValidResults then
      begin
        extraResult := GetETResult(i, testID, xTraTable, resList,
                                   orgNum, f, @LocalBits, PMICData(@MICs));
        if (extraResult = 1) or (extraResult = 2) then
        begin
                            { blank out period }
          StrLCat(printStr, @testID[1], printStrLen);
          ConcatBlanks(1, printStr);
          case extraResult of
            1 : StrLCat(printStr, '-', printStrLen);
            2 : StrLCat(printStr, '+', printStrLen);
          end;
          ConcatBlanks(3, printStr);
        end
        else
        if (extraResult=3) and (orgNum <> -1) and not resList^.traysObj^.TrayFlo then
        begin
          if StrComp(testID,'.StS') = 0 then
            exceptions^.NeedStS := TRUE
          else if StrComp(testID,'.BL') = 0 then
            exceptions^.NeedBLac := TRUE
          else if StrComp(testID,'.GmS') =  0 then
            exceptions^.NeedGmS := TRUE
          else if StrComp(testID,'.TFG') = 0 then
            exceptions^.NeedTFG := TRUE;
        end;
      end;
    end
    else if not fromWA then  { get info from the database }
    begin
      resList^.GetResult(testID, @testID[1], fieldVal, 20);
      if StrLen(fieldVal) > 0 then
      begin
        StrLCat(printStr, @testID[1], printStrLen);
        ConcatBlanks(1, printStr);
        StrLCat(printStr, fieldVal, printStrLen);
        ConcatBlanks(3, printStr);
      end;
    end;
  end;

  {Print ESBL as an xtra test}
  if PrintESBL then
  begin
    StrCat(printStr,'ESBL');
    StrCat(printStr, ' ');
    StrCat(printStr,ESBLResult);
  end;

  PrintTheLine;
  Close(f);
  if IOResult <> 0 then ;
{$I+}
end;

function TCPSRObj.GetVirtualTests: boolean;
begin
  GetVirtualTests := TRUE;
  FillChar(virtualTests[0], SizeOf(TPanelBitmap), Chr(0));
  if reportData then
  begin
    if waFlags^.VirtualTests then
    begin
      WAPEICmd^.SetParams(panelID);
      { get the extra tests from the WA }
      if WAPEICmd^.SendWAMessage then
      begin
        Move(WAPEICmd^.recvMsg^.priBits[1], virtualTests[0], SizeOf(PRIArray));
      end
      else
      begin
        SR(IDS_ERR_WA_COMM, printStr, printStrLen); { Error communicating with W/A }
        PrintTheLine;
        GetVirtualTests := FALSE;
      end;
    end;
  end;
end;

function TCPSRObj.CheckStatus: boolean;
begin
  CheckStatus := FALSE;
  case WAPSICmd^.recvMsg^.psiStat of
    SlotEmpty : CheckStatus := TRUE;
    PanelUnrecognized : CheckStatus := TRUE;
    AbortedPanel : CheckStatus := TRUE;
    PanelMissing : CheckStatus := TRUE;
    LampFailure : CheckStatus := TRUE;
  end;
end;


{ EXPORTED PROCEDURES }

{----------------------------------------------------------------------------}
{  Controls the printing of each isolate order in the selected session.      }
{----------------------------------------------------------------------------}
procedure PrintCPSR(aParent: PWindowsObject; dbIso, dbPat, dbSpec, dbWap: PDBFile);
var
  cpsr    : PCPSRObj;
begin
(* if dbIso is passed in, the rec must cleared and session Seq number put in the field *)
(* may need to test session seq # if GetFirstContains does not work properly *)

  cpsr := New(PCPSRObj, Init(aParent, dbIso, dbPat, dbSpec, dbWap));
  if cpsr = nil then
  begin
    ShowError(aParent, IDS_WACPSR_ERR, nil, MOD_WACPSR+1, MOD_WACPSR, 0);
    Exit;
  end;
  cpsr^.InitHeadings;
  cpsr^.AllowCancel;
  cpsr^.PrintReport;
  MSDisposeObj(cpsr);
end;


END.
