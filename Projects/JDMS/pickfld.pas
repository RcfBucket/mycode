{----------------------------------------------------------------------------}
{ Module Name  : PickFld.PAS                                                 }
{ Programmer   : EJ                                                          }
{ Date Created : 7/18/1995                                                   }
{ Requirements : S3.28 [ref. a]                                              }
{                                                                            }
{ Purpose - This unit provides a dialog box for selecting a user field in a  }
{   specimen record [ref. b].                                                }
{                                                                            }
{ Assumptions                                                                }
{    a.
{                                                                            }
{ Limitations                                                                }
{    a.  Only one dialog box should be created at a time.                    }
{                                                                            }
{ Referenced Documents                                                       }
{    a. S3.28 Design Document, Revision 2                                    }
{    b. Database Design Document, Revision 0                                 }
{                                                                            }
{ Revision History - This project is under version control, use it to view   }
{    revision history.                                                       }
{                                                                            }
{----------------------------------------------------------------------------}

unit PickFld;

INTERFACE

uses
  OWindows;


function ChooseSpecimenField( aParent: PWindowsObject ): Word;


IMPLEMENTATION

uses
  Objects,
  ODialogs,
  WinProcs,
  WinTypes,
  MScan,
  Strings,
  DlgLib,
  DBIDs,
  DBLib,
  DBFile,
  DBTypes,
  DMSDebug;

{$I PICKFLD.INC}
{$R PICKFLD.RES}

type
  {--------------------------------------------------------------------------}
  { This record is the dialog box's transfer buffer.
  {--------------------------------------------------------------------------}
  TXferBuf = Record
    FieldList: PStrCollection;
    SelectedField: integer;
  end;

  {--------------------------------------------------------------------------}
  { This object represents the dialog box.
  {--------------------------------------------------------------------------}
  PChooseFldDlg = ^TChooseFldDlg;
  TChooseFldDlg = object( TCenterDlg )
    FieldList: PListBox;
    constructor Init( AParent:PWindowsObject );
    procedure SetupWindow; virtual;
    procedure List( var msg: TMessage ); virtual id_First + IDC_FIELD_LIST;
    procedure EnableButtons; virtual;
  end;



{----------------------------------------------------------------------------}
{ This variable limits this unit to only one instance.
{----------------------------------------------------------------------------}
var
  db: PDBFile;



{----------------------------------------------------------------------------}
{ The constructor initializes the dialog box.
{----------------------------------------------------------------------------}

constructor TChooseFldDlg.Init(AParent: PWindowsObject);
begin
  inherited Init(AParent, 'PickFld');
  FieldList := New( PListBox, InitResource( @Self, IDC_FIELD_LIST ) );
end;



{----------------------------------------------------------------------------}
{ This override calls the base class and loads the list with applicable field
{ names.
{----------------------------------------------------------------------------}

procedure TChooseFldDlg.SetupWindow;
var
  k: word;
  fi: TFldInfo;
begin
  inherited SetupWindow;
  for k := 1 to db^.dbd^.NumFields do
  begin
    db^.dbd^.FieldInfo( k, fi );
    if ( fi.userFlag and db_Hidden ) <> 0 then
      Continue;
    SendDlgItemMsg( IDC_FIELD_LIST, lb_AddString, 0, longint( @fi.fldName ) );
  end;
end;



{----------------------------------------------------------------------------}
{ This message handler was added to the list control to enable the OK button
{ when an item is selected.
{----------------------------------------------------------------------------}

procedure TChooseFldDlg.List( var msg: TMessage );
begin
  if msg.lParamHi = LBN_SELCHANGE then
    EnableButtons
  else if msg.lParamHI = LBN_DBLCLK then
    OK( msg );
  DefWndProc( msg );
end;



{----------------------------------------------------------------------------}
{ This procedure updates the state of the OK button depending on the
{ selection state of the list control.
{----------------------------------------------------------------------------}

procedure TChooseFldDlg.EnableButtons;
var
  bEnable: boolean;
begin
  bEnable := SendDlgItemMsg( IDC_FIELD_LIST, lb_GetCurSel, 0, 0 ) <> lb_Err;
  EnableWindow( GetItemHandle( IDOK ), bEnable );
end;



                        {----------------------}
                        {   EXPORT procedure   }
                        {----------------------}



{----------------------------------------------------------------------------}
{ This function uses a dialog box to prompt the user for a field selection.
{ If the user selects one, the function returns the field number, otherwise
{ it returns 0 indicating the user canceled.
{----------------------------------------------------------------------------}

function ChooseSpecimenField( aParent: PWindowsObject ): word;
var
  d: PChooseFldDlg;
  xfer: TXferBuf;
  retVal: word;
  k: word;
  fi: TFldInfo;
begin
  db := New( PDBFile, Init( DBSPECFile, '', dbOpenNormal ) );
  d := New( PChooseFldDlg, Init( aParent ) );
  FillChar(xfer, SizeOf( xfer ), #0 );
  xfer.FieldList := New( PStrCollection, Init( 10, 10 ) );
  d^.TransferBuffer := @xfer;
  retVal := Application^.ExecDialog( d );
  if retVal = IDOK then
  begin
    retVal := Xfer.SelectedField + 1;
    for k := 1 to db^.dbd^.NumFields do
    begin
      db^.dbd^.FieldInfo( k, fi );
      if ( fi.userFlag and db_Hidden ) <> 0 then
        Continue;
      Dec( retVal );
      if retVal = 0 then
        Break;
    end;
    ChooseSpecimenField := k;
  end
  else
  begin
    ChooseSpecimenField := 0;
  end;
  MSDisposeObj( xfer.FieldList );
  MSDisposeObj( db );
end;

END.
