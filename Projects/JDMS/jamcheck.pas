{----------------------------------------------------------------------------}
{  Module Name  : JamCheck.PAS                                               }
{  Programmer   : EJ                                                         }
{  Date Created : 05/28/95                                                   }
{                                                                            }
{  Purpose -                                                                 }
{  This module provides the system with functions related to WA              }
{  communication and jam checking.                                           }
{                                                                            }
{  Assumptions -                                                             }
{  None.                                                                     }
{                                                                            }
{  Revision History -                                                        }
{                                                                            }
{  Version  Date      Who    Did What                                        }
{  -------  --------  ------ ----------------------------------------------- }
{  1.00     05/28/95  EJ     Initial release.                                }
{                                                                            }
{----------------------------------------------------------------------------}

unit JamCheck;

INTERFACE

uses
  OWindows,
  WinTypes,
  WAGPIB;

const
  { WalkAway Error Constants - all in lo word }
  WaBCSError                  = $00000001;
  WaJam                       = $00000002;
  WaAccessTimeExpired         = $00000004;
  WaTempTooLow                = $00000008;
  WaTempTooHigh               = $00000010;
  WaDispensePressureLow       = $00000020;
  WaColorLampFailure          = $00000040;
  WaSystemNotReady            = $00000080;
  WaCantTalk                  = $00008000;

  { Walkaway Status Constants - all in hi word }
  WaBCSInProgress             = $00010000;
  WaSterilizationInProgress   = $00020000;
  WaBCSDone                   = $00040000;
  WaDataAvailable             = $00080000;


type
  PJamCheckerWindow = ^TJamCheckerWindow;
  TJamCheckerWindow = object(TWindow)
    constructor Init(aParent: PWindowsObject; aTitle: PChar; id: integer);
    procedure SetupWindow; virtual;
    procedure TimerTick(var Msg: TMessage); virtual wm_First + wm_Timer;
    procedure WMDestroy(var Msg: TMessage); virtual wm_First + wm_Destroy;
    procedure SetWaStatusWindow(Dlg: HWnd; BA: PChar);
    {}
    private
    TimerSet       : boolean;
    MessageUp      : boolean;
    WaStatus       : longint;
    StatusShowing  : longint;  { The status corresponding to the message
                                 currently on the screen }
    CurrentMsg     : array [0..128] of char;
    WindowForText  : HWnd;
    BuffAddr       : PChar;
    pvtID          : integer;
    {}
    procedure CheckWaNow;
    procedure ClearCommError;
    procedure SetWaStatus(CommStat: word; var StiStuff: StiArray);
    function  SetUpWAType: boolean;
  end;

  function GetWaStatus(id: integer): longint;
  function CheckComm(id: integer): integer;

var
  TheJamChecker   : PJamCheckerWindow;

IMPLEMENTATION

uses
  WinProcs,
  DlgLib,
  Strings,
  StrsW,
  APITools,
  Bits,
  MScan,
  UserMsgs,
  DMSDebug;

{$I WAMAIN.INC}
{$R JAM.RES}

const
  JamBase       = MOD_JAMCHECK;  { Base of strings in JAM.RES }
  TimerInterval = 5000;          { Check for jam every 5 seconds }
  id_JamTimer         = 589;

var
  commErrorMsgUp : boolean;


{-----------------------------------------------------------------------}
{                                                                       }
{-----------------------------------------------------------------------}
function GetWaStatus(id: integer): longint;
begin
  GetWaStatus := WaCantTalk;
  if (id = 1) and (TheJamChecker <> nil) then
  begin
    TheJamChecker^.CheckWaNow;
    GetWaStatus := TheJamChecker^.WaStatus;
  end;
end;


{-----------------------------------------------------------------------}
{                                                                       }
{-----------------------------------------------------------------------}
function CheckComm(id: integer): integer;
var
  currentWAStat : longint;
  retWindow     : HWnd;
  tempStr       : array [0..64] of char;
  tempStr2      : array [0..64] of char;
begin
  CheckComm := TryAgain;

  currentWAStat := GetWaStatus(id);
  if ((currentWAStat and WASystemNotReady) = WASystemNotReady) or
      (((currentWAStat and WABCSInProgress) = WABCSInProgress) and
       ((currentWAStat and WAJam) <> WAJam)) then
    Exit;

  if ((currentWAStat and WACantTalk) = WACantTalk) then { bc 1/21/92 }
  begin
    if not commErrorMsgUp then
    begin
      commErrorMsgUp := TRUE;
      retWindow := GetActiveWindow;
      SR(IDS_ERR_TITLE, tempStr, 64);
      SR(IDS_RETRY_COMM, tempStr2, 64);
      if YesNoMsg(retWindow, tempStr, tempStr2) then
      begin
        if TheJamChecker <> nil then
          TheJamChecker^.ClearCommError;
      end
      else
        CheckComm := QuitTrying;
(*    SetFocus(focusWndw); *)
      commErrorMsgUp := FALSE;
    end
    else
      CheckComm := QuitTrying;
  end
  else
    CheckComm := AOK;
end;


{-----------------------------------------------------------------------}
{                                                                       }
{-----------------------------------------------------------------------}
constructor TJamCheckerWindow.Init(aParent: PWindowsObject; aTitle: PChar; id: integer);
begin
  inherited Init(aParent, aTitle);
  if id = -1 then
  begin
    Done;
    Fail;
  end;
  pvtID := id;
  TimerSet := FALSE;
  MessageUp := FALSE;
  CurrentMsg[0] := Chr(0);
  WindowForText := 0;
  BuffAddr := nil;
  SetUpWAType;
  if (WA[pvtID].slotsPerTower = 0) then
    WaStatus := WaCantTalk
  else
    WaStatus := 0;
  StatusShowing := not WaStatus; { to force it to be displayed }
  TheJamChecker := @Self;
end;

{-----------------------------------------------------------------------}
{                                                                       }
{-----------------------------------------------------------------------}
procedure TJamCheckerWindow.SetupWindow;
var
  Result      : integer;
  PStr, PStr2 : array [0..50] of char;
begin
  inherited SetupWindow;
  Result := IDRetry;
  SR(JamBase + 19, PStr, 50);    { Could not create timer }
  SR(JamBase + 20, PStr2, 50);   { Jam Checker }
  while (SetTimer(hWindow, id_JamTimer, TimerInterval, nil) = 0) and (Result = IDRetry) do
    Result := MessageBox(GetActiveWindow, PStr, PStr2, mb_RetryCancel);
  if Result = IDCancel then
    PostQuitMessage(0)
  else
    TimerSet := TRUE;
end;

{-----------------------------------------------------------------------}
{                                                                       }
{-----------------------------------------------------------------------}
procedure TJamCheckerWindow.TimerTick(var Msg: TMessage);
begin
  if TimerSet then
    KillTimer(HWindow, id_JamTimer);
  CheckWaNow;
  if TimerSet then
    SetTimer(HWindow, id_JamTimer, TimerInterval, nil);
end;

{-----------------------------------------------------------------------}
{                                                                       }
{-----------------------------------------------------------------------}
procedure TJamCheckerWindow.WMDestroy(var Msg: TMessage);
begin
  if TimerSet then
    KillTimer(HWindow, id_JamTimer);
  inherited WMDestroy(Msg);
end;

{-----------------------------------------------------------------------}
{                                                                       }
{-----------------------------------------------------------------------}
procedure TJamCheckerWindow.SetWaStatusWindow(Dlg: HWnd; BA: PChar);
var
  DummySti : StiArray;
begin
  WindowForText := Dlg;
  BuffAddr := BA;

  if Dlg <> 0 then
  begin
    { if the can't-talk bit is already set, then just put up
      a message saying there's a communications error.
      Otherwise check the wa normally. }
    if (WaStatus and WaCantTalk) <> 0 then
      SetWaStatus(WaCantTalk, DummySti);
  end;
end;

{-----------------------------------------------------------------------}
{                                                                       }
{-----------------------------------------------------------------------}
procedure TJamCheckerWindow.CheckWaNow;
var
  Result        : integer;
  WACmd         : PWASTICmd;
  CarCmd        : PWaCarCmd;           { Lock door again }
  PiezoCmd      : PWAROMCMDCmd;        { To turn off WA alarm }
  HomeCmd       : PWAHomCmd;
  OriginalFocus : HWnd;
  DummySti      : StiArray;
  WaError       : boolean;

  {-----------------------------------------------------------------------}
  procedure DoRegularDialogs;
  begin
    Result := Application^.ExecDialog(New(PCenterDlg, Init(@Self, 'CLASSICJAM1')));
  end;

  {-----------------------------------------------------------------------}
  procedure DoDispenserDialogs;
  begin
    Result := Application^.ExecDialog(New(PCenterDlg, Init(@Self, 'DispenserJam')));
  end;

begin
  WaError := FALSE;
  if MessageUp then
    Exit;
  if (WaStatus and WaCantTalk) <> 0 then
    Exit;  { bc 1/21/92 }
  if WA[pvtID].slotsPerTower = 0 then
  begin
    SetWaStatus(WaCantTalk, DummySti);    { Wa can't talk }
    Exit;
  end;

  WACmd := New(PWASTICmd, Init(pvtID));
  WACmd^.SetErrorMode(1);      { Don't put up a DMS error box }
  WACmd^.SendWAMessage;
  if (WACmd^.WAGpibErrNum <> 0) or (WaCmd^.WAErrNum = -1) then
  begin
    SetWaStatus(WaCantTalk, DummySti);
    WaError := TRUE;
  end
  else
  begin
    SetWaStatus(WACmd^.RecvMsg^.Status, WACmd^.RecvMsg^.StiBits);
    WaError := FALSE;
  end;
  Dispose(WACmd, Done);

  if (not WaError) and ((WaStatus and WaJam) <> 0) then
  begin
    MessageUp := TRUE;
    OriginalFocus := GetFocus;

    PiezoCmd := New(PWAROMCMDCmd, Init(pvtID));
    PiezoCmd^.SetParams($31, 0);   { Turns off alarm }
    PiezoCmd^.SendWAMessage;
    Dispose(PiezoCmd, Done);

    if WA[pvtID].waType <> ClassicWa then
    begin
      Result := Application^.ExecDialog(New(PCenterDlg,
                                        Init(@Self, 'DISPENSERAREA')));
      if Result = IDOK then
        DoDispenserDialogs
      else
        DoRegularDialogs;
    end
    else
      DoRegularDialogs;

    HomeCmd := New(PWAHomCmd, Init(pvtID));
    HomeCmd^.SendWAMessage;
    Dispose(HomeCmd, Done);

    Result := Application^.ExecDialog(New(PCenterDlg,
                                          Init(@Self, 'ClassicJamCleared')));
    if WA[pvtID].waType <> ClassicWa then
      Result := Application^.ExecDialog(New(PCenterDlg,
                                            Init(@Self, 'WaXXGoodbye')))
    else
      Result := Application^.ExecDialog(New(PCenterDlg,
                                        Init(@Self, 'WaClassicGoodbye')));
    CarCmd := New(PWaCarCmd, Init(pvtID));
    CarCmd^.SendWaMessage;          { Lock door }
    Dispose(CarCmd, Done);

    SetFocus(OriginalFocus);
    MessageUp := FALSE;
  end;
end;

{-----------------------------------------------------------------------}
{                                                                       }
{-----------------------------------------------------------------------}
procedure TJamCheckerWindow.ClearCommError;
begin
  if WA[pvtID].slotsPerTower = 0 then
    SetUpWAType;
  if WA[pvtID].slotsPerTower <> 0 then
    WaStatus := WaStatus and (not WaCantTalk);
end;

{-----------------------------------------------------------------------}
{                                                                       }
{-----------------------------------------------------------------------}
procedure TJamCheckerWindow.SetWaStatus(CommStat: word; var StiStuff: StiArray);
var
  tStr, tStr2  : array [0..255] of char;
  StatusString : array [0..128] of char;

  function PostStatus: boolean;
  begin
    PostStatus := FALSE;
    if StatusString[0] = #0 then
      SR(JamBase + 2, StatusString, 128);   { OK }
    if (BuffAddr <> nil) and
       (StrComp(CurrentMsg, StatusString) <> 0) then
    begin
      StrCopy(BuffAddr, StatusString);
      if WindowForText <> 0 then
      begin
        PostMessage(WindowForText, WM_WASTATUS, 0, longint(BuffAddr));
        StrCopy(CurrentMsg, StatusString);
        PostStatus := TRUE;
      end;
    end;
  end;

  procedure AddStatus(S: PChar);
  begin
    if StatusString[0] <> #0 then
      StrLCat(StatusString, ', ', 128);
    StrLCat(StatusString, S, 128);
  end;

  {-----------------------------------------------------------------------}
  procedure CheckError (StiBit: integer; WaStatusBit: longint; BadMsg, GoodMsg: PChar);
  var
    NewState, OldState : boolean;
  begin
    NewState := TestBit(StiStuff, StiErr) and TestBit(StiStuff, StiBit);
    OldState := (WaStatus and WaStatusBit) <> 0;
    if NewState and (not OldState) then { Error just happened! }
    begin
      if (StatusShowing and WaStatusBit) = 0 then
        AddStatus(BadMsg);

      WaStatus := WaStatus or WaStatusBit;
    end
    else if OldState and (not NewState) then { Error was just cleared }
    begin
      if (StatusShowing and WaStatusBit) <> 0 then
        AddStatus(GoodMsg);

      WaStatus := WaStatus and (not WaStatusBit);
    end;
  end;

  {-----------------------------------------------------------------------}
  procedure CheckStatus(StiBit, StiNotBit: integer; WaStatusBit: longint; Mess: PChar);
  var
    NewState, OldState: boolean;
  begin
    if (WaStatus and WaCantTalk) <> 0 then
    begin
      if StatusShowing <> WaCantTalk then
      begin
        SR(JamBase + 3, tStr, 255);    { Unable to communicate }
        AddStatus(tStr);
      end;
      Exit;
    end;
    NewState := TestBit(StiStuff, StiBit);
    if StiNotBit <> 0 then
      NewState := NewState and (not (TestBit(StiStuff, StiNotBit)));
    OldState := (WaStatus and WaStatusBit) <> 0;
    if NewState and (not OldState) then { Status just came on }
    begin
      if (StatusShowing and WaStatusBit) = 0 then
        AddStatus(Mess);

      WaStatus := WaStatus or WaStatusBit;
    end
    else if OldState and (not NewState) then { Status just turned off }
    begin
      WaStatus := WaStatus and (Not WaStatusBit);
    end;
  end;

begin
  StatusString[0] := #0;

  if CommStat <> 0 then
    WaStatus := WaStatus or WaCantTalk
  else
    WaStatus := WaStatus and (not WaCantTalk);

  if (WaStatus and WaCantTalk) <> 0 then
  begin
    SR(JamBase + 3, tStr, 255);  { Unable to communicate }
    CheckStatus(0, 0, 0, tStr);
  end
  else
  begin
    SR(JamBase + 17, tStr, 255);     { System not ready }
    CheckError(SystemNotReady, WaSystemNotReady, tStr, '');
    if (WaStatus and WaSystemNotReady) = 0 then
    begin
      SR(JamBase + 10, tStr, 255);     { WalkAway Jam }
      SR(JamBase + 11, tStr2, 255);    { WalkAway jam cleared }
      CheckError(StiJam, WaJam, tStr, tStr2);
      if (WaStatus and WaJam) = 0 then
      begin
        SR(JamBase + 16, tStr, 255);     { Colorimetric lamp failure }
        CheckError(ColorLampFailure, WaColorLampFailure, tStr, '');
        SR(JamBase + 15, tStr, 255);     { Dispense pressure low }
        CheckError(DispensePressureLow, WaDispensePressureLow, tStr, '');
        SR(JamBase + 14, tStr, 255);     { Temperature too low }
        CheckError(TempTooLow, WaTempTooHigh, tStr, '');
        SR(JamBase + 13, tStr, 255);     { Temperature too high }
        CheckError(TempTooHigh, WaTempTooLow, tStr, '');
        SR(JamBase + 12, tStr, 255);     { Access time expired }
        CheckError(AccessTimeExpired, WaAccessTimeExpired, tStr, '');
        SR(JamBase + 8, tStr, 255);      { Bar code scan error }
        SR(JamBase + 9, tStr2, 255);     { Bar code scan error cleared }
        CheckError(BarCodeScanErrors, WaBCSError, tStr, tStr2);

        SR(JamBase + 4, tStr, 255);      { Bar code scan in progress }
        CheckStatus(AccessNixed, BarCodeScanDone, WaBCSInProgress, tStr);
        SR(JamBase + 5, tStr, 255);      { Sterilization in progress }
        CheckStatus(Sterilizing, 0, WaSterilizationInProgress, tStr);
        SR(JamBase + 7, tStr, 255);      { Data available }
        CheckStatus(DataAvailable, 0, WaDataAvailable, tStr);
      end;
    end;
  end;

  if PostStatus then
    StatusShowing := WaStatus;
end;

{-----------------------------------------------------------------------}
{                                                                       }
{-----------------------------------------------------------------------}
function TJamCheckerWindow.SetUpWAType: boolean;
var
  WACmd : PWAGetWATypeCmd;
begin
  SetUpWAType := FALSE;
  WACmd := New(PWAGetWATypeCmd, Init(pvtID));
  WA[pvtID].slotTotal := 0;
  WA[pvtID].slotsPerTower := 0;
  if WACmd^.SendWAMessage then
  begin
    SetUpWAType := TRUE;
    WA[pvtID].slotsPerTower := 12;
    WA[pvtID].slotTotal := 96;
    WACmd^.GetParams(WA[pvtID].waType);
  end;
  Dispose(WACmd, Done);
end;


BEGIN
  TheJamChecker := nil;
  commErrorMsgUp := FALSE;
END.
