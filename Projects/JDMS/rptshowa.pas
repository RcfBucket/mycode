unit RptShowAll;

INTERFACE

uses
  DMSDebug,
  ApiTools,
  DlgLib,
  BoxWin,
  GridLib,
  Objects,
  ODialogs,
  OWindows,
  WinProcs,
  WinTypes;

{$I RPTGEN.INC}

type
  PShowAllDlg = ^TShowAllDlg;
  TShowAllDlg = object(TCenterDlg)
    boxes     : PCollection;
    grid      : PGrid;
    selIdx    : ^integer;
    title     : PChar;
    constructor Init(AParent: PWindowsObject; aTitle: PChar;
                     aBoxes: PCollection; var aSelIdx: integer);
    procedure SetupWindow; virtual;
    procedure WMMeasureItem(var msg: TMessage); virtual WM_FIRST + WM_MEASUREITEM;
    procedure WMDrawItem(var msg: TMessage); virtual WM_FIRST + WM_DRAWITEM;
    procedure WMCharToItem(var msg: TMessage); virtual WM_FIRST + WM_CHARTOITEM;
    procedure IDDefine(var msg: TMessage); virtual ID_FIRST + IDC_DEFINE;
    procedure IDAlign(var msg: TMessage); virtual ID_FIRST + IDC_ALIGN;
    procedure IDRemove(var msg: TMessage); virtual ID_FIRST + IDC_REMOVE;
    procedure OK(var msg: TMessage); virtual ID_FIRST + ID_OK;
    procedure CheckButtons;
    procedure IDGrid(var msg: TMessage); virtual ID_FIRST + IDC_LIST;
  end;

IMPLEMENTATION

constructor TShowAllDlg.Init(AParent: PWindowsObject; aTitle: PChar;
                             aBoxes: PCollection; var aSelIdx: integer);
begin
  inherited Init(aParent, MakeIntResource(DLG_SHOWALL));
  grid:= New(PGrid, InitResource(@self, IDC_LIST, 1, false));
  boxes:= aBoxes;
  selIdx:= @aSelIdx;
  selIdx^:= -1;
  title:= aTitle;
end;

procedure TShowAllDlg.SetupWindow;
var
  pstr  : array[0..100] of char;

  procedure AddOne(b: PBoxWin); far;
  var
    row   : longint;
  begin
    b^.GetName(pstr, 100);
    row:= grid^.AddString('');
    grid^.SetData(0, row, pstr);
  end;

begin
  inherited SetupWindow;
  SetWindowText(HWindow, title);

  grid^.StretchColumn(0);
  grid^.EnableHeader(false);
  grid^.EnableResize(false);

  boxes^.ForEach(@AddOne);

  grid^.SetSelIndex(0);
  CheckButtons;
end;

procedure TShowAllDlg.IDDefine(var msg: TMessage);
var
  k     : integer;
  pstr  : array[0..100] of char;
  b     : PBoxWin;
begin
  k:= grid^.GetSelIndex;
  if k >= 0 then
  begin
    b:= boxes^.At(k);
    b^.DefineBox(@self);
    b^.GetName(pstr, 100);
    grid^.SetData(0, k, pstr);
    InvalidateRect(b^.HWindow, nil, false);
  end;
end;

procedure TShowAllDLg.IDAlign(var msg: TMessage);
var
  k     : integer;
  pstr  : array[0..100] of char;
  b     : PBoxWin;
begin
  k:= grid^.GetSelIndex;
  if k >= 0 then
  begin
    b:= boxes^.At(k);
    b^.AlignBox(@self);
  end;
end;

procedure TShowAllDlg.IDRemove(var msg: TMessage);
var
  k     : integer;
  pstr  : array[0..100] of char;
  b     : PBoxWin;
begin
  k:= grid^.GetSelIndex;
  if k >= 0 then
  begin
    b:= boxes^.At(k);
    if b^.DeleteBox(@self) then
    begin
      grid^.DeleteString(k);
      CheckButtons;
    end;
  end;
end;

procedure TShowAllDlg.OK(var msg: TMessage);
var
  k     : integer;
  pstr  : array[0..100] of char;
  b     : PBoxWin;

  procedure KillFoc(box: PBoxWin); far;
  begin
    SendMessage(box^.HWindow, WM_KILLFOCUS, 0, 0);
  end;

begin
  k:= grid^.GetSelIndex;
  if k >= 0 then
  begin
(*    boxes^.ForEach(@KillFoc);
    b:= boxes^.At(k);
    PostMessage(b^.HWindow, WM_SETFOCUS, 0, 0);  *)
    selIdx^:= k;
  end;
  inherited OK(msg);
end;

procedure TShowAllDlg.WMMeasureItem(var msg: TMessage);
begin
  grid^.MeasureItem(msg);
end;

procedure TShowAllDlg.WMDrawItem(var msg: TMessage);
begin
  grid^.DrawItem(msg);
end;

procedure TShowAllDlg.WMCharToItem(var msg: TMessage);
begin
  grid^.DrawItem(msg);
end;

procedure TShowAllDlg.CheckButtons;
var
  k   : longint;
begin
  k:= grid^.GetCount;
  EnableWindow(GetItemHandle(IDC_DEFINE), k > 0);
  EnableWindow(GetItemHandle(IDC_REMOVE), k > 0);
  EnableWindow(GetItemHandle(IDC_ALIGN), k > 0);
  EnableWindow(GetItemHandle(IDOK), k > 0);
end;

procedure TShowAllDlg.IDGrid(var msg: TMessage);
var
  k   : integer;
begin
  if msg.lParamHi = LBN_DBLCLK then
  begin
    k:= grid^.GetCount;
    if k >= 0 then
      OK(msg);
  end;
  DefWndProc(msg);
end;

END.

{- rcf }
