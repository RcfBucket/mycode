Unit IntSend;

Interface

Uses
  ApiTools,
  WinTypes,
  Objects,
  ODialogs,
  OWindows,
  Strings,
  WinCrt,
  IntGlob,
  Builder,
  IntMap,
  FileIO,
  IntIO,
  ResLib,
  Log,
  LogEvent,
  IntAuto,
  IntQuick,
  WinProcs,
  DBFile,
  DBLib,
  DBTypes;

const
	id_PatientRanges  = 101;
	id_SpecimenRanges = 102;
	id_CollectDateRanges = 103;
	id_TestDateRanges = 104;
	id_StartRangeLable = 105;
	id_EndRangeLable = 106;
	id_StartRangeText = 107;
	id_EndRangeText = 108;
	id_Send = 109;
	id_TransmitFlag = 110;
	id_SaveRanges = 111;
	id_AutoRanges = 112;
	id_QuickRanges = 113;


	InterfaceTempFile = 'INTXFER.FIL';
	SenderID: array[0..9] of char = 'MicroScan';
  ProcessingID = 'P';

Type
	TFldCmpRep = record
								 FieldNo: integer;
								 CompNo : integer;
								 RepNo  : integer;
               end;

	PGetRangesDlg = ^TGetRangesDlg;
	TGetRangesDlg = object(TDialog)
    StartLable, EndLable: array[0..30] of char;
		procedure SetupWindow;virtual;
		procedure SpecimenRanges(var Msg: TMessage);virtual id_First + id_SpecimenRanges;
		procedure PatientRanges(var Msg: TMessage);virtual id_First + id_PatientRanges;
		procedure CollectDateRanges(var Msg: TMessage);virtual id_First + id_CollectDateRanges;
		procedure TestDateRanges(var Msg: TMessage);virtual id_First + id_TestDateRanges;
		procedure Send(var Msg: TMessage);virtual id_First + id_Send;
  	procedure SaveRanges(var Msg: TMessage);virtual id_First + id_SaveRanges;
		procedure FormatRanges;virtual;
    procedure GetRanges;virtual;
		procedure SetAutoRanges(var Msg: TMessage);virtual id_First + id_AutoRanges;
		procedure SetQuickRanges(var Msg: TMessage);virtual id_First + id_QuickRanges;
    procedure SetRanges;virtual;
		function RangesBlank: boolean;virtual;
	end;

  procedure IntSend_SendUserData(CurrentWnd: HWnd; PWindowsObjPtr: PWindowsObject);
  procedure IntSend_SendRequestedData(CurrentWnd: HWnd; WndObjPtr: PWindowsObject);
  procedure IntSend_SendRequest(CurrentWnd: HWnd; WndObjPtr: PWindowsObject);

Implementation

Uses
  Bits,
  DBIds,
  DMString,
  IntlLib,
  ListLib,
  MPSS,
  MPSSObjs,
  MScan,
  IntCstm,
  Trays;

var
  SendFile: Text;
  IsolateFile, SpecimenFile, PatientFile: PDBFile;
  CurrPatientID: array[0..MaxPatIDLen] of char;
  CurrSpecimenID: array[0..MaxSpecIDLen] of char;
  CurrCollectDate: array[0..MaxDateLen] of char;
  CurrIsolateID: array[0..MaxIsoIDLen] of char;
  CurrOrderID: array[0..MaxOrderIDLen] of char;
  listObj: PListObject;
  listRec: PMnemListRec;

{
*******************
GetRangesDlg Object
*******************
}

procedure TGetRangesDlg.SetupWindow;
var
  TempMsg: array[0..40] of char;

begin
  inherited SetupWindow;
  SendDlgItemMsg(id_PatientRanges, bm_SetCheck, 1, 0);
  LoadString(HInstance, str_IntBase+str_BegPatID, TempMsg, SizeOf(TempMsg));
  StrCopy(StartLable, TempMsg);
  SendDlgItemMsg(id_StartRangeLable, wm_SetText, 0, longint(@StartLable));
  LoadString(HInstance, str_IntBase+str_EndPatID, TempMsg, SizeOf(TempMsg));
  StrCopy(EndLable, TempMsg);
  SendDlgItemMsg(id_EndRangeLable, wm_SetText, 0, longint(@EndLable));
end;

procedure TGetRangesDlg.SpecimenRanges(var Msg: TMessage);
var
  TempMsg: array[0..40] of char;

begin
  LoadString(HInstance, str_IntBase+str_BegSpcID, TempMsg, SizeOf(TempMsg));
  StrCopy(StartLable, TempMsg);
  SendDlgItemMsg(id_StartRangeLable, wm_SetText, 0, longint(@StartLable));
  LoadString(HInstance, str_IntBase+str_EndSpcID, TempMsg, SizeOf(TempMsg));
  StrCopy(EndLable, TempMsg);
  SendDlgItemMsg(id_EndRangeLable, wm_SetText, 0, longint(@EndLable));
end;

procedure TGetRangesDlg.PatientRanges(var Msg: TMessage);
var
  TempMsg: array[0..40] of char;

begin
  LoadString(HInstance, str_IntBase+str_BegPatID, TempMsg, SizeOf(TempMsg));
  StrCopy(StartLable, TempMsg);
  SendDlgItemMsg(id_StartRangeLable, wm_SetText, 0, longint(@StartLable));
  LoadString(HInstance, str_IntBase+str_EndPatID, TempMsg, SizeOf(TempMsg));
  StrCopy(EndLable, TempMsg);
  SendDlgItemMsg(id_EndRangeLable, wm_SetText, 0, longint(@EndLable));
end;

procedure TGetRangesDlg.CollectDateRanges(var Msg: TMessage);
var
  TempMsg: array[0..40] of char;

begin
  LoadString(HInstance, str_IntBase+str_BegColl, TempMsg, SizeOf(TempMsg));
  StrCopy(StartLable, TempMsg);
  SendDlgItemMsg(id_StartRangeLable, wm_SetText, 0, longint(@StartLable));
  LoadString(HInstance, str_IntBase+str_EndColl, TempMsg, SizeOf(TempMsg));
  StrCopy(EndLable, TempMsg);
  SendDlgItemMsg(id_EndRangeLable, wm_SetText, 0, longint(@EndLable));
end;

procedure TGetRangesDlg.TestDateRanges(var Msg: TMessage);
var
  TempMsg: array[0..40] of char;

begin
  LoadString(HInstance, str_IntBase+str_BegTest, TempMsg, SizeOf(TempMsg));
  StrCopy(StartLable, TempMsg);
  SendDlgItemMsg(id_StartRangeLable, wm_SetText, 0, longint(@StartLable));
  LoadString(HInstance, str_IntBase+str_EndTest, TempMsg, SizeOf(TempMsg));
  StrCopy(EndLable, TempMsg);
  SendDlgItemMsg(id_EndRangeLable, wm_SetText, 0, longint(@EndLable));
end;

function TGetRangesDlg.RangesBlank: boolean;
var
  tstart, tend: array[0..30] of char;

begin
  SendDlgItemMsg(id_StartRangeText, wm_GetText, SizeOf(tstart), longint(@tstart));
  SendDlgItemMsg(id_EndRangeText, wm_GetText, SizeOf(tend), longint(@tend));
  if (StrComp(tstart, '') = 0) and (StrComp(tend, '') = 0) then
    RangesBlank:= True
	else
    RangesBlank:= False;
end;

procedure TGetRangesDlg.SetRanges;
var
  PatChk, SpcChk, ColChk, TstChk: integer;
  msg: TMessage;

begin
  PatChk:= 0;
  SpcChk:= 0;
  ColChk:= 0;
  TstChk:= 0;
	case Ranges.RequestType[0] of
    'P': begin
          PatChk:= 1;
          PatientRanges(msg); {msg is required, but not used}
         end;
    'C': begin
          SpcChk:= 1;
          SpecimenRanges(msg); {msg is required, but not used}
         end;
    'S': begin
          ColChk:= 1;
          CollectDateRanges(msg); {msg is required, but not used}
         end;
    'R': begin
          TstChk:= 1;
          TestDateRanges(msg); {msg is required, but not used}
         end;
  end;
  SendDlgItemMsg(id_PatientRanges, bm_SetCheck, PatChk, 0);
  SendDlgItemMsg(id_SpecimenRanges, bm_SetCheck, SpcChk, 0);
  SendDlgItemMsg(id_CollectDateRanges, bm_SetCheck, ColChk, 0);
  SendDlgItemMsg(id_TestDateRanges, bm_SetCheck, TstChk, 0);
  SendDlgItemMsg(id_StartRangeText, wm_SetText, 0, longint(@Ranges.StartRange));
  SendDlgItemMsg(id_EndRangeText, wm_SetText, 0, longint(@Ranges.EndRange));
  if Ranges.ReTransmit then
    SendDlgItemMsg(id_TransmitFlag, bm_SetCheck, 1, 0)
  else
    SendDlgItemMsg(id_TransmitFlag, bm_SetCheck, 0, 0);
end;

procedure TGetRangesDlg.SetAutoRanges(var Msg: TMessage);
begin
	if RangesBlank then
	begin
		IntAuto_ReadAutoRanges;
    SetRanges;
  end;
end;

procedure TGetRangesDlg.SetQuickRanges(var Msg: TMessage);
begin
	if RangesBlank then
	begin
		IntQuick_ReadQuickRanges;
    SetRanges;
	end;
end;

procedure TGetRangesDlg.FormatRanges;
begin
	with Ranges do
	begin
    if StrComp(EndRange, '') = 0 then
      StrCopy(EndRange, StartRange);
		case RequestType[0] of
			'P':begin
            FormatSpecialNumField(StartRange, MaxPatIDLen);
            FormatSpecialNumField(EndRange, MaxPatIDLen);
					end;
			'C':begin
            FormatSpecialNumField(StartRange, MaxSpecIDLen);
            FormatSpecialNumField(EndRange, MaxSpecIDLen);
					end;
		end;
	end;
end;

procedure TGetRangesDlg.GetRanges;
begin
	with Ranges do
	begin
    SendDlgItemMsg(id_StartRangeText, wm_GetText, SizeOf(StartRange), longint(@StartRange));
    SendDlgItemMsg(id_EndRangeText, wm_GetText, SizeOf(EndRange), longint(@EndRange));
    if SendDlgItemMsg(id_PatientRanges, bm_GetCheck, 0, 0) <> 0 then
      StrCopy(Ranges.RequestType, 'P')
    else
      if SendDlgItemMsg(id_SpecimenRanges, bm_GetCheck, 0, 0) <> 0 then
        StrCopy(Ranges.RequestType, 'C')
      else
        if SendDlgItemMsg(id_CollectDateRanges, bm_GetCheck, 0, 0) <> 0 then
          StrCopy(Ranges.RequestType, 'S')
				else
          StrCopy(Ranges.RequestType, 'R');
    ReTransmit:= (SendDlgItemMsg(id_TransmitFlag, bm_GetCheck, 0, 0) <> 0);
	end;
end;

procedure TGetRangesDlg.Send(var Msg: TMessage);
begin
	GetRanges;
	FormatRanges;
  if (Ranges.RequestType[0] = 'S') or (Ranges.RequestType[0] = 'R') then
  begin
    CheckDateField(Ranges.StartRange, SizeOf(Ranges.StartRange)-1);
    CheckDateField(Ranges.EndRange, SizeOf(Ranges.EndRange)-1);
	end;
	EndDlg(0);
end;

procedure TGetRangesDlg.SaveRanges(var Msg: TMessage);
var
  SaveAuto, SaveQuick: boolean;
  TempMsg: array[0..60] of char;
  TempVerMsg: array[0..25] of char;

begin
  if SendDlgItemMsg(id_AutoRanges, bm_GetCheck, 0, 0) <> 0 then
    SaveAuto:= True
	else
    SaveAuto:= False;
  if SendDlgItemMsg(id_QuickRanges, bm_GetCheck, 0, 0) <> 0 then
    SaveQuick:= True
	else
    SaveQuick:= False;
	if (not SaveAuto) and (not SaveQuick) then
	begin
    LoadString(HInstance, str_IntBase+str_SaveRange, TempMsg, SizeOf(TempMsg));
    LoadString(HInstance, str_IntBase+str_IntMsg, TempVerMsg, SizeOf(TempVerMsg));
    MessageBox(HWindow, TempMsg, TempVerMsg, mb_OK);
  	Exit;
  end;
  GetRanges;
  FormatRanges;
	if SaveAuto then
		IntAuto_SaveAutoRanges;
	if SaveQuick then
		IntQuick_SaveQuickRanges;
	EndDlg(3);
end;


{
********************
Non-Object Functions
********************
}

procedure GetFieldIdx(IntRecNo, IntIdxNo: integer;var FldCmpRep: TFldCmpRep);
var
	 FieldName: array[0..SetupNameLen] of char;
   FieldNo, CompNo, RepNo, i: integer;
	 BlankChar: char;

begin
  Assign(SetupFile, SetupFiles[IntRecNo]);
	Reset(SetupFile);
  i:= 0;
	while (not Eof(SetupFile)) and (i <= IntIdxNo) do
  begin
    readln(SetupFile, FieldNo, CompNo, RepNo, BlankChar, FieldName);
    i:= i + 1;
  end;
	Close(SetupFile);
  FldCmpRep.FieldNo:= FieldNo;
  FldCmpRep.CompNo:= CompNo;
  FldCmpRep.RepNo:= RepNo;
end;

procedure GetPatRec(PatID: PChar);
var
  curKey: integer;

begin
  PatientFile^.dbr^.ClearRecord;
  PatientFile^.dbr^.PutFieldAsStr(DBPATPatID, PatID);
  curKey:= PatientFile^.dbc^.GetCurKeyNum;
  PatientFile^.dbc^.SetCurKeyNum(DBPAT_ID_KEY);
  PatientFile^.dbc^.GetEQ(PatientFile^.dbr);
  PatientFile^.dbc^.SetCurKeyNum(curKey);
end;

procedure GetSpecRec(SpecID, CollectDate: PChar);
var
  curKey: integer;

begin
  SpecimenFile^.dbr^.ClearRecord;
  SpecimenFile^.dbr^.PutFieldAsStr(DBSPECSpecID, SpecID);
  SpecimenFile^.dbr^.PutFieldAsStr(DBSPECCollectDate, CollectDate);
  curKey:= SpecimenFile^.dbc^.GetCurKeyNum;
  SpecimenFile^.dbc^.SetCurKeyNum(DBSPEC_ID_KEY);
  SpecimenFile^.dbc^.GetEQ(SpecimenFile^.dbr);
  SpecimenFile^.dbc^.SetCurKeyNum(curKey);
end;

procedure GetIsoRec(SpecID, CollectDate, IsoID: PChar);
var
  curKey: integer;

begin
  IsolateFile^.dbr^.ClearRecord;
  IsolateFile^.dbr^.PutFieldAsStr(DBISOSpecID, SpecID);
  IsolateFile^.dbr^.PutFieldAsStr(DBISOCollectDate, CollectDate);
  IsolateFile^.dbr^.PutFieldAsStr(DBISOIso, IsoID);
  curKey:= IsolateFile^.dbc^.GetCurKeyNum;
  IsolateFile^.dbc^.SetCurKeyNum(DBISO_SpecIDCDate_KEY);
  IsolateFile^.dbc^.GetEQ(IsolateFile^.dbr);
  IsolateFile^.dbc^.SetCurKeyNum(curKey);
end;

function GetDMSField(DMSFileNo, DMSIdxNo: integer): PChar;
var
	DMSField: array[0..255] of char;
  fInfo   : TFldInfo;
  assInfo : TAssocInfo;
  seq     : TSeqNum;
  aDBFile : PDBFile;
  tBool   : boolean;

begin
  StrCopy(DMSField, '');

  case DMSFileNo of
  0:begin
      GetPatRec(CurrPatientID);
      aDBFile:= PatientFile;
    end;
  1:begin
      GetSpecRec(CurrSpecimenID, CurrCollectDate);
      aDBFile:= SpecimenFile;
    end;
  2:begin
      GetIsoRec(CurrSpecimenID, CurrCollectDate, CurrIsolateID);
      aDBFile:= IsolateFile;
    end;
	end;

  if aDBFile^.dbd^.FieldInfo(DMSIdxNo, fInfo) then
  begin
    if (fInfo.userFlag and db_Mnemonic = db_Mnemonic) then
    begin
      aDBFile^.dbr^.GetField(DMSIdxNo, @seq, SizeOf(seq));
      if (fInfo.fldAssoc > 0) then
      begin
        if aDBFile^.dbd^.AssocInfo(fInfo.fldAssoc, assInfo) then
          if listObj^.FindMnemSeq(assInfo.fName, listRec, seq) then
          begin
            if (DMSFileNo = 1) and (DMSIdxNo = DBSPECStat) then
            begin
              if (StrComp(listRec^.id, '2') = 0) then  { final }
                StrCopy(DMSField, 'Y')
              else { preliminary }
                StrCopy(DMSField, 'N');
            end
            else
              StrCopy(DMSField, listRec^.id);
          end;
      end;
    end
    else if (DMSFileNo = 2) and (DMSIdxNo = DBISOTransFlag) then
    begin
      aDBFile^.dbr^.GetField(DMSIdxNo, @tBool, SizeOf(tBool));
      if tBool then
        StrCopy(DMSField, 'T')
      else
        StrCopy(DMSField, 'N');
    end
    else if (fInfo.fldType = dbBoolean) then
    begin
      aDBFile^.dbr^.GetField(DMSIdxNo, @tBool, SizeOf(tBool));
      if tBool then
        StrCopy(DMSField, 'Y')
      else
        StrCopy(DMSField, 'N');
    end
    else if (fInfo.fldType <> dbSeqRef) then
      aDBFile^.dbr^.GetFieldAsStr(DMSIdxNo, DMSField, SizeOf(DMSField)-1);
  end;

  GetDMSField:= DMSField;
end;

function GetXRef(Index, CompNo: integer;CurrField: PChar): PChar;
const
	TrueStr: array[0..2] of char = 'T';

var
	XRefedField: array[0..255] of char;
	MnemType: array[0..2] of char;
	PrimFound: boolean;
	MnemPrimRec: TMnemPrimRec;
	MnemSearchKey: TMnemSearchKey;
  BtrvStat: integer;

begin
  StrCopy(XRefedField, CurrField);
	if (CustomCfgBuff.XRefChksCfg[Index] = bf_Checked) and
	   (CompNo = 1) then
	begin
    MnemType[0]:= XRefMnemTypes[Index];
    MnemType[1]:= #0;
    BtrvData[MnemPrimFile].Size.Len      := SizeOf(TMnemPrimRec);
    BtrvData[MnemPrimFile].FileName.Name := 'MNEMPRIM.DAT  ';
    OpenCloseFiles(ParentWnd, B_Open, [MnemPrimFile]);
    StrCopy(MnemSearchKey.MType, MnemType);
    StrCopy(MnemSearchKey.Mnemonic, XRefedField);
    BtrvStat:= CallDB(B_Get_GTEQ, MnemPrimFile, MnemPrimRec.Start, MnemSearchKey.Start, 1);
    PrimFound:= False;
		while (BtrvStat = 0) and
          (StrComp(MnemType, MnemPrimRec.MType) = 0) and
          (StrComp(XRefedField, MnemPrimRec.Mnemonic) = 0) and
					(not PrimFound) do
		begin
      if StrComp(MnemPrimRec.IsPrim, TrueStr) = 0 then
        PrimFound:= True
      else
        BtrvStat:= CallDB(B_Next, MnemPrimFile, MnemPrimRec.Start, MnemSearchKey.Start, 1);
		end;
		if PrimFound then
      StrCopy(XRefedField, MnemPrimRec.Alternate);
    OpenCloseFiles(ParentWnd, B_Close, [MnemPrimFile]);
	end;
  GetXRef:= XRefedField;
end;

function GetResultXRef(FieldNo, CompNo: integer;CurrField: PChar): PChar;
var
	Index: integer;
	XRefField: boolean;
	TempField: array[0..255] of char;

begin
  XRefField:= True;
	case FieldNo of
     3: begin
					case CompNo of
						4: Index := 9;
						5: Index := 10;
						6: Index := 11;
					else
          	XRefField := False;
          end;
        end;
     4: Index:= 12;
	else
    XRefField:= False;
  end;
	if XRefField then
    StrCopy(TempField, GetXRef(Index, 1, CurrField))
	else
    StrCopy(TempField, CurrField);
  GetResultXRef:= TempField;
end;

function GetTestOrdXRef(FieldNo, CompNo: integer;CurrField: PChar): PChar;
const
	TrueStr: string[2] = 'T';

var
	Index: integer;
	XRefField: boolean;
	TempField: array[0..255] of char;

begin
  XRefField:= True;
	case FieldNo of
		 5: if CompNo = 4 then
				begin
          Index:= 9;
          CompNo:= 1; {Reset to one, so XRef will Work}
				end
				else
          XRefField:= False;
    16: Index:= 2;
    17: Index:= 0;
    28: Index:= 1;
    30: Index:= 4;
    31: Index:= 5;
	else
    XRefField:= False;
  end;
	if XRefField then
    StrCopy(TempField, GetXRef(Index, CompNo, CurrField))
	else
    StrCopy(TempField, CurrField);
  GetTestOrdXRef:= TempField;
end;

function GetPatXRef(FieldNo, CompNo: integer;CurrField: PChar): PChar;
const
	TrueStr: string[2] = 'T';

var
	Index: integer;
	XRefField: boolean;
	TempField: array[0..255] of char;

begin
  XRefField:= True;
	case FieldNo of
     9: Index:= 7;
    14: Index:= 0;
    25: Index:= 3;
    26: Index:= 1;
    33: Index:= 4;
    34: Index:= 5;
    35: Index:= 8;
	else
    XRefField:= False;
  end;
	if XRefField then
    StrCopy(TempField, GetXRef(Index, CompNo, CurrField))
	else
    StrCopy(TempField, CurrField);
  GetPatXRef:= TempField;
end;

function XRefField(CurrIntRecNo, FieldNo, CompNo: integer;CurrField: PChar): PChar;
var
	TempField: array[0..255] of char;

begin
	case CurrIntRecNo of
    0: StrCopy(TempField, GetPatXRef(FieldNo, CompNo, CurrField));
    1: StrCopy(TempField, GetTestOrdXRef(FieldNo, CompNo, CurrField));
    2: StrCopy(TempField, GetResultXRef(FieldNo, CompNo, CurrField));
	end;
  XRefField:= TempField;
end;

procedure LogField(IntRec, FieldNo: integer; AField: PChar);
var
	TempData: array[0..255] of char;

begin
	if LogFile^.CurrLogLevel <= 1 then {this is for log level 2 or 3}
  	Exit;
	if (IntRec = 0) and (FieldNo = 3) then {already logged keys}
		Exit;
	if (IntRec = 1) and ((FieldNo = 3) or (FieldNo = 5) or
											 (FieldNo = 8)) then
    Exit;
  if StrComp(AField, '') = 0 then {don't log field if null}
    Exit;
  Str(FieldNo, TempData);
  StrCat(TempData, ': ');
  StrCat(TempData, AField);
  LogFile^.LogEvent(0, SendBit, lgevSentField, TempData, -1);
end;

procedure BuildRecord(RecordTypeID: char;IntRecNo: integer);
var
	ToIntMapRec: TToIntMapRec;
	IntKey: TIntKey;
  CurrField: array[0..255] of char;
  FieldCount, RepCount, ComCount: integer;
	FieldIdx: TFldCmpRep;
	i: integer;

begin
  IntKey.IntRecNo:= IntRecNo;
  IntKey.IntIdxNo:= 0;
  FieldCount:= 2;
  RepCount:= 1;
  ComCount:= 1;
  BtrvStat:= CallDB(B_Get_GTEQ, ToIntMapFile, ToIntMapRec.Start, IntKey.Start, 2);
	if (BtrvStat = 0) and (ToIntMapRec.IntRecNo = IntRecNo) then
  begin
		Builder_PutChr(RecordTypeID);
  	Builder_PutNulFields(1);
  end;
	while (BtrvStat = 0) and (ToIntMapRec.IntRecNo = IntRecNo) do
	begin
    StrCopy(CurrField, GetDMSField(ToIntMapRec.DMSFileNo, ToIntMapRec.DMSIdxNo));
    GetFieldIdx(ToIntMapRec.IntRecNo, ToIntMapRec.IntIdxNo, FieldIdx);
    {Get Cross Referenced Field}
    StrCopy(CurrField, XRefField(IntRecNo, FieldIdx.FieldNo, FieldIdx.CompNo, CurrField));
		if FieldCount <> FieldIdx.FieldNo then
		begin
      for i:= FieldCount to (FieldIdx.FieldNo-1) do
				Builder_PutChr(FieldDel);
      FieldCount:= FieldIdx.FieldNo;
      RepCount:= 1;
      ComCount:= 1;
    end;
		if RepCount <> FieldIdx.RepNo then
    begin
      for i:= RepCount to (FieldIdx.RepNo-1) do
				Builder_PutChr(RepeatDel);
      RepCount:= FieldIdx.RepNo;
      ComCount:= 1;
    end;
		if ComCount <> FieldIdx.CompNo then
		begin
      for i:= ComCount to (FieldIdx.CompNo - 1) do
				Builder_PutChr(ComponentDel);
      ComCount:= FieldIdx.CompNo;
		end;
    LogField(IntRecNo, FieldIDX.FieldNo, CurrField);
		Builder_PutField(CurrField);
    BtrvStat:= CallDB(B_Next, ToIntMapFile, ToIntMapRec.Start, IntKey.Start, 2);
	end;
	Builder_PutEndRecord;
	IntIO_PutSingleRecord;
	Builder_ClearRecord;
end;

procedure BuildCommentRecord(CommentText: PChar);
begin
	Builder_PutChr('C');
	Builder_PutNulFields(3);
	Builder_PutChr(ComponentDel);
	Builder_PutField(CommentText);
	Builder_PutEndRecord;
  IntIO_PutSingleRecord;
  Builder_ClearRecord;
end;

procedure BuildPatientRecord;
var
  RecordTypeID: char;
  IntRecNo: integer;
  CommentText: array[0..255] of char;

begin
  RecordTypeID:= 'P';
  IntRecNo:= 0;
  BuildRecord(RecordTypeID, IntRecNo);
  PatientFile^.dbr^.GetFieldAsStr(DBPATFreeText, CommentText, SizeOf(CommentText)-1);
  if StrComp(CommentText, '') <> 0 then
		BuildCommentRecord(CommentText);
end;

procedure BuildTestOrderRecord;
var
	RecordTypeID: char;
	IntRecNo: integer;
	CommentText: array[0..255] of char;

begin
  RecordTypeID:= 'O';
  IntRecNo:= 1;
  BuildRecord(RecordTypeID, IntRecNo);
  SpecimenFile^.dbr^.GetFieldAsStr(DBSPECFreeText, CommentText, SizeOf(CommentText)-1);
  if StrComp(CommentText, '') <> 0 then
		BuildCommentRecord(CommentText);
end;

procedure BuildResultRecord(aOrderID, aTestCatID, aTestID, aDataValue: PChar);
var
  tempField: array[0..255] of char;
begin
	Builder_PutChr('R');
	Builder_PutNulFields(2);
  Builder_PutChr(ComponentDel);
  Builder_PutChr(ComponentDel);
  Builder_PutChr(ComponentDel);
  StrCopy(tempField, XRefField(2, 3, 4, aOrderID));
  Builder_PutField(tempField);
  Builder_PutChr(ComponentDel);
  if (StrComp(aTestCatID, 'MIC') = 0) then
  begin
    StrCopy(tempField, XRefField(2, 3, 5, aTestID));
    Builder_PutField(tempField);
    Builder_PutChr(ComponentDel);
    StrCopy(tempField, XRefField(2, 3, 6, '0'));
    Builder_PutField(tempField);
  end
  else if (StrComp(aTestCatID, 'NCCLS') = 0) then
  begin
    StrCopy(tempField, XRefField(2, 3, 5, aTestID));
    Builder_PutField(tempField);
    Builder_PutChr(ComponentDel);
    StrCopy(tempField, XRefField(2, 3, 6, '1'));
    Builder_PutField(tempField);
  end
  else if (StrComp(aTestCatID, 'BIO') = 0) or (StrComp(aTestCatID, 'BL') = 0) or
          (StrComp(aTestCatID, 'GmS') = 0) or (StrComp(aTestCatID, 'HEM') = 0) or
          (StrComp(aTestCatID, 'IND') = 0) or (StrComp(aTestCatID, 'OXI') = 0) or
          (StrComp(aTestCatID, 'StS') = 0) or (StrComp(aTestCatID, 'TFG') = 0) or
          (StrComp(aTestCatID, 'ORG') = 0) or (StrComp(aTestCatID, 'FAMILY') = 0) or
          (StrComp(aTestCatID, 'ESBL') = 0) then
  begin
    StrCopy(tempField, XRefField(2, 3, 5, aTestCatID));
    Builder_PutField(tempField);
  end;
  StrCopy(tempField, XRefField(2, 4, 1, aDataValue));
  Builder_PutStrField(tempField);
	Builder_PutEndRecord;
  IntIO_PutSingleRecord;
  Builder_ClearRecord;
end;

procedure SendDataRecords;
var
	SendKeyRec: TSendKeyRec;
	SendKeyKey: TSendKeyKey;
  TempPatientID: array[0..MaxPatIDLen] of char;
  results: PResults;
  ResCount, Index: integer;
	Organism: array[0..4] of char;
  Family: array[0..2] of char;
  seq: TSeqNum;
  orderID: PChar;
  orgID: PChar;
  setFamily: word;
  tByte: byte;
  str2: String[2];
  firstPatient: boolean;

  FilterMulti: boolean;
  TrayDBObj: PTraysObject;
  XStr: array[0..3] of Char;
  oAbbrPChar: array[0..64] of char;


  procedure BuildIt(aResult: PResRecObj); far;
  var IgnoreMultiDrug: boolean;
  begin
    StrLCopy(oAbbrPChar, aResult^.testID, 64);

    IgnoreMultiDrug :=
      FilterMulti and
      Results^.GetFlag(aResult^.TestID, 'NCCLS', RESContraindicated);

    if not IgnoreMultiDrug then
    begin
       If FilterMulti then TrayDBObj^.MapDrgAbbr(aResult^.testID,oAbbrPChar);
       BuildResultRecord(orderID, aResult^.testCatID, oAbbrPChar, aResult^.result);
    end;
  end;

begin
  BtrvStat:= CallDB(B_Get_Low, SendKeyFile, SendKeyRec.Start, SendKeyKey.Start, 0);
  StrCopy(TempPatientID, '');
  OkToSend:= True;
  firstPatient:= True;
	while (BtrvStat = 0) and (OkToSend) and (not ProcessStatDlg^.Abort) do
	begin
		{Must build data records to be sent - i.e. info from specimen file might
		 be mapped into patient record}
    with SendKeyRec do
    begin
      StrCopy(CurrPatientID, PatientID);
      StrCopy(CurrSpecimenID, SpecimenID);
      StrCopy(CurrCollectDate, CollectDate);
      StrCopy(CurrIsolateID, IsolateID);
		end;
		ProcessStatDlg^.Update;
    if (StrComp(TempPatientID, CurrPatientID) <> 0) or firstPatient then
    begin
      firstPatient:= False;
      StrCopy(TempPatientID, CurrPatientID);
      SetDlgItemText(ProcessStatDlg^.HWindow, id_ShowPatient, CurrPatientID);
      SetDlgItemText(ProcessStatDlg^.HWindow, id_ShowSpecimen, '');
      SetDlgItemText(ProcessStatDlg^.HWindow, id_ShowIsolate, '');
      UpdateWindow(ProcessStatDlg^.HWindow);
      LogFile^.LogEvent(0, SendBit, lgevSendPat, CurrPatientID, -1);
      BuildPatientRecord;
    end;
    if OKToSend then
    begin
      SetDlgItemText(ProcessStatDlg^.HWindow, id_ShowSpecimen, CurrSpecimenID);
      SetDlgItemText(ProcessStatDlg^.HWindow, id_ShowIsolate, CurrIsolateID);
      UpdateWindow(ProcessStatDlg^.HWindow);
      LogFile^.LogEvent(0, SendBit, lgevSendSpc, CurrSpecimenID, -1);
      LogFile^.LogEvent(0, SendBit, lgevSendColDate, CurrCollectDate, -1);
      LogFile^.LogEvent(0, SendBit, lgevSendIso, CurrIsolateID, -1);
      GetIsoRec(CurrSpecimenID, CurrCollectDate, CurrIsolateID);
      listRec^.Clear;
      if IsolateFile^.dbr^.GetField(DBISOOrdRef, @seq, SizeOf(seq)) then
        if listObj^.FindMnemSeq(DBORDFile, listRec, seq) then;
      GetMem(orderID, StrLen(listRec^.ID)+1);
      StrCopy(orderID, listRec^.ID);
      StrLCopy(CurrOrderID, orderID, SizeOf(CurrOrderID)-1);
      if (StrLen(CurrOrderID) > 0) then
        LogFile^.LogEvent(0, SendBit, lgevSendOrd, CurrOrderID, -1);
      BuildTestOrderRecord;
      if OKToSend then
      begin
        listRec^.Clear;
        if IsolateFile^.dbr^.GetField(DBISOOrgRef, @seq, SizeOf(seq)) then
          if listObj^.FindMnemSeq(DBORGFile, listRec, seq) then;
        GetMem(orgID, StrLen(listRec^.ID)+1);
        StrCopy(orgID, listRec^.ID);
        BuildResultRecord(orderID, 'ORG', 'ORG', orgID);
        if OKToSend then
        begin
          if IsolateFile^.dbr^.GetField(DBISOSetFamily, @setFamily, SizeOf(setFamily)) then;
          tByte:= ExtractFamily(setFamily);
          Str(tByte, str2);
          StrPCopy(Family, str2);
          BuildResultRecord(orderID, 'FAMILY', 'FAMILY', Family);
          if OKToSend then
          begin
            TrayDBObj:=New(PTraysObject,Init);
            results:= new(PResults, Init);
            results^.doYield:= True;

            GetPrivateProfileString
              ('IntCstm', 'FilterMultiDrugs', '', XStr, SizeOf(XStr), 'IntCstm.ini');
            FilterMulti:=StrComp(XStr, 'Yes') = 0;

            if results^.LoadResults(IsolateFile^.dbr) then
              results^.ForEach(@BuildIt);
            MSDisposeObj(results);
            MSDisposeObj(TrayDBObj);
            if OKToSend then
              BtrvStat:= CallDB(B_Next, SendKeyFile, SendKeyRec.Start, SendKeyKey.Start, 0);
          end;
        end;
        MSFreeMem(orderID, StrLen(orderID)+1);
      end;
    end;
	end;
end;

procedure SaveRecordKeys;
{order of transmission:
 - patient id
	- specimen id
   - isolate id
}
var
  SendKeyRec, TempSendKeyRec: TSendKeyRec;
  SendKeyKey: TSendKeyKey;
  Error, i:integer;
  TestField: array[0..255] of char;
  tBool: boolean;

begin
  PatientFile^.dbr^.GetFieldAsStr(DBPATPatID, SendKeyRec.PatientID, SizeOf(SendKeyRec.PatientID)-1);
  IsolateFile^.dbr^.GetFieldAsStr(DBISOSpecID, SendKeyRec.SpecimenID, SizeOf(SendKeyRec.SpecimenID)-1);
  IsolateFile^.dbr^.GetFieldAsStr(DBISOCollectDate, SendKeyRec.CollectDate, SizeOf(SendKeyRec.CollectDate)-1);
  IsolateFile^.dbr^.GetFieldAsStr(DBISOIso, SendKeyRec.IsolateID, SizeOf(SendKeyRec.IsolateID)-1);
  StrCopy(SendKeyKey.PatientID, SendKeyRec.PatientID);
  StrCopy(SendKeyKey.SpecimenID, SendKeyRec.SpecimenID);
  StrCopy(SendKeyKey.IsolateID, SendKeyRec.IsolateID);
  StrCopy(SendKeyKey.CollectDate, SendKeyRec.CollectDate);
	{Insert SendKeyData to BtrvFile}
  BtrvStat:= CallDB(B_Get_EQU, SendKeyFile, TempSendKeyRec.Start, SendKeyKey.Start, 0);
	if BtrvStat <> 0 then
  begin
    StrCopy(SendKeyKey.PatientID, SendKeyRec.PatientID);
    StrCopy(SendKeyKey.SpecimenID, SendKeyRec.SpecimenID);
    StrCopy(SendKeyKey.IsolateID, SendKeyRec.IsolateID);
    StrCopy(SendKeyKey.CollectDate, SendKeyRec.CollectDate);
    BtrvStat:= CallDB(B_Insert, SendKeyFile, SendKeyRec.Start, SendKeyKey.Start, 0);
	end;
  tBool:= True;
  IsolateFile^.dbr^.PutField(DBISOTransFlag, @tBool);
  IsolateFile^.dbc^.UpdateRec(IsolateFile^.dbr);
end;

procedure SelectDataRecords(CurrentWnd: HWnd;MPSSObj: PMPSS);
var
	SendKeyRec: TSendKeyRec;
  SendKeyKey: TSendKeyKey;
  seq       : TSeqNum;

begin
  BtrvStat:= CallDB(B_Get_Low, SendKeyFile, SendKeyRec.Start, SendKeyKey.Start, 0);
	while BtrvStat = 0 do
	begin
    BtrvStat:= CallDB(B_Delete, SendKeyFile, SendKeyRec.Start, SendKeyKey.Start, 0);
    BtrvStat:= CallDB(B_Next, SendKeyFile, SendKeyRec.Start, SendKeyKey.Start, 0);
	end;
  if MPSSObj^.MPSSGetFirst(IsolateFile) then
  repeat
    IsolateFile^.dbr^.GetField(DBISOSpecRef, @seq, SizeOf(seq));
    if (seq > 0) then
      SpecimenFile^.dbc^.GetSeq(SpecimenFile^.dbr, seq)
    else
      SpecimenFile^.dbr^.ClearRecord;
    SpecimenFile^.dbr^.GetField(DBSPECPatRef, @seq, SizeOf(seq));
    if (seq > 0) then
      PatientFile^.dbc^.GetSeq(PatientFile^.dbr, seq)
    else
      PatientFile^.dbr^.ClearRecord;
    SaveRecordKeys; {Save keys in file to read in transmission sorted order}
  until not MPSSObj^.MPSSGetNext(IsolateFile);
end;

procedure CreateHeaderRecord;
const
  Delimiters: array[0..4] of char = (FieldDel, RepeatDel, ComponentDel,
    EscapeDel, #0);
var
  version: array[0..30] of char;

begin
	Builder_PutChr('H');
	Builder_PutStrField(Delimiters);
  Builder_PutNulFields(2);
  Builder_PutStrField(SenderID);
  Builder_PutNulFields(6);
  Builder_PutChrField(ProcessingID);
  StrLCopy(version, MFIVersion, SizeOf(version)-1);
  Builder_PutStrField(version);
	Builder_PutEndRecord;
	IntIO_PutSingleRecord;
	Builder_ClearRecord;
end;

procedure CreateTerminatorRecord;
begin
	Builder_PutChr('L');
  Builder_PutChrField('1');
  Builder_PutChrField('N');
	Builder_PutEndRecord;
	IntIO_PutSingleRecord;
	Builder_ClearRecord;
end;

procedure OpenFilesRecs(CurrWnd: HWnd);
begin
  PatientFile:= New(PDBFile, Init(DBPATFile, '', dbOpenNormal));
  SpecimenFile:= New(PDBFile, Init(DBSPECFile, '', dbOpenNormal));
  IsolateFile:= New(PDBFile, Init(DBISOFile, '', dbOpenNormal));
  BtrvData[SendKeyFile].Size.Len      := SizeOf(TSendKeyRec);
  BtrvData[SendKeyFile].FileName.Name := 'SENDKEY.DAT   ';
  BtrvData[ToIntMapFile].Size.Len      := SizeOf(TToIntMapRec);
  BtrvData[ToIntMapFile].FileName.Name := 'TOINTMAP.DAT  ';
  OpenCloseFiles(CurrWnd, B_Open, [SendKeyFile, ToIntMapFile]);
  listRec:= new(PMnemListRec, Init);
  listObj:= new(PListObject, Init);
end;

procedure CloseFilesRecs(CurrWnd: HWnd);
begin
  if (PatientFile <> nil) then
    MSDisposeObj(PatientFile);
  if (SpecimenFile <> nil) then
    MSDisposeObj(SpecimenFile);
  if (IsolateFile <> nil) then
    MSDisposeObj(IsolateFile);
  OpenCloseFiles(CurrWnd, B_Close, [SendKeyFile, ToIntMapFile]);
  MSDisposeObj(listRec);
  MSDisposeObj(listObj);
end;

procedure SendData(CurrentWnd: HWnd; WndObjPtr: PWindowsObject; MPSSObj: PMPSS);
var
	UseFileXFer: boolean;
  TempMsg: array[0..25] of char;
  LastCursor: HCursor;

begin
  IntCstm_InitCustomCfgBuff;
  UseFileXFer:= False;
  LoadString(HInstance, str_IntBase+str_File, TempMsg, SizeOf(TempMsg));
  if StrComp(CustomCfgBuff.CstmParmsCfg[0].ComboSelection, TempMsg) = 0 then
    UseFileXFer:= True;
  if IntIO_OpenXFer then
  begin
    Builder_ClearRecord;
    CreateHeaderRecord;
    SelectDataRecords(CurrentWnd, MPSSObj);
    ProcessStatDlg:= New(PProcessStatusDlg, Init(WndObjPtr, 'INT_PROCESS_DATA'));
    ProcessStatDlg^.EnableAutoCreate;
    if ShowReceive then
    begin
      Application^.MakeWindow(ProcessStatDlg);
      ShowWindow(ProcessStatDlg^.HWindow, SW_SHOW);
    end;
    ProcessStatDlg^.Abort:= False;
    LoadString(HInstance, str_IntBase+str_SendData, TempMsg, SizeOf(TempMsg));
    SetWindowText(ProcessStatDlg^.HWindow, TempMsg);
    SendDataRecords;
    ProcessStatDlg^.CloseWindow;
    CreateTerminatorRecord;
    IntIO_PutEndXFer;
    LogFile^.LogEvent(0, SendBit, lgevEndRcv, '', -1);
    LastCursor := SetCursor(LoadCursor(0, idc_Wait));
    LogFile^.ClearOldRecs;
    SetCursor(LastCursor);
  end;
end;

procedure IntSend_SendRequest(CurrentWnd: HWnd; WndObjPtr: PWindowsObject);
var
	GetRangesDlg: PGetRangesDlg;
	UseFileXFer: boolean;

begin
  DisableMenu(GlobHWnd);
  GetRangesDlg:= New(PGetRangesDlg, Init(WndObjPtr, 'INT_SEND_REQ'));
  if (Application^.ExecDialog(GetRangesDlg) = 0) then
  begin
    if IntIO_OpenXFer then
    begin
      Builder_ClearRecord;
      CreateHeaderRecord;
      Builder_PutChr('Q');
      Builder_PutNulFields(1);
      if StrComp(Ranges.StartRange, '') = 0 then
      begin
        StrCopy(Ranges.RequestType, 'P');
        StrCopy(Ranges.StartRange, 'ALL');
      end;
      if StrComp(Ranges.RequestType, 'P') = 0 then
      begin
        Builder_PutStrField(Ranges.StartRange);
        Builder_PutStrField(Ranges.EndRange);
        Builder_PutNulFields(8);
      end
      else
        if StrComp(Ranges.RequestType, 'C') = 0 then
        begin
          Builder_PutNulFields(1);
          Builder_PutChr(ComponentDel);
          Builder_PutField(Ranges.StartRange);
          Builder_PutChr(FieldDel);
          Builder_PutChr(ComponentDel);
          Builder_PutField(Ranges.EndRange);
          Builder_PutNulFields(8);
        end
        else
        begin
          Builder_PutNulFields(4);
          Builder_PutField(Ranges.RequestType);
          Builder_PutStrField(Ranges.StartRange);
          Builder_PutStrField(Ranges.EndRange);
          Builder_PutNulFields(4);
        end;
      if not Ranges.ReTransmit then
        Builder_PutChrField('N');
      Builder_PutEndRecord;
      IntIO_PutSingleRecord;
      Builder_ClearRecord;
      CreateTerminatorRecord;
      IntIO_PutEndXFer;
      LogFile^.LogEvent(0, SendBit, lgevEndRcv, '', -1);
      LogFile^.LogEvent(0, SendBit, lgevSendReq, '', -1);
    end;
  end;
  EnableMenu(GlobHWnd);
end;

procedure BuildMFIRules(MPSSObj: PMPSS; ReqType: char;
                        BegRange, EndRange: PChar; ReTransmit: boolean);
var
	RuleFieldNo: integer;
  RuleFH: PDBFile;
  tdate1, tdate2: array[0..MaxDateLen] of char;

begin
  With MPSSObj^ do
  begin
    mRuleLoaded:= False;
    if (StrComp(BegRange, 'ALL') <> 0) then
    begin
      mRules^[0].FH:= nil;
      {Set rules}
      RuleFH:= mFiles^[1].FH; {isolate file}
      case ReqType of
        'P': begin {Range is Patient IDs}
              RuleFH:= mFiles^[3].FH; {patient file}
              RuleFieldNo:= DBPATPatID;
            end;
        'C': RuleFieldNo:= DBISOSpecID; {Range is Specimen IDs}
        'S': RuleFieldNo:= DBISOCollectDate; {Range is Collect Date}
        'R': RuleFieldNo:= DBISOTstDate; {Range is Test Date}
      end;


      if (ReqType = 'S') or (ReqType = 'R') then
      begin
        StrCopy(tdate1, BegRange);
        StrCopy(tdate2, EndRange);
        CheckDateField(tdate1, SizeOf(tdate1)-1);
        CheckDateField(tdate2, SizeOf(tdate2)-1);
        StrCopy(BegRange, tdate1);
        StrCopy(EndRange, tdate2);
      end;

      mRules^[0].AndOr:= MPSSAnd;
      mRules^[0].LParen:= False;
      mRules^[0].RParen:= False;
      mRules^[0].FH:= RuleFH;
      RuleFH^.dbd^.FileDesc(mRules^[0].FileDesc, SizeOf(mRules^[0].FileDesc)-1);
      StrCopy(mRules^[0].FieldName, '');
      mRules^[0].FieldNo:= RuleFieldNo;
      mRules^[0].FieldType:= dbZCode;
      mRules^[0].TestRef:= -1;
      mRules^[0].Operator:= MPSSGE;
      StrCopy(mRules^[0].Value, BegRange);
      mRules^[0].lValue   := 0;

      mRules^[1].AndOr:= MPSSAnd;
      mRules^[1].LParen:= False;
      mRules^[1].RParen:= False;
      mRules^[1].FH:= RuleFH;
      RuleFH^.dbd^.FileDesc(mRules^[1].FileDesc, SizeOf(mRules^[1].FileDesc)-1);
      StrCopy(mRules^[1].FieldName, '');
      mRules^[1].FieldNo:= RuleFieldNo;
      mRules^[1].FieldType:= dbZCode;
      mRules^[1].TestRef:= -1;
      mRules^[1].Operator:= MPSSLE;
      if (StrLen(EndRange) > 0) then
        StrCopy(mRules^[1].Value, EndRange)
      else
        StrCopy(mRules^[1].Value, BegRange);
      mRules^[1].lValue   := 0;

      if ReTransmit then
      begin
        mRules^[2].AndOr:= MPSSAnd;
        mRules^[2].FH:= nil;
      end
      else
      begin
        mRules^[2].AndOr:= MPSSAnd;
        mRules^[2].LParen:= False;
        mRules^[2].RParen:= False;
        mRules^[2].FH:= mFiles^[1].FH; {isolate file}
        IsolateFile^.dbd^.FileDesc(mRules^[2].FileDesc, SizeOf(mRules^[2].FileDesc)-1);
        StrCopy(mRules^[2].FieldName, '');
        mRules^[2].FieldNo:= DBISOTransFlag;
        mRules^[2].FieldType:= dbZCode;
        mRules^[2].TestRef:= -1;
        mRules^[2].Operator:= MPSSEQ;
        BoolToStr(False, mRules^[2].Value, SizeOf(mRules^[2].Value)-1);
        mRules^[2].lValue   := 0;

        mRules^[3].AndOr:= MPSSAnd;
        mRules^[3].FH:= nil;
      end;
      mRuleLoaded:= True;
    end;
  end;
end;

procedure IntSend_SendRequestedData(CurrentWnd: HWnd; WndObjPtr: PWindowsObject);
var
	MPSSObj: PMPSS;
  StartP, EndP: array[0..25] of char;

begin
  DisableMenu(GlobHWnd);
  OpenFilesRecs(CurrentWnd);
  with Ranges do
  begin
    StrCopy(StartP, StartRange);
    StrCopy(EndP, EndRange);
    MPSSObj:= New(PMPSS, Init(WndObjPtr, IsolateFile, IncludeUndefDates));
    if MPSSObj <> Nil then
    begin
      BuildMFIRules(MPSSObj, RequestType[0], StartP, EndP, ReTransmit);
      SendData(CurrentWnd, WndObjPtr, MPSSObj);
      MSDisposeObj(MPSSObj);
    end;
  end;
  CloseFilesRecs(CurrentWnd);
  EnableMenu(GlobHWnd);
end;

procedure IntSend_SendUserData(CurrentWnd: HWnd; PWindowsObjPtr: PWindowsObject);
var
	MPSSObj: PMPSS;
  TempMsg: array[0..25] of char;

begin
  DisableMenu(GlobHWnd);
  OpenFilesRecs(CurrentWnd);
  LoadString(HInstance, str_IntBase+str_TransSel, TempMsg, SizeOf(TempMsg));
  MPSSObj:= New(PMPSS, Init(PWindowsObjPtr, IsolateFile, IncludeUndefDates));
  if MPSSObj <> Nil then
  begin
    If MPSSObj^.MPSSLoadRules(PWindowsObjPtr, TempMsg) then
      SendData(CurrentWnd, PwindowsObjPtr, MPSSObj);
    MSDisposeObj(MPSSObj);
  end;
  CloseFilesRecs(CurrentWnd);
  EnableMenu(GlobHWnd);
end;

Begin
End.
