{----------------------------------------------------------------------------}
{  Module Name  : JNDATA.PAS                                                 }
{  Programmer   : EJ                                                         }
{  Date Created : 01/24/95                                                   }
{                                                                            }
{  Purpose -                                                                 }
{  This module is used to create data for Nihongo DMS database files.        }
{                                                                            }
{  Assumptions -                                                             }
{  None.                                                                     }
{                                                                            }
{  Initialization -                                                          }
{  None.                                                                     }
{                                                                            }
{  Revision History -                                                        }
{                                                                            }
{  Version  Date      Who    Did What                                        }
{  -------  --------  ------ ----------------------------------------------- }
{  1.00     01/24/94  EJ     Initial release                                 }
{                                                                            }
{----------------------------------------------------------------------------}

program JNData;

uses
  WinCrt,
  APITools,
  DBIDs,
  DBFile,
  DBLib,
  WaitLib,
  DBTypes,
  DBErrors,
  Strings,
  StrsW,
  RptXFer,
  MScan,
  DMString,
  WinProcs,
  DMSDebug;

const
  BIOstr        = '.BIO';
  ESBLstr       = '.ESBL';
  BLstr         = '.BL';
  GMSstr        = '.GmS';
  HEMstr        = '.HEM';
  INDstr        = '.IND';
  OXIstr        = '.OXI';
  STSstr        = '.StS';
  TFGstr        = '.TFG';
  NCCLSIDstr    = 'NCCLS';
  MICIDstr      = 'MIC';
  BIOIDstr      = 'BIO';
  ESBLIDstr     = 'ESBL';
  BLIDstr       = 'BL';
  GMSIDstr      = 'GmS';
  HEMIDstr      = 'HEM';
  INDIDstr      = 'IND';
  OXIIDstr      = 'OXI';
  STSIDstr      = 'StS';
  TFGIDstr      = 'TFG';


var
  hour   : PWaitCursor;
  ioRet : integer;
  eStr  : array [0..255] of char;

function ReadFile(var fil: text; var s: string; locCode: integer): boolean;
var
  lineOK: boolean;
  p1    : array[0..255] of char;
  p2    : array[0..255] of char;
begin
  lineOK:= true;
  repeat
    ReadLn(fil, p2);
    ioRet := IOResult;
    if ioRet <> 0 then
    begin
      Str(ioRet, eStr);
      StrCopy(p1, 'JN Data (');
      Str(locCode, p2);
      StrCat(p1, p2);
      StrCat(p1, ') - IO Error');
      StrCopy(p2, 'IO Result Code = ');
      StrCat(p2, eStr);
      ErrorMsg(0, p1, p2);
      Halt;
    end;
    {- is the line a comment ? }
    TrimAll(p1, p2, ' ', sizeof(p1)-1);
    if (p1[0] = '/') and (p1[1] = '/') then
      lineOK:= false
    else
      lineOK:= true;
  until Eof(fil) or lineOK;
  s:= StrPas(p2);
  ReadFile:= lineOK;
end;

procedure Prep;
var
  dbd       : PDBFile;
begin
  dbd := New(PDBFile, Init(DBSPECFile, '', dbOpenExclusive));
  if dbd = nil then
  begin
    ErrorMsg(0, 'JNData - SPECIMEN', dbErrorStr(dbLastOpenError, eStr, sizeof(estr)-1));
    Exit;
  end;
  if dbd^.dbc^.GetFirst(dbd^.dbr) then
    repeat
      dbd^.dbc^.DeleteRec(dbd^.dbr);
    until (dbd^.dbc^.dbErrorNum <> 0) or not dbd^.dbc^.GetNext(dbd^.dbr);
  Dispose(dbd, Done);

  dbd := New(PDBFile, Init(DBPATFile, '', dbOpenExclusive));
  if dbd = nil then
  begin
    ErrorMsg(0, 'JNData - PATIENT', dbErrorStr(dbLastOpenError, eStr, 255));
    Exit;
  end;
  if dbd^.dbc^.GetFirst(dbd^.dbr) then
    repeat
      dbd^.dbc^.DeleteRec(dbd^.dbr);
    until (dbd^.dbc^.dbErrorNum <> 0) or not dbd^.dbc^.GetNext(dbd^.dbr);
  Dispose(dbd, Done);

  dbd := New(PDBFile, Init(DBSRCFile, '', dbOpenExclusive));
  if dbd = nil then
  begin
    ErrorMsg(0, 'JNData - SOURCE', dbErrorStr(dbLastOpenError, eStr, 255));
    Exit;
  end;
  if dbd^.dbc^.GetFirst(dbd^.dbr) then
    repeat
      dbd^.dbc^.DeleteRec(dbd^.dbr);
    until (dbd^.dbc^.dbErrorNum <> 0) or not dbd^.dbc^.GetNext(dbd^.dbr);
  Dispose(dbd, Done);

  dbd := New(PDBFile, Init(DBWARDFile, '', dbOpenExclusive));
  if dbd = nil then
  begin
    ErrorMsg(0, 'JNData - WARD', dbErrorStr(dbLastOpenError, eStr, 255));
    Exit;
  end;
  if dbd^.dbc^.GetFirst(dbd^.dbr) then
    repeat
      dbd^.dbc^.DeleteRec(dbd^.dbr);
    until (dbd^.dbc^.dbErrorNum <> 0) or not dbd^.dbc^.GetNext(dbd^.dbr);
  Dispose(dbd, Done);

  dbd := New(PDBFile, Init(DBSTATFile, '', dbOpenExclusive));
  if dbd = nil then
  begin
    ErrorMsg(0, 'JNData - SPECSTAT', dbErrorStr(dbLastOpenError, eStr, 255));
    Exit;
  end;
  if dbd^.dbc^.GetFirst(dbd^.dbr) then
    repeat
      dbd^.dbc^.DeleteRec(dbd^.dbr);
    until (dbd^.dbc^.dbErrorNum <> 0) or not dbd^.dbc^.GetNext(dbd^.dbr);
  Dispose(dbd, Done);

  dbd := New(PDBFile, Init(DBGSDefFile, '', dbOpenExclusive));
  if dbd = nil then
  begin
    ErrorMsg(0, 'JNData - GSDEF', dbErrorStr(dbLastOpenError, eStr, 255));
    Exit;
  end;
  if dbd^.dbc^.GetFirst(dbd^.dbr) then
    repeat
      dbd^.dbc^.DeleteRec(dbd^.dbr);
    until (dbd^.dbc^.dbErrorNum <> 0) or not dbd^.dbc^.GetNext(dbd^.dbr);
  Dispose(dbd, Done);

  dbd := New(PDBFile, Init(DBGSMnemFile, '', dbOpenExclusive));
  if dbd = nil then
  begin
    ErrorMsg(0, 'JNData - GSMNEM', dbErrorStr(dbLastOpenError, eStr, 255));
    Exit;
  end;
  if dbd^.dbc^.GetFirst(dbd^.dbr) then
    repeat
      dbd^.dbc^.DeleteRec(dbd^.dbr);
    until (dbd^.dbc^.dbErrorNum <> 0) or not dbd^.dbc^.GetNext(dbd^.dbr);
  Dispose(dbd, Done);

  dbd := New(PDBFile, Init(DBWAPNLFile, '', dbOpenExclusive));
  if dbd = nil then
  begin
    ErrorMsg(0, 'JNData - WAPANEL', dbErrorStr(dbLastOpenError, eStr, 255));
    Exit;
  end;
  if dbd^.dbc^.GetFirst(dbd^.dbr) then
    repeat
      dbd^.dbc^.DeleteRec(dbd^.dbr);
    until (dbd^.dbc^.dbErrorNum <> 0) or not dbd^.dbc^.GetNext(dbd^.dbr);
  Dispose(dbd, Done);

  dbd := New(PDBFile, Init(DBSESSFile, '', dbOpenExclusive));
  if dbd = nil then
  begin
    ErrorMsg(0, 'JNData - SESSION', dbErrorStr(dbLastOpenError, eStr, 255));
    Exit;
  end;
  if dbd^.dbc^.GetFirst(dbd^.dbr) then
    repeat
      dbd^.dbc^.DeleteRec(dbd^.dbr);
    until (dbd^.dbc^.dbErrorNum <> 0) or not dbd^.dbc^.GetNext(dbd^.dbr);
  Dispose(dbd, Done);

  dbd := New(PDBFile, Init(DBORGFile, '', dbOpenExclusive));
  if dbd = nil then
  begin
    ErrorMsg(0, 'JNData - ORGANISM', dbErrorStr(dbLastOpenError, eStr, 255));
    Exit;
  end;
  if dbd^.dbc^.GetFirst(dbd^.dbr) then
    repeat
      dbd^.dbc^.DeleteRec(dbd^.dbr);
    until (dbd^.dbc^.dbErrorNum <> 0) or not dbd^.dbc^.GetNext(dbd^.dbr);
  Dispose(dbd, Done);

  dbd := New(PDBFile, Init(DBORDFile, '', dbOpenExclusive));
  if dbd = nil then
  begin
    ErrorMsg(0, 'JNData - ORDER', dbErrorStr(dbLastOpenError, eStr, 255));
    Exit;
  end;
  if dbd^.dbc^.GetFirst(dbd^.dbr) then
    repeat
      dbd^.dbc^.DeleteRec(dbd^.dbr);
    until (dbd^.dbc^.dbErrorNum <> 0) or not dbd^.dbc^.GetNext(dbd^.dbr);
  Dispose(dbd, Done);

  dbd := New(PDBFile, Init(DBTSTGRPFile, '', dbOpenExclusive));
  if dbd = nil then
  begin
    ErrorMsg(0, 'JNData - TESTGRP', dbErrorStr(dbLastOpenError, eStr, 255));
    Exit;
  end;
  if dbd^.dbc^.GetFirst(dbd^.dbr) then
    repeat
      dbd^.dbc^.DeleteRec(dbd^.dbr);
    until (dbd^.dbc^.dbErrorNum <> 0) or not dbd^.dbc^.GetNext(dbd^.dbr);
  Dispose(dbd, Done);

  dbd := New(PDBFile, Init(DBTSTFile, '', dbOpenExclusive));
  if dbd = nil then
  begin
    ErrorMsg(0, 'JNData - TEST', dbErrorStr(dbLastOpenError, eStr, 255));
    Exit;
  end;
  if dbd^.dbc^.GetFirst(dbd^.dbr) then
    repeat
      dbd^.dbc^.DeleteRec(dbd^.dbr);
    until (dbd^.dbc^.dbErrorNum <> 0) or not dbd^.dbc^.GetNext(dbd^.dbr);
  Dispose(dbd, Done);

  dbd := New(PDBFile, Init(DBDRUGFile, '', dbOpenExclusive));
  if dbd = nil then
  begin
    ErrorMsg(0, 'JNData - DRUG', dbErrorStr(dbLastOpenError, eStr, 255));
    Exit;
  end;
  if dbd^.dbc^.GetFirst(dbd^.dbr) then
    repeat
      dbd^.dbc^.DeleteRec(dbd^.dbr);
    until (dbd^.dbc^.dbErrorNum <> 0) or not dbd^.dbc^.GetNext(dbd^.dbr);
  Dispose(dbd, Done);

  dbd := New(PDBFile, Init(DBTSTCATFile, '', dbOpenExclusive));
  if dbd = nil then
  begin
    ErrorMsg(0, 'JNData - TESTCAT', dbErrorStr(dbLastOpenError, eStr, 255));
    Exit;
  end;
  if dbd^.dbc^.GetFirst(dbd^.dbr) then
    repeat
      dbd^.dbc^.DeleteRec(dbd^.dbr);
    until (dbd^.dbc^.dbErrorNum <> 0) or not dbd^.dbc^.GetNext(dbd^.dbr);
  Dispose(dbd, Done);

  dbd := New(PDBFile, Init(DBINTRPMAPFile, '', dbOpenExclusive));
  if dbd = nil then
  begin
    ErrorMsg(0, 'JNData - INTRPMAP', dbErrorStr(dbLastOpenError, eStr, 255));
    Exit;
  end;
  if dbd^.dbc^.GetFirst(dbd^.dbr) then
    repeat
      dbd^.dbc^.DeleteRec(dbd^.dbr);
    until (dbd^.dbc^.dbErrorNum <> 0) or not dbd^.dbc^.GetNext(dbd^.dbr);
  Dispose(dbd, Done);

  dbd := New(PDBFile, Init(DBMICFile, '', dbOpenExclusive));
  if dbd = nil then
  begin
    ErrorMsg(0, 'JNData - MPSSMIC', dbErrorStr(dbLastOpenError, eStr, 255));
    Exit;
  end;
  if dbd^.dbc^.GetFirst(dbd^.dbr) then
    repeat
      dbd^.dbc^.DeleteRec(dbd^.dbr);
    until (dbd^.dbc^.dbErrorNum <> 0) or not dbd^.dbc^.GetNext(dbd^.dbr);
  Dispose(dbd, Done);
end;


procedure JNIntrp;
var
  dbd       : PDBFile;
  srcFile   : text;
  srcLine   : string;
  pstr      : array [0..30] of char;
begin
{$I-}
  if IOResult <> 0 then ;

  Assign(srcFile, 'INTRPMAP.JN');
  Reset(srcFile);
  ioRet := IOResult;
  if ioRet <> 0 then
  begin
    Str(ioRet, eStr);
    ErrorMsg(0, 'JNData - IO Error - 11', eStr);
    Halt;
  end;
  dbd := New(PDBFile, Init(DBINTRPMAPFile, '', dbOpenExclusive));
  if dbd = nil then
  begin
    ErrorMsg(0, 'JNData - INTRPMAP', dbErrorStr(dbLastOpenError, eStr, 255));
    Exit;
  end;

  if dbd^.dbc^.NumRecords <> 0 then
  begin
    ErrorMsg(0, 'JNData', 'INTRPMAP file is not empty!');
    Exit;
  end;

  while not Eof(srcFile) and (dbd^.dbc^.dbErrorNum = 0) do
  begin
    if ReadFile(srcFile, srcLine, 21) then
    begin
      TrimAll(pstr, StrPCopy(pstr, Copy(srcLine, 1, 4)), ' ', 30);
      dbd^.dbr^.PutFieldAsStr(DBINTRPMAPCode, pstr);
      TrimAll(pstr, StrPCopy(pstr, Copy(srcLine, 5, 5)), ' ', 30);
      dbd^.dbr^.PutFieldAsStr(DBINTRPMAPDfltText, pstr);
      dbd^.dbr^.PutFieldAsStr(DBINTRPMAPMappedText, pstr);
      if not dbd^.dbc^.InsertRec(dbd^.dbr) then
        ErrorMsg(0, 'JNData', 'Cannot insert record in INTRPMAP file!');
    end;
  end;
  Dispose(dbd, Done);
  Close(srcFile);
  if IOResult <> 0 then {nothing};
{$I+}
end;


procedure JNDrug;
var
  dbd       : PDBFile;
  srcFile   : text;
  srcLine   : string;
  pstr      : array [0..30] of char;
begin
{$I-}
  if IOResult <> 0 then ;

  Assign(srcFile, 'DRUGS.JN');
  Reset(srcFile);
  ioRet := IOResult;
  if ioRet <> 0 then
  begin
    Str(ioRet, eStr);
    ErrorMsg(0, 'JNData - IO Error - 12', eStr);
    Halt;
  end;
  dbd := New(PDBFile, Init(DBDRUGFile, '', dbOpenExclusive));
  if dbd = nil then
  begin
    ErrorMsg(0, 'JNData - DRUGDEF', dbErrorStr(dbLastOpenError, eStr, 255));
    Exit;
  end;

  if dbd^.dbc^.NumRecords <> 0 then
  begin
    ErrorMsg(0, 'JNData', 'DRUGDEF file is not empty!');
    Exit;
  end;

  while not Eof(srcFile) and (dbd^.dbc^.dbErrorNum = 0) do
  begin
    if ReadFile(srcFile, srcLine, 22) then
    begin
      TrimAll(pstr, StrPCopy(pstr, Copy(srcLine, 1, 5)), ' ', 30);
      dbd^.dbr^.PutFieldAsStr(DBDRUGAbbr, pstr);
      TrimAll(pstr, StrPCopy(pstr, Copy(srcLine, 7, 12)), ' ', 30);
      dbd^.dbr^.PutFieldAsStr(DBDRUGAltAbbr, pstr);
      TrimAll(pstr, StrPCopy(pstr, Copy(srcLine, 20, 4)), ' ', 30);
      dbd^.dbr^.PutFieldAsStr(DBDRUGSortNum, pstr);
      dbd^.dbr^.PutFieldAsStr(DBDRUGDfltSortNum, pstr);
      TrimAll(pstr, StrPCopy(pstr, Copy(srcLine, 25, 30)), ' ', 30);
      dbd^.dbr^.PutFieldAsStr(DBDRUGName, pstr);
      if not dbd^.dbc^.InsertRec(dbd^.dbr) then
        ErrorMsg(0, 'JNData', 'Cannot insert record in DRUGDEF file!');
    end;
  end;
  Dispose(dbd, Done);
  Close(srcFile);
  if IOResult <> 0 then {nothing};
{$I+}
end;


procedure JNTestCat;
var
  dbd       : PDBFile;
  pstr      : array [0..30] of char;
begin
  dbd := New(PDBFile, Init(DBTSTCATFile, '', dbOpenExclusive));
  if dbd = nil then
  begin
    ErrorMsg(0, 'JNData - TCATDEF', dbErrorStr(dbLastOpenError, eStr, 255));
    Exit;
  end;

  if dbd^.dbc^.NumRecords <> 0 then
  begin
    ErrorMsg(0, 'JNData', 'TCATDEF file is not empty!');
    Exit;
  end;

  dbd^.dbr^.PutFieldAsStr(DBTSTCATID, BIOIDstr);
  SR(IDS_BIOTYPE, pstr, 30); {Biotype}
  dbd^.dbr^.PutFieldAsStr(DBTSTCATDesc, pstr);
  dbd^.dbr^.PutFieldAsStr(DBTSTCATResLen, '20');
  if not dbd^.dbc^.InsertRec(dbd^.dbr) then
    ErrorMsg(0, 'JNData', 'Cannot insert record in TCATDEF file!');

  dbd^.dbr^.PutFieldAsStr(DBTSTCATID, BLIDstr);
  SR(IDS_BETA_LACTAMASE, pstr, 30); {Beta-Lactamase}
  dbd^.dbr^.PutFieldAsStr(DBTSTCATDesc, pstr);
  dbd^.dbr^.PutFieldAsStr(DBTSTCATResLen, '1');
  if not dbd^.dbc^.InsertRec(dbd^.dbr) then
    ErrorMsg(0, 'JNData', 'Cannot insert record in TCATDEF file!');

  dbd^.dbr^.PutFieldAsStr(DBTSTCATID, GMSIDstr);
  SR(IDS_GENTAMICIN_SYNERGY, pstr, 30); {Gentamycin Synergy}
  dbd^.dbr^.PutFieldAsStr(DBTSTCATDesc, pstr);
  dbd^.dbr^.PutFieldAsStr(DBTSTCATResLen, '1');
  if not dbd^.dbc^.InsertRec(dbd^.dbr) then
    ErrorMsg(0, 'JNData', 'Cannot insert record in TCATDEF file!');

  dbd^.dbr^.PutFieldAsStr(DBTSTCATID, HEMIDstr);
  SR(IDS_BETA_HEMOLYSIS, pstr, 30); {Beta-Hemolysis}
  dbd^.dbr^.PutFieldAsStr(DBTSTCATDesc, pstr);
  dbd^.dbr^.PutFieldAsStr(DBTSTCATResLen, '1');
  if not dbd^.dbc^.InsertRec(dbd^.dbr) then
    ErrorMsg(0, 'JNData', 'Cannot insert record in TCATDEF file!');

  dbd^.dbr^.PutFieldAsStr(DBTSTCATID, INDIDstr);
  SR(IDS_INDOLE, pstr, 30); {Indole}
  dbd^.dbr^.PutFieldAsStr(DBTSTCATDesc, pstr);
  dbd^.dbr^.PutFieldAsStr(DBTSTCATResLen, '1');
  if not dbd^.dbc^.InsertRec(dbd^.dbr) then
    ErrorMsg(0, 'JNData', 'Cannot insert record in TCATDEF file!');

  dbd^.dbr^.PutFieldAsStr(DBTSTCATID, OXIIDstr);
  SR(IDS_OXIDASE, pstr, 30); {Oxidase}
  dbd^.dbr^.PutFieldAsStr(DBTSTCATDesc, pstr);
  dbd^.dbr^.PutFieldAsStr(DBTSTCATResLen, '1');
  if not dbd^.dbc^.InsertRec(dbd^.dbr) then
    ErrorMsg(0, 'JNData', 'Cannot insert record in TCATDEF file!');

  dbd^.dbr^.PutFieldAsStr(DBTSTCATID, STSIDstr);
  SR(IDS_STREPTOMYCIN_SYNERGY, pstr, 30); {Streptamycin Synergy}
  dbd^.dbr^.PutFieldAsStr(DBTSTCATDesc, pstr);
  dbd^.dbr^.PutFieldAsStr(DBTSTCATResLen, '1');
  if not dbd^.dbc^.InsertRec(dbd^.dbr) then
    ErrorMsg(0, 'JNData', 'Cannot insert record in TCATDEF file!');

  dbd^.dbr^.PutFieldAsStr(DBTSTCATID, TFGIDstr);
  SR(IDS_THYMIDINE_FREE_GROWTH, pstr, 30); {Thymadine Free Growth}
  dbd^.dbr^.PutFieldAsStr(DBTSTCATDesc, pstr);
  dbd^.dbr^.PutFieldAsStr(DBTSTCATResLen, '1');
  if not dbd^.dbc^.InsertRec(dbd^.dbr) then
    ErrorMsg(0, 'JNData', 'Cannot insert record in TCATDEF file!');

  dbd^.dbr^.PutFieldAsStr(DBTSTCATID, MICIDstr);
  dbd^.dbr^.PutFieldAsStr(DBTSTCATDesc, MICIDstr);
  dbd^.dbr^.PutFieldAsStr(DBTSTCATResLen, '1');
  if not dbd^.dbc^.InsertRec(dbd^.dbr) then
    ErrorMsg(0, 'JNData', 'Cannot insert record in TCATDEF file!');

  dbd^.dbr^.PutFieldAsStr(DBTSTCATID, NCCLSIDstr);
  dbd^.dbr^.PutFieldAsStr(DBTSTCATDesc, NCCLSIDstr);
  dbd^.dbr^.PutFieldAsStr(DBTSTCATResLen, '1');
  if not dbd^.dbc^.InsertRec(dbd^.dbr) then
    ErrorMsg(0, 'JNData', 'Cannot insert record in TCATDEF file!');

  dbd^.dbr^.PutFieldAsStr(DBTSTCATID, ESBLIDstr);
  SR(IDS_ESBL, pstr, 30); {ESBL Screen}
  dbd^.dbr^.PutFieldAsStr(DBTSTCATDesc, pstr);
  dbd^.dbr^.PutFieldAsStr(DBTSTCATResLen, '20');
  if not dbd^.dbc^.InsertRec(dbd^.dbr) then
    ErrorMsg(0, 'JNData', 'Cannot insert record in TCATDEF file!');

  Dispose(dbd, Done);
end;


procedure JNTest;
var
  drug      : PDBFile;
  tcat      : PDBFile;
  dbd       : PDBFile;
  pstr      : array [0..32] of char;
  drugID    : array [0..5] of char;
  drugSeq   : TSeqNum;
  tcatSeq   : TSeqNum;
  micSeq    : TSeqNum;
begin
  drug := New(PDBFile, Init(DBDRUGFile, '', dbOpenExclusive));
  if drug = nil then
  begin
    ErrorMsg(0, 'JNData - DRUGDEF(Test)', dbErrorStr(dbLastOpenError, eStr, 255));
    Exit;
  end;

  tcat := New(PDBFile, Init(DBTSTCATFile, '', dbOpenExclusive));
  if drug = nil then
  begin
    ErrorMsg(0, 'JNData - TCATDEF(Test)', dbErrorStr(dbLastOpenError, eStr, 255));
    Exit;
  end;

  dbd := New(PDBFile, Init(DBTSTFile, '', dbOpenExclusive));
  if dbd = nil then
  begin
    ErrorMsg(0, 'JNData - TEST', dbErrorStr(dbLastOpenError, eStr, 255));
    Exit;
  end;

  if dbd^.dbc^.NumRecords <> 0 then
  begin
    ErrorMsg(0, 'JNData', 'TEST file is not empty!');
    Exit;
  end;

  dbd^.dbr^.PutFieldAsStr(DBTSTID, BIOstr);
  SR(IDS_BIOTYPE, pstr, 32); {Biotype}
  dbd^.dbr^.PutFieldAsStr(DBTSTName,  pstr);
  dbd^.dbr^.PutFieldAsStr(DBTSTTstCat, BIOIDstr);
  tcat^.dbr^.PutFieldAsStr(DBTSTCATID, BIOIDstr);
  tcat^.dbc^.GetEQ(tcat^.dbr);
  tcatSeq := tcat^.dbr^.GetSeqValue;
  dbd^.dbr^.PutField(DBTSTTstCatRef, @tcatSeq);
  if not dbd^.dbc^.InsertRec(dbd^.dbr) then
    ErrorMsg(0, 'JNData', 'Cannot insert record in TEST file!');

  dbd^.dbr^.PutFieldAsStr(DBTSTID, BLstr);
  SR(IDS_BETA_LACTAMASE, pstr, 32); {Beta-Lactamase}
  dbd^.dbr^.PutFieldAsStr(DBTSTName,  pstr);
  dbd^.dbr^.PutFieldAsStr(DBTSTTstCat, BLIDstr);
  tcat^.dbr^.PutFieldAsStr(DBTSTCATID, BLIDstr);
  tcat^.dbc^.GetEQ(tcat^.dbr);
  tcatSeq := tcat^.dbr^.GetSeqValue;
  dbd^.dbr^.PutField(DBTSTTstCatRef, @tcatSeq);
  if not dbd^.dbc^.InsertRec(dbd^.dbr) then
    ErrorMsg(0, 'JNData', 'Cannot insert record in TEST file!');

  dbd^.dbr^.PutFieldAsStr(DBTSTID, GMSstr);
  SR(IDS_GENTAMICIN_SYNERGY, pstr, 32); {Gentamicin Synergy}
  dbd^.dbr^.PutFieldAsStr(DBTSTName,  pstr);
  dbd^.dbr^.PutFieldAsStr(DBTSTTstCat, GMSIDstr);
  tcat^.dbr^.PutFieldAsStr(DBTSTCATID, GMSIDstr);
  tcat^.dbc^.GetEQ(tcat^.dbr);
  tcatSeq := tcat^.dbr^.GetSeqValue;
  dbd^.dbr^.PutField(DBTSTTstCatRef, @tcatSeq);
  if not dbd^.dbc^.InsertRec(dbd^.dbr) then
    ErrorMsg(0, 'JNData', 'Cannot insert record in TEST file!');

  dbd^.dbr^.PutFieldAsStr(DBTSTID, HEMstr);
  SR(IDS_BETA_HEMOLYSIS, pstr, 32); {Beta Hemolysis}
  dbd^.dbr^.PutFieldAsStr(DBTSTName,  pstr);
  dbd^.dbr^.PutFieldAsStr(DBTSTTstCat, HEMIDstr);
  tcat^.dbr^.PutFieldAsStr(DBTSTCATID, HEMIDstr);
  tcat^.dbc^.GetEQ(tcat^.dbr);
  tcatSeq := tcat^.dbr^.GetSeqValue;
  dbd^.dbr^.PutField(DBTSTTstCatRef, @tcatSeq);
  if not dbd^.dbc^.InsertRec(dbd^.dbr) then
    ErrorMsg(0, 'JNData', 'Cannot insert record in TEST file!');

  dbd^.dbr^.PutFieldAsStr(DBTSTID, INDstr);
  SR(IDS_INDOLE, pstr, 32); {Indole}
  dbd^.dbr^.PutFieldAsStr(DBTSTName,  pstr);
  dbd^.dbr^.PutFieldAsStr(DBTSTTstCat, INDIDstr);
  tcat^.dbr^.PutFieldAsStr(DBTSTCATID, INDIDstr);
  tcat^.dbc^.GetEQ(tcat^.dbr);
  tcatSeq := tcat^.dbr^.GetSeqValue;
  dbd^.dbr^.PutField(DBTSTTstCatRef, @tcatSeq);
  if not dbd^.dbc^.InsertRec(dbd^.dbr) then
    ErrorMsg(0, 'JNData', 'Cannot insert record in TEST file!');

  dbd^.dbr^.PutFieldAsStr(DBTSTID, OXIstr);
  SR(IDS_OXIDASE, pstr, 32); {Oxidase}
  dbd^.dbr^.PutFieldAsStr(DBTSTName,  pstr);
  dbd^.dbr^.PutFieldAsStr(DBTSTTstCat, OXIIDstr);
  tcat^.dbr^.PutFieldAsStr(DBTSTCATID, OXIIDstr);
  tcat^.dbc^.GetEQ(tcat^.dbr);
  tcatSeq := tcat^.dbr^.GetSeqValue;
  dbd^.dbr^.PutField(DBTSTTstCatRef, @tcatSeq);
  if not dbd^.dbc^.InsertRec(dbd^.dbr) then
    ErrorMsg(0, 'JNData', 'Cannot insert record in TEST file!');

  dbd^.dbr^.PutFieldAsStr(DBTSTID, STSstr);
  SR(IDS_STREPTOMYCIN_SYNERGY, pstr, 32); {Streptomycin Synergy}
  dbd^.dbr^.PutFieldAsStr(DBTSTName,  pstr);
  dbd^.dbr^.PutFieldAsStr(DBTSTTstCat, STSIDstr);
  tcat^.dbr^.PutFieldAsStr(DBTSTCATID, STSIDstr);
  tcat^.dbc^.GetEQ(tcat^.dbr);
  tcatSeq := tcat^.dbr^.GetSeqValue;
  dbd^.dbr^.PutField(DBTSTTstCatRef, @tcatSeq);
  if not dbd^.dbc^.InsertRec(dbd^.dbr) then
    ErrorMsg(0, 'JNData', 'Cannot insert record in TEST file!');

  dbd^.dbr^.PutFieldAsStr(DBTSTID, TFGstr);
  SR(IDS_THYMIDINE_FREE_GROWTH, pstr, 32); {Thymidine Free Growth}
  dbd^.dbr^.PutFieldAsStr(DBTSTName,  pstr);
  dbd^.dbr^.PutFieldAsStr(DBTSTTstCat, TFGIDstr);
  tcat^.dbr^.PutFieldAsStr(DBTSTCATID, TFGIDstr);
  tcat^.dbc^.GetEQ(tcat^.dbr);
  tcatSeq := tcat^.dbr^.GetSeqValue;
  dbd^.dbr^.PutField(DBTSTTstCatRef, @tcatSeq);
  if not dbd^.dbc^.InsertRec(dbd^.dbr) then
    ErrorMsg(0, 'JNData', 'Cannot insert record in TEST file!');

  tcat^.dbr^.PutFieldAsStr(DBTSTCATID, MICIDstr);
  tcat^.dbc^.GetEQ(tcat^.dbr);
  micSeq := tcat^.dbr^.GetSeqValue;

  tcat^.dbr^.PutFieldAsStr(DBTSTCATID, NCCLSIDstr);
  tcat^.dbc^.GetEQ(tcat^.dbr);
  tcatSeq := tcat^.dbr^.GetSeqValue;

  drug^.dbc^.GetFirst(drug^.dbr);
  repeat
    drug^.dbr^.GetFieldAsStr(DBDRUGAbbr, drugID, 5);
    drug^.dbr^.GetFieldAsStr(DBDRUGName, pstr, 30);
    drugSeq := drug^.dbr^.GetSeqValue;

    StrLCat(pstr, ' ', 32);
    StrLCat(pstr, MICIDstr, 32);

    dbd^.dbr^.PutField(DBTSTID, @drugID);
    dbd^.dbr^.PutField(DBTSTName, @pstr);
    dbd^.dbr^.PutField(DBTSTDrugRef, @drugSeq);
    dbd^.dbr^.PutFieldAsStr(DBTSTTstCat, MICIDstr);
    dbd^.dbr^.PutField(DBTSTTstCatRef, @micSeq);
    if not dbd^.dbc^.InsertRec(dbd^.dbr) then
      ErrorMsg(0, 'JNData', 'Cannot insert record in TEST file!');

    pstr[StrLen(pstr)-4] := #0;
    StrLCat(pstr, ' ', 32);
    StrLCat(pstr, NCCLSIDstr, 32);

    dbd^.dbr^.PutField(DBTSTName, @pstr);
    dbd^.dbr^.PutFieldAsStr(DBTSTTstCat, NCCLSIDstr);
    dbd^.dbr^.PutField(DBTSTTstCatRef, @tcatSeq);
    if not dbd^.dbc^.InsertRec(dbd^.dbr) then
      ErrorMsg(0, 'JNData', 'Cannot insert record in TEST file!');

    drug^.dbc^.GetNext(drug^.dbr);
  until drug^.dbc^.dbErrorNum <> 0;

  dbd^.dbr^.PutFieldAsStr(DBTSTID, ESBLstr);
  SR(IDS_ESBL, pstr, 32); {ESBL Screen}
  dbd^.dbr^.PutFieldAsStr(DBTSTName,  pstr);
  dbd^.dbr^.PutFieldAsStr(DBTSTTstCat, ESBLIDstr);
  tcat^.dbr^.PutFieldAsStr(DBTSTCATID, ESBLIDstr);
  tcat^.dbc^.GetEQ(tcat^.dbr);
  tcatSeq := tcat^.dbr^.GetSeqValue;
  dbd^.dbr^.PutField(DBTSTTstCatRef, @tcatSeq);
  if not dbd^.dbc^.InsertRec(dbd^.dbr) then
    ErrorMsg(0, 'JNData', 'Cannot insert record in TEST file!');

  Dispose(dbd, Done);
  Dispose(tcat, Done);
  Dispose(drug, Done);
end;


procedure JNTestGroup;
var
  dbd       : PDBFile;
  srcFile   : text;
  srcLine   : string;
  ID        : array [0..8] of char;
  pstr      : array [0..32] of char;
begin
{$I-}
  if IOResult <> 0 then ;

  Assign(srcFile, 'BATTERY.JN');
  Reset(srcFile);
  ioRet := IOResult;
  if ioRet <> 0 then
  begin
    Str(ioRet, eStr);
    ErrorMsg(0, 'JNData - IO Error - 13', eStr);
    Halt;
  end;
  dbd := New(PDBFile, Init(DBTSTGRPFile, '', dbOpenExclusive));
  if dbd = nil then
  begin
    ErrorMsg(0, 'JNData - TESTGRP', dbErrorStr(dbLastOpenError, eStr, 255));
    Exit;
  end;

  if dbd^.dbc^.NumRecords <> 0 then
  begin
    ErrorMsg(0, 'JNData', 'TESTGRP file is not empty!');
    Exit;
  end;

  while not Eof(srcFile) and (dbd^.dbc^.dbErrorNum = 0) do
  begin
    dbd^.dbr^.ClearRecord;
    if ReadFile(srcFile, srcLine, 23) then
    begin
      TrimAll(ID, StrPCopy(ID, Copy(srcLine, 1, 8)), ' ', 8);
      dbd^.dbr^.PutFieldAsStr(DBTSTGRPID, ID);
      TrimAll(pstr, StrPCopy(pstr, Copy(srcLine, 10, 32)), ' ', 32);
      dbd^.dbr^.PutFieldAsStr(DBTSTGRPDesc, pstr);
      StrPCopy(pstr, Copy(srcLine, 45, 1));
      dbd^.dbr^.PutFieldAsStr(DBTSTGRPSet, pstr);
      StrPCopy(pstr, Copy(srcLine, 50, 2));
      dbd^.dbr^.PutFieldAsStr(DBTSTGRPLayout, pstr);
      if not dbd^.dbc^.InsertRec(dbd^.dbr) then
        ErrorMsg(0, 'JNData', 'Cannot insert record in TESTGRP file!');
    end;
  end;

  Dispose(dbd, Done);
  Close(srcFile);
  if IOResult <> 0 then {nothing};
{$I+}
end;


procedure JNTestGroupDef;
var
  dbd       : PDBFile;
  todb      : PDBFile;
  tdb       : PDBFile;
  tcdb      : PDBFile;
  srcFile   : text;
  srcLine   : string;
  pstr      : array [0..32] of char;
  toID      : array [0..8] of char;
  testID    : array [0..5] of char;
  pos       : integer;
  len       : byte;
  toSeq     : TSeqNum;
  testSeq   : TSeqNum;
  ErrorStr  : Array[0..255] of char;

begin
{$I-}
  if IOResult <> 0 then ;

  Assign(srcFile, 'ADDDEF.JN');
  Reset(srcFile);
  ioRet := IOResult;
  if ioRet <> 0 then
  begin
    Str(ioRet, eStr);
    ErrorMsg(0, 'JNData - IO Error - 14', eStr);
    Halt;
  end;
  dbd := New(PDBFile, Init(DBTSTGRPXRFFile, '', dbOpenExclusive));
  if dbd = nil then
  begin
    ErrorMsg(0, 'JNData - TSTGPXRF', dbErrorStr(dbLastOpenError, eStr, 255));
    Exit;
  end;

  todb := New(PDBFile, Init(DBTSTGRPFile, '', dbOpenExclusive));
  if todb = nil then
  begin
    ErrorMsg(0, 'JNData - TESTGRP', dbErrorStr(dbLastOpenError, eStr, 255));
    Exit;
  end;

  tdb := New(PDBFile, Init(DBTSTFile, '', dbOpenExclusive));
  if tdb = nil then
  begin
    ErrorMsg(0, 'JNData - TEST', dbErrorStr(dbLastOpenError, eStr, 255));
    Exit;
  end;

  tcdb := New(PDBFile, Init(DBTSTCATFile, '', dbOpenExclusive));
  if tcdb = nil then
  begin
    ErrorMsg(0, 'JNData - TCATDEF', dbErrorStr(dbLastOpenError, eStr, 255));
    Exit;
  end;

  if dbd^.dbc^.NumRecords <> 0 then
  begin
    ErrorMsg(0, 'JNData', 'TSTGPXRF file is not empty!');
    Exit;
  end;

  while not Eof(srcFile) and (dbd^.dbc^.dbErrorNum = 0) do
  begin
    if ReadFile(srcFile, srcLine, 24) then
    begin
      TrimAll(toID, StrPCopy(toID, Copy(srcLine, 1, 8)), ' ', 8);
      TrimAll(testID, StrPCopy(testID, Copy(srcLine, 10, 5)), ' ', 5);

      todb^.dbr^.PutFieldAsStr(DBTSTGRPID, toID);
      todb^.dbc^.GetEQ(todb^.dbr);
      toSeq := todb^.dbr^.GetSeqValue;
      todb^.dbr^.GetField(DBTSTGRPLen, @pos, 2);

      tdb^.dbr^.PutFieldAsStr(DBTSTID, testID);
      tdb^.dbr^.PutFieldAsStr(DBTSTTstCat, @testID[1]);
      tdb^.dbc^.GetEQ(tdb^.dbr);
      testSeq := tdb^.dbr^.GetSeqValue;

      tcdb^.dbr^.PutFieldAsStr(DBTSTCATID, @testID[1]);
      tcdb^.dbc^.GetEQ(tcdb^.dbr);
      tcdb^.dbr^.GetField(DBTSTCATResLen, @len, 1);

      dbd^.dbr^.PutField(DBTSTGRPXRFTstGrpRef, @toSeq);
      dbd^.dbr^.PutField(DBTSTGRPXRFTstRef, @testSeq);
      dbd^.dbr^.PutField(DBTSTGRPXRFResBlockPos, @pos);
      if not dbd^.dbc^.InsertRec(dbd^.dbr) then
      begin
        StrCopy(ErrorStr,'Insert Failed [TstGrpDef1.TstGpXrf]: Panel ');
        StrCat(ErrorStr, toID);
        StrCat(ErrorStr, ', ');
        StrCat(ErrorStr, testID);
        ErrorMsg(0, 'JNData', ErrorStr);
      end;
      Inc(pos, len);

      todb^.dbr^.PutField(DBTSTGRPLen, @pos);
      todb^.dbc^.UpdateRec(todb^.dbr);
    end;
  end;

  if dbd^.dbc^.dbErrorNum <> 0 then
    ErrorMsg(0, 'JNData', dbErrorStr(dbd^.dbc^.dbErrorNum, eStr, 255));

  Close(srcFile);
  if IOResult <> 0 then {nothing};
  Assign(srcFile, 'BATDEF.JN');
  Reset(srcFile);
  ioRet := IOResult;
  if ioRet <> 0 then
  begin
    Str(ioRet, eStr);
    ErrorMsg(0, 'JNData - IO Error - 15', eStr);
    Halt;
  end;

  len := 1;
  while not Eof(srcFile) and (dbd^.dbc^.dbErrorNum = 0) do
  begin
    if ReadFile(srcFile, srcLine, 25) then
    begin
      TrimAll(toID, StrPCopy(toID, Copy(srcLine, 1, 8)), ' ', 8);
      TrimAll(testID, StrPCopy(testID, Copy(srcLine, 10, 5)), ' ', 5);

      todb^.dbr^.PutFieldAsStr(DBTSTGRPID, toID);
      todb^.dbc^.GetEQ(todb^.dbr);
      toSeq := todb^.dbr^.GetSeqValue;
      todb^.dbr^.GetField(DBTSTGRPLen, @pos, 2);

      tdb^.dbr^.PutFieldAsStr(DBTSTID, testID);
      tdb^.dbr^.PutFieldAsStr(DBTSTTstCat, MICIDstr);
      tdb^.dbc^.GetEQ(tdb^.dbr);
      testSeq := tdb^.dbr^.GetSeqValue;

      dbd^.dbr^.PutField(DBTSTGRPXRFTstGrpRef, @toSeq);
      dbd^.dbr^.PutField(DBTSTGRPXRFTstRef, @testSeq);
      dbd^.dbr^.PutField(DBTSTGRPXRFResBlockPos, @pos);
      if not dbd^.dbc^.InsertRec(dbd^.dbr) then
      begin
        StrCopy(ErrorStr,'Insert Failed [TstGrpDef2.TstGpXrf]: Panel ');
        StrCat(ErrorStr, toID);
        StrCat(ErrorStr, ', ');
        StrCat(ErrorStr, testID);
        ErrorMsg(0, 'JNData', ErrorStr);
      end;

      Inc(pos, len);

      tdb^.dbr^.PutFieldAsStr(DBTSTTstCat, NCCLSIDstr);
      tdb^.dbc^.GetEQ(tdb^.dbr);
      testSeq := tdb^.dbr^.GetSeqValue;

      dbd^.dbr^.PutField(DBTSTGRPXRFTstRef, @testSeq);
      dbd^.dbr^.PutField(DBTSTGRPXRFResBlockPos, @pos);
      if not dbd^.dbc^.InsertRec(dbd^.dbr) then
      begin
        StrCopy(ErrorStr,'Insert Failed [TstGrpDef3.TstGpXrf]: Panel ');
        StrCat(ErrorStr, toID);
        StrCat(ErrorStr, ', ');
        StrCat(ErrorStr, testID);
        ErrorMsg(0, 'JNData', ErrorStr);
      end;
      Inc(pos, len);

      todb^.dbr^.PutField(DBTSTGRPLen, @pos);
      todb^.dbc^.UpdateRec(todb^.dbr);
    end;
  end;

  Dispose(tcdb, Done);
  Dispose(tdb, Done);
  Dispose(todb, Done);
  Dispose(dbd, Done);
  Close(srcFile);
  if IOResult <> 0 then {nothing};
{$I+}
end;


procedure JNOrg;
var
  srcFile   : text;
  srcLine   : string;
  dbd       : PDBFile;
  orgID     : array [0..7] of char;
  setID     : array [0..1] of char;
  classID   : array [0..1] of char;
  shortName : array [0..32] of char;
  orgName   : array [0..64] of char;
  i, j      : integer;
begin
{$I-}
  if IOResult <> 0 then ;

  Assign(srcFile, 'ORGNAM.TXT');
  Reset(srcFile);
  ioRet := IOResult;
  if ioRet <> 0 then
  begin
    Str(ioRet, eStr);
    ErrorMsg(0, 'JNData - IO Error - 16', eStr);
    Halt;
  end;

  dbd := New(PDBFile, Init(DBORGFile, '', dbOpenExclusive));
  if dbd = nil then
  begin
    ErrorMsg(0, 'JNData - ORGANISM', dbErrorStr(dbLastOpenError, eStr, 255));
    Exit;
  end;

  if dbd^.dbc^.NumRecords <> 0 then
  begin
    ErrorMsg(0, 'JNData', 'ORGANISM file is not empty!');
    Exit;
  end;

  while not Eof(srcFile) and (dbd^.dbc^.dbErrorNum = 0) do
  begin
    if ReadFile(srcFile, srcLine, 26) then
    begin
      if Copy(srcLine, 1, 1) <> '*' then
      begin
        TrimAll(orgID, StrPCopy(orgID, Copy(srcLine, 2, 3)), ' ', 7);
        Val(orgID, i, j);
        StrPCopy(setID, Copy(srcLine, 6, 1));
        StrPCopy(classID, Copy(srcLine, 7, 1));
        TrimAll(shortName, StrPCopy(shortName, Copy(srcLine, 33, 16)), ' ', 32);
        TrimAll(orgName, StrPCopy(orgName, Copy(srcLine, 51, 64)), ' ', 64);
        j := StrLen(orgName) - 1;
        if orgName[j] = '''' then
          orgName[j] := #0;
        if (StrLen(orgName) > 0) and
          (i <> 998) and (i <> 999) and (i <> 0) then
        begin
          dbd^.dbr^.PutFieldAsStr(DBORGOrg, orgID);
          dbd^.dbr^.PutFieldAsStr(DBORGSName, shortName);
          dbd^.dbr^.PutFieldAsStr(DBORGLName, orgName);
          dbd^.dbr^.PutFieldAsStr(DBORGSet, setID);
          dbd^.dbr^.PutFieldAsStr(DBORGFamily, classID);
          if not dbd^.dbc^.InsertRec(dbd^.dbr) then
            ErrorMsg(0, 'JNData', 'Cannot insert record in ORGANISM file!');
        end;
      end;
    end;
  end;

  if dbd^.dbc^.dbErrorNum <> 0 then
    ErrorMsg(0, 'JNData', dbErrorStr(dbd^.dbc^.dbErrorNum, eStr, 255));

  Dispose(dbd, Done);
  Close(srcFile);
  if IOResult <> 0 then {nothing};
{$I+}
end;


procedure JNStat;
var
  dbd       : PDBFile;
  pstr      : array [0..30] of char;
begin
  dbd := New(PDBFile, Init(DBSTATFile, '', dbOpenExclusive));
  if dbd = nil then
  begin
    ErrorMsg(0, 'JNData - SPECSTAT', dbErrorStr(dbLastOpenError, eStr, 255));
    Exit;
  end;

  if dbd^.dbc^.NumRecords <> 0 then
  begin
    ErrorMsg(0, 'JNData', 'SPECSTAT file is not empty!');
    Exit;
  end;

  dbd^.dbr^.PutFieldAsStr(DBSTATID, '1');
  SR(IDS_PRELIM_STATUS, pstr, 30);
  dbd^.dbr^.PutFieldAsStr(DBSTATDesc, pstr);
  if not dbd^.dbc^.InsertRec(dbd^.dbr) then
    ErrorMsg(0, 'JNData', 'Cannot insert record in SPECSTAT file!');

  dbd^.dbr^.PutFieldAsStr(DBSTATID, '2');
  SR(IDS_FINAL_STATUS, pstr, 30);
  dbd^.dbr^.PutFieldAsStr(DBSTATDesc, pstr);
  if not dbd^.dbc^.InsertRec(dbd^.dbr) then
    ErrorMsg(0, 'JNData', 'Cannot insert record in SPECSTAT file!');

  Dispose(dbd, Done);
end;


procedure JNMPSS;
var
  dbd       : PDBFile;
  srcFile   : text;
  srcLine   : string;
  pstr      : array [0..30] of char;
begin
  Assign(srcFile, 'MPSSMIC.JN');
  Reset(srcFile);
  dbd := New(PDBFile, Init(DBMICFile, '', dbOpenExclusive));
  if dbd = nil then
  begin
    ErrorMsg(0, 'JNData - MPSSMIC', dbErrorStr(dbLastOpenError, eStr, 255));
    Exit;
  end;

  if dbd^.dbc^.NumRecords <> 0 then
  begin
    ErrorMsg(0, 'JNData', 'MPSSMIC file is not empty!');
    Exit;
  end;

  while not Eof(srcFile) and (dbd^.dbc^.dbErrorNum = 0) do
  begin
    if ReadFile(srcFile, srcLine, 27) then
    begin
      TrimAll(pstr, StrPCopy(pstr, Copy(srcLine, 1, 4)), ' ', 30);
      dbd^.dbr^.PutFieldAsStr(DBMICCode, pstr);
      TrimAll(pstr, StrPCopy(pstr, Copy(srcLine, 5, 5)), ' ', 30);
      dbd^.dbr^.PutFieldAsStr(DBMICDesc, pstr);
      if not dbd^.dbc^.InsertRec(dbd^.dbr) then
        ErrorMsg(0, 'JNData', 'Cannot insert record in MPSSMIC file!');
    end;
  end;
  Dispose(dbd, Done);
  Close(srcFile);
end;

BEGIN
  ClrScr;
  writeln('JN Data');
  writeln;
  writeln('Preparing files...');
  Prep;
  writeln('Creating Drugs...');
  JNDrug;
  writeln('Interpretations...');
  JNIntrp;
  writeln('Test Categories...');
  JNTestCat;
  writeln('Tests...');
  JNTest;
  writeln('Test Groups...');
  JNTestGroup;
  writeln('Test Group Definition...');
  JNTestGroupDef;
  writeln('Organisms...');
  JNOrg;
  writeln('Specimen Status...');
  JNStat;
  writeln('MPSS...');
  JNMPSS;
  writeln('Report formats...');
  ImportReport('REPORT.JN');
  writeln('Done!');
  MSWait(1000);
  DoneWinCrt;
END.

