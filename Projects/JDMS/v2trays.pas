unit V2Trays;
{-----------------------------------------------------------------------}
{                                                                       }
{ modified from version 2.25 Trays.pas                                  }
{                                                                       }
{-----------------------------------------------------------------------}
INTERFACE

uses
  WinTypes,
  WinProcs,
  Strings;

const
  MaxMdaEntries = 31; { Maximum number of entries in an Master Dilution Array.}
  MaxTray       = 75; { Max number of tray types    }

type
  TrayStr      = string[16];
  MastDilArray = array[0..MaxMdaEntries] of string[4];

  MapPair = record
    iDrgNum : byte;
    oDrgNum : byte;
    end;
  MapTable  = array [0..20] of MapPair;

  TrayRec = RECORD CASE integer OF
      1 : ( {1 + 1 + 17 + (5*32=160) + (2*21=42) + 1 + 1 + 261 = 484}
      { Record 0 }
    number  : byte;
    trayCnt : byte;
    aName   : TrayStr;
    mda     : MastDilArray;
    mapTbl  : MapTable;
    fstCust : byte;
    custCnt : byte;
    filler  : array[1..261] of byte);
    end;

  PTraysObject=^TTraysObject;
  TTraysObject=OBJECT
    gMda    : MastDilArray;
    gTrayFile: FILE OF Trayrec;
    gTray   : TrayRec;
    constructor INIT;
    procedure TrayAbort(ErrNum: integer);
    procedure LoadSTray(trayNum: integer);
    destructor DONE;
    end;

IMPLEMENTATION

const
  err_TrayLib   =  600;

procedure SetError(errorNum : integer);
{ Looks up the error number in the list of errors and displays the error
  text in a message box and waits for the user to acknowledge the error.}
var
  errStr : array[0..32] of char;
begin
  Str(errorNum, errStr);
  StrCat(ADDR(errStr),' Error occurred!');
  MessageBox(0, errStr, 'DMS Error Monitor',  mb_TaskModal or mb_OK or mb_IconStop);
end;


constructor TTraysObject.INIT;
{-----------------------------------------------------------------------}
{ INIT opens the tray record file and initializes variables for  }
{ the running of TrayLib routines.  This must be loaded in your root. }
{                 }
{ input  :  <none>              }
{ output :     <none>             }
{ The function returns the number of tray records or a  -1 on error. }
{-----------------------------------------------------------------------}
var
  i,
  j,
  k : integer;
  filName : array [0..30] of char;
begin
  Assign(gTrayFile, 'TRAYREC.DAT');
  if IOResult <> 0 then TrayAbort(10);
  Reset(gTrayFile);
  if IOResult <> 0 then TrayAbort(11);
  gTray.number := 0;
  LoadSTray(MaxTray+1);
  gMda := gTray.mda;
end;  { INIT }

destructor TTraysObject.DONE;
{-----------------------------------------------------------------------}
{-----------------------------------------------------------------------}
begin
  Close(gTrayFile);
end;

procedure TTraysObject.TrayAbort(ErrNum: integer);
{-----------------------------------------------------------------------}
{ purpose : Error out when some condition has been met.   }
{ input   : errnum  - The error message to be displayed.    }
{-----------------------------------------------------------------------}
begin
  SetError(ErrNum + err_TrayLib);   { add error num to base constant }
  Halt;
end;

procedure TTraysObject.LoadSTray(trayNum: integer);
{-----------------------------------------------------------------------}
{ purpose : Load into the global tray record variable, the record }
{     whose drn corrisponds with trayNum      }
{ input   : trayNum - The tray id number and record number - 1 }
{-----------------------------------------------------------------------}
var
  recNum : integer;
begin
  if (TrayNum < 1) OR (TrayNum > (MaxTray + 3)) then TrayAbort(2);
  if (TrayNum > MaxTray)
    then recNum := TrayNum - (MaxTray + 1)
    else recNum := TrayNum + 2;
  Seek(gTrayFile, recnum);
  if IOResult <> 0 then TrayAbort(3);
  Read(gTrayFile, gTray);
  if IOResult <> 0 then TrayAbort(4);
  if ((trayNum <> -1) AND (trayNum <> gTray.number))
    then TrayAbort(5);
end;


END.
