{$I-}

unit CvtMisc;

{- Miscellaneous constents used by the convert program (CONVERT.PAS }

INTERFACE

uses
  DBTypes,
  Objects,
  ODialogs,
  GridLib,
  DBFile,
  WinTypes,
  oWindows,
  MScan;

const
  {- MOD_CONVERT has been reserved in MSCAN. Range is $4600-$46FF }
  CVT_BASE      = MOD_CONVERT;       {- s/b $4600 }
  CVT_20BASE    = MOD_CONVERT + 56;  {- s/b $4638 }
  CVT_18BASE    = CVT_20BASE + 100;  {- s/b $469C }

  {- note: all directory consts and vars will have a \ appended to them }
  OldDataDir    = 'DMSRSTR\';   {- dir where old v2/v18 files are restored from floppy }
  MergeDataDir  = 'DMSMERGE\';  {- dir where old data is merged into }
(*  SaveDataDir   = 'DMSBACK\';   {- dir where v3.0 files are temporarily saved}*)

var
  CurDMSDir     : array[0..80] of char;  {- this is the working DMS dir (do not change)}

type
  TVersionTypes = (Version20, Version18);

type
  PMapObj = ^TMapObj;
  TMapObj = object(TObject)
    fldOldNum   : integer;
    fld3Num     : integer;    {- mapped field number }
    size        : word;
    fileName    : array[0..10] of char;
    fldOldName  : array[0..30] of char;
    fld3Name    : array[0..30] of char;
    fldType     : TDBFldTypes;  {- note: all seqRef types are assumed to be mnemonics
                                       for the purpose of this program }
    constructor Init(aFileName, aFldOldName: PChar; aFldOldNum: integer;
                     aSize: integer; aType: TDBFldTypes);
  end;

  PMapCollection = ^TMapCollection;
  TMapCollection = object(TCollection)
    vType : TVersionTypes;
    constructor Init(aVer: TVersionTypes);
    function  FindMappedField(aFile: PChar; aOldFldNum: integer): integer;
    function  V3FldMapped(aFile: PChar; aFld3Num: integer): integer;
    procedure AddField(aFldOldNum, aSize: word; aFile, aFldOldName: PChar;
                       aType: TDBFldTypes);
    procedure SetV3Fld(aRow, aFld3Num: integer; aFld3Name: PChar);
    procedure LogUnMappedFields;
    procedure LogMappedFields;
  end;

  PMapGrid = ^TMapGrid;
  TMapGrid = object(TGrid)
    maps  : PMapCollection;
    constructor InitResource(aParent: PWindowsObject; anID: integer; aMapCol: PMapCollection);
    procedure SetupWindow; virtual;
    procedure FillRowData(aRow: integer);
    procedure MapAField(aRow: integer);
    procedure ClearFieldMap(aRow: integer);
  end;

procedure AppendBackslash(aPath: PChar);

function CopyFile(source, destination: PChar; appendToDest: boolean): integer;
function MoveFile(source, destination: PChar): integer;
function MoveFiles(src, dest, mask: PChar): integer;
function CopyFiles(src, dest, mask: PChar): integer;

{- functions to get/set the saved data restore CRCs }
procedure ClearCRC(ver: TVersionTypes);
procedure SetLastCRC(ver: TVersionTypes; newCRC: word);
function GetLastCRC(ver: TVersionTypes; var crcVal: word): boolean;

{- clean a directory of files }
function CleanDir(adir, aMask: PChar): boolean;

procedure AddNumToString(sID: word; num: integer; pstr: PChar; maxLen: integer);
procedure AddNameToList(fName: PChar; aList: PListBox);

procedure GetUniqueFileName(aDir, aName: PChar);

procedure ShowLogFile(aParent: PWindowsObject);
procedure LogStart;
procedure LogEnd;
procedure LogStr(aStr: PChar);
procedure LogInt(num: longint);
procedure LogHex(num: longint);
procedure LogLn;
procedure CriticalError(msg: PChar);

function OpenV30File(aName: PChar): PDBFile;
function PrepareMergeDir: integer;

function OrderExists(aOrd, aDesc: PChar): TSeqNum;
function GroupExists(aGrp, aDesc: PChar): TSeqNum;

function ConfirmExit(hWindow: HWND): boolean;
procedure ShowCurrentIso(iso: PDBFile; aList: PListBox);
procedure FormatDMSField(aFld: PChar; len: integer);
function PromptForDisk(hWindow: HWND; title, msg: PChar; doRetry: boolean): boolean;
procedure FinalizeClean;

IMPLEMENTATION

uses
  WinProcs,
  APITools,
  DBIDS,
  IntlLib,
  StrsW,
  DMSErr,
  CvtMpFld,
  Strings,
  WinDos;

type
  PListRec = ^TListRec;
  TListRec = record      {- linked list of buffers }
    buf   : Pointer;
    size  : word;
    next  : PListRec;
  end;

var
  listTail  : PListRec;
  listHead  : PListRec;
  logFile   : Text;
  logging   : boolean;

procedure AppendBackslash(aPath: PChar);
{- append a backslash to path if none exists. It's assumed that aPath has
   enough room for the slash if one is required }
var
  len   : integer;
begin
  len:= StrLen(aPath);
  if len = 0 then
    StrCopy(aPath, '.\')
  else if aPath[len-1] <> '\' then
    StrCat(aPath, '\');
end;

function CleanDir(adir, aMask: PChar): boolean;
{- clean a directory,  aDir is the directory WITH a "\" appended to the end
   aMask is a file wildcard (ie, *.* or *.DAT).
   returns true if no error occured }
var
  path    : array[0..100] of char;
  filePath: array[0..100] of char;
  dirInfo : TSearchRec;
  aFile   : file;
  len     : integer;
  isOK    : boolean;
Begin
  isOK:= true;
  StrCopy(path, adir);
  AppendBackslash(path);
  StrCat(path, aMask);
  FindFirst(path, faArchive or faReadOnly, dirInfo);
  while (DosError = 0) and isOK do
  begin
    StrCopy(filePath, adir);
    AppendBackSlash(filePath);
    StrCat(filePath, dirInfo.name);
    Assign(aFile, filePath);
    SetFAttr(aFile, 0);
    Erase(afile);
    isOK:= (IOResult = 0);
    if not isOK then
      FatalError('Error Deleting File', filePath);
    FindNext(dirInfo);
  end;
  CleanDir:= isOK;
end;

procedure AddToList(buf: pointer; bytes: word);
{- add buf to tail of cbuf. list should point to the head of the list (or nil
   if no list exists yet) }
var
  aux   : PListRec;
begin
  GetMem(aux, SizeOf(TListRec));     {- allocate memory for list record }
  with aux^ do
  begin
    next:= nil;
    buf:= nil;
    size:= bytes;
  end;

  if listHead = nil then  {- no buffer list exists yet}
  begin
    listHead:= aux;
    listTail:= aux;
  end
  else
  begin                  {- find last buffer in list }
    listTail^.next:= aux;
    listTail:= aux;
  end;

  GetMem(aux^.buf, bytes);      {- allocate buffer for data }
  Move(buf^, aux^.buf^, bytes); {- copy data from buffer to list record }
End;

procedure FreeList;
{- free memory taken by list buffer. }
var
  temp    : PListRec;
begin
  while listHead <> nil do
  begin
    temp:= listHead;
    listHead:= listHead^.next;
    FreeMem(temp^.buf, temp^.size);   {- free buffer }
    FreeMem(temp, SizeOf(TListRec));   {- free list record }
  end;
  listHead:= nil;
  listTail:= nil;
end;

function CopyFile(source, destination: PChar; appendToDest: boolean): integer;
{- copy [src] to [dest], return 0 if no error }
var
  oldattr     : word;
  retval      : integer;
  src, dest   : file;
  tlist       : PListRec;
  buf         : pointer;      {- i/o buffer }
  numwritten  : word;
  numread     : word;
  bufsize     : word;
  temp        : longint;

  procedure OpenFiles;
  begin
    FileMode:= 0;  {- r/o }
    Assign(src, source);
(*    GetFAttr(src, oldattr);*)
(*    SetFAttr(src, faArchive);     {- remove any read-only attributes }*)
    Reset(src, 1);              {- open files (buffer size of 1) }
    retval:= IoResult;
    if retval <> 0 then
      exit;

    FileMode:= 2; {- r/w }
    Assign(dest, destination);
    if appendToDest then
    begin
      Reset(dest, 1);
      retVal:= IoResult;
      if retVal = 0 then
      begin
        Seek(dest, FileSize(dest));   {- seek to the end of the file }
        retVal:= IoResult;
      end;
    end
    else
    begin
      Rewrite(dest, 1);
      retval:= IoResult;
    end;
  end;

  procedure CloseFiles;
  begin
(*    SetFAttr(src, oldattr);     {- replace original attribute }*)
    Close(src);
    if IoResult <> 0 Then ;
    Close(dest);
    if IoResult <> 0 Then ;
  end;

begin
  OpenFiles;

  if retval = 0 then
  begin
    if MaxAvail > 128000 + 1024 then
      bufsize:= 64000
    else
    begin
      temp:= (MaxAvail shr 1) - 1024;   {- maxAvail/2 - 1024 }
      if temp > 0 then
        bufSize:= temp
      else
        retVal:= 8;                     {- not enough memory }
    end;
    {- NOTE: requires at least (bufsize*2) bytes available }
    {-       leave at least 1k (1024 bytes) free for slack}
  end;

  if retval = 0 then
  begin
    GetMem(buf, bufsize);     {- allocate I/O buffer }
    listHead:= nil;
    listTail:= nil;

    while not Eof(src) and (retval = 0) do
    begin
      BlockRead(src, buf^, bufsize, numread);   {- read a block from the file }
      retVal:= IoResult;
      if retVal = 0 then
      begin
        AddToList(buf, numread);    {- add chunk to list }

        {- see if there is no more memory for list, if so, write out list buffer}
        {- or, if the src file is completely read in, then write out list butter}
        if (MemAvail < (bufsize + 256)) or Eof(src) then
        begin
          tlist:= listHead;
          repeat
            numread:= tlist^.size;

            BlockWrite(dest, tlist^.buf^, numread, numwritten);
            If IoResult <> 0 Then
              retVal:= IoResult
            else if numwritten < numread then
              retVal:= 98;         {- disk is full }

            tlist:= tlist^.next;
          until (tlist = nil) or (numwritten <> numread) or (retval <> 0);
          FreeList;   {- clear the copy list }
        end;
      end;
    end;

    FreeMem(buf, bufsize);    {- free buffer space }

    if retVal = 0 then
    begin
      GetFTime(src, temp);      {- equate file dates/times to original file }
      SetFTime(dest, temp);
    end;
  end;

  CloseFiles;

  if retVal <> 0 then   {- delete dest file when error occurs }
  begin
    Erase(dest);
    if IoResult <> 0 then {- clear IoResult };
  end;

  CopyFile:= retval;
end;

function MoveFile(source, destination: PChar): integer;
{- move a file from source to destination }
{- if destination is on another drive then copy the source and delete it }
var
  s, d  : file;
  k     : integer;
begin
  if source <> destination then
  begin
    Assign(s, source);
    Rename(s, destination); {- first try to rename the file, this is faster than
                              copying it. Only works if src/dest are on same drive}
    k:= IoResult;
    if k = 17 then  {- attempting to rename across drives }
    begin
      k:= CopyFile(source, destination, false);
      if k = 0 then
      begin
        Erase(s);     {- only erase source file after is was copied succesfully }
        k:= IoResult;
      end;
    end;
    MoveFile:= k;
  end
  else
    MoveFile:= 0;
end;

function CopyFiles(src, dest, mask: PChar): integer;
{- copies files matching mask from src to dest. returns 0 if OK else
   returns error code }
var
  s, d  : array[0..100] of char;
  filepath : array[0..100] of char;
  ret   : integer;
  sr    : TSearchRec;
begin
  ret:= 0;
  StrCopy(s, src);
  AppendBackSlash(s);
  StrCat(s, mask);

  FindFirst(s, faArchive or faReadOnly, sr);
  while (DosError = 0) and (ret = 0) do
  begin
    StrCopy(s, src);
    AppendBackslash(s);
    StrCat(s, sr.name);

    StrCopy(d, dest);
    AppendBackslash(d);
    StrCat(d, sr.name);
    ret:= CopyFile(s, d, false);
    FindNext(sr);
  end;
  CopyFiles:= ret;
end;

function MoveFiles(src, dest, mask: PChar): integer;
{- moves files matching mask from src to dest. returns 0 if OK else
   returns error code. if a error occurs, NO files will be moved, however,
   the destination dir may contain some of the files copied }
var
  s, d  : array[0..100] of char;
  filepath : array[0..100] of char;
  ret   : integer;
  sr    : TSearchRec;
begin
  ret:= 0;
  StrCopy(s, src);
  AppendBackSlash(s);
  StrCat(s, mask);

  FindFirst(s, faArchive or faReadOnly, sr);
  while (DosError = 0) and (ret = 0) do
  begin
    StrCopy(s, src);
    AppendBackslash(s);
    StrCat(s, sr.name);

    StrCopy(d, dest);
    AppendBackslash(d);
    StrCat(d, sr.name);
    ret:= CopyFile(s, d, false);
    FindNext(sr);
  end;

  if ret = 0 then {- all files were copied OK }
  begin
    CleanDir(src, mask);
  end;

  MoveFiles:= ret;
end;

type
  TCRCRec = record
    v20crc  : word;
    v18crc  : word;
    v20Set  : boolean;
    v18Set  : boolean;
  end;

function GetLastCRC(ver: TVersionTypes; var crcVal: word): boolean;
{- return the last saved CRC for the specified version. Returns false if no
   CRC exists for specified version }
var
  rec     : TCRCRec;
  fil     : file of TCRCRec;
  ret     : boolean;
begin
  crcVal:= 0;
  ret:= false;

  Assign(fil, 'CVTCRC.FIL');
  Reset(fil);
  if IoResult = 0 then
    Read(fil, rec)
  else
  begin
    GetLastCRC:= ret;
    exit;   {- file does not exist }
  end;

  Close(fil);
  if IoResult <> 0 then {clear ioresult};

  case ver of
    Version20: if rec.v20Set then
               begin
                 CRCval:= rec.v20CRC;
                 ret:= true;
               end;
    Version18: if rec.v18Set then
               begin
                 CRCval:= rec.v18CRC;
                 ret:= true;
               end;
  end;
  GetLastCRC:= ret;
end;

procedure ClearCRC(ver: TVersionTypes);
var
  rec     : TCRCRec;
  fil     : file of TCRCRec;
begin
  Assign(fil, 'CVTCRC.FIL');
  Reset(fil);
  if IoResult <> 0 then
  begin
    exit;  {- no need to clear }
  end
  else
  begin
    Read(fil, rec);
    if IoResult <> 0 then
    begin
      Close(fil);
      if IoResult <> 0 then {clear ioresult};
      exit;
    end;
  end;

  case ver of
    Version20:
      begin
        rec.v20Set:= false;
        rec.v20CRC:= 0;
      end;
    Version18:
      begin
        rec.v18Set:= false;
        rec.v18CRC:= 0;
      end;
  end;
  Rewrite(fil);
  write(fil, rec);

  Close(fil);
  if IoResult <> 0 then {clear IO result };
end;

procedure SetLastCRC(ver: TVersionTypes; newCRC: word);
{- Set a CRC for a version restore }
var
  rec     : TCRCRec;
  fil     : file of TCRCRec;
begin
  Assign(fil, 'CVTCRC.FIL');
  Reset(fil);
  if IoResult <> 0 then
  begin
    Rewrite(fil);
    if IoResult <> 0 then   {- cant create the file }
      exit;
    FillChar(rec, sizeof(rec), 0);
    rec.v20Set:= false;
    rec.v18Set:= false;
  end
  else
  begin
    Read(fil, rec);
    if IoResult <> 0 then
    begin
      Close(fil);
      if IoResult <> 0 then {clear ioresult};
      exit;
    end;
  end;

  case ver of
    Version20:
      begin
        rec.v20Set:= true;
        rec.v20CRC:= newCRC;
      end;
    Version18:
      begin
        rec.v18Set:= true;
        rec.v18CRC:= newCRC;
      end;
  end;
  Rewrite(fil);
  write(fil, rec);

  Close(fil);
  if IoResult <> 0 then {clear IO result };
end;

procedure InitDirs;
{- initialize the curDMSDir, and Make sure temporary directories exist }
var
  path    : array[0..100] of char;

  procedure RemoveSlash(p: PChar);
  var
    len     : integer;
  begin
    len:= StrLen(p);
    if p[len-1] = '\' then
      p[len-1]:= #0;
  end;

begin
  GetCurDir(curDMSDir, 0);
  AppendBackSlash(curDMSDir);

(*  StrCopy(path, curDMSDir);
  StrCat(path, SaveDataDir);
  RemoveSlash(path);
  MkDir(path);
  if IoResult <> 0 then ;
*)
  StrCopy(path, curDMSDir);
  StrCat(path, OldDataDir);
  RemoveSlash(path);
  MkDir(path);
  if IoResult <> 0 then ;

  StrCopy(path, curDMSDir);
  StrCat(path, MergeDataDir);
  RemoveSlash(path);
  MkDir(path);
  if IoResult <> 0 then ;
end;

procedure AddNumToString(sID: word; num: integer; pstr: PChar; maxLen: integer);
{- sID = string resource ID. num = num to insert into string. pstr = final result }
var
  p1,p2 : array[0..200] of char;
  subst : PErrSubst;
begin
  Str(num, p1);
  LeftPad(p2, p1, '0', 3);

  subst:= New(PErrSubst, Init(p2));
  SR(sID, p2, sizeof(p2)-1);
  WVSPrintf(p1, p2, subst^.strs);
  Dispose(subst);

  StrLCopy(pstr, p1, maxlen);
end;

procedure AddNameToList(fName: PChar; aList: PListBox);
{- adds fName to list if fName does not already exist in the list }
var
  pstr    : array[0..200] of char;
  row     : integer;
begin
  if aList^.GetCount > 0 then
  begin
    aList^.GetString(pstr, aList^.GetCount-1);
    if StrComp(fName, pstr) <> 0 then
      row:= aList^.AddString(fname)
    else
      row:= aList^.GetCount - 1;
  end
  else
    row:= aList^.AddString(fName);
  aList^.SetSelIndex(row);
end;

procedure GetUniqueFileName(aDir, aName: PChar);
var
  fil   : file;
  tstr  : string[25];
  path  : string[80];
  numStr: string[4];
  uName : string[12];
  k, i  : integer;
  found : boolean;
begin
  AppendBackSlash(aDir);
  path:= StrPas(aDir);
  k:= 1;
  found:= false;
  repeat
    Str(k:4, numStr);
    for i:= 1 to Length(numStr) do
      if numStr[i] = ' ' then
        numStr[i]:= '0';
    uName:= 'CVT~' + numStr + '.DAT';
    tstr:= path + uName;
    Assign(fil, tstr);
    Reset(fil);
    if IoResult = 0 then
      Close(fil)
    else
    begin
      found:= true;
    end;
  until found or (k > 9999);
  StrPCopy(aName, uName);
end;

constructor TMapObj.Init(aFileName, aFldOldName: PChar; aFldOldNum: integer; aSize: integer;
                         aType: TDBFldTypes);
begin
  inherited Init;
  fldOldNum:= aFldOldNum;
  StrLCopy(fileName, aFileName, sizeof(fileName)-1);
  size:= aSize;
  StrLCopy(fldOldName, aFldOldName, sizeof(fldOldName)-1);
  fld3Num:= 0;  {- unmapped }
  StrCopy(fld3Name, '');
  fldType:= aType;
end;

constructor TMapCollection.Init(aVer: TVersionTypes);
begin
  inherited Init(25, 25);
  vType:= aVer;
end;

procedure TMapCollection.AddField(aFldOldNum, aSize: word; aFile,
                                  aFldOldName: PChar; aType: TDBFldTypes);
{- This function adds field information to the collection. This is information
   describes the v2/v18 field information. The mapped to v3 information will
   be cleared (no mappings).  The user must set this information to actually
   map fields.  This is done with the SetV3Fld function.
   aFldOldNum - This is for identification only. The application using this
                collection is responsible for understanding this number.
   aSize      - This is the size of the field. It valid only for fields of
                string type. The size must include the length byte/terminating null.
   aFile      - This is the file that is associated with aFld2Num. This name
                MUST be version 3.0 compatible. (ie, only 'SPECIMEN', 'PATIENT' or 'ISOLATE')
   aFldOldName- This is used for display purposes only. It should be the name
                of the V18/v20 field.
   aType      - This is the Version 3.0 field type in which this field is compatible.
                (dbDate, dbTime, dbSeqRef...). If the field is to be mapped to
                a mnemonic field, then aType must be dbSeqRef. }
var
  m   : PMapObj;
begin
  m:= New(PMapObj, Init(aFile, aFldOldName, aFldOldNum, aSize, aType));
  Insert(m);
end;

procedure TMapCollection.LogMappedFields;
{- this procedure will log all unmapped fields }
var
  foundOne  : boolean;
    fldOldNum   : integer;
    fld3Num     : integer;    {- mapped field number }
    size        : word;
    fileName    : array[0..10] of char;
    fldOldName  : array[0..30] of char;
    fld3Name    : array[0..30] of char;

  procedure LogIt(item: PMapObj); far;
  begin
    if item^.fld3Num <> 0 then
    begin
      foundOne:= true;
      LogStr(' -');
      LogStr(item^.fileName);
      LogStr(' : ');
      LogStr(item^.fldOldName);
      LogLn;
    end;
  end;

begin
  LogStr('List of MAPPED fields:'); logln;
  foundOne:= false;
  ForEach(@Logit);
  if not foundOne then
  begin
    LogStr('  No MAPPED fields found.');
    LogLn;
  end;
end;

procedure TMapCollection.LogUnMappedFields;
{- this procedure will log all unmapped fields }
var
  foundOne  : boolean;
    fldOldNum   : integer;
    fld3Num     : integer;    {- mapped field number }
    size        : word;
    fileName    : array[0..10] of char;
    fldOldName  : array[0..30] of char;
    fld3Name    : array[0..30] of char;

  procedure LogIt(item: PMapObj); far;
  begin
    if item^.fld3Num = 0 then
    begin
      foundOne:= true;
      LogStr(' -');
      LogStr(item^.fileName);
      LogStr(' : ');
      LogStr(item^.fldOldName);
      LogLn;
    end;
  end;

begin
  LogStr('List of UN-MAPPED fields:'); logln;
  foundOne:= false;
  ForEach(@Logit);
  if not foundOne then
  begin
    LogStr('  No UN-MAPPED fields found.');
    LogLn;
  end;
end;

procedure TMapCollection.SetV3Fld(aRow, aFld3Num: integer; aFld3Name: PChar);
{- set a version 3 field number for specified row.
   aRow     - The collection index of the field to be mapped.
   aFld3Num - This is the field number of the file associated with the mapobj
              at the specified index (aRow) that the field will be mapped into.
   aFld3Name- This is for display purposes only, and should be set to the proper
              v3 field name of the specified v3FldNum }
var
  m   : PMapObj;
begin
  if (aRow < Count) and (aRow >= 0) then
  begin
    m:= At(aRow);
    m^.fld3Num:= aFld3Num;
    StrCopy(m^.fld3Name, aFld3Name);
  end;
end;

function TMapCollection.FindMappedField(aFile: PChar; aOldFldNum: integer): integer;
{- for a given file and old Field number, find the field in the collection and
   return the version 3 field number that it is mapped to. 0 return means no mapping }

  function Matches(aMap: PMapObj): boolean; far;
  begin
    Matches:= (StrIComp(aFile, aMap^.fileName) = 0) and
              (aOldFldNum = aMap^.fldOldNum);
  end;

var
  m   : PMapObj;
begin
  m:= FirstThat(@Matches);
  if m = nil then
    FindMappedField:= 0
  else
    FindMappedField:= m^.fld3Num;
end;

function TMapCollection.V3FldMapped(aFile: PChar; aFld3Num: integer): integer;
{- for a given file and version 3 field number, find the field in the collection and
   return the index ot the field that it is mapped to.  -1 return means no mapping }

  function Matches(aMap: PMapObj): boolean; far;
  begin
    Matches:= (StrIComp(aFile, aMap^.fileName) = 0) and
              (aFld3Num = aMap^.fld3Num);
  end;

var
  m   : PMapObj;
begin
  m:= FirstThat(@Matches);
  if m = nil then
    V3FldMapped := -1
  else
    V3FldMapped := IndexOf(m);
end;

constructor TMapGrid.InitResource(aParent: PWindowsObject; anID: integer;
                                  aMapCol: PMapCollection);
begin
  inherited InitResource(aParent, anID, 8, false);
  maps:= aMapCol;
end;

const
  g_FileDisp    = 0;
  g_V2FldName   = 1;
  g_Type        = 2;
  g_V3FldName   = 3;
  g_FileName    = 4;
  g_Size        = 5;
  g_Fld2Num     = 6;
  g_Fld3Num     = 7;


procedure TMapGrid.SetupWindow;
var
  m   : PMapObj;
  k   : integer;
  pstr: array[0..100] of char;
  row : integer;

begin
  inherited SetupWindow;

  SetColumnWidth(g_FileDisp, 10);
  SetColumnWidth(g_V2FldName, 10);
  SetColumnWidth(g_Type, 10);
  SetColumnWidth(g_V3FldName, 10);

  SetHeader(g_FileDisp, 'File');
  SetHeader(g_Type, 'Type');
  SetHeader(g_V3FldName, 'Mapped to V3.02 Field');
  if maps^.vType = version20 then
    SetHeader(g_V2FldName, 'V2.x Field Name')
  else
    SetHeader(g_V2FldName, 'V18.10 Field Name');
  SetHeader(g_FileName, 'FileName');
  SetHeader(g_Size, 'Size');
  SetHeader(g_Fld2Num, 'Fld2Num');
  SetHeader(g_Fld3Num, 'Fld3Num');

  for k:= 0 to Pred(maps^.count) do
  begin
    row:= AddString(nil);
    FillRowData(row);
  end;
  OptColumnWidth(g_FileDisp);
  OptColumnWidth(g_V2FldName);
  OptColumnWidth(g_Type);
  StretchColumn(g_V3FldName);

  OptColumnWidth(g_FileName);
  OptColumnWidth(g_Size);
  OptColumnWidth(g_Fld2Num);
  OptColumnWidth(g_Fld3Num);
end;

procedure TMapGrid.FillRowData(aRow: integer);
{- refile current row from map collection and repaint }
var
  pstr : array[0..100] of char;
  m    : PMapObj;

  function PrevFileNameSame(aFile: PChar): boolean;
  var
    j       : integer;
    isSame  : boolean;
  begin
    isSame:= false;
    for j:= aRow downto 0 do
    begin
      GetData(g_FileDisp, j, pstr, sizeof(pstr)-1);
      if StrComp(pstr, aFIle) = 0 then
        isSame:= true
      else if StrLen(pstr) > 0 then
        break;
    end;
    PrevFIleNameSame:= isSame;
  end;

begin
  if (aRow < 0) or (aRow >= GetRowCount) then exit;
  m:= maps^.At(aRow);
  with m^ do
  begin
    SetRedraw(false);
    if not PrevFileNameSame(fileName) then
      SetData(g_FileDisp, aRow, fileName);
    SetData(g_FileName, aRow, fileName);
    SetData(g_V2FldName, aRow, fldOldName);
    SetData(g_V3FldName, aRow, fld3Name);
    if fldType = dbSeqRef then
      StrCopy(pstr, 'Mnemonic')
    else
      DBUserTypeStr(fldType, size, true, fileName, pstr, sizeof(pstr)-1);
    SetData(g_Type, aRow, pstr);
    Str(size, pstr);

    SetData(g_Size, aRow, pstr);

    Str(fldOldNum, pstr);
    SetData(g_Fld2Num, aRow, pstr);

    Str(fld3Num, pstr);
    SetData(g_Fld3Num, aRow, pstr);

    SetRedraw(true);
  end;
end;

procedure TMapGrid.MapAField(aRow: integer);
begin
  if (aRow >= 0) and (aRow < GetRowCount) then
  begin
    if GetMapping(parent, maps, aRow) then
      FillRowData(aRow);
  end
  else
    MessageBeep(0);
end;

procedure TMapGrid.ClearFieldMap(aRow: integer);
begin
  if (aRow >= 0) and (aRow < GetRowCount) then
  begin
    maps^.SetV3Fld(aRow, 0, '');
    FillRowData(aRow);
  end
  else
    MessageBeep(0);
end;

function LogOpen: boolean;
var
  ret : integer;
begin
  if not logging then
  begin
    LogOpen := false;
    Exit;
  end;
  Append(logFile);
  ret := IOResult;
  {- if ret <> 0 then InfoMsg(0, 'Fatal Error', 'ioret <> 0'); }
  LogOpen := (ret = 0);
end;

procedure LogStr(aStr: PChar);
begin
  if logOpen then
    write(logfile, aStr);
end;

procedure LogInt(num: longint);
begin
  if logOpen then
    write(logfile, num);
end;

procedure LogHex(num: longint);
var
  pstr  : array[0..100] of char;
begin
  HexStr(num, sizeof(num), pstr, sizeof(pstr)-1);
  TrimLead(pstr, pstr, '0', sizeof(pstr)-1);
  if logOpen then
    write(logFile, pstr);
end;

procedure LogLn;
begin
  if logOpen then
    writeln(logFile);
end;

procedure ShowLogFile(aParent: PWindowsObject);
begin
  Close(logFile);
  if IoResult = 0 then ;
  WinExec('notepad CVTLOG.TXT', SW_SHOWNORMAL);
end;

procedure LogStart;
{- create the log file (erasing any existing one) and open the log file for use.
   This function should be called before any other log functions }
var
  pstr  : array[0..100] of char;
begin
  StrCopy(pstr, curDMSDir);
  StrCat(pstr, 'CVTLOG.TXT');
  Assign(logFile, pstr);
  Rewrite(logFile);
  if IoResult = 0 then
  begin
    logging := true;
    write(logFile, 'Conversion Log - Started ');
    write(logFile, IntlDateStr(IntlCurrentDate, pstr, sizeof(pstr)-1));
    writeln(logFile, '  ', IntlTimeStr(IntlCurrentTime, false, pstr, sizeof(pstr)-1));
    Close(logFile);
  end
  else
  begin
    logging := false;
    CriticalError('Cannot open log file.');
  end;
end;

procedure LogEnd;
var
  pstr  : array[0..100] of char;
begin
  if logOpen then
  begin
    write(logFile, 'Convert Log - END ');
    write(logFile, IntlDateStr(IntlCurrentDate, pstr, sizeof(pstr)-1));
    writeln(logFile, '  ', IntlTimeStr(IntlCurrentTime, false, pstr, sizeof(pstr)-1));
    Close(logFile);
    logging := false;
  end;
end;


procedure CriticalError(msg: PChar);
begin
  FatalError('Fatal Error', msg);
  LogStr('Fatal Error Occured - ');
  LogStr(msg);
  LogLn;
  LogStr('Conversion Failed');
  LogLn;
  Close(logFile);
  Halt;
end;

var
  exitSave  : pointer;

procedure ExitRoutine; far;
begin
  exitProc:= exitSave;
  LogEnd;
end;

function OpenV30File(aName: PChar): PDBFile;
{- open a v3 database file in the merge directory }
var
  db  : PDBFile;
  dir : array[0..100] of char;
begin
  StrCopy(dir, curDMSDir);
  StrCat(dir, MergeDataDir);
  if dir[StrLen(dir)-1] = '\' then  {- remove trailing backslash }
    dir[StrLen(dir)-1]:= #0;
  db:= New(PDBFile, Init(aName, dir, dbOpenNormal));
  OpenV30File:= db;
end;

function PrepareMergeDir: integer;
{- prepare the merge directory with the contents of the online database }
var
  ret     : integer;
  pstr    : array[0..100] of char;
begin
  {- now clear out the merge directory }
  StrCopy(pstr, curDMSDir);
  StrCat(pstr, mergeDataDir);
  if not CleanDir(pstr, '*.*') then
    ret:= -1
  else
    ret:= 0;

  {- copy online database files to Merge directory }
  if ret = 0 then
  begin
    StrCopy(pstr, curDMSDir);
    StrCat(pstr, MergeDataDir);
    ret:= CopyFiles(curDMSDir, pstr, '*.DB*');
  end;
  PrepareMergeDir:= ret;
end;

function OrderExists(aOrd, aDesc: PChar): TSeqNum;
{- return order seq if aOrd exists, else return 0 }
var
  db  : PDBFile;
begin
  OrderExists:= 0;
  if aDesc <> nil then
    StrCopy(aDesc, '');
  db:= OpenV30File(DBORDFile);
  if db <> nil then
  begin
    db^.dbr^.PutFieldAsStr(DBORDID, aOrd);
    if db^.dbc^.GetEQ(db^.dbr) then
    begin
      OrderExists:= db^.dbr^.GetSeqValue;
      if aDesc <> nil then
        db^.dbr^.GetFieldAsStr(DBORDDesc, aDesc, 32);
    end;
    Dispose(db, Done);
  end;
end;

function GroupExists(aGrp, aDesc: PChar): TSeqNum;
{- return group seq if aGrp exists, else return 0 }
var
  db  : PDBFile;
begin
  GroupExists:= 0;
  if aDesc <> nil then
    StrCopy(aDesc, '');
  db:= OpenV30File(DBTSTGRPFile);
  if db <> nil then
  begin
    db^.dbr^.PutFieldAsStr(DBTSTGRPID, aGrp);
    if db^.dbc^.GetEQ(db^.dbr) then
    begin
      GroupExists:= db^.dbr^.GetSeqValue;
      if aDesc <> nil then
        db^.dbr^.GetFieldAsStr(DBTSTGRPDesc, aDesc, 32);
    end;
    Dispose(db, Done);
  end;
end;

function ConfirmExit(hWindow: HWND): boolean;
var
  b   : boolean;
begin
  b:= YesNoMsg(hWindow, 'Confirm',
           'Exit'#10#10'Are you sure you want to cancel the Convert Process?');
  if b then
  begin
    LogStr('* CONVERSION STOPPED BY USER *');
    LogLn;
  end;
  ConfirmExit:= b;
end;

procedure ShowCurrentIso(iso: PDBFile; aList: PListBox);
{- show isolate in list box }
var
  pstr  : array[0..100] of char;
  p2    : array[0..25] of char;
begin
  if aList^.GetCount > 20 then  {- clear the list out after 20 or so. Otherwise}
    aList^.ClearList;           {- the list may get to big }
  iso^.dbr^.GetFieldAsStr(DBISOSpecID, p2, sizeof(p2)-1);
  StrCopy(pstr, 'Spec: ');
  StrCat(pstr, p2);
  StrCat(pstr, '  ');
  iso^.dbr^.GetFieldAsStr(DBISOCollectDate, p2, sizeof(p2)-1);
  StrCat(pstr, p2);
  StrCat(pstr, '  Iso: ');
  iso^.dbr^.GetFieldAsStr(DBISOIso, p2, sizeof(p2)-1);
  StrCat(pstr, p2);
  AddNameToList(pstr, aList);
end;

procedure FormatDMSField(aFld: PChar; len: integer);
var
  num, code : integer;
  tstr      : array[0..50] of char;
begin
  Val(aFld, num, code);
  if code = 0 then
  begin
    LeftPad(tstr, aFld, '0', len);
    StrCopy(aFld, tstr);
  end;
end;

function PromptForDisk(hWindow: HWND; title, msg: PChar; doRetry: boolean): boolean;
{- doRetry = true for retry/cancel msgbox, otherwise OK/Cancel msgbox }
var
  isOK, ret : boolean;
begin
  isOK:= true;
  repeat
    if doRetry then
      ret:= RetryCancelMsg(hWindow, title, msg)
    else
      ret:= OkCancelMsg(hWindow, title, msg);
    if not ret then
      isOK:= ConfirmExit(hWindow);
  until isOK or ret;
  PromptForDisk:= ret;
end;

procedure FinalizeClean;
{- clean up the restore directory after a finalized convert }
var
  pstr  : array[0..100] of char;
  p2    : array[0..100] of char;
begin
  if ParamStr(1) = 'DEBUG' then
    exit;
  StrCopy(pstr, curDMSDir);
  StrCat(pstr, oldDataDir);
  if not CleanDir(pstr, '*.*') then
  begin
    StrCopy(p2, 'Error cleaning up restore directory: ');
    StrCat(p2, pstr);
    FatalError('Error', p2);
  end;
end;


BEGIN
  logging := false;
  exitSave:= exitProc;
  exitProc:= @ExitRoutine;
  InitDirs;
  listHead:= nil;
  listTail:= nil;
END.

{- rcf }
