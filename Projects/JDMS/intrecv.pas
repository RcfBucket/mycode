Unit IntRecv;

Interface

Uses
  ApiTools,
  WinTypes,
  Objects,
  OWindows,
  Strings,
  IntGlob,
  Parser,
  IntSend,
  BtrAPIW,
  FileIO,
  IntMap,
  IntIO,
  Trays,
  ResLib,
  WinCrt,
  MScan,
  Bits,
  WinProcs;

const
	PatientComment   = 'P';
	SpecimenComment = 'S';
  TrueStr: array[0..2] of char = 'T';


type
	IntHeaderType = record                                         {Field Nos}
		  							SendID:      array[0..40] of char;  {5}
                    RecvID:      array[0..40] of char;  {10}
                    ProcessID:   array[0..1] of char;   {12}
                    VerNum:      array[0..8] of char;   {13}
                  end;


procedure IntRecv_RecvData(CurrentWnd: HWnd; PWindowsObjPtr: PWindowsObject; SendRequest: boolean);
procedure IntRecv_RecvRec(Buf: PChar);

Implementation

Uses
  DBFile,
  DBIDs,
  DBLib,
  DBTypes,
  INILib,
  ODialogs,
  ListLib,
  Log,
  LogEvent,
  IntAstm,
  IntCstm,
  UICommon,
  Builder;

var
	CommentType    : Char;
  CurrPatSeq,
  CurrSpecSeq,
  CurrIsoSeq     : TSeqNum;
  CurrPatientID  : array[0..MaxPatIDLen] of char;
  CurrSpecimenID : array[0..MaxSpecIDLen] of char;
  CurrCollectDate: array[0..MaxDateLen] of char;
  Ver17SpecimenID : array[0..MaxSpecIDLen] of char;
  Ver17CollectDate: array[0..MaxDateLen] of char;
  CurrIsolateID  : array[0..MaxIsoIDLen] of char;
	RequestMade    : boolean;
  PatientFile,
  SpecimenFile,
  IsolateFile    : PDBFile;
  tempSpecRec,
  tempIsoRec     : PDBRec;
  tempSpecChgMap,
  tempIsoChgMap  : longint;
  UseFileXFer: boolean;
  UseVer17Flag: boolean;
	StringDelimiter: char;
  CharDelimiter: char;
  FirstIso: boolean;
  StoredBySpec, SpcRead, IsoRead: boolean;
  results: PResults;
  listObj: PListObject;
  listRec: PMnemListRec;


{
********************
Non-Object Functions
********************
}

function GetCurSession: TSeqNum;
{- returns the currently selected session sequence number. A zero is returned if
   the sessions are disabled. }
var
  retVal: TSeqNum;
  curID : array[0..50] of char;
  db    : PDBFile;
begin
  retVal:= 0;
  INIGetLastSession(curID, 50);  {- get ID of last session }
  if (StrLen(curID) > 0) then
  begin
    db:= New(PDBFile, Init(DBSessFile, '', dbOpenNormal));
    if db <> nil then
    begin
      db^.dbr^.PutField(DBSessID, @curID);
      if db^.dbc^.GetEQ(db^.dbr) then
      begin
        retVal:= db^.dbr^.GetSeqValue;
      end;
      MSDisposeObj(db);
    end;
  end;
  GetCurSession:= retVal;
end;

function TruncateField(CurrField: PChar;FieldLen:integer): PChar;
var
  TempField, TruncField: array[0..255] of char;

begin
  StrCopy(TempField, CurrField);
  if FieldLen <> -1 then
    StrLCopy(TruncField, TempField, FieldLen)
	else
    StrCopy(TruncField, TempField);
  TruncateField := TruncField;
end;

function PutDBField(aDBRec: PDBRec; fieldNum: integer; aStr: PChar): boolean;
var
  retVal  : boolean;
  fInfo   : TFldInfo;
  assInfo : TAssocInfo;
  seq     : TSeqNum;
  FieldLen: integer;
  tBool   : boolean;
  fName   : array[0..maxFileNameLen] of char;

begin
  retVal:= False;
  aDBRec^.Desc^.FileName(fName, SizeOf(fName)-1);
  if aDBRec^.Desc^.FieldInfo(fieldNum, fInfo) then
  begin
    if (fInfo.userFlag and db_Mnemonic = db_Mnemonic) then
    begin
      seq:= 0;
      if (fInfo.fldAssoc > 0) then
      begin
        if aDBRec^.Desc^.AssocInfo(fInfo.fldAssoc, assInfo) then
        begin
          if (StrIComp(fName, dbSpecFile) = 0) and (fieldNum = DBSPECStat) then
          begin
            if (StrIComp(aStr, 'Y') = 0) then { final }
              StrCopy(aStr, '2')
            else { preliminary }
              StrCopy(aStr, '1');
          end;
          if listObj^.FindMnemID(nil, assInfo.fName, aStr, listRec, True) then
          begin
            seq:= listRec^.seq;
          end;
        end;
      end;
      retVal:= aDBRec^.PutField(fieldNum, @seq);
    end
    else if (StrIComp(fName, dbIsoFile) = 0) and (fieldNum = DBISOTransFlag) then
    begin
      tBool:= (StrIComp(aStr, 'T') = 0);
      retVal:= aDBRec^.PutField(fieldNum, @tBool);
    end
    else if (fInfo.fldType = dbBoolean) then
    begin
      tBool:= (StrIComp(aStr, 'Y') = 0);
      retVal:= aDBRec^.PutField(fieldNum, @tBool);
    end
    else if (fInfo.fldType <> dbSeqRef) then
    begin
      FieldLen:= aDBRec^.Desc^.FieldSizeForStr(fieldNum)-1;
      StrCopy(aStr, TruncateField(aStr, FieldLen));
      retVal:= aDBRec^.PutFieldAsStr(fieldNum, aStr);
    end;
  end;
  PutDBField:= retVal;
end;

function GetXRef(Index, CompNo: integer;CurrField: PChar): PChar;
var
	XRefedField: array[0..255] of char;
	MnemType: array[0..2] of char;
	PrimFound: boolean;
	AltPrimRec: TAltPrimRec;
	AltSearchKey: TAltSearchKey;
  BtrvStat: integer;

begin
  StrCopy(XRefedField, CurrField);
	if (CustomCfgBuff.XRefChksCfg[Index] = bf_Checked) and
	   (CompNo = 1) then
	begin
		MnemType[0] := XRefMnemTypes[Index];
    MnemType[1] := #0;
		BtrvData[AltPrimFile].Size.Len       := SizeOf(TAltPrimRec);
		BtrvData[AltPrimFile].FileName.Name  := 'ALTPRIM.DAT   ';
    OpenCloseFiles(ParentWnd, B_Open, [AltPrimFile]);
    StrCopy(AltSearchKey.MType, MnemType);
    StrCopy(AltSearchKey.Alternate, XRefedField);
    BtrvStat := CallDB(B_Get_GTEQ, AltPrimFile, AltPrimRec.Start, AltSearchKey.Start, 1);
		PrimFound := False;
		while (BtrvStat = 0) and
          (StrComp(MnemType, AltPrimRec.MType) = 0) and
          (StrComp(XRefedField, AltPrimRec.Alternate) = 0) and
					(not PrimFound) do
		begin
      if StrComp(AltPrimRec.IsPrim, TrueStr) = 0 then
				PrimFound := True
      else
        BtrvStat := CallDB(B_Next, AltPrimFile, AltPrimRec.Start, AltSearchKey.Start, 1);
		end;
		if PrimFound then
      StrCopy(XRefedField, AltPrimRec.Mnemonic);
    OpenCloseFiles(ParentWnd, B_Close, [AltPrimFile]);
	end;
  GetXRef := XRefedField;
end;

function GetResultXRef(FieldNo, CompNo: integer;CurrField: PChar): PChar;
var
	Index: integer;
	XRefField: boolean;
	TempField: array[0..255] of char;

begin
	XRefField := True;
  StrCopy(TempField, CurrField);
	case FieldNo of
		 3: begin
					case CompNo of
						4: Index := 9;
						5: Index := 10;
						6: Index := 11;
					else
          	XRefField := False;
          end;
		 	  end;
		 4: Index := 12;
	else
    XRefField := False;
  end;
	if XRefField then
    StrCopy(TempField, GetXRef(Index, 1, CurrField));
	GetResultXRef := TempField;
end;

function GetTestOrdXRef(FieldNo, CompNo: integer;CurrField: PChar): PChar;
const
	TrueStr: string[2] = 'T';

var
	Index: integer;
	XRefField: boolean;
	TempField: array[0..255] of char;

begin
	XRefField := True;
	case FieldNo of
		 5: if CompNo = 4 then
				begin
					Index := 9;
          CompNo := 1; {Reset to one, so XRef will Work}
				end
				else
        	XRefField := False;
		16: Index := 2;
		17: Index := 0;
		28: Index := 1;
		30: Index := 4;
		31: Index := 5;
	else
    XRefField := False;
  end;
	if XRefField then
  begin
    StrCopy(TempField, GetXRef(Index, CompNo, CurrField));
		AnsiUpper(TempField);
	end
  else
    StrCopy(TempField, CurrField);
	if FieldNo = 3 then
		if CompNo = 1 then
      FormatSpecialNumField(TempField, MaxSpecIDLen)
		else
			if CompNo = 2 then
        FormatSpecialNumField(TempField, MaxIsoIDLen);
	GetTestOrdXRef := TempField;
end;

function GetPatXRef(FieldNo, CompNo: integer;CurrField: PChar): PChar;
const
	TrueStr: string[2] = 'T';

var
	Index: integer;
	XRefField: boolean;
	TempField: array[0..255] of char;

begin
	XRefField := True;
	case FieldNo of
		 9: Index := 7;
		14: Index := 0;
		25: Index := 3;
		26: Index := 1;
		33: Index := 4;
		34: Index := 5;
		35: Index := 8;
	else
    XRefField := False;
  end;
	if XRefField then
	begin
    StrCopy(TempField, GetXRef(Index, CompNo, CurrField));
    AnsiUpper(TempField);
	end
  else
    StrCopy(TempField, CurrField);
	if FieldNo = 3 then
    FormatSpecialNumField(TempField, MaxPatIDLen);
	GetPatXRef := TempField;
end;

function XRefField(CurrIntRecNo, FieldNo, CompNo: integer;CurrField: PChar): PChar;
var
	TempField: array[0..255] of char;

begin
	case CurrIntRecNo of
    0: StrCopy(TempField, GetPatXRef(FieldNo, CompNo, CurrField));
    1: StrCopy(TempField, GetTestOrdXRef(FieldNo, CompNo, CurrField));
    2: StrCopy(TempField, GetResultXRef(FieldNo, CompNo, CurrField));
	end;
  XRefField := TempField;
end;

procedure StoreSpecimen;
begin
  if (CurrSpecSeq > 0) and
     (StrComp(CurrSpecimenID, '') <> 0) and
     (StrComp(CurrCollectDate, '') <> 0) then {keys are not blank, store data}
  begin
    if SpecimenFile^.dbc^.UpdateRec(SpecimenFile^.dbr) then;
    if SpecimenFile^.dbr^.ClearRecord then;
    SpcRead := False;
  end;
end;

procedure StoreIsolate;
begin
  if (CurrSpecSeq > 0) and (CurrIsoSeq > 0) and
     (StrComp(CurrIsolateID, '') <> 0) and
     (StrComp(CurrSpecimenID, '') <> 0) and
     (StrComp(CurrCollectDate, '') <> 0) then {keys are not blank, store data}
  begin
    if UseVer17Flag then
      FirstIso := True;
    results^.SaveResults;
    if IsolateFile^.dbc^.UpdateRec(IsolateFile^.dbr) then;
    if IsolateFile^.dbr^.ClearRecord then;
    IsoRead := False;
  end;
end;

procedure ConvertPanelNumber(OrderID: PChar; maxLen: word);
var
  PanelID: array[0..9] of char;

begin
  GetPrivateProfileString('PanelXRef', OrderID, OrderID, PanelID, SizeOf(PanelID), 'IntCstm.ini');
  StrLCopy(OrderID, PanelID, maxLen);
end;

procedure UpdateRecvDlg(CurrIntRecNo: integer);
begin
	ProcessStatDlg^.Update;
  SetDlgItemText(ProcessStatDlg^.HWindow, id_ShowPatient, CurrPatientID);
	if CurrIntRecNo = 0 then {clear display of Spec and Iso IDs if new pat}
	begin
    SetDlgItemText(ProcessStatDlg^.HWindow, id_ShowSpecimen, '');
    SetDlgItemText(ProcessStatDlg^.HWindow, id_ShowIsolate, '');
	end
	else
  begin
    SetDlgItemText(ProcessStatDlg^.HWindow, id_ShowSpecimen, CurrSpecimenID);
    SetDlgItemText(ProcessStatDlg^.HWindow, id_ShowIsolate, CurrIsolateID);
	end;
  UpdateWindow(ProcessStatDlg^.HWindow);
end;

procedure LogField(IntRec, FieldNo: integer; AField: PChar);
var
	TempData: array[0..255] of char;

begin
	if LogFile^.CurrLogLevel <= 1 then {this is for log level 2 or 3}
  	Exit;
	if (IntRec = 0) and (FieldNo = 3) then {already logged keys}
		Exit;
	if (IntRec = 1) and ((FieldNo = 3) or (FieldNo = 5) or
											 (FieldNo = 8)) then
    Exit;
  if StrComp(AField, '') = 0 then {don't log field if null}
    Exit;
  Str(FieldNo, TempData);
  StrCat(TempData, ': ');
  StrCat(TempData, AField);
  LogFile^.LogEvent(0, RecvBit, lgevRcvField, TempData, -1);
end;

procedure TransferTempObjs;
var
	FieldIdx: integer;
  TempField: array[0..255] of char;

begin
  for FieldIdx:= 1 To SpecimenFile^.dbd^.NumFields do
  begin
    if TestBit(tempSpecChgMap, FieldIdx) then
    begin
      tempSpecRec^.GetFieldAsStr(FieldIdx, TempField, SizeOf(TempField)-1);
      if PutDBField(SpecimenFile^.dbr, FieldIdx, TempField) then;
    end;
  end;
  for FieldIdx:= 1 To IsolateFile^.dbd^.NumFields do
  begin
    if TestBit(tempIsoChgMap, FieldIdx) then
    begin
      tempIsoRec^.GetFieldAsStr(FieldIdx, TempField, SizeOf(TempField)-1);
      if PutDBField(IsolateFile^.dbr, FieldIdx, TempField) then;
    end;
  end;
end;

procedure ReadRecord(CurrIntRecNo: integer);
var
	IntDefFile: Text;
  FieldNo, CompNo, RepNo, i, FieldCount, IntIdx, DMSIdx, IntRecNo: integer;
  FieldName: array[0..SetupNameLen] of char;
	BlankChar: char;
  CurrField: array[0..255] of char;
	ToDMSMapRec: TToDMSMapRec;
	IntKey: TIntKey;
  TempUsed: boolean;
  CurrFile: PDBFile;
  TempRec : PDBRec;
  tempChgMap: ^longint;

begin
	FieldCount := 0;
	if UseVer17Flag then
    Assign(IntDefFile, V17SetupFiles[CurrIntRecNo])
  else
    Assign(IntDefFile, SetupFiles[CurrIntRecNo]);
	{$I-} Reset(IntDefFile); {$I+}
	if IOResult <> 0 then
    Exit;
	UpdateRecvDlg(CurrIntRecNo);
  while not Eof(IntDefFile) do
  begin
    Readln(IntDefFile, FieldNo, CompNo, RepNo, BlankChar, FieldName);
    StrCopy(CurrField, Parser_GetFieldRepCom(FieldNo, RepNo, CompNo));
		if not UseVer17Flag then
      LogField(CurrIntRecNo, FieldNo, CurrField);
		IntKey.IntRecNo := CurrIntRecNo;
		IntKey.IntIdxNo := FieldCount;
    BtrvStat := CallDB(B_Get_GTEQ, ToDMSMapFile, ToDMSMapRec.Start, IntKey.Start, 1);
		while (BtrvStat = 0) and
					(ToDMSMapRec.IntIdxNo = FieldCount) and
					(ToDMSMapRec.IntRecNo = CurrIntRecNo) do
		begin
      TempUsed := False;
			case ToDMSMapRec.DMSFileNo of
				0: begin
             CurrFile:= PatientFile;
           end;
				1: begin
             CurrFile:= SpecimenFile;
             if CurrIntRecNo = 0 then
             begin
               TempRec:= tempSpecRec;
               tempChgMap:= @tempSpecChgMap;
               TempUsed := True;
             end;
           end;
				2: begin
             CurrFile:= IsolateFile;
             if CurrIntRecNo = 0 then
             begin
               TempRec:= tempIsoRec;
               tempChgMap:= @tempIsoChgMap;
               TempUsed := True;
             end;
					 end;
			end;
      if UseVer17Flag then
      begin
        if (CurrIntRecNo = 2) and (FieldNo = 5) then {convert ver17 panel numbers to nihongo order ids}
          ConvertPanelNumber(CurrField, SizeOf(CurrField)-1);
      end
      else
      begin
        if StrComp(CurrField, '') <> 0 then
          StrCopy(CurrField, XRefField(CurrIntRecNo, FieldNo, CompNo, CurrField));
      end;

      if StrComp(CurrField, '') <> 0 then {CurrField is not Null}
        if StrComp(CurrField, '""') = 0 then {Delete field}
        begin
          if PutDBField(CurrFile^.dbr, ToDMSMapRec.DMSIdxNo, '') then;
        end
        else {Store/Overwrite field}
        begin
          if PutDBField(CurrFile^.dbr, ToDMSMapRec.DMSIdxNo, CurrField) then;
          if TempUsed then
          begin
            if PutDBField(TempRec, ToDMSMapRec.DMSIdxNo, CurrField) then
              SetBit(tempChgMap^, ToDMSMapRec.DMSIdxNo);
          end;
        end;
      BtrvStat := CallDB(B_Next, ToDMSMapFile, ToDMSMapRec.Start, IntKey.Start, 1);
    end;
		FieldCount := FieldCount + 1;
	end;
	if (CurrIntRecNo = 1) and (not UseVer17Flag) then
    TransferTempObjs;
	Close(IntDefFile);
end;

procedure ReadPatientRecord;
var
	CurrIntRecNo: integer;
  NewPatientID  : array[0..MaxPatIDLen] of char;

begin
  CommentType := PatientComment;
	CurrIntRecNo := 0;
  StrLCopy(NewPatientID, Parser_GetField(3), SizeOf(NewPatientID)-1); {get key}
  FormatSpecialNumField(NewPatientID, MaxPatIDLen);
  LogFile^.LogEvent(0, RecvBit, lgevRcvPat, NewPatientID, -1);
  {write previous record, if any}
  if StrComp(CurrPatientID, '') <> 0 then
	begin
    if PatientFile^.dbc^.UpdateRec(PatientFile^.dbr) then;
    if PatientFile^.dbr^.ClearRecord then;
    if SpcRead then
			StoreSpecimen;
    if IsoRead then
      StoreIsolate;
  end;
  if PatientFile^.dbr^.ClearRecord then;
  if (StrComp(NewPatientID, '') <> 0) then
  begin
    CurrPatSeq:= 0;
    if PatientFile^.dbr^.PutFieldAsStr(DBPATPatID, NewPatientID) then
    begin
      PatientFile^.dbr^.GetFieldAsStr(DBPATPatID, CurrPatientID, SizeOf(CurrPatientID)-1);
      if not PatientFile^.dbc^.GetEQ(PatientFile^.dbr) then
        PatientFile^.dbc^.InsertRec(PatientFile^.dbr);
      CurrPatSeq:= PatientFile^.dbr^.GetSeqValue;
    end;
  end;
  tempSpecRec^.ClearRecord;
  tempIsoRec^.ClearRecord;
  tempSpecChgMap:= 0;
  tempIsoChgMap:= 0;
  ReadRecord(CurrIntRecNo);
end;

procedure ReadTestOrderRecord;
var
	CurrIntRecNo: integer;
	TempField: array[0..255] of char;
  NewSpecimenID : array[0..MaxSpecIDLen] of char;
  NewCollectDate: array[0..MaxDateLen] of char;
  NewIsolateID  : array[0..MaxIsoIDLen] of char;
  NewOrderID  : array[0..MaxOrderIDLen] of char;
  OkToStoreSpc, OkToStoreIso: boolean;
  seq: TSeqNum;
  isoOrdFile: PDBFile;
  ordRec: PDBRec;
  setFamily: word;
  setByte: Byte;

begin
	CommentType := SpecimenComment;
	CurrIntRecNo := 1;
  StrLCopy(NewSpecimenID, Parser_GetFieldComponent(3, 1), SizeOf(NewSpecimenID)-1);
  FormatSpecialNumField(NewSpecimenID, MaxSpecIDLen);
  StrLCopy(NewIsolateID, Parser_GetFieldComponent(3, 2), SizeOf(NewIsolateID)-1);
  FormatSpecialNumField(NewIsolateID, MaxIsoIDLen);
  StrLCopy(NewCollectDate, Parser_GetField(8), SizeOf(NewCollectDate)-1);
  LogFile^.LogEvent(0, RecvBit, lgevRcvSpc, NewSpecimenID, -1);
  LogFile^.LogEvent(0, RecvBit, lgevRcvColDate, NewCollectDate, -1);
  LogFile^.LogEvent(0, RecvBit, lgevRcvIso, NewIsolateID, -1);
  StrCopy(TempField, Parser_GetFieldComponent(5, 4));
  { need to log BEFORE the cross-reference }
  if (StrLen(TempField) > 0) then
    LogFile^.LogEvent(0, RecvBit, lgevRcvOrd, TempField, -1);
  StrCopy(TempField, GetXRef(9, 1, TempField));
  StrLCopy(NewOrderID, TempField, SizeOf(NewOrderID)-1);

  OkToStoreSpc := SpcRead;
  OkToStoreIso := IsoRead;

	if OkToStoreSpc then
    StoreSpecimen;
  if OkToStoreIso then
    StoreIsolate;

  if SpecimenFile^.dbr^.ClearRecord then;
  if SpecimenFile^.dbr^.PutField(DBSPECPatRef, @CurrPatSeq) then;
  if SpecimenFile^.dbr^.PutFieldAsStr(DBSPECSpecID, NewSpecimenID) then;
  SpecimenFile^.dbr^.GetFieldAsStr(DBSPECSpecID, CurrSpecimenID, SizeOf(CurrSpecimenID)-1);
  if SpecimenFile^.dbr^.PutFieldAsStr(DBSPECCollectDate, NewCollectDate) then;
  SpecimenFile^.dbr^.GetFieldAsStr(DBSPECCollectDate, CurrCollectDate, SizeOf(CurrCollectDate)-1);

  CurrSpecSeq:= 0;
  if (StrLen(CurrSpecimenID) > 0) and (StrLen(CurrCollectDate) > 0) then
  begin
    if not SpecimenFile^.dbc^.GetEQ(SpecimenFile^.dbr) then
    begin
      if listObj^.FindMnemID(nil, DBStatFile, '1', listRec, True) then
        if SpecimenFile^.dbr^.PutField(DBSpecStat, @(listRec^.seq)) then;
      SpecimenFile^.dbc^.InsertRec(SpecimenFile^.dbr);
    end;
    CurrSpecSeq:= SpecimenFile^.dbr^.GetSeqValue;
  end;

  if IsolateFile^.dbr^.ClearRecord then;
  if IsolateFile^.dbr^.PutField(DBISOSpecRef, @CurrSpecSeq) then;
  if IsolateFile^.dbr^.PutFieldAsStr(DBISOSpecID, CurrSpecimenID) then;
  if IsolateFile^.dbr^.PutFieldAsStr(DBISOCollectDate, CurrCollectDate) then;
  if IsolateFile^.dbr^.PutFieldAsStr(DBISOIso, NewIsolateID) then;
  IsolateFile^.dbr^.GetFieldAsStr(DBISOIso, CurrIsolateID, SizeOf(CurrIsolateID)-1);
  seq:= GetCurSession;
  if IsolateFile^.dbr^.PutField(DBISOSess, @seq) then;
  CurrIsoSeq:= 0;
  if (CurrSpecSeq > 0) and (StrLen(CurrIsolateID) > 0) then
  begin
    if not IsolateFile^.dbc^.GetEQ(IsolateFile^.dbr) then
      IsolateFile^.dbc^.InsertRec(IsolateFile^.dbr);
    CurrIsoSeq:= IsolateFile^.dbr^.GetSeqValue;
  end;

  if (CurrIsoSeq > 0) then
  begin
    if IsolateFile^.dbr^.GetField(DBISOOrdRef, @seq, SizeOf(seq)) then;
    if (seq = 0) then
    begin
      if PutDBField(IsolateFile^.dbr, DBISOOrdRef, NewOrderID) then
      begin
        if listObj^.FindMnemID(nil, DBORDFile, NewOrderID, listRec, True) then
        begin
          setByte:= 0;
          ordRec:= listObj^.PeekSeq(DBORDFile, listRec^.seq);
          if (ordRec <> nil) then
            ordRec^.GetField(DBORDSet, @setByte, SizeOf(setByte));
          setFamily:= MakeSetFamily(setByte, 0);
          if IsolateFile^.dbr^.PutField(DBISOSetFamily, @setFamily) then;
        end;
        IsoOrdFile:= nil;
        if CreateOrders(nil, IsolateFile, IsoOrdFile, nil) then;
        if (IsoOrdFile <> nil) then
          MSDisposeObj(IsoOrdFile); {created (but not disposed) in CreateOrders}
      end;
    end;
  end;

  if results^.LoadResults(IsolateFile^.dbr) then;
  {Unhide ESBL, in case we need to set its value}
  results^.ReInsertESBLObj;
  ReadRecord(CurrIntRecNo);

  SpcRead := True;
  IsoRead := True;
end;

procedure StoreResult(TestType, DrugResultType, DataValue: PChar);
var
  AResult: boolean;
  Overwrite: boolean;
  i: integer;
  str2: String[2];
  setFamily: word;
  setByte, familyByte: byte;
  TestCatID, TestID: array [0..12] of char;

begin
  AResult := False;
  if StrComp(TestType, 'FAMILY') = 0 then
  begin
    if IsolateFile^.dbr^.GetField(DBISOSetFamily, @setFamily, SizeOf(setFamily)) then;
    setByte:= ExtractSet(setFamily);
    str2:= StrPas(DataValue);
    Val(str2, familyByte, i);
    setFamily:= MakeSetFamily(setByte, familyByte);
    if IsolateFile^.dbr^.PutField(DBISOSetFamily, @setFamily) then;
  end
  else if StrComp(TestType, 'ORG') = 0 then
  begin
    if listObj^.FindMnemID(nil, DBORGFile, DataValue, listRec, True) then
      if IsolateFile^.dbr^.PutField(DBISOOrgRef, @(listRec^.seq)) then;
  end
  else if (StrComp(TestType, 'BIO') = 0) or (StrComp(TestType, 'BL') = 0) or
          (StrComp(TestType, 'GmS') = 0) or (StrComp(TestType, 'HEM') = 0) or
          (StrComp(TestType, 'IND') = 0) or (StrComp(TestType, 'OXI') = 0) or
          (StrComp(TestType, 'StS') = 0) or (StrComp(TestType, 'TFG') = 0) or
          (StrComp(TestType, 'ESBL') = 0) then
  begin
    StrLCopy(TestCatID, TestType, SizeOf(TestCatID)-1);
    StrCopy(TestID, '.');
    StrLCat(TestID, TestType, SizeOf(TestID)-1);
    AResult:= True;
  end
  else if StrComp(DrugResultType,'1') = 0 then
  begin
    StrCopy(TestCatID, 'NCCLS');
    StrLCopy(TestID, TestType, SizeOf(TestID)-1);
    AResult:= True;
  end
  else if StrComp(DrugResultType,'0') = 0 then
	begin
    StrCopy(TestCatID, 'MIC');
    StrLCopy(TestID, TestType, SizeOf(TestID)-1);
    AResult:= True;
  end;
	if AResult then
  begin
    Overwrite := True;
    if StrComp(DataValue, '') = 0 then
		  Overwrite := False;
    if StrComp(DataValue, '""') = 0 then
      StrCopy(DataValue, '');
    results^.SetResult(TestID, TestCatID, DataValue, Overwrite);
  end;
end;

procedure ReadResultRecord;
var
  TestType: array[0..12] of char;
  DrugResultType: array[0..1] of char;
  DataValue: array[0..15] of char;
  CurrIntRecNo: integer;
  CurrField: array[0..255] of char;

begin
  CurrIntRecNo := 2;
  StrCopy(CurrField, Parser_GetFieldComponent(3, 5));
  StrLCopy(TestType, XRefField(CurrIntRecNo, 3, 5, CurrField), SizeOf(TestType)-1);
  StrCopy(CurrField, Parser_GetFieldComponent(3, 6));
  StrLCopy(DrugResultType, XRefField(CurrIntRecNo, 3, 6, CurrField), SizeOf(DrugResultType)-1);
  StrCopy(CurrField, Parser_GetField(4));
  StrLCopy(DataValue, XRefField(CurrIntRecNo, 4, 1, CurrField), SizeOf(DataValue)-1);
  StoreResult(TestType, DrugResultType, DataValue);
end;

procedure StoreComment(DMSFileNo: integer;CommentText: PChar);
var
  FieldLen: integer;

begin
	if CommentType = PatientComment then
  begin
    FieldLen:= PatientFile^.dbd^.FieldSizeForStr(DBPATFreeText)-1;
    StrCopy(CommentText, TruncateField(CommentText, FieldLen));
    if StrComp(CommentText, '') <> 0 then
      if StrComp(CommentText, '""') = 0 then
        PatientFile^.dbr^.ClearField(DBPATFreeText)
      else
        PatientFile^.dbr^.PutFieldAsStr(DBPATFreeText, CommentText);
	end
	else
	begin
    FieldLen:= SpecimenFile^.dbd^.FieldSizeForStr(DBSPECFreeText)-1;
    StrCopy(CommentText, TruncateField(CommentText, FieldLen));
    if StrComp(CommentText, '') <> 0 then
      if StrComp(CommentText, '""') = 0 then
        SpecimenFile^.dbr^.ClearField(DBSPECFreeText)
      else
        SpecimenFile^.dbr^.PutFieldAsStr(DBSPECFreeText, CommentText);
  end;
end;

procedure ReadCommentRecord;
var
	CommentMnem: array[0..255] of char;
	CommentText: array[0..255] of char;
  CommRecType: array[0..1] of char;

begin
  StrCopy(CommentMnem, Parser_GetFieldRepCom(4, 1, 1));
  StrCopy(CommentMnem, GetXRef(6, 1, CommentMnem));
  StrCopy(CommentText, Parser_GetFieldRepCom(4, 1, 2));
  StrLCopy(CommRecType, Parser_GetFieldRepCom(5, 1, 1), SizeOf(CommRecType)-1);
	if CommentType = PatientComment then {store comment in patient}
    StoreComment(0, CommentText)
	else {store comment in specimen}
    StoreComment(1, CommentText);
end;

procedure ReadRequestRecord;
var
  DateType, RequestStatus: array[0..2] of char;
	CompNo: integer;

begin
  RequestMade := True;
	with Ranges do
  begin
    StrLCopy(StartRange, Parser_GetFieldComponent(3, 1), SizeOf(StartRange)-1);
    if StrComp(StartRange, 'ALL') = 0 then {All data selected}
			Exit
		else
      if StrComp(StartRange, '') = 0 then {not patient range, check for specimen range}
    	begin
        StrLCopy(StartRange, Parser_GetFieldComponent(3, 2), SizeOf(StartRange)-1);
				CompNo := 2;
        StrCopy(RequestType, 'C');
			end
			else {Set CompNO for patient to get end range}
    	begin
				CompNo := 1;
        StrCopy(RequestType, 'P');
   	  end;
    if StrComp(StartRange, '') <> 0 then {start range exist, get end range}
 		begin
      StrLCopy(EndRange, Parser_GetFieldComponent(4, CompNo), SizeOf(EndRange)-1);
      if StrComp(EndRange, '') = 0 then {Single item}
        StrLCopy(EndRange, StartRange, SizeOf(EndRange)-1);
		end
    else {start range does not exist, get date ranges}
		begin
      StrLCopy(DateType, Parser_GetField(6), SizeOf(DateType)-1);
      StrLCopy(RequestType, DateType, SizeOf(RequestType)-1);
      StrLCopy(StartRange, Parser_GetField(7), SizeOf(StartRange)-1);
      StrLCopy(EndRange, Parser_GetField(8), SizeOf(EndRange)-1);
		end;
    StrLCopy(RequestStatus, Parser_GetField(13), SizeOf(RequestStatus)-1);
    ReTransmit:= (StrComp(RequestStatus, 'N') <> 0);
  end;
  LogFile^.LogEvent(0, RecvBit, lgevRcvReq, '', -1);
end;

procedure ReadTerminatorRecord;
var
	TerminationCode: array[0..1] of char;

begin
  if PatientFile^.dbc^.UpdateRec(PatientFile^.dbr) then;
  if SpcRead then
		StoreSpecimen;
  if IsoRead then
    StoreIsolate;
  StrLCopy(TerminationCode, Parser_GetField(3), SizeOf(TerminationCode)-1);
end;

procedure Read17Specimen;
var
	CurrIntRecNo: integer;
  NewSpecimenID : array[0..MaxSpecIDLen] of char;
  NewCollectDate: array[0..MaxDateLen] of char;

begin
  StrLCopy(NewSpecimenID, Parser_GetField(3), SizeOf(NewSpecimenID)-1);
  StrLCopy(NewCollectDate, Parser_GetField(10), SizeOf(NewCollectDate)-1);
  LogFile^.LogEvent(0, RecvBit, lgevRcvSpc, NewSpecimenID, -1);
  LogFile^.LogEvent(0, RecvBit, lgevRcvColDate, NewCollectDate, -1);

  if IsoRead then
    StoreIsolate;

	CommentType := SpecimenComment;
	CurrIntRecNo := 1;

	if SpcRead then
		StoreSpecimen;

  StrLCopy(CurrCollectDate, NewCollectDate, SizeOf(CurrCollectDate)-1);

  if SpecimenFile^.dbr^.ClearRecord then;
  if SpecimenFile^.dbr^.PutField(DBSPECPatRef, @CurrPatSeq) then;
  if SpecimenFile^.dbr^.PutFieldAsStr(DBSPECSpecID, NewSpecimenID) then;
  SpecimenFile^.dbr^.GetFieldAsStr(DBSPECSpecID, CurrSpecimenID, SizeOf(CurrSpecimenID)-1);
  if SpecimenFile^.dbr^.PutFieldAsStr(DBSPECCollectDate, CurrCollectDate) then;
  if not SpecimenFile^.dbc^.GetEQ(SpecimenFile^.dbr) then
    SpecimenFile^.dbc^.InsertRec(SpecimenFile^.dbr);
  CurrSpecSeq:= SpecimenFile^.dbr^.GetSeqValue;

	ReadRecord(CurrIntRecNo);
  SpcRead := True;
end;

procedure	Read17Results;
const
  NoOfNames = 6;
  TestNames: array[0..NoOfNames-1] of PChar = ('BIO','BL','OXI','STS','GMS','TFG');

var
  CurrIntRecNo, i: integer;
  TestType: array[0..6] of char;
  DrugResultType: array[0..1] of char;
  DataValue: array[0..15] of char;
  NewIsolateID  : array[0..MaxIsoIDLen] of char;
  NewOrderID  : array[0..MaxOrderIDLen] of char;
  IsoOrdFile: PDBFile;
  str2: String[2];
  setFamily: word;
  setByte, familyByte: byte;
  seq: TSeqNum;

begin
  if IsoRead then
    StoreIsolate;
	CurrIntRecNo := 2;

  StrLCopy(NewIsolateID, Parser_GetField(3), SizeOf(NewIsolateID)-1);
  StrLCopy(NewOrderID, Parser_GetField(5), SizeOf(NewOrderID)-1);
  ConvertPanelNumber(NewOrderID, SizeOf(NewOrderID)-1);

  LogFile^.LogEvent(0, RecvBit, lgevRcvIso, NewIsolateID, -1);
  LogFile^.LogEvent(0, RecvBit, lgevRcvOrd, NewOrderID, -1);

  if IsolateFile^.dbr^.ClearRecord then;
  if IsolateFile^.dbr^.PutField(DBISOSpecRef, @CurrSpecSeq) then;
  if IsolateFile^.dbr^.PutFieldAsStr(DBISOSpecID, CurrSpecimenID) then;
  if IsolateFile^.dbr^.PutFieldAsStr(DBISOCollectDate, CurrCollectDate) then;
  if IsolateFile^.dbr^.PutFieldAsStr(DBISOIso, NewIsolateID) then;
  seq:= GetCurSession;
  if IsolateFile^.dbr^.PutField(DBISOSess, @seq) then;
  IsolateFile^.dbr^.GetFieldAsStr(DBISOIso, CurrIsolateID, SizeOf(CurrIsolateID)-1);
  if not IsolateFile^.dbc^.GetEQ(IsolateFile^.dbr) then
    IsolateFile^.dbc^.InsertRec(IsolateFile^.dbr);
  CurrIsoSeq:= IsolateFile^.dbr^.GetSeqValue;

  { handle organism }
  StrLCopy(DataValue, Parser_GetField(12), SizeOf(DataValue)-1);
  if listObj^.FindMnemID(nil, DBORGFile, DataValue, listRec, True) then
  begin
    if IsolateFile^.dbr^.PutField(DBISOOrgRef, @(listRec^.seq)) then;
  end;

  { handle set/family }
  StrLCopy(DataValue, Parser_GetField(34), SizeOf(DataValue)-1);
  Val(DataValue, familyByte, i);
  StrLCopy(DataValue, Parser_GetField(35), SizeOf(DataValue)-1);
  Val(DataValue, setByte, i);
  setFamily:= MakeSetFamily(setByte, familyByte);
  if IsolateFile^.dbr^.PutField(DBISOSetFamily, @setFamily) then;

  if listObj^.FindMnemID(nil, DBORDFile, NewOrderID, listRec, True) then
  begin
    if IsolateFile^.dbr^.PutField(DBISOOrdRef, @(listRec^.seq)) then;
  end;

  IsoOrdFile:= nil;
  if CreateOrders(nil, IsolateFile, IsoOrdFile, nil) then;
  if (IsoOrdFile <> nil) then
    MSDisposeObj(IsoOrdFile); {created (but not disposed) in CreateOrders}

  if results^.LoadResults(IsolateFile^.dbr) then;

	ReadRecord(CurrIntRecNo);
  for i := 0 to NoOfNames-1 do
  begin
    StrLCopy(DataValue, Parser_GetField(i+14), SizeOf(DataValue)-1);
    StrLCopy(DataValue, GetXRef(12, 1, DataValue), SizeOf(DataValue)-1);
    StrLCopy(TestType, TestNames[i], SizeOf(TestType)-1);
    StrCopy(DrugResultType, '');
    StoreResult(TestType, DrugResultType, DataValue);
  end;

  {Field #20 contains Mpc's result. Ignore it, it has already come accross as an MIC result}

  StrLCopy(DataValue, Parser_GetField(22), SizeOf(DataValue)-1);
  StrLCopy(DataValue, GetXRef(12, 1, DataValue), SizeOf(DataValue)-1);
  StrCopy(TestType, 'HEM');
  StrCopy(DrugResultType, '');
  StoreResult(TestType, DrugResultType, DataValue);

  {Field #28, DMS v22.00 sticks a Pos, Neg, Sus ESBL result here}
  StrLCopy(DataValue, Parser_GetField(28), SizeOf(DataValue)-1);
  StrLCopy(DataValue, GetXRef(12, 1, DataValue), SizeOf(DataValue)-1);
  StrCopy(TestType, 'ESBL');
  StrCopy(DrugResultType, '');
  StoreResult(TestType, DrugResultType, DataValue);

  FirstIso := False;
  IsoRead := True;
end;

procedure Read17MIC;
var
  TestType: array[0..6] of char;
  DrugResultType: array[0..1] of char;
  DataValue: array[0..15] of char;

begin
  StrLCopy(TestType, Parser_GetField(3), SizeOf(TestType)-1);
  StrLCopy(TestType, GetXRef(10, 1, TestType), SizeOf(TestType)-1);
  StrLCopy(DataValue, Parser_GetField(5), SizeOf(DataValue)-1);
  StrLCopy(DataValue, GetXRef(12, 1, DataValue), SizeOf(DataValue)-1);
  StrCopy(DrugResultType, '0');
  StoreResult(TestType, DrugResultType, DataValue);
  StrLCopy(TestType, Parser_GetField(3), SizeOf(TestType)-1);
  StrLCopy(TestType, GetXRef(10, 1, TestType), SizeOf(TestType)-1);
  StrLCopy(DataValue, Parser_GetField(8), SizeOf(DataValue)-1);
  StrLCopy(DataValue, GetXRef(12, 1, DataValue), SizeOf(DataValue)-1);
  StrCopy(DrugResultType, '1');
  StoreResult(TestType, DrugResultType, DataValue);
end;

procedure ReadHeaderRecord;
var
	HeaderBuff: IntHeaderType;

begin
	if UseVer17Flag then
  begin
    FieldDelimiter := SingleRecord[98];
    StringDelimiter := SingleRecord[99];
		CharDelimiter := '?';
	end
	else
  begin
		FieldDelimiter := SingleRecord[2];
		RepeatDelimiter := SingleRecord[3];
		ComponentDelimiter := SingleRecord[4];
		EscapeCharDelimiter := SingleRecord[5];
		with HeaderBuff do
		begin
      StrLCopy(ProcessID, Parser_GetField(12), SizeOf(ProcessID)-1);
      StrLCopy(SendID, Parser_GetField(5), SizeOf(SendID)-1);
      StrLCopy(RecvID, Parser_GetField(10), SizeOf(RecvID)-1);
      StrLCopy(VerNum, Parser_GetField(13), SizeOf(VerNum)-1);
		end;
	end;
end;

procedure ProcessRecord;
begin
	if UseVer17Flag then
		case RecordID of
      'H':ReadHeaderRecord;
      'P':ReadPatientRecord;
      'B':Read17Specimen;
			'R':Read17Results;
			'M':Read17MIC;
			'L':ReadTerminatorRecord;
		else
			;{invalid Record}
		end
  else
		case RecordID of
			'H':ReadHeaderRecord;
			'P':ReadPatientRecord;
			'C':ReadCommentRecord;
			'O':ReadTestOrderRecord;
      'R':ReadResultRecord;
			'Q':ReadRequestRecord;
			'L':ReadTerminatorRecord;
		else
			;{invalid record}
		end;
end;

procedure InitializeCurrIDs;
begin
  StrCopy(CurrPatientID, '');
  StrCopy(CurrSpecimenID, '');
  StrCopy(CurrCollectDate, '');
  StrCopy(CurrIsolateID, '');
  CurrPatSeq:= 0;
  CurrSpecSeq:= 0;
  CurrIsoSeq:= 0;
end;

function OpenFiles(CurrWnd: HWnd): integer;
var
	OpenFileError: integer;

begin
	BtrvData[ToDMSMapFile].Size.Len       := SizeOf(TToDMSMapRec);
  BtrvData[ToDMSMapFile].FileName.Name  := 'TODMSMAP.DAT  ';
  OpenCloseFiles(CurrWnd, B_Open, [ToDMSMapFile]);
  OpenFileError := BtrvStat;
  OpenFiles := OpenFileError;
  PatientFile:= New(PDBFile, Init(DBPATFile, '', dbOpenNormal));
  SpecimenFile:= New(PDBFile, Init(DBSPECFile, '', dbOpenNormal));
  IsolateFile:= New(PDBFile, Init(DBISOFile, '', dbOpenNormal));
  tempSpecRec:= New(PDBRec, Init(SpecimenFile^.dbc));
  tempIsoRec:= New(PDBRec, Init(IsolateFile^.dbc));
  results:= new(PResults, Init);
  results^.doYield:= True;
  listRec:= new(PMnemListRec, Init);
  listObj:= new(PListObject, Init);
  tempSpecChgMap:= 0;
  tempIsoChgMap:= 0;
end;

procedure CloseFiles(CurrWnd: HWnd);
begin
  OpenCloseFiles(CurrWnd, B_Close, [ToDMSMapFile]);
  MSDisposeObj(IsolateFile);
  MSDisposeObj(SpecimenFile);
  MSDisposeObj(PatientFile);
  MSDisposeObj(tempIsoRec);
  MSDisposeObj(tempSpecRec);
  MSDisposeObj(results);
  MSDisposeObj(listRec);
  MSDisposeObj(listObj);
end;

procedure InitializeReceive(CurrentWnd: HWnd; PWindowsObjPtr: PWindowsObject);
var
	tstr: array[0..3] of char;
  TempMsg: array[0..25] of char;

begin
  DisableMenu(GlobHWnd);
  { set up default delimeters just in case a header record is not received }
  FieldDelimiter := FieldDel;
  RepeatDelimiter := RepeatDel;
  ComponentDelimiter := ComponentDel;
  EscapeCharDelimiter := EscapeDel;
  IntCstm_InitCustomCfgBuff;
{Removed v17 (v18) support - kmr 10/95}
(*  GetPrivateProfileString('IntCstm', 'UseVer17', '', tstr, SizeOf(tstr), 'IntCstm.ini');*)
(*  if StrComp(tstr, 'Yes') = 0 then*)
(*    UseVer17Flag := True*)
(*  else*)
    UseVer17Flag := False;
  RequestMade := False;
  ProcessStatDlg:= New(PProcessStatusDlg, Init(PWindowsObjPtr, 'INT_PROCESS_DATA'));
	ProcessStatDlg^.EnableAutoCreate;
  if ShowReceive then
  begin
    Application^.MakeWindow(ProcessStatDlg);
    ShowWindow(ProcessStatDlg^.HWindow, SW_SHOW);
  end;
  LoadString(HInstance, str_IntBase+str_RecvData, TempMsg, SizeOf(TempMsg));
  SetWindowText(ProcessStatDlg^.HWindow, TempMsg);
	ProcessStatDlg^.Abort := False;
  OpenFiles(CurrentWnd);
	InitializeCurrIDs;
  SpcRead := False;
  IsoRead := False;
  FirstIso := True;
end;

procedure EndReceive(CurrentWnd: HWnd; PWindowsObjPtr: PWindowsObject);
var
  LastCursor: HCursor;
begin
  CloseFiles(CurrentWnd);
	if ProcessStatDlg^.Abort then
		Terminate;
  ProcessStatDlg^.CloseWindow;
  if not FirstReceive then
    LogFile^.LogEvent(0, RecvBit, lgevEndRcv, '', -1);
  if RequestMade then
    IntSend_SendRequestedData(CurrentWnd, PWindowsObjPtr);
	FirstReceive := True;
  LastCursor := SetCursor(LoadCursor(0, idc_Wait));
  LogFile^.ClearOldRecs;
  SetCursor(LastCursor);
  EnableMenu(GlobHWnd);
end;

procedure IntRecv_RecvRec(Buf: PChar);
begin
  StrCopy(SingleRecord, Buf);
	if FirstReceive then
	begin
    InitializeReceive(GlobHWnd, GlobWndObj);
	end;
  if (StrComp(SingleRecord, '') = 0) or (ProcessStatDlg^.Abort) then
    EndReceive(GlobHwnd, GlobWndObj)
  else
  begin
    FirstReceive := False;
    if (UseVer17Flag) and (SingleRecord[1] = 'H') then
      RecordID := 'H'
    else
      RecordID := SingleRecord[0];

    ProcessRecord;

    if (recordID = 'L') and (usexon) and (not usemicroscan) then
      endreceive(globhwnd, globwndobj);
  end;
end;

procedure IntRecv_RecvData(CurrentWnd: HWnd; PWindowsObjPtr: PWindowsObject; SendRequest: boolean);
var
	tstr: array[0..3] of char;
  TempMsg: array[0..10] of char;

begin
  UseFileXFer := False;
  LoadString(HInstance, str_IntBase+str_File, TempMsg, SizeOf(TempMsg));
  if StrComp(CustomCfgBuff.CstmParmsCfg[0].ComboSelection, TempMsg) = 0 then
		UseFileXFer := True;
  if UseFileXFer then
    IntIO_GetFileData {read from file}
  else
    IntSend_SendRequest(CurrentWnd, PWindowsObjPtr); {Recv data from LIS via request record}
end;

Begin
End.
