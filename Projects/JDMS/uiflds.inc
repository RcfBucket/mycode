(****************************************************************************


uiflds.inc

produced by Borland Resource Workshop

UIFLDS base is $2000 = 8192

*****************************************************************************)

const
  DLG_UIFREETEXT        = 8192;

  IDC_FREETEXT          = 201;
  IDC_UICLEAR           = 101;

  IDS_CANTINIT          = 8192;
  IDS_CANTINITLISTREC   = 8193;
  IDS_MNEMLISTERR       = 8194;
  IDS_UIDELFREE         = 8195;
  IDS_UINOMNEMFILE      = 8196;
	IDC_SELLIST	=	101;
	IDS_UIFLDSMNEMIDNOTFOUND	=	8197;
	IDS_UIFLDSINVALIDNUMBER	=	8198;
	IDS_UIFLDSOUTOFRANGE	=	8199;
	IDS_UIFLDSMEMALLOCERR	=	8200;
	IDS_UIFLDSINVALIDFLD	=	8201;
	IDS_UIFLDSFLDREQ	=	8202;
	IDS_UIFLDSFLDENTERR	=	8203;
	IDS_UIFLDSINVINIT	=	8204;
	IDS_UIFLDSINVFLDNUM	=	8205;
	IDS_INCOMPATIBLESTRUCT	=	8206;
