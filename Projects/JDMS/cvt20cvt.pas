unit Cvt20Cvt;

{- this unit drives the conversion process for version 2.0 data }

INTERFACE

uses
  CvtMisc,
  OWindows;

procedure Cvt20ConvertData(aParent: PWindowsObject; aMaps: PMapCollection);

IMPLEMENTATION

uses
  ApiTools,
  WinProcs,
  WinTypes,
  ODialogs,
  Cvt20Lib,
  Strings,
  UserMsgs,
  DlgLib;

{$I CVT20.INC}

type
  PCvtDlg = ^TCvtDlg;
  TCvtDlg = object(TCenterDlg)
    maps     : PMapCollection;
    stopped  : boolean;
    working  : boolean;
    finalizing: boolean;
    list     : PListBox;
    {}
    constructor Init(aParent: PWindowsObject; aMaps: PMapCollection);
    procedure SetupWindow; virtual;
    procedure StartProcess(var msg: TMessage); virtual WM_FIRST + WM_FMPROCESS;
    procedure IDStop(var msg: TMessage); virtual ID_FIRST + IDC_STOP;
    procedure IDFinish(var msg: TMessage); virtual ID_FIRST + IDC_FINISH;
    procedure IDLog(var msg: TMessage); virtual ID_FIRST + IDC_LOG;
    procedure Cancel(var msg: TMessage); virtual ID_FIRST + ID_CANCEL;
    procedure EnableButtons; virtual;
    function CanClose: boolean; virtual;
  end;

constructor TCvtDlg.Init(aParent: PWindowsObject; aMaps: PMapCollection);
begin
  inherited Init(aParent, MakeIntResource(DLG_CVT20CONVERT));
  list:= New(PListBox, InitResource(@self, IDC_LIST));
  maps:= aMaps;
  stopped:= false;
  working:= true;
  finalizing:= false;
end;

procedure TCvtDlg.SetupWindow;
begin
  inherited SetupWindow;
  PostMessage(hWindow, WM_FMPROCESS, 0, 0);
end;

procedure TCvtDlg.EnableButtons;
begin
  if stopped then
  begin
    EnableWindow(GetItemHandle(IDC_FINISH), false);
    EnableWindow(GetItemHandle(IDC_STOP), false);
    EnableWindow(GetItemHandle(IDC_LOG), false);
    EnableWindow(GetItemHandle(ID_OK), true);
  end
  else if finalizing then
  begin
    EnableWindow(GetItemHandle(IDC_FINISH), false);
    EnableWindow(GetItemHandle(IDC_STOP), false);
    EnableWindow(GetItemHandle(IDC_LOG), not working);
    EnableWindow(GetItemHandle(ID_OK), not working);
  end
  else if working then
  begin
    EnableWindow(GetItemHandle(IDC_FINISH), false);
    EnableWindow(GetItemHandle(IDC_STOP), true);
    EnableWindow(GetItemHandle(IDC_LOG), false);
    EnableWindow(GetItemHandle(ID_OK), false);
  end
  else
  begin
    EnableWindow(GetItemHandle(IDC_FINISH), true);
    EnableWindow(GetItemHandle(IDC_STOP), false);
    EnableWindow(GetItemHandle(IDC_LOG), true);
    EnableWindow(GetItemHandle(ID_OK), true);
  end;
end;

procedure TCvtDlg.StartProcess(var msg: TMessage);
var
  k   : longint;
  tc  : longint;
  pstr: array[0..100] of char;
  ret : boolean;
begin
  {- start the conversion process }

  AddNameToList('Patient Database', list);
  ret:= Cvt20_Patients(@self, @stopped, maps);
  if ret then
  begin
    AddNameToList('Specimen Database', list);
    ret:= Cvt20_Specimens(@self, @stopped, maps);
  end;

  if ret then
  begin
    AddNameToList('Isolate/Result Database', list);
    ret:= Cvt20_Isolates(@self, @stopped, maps, list);
  end;

  if not ret then
  begin
    EndDlg(IDOK);
  end;

  working:= false;
  EnableButtons;
  if not stopped then
  begin
    SetDlgItemText(hWindow, IDC_TEXT0, 'Conversion process COMPLETE.');
  end
  else
  begin
    SetDlgItemText(hWindow, IDC_TEXT0, 'Conversion process CANCELLED.');
  end;
  FocusCtl(hWindow, IDC_FINISH);
  MessageBeep(0);
end;

procedure TCvtDlg.IDStop(var msg: TMessage);
begin
  if not stopped then
  begin
    stopped:= YesNoMsg(hWindow, 'Confirm', 'Conversion interrupted by user.'#10#10+
                       'Are you sure want to cancel the conversion process?');
    if stopped then
    begin
      LogStr('* CONVERTION STOPPED BY USER *');
      LogLn;
    end;
  end;
end;

procedure TCvtDlg.Cancel(var msg: TMessage);
begin
  if not stopped then
    IDStop(msg)
  else if CanClose then
    EndDlg(IDCANCEL);
end;

function TCvtDlg.CanClose: boolean;
begin
  if not stopped and working then
    CanClose:= false
  else if stopped then
    CanClose:= true
  else if finalizing then
    CanClose:= true
  else
    CanClose:= ConfirmExit(hWindow);
end;

procedure TCvtDlg.IDLog(var msg: TMessage);
begin
  ShowLogFile(@self);
end;

procedure TCvtDlg.IDFinish(var msg: TMessage);
var
  k, tc : longint;
  pstr  : array[0..100] of char;
begin
  finalizing:= true;
  working:= true;
  EnableButtons;
  list^.ClearList;
  SetDlgItemText(hWIndow, IDC_TEXT0, 'Finalizing Conversion...');
  list^.AddString('Copying database files.');
  HandleEvents(hWindow);
  {- copy merge database to Online database }
  StrCopy(pstr, curDMSDir);
  StrCat(pstr, MergeDataDir);

  if CopyFiles(pstr, curDMSDir, '*.DB*') <> 0 then
  begin
    LogStr('FATAL ERROR OCCURED COPYING MERGE FILES TO ONLINE DATABASE.'); LogLn;
    LogStr('--> A fatal error has occured. Your online database is not valid. You must');
    LogLn;
    LogStr('--> restore a previous backup to continue using the DMS.');
    LogLn;
    ErrorMsg(hWindow, 'FATAL ERROR',
    'A fatal error has occured. You online database is not valid. '#10+
    'You must restore a previous backup to continue using the DMS.');
    EndDlg(IDOK);
    Exit;
  end
  else
    FinalizeClean;

  working:= false;
  EnableButtons;
  LogStr('Conversion process completed successfully!'); LogLn;
  SetDlgItemText(hWIndow, IDC_TEXT0, 'Finalization COMPLETE.');
  FocusCtl(hWindow, ID_OK);
  list^.ClearList;
  MessageBeep(0);
  LogEnd;
end;


procedure Cvt20ConvertData(aParent: PWindowsObject; aMaps: PMapCollection);
begin
  aMaps^.LogUnmappedFields;
  aMaps^.LogMappedFields;
  application^.ExecDialog(New(PCvtDlg, Init(aParent, aMaps)));
end;

END.

{- rcf }
