unit WAGPIB;

INTERFACE

uses
  Objects;

const
  MaxGPIBBytes  = 4160;

  {**  W/A Commands Constants **}

  ABFcmd        = $0801; {** ABORT VOLUME FORMAT **}
  AIRcmd        = $3004; {** REPORT 620 AIR READ FROM LAST CALIBRATE **}
  BOOTcmd       = $0001; {** BOOT OS **}
  BSIcmd        = $2002; {** BARCODE SCAN INQUIRY COMMAND **}
  CAFcmd        = $200B; {** CLOSE APP DATA FILES **}
  CARcmd        = $2103; {** CANCEL ACCESS REQUEST **}
  CLOSEcmd      = $0805; {** CLOSE THE CURRENTLY OPENED FILE **}
  CLTcmd        = $3001; {** CALIBRATION DATE TIME INQUIRY **}
  CREATEcmd     = $0807; {** CREATE A NEW FILE **}
  CSTcmd        = $2202; {** CANCEL STERILIZATION **}
  DACcmd        = $3002; {** WASUP DAC SETTING INQUIRY. **}
  DFMTcmd       = $0003; {** LOW-LEVEL DISK FORMAT **}
  DVIcmd        = $2005; {** DATA AVAILABLE INQUIRY **}
  ECHOcmd       = $001F; {** ECHO DATA COMMAND (COMM. TEST) **}
  EDLcmd        = $0005; {** ENTER DEFECT LIST **}
  ERAcmd        = $080D; {** ERASE A FILE **}
  EXcmd         = $0006; {** EXECUTE AT ADDRESS **}
  FASTcmd       = $0810; {** TURN W/A HARD DISK IMMEDIATE UPDATE OFF **}
  GETWATYPEcmd  = $1256; {** RETURN THE TYPE AND SIZE OF THE W/A **}
  GSIZEcmd      = $001C; {** GPIB BUFFER SIZE **}
  HOMcmd        = $2204; {** HOME ALL AXES **}
  HSIcmd        = $000C; {** HARDWARE STATUS INQUIRY **}
  HugeECHOcmd   = $001F; {** ECHO DATA COMMAND (COMM. TEST) **}
  IDIcmd        = $0007; {** IDENTIFY CODE LEVEL **}
  IDPHOTOcmd    = $3000; {** IDENTIFY ACTIVE PHOTOSYSTEM **}
  INTcmd        = $3003; {** WASUP INTEGRATE TIME INQUIRY. **}
  JRADSYScmd    = $3106; {** REPORT A/D SYSTEM CALIBRATION PARAMETERS. **}
  JRAIRcmd      = $3102; {** REPORT FLUOROMETER STATUS. **}
  JRENVSTATcmd  = $3107; {** REPORT VARIOUS ENVIRONMENTAL PARAMETERS. **}
  JRFLUORcmd    = $3105; {** REPORT FLUOROMETER STATUS. **}
  JRGAINScmd    = $3100; {** REPORT GAINS USED FOR EACH FILTER BY DIODE. **}
  KILLCmd       = $2108; {** Terminate Panel Processing **}
  LARcmd        = $2101; {** LOAD ACCESS **}
  MARcmd        = $2102; {** MAINTENANCE ACCESS **}
  MPPURGEcmd    = $210A; {** MAINTENANCE POSITION FOR PURGE **}
  MPREFcmd      = $210C; {** MAINTENANCE POSITION FOR CLEANING REFERENCE DISK **}
  MPSHIELDcmd   = $210D; {** MAINTENANCE POSITION FOR CLEANING SHIELD **}
  OPENcmd       = $0814; {** OPEN A FILE **}
  PDIcmd        = $2008; {** PROCESSED DATA INQUIRY **}
  PEIcmd        = $2009; {** VIRTUAL TESTS INQUIRY **}
  PFIcmd        = $0010; {** POWER FAIL INQUIRY **}
  PMIcmd        = $2007; {** MIC INQUIRY **}
  PRIcmd        = $2006; {** PANEL RESULTS INQUIRY **}
  PSIcmd        = $2004; {** PANEL STATUS INQUIRY **}
  PSTcmd        = $2203; {** PREP FOR STERILIZE **}
  PURcmd        = $2107; {** PURGE REAGENT **}
  QSTcmd        = $2205; {** QUICKY SELF TEST **}
  RDIcmd        = $200A; {** RAW DATA INQUIRY **}
  READcmd       = $0816; {** READ SOME BYTES FROM THE CURRENTLY OPEN FILE **}
  READREGcmd    = $001B; {** READ CMOS REGISTER **}
  RESETcmd      = $0013; {** RESET SYSTEM (REBOOT) **}
  ROIcmd        = $200F; {** GET RAW DATA OFFSET **}
  ROMCMDcmd     = $0024; {** ROMCMD CALL **}
  RSTcmd        = $000B; {** RESET SYSTEM TIME.  SETS INTERNAL  **}
  RSTPURcmd     = $2109; {** RESET THE LAST PURGE TIME TO NOW **}
  SAFEcmd       = $0811; {** TURN W/A HARD DISK IMMEDIATE UPDATE ON. **}
  SAVEcmd       = $081A; {** SAVE THE OPERATING SYSTEM ON THE DISK **}
  SCIcmd        = $2003; {** SLOT CONTENT INQUIRY **}
  SETREGcmd     = $001A; {** SET CMOS REGISTER **}
  SSCcmd        = $2104; {** SET SLOT CONTENTS **}
  SSIcmd        = $2001; {** SLOT STATUS INQUIRY COMMAND **}
  SSTcmd        = $000F; {** SET SYSTEM TIME **}
  STIcmd        = $081D; {** STATUS INQUIRY **}
  STOcmd        = $0008; {** STORE MEMORY IMAGE **}
  STRcmd        = $2201; {** STERILIZE **}
  UNMPPURGEcmd  = $210B; {** RETURN FROM MAINTENANCE PURGE POSITION **}
  USEDOFScmd    = $3103; {** REPORT OFFSETS ACTUALLY USED. **}
  USEDVOLTScmd  = $310D; {** REPORT USED DAC VOLTAGES **}
  VFMTcmd       = $081F; {** VOLUME FORMAT **}
  WATYPEcmd     = $210E; {** RETURN W/A TYPE AND HEIGHT **}
  WRITEcmd      = $0820; {** WRITE A RECORD TO DISK **}
  SetPDLCmd     = $2106; { Send PDL file to W/A }
  GPnOptCmd     = $2011; { Get panel option }

  MaxDefects    = 30;    { For EDL command }
  MaxSectors    = 32;    { For block transfer commands }

{** Message Lengths **}
  SSTLen        = 7;
  SCILen        = 1;
  PSILen        = 2;
  PDILen        = 3;
  RDILen        = 4;
  PURLen        = 1;
  LARLen        = 1;
  SSCLen        = 5;
  CDILen        = 1;
  ECHOLen       = 257;
  ROMCMDLen     = 4;
  ROILen        = 2;
  RRegLen       = 1;
  SRegLen       = 5;
  RESETLen      = 1;
  GPnOptLen     = 3;

{** Panel Status Codes **}
  SlotEmpty         = 0;
  NoDataYet         = 1;
  MICsAvailable     = 2;
  ReagentsAdded     = 3;
  ProcessComplete   = 4;
  PanelUnrecognized = 5;
  AbortedPanel      = 6;
  PanelMissing      = 7;
  DuplicatePanelId  = 8;
  LampFailure       = 9;

{** Max # of MICs + extra tests back from PMI command = maxmics + 4 **}
  MaxPmi        = 48;

{ return values for ReadyForWAMessage }
  AOK           = 0;
  TryAgain      = 1;
  QuitTrying    = 2;

{ WA TYPES }
  UnknownWA     = -1;
  ClassicWA     = 0;
  WA40          = 1;
  WA96          = 2;

  { Bit offsets for STI command }
  StiErr                  = 64;
  BarCodeScanDone         = 65;
  AccessGranted           = 66;
  AccessNixed             = 67;
  DataAvailable           = 68;
  Sterilizing             = 69;
  DiagResultAvailable     = 70;
  SystemNotReady          = 71;

  StiJam                  = 80;
  AccessTimeExpired       = 81;
  TempTooLow              = 82;
  TempTooHigh             = 83;
  DispensePressureLow     = 84;
  BarcodeScanErrors       = 85;
  DiagOvertimeError       = 86;
  ColorLampFailure        = 87;


type
  PPanelFlags = ^TPanelFlags;
  TPanelFlags = object(TObject)
    flagVal        : integer;
    PowerFailure   : boolean;
    ISFGrowth      : boolean;
    BadSterileWell : boolean;
    SkippedWells   : boolean;
    BadDCB         : boolean;
    BadOFB         : boolean;
    VerifyPanelId  : boolean;
    PanelInWA      : boolean;
    PossibleMRSASE : boolean;
    SlowGrower     : boolean;
    virtualTests   : boolean;
    DelayedRead    : boolean;
    InvSterileWell : boolean;
    IDAvailOnRapid : boolean;
    NoDispense     : boolean;
    BadNPC         : boolean;
    BadBNAC        : boolean;
    {}
    constructor Init(flags: integer);
  end;

  TimeDate      = record
    sec   : byte;
    min   : byte;
    hour  : byte;
    day   : byte;
    month : byte;
    year  : integer;
  end;

  PfiRec        = record
    pfiOff   : TimeDate;
    pfiOn    : TimeDate;
    pfiLen   : longint;
  end;

  HugeArray   = array[1..MaxGpibBytes] of byte;

  SendBlockType = record
    Class       : byte;    { 0 = data, 1 = start addr }
    LoadOffset  : integer;
    LoadSegment : integer;
    SendData    : HugeArray;
  end;

  Sector = array [0..127] of byte;

  XferBlockType = record
    Offset : longint;
    Size   : integer;
    Data   : array [1..MaxSectors] of Sector;
  end;

  WaDefectType  = record
    Cylinder : integer;
    Track    : byte;
  end;
  PDefectType   = ^WaDefectType;

  StiArray      = array[1..12] of byte;
  BsiArray      = array[1..96] of byte;
  PriArray      = array[1..12] of byte;
  PmiArray      = array[1..MaxPmi] of byte;
  PdiArray      = array[1..12] of longint;
  CdiArray      = array[1..12] of word;
  IntArray      = array[1..8] of word;
  DacArray      = array[1..16] of byte;
  RdiArray      = array[1..12] of integer;
  EchoArray     = array[1..255] of byte;
  PDefectArray  = ^DefectArray;
  DefectArray   = array[1..MaxDefects] of WaDefectType;

  JrGnArray     = array[1..72] of byte;          { gain values for WA-40 color optics }
  JrAirArray    = array[1..72] of word;          { Air reference reading by diode/filter combination }
  UsdOArray     = array[1..72] of word;          { actual offset values used, by diode/filter combinations }
  JrDACArray    = array[1..72] of byte;          { actual values used in reading... }
  JrMscArray    = array[1..5] of integer;        { used to hold calibration values }

  PSendMsgRec   = ^TSendMsgRec;
  TSendMsgRec   = record
    cmd    : integer;
    msgLen : integer;
    case integer of
      1: (sstTime   : TimeDate);              {** SST **}
      2: (sciSlot   : byte);                  {** SCI **}
      3: (psiPanId  : integer);               {** PSI,PRI,PMI,PEI **}
      4: (pdiPanId  : integer;                {** PDI **}
          pdiRow    : byte);
      5: (rdiPanId  : integer;                {** RDI **}
          rdiRow    : byte;
          rdiFiltr  : byte);
      6: (purReag   : word);                  {** PUR **}
      7: (arMins    : byte);                  {** LAR,MAR **}
      8: (sscSlot   : byte;                   {** SSC **}
          sscType   : byte;
          sscClass  : byte;
          sscPanId  : integer);
      9: (cdiRow    : byte);                  {** CDI **}
     10: (rmCmdNum  : integer;                {** ROM **}
          rmCmdAX   : integer);
     11: (echoMsg   : EchoArray;              {** ECHO,IDI **}
          crcBytes  : integer);
     12: (RoiPanId  : integer);
     13: (RegNum    : byte;                   {** ReadReg,SetReg **}
          RegValue  : longint);
     14: (ResetType : byte);                  { ** RESET ** }
     15: (DefectList: DefectArray);
     16: (Passes    : integer);               { ** VFMT ** }
     17: (FileName  : string);                { ** CREATE, OPEN ** }
     18: (GPnOptPId : Integer;                { ** GPnOpt ** }
          GPnOptNum : Byte);
  end;

  PRecvMsgRec   = ^TRecvMsgRec;
  TRecvMsgRec   = record
    cmd : integer;
    msgLen : integer;
    status : integer;
    case integer of
      1: (stiBits  : StiArray);              {** STI,ERI **}
      2: (pfiData  : PfiRec);                {** PFI **}
      3: (bsiInfo  : BsiArray);              {** SSI,BSI **}
      4: (psiPid   : integer;                {** SCI,PSI,DVI **}
          psiSlot  : byte;
          psiStat  : byte;
          psiFlags : integer;
          psiPtyp  : byte;
          psiLastRd: TimeDate);
      5: (priPid   : integer;                {** PRI,PEI **}
          priSlot  : byte;
          priClass : byte;
          priBits  : PriArray);
      6: (pmiPid   : integer;                {** PMI **}
          pmiSlot  : byte;
          pmiCount : byte;
          pmiMICs  : PmiArray);
      7: (pdiPid   : integer;                {** PDI **}
          pdiSlot  : byte;
          pdiRow   : byte;
          pdiData  : PdiArray);
      8: (cdiInfo  : CdiArray);              {** CDI **}
      9: (rdiPid   : integer;                {** RDI **}
          rdiSlot  : byte;
          rdiFiltr : byte;
          rdiRow   : byte;
          rdiData  : RdiArray);
     10: (arDelay  : byte;                   {** LAR,MAR **}
          arDuratn : byte);
     11: (qstStat  : integer);               {** QST **}
     12: (calDate  : TimeDate);
     13: (intInfo  : IntArray);              {** INT **}
     14: (dacInfo  : DacArray);              {** DAC **}
     15: (errPid   : integer;                {** ERROR **}
          MWErrNum : word);
     16: (jrGains  : JrGnArray);             {** JRGAINS **}
     17: (jrOffsts : UsdOArray);             {** USED OFFSETS **}
     18: (jrAirs   : JrAirArray);            {** JRAIRS **}
     19: (jrMisc   : JrMscArray);            {** JRFLUOR etc **}
     20: (jrDACs   : JrDACArray);
     21: (echoMsg  : EchoArray;              {** ECHO,IDI **}
          crcBytes : integer);
     22: (RoiPanID : integer;
          RoiSlot  : byte;
          RoiOffset: RdiArray);
     23: (RegNum   : byte;                   {** ReadReg **}
          RegValue : longint);
     24: (GPnOptPId: Integer;                { ** GPnOpt ** }
          GPnOptNum: Byte;
          GPnOptVal: LongInt);
  end;

  PHugeSendMsgRec = ^THugeSendMsgRec;
  THugeSendMsgRec = record
    cmd    : integer;
    msgLen : integer;
    case integer of
      1:  (Data      : HugeArray);
      2:  (SendBlock : SendBlockType);
      3:  (XferBlock : XferBlockType);
  end;

  PHugeRecvMsgRec = ^THugeRecvMsgRec;
  THugeRecvMsgRec = record
    cmd    : integer;
    msgLen : integer;
    status : integer;
    case integer of
      1:  (Data: HugeArray);
      2:  (XferBlock: XferBlockType);
  end;

  PBaseWACommand  = ^TBaseWACommand;
  TBaseWACommand  = object(TObject)
    waID         : integer;
    WAStatNum    : integer;
    WAErrNum     : integer;
    WAGpibErrNum : integer;
    ErrorMode    : integer;
    {}
    constructor Init(id: integer);
    function  WAStatus(statNum: integer): boolean; virtual;
    function  WAError(errNum: integer): boolean; virtual;
    function  GPIBError(errNum: integer): boolean; virtual;
    procedure SetErrorMode(NewErrorMode: integer); virtual; { 0 = Call ShowError on errors }
                                                            { 1 = Don't call ShowError on GPIB errors }
  end;

  PWACommand    = ^TWACommand;
  TWACommand    = object(TBaseWACommand)
    sendMsg : PSendMsgRec;
    recvMsg : PRecvMsgRec;
    {}
    constructor Init(id: integer);
    destructor Done; virtual;
    function SendWAMessage: boolean; virtual;
    {}
    private
    procedure MakeSendMessage; virtual;
    function WAReadyForMessage: integer; virtual;
  end;

  PHugeWACommand = ^TWACommand;
  THugeWACommand = object(TBaseWACommand)
    sendMsg : PHugeSendMsgRec;
    recvMsg : PHugeRecvMsgRec;
    constructor Init(id: integer);
    destructor Done; virtual;
    function SendWAMessage: boolean; virtual;
    {}
    private
    procedure MakeSendMessage; virtual;
  end;

  PWAABFcmd = ^TWAABFCmd;
  TWAABFCmd = object(TWACommand)
    procedure MakeSendMessage; virtual;
  end;

  PWAAIRcmd = ^TWAAIRCmd;
  TWAAIRCmd = object(TWACommand)
    procedure MakeSendMessage; virtual;
    procedure SetParams(row: byte); virtual;
  end;

  PWABOOTcmd = ^TWABOOTCmd;
  TWABOOTCmd = object(TWACommand)
    procedure MakeSendMessage; virtual;
  end;

  PWABSIcmd = ^TWABSICmd;
  TWABSICmd = object(TWACommand)
    procedure MakeSendMessage; virtual;
    procedure GetParams; virtual;
  end;

  PWACAFcmd = ^TWACAFCmd;
  TWACAFCmd = object(TWACommand)
    procedure MakeSendMessage; virtual;
  end;

  PWACARCmd = ^TWACARCmd;
  TWACARCmd = object(TWACommand)
    procedure MakeSendMessage; virtual;
  end;

  PWACLOSEcmd = ^TWACLOSECmd;
  TWACLOSECmd = object(TWACommand)
    procedure MakeSendMessage; virtual;
  end;

  PWACLTcmd = ^TWACLTCmd;
  TWACLTCmd = object(TWACommand)
    procedure MakeSendMessage; virtual;
  end;

  PWACREATEcmd = ^TWACREATECmd;
  TWACREATECmd = object(TWACommand)
    procedure MakeSendMessage; virtual;
    procedure SetParams(Name: string); virtual;
  end;

  PWACSTcmd = ^TWACSTCmd;
  TWACSTCmd = object(TWACommand)
    procedure MakeSendMessage; virtual;
  end;

  PWADACcmd = ^TWADACCmd;
  TWADACCmd = object(TWACommand)
    procedure MakeSendMessage; virtual;
    procedure GetParams; virtual;
  end;

  PWADFMTcmd = ^TWADFMTCmd;
  TWADFMTCmd = object(TWACommand)
    procedure MakeSendMessage; virtual;
  end;

  PWADVIcmd = ^TWADVICmd;
  TWADVICmd = object(TWACommand)
    procedure MakeSendMessage; virtual;
    procedure GetParams(var panelId: integer;
                        var slot, stat, panelType: byte;
                        var flags: integer); virtual;
  end;

  PWAECHOcmd = ^TWAECHOCmd;
  TWAECHOCmd = object(TWACommand)
    procedure MakeSendMessage; virtual;
    procedure GetParams; virtual;
    procedure SetParams; virtual;
  end;

  PWAEDLcmd = ^TWAEDLCmd;
  TWAEDLCmd = object(TWACommand)
    procedure MakeSendMessage; virtual;
    procedure SetParams(Entries: integer; DArray: PDefectArray); virtual;
  end;

  PWAERAcmd = ^TWAERACmd;
  TWAERACmd = object(TWACommand)
    procedure MakeSendMessage; virtual;
    procedure SetParams(FileName: string); virtual;
  end;

  PWAEXcmd = ^TWAEXCmd;
  TWAEXCmd = object(TWACommand)
    procedure MakeSendMessage; virtual;
  end;

  PWAFASTcmd = ^TWAFASTCmd;
  TWAFASTCmd = object(TWACommand)
    procedure MakeSendMessage; virtual;
  end;

  PWAGETWATYPEcmd = ^TWAGETWATYPECmd;
  TWAGETWATYPECmd = object(TWACommand)
    procedure MakeSendMessage; virtual;
    procedure GetParams(var WaType: integer); virtual;
  end;

  PWAGSIZEcmd = ^TWAGSIZECmd;
  TWAGSIZECmd = object(TWACommand)
    procedure MakeSendMessage; virtual;
    procedure GetParams(var rxSize, txSize: integer);  virtual;{ Sizes of WA send and receive buffers }
  end;

  PWAHOMcmd = ^TWAHOMCmd;
  TWAHOMCmd = object(TWACommand)
    procedure MakeSendMessage; virtual;
  end;

  PWAHSIcmd = ^TWAHSICmd;
  TWAHSICmd = object(TWACommand)
    procedure MakeSendMessage; virtual;
  end;

  PWAIDICmd = ^TWAIDICmd;
  TWAIDICmd = object(TWACommand)
    procedure MakeSendMessage; virtual;
    procedure GetParams(var Len: integer; var IdiData: EchoArray); virtual;
    procedure SetParams;
  end;

  PWAIDPHOTOcmd = ^TWAIDPHOTOCmd;
  TWAIDPHOTOCmd = object(TWACommand)
    procedure MakeSendMessage; virtual;
  end;

  PWAINTcmd = ^TWAINTCmd;
  TWAINTCmd = object(TWACommand)
    procedure MakeSendMessage; virtual;
    procedure GetParams; virtual;
  end;

  PWAJRADSYScmd = ^TWAJRADSYSCmd;
  TWAJRADSYSCmd = object(TWACommand)
    procedure MakeSendMessage; virtual;
  end;

  PWAJRAIRcmd = ^TWAJRAIRCmd;
  TWAJRAIRCmd = object(TWACommand)
    procedure MakeSendMessage; virtual;
    procedure GetParams; virtual;
  end;

  PWAJRENVSTATcmd = ^TWAJRENVSTATCmd;
  TWAJRENVSTATCmd = object(TWACommand)
    procedure MakeSendMessage; virtual;
  end;

  PWAJRFLUORcmd = ^TWAJRFLUORCmd;
  TWAJRFLUORCmd = object(TWACommand)
    procedure MakeSendMessage; virtual;
    procedure GetParams; virtual;
  end;

  PWAJRGAINScmd = ^TWAJRGAINSCmd;
  TWAJRGAINSCmd = object(TWACommand)
    procedure MakeSendMessage; virtual;
    procedure GetParams; virtual;
  end;

  PWAKILLcmd = ^TWAKILLCmd;
  TWAKILLCmd = object(TWACommand)
    procedure MakeSendMessage; virtual;
    procedure SetParams(pID: integer);
  end;

  PWALARCmd = ^TWALARCmd;
  TWALARCmd = object(TWACommand)
    procedure MakeSendMessage; virtual;
    procedure GetParams; virtual;
    procedure SetParams(mins: byte); virtual;
  end;

  PWAMARcmd = ^TWAMARCmd;
  TWAMARCmd = object(TWACommand)
    procedure MakeSendMessage; virtual;
    procedure GetParams; virtual;
    procedure SetParams(mins: byte); virtual;
  end;

  PWAMPPURGEcmd = ^TWAMPPURGECmd;
  TWAMPPURGECmd = object(TWACommand)
    procedure MakeSendMessage; virtual;
  end;

  PWAMPREFcmd = ^TWAMPREFCmd;
  TWAMPREFCmd = object(TWACommand)
    procedure MakeSendMessage; virtual;
  end;

  PWAMPSHIELDcmd = ^TWAMPSHIELDCmd;
  TWAMPSHIELDCmd = object(TWACommand)
    procedure MakeSendMessage; virtual;
  end;

  PWAOPENcmd = ^TWAOPENCmd;
  TWAOPENCmd = object(TWACommand)
    procedure MakeSendMessage; virtual;
    procedure SetParams(FileName: string); virtual;
  end;

  PWAPDIcmd = ^TWAPDICmd;
  TWAPDICmd = object(TWACommand)
    procedure MakeSendMessage; virtual;
    procedure GetParams; virtual;
    procedure SetParams(panelID: integer; row: byte); virtual;
  end;

  PWAPEIcmd = ^TWAPEICmd;
  TWAPEICmd = object(TWACommand)
    procedure MakeSendMessage; virtual;
    procedure GetParams(var panelId: integer;
                        var slot, class: byte;
                        var bits: PriArray); virtual;
    procedure SetParams(pId: integer); virtual;
  end;

  PWAPFIcmd = ^TWAPFICmd;
  TWAPFICmd = object(TWACommand)
    procedure MakeSendMessage; virtual;
    procedure GetParams; virtual;
  end;

  PWAPMIcmd = ^TWAPMICmd;
  TWAPMICmd = object(TWACommand)
    procedure MakeSendMessage; virtual;
    procedure GetParams(var panelId: integer;
                        var slot, count: byte;
                        var MICs: PMIArray); virtual;
    procedure SetParams(pId: integer); virtual;
  end;

  PWAPRIcmd = ^TWAPRICmd;
  TWAPRICmd = object(TWACommand)
    procedure MakeSendMessage; virtual;
    procedure GetParams(var panelId: integer;
                        var slot, class: byte;
                        var bits: PriArray); virtual;
    procedure SetParams(pId: integer); virtual;
  end;

  PWAPSIcmd = ^TWAPSICmd;
  TWAPSICmd = object(TWACommand)
    procedure MakeSendMessage; virtual;
    procedure GetParams(var panelId: integer;
                        var slot, stat, panelType: byte;
                        var flags: integer;
                        var lastRead: TimeDate); virtual;
    procedure SetParams(pId: integer); virtual;
  end;

  PWAPSTcmd = ^TWAPSTCmd;
  TWAPSTCmd = object(TWACommand)
    procedure MakeSendMessage; virtual;
  end;

  PWAPURcmd = ^TWAPURCmd;
  TWAPURCmd = object(TWACommand)
    procedure MakeSendMessage; virtual;
    procedure SetParams(reagent: word); virtual;
  end;

  PWAQSTcmd = ^TWAQSTCmd;
  TWAQSTCmd = object(TWACommand)
    procedure MakeSendMessage; virtual;
    procedure GetParams; virtual;
  end;

  PWARDIcmd = ^TWARDICmd;
  TWARDICmd = object(TWACommand)
    procedure MakeSendMessage; virtual;
    procedure GetParams; virtual;
    procedure SetParams(panId: integer; row, filter: byte); virtual;
  end;

  PWAREADREGcmd = ^TWAREADREGCmd;
  TWAREADREGCmd = object(TWACommand)
    procedure MakeSendMessage; virtual;
    procedure GetParams; virtual;
    procedure SetParams; virtual;
  end;

  PWARESETcmd = ^TWARESETCmd;
  TWARESETCmd = object(TWACommand)
    procedure MakeSendMessage; virtual;
    procedure SetParams(RType: byte); virtual;
  end;

  PWAROIcmd = ^TWAROICmd;
  TWAROICmd = object(TWACommand)
    procedure MakeSendMessage; virtual;
    procedure SetParams(pId: integer); virtual;
  end;

  PWAROMCMDcmd = ^TWAROMCMDCmd;
  TWAROMCMDCmd = object(TWACommand)
    procedure MakeSendMessage; virtual;
    procedure SetParams(CmdNum, AXValue: integer); virtual;
  end;

  PWARSTcmd = ^TWARSTCmd;
  TWARSTCmd = object(TWACommand)
    procedure MakeSendMessage; virtual;
  end;

  PWARSTPURcmd = ^TWARSTPURCmd;
  TWARSTPURCmd = object(TWACommand)
    procedure MakeSendMessage; virtual;
  end;

  PWASAFEcmd = ^TWASAFECmd;
  TWASAFECmd = object(TWACommand)
    procedure MakeSendMessage; virtual;
  end;

  PWASAVEcmd = ^TWASAVECmd;
  TWASAVECmd = object(TWACommand)
    procedure MakeSendMessage; virtual;
  end;

  PWASCIcmd = ^TWASCICmd;
  TWASCICmd = object(TWACommand)
    procedure MakeSendMessage; virtual;
    procedure GetParams(var panelId: integer;
                        var slot, stat, paneltype: byte;
                        var flags: integer;
                        var NextRead: TimeDate); virtual;
    procedure SetParams(slot: byte); virtual;
  end;

  PWASETREGcmd = ^TWASETREGCmd;
  TWASETREGCmd = object(TWACommand)
    procedure MakeSendMessage; virtual;
    procedure SetParams(regNum: byte; regVal: longint); virtual;
  end;

  PWASSCcmd = ^TWASSCCmd;
  TWASSCCmd = object(TWACommand)
    procedure MakeSendMessage; virtual;
    procedure SetParams(slot, panelType, family: byte; panelID: integer); virtual;
  end;

  PWASSIcmd = ^TWASSICmd;
  TWASSICmd = object(TWACommand)
    procedure MakeSendMessage; virtual;
    procedure GetParams; virtual;
  end;

  PWASSTcmd = ^TWASSTCmd;
  TWASSTCmd = object(TWACommand)
    procedure MakeSendMessage; virtual;
    procedure SetParams(WATime: TimeDate); virtual;
  end;

  PWASTIcmd = ^TWASTICmd;
  TWASTICmd = object(TWACommand)
    procedure MakeSendMessage; virtual;
    procedure GetParams(var stiStat: STIArray); virtual;
  end;

  PWASTRcmd = ^TWASTRCmd;
  TWASTRCmd = object(TWACommand)
    procedure MakeSendMessage; virtual;
  end;

  PWAUNMPPURGEcmd = ^TWAUNMPPURGECmd;
  TWAUNMPPURGECmd = object(TWACommand)
    procedure MakeSendMessage; virtual;
  end;

  PWAUSEDOFScmd = ^TWAUSEDOFSCmd;
  TWAUSEDOFSCmd = object(TWACommand)
    procedure MakeSendMessage; virtual;
    procedure GetParams; virtual;
  end;

  PWAUSEDVOLTScmd = ^TWAUSEDVOLTSCmd;
  TWAUSEDVOLTSCmd = object(TWACommand)
    procedure MakeSendMessage; virtual;
  end;

  PWAVFMTcmd = ^TWAVFMTCmd;
  TWAVFMTCmd = object(TWACommand)
    procedure MakeSendMessage; virtual;
    procedure SetParams(NumPasses: integer); virtual;
  end;

  PWAWATYPEcmd = ^TWAWATYPECmd;
  TWAWATYPECmd = object(TWACommand)
    procedure MakeSendMessage; virtual;
  end;

  { Descendants of THugeWACommand }

  PWAREADcmd = ^TWAREADCmd;
  TWAREADCmd = object(THugeWACommand)
    procedure MakeSendMessage; virtual;
    procedure SetParams(Offset: longint; Size: word); virtual;
  end;

  PWASTOcmd = ^TWASTOCmd;
  TWASTOCmd = object(THugeWACommand)
    procedure MakeSendMessage; virtual;
    procedure SetParams(Class: byte; LoadOffset, LoadSegment, Len: word;
                        DataPtr: pointer); virtual;
  end;

  PWAWRITEcmd = ^TWAWRITECmd;
  TWAWRITECmd = object(THugeWACommand)
    procedure MakeSendMessage; virtual;
    procedure SetParams(Offset: longint; Size: word; DataPtr: pointer); virtual;
  end;

  PWASetPDLcmd = ^TWASetPDLCmd;
  TWASetPDLCmd = object(TWACommand)
    procedure MakeSendMessage; virtual;
    procedure SetParams(pdlFileName: PChar); virtual;
  end;

  PWAHugeEchoCmd = ^TWAHugeEchoCmd;
  TWAHugeEchoCmd = object(THugeWaCommand)
    procedure MakeSendMessage; virtual;
    procedure SetParams(Size: word; DataPtr: pointer); virtual;
    procedure GetParams(Size: word; DataPtr: pointer); virtual;
  end;

  PWAGPnOptcmd = ^TWAGPnOptCmd;
  TWAGPnOptCmd = OBJECT(TWACommand)
    PROCEDURE MakeSendMessage; VIRTUAL;
    PROCEDURE GetParams(VAR PnlId     : INTEGER;
                        VAR PnlOptNum : BYTE;
                        VAR PnlOptVal : LONGINT); VIRTUAL;
    PROCEDURE SetParams(PnlId     : INTEGER;
                        PnlOptNum : BYTE); VIRTUAL;
  END;

  WaInfoRec = record
    waType         : integer;
    slotsPerTower  : integer;
    slotTotal      : integer;
  end;


function InitWA(WaNum: integer): integer;
function WAComm(waID: integer;
                transmitMsg: PSendMsgRec;
                receiveMsg: PRecvMsgRec ) : integer;

var
  WA    : array [1..1] of WaInfoRec;

IMPLEMENTATION

uses
  WinProcs,
  MScan,
  APITools,
  WaitLib,
  Strings,
  JamCheck,
  DMSErr,
  DMSDebug;

{$I WAMAIN.INC}

var
  wagpibErr : array [0..150] of char;

function InitWA;        EXTERNAL 'WAGPIB' name 'INITWA';
function WAComm;        EXTERNAL 'WAGPIB' name 'WACOMM';


{ Methods for TPanelFlags }

constructor TPanelFlags.Init(flags: integer);
begin
  inherited Init;
  flagVal := flags;
  PowerFailure   := ((flags and $0001) <> 0);
  ISFGrowth      := ((flags and $0002) <> 0);
  BadSterileWell := ((flags and $0004) <> 0);
  SkippedWells   := ((flags and $0008) <> 0);
  BadDCB         := ((flags and $0010) <> 0);
  BadOFB         := ((flags and $0020) <> 0);
  VerifyPanelId  := ((flags and $0040) <> 0);
  PanelInWA      := ((flags and $0080) <> 0);
  PossibleMRSASE := ((flags and $0100) <> 0);
  SlowGrower     := ((flags and $0200) <> 0);
  virtualTests   := ((flags and $0400) <> 0);
  DelayedRead    := ((flags and $0800) <> 0);
  InvSterileWell := ((flags and $1000) <> 0);
  IDAvailOnRapid := ((flags and $2000) <> 0);
  NoDispense     := ((flags and $8000) <> 0);
  BadNPC := FALSE;
  BadBNAC := FALSE;
end;

{ Methods for TBaseWACommand }

constructor TBaseWACommand.Init(id: integer);
begin
  inherited Init;
  waID := id;
  ErrorMode := 0;
end;

function TBaseWACommand.WAError(errNum: integer): boolean;
begin
  WAError := FALSE;
  if (errNum = 0) then
    Exit;
  if (errNum = $468) or (errNum = $469) or (errNum = $46A) then
    Exit;
  WAError := TRUE;
  WAErrNum := longint(MOD_WA) + errNum;
end;

function TBaseWACommand.WAStatus(statNum: integer): boolean;
begin
  WAStatus := FALSE;
  if statNum = 0 then
    Exit;
  if (statNum = $468) or (statNum = $469) or (statNum = $46A) then
  begin
    WAStatus := TRUE;
    WAStatNum := statNum;
  end;
end;

function TBaseWACommand.GPIBError(errNum: integer): boolean;
begin
  GPIBError := FALSE;
  WAGpibErrNum := errNum;
  if errNum = 0 then
    Exit;
  GPIBError := TRUE;
  if ErrorMode = 0 then
  begin
    SR(IDS_GPIB_ERR, wagpibErr, 64);
    ShowErrorStr(GetActiveWindow, wagpibErr, MOD_GPIB + errNum, MOD_WA, 0, 150);
  end;
end;

procedure TBaseWACommand.SetErrorMode(NewErrorMode: integer);
begin
  ErrorMode := NewErrorMode;
end;

{ Methods for TWACommand }

constructor TWACommand.Init(id: integer);
begin
  inherited Init(id);
  GetMem(sendMsg, SizeOf(TSendMsgRec));
  GetMem(recvMsg, SizeOf(TRecvMsgRec));
  MakeSendMessage;
end;

destructor TWACommand.Done;
begin
  MSFreeMem(sendMsg, SizeOf(TSendMsgRec));
  MSFreeMem(recvMsg, SizeOf(TREcvMsgRec));
  inherited Done;
end;

function TWACommand.WAReadyForMessage: integer;
begin
  WAReadyForMessage := AOK;
  if SendMsg^.Cmd = STICmd then
    Exit;
  if SendMsg^.Cmd = GetWATypeCmd then
    Exit;

  WAReadyForMessage := JamCheck.CheckComm(waID);
end;

function TWACommand.SendWAMessage: boolean;
var
  ret : integer;
begin
  SendWAMessage := FALSE;
  WAStatNum := 0;
  WAErrNum := 0;
  repeat
    ret := WAReadyForMessage;
    MSWait(50);
  until (ret = AOK) or (ret = QuitTrying);
  if ret = QuitTrying then
    Exit;
  if GPIBError(WAGPIB.WaComm(waID, sendMsg, recvMsg)) then
    Exit;
  if integer(sendMsg^.cmd or $8000) <> recvMsg^.cmd then
  begin
    if errorMode = 0 then                (* out of sync error *)
                                         (* temp fix till Christie gets WA code fixed.*)
    begin
      SR(IDS_WA_ERR, wagpibErr, 64);
      ShowErrorStr(GetActiveWindow, wagpibErr, MOD_WA + recvMsg^.cmd, MOD_WA, 99, 150);
    end;
    Exit;
  end;
  if WAError(recvMsg^.status) then
  begin
    if errorMode = 0 then
    begin
      SR(IDS_WA_ERR, wagpibErr, 64);
      ShowErrorStr(GetActiveWindow, wagpibErr, WAErrNum, MOD_WA, 0, 150);
    end;
    Exit;
  end;
  WAStatus(recvMsg^.status);
  SendWAMessage := TRUE;
end;

procedure TWACommand.MakeSendMessage;
begin
  FillChar(sendMsg^, SizeOf(TSendMsgRec), Chr(0));
end;

{ Methods for THugeWACommand }

constructor THugeWACommand.Init(id: integer);
begin
  TBaseWACommand.Init(id);
  GetMem(sendMsg, SizeOf(THugeSendMsgRec));
  GetMem(recvMsg, SizeOf(THugeRecvMsgRec));
  MakeSendMessage;
end;

destructor THugeWACommand.Done;
begin
  MSFreeMem(sendMsg, SizeOf(THugeSendMsgRec));
  MSFreeMem(recvMsg, SizeOf(THugeRecvMsgRec));
  inherited Done;
end;

function THugeWACommand.SendWAMessage: boolean;
begin
  SendWAMessage := FALSE;
  WAStatNum := 0;
  WAErrNum := 0;
  if GPIBError(WAGPIB.WaComm(waID, PSendMsgRec(sendMsg),
                             PRecvMsgRec(recvMsg))) then
    Exit;
  if WAError(recvMsg^.status) then
  begin
    SR(IDS_WA_ERR, wagpibErr, 64);
    ShowErrorStr(GetActiveWindow, wagpibErr, WAErrNum, MOD_WA, 2, 150);
    Exit;
  end;
  WAStatus(recvMsg^.status);
  SendWAMessage := TRUE;
end;

procedure THugeWACommand.MakeSendMessage;
begin
  FillChar(sendMsg^, SizeOf(THugeSendMsgRec), Chr(0));
end;


{**********************************************************************}
{**            Methods for the descendents of TWACommand             **}
{**********************************************************************}

procedure TWAABFCmd.MakeSendMessage;
begin
  inherited MakeSendMessage;
  sendMsg^.cmd    := ABFCmd;
end;

procedure TWAAIRCmd.MakeSendMessage;
begin
  inherited MakeSendMessage;
  sendMsg^.cmd    := AIRCmd;
  sendMsg^.msgLen := CDILen;
end;

procedure TWAAIRCmd.SetParams(row: byte);
begin
  sendMsg^.CDIRow := row;
end;

procedure TWABOOTCmd.MakeSendMessage;
begin
  inherited MakeSendMessage;
  sendMsg^.cmd    := BOOTCmd;
end;

procedure TWABSICmd.MakeSendMessage;
begin
  inherited MakeSendMessage;
  sendMsg^.cmd    := BSICmd;
end;

procedure TWABSICmd.GetParams;
begin
end;

procedure TWACAFCmd.MakeSendMessage;
begin
  inherited MakeSendMessage;
  sendMsg^.cmd    := CAFCmd;
end;

procedure TWACARCmd.MakeSendMessage;
begin
  inherited MakeSendMessage;
  sendMsg^.cmd    := CARCmd;
end;

procedure TWACLOSECmd.MakeSendMessage;
begin
  inherited MakeSendMessage;
  sendMsg^.cmd    := CLOSECmd;
end;

procedure TWACLTCmd.MakeSendMessage;
begin
  inherited MakeSendMessage;
  sendMsg^.cmd    := CLTCmd;
end;

procedure TWACREATECmd.MakeSendMessage;
begin
  inherited MakeSendMessage;
  sendMsg^.cmd    := CREATECmd;
end;

procedure TWACREATECmd.SetParams(Name: string);
begin
  SendMsg^.MsgLen := Length(Name);
  Move(Name[1], SendMsg^.FileName, Length(Name));
end;

procedure TWACSTCmd.MakeSendMessage;
begin
  inherited MakeSendMessage;
  sendMsg^.cmd    := CSTCmd;
end;

procedure TWADACCmd.MakeSendMessage;
begin
  inherited MakeSendMessage;
  sendMsg^.cmd    := DACCmd;
end;

procedure TWADACCmd.GetParams;
begin
end;

procedure TWADFMTCmd.MakeSendMessage;
begin
  inherited MakeSendMessage;
  sendMsg^.cmd    := DFMTCmd;
end;

procedure TWADVICmd.MakeSendMessage;
begin
  inherited MakeSendMessage;
  sendMsg^.cmd    := DVICmd;
  FillChar(recvMsg^, SizeOf(TRecvMsgRec), Chr(0));
end;

procedure TWADVICmd.GetParams(var panelId: integer;
                              var slot, stat, paneltype: byte;
                              var flags: integer);
begin
  panelId := recvMsg^.psiPId;
  slot := recvMsg^.psiSlot;
  stat := recvMsg^.psiStat;
  panelType := recvMsg^.psiPTyp;
  flags := recvMsg^.psiFlags;
end;

procedure TWAECHOCmd.MakeSendMessage;
var
  i : integer;
begin
  inherited MakeSendMessage;
  sendMsg^.cmd    := ECHOCmd;
  sendMsg^.msgLen := ECHOLen-2; { according to 18.20 WASUP code }
  for i := 1 to ECHOLen-2 do
    sendMsg^.echoMsg[i] := i;
end;

procedure TWAECHOCmd.GetParams;
begin
end;

procedure TWAECHOCmd.SetParams;
begin
end;

procedure TWAEDLCmd.MakeSendMessage;
begin
  inherited MakeSendMessage;
  sendMsg^.cmd    := EDLCmd;
end;

procedure TWAEDLCmd.SetParams(Entries: integer; DArray: PDefectArray);
begin
  SendMsg^.MsgLen := Entries * SizeOf(WaDefectType);
  Move(DArray^, SendMsg^.DefectList, SendMsg^.MsgLen);
end;

procedure TWAERACmd.MakeSendMessage;
begin
  inherited MakeSendMessage;
  sendMsg^.cmd    := ERACmd;
end;

procedure TWAERACmd.SetParams(FileName: string);
begin
  SendMsg^.MsgLen := Length(FileName);
  Move(FileName[1], SendMsg^.FileName, Length(FileName));
end;

procedure TWAEXCmd.MakeSendMessage;
begin
  inherited MakeSendMessage;
  sendMsg^.cmd    := EXCmd;
end;

procedure TWAFASTCmd.MakeSendMessage;
begin
  inherited MakeSendMessage;
  sendMsg^.cmd    := FASTCmd;
end;

procedure TWAGETWATYPECmd.MakeSendMessage;
begin
  inherited MakeSendMessage;
  sendMsg^.cmd    := GETWATYPECmd;
end;

procedure TWAGETWATYPECmd.GetParams(var waType: integer);
begin
  waType := UnknownWA;    { Unknown }
  if RecvMsg^.Status <> 0 then
    Exit;
  if RecvMsg^.EchoMsg[3] = 0 then { Classic }
    waType := ClassicWA
  else if RecvMsg^.EchoMsg[4] = 5 then
    waType := WA40
  else
    waType := WA96;
end;

procedure TWAGSIZECmd.MakeSendMessage;
begin
  inherited MakeSendMessage;
  sendMsg^.cmd    := GSIZECmd;
end;

procedure TWAGSIZECmd.GetParams(var rxSize, txSize: integer); { Sizes of WA send and receive buffers }
begin
   Move(RecvMsg^.EchoMsg[1], rxSize, SizeOf(rxSize));
   Move(RecvMsg^.EchoMsg[3], txSize, SizeOf(txSize));
end;

procedure TWAHOMCmd.MakeSendMessage;
begin
  inherited MakeSendMessage;
  sendMsg^.cmd    := HOMCmd;
end;

procedure TWAHSICmd.MakeSendMessage;
begin
  inherited MakeSendMessage;
  sendMsg^.cmd    := HSICmd;
end;

procedure TWAIDICmd.MakeSendMessage;
begin
  inherited MakeSendMessage;
  sendMsg^.cmd    := IDICmd;
  sendMsg^.msgLen := 0;
end;

procedure TWAIDICmd.GetParams(var Len: integer; var IdiData: EchoArray);
begin
    Len := RecvMsg^.MsgLen;
    IdiData := RecvMsg^.EchoMsg;
end;

procedure TWAIDICmd.SetParams;
begin
end;

procedure TWAIDPHOTOCmd.MakeSendMessage;
begin
  inherited MakeSendMessage;
  sendMsg^.cmd    := IDPHOTOCmd;
end;

procedure TWAINTCmd.MakeSendMessage;
begin
  inherited MakeSendMessage;
  sendMsg^.cmd    := INTCmd;
end;

procedure TWAINTCmd.GetParams;
begin
end;

procedure TWAJRADSYSCmd.MakeSendMessage;
begin
  inherited MakeSendMessage;
  sendMsg^.cmd    := JRADSYSCmd;
end;

procedure TWAJRAIRCmd.MakeSendMessage;
begin
  inherited MakeSendMessage;
  sendMsg^.cmd    := JRAIRCmd;
end;

procedure TWAJRAIRCmd.GetParams;
begin
end;

procedure TWAJRENVSTATCmd.MakeSendMessage;
begin
  inherited MakeSendMessage;
  sendMsg^.cmd    := JRENVSTATCmd;
end;

procedure TWAJRFLUORCmd.MakeSendMessage;
begin
  inherited MakeSendMessage;
  sendMsg^.cmd    := JRFLUORCmd;
end;

procedure TWAJRFLUORCmd.GetParams;
begin
end;

procedure TWAJRGAINSCmd.MakeSendMessage;
begin
  inherited MakeSendMessage;
  sendMsg^.cmd    := JRGAINSCmd;
end;

procedure TWAJRGAINSCmd.GetParams;
begin
end;

procedure TWAKILLCmd.MakeSendMessage;
begin
  inherited MakeSendMessage;
  sendMsg^.cmd    := KILLCmd;
  sendMsg^.msgLen := PSILen;
end;

procedure TWAKILLCmd.SetParams(pId: integer);
begin
  sendMsg^.psiPanId := PId;
end;

procedure TWALARCmd.MakeSendMessage;
begin
  inherited MakeSendMessage;
  sendMsg^.cmd    := LARCmd;
  sendMsg^.msgLen := LARLen;
end;

procedure TWALARCmd.GetParams;
begin
end;

procedure TWALARCmd.SetParams(mins: byte);
begin
  sendMsg^.arMins := mins;
end;

procedure TWAMARCmd.MakeSendMessage;
begin
  inherited MakeSendMessage;
  sendMsg^.cmd    := MARCmd;
  sendMsg^.msgLen := LARLen;
end;

procedure TWAMARCmd.GetParams;
begin
end;

procedure TWAMARCmd.SetParams(mins: byte);
begin
  sendMsg^.arMins := mins;
end;

procedure TWAMPPURGECmd.MakeSendMessage;
begin
  inherited MakeSendMessage;
  sendMsg^.cmd    := MPPURGECmd;
end;

procedure TWAMPREFCmd.MakeSendMessage;
begin
  inherited MakeSendMessage;
  sendMsg^.cmd    := MPREFCmd;
end;

procedure TWAMPSHIELDCmd.MakeSendMessage;
begin
  inherited MakeSendMessage;
  sendMsg^.cmd    := MPSHIELDCmd;
end;

procedure TWAOPENCmd.MakeSendMessage;
begin
  inherited MakeSendMessage;
  sendMsg^.cmd    := OPENCmd;
end;

procedure TWAOPENCmd.SetParams(FileName: string);
begin
  SendMsg^.MsgLen := Length(FileName);
  Move(FileName[1], SendMsg^.FileName, Length(FileName));
end;

procedure TWAPDICmd.MakeSendMessage;
begin
  inherited MakeSendMessage;
  sendMsg^.cmd    := PDICmd;
  sendMsg^.msgLen := PDILen;
end;

procedure TWAPDICmd.GetParams;
begin
end;

procedure TWAPDICmd.SetParams(panelID: integer; row: byte);
begin
  sendMsg^.PDIPanID := panelID;
  sendMsg^.PDIRow := row;
end;

procedure TWAPEICmd.MakeSendMessage;
begin
  inherited MakeSendMessage;
  sendMsg^.cmd    := PEICmd;
  sendMsg^.msgLen := PSILen;
  FillChar(recvMsg^, SizeOf(TRecvMsgRec), Chr(0));
end;

procedure TWAPEICmd.GetParams(var panelId: integer;
                              var slot, class : byte;
                              var bits : PriArray);
begin
  panelId := recvMsg^.priPid;
  slot := recvMsg^.priSlot;
  class := recvMsg^.priClass;
  Move(recvMsg^.priBits, bits, SizeOf(PRIArray));
end;

procedure TWAPEICmd.SetParams(pId: integer);
begin
  sendMsg^.psiPanId := pId;
end;

procedure TWAPFICmd.MakeSendMessage;
begin
  inherited MakeSendMessage;
  sendMsg^.cmd    := PFICmd;
end;

procedure TWAPFICmd.GetParams;
begin
end;

procedure TWAPMICmd.MakeSendMessage;
begin
  inherited MakeSendMessage;
  sendMsg^.cmd    := PMICmd;
  sendMsg^.msgLen := PSILen;
  FillChar(recvMsg^, SizeOf(TRecvMsgRec), Chr(0));
end;

procedure TWAPMICmd.GetParams(var panelId: integer;
                              var slot, count : byte;
                              var MICs : PMIArray);
begin
  panelId := recvMsg^.pmiPId;
  slot := recvMsg^.pmiSlot;
  count := recvMsg^.pmiCount;
  Move(recvMsg^.pmiMICs, MICs, SizeOf(PMIArray));
end;

procedure TWAPMICmd.SetParams(pId: integer);
begin
  sendMsg^.psiPanId := pId;
end;

procedure TWAPRICmd.MakeSendMessage;
begin
  inherited MakeSendMessage;
  sendMsg^.cmd    := PRICmd;
  sendMsg^.msgLen := PSILen;
  FillChar(recvMsg^, SizeOf(TRecvMsgRec), Chr(0));
end;

procedure TWAPRICmd.GetParams(var panelId: integer;
                              var slot, class : byte;
                              var bits : PriArray);
begin
  panelId := recvMsg^.priPId;
  slot := recvMsg^.priSlot;
  class := recvMsg^.priClass;
  Move(recvMsg^.priBits, bits, SizeOf(PRIArray));
end;

procedure TWAPRICmd.SetParams(pId: integer);
begin
  sendMsg^.psiPanId := pId;
end;

procedure TWAPSICmd.MakeSendMessage;
begin
  inherited MakeSendMessage;
  sendMsg^.cmd    := PSICmd;
  sendMsg^.msgLen := PSILen;
  FillChar(recvMsg^, SizeOf(TRecvMsgRec), Chr(0));
end;

procedure TWAPSICmd.GetParams(var panelId: integer;
                              var slot, stat, paneltype : byte;
                              var flags : integer;
                              var lastRead : TimeDate);
begin
  panelId := recvMsg^.psiPId;
  slot := recvMsg^.psiSlot;
  stat := recvMsg^.psiStat;
  panelType := recvMsg^.psiPTyp;
  flags := recvMsg^.psiFlags;
  Move(recvMsg^.psiLastRd, lastRead, SizeOf(TimeDate));
end;

procedure TWAPSICmd.SetParams(pId: integer);
begin
  sendMsg^.psiPanId := PId;
end;

procedure TWAPSTCmd.MakeSendMessage;
begin
  inherited MakeSendMessage;
  sendMsg^.cmd    := PSTCmd;
end;

procedure TWAPURCmd.MakeSendMessage;
begin
  inherited MakeSendMessage;
  sendMsg^.cmd    := PURCmd;
  sendMsg^.msgLen := PURLen;
end;

procedure TWAPURCmd.SetParams(reagent: word);
begin
  sendMsg^.purReag := reagent;
end;

procedure TWAQSTCmd.MakeSendMessage;
begin
  inherited MakeSendMessage;
  sendMsg^.cmd    := QSTCmd;
end;

procedure TWAQSTCmd.GetParams;
begin
end;

procedure TWARDICmd.MakeSendMessage;
begin
  inherited MakeSendMessage;
  sendMsg^.cmd    := RDICmd;
  sendMsg^.msgLen := RDILen;
end;

procedure TWARDICmd.GetParams;
begin
end;

procedure TWARDICmd.SetParams(panId: integer; row, filter: byte);
begin
  sendMsg^.rdiPanId := panID;
  sendMsg^.rdiRow := row;
  sendMsg^.rdiFiltr := filter;
end;

procedure TWAREADREGCmd.MakeSendMessage;
begin
  inherited MakeSendMessage;
  sendMsg^.cmd    := READREGCmd;
  sendMsg^.msgLen := RREGLen;
end;

procedure TWAREADREGCmd.GetParams;
begin
end;

procedure TWAREADREGCmd.SetParams;
begin
end;

procedure TWARESETCmd.MakeSendMessage;
begin
  inherited MakeSendMessage;
  SendMsg^.MsgLen := RESETLen;
  sendMsg^.cmd    := RESETCmd;
end;

procedure TWARESETCmd.SetParams(RType: byte);
begin
  SendMsg^.ResetType := RType;
end;

procedure TWAROICmd.MakeSendMessage;
begin
  inherited MakeSendMessage;
  SendMsg^.MsgLen := ROILen;
  sendMsg^.cmd    := ROICmd;
end;

procedure TWAROICmd.SetParams(pId: integer);
begin
  sendMsg^.ROIPanId := PId;
end;

procedure TWAROMCMDCmd.MakeSendMessage;
begin
  inherited MakeSendMessage;
  sendMsg^.cmd    := ROMCMDCmd;
  sendMsg^.msgLen := ROMCMDLen;
end;

procedure TWARSTCmd.MakeSendMessage;
begin
  inherited MakeSendMessage;
  sendMsg^.cmd    := RSTCmd;
end;

procedure TWAROMCMDCmd.SetParams(CmdNum, AXValue: integer);
begin
  SendMsg^.RmCmdNum := CmdNum;
  SendMsg^.RmCmdAX := AXValue;
end;

procedure TWARSTPURCmd.MakeSendMessage;
begin
  inherited MakeSendMessage;
  sendMsg^.cmd    := RSTPURCmd;
end;

procedure TWASAFECmd.MakeSendMessage;
begin
  inherited MakeSendMessage;
  sendMsg^.cmd    := SAFECmd;
end;

procedure TWASAVECmd.MakeSendMessage;
begin
  inherited MakeSendMessage;
  sendMsg^.cmd    := SAVECmd;
end;

procedure TWASCICmd.MakeSendMessage;
begin
  inherited MakeSendMessage;
  sendMsg^.cmd    := SCICmd;
  sendMsg^.msgLen := SCILen;
  FillChar(recvMsg^, SizeOf(TRecvMsgRec), Chr(0));
end;

procedure TWASCICmd.GetParams(var panelId: integer;
                        var slot, stat, panelType: byte;
                        var flags : integer;
                        var nextRead : TimeDate);
begin
  panelId := recvMsg^.psiPId;
  slot := recvMsg^.psiSlot;
  stat := recvMsg^.psiStat;
  panelType := recvMsg^.psiPTyp;
  flags := recvMsg^.psiFlags;
  Move(recvMsg^.psiLastRd, nextRead, SizeOf(TimeDate));
end;

procedure TWASCICmd.SetParams(slot: byte);
begin
  sendMsg^.sciSlot := slot;
end;

procedure TWASETREGCmd.MakeSendMessage;
begin
  inherited MakeSendMessage;
  sendMsg^.cmd    := SETREGCmd;
  sendMsg^.msgLen := SREGLen;
end;

procedure TWASETREGCmd.SetParams(regNum: byte; regVal: longint);
begin
  sendMsg^.regNum := regNum;
  sendMsg^.regValue := regVal;
end;

procedure TWASSCCmd.MakeSendMessage;
begin
  inherited MakeSendMessage;
  sendMsg^.cmd    := SSCCmd;
  sendMsg^.msgLen := SSCLen;
end;

procedure TWASSCCmd.SetParams(slot, panelType, family: byte; panelID: integer);
begin
  sendMsg^.sscSlot := slot;
  sendMsg^.sscType := panelType;
  sendMsg^.sscClass := family;
  sendMsg^.sscPanID := panelID;
end;

procedure TWASSICmd.MakeSendMessage;
begin
  inherited MakeSendMessage;
  sendMsg^.cmd    := SSICmd;
end;

procedure TWASSICmd.GetParams;
begin
end;

procedure TWASSTCmd.MakeSendMessage;
begin
  inherited MakeSendMessage;
  sendMsg^.cmd    := SSTCmd;
  sendMsg^.msgLen := SSTLen;
end;

procedure TWASSTCmd.SetParams(WATime:TimeDate);
begin
  Move(WATime, sendMsg^.sstTime, SizeOf(TimeDate));
end;

procedure TWASTICmd.MakeSendMessage;
begin
  inherited MakeSendMessage;
  sendMsg^.cmd    := STICmd;
end;

procedure TWASTICmd.GetParams(var stiStat: STIArray);
begin
  Move(recvMsg^.stiBits[1], stiStat[1], SizeOf(STIArray));
end;

procedure TWASTRCmd.MakeSendMessage;
begin
  inherited MakeSendMessage;
  sendMsg^.cmd    := STRCmd;
end;

procedure TWAUNMPPURGECmd.MakeSendMessage;
begin
  inherited MakeSendMessage;
  sendMsg^.cmd    := UNMPPURGECmd;
end;

procedure TWAUSEDOFSCmd.MakeSendMessage;
begin
  inherited MakeSendMessage;
  sendMsg^.cmd    := USEDOFSCmd;
end;

procedure TWAUSEDOFSCmd.GetParams;
begin
end;

procedure TWAUSEDVOLTSCmd.MakeSendMessage;
begin
  inherited MakeSendMessage;
  sendMsg^.cmd    := USEDVOLTSCmd;
end;

procedure TWAVFMTCmd.MakeSendMessage;
begin
  inherited MakeSendMessage;
  sendMsg^.cmd    := VFMTCmd;
end;

procedure TWAVFMTCmd.SetParams(NumPasses: integer);
begin
  SendMsg^.MsgLen := 2;
  SendMsg^.Passes := NumPasses;
end;

procedure TWAWATYPECmd.MakeSendMessage;
begin
  inherited MakeSendMessage;
  sendMsg^.cmd    := WATYPECmd;
end;

{ Methods for Huge wa command objects }

procedure TWAREADCmd.MakeSendMessage;
begin
  inherited MakeSendMessage;
  sendMsg^.cmd    := READCmd;
end;

procedure TWAREADCmd.SetParams(Offset: longint; Size: word);
begin
  SendMsg^.MsgLen := 6;
  SendMsg^.XferBlock.Offset := Offset;
  SendMsg^.XferBlock.Size := Size;
end;

procedure TWASTOCmd.MakeSendMessage;
begin
  inherited MakeSendMessage;
  sendMsg^.cmd    := STOCmd;
end;

procedure TWASTOCmd.SetParams(Class: byte; LoadOffset, LoadSegment,
                              Len: word; DataPtr: pointer);
begin
  SendMsg^.MsgLen := Len + SizeOf(Class) + SizeOf(LoadOffset) + SizeOf(LoadSegment);
  SendMsg^.SendBlock.Class := Class;
  SendMsg^.SendBlock.LoadOffset := LoadOffset;
  SendMsg^.SendBlock.LoadSegment := LoadSegment;
  if DataPtr <> nil then
      Move(DataPtr^, SendMsg^.SendBlock.SendData, Len);
end;

procedure TWAHugeEchoCmd.MakeSendMessage;
begin
  inherited MakeSendMessage;
  sendMsg^.cmd := EchoCmd;
end;

procedure TWaHugeEchoCmd.SetParams(Size: word; DataPtr: pointer);
begin
  SendMsg^.MsgLen := Size;
  if DataPtr <> nil then
    Move(DataPtr^, SendMsg^.Data, Size);
end;

procedure TWaHugeEchoCmd.GetParams(Size: word; DataPtr: pointer);
begin
  if DataPtr <> nil then
    Move(RecvMsg^.Data, DataPtr^, Size);
end;

procedure TWAWRITECmd.MakeSendMessage;
begin
  inherited MakeSendMessage;
  sendMsg^.cmd    := WRITECmd;
end;

procedure TWAWRITECmd.SetParams(Offset: longint; Size: word; DataPtr: pointer);
begin
  SendMsg^.MsgLen := Size + SizeOf(Offset) + SizeOf(Size);
  SendMsg^.XferBlock.Offset := Offset;
  SendMsg^.XferBlock.Size := Size;
  if DataPtr <> nil then
    Move(DataPtr^, SendMsg^.XferBlock.Data, Size);
end;

procedure TWASetPDLCmd.MakeSendMessage;
begin
  inherited MakeSendMessage;
  sendMsg^.cmd := SetPDLCmd;
end;

procedure TWASetPDLCmd.SetParams(pdlFileName: PChar);
begin
  SendMsg^.MsgLen := StrLen(pdlFileName);
  Move(pdlFileName[0], sendMsg^.FileName[0], sendMsg^.msgLen);
end;

PROCEDURE TWAGPnOptCmd.MakeSendMessage;
BEGIN
  inherited MakeSendMessage;
  sendMsg^.cmd    := GPnOptCmd;
  sendMsg^.msgLen := GPnOptLen;
END;

PROCEDURE TWAGPnOptCmd.GetParams(VAR PnlId     : INTEGER;
                                 VAR PnlOptNum : BYTE;
                                 VAR PnlOptVal : LONGINT);
BEGIN
  PnlId     := recvMsg^.GPnOptPId;
  PnlOptNum := recvMsg^.GPnOptNum;
  PnlOptVal := recvMsg^.GPnOptVal;
END;

PROCEDURE TWAGPnOptCmd.SetParams(PnlId     : INTEGER;
                                 PnlOptNum : BYTE);
BEGIN
  sendMsg^.GPnOptPId := PnlId;
  sendMsg^.GPnOptNum := PnlOptNum;
END;

{ unit initialization }

BEGIN
  FillChar(WA, SizeOf(WA), 0);
  WA[1].waType := UnknownWA;
END.
