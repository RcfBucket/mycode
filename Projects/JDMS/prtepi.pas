unit PRTEPI;

{- EPI reports }

INTERFACE

uses
  OWindows;

procedure PrintEPIReports(aParent: PWindowsObject; useMPSS: boolean);

IMPLEMENTATION

uses
  APITools,
  WinTypes,
  GridLib,
  EpiMIC,
  EpiSus,
  EpiUser,
  WinProcs,
  DlgLib;

{$R PRTEPI.RES}
{$I PRTEPI.INC}

type
  PEpiDlg = ^TEpiDlg;
  TEpiDlg = object(TCenterDlg)
    useMPSS   : boolean;
    grid      : PGrid;
    {}
    constructor Init(aParent: PWindowsObject; doUseMPSS: boolean);
    procedure SetupWindow; virtual;
    procedure WMCharToItem(var msg: TMessage);  virtual WM_FIRST + WM_CHARTOITEM;
    procedure WMDrawItem(var msg: TMessage);    virtual WM_FIRST + WM_DRAWITEM;
    procedure WMMeasureItem(var msg: TMessage); virtual WM_FIRST + WM_MEASUREITEM;
    procedure IDPrint(var msg: TMessage);       virtual ID_FIRST + IDC_PRINT;
    procedure IDGrid(var msg: TMessage);        virtual ID_FIRST + IDC_GRID;
    procedure PrintIt;
  end;

constructor TEpiDlg.Init(aParent: PWindowsObject; doUseMPSS: boolean);
begin
  inherited Init(aParent, MakeIntResource(DLG_PRTEPI));
  useMPSS:= doUseMPSS;
  grid:= New(PGrid, InitResource(@self, IDC_GRID, 1, false));
end;

procedure TEpiDlg.SetupWindow;

  procedure AddOne(idx: integer);
  var
    pstr  : array[0..100] of char;
    row   : integer;
  begin
    row:= grid^.AddString('');
    grid^.SetItemData(row, idx);
    grid^.SetData(0, row, SR(idx, pstr, sizeof(pstr)-1));
  end;

begin
  inherited SetupWindow;

  grid^.EnableHeader(false);
  grid^.StretchColumn(0);
  AddOne(IDS_EPIMIC);
  AddOne(IDS_EPISUSC);
  AddOne(IDS_EPIITEM);
  AddOne(IDS_EPISIR);
  grid^.SetSelIndex(0);
end;

procedure TEpiDlg.WMCharToItem(var msg: TMessage);
begin
  grid^.CharToItem(msg);
end;

procedure TEpiDlg.WMDrawItem(var msg: TMessage);
begin
  grid^.DrawItem(msg);
end;

procedure TEpiDlg.WMMeasureItem(var msg: TMessage);
begin
  grid^.MeasureItem(msg);
end;

procedure TEpiDlg.IDGrid(var msg: TMessage);
begin
  DefWndProc(msg);
  if msg.lParamHi = LBN_DBLCLK then
    PrintIt;
end;

procedure TEpiDlg.IDPrint(var msg: TMessage);
begin
  PrintIt;
end;

procedure TEpiDlg.PrintIt;
var
  row : integer;
begin
  row:= grid^.GetSelIndex;
  if row >= 0 then
  begin
    case grid^.GetItemData(row) of
      IDS_EPIMIC:   EpiAntimicrobicMIC(@self, useMPSS);
      IDS_EPISUSC:  EpiAntimicrobicSus(@self, useMPSS, false);
      IDS_EPIITEM:  EpiOrganismByUserField(@self, useMPSS);
      IDS_EPISIR:   EpiAntimicrobicSus(@self, useMPSS, true);
    end;
  end;
end;

procedure PrintEPIReports(aParent: PWindowsObject; useMPSS: boolean);
var
  dlg : PEpiDlg;
begin
  dlg:= New(PEpiDlg, Init(aParent, useMPSS));
  application^.ExecDialog(dlg);
end;

END.

{- rcf }

