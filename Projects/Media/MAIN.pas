unit Main;

{-
  * Track artists needs to be looked at. I dont thing the ARTIST link is valid.

  * When entering new music, if tracks are entered, then the music record
    should be posted.
}

interface

uses
  SysUtils, WinTypes, WinProcs, Messages, Classes, Graphics, Controls,
  Forms, Dialogs, StdCtrls, Buttons, Music,
  songs, books, tableEd, options, ExtCtrls, DB, DBTables;

const
  IDD_ABOUT  = $0010;

type
  TFrmMain = class(TForm)
    Panel1: TPanel;
    BtnMusic: TBitBtn;
    BtnReports: TBitBtn;
    BtnVideo: TBitBtn;
    BtnSetup: TBitBtn;
    BtnLiterature: TBitBtn;
    Button1: TButton;
    Button2: TButton;
    BtnExit: TBitBtn;
    BtnSoftware: TBitBtn;
    dbMedia: TDatabase;
    procedure BtnExitClick(Sender: TObject);
    procedure BtnMusicClick(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure BtnReportsClick(Sender: TObject);
    procedure BtnLiteratureClick(Sender: TObject);
    procedure BtnSetupClick(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    procedure WMSysCommand(var msg: TWMSysCommand); message WM_SYSCOMMAND;
  public
end;

var
  FrmMain: TFrmMain;

implementation

uses about, identify;

{$R *.DFM}

procedure TFrmMain.BtnExitClick(Sender: TObject);
begin
  Close;
end;

procedure TFrmMain.BtnMusicClick(Sender: TObject);
var
  frm : TFrmMusic;
begin
  frm:= TFrmMusic.Create(self);
  frm.ShowModal;
  frm.Free;
end;

procedure TFrmMain.Button1Click(Sender: TObject);
var
  f : TForm;
begin
  IdentifyCDs(self);
exit;
  f:= TFrmSongs.Create(self);
  f.ShowModal;
  f.Free;
end;

procedure TFrmMain.BtnReportsClick(Sender: TObject);
begin
  MessageBeep(0);
end;

procedure TFrmMain.BtnLiteratureClick(Sender: TObject);
var
  frm : TFrmBooks;
begin
  frm:= TFrmBooks.Create(self);
  frm.ShowModal;
  frm.Free;
end;

procedure TFrmMain.BtnSetupClick(Sender: TObject);
var
  f   : TForm;
begin
  f:= TFrmOptions.Create(self);
  f.ShowModal;
  f.Free;
end;

procedure TFrmMain.Button2Click(Sender: TObject);
var
  id  : string;
begin
  id:= 'COMP';
  EditTable(self, 'CATEGORY', 'Test Category', true, ID);

end;

procedure TFrmMain.WMSysCommand(var msg: TWMSysCommand);
begin
  if ((msg.cmdType and $FFF0) = IDD_ABOUT) then
  begin
    DoAbout;
  end
  else
    inherited;
end;


procedure TFrmMain.FormCreate(Sender: TObject);
var
  hm  : HMenu;
begin
  hm:= GetSystemMenu(handle, false);
  AppendMenu(hm, MF_SEPARATOR, 0, nil);
  AppendMenu(hm, MF_STRING, IDD_ABOUT, '&About Media DB');
end;

end.