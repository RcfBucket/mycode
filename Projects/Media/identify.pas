unit identify;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, MPlayer, ComCtrls, ExtCtrls, DB, DBTables;

type
  TFrmIdentify = class(TForm)
    mp: TMediaPlayer;
    BtnClose: TBitBtn;
    sb: TStatusBar;
    Timer1: TTimer;
    BtnLink: TBitBtn;
    GroupBox1: TGroupBox;
    Label1: TLabel;
    LblTrack: TLabel;
    LblTime: TLabel;
    Label2: TLabel;
    Label4: TLabel;
    LblTotalTracks: TLabel;
    LblCDID: TLabel;
    Label6: TLabel;
    Label3: TLabel;
    lblUPC: TLabel;
    Label5: TLabel;
    Label7: TLabel;
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    Label8: TLabel;
    Label9: TLabel;
    Label10: TLabel;
    lblTotalTime: TLabel;
    Table1: TTable;
    DataSource1: TDataSource;
    procedure mpNotify(Sender: TObject);
    procedure BtnCloseClick(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure mpClick(Sender: TObject; Button: TMPBtnType;
      var DoDefault: Boolean);
    procedure mpPostClick(Sender: TObject; Button: TMPBtnType);
    procedure FormCreate(Sender: TObject);
    procedure BtnLinkClick(Sender: TObject);
  private
    lastMode  : TMPModes;
    procedure ShowMode;
    procedure CloseCDDoor;
    procedure ShowID;
    procedure DoorWasClosed;
  public
    { Public declarations }
  end;

procedure IdentifyCDs(aOwner: TComponent);

implementation

uses
  MMSystem, muslist;

{$R *.DFM}

procedure IdentifyCDs(aOwner: TComponent);
var
  f : TForm;
begin
  f:= TFrmIdentify.Create(aOwner);
  f.ShowModal;
  f.Free;
end;


procedure TFrmIdentify.mpNotify(Sender: TObject);
begin
  screen.Cursor:= crDefault;
  mp.Notify:= true;
end;

procedure TFrmIdentify.BtnCloseClick(Sender: TObject);
begin
  Close;
end;

procedure TFrmIdentify.Button1Click(Sender: TObject);
begin
  ShowMode;
end;

procedure TFrmIdentify.ShowMode;
type
  TCDTime = record
    Tracks,  // (least significant byte)
    Minutes,
    Seconds,
    Frames  : byte; // (most significant byte)
  end;
var
  cdTime  : TCDTime;
begin
  mp.TimeFormat:= tfTMSF;   // set proper time format

  lblTotalTracks.caption:= IntToStr(mp.Tracks);
  cdTime:= TCDTime(mp.Length);
  lblTotalTime.caption:= Format('%.2d:%.2d', [cdTime.minutes, cdTime.seconds]);

  if (mp.DeviceID = 0) or (mp.Mode = mpOpen) then
  begin
    if mp.Mode <> mpOpen then
      sb.panels[0].text:= 'CD not ready'
    else
      sb.panels[0].text:= 'Door is open';
    lblTrack.caption:= '---';
    lblTime.caption:= '--:--';
  end
  else
  begin
    lblTrack.caption:= IntToStr(mp.position and $F);
    cdTime:= TCDTime(mp.position);
    LblTime.caption:= Format('%.2d:%.2d',
                        [cdTime.minutes, cdTime.seconds]);
    case mp.Mode of
      mpNotReady  : sb.panels[0].text:= 'Not ready';
      mpStopped   : sb.panels[0].text:= 'Stopped';
      mpPlaying	  : sb.panels[0].text:= 'Playing';
      mpRecording	: sb.panels[0].text:= 'Recording';
      mpSeeking	  : sb.panels[0].text:= 'Seeking';
      mpPaused	  : sb.panels[0].text:= 'Paused';
      mpOpen	    : sb.panels[0].text:= 'Open';   // door is open
      else          sb.panels[0].text:= '??';
    end;
  end;

end;

procedure TFrmIdentify.Timer1Timer(Sender: TObject);
var
  theLastMode : TMPModes;
begin
  timer1.Enabled:= false;
  theLastMode:= lastMode;
  lastMode:= mp.Mode;
  ShowMode;
  if (theLastMode = mpOpen) and (mp.mode <> mpOpen) and (mp.DeviceID <> 0) then
    DoorWasClosed;
  timer1.Enabled:= true;
end;

procedure TFrmIdentify.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  try
    mp.Stop;
    mp.Close;
  except
  end;
end;

procedure TFrmIdentify.CloseCDDoor;
var
  setParams : TMCI_SET_Parms;
  ret       : integer;
begin
  screen.Cursor:= crHourGlass;
  setParams.dwCallback:= handle;
  ret:= mciSendCommand(mp.DeviceID, MCI_SET, MCI_SET_DOOR_CLOSED,
                       longint(@setParams));
  screen.Cursor:= crDefault;
end;

procedure TFrmIdentify.mpClick(Sender: TObject; Button: TMPBtnType;
  var DoDefault: Boolean);
begin
  mp.Notify:= true;
  if (button = btEject) and (mp.Mode = mpOpen) then
  begin
    CloseCDDoor;
    DoDefault:= false;
  end
  else
    screen.Cursor:= crHourGlass;
end;

procedure TFrmIdentify.mpPostClick(Sender: TObject; Button: TMPBtnType);
begin
  screen.cursor:= crDefault;
end;

procedure TFrmIdentify.FormCreate(Sender: TObject);
begin
  mp.TimeFormat:= tfTMSF;
  lastMode:= mp.Mode;
  ShowID;
end;

procedure TFrmIdentify.ShowID;
var
  infoParams : TMCI_Info_Parms;
  buf        : array[0..100] of char;
  tstr       : string;
  ret        : MCIERROR;
  k          : integer;
begin
  infoParams.dwCallback:= handle;
  SetLength(tstr, 100);
  infoParams.lpstrReturn:= PChar(tstr);
  infoParams.dwRetSize:= 100;
  ret:= mciSendCommand(mp.DeviceID, MCI_INFO, MCI_INFO_MEDIA_IDENTITY,
                       longint(@infoParams));
  if ret = 0 then
  begin
    k:= StrToInt(tstr);
    lblCDID.caption:= Format('%x', [k]);
  end
  else
    lblCDID.caption:= '<not available>';

  ret:= mciSendCommand(mp.DeviceID, MCI_INFO, MCI_INFO_MEDIA_UPC,
                         longint(@infoParams));
  if ret <> 0 then
  begin
    SetLength(tstr, 255);
    mciGetErrorString(ret, PChar(tstr), 255);
    SetLength(tstr, StrLen(PChar(tstr)));
//    ShowMessage(tstr);
    lblUPC.caption:= '<not available>';
  end
  else
    lblUPC.caption:= tstr;
end;

procedure TFrmIdentify.DoorWasClosed;
begin
  ShowID;
end;

procedure TFrmIdentify.BtnLinkClick(Sender: TObject);
begin
  SelectMusic(self);
end;

END.

