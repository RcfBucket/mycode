unit Artists;

interface

uses
  SysUtils, WinTypes, WinProcs, Messages, Classes, Graphics, Controls,
  Forms, Dialogs, StdCtrls, Buttons, DBCtrls, ExtCtrls, Grids, DBGrids, DB,
  DBTables, Global;

type
  TFrmArtists = class(TForm)
    DataSource1: TDataSource;
    Table1: TTable;
    DBGrid1: TDBGrid;
    panel1: TPanel;
    DBNav: TDBNavigator;
    btnSelect: TBitBtn;
    BtnCancel: TBitBtn;
    procedure DataSource1StateChange(Sender: TObject);
    procedure DBGrid1DblClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

function EditArtists(anOwner: TComponent; var anID: string): boolean;

implementation

{$R *.DFM}

function EditArtists(anOwner: TComponent; var anID: string): boolean;
var
  f   : TFrmArtists;
begin
  result:= false;
  f:= TFrmArtists.Create(anOwner);
  with f do
  begin
    caption:= 'Artists';
    table1.TableName:= 'ARTIST.DB';
    table1.active:= true;
    if anID <> EmptyStr then
      table1.FindKey([anID]);   {- position to record }
  end;
  if f.ShowModal = mrOK then
  begin
    anID:= f.Table1.FieldByName('ID').AsString;
    result:= true;
  end;
  f.table1.active:= false;
  f.Free;
end;

procedure TFrmArtists.DataSource1StateChange(Sender: TObject);
begin
  btnCancel.enabled:= table1.state = dsBrowse;
  btnSelect.enabled:= btnCancel.enabled;
  panel1.caption:= GetTableStateString(table1.State);
end;

procedure TFrmArtists.DBGrid1DblClick(Sender: TObject);
begin
  if table1.state = dsBrowse then
    ModalResult:= mrOK;
end;

end.
