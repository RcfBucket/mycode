�
 TFRMMUSICEDIT 0�3  TPF0TFrmMusicEditFrmMusicEditLeft� ToplBorderIconsbiSystemMenu BorderStylebsDialogCaptionMusic EntryClientHeightqClientWidth1
Font.ColorclBlackFont.Height�	Font.NameMS Sans Serif
Font.Style 
KeyPreview	PositionpoScreenCenterOnClose	FormCloseOnCreate
FormCreate	OnDestroyFormDestroy	OnKeyDownFormKeyDownPixelsPerInch`
TextHeight TBevelBevel1LeftTopWidth!Height9ShapebsFrame  TLabelLabel1LeftTopWidth6HeightCaptionRetail Price  TLabelLabel2LeftTop� Width+HeightCaption
List Price  TLabel	lblArtistLeftTopDWidthHeightCaptionArtist  TLabellblLabelLeftTop\Width2HeightCaption
Disk Label  TLabellblConditionLeftTop� Width,HeightCaption	Condition  TLabellblMediaLeftTop|Width8HeightCaption
Media Type  TLabellblPackagingLeftTop� Width3HeightCaption	Packaging  TLabellblCategoryLeftTop� Width*HeightCaptionCategory  TLabel	lblCopiesLeftTop� Width-HeightCaption	Disk Sets  TLabellblLocationLeftTop� Width)HeightCaptionLocation  TLabel
lblCountryLeftTop|Width$HeightCaptionCountry  TLabellblNotesLeftTop� WidthHeightCaption&Notes  TLabellblItemsLeft�Top� Width=HeightCaptionDisks per set  TLabellblTitleLeftTop,Width,HeightCaption
Disk Title  TLabellblDateLeftTop� WidthHeightCaptionDate  TLinkButtonLinkEdCategoryLeft� Top� WidthHeight
Glyph.Data
    BM      v   (               x                         �  �   �� �   � � ��  ��� ���   �  �   �� �   � � ��  ��� wwwwwwwwwwwwwwwwwwwwwwwww wpw ww wpw wwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwww                          OnClickLookupClickLinkComponent
edCategory  TLinkButtonLinkEdMediaLeft� TopxWidthHeight
Glyph.Data
    BM      v   (               x                         �  �   �� �   � � ��  ��� ���   �  �   �� �   � � ��  ��� wwwwwwwwwwwwwwwwwwwwwwwww wpw ww wpw wwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwww                          OnClickLookupClickLinkComponentedMedia  TLinkButtonLinkButton3Left� Top� WidthHeight
Glyph.Data
    BM      v   (               x                         �  �   �� �   � � ��  ��� ���   �  �   �� �   � � ��  ��� wwwwwwwwwwwwwwwwwwwwwwwww wpw ww wpw wwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwww                          OnClickLookupClickLinkComponent	edPackage  TLinkButtonLinkButton4Left� Top� WidthHeight
Glyph.Data
    BM      v   (               x                         �  �   �� �   � � ��  ��� ���   �  �   �� �   � � ��  ��� wwwwwwwwwwwwwwwwwwwwwwwww wpw ww wpw wwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwww                          OnClickLookupClickLinkComponent
edLocation  TLinkButtonLinkButton5Left� Top� WidthHeight
Glyph.Data
    BM      v   (               x                         �  �   �� �   � � ��  ��� ���   �  �   �� �   � � ��  ��� wwwwwwwwwwwwwwwwwwwwwwwww wpw ww wpw wwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwww                          OnClickLookupClickLinkComponentedCondition  TLinkButtonLinkButton6LeftTop@WidthHeight
Glyph.Data
    BM      v   (               x                         �  �   �� �   � � ��  ��� ���   �  �   �� �   � � ��  ��� wwwwwwwwwwwwwwwwwwwwwwwww wpw ww wpw wwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwww                          OnClickLinkButton6ClickLinkComponentedArtist  TLinkButtonLinkButton7LeftTopXWidthHeight
Glyph.Data
    BM      v   (               x                         �  �   �� �   � � ��  ��� ���   �  �   �� �   � � ��  ��� wwwwwwwwwwwwwwwwwwwwwwwww wpw ww wpw wwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwww                          OnClickLookupClickLinkComponentedLabel  TLabelLabel4LeftTop� WidthHeightCaptionID  TLabelLabel5LeftTop� WidthHeightCaptionUPC  TLabellblValueLeftTop0Width9HeightCaptionActual Price  TDBMemoedNotesLeftPTop� Width� HeightI	DataFieldNotes
DataSourceDSParentShowHintShowHintTabOrder  TDBSpinEditedListLeft`Top� WidthaHeight	Increment       ��?TabOrder	DataField
List Price
DataSourceDS  TBitBtnBtnCloseLeft�TopPWidthaHeightCancel	CaptionCloseModalResultTabOrder
Glyph.Data
�  �  BM�      6  (   $            �                        �  �   �� �   � � ��  ��� ��� �ʦ                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 ��� ��� ���   �  �   �� �   � � ��  ��� ������������������������������������������������������������������������������������������������������������������������	NumGlyphs  TBitBtn	BtnTracksLeftTopPWidthaHeightCaption&Tracks
Font.ColorclBlackFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrderOnClickBtnTracksClick
Glyph.Data
z  v  BMv      v   (                                     �  �   �� �   � � ��  ��� ���   �  �   �� �   � � ��  ��� 333333333333333�333333<�3333338�333333<�?�������Ȉ���������������333333<��������Ȉ���Ȉ�����������33<�3<��������Ȉ̈����������������3<3<��������Ȉ�Ȉ������������33�3<33��������Ȉ�Ȉ�����������333���33333����3333�3<33333����3333���33333���333333333333333333	NumGlyphs  TBitBtnBtnSaveLeftXTopPWidthaHeightCaption&SaveTabOrderOnClickBtnSaveClick
Glyph.Data
�  �  BM�      6  (   $            �                        �  �   �� �   � � ��  ��� ��� �ʦ                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 ��� ��� ���   �  �   �� �   � � ��  ��� �����������������������������������������������������������������������������	NumGlyphs  TDBEditedTitleLeft`Top(Width�Height	DataFieldTitle
DataSourceDS	MaxLength ParentShowHintShowHintTabOrder   TDBEdit	edCountryLeftNTopxWidth� Height	DataFieldCountry
DataSourceDS	MaxLength ParentShowHintShowHintTabOrder  TDBEditedDateLeftNTop� Width� Height	DataFieldDate
DataSourceDS	MaxLength ParentShowHintShowHintTabOrder  TDBCodeEdit
edCategoryLeft`Top� WidthyHeight	DataFieldCategory
DataSourceDSLookupTableCATEGORY.DBLookupDisplayCategoryLookupDatabaseMEDIADB
LookupCaseluUpperCtl3D		MaxLength ParentCtl3DTabOrder  TDBCodeEditedMediaLeft`TopxWidthyHeight	DataField
Media Type
DataSourceDSLookupTableMEDIA.DBLookupDisplay
Media TypeLookupDatabaseMEDIADB
LookupCaseluUpper	MaxLength TabOrder  TDBCodeEdit	edPackageLeft`Top� WidthyHeight	DataField	Packaging
DataSourceDSLookupTable
PACKAGE.DBLookupDisplay	PackagingLookupDatabaseMEDIADB
LookupCaseluUpper	MaxLength TabOrder  TDBCodeEdit
edLocationLeft`Top� WidthyHeight	DataFieldLocation
DataSourceDSLookupTableLOCATION.DBLookupDisplayLocationLookupDatabaseMEDIADB
LookupCaseluUpper	MaxLength TabOrder  TDBCodeEditedConditionLeft`Top� WidthyHeight	DataField	Condition
DataSourceDSLookupTableCONDTION.DBLookupDisplay	ConditionLookupDatabaseMEDIADB
LookupCaseluUpper	MaxLength TabOrder  TDBCodeEditedArtistLeft`Top@Width�Height	DataFieldArtist
DataSourceDSLookupTable	ARTIST.DBLookupDisplayArtistLookupDatabaseMEDIADB
LookupCaseluUpper	MaxLength TabOrder  TDBCodeEditedLabelLeft`TopXWidth�Height	DataFieldLabel
DataSourceDSLookupTableLABEL.DBLookupDisplayLabelLookupDatabaseMEDIADB
LookupCaseluUpper	MaxLength TabOrder  TDBSpinEditedSetsLeftNTop� WidthAHeight	Increment       ��?MaxValue      `�@TabOrder	DataFieldSets
DataSourceDS  TDBSpinEditedDisksInSetLeft�Top� WidthAHeight	Increment       ��?MaxValue      `�@TabOrder	DataFieldDisks in set
DataSourceDS  TDBEditedIDLeftNTop� Width� Height	DataFieldID
DataSourceDS	MaxLength ParentShowHintShowHintTabOrder  TDBEdit	edBarcodeLeftNTop� Width� Height	DataFieldUPC
DataSourceDS	MaxLength ParentShowHintShowHintTabOrder  TDBSpinEditedActualLeft`Top+WidthaHeight	Increment       ��?TabOrder
	DataFieldActual Price
DataSourceDS  TDBSpinEditedRetailLeft`TopWidthaHeight	Increment       ��?TabOrder		DataFieldRetail Price
DataSourceDS  TPanelpnlModeLeftTopWidthHeight	AlignmenttaLeftJustifyCaption <mode>
Font.ColorclBlueFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrder TLabellblTotalTimeLeft� TopWidthiHeightAutoSizeCaptionTotal Time 00'00
Font.ColorclBlueFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont  TLabellblTotalTracksLeft�TopWidthaHeight	AlignmenttaRightJustifyAutoSizeCaption
Tracks 999   TTabletblAudioOnNewRecordtblAudioNewRecordDatabaseNameMEDIADB	TableNameAUDIO.DBLeft� TopP TAutoIncFieldtblAudioSeq	FieldNameSeq  TStringField
tblAudioID	FieldNameID  TStringFieldtblAudioTitle	FieldNameTitleSize2  TStringFieldtblAudioArtist	FieldNameArtistSize  TStringFieldtblAudioLabel	FieldNameLabelSize
  TStringFieldtblAudioMediaType	FieldName
Media TypeSize
  TStringFieldtblAudioCategory	FieldNameCategorySize
  TStringFieldtblAudioPackaging	FieldName	PackagingSize
  TIntegerFieldtblAudioSets	FieldNameSets  TCurrencyFieldtblAudioActualPrice	FieldNameActual Price  TCurrencyFieldtblAudioRetailPrice	FieldNameRetail Price  TCurrencyFieldtblAudioListPrice	FieldName
List Price  TStringFieldtblAudioCountry	FieldNameCountrySize  TStringFieldtblAudioCondition	FieldName	ConditionSize
  TStringFieldtblAudioLocation	FieldNameLocationSize
  TSmallintFieldtblAudioDisksinset	FieldNameDisks in set  TStringFieldtblAudioDate	FieldNameDate  
TMemoFieldtblAudioNotes	FieldNameNotesBlobTypeftMemoSize2  TStringFieldtblAudioUPC	FieldNameUPCSize   TDataSourceDSDataSettblAudioOnStateChangeDSStateChangeLeft� TopP   