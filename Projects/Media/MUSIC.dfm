˙
 TFRMMUSIC 0Â  TPF0	TFrmMusicFrmMusicLeftú TopkBorderIconsbiSystemMenu BorderStylebsDialogCaptionMusicClientHeightIClientWidtha
Font.ColorclBlackFont.Heightő	Font.NameMS Sans Serif
Font.Style PositionpoScreenCenterShowHint	OnCreate
FormCreatePixelsPerInch`
TextHeight TBevelBevel1LeftTopWidthQHeightShapebsFrame  TLabelLabel1LeftTop0WidthIHeightAutoSizeCaptionRecord Count  TLabellblRecordCountLeft`Top0WidthaHeightAutoSizeCaptionlblRecordCount  TBitBtnBtnCloseLeft Top(WidthYHeightCaption&CloseTabOrderKindbkCancel  TDBGridDBGrid1LeftTop0WidthAHeighté 
DataSourceDSOptionsdgTitlesdgIndicatordgColumnResize
dgColLines
dgRowLinesdgTabsdgRowSelectdgConfirmDeletedgCancelOnExit TabOrderTitleFont.ColorclBlackTitleFont.HeightőTitleFont.NameMS Sans SerifTitleFont.Style 
OnDblClickDBGrid1DblClick	OnKeyDownDBGrid1KeyDown  TPanelpanel1LeftTopWidthAHeight 	AlignmenttaLeftJustifyCaption
<editMode>
Font.ColorclBlueFont.Heightő	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrder  TDBNavigatorDBNavLeft@TopWidthú Height
DataSourceDSCtl3DHints.StringsFirst Record - (Home)Prior Record - (Up)Next Record - (Down)Last Record - (End)Insert Record - (Insert)Delete Record - (Ctrl-Delete)Edit Record - (F2)Post ChangesCancel Changes - (Esc)Refresh Database ParentCtl3DTabOrder    TBitBtnBtnValueLeft Top(WidthYHeightCaption&ValueTabOrderOnClickBtnValueClick
Glyph.Data
â  Ţ  BMŢ      v   (   $            h                                     ŔŔŔ    ˙  ˙   ˙˙ ˙   ˙ ˙ ˙˙  ˙˙˙ 333333333333333333  333333333333333333  333:#333333338ó333  333˘"33333333333  33:""#33333388xó33  33˘*("3333338÷33  33˘##"3333338÷33  333:""33333338w33  333˘"(3333333x333  33:""3333338w333  33˘"(3333333x3333  33˘(#3333333x3ó33  33˘##"333333xř33  33:""(3333338řř333  333˘"3333333333  333:(333333338ó333  3333#3333333383333  333333333333333333  	NumGlyphs  TBitBtn	BtnTracksLeft8Top(WidthaHeightCaption&Tracks
Font.ColorclBlackFont.Heightő	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrderOnClickBtnTracksClick
Glyph.Data
z  v  BMv      v   (                                                  ŔŔŔ    ˙  ˙   ˙˙ ˙   ˙ ˙ ˙˙  ˙˙˙ 333333333333333˙333333<Ă3333338333333<Ě?˙˙˙˙˙řČĂ333333<˙˙˙˙˙˙řČČĂĂ33<Ě3<˙˙ř˙řČĚĂĂĚĂ3<3<˙ř˙řČČĂ33Ă3<33˙˙˙ř˙˙ČČ333ĚĚĚ33333ó3333Ă3<33333˙řó3333ĚĚĚ33333333333333333333333	NumGlyphs  TBitBtnBtnIdentifyLeftĐ Top(WidthaHeightCaption	&IdentifyTabOrderOnClickBtnIdentifyClick
Glyph.Data
|  x  BMx      v   (                                                    żżż   ˙  ˙   ˙˙ ˙   ˙ ˙ ˙˙  ˙˙˙ 33333333333˙˙ó33330  33333÷ww?ó330ř 3337s33w?33˙33s3333só0Ţ˙řřţĐ37ó33337ó0Ýď˙˙íĐ3733?˙37?żŢ ßť33wsó3
Ť°ťŞ37ó÷ó3ę Şî37÷7ó3
Ť°ťŞ37?÷33żŢ Űťsó3ws33s0Ýď˙˙íĐ37ó33337ó0Ţřř˙ţĐ37?3333733˙˙33s˙33?s330řř 3337s˙˙w33330  333337ww333  	NumGlyphs  TDataSourceDSDataSetTblAudioOnStateChangeDSStateChangeLeftřTopŕ   TTable
tblArtistsActive	DatabaseNameMEDIADB	TableName	ARTIST.DBLeftŘTopŕ   TQueryTblAudioActive	BeforeInserttblAudioBeforeInsertAfterInserttblAudioAfterInsert
BeforeEdittblAudioBeforeEditBeforeDeletetblAudioBeforeDeleteAfterDeletetblAudioAfterDeleteDatabaseNameMEDIADBRequestLive	SQL.Stringsselect * from AUDIO.DB LeftTopŕ  TAutoIncFieldTblAudioSeq	FieldNameSeqVisible  TStringFieldTblAudioTitleDisplayWidth9	FieldNameTitleSize2  TStringFieldTblAudioArtistDisplayWidth/	FieldNameArtistVisibleSize  TStringFieldTblAudioArtistNameDisplayLabelArtistDisplayWidth2	FieldName
ArtistNameLookup	LookupDataSet
tblArtistsLookupKeyFieldsIDLookupResultFieldArtist	KeyFieldsArtist    