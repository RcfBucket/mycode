unit fmtmain;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls;

type
  TForm1 = class(TForm)
    Label1: TLabel;
    Label2: TLabel;
    edMask: TEdit;
    edValue: TEdit;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    procedure edMaskChange(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

{$R *.DFM}

procedure TForm1.edMaskChange(Sender: TObject);
var
  k   : integer;
begin
  k:= StrToInt(edValue.text);
  label2.caption:= Format(edMask.text, [k]);
end;

end.

