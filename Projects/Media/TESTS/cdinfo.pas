unit cdinfo;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  MPlayer, StdCtrls, Grids, ExtCtrls, MMSystem, Buttons;

type
  TForm1 = class(TForm)
    mp: TMediaPlayer;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    BtnUpdate: TButton;
    grid: TStringGrid;
    Timer1: TTimer;
    BtnNext: TButton;
    Label5: TLabel;
    Label6: TLabel;
    BtnID: TButton;
    Label7: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    Label10: TLabel;
    Label11: TLabel;
    Label12: TLabel;
    Label13: TLabel;
    Label14: TLabel;
    Label15: TLabel;
    lblMode: TLabel;
    SpeedButton1: TSpeedButton;
    Label16: TLabel;
    lblDevType: TLabel;
    Label17: TLabel;
    lblCDType: TLabel;
    Button1: TButton;
    Button2: TButton;
    Label18: TLabel;
    lblMedPres: TLabel;
    procedure FormCreate(Sender: TObject);
    procedure BtnUpdateClick(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
    procedure mpPostClick(Sender: TObject; Button: TMPBtnType);
    procedure mpClick(Sender: TObject; Button: TMPBtnType;
      var DoDefault: Boolean);
    procedure BtnNextClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure BtnIDClick(Sender: TObject);
    procedure ShowMode;
    procedure SpeedButton1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure Button1Click(Sender: TObject);
  private
    procedure ShowPos;
    procedure ShowCDType;
    procedure MediaPresent;
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

{$R *.DFM}

function MSToStr(ms: integer): string;
// convert milliseconds to h:m:s
var
  d   : double;
  h,m,s : integer;
begin
  d:= ms / 1000.0;    // convert to seconds
  m:= Trunc(d / 60.0);
  s:= Trunc(Frac(d / 60.0) * 60);
  result:= Format('%d:%d', [m, s]);
end;

procedure TForm1.FormCreate(Sender: TObject);
begin
  mp.TimeFormat:= tfMilliseconds;
  BtnUpdate.Click;
  label6.caption:= Format('%x', [mp.DeviceID]);
end;

procedure TForm1.BtnUpdateClick(Sender: TObject);
var
  t : integer;
  k : integer;
  f : double;
begin
  ShowPos;
  label1.caption:= IntToStr(mp.Tracks);
  grid.cells[0, 0]:= 'Track';
  grid.cells[1, 0]:= 'Length';
  grid.cells[2, 0]:= 'Start';
  grid.cells[3, 0]:= 'Start2';
  grid.cells[4, 0]:= 'Length2';
  for k:= 1 to mp.Tracks do
  begin
    grid.cells[0, k]:= IntToStr(k);

    t:= mp.TrackLength[k];
    grid.cells[4, k]:= IntToStr(t);
    grid.cells[1, k]:= MSToStr(t);

    t:= mp.TrackPosition[k];
    grid.cells[3, k]:= IntToStr(t);
    grid.cells[2, k]:= MSToStr(t);
  end;
  label6.caption:= IntToStr(mp.DeviceID);
end;

procedure TForm1.Timer1Timer(Sender: TObject);
begin
  ShowPos;
  ShowMode;
end;

procedure TForm1.mpPostClick(Sender: TObject; Button: TMPBtnType);
begin
  ShowPos;
end;

procedure TForm1.ShowPos;
type
  TCDTime = record
    Tracks  : byte;
    Minutes : byte;
    Seconds : byte;
    Frames  : byte;
  end;
var
  cdTime  : TCDTime;
  tstr    : string;
begin
  try
    mp.TimeFormat:= tfMilliseconds;
    label4.caption:= IntToStr(mp.position) + ' ms';

    mp.TimeFormat:= tfTMSF;
    label11.caption:= IntToStr(mp.position and $F);

    label1.caption:= IntToStr(mp.Tracks);
    label6.caption:= Format('%x', [mp.DeviceID]);

    cdTime:= TCDTime(mp.position);
    label4.caption:= Format('%.2d:%.2d', [cdTime.minutes, cdTime.seconds]);

    case mp.DeviceType of
      dtAutoSelect :  lblDevType.caption:= 'dtAutoSelect';
      dtAVIVideo  : lblDevType.caption:= 'dtAVIVideo';
      dtCDAudio : lblDevType.caption:= 'dtCDAudio';
      dtDAT : lblDevType.caption:= 'dtDAT';
      dtDigitalVideo  : lblDevType.caption:= 'dtDigitalVideo';
      dtMMMovie : lblDevType.caption:= 'dtMMMovie';
      dtOther : lblDevType.caption:= 'dtOther';
      dtOverlay : lblDevType.caption:= 'dtOverlay';
      dtScanner : lblDevType.caption:= 'dtScanner';
      dtSequencer : lblDevType.caption:= 'dtSequencer';
      dtVCR : lblDevType.caption:= 'dtVCR';
      dtVideodisc : lblDevType.caption:= 'dtVideodisc';
      dtWaveAudio  : lblDevType.caption:= 'dtWaveAudio';
    end;

    ShowCDType;
    MediaPresent;
    ShowMode;
  except
  end;
end;

procedure TForm1.mpClick(Sender: TObject; Button: TMPBtnType;
  var DoDefault: Boolean);
begin
  mp.TimeFormat:= tfTMSF;
  DoDefault:= true;
end;

procedure TForm1.BtnNextClick(Sender: TObject);
begin
  mp.Wait:= true;
  mp.TimeFormat:= tfTMSF;
  mp.Next;
  if mp.Error <> 0 then
    ShowMessage(mp.ErrorMessage);
  ShowPos;
end;

procedure TForm1.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  mp.Stop;
  mp.Close;
end;

procedure TForm1.BtnIDClick(Sender: TObject);
var
  infoParams : TMCI_Info_Parms;
  buf        : array[0..100] of char;
  tstr       : string;
  ret        : MCIERROR;
  k          : integer;
begin
  infoParams.dwCallback:= handle;
  SetLength(tstr, 100);
  infoParams.lpstrReturn:= PChar(tstr);
  infoParams.dwRetSize:= 100;
  ret:= mciSendCommand(mp.DeviceID, MCI_INFO, MCI_INFO_MEDIA_IDENTITY,
                  longint(@infoParams));
  if ret <> 0 then
    MessageBeep(0);
  k:= StrToInt(tstr);
  label7.caption:= Format('%x', [k]);

  ret:= mciSendCommand(mp.DeviceID, MCI_INFO, MCI_INFO_MEDIA_UPC,
                  longint(@infoParams));
  if ret <> 0 then
  begin
    SetLength(tstr, 255);
    mciGetErrorString(ret, PChar(tstr), 255);
    SetLength(tstr, StrLen(PChar(tstr)));
    ShowMessage(tstr);
    label10.caption:= '???';
  end
  else
    label10.caption:= tstr;
end;

procedure TForm1.MediaPresent;
var
  params     : TMCI_Status_Parms;
  tstr       : string;
  ret        : MCIERROR;
  k          : integer;
  fflags     : longint;
begin
  params.dwItem:= MCI_STATUS_MEDIA_PRESENT;
  params.dwTrack:= 1;
  fflags:= mci_Status_Item {or mci_Track};
  ret:= mciSendCommand(mp.DeviceID, mci_STATUS, fflags, longint(@params));

  if ret <> 0 then
  begin
    timer1.enabled:= false;
    SetLength(tstr, 255);
    mciGetErrorString(ret, PChar(tstr), 255);
    SetLength(tstr, StrLen(PChar(tstr)));
    ShowMessage(tstr);
  end;
  if params.dwReturn = 0 then
    tstr:= 'media not present'
  else
    tstr:= 'media present';
  lblMedPres.caption:= tstr;
end;

procedure TForm1.ShowCDType;
var
  params     : TMCI_Status_Parms;
  tstr       : string;
  ret        : MCIERROR;
  k          : integer;
  fflags     : longint;
begin
//  params.dwCallback:= handle;
  params.dwItem:= MCI_CDA_STATUS_TYPE_TRACK;
  params.dwTrack:= 0;
  fflags:= mci_Status_Item or mci_Track;
  ret:= mciSendCommand(mp.DeviceID, mci_STATUS, fflags, longint(@params));

  if ret <> 0 then
  begin
    timer1.enabled:= false;
    SetLength(tstr, 255);
    mciGetErrorString(ret, PChar(tstr), 255);
    SetLength(tstr, StrLen(PChar(tstr)));
    application.MessageBox(PChar(tstr), 'CD Type', MB_OK);
    lblCDType.caption:= '---';
  end
  else
  begin
    if params.dwReturn = MCI_CDA_TRACK_AUDIO then
      tstr:= 'Audio'
    else
      tstr:= 'Other';

    lblCDType.caption:= tstr;
  end;
end;

procedure TForm1.ShowMode;
begin
  if mp.DeviceID = 0 then
  begin
    lblMode.caption:= 'CD not ready (deviceID = 0)';
  end
  else
  begin
    case mp.Mode of
      mpNotReady  : lblMode.caption:= 'Not ready';
      mpStopped   : lblMode.caption:= 'Stopped';
      mpPlaying	  : lblMode.caption:= 'Playing';
      mpRecording	: lblMode.caption:= 'Recording';
      mpSeeking	  : lblMode.caption:= 'Seeking';
      mpPaused	  : lblMode.caption:= 'Paused';
      mpOpen	    : lblMode.caption:= 'Open';   // door is open
      else          lblMode.caption:= '??';
    end;
  end;
end;

procedure TForm1.SpeedButton1Click(Sender: TObject);
var
  setParams : TMCI_SET_Parms;
  buf        : array[0..100] of char;
  tstr       : string;
  ret        : MCIERROR;
  k          : integer;
begin
  setParams.dwCallback:= handle;
  ret:= mciSendCommand(mp.DeviceID, MCI_SET, MCI_SET_DOOR_CLOSED,
                  longint(@setParams));
end;

procedure TForm1.Button2Click(Sender: TObject);
begin
  mp.close;
end;

procedure TForm1.Button1Click(Sender: TObject);
begin
  mp.open;
end;

END.
