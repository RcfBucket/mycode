unit Options;

interface

uses
  SysUtils, WinTypes, WinProcs, Messages, Classes, Graphics, Controls,
  {$IFNDEF Ver80}
  ComCtrls,
  {$ENDIF}
  Forms, Dialogs, StdCtrls, Buttons, Global, Tabnotbk;

type
  TFrmOptions = class(TForm)
    BitBtn2: TBitBtn;
    BitBtn1: TBitBtn;
    pc: TPageControl;
    TabOptions: TTabSheet;
    TabSheet2: TTabSheet;
    GroupBox1: TGroupBox;
    Label1: TLabel;
    Label2: TLabel;
    lblSample: TLabel;
    edSeperator: TEdit;
    BtnMaintainTables: TBitBtn;
    BtnRebuild: TBitBtn;
    BtnPurge: TBitBtn;
    procedure FormCreate(Sender: TObject);
    procedure edSeperatorChange(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure BtnRebuildClick(Sender: TObject);
    procedure BtnMaintainTablesClick(Sender: TObject);
  private
    sample    : longint;
    sep       : string[3];
  public
    { Public declarations }
  end;

implementation

uses maintain;

{$R *.DFM}

procedure TFrmOptions.FormCreate(Sender: TObject);
var
  err : boolean;
begin
  sample:= StrToSongTime('12''43', err);
  sep:= minuteSeperator;
  edSeperator.text:= sep;
  pc.ActivePage:= TabOptions;  
end;

procedure TFrmOptions.edSeperatorChange(Sender: TObject);
var
  s   : string;
  k   : integer;
  te  : TEdit;
begin
  te:= (sender as TEdit);
  s:= te.text;
  for k:= 1 to Length(s) do
    if (s[k] >= '0') and (s[k] <= '9') then
    begin
      lblSample.caption:= 'Invalid';
      exit;
    end;

  if s <> emptyStr then
    MinuteSeperator:= s;
  lblSample.caption:= SongTimeToStr(sample, true);
end;

procedure TFrmOptions.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  if ModalResult <> mrOK then
    MinuteSeperator:= sep;
end;

procedure TFrmOptions.BtnRebuildClick(Sender: TObject);
begin
  RebuildAllIndexes;
end;

procedure TFrmOptions.BtnMaintainTablesClick(Sender: TObject);
// Let user edit miscellaneous support tables
begin
  MaintainTables(self);
end;

end.
