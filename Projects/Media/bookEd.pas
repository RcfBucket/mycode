unit BookEd;

interface

uses
  SysUtils, WinTypes, WinProcs, Messages, Classes, Graphics, Controls,
  Forms, Dialogs, Mask, Buttons, StdCtrls, DB, DBTables,
  ExtCtrls, Linkbtn, DBCtrls, dbspin, CodeEdit, TableEd, Spin, Global;

type
  TFrmBookEdit = class(TForm)
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    Label10: TLabel;
    Label11: TLabel;
    Label13: TLabel;
    Label14: TLabel;
    Bevel1: TBevel;
    lblTitle: TLabel;
    Label1: TLabel;
    pnlMode: TPanel;
    BtnClose: TBitBtn;
    BtnSave: TBitBtn;
    LinkButton1: TLinkButton;
    LinkButton2: TLinkButton;
    LinkButton3: TLinkButton;
    LinkButton4: TLinkButton;
    Label12: TLabel;
    Label15: TLabel;
    Label16: TLabel;
    Label17: TLabel;
    Label18: TLabel;
    edBarcode: TDBEdit;
    edID: TDBEdit;
    TblBook: TTable;
    DS: TDataSource;
    edTitle: TDBEdit;
    edAuthor: TDBCodeEdit;
    edPublisher: TDBCodeEdit;
    edMedia: TDBCodeEdit;
    edCategory: TDBCodeEdit;
    edISBN: TDBEdit;
    edNotes: TDBMemo;
    edEdition: TDBEdit;
    edIssue: TDBEdit;
    edVolume: TDBEdit;
    edCopyright: TDBEdit;
    edCountry: TDBEdit;
    edCopies: TDBSpinEdit;
    edActual: TDBSpinEdit;
    edRetail: TDBSpinEdit;
    edList: TDBSpinEdit;
    edPages: TDBSpinEdit;
    procedure btnOKClick(Sender: TObject);
    procedure BtnCloseClick(Sender: TObject);
    procedure BtnSaveClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure LookupClick(sender: TObject);
    procedure DSStateChange(Sender: TObject);
  private
    editMode  : boolean;
    bm        : TBookMark;
    procedure PrepareMode;
  public
    { Public declarations }
  end;

procedure EditBooks(aOwner: TComponent; aTable: TTable);

implementation

{$R *.DFM}

procedure EditBooks(aOwner: TComponent; aTable: TTable);
var
  frm : TFrmBookEdit;
begin
  screen.cursor:= crHourGlass;
  frm:= TFrmBookEdit.Create(aOwner);
  try
    frm.tblBook.active:= true;
  except
    screen.cursor:= crDefault;
  end;
  frm.editMode:= aTable <> nil;
  if aTable <> nil then
    frm.bm:= aTable.GetBookMark
  else
    frm.bm:= nil;
  try
    frm.PrepareMode;
  except
    frm.tblBook.Active:= false;
    screen.cursor:= crDefault;
    raise;
  end;
  screen.cursor:= crDefault;
  frm.ShowModal;
  frm.tblBook.active:= false;
  frm.Free;
end;

procedure TFrmBookEdit.btnOKClick(Sender: TObject);
begin
  Close;
end;

procedure TFrmBookEdit.BtnCloseClick(Sender: TObject);
begin
  Close;
end;

procedure TFrmBookEdit.PrepareMode;
begin
  if editMode then
  begin
    tblBook.GotoBookMark(bm);
    tblBook.Edit;
  end
  else
    tblBook.Insert;
end;

procedure TFrmBookEdit.BtnSaveClick(Sender: TObject);
begin
  tblBook.Post;    {- post changes }
  if not editMode then
    PrepareMode
  else
    modalResult:= mrOK;
end;

procedure TFrmBookEdit.FormCreate(Sender: TObject);
begin
  bm:= nil;
end;

procedure TFrmBookEdit.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  tblBook.Cancel;    {- cancel any pending modes }
end;

procedure TFrmBookEdit.LookupClick(Sender: TObject);
var
  ID    : string;
  isOK  : boolean;
begin
  with sender as TLinkButton do
  begin
    if LinkComponent <> nil then
    begin
      with LinkComponent as TDBCodeEdit do
      begin
        SetFocus;
        try
          ID:= RawValue;
        except
          ID:= '';
        end;
        isOK:= EditTable(self, LookupTable, LookupDisplay, true, ID);
        if isOK then
          RawValue:= ID;
        Refresh;
        SelectAll;
      end;
    end;
  end;
end;

procedure TFrmBookEdit.DSStateChange(Sender: TObject);
begin
  pnlMode.caption:= GetTableStateString(tblBook.State);
end;

END.
