’
 TFRMBOOKS 0š  TPF0	TFrmBooksFrmBooksLeft÷ Top{WidthiHeightdCaptionPrinted Media
Font.ColorclBlackFont.Heightõ	Font.NameMS Sans Serif
Font.Style PositionpoScreenCenterOnCreate
FormCreatePixelsPerInch`
TextHeight TBevelBevel1LeftTopWidthQHeightShapebsFrame  TPanelpanel1LeftTopWidthAHeight 	AlignmenttaLeftJustifyCaption
<editMode>
Font.ColorclBlueFont.Heightõ	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrder  TDBNavigatorDBNavLeft@TopWidthś Height
DataSourceDSCtl3DHints.StringsFirst Record - (Home)Prior Record - (Up)Next Record - (Down)Last Record - (End)Insert Record - (Insert)Delete Record - (Ctrl-Delete)Edit Record - (F2)Post ChangesCancel Changes - (Esc)Refresh Database ParentCtl3DTabOrder    TDBGridDBGrid1LeftTop0WidthAHeighté 
DataSourceDSOptionsdgTitlesdgIndicatordgColumnResize
dgColLines
dgRowLinesdgTabsdgRowSelectdgConfirmDeletedgCancelOnExit TabOrderTitleFont.ColorclBlackTitleFont.HeightõTitleFont.NameMS Sans SerifTitleFont.Style 
OnDblClickDBGrid1DblClick	OnKeyDownDBGrid1KeyDown  TBitBtnBitBtn3Left Top(WidthYHeightCaption&CloseTabOrderKindbkCancel  TBitBtnBitBtn1Left Top(WidthYHeightCaption&ValueTabOrderOnClickBitBtn1Click
Glyph.Data
ā  Ž  BMŽ      v   (   $            h                                     ĄĄĄ    ’  ’   ’’ ’   ’ ’ ’’  ’’’ 333333333333333333  333333333333333333  333:#333333338ó333  333¢"33333333333  33:""#33333388xó33  33¢*("3333338÷33  33¢##"3333338÷33  333:""33333338w33  333¢"(3333333x333  33:""3333338w333  33¢"(3333333x3333  33¢(#3333333x3ó33  33¢##"333333xų33  33:""(3333338ųų333  333¢"3333333333  333:(333333338ó333  3333#3333333383333  333333333333333333  	NumGlyphs  TDataSourceDSDataSetTblBooksOnStateChangeDSStateChangeLeft(Top(  TTableTblBooksBeforeInsertTblBooksBeforeInsert
BeforeEditTblBooksBeforeEditDatabaseNameMEDIADB	TableName
PRINTED.DBLeftTop( TStringFieldTblBooksTitleDisplayWidth<	FieldNameTitleSize2  TStringFieldTblBooksAuthorDisplayWidth/	FieldNameAuthorSize   TTable
TblAuthorsDatabaseNameMEDIADB	TableName	Author.dbLeftXTop(   