unit maintain;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, Grids;

type
  TFrmMaintain = class(TForm)
    Grid: TStringGrid;
    BtnClose: TBitBtn;
    BtnEdit: TBitBtn;
    procedure FormCreate(Sender: TObject);
    procedure BtnEditClick(Sender: TObject);
    procedure GridDblClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

procedure MaintainTables(aOwner: TComponent);

implementation

uses Tableed;

{$R *.DFM}

procedure MaintainTables(aOwner: TComponent);
var
  f   : TFrmMaintain;
begin
  f:= TFrmMaintain. Create(aOwner);
  f.ShowModal;
  f.Free;
end;

procedure TFrmMaintain.FormCreate(Sender: TObject);
begin
  with grid do
  begin
    colWidths[2]:= 0;
    RowCount:= 7;

    cells[0,0]:= 'Table';
    cells[1,0]:= 'Description';

    cells[0,1]:= 'Artists';
    cells[1,1]:= 'Table of artist names';
    cells[2,1]:= 'ARTIST.DB';

    cells[0,2]:= 'Authors';
    cells[1,2]:= 'Table of author names';
    cells[2,2]:= 'AUTHOR.DB';

    cells[0,3]:= 'Category';
    cells[1,3]:= 'Table of all media categories';
    cells[2,3]:= 'CATEGORY.DB';

    cells[0,4]:= 'Condition';
    cells[1,4]:= 'Table of all conditions';
    cells[2,4]:= 'CONDTION.DB';

    cells[0,5]:= 'Location';
    cells[1,5]:= 'Table of all locations';
    cells[2,5]:= 'LOCATION.DB';

    cells[0,6]:= 'Package';
    cells[1,6]:= 'Table of all packaging';
    cells[2,6]:= 'PACKAGE.DB';

  end;
end;

procedure TFrmMaintain.BtnEditClick(Sender: TObject);
var
  tblStr  : string;
  titleStr: string;
  ID      : string;
begin
  tblStr:= grid.cells[2, grid.row];
  titleStr:= grid.cells[1, grid.row];
  EditTable(self, tblStr, titleStr, false, ID);
end;

procedure TFrmMaintain.GridDblClick(Sender: TObject);
begin
  BtnEdit.Click;
end;

end.

