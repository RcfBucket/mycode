unit about;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons;

type
  TFrmAbout = class(TForm)
    Label1: TLabel;
    Label2: TLabel;
    BtnOK: TBitBtn;
    Label3: TLabel;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

procedure DoAbout;

implementation

{$R *.DFM}

procedure DoAbout;
var
  FrmAbout: TFrmAbout;
begin
  FrmAbout:= TFrmAbout.Create(application);
  FrmAbout.ShowModal;
  FrmAbout.Free;
end;

end.

