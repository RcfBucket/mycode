unit cdvalue;

interface

uses
  SysUtils, Classes, Graphics, Forms, Controls, StdCtrls,
  Buttons, ExtCtrls, DB, DBTables;

type
  TFrmValue = class(TForm)
    Bevel1: TBevel;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    lblActual: TLabel;
    lblRetail: TLabel;
    lblList: TLabel;
    BitBtn1: TBitBtn;
    Query1: TQuery;
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

procedure ShowValue(aOwner: TComponent; aTblName, aTitle: string);

implementation

{$R *.DFM}

procedure ShowValue(aOwner: TComponent; aTblName, aTitle: string);
var
  f   : TFrmValue;
begin
  f:= TFrmValue.Create(aOwner);
  f.Caption:= aTitle;
  with f.query1 do
  begin
    SQL.Clear;
    SQL.Add('SELECT SUM(A."Actual Price"),');
    SQL.Add('  SUM(A."List Price"),');
    SQL.Add('  SUM(a."Retail Price")');
    SQL.Add('FROM ' + aTblName + ' A');
  end;
  try
    f.query1.Active:= true;
  except
    raise;
  end;
  f.ShowModal;
  f.query1.active:= false;
  f.Free;
end;

procedure TFrmValue.FormShow(Sender: TObject);
var
  listVal, actualVal, retailVal: double;
begin
  lblActual.caption:= FormatFloat('$#,##0.00', query1.Fields[0].AsFloat);
  lblList.caption:= FormatFloat('$#,##0.00', query1.Fields[1].AsFloat);
  lblRetail.caption:= FormatFloat('$#,##0.00', query1.Fields[2].AsFloat);
(*  screen.Cursor:= crHourGlass;
  lblActual.caption:= '$0.0';
  lblRetail.caption:= '$0.0';
  lblList.caption:= '$0.0';
  with table1 do
  begin
    listVal:= 0.0;
    actualVal:= 0.0;
    retailVal:= 0.0;
    First;
    while not EOF do
    begin
      listVal:= listVal + FieldByName('List Price').AsFloat;
      actualVal:= actualVal + FieldByName('Actual Price').AsFloat;
      retailVal:= retailVal + FieldByName('Retail Price').AsFloat;
      Next;
    end;
  end;
  lblActual.caption:= FormatFloat('$#,##0.00', actualVal);
  lblRetail.caption:= FormatFloat('$#,##0.00', RetailVal);
  lblList.caption:= FormatFloat('$#,##0.00', listVal);
  screen.Cursor:= crDefault;*)
end;

end.
