VERSION 2.00
Begin Form frmMortReceive 
   BackColor       =   &H00C0C0C0&
   BorderStyle     =   3  'Fixed Double
   Caption         =   "Receiving Mortgaged Property"
   ClientHeight    =   4905
   ClientLeft      =   1860
   ClientTop       =   1980
   ClientWidth     =   7455
   Height          =   5310
   Left            =   1800
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   4905
   ScaleWidth      =   7455
   Top             =   1635
   Width           =   7575
   Begin CommandButton cmdCancel 
      Caption         =   "Cancel"
      Height          =   375
      Left            =   3795
      TabIndex        =   5
      Top             =   4440
      Width           =   1455
   End
   Begin CommandButton cmdOK 
      Caption         =   "&OK"
      Default         =   -1  'True
      Height          =   375
      Left            =   2115
      TabIndex        =   4
      Top             =   4440
      Width           =   1455
   End
   Begin SpreadSheet ss 
      AllowResize     =   -1  'True
      DisplayRowHeaders=   0   'False
      FontBold        =   0   'False
      FontItalic      =   0   'False
      FontName        =   "MS Sans Serif"
      FontSize        =   8.25
      FontStrikethru  =   0   'False
      FontUnderline   =   0   'False
      Height          =   2895
      InterfaceDesigner=   RCVMORT.FRX:0000
      Left            =   120
      MaxCols         =   6
      MaxRows         =   1
      ScrollBars      =   2  'Vertical
      TabIndex        =   3
      Top             =   1320
      Width           =   7215
   End
   Begin Line Line1 
      X1              =   120
      X2              =   7320
      Y1              =   360
      Y2              =   360
   End
   Begin Label txtafter 
      BackColor       =   &H00C0C0C0&
      Caption         =   "$999,999"
      ForeColor       =   &H00000080&
      Height          =   255
      Left            =   6480
      TabIndex        =   13
      Top             =   120
      Width           =   855
   End
   Begin Label Label3 
      BackColor       =   &H00C0C0C0&
      Caption         =   "After payoff"
      FontBold        =   0   'False
      FontItalic      =   0   'False
      FontName        =   "MS Sans Serif"
      FontSize        =   8.25
      FontStrikethru  =   0   'False
      FontUnderline   =   0   'False
      Height          =   255
      Index           =   1
      Left            =   5520
      TabIndex        =   12
      Top             =   120
      Width           =   855
   End
   Begin Label txtBefore 
      BackColor       =   &H00C0C0C0&
      Caption         =   "$999,999"
      ForeColor       =   &H00000080&
      Height          =   255
      Left            =   4440
      TabIndex        =   11
      Top             =   120
      Width           =   855
   End
   Begin Label Label3 
      BackColor       =   &H00C0C0C0&
      Caption         =   "Current cash"
      FontBold        =   0   'False
      FontItalic      =   0   'False
      FontName        =   "MS Sans Serif"
      FontSize        =   8.25
      FontStrikethru  =   0   'False
      FontUnderline   =   0   'False
      Height          =   255
      Index           =   0
      Left            =   3360
      TabIndex        =   10
      Top             =   120
      Width           =   975
   End
   Begin Label txtPlayer 
      BackColor       =   &H00C0C0C0&
      Caption         =   "txtPlayer"
      ForeColor       =   &H0000FFFF&
      Height          =   255
      Left            =   840
      TabIndex        =   9
      Top             =   120
      Width           =   2175
   End
   Begin Label Label2 
      BackColor       =   &H00C0C0C0&
      Caption         =   "Player: "
      FontBold        =   0   'False
      FontItalic      =   0   'False
      FontName        =   "MS Sans Serif"
      FontSize        =   8.25
      FontStrikethru  =   0   'False
      FontUnderline   =   0   'False
      Height          =   255
      Left            =   120
      TabIndex        =   8
      Top             =   120
      Width           =   615
   End
   Begin Label txtPlayerNum 
      Height          =   255
      Left            =   1200
      TabIndex        =   7
      Top             =   4560
      Visible         =   0   'False
      Width           =   495
   End
   Begin Label txtCash 
      Height          =   255
      Left            =   120
      TabIndex        =   6
      Top             =   4560
      Visible         =   0   'False
      Width           =   975
   End
   Begin Label Label1 
      BackColor       =   &H00C0C0C0&
      Caption         =   "If you wait and un-mortgage the property later, an additional 10% interest charge will be applied."
      FontBold        =   0   'False
      FontItalic      =   0   'False
      FontName        =   "MS Sans Serif"
      FontSize        =   8.25
      FontStrikethru  =   0   'False
      FontUnderline   =   0   'False
      Height          =   255
      Index           =   2
      Left            =   120
      TabIndex        =   2
      Top             =   960
      Width           =   7095
   End
   Begin Label Label1 
      BackColor       =   &H00C0C0C0&
      Caption         =   "If you un-mortgage the property now you will not be charged an additional 10% interest charge."
      FontBold        =   0   'False
      FontItalic      =   0   'False
      FontName        =   "MS Sans Serif"
      FontSize        =   8.25
      FontStrikethru  =   0   'False
      FontUnderline   =   0   'False
      Height          =   255
      Index           =   1
      Left            =   120
      TabIndex        =   1
      Top             =   720
      Width           =   7095
   End
   Begin Label Label1 
      BackColor       =   &H00C0C0C0&
      Caption         =   "You are receiving mortgaged property.  You are required to pay the 10% interest charge immediately."
      FontBold        =   0   'False
      FontItalic      =   0   'False
      FontName        =   "MS Sans Serif"
      FontSize        =   8.25
      FontStrikethru  =   0   'False
      FontUnderline   =   0   'False
      Height          =   255
      Index           =   0
      Left            =   120
      TabIndex        =   0
      Top             =   480
      Width           =   7095
   End
End
Option Explicit

Dim alreadyUp As Integer

Sub cmdCancel_Click ()
   Unload Me
End Sub

Sub cmdOK_Click ()
   Dim k As Integer
   Dim num As Integer
   Dim vstr As Variant
   ' calculate payoff for selected properties
   num = UnFormat(txtAfter)
   If num < 0 Then
      ErrorMsg "You don't have enough cash to pay off selected mortgages."
      Exit Sub
   End If

   ' now need to pay off mortgages
   For k = 1 To ss.MaxRows
      ss.Col = 5: ss.Row = k
      If ss.Text = "1" Then
         ' pay off mortgage
         If SpreadGetText(ss, 6, k, vstr) Then :
         properties(Val(vstr)).flagPayOff = True
'         PlaceMortgagePayOff Val(vstr), Val(txtPlayerNum)
'      Else
'         ' just charge interest
'         If SpreadGetText(ss, 4, k, vstr) Then :
'         vstr = UnFormat(vstr)
'         Transaction Val(txtPlayerNum), -vstr
      End If
   Next k
   selectData = True
   Unload Me
End Sub

Sub Form_Activate ()
   Dim k As Integer
   Dim vstr As Variant
   Dim num As Integer
   Dim totalInt As Integer, temp As Integer
   If Not alreadyUp Then
      ' caller should fill ss with property positions
      ' in col 6
      ' also, player index should be set in txtPlayerNum
      ' and players cash in txtBefore
      totalInt = 0
      For k = 1 To ss.MaxRows
         ' get property position
         If SpreadGetText(ss, 6, k, vstr) Then :
         num = Val(vstr)
         ss.Col = 1: ss.Row = k
         ss.BackColor = PropertyColor(num)
         vstr = properties(num).name
         SpreadSetText ss, 2, k, vstr
         vstr = Format(Int(properties(num).mortval * 1.1), "$###,##0")
         SpreadSetText ss, 3, k, vstr
         temp = Int(properties(num).mortval * .1)
         totalInt = totalInt + temp
         vstr = Format(temp, "$###")
         SpreadSetText ss, 4, k, vstr
      Next k
      txtBefore = Format(Val(txtCash), "$###,##0")
      txtAfter = Format(Val(txtCash) - totalInt, "$###,##0")
      txtplayer = players(Val(txtPlayerNum)).name
      selectData = 0
   End If
   alreadyUp = True
   ShowHourGlass False
End Sub

Sub Form_Load ()
   Centerform Me
   ss.MaxRows = 0
   selectData = False
   alreadyUp = False
End Sub

Sub ss_ButtonClicked (Col As Long, Row As Long, buttonDown As Integer)
   Dim num As Integer
   Dim p As Variant
   Dim i As Variant
   If Col = 5 Then
      If SpreadGetText(ss, 3, Row, p) Then :
      If SpreadGetText(ss, 4, Row, i) Then :
      i = UnFormat(i): p = UnFormat(p)
      num = UnFormat(txtAfter)
      If buttonDown Then   ' checked
         num = num + i - p
      Else
         num = num + p - i
      End If
      txtAfter = Format(num, "$###,##0")
   End If
End Sub

