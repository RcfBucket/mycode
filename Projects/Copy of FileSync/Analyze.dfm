object FrmAnalyze: TFrmAnalyze
  Left = 418
  Top = 104
  BorderStyle = bsDialog
  Caption = 'Analyzing Folders . . .'
  ClientHeight = 287
  ClientWidth = 417
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = True
  Position = poScreenCenter
  OnCloseQuery = FormCloseQuery
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Label2: TLabel
    Left = 16
    Top = 64
    Width = 51
    Height = 13
    Caption = 'Files found'
  end
  object Label3: TLabel
    Left = 16
    Top = 48
    Width = 64
    Height = 13
    Caption = 'Folders found'
  end
  object lblSrcFolders: TLabel
    Left = 104
    Top = 48
    Width = 60
    Height = 13
    Caption = 'lblSrcFolders'
  end
  object lblSrcFiles: TLabel
    Left = 104
    Top = 64
    Width = 47
    Height = 13
    Caption = 'lblSrcFiles'
  end
  object Label1: TLabel
    Left = 8
    Top = 16
    Width = 80
    Height = 13
    Caption = 'From Source :'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clNavy
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label4: TLabel
    Left = 16
    Top = 144
    Width = 51
    Height = 13
    Caption = 'Files found'
  end
  object Label5: TLabel
    Left = 16
    Top = 128
    Width = 64
    Height = 13
    Caption = 'Folders found'
  end
  object lblDstFiles: TLabel
    Left = 104
    Top = 144
    Width = 47
    Height = 13
    Caption = 'lblDstFiles'
  end
  object lblDstFolders: TLabel
    Left = 104
    Top = 128
    Width = 60
    Height = 13
    Caption = 'lblDstFolders'
  end
  object Label9: TLabel
    Left = 8
    Top = 96
    Width = 104
    Height = 13
    Caption = 'From Destination :'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clNavy
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object lblSrcCur: TEllipsisLabel
    Left = 16
    Top = 32
    Width = 393
    Height = 13
    AutoSize = False
    Caption = 'lblSrcCur'
  end
  object lblDstCur: TEllipsisLabel
    Left = 16
    Top = 112
    Width = 393
    Height = 13
    AutoSize = False
    Caption = 'lblDstCur'
  end
  object Label6: TLabel
    Left = 8
    Top = 176
    Width = 131
    Height = 13
    Caption = 'Comparing folders/files'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clNavy
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object lblCompareFile: TLabel
    Left = 16
    Top = 208
    Width = 68
    Height = 13
    Caption = 'lblCompareFile'
  end
  object lblCompareFolder: TEllipsisLabel
    Left = 16
    Top = 192
    Width = 393
    Height = 13
    AutoSize = False
    Caption = 'K:\TestFolder\BulkTrans\'
  end
  object Bevel1: TBevel
    Left = 32
    Top = 232
    Width = 353
    Height = 9
    Shape = bsTopLine
  end
  object Label7: TLabel
    Left = 160
    Top = 176
    Width = 32
    Height = 13
    Caption = 'Label7'
  end
  object lblStat: TLabel
    Left = 8
    Top = 272
    Width = 6
    Height = 13
    Caption = '--'
    Visible = False
  end
  object Label8: TLabel
    Left = 40
    Top = 272
    Width = 32
    Height = 13
    Caption = 'Label8'
    Visible = False
  end
  object BtnCancel: TBitBtn
    Left = 160
    Top = 248
    Width = 97
    Height = 27
    Caption = 'Cancel'
    ModalResult = 2
    TabOrder = 0
    Glyph.Data = {
      DE010000424DDE01000000000000760000002800000024000000120000000100
      0400000000006801000000000000000000001000000010000000000000000000
      80000080000000808000800000008000800080800000C0C0C000808080000000
      FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
      333333333333333333333333000033338833333333333333333F333333333333
      0000333911833333983333333388F333333F3333000033391118333911833333
      38F38F333F88F33300003339111183911118333338F338F3F8338F3300003333
      911118111118333338F3338F833338F3000033333911111111833333338F3338
      3333F8330000333333911111183333333338F333333F83330000333333311111
      8333333333338F3333383333000033333339111183333333333338F333833333
      00003333339111118333333333333833338F3333000033333911181118333333
      33338333338F333300003333911183911183333333383338F338F33300003333
      9118333911183333338F33838F338F33000033333913333391113333338FF833
      38F338F300003333333333333919333333388333338FFF830000333333333333
      3333333333333333333888330000333333333333333333333333333333333333
      0000}
    NumGlyphs = 2
  end
  object Timer1: TTimer
    Enabled = False
    Interval = 100
    OnTimer = Timer1Timer
    Left = 376
    Top = 8
  end
end
