object FrmMain: TFrmMain
  Left = 268
  Top = 122
  BorderIcons = [biSystemMenu, biMinimize]
  BorderStyle = bsSingle
  Caption = 'File Sync'
  ClientHeight = 292
  ClientWidth = 647
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = True
  Position = poDefault
  ShowHint = True
  OnCloseQuery = FormCloseQuery
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  OnKeyDown = FormKeyDown
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 16
    Top = 16
    Width = 66
    Height = 13
    Caption = 'Source Folder'
  end
  object Label2: TLabel
    Left = 16
    Top = 64
    Width = 85
    Height = 13
    Caption = 'Destination Folder'
  end
  object btnListSource: TSpeedButton
    Left = 424
    Top = 32
    Width = 25
    Height = 22
    Flat = True
    Glyph.Data = {
      36010000424D3601000000000000760000002800000012000000100000000100
      040000000000C000000000000000000000001000000010000000000000000000
      8000008000000080800080000000800080008080000080808000C0C0C0000000
      FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00888888888888
      8888880000008888888888877777770000008888888888000000070000008888
      8888840FBFBF0700000088888888480000000800000088888884888888888800
      00008777774888877777770000000000007777000000070000000FFFF044440F
      BFBF070000000000008788000000080000008888884878888888880000008888
      8884878777777700000088888888480000000700000088888888840FBFBF0700
      0000888888888800000008000000888888888888888888000000}
    OnClick = btnListSourceClick
  end
  object BtnListDest: TSpeedButton
    Left = 424
    Top = 80
    Width = 25
    Height = 22
    Flat = True
    Glyph.Data = {
      36010000424D3601000000000000760000002800000012000000100000000100
      040000000000C000000000000000000000001000000010000000000000000000
      8000008000000080800080000000800080008080000080808000C0C0C0000000
      FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00888888888888
      8888880000008888888888877777770000008888888888000000070000008888
      8888840FBFBF0700000088888888480000000800000088888884888888888800
      00008777774888877777770000000000007777000000070000000FFFF044440F
      BFBF070000000000008788000000080000008888884878888888880000008888
      8884878777777700000088888888480000000700000088888888840FBFBF0700
      0000888888888800000008000000888888888888888888000000}
    OnClick = BtnListDestClick
  end
  object lblDebugText: TLabel
    Left = 0
    Top = 0
    Width = 647
    Height = 13
    Align = alTop
    Alignment = taCenter
    AutoSize = False
    Caption = 'DEBUG VERSION'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clRed
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object BtnAbout: TBitBtn
    Left = 16
    Top = 256
    Width = 185
    Height = 25
    Caption = 'A&bout...'
    TabOrder = 3
    OnClick = BtnAboutClick
    Glyph.Data = {
      F6000000424DF600000000000000760000002800000010000000100000000100
      0400000000008000000000000000000000001000000000000000000000000000
      8000008000000080800080000000800080008080000080808000C0C0C0000000
      FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF0088888887B888
      888888888877B88888888888887BB88888888888877BB8888888888887BBB888
      8888888877BBB888888888887BBBB888888888877BBBB77777788887BBBBBBBB
      BFFB8877BBBBBBBFFBB8887BBBBBBFFBB888877BBBBFFBB8888887BBBFFBB888
      888877BBFBB8888888887BBBB888888888888B88888888888888}
  end
  object BtnSync: TBitBtn
    Left = 208
    Top = 256
    Width = 241
    Height = 25
    Caption = '&Analyze Files'
    Default = True
    TabOrder = 4
    OnClick = BtnSyncClick
    Glyph.Data = {
      42010000424D4201000000000000760000002800000011000000110000000100
      040000000000CC00000000000000000000001000000010000000000000000000
      BF0000BF000000BFBF00BF000000BF00BF00BFBF0000C0C0C000808080000000
      FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00777777777777
      7777700000007777777777777777700000007777747474747477700000007777
      7477777777777000000077774447777774777000000077777477777777777000
      00007777777777777477700000007770000007700000000000007000FBFB0000
      FBFB0000000070F0000000F000000000000070FFFFF070FFFFF07000000070F8
      88F070F888F07000000070FFFFF070FFFFF07000000070F888F070F888F07000
      000070FFFFF070FFFFF070000000700000007000000070000000777777777777
      777770000000}
  end
  object BtnClose: TBitBtn
    Left = 456
    Top = 256
    Width = 185
    Height = 25
    Cancel = True
    Caption = 'Close'
    TabOrder = 5
    OnClick = BtnCloseClick
    Glyph.Data = {
      42010000424D4201000000000000760000002800000011000000110000000100
      040000000000CC00000000000000000000001000000010000000000000000000
      BF0000BF000000BFBF00BF000000BF00BF00BFBF0000C0C0C000808080000000
      FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00777777777777
      777770000000777777777777777770000000777777777777770F700000007777
      0F777777777770000000777000F7777770F770000000777000F777770F777000
      00007777000F77700F777000000077777000F700F7777000000077777700000F
      7777700000007777777000F777777000000077777700000F7777700000007777
      7000F70F7777700000007770000F77700F7770000000770000F7777700F77000
      00007700F7777777700F70000000777777777777777770000000777777777777
      777770000000}
  end
  object cboSource: TComboBox
    Left = 16
    Top = 32
    Width = 401
    Height = 21
    ItemHeight = 13
    TabOrder = 0
    OnClick = cboSourceClick
    OnDblClick = cboSourceDblClick
  end
  object cboDest: TComboBox
    Left = 16
    Top = 80
    Width = 401
    Height = 21
    ItemHeight = 13
    TabOrder = 1
    OnClick = cboDestClick
    OnDblClick = cboDestDblClick
  end
  object pageOptions: TPageControl
    Left = 16
    Top = 116
    Width = 433
    Height = 133
    ActivePage = TabOptions
    TabOrder = 2
    TabStop = False
    object TabCompare: TTabSheet
      Caption = '&Compare'
      object lblVersion: TLabel
        Left = 376
        Top = 88
        Width = 45
        Height = 13
        Alignment = taRightJustify
        Caption = 'lblVersion'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clTeal
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object RdoByteOnly: TRadioButton
        Left = 8
        Top = 56
        Width = 401
        Height = 17
        Hint = 'Always perform a byte compare'
        Caption = 'Always perform byte compare'
        TabOrder = 2
      end
      object RdoDateByte: TRadioButton
        Left = 8
        Top = 32
        Width = 401
        Height = 17
        Hint = 'If files are same size but date/time differ, do a byte compare'
        Caption = 
          'Perform byte compare if files are the same size but Date/Time di' +
          'ffer'
        TabOrder = 1
      end
      object RdoDateOnly: TRadioButton
        Left = 8
        Top = 8
        Width = 401
        Height = 17
        Hint = 'Only compare files based on date/time/size'
        Caption = 'Compare Date/Time/Size Only'
        Checked = True
        TabOrder = 0
        TabStop = True
      end
      object RdoSuperSync: TRadioButton
        Left = 8
        Top = 80
        Width = 113
        Height = 17
        Hint = 'If file size/date/time are same then perform byte compare'
        Caption = 'Super Sync'
        TabOrder = 3
      end
    end
    object TabIncludeExclude: TTabSheet
      Caption = '&Include/Exclude'
      ImageIndex = 1
      object Label4: TLabel
        Left = 8
        Top = 76
        Width = 69
        Height = 13
        Caption = 'File to Exclude'
      end
      object Label5: TLabel
        Left = 8
        Top = 52
        Width = 66
        Height = 13
        Caption = 'File to Include'
      end
      object ChkSysDirs: TCheckBox
        Left = 240
        Top = 8
        Width = 153
        Height = 17
        Caption = 'Include System Folders'
        TabOrder = 1
      end
      object chkSubDirs: TCheckBox
        Left = 8
        Top = 8
        Width = 137
        Height = 17
        Caption = 'Include sub-folders'
        Checked = True
        State = cbChecked
        TabOrder = 0
      end
      object edExclude: TNubEdit
        Left = 96
        Top = 72
        Width = 321
        Height = 21
        ClearOnFirstKey = False
        NubinAlign = nubTop
        NubinGlyphColor = clBlack
        NubinWidth = 12
        NubinGlyphShape = nubCircle
        NubinEnabled = True
        WantReturns = True
        OnNubinClick = EditMaskList
        AutoSelect = True
        BorderStyle = bsSingle
        CharCase = ecNormal
        Color = clWindow
        HideSelection = True
        ImeMode = imDontCare
        MaxLength = 0
        OEMConvert = False
        PasswordChar = #0
        ReadOnly = False
        TabOrder = 3
        TabStop = True
      end
      object edInclude: TNubEdit
        Left = 96
        Top = 48
        Width = 321
        Height = 21
        ClearOnFirstKey = False
        NubinAlign = nubTop
        NubinGlyphColor = clBlack
        NubinWidth = 12
        NubinGlyphShape = nubCircle
        NubinEnabled = True
        WantReturns = True
        OnNubinClick = EditMaskList
        AutoSelect = True
        BorderStyle = bsSingle
        CharCase = ecNormal
        Color = clWindow
        HideSelection = True
        ImeMode = imDontCare
        MaxLength = 0
        OEMConvert = False
        PasswordChar = #0
        ReadOnly = False
        TabOrder = 2
        TabStop = True
        Text = '*.*'
      end
    end
    object TabOptions: TTabSheet
      Caption = '&Options'
      ImageIndex = 2
      object BtnModifyOpts: TButton
        Left = 192
        Top = 8
        Width = 105
        Height = 25
        Caption = '<< Modify'
        TabOrder = 0
        OnClick = BtnModifyOptsClick
      end
      object lbOptions: TListBox
        Left = 8
        Top = 4
        Width = 169
        Height = 97
        ItemHeight = 13
        Items.Strings = (
          'User Profiles'
          'Filters'
          'Source Compare'
          'Backup Configuration'
          'Restore Configuration')
        TabOrder = 1
        OnDblClick = lbOptionsDblClick
      end
    end
  end
  object GroupBox1: TGroupBox
    Left = 456
    Top = 24
    Width = 185
    Height = 225
    Caption = 'History Profiles'
    TabOrder = 6
    object lstProfiles: TListBox
      Left = 8
      Top = 16
      Width = 169
      Height = 201
      ItemHeight = 13
      Items.Strings = (
        'sfsdf'
        'sdfsdfds'
        'dfsd'
        'fsdf'
        'sdfds'
        'fsd')
      TabOrder = 0
      OnClick = lstProfilesClick
    end
  end
  object ExecFile1: TExecFile
    Associate = False
    CommandLine = 'EXPLORER.EXE'
    Parameters = 'C:\Windows'
    Wait = False
    Left = 152
  end
  object dropSource: TStDropFiles
    DropTarget = cboSource
    OnDropFiles = dropSourceDropFiles
    Left = 360
    Top = 40
  end
  object dropDest: TStDropFiles
    DropTarget = cboDest
    OnDropFiles = dropDestDropFiles
    Left = 360
    Top = 80
  end
  object bf: TStBrowser
    SpecialRootFolder = sfNone
    Left = 120
  end
  object ver: TStVersionInfo
    Left = 552
    Top = 200
  end
  object XPManifest1: TXPManifest
    Left = 512
    Top = 184
  end
  object OpenDialog1: TOpenDialog
    DefaultExt = 'SyncBack'
    Filter = 'SyncBack (*.SyncBack)|*.SyncBack|All Files (*.*)|*.*'
    Options = [ofHideReadOnly, ofFileMustExist, ofEnableSizing]
    Left = 516
    Top = 204
  end
  object SaveDialog1: TSaveDialog
    DefaultExt = 'SyncBack'
    Filter = 'SyncBack (*.SyncBack)|*.SyncBack|All Files (*.*)|*.*'
    Options = [ofHideReadOnly, ofPathMustExist, ofEnableSizing]
    Left = 548
    Top = 180
  end
end
