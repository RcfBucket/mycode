unit Filter;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, Registry, SyncData, Globals;

type
  TFilterOpts = record
    noChange            : boolean;
    copyFiles           : boolean;
    deleteFiles         : boolean;
    overwriteNewerFiles : boolean;
    createFolder        : boolean;
    updateFolder        : boolean;
    deleteFolder        : boolean;
    overwriteNewerFolder: boolean;
    forceNoChange       : boolean;
  end;

  TFrmFilter = class(TForm)
    grpFolders: TGroupBox;
    chkUpdate: TCheckBox;
    chkOverwriteFolder: TCheckBox;
    chkCreate: TCheckBox;
    grpFiles: TGroupBox;
    chkFileCopy: TCheckBox;
    chkFileDelete: TCheckBox;
    chkOverwriteFile: TCheckBox;
    chkDeleteFolder: TCheckBox;
    BtnOK: TBitBtn;
    BtnCancel: TBitBtn;
    GrpOther: TGroupBox;
    ChkForceNochange: TCheckBox;
    chkNoChange: TCheckBox;
    BtnFiles: TSpeedButton;
    BtnFolders: TSpeedButton;
    BtnOther: TSpeedButton;
    BtnDefaults: TBitBtn;
    procedure BtnFilesClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtnOKClick(Sender: TObject);
    procedure BtnDefaultsClick(Sender: TObject);
  private
    tmp : TFilterOpts;
    procedure SetupScreen;
  public
    { Public declarations }
  end;

function CheckFileFilter(action: TSyncAction): boolean;
function CheckFolderFilter(action: TSyncAction): boolean;
procedure ReadFilterOptsFromReg;
procedure SaveFilterOptsToReg;

var
  filterOpts    : TFilterOpts;

IMPLEMENTATION

{$R *.DFM}

procedure SetDefaults;
begin
  filterOpts.noChange:= false;
  filterOpts.copyFiles:= true;
  filterOpts.deleteFiles:= true;
  filterOpts.overwriteNewerFiles:= true;
  filterOpts.createFolder:= true;
  filterOpts.updateFolder:= true;
  filterOpts.deleteFolder:= true;
  filterOpts.overwriteNewerFolder:= true;
  filterOpts.ForceNoChange:= true;
end;

function CheckFileFilter(action: TSyncAction): boolean;
begin
  case action of
    actNoChange:
      result:= filterOpts.NoChange;
    actCopy:
      result:= filterOpts.CopyFiles;
    actDelete:
      result:= filterOpts.DeleteFiles;
    actOverwriteNewer:
      result:= filterOpts.OverwriteNewerFiles;
    actUserNochanged:
      result:= filterOpts.ForceNoChange;
    else
      raise Exception.Create('Invalid action passed to CheckFile filter');
  end;
end;

function CheckFolderFilter(action: TSyncAction): boolean;
begin
  case action of
    actNoChange:
      result:= filterOpts.NoChange;
    actUpdate:
      result:= filterOpts.UpdateFolder;
    actCreate:
      result:= filterOpts.CreateFolder;
    actDelete:
      result:= filterOpts.DeleteFolder;
    actOverwriteNewer:
      result:= filterOpts.OverwriteNewerFolder;
    actUserNoChanged:
      result:= filterOpts.forceNoChange;
    else
      raise Exception.Create('Invalid action passed to CheckFolder filter');
  end;
end;

procedure TFrmFilter.BtnFilesClick(Sender: TObject);
begin
  if sender is TSpeedButton then
  begin
    with sender as TSpeedButton do
    begin
      if tag = 1 then
        tag:= 0
      else
        tag:= 1;

      if name = 'BtnFiles' then
      begin
        chkFileCopy.checked:= tag = 1;
        chkFileDelete.checked:= tag = 1;
        chkOverwriteFile.checked:= tag = 1;
      end;
      if name = 'BtnFolders' then
      begin
        chkCreate.checked:= tag = 1;
        chkUpdate.checked:= tag = 1;
        chkDeleteFolder.checked:= tag = 1;
        chkOverwriteFolder.checked:= tag = 1;
      end;
      if name = 'BtnOther' then
      begin
        chkNoChange.checked:= tag = 1;
        ChkForceNochange.checked:= tag = 1;
      end;
    end;
  end;
end;

procedure TFrmFilter.FormShow(Sender: TObject);
begin
  if not (chkFileCopy.checked and chkFileDelete.checked and chkOverwriteFile.checked) then
    BtnFiles.Tag:= 0;

  if not (chkCreate.checked and chkUpdate.checked and chkDeleteFolder.checked and
     chkOverwriteFolder.checked) then
    BtnFolders.Tag:= 0;

  if not (chkNoChange.checked and ChkForceNochange.checked) then
    BtnOther.Tag:= 0;
end;

procedure TFrmFilter.FormCreate(Sender: TObject);
begin
  SetupScreen;
  tmp:= filterOpts;
end;

procedure TFrmFilter.SetupScreen;
begin
  chkUpdate.checked:= filterOpts.updateFolder;
  chkOverwriteFolder.checked:= filterOpts.overwriteNewerFolder;
  chkForceNoChange.checked:= filterOpts.forceNoChange;
  chkCreate.checked:= filterOpts.createFolder;
  chkFileCopy.checked:= filterOpts.copyFiles;
  chkFileDelete.checked:= filterOpts.deleteFiles;
  chkOverwriteFile.checked:= filterOpts.overwriteNewerFiles;
  chkDeleteFolder.checked:= filterOpts.deleteFolder;
  chkNoChange.checked:= filterOpts.noChange;
end;

procedure TFrmFilter.BtnOKClick(Sender: TObject);
begin
  filterOpts.updateFolder:= chkUpdate.checked;
  filterOpts.overwriteNewerFolder:= chkOverwriteFolder.checked;
  filterOpts.ForceNoChange:= chkForceNoChange.checked;
  filterOpts.createFolder:= chkCreate.checked;
  filterOpts.copyFiles:= chkFileCopy.checked;
  filterOpts.deleteFiles:= chkFileDelete.checked;
  filterOpts.overwriteNewerFiles:= chkOverwriteFile.checked;
  filterOpts.deleteFolder:= chkDeleteFolder.checked;
  filterOpts.noChange:= chkNoChange.checked;
  if not CompareMem(@tmp, @filterOpts, SizeOf(TFilterOpts)) then
    ModalResult:= mrOK
  else
    ModalResult:= mrCancel;
end;

procedure SaveFilterOptsToReg;
// save option to the registry
var
  r   : TRegistry;
begin
  r:= TRegistry.Create;
  r.OpenKey(SyncRegKeyOptionsBase + 'Filters', true);
  r.WriteBool('NoChange', filterOpts.NoChange);
  r.WriteBool('CopyFiles', filterOpts.CopyFiles);
  r.WriteBool('DeleteFiles', filterOpts.DeleteFiles);
  r.WriteBool('OverwriteNewerFiles', filterOpts.OverwriteNewerFiles);
  r.WriteBool('CreateFolder', filterOpts.CreateFolder);
  r.WriteBool('UpdateFolder', filterOpts.UpdateFolder);
  r.WriteBool('DeleteFolder', filterOpts.DeleteFolder);
  r.WriteBool('OverwriteNewerFolder', filterOpts.OverwriteNewerFolder);
  r.WriteBool('ForceNoChange', filterOpts.ForceNoChange);
  r.CloseKey;
  r.Free;
end;

procedure ReadFilterOptsFromReg;
var
  r   : TRegistry;
  vals: TStringList;
  k   : integer;
begin
  r:= TRegistry.Create;
  try
    if r.OpenKey(SyncRegKeyOptionsBase + 'Filters', true) then
    begin
      vals:= TStringList.Create;
      try
        r.GetValueNames(vals);
        for k:= 0 to vals.Count - 1 do
        begin
          try
            if UpperCase(vals[k]) = 'NOCHANGE' then
              filterOpts.NoChange:= r.ReadBool(vals[k])
            else if UpperCase(vals[k]) = 'COPYFILES' then
              filterOpts.CopyFiles:= r.ReadBool(vals[k])
            else if UpperCase(vals[k]) = 'DELETEFILES' then
              filterOpts.DeleteFiles:= r.ReadBool(vals[k])
            else if UpperCase(vals[k]) = 'OVERWRITENEWERFILES' then
              filterOpts.OverwriteNewerFiles:= r.ReadBool(vals[k])
            else if UpperCase(vals[k]) = 'CREATEFOLDER' then
              filterOpts.CreateFolder:= r.ReadBool(vals[k])
            else if UpperCase(vals[k]) = 'UPDATEFOLDER' then
              filterOpts.UpdateFolder:= r.ReadBool(vals[k])
            else if UpperCase(vals[k]) = 'DELETEFOLDER' then
              filterOpts.DeleteFolder:= r.ReadBool(vals[k])
            else if UpperCase(vals[k]) = 'OVERWRITENEWERFOLDER' then
              filterOpts.OverwriteNewerFolder:= r.ReadBool(vals[k])
            else if UpperCase(vals[k]) = 'FORCENOCHANGE' then
              filterOpts.ForceNoChange:= r.ReadBool(vals[k])
          except
          end;
        end;
      finally
        vals.Free;
      end;
    end;
    r.CloseKey;
  finally
    r.Free;
  end;
end;

procedure TFrmFilter.BtnDefaultsClick(Sender: TObject);
begin
  SetDefaults;
  SetupScreen;
end;

INITIALIZATION
  SetDefaults;
  ReadFilterOptsFromReg;
FINALIZATION
  SaveFilterOptsToReg;
END.
