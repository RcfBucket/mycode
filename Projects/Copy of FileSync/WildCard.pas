unit WildCard;

{- unit for wildcard support }

{- TMaskObj is the object used for maintaining mask.

 It is initialized with a mask string which can contain several masks
   separated with commas (ie, '*.EXE,*.COM,*.PAS').

    A mask string is any string that contains up to [MaxMasks] wild cards.
      Each wildcard separated with a comma.

    constructor Init(mStr: TMaskString);
      - Initialize the Mask Object with the specified mask string.
    If no mask string is specified ('') then *.* will be assumed.

  destructor Done; virtual;
  - destroy the object.

  function IsMatch(fname: String): Boolean;
  - Return TRUE if specified file name matches the current list of
    wild cards in the object.

  function NumMasks: Integer;
  - return the number of masks in the current object.

  procedure Change(mStr: TMaskString);
  - Change the current list of masks. mStr can contain up to MaxMasks
    wild cards (see above).

  function MaskString: TMaskString;
  - Return list of current masks in string format.
}

interface

uses
  SysUtils, Classes, StStrL, Masks;

type
  TFileMasks = class
  private
    fMasks: TStringList;
    fBlankMaskMatchesAll: boolean;
    function GetMask(idx: integer): string;
    procedure SetMask(idx: integer; const Value: string);
    function GetMaskStr: string;
    procedure SetMaskStr(const Value: string);
  public
    constructor Create;
    destructor Destroy; override;

    procedure Add(mStr: string);
    function IsMatch(aName: string): boolean;
    function Count: Integer;
    procedure Remove(idx: integer);
    procedure Replace(idx: integer; aValue: string);
    property Mask[idx: integer]: string read GetMask write SetMask; default;
    property MaskString: string read GetMaskStr write SetMaskStr;
    property BlankMaskMatchesAll: boolean read fBlankMaskMatchesAll write fBlankMaskMatchesAll;
  end;

implementation

constructor TFileMasks.Create;
begin
  inherited;
  fBlankMaskMatchesAll:= false;
  fMasks:= TStringList.Create;
  fMasks.CaseSensitive:= false;
end;

destructor TFileMasks.Destroy;
begin
  fMasks.Free;
  inherited;
end;

function TFileMasks.IsMatch(aName: string): Boolean;
{- return true if fname matches current wildcard list }
var
  k: Integer;
  matched: boolean;
begin
  matched:= false;
  if (Count = 0) and (BlankMaskMatchesAll) then
    matched:= true
  else
  begin
    k:= 0;
    while not matched and (k < Count) do
    begin
      if Pos('.', aName) = 0 then
        aName:= aName + '.';
      matched:= MatchesMask(aName, fmasks[k]);
      Inc(k);
    end;
  end;
  result:= matched;
end;

procedure TFileMasks.Add(mStr: string);
begin
  if fMasks.IndexOf(mStr) < 0 then
    fMasks.Add(mStr);
end;

function TFileMasks.Count: Integer;
begin
  result:= fMasks.Count;
end;

function TFileMasks.GetMask(idx: integer): string;
begin
  if (idx >= 0) and (idx < Count) then
    result:= fMasks[idx]
  else
    result:= '';
end;

procedure TFileMasks.SetMask(idx: integer; const Value: string);
begin
  if (idx >= 0) and (idx < Count) then
    fMasks[idx]:= value;
end;

function TFileMasks.GetMaskStr: string;
begin
  result:= fMasks.CommaText;
end;

procedure TFileMasks.SetMaskStr(const Value: string);
begin
  fMasks.CommaText:= value;
end;

procedure TFileMasks.Remove(idx: integer);
begin
  if (idx >= 0) and (idx < fMasks.Count) then
    fMasks.Delete(idx);
end;

procedure TFileMasks.Replace(idx: integer; aValue: string);
var
  foundIdx: integer;
begin
  if (idx >= 0) and (idx < fMasks.Count) then
  begin
    foundIdx:= fMasks.IndexOf(aValue);
    if (foundIdx < 0) or (foundIdx = idx) then
      fMasks[idx]:= aValue;
  end;
end;

end.

