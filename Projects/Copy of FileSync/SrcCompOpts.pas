unit SrcCompOpts;

interface


uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, ComCtrls, SCM, Registry, Globals;

type
  TSCMOptions = record
    charsToIgnore     : string;
    ConvertTabs       : boolean;
    IgnoreCase        : boolean;
    IgnoreWhiteSpace  : boolean;
    minLinesForMatch  : integer;
    ShowFileInfo      : boolean;
    TabSize           : integer;
    UseLineNumbers    : boolean;
    Width             : integer;
    optimize          : boolean;
    fontName          : string;
    fontSize          : integer;
  end;

  TFrmSrcCompOpts = class(TForm)
    Label1: TLabel;
    Label3: TLabel;
    ChkConvertTabs: TCheckBox;
    ChkIgnoreWS: TCheckBox;
    ChkDispLineNums: TCheckBox;
    edCharsToIgnore: TEdit;
    edTabSize: TEdit;
    udTabSize: TUpDown;
    BtnCancel: TBitBtn;
    BtnOK: TBitBtn;
    edMinLines: TEdit;
    udMinLines: TUpDown;
    Label2: TLabel;
    ChkShowFileInfo: TCheckBox;
    ChkCaseSensitive: TCheckBox;
    ChkOptimize: TCheckBox;
    BtnDefaults: TBitBtn;
    Label4: TLabel;
    edDispWidth: TEdit;
    udDispWidth: TUpDown;
    BtnFont: TBitBtn;
    FontDialog1: TFontDialog;
    procedure FormCreate(Sender: TObject);
    procedure BtnOKClick(Sender: TObject);
    procedure BtnDefaultsClick(Sender: TObject);
    procedure BtnFontClick(Sender: TObject);
  private
    procedure SetupScreen;
  public
    { Public declarations }
  end;

procedure SetSCMOptions(aSCM: TSCM);
procedure SaveSCMOptsToReg;
procedure ReadSCMOptsFromReg;
procedure SetSrcCompFont(aFont: TFont);

implementation

{$R *.DFM}

var
  scmOpts   : TSCMOptions;

procedure SetSrcCompFont(aFont: TFont);
begin
  aFont.Name:= scmOpts.fontName;
  aFont.Size:= scmOpts.fontSize;
end;

procedure SetDefaults;
begin
  scmOpts.charsToIgnore:= '';
  scmOpts.ConvertTabs:= true;
  scmOpts.IgnoreCase:= true;
  scmOpts.IgnoreWhiteSpace:= false;
  scmOpts.minlinesformatch:= 3;
  scmOpts.ShowFileInfo:= true;
  scmOpts.TabSize:= 3;
  scmOpts.UseLineNumbers:= true;
  scmOpts.Width:= 128;
  scmOpts.optimize:= true;
  scmOpts.FontName:= 'Lucida Console';
  scmOpts.fontSize:= 10;
end;

procedure SetSCMOptions(aSCM: TSCM);
begin
  aSCM.charsToIgnore     := scmOpts.charsToIgnore;
  aSCM.ConvertTabs       := scmOpts.ConvertTabs;
  aSCM.IgnoreCase        := scmOpts.IgnoreCase;
  aSCM.IgnoreWhiteSpace  := scmOpts.IgnoreWhiteSpace;
  aSCM.minLinesForMatch  := scmOpts.minlinesformatch;
  aSCM.ShowFileInfo      := scmOpts.ShowFileInfo;
  aSCM.TabSize           := scmOpts.TabSize;
  aSCM.UseLineNumbers    := scmOpts.UseLineNumbers;
  aSCM.DisplayWidth      := scmOpts.Width;
  aSCM.optimize          := scmOpts.optimize;
end;

procedure TFrmSrcCompOpts.FormCreate(Sender: TObject);
begin
  SetupScreen;
end;

procedure TFrmSrcCompOpts.SetupScreen;
// extract scm options from structure to screen
begin
  edCharsToIgnore.Text:= scmOpts.CharsToIgnore;
  udTabSize.Position:= scmOpts.TabSize;
  udMinLines.Position:= scmOpts.MinLinesForMatch;
  udDispWidth.Position:= scmOpts.Width;
  ChkShowFileInfo.Checked:= scmOpts.ShowFileInfo;
  ChkConvertTabs.Checked:= scmOpts.ConvertTabs;
  ChkIgnoreWS.Checked:= scmOpts.IgnoreWhiteSpace;
  ChkCaseSensitive.Checked:= not scmOpts.ignorecase;
  ChkDispLineNums.checked:= scmOpts.UseLineNumbers;
  ChkOptimize.checked:= scmOpts.optimize;
  FontDialog1.Font.Name:= scmOpts.fontName;
  FontDialog1.Font.Size:= scmOpts.fontSize;
end;

procedure TFrmSrcCompOpts.BtnOKClick(Sender: TObject);
begin
  scmOpts.ShowFileInfo:= chkShowFileInfo.checked;
  scmOpts.CharsToIgnore:= edCharsToIgnore.Text;
  scmOpts.TabSize:= udTabSize.Position;
  scmOpts.MinLinesForMatch:= udMinLines.Position;
  scmOpts.ConvertTabs:= ChkConvertTabs.Checked;
  scmOpts.IgnoreWhiteSpace:= ChkIgnoreWS.Checked;
  scmOpts.IgnoreCase:= not ChkCaseSensitive.Checked;
  scmOpts.UseLineNumbers:= ChkDispLineNums.checked;
  scmOpts.Optimize:= ChkOptimize.checked;
  scmOpts.Width:= udDispWidth.Position;
  scmOpts.fontName:= FontDialog1.Font.Name;
  scmOpts.fontSize:= FontDialog1.Font.Size; 
  ModalResult:= mrOK;
end;

procedure SaveSCMOptsToReg;
// save option to the registry
var
  r   : TRegistry;
begin
  r:= TRegistry.Create;
  r.OpenKey(SyncRegKeyOptionsBase + 'SourceCompare', true);
  r.WriteString('CharsToIgnore', scmOpts.charsToIgnore);
  r.WriteBool('ConvertTabs', scmOpts.ConvertTabs);
  r.WriteBool('IgnoreCase', scmOpts.IgnoreCase);
  r.WriteBool('IgnoreWhiteSpace', scmOpts.IgnoreWhiteSpace);
  r.WriteInteger('MinLinesForMatch', scmOpts.minLinesForMatch);
  r.WriteBool('ShowFileInfo', scmOpts.ShowFileInfo);
  r.WriteInteger('TabSize', scmOpts.TabSize);
  r.WriteBool('UseLineNumbers', scmOpts.UseLineNumbers);
  r.WriteInteger('Width', scmOpts.Width);
  r.WriteBool('Optimize', scmOpts.Optimize);
  r.WriteString('FontName', scmOpts.fontName);
  r.WriteInteger('FontSize', scmOpts.fontSize);
  r.CloseKey;
  r.Free;
end;

procedure ReadSCMOptsFromReg;
var
  r   : TRegistry;
  vals: TStringList;
  k   : integer;
  keyName: string;
begin
  r:= TRegistry.Create;
  try
    if r.OpenKey(SyncRegKeyOptionsBase + 'SourceCompare', true) then
    begin
      vals:= TStringList.Create;
      try
        r.GetValueNames(vals);
        for k:= 0 to vals.Count - 1 do
        begin
          keyName:= UpperCase(vals[k]);
          try
            if keyName = 'CHARSTOIGNORE' then
              scmOpts.CharsToIgnore:= r.ReadString(vals[k])
            else if keyName = 'CONVERTTABS' then
              scmOpts.ConvertTabs:= r.ReadBool(vals[k])
            else if keyName = 'IGNORECASE' then
              scmOpts.IgnoreCase:= r.ReadBool(vals[k])
            else if keyName = 'IGNOREWHITESPACE' then
              scmOpts.IgnoreWhiteSpace:= r.ReadBool(vals[k])
            else if keyName = 'MINLINESFORMATCH' then
              scmOpts.minLinesForMatch:= r.ReadInteger(vals[k])
            else if keyName = 'SHOWFILEINFO' then
              scmOpts.ShowFileInfo:= r.ReadBool(vals[k])
            else if keyName = 'TABSIZE' then
              scmOpts.TabSize:= r.ReadInteger(vals[k])
            else if keyName = 'USELINENUMBERS' then
              scmOpts.UselineNumbers:= r.ReadBool(vals[k])
            else if keyName = 'WIDTH' then
              scmOpts.Width:= r.ReadInteger(vals[k])
            else if keyName = 'OPTIMIZE' then
              scmOpts.optimize:= r.ReadBool(vals[k])
            else if keyName = 'FONTNAME' then
              scmOpts.fontName:= r.ReadString(keyName)
            else if keyName = 'FONTSIZE' then
              scmOpts.fontSize:= r.ReadInteger(keyName);
          except
            // eat up reg exception
          end;
        end;
      finally
        vals.Free;
      end;
    end;
    r.CloseKey;
  finally
    r.Free;
  end;
end;

procedure TFrmSrcCompOpts.BtnDefaultsClick(Sender: TObject);
begin
  SetDefaults;
  SetupScreen;
end;

procedure TFrmSrcCompOpts.BtnFontClick(Sender: TObject);
begin
  fontDialog1.Execute;
end;

initialization
  SetDefaults;
  ReadSCMOptsFromReg;
finalization
  SaveSCMOptsToReg;
end.
