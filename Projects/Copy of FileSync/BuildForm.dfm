object FrmBuild: TFrmBuild
  Left = 354
  Top = 142
  ActiveControl = ChkCopy
  BorderIcons = []
  BorderStyle = bsNone
  ClientHeight = 218
  ClientWidth = 203
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = True
  Position = poScreenCenter
  ShowHint = True
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 203
    Height = 218
    Align = alClient
    BevelInner = bvLowered
    BevelWidth = 3
    BorderWidth = 2
    TabOrder = 0
    object Label1: TLabel
      Left = 16
      Top = 14
      Width = 96
      Height = 13
      Caption = 'Building copy list . . .'
    end
    object Label2: TLabel
      Left = 123
      Top = 14
      Width = 62
      Height = 13
      Alignment = taRightJustify
      AutoSize = False
      Caption = '0'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label3: TLabel
      Left = 16
      Top = 54
      Width = 99
      Height = 13
      Caption = 'Building delete list . . '
      Visible = False
    end
    object Label4: TLabel
      Left = 123
      Top = 54
      Width = 62
      Height = 13
      Alignment = taRightJustify
      AutoSize = False
      Caption = '0'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      Visible = False
    end
    object Label7: TLabel
      Left = 24
      Top = 30
      Width = 84
      Height = 13
      Caption = 'Empty Folders . . .'
    end
    object Label8: TLabel
      Left = 123
      Top = 30
      Width = 62
      Height = 13
      Alignment = taRightJustify
      AutoSize = False
      Caption = '0'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object BtnYes: TButton
      Left = 16
      Top = 176
      Width = 73
      Height = 25
      Caption = '&Continue'
      Default = True
      ModalResult = 6
      TabOrder = 3
    end
    object BtnNo: TButton
      Left = 112
      Top = 176
      Width = 73
      Height = 25
      Cancel = True
      Caption = '&Stop'
      ModalResult = 7
      TabOrder = 4
    end
    object ChkCopy: TCheckBox
      Left = 16
      Top = 88
      Width = 97
      Height = 17
      Caption = 'Perform Copy'
      Checked = True
      State = cbChecked
      TabOrder = 0
    end
    object ChkDelete: TCheckBox
      Left = 16
      Top = 104
      Width = 97
      Height = 17
      Caption = 'Perform Delete'
      Checked = True
      State = cbChecked
      TabOrder = 1
    end
    object chkCreate: TCheckBox
      Left = 16
      Top = 120
      Width = 121
      Height = 17
      Caption = 'Create empty folders'
      Checked = True
      State = cbChecked
      TabOrder = 2
    end
    object BtnShowIt: TButton
      Left = 16
      Top = 144
      Width = 169
      Height = 25
      Caption = 'View Task Lists'
      TabOrder = 5
      OnClick = BtnShowItClick
    end
  end
end
