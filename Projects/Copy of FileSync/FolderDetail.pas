unit FolderDetail;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Grids, EllipsisLabel, StdCtrls, ExtCtrls, Buttons;

type
  TFrmFolderDetail = class(TForm)
    Source: TLabel;
    Destination: TLabel;
    LblSource: TEllipsisLabel;
    LblDest: TEllipsisLabel;
    Label1: TLabel;
    Label2: TLabel;
    label26: TLabel;
    Label4: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    LblSrcTotal: TLabel;
    lblSrcCopied: TLabel;
    lblSrcNewer: TLabel;
    Label17: TLabel;
    Label18: TLabel;
    Label19: TLabel;
    BitBtn1: TBitBtn;
    Label10: TLabel;
    LblDstTotal: TLabel;
    Label23: TLabel;
    LblDstDelete: TLabel;
    Label25: TLabel;
    LblDstNewer: TLabel;
    Bevel1: TBevel;
    Label14: TLabel;
    LblAction: TLabel;
    Label5: TLabel;
    Label20: TLabel;
    lblUnchanged: TLabel;
    LblForced: TLabel;
    Label21: TLabel;
    Label11: TLabel;
    Label15: TLabel;
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    procedure SetNum(aLbl: TLabel; aNum: integer);
  end;

var
  FrmFolderDetail: TFrmFolderDetail;

implementation

{$R *.DFM}

procedure TFrmFolderDetail.SetNum(aLbl: TLabel; aNum: integer);
begin
  aLbl.Caption:= FormatFloat('#,##0', aNum);
end;

procedure TFrmFolderDetail.FormCreate(Sender: TObject);
begin
  lblSource.Caption:= '- - -';
  lblDest.Caption:= '- - -';
  lblSrcTotal.caption:= '0';
  lblSrcCopied.caption:= '0';
  lblSrcNewer.caption:= '0';
  lblUnchanged.caption:= '0';
  lblForced.caption:= '0';
  lblDstTotal.caption:= '0';
  lblDstDelete.caption:= '0';
  lblDstNewer.caption:= '0';
end;

end.
