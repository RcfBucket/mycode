unit Main;

INTERFACE

{ Design: Simple, yet complex...

  Takes files from a source folder and syncs up with a destination folder.
  C:\ -> D:\  would make D: sync to C:

  Options:    Delete from destination
              Sync SubFolders
              Byte compare if date/times are different
              Force byte compare

  Get source folder.
  Get destination folder
  Read all files/dirs in source and dest.
  for all folders in src
    if not folder exists in dest
      mark for copy
  for all folders in dst
    if not exist in src then
      mark for delete
  for every file in src,
    if not exists in dst
      set COPY(src)
    else if exists then
      if not compare file then
        set copy(src)
  if delete from dst then
    for every file in dst
      if file not in src then
        set delete(dst)

  Show Report
  Perform operations on src
  Perform operations on dst
}

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, ToolWin, ComCtrls, ExtCtrls, Buttons, FileNameEd, FileCtrl,
  Syncdata, Registry, Menus, Globals, Execfile, MaskEdit,
  StBase, StDrop, NubEdit, SsBase, StBrowsr, StVInfo, XPMan,

  Analyze, Results, Filter, About, SrcCompOpts, ProfileEditor;

type
  TFrmMain = class(TForm)
    Label1: TLabel;
    Label2: TLabel;
    btnListSource: TSpeedButton;
    BtnListDest: TSpeedButton;
    BtnSync: TBitBtn;
    BtnClose: TBitBtn;
    ExecFile1: TExecFile;
    lblDebugText: TLabel;
    cboSource: TComboBox;
    cboDest: TComboBox;
    BtnAbout: TBitBtn;
    pageOptions: TPageControl;
    TabCompare: TTabSheet;
    TabIncludeExclude: TTabSheet;
    TabOptions: TTabSheet;
    RdoByteOnly: TRadioButton;
    RdoDateByte: TRadioButton;
    RdoDateOnly: TRadioButton;
    ChkSysDirs: TCheckBox;
    chkSubDirs: TCheckBox;
    dropSource: TStDropFiles;
    dropDest: TStDropFiles;
    RdoSuperSync: TRadioButton;
    lblVersion: TLabel;
    Label4: TLabel;
    edExclude: TNubEdit;
    bf: TStBrowser;
    ver: TStVersionInfo;
    XPManifest1: TXPManifest;
    edInclude: TNubEdit;
    Label5: TLabel;
    OpenDialog1: TOpenDialog;
    SaveDialog1: TSaveDialog;
    GroupBox1: TGroupBox;
    lstProfiles: TListBox;
    BtnModifyOpts: TButton;
    lbOptions: TListBox;
    procedure BtnCloseClick(Sender: TObject);
    procedure btnListSourceClick(Sender: TObject);
    procedure BtnListDestClick(Sender: TObject);
    procedure BtnSyncClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure BtnExplorerClick(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure BtnAboutClick(Sender: TObject);
    procedure cboSourceClick(Sender: TObject);
    procedure cboDestClick(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure FormShow(Sender: TObject);
    procedure lstProfilesClick(Sender: TObject);
    procedure dropSourceDropFiles(Sender: TObject; Point: TPoint);
    procedure dropDestDropFiles(Sender: TObject; Point: TPoint);
    procedure EditMaskList(Sender: TObject);
    procedure cboSourceDblClick(Sender: TObject);
    procedure cboDestDblClick(Sender: TObject);
    procedure BtnBackupClick(Sender: TObject);
    procedure BtnRestoreClick(Sender: TObject);
    procedure BtnModifyOptsClick(Sender: TObject);
    procedure lbOptionsDblClick(Sender: TObject);
  private
    sync  : TSync;
    procedure ProfilesEdit;
    procedure SourceCompareEdit;
    procedure MoveToTop(aStr: string; aCbo: TComboBox);
    function GetCompareType: TSyncCompareType;
    procedure SaveAllOptions;
    procedure LoadAllOptions;
    procedure SaveSettings;
    procedure ReadSettings;
    procedure AddFolderToList(aCbo: TComboBox; aHistProfile: TStringList);
    procedure FiltersEdit;
  public
    function ValidateFolder(ed: TComboBox; errMsgStr: string; allowCreateFolder: boolean): boolean;
  end;

const
  ListFolderKey = 'F3';

  optionTagUserProfile   = 1;
  optionTagFilters       = 2;
  optionTagSourceCompare = 3;
  optionTagBackup        = 4;
  optionTagRestore       = 5;

var
  FrmMain: TFrmMain;

IMPLEMENTATION

uses Math;

{$R *.DFM}

procedure TFrmMain.BtnCloseClick(Sender: TObject);
begin
  Close;
end;

procedure TFrmMain.btnListSourceClick(Sender: TObject);
begin
  bf.Caption:= 'Source Folder';
  bf.SelectedFolder:= cboSource.text;
  if bf.Execute then
    cboSource.text:= bf.SelectedFolder;
  cboSource.SetFocus;
end;

procedure TFrmMain.BtnListDestClick(Sender: TObject);
begin
  bf.Caption:= 'Destination Folder';
  bf.SelectedFolder:= cboDest.text;
  if bf.Execute then
    cboDest.text:= bf.SelectedFolder;
  cboDest.SetFocus;
end;

function TFrmMain.ValidateFolder(ed: TComboBox; errMsgStr: string; allowCreateFolder: boolean): boolean;
var
  tstr  : string;
  sRec : TSearchRec;
begin
  tstr:= ed.text;
  ed.SetFocus;
  result:= false;
  if tstr = '' then
  begin
    MessageDlg('Folder name cannot be blank.', mtError, [mbOK], 0);
    Exit;
  end;
  tstr:= AppendToPath(tstr, '*.*');
  try
    if FindFirst(tstr, faAnyFile, sRec) <> 0 then
    begin
      MessageDlg(errMsgStr, mtError, [mbOK], 0);
      result:= false
    end
    else
      result:= true;
  finally
    FindClose(sRec);
  end;
end;

function TFrmMain.GetCompareType: TSyncCompareType;
begin
  if rdoDateOnly.checked then
    result:= syncDateTime
  else if rdoDateByte.checked then
    result:= syncDateByte
  else if RdoByteOnly.Checked then
    result:= syncByteOnly
  else
    result:= syncSuper;
end;

procedure TFrmMain.AddFolderToList(aCbo: TComboBox; aHistProfile: TStringList);
// add a folder name to the combo box history
begin
  if aHistProfile.IndexOf(aCbo.Text) < 0 then
  begin
    aHistProfile.Insert(0, aCbo.Text);
    aCbo.Items.Assign(aHistProfile);
  end;
(*  tstr:= Trim(UpperCase(aCbo.Text));
  if tstr <> '' then
  begin
    found:= false;
    k:= 0;
    while not found and (k < aCbo.Items.Count) do
    begin
      found:= UpperCase(aCbo.Items[k]) = tstr;
      Inc(k);
    end;
    if not found then
      aCbo.Items.Insert(0, aCbo.text);
  end;*)
end;

procedure TFrmMain.BtnSyncClick(Sender: TObject);
var
  okToContinue : boolean;
begin
{  if ValidateFolder(cboSource, 'Invalid Source Folder.', false) and
     ValidateFolder(cboDest, 'Invalid Destination Folder.', true) then}
  begin
    // Add newly typed in folder to history list
    AddFolderToList(cboSource, profiles.SrcHistory[profiles.CurrentProfileIdx]);
    AddFolderToList(cboDest, profiles.DstHistory[profiles.CurrentProfileIdx]);

    sync.Clear;
    if edInclude.Text = '' then
    begin
      edInclude.Text:= '*.*';
    end;

    sync.CompareType:= GetCompareType;

    sync.Source.BaseFolder:= cboSource.text;
    sync.Source.ProcessSubFolders:= chkSubDirs.checked;
    sync.Source.ProcessSystemFolders:= chkSysDirs.Checked;
    sync.Source.IncludeMasks.MaskString:= edInclude.Text;
    sync.Source.ExcludeMasks.MaskString:= edExclude.Text;

    sync.Dest.BaseFolder:= cboDest.text;
    sync.Dest.ProcessSubFolders:= chkSubDirs.checked;
    sync.Dest.ProcessSystemFolders:= chkSysDirs.Checked;
    sync.Dest.IncludeMasks.MaskString:= edInclude.Text;
    sync.Dest.ExcludeMasks.MaskString:= edExclude.Text;

    okToContinue:= false;
    with TFrmAnalyze.Create(application) do
    begin
      sync:= self.Sync;
      try
        if ShowModal = mrOK then
          okToContinue:= true;
      finally
        Free;
      end;
    end;

    if okToContinue then
    begin
      with TFrmResults.Create(application) do
      begin
        sync:= self.sync;
        try
          ShowModal;
        finally
          Free;
        end;
      end;
    end;
  end;
end;

procedure TFrmMain.FormCreate(Sender: TObject);
var
  tstr  : string;
begin
  edExclude.Clear;
  edExclude.Text:= '';
  edInclude.Text:= '*.*';

  lstProfiles.Items.Assign(profiles.Profiles);
  lstProfiles.ItemIndex:= profiles.CurrentProfileIdx;
  lstProfilesClick(nil);

  tstr:= ShortCutToText(TextToShortCut(ListFolderKey));
  btnListSource.Hint:= Format('List Folders (%s)', [tstr]);
  btnListDest.Hint:= Format('List Folders (%s)', [tstr]);

  sync:= TSync.Create;
  pageOptions.ActivePage:= TabCompare;
  LoadAllOptions;
  cboSource.ItemIndex:= 0;
  cboDest.ItemIndex:= 0;

  lbOptions.Clear;
  lbOptions.Items.AddObject('History Profiles', TObject(optionTagUserProfile));
  lbOptions.Items.AddObject('Filters', TObject(optionTagFilters));
  lbOptions.Items.AddObject('Source Compare', TObject(optionTagSourceCompare));
  lbOptions.Items.AddObject('Backup Configuration', TObject(optionTagBackup));
  lbOptions.Items.AddObject('Restore Configuration', TObject(optionTagRestore));
  lbOptions.ItemIndex:= 0;
  
{$IFNDEF DEBUG}
  lblDebugText.visible:= false;
{$ENDIF}
end;

procedure TFrmMain.FormDestroy(Sender: TObject);
begin
  sync.Free;
end;

procedure TFrmMain.BtnExplorerClick(Sender: TObject);
begin
(*  ExecFile1.CommandLine:= 'EXPLORER.EXE';
  ExecFile1.Parameters:= '/n,/e,/select,C:\';
  ExecFile1.Execute;*)
end;

procedure TFrmMain.SourceCompareEdit;
begin
  with TFrmSrcCompOpts.Create(self) do
  begin
    ShowModal;
    Free;
  end;
end;

procedure TFrmMain.FiltersEdit;
begin
  with TFrmFilter.Create(self) do
  begin
    ShowModal;
    Free;
  end;
end;

procedure TFrmMain.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  k   : TShortCut;
begin
  k:= ShortCut(key, shift);
  if k = TextToShortCut(ListFolderKey) then
  begin
    if activeControl = cboSource then
      BtnListSource.Click
    else if activeControl = cboDest then
      BtnListDest.Click
    else if ActiveControl.Parent = edInclude then
      edInclude.NubinClick
    else if ActiveControl.Parent = edExclude then
      edExclude.NubinClick;
    key:= 0;
  end;
end;

procedure TFrmMain.BtnAboutClick(Sender: TObject);
begin
  with TFrmAbout.Create(application) do
  begin
    ShowModal;
    Free;
  end;
end;

procedure TFrmMain.MoveToTop(aStr: string; aCbo: TComboBox);
// move the specified string to the top of the combo list.
var
  idx   : integer;
begin
  if aCbo.Items.Count > 0 then
  begin
    idx:= aCbo.Items.IndexOf(aStr);
    if idx > 0 then
    begin
      aCbo.Items.Move(idx, 0);
      aCbo.ItemIndex:= 0;
    end;
  end;
end;

procedure TFrmMain.cboSourceClick(Sender: TObject);
begin
  MoveToTop(cboSource.Text, cboSource);
end;

procedure TFrmMain.cboDestClick(Sender: TObject);
begin
  MoveToTop(cboDest.Text, cboDest);
end;

procedure TFrmMain.SaveSettings;
var
  r   : TRegistry;
begin
  r:= TRegistry.Create;
  try
    r.OpenKey(SyncRegKeyOptionsBase + 'Options', true);
    r.WriteInteger('Compare', Ord(GetCompareType));
    r.WriteBool('IncludeSubFolders', chkSubDirs.Checked);
    r.WriteBool('IncludeSysFolders', chkSysDirs.Checked);
    r.WriteString('IncludeFiles', edInclude.Text);
    r.WriteString('ExcludeFiles', edExclude.Text);
    r.CloseKey;
  finally
    r.Free;
  end;
end;

procedure TFrmMain.ReadSettings;
var
  r   : TRegistry;
  vals: TStringList;
  c   : TSyncCompareType;
  k   : integer;
  keyName: string;
begin
  r:= TRegistry.Create;
  try
    if r.OpenKey(SyncRegKeyOptionsBase + 'Options', false) then
    begin
      vals:= TStringList.Create;
      try
        r.GetValueNames(vals);
        for k:= 0 to vals.Count - 1 do
        begin
          try
            keyName:= UpperCase(vals[k]);
            if keyName = 'COMPARE' then
            begin
              c:= TSyncCompareType(r.ReadInteger(keyName));
              if (c >= Low(TSyncCompareType)) and (c <= High(TSyncCompareType)) then
              begin
                rdoDateOnly.Checked:= c = syncDateTime;
                rdoDateByte.Checked:= c = syncDateByte;
                rdoByteOnly.Checked:= c = syncByteOnly;
                rdoSuperSync.Checked:= c = syncSuper;
              end;
            end
            else if keyName = 'INCLUDESUBFOLDERS' then
              chkSubDirs.Checked:= r.ReadBool(keyName)
            else if keyName = 'INCLUDESYSFOLDERS' then
              chkSysDirs.Checked:= r.ReadBool(keyName)
            else if keyName = 'INCLUDEFILES' then
              edInclude.Text:= r.ReadString(keyName)
            else if keyName = 'EXCLUDEFILES' then
              edExclude.Text:= r.ReadString(keyName);
          except
          end;
        end;
      finally
        vals.Free;
      end;
    end;
  finally
    r.Free;
  end;
end;

procedure TFrmMain.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin
  dropSource.DropTarget:= self;
  dropDest.DropTarget:= self;
  SaveAllOptions;
  SaveFormPosition(self);
  canClose:= true;
end;

procedure TFrmMain.FormShow(Sender: TObject);
begin
  lblVersion.caption:= Format('%d.%d.%d%d',
                                [HiWord(ver.FileMajorVersion), LoWord(ver.FileMajorVersion),
                                 HiWord(ver.FileMinorVersion), LoWord(ver.FileMinorVersion)]);
  ReadFormPosition(self);
end;

procedure TFrmMain.lstProfilesClick(Sender: TObject);
var
  hist : TStringList;
begin
  // Change the profile
  if lstProfiles.ItemIndex >= 0 then
  begin
    profiles.CurrentProfileIdx:= lstProfiles.ItemIndex;

    cboSource.Clear;
    cboDest.Clear;
    hist:= profiles.SrcHistory[profiles.CurrentProfileIdx];
    cboSource.Items.Assign(hist);
    hist:= profiles.DstHistory[profiles.CurrentProfileIdx];
    cboDest.Items.Assign(hist);

    cboSource.ItemIndex:= 0;
    cboDest.ItemIndex:= 0;
  end;
end;

procedure TFrmMain.ProfilesEdit;
var
  mResult: integer;
begin
  // First, save any additions to the current profile list.
  profiles.Save;
  
{ TODO : Need to add a save all options function. }
  with TFrmProfiles.Create(application) do
  begin
    mResult:= ShowModal;
    Free;
  end;

  if mResult = mrOK then
  begin
    lstProfiles.Items.Assign(profiles.Profiles);
    lstProfiles.ItemIndex:= profiles.currentProfileIdx;
    if (profiles.currentProfileIdx >= 0) then
    begin
      cboSource.Items.Assign(profiles.SrcHistory[profiles.currentProfileIdx]);
      cboDest.Items.Assign(profiles.DstHistory[profiles.currentProfileIdx]);
    end
    else
    begin
      cboSource.Clear;
      cboDest.Clear;
    end;
    cboSource.ItemIndex:= -1;
    cboDest.ItemIndex:= -1;
  end;
end;

procedure TFrmMain.dropSourceDropFiles(Sender: TObject; Point: TPoint);
begin
  if dropSource.Count = 1 then
  begin
    cboSource.Text:= dropSource.Files[0];
  end
  else
    Beep;
end;

procedure TFrmMain.dropDestDropFiles(Sender: TObject; Point: TPoint);
begin
  if dropDest.Count = 1 then
  begin
    cboDest.Text:= dropDest.Files[0];
  end
  else
    Beep;
end;

procedure TFrmMain.EditMaskList(Sender: TObject);
var
  edBox : TNubEdit;
  frm   : TFrmMaskEdit;
begin
  edBox:= sender as TNubEdit;
  frm:= TFrmMaskEdit.Create(self);
  try
    if edBox = edExclude then
      frm.Caption:= 'Exclude Files'
    else
      frm.Caption:= 'Include Files';

    frm.MaskStr:= edBox.Text;
    if frm.ShowModal = mrOK then
    begin
      edBox.Text:= frm.MaskStr;
      if (edBox = edInclude) and (edBox.Text = '') then
        edBox.Text:= '*.*';
    end;
  finally
    frm.Free;
  end;
end;

procedure TFrmMain.cboSourceDblClick(Sender: TObject);
begin
  cboSource.DroppedDown:= true;
end;

procedure TFrmMain.cboDestDblClick(Sender: TObject);
begin
  cboDest.DroppedDown:= true;
end;

procedure TFrmMain.BtnBackupClick(Sender: TObject);
begin
  if SaveDialog1.Execute then
  begin

  end;
end;

procedure TFrmMain.BtnRestoreClick(Sender: TObject);
begin
  if OpenDialog1.Execute then
  begin
    // Restore options from backup file.
  end;
end;

procedure TFrmMain.BtnModifyOptsClick(Sender: TObject);
var
  optTag  : integer;
begin
  if lbOptions.ItemIndex >= 0 then
  begin
    optTag:= integer(lbOptions.Items.Objects[lbOptions.ItemIndex]);
    case optTag of
      optionTagUserProfile:
        ProfilesEdit;

      optionTagFilters:
        FiltersEdit;

      optionTagSourceCompare:
        SourceCompareEdit;

      optionTagBackup:
        ;

      optionTagRestore:
        ;
    end;
  end;
end;

procedure TFrmMain.lbOptionsDblClick(Sender: TObject);
begin
  BtnModifyOpts.Click;
end;

procedure TFrmMain.LoadAllOptions;
begin
  SaveSettings;
  SaveSCMOptsToReg;
  SaveFilterOptsToReg;
end;

procedure TFrmMain.SaveAllOptions;
begin
  ReadSettings;
  ReadSCMOptsFromReg;
  ReadFilterOptsFromReg;
end;

END.


