object FrmSrcCompOpts: TFrmSrcCompOpts
  Left = 280
  Top = 107
  BorderStyle = bsDialog
  Caption = 'Source Compare Options'
  ClientHeight = 224
  ClientWidth = 365
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = True
  Position = poOwnerFormCenter
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 16
    Top = 12
    Width = 95
    Height = 13
    Caption = 'Characters to ignore'
  end
  object Label3: TLabel
    Left = 16
    Top = 60
    Width = 40
    Height = 13
    Caption = 'Tab size'
  end
  object Label2: TLabel
    Left = 16
    Top = 36
    Width = 147
    Height = 13
    Caption = 'Minimum lines to make a match'
  end
  object Label4: TLabel
    Left = 16
    Top = 84
    Width = 65
    Height = 13
    Caption = 'Display Width'
  end
  object ChkConvertTabs: TCheckBox
    Left = 16
    Top = 136
    Width = 129
    Height = 17
    Caption = 'Convert Tabs'
    TabOrder = 8
  end
  object ChkIgnoreWS: TCheckBox
    Left = 16
    Top = 152
    Width = 129
    Height = 17
    Caption = 'Ignore Whitespace'
    TabOrder = 9
  end
  object ChkDispLineNums: TCheckBox
    Left = 216
    Top = 136
    Width = 129
    Height = 17
    Caption = 'Display Line Numbers'
    TabOrder = 11
  end
  object edCharsToIgnore: TEdit
    Left = 192
    Top = 8
    Width = 160
    Height = 21
    TabOrder = 0
  end
  object edTabSize: TEdit
    Left = 192
    Top = 56
    Width = 145
    Height = 21
    ReadOnly = True
    TabOrder = 4
    Text = '1'
  end
  object udTabSize: TUpDown
    Left = 337
    Top = 56
    Width = 15
    Height = 21
    Associate = edTabSize
    Min = 1
    Max = 15
    Position = 1
    TabOrder = 2
  end
  object BtnCancel: TBitBtn
    Left = 272
    Top = 184
    Width = 81
    Height = 27
    TabOrder = 15
    Kind = bkCancel
  end
  object BtnOK: TBitBtn
    Left = 184
    Top = 184
    Width = 81
    Height = 27
    Caption = 'OK'
    Default = True
    TabOrder = 16
    OnClick = BtnOKClick
    Glyph.Data = {
      DE010000424DDE01000000000000760000002800000024000000120000000100
      0400000000006801000000000000000000001000000010000000000000000000
      80000080000000808000800000008000800080800000C0C0C000808080000000
      FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
      3333333333333333333333330000333333333333333333333333F33333333333
      00003333344333333333333333388F3333333333000033334224333333333333
      338338F3333333330000333422224333333333333833338F3333333300003342
      222224333333333383333338F3333333000034222A22224333333338F338F333
      8F33333300003222A3A2224333333338F3838F338F33333300003A2A333A2224
      33333338F83338F338F33333000033A33333A222433333338333338F338F3333
      0000333333333A222433333333333338F338F33300003333333333A222433333
      333333338F338F33000033333333333A222433333333333338F338F300003333
      33333333A222433333333333338F338F00003333333333333A22433333333333
      3338F38F000033333333333333A223333333333333338F830000333333333333
      333A333333333333333338330000333333333333333333333333333333333333
      0000}
    NumGlyphs = 2
  end
  object edMinLines: TEdit
    Left = 192
    Top = 32
    Width = 145
    Height = 21
    ReadOnly = True
    TabOrder = 1
    Text = '1'
  end
  object udMinLines: TUpDown
    Left = 337
    Top = 32
    Width = 15
    Height = 21
    Associate = edMinLines
    Min = 1
    Max = 32767
    Position = 1
    TabOrder = 3
  end
  object ChkShowFileInfo: TCheckBox
    Left = 16
    Top = 120
    Width = 129
    Height = 17
    Caption = 'Show file information'
    TabOrder = 7
  end
  object ChkCaseSensitive: TCheckBox
    Left = 216
    Top = 120
    Width = 129
    Height = 17
    Caption = 'Case sensitive'
    TabOrder = 10
  end
  object ChkOptimize: TCheckBox
    Left = 216
    Top = 152
    Width = 129
    Height = 17
    Caption = 'Optimize results'
    TabOrder = 12
  end
  object BtnDefaults: TBitBtn
    Left = 8
    Top = 184
    Width = 81
    Height = 27
    Caption = '&Defaults'
    TabOrder = 13
    OnClick = BtnDefaultsClick
    Glyph.Data = {
      42010000424D4201000000000000760000002800000011000000110000000100
      040000000000CC00000000000000000000001000000010000000000000000000
      BF0000BF000000BFBF00BF000000BF00BF00BFBF0000C0C0C000808080000000
      FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00777777777777
      77777000000070000000000007777000000070FFFFFFFFFF07777000000070FC
      CFCCCCCF07777000000070FFFFFFFFFF07777000000070FCCFCCCCCF07777000
      000070FFFFFFFFFF07777000000070FFFFFFF0FF07777000000070F00FFF0B0F
      07770000000070F0F0F0B0F000700000000070FF0B0B0F0FBF00000000007000
      00F0F0FBFBF0000000007777770B0FBFBFB00000000077777770FBFBFB000000
      0000777777770000007000000000777777777777777770000000777777777777
      777770000000}
  end
  object edDispWidth: TEdit
    Left = 192
    Top = 80
    Width = 145
    Height = 21
    ReadOnly = True
    TabOrder = 5
    Text = '40'
  end
  object udDispWidth: TUpDown
    Left = 337
    Top = 80
    Width = 15
    Height = 21
    Associate = edDispWidth
    Min = 40
    Max = 256
    Position = 40
    TabOrder = 6
  end
  object BtnFont: TBitBtn
    Left = 96
    Top = 184
    Width = 81
    Height = 27
    Caption = '&Font'
    TabOrder = 14
    OnClick = BtnFontClick
    Glyph.Data = {
      4E010000424D4E01000000000000760000002800000012000000120000000100
      040000000000D800000000000000000000001000000000000000000000000000
      8000008000000080800080000000800080008080000080808000C0C0C0000000
      FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00888888888888
      88888800000088888888888800078800000088888888888DDDD0780000008078
      8880070D788D880000008007770A08DD78888800000088AAAAAA78DD78887800
      0000880080A0880D788D78000000888A7AA7888DD1DD7800000088800A088877
      0180880000008888AA7870CC07888800000088880088CCC00078880000008888
      888800888C0888000000888888870CC0CC0888000000888888080C7800888800
      0000888888070C78C078880000008888880CCC0CC08888000000888888800880
      888888000000888888888888888888000000}
  end
  object FontDialog1: TFontDialog
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    Options = []
    Left = 168
    Top = 120
  end
end
