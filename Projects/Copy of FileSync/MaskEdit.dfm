object FrmMaskEdit: TFrmMaskEdit
  Left = 340
  Top = 131
  BorderStyle = bsDialog
  Caption = 'Exclude Masks'
  ClientHeight = 290
  ClientWidth = 298
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poOwnerFormCenter
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 8
    Top = 232
    Width = 66
    Height = 13
    Caption = 'Test Filename'
  end
  object lblTestResult: TLabel
    Left = 8
    Top = 272
    Width = 61
    Height = 13
    Caption = 'lblTestResult'
  end
  object Bevel1: TBevel
    Left = 8
    Top = 216
    Width = 281
    Height = 9
    Shape = bsBottomLine
  end
  object lbMasks: TListBox
    Left = 8
    Top = 8
    Width = 193
    Height = 177
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Lucida Console'
    Font.Style = []
    ItemHeight = 12
    ParentFont = False
    TabOrder = 0
    OnDblClick = lbMasksDblClick
  end
  object BtnReplace: TButton
    Left = 216
    Top = 40
    Width = 75
    Height = 25
    Caption = '&Replace'
    TabOrder = 3
    OnClick = BtnReplaceClick
  end
  object BtnAdd: TButton
    Left = 216
    Top = 8
    Width = 75
    Height = 25
    Caption = '&Add'
    TabOrder = 2
    OnClick = BtnAddClick
  end
  object BtnDelete: TButton
    Left = 216
    Top = 72
    Width = 75
    Height = 25
    Caption = 'R&emove'
    TabOrder = 4
    OnClick = BtnDeleteClick
  end
  object BtnOK: TButton
    Left = 216
    Top = 152
    Width = 75
    Height = 25
    Caption = '&OK'
    ModalResult = 1
    TabOrder = 5
  end
  object BtnCancel: TButton
    Left = 216
    Top = 184
    Width = 75
    Height = 25
    Cancel = True
    Caption = 'Cancel'
    ModalResult = 2
    TabOrder = 6
  end
  object BtnTest: TButton
    Left = 216
    Top = 246
    Width = 75
    Height = 25
    Caption = '&Test'
    TabOrder = 8
    OnClick = BtnTestClick
  end
  object edTestFilename: TDMSEdit
    Left = 8
    Top = 248
    Width = 193
    Height = 21
    TabOrder = 7
    Text = 'edTestFilename'
    ClearOnFirstKey = False
  end
  object edMask: TDMSEdit
    Left = 8
    Top = 192
    Width = 193
    Height = 21
    TabOrder = 1
    Text = 'edMask'
    ClearOnFirstKey = False
  end
end
