Unit WildCard;

{- unit for wildcard support }

{- TMaskObj is the object used for maintaining mask.

	It is initialized with a mask string which can contain several masks
   separated with commas (ie, '*.EXE,*.COM,*.PAS').

   	A mask string is any string that contains up to [MaxMasks] wild cards.
      Each wildcard separated with a comma.

   	constructor Init(mStr: TMaskString);
      - Initialize the Mask Object with the specified mask string.
		  If no mask string is specified ('') then *.* will be assumed.

		destructor Done; virtual;
		- destroy the object.

		function IsMatch(fname: String): Boolean;
		- Return TRUE if specified file name matches the current list of
		  wild cards in the object.

		function MatchWildcard(mask, fname: String): Boolean;
		- Used internally. Returns true if fname fname matches the specified
		  wildcard. mask should contain only ONE wild card (ie, *.PAS).
		  Multiple wildcards are not supported in this routine.
		  Can be useful for checking a file against a single wildcard.

		function NumMasks: Integer;
		- return the number of masks in the current object.

		procedure Change(mStr: TMaskString);
		- Change the current list of masks. mStr can contain up to MaxMasks
		  wild cards (see above).

		function MaskString: TMaskString;
		- Return list of current masks in string format.
}

INTERFACE

uses
  SysUtils, Classes, StStrL;

type
  TMaskObj = class
  private
    fMasks  : TStringList;
    function GetMask(idx: integer): string;
    function NumMasks: Integer;
    procedure SetMask(idx: integer; const Value: string);
  public
    constructor Create;
    destructor  Destroy; override;

    procedure Add(mStr: string);
    function IsMatch(aName: string): boolean;
    function Count: Integer;
    property Mask[idx: integer]: string read GetMask write SetMask;
    function MaskString: string;
    function MatchWildcard(mask, fname: String): boolean;
  end;

IMPLEMENTATION

constructor TMaskObj.Create;
begin
  inherited;
  fMasks:= TStringList.Create;
end;

destructor TMaskObj.Destroy;
begin
  fMasks.Free;
  inherited;
end;

function TMaskObj.MatchWildcard(mask, fname: string): boolean;
// return true if fname matches current wildcard list.
// fname must be a valid filename/path.
Var
	w, f	: Byte;
	wName,
	tname	: string;
	wExt,
	ext	: string;
	match	: Boolean;

	function MatchName(wild, name: string): boolean;
	var
		matches	: Boolean;
	Begin
		If (wild = name) then
      result:= true
    else
    begin
      If Length(wild) > Length(name) then
        Pad(name, Length(wild));

      f:= 1;
      w:= 1;
      matches:= (wild <> '');		{- assume a match }
      While matches and (f <= Length(name)) Do Begin		{- check filename }
        If wild[w] = '*' Then 	{- rest of name matches }
          f:= Length(name)+1
        else Begin
          If (wild[w] <> '?') and (name[f] <> wild[w]) Then
            matches:= False
          else Begin
            Inc(w); Inc(f);
            If ((w > Length(wild)) and (f < Length(name))) Then
              matches:= False;
          end;
        end;
      end;
      MatchName:= matches;
    end;
	end;

Begin
	fname:= StUpCase(fname);
	mask:= StUpCase(mask);

	fSplit(fname, fname, tname, ext);
	ext:= StripRange(ext, '.', '.');	{- remove '.' from extension }

	fSplit(mask, fname, wName, wExt);
	wext:= StripRange(wext, '.', '.');	{- remove '.' from wild card extension }

	If (wExt = '') and (Pos('.', mask) = 0) Then
		wExt:= '*';

	match:= MatchName(wName, tname);
	If match Then
		match:= MatchName(wExt, ext);
	MatchWildcard:= match;
End;

Function TMaskObj.MaskString: TMaskString;
{- return all masks in a string format }
Var
	k		: Integer;
   mstr	: TMaskString;
Begin
	If masks[0].NumMasks = 0 Then
		mStr:= StarDotStar
	Else Begin
		mStr:= '';
		For k:= 1 To masks[0].NumMasks Do Begin
			mStr:= mStr + masks[k].mask;
			If k <> masks[0].numMasks Then
				mStr:= mStr + ',';
		End;
	End;
   MaskString:= mstr;
End;

Function TMaskObj.IsMatch(fname: String): Boolean;
{- return true if fname matches current wildcard list }
Var
	k			: Integer;
   matched	: Boolean;
Begin
	matched:= False;
   k:= 1;
   While not matched and (k <= NumMasks) Do Begin
		matched:= MatchWildcard(masks[k].mask, fname);
      Inc(k);
   End;
   IsMatch:= matched;
End;

Procedure TMaskObj.Change(mStr: TMaskString);
{- parse MStr into mask array. Masks should be separated with commas }
Var
	k		: Integer;
	tstr	: String;
   temp	: String;
Begin
	FillChar(masks, SizeOf(TMaskArray), #0);
   If mstr = '' Then
   	mstr:= StarDotStar;
	k:= 1;
	mStr:= StUpCase(mstr);
	While (mStr <> '') and (masks[0].numMasks < MaxMasks) Do Begin
		k:= Pos(',', mStr);
		If k > 0 Then
			tstr:= Copy(mStr, 1, k-1)
		Else Begin
			tstr:= mStr;
			k:= Length(mStr);
		End;
		Delete(mStr, 1, k);
		tstr:= Trim(tstr);
		If tstr <> '' Then Begin
			If Length(tstr) > 12 Then
				tstr[0]:= #12;
			k:= Pos('.', tstr);
         If k > 0 Then Begin
         	{- remove extra characters in mask (if > 3 chars) }
         	temp:= Copy(tstr, k+1, 3);
            tstr[0]:= Chr(k);
            tstr:= tstr + temp;
         End;
			Inc(masks[0].numMasks);
			masks[masks[0].numMasks].mask:= tstr;
		End;
	End;
End;

Function TMaskObj.NumMasks: Integer;
{- return current number of masks }
Begin
	NumMasks:= masks[0].numMasks;
End;

procedure TMaskObj.Add(mStr: string);
begin

end;

function TMaskObj.Count: Integer;
begin

end;

function TMaskObj.GetMask(idx: integer): string;
begin

end;

procedure TMaskObj.SetMask(idx: integer; const Value: string);
begin

end;

END.
