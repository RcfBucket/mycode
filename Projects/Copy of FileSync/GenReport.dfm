object FrmGenReport: TFrmGenReport
  Left = 405
  Top = 206
  BorderIcons = []
  BorderStyle = bsToolWindow
  Caption = 'File Sync'
  ClientHeight = 66
  ClientWidth = 176
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 12
    Top = 24
    Width = 105
    Height = 13
    Caption = 'Generating Report . . .'
  end
  object Label2: TLabel
    Left = 120
    Top = 24
    Width = 6
    Height = 13
    Caption = '0'
  end
end
