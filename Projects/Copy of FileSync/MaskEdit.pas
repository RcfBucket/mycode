unit MaskEdit;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, dmsCtrls, Wildcard, ExtCtrls;

type
  TFrmMaskEdit = class(TForm)
    lbMasks: TListBox;
    BtnReplace: TButton;
    BtnAdd: TButton;
    BtnDelete: TButton;
    BtnOK: TButton;
    BtnCancel: TButton;
    BtnTest: TButton;
    Label1: TLabel;
    edTestFilename: TDMSEdit;
    lblTestResult: TLabel;
    Bevel1: TBevel;
    edMask: TDMSEdit;
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure BtnAddClick(Sender: TObject);
    procedure BtnDeleteClick(Sender: TObject);
    procedure BtnReplaceClick(Sender: TObject);
    procedure lbMasksDblClick(Sender: TObject);
    procedure BtnTestClick(Sender: TObject);
  private
    fMasks : TFileMasks;
    function GetMaskStr: string;
    procedure SetMaskStr(const Value: string);
    procedure DisplayMasks;
  public
    property MaskStr: string read GetMaskStr write SetMaskStr;
  end;

var
  FrmMaskEdit: TFrmMaskEdit;

implementation

{$R *.DFM}

{ TFrmMaskEdit }

function TFrmMaskEdit.GetMaskStr: string;
begin
  result:= fMasks.MaskString;
end;

procedure TFrmMaskEdit.SetMaskStr(const Value: string);
begin
  fMasks.MaskString:= value;
  DisplayMasks;
end;

procedure TFrmMaskEdit.DisplayMasks;
var
  k : integer;
begin
  lbMasks.Clear;
  for k:= 0 to fMasks.Count - 1 do
  begin
    lbMasks.Items.Add(fMasks[k]);
  end;
end;

procedure TFrmMaskEdit.FormCreate(Sender: TObject);
begin
  fMasks:= TFileMasks.Create;
  edMask.Clear;
  edTestFilename.Clear;
  lblTestResult.Caption:= '';
end;

procedure TFrmMaskEdit.FormDestroy(Sender: TObject);
begin
  fMasks.Free;
end;

procedure TFrmMaskEdit.BtnAddClick(Sender: TObject);
begin
  if Trim(edMask.Text) <> '' then
  begin
    fMasks.Add(Trim(edMask.Text));
    DisplayMasks;
  end;
end;

procedure TFrmMaskEdit.BtnDeleteClick(Sender: TObject);
var
  idx : integer;
begin
  idx:= lbMasks.ItemIndex;
  fMasks.Remove(idx);
  DisplayMasks;
end;

procedure TFrmMaskEdit.BtnReplaceClick(Sender: TObject);
var
  idx : integer;
begin
  idx:= lbMasks.ItemIndex;
  if (idx >= 0) then
  begin
    fMasks.Replace(idx, edMask.Text);
    DisplayMasks;
  end;
end;

procedure TFrmMaskEdit.lbMasksDblClick(Sender: TObject);
var
  idx : integer;
begin
  idx:= lbMasks.ItemIndex;
  if (idx >= 0) and (idx < lbMasks.Count) then
    edMask.Text:= lbMasks.Items[lbMasks.ItemIndex];
end;

procedure TFrmMaskEdit.BtnTestClick(Sender: TObject);
begin
  if fMasks.IsMatch(edTestFilename.Text) then
  begin
    lblTestResult.Caption:= 'Match Found';
    lblTestResult.Font.Color:= clWindowText;
  end
  else
  begin
    lblTestResult.Caption:= 'No Match Found';
    lblTestResult.Font.Color:= clRed;
  end;
end;

end.
