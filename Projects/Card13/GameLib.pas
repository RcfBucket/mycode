unit GameLib;

{
  Always get rid of hand with most cards if in control?

  when picking fULL houses, the triple card should be larger than the pair card

  Seperate the sorting from the picking of hands.


I may need to compute a "Play" rank, rather than just adding up card ranks.
Mainly for straights.  A2345 is high sometimes...
Also, need to allow for A2345 and AKQJ10
Consider adding a RankModifier to TPlay to adjust the normal card rank of odd
plays.
}
interface

uses
  SysUtils, Windows, Classes, Generics.Collections, Generics.Defaults, Dialogs,
  Cards;

type
  TPlayTypes = (ptSingle, ptPair, ptTriple, ptStrait, ptFlush, ptFullHouse, ptFourKind, ptStraitFlush);

const
  PlayTypeStrs : array[TPlayTypes] of string =
  ('Single', 'Pair', 'Triple', 'Strait', 'Flush', 'Full House', 'Four of a Kind', 'StraitFlush');
type
  // represents a single card
  TSingleCard = class
  private
    fSuit: TCardSuit;
    fValue: TCardValue;
    fOrder: integer;
    fDealtIdx: integer;
    fCardUI: TCard;
    function GetRank: integer;
    function GetSuitRank: integer;
    function GetValueRank: integer;
    function GetName: string;
    function GetSuitName: string;    // used for shuffling
  public
    constructor Create(aCardSuit: TCardSuit; aCardValue: TCardValue);
    destructor Destroy; override;
    property Name: string read GetName;
    property SuitName: string read GetSuitName;
    property Suit: TCardSuit read fSuit;
    property SuitRank: integer read GetSuitRank;
    property Value: TCardValue read fValue;
    property ValueRank: integer read GetValueRank;
    property Order: integer read fOrder;    // used to shuffling (decks will modify this to the proper order val when shuffled
    property Rank: integer read GetRank;
    property DealtIndex: integer read fDealtIdx;  // used to record original ordering of cards in a hand
    property CardUI: TCard read fCardUI write fCardUI;
  end;

  // contains all cards for deck
  TDeck = class
  private
    fCards : TObjectList<TSingleCard>;
    function GetCard(aIndex: integer): TSingleCard;
    function GetCount: integer;
  public
    constructor Create;
    destructor Destroy; override;
    property Cards[aIndex: integer]: TSingleCard read GetCard; default;
    property Count: integer read GetCount;
  end;

  // contains all cards for game
  TDecks = class
  private
    fCount: integer;
    fDecks: TObjectList<TDeck>;
    fDrawIdx: integer;  // index of current card ready to be pulled from deck (for dealing)
    procedure SetCount(const aValue: integer);
    function GetDeck(aIndex: integer): TDeck;
    function GetCard(aIndex: integer): TSingleCard;
    function GetCardByOrder(aIndex: integer): TSingleCard;
  public
    constructor Create;
    destructor Destroy; override;
    function TotalCards: integer;
    procedure Shuffle;
    procedure ClearShuffle;
    function DrawCard: TSingleCard;
    function CanDraw(aHowMany: integer): boolean;
    property Count: integer read fCount write SetCount;
    property Deck[aIndex: integer]: TDeck read GetDeck; default;
    property Card[aIndex: integer]: TSingleCard read GetCard;
    property CardByOrder[aIndex: integer]: TSingleCard read GetCardByOrder;
    property NextDrawIndex: integer read fDrawIdx;
  end;

  TPlay = class
  private
    fPlayType: TPlayTypes;
    fCards : TList<TSingleCard>;   // holds indexs into original card list
    function GetRank: integer;
  public
    constructor Create(aPlayType: TPlayTypes);
    destructor Destroy; override;
    property PlayType: TPlayTypes read fPlayType;
    property Rank: integer read GetRank;
    property Cards: TList<TSingleCard> read fCards;
  end;

  TPlayList = TObjectList<TPlay>;

  // contains the permutations of all hands available
  THand = class
  private
    fOriginalCards: TList<TSingleCard>;  // list of original cards
    fStraitFlushes: TPlayList;
    fFourOfKinds: TPlayList;
    fFullHouses: TPlayList;
    fFlushes: TPlayList;
    fStraits: TPlayList;
    fTriples: TPlayList;
    fPairs: TPlayList;
    fSingles: TPlayList;
    function GetLowest: TSingleCard;
    procedure BuildFlushList;
    function GetPlay(aPlayType: TPlayTypes): TPlayList;
    procedure BuildCombinations(aTotalCards, aCardsToPull, aStartingCard: integer; aPlayType: TPlayTypes);
    procedure WalkCards(curIdx: integer; curValue: TCardValue; var aCount: integer; var aBuf: array of integer);
  public
    constructor Create;
    destructor Destroy; override;
    procedure Clear;
    procedure AddCard(aCard: TSingleCard);

    procedure SortByOriginalDeal;
    procedure SortBySingles;
    procedure SortByCardValue(aPlayType: TPlayTypes);
    procedure SortByStraits;
    procedure SortByFlushes;
    procedure SortByFullHouse;
    procedure SortByStraitFlush;

    function BestPlay(aPlayType: TPlayTypes): TPlay;

    property OriginalHand: TList<TSingleCard> read fOriginalCards;
    property LowestCard: TSingleCard read GetLowest;
    property Plays[aPlayType: TPlayTypes]: TPlayList read GetPlay;
  end;

function NChooseR(aN, aR: integer): integer;

implementation

function NFact(aNum: integer): integer;
// Compute the factorial of aNum
begin
  if (aNum > 1) then
    result:= aNum * NFact(aNum - 1)
  else
    result:= 1;
end;

function NChooseR(aN, aR: integer): integer;
// return the number of combinations of N choose R
// nCr
begin
  result:= NFact(aN) div (NFact(aR) * NFact(aN - aR));
end;

{ TDecks }

constructor TDecks.Create;
begin
  fDecks:= TObjectList<TDeck>.Create;
  Count:= 1;
end;

destructor TDecks.Destroy;
begin
  fDecks.Free;
  inherited;
end;

function TDecks.CanDraw(aHowMany: integer): boolean;
begin
  if fDrawIdx + aHowMany <= TotalCards then
    result:= true
  else
    result:= false;
end;

function TDecks.DrawCard: TSingleCard;
// draw single card from decks
var
  totCards: integer;
begin
  totCards:= TotalCards;
  if fDrawIdx <= totCards - 1 then
  begin
    result:= CardByOrder[fDrawIdx];
    Inc(fDrawIdx);
  end
  else
    result:= nil;
end;

function TDecks.GetCard(aIndex: integer): TSingleCard;
// treats all cards in all decks as a single array of cards
var
  deckIdx: integer;
  cardIdx: integer;
begin
  if (aIndex >= 0) and (aIndex < TotalCards) then
  begin
    deckIdx:= aIndex div 52;
    cardIdx:= aIndex mod 52;
    result:= fDecks[deckIdx][cardIdx];
  end
  else
    result:= nil;
end;

function TDecks.GetCardByOrder(aIndex: integer): TSingleCard;
// find the card for the given order index.
var
  k   : integer;
  tmp : TSingleCard;
  totCards: integer;
begin
  result:= nil;
  totCards:= TotalCards;
  k:= 0;
  while (k < totCards) and (result = nil) do
  begin
    tmp:= Card[k];
    if tmp.order = aIndex then
      result:= tmp;
    Inc(k);
  end;
end;

function TDecks.GetDeck(aIndex: integer): TDeck;
begin
  if (aIndex >= 0) and (aIndex <= fDecks.Count) then
    result:= fDecks[aIndex]
  else
    result:= nil;
end;

procedure TDecks.SetCount(const aValue: integer);
var
  k: integer;
begin
  if aValue > 0 then
  begin
    fDecks.Clear;
    for k := 1 to aValue do
      fDecks.Add(TDeck.Create);
    fCount:= aValue;
  end;
end;

procedure TDecks.ClearShuffle;
var
  num : integer;
  totCards: integer;
begin
  fDrawIdx:= 0;
  totCards:= TotalCards;
  for num:= 0 to totCards - 1 do
    Card[num].fOrder:= num;
end;

procedure TDecks.Shuffle;
// shuffle all cards by placing a random order value on each card in all the
// decks.
var
  counter : integer;
  totCards : integer;
  tmp: TSingleCard;
  num: integer;
begin
  fDrawIdx:= 0; // reset next card to draw

  counter:= 0;
  totCards:= TotalCards;

  // clear current shuffle
  for num:= 0 to totCards - 1 do
    Card[num].fOrder:= -1;

  repeat
    num:= Random(totCards);
    tmp:= Card[num];
    if tmp.order = -1 then
    begin
      tmp.fOrder:= counter;
      Inc(counter);
    end;
  until counter = totCards;
end;

function TDecks.TotalCards: integer;
var
  k: Integer;
// return total number of cards in deck
begin
  result:= 0;
  for k:= 0 to fDecks.Count - 1 do
    Inc(result, fDecks[k].Count);
end;

{ TDeck }

constructor TDeck.Create;
var
  tmp: TSingleCard;
  s: TCardSuit;
  v: TCardValue;
begin
  fCards:= TObjectList<TSingleCard>.Create;
  for s:= Low(TCardSuit) to High(TCardSuit) do
  begin
    for v:= Low(TCardValue) to High(TCardValue) do
    begin
      tmp:= TSingleCard.Create(s, v);
      fCards.Add(tmp);
    end;
  end;
end;

destructor TDeck.Destroy;
begin
  fCards.Free;
  inherited;
end;

function TDeck.GetCard(aIndex: integer): TSingleCard;
begin
  if (aIndex >= 0) and (aIndex < fCards.Count) then
    result:= fCards[aIndex]
  else
    result:= nil;
end;

function TDeck.GetCount: integer;
begin
  result:= fCards.Count;
end;

{ TSingleCard }

constructor TSingleCard.Create(aCardSuit: TCardSuit; aCardValue: TCardValue);
begin
  fCardUI:= nil;
  fSuit:= aCardSuit;
  fValue:= aCardValue;
  fOrder:= -1;
end;

destructor TSingleCard.Destroy;
begin
  inherited;
end;

function TSingleCard.GetName: string;
begin
  result:= cCardName[Value];
end;

function TSingleCard.GetRank: integer;
// return the numeric Rack of card
// This accounts for suit as well
begin
  result:= SuitRank + ((ValueRank - 1) * 52);
//  if Value = cvTwo then
//    Inc(result, 5000);
end;

function TSingleCard.GetSuitName: string;
begin
  result:= Cards.cSuitName[fSuit];
end;

function TSingleCard.GetSuitRank: integer;
begin
  result:= Cards.gSuitRank[fSuit];
end;

function TSingleCard.GetValueRank: integer;
begin
  result:= Cards.gValueRank[fValue];
end;

{ THand }

procedure THand.AddCard(aCard: TSingleCard);
var
  idx: integer;
begin
  idx:= fOriginalCards.Add(aCard);
  aCard.fDealtIdx:= idx;  // save the original dealt index
end;

procedure THand.BuildFlushList;
begin
end;

procedure THand.Clear;
begin
  fOriginalCards.Clear;
end;

constructor THand.Create;
begin
  fOriginalCards:= TList<TSingleCard>.Create;
  fFlushes:= TPlayList.Create;
  fSingles:= TPlayList.Create;
  fPairs:= TPlayList.Create;
  fTriples:= TPlayList.Create;
  fFourOfKinds:= TPlayList.Create;
  fStraits:= TPlayList.Create;
  fStraitFlushes:= TPlayList.Create;
  fFullHouses:= TPlayList.Create;
end;

destructor THand.Destroy;
begin
  fOriginalCards.Free;
  fFlushes.Free;
  fSingles.Free;
  fPairs.Free;
  fFourOfKinds.Free;
  fTriples.Free;
  fStraitFlushes.Free;
  fStraits.Free;
  fFullHouses.Free;
  inherited;
end;

function THand.GetPlay(aPlayType: TPlayTypes): TPlayList;
begin
  case aPlayType of
    ptSingle:       result:= fSingles;
    ptPair:         result:= fPairs;
    ptTriple:       result:= fTriples;
    ptStrait:       result:= fStraits;
    ptFlush:        result:= fFlushes;
    ptFullHouse:    result:= fFullHouses;
    ptFourKind:     result:= fFourOfKinds;
    ptStraitFlush:  result:= fStraitFlushes;
  end;
end;

function THand.GetLowest: TSingleCard;
var
  tmp: integer;
  k: integer;
begin
  tmp:= MaxInt;
  result:= nil;
  for k:= 0 to fOriginalCards.Count - 1 do
  begin
    if fOriginalCards[k].Rank < tmp then
    begin
      tmp:= fOriginalCards[k].Rank;
      result:= fOriginalCards[k];
    end;
  end;
end;

function THand.BestPlay(aPlayType: TPlayTypes): TPlay;
// return the best play for a given play type
// will have to sort all different hands.
// then get the plays for the play type
// then find the one with the highest rank
var
  play: TPlay;
begin
  Plays[aPlayType];
end;

procedure THand.BuildCombinations(aTotalCards, aCardsToPull, aStartingCard: integer; aPlayType: TPlayTypes);
// This will build a list of combinations of nCr
// where n = aTotalCards, r = aCardsToPull
// hands to build is the list of plays we are adding plays to (eg, flushes, pairs, etc)
var
  curPlays: TPlayList;

  procedure BuildPlay(aComb: array of integer);
  var
    idx: integer;
    card: TSingleCard;
    play: TPlay;
  begin
    // create play here
    // aComb has index ofsets for the cards to choose from, starting with aStartingCard
    // find each card in fOriginalCards and add it to the play
    play:= TPlay.Create(aPlayType);
    curPlays.Add(play);
    for idx:= 0 to (aCardsToPull - 1) do
    begin
      card:= fOriginalCards[aStartingCard + aComb[idx]];
      play.fCards.Add(card);
    end;
  end;

  function NextCombination(var aComb: array of integer; r, n: integer): boolean;
  var
    i : integer;
  begin
    // Generates the next combination of n elements as r after comb
    // comb => the previous combination ( use (0, 1, 2, ..., r) for first)
    // r => the size of the subsets to generate
    // n => the size of the original set

    //  Returns: true if a valid combination was found
    //  false, otherwise

    i:= r - 1;
    Inc(aComb[i]);
    while ((i > 0) and (aComb[i] >= (n - r + 1 + i))) do
    begin
      Dec(i);
      Inc(aComb[i]);
    end;

    if (aComb[0] > n - r) then    // Combination (n-k, n-k+1, ..., n) reached
      result:= false              // No more combinations can be generated
    else
    begin
      //  comb now looks like (..., x, n, n, n, ..., n).
      //  Turn it into (..., x, x + 1, x + 2, ...)
      for i:= i + 1 to r - 1 do
        aComb[i]:= aComb[i - 1] + 1;

      result:= true;
    end;
  end;

var
  n, r, i : integer;
  comb: array of integer;
begin
  curPlays:= Plays[aPlayType];

  n:= aTotalCards;       // The size of the set; for {1, 2, 3, 4} it's 4
  r:= aCardsToPull;      // The size of the subsets; for {1, 2}, {1, 3}, ... it's 2
  SetLength(comb, 6);    // comb[i] is the index of the i-th element in the combination
                         // plays are only 5 cards, so 6 slots is plenty to create combinations

  // Setup comb for the initial combination
  for i:= 0 to r - 1 do
    comb[i]:= i;

  // Setup the first combination
  BuildPlay(comb);

  // Generate and print all the other combinations
  while (NextCombination(comb, r, n)) do
    BuildPlay(comb);
end;

procedure THand.SortByFlushes;
var
  comparer : IComparer<TSingleCard>;
  k: integer;
  firstCard: TSingleCard;
  j: integer;
  counter: integer;
begin
  comparer:= TDelegatedComparer<TSingleCard>.Create(
    // TComparison<TSingleCard>
    function(const Left, Right: TSingleCard): integer
    begin
      if left.Suit = right.Suit then
      begin
        result:= left.ValueRank - right.ValueRank;
      end
      else
        result:= left.SuitRank - right.SuitRank;
    end);

  fOriginalCards.Sort(comparer);
  fFlushes.Clear;

  k:= 0;
  while (k < fOriginalCards.Count) do
  begin
    firstCard:= fOriginalCards[k];
    j:= k + 1;
    counter:= 1;
    while (j < fOriginalCards.Count) do
    begin
      if fOriginalCards[j].Suit = firstCard.Suit then
      begin
        Inc(counter);
        Inc(j);
      end
      else
        j:= fOriginalCards.Count; // force bailout
    end;

    if counter >= 5 then
    begin
      // here I figure out combinations (counter CHOOSE 5)
      // for each combination, create a play.
      // index the combintions from k into OriginalCards to find actual card to play
      BuildCombinations(counter, 5, k, ptFlush);
      Inc(k, counter);  // jump past all the cards in this suit
    end
    else
      Inc(k);
  end;
end;

procedure THand.SortByFullHouse;
var
  p: integer;
  t: integer;
  play: TPlay;
  card: TSingleCard;
begin
  // ensure pair and triple plays are built
  SortByCardValue(ptPair);
  SortByCardValue(ptTriple);

  fFullHouses.Clear;

  for p:= 0 to fPairs.Count - 1 do
  begin
    for t:= 0 to fTriples.Count - 1 do
    begin
      // make sure I dont combine a pair and a trip that have the save card value
      if fPairs[p].Cards[0].Value <> fTriples[t].Cards[0].Value then
      begin
        play:= TPlay.Create(ptFullHouse);
        fFullHouses.Add(play);
        for card in fTriples[t].Cards do
        begin
          play.Cards.Add(card);
        end;
        for card in fPairs[p].Cards do
        begin
          play.Cards.Add(card);
        end;
      end;
    end;
  end;
end;

procedure THand.SortByOriginalDeal;
var
  comparer : IComparer<TSingleCard>;
begin
  comparer:= TDelegatedComparer<TSingleCard>.Create(
    // TComparison<TSingleCard>
    function(const Left, Right: TSingleCard): integer
    begin
      result:= left.DealtIndex - right.DealtIndex;
    end);

  fOriginalCards.Sort(comparer);
end;

procedure THand.SortByCardValue(aPlayType: TPlayTypes);
var
  comparer : IComparer<TSingleCard>;
  k: integer;
  play: TPlay;
  idx2: integer;
  allMatchesFound: boolean;
  cardsToMatch: integer;
  plays: TPlayList;
  counter: integer;
  firstCard: TSingleCard;
begin
  case aPlayType of
    ptPair:
      begin
        cardsToMatch:= 2;
        plays:= fPairs;
      end;
    ptTriple:
      begin
        cardsToMatch:= 3;
        plays:= fTriples;
      end;
    ptFourKind:
      begin
        cardsToMatch:= 4;
        plays:= fFourOfKinds;
      end;
  else
    exit;
  end;

  comparer:= TDelegatedComparer<TSingleCard>.Create(
    // TComparison<TSingleCard>
    function(const Left, Right: TSingleCard): integer
    begin
      if left.ValueRank = right.ValueRank then
        result:= left.SuitRank - right.SuitRank
      else
        result:= left.ValueRank - right.ValueRank;
    end);

  fOriginalCards.Sort(comparer);

  plays.Clear;
  k:= 0;
  while k < fOriginalCards.Count do
  begin
    // keep looking forward until end of list or value does not match
    idx2:= k + 1;
    counter:= 1;  // count first card
    firstCard:= fOriginalCards[k];
    // count up number of cards that match first card
    while (idx2 < fOriginalCards.Count) do
    begin
      if (firstCard.Value = fOriginalCards[idx2].Value) then
      begin
        Inc(counter);
        Inc(idx2);
      end
      else
        idx2:= fOriginalCards.Count;    // force bailout
    end;

    // here I have found a series of same cards of at least cardsToMatch in number (eg, pairs=2, triples=3)
    // for each combination, create a play.
    // index the combintions from k into OriginalCards to find actual card to play
    if counter >= cardsToMatch then
    begin
      BuildCombinations(counter, cardsToMatch, k, aPlayType);
      Inc(k, counter);  // jump past all the cards in this series
    end
    else
      Inc(k);
  end;
end;

procedure THand.SortBySingles;
var
  comparer : IComparer<TSingleCard>;
  k: integer;
  play: TPlay;
begin
  comparer:= TDelegatedComparer<TSingleCard>.Create(
    // TComparison<TSingleCard>
    function(const Left, Right: TSingleCard): integer
    begin
      result:= left.Rank - right.Rank;
    end);

  fOriginalCards.Sort(comparer);

  fSingles.Clear;
  for k:= 0 to fOriginalCards.Count - 1 do
  begin
    play:= TPlay.Create(ptSingle);
    play.fCards.Add(fOriginalCards[k]);
    fSingles.Add(play);
  end;
end;

procedure THand.SortByStraitFlush;
var
  play  : TPlay;
  tmpPlay: TPlay;
  curSuit: TCardSuit;
  card: TSingleCard;
  match: boolean;
  idx: integer;
  k : integer;
begin
  SortByStraits;    // build strait hands first
  fStraitFlushes.Clear;
  for k:= 0 to fStraits.Count - 1 do
  begin
    play:= fStraits[k];
    curSuit:= play.fCards[0].Suit;
    match:= true;
    idx:= 1;
    while (idx < play.Cards.Count) and (match) do
    begin
      if play.Cards[idx].Suit <> curSuit then
        match:= false;
      Inc(idx);
    end;
    if match then
    begin
      tmpPlay:= TPlay.Create(ptStraitFlush);
      fStraitFlushes.Add(tmpPlay);
      for card in play.Cards do
      begin
        tmpPlay.Cards.Add(card);
      end;
    end;
  end;
end;

procedure THand.WalkCards(curIdx: integer; curValue: TCardValue; var aCount: integer; var aBuf: array of integer);
begin
  if curIdx < (fOriginalCards.Count - 1) then
  begin
    if Succ(curValue) = fOriginalCards[curIdx + 1].Value then
    begin
      if aCount < 5 then
      begin
        Inc(aCount);
        aBuf[aCount - 1]:= curIdx + 1;
        WalkCards(curIdx + 1, fOriginalCards[curIdx + 1].Value, aCount, aBuf);
      end;
    end
    else
    begin
      WalkCards(curIdx + 1, curValue, aCount, aBuf);
    end;
  end;
end;

procedure THand.SortByStraits;
var
  comparer : IComparer<TSingleCard>;
  k: integer;
  idx2, idx3: integer;
  allMatchesFound: boolean;
  counter: integer;
  curCard: TSingleCard;
  buf: array[0..4] of integer;
  play: TPlay;
  aces: TList<TSingleCard>;
begin
  comparer:= TDelegatedComparer<TSingleCard>.Create(
    // TComparison<TSingleCard>
    function(const Left, Right: TSingleCard): integer
    begin
      result:= Ord(left.Value) - Ord(right.Value);
    end);

  fOriginalCards.Sort(comparer);
  fStraits.Clear;

  for k:= 0 to fOriginalCards.Count - 1 do
  begin
    curCard:= fOriginalCards[k];
    FillChar(buf, SizeOf(buf), 0);
    counter:= 1;
    buf[0]:= k;
    WalkCards(k, curCard.Value, counter, buf);
    if (counter = 5) then
    begin
      play:= TPlay.Create(ptStrait);
      fStraits.Add(play);
      for idx2:= 0 to 4 do
      begin
        play.fCards.Add(fOriginalCards[buf[idx2]]);
      end;
    end;
  end;

  // At this point, I have all straits built.  But since "Ace" is orinally after
  // K, the code above will not consider an A2345 as a strait.  It will only find
  // 10JQKA. So, here, I am going to manually build any of these straits.
  // walk thru all the straits that I have found, and see if there are any that
  // start with a "2".  If so, then I will look for any Aces in the hand and, if
  // found, add the strait to the hand.
  aces:= TList<TSingleCard>.Create;
  try
    for k:= 0 to fOriginalCards.Count - 1 do
    begin
      if fOriginalCards[k].Value = cvAce then
      begin
        aces.Add(fOriginalCards[k]);
      end;
    end;

    if (aces.Count > 0) and (fStraits.Count > 0) then
    begin
      k:= fStraits.Count - 1; // walk backwards, cuz I am going to be adding items to the fStraits list
      while k >= 0 do
      begin
        if fStraits[k].Cards[0].Value = cvTwo then
        begin
          for idx2:= 0 to aces.Count - 1 do
          begin
            // -2 on limiting end cuz I wont be using last card from this strait
            play:= TPlay.Create(ptStrait);
            fStraits.Add(play);
            play.Cards.Add(aces[idx2]); // add the Ace in
            for idx3:= 0 to fStraits[k].Cards.Count - 2 do
            begin
              // now add remain 4 cards of strait
              play.Cards.Add(fStraits[k].Cards[idx3]);
            end;
          end;
        end;
        Dec(k);
      end;
    end;
  finally
    aces.Free;
  end;
end;

{ TPlay }

constructor TPlay.Create(aPlayType: TPlayTypes);
begin
  inherited Create;
  fPlayType:= aPlayType;
  fCards:= TList<TSingleCard>.Create;
end;

destructor TPlay.Destroy;
begin
  fCards.Free;
  inherited;
end;

function TPlay.GetRank: integer;
var
  k: integer;
begin
  result:= 0;
  for k:= 0 to fCards.Count - 1 do
  begin
    Inc(result, fCards[k].Rank);
  end;
end;

initialization
  Randomize;
end.
