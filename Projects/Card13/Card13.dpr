program Card13;

uses
  SysUtils,
  Forms,
  Main in 'Main.pas' {Form1},
  GameLib in 'GameLib.pas',
  Cards in '..\..\Components\Cards.pas';

{$R *.res}

begin
  ReportMemoryLeaksOnShutdown:= true;
  if CheckWin32Version(6) then
    Application.DefaultFont.Name:= 'Segoe UI'
  else
    Application.DefaultFont.Name:= 'Tahoma';
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(TForm1, Form1);
  Application.Run;
end.
