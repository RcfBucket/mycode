object Form1: TForm1
  Left = 0
  Top = 0
  Caption = '13'
  ClientHeight = 624
  ClientWidth = 798
  Color = clBtnFace
  DefaultMonitor = dmPrimary
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Segoe UI'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 16
    Top = 24
    Width = 30
    Height = 13
    Caption = 'Decks'
  end
  object lblTotalCards: TLabel
    Left = 160
    Top = 24
    Width = 67
    Height = 13
    Caption = 'lblTotalCards'
  end
  object Label2: TLabel
    Left = 149
    Top = 352
    Width = 33
    Height = 13
    Caption = 'Label2'
  end
  object Label3: TLabel
    Left = 289
    Top = 307
    Width = 70
    Height = 13
    Caption = 'Current Hand'
  end
  object lblowest: TLabel
    Left = 425
    Top = 307
    Width = 44
    Height = 13
    Caption = 'lblowest'
  end
  object Label5: TLabel
    Left = 8
    Top = 133
    Width = 45
    Height = 13
    Caption = 'Draw Idx'
  end
  object lblDrawIdx: TLabel
    Left = 58
    Top = 133
    Width = 55
    Height = 13
    Caption = 'lblDrawIdx'
  end
  object lblPlayCount: TLabel
    Left = 15
    Top = 485
    Width = 65
    Height = 13
    Caption = 'lblPlayCount'
  end
  object Label6: TLabel
    Left = 15
    Top = 509
    Width = 72
    Height = 13
    Caption = 'Highlight Play'
  end
  object lblPlayRank: TLabel
    Left = 170
    Top = 509
    Width = 59
    Height = 13
    Caption = 'lblPlayRank'
  end
  object lblSelected: TLabel
    Left = 739
    Top = 288
    Width = 56
    Height = 13
    Caption = 'lblSelected'
  end
  object lblNChooseR: TLabel
    Left = 680
    Top = 596
    Width = 67
    Height = 13
    Caption = 'lblNChooseR'
  end
  object lblHandPlayed: TLabel
    Left = 20
    Top = 575
    Width = 74
    Height = 13
    Caption = 'lblHandPlayed'
  end
  object lblHandScore: TLabel
    Left = 20
    Top = 594
    Width = 69
    Height = 13
    Caption = 'lblHandScore'
  end
  object edDecks: TcxSpinEdit
    Left = 88
    Top = 20
    Properties.ImmediatePost = True
    Properties.MaxValue = 4.000000000000000000
    Properties.MinValue = 1.000000000000000000
    Properties.OnEditValueChanged = edDecksPropertiesEditValueChanged
    TabOrder = 0
    Value = 1
    Width = 65
  end
  object cxGrid1: TcxGrid
    Left = 280
    Top = 8
    Width = 509
    Height = 274
    TabOrder = 1
    object tvDecks: TcxGridTableView
      NavigatorButtons.ConfirmDelete = False
      OnFocusedRecordChanged = tvDecksFocusedRecordChanged
      OnSelectionChanged = tvDecksSelectionChanged
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      OptionsBehavior.PullFocusing = True
      OptionsCustomize.ColumnFiltering = False
      OptionsCustomize.ColumnMoving = False
      OptionsSelection.CellSelect = False
      OptionsSelection.MultiSelect = True
      OptionsView.ColumnAutoWidth = True
      OptionsView.GroupByBox = False
      object tvDecksColumn1: TcxGridColumn
        Caption = 'Deck'
        Width = 58
      end
      object tvDecksColumn2: TcxGridColumn
        Caption = 'Card'
        Width = 136
      end
      object tvDecksColumn3: TcxGridColumn
        Caption = 'Suit Rank'
        DataBinding.ValueType = 'Integer'
        Width = 69
      end
      object tvDecksColumn6: TcxGridColumn
        Caption = 'Value Rank'
        DataBinding.ValueType = 'Integer'
        Width = 73
      end
      object tvDecksColumn4: TcxGridColumn
        Caption = 'Order'
        DataBinding.ValueType = 'Integer'
        Width = 54
      end
      object tvDecksColumn5: TcxGridColumn
        Caption = 'Card Rank'
        DataBinding.ValueType = 'Integer'
        Width = 117
      end
    end
    object cxGrid1Level1: TcxGridLevel
      GridView = tvDecks
    end
  end
  object testCard: TCard
    Left = 233
    Top = 56
    Width = 41
    Height = 65
    Color = clBtnFace
    Selected = True
    Suit = csClub
    Value = cvAce
    SymbolSize = 20
    FontSize = 14
  end
  object BtnShuffle: TButton
    Left = 8
    Top = 56
    Width = 75
    Height = 25
    Caption = 'Shuffle'
    TabOrder = 3
    OnClick = BtnShuffleClick
  end
  object Card1: TCard
    Left = 7
    Top = 183
    Width = 41
    Height = 65
    Color = clBtnFace
    Suit = csClub
    Value = cvAce
    SymbolSize = 20
    FontSize = 12
    OnClick = Card1Click
  end
  object Card2: TCard
    Left = 55
    Top = 183
    Width = 41
    Height = 65
    Color = clBtnFace
    Suit = csClub
    Value = cvAce
    SymbolSize = 20
    FontSize = 12
    OnClick = Card1Click
  end
  object Card3: TCard
    Left = 102
    Top = 183
    Width = 41
    Height = 65
    Color = clBtnFace
    Suit = csClub
    Value = cvAce
    SymbolSize = 20
    FontSize = 12
    OnClick = Card1Click
  end
  object Card4: TCard
    Left = 149
    Top = 183
    Width = 41
    Height = 65
    Color = clBtnFace
    Suit = csClub
    Value = cvAce
    SymbolSize = 20
    FontSize = 12
    OnClick = Card1Click
  end
  object Card5: TCard
    Left = 196
    Top = 183
    Width = 41
    Height = 65
    Color = clBtnFace
    Suit = csClub
    Value = cvAce
    SymbolSize = 20
    FontSize = 12
    OnClick = Card1Click
  end
  object Card6: TCard
    Left = 7
    Top = 254
    Width = 41
    Height = 65
    Color = clBtnFace
    Suit = csClub
    Value = cvAce
    SymbolSize = 20
    FontSize = 12
    OnClick = Card1Click
  end
  object Card7: TCard
    Left = 54
    Top = 254
    Width = 41
    Height = 65
    Color = clBtnFace
    Suit = csClub
    Value = cvAce
    SymbolSize = 20
    FontSize = 12
    OnClick = Card1Click
  end
  object Card8: TCard
    Left = 102
    Top = 254
    Width = 41
    Height = 65
    Color = clBtnFace
    Suit = csClub
    Value = cvAce
    SymbolSize = 20
    FontSize = 12
    OnClick = Card1Click
  end
  object Card9: TCard
    Left = 149
    Top = 254
    Width = 41
    Height = 65
    Color = clBtnFace
    Suit = csClub
    Value = cvAce
    SymbolSize = 20
    FontSize = 12
    OnClick = Card1Click
  end
  object Card10: TCard
    Left = 196
    Top = 254
    Width = 41
    Height = 65
    Color = clBtnFace
    Suit = csClub
    Value = cvAce
    SymbolSize = 20
    FontSize = 12
    OnClick = Card1Click
  end
  object Card11: TCard
    Left = 8
    Top = 325
    Width = 41
    Height = 65
    Color = clBtnFace
    Suit = csClub
    Value = cvAce
    SymbolSize = 20
    FontSize = 12
    OnClick = Card1Click
  end
  object Card12: TCard
    Left = 55
    Top = 325
    Width = 41
    Height = 65
    Color = clBtnFace
    Suit = csClub
    Value = cvAce
    SymbolSize = 20
    FontSize = 12
    OnClick = Card1Click
  end
  object Card13: TCard
    Left = 102
    Top = 325
    Width = 41
    Height = 65
    Color = clBtnFace
    Suit = csClub
    Value = cvAce
    SymbolSize = 20
    FontSize = 12
    OnClick = Card1Click
  end
  object BtnDeal: TButton
    Left = 8
    Top = 152
    Width = 75
    Height = 25
    Caption = '&Deal'
    TabOrder = 17
    OnClick = BtnDealClick
  end
  object cxGrid2: TcxGrid
    Left = 281
    Top = 326
    Width = 509
    Height = 257
    TabOrder = 18
    object tvHand: TcxGridTableView
      NavigatorButtons.ConfirmDelete = False
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      OptionsBehavior.PullFocusing = True
      OptionsCustomize.ColumnFiltering = False
      OptionsCustomize.ColumnMoving = False
      OptionsSelection.CellSelect = False
      OptionsSelection.InvertSelect = False
      OptionsSelection.MultiSelect = True
      OptionsView.ColumnAutoWidth = True
      OptionsView.GroupByBox = False
      object cxGridColumn1: TcxGridColumn
        Caption = 'Index'
        Width = 58
      end
      object cxGridColumn2: TcxGridColumn
        Caption = 'Card'
        Width = 136
      end
      object cxGridColumn3: TcxGridColumn
        Caption = 'Suit Rank'
        DataBinding.ValueType = 'Integer'
        Width = 69
      end
      object cxGridColumn4: TcxGridColumn
        Caption = 'Value Rank'
        DataBinding.ValueType = 'Integer'
        Width = 73
      end
      object cxGridColumn5: TcxGridColumn
        Caption = 'Order'
        DataBinding.ValueType = 'Integer'
        Width = 54
      end
      object cxGridColumn6: TcxGridColumn
        Caption = 'Card Rank'
        DataBinding.ValueType = 'Integer'
        Width = 117
      end
    end
    object cxGridLevel1: TcxGridLevel
      GridView = tvHand
    end
  end
  object BtnClearShuffle: TButton
    Left = 89
    Top = 56
    Width = 75
    Height = 25
    Caption = 'Clear Shuffle'
    TabOrder = 19
    OnClick = BtnClearShuffleClick
  end
  object edPlayHighlight: TcxSpinEdit
    Left = 88
    Top = 506
    Properties.ImmediatePost = True
    Properties.MinValue = 1.000000000000000000
    Properties.OnEditValueChanged = edPlayHighlightPropertiesEditValueChanged
    TabOrder = 20
    Value = 1
    Width = 66
  end
  object rdoSorts: TcxRadioGroup
    Left = 7
    Top = 396
    Caption = 'Sorts'
    Properties.Columns = 3
    Properties.Items = <
      item
        Caption = 'Dealt'
      end
      item
        Caption = 'Single Cards'
      end
      item
        Caption = 'Pairs'
      end
      item
        Caption = 'Triples'
      end
      item
        Caption = 'Straits'
      end
      item
        Caption = 'Flushes'
      end
      item
        Caption = 'Full House'
      end
      item
        Caption = '4 Of Kind'
      end
      item
        Caption = 'Straigh Flush'
      end>
    Properties.OnChange = rdoSortsPropertiesChange
    ItemIndex = 5
    TabOrder = 21
    Height = 80
    Width = 268
  end
  object BtnManualDeal: TButton
    Left = 102
    Top = 152
    Width = 75
    Height = 25
    Caption = 'Manual Deal'
    TabOrder = 22
    OnClick = BtnManualDealClick
  end
  object BtnNChooseR: TButton
    Left = 559
    Top = 592
    Width = 50
    Height = 21
    Caption = 'n C r'
    TabOrder = 23
    OnClick = BtnNChooseRClick
  end
  object edN: TEdit
    Left = 504
    Top = 592
    Width = 49
    Height = 21
    TabOrder = 24
    Text = '7'
  end
  object edR: TEdit
    Left = 615
    Top = 592
    Width = 49
    Height = 21
    TabOrder = 25
    Text = '5'
  end
  object BtnAnalyzeHand: TButton
    Left = 16
    Top = 544
    Width = 127
    Height = 25
    Caption = 'Analyze Hand'
    TabOrder = 26
    OnClick = BtnAnalyzeHandClick
  end
end
