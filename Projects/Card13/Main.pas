﻿unit Main;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, GameLib, StdCtrls, cxControls, cxContainer, cxEdit, cxTextEdit,
  cxMaskEdit, cxSpinEdit, cxStyles, cxClasses, cxGridLevel, cxGrid,
  cxCustomData, cxGraphics, cxFilter, cxData, cxDataStorage,
  cxGridCustomTableView, cxGridTableView, cxGridCustomView, Cards,
  Generics.Collections, cxLookAndFeelPainters, cxGroupBox, cxRadioGroup, cxLookAndFeels;

type
  TForm1 = class(TForm)
    edDecks: TcxSpinEdit;
    Label1: TLabel;
    cxGrid1Level1: TcxGridLevel;
    cxGrid1: TcxGrid;
    tvDecks: TcxGridTableView;
    tvDecksColumn1: TcxGridColumn;
    tvDecksColumn2: TcxGridColumn;
    tvDecksColumn3: TcxGridColumn;
    testCard: TCard;
    lblTotalCards: TLabel;
    BtnShuffle: TButton;
    tvDecksColumn4: TcxGridColumn;
    Card1: TCard;
    Card2: TCard;
    Card3: TCard;
    Card4: TCard;
    Card5: TCard;
    Card6: TCard;
    Card7: TCard;
    Card8: TCard;
    Card9: TCard;
    Card10: TCard;
    Card11: TCard;
    Card12: TCard;
    Card13: TCard;
    BtnDeal: TButton;
    Label2: TLabel;
    tvDecksColumn5: TcxGridColumn;
    tvDecksColumn6: TcxGridColumn;
    cxGrid2: TcxGrid;
    tvHand: TcxGridTableView;
    cxGridColumn1: TcxGridColumn;
    cxGridColumn2: TcxGridColumn;
    cxGridColumn3: TcxGridColumn;
    cxGridColumn4: TcxGridColumn;
    cxGridColumn5: TcxGridColumn;
    cxGridColumn6: TcxGridColumn;
    cxGridLevel1: TcxGridLevel;
    Label3: TLabel;
    lblowest: TLabel;
    Label5: TLabel;
    lblDrawIdx: TLabel;
    BtnClearShuffle: TButton;
    lblPlayCount: TLabel;
    edPlayHighlight: TcxSpinEdit;
    Label6: TLabel;
    rdoSorts: TcxRadioGroup;
    lblPlayRank: TLabel;
    BtnManualDeal: TButton;
    lblSelected: TLabel;
    BtnNChooseR: TButton;
    edN: TEdit;
    edR: TEdit;
    lblNChooseR: TLabel;
    BtnAnalyzeHand: TButton;
    lblHandPlayed: TLabel;
    lblHandScore: TLabel;
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure edDecksPropertiesEditValueChanged(Sender: TObject);
    procedure tvDecksFocusedRecordChanged(Sender: TcxCustomGridTableView;
      APrevFocusedRecord, AFocusedRecord: TcxCustomGridRecord;
      ANewItemRecordFocusingChanged: Boolean);
    procedure BtnShuffleClick(Sender: TObject);
    procedure BtnDealClick(Sender: TObject);
    procedure BtnClearShuffleClick(Sender: TObject);
    procedure rdoSortsPropertiesChange(Sender: TObject);
    procedure edPlayHighlightPropertiesEditValueChanged(Sender: TObject);
    procedure BtnManualDealClick(Sender: TObject);
    procedure tvDecksSelectionChanged(Sender: TcxCustomGridTableView);
    procedure BtnNChooseRClick(Sender: TObject);
    procedure Card1Click(Sender: TObject);
    procedure BtnAnalyzeHandClick(Sender: TObject);
  private
    fDecks : TDecks;
    fHand  : THand;
    fHandDisplay: array[0..12] of TCard;
    procedure FillDeckGrid;
    procedure FillCurentHandGrid;
    procedure HighlightPlay(aPlayIdx: integer);
    function GridIndexOfCard(aCard: TSingleCard): integer;
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

{$R *.dfm}


procedure TForm1.FillDeckGrid;
var
  deckIdx: integer;
  cardIdx: integer;
  idx: integer;
  card: TSingleCard;
  deck: TDeck;
begin

  tvDecks.BeginUpdate;
  tvDecks.DataController.RecordCount:= 0;
  for deckIdx:= 0 to fDecks.Count - 1 do
  begin
    deck:= fDecks[deckIdx];
    for cardIdx:= 0 to deck.Count - 1 do
    begin
      card:= deck[cardIdx];
      idx:= tvDecks.DataController.AppendRecord;
      tvDecks.DataController.Values[idx, 0]:= deckIdx + 1;
      tvDecks.DataController.Values[idx, 1]:= Format('%s %s', [card.Name, card.SuitName]);
      tvDecks.DataController.Values[idx, 2]:= card.SuitRank;
      tvDecks.DataController.Values[idx, 3]:= card.ValueRank;
      tvDecks.DataController.Values[idx, 4]:= card.Order;
      tvDecks.DataController.Values[idx, 5]:= card.Rank;
    end;
  end;
  tvDecks.EndUpdate;
  lblDrawIdx.Caption:= IntToStr(fDecks.NextDrawIndex);
end;

procedure TForm1.BtnShuffleClick(Sender: TObject);
begin
  fDecks.Shuffle;
  FillDeckGrid;
end;

procedure TForm1.Card1Click(Sender: TObject);
var
  card: TCard;
begin
  card:= sender as TCard;
  if card <> nil then
  begin
    card.Selected:= not card.Selected;
  end;
end;

procedure TForm1.BtnManualDealClick(Sender: TObject);
var
  k: integer;
  card : TSingleCard;
begin
  if tvDecks.Controller.SelectedRecordCount <> 13 then
  begin
    MessageDlg('13 Cards must be selected from deck first', mtError, [mbOK], 0);
    exit;
  end;

  fHand.Clear;

  for k:= 0 to tvDecks.Controller.SelectedRecordCount - 1 do
  begin
    card:= fDecks.Card[tvDecks.Controller.SelectedRows[k].RecordIndex];
    card.CardUI:= fHandDisplay[k];
    fHand.AddCard(card);
    fHandDisplay[k].Suit:= card.Suit;
    fHandDisplay[k].Value:= card.Value;
  end;

  fHand.SortByOriginalDeal;
  FillCurentHandGrid;
  card:= fHand.LowestCard;
  if card <> nil then
    lblowest.Caption:= 'Lowest Card: ' + card.Name + ' ' + card.SuitName
  else
    lblowest.Caption:= 'none';

  lblDrawIdx.Caption:= IntToStr(fDecks.NextDrawIndex);
  rdoSorts.ItemIndex:= 0;
end;

procedure TForm1.BtnNChooseRClick(Sender: TObject);
var
  n: integer;
  r: integer;
begin
  TryStrToInt(edN.Text, n);
  TryStrToInt(edR.Text, r);
  n:= NChooseR(n, r);
  lblNChooseR.Caption:= IntToStr(n);
end;

procedure TForm1.FillCurentHandGrid;
var
  k: integer;
  card: TSingleCard;
begin
  tvHand.BeginUpdate;
  tvHand.DataController.ClearSelection;
  tvHand.DataController.RecordCount:= fHand.OriginalHand.Count;
  for k:= 0 to fHand.OriginalHand.Count - 1 do
  begin
    card:= fHand.OriginalHand[k];
    tvHand.DataController.Values[k, 0]:= card.DealtIndex;
    tvHand.DataController.Values[k, 1]:= Format('%s %s', [card.Name, card.SuitName]);
    tvHand.DataController.Values[k, 2]:= card.SuitRank;
    tvHand.DataController.Values[k, 3]:= card.ValueRank;
    tvHand.DataController.Values[k, 4]:= card.Order;
    tvHand.DataController.Values[k, 5]:= card.Rank;
  end;
  tvHand.EndUpdate;
end;

procedure TForm1.BtnAnalyzeHandClick(Sender: TObject);
var
  hand : THand;
  k: integer;
  card: TSingleCard;
  play: TPlay;
begin
  if fHand.OriginalHand.Count = 0 then
  begin
    MessageDlg('Deal a hand first', mtError, [mbOK], 0);
    exit;
  end;

  hand:= THand.Create;
  for k:= 0 to High(fHandDisplay) do
  begin
    if fHandDisplay[k].Selected then
    begin
      card:= fHand.OriginalHand[k];
      hand.AddCard(card);
    end;
  end;

  // flash selected cards
  for k:= 0 to 3 do
  begin
    for card in hand.OriginalHand do
    begin
      card.CardUI.FaceUp:= not card.CardUI.FaceUp;
    end;
    Application.ProcessMessages;
    Sleep(100);
  end;

  // at this point, the hand has the cards selected to play
  // Build the differing plays and choose the best on.
  case hand.OriginalHand.Count of
    1: lblHandPlayed.Caption:= PlayTypeStrs[ptSingle];
    2: lblHandPlayed.Caption:= PlayTypeStrs[ptPair];
    3: lblHandPlayed.Caption:= PlayTypeStrs[ptTriple];
  end;
//  play:= hand.Plays[ptSingle];

  hand.Free;
end;

procedure TForm1.BtnClearShuffleClick(Sender: TObject);
begin
  fDecks.ClearShuffle;
  FillDeckGrid;
  lblDrawIdx.Caption:= IntToStr(fDecks.NextDrawIndex);
end;

procedure TForm1.BtnDealClick(Sender: TObject);
var
  k: integer;
  card : TSingleCard;
begin
  fHand.Clear;
  if not fDecks.CanDraw(13) then
  begin
    fDecks.Shuffle;
    FillDeckGrid;
  end;

  for k:= 0 to 12 do
  begin
    card:= fDecks.DrawCard;
    card.CardUI:= fHandDisplay[k];
    fHand.AddCard(card);
    fHandDisplay[k].Suit:= card.Suit;
    fHandDisplay[k].Value:= card.Value;
  end;

  fHand.SortByOriginalDeal;
  FillCurentHandGrid;
  card:= fHand.LowestCard;
  if card <> nil then
    lblowest.Caption:= 'Lowest Card: ' + card.Name + ' ' + card.SuitName
  else
    lblowest.Caption:= 'none';

  lblDrawIdx.Caption:= IntToStr(fDecks.NextDrawIndex);
  rdoSorts.ItemIndex:= 0;
end;

procedure TForm1.edDecksPropertiesEditValueChanged(Sender: TObject);
begin
  fDecks.Count:= edDecks.Value;
  FillDeckGrid;
  lblTotalCards.Caption:= IntToStr(fDecks.TotalCards);
end;

procedure TForm1.edPlayHighlightPropertiesEditValueChanged(Sender: TObject);
begin
  HighlightPlay(edPlayHighlight.Value);
end;

procedure TForm1.FormCreate(Sender: TObject);
begin
  fDecks:= TDecks.Create;
  fHand:= THand.Create;
  fHandDisplay[0]:= Card1;
  fHandDisplay[1]:= Card2;
  fHandDisplay[2]:= Card3;
  fHandDisplay[3]:= Card4;
  fHandDisplay[4]:= Card5;
  fHandDisplay[5]:= Card6;
  fHandDisplay[6]:= Card7;
  fHandDisplay[7]:= Card8;
  fHandDisplay[8]:= Card9;
  fHandDisplay[9]:= Card10;
  fHandDisplay[10]:= Card11;
  fHandDisplay[11]:= Card12;
  fHandDisplay[12]:= Card13;
  BtnShuffle.Click;
end;

procedure TForm1.FormDestroy(Sender: TObject);
begin
  fDecks.Free;
  fHand.Free;
end;

function TForm1.GridIndexOfCard(aCard: TSingleCard): integer;
var
  k: integer;
  idx: integer;
begin
  result:= -1;
  k:= 0;
  while (k < tvHand.DataController.RecordCount) and (result = -1) do
  begin
    idx:= tvHand.ViewData.Records[k].RecordIndex;
    if tvHand.DataController.Values[idx, 0] = aCard.DealtIndex then
      result:= k;
    Inc(k);
  end;
end;

procedure TForm1.HighlightPlay(aPlayIdx: integer);
var
  plays : TList<TPlay>;
  k: integer;
  idx: integer;
  gIdx: integer;
  card: TSingleCard;
begin
  case rdoSorts.ItemIndex of
    1:  plays:= fHand.Plays[ptSingle];
    2:  plays:= fHand.Plays[ptPair];
    3:  plays:= fHand.Plays[ptTriple];
    4:  plays:= fHand.Plays[ptStrait];
    5:  plays:= fHand.Plays[ptFlush];
    6:  plays:= fHand.Plays[ptFullHouse];
    7:  plays:= fHand.Plays[ptFourKind];
    8:  plays:= fHand.Plays[ptStraitFlush];
  else
    plays:= nil;
  end;

  tvHand.DataController.ClearSelection;
  lblPlayRank.Caption:= '';
  Dec(aPlayIdx);
  if (plays <> nil) and (plays.Count > 0) and (aPlayIdx >= 0) and (aPlayIdx < plays.Count) then
  begin
    lblPlayRank.Caption:= Format('Play Rank: %d', [plays[aPlayIdx].Rank]);
    for k:= 0 to plays[aPLayIdx].Cards.Count - 1 do
    begin
      card:= plays[aPLayIdx].Cards[k];
      idx:= GridIndexOfCard(card);
      tvHand.DataController.ChangeRowSelection(idx, true);
    end;
  end;
end;

procedure TForm1.rdoSortsPropertiesChange(Sender: TObject);
var
  num: integer;
  k : integer;
  plays: TList<TPlay>;
begin
  lblPlayCount.Caption:= '';
  plays:= nil;
  case rdoSorts.ItemIndex of
    0 :
      begin
        fHand.SortByOriginalDeal;
        FillCurentHandGrid;
      end;
    1:
      begin
        fHand.SortBySIngles;
        FillCurentHandGrid;
        plays:= fHand.Plays[ptSingle];
        lblPlayCount.Caption:= Format('%d single card hands found', [plays.Count]);
      end;
    2:
      begin
        fHand.SortByCardValue(ptPair);
        FillCurentHandGrid;
        plays:= fHand.Plays[ptPair];
        lblPlayCount.Caption:= Format('%d pair hands found', [plays.Count]);
      end;
    3:
      begin
        fHand.SortByCardValue(ptTriple);
        FillCurentHandGrid;
        plays:= fHand.Plays[ptTriple];
        lblPlayCount.Caption:= Format('%d Triple hands found', [plays.Count]);
      end;
    4:
      begin
        fHand.SortByStraits;
        FillCurentHandGrid;
        plays:= fHand.Plays[ptStrait];
        lblPlayCount.Caption:= Format('%d Strait hands found', [plays.Count]);
      end;
    5:
      begin
        fHand.SortByFlushes;
        FillCurentHandGrid;
        plays:= fHand.Plays[ptFlush];
        lblPlayCount.Caption:= Format('%d Flush hands found', [plays.Count]);
      end;
    6:
      begin
        fHand.SortByFullHouse;
        FillCurentHandGrid;
        plays:= fHand.Plays[ptFullHouse];
        lblPlayCount.Caption:= Format('%d Full House hands found', [plays.Count]);
      end;
    7:
      begin
        fHand.SortByCardValue(ptFourKind);
        FillCurentHandGrid;
        plays:= fHand.Plays[ptFourKind];
        lblPlayCount.Caption:= Format('%d Four of Kind hands found', [plays.Count]);
      end;
    8:
      begin
        fHand.SortByStraitFlush;
        FillCurentHandGrid;
        plays:= fHand.Plays[ptStraitFlush];
        lblPlayCount.Caption:= Format('%d Strait Flush hands found', [plays.Count]);
      end;
  end;

  if plays <> nil then
  begin
    edPlayHighlight.Properties.MaxValue:= plays.Count;
    edPlayHighlight.Value:= 1;
    edPlayHighlight.Enabled:= plays.Count > 1;
    label6.Enabled:= plays.Count > 1;
    HighlightPlay(edPlayHighlight.Value);
  end
  else
  begin
    edPlayHighlight.Enabled:= false;
    label6.Enabled:= false;
  end;
end;

procedure TForm1.tvDecksFocusedRecordChanged(Sender: TcxCustomGridTableView;
  APrevFocusedRecord, AFocusedRecord: TcxCustomGridRecord;
  ANewItemRecordFocusingChanged: Boolean);
var
  card: TSingleCard;
begin
  card:= fDecks.Card[AFocusedRecord.RecordIndex];
  testCard.Suit:= card.Suit;
  testCard.Value:= card.Value;
end;

procedure TForm1.tvDecksSelectionChanged(Sender: TcxCustomGridTableView);
begin
  lblSelected.Caption:= Format('Selected: %d', [tvDecks.Controller.SelectedRecordCount]);
end;

end.
