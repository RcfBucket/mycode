namespace EventLogViewer
{
  partial class Form1
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.gridLog = new DevExpress.XtraGrid.GridControl();
      this.gvLog = new DevExpress.XtraGrid.Views.Grid.GridView();
      this.colLevel = new DevExpress.XtraGrid.Columns.GridColumn();
      this.colSource = new DevExpress.XtraGrid.Columns.GridColumn();
      this.colDateTime = new DevExpress.XtraGrid.Columns.GridColumn();
      this.colMessage = new DevExpress.XtraGrid.Columns.GridColumn();
      this.repositoryItemMemoEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoEdit();
      this.repositoryItemTextEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
      this.btnFill = new DevExpress.XtraEditors.SimpleButton();
      this.cbLogs = new DevExpress.XtraEditors.ComboBoxEdit();
      this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
      this.chkAutoRowHeight = new DevExpress.XtraEditors.CheckEdit();
      this.textEdit1 = new DevExpress.XtraEditors.TextEdit();
      this.memoEdit1 = new DevExpress.XtraEditors.MemoEdit();
      ((System.ComponentModel.ISupportInitialize)(this.gridLog)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.gvLog)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoEdit1)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit1)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.cbLogs.Properties)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.chkAutoRowHeight.Properties)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.textEdit1.Properties)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.memoEdit1.Properties)).BeginInit();
      this.SuspendLayout();
      // 
      // gridLog
      // 
      this.gridLog.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                  | System.Windows.Forms.AnchorStyles.Left)
                  | System.Windows.Forms.AnchorStyles.Right)));
      this.gridLog.EmbeddedNavigator.Name = "";
      this.gridLog.Location = new System.Drawing.Point(12, 32);
      this.gridLog.MainView = this.gvLog;
      this.gridLog.Name = "gridLog";
      this.gridLog.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemTextEdit1,
            this.repositoryItemMemoEdit1});
      this.gridLog.Size = new System.Drawing.Size(744, 386);
      this.gridLog.TabIndex = 0;
      this.gridLog.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gvLog});
      // 
      // gvLog
      // 
      this.gvLog.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colLevel,
            this.colSource,
            this.colDateTime,
            this.colMessage});
      this.gvLog.GridControl = this.gridLog;
      this.gvLog.Name = "gvLog";
      this.gvLog.OptionsCustomization.AllowRowSizing = true;
      this.gvLog.OptionsSelection.InvertSelection = true;
      this.gvLog.OptionsView.RowAutoHeight = true;
      // 
      // colLevel
      // 
      this.colLevel.Caption = "Level";
      this.colLevel.FieldName = "Level";
      this.colLevel.Name = "colLevel";
      this.colLevel.Visible = true;
      this.colLevel.VisibleIndex = 0;
      this.colLevel.Width = 110;
      // 
      // colSource
      // 
      this.colSource.Caption = "Source";
      this.colSource.FieldName = "Source";
      this.colSource.Name = "colSource";
      this.colSource.OptionsColumn.AllowEdit = false;
      this.colSource.Visible = true;
      this.colSource.VisibleIndex = 1;
      this.colSource.Width = 158;
      // 
      // colDateTime
      // 
      this.colDateTime.Caption = "Date/Time";
      this.colDateTime.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
      this.colDateTime.FieldName = "DateTimeGenerated";
      this.colDateTime.Name = "colDateTime";
      this.colDateTime.OptionsColumn.AllowEdit = false;
      this.colDateTime.Visible = true;
      this.colDateTime.VisibleIndex = 2;
      this.colDateTime.Width = 134;
      // 
      // colMessage
      // 
      this.colMessage.AppearanceCell.Options.UseTextOptions = true;
      this.colMessage.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Top;
      this.colMessage.Caption = "Message";
      this.colMessage.ColumnEdit = this.repositoryItemMemoEdit1;
      this.colMessage.FieldName = "Message";
      this.colMessage.Name = "colMessage";
      this.colMessage.OptionsColumn.AllowEdit = false;
      this.colMessage.UnboundType = DevExpress.Data.UnboundColumnType.String;
      this.colMessage.Visible = true;
      this.colMessage.VisibleIndex = 3;
      this.colMessage.Width = 321;
      // 
      // repositoryItemMemoEdit1
      // 
      this.repositoryItemMemoEdit1.Appearance.Options.UseTextOptions = true;
      this.repositoryItemMemoEdit1.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
      this.repositoryItemMemoEdit1.Name = "repositoryItemMemoEdit1";
      // 
      // repositoryItemTextEdit1
      // 
      this.repositoryItemTextEdit1.AutoHeight = false;
      this.repositoryItemTextEdit1.Name = "repositoryItemTextEdit1";
      // 
      // btnFill
      // 
      this.btnFill.Location = new System.Drawing.Point(12, 424);
      this.btnFill.Name = "btnFill";
      this.btnFill.Size = new System.Drawing.Size(75, 23);
      this.btnFill.TabIndex = 1;
      this.btnFill.Text = "Fill";
      this.btnFill.Click += new System.EventHandler(this.btnFill_Click);
      // 
      // cbLogs
      // 
      this.cbLogs.Location = new System.Drawing.Point(99, 6);
      this.cbLogs.Name = "cbLogs";
      this.cbLogs.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
      this.cbLogs.Properties.DropDownRows = 15;
      this.cbLogs.Properties.Items.AddRange(new object[] {
            "Item 1",
            "Item 2",
            "Item 3"});
      this.cbLogs.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
      this.cbLogs.Size = new System.Drawing.Size(232, 20);
      this.cbLogs.TabIndex = 2;
      // 
      // labelControl1
      // 
      this.labelControl1.Location = new System.Drawing.Point(12, 9);
      this.labelControl1.Name = "labelControl1";
      this.labelControl1.Size = new System.Drawing.Size(48, 13);
      this.labelControl1.TabIndex = 3;
      this.labelControl1.Text = "Event Log";
      // 
      // chkAutoRowHeight
      // 
      this.chkAutoRowHeight.Location = new System.Drawing.Point(383, 6);
      this.chkAutoRowHeight.Name = "chkAutoRowHeight";
      this.chkAutoRowHeight.Properties.Caption = "checkEdit1";
      this.chkAutoRowHeight.Size = new System.Drawing.Size(75, 19);
      this.chkAutoRowHeight.TabIndex = 5;
      // 
      // textEdit1
      // 
      this.textEdit1.Location = new System.Drawing.Point(492, 424);
      this.textEdit1.Name = "textEdit1";
      this.textEdit1.Size = new System.Drawing.Size(100, 20);
      this.textEdit1.TabIndex = 6;
      // 
      // memoEdit1
      // 
      this.memoEdit1.Location = new System.Drawing.Point(630, 425);
      this.memoEdit1.Name = "memoEdit1";
      this.memoEdit1.Size = new System.Drawing.Size(100, 96);
      this.memoEdit1.TabIndex = 7;
      // 
      // Form1
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.ClientSize = new System.Drawing.Size(768, 459);
      this.Controls.Add(this.memoEdit1);
      this.Controls.Add(this.textEdit1);
      this.Controls.Add(this.chkAutoRowHeight);
      this.Controls.Add(this.labelControl1);
      this.Controls.Add(this.cbLogs);
      this.Controls.Add(this.btnFill);
      this.Controls.Add(this.gridLog);
      this.Name = "Form1";
      this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
      this.Text = "Form1";
      this.Load += new System.EventHandler(this.Form1_Load);
      ((System.ComponentModel.ISupportInitialize)(this.gridLog)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.gvLog)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoEdit1)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit1)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.cbLogs.Properties)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.chkAutoRowHeight.Properties)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.textEdit1.Properties)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.memoEdit1.Properties)).EndInit();
      this.ResumeLayout(false);
      this.PerformLayout();

    }

    #endregion

    private DevExpress.XtraGrid.GridControl gridLog;
    private DevExpress.XtraGrid.Views.Grid.GridView gvLog;
    private DevExpress.XtraGrid.Columns.GridColumn colSource;
    private DevExpress.XtraGrid.Columns.GridColumn colDateTime;
    private DevExpress.XtraGrid.Columns.GridColumn colMessage;
    private DevExpress.XtraEditors.SimpleButton btnFill;
    private DevExpress.XtraGrid.Columns.GridColumn colLevel;
    private DevExpress.XtraEditors.ComboBoxEdit cbLogs;
    private DevExpress.XtraEditors.LabelControl labelControl1;
    private DevExpress.XtraEditors.CheckEdit chkAutoRowHeight;
    private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit1;
    private DevExpress.XtraEditors.Repository.RepositoryItemMemoEdit repositoryItemMemoEdit1;
    private DevExpress.XtraEditors.TextEdit textEdit1;
    private DevExpress.XtraEditors.MemoEdit memoEdit1;
  }
}

