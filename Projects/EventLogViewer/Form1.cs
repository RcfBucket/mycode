using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Collections;
using System.Diagnostics;

namespace EventLogViewer
{
  public partial class Form1 : Form
  {
    ArrayList fList;

    public class LogData
    {
      private string fLevel;
      private DateTime fDateTime;
      private string fSource;
      private string fMessage;

      public LogData(string aLevel, DateTime aDateTime, string aSource, string aMsg)
      {
        fLevel = aLevel;
        fDateTime = aDateTime;
        fSource = aSource;
        fMessage = aMsg;
      }

      public string Level
      {
        get { return fLevel; }
      }

      public DateTime DateTimeGenerated
      {
        get { return fDateTime; }
      }

      public string Source
      {
        get { return fSource; }
      }

      public string Message
      {
        get { return fMessage; }
      }
    }

    public Form1()
    {
      InitializeComponent();
    }

    private void btnFill_Click(object sender, EventArgs e)
    {
      fList = new ArrayList();

      EventLog el = new EventLog("Application");
      foreach (EventLogEntry ev in el.Entries)
      {
        fList.Add(new LogData(ev.EntryType.ToString(), ev.TimeGenerated, ev.Source, ev.Message));
      }

      gridLog.DataSource = fList;
    }

    private void Form1_Load(object sender, EventArgs e)
    {
      cbLogs.Properties.Items.Clear();
      EventLog[] logs = EventLog.GetEventLogs();

      foreach (EventLog l in logs)
        cbLogs.Properties.Items.Add(l.LogDisplayName);

      int idx = cbLogs.Properties.Items.IndexOf("Application");
      if (idx >= 0)
        cbLogs.SelectedIndex = idx;
      else
        cbLogs.SelectedIndex = 0;
    }
  }
}