using System;
using System.Collections.Generic;
using System.Text;
using System.Diagnostics;
using System.Threading;

namespace ViewEventLog
{
  class Program
  {
    /*
     * Parameters: 
     * /L"LogName"  - specify which log to view. If blank, Application will be used"
     * /Mnn         - specify maximum number of log items to display. If not specified, then all will be displayed
     * /DL          - Display all available log entries. Optional
     * /X           - Clear event log
     * /O           - Ascending order (default is reverse order)
     */

    static void Main(string[] args)
    {
      string logName = "Application";
      int maxDisplay = -1;
      bool displayLogNames = false;
      bool clearEventLog = false;
      bool ReverseOrder = true;
      bool promptForDelete = true;

      string tstr;

      foreach (string s in args)
      {
        tstr = s.ToUpper();
        try
        {
          if (tstr.StartsWith("/?"))
          {
            Console.WriteLine("Event Log Viewer");
            Console.WriteLine("options: ");
            Console.WriteLine("/L\"LogName\" - Specify which log to view. If blank, \"Application\" will be used");
            Console.WriteLine("/Mnn        - Specify maximum number of log items to display."); 
            Console.WriteLine("              If not specified, then all will be displayed");
            Console.WriteLine("/A          - Display the names of all available logs. Optional");
            Console.WriteLine("/X          - Clear event log");
            Console.WriteLine("/F          - Force deletion of log with out prompting (goes with /X)");
            Console.WriteLine("/O          - Ascending order (default is reverse order)");
            Console.WriteLine("");
            System.Environment.Exit(0);
          }
          
          if (tstr.StartsWith("/O"))
            ReverseOrder = false;
          
          if (tstr.StartsWith("/DL"))
            displayLogNames = true;

          if (tstr.StartsWith("/X"))
            clearEventLog = true;

          if (tstr.StartsWith("/F"))
            promptForDelete = false;

          if (tstr.StartsWith("/L"))
            logName = s.Substring(2, s.Length - 2);

          if (tstr.StartsWith("/M"))
          {
            maxDisplay = Convert.ToInt32(s.Substring(2, s.Length - 2));
            if (maxDisplay <= 0)
              throw new Exception("Invalid parameter.  Maximum items must be > 0");
          }
        }
        catch
        {
          Console.WriteLine("Invalid parameter: " + s);
          Console.WriteLine("");
          System.Environment.Exit(-1);
        }
      }

      if (displayLogNames)
      {
        EventLog[] logs = EventLog.GetEventLogs();
        foreach (EventLog l in logs)
          Console.WriteLine(l.Log);

        Console.WriteLine("");
      }

      EventLog log = new EventLog(logName);
      Console.Write("EventLog: {0}, Total Count: {1}", logName, log.Entries.Count);
      if (maxDisplay > 0)
        Console.WriteLine(" Displaying: {0}", Math.Min(maxDisplay, log.Entries.Count));
      else
        Console.WriteLine("");
      Console.WriteLine("--------------------------------------------------------");

      if (clearEventLog)
      {
        string resp = "Y";
        if (promptForDelete)
        {
          Console.Write(string.Format("Clear contents of '{0}' Event Log (y/n)?", logName));
          resp = Console.ReadLine();
          resp = resp.ToUpper();
        }
        if (resp == "Y")
        {
          log.Clear();
          Console.WriteLine(string.Format("Event log '{0}' cleared", logName));
        }
        else
          Console.WriteLine("Event log not cleared");
      }
      else
      {
        int counter = 0;
        int idx = (ReverseOrder ? log.Entries.Count - 1 : 0);
        int loopFactor = (ReverseOrder ? -1 : 1);
        char eventType;
        EventLogEntry entry;
        for (int k = 0; k < log.Entries.Count; k++)
        {
          entry = log.Entries[idx];
          idx += loopFactor;
          if ((maxDisplay == -1) || (counter < maxDisplay))
          {
            tstr = entry.EntryType.ToString();
            if (tstr.Length > 0)
              eventType = tstr[0];
            else
              eventType = ' ';
            
            Console.WriteLine(string.Format("{0}:{1}-{2} {3} - {4}", counter, eventType, 
              entry.TimeGenerated.ToLocalTime(), entry.Source, entry.Message));
          }
          counter++;
        }
        //foreach (EventLogEntry entry in log.Entries)
        //{
        //}
      }
    }
  }
}
