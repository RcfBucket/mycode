unit MinePnl;
{
  Current Bugs:

  ??? Is there anyway to force the object inspector to refresh other property
      when a property changes. Some properties may affect others.
      See changing of 'Mines' when user changes fieldwidth/height.

  states:

    UP/NO BOMB   UP/BOMB

    PRESSED

    DOWN

    mouse press

      get click cell
      press click cell

    two mouse press

    right mouse press
      toggle flag
}

INTERFACE

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls;

const
  MaxPlayFieldWidth = 30;
  MaxPlayFieldHeight = 30;
  MinPlayFieldWidth = 2;
  MinPlayFieldHeight = 2;

type
  TFieldHorzAlign = (FieldLeft, FieldCenter, FieldRight);
  TFieldVertAlign = (FieldTop, FieldMiddle, FieldBottom);
  TAdjCell = (AdjTopLeft,    AdjTopMiddle,    AdjTopRight,
              AdjLeft,                        AdjRight,
              AdjBottomLeft, AdjBottomMiddle, AdjBottomRight);
  TFlags = (fNotFlagged, fFlagged, fQuestionFlag);

  TCell = class     // cell object
    isBomb    : boolean;
    isDown    : boolean;
    flagged   : TFlags;
    isPressed : boolean;
    blown     : boolean;
    AdjBombs  : integer;  // number of adjacent bombs
    constructor Create;
    procedure ResetCell;
  end;

  TMines = class(TCustomPanel)
  private
    fNumMines   : integer;
    fHorzAlign  : TFieldHorzAlign;
    fVertAlign  : TFieldVertAlign;
    fFieldWidth : integer;
    fFieldHeight: integer;
    fWidth      : integer;
    fAutoSize   : boolean;
    fShowBombs  : boolean;
    btns        : TBitMap;  // bitmap buttons
    fBtnIndex   : integer;
    cells       : TList;
    clickedCell : integer;
    // property edit functions.
    procedure SetFieldWidth(aWidth: integer);
    procedure SetFieldHeight(aHeight: integer);
    procedure SetBtnIndex(anIndex: integer);
    procedure SetHorzAlign(anAlign: TFieldHorzAlign);
    procedure SetVertAlign(anAlign: TFieldVertAlign);
    procedure SetShowBombs(aShow: boolean);

    function  GetEdgeWidths(index: integer): integer;
    procedure SetEdgeWidths(index, aWidth: integer);
    function  GetPanelBevels(index: integer): TPanelBevel;
    procedure SetPanelBevels(index: integer; aBevel: TPanelBevel);
    procedure SetAutoSize(aAuto: boolean);
    procedure SetBorderStyle(aStyle: TBorderStyle);
    function  GetBorderStyle: TBorderStyle;
    procedure SetMines(aNum: integer);

    function CountFlagged(aCellIdx: integer): integer;
    procedure PressAround(aCellIdx: integer; pressed, doPaint: boolean);
    procedure SetCellDown(aCellIdx: integer; doPaint: boolean);
    procedure SetCellPerimiterDown(aCellIdx: integer; doPaint: boolean);
    function AdjacentCell(aCellIdx: integer; aAdjIdx: TAdjCell): TCell;
    procedure CalcStartingRect(var rect: TRect);
    function CalcBtnRec(cellIdx: integer): TRect;
    procedure CalcClientRect(var rect: TRect);
    function TotalBorderWidth : integer;
    procedure PaintField;
    function MouseInField(x, y: integer): integer;
    procedure MouseDown(Button: TMouseButton;
                        Shift: TShiftState; X, Y: Integer); override;
    procedure MouseUp(Button: TMouseButton;
                        Shift: TShiftState; X, Y: Integer); override;
    procedure MouseMove(Shift: TShiftState; X, Y: Integer); override;
  protected
    procedure Paint; override;
  public
    constructor Create(aOwner: TComponent); override;
    destructor Destroy; override;
  published
    property  PlayFieldWidth: integer
      read    fFieldWidth
      write   SetFieldWidth
      default 8;
    property PlayFieldHeight: integer
      read    fFieldHeight
      write   SetFieldHeight
      default 8;
    property AutoSize: boolean
      read fAutoSize write SetAutoSize default true;
    property FieldHorzAlign: TFieldHorzAlign
      read fHorzAlign write SetHorzAlign default FieldLeft;
    property FieldVertAlign: TFieldVertAlign
      read fVertAlign write SetVertAlign default FieldTop;
    property Mines: integer
      read fNumMines write SetMines;

    property BevelWidth: integer index 0
      read GetEdgeWidths write SetEdgeWidths default 1;
    property BorderWidth: integer index 1
      read GetEdgeWidths write SetEdgeWidths default 2;
    property BevelInner: TPanelBevel index 0
      read GetPanelBevels write SetPanelBevels default bvLowered;
    property BevelOuter: TPanelBevel index 1
      read GetPanelBevels write SetPanelBevels default bvRaised;
    property BorderStyle: TBorderStyle
      read GetBorderStyle write SetBorderStyle default bsSingle;
    property ShowBombs: boolean
      read fShowBombs write SetShowBombs;

    (***** Remove following at finish ******)
    property Ctl3D;
  end;

procedure Register;

IMPLEMENTATION

{$R MINEPNL.RES}

const
  CellWidth  = 16; // cell dimensions of the supporting bitmap (see res file)
  CellHeight = 16;

type
  // NOTE: These status enums must be in the order in which the appear in the
  //       bitmap is the resource file.
  TCellStatus = (cNone,         // normal Up button
                 cFlag,         // flagged button (UP)
                 cQuestionUP,   // Marked ? (UP pos)
                 cBam,          // Bomb. red (shown when bomb selected.
                 cWrong,        // X mark for wrong selected cell
                 cBomb,         // bomb
                 cQuestionDown, // Marked ? (DOWN pos)
                 cEight, cSeven, cSix, cFive, cFour, cThree, cTwo, cOne,
                 cDownClear);


constructor TCell.Create;
begin
  inherited Create;
  ResetCell;
end;

procedure TCell.ResetCell;
begin
  isBomb:= false;
  isDown:= false;
  isPressed:= false;
  flagged:= fNotFlagged;
  blown:= false;
end;

constructor TMines.Create(aOwner: TComponent);
var
  k   : integer;
begin
  inherited Create(aOwner);
  controlStyle:= controlStyle - [csSetCaption];
  fShowBombs:= false;
  fVertAlign:= FieldTop;
  fHorzAlign:= FieldLeft;
  fBtnIndex:= 0;
  caption:= '';
  fFieldHeight:= 8;
  fFieldWidth:= 8;
  BevelWidth:= 1;
  BevelInner:= bvLowered;
  BevelOuter:= bvRaised;
  BorderStyle:= bsSingle;
  BorderWidth:= 1;
  Ctl3d:= false;
  parentCtl3D:= false;

  btns:= TBitMap.Create;
  try
    btns.LoadFromResourceName(hInstance, 'IDB_MINEFIELDBTNS');
  except
    raise EResNotFound.Create('Mine field bitmap not found.');
  end;

  cells:= TList.Create;
  for k:= 1 to fFieldWidth * fFieldHeight do
    cells.Add(TCell.Create);

  fNumMines:= 0;
  Randomize;
  if csDesigning in ComponentState then
    SetAutoSize(true);
end;

destructor TMines.Destroy;
var
  k   : integer;
begin
  btns.Free;
  for k:= 0 to cells.count - 1 do
    TCell(cells.Items[k]).Free;
  cells.Free;
  inherited Destroy;
end;

function TMines.CalcBtnRec(cellIdx: integer): TRect;
// return a rectangle that bounds a button in the bitmap.
var
  cell  : TCell;
  idx   : integer;
begin
  result.left:= 0;
  result.right:= CellWidth;

  cell:= cells.items[cellIdx];
  if cell.isDown then
  begin
    if cell.blown then
      idx:= Ord(cBam)
    else if cell.isBomb then
      idx:= Ord(cBomb)
    else
    begin
      idx:= 15 - cell.AdjBombs;
    end;
  end
  else if cell.isPressed then // cell is just being held down
  begin
    if cell.flagged = fQuestionFlag then
      idx:= Ord(cQuestionDown)
    else
      idx:= Ord(cDownClear);
  end
  else  // cell is in the UP position at this point
  begin
    if cell.flagged = fFlagged then
      idx:= Ord(cFlag)
    else if cell.flagged = fQuestionFlag then
      idx:= Ord(cQuestionUp)
    else
      idx:= Ord(cNone);
  end;

  result.Top:= idx * CellHeight;
  result.bottom:= result.top + CellHeight;
end;

procedure TMines.CalcClientRect(var rect: TRect);
// calculate the workable client area based on current settings of bevel
// and border widths
var
  totBorderWidth : integer;
begin
  rect:= GetClientRect;
  totBorderWidth:= BorderWidth;
  if BevelInner <> bvNone then
    Inc(totBorderWidth, BevelWidth);
  if BevelOuter <> bvNone then
    Inc(totBorderWidth, BevelWidth);
  InflateRect(rect, -totBorderWidth, -totBorderWidth); // shrink rect
end;

procedure TMines.CalcStartingRect(var rect: TRect);
// calculate the position of a the starting rectangle for the first button
// This takes into account the border widths and the playfield Alignment
var
  client  : TRect;
  wRect   : TRect;
  totWidth: integer;  // total width of play field
  totHeight: integer; // total height of play field
begin
  CalcClientRect(client);  {- get workable client rectangle }
  wRect:= GetClientRect;
  totWidth:= fFieldWidth * CellWidth;
  totHeight:= fFieldHeight * CellHeight;
  case fHorzAlign of
    FieldCenter:
      rect.Left:= ((wRect.Right - wRect.Left) div 2) - (totWidth div 2);

    FieldRight:
      rect.Left:= client.right - totWidth;

    else // FieldLeft:
      rect.Left:= client.Left;
  end;

  case fVertAlign of
    FieldMiddle:
      rect.Top:= ((wRect.Bottom - wRect.Top) div 2) -  (totHeight div 2);

    FieldBottom:
      rect.Top:= client.Bottom - totHeight;

    else // FieldTop
      rect.Top:= client.Top;
  end;
  rect.Right:= rect.Left + CellWidth;
  rect.Bottom:= rect.Top + CellHeight;
end;

procedure TMines.Paint;
begin
  inherited Paint;
  PaintField;
end;

procedure TMines.PaintField;
var
  rect      : TRect;
  dstRect   : TRect;
  srcRect   : TRect;
  col       : integer;
  row       : integer;
  cellIdx   : integer;
begin
  with canvas do
  begin
    CalcStartingRect(dstRect);
    rect:= dstRect;  // save original starting rect in rect

    cellIdx:= 0;
    for row:= 0 to fFieldHeight - 1 do
    begin
      for col:= 0 to fFieldWidth - 1 do
      begin
        srcRect:= CalcBtnRec(cellIdx);
        CopyRect(dstRect, btns.canvas, srcRect);
        if fShowBombs then
        begin
          if TCell(cells.items[cellIdx]).isBomb then
          begin
            pen.Color:= clRed;
            MoveTo(dstRect.left+2, dstRect.top+2);
            LineTo(dstRect.left+3, dstRect.top+2);
            LineTo(dstRect.left+3, dstRect.top+3);
            LineTo(dstRect.left+2, dstRect.top+3);
            LineTo(dstRect.left+2, dstRect.top+2);
          end;
        end;
        Inc(dstRect.left, CellWidth);
        Inc(dstRect.Right, CellWidth);
        Inc(cellIdx);
      end;
      Inc(dstRect.top, cellHeight);
      Inc(dstRect.bottom, CellHeight);
      dstRect.left:= rect.left;
      dstRect.right:= dstRect.left + CellWidth;
    end;

    canvas.pen.Color:= clWhite;
    if rect.left > TotalBorderWidth then
    begin
      MoveTo(rect.left, rect.top);
      LineTo(rect.left, rect.top + (CellHeight * fFieldHeight));
    end;
    if rect.top > TotalBorderWidth then
    begin
      MoveTo(rect.left, rect.top);
      LineTo(rect.left + (CellWidth * fFieldWidth), rect.top);
    end;
  end;
end;

function TMines.TotalBorderWidth : integer;
// return the width of the panels borders.
begin
  result:= BorderWidth * 2;
  if BorderStyle = bsSingle then
    Inc(result, 2);
  if BevelInner <> bvNone then
    Inc(result, BevelWidth * 2);
  if BevelOuter <> bvNone then
    Inc(result, BevelWidth * 2);
end;

procedure TMines.SetFieldWidth(aWidth: integer);
var
  k       : integer;
begin
  if (aWidth >= MinPlayFieldWidth) and (aWidth <= MaxPlayFieldWidth) then
  begin
    // clear all current cells
    for k:= 0 to cells.Count - 1 do
      TCell(cells.items[k]).Free;
    cells.Clear;

    fFieldWidth:= aWidth;

    for k:= 1 to fFieldWidth * fFieldHeight do
      cells.Add(TCell.Create);

    Mines:= 0;

    if fAutoSize then
      SetAutoSize(true)
    else
      Invalidate;
  end;
end;

procedure TMines.SetFieldHeight(aHeight: integer);
// set the height of the playing field.
var
  k   : integer;
begin
  if (aHeight >= MinPlayFieldWidth) and (aHeight <= MaxPlayFieldHeight) then
  begin
    // clear all current cells
    for k:= 0 to cells.Count - 1 do
      TCell(cells.items[k]).Free;
     cells.Clear;

    fFieldHeight:= aHeight;

    for k:= 1 to fFieldWidth*fFieldHeight do
      cells.Add(TCell.Create);

    Mines:= 0;

    if fAutoSize then
      SetAutoSize(true)
    else
      Invalidate;
  end;
end;

procedure TMines.SetBtnIndex(anIndex: integer);
begin
  if (anIndex >= 0) and (anIndex <= 15) and (anIndex <> fBtnIndex) then
  begin
    fBtnIndex:= anIndex;
    PaintField;
  end;
end;

procedure TMines.SetEdgeWidths(index, aWidth: integer);
// This function is used to intercept setting of the inherited
// BorderWidth and BevelWidth properties of TCustomPanel.
// The index is 0 when setting BevelWidth and 1 when setting BorderWidth
// The interception was required to perform auto-sizing of the TMines panel
begin
  if index = 0 then
    inherited BevelWidth:= aWidth
  else
    inherited BorderWidth:= aWidth;
  if fAutoSize then
    SetAutoSize(true);
end;

function TMines.GetEdgeWidths(index: integer): integer;
// see comment for SetBorderWidths
begin
  if index = 0 then
    result:= inherited BevelWidth
  else
    result:= inherited BorderWidth;
end;

procedure TMines.SetPanelBevels(index: integer; aBevel: TPanelBevel);
// This function is used to intercept setting of the inherited
// BevelInner and BevelOuter properties of TCustomPanel.
// The index is 0 when setting BevelInner and 1 when setting BevelOuter
// The interception was required to perform auto-sizing of the TMines panel
begin
  if index = 0 then
    inherited BevelInner:= aBevel
  else
    inherited BevelOuter:= aBevel;
  if fAutoSize then
    SetAutoSize(true);
end;

function TMines.GetPanelBevels(index: integer): TPanelBevel;
begin
  if index = 0 then
    result:= inherited BevelInner
  else
    result:= inherited BevelOuter;
end;

procedure TMines.SetBorderStyle(aStyle: TBorderStyle);
begin
  if aStyle <> BorderStyle then
  begin
    inherited BorderStyle:= aStyle;
    if fAutoSize then
      SetAutoSize(true);
  end;
end;

function  TMines.GetBorderStyle: TBorderStyle;
begin
  result:= inherited BorderStyle;
end;

procedure TMines.SetAutoSize(aAuto: boolean);
{- Size the control to fit around the LED display }
begin
  fAutoSize:= aAuto;
  if fAutoSize then // if autosize specified then resize control to fit LED
  begin
    Width:= TotalBorderWidth + (CellWidth * fFieldWidth);
    Height:= TotalBorderWidth + (CellHeight * fFieldHeight);
  end;
end;

procedure TMines.SetHorzAlign(anAlign: TFieldHorzAlign);
begin
  if (anAlign <> fHorzAlign) then
  begin
    fHorzAlign:= anAlign;
    Invalidate;
  end;
end;

procedure TMines.SetVertAlign(anAlign: TFieldVertAlign);
begin
  if (anAlign <> fVertAlign) then
  begin
    fVertAlign:= anAlign;
    Invalidate;
  end;
end;

procedure TMines.SetShowBombs(aShow: boolean);
begin
  fShowBombs:= aShow;
  PaintField;
end;

function TMines.MouseInField(x, y: integer): integer;
// return true if mouse is in the field of play.
// Also sets the cell index that the mouse coords indicate.
// returns >= 0 (valid play field cell index) if x,y in playfield,
// otherwise returns -1
var
  rect  : TRect;
  pt    : TPoint;
begin
  pt.x:= x;
  pt.y:= y;

  // set rect to the rectangle bounding the playing field.
  CalcStartingRect(rect);
  rect.right:= rect.left + fFieldWidth * cellWidth;
  rect.bottom:= rect.Top + fFieldHeight * CellHeight;

  // see if the mouse point is in the playing field rectangle
  if PtInRect(rect, pt) then
  begin
    // set x,y to the offset from the top/left of the playing field
    Dec(x, rect.left);
    Dec(y, rect.top);

    // calculate the button index of the mouse coords
    x:= x div CellWidth;  // x = column of mouse x,y (0-fFieldWidth)
    y:= y div CellHeight; // y = column of mouse x,y (0-fFieldHeight)
    result:= (y * fFieldWidth) + x; // calc cell index
  end
  else
    result:= -1;    // mouse outside of playing field.
end;

procedure TMines.SetCellDown(aCellIdx: integer; doPaint: boolean);
// press the specified cell down. if it is empty (no adjacent cells) then
// clear around it as well
var
  cell  : TCell;
  adj   : TAdjCell;
begin
  cell:= cells.items[aCellIdx];
  cell.isDown:= true;
  if cell.adjBombs = 0 then
  begin
    for adj:= AdjTopLeft to AdjBottomRight do
    begin
      cell:= AdjacentCell(aCellIdx, adj);
      if (cell <> nil) and (not cell.IsBomb) and (not cell.isDown) and
         (cell.Flagged <> fFlagged) then
        SetCellDown(cells.IndexOf(cell), false);
    end;
  end;

  if doPaint then
    PaintField;
end;

procedure TMines.SetCellPerimiterDown(aCellIdx: integer; doPaint: boolean);
var
  adj   : TAdjCell;
  cell  : TCell;
begin
  for adj:= AdjTopLeft to AdjBottomRight do
  begin
    cell:= AdjacentCell(aCellIdx, adj);
    if (cell <> nil) and (not cell.IsBomb) then
      SetCellDown(cells.IndexOf(cell), false);
  end;
  if doPaint then
    PaintField;
end;

procedure TMines.PressAround(aCellIdx: integer; pressed, doPaint: boolean);
var
  cell  : TCell;
  adj   : TAdjCell;

  procedure PressIt(theCell: TCell);
  begin
    if (not theCell.isDown) and (theCell.flagged <> fFlagged) then
      theCell.IsPressed:= pressed;
  end;

begin
  cell:= cells.items[aCellIdx];
  PressIt(cell);
  for adj:= AdjTopLeft to AdjBottomRight do
  begin
    cell:= AdjacentCell(aCellIdx, adj);
    if (cell <> nil) then
      PressIt(cell);
  end;

  if doPaint then
    PaintField;
end;

function TMines.CountFlagged(aCellIdx: integer): integer;
var
  aIdx  : TAdjCell;
  cell  : TCell;
begin
  result:= 0;
  for aIdx:= AdjTopLeft to AdjBottomRight do
  begin
    cell:= AdjacentCell(aCellIdx, aIdx);
    if cell <> nil then
      if cell.flagged = fFlagged then
        Inc(result);
  end;
end;

procedure TMines.MouseDown(button: TMouseButton; shift: TShiftState; x,y: integer);
var
  rect    : TRect;
  cellIdx : integer;
  cell    : TCell;
begin
  inherited;
  cellIdx:= MouseInField(x, y);
  if cellIdx >= 0 then
  begin
    clickedCell:= cellIdx;
    cell:= cells.items[cellIdx];
    if (ssRight in shift) and (ssLeft in shift) then  // both buttons
    begin
      SetCapture(handle);
      PressAround(cellIdx, true, false);
      clickedCell:= cellIdx;
      PaintField;
    end
    else if button = mbRight then
    begin
      if not cell.isDown then
      begin
        if cell.flagged = fQuestionFlag then
          cell.flagged:= fNotFlagged
        else
          Inc(cell.flagged);
        PaintField;
      end;
    end
    else if button = mbLeft then
    begin
      SetCapture(handle);
      clickedCell:= cellIdx;
      if cell.flagged <> fFlagged then
      begin
        cell.isPressed:= true;
        PaintField;
      end;
    end;
  end
  else
    clickedCell:= -1;
// see if mouse point is in playing field.
// if so, then set up rectangles, button index etc.
// capture the mouse.
// press the buttons
end;

procedure TMines.MouseUp(Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
var
  cell  : TCell;
  tmp   : TCell;
  k     : integer;
begin
  inherited;
  ReleaseCapture;
  if (clickedCell >= 0) then
  begin
    if ((button = mbLeft) and (ssRight in Shift)) or
       ((button = mbRight) and (ssLeft in Shift)) then
    begin
      // click all cells around the current cell that are not already down
      // and are not flagged.
      // The must be the correct number of cells flagged for the current
      // cell pressed.
      PressAround(clickedCell, false, false);
      cell:= cells.items[clickedCell];
      if cell.isDown then
      begin
        k:= CountFlagged(clickedCell);
        if k = cell.AdjBombs then
          SetCellPerimiterDown(clickedCell, false);
      end;
      clickedCell:= -1;
      PaintField;
    end
    else if (button = mbLeft) then
    begin
      cell:= cells.items[clickedCell];
      cell.isPressed:= false;
      if cell.isBomb then
      begin
        cell.IsDown:= true;
        cell.blown:= true;
        for k:= 0 to cells.count - 1 do
        begin
          if (k <> clickedCell) then
          begin
            tmp:= cells.items[k];
            if tmp.isBomb then
              tmp.isDown:= true;
          end;
        end;
      end
      else if cell.flagged <> fFlagged then
      begin
        SetCellDown(clickedCell, false);
      end;

      PaintField;
    end
    else if (clickedCell >= 0) and (button = mbRight) then
    begin
      cell:= cells.items[clickedCell];
      cell.isPressed:= false;
      PaintField;
      clickedCell:= -1;
    end;
  end;
end;

procedure TMines.MouseMove(Shift: TShiftState; X, Y: Integer);
var
  rect    : TRect;
  cellIdx : integer;
  cell    : TCell;
begin
  inherited;
  if GetCapture = handle then
  begin
    cellIdx:= MouseInField(x, y);
    if cellIdx <> clickedCell then
    begin
      if (ssRight in shift) and (ssLeft in shift) then  // both buttons
      begin
        if clickedCell >= 0 then
          PressAround(clickedCell, false, false); // clear last group
        if cellIdx >= 0 then
          PressAround(cellIdx, true, false);
        clickedCell:= cellIdx;
        PaintField;
      end
      else
      begin
        if clickedCell >= 0 then
          TCell(cells.items[clickedCell]).isPressed:= false;

        if (cellIdx >= 0) then
        begin
          cell:= cells.items[cellIdx];
          if cell.flagged <> fFLagged then
            cell.isPressed:= true;
        end;

        clickedCell:= cellIdx;
        PaintField;
      end
    end;
  end;
end;

function TMines.AdjacentCell(aCellIdx: integer; aAdjIdx: TAdjCell): TCell;
// return adjacent cell from cellIdx based on adjIdx
// returns nil if adjIdx is invalid

  function IsLeftCol(aCell: integer): boolean;
  begin
    result:= (aCell mod fFieldWidth) = 0;
  end;

  function IsRightCol(aCell: integer): boolean;
  begin
    result:= IsLeftCol(aCell + 1);
  end;

  function IsTopRow(aCell: integer): boolean;
  begin
    result:= (aCell < fFieldWidth) and (aCell >= 0);
  end;

  function IsBottomRow(aCell: integer): boolean;
  begin
    result:= (aCell >= 0) and
             (aCell >= ((fFieldWidth * fFieldHeight) - fFieldWidth)) and
             (aCell < (fFieldWidth * fFieldHeight));
  end;

begin
  result:= nil;
  case aAdjIdx of
    AdjLeft:
      if not IsLeftCol(aCellIdx) then
        result:= cells.items[aCellIdx - 1];
    AdjRight:
      if not IsRightCol(aCellIdx) then
        result:= cells.items[aCellIdx + 1];
    AdjTopLeft:
      if not (IsLeftCol(aCellIdx) or IsTopRow(aCellIdx)) then
        result:= cells.items[aCellIdx - fFieldWidth - 1];
    AdjTopMiddle:
      if not IsTopRow(aCellIdx) then
        result:= cells.items[aCellIdx - fFieldWidth];
    AdjTopRight:
      if not (IsRightCol(aCellIdx) or IsTopRow(aCellIdx)) then
        result:= cells.items[aCellIdx - fFieldWidth + 1];
    AdjBottomLeft:
      if not (IsLeftCol(aCellIdx) or IsBottomRow(aCellIdx)) then
        result:= cells.items[aCellIdx + fFieldWidth - 1];
    AdjBottomMiddle:
      if not (IsBottomRow(aCellIdx)) then
        result:= cells.items[aCellIdx + fFieldWidth];
    AdjBottomRight:
      if not (IsRightCol(aCellIdx) or IsBottomRow(aCellIdx)) then
        result:= cells.items[aCellIdx + fFieldWidth + 1];
  end;
end;

procedure TMines.SetMines(aNum: integer);
// set number of mines in playing field. Also clears all current cell info.
var
  k   : integer;
  idx : integer;


  procedure CountAllAdjacentBombs;
  var
    aIdx  : TAdjCell;
    cIdx  : integer;
    count : integer;
    cell  : TCell;
  begin
    for cIdx:= 0 to cells.count - 1 do
    begin
      count:= 0;
      for aIdx:= AdjTopLeft to AdjBottomRight do
      begin
        cell:= AdjacentCell(cIdx, aIdx);
        if cell <> nil then
          if cell.isBomb then
            Inc(count);
      end;
      TCell(cells.items[cIdx]).AdjBombs:= count;
    end;
  end;

begin
  if (aNum >= 0) and (aNum <= fFieldWidth * fFieldHeight) then
  begin
    for k:= 0 to cells.count - 1 do // clear all current mines info
      TCell(cells.Items[k]).ResetCell;

    fNumMines:= aNum;

    for k:= 1 to fNumMines do
    begin
      repeat
        idx:= Random(cells.count);
        if TCell(cells.items[idx]).isBomb then
          idx:= -1
        else
          TCell(cells.items[idx]).isBomb:= true;
      until idx >= 0;
    end;
    CountAllAdjacentBombs;
    PaintField;
  end;
end;

procedure Register;
begin
  RegisterComponents('Custom', [TMines]);
end;

END.
