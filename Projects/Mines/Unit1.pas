unit Unit1;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Spin, ExtCtrls, Buttons, MinePnl, MPlayer, led, ComCtrls;

type
  TForm1 = class(TForm)
    Image1: TImage;
    Button3: TButton;
    Panel1: TPanel;
    Bevel1: TBevel;
    LED1: TLED;
    LED2: TLED;
    Button1: TButton;
    SpinEdit1: TSpinEdit;
    Button2: TButton;
    Mines1: TMines;
    Mines2: TMines;
    TabControl1: TTabControl;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    TabSheet3: TTabSheet;
    procedure Button1Click(Sender: TObject);
    procedure Button3Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure SpinEdit1Change(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

{$R *.DFM}

procedure TForm1.Button1Click(Sender: TObject);
begin
  Close;
end;

procedure TForm1.Button3Click(Sender: TObject);
begin
  mines1.refresh;
end;

procedure TForm1.Button2Click(Sender: TObject);
begin
  mines1.mines:= mines1.Mines;
end;

procedure TForm1.SpinEdit1Change(Sender: TObject);
begin
  mines1.mines:= spinedit1.value;
  spinedit1.value:= mines1.mines;
end;

procedure TForm1.FormCreate(Sender: TObject);
begin
  spinedit1.value:= mines1.mines;
end;

end.
