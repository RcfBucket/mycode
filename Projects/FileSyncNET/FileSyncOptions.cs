using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraTreeList.Nodes;

namespace FileSyncNET
{
  public partial class FileSyncOptions : Form
  {
    private class MyData
    {
      private string fSrcFolder;
      private string fDstFolder;
      private string fProfileName;

      public string ProfileName
      {
        get { return fProfileName; }

      }

      public string SrcFolder
      {
        get { return fSrcFolder; }
        set { fSrcFolder = value; }
      }
      public string DstFolder
      {
        get { return fDstFolder; }
        set { fDstFolder = value; }
      }

      public MyData()
      {
        fSrcFolder = "MyDataSrc";
        fProfileName = "MyProfile";
        fDstFolder = "MyDst";
      }
    }

    private List<TProfileBase> fProfiles;
    private List<MyData> fMyData;

    public FileSyncOptions()
    {
      TProfileGroup pg;
      TProfile p;

      InitializeComponent();

      fProfiles = new List<TProfileBase>();

      pg = new TProfileGroup("profile group 1");
      fProfiles.Add(pg);
      fProfiles.Add(new TProfileGroup("profile group 2"));

      p = new TProfile("profile 1");
      fProfiles.Add(p);
      pg.Profiles.Add(p);
      p = new TProfile("profile 2");
      fProfiles.Add(p);
      pg.Profiles.Add(p);
    }

    private void FillProfileList()
    {
      TreeListNode profileNode;
      TreeListNode srcNode, dstNode;
      TreeListNode tmpNode;
      TProfile prof;
      TProfileGroup profGroup;

      treeProfiles.BeginUnboundLoad();
      treeProfileGroups.BeginUnboundLoad();

      treeProfiles.ClearNodes();
      treeProfileGroups.ClearNodes();
      try
      {
        // fill folder names
        foreach (TProfileBase p in fProfiles)
        {
          if (p.ProfileType == TProfileType.ptProfile)
          {
            prof = (TProfile) p;
            profileNode = treeProfiles.AppendNode(new object[] { prof }, null);
            profileNode.StateImageIndex = 0;
            profileNode.HasChildren = true;
            profileNode.Tag = (TProfile)prof;
            srcNode = treeProfiles.AppendNode(new object[] { "Source Folder", prof.SrcFolder }, profileNode);
            srcNode.Tag = prof;
            srcNode.StateImageIndex = 1;

            dstNode = treeProfiles.AppendNode(new object[] { "Destination Folder", prof.DstFolder }, profileNode);
            dstNode.Tag = prof;
            dstNode.StateImageIndex = 2;
          }
          else
          {
            profGroup = (TProfileGroup)p;
            profileNode = treeProfileGroups.AppendNode(new object[] { profGroup }, null);
            profileNode.Tag = profGroup;
            profileNode.StateImageIndex = 3;
            if (profGroup.Profiles.Count > 0)
            {
              profileNode.HasChildren = true;
              foreach (TProfile p2 in profGroup.Profiles)
              {
                tmpNode = treeProfileGroups.AppendNode(new object[] { p2 }, profileNode);
                tmpNode.Tag = p2;
                tmpNode.StateImageIndex = 0;
              }
            }
            else
              profileNode.HasChildren = false;
          }
        }
      }
      finally
      {
        treeProfiles.EndUnboundLoad();
        treeProfileGroups.EndUnboundLoad();
      }
    }

    private void FileSyncOptions_Load(object sender, EventArgs e)
    {
      FillProfileList();

      fMyData = new List<MyData>();
      fMyData.Add(new MyData());

      gridControl1.DataSource = fMyData;
    }

    private void treeProfiles_ShowingEditor(object sender, CancelEventArgs e)
    {
      // do not allow editing of second level nodes as these are just identifiers to Source or Destination
      if ((treeProfiles.FocusedNode.Level == 0) && (treeProfiles.FocusedColumn.AbsoluteIndex == 1))
        e.Cancel = true;
      else if ((treeProfiles.FocusedNode.Level == 1) && (treeProfiles.FocusedColumn.AbsoluteIndex == 0))
        e.Cancel = true;
    }

    private void treeProfiles_CellValueChanged(object sender, DevExpress.XtraTreeList.CellValueChangedEventArgs e)
    {
      TProfile p;

      if (e.Node.Level == 0)
      {
        labelControl1.Text = string.Format("ProfEdited: {0}", e.Node.Tag);
        p = (TProfile) e.Node.Tag;
        p.ProfileName = e.Value.ToString();
        treeProfileGroups.Refresh();
      }
      else if (e.Node.Level == 1)
      {
        p = (TProfile) e.Node.Tag;
        labelControl1.Text = string.Format("Folder: {0} Parent: {1}", e.Node.Tag, p);
        p.SrcFolder = e.Value.ToString();
      }
    }

    private void treeProfiles_FocusedNodeChanged(object sender, DevExpress.XtraTreeList.FocusedNodeChangedEventArgs e)
    {
      if (e.Node != null)
        reposProfileEdit.Buttons[0].Visible = (e.Node.Level == 1);
    }

    private void reposProfileEdit_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
    {
      MessageBox.Show("Button Clicked here, show folder list");
    }

    private void treeProfileGroups_CellValueChanged(object sender, DevExpress.XtraTreeList.CellValueChangedEventArgs e)
    {
      if (e.Node.Level == 0)
      {
        TProfileGroup pg = (TProfileGroup) e.Node.Tag;
        pg.ProfileName = e.Value.ToString();
      }
      else
      {
        TProfile p = (TProfile)e.Node.Tag;
        p.ProfileName = e.Value.ToString();
        treeProfiles.Refresh();
      }
    }

  }
}