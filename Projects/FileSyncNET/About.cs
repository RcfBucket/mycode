using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
using System.Reflection;

namespace FileSyncNET
{
  public partial class About : Form
  {
    public About()
    {
      InitializeComponent();

      this.lblCopyright.Text = AssemblyCopyright;
    }

    private void lblReleaseNotes_MouseEnter(object sender, EventArgs e)
    {
      lblReleaseNotes.Appearance.Font = new Font(lblReleaseNotes.Appearance.Font, lblReleaseNotes.Appearance.Font.Style | FontStyle.Underline);
    }

    private void lblReleaseNotes_MouseLeave(object sender, EventArgs e)
    {
      lblReleaseNotes.Appearance.Font = new Font(lblReleaseNotes.Appearance.Font, FontStyle.Regular);
    }

    private void btnOK_Click(object sender, EventArgs e)
    {
      Close();
    }

    private void lblReleaseNotes_Click(object sender, EventArgs e)
    {
      MessageBox.Show("Show Release Notes Here...", "Caption", MessageBoxButtons.OKCancel, MessageBoxIcon.Information);
    }

    public string AssemblyCopyright
    {
      get
      {
        // Get all Copyright attributes on this assembly
        object[] attributes = Assembly.GetExecutingAssembly().GetCustomAttributes(typeof(AssemblyCopyrightAttribute), false);
        // If there aren't any Copyright attributes, return an empty string
        if (attributes.Length == 0)
          return "";
        // If there is a Copyright attribute, return its value
        return ((AssemblyCopyrightAttribute)attributes[0]).Copyright;
      }
    }
  }
}