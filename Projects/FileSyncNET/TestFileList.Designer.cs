namespace FileSyncNET
{
  partial class TestFileList
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.components = new System.ComponentModel.Container();
      System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(TestFileList));
      this.imageList1 = new System.Windows.Forms.ImageList(this.components);
      this.xtraTabControl1 = new DevExpress.XtraTab.XtraTabControl();
      this.xtraTabPage1 = new DevExpress.XtraTab.XtraTabPage();
      this.treeFolders = new DevExpress.XtraTreeList.TreeList();
      this.colFullName = new DevExpress.XtraTreeList.Columns.TreeListColumn();
      this.colName = new DevExpress.XtraTreeList.Columns.TreeListColumn();
      this.colType = new DevExpress.XtraTreeList.Columns.TreeListColumn();
      this.colSize = new DevExpress.XtraTreeList.Columns.TreeListColumn();
      this.xtraTabPage2 = new DevExpress.XtraTab.XtraTabPage();
      this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
      this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
      this.lblCount = new DevExpress.XtraEditors.LabelControl();
      this.lbFolderList = new DevExpress.XtraEditors.ListBoxControl();
      this.btnFolderList = new DevExpress.XtraEditors.SimpleButton();
      this.lblTotalBytes = new DevExpress.XtraEditors.LabelControl();
      this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
      this.lblTotalFiles = new DevExpress.XtraEditors.LabelControl();
      this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
      this.lblTotalFolders = new DevExpress.XtraEditors.LabelControl();
      this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
      this.btnCalcTotals = new DevExpress.XtraEditors.SimpleButton();
      this.checkEdit1 = new DevExpress.XtraEditors.CheckEdit();
      ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl1)).BeginInit();
      this.xtraTabControl1.SuspendLayout();
      this.xtraTabPage1.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.treeFolders)).BeginInit();
      this.xtraTabPage2.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.lbFolderList)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.checkEdit1.Properties)).BeginInit();
      this.SuspendLayout();
      // 
      // imageList1
      // 
      this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
      this.imageList1.TransparentColor = System.Drawing.Color.Magenta;
      this.imageList1.Images.SetKeyName(0, "");
      this.imageList1.Images.SetKeyName(1, "");
      this.imageList1.Images.SetKeyName(2, "");
      // 
      // xtraTabControl1
      // 
      this.xtraTabControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                  | System.Windows.Forms.AnchorStyles.Left)
                  | System.Windows.Forms.AnchorStyles.Right)));
      this.xtraTabControl1.Location = new System.Drawing.Point(12, 12);
      this.xtraTabControl1.Name = "xtraTabControl1";
      this.xtraTabControl1.SelectedTabPage = this.xtraTabPage1;
      this.xtraTabControl1.Size = new System.Drawing.Size(753, 473);
      this.xtraTabControl1.TabIndex = 5;
      this.xtraTabControl1.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.xtraTabPage1,
            this.xtraTabPage2});
      // 
      // xtraTabPage1
      // 
      this.xtraTabPage1.Controls.Add(this.treeFolders);
      this.xtraTabPage1.Name = "xtraTabPage1";
      this.xtraTabPage1.Size = new System.Drawing.Size(747, 446);
      this.xtraTabPage1.Text = "Folder List";
      // 
      // treeFolders
      // 
      this.treeFolders.Columns.AddRange(new DevExpress.XtraTreeList.Columns.TreeListColumn[] {
            this.colFullName,
            this.colName,
            this.colType,
            this.colSize});
      this.treeFolders.Dock = System.Windows.Forms.DockStyle.Fill;
      this.treeFolders.Location = new System.Drawing.Point(0, 0);
      this.treeFolders.Name = "treeFolders";
      this.treeFolders.BeginUnboundLoad();
      this.treeFolders.AppendNode(new object[] {
            null,
            null,
            null,
            null}, -1, 0, 0, 0);
      this.treeFolders.AppendNode(new object[] {
            null,
            null,
            null,
            null}, 0);
      this.treeFolders.EndUnboundLoad();
      this.treeFolders.OptionsBehavior.Editable = false;
      this.treeFolders.OptionsSelection.EnableAppearanceFocusedCell = false;
      this.treeFolders.Size = new System.Drawing.Size(747, 446);
      this.treeFolders.StateImageList = this.imageList1;
      this.treeFolders.TabIndex = 1;
      // 
      // colFullName
      // 
      this.colFullName.Caption = "FullName";
      this.colFullName.FieldName = "FullName";
      this.colFullName.Name = "colFullName";
      this.colFullName.OptionsColumn.AllowEdit = false;
      this.colFullName.OptionsColumn.ReadOnly = true;
      this.colFullName.Width = 291;
      // 
      // colName
      // 
      this.colName.Caption = "Name";
      this.colName.FieldName = "Name";
      this.colName.MinWidth = 63;
      this.colName.Name = "colName";
      this.colName.OptionsColumn.AllowEdit = false;
      this.colName.OptionsColumn.ReadOnly = true;
      this.colName.Visible = true;
      this.colName.VisibleIndex = 0;
      this.colName.Width = 204;
      // 
      // colType
      // 
      this.colType.Caption = "Type";
      this.colType.FieldName = "Type";
      this.colType.Name = "colType";
      this.colType.Visible = true;
      this.colType.VisibleIndex = 1;
      this.colType.Width = 112;
      // 
      // colSize
      // 
      this.colSize.Caption = "Size";
      this.colSize.FieldName = "Size";
      this.colSize.Name = "colSize";
      this.colSize.Visible = true;
      this.colSize.VisibleIndex = 2;
      this.colSize.Width = 116;
      // 
      // xtraTabPage2
      // 
      this.xtraTabPage2.Controls.Add(this.labelControl5);
      this.xtraTabPage2.Controls.Add(this.labelControl2);
      this.xtraTabPage2.Controls.Add(this.lblCount);
      this.xtraTabPage2.Controls.Add(this.lbFolderList);
      this.xtraTabPage2.Controls.Add(this.btnFolderList);
      this.xtraTabPage2.Controls.Add(this.lblTotalBytes);
      this.xtraTabPage2.Controls.Add(this.labelControl4);
      this.xtraTabPage2.Controls.Add(this.lblTotalFiles);
      this.xtraTabPage2.Controls.Add(this.labelControl3);
      this.xtraTabPage2.Controls.Add(this.lblTotalFolders);
      this.xtraTabPage2.Controls.Add(this.labelControl1);
      this.xtraTabPage2.Controls.Add(this.btnCalcTotals);
      this.xtraTabPage2.Name = "xtraTabPage2";
      this.xtraTabPage2.Size = new System.Drawing.Size(747, 446);
      this.xtraTabPage2.Text = "Methods";
      // 
      // labelControl5
      // 
      this.labelControl5.Location = new System.Drawing.Point(149, 262);
      this.labelControl5.Name = "labelControl5";
      this.labelControl5.Size = new System.Drawing.Size(63, 13);
      this.labelControl5.TabIndex = 15;
      this.labelControl5.Text = "labelControl5";
      // 
      // labelControl2
      // 
      this.labelControl2.Location = new System.Drawing.Point(148, 276);
      this.labelControl2.Name = "labelControl2";
      this.labelControl2.Size = new System.Drawing.Size(63, 13);
      this.labelControl2.TabIndex = 14;
      this.labelControl2.Text = "labelControl2";
      // 
      // lblCount
      // 
      this.lblCount.Location = new System.Drawing.Point(16, 124);
      this.lblCount.Name = "lblCount";
      this.lblCount.Size = new System.Drawing.Size(63, 13);
      this.lblCount.TabIndex = 13;
      this.lblCount.Text = "labelControl2";
      // 
      // lbFolderList
      // 
      this.lbFolderList.Location = new System.Drawing.Point(149, 95);
      this.lbFolderList.Name = "lbFolderList";
      this.lbFolderList.Size = new System.Drawing.Size(592, 161);
      this.lbFolderList.TabIndex = 12;
      this.lbFolderList.SelectedIndexChanged += new System.EventHandler(this.lbFolderList_SelectedIndexChanged);
      // 
      // btnFolderList
      // 
      this.btnFolderList.Location = new System.Drawing.Point(16, 95);
      this.btnFolderList.Name = "btnFolderList";
      this.btnFolderList.Size = new System.Drawing.Size(104, 23);
      this.btnFolderList.TabIndex = 11;
      this.btnFolderList.Text = "Full Folder List";
      this.btnFolderList.Click += new System.EventHandler(this.btnFolderList_Click);
      // 
      // lblTotalBytes
      // 
      this.lblTotalBytes.Location = new System.Drawing.Point(231, 59);
      this.lblTotalBytes.Name = "lblTotalBytes";
      this.lblTotalBytes.Size = new System.Drawing.Size(61, 13);
      this.lblTotalBytes.TabIndex = 10;
      this.lblTotalBytes.Text = "lblTotalBytes";
      // 
      // labelControl4
      // 
      this.labelControl4.Location = new System.Drawing.Point(149, 59);
      this.labelControl4.Name = "labelControl4";
      this.labelControl4.Size = new System.Drawing.Size(54, 13);
      this.labelControl4.TabIndex = 9;
      this.labelControl4.Text = "Total Bytes";
      // 
      // lblTotalFiles
      // 
      this.lblTotalFiles.Location = new System.Drawing.Point(231, 40);
      this.lblTotalFiles.Name = "lblTotalFiles";
      this.lblTotalFiles.Size = new System.Drawing.Size(55, 13);
      this.lblTotalFiles.TabIndex = 8;
      this.lblTotalFiles.Text = "lblTotalFiles";
      // 
      // labelControl3
      // 
      this.labelControl3.Location = new System.Drawing.Point(149, 40);
      this.labelControl3.Name = "labelControl3";
      this.labelControl3.Size = new System.Drawing.Size(48, 13);
      this.labelControl3.TabIndex = 7;
      this.labelControl3.Text = "Total Files";
      // 
      // lblTotalFolders
      // 
      this.lblTotalFolders.Location = new System.Drawing.Point(231, 21);
      this.lblTotalFolders.Name = "lblTotalFolders";
      this.lblTotalFolders.Size = new System.Drawing.Size(69, 13);
      this.lblTotalFolders.TabIndex = 6;
      this.lblTotalFolders.Text = "lblTotalFolders";
      // 
      // labelControl1
      // 
      this.labelControl1.Location = new System.Drawing.Point(149, 21);
      this.labelControl1.Name = "labelControl1";
      this.labelControl1.Size = new System.Drawing.Size(62, 13);
      this.labelControl1.TabIndex = 5;
      this.labelControl1.Text = "Total Folders";
      // 
      // btnCalcTotals
      // 
      this.btnCalcTotals.Location = new System.Drawing.Point(16, 16);
      this.btnCalcTotals.Name = "btnCalcTotals";
      this.btnCalcTotals.Size = new System.Drawing.Size(104, 23);
      this.btnCalcTotals.TabIndex = 0;
      this.btnCalcTotals.Text = "Calculate Totals";
      this.btnCalcTotals.Click += new System.EventHandler(this.btnCalcTotals_Click);
      // 
      // checkEdit1
      // 
      this.checkEdit1.Location = new System.Drawing.Point(13, 510);
      this.checkEdit1.Name = "checkEdit1";
      this.checkEdit1.Properties.Caption = "Show Full Name";
      this.checkEdit1.Size = new System.Drawing.Size(160, 19);
      this.checkEdit1.TabIndex = 6;
      this.checkEdit1.CheckedChanged += new System.EventHandler(this.checkEdit1_CheckedChanged);
      // 
      // TestFileList
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.ClientSize = new System.Drawing.Size(777, 541);
      this.Controls.Add(this.checkEdit1);
      this.Controls.Add(this.xtraTabControl1);
      this.Name = "TestFileList";
      this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
      this.Text = "Test TSyncFolder Class";
      this.Load += new System.EventHandler(this.TestFileList_Load);
      ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl1)).EndInit();
      this.xtraTabControl1.ResumeLayout(false);
      this.xtraTabPage1.ResumeLayout(false);
      ((System.ComponentModel.ISupportInitialize)(this.treeFolders)).EndInit();
      this.xtraTabPage2.ResumeLayout(false);
      this.xtraTabPage2.PerformLayout();
      ((System.ComponentModel.ISupportInitialize)(this.lbFolderList)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.checkEdit1.Properties)).EndInit();
      this.ResumeLayout(false);

    }

    #endregion

    private System.Windows.Forms.ImageList imageList1;
    private DevExpress.XtraTab.XtraTabControl xtraTabControl1;
    private DevExpress.XtraTab.XtraTabPage xtraTabPage1;
    private DevExpress.XtraTreeList.TreeList treeFolders;
    private DevExpress.XtraTreeList.Columns.TreeListColumn colFullName;
    private DevExpress.XtraTreeList.Columns.TreeListColumn colName;
    private DevExpress.XtraTreeList.Columns.TreeListColumn colType;
    private DevExpress.XtraTreeList.Columns.TreeListColumn colSize;
    private DevExpress.XtraTab.XtraTabPage xtraTabPage2;
    private DevExpress.XtraEditors.SimpleButton btnCalcTotals;
    private DevExpress.XtraEditors.LabelControl lblTotalBytes;
    private DevExpress.XtraEditors.LabelControl labelControl4;
    private DevExpress.XtraEditors.LabelControl lblTotalFiles;
    private DevExpress.XtraEditors.LabelControl labelControl3;
    private DevExpress.XtraEditors.LabelControl lblTotalFolders;
    private DevExpress.XtraEditors.LabelControl labelControl1;
    private DevExpress.XtraEditors.CheckEdit checkEdit1;
    private DevExpress.XtraEditors.SimpleButton btnFolderList;
    private DevExpress.XtraEditors.ListBoxControl lbFolderList;
    private DevExpress.XtraEditors.LabelControl lblCount;
    private DevExpress.XtraEditors.LabelControl labelControl5;
    private DevExpress.XtraEditors.LabelControl labelControl2;
  }
}