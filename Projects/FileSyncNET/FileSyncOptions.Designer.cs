namespace FileSyncNET
{
  partial class FileSyncOptions
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.components = new System.ComponentModel.Container();
      System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FileSyncOptions));
      this.buttonEdit5 = new DevExpress.XtraEditors.ButtonEdit();
      this.buttonEdit4 = new DevExpress.XtraEditors.ButtonEdit();
      this.labelControl8 = new DevExpress.XtraEditors.LabelControl();
      this.labelControl7 = new DevExpress.XtraEditors.LabelControl();
      this.chkIncludeSystemFolders = new DevExpress.XtraEditors.CheckEdit();
      this.chkIncludeSubfolders = new DevExpress.XtraEditors.CheckEdit();
      this.labelControl6 = new DevExpress.XtraEditors.LabelControl();
      this.buttonEdit3 = new DevExpress.XtraEditors.ButtonEdit();
      this.treeProfiles = new DevExpress.XtraTreeList.TreeList();
      this.ColProfileName = new DevExpress.XtraTreeList.Columns.TreeListColumn();
      this.ColFolder = new DevExpress.XtraTreeList.Columns.TreeListColumn();
      this.reposProfileEdit = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
      this.imageList1 = new System.Windows.Forms.ImageList(this.components);
      this.treeProfileGroups = new DevExpress.XtraTreeList.TreeList();
      this.colProfileGroupName = new DevExpress.XtraTreeList.Columns.TreeListColumn();
      this.btnAdd = new DevExpress.XtraEditors.SimpleButton();
      this.simpleButton2 = new DevExpress.XtraEditors.SimpleButton();
      this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
      this.gridControl1 = new DevExpress.XtraGrid.GridControl();
      this.gvProfiles = new DevExpress.XtraGrid.Views.Grid.GridView();
      this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
      this.gridColumn2 = new DevExpress.XtraGrid.Columns.GridColumn();
      this.gridColumn3 = new DevExpress.XtraGrid.Columns.GridColumn();
      this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
      ((System.ComponentModel.ISupportInitialize)(this.buttonEdit5.Properties)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.buttonEdit4.Properties)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.chkIncludeSystemFolders.Properties)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.chkIncludeSubfolders.Properties)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.buttonEdit3.Properties)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.treeProfiles)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.reposProfileEdit)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.treeProfileGroups)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.gvProfiles)).BeginInit();
      this.SuspendLayout();
      // 
      // buttonEdit5
      // 
      this.buttonEdit5.Location = new System.Drawing.Point(470, 32);
      this.buttonEdit5.Name = "buttonEdit5";
      this.buttonEdit5.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
      this.buttonEdit5.Size = new System.Drawing.Size(365, 20);
      this.buttonEdit5.TabIndex = 14;
      // 
      // buttonEdit4
      // 
      this.buttonEdit4.Location = new System.Drawing.Point(12, 70);
      this.buttonEdit4.Name = "buttonEdit4";
      this.buttonEdit4.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
      this.buttonEdit4.Size = new System.Drawing.Size(365, 20);
      this.buttonEdit4.TabIndex = 13;
      // 
      // labelControl8
      // 
      this.labelControl8.Location = new System.Drawing.Point(470, 13);
      this.labelControl8.Name = "labelControl8";
      this.labelControl8.Size = new System.Drawing.Size(74, 13);
      this.labelControl8.TabIndex = 12;
      this.labelControl8.Text = "Files to Exclude";
      // 
      // labelControl7
      // 
      this.labelControl7.Location = new System.Drawing.Point(12, 51);
      this.labelControl7.Name = "labelControl7";
      this.labelControl7.Size = new System.Drawing.Size(72, 13);
      this.labelControl7.TabIndex = 11;
      this.labelControl7.Text = "Files to Include";
      // 
      // chkIncludeSystemFolders
      // 
      this.chkIncludeSystemFolders.Location = new System.Drawing.Point(187, 15);
      this.chkIncludeSystemFolders.Name = "chkIncludeSystemFolders";
      this.chkIncludeSystemFolders.Properties.Caption = "Include system folders";
      this.chkIncludeSystemFolders.Size = new System.Drawing.Size(145, 19);
      this.chkIncludeSystemFolders.TabIndex = 10;
      // 
      // chkIncludeSubfolders
      // 
      this.chkIncludeSubfolders.Location = new System.Drawing.Point(12, 15);
      this.chkIncludeSubfolders.Name = "chkIncludeSubfolders";
      this.chkIncludeSubfolders.Properties.Caption = "Include sub-folders";
      this.chkIncludeSubfolders.Size = new System.Drawing.Size(144, 19);
      this.chkIncludeSubfolders.TabIndex = 9;
      // 
      // labelControl6
      // 
      this.labelControl6.Location = new System.Drawing.Point(470, 65);
      this.labelControl6.Name = "labelControl6";
      this.labelControl6.Size = new System.Drawing.Size(118, 13);
      this.labelControl6.TabIndex = 16;
      this.labelControl6.Text = "Text Difference Program";
      // 
      // buttonEdit3
      // 
      this.buttonEdit3.Location = new System.Drawing.Point(470, 84);
      this.buttonEdit3.Name = "buttonEdit3";
      this.buttonEdit3.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
      this.buttonEdit3.Size = new System.Drawing.Size(365, 20);
      this.buttonEdit3.TabIndex = 15;
      // 
      // treeProfiles
      // 
      this.treeProfiles.Columns.AddRange(new DevExpress.XtraTreeList.Columns.TreeListColumn[] {
            this.ColProfileName,
            this.ColFolder});
      this.treeProfiles.Location = new System.Drawing.Point(14, 119);
      this.treeProfiles.Name = "treeProfiles";
      this.treeProfiles.BeginUnboundLoad();
      this.treeProfiles.AppendNode(new object[] {
            "Profile 1",
            null}, -1);
      this.treeProfiles.AppendNode(new object[] {
            "Source",
            "E:\\Test2"}, 0);
      this.treeProfiles.AppendNode(new object[] {
            "Dest",
            "I:\\Test2"}, 0);
      this.treeProfiles.EndUnboundLoad();
      this.treeProfiles.OptionsBehavior.ImmediateEditor = false;
      this.treeProfiles.OptionsSelection.InvertSelection = true;
      this.treeProfiles.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.reposProfileEdit});
      this.treeProfiles.Size = new System.Drawing.Size(424, 217);
      this.treeProfiles.StateImageList = this.imageList1;
      this.treeProfiles.TabIndex = 18;
      this.treeProfiles.ShowingEditor += new System.ComponentModel.CancelEventHandler(this.treeProfiles_ShowingEditor);
      this.treeProfiles.FocusedNodeChanged += new DevExpress.XtraTreeList.FocusedNodeChangedEventHandler(this.treeProfiles_FocusedNodeChanged);
      this.treeProfiles.CellValueChanged += new DevExpress.XtraTreeList.CellValueChangedEventHandler(this.treeProfiles_CellValueChanged);
      // 
      // ColProfileName
      // 
      this.ColProfileName.Caption = "Profiles";
      this.ColProfileName.FieldName = "ProfileName";
      this.ColProfileName.MinWidth = 63;
      this.ColProfileName.Name = "ColProfileName";
      this.ColProfileName.Visible = true;
      this.ColProfileName.VisibleIndex = 0;
      this.ColProfileName.Width = 126;
      // 
      // ColFolder
      // 
      this.ColFolder.Caption = "Folder";
      this.ColFolder.ColumnEdit = this.reposProfileEdit;
      this.ColFolder.FieldName = "FolderName";
      this.ColFolder.Name = "ColFolder";
      this.ColFolder.Visible = true;
      this.ColFolder.VisibleIndex = 1;
      this.ColFolder.Width = 277;
      // 
      // reposProfileEdit
      // 
      this.reposProfileEdit.AutoHeight = false;
      this.reposProfileEdit.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
      this.reposProfileEdit.Name = "reposProfileEdit";
      this.reposProfileEdit.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.reposProfileEdit_ButtonClick);
      // 
      // imageList1
      // 
      this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
      this.imageList1.TransparentColor = System.Drawing.Color.Fuchsia;
      this.imageList1.Images.SetKeyName(0, "notes.bmp");
      this.imageList1.Images.SetKeyName(1, "folder1.bmp");
      this.imageList1.Images.SetKeyName(2, "folder2.bmp");
      this.imageList1.Images.SetKeyName(3, "Groups.bmp");
      // 
      // treeProfileGroups
      // 
      this.treeProfileGroups.Columns.AddRange(new DevExpress.XtraTreeList.Columns.TreeListColumn[] {
            this.colProfileGroupName});
      this.treeProfileGroups.Location = new System.Drawing.Point(14, 356);
      this.treeProfileGroups.Name = "treeProfileGroups";
      this.treeProfileGroups.OptionsBehavior.ImmediateEditor = false;
      this.treeProfileGroups.OptionsSelection.InvertSelection = true;
      this.treeProfileGroups.Size = new System.Drawing.Size(424, 200);
      this.treeProfileGroups.StateImageList = this.imageList1;
      this.treeProfileGroups.TabIndex = 19;
      this.treeProfileGroups.CellValueChanged += new DevExpress.XtraTreeList.CellValueChangedEventHandler(this.treeProfileGroups_CellValueChanged);
      // 
      // colProfileGroupName
      // 
      this.colProfileGroupName.Caption = "Profile Groups";
      this.colProfileGroupName.FieldName = "ProfileGroupName";
      this.colProfileGroupName.MinWidth = 27;
      this.colProfileGroupName.Name = "colProfileGroupName";
      this.colProfileGroupName.Visible = true;
      this.colProfileGroupName.VisibleIndex = 0;
      // 
      // btnAdd
      // 
      this.btnAdd.Location = new System.Drawing.Point(653, 119);
      this.btnAdd.Name = "btnAdd";
      this.btnAdd.Size = new System.Drawing.Size(75, 23);
      this.btnAdd.TabIndex = 20;
      this.btnAdd.Text = "Add";
      // 
      // simpleButton2
      // 
      this.simpleButton2.Location = new System.Drawing.Point(743, 119);
      this.simpleButton2.Name = "simpleButton2";
      this.simpleButton2.Size = new System.Drawing.Size(75, 23);
      this.simpleButton2.TabIndex = 21;
      this.simpleButton2.Text = "Delete";
      // 
      // labelControl1
      // 
      this.labelControl1.Location = new System.Drawing.Point(519, 155);
      this.labelControl1.Name = "labelControl1";
      this.labelControl1.Size = new System.Drawing.Size(63, 13);
      this.labelControl1.TabIndex = 22;
      this.labelControl1.Text = "labelControl1";
      // 
      // gridControl1
      // 
      this.gridControl1.EmbeddedNavigator.Name = "";
      this.gridControl1.Location = new System.Drawing.Point(458, 189);
      this.gridControl1.MainView = this.gvProfiles;
      this.gridControl1.Name = "gridControl1";
      this.gridControl1.Size = new System.Drawing.Size(490, 367);
      this.gridControl1.TabIndex = 24;
      this.gridControl1.UseEmbeddedNavigator = true;
      this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gvProfiles});
      // 
      // gvProfiles
      // 
      this.gvProfiles.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn1,
            this.gridColumn2,
            this.gridColumn3});
      this.gvProfiles.GridControl = this.gridControl1;
      this.gvProfiles.Name = "gvProfiles";
      this.gvProfiles.OptionsSelection.InvertSelection = true;
      // 
      // gridColumn1
      // 
      this.gridColumn1.Caption = "Profile";
      this.gridColumn1.FieldName = "ProfileName";
      this.gridColumn1.Name = "gridColumn1";
      this.gridColumn1.Visible = true;
      this.gridColumn1.VisibleIndex = 0;
      // 
      // gridColumn2
      // 
      this.gridColumn2.Caption = "Source Folder";
      this.gridColumn2.FieldName = "SrcFolder";
      this.gridColumn2.Name = "gridColumn2";
      this.gridColumn2.Visible = true;
      this.gridColumn2.VisibleIndex = 1;
      // 
      // gridColumn3
      // 
      this.gridColumn3.Caption = "Destination Folder";
      this.gridColumn3.FieldName = "DstFolder";
      this.gridColumn3.Name = "gridColumn3";
      this.gridColumn3.Visible = true;
      this.gridColumn3.VisibleIndex = 2;
      // 
      // labelControl2
      // 
      this.labelControl2.Location = new System.Drawing.Point(6, 96);
      this.labelControl2.Name = "labelControl2";
      this.labelControl2.Size = new System.Drawing.Size(458, 13);
      this.labelControl2.TabIndex = 25;
      this.labelControl2.Text = "Add an \"Auto Group/Profile option to tell whether to update with new selected fol" +
          "der at runtime";
      // 
      // FileSyncOptions
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.ClientSize = new System.Drawing.Size(960, 586);
      this.Controls.Add(this.labelControl2);
      this.Controls.Add(this.gridControl1);
      this.Controls.Add(this.labelControl1);
      this.Controls.Add(this.simpleButton2);
      this.Controls.Add(this.btnAdd);
      this.Controls.Add(this.treeProfileGroups);
      this.Controls.Add(this.treeProfiles);
      this.Controls.Add(this.labelControl6);
      this.Controls.Add(this.buttonEdit3);
      this.Controls.Add(this.buttonEdit5);
      this.Controls.Add(this.buttonEdit4);
      this.Controls.Add(this.labelControl8);
      this.Controls.Add(this.labelControl7);
      this.Controls.Add(this.chkIncludeSystemFolders);
      this.Controls.Add(this.chkIncludeSubfolders);
      this.Name = "FileSyncOptions";
      this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
      this.Text = "File Sync - Options";
      this.Load += new System.EventHandler(this.FileSyncOptions_Load);
      ((System.ComponentModel.ISupportInitialize)(this.buttonEdit5.Properties)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.buttonEdit4.Properties)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.chkIncludeSystemFolders.Properties)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.chkIncludeSubfolders.Properties)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.buttonEdit3.Properties)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.treeProfiles)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.reposProfileEdit)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.treeProfileGroups)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.gvProfiles)).EndInit();
      this.ResumeLayout(false);
      this.PerformLayout();

    }

    #endregion

    private DevExpress.XtraEditors.ButtonEdit buttonEdit5;
    private DevExpress.XtraEditors.ButtonEdit buttonEdit4;
    private DevExpress.XtraEditors.LabelControl labelControl8;
    private DevExpress.XtraEditors.LabelControl labelControl7;
    private DevExpress.XtraEditors.CheckEdit chkIncludeSystemFolders;
    private DevExpress.XtraEditors.CheckEdit chkIncludeSubfolders;
    private DevExpress.XtraEditors.LabelControl labelControl6;
    private DevExpress.XtraEditors.ButtonEdit buttonEdit3;
    private DevExpress.XtraTreeList.TreeList treeProfiles;
    private DevExpress.XtraTreeList.Columns.TreeListColumn ColProfileName;
    private DevExpress.XtraTreeList.TreeList treeProfileGroups;
    private DevExpress.XtraTreeList.Columns.TreeListColumn colProfileGroupName;
    private DevExpress.XtraEditors.SimpleButton btnAdd;
    private DevExpress.XtraEditors.SimpleButton simpleButton2;
    private DevExpress.XtraEditors.LabelControl labelControl1;
    private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit reposProfileEdit;
    private System.Windows.Forms.ImageList imageList1;
    private DevExpress.XtraTreeList.Columns.TreeListColumn ColFolder;
    private DevExpress.XtraGrid.GridControl gridControl1;
    private DevExpress.XtraGrid.Views.Grid.GridView gvProfiles;
    private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
    private DevExpress.XtraGrid.Columns.GridColumn gridColumn2;
    private DevExpress.XtraGrid.Columns.GridColumn gridColumn3;
    private DevExpress.XtraEditors.LabelControl labelControl2;
  }
}