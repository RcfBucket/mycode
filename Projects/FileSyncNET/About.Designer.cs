namespace FileSyncNET
{
  partial class About
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(About));
      this.pictureBox1 = new System.Windows.Forms.PictureBox();
      this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
      this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
      this.lblCopyright = new DevExpress.XtraEditors.LabelControl();
      this.lblReleaseNotes = new DevExpress.XtraEditors.LabelControl();
      this.lblVersion = new DevExpress.XtraEditors.LabelControl();
      this.lblBuild = new DevExpress.XtraEditors.LabelControl();
      this.btnOK = new DevExpress.XtraEditors.SimpleButton();
      ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
      this.SuspendLayout();
      // 
      // pictureBox1
      // 
      this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
      this.pictureBox1.Location = new System.Drawing.Point(12, 12);
      this.pictureBox1.Name = "pictureBox1";
      this.pictureBox1.Size = new System.Drawing.Size(32, 32);
      this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
      this.pictureBox1.TabIndex = 0;
      this.pictureBox1.TabStop = false;
      // 
      // labelControl1
      // 
      this.labelControl1.Appearance.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.labelControl1.Appearance.Options.UseFont = true;
      this.labelControl1.Location = new System.Drawing.Point(69, 14);
      this.labelControl1.Name = "labelControl1";
      this.labelControl1.Size = new System.Drawing.Size(61, 21);
      this.labelControl1.TabIndex = 1;
      this.labelControl1.Text = "File Sync";
      // 
      // labelControl2
      // 
      this.labelControl2.Appearance.Font = new System.Drawing.Font("Segoe UI", 12F);
      this.labelControl2.Appearance.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical;
      this.labelControl2.Appearance.Options.UseFont = true;
      this.labelControl2.Location = new System.Drawing.Point(69, 41);
      this.labelControl2.Name = "labelControl2";
      this.labelControl2.Size = new System.Drawing.Size(176, 21);
      this.labelControl2.TabIndex = 4;
      this.labelControl2.Text = "File Syncronization Utility";
      // 
      // lblCopyright
      // 
      this.lblCopyright.Location = new System.Drawing.Point(69, 77);
      this.lblCopyright.Name = "lblCopyright";
      this.lblCopyright.Size = new System.Drawing.Size(175, 13);
      this.lblCopyright.TabIndex = 5;
      this.lblCopyright.Text = "Copyright (c) Bob Fisher, 1997-2008";
      // 
      // lblReleaseNotes
      // 
      this.lblReleaseNotes.Appearance.ForeColor = System.Drawing.SystemColors.HotTrack;
      this.lblReleaseNotes.Appearance.Options.UseForeColor = true;
      this.lblReleaseNotes.Cursor = System.Windows.Forms.Cursors.Hand;
      this.lblReleaseNotes.Location = new System.Drawing.Point(69, 147);
      this.lblReleaseNotes.Name = "lblReleaseNotes";
      this.lblReleaseNotes.Size = new System.Drawing.Size(69, 13);
      this.lblReleaseNotes.TabIndex = 7;
      this.lblReleaseNotes.Text = "Release Notes";
      this.lblReleaseNotes.MouseLeave += new System.EventHandler(this.lblReleaseNotes_MouseLeave);
      this.lblReleaseNotes.Click += new System.EventHandler(this.lblReleaseNotes_Click);
      this.lblReleaseNotes.MouseEnter += new System.EventHandler(this.lblReleaseNotes_MouseEnter);
      // 
      // lblVersion
      // 
      this.lblVersion.Location = new System.Drawing.Point(69, 99);
      this.lblVersion.Name = "lblVersion";
      this.lblVersion.Size = new System.Drawing.Size(45, 13);
      this.lblVersion.TabIndex = 8;
      this.lblVersion.Text = "lblVersion";
      // 
      // lblBuild
      // 
      this.lblBuild.Location = new System.Drawing.Point(69, 118);
      this.lblBuild.Name = "lblBuild";
      this.lblBuild.Size = new System.Drawing.Size(32, 13);
      this.lblBuild.TabIndex = 9;
      this.lblBuild.Text = "lblBuild";
      // 
      // btnOK
      // 
      this.btnOK.DialogResult = System.Windows.Forms.DialogResult.Cancel;
      this.btnOK.Location = new System.Drawing.Point(97, 183);
      this.btnOK.Name = "btnOK";
      this.btnOK.Size = new System.Drawing.Size(91, 23);
      this.btnOK.TabIndex = 10;
      this.btnOK.Text = "&OK";
      this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
      // 
      // About
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.CancelButton = this.btnOK;
      this.ClientSize = new System.Drawing.Size(284, 223);
      this.Controls.Add(this.btnOK);
      this.Controls.Add(this.lblBuild);
      this.Controls.Add(this.lblVersion);
      this.Controls.Add(this.lblReleaseNotes);
      this.Controls.Add(this.lblCopyright);
      this.Controls.Add(this.labelControl2);
      this.Controls.Add(this.labelControl1);
      this.Controls.Add(this.pictureBox1);
      this.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
      this.MaximizeBox = false;
      this.MinimizeBox = false;
      this.Name = "About";
      this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
      this.Text = "About";
      ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
      this.ResumeLayout(false);
      this.PerformLayout();

    }

    #endregion

    private System.Windows.Forms.PictureBox pictureBox1;
    private DevExpress.XtraEditors.LabelControl labelControl1;
    private DevExpress.XtraEditors.LabelControl labelControl2;
    private DevExpress.XtraEditors.LabelControl lblCopyright;
    private DevExpress.XtraEditors.LabelControl lblReleaseNotes;
    private DevExpress.XtraEditors.LabelControl lblVersion;
    private DevExpress.XtraEditors.LabelControl lblBuild;
    private DevExpress.XtraEditors.SimpleButton btnOK;
  }
}