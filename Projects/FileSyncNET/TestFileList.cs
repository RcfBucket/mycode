using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraTreeList.Nodes;
using System.IO;
using System.Collections;

namespace FileSyncNET
{
  public partial class TestFileList : Form
  {
    private TFileSyncFolder fSyncFolder;

    public TestFileList()
    {
      InitializeComponent();
    }

    public TFileSyncFolder SyncFolder
    {
      get { return fSyncFolder; }
      set { fSyncFolder = value; }
    }

    private void TestFileList_Load(object sender, EventArgs e)
    {
      Cursor.Current = Cursors.WaitCursor;
      treeFolders.Nodes.Clear();
      try
      {
        FillFileList(fSyncFolder, null);
      }
      finally
      {
        Cursor.Current = Cursors.Default;
      }
    }

    private void CountEmUp(TFileSyncFolder aNode, ref int aFiles, ref long aFileSize, ref int aFolders)
    {
      aFiles += aNode.FileCount;

      foreach (FileInfo f in aNode.Files)
        aFileSize += f.Length;

      if (aNode.FolderCount > 0)
      {
        foreach (TFileSyncFolder fs in aNode.Folders)
        {
          aFolders++;
          CountEmUp(fs, ref aFiles, ref aFileSize, ref aFolders);
        }
      }
    }

    private void FillFileList(TFileSyncFolder aFolder, TreeListNode aParentNode)
    {
      TreeListNode node;
      double num;

      treeFolders.BeginUnboundLoad();
      try
      {
        // fill folder names
        foreach (TFileSyncFolder fs in aFolder.Folders)
        {
          num = fs.FileCount;
          node = treeFolders.AppendNode(new object[] { fs.FullPathName, fs.FolderName, "Folder", String.Format("{0:N0} files", num) }, aParentNode);
          node.StateImageIndex = 0;
          node.HasChildren = ((aFolder.FileCount > 0) || (aFolder.FolderCount > 0));
          FillFileList(fs, node);
        }

        // fill file names
        foreach (FileInfo f in aFolder.Files)
        {
          if (f.Length < 1024)
            num = 1;
          else
            num = f.Length / 1024.0;
          node = treeFolders.AppendNode(new object[] { f.FullName, f.Name, "File", String.Format("{0:N0} KB", num) }, aParentNode);
          node.StateImageIndex = 1;
          node.HasChildren = false;
        }
      }
      finally
      {
        treeFolders.EndUnboundLoad();
      }
    }

    private void treeFolders_AfterExpand(object sender, DevExpress.XtraTreeList.NodeEventArgs e)
    {
      if (e.Node.HasChildren)
        e.Node.StateImageIndex = 2;
    }

    private void treeFolders_AfterCollapse(object sender, DevExpress.XtraTreeList.NodeEventArgs e)
    {
      if (e.Node.HasChildren)
        e.Node.StateImageIndex = 0;
    }

    private void btnCalcTotals_Click(object sender, EventArgs e)
    {
      int fileCount = 0;
      long fileBytes = 0;
      int folderCount = 0;

      CountEmUp(SyncFolder, ref fileCount, ref fileBytes, ref folderCount);
      lblTotalBytes.Text = String.Format("{0:N0} KB  ({1:N0} bytes", fileBytes / 1024.0, fileBytes);
      lblTotalFiles.Text = String.Format("{0:N0}", fileCount);
      lblTotalFolders.Text = String.Format("{0:N0}", folderCount);
    }

    private void checkEdit1_CheckedChanged(object sender, EventArgs e)
    {
      colFullName.Visible = checkEdit1.Checked;
    }

    private void btnFolderList_Click(object sender, EventArgs e)
    {
      ArrayList list = SyncFolder.FullFolderList;
      lbFolderList.Items.Clear();
      lblCount.Text = string.Format("Count: {0}", list.Count);
      lbFolderList.BeginUpdate();
      foreach (string s in list)
        lbFolderList.Items.Add(s);
      lbFolderList.EndUpdate();
    }

    private void lbFolderList_SelectedIndexChanged(object sender, EventArgs e)
    {
      string tstr;

      tstr = lbFolderList.SelectedItem.ToString();

      labelControl5.Text = SyncFolder.RootPath;
      labelControl2.Text = SyncFolder.PathWithoutRoot(tstr);
    }


  }
}