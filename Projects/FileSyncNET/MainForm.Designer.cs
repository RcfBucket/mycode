namespace FileSyncNET
{
  partial class FrmFileSync
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.components = new System.ComponentModel.Container();
      DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject1 = new DevExpress.Utils.SerializableAppearanceObject();
      DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject2 = new DevExpress.Utils.SerializableAppearanceObject();
      this.cbSource = new DevExpress.XtraEditors.ComboBoxEdit();
      this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
      this.dlgSrcFolder = new System.Windows.Forms.FolderBrowserDialog();
      this.dlgDstFolder = new System.Windows.Forms.FolderBrowserDialog();
      this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
      this.cbDest = new DevExpress.XtraEditors.ComboBoxEdit();
      this.lbProfiles = new DevExpress.XtraEditors.ListBoxControl();
      this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
      this.xtraTabControl1 = new DevExpress.XtraTab.XtraTabControl();
      this.xtraTabPage1 = new DevExpress.XtraTab.XtraTabPage();
      this.checkEdit1 = new DevExpress.XtraEditors.CheckEdit();
      this.checkEdit5 = new DevExpress.XtraEditors.CheckEdit();
      this.checkEdit4 = new DevExpress.XtraEditors.CheckEdit();
      this.checkEdit3 = new DevExpress.XtraEditors.CheckEdit();
      this.checkEdit2 = new DevExpress.XtraEditors.CheckEdit();
      this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
      this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
      this.xtraTabPage2 = new DevExpress.XtraTab.XtraTabPage();
      this.buttonEdit5 = new DevExpress.XtraEditors.ButtonEdit();
      this.buttonEdit4 = new DevExpress.XtraEditors.ButtonEdit();
      this.labelControl8 = new DevExpress.XtraEditors.LabelControl();
      this.labelControl7 = new DevExpress.XtraEditors.LabelControl();
      this.chkIncludeSystemFolders = new DevExpress.XtraEditors.CheckEdit();
      this.chkIncludeSubfolders = new DevExpress.XtraEditors.CheckEdit();
      this.xtraTabPage3 = new DevExpress.XtraTab.XtraTabPage();
      this.btnProfiles = new DevExpress.XtraEditors.SimpleButton();
      this.labelControl6 = new DevExpress.XtraEditors.LabelControl();
      this.buttonEdit3 = new DevExpress.XtraEditors.ButtonEdit();
      this.tabDebug = new DevExpress.XtraTab.XtraTabPage();
      this.edDebug = new DevExpress.XtraEditors.MemoEdit();
      this.defaultLookAndFeel1 = new DevExpress.LookAndFeel.DefaultLookAndFeel(this.components);
      this.btnAbout = new DevExpress.XtraEditors.SimpleButton();
      this.btnAnalyze = new DevExpress.XtraEditors.SimpleButton();
      this.lblTest = new DevExpress.XtraEditors.LabelControl();
      this.simpleButton1 = new DevExpress.XtraEditors.SimpleButton();
      this.btnTestSrcFileList = new DevExpress.XtraEditors.SimpleButton();
      this.comboBoxEdit1 = new DevExpress.XtraEditors.ComboBoxEdit();
      this.btnOptions = new DevExpress.XtraEditors.SimpleButton();
      this.dockManager1 = new DevExpress.XtraBars.Docking.DockManager(this.components);
      this.dockCompare = new DevExpress.XtraBars.Docking.DockPanel();
      this.dockPanel1_Container = new DevExpress.XtraBars.Docking.ControlContainer();
      ((System.ComponentModel.ISupportInitialize)(this.cbSource.Properties)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.cbDest.Properties)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.lbProfiles)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl1)).BeginInit();
      this.xtraTabControl1.SuspendLayout();
      this.xtraTabPage1.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.checkEdit1.Properties)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.checkEdit5.Properties)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.checkEdit4.Properties)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.checkEdit3.Properties)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.checkEdit2.Properties)).BeginInit();
      this.xtraTabPage2.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.buttonEdit5.Properties)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.buttonEdit4.Properties)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.chkIncludeSystemFolders.Properties)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.chkIncludeSubfolders.Properties)).BeginInit();
      this.xtraTabPage3.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.buttonEdit3.Properties)).BeginInit();
      this.tabDebug.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.edDebug.Properties)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.comboBoxEdit1.Properties)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.dockManager1)).BeginInit();
      this.dockCompare.SuspendLayout();
      this.SuspendLayout();
      // 
      // cbSource
      // 
      this.cbSource.Location = new System.Drawing.Point(267, 28);
      this.cbSource.Name = "cbSource";
      this.cbSource.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.cbSource.Properties.Appearance.Options.UseFont = true;
      this.cbSource.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Ellipsis, "", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, null, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject1, "Select Folder", null, null, true)});
      this.cbSource.Properties.Items.AddRange(new object[] {
            "E:\\LabPro4.0",
            "E:\\LabPro4.0\\Projects",
            "C:\\Windows"});
      this.cbSource.Properties.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.cbSource_Properties_ButtonClick);
      this.cbSource.Size = new System.Drawing.Size(422, 21);
      this.cbSource.TabIndex = 1;
      this.cbSource.SelectedIndexChanged += new System.EventHandler(this.cbSource_SelectedIndexChanged);
      // 
      // labelControl1
      // 
      this.labelControl1.Appearance.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.labelControl1.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.Vertical;
      this.labelControl1.LineVisible = true;
      this.labelControl1.Location = new System.Drawing.Point(267, 9);
      this.labelControl1.Name = "labelControl1";
      this.labelControl1.Size = new System.Drawing.Size(422, 13);
      this.labelControl1.TabIndex = 0;
      this.labelControl1.Text = "Source Folder";
      // 
      // dlgSrcFolder
      // 
      this.dlgSrcFolder.Description = "Source Folder";
      // 
      // dlgDstFolder
      // 
      this.dlgDstFolder.Description = "Destination Folder";
      // 
      // labelControl2
      // 
      this.labelControl2.Appearance.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.labelControl2.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.Vertical;
      this.labelControl2.LineVisible = true;
      this.labelControl2.Location = new System.Drawing.Point(267, 65);
      this.labelControl2.Name = "labelControl2";
      this.labelControl2.Size = new System.Drawing.Size(422, 13);
      this.labelControl2.TabIndex = 2;
      this.labelControl2.Text = "Destination Folder";
      // 
      // cbDest
      // 
      this.cbDest.Location = new System.Drawing.Point(267, 84);
      this.cbDest.Name = "cbDest";
      this.cbDest.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.cbDest.Properties.Appearance.Options.UseFont = true;
      this.cbDest.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Ellipsis, "", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, null, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject2, "Select Folder", null, null, true)});
      this.cbDest.Properties.Items.AddRange(new object[] {
            "E:\\LabPro3.0",
            "C:\\Windows"});
      this.cbDest.Properties.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.cbDest_Properties_ButtonClick_1);
      this.cbDest.Size = new System.Drawing.Size(422, 21);
      this.cbDest.TabIndex = 3;
      this.cbDest.SelectedIndexChanged += new System.EventHandler(this.cbDest_SelectedIndexChanged);
      // 
      // lbProfiles
      // 
      this.lbProfiles.Items.AddRange(new object[] {
            "Default Profile",
            "note: default profile will",
            "store last folder selected"});
      this.lbProfiles.Location = new System.Drawing.Point(725, 31);
      this.lbProfiles.Name = "lbProfiles";
      this.lbProfiles.Size = new System.Drawing.Size(202, 352);
      this.lbProfiles.TabIndex = 5;
      // 
      // labelControl3
      // 
      this.labelControl3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
      this.labelControl3.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.Vertical;
      this.labelControl3.LineVisible = true;
      this.labelControl3.Location = new System.Drawing.Point(726, 12);
      this.labelControl3.Name = "labelControl3";
      this.labelControl3.Size = new System.Drawing.Size(201, 13);
      this.labelControl3.TabIndex = 6;
      this.labelControl3.Text = "Profiles";
      // 
      // xtraTabControl1
      // 
      this.xtraTabControl1.Location = new System.Drawing.Point(267, 161);
      this.xtraTabControl1.Name = "xtraTabControl1";
      this.xtraTabControl1.SelectedTabPage = this.xtraTabPage1;
      this.xtraTabControl1.Size = new System.Drawing.Size(380, 263);
      this.xtraTabControl1.TabIndex = 7;
      this.xtraTabControl1.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.xtraTabPage1,
            this.xtraTabPage2,
            this.xtraTabPage3,
            this.tabDebug});
      // 
      // xtraTabPage1
      // 
      this.xtraTabPage1.Controls.Add(this.checkEdit1);
      this.xtraTabPage1.Controls.Add(this.checkEdit5);
      this.xtraTabPage1.Controls.Add(this.checkEdit4);
      this.xtraTabPage1.Controls.Add(this.checkEdit3);
      this.xtraTabPage1.Controls.Add(this.checkEdit2);
      this.xtraTabPage1.Controls.Add(this.labelControl5);
      this.xtraTabPage1.Controls.Add(this.labelControl4);
      this.xtraTabPage1.Name = "xtraTabPage1";
      this.xtraTabPage1.Size = new System.Drawing.Size(374, 236);
      this.xtraTabPage1.Text = "Compare";
      // 
      // checkEdit1
      // 
      this.checkEdit1.EditValue = true;
      this.checkEdit1.Location = new System.Drawing.Point(10, 37);
      this.checkEdit1.Name = "checkEdit1";
      this.checkEdit1.Properties.Caption = "Compare Date/Time?Size only";
      this.checkEdit1.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
      this.checkEdit1.Size = new System.Drawing.Size(348, 19);
      this.checkEdit1.TabIndex = 8;
      // 
      // checkEdit5
      // 
      this.checkEdit5.Location = new System.Drawing.Point(9, 175);
      this.checkEdit5.Name = "checkEdit5";
      this.checkEdit5.Properties.Caption = "Visual Sync";
      this.checkEdit5.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
      this.checkEdit5.Size = new System.Drawing.Size(348, 19);
      this.checkEdit5.TabIndex = 7;
      // 
      // checkEdit4
      // 
      this.checkEdit4.Location = new System.Drawing.Point(10, 112);
      this.checkEdit4.Name = "checkEdit4";
      this.checkEdit4.Properties.Caption = "Super Sync";
      this.checkEdit4.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
      this.checkEdit4.Size = new System.Drawing.Size(348, 19);
      this.checkEdit4.TabIndex = 6;
      // 
      // checkEdit3
      // 
      this.checkEdit3.Location = new System.Drawing.Point(10, 87);
      this.checkEdit3.Name = "checkEdit3";
      this.checkEdit3.Properties.Caption = "Byte compare";
      this.checkEdit3.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
      this.checkEdit3.Size = new System.Drawing.Size(348, 19);
      this.checkEdit3.TabIndex = 5;
      // 
      // checkEdit2
      // 
      this.checkEdit2.Location = new System.Drawing.Point(10, 62);
      this.checkEdit2.Name = "checkEdit2";
      this.checkEdit2.Properties.Caption = "Byte compare if files are same size but Date/Time differ";
      this.checkEdit2.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
      this.checkEdit2.Size = new System.Drawing.Size(348, 19);
      this.checkEdit2.TabIndex = 4;
      // 
      // labelControl5
      // 
      this.labelControl5.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.Vertical;
      this.labelControl5.LineVisible = true;
      this.labelControl5.Location = new System.Drawing.Point(11, 156);
      this.labelControl5.Name = "labelControl5";
      this.labelControl5.Size = new System.Drawing.Size(346, 13);
      this.labelControl5.TabIndex = 3;
      this.labelControl5.Text = "Visual Sync Modes";
      // 
      // labelControl4
      // 
      this.labelControl4.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.Vertical;
      this.labelControl4.LineVisible = true;
      this.labelControl4.Location = new System.Drawing.Point(11, 18);
      this.labelControl4.Name = "labelControl4";
      this.labelControl4.Size = new System.Drawing.Size(346, 13);
      this.labelControl4.TabIndex = 0;
      this.labelControl4.Text = "Auto Sync Modes";
      // 
      // xtraTabPage2
      // 
      this.xtraTabPage2.Controls.Add(this.buttonEdit5);
      this.xtraTabPage2.Controls.Add(this.buttonEdit4);
      this.xtraTabPage2.Controls.Add(this.labelControl8);
      this.xtraTabPage2.Controls.Add(this.labelControl7);
      this.xtraTabPage2.Controls.Add(this.chkIncludeSystemFolders);
      this.xtraTabPage2.Controls.Add(this.chkIncludeSubfolders);
      this.xtraTabPage2.Name = "xtraTabPage2";
      this.xtraTabPage2.Size = new System.Drawing.Size(374, 236);
      this.xtraTabPage2.Text = "Include/Exclude";
      // 
      // buttonEdit5
      // 
      this.buttonEdit5.Location = new System.Drawing.Point(3, 128);
      this.buttonEdit5.Name = "buttonEdit5";
      this.buttonEdit5.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
      this.buttonEdit5.Size = new System.Drawing.Size(365, 20);
      this.buttonEdit5.TabIndex = 5;
      // 
      // buttonEdit4
      // 
      this.buttonEdit4.Location = new System.Drawing.Point(3, 75);
      this.buttonEdit4.Name = "buttonEdit4";
      this.buttonEdit4.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
      this.buttonEdit4.Size = new System.Drawing.Size(365, 20);
      this.buttonEdit4.TabIndex = 4;
      // 
      // labelControl8
      // 
      this.labelControl8.Location = new System.Drawing.Point(3, 109);
      this.labelControl8.Name = "labelControl8";
      this.labelControl8.Size = new System.Drawing.Size(74, 13);
      this.labelControl8.TabIndex = 3;
      this.labelControl8.Text = "Files to Exclude";
      // 
      // labelControl7
      // 
      this.labelControl7.Location = new System.Drawing.Point(3, 56);
      this.labelControl7.Name = "labelControl7";
      this.labelControl7.Size = new System.Drawing.Size(72, 13);
      this.labelControl7.TabIndex = 2;
      this.labelControl7.Text = "Files to Include";
      // 
      // chkIncludeSystemFolders
      // 
      this.chkIncludeSystemFolders.Location = new System.Drawing.Point(178, 20);
      this.chkIncludeSystemFolders.Name = "chkIncludeSystemFolders";
      this.chkIncludeSystemFolders.Properties.Caption = "Include system folders";
      this.chkIncludeSystemFolders.Size = new System.Drawing.Size(145, 19);
      this.chkIncludeSystemFolders.TabIndex = 1;
      // 
      // chkIncludeSubfolders
      // 
      this.chkIncludeSubfolders.Location = new System.Drawing.Point(3, 20);
      this.chkIncludeSubfolders.Name = "chkIncludeSubfolders";
      this.chkIncludeSubfolders.Properties.Caption = "Include sub-folders";
      this.chkIncludeSubfolders.Size = new System.Drawing.Size(144, 19);
      this.chkIncludeSubfolders.TabIndex = 0;
      // 
      // xtraTabPage3
      // 
      this.xtraTabPage3.Controls.Add(this.btnProfiles);
      this.xtraTabPage3.Controls.Add(this.labelControl6);
      this.xtraTabPage3.Controls.Add(this.buttonEdit3);
      this.xtraTabPage3.Name = "xtraTabPage3";
      this.xtraTabPage3.Size = new System.Drawing.Size(374, 236);
      this.xtraTabPage3.Text = "Options";
      // 
      // btnProfiles
      // 
      this.btnProfiles.Location = new System.Drawing.Point(11, 76);
      this.btnProfiles.Name = "btnProfiles";
      this.btnProfiles.Size = new System.Drawing.Size(104, 23);
      this.btnProfiles.TabIndex = 2;
      this.btnProfiles.Text = "Profiles";
      // 
      // labelControl6
      // 
      this.labelControl6.Location = new System.Drawing.Point(11, 17);
      this.labelControl6.Name = "labelControl6";
      this.labelControl6.Size = new System.Drawing.Size(118, 13);
      this.labelControl6.TabIndex = 1;
      this.labelControl6.Text = "Text Difference Program";
      // 
      // buttonEdit3
      // 
      this.buttonEdit3.Location = new System.Drawing.Point(11, 36);
      this.buttonEdit3.Name = "buttonEdit3";
      this.buttonEdit3.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
      this.buttonEdit3.Size = new System.Drawing.Size(345, 20);
      this.buttonEdit3.TabIndex = 0;
      // 
      // tabDebug
      // 
      this.tabDebug.Controls.Add(this.edDebug);
      this.tabDebug.Name = "tabDebug";
      this.tabDebug.Size = new System.Drawing.Size(374, 236);
      this.tabDebug.Text = "Debug";
      // 
      // edDebug
      // 
      this.edDebug.Dock = System.Windows.Forms.DockStyle.Fill;
      this.edDebug.Location = new System.Drawing.Point(0, 0);
      this.edDebug.Name = "edDebug";
      this.edDebug.Size = new System.Drawing.Size(374, 236);
      this.edDebug.TabIndex = 1;
      // 
      // btnAbout
      // 
      this.btnAbout.Location = new System.Drawing.Point(648, 449);
      this.btnAbout.Name = "btnAbout";
      this.btnAbout.Size = new System.Drawing.Size(112, 23);
      this.btnAbout.TabIndex = 10;
      this.btnAbout.Text = "&About";
      this.btnAbout.Click += new System.EventHandler(this.btnAbout_Click);
      // 
      // btnAnalyze
      // 
      this.btnAnalyze.Location = new System.Drawing.Point(777, 449);
      this.btnAnalyze.Name = "btnAnalyze";
      this.btnAnalyze.Size = new System.Drawing.Size(112, 23);
      this.btnAnalyze.TabIndex = 11;
      this.btnAnalyze.Text = "Analyze Folders";
      this.btnAnalyze.Click += new System.EventHandler(this.btnAnalyze_Click);
      // 
      // lblTest
      // 
      this.lblTest.Location = new System.Drawing.Point(308, 430);
      this.lblTest.Name = "lblTest";
      this.lblTest.Size = new System.Drawing.Size(63, 13);
      this.lblTest.TabIndex = 12;
      this.lblTest.Text = "labelControl9";
      // 
      // simpleButton1
      // 
      this.simpleButton1.Location = new System.Drawing.Point(308, 449);
      this.simpleButton1.Name = "simpleButton1";
      this.simpleButton1.Size = new System.Drawing.Size(75, 23);
      this.simpleButton1.TabIndex = 13;
      this.simpleButton1.Text = "Test";
      this.simpleButton1.Click += new System.EventHandler(this.simpleButton1_Click);
      // 
      // btnTestSrcFileList
      // 
      this.btnTestSrcFileList.Location = new System.Drawing.Point(389, 449);
      this.btnTestSrcFileList.Name = "btnTestSrcFileList";
      this.btnTestSrcFileList.Size = new System.Drawing.Size(124, 23);
      this.btnTestSrcFileList.TabIndex = 14;
      this.btnTestSrcFileList.Text = "Test Src File List";
      this.btnTestSrcFileList.Click += new System.EventHandler(this.btnTestSrcFileList_Click);
      // 
      // comboBoxEdit1
      // 
      this.comboBoxEdit1.Location = new System.Drawing.Point(267, 124);
      this.comboBoxEdit1.Name = "comboBoxEdit1";
      this.comboBoxEdit1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
      this.comboBoxEdit1.Size = new System.Drawing.Size(281, 20);
      this.comboBoxEdit1.TabIndex = 15;
      // 
      // btnOptions
      // 
      this.btnOptions.Location = new System.Drawing.Point(530, 449);
      this.btnOptions.Name = "btnOptions";
      this.btnOptions.Size = new System.Drawing.Size(112, 23);
      this.btnOptions.TabIndex = 16;
      this.btnOptions.Text = "Options";
      this.btnOptions.Click += new System.EventHandler(this.btnOptions_Click);
      // 
      // dockManager1
      // 
      this.dockManager1.Form = this;
      this.dockManager1.RootPanels.AddRange(new DevExpress.XtraBars.Docking.DockPanel[] {
            this.dockCompare});
      this.dockManager1.TopZIndexControls.AddRange(new string[] {
            "DevExpress.XtraBars.BarDockControl",
            "System.Windows.Forms.StatusBar",
            "DevExpress.XtraBars.Ribbon.RibbonStatusBar",
            "DevExpress.XtraBars.Ribbon.RibbonControl"});
      // 
      // dockCompare
      // 
      this.dockCompare.Controls.Add(this.dockPanel1_Container);
      this.dockCompare.Dock = DevExpress.XtraBars.Docking.DockingStyle.Left;
      this.dockCompare.ID = new System.Guid("ab96697d-52cc-47cb-a5a4-93371f6f6d29");
      this.dockCompare.Location = new System.Drawing.Point(0, 0);
      this.dockCompare.Name = "dockCompare";
      this.dockCompare.OriginalSize = new System.Drawing.Size(200, 200);
      this.dockCompare.Size = new System.Drawing.Size(200, 481);
      this.dockCompare.Text = "Compare";
      // 
      // dockPanel1_Container
      // 
      this.dockPanel1_Container.Location = new System.Drawing.Point(4, 23);
      this.dockPanel1_Container.Name = "dockPanel1_Container";
      this.dockPanel1_Container.Size = new System.Drawing.Size(192, 454);
      this.dockPanel1_Container.TabIndex = 0;
      // 
      // FrmFileSync
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.ClientSize = new System.Drawing.Size(942, 481);
      this.Controls.Add(this.btnOptions);
      this.Controls.Add(this.comboBoxEdit1);
      this.Controls.Add(this.btnTestSrcFileList);
      this.Controls.Add(this.simpleButton1);
      this.Controls.Add(this.lblTest);
      this.Controls.Add(this.btnAnalyze);
      this.Controls.Add(this.btnAbout);
      this.Controls.Add(this.xtraTabControl1);
      this.Controls.Add(this.labelControl3);
      this.Controls.Add(this.lbProfiles);
      this.Controls.Add(this.labelControl2);
      this.Controls.Add(this.cbDest);
      this.Controls.Add(this.labelControl1);
      this.Controls.Add(this.cbSource);
      this.Controls.Add(this.dockCompare);
      this.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.Name = "FrmFileSync";
      this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
      this.Text = "File Sync .NET";
      this.Load += new System.EventHandler(this.FrmFileSync_Load);
      ((System.ComponentModel.ISupportInitialize)(this.cbSource.Properties)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.cbDest.Properties)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.lbProfiles)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl1)).EndInit();
      this.xtraTabControl1.ResumeLayout(false);
      this.xtraTabPage1.ResumeLayout(false);
      ((System.ComponentModel.ISupportInitialize)(this.checkEdit1.Properties)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.checkEdit5.Properties)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.checkEdit4.Properties)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.checkEdit3.Properties)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.checkEdit2.Properties)).EndInit();
      this.xtraTabPage2.ResumeLayout(false);
      this.xtraTabPage2.PerformLayout();
      ((System.ComponentModel.ISupportInitialize)(this.buttonEdit5.Properties)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.buttonEdit4.Properties)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.chkIncludeSystemFolders.Properties)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.chkIncludeSubfolders.Properties)).EndInit();
      this.xtraTabPage3.ResumeLayout(false);
      this.xtraTabPage3.PerformLayout();
      ((System.ComponentModel.ISupportInitialize)(this.buttonEdit3.Properties)).EndInit();
      this.tabDebug.ResumeLayout(false);
      ((System.ComponentModel.ISupportInitialize)(this.edDebug.Properties)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.comboBoxEdit1.Properties)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.dockManager1)).EndInit();
      this.dockCompare.ResumeLayout(false);
      this.ResumeLayout(false);
      this.PerformLayout();

    }

    #endregion

    private DevExpress.XtraEditors.ComboBoxEdit cbSource;
    private DevExpress.XtraEditors.LabelControl labelControl1;
    private System.Windows.Forms.FolderBrowserDialog dlgSrcFolder;
    private System.Windows.Forms.FolderBrowserDialog dlgDstFolder;
    private DevExpress.XtraEditors.LabelControl labelControl2;
    private DevExpress.XtraEditors.ComboBoxEdit cbDest;
    private DevExpress.XtraEditors.ListBoxControl lbProfiles;
    private DevExpress.XtraEditors.LabelControl labelControl3;
    private DevExpress.XtraTab.XtraTabControl xtraTabControl1;
    private DevExpress.XtraTab.XtraTabPage xtraTabPage1;
    private DevExpress.XtraTab.XtraTabPage xtraTabPage2;
    private DevExpress.XtraTab.XtraTabPage xtraTabPage3;
    private DevExpress.LookAndFeel.DefaultLookAndFeel defaultLookAndFeel1;
    private DevExpress.XtraEditors.SimpleButton btnAbout;
    private DevExpress.XtraEditors.LabelControl labelControl4;
    private DevExpress.XtraEditors.CheckEdit checkEdit5;
    private DevExpress.XtraEditors.CheckEdit checkEdit4;
    private DevExpress.XtraEditors.CheckEdit checkEdit3;
    private DevExpress.XtraEditors.CheckEdit checkEdit2;
    private DevExpress.XtraEditors.LabelControl labelControl5;
    private DevExpress.XtraEditors.LabelControl labelControl6;
    private DevExpress.XtraEditors.ButtonEdit buttonEdit3;
    private DevExpress.XtraEditors.ButtonEdit buttonEdit5;
    private DevExpress.XtraEditors.ButtonEdit buttonEdit4;
    private DevExpress.XtraEditors.LabelControl labelControl8;
    private DevExpress.XtraEditors.LabelControl labelControl7;
    private DevExpress.XtraEditors.CheckEdit chkIncludeSystemFolders;
    private DevExpress.XtraEditors.CheckEdit chkIncludeSubfolders;
    private DevExpress.XtraEditors.SimpleButton btnProfiles;
    private DevExpress.XtraEditors.SimpleButton btnAnalyze;
    private DevExpress.XtraEditors.LabelControl lblTest;
    private DevExpress.XtraEditors.SimpleButton simpleButton1;
    private DevExpress.XtraTab.XtraTabPage tabDebug;
    private DevExpress.XtraEditors.MemoEdit edDebug;
    private DevExpress.XtraEditors.SimpleButton btnTestSrcFileList;
    private DevExpress.XtraEditors.ComboBoxEdit comboBoxEdit1;
    private DevExpress.XtraEditors.SimpleButton btnOptions;
    private DevExpress.XtraBars.Docking.DockManager dockManager1;
    private DevExpress.XtraBars.Docking.DockPanel dockCompare;
    private DevExpress.XtraBars.Docking.ControlContainer dockPanel1_Container;
    private DevExpress.XtraEditors.CheckEdit checkEdit1;
  }
}

