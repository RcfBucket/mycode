using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.IO;

namespace FileSyncNET
{
  public partial class FrmFileSync : Form
  {
    public FrmFileSync()
    {
      InitializeComponent();
    }

    private void cbSource_Properties_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
    {
      if (e.Button.Index == 1)
      {
        dlgSrcFolder.SelectedPath = cbSource.Text;
        if (dlgSrcFolder.ShowDialog() == DialogResult.OK)
        {
          cbSource.Text = dlgSrcFolder.SelectedPath;
        }
      }
    }

    private void cbDest_Properties_ButtonClick_1(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
    {
      if (e.Button.Index == 1)
      {
        dlgDstFolder.SelectedPath = cbDest.Text;
        if (dlgDstFolder.ShowDialog() == DialogResult.OK)
        {
          cbDest.Text = dlgDstFolder.SelectedPath;
        }
      }
    }

    private void btnAbout_Click(object sender, EventArgs e)
    {
      AboutBox frm = new AboutBox();
      frm.ShowDialog();

      About frm2 = new About();
      frm2.ShowDialog();
    }

    private void btnAnalyze_Click(object sender, EventArgs e)
    {
      SavePathsToCombos(cbSource);
      SavePathsToCombos(cbDest);

      // Create a new file sync object
      TFileSyncData sync = new TFileSyncData();
      sync.SrcFolder = cbSource.Text;
      sync.DstFolder = cbDest.Text;
    }

    private void SavePathsToCombos(DevExpress.XtraEditors.ComboBoxEdit cbe)
    {
      string tstr = cbe.Text;
      if ((tstr.Length > 0) && (cbe.Properties.Items.IndexOf(tstr) < 0))
        cbe.Properties.Items.Add(tstr);
    }

    private void simpleButton1_Click(object sender, EventArgs e)
    {
      TFileSyncFolder fs = new TFileSyncFolder(cbSource.Text, null);

      string tstr = fs.FullPathName;

      tstr = String.Format("{0}\r\nFiles: {1}", tstr, fs.FileCount);

      tstr += "\r\nFolders: ";
      foreach (TFileSyncFolder ff in fs.Folders)
      {
        tstr += "\r\n" + String.Format("{0}  Files: {1}", ff.FolderName, ff.FileCount);
      }

      tstr += "\r\nFiles: ";
      foreach (FileInfo f in fs.Files)
      {
        tstr += "\r\n" + String.Format("{0}  Size: {1}", f.Name, f.Length);
      }

      edDebug.Text = tstr;
    }

    private void FrmFileSync_Load(object sender, EventArgs e)
    {
      cbSource.SelectedIndex = 0;
    }

    private void btnTestSrcFileList_Click(object sender, EventArgs e)
    {
      TestFileList frm = new TestFileList();
      Cursor.Current = Cursors.WaitCursor;
      TFileSyncFolder fs = new TFileSyncFolder(cbSource.Text, null);
      Cursor.Current = Cursors.Default;
      frm.SyncFolder = fs;
      frm.ShowDialog();
    }

    private void cbDest_SelectedIndexChanged(object sender, EventArgs e)
    {

    }

    private void cbSource_SelectedIndexChanged(object sender, EventArgs e)
    {

    }

    private void btnOptions_Click(object sender, EventArgs e)
    {
      FileSyncOptions frm = new FileSyncOptions();
      frm.ShowDialog();
    }
  }
}