using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Collections;

namespace FileSyncNET
{
  public class Utils
  {
    public static string IncludeTrailingPathDelimiter(string aPath)
    {
      string res = aPath;

      if ((aPath.Length > 0) && (aPath[aPath.Length - 1] != Path.DirectorySeparatorChar))
        res = res + Path.DirectorySeparatorChar;

      return res;
    }

    /// <summary>
    /// Compare two files
    /// </summary>
    /// <param name="aFile1"></param>
    /// <param name="aFile2"></param>
    /// <returns>
    /// -1 : Files are not same size
    ///  0 : Files are the same
    /// >0 : Position in file where a difference was found
    /// </returns>
    public long FileCompare(string aFile1, string aFile2)
    {
      FileStream fs1;
      FileStream fs2;
      int b1, b2;
      long result = 0;  // 0 indicates files are the same

      if (aFile1 != aFile2)   // if not the same file name
      {
        fs1 = new FileStream(aFile1, FileMode.Open, FileAccess.Read);
        fs2 = new FileStream(aFile2, FileMode.Open, FileAccess.Read);
        if (fs1.Length == fs2.Length)
        {
          do
          {
            b1 = fs1.ReadByte();
            b2 = fs2.ReadByte();
            if (b1 != b2)
              result = fs1.Position;
          }
          while ((b1 != -1) && (b2 != -1) && (result == 0));
          fs1.Close();
          fs2.Close();
        }
        else
          result = -1;
      }

      return result;
    }
  }

  public enum TProfileType { ptProfile, ptProfileGroup };

  // TProfileBase class
  //------------------------------------------------
  public class TProfileBase
  {
    private string fName;
    protected TProfileType fProfileType;

    public TProfileBase()
    {
      fProfileType = TProfileType.ptProfile;
      fName = "";
    }

    public TProfileBase(string aProfileName)
    {
      fName = aProfileName;
    }

    public string ProfileName
    {
      get { return fName; }
      set { fName = value; }
    }

    public TProfileType ProfileType
    {
      get { return fProfileType; }
    }

    public override string ToString()
    {
      return fName;
    }
  }

  // TProfile class
  //------------------------------------------------
  public class TProfile : TProfileBase
  {
    private string fSrcFolder;
    private string fDstFolder;

    public TProfile()
      : base()
    {
      Initialize();
    }

    public TProfile(string aProfileName)
      : base(aProfileName)
    {
      Initialize();
      fSrcFolder = @"E:\LabPro4.0";
      fDstFolder = @"E:\LabPro3.0";
    }

    private void Initialize()
    {
      fProfileType = TProfileType.ptProfile;
      fSrcFolder = "";
      fDstFolder = "";
    }

    public string SrcFolder
    {
      get { return fSrcFolder; }
      set { fSrcFolder = value; }
    }

    public string DstFolder
    {
      get { return fDstFolder; }
      set { fDstFolder = value; }
    }

  }

  // TProfile Group
  //------------------------------------------------
  public class TProfileGroup : TProfileBase
  {
    private List<TProfile> fProfiles;

    public TProfileGroup()
      : base()
    {
      Initialize();
    }

    public TProfileGroup(string aProfileName)
      : base(aProfileName)
    {
      Initialize();
    }
    
    private void Initialize()
    {
      fProfileType = TProfileType.ptProfileGroup;
      fProfiles = new List<TProfile>();
    }

    public List<TProfile> Profiles
    {
      get { return fProfiles; }
    }
  }

  public class TFileSyncFolder
  {
    private TFileSyncFolder fParent;
    private string fFolderName;
    private List<FileInfo> fFiles;
    private List<TFileSyncFolder> fFolders;

    public TFileSyncFolder()
    {
      fParent = null;
      fFolderName = "";
    }

    public TFileSyncFolder Parent
    {
      get { return fParent; }
    }

    public TFileSyncFolder(string aFolder, TFileSyncFolder aParent)
    {
      fParent = aParent;
      FolderName = aFolder;
    }

    public string FullPathName
    {
      get
      {
        if (fParent != null)
          return Utils.IncludeTrailingPathDelimiter(fParent.FullPathName) + fFolderName;
        else
          return fFolderName;
      }
    }

    public string FolderName
    {
      get { return fFolderName; }
      set
      {
        fFolderName = value;

        fFiles = new List<FileInfo>();
        foreach (string s in Directory.GetFiles(FullPathName))
        {
          fFiles.Add(new FileInfo(s));
        }

        TFileSyncFolder fs;
        fFolders = new List<TFileSyncFolder>();
        foreach (string s in Directory.GetDirectories(FullPathName))
        {
          fs = new TFileSyncFolder(Path.GetFileName(s), this);
          fFolders.Add(fs);
        }
      }
    }

    public int FileCount
    {
      get { return fFiles.Count; }
    }

    public List<FileInfo> Files
    {
      get { return fFiles; }
    }

    public int FolderCount
    {
      get { return fFolders.Count; }
    }

    public List<TFileSyncFolder> Folders
    {
      get { return fFolders; }
    }

    private void BuildFolderList(TFileSyncFolder aFolder, ArrayList aList)
    {
      aList.Add(aFolder.FullPathName);
      if (aFolder.FolderCount > 0)
      {
        foreach (TFileSyncFolder sf in aFolder.Folders)
          BuildFolderList(sf, aList);
      }
    }

    public ArrayList FullFolderList
    {
      get
      {
        ArrayList list = new ArrayList();
        BuildFolderList(this, list);
        return list;
      }
    }

    public string RootPath
    {
      get
      {
        TFileSyncFolder fs = this;

        while (fs.Parent != null)
        {
          fs = fs.Parent;
        }
        return fs.FullPathName;
      }
    }

    public string PathWithoutRoot(string aPath)
    {
      string root = RootPath;
      if (aPath.StartsWith(root))
        return aPath.Substring(root.Length, aPath.Length - root.Length);
      else
        return "";
    }
  }

  /// <summary>
  /// Class to hold all file sync data
  /// </summary>
  public class TFileSyncData
  {
    private string fSrcFolder;
    private string fDstFolder;
    private List<TFileSyncFolder> fFolders;

    public TFileSyncData()
    {
      fFolders = new List<TFileSyncFolder>();
      fFolders.Add(new TFileSyncFolder());
    }

    public string SrcFolder
    {
      get { return fSrcFolder; }
      set { fSrcFolder = value; }
    }

    public string DstFolder
    {
      get { return fDstFolder; }
      set { fDstFolder = value; }
    }

    public void SyncData()
    {
      // Gather all folder data
    }
  }


}
