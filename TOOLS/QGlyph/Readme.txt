QuickGlyph Version 1.0 ( 32-Bit for Windows 95/NT )
12th August 1996



Introduction
------------

Hi ! Thanks for taking the time to read this readme file. QuickGlyph 
is generally a Glyph Viewer with some extras. I decided to write my 
own Glyph Viewer because the ones I've tried didn't exactly quite 
meet my needs. By the way, QuickGlyph is freeware ( I don't  think a 
simple app like this deserve any payment, but if you really like this 
program and write some Delphi programs/components of your own, it 
would be nice if you would email me a copy of it ) , so you may 
distribute this program as you wish. 


Disclaimer
----------

This program is provided AS IS without any warranty of any kind, 
either expressed or implied. The author does not assume any liability 
for any alleged or actual damages arising from the use of this program. 
Use this program at YOUR OWN RISK.


Installation
------------

To install QuickGlyph, just create a new directory and copy all the
files in this ZIP archive to that directory. You can then add this 
program to the TOOLS menu in Delphi, or create a shortcut to it and 
place it wherever you like.



Contacting The Author
---------------------

Name  : Christopher Tan
Email : chris@cytec.pl.my

Please use the word "QuickGlyph" in the subject field of your email.
By the way, if you're interested in the source code, just email me and 
I'll email it to you.
