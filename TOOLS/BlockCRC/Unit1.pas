unit Unit1;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls;

type
  TForm1 = class(TForm)
    Button1: TButton;
    Memo1: TMemo;
    Label1: TLabel;
    procedure Button1Click(Sender: TObject);
  private
    { Private declarations }
  public
    procedure CalcIt(buf: PChar);
  end;

var
  Form1: TForm1;

implementation

{$R *.DFM}

function Test2(inParm: integer): integer;
begin
  asm
    mov eax,$020;
//    mov @result,eax
  end;
end;

function BlockCRC(OldCrc: Word; Buf: Pointer; BufSize: Integer): Word; register;
(* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
This function is a recreation of the BlockCRC module in the old WAGPIB that is
used to calculate the CRC for a w/a msg.

This is to calculate the CRC of a block of data. This routine uses a byte-wise
algorythm to calculate the CRC For information on how this works see Byte-wise
CRC Calculations", IEEE Micro, June 83 All subscripts have been decremented by
1 the value to xor with CRC in <dx> is n15 through n0 the byte of memory is m0
to m7, xi = mi xor ri

Function block_crc( old_crc    : Integer;
block_addr : ptr;
block_len  : Integer ) : Integer;

Compute the CRC of bytes starting at BLOCK_START for BLOCK_SIZE bytes, using
OLD_CRC as the starting value (for blocks larger than 64K bytes.).  Return the
new CRC value.

1/Aug/91, Brett - modified to work with Borland C for Windows, where caller
may be using SI for register variable. Would also save DI if used by this
routine.

30/Sept/97, Joseph Good - Modified code to work in Delphi (32 bit)

NOTE: We must preserve the EDI, ESI, ESP, EBP, and EBX  (So Delphi
says...)

EAX   ................................
AX                   ................
AH                   ........
AL                           ........

* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *)
begin
  try
    asm
      push  EBX                  // preserve this one because we play with BX
      push  ESI                  // preserve ESI cuz Delphi says to
      xor   EDX, EDX             // Clear EDX
      mov   DX,  oldCrc          // Integer ptr 10[bp]; get initial CRC
      mov   ESI, buf             // get address of block
      mov   ECX, BufSize         // get the size of our buffer

@@crc1:
      lodsb                      // get next byte from buffer
      xor   AL, DL               // Start the CRC
      mov   BH, AL               // <bh> = x7 to x0
      add   AL, AL               // <al> = x6 to x0 <fPARITY> = n14 <fCARRY> = x7
      pushf                      // Save <FLAGS>, <fPARITY> <fCARRY>
      xor   BH, AL               // <bh> = n13 to n6
      xor   AL, AL               // make xx7 = 0 and xx6 = 0
      popf                       // Get <FLAGS>, <fPARITY> <fCARRY>
      jpe   @@crc2
      mov   AL, 011b             // <al1>,<al0> = n14
@@crc2:
      jnc   @@crc3
      xor   AL, 010b             // if x7 = 1 then xx8 = not xx8
@@crc3:
      mov   BL, AL               // <b1> = n15, <b0> = n14
      shr   AL, 1                // n0 = n15, <a0> = n0
      ror   BX, 1
      ror   BX, 1
      or    BL, AL               // <bx> = n15 to n0
      xor   BL, DH               // <bx> = r15 to r0
      mov   DX, BX               // save CRC in dx
      loop  @@crc1               // do all bytes
      xor   EAX, EAX
      mov   AX, DX
      pop   ESI
      pop   EBX
      mov   @result, AX          // return CRC in AX
    end; {asm}
  except
    result := 0;
  end;
end;

(* This is the old one!
function BlockCRC(oldCrc: integer; buf: pointer; bufSize: integer): integer; register;
// This function is a recreation of the BlockCRC module in the old
// WAGPIB that is used to calculate the CRC for a w/a msg.
begin
{       This is to calculate the CRC of a block of data.
;       This routine uses a byte-wise algorythm to calculate the CRC
;       For information on how this works see
;         "Byte-wise CRC Calculations", IEEE Micro, June 83
;       All subscripts have been decremented by 1
;       the value to xor with CRC in <dx> is n15 through n0
;       the byte of memory is m0 to m7, xi = mi xor ri
;
;       Function block_crc( old_crc    : integer;
;                           block_addr : ptr;
;                           block_len  : integer ) : integer;
;
;       Compute the CRC of bytes starting at BLOCK_START for BLOCK_SIZE
;       bytes, using OLD_CRC as the starting value (for blocks larger
;       than 64K bytes.).  Return the new CRC value.
;
;       1/Aug/91, Brett - modified to work with Borland C for Windows, where
;                         caller may be using SI for register variable.
;                         Would also save DI if used by this routine.
}
  ASM
    push    EBX
    mov     EDX,oldCrc          // word ptr 10[bp]      ; get initial CRC
    mov     ESI,buf             // get address of block
@@crc1:
    lodsb
    xor     AL,DL
    mov     BH,AL               // <bh> = x7 to x0
    add     al,al               // <al> = x6 to x0 <pf> = n14 <cf> = x7
    pushf                       // save <pf> and <cf>
    xor     bh,al               // <bh> = n13 to n6
    xor     al,al               // make xx7 = 0 and xx6 = 0
    popf
    jpe      @@crc2
    mov      al,011b            // <al1>,<al0> = n14
@@crc2:
    jnc     @@crc3
    xor     al,010b             // if x7 = 1 then xx8 = not xx8
@@crc3:
    mov     bl,al               // <b1> = n15, <b0> = n14
    shr     al,1                // n0 = n15, <a0> = n0
    ror     bx,1
    ror     bx,1
    or      bl,al               // <bx> = n15 to n0
    xor     bl,dh               // <bx> = r15 to r0
    mov     dx,bx               // save CRC in dx
    loop    @@crc1              // do all bytes
    xor     EAX,EAX
    mov     ax, dx
    pop     EBX
    mov     @result,eax          //return CRC in AX
  end;
end;
*)

procedure TForm1.CalcIt(buf: PChar);
var
  k   : integer;
begin
  k:= BlockCRC(0, buf, StrLen(buf));
  label1.caption:= IntToStr(k);
end;

procedure TForm1.Button1Click(Sender: TObject);
var
  s   : string;
begin
  s:= memo1.text;
  CalcIt(PChar(s));
end;

end.
