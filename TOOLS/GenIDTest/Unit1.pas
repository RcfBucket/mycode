unit Unit1;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Db, DBTables;

type
  TForm1 = class(TForm)
    Database1: TDatabase;
    sNextSeq: TStoredProc;
    sGenNextSeq: TStoredProc;
    Button1: TButton;
    Button2: TButton;
    Edit1: TEdit;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

{$R *.DFM}

procedure TForm1.Button1Click(Sender: TObject);
var
  k   : integer;

begin
  label4.caption:= '---';
  label3.Caption:= TimeToStr(Now);
  for k:= 1 to strtoint(edit1.text) do
  begin
    sNextSeq.ExecProc;
    label1.caption:= sNextSeq.ParamByName('ASEQ').AsString;
    label1.Update;
  end;
  label4.Caption:= TimeToStr(Now);
end;

procedure TForm1.Button2Click(Sender: TObject);
var
  k   : integer;
begin
  label6.caption:= '---';
  label5.Caption:= TimeToStr(Now);
  for k:= 1 to strtoint(edit1.text) do
  begin
    sGenNextSeq.ExecProc;
    label2.caption:= sGenNextSeq.ParamByName('ASEQ').AsString;
    label2.Update;
  end;
  label6.Caption:= TimeToStr(Now);
end;

end.
