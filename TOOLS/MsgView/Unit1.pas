unit Unit1;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Db, DBTables;

type
  TMyEdit = class(TEdit)
    procedure WndProc(var msg: TMessage); override;
  end;

  TForm1 = class(TForm)
    Edit1: TEdit;
    Button1: TButton;
    Button2: TButton;
    ListBox1: TListBox;
    Button3: TButton;
    Edit2: TEdit;
    Label1: TLabel;
    Database1: TDatabase;
    Query1: TQuery;
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure Button3Click(Sender: TObject);
    procedure Edit2Change(Sender: TObject);
  private
    e   : TMyEdit;
  public
    { Public declarations }
  end;

var
  alist   : TListBox;
  Form1   : TForm1;

implementation

{$R *.DFM}

const
  sWndProc = 'WndProc - %s [%d : %s]';

function MsgStr(amsg: integer): string;
var            tmsg
  tstr  : string;
begin
  with form1.query1 do
  begin
    SQL.Clear;
    SQL.Add('SELECT * FROM WMConsts where AsDec = ' + IntToStr(amsg));
    SQL.Add('ORDER BY CONST');
    Open;
    if not Eof then
    begin
      tstr:= '';
      while not Eof do
      begin
        tstr:= tstr + FieldByName('Const').AsString + ',';
        Next;
      end;
      if tstr[Length(tstr)] = ',' then
        system.Delete(tstr, Length(tstr), 1);
      result:= tstr;
    end
    else
      result:= '???';
    Close;
  end;
end;

procedure TMyEdit.WndProc(var msg: TMessage);
begin
  if (msg.Msg = WM_KEYDOWN) or (msg.msg >= CN_BASE) then
    aList.items.Add(Format(sWndProc, ['BEGIN', msg.msg, MsgStr(msg.msg)]));
  inherited;
  if (msg.Msg = WM_KEYDOWN) then
    aList.items.Add(Format(sWndProc, ['END', msg.msg, MsgStr(msg.msg)]));
end;

procedure TForm1.Button1Click(Sender: TObject);
begin
  Close;
end;

procedure TForm1.Button2Click(Sender: TObject);
begin
  if e = nil then
  begin
    aList:= listBox1;
    e:= TMyEdit.Create(self);
    e.left:= edit1.left;
    e.top:= edit1.top + edit1.Height + 10;
    e.parent:= self;
  end
  else
  begin
    e.Free;
    e:= nil;
  end;
end;

procedure TForm1.Button3Click(Sender: TObject);
begin
  listbox1.Clear;
end;

procedure TForm1.Edit2Change(Sender: TObject);
begin
  with query1 do
  begin
    SQL.Clear;
    SQL.Add('SELECT * FROM WMConsts where Const = ''' + edit2.text + '''');
    Open;
    if not Eof then
      label1.Caption:= FieldByName('AsHex').AsString + ' - ' + FieldByName('AsDec').AsString
    else
      label1.caption:= 'n/r';
    Close;
  end;
end;

end.
