unit Main;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, FileCtrl, ExtCtrls, DBTables, Db;

Const
  WM_MyOne = $20;
  WM_MyTwo = 64;
  WM_MyThree = WM_MyOne + $3;
  WM_MyFour = (WM_MyOne + WM_MyTwo) + 10;


type
  TFrmMain = class(TForm)
    dirList: TDirectoryListBox;
    driveList: TDriveComboBox;
    BtnBuild: TButton;
    BtnView: TButton;
    BtnClose: TButton;
    DBMain: TDatabase;
    Query1: TQuery;
    Label1: TLabel;
    CheckBox1: TCheckBox;
    procedure BtnViewClick(Sender: TObject);
    procedure BtnCloseClick(Sender: TObject);
    procedure BtnBuildClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
  end;

var
  FrmMain: TFrmMain;

implementation

uses viewdb, Search;

{$R *.DFM}



procedure TFrmMain.BtnViewClick(Sender: TObject);
begin
  with TFrmViewDB.Create(self) do
  begin
    ShowModal;
  end;
end;

procedure TFrmMain.BtnCloseClick(Sender: TObject);
begin
  Close;
end;

procedure TFrmMain.BtnBuildClick(Sender: TObject);
begin
  screen.Cursor:= crHourglass;
  if checkbox1.Checked then
    query1.ExecSQL;
  screen.Cursor:= crDefault;

    with TFrmSearch.Create(self) do
    begin
      lblSearch.caption:= dirList.directory;
      ShowModal;
    end;
end;

procedure TFrmMain.FormShow(Sender: TObject);
begin
//  dirList.Directory:= 'F:\Borland\Delphi\Source';
end;

end.
