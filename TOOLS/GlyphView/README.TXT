GlyphView 32bit version 2.0
***************

Welcome to GlyphView 32 bit...

Install GVIEW32.EXE into your Delphi 2.0 bin directory
	eg: copy glyphview32.exe \program files\borland\delphi 2.0\bin

If you want to, add it to your Tools menu in Delphi.
	eg: Tools|Tools|Add...

Read the help file (GVIEW32.HLP) first! It contains operation instructions
and legal stuff.

***************************************************************************
If you are using GlyphView 32 for free....

* Please consider sending us $20. You dont have to... and there is no 
limitation on the program. We will send you the source code if you send
($20 or more). If we receive enough input $:-) we will consider
developing a better version...

You can do it even simpler.  GO SWREG on CompuServe, ID is 11508.

NB: Your honest pays! US! :)
****************************************************************************

keywest software
Copyright 1996� All Rights Reserved.

GLYPHVIEW 32 was written in Borland Delphi 2.0 :) (WAY COOL!)

** By using the program, you accept a standard software agreement as detailed in
the help file GVIEW32.HLP.
