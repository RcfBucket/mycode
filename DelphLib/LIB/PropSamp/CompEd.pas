unit CompEd;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, ExtCtrls;

type
  TFrmCompEd = class(TForm)
    Label1: TLabel;
    Bevel1: TBevel;
    BtnOK: TButton;
    BtnCancel: TButton;
    Label2: TLabel;
    Label3: TLabel;
    edSpecProp: TEdit;
    edProp: TMemo;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    edObjNum: TEdit;
    edObjStr: TEdit;
    Label7: TLabel;
    edObjNonPub: TEdit;
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

implementation

{$R *.DFM}

procedure TFrmCompEd.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
var
  k, code : integer;
begin
  CanClose:= true;
  if (ModalResult = mrOK) then
  begin
    if (edObjNum.text <> '') then
    begin
      Val(edObjNum.text, k, code);
      if code <> 0 then
      begin
        application.MessageBox('Invalid numeric format.', 'Error', MB_ICONERROR or MB_OK);
        edObjNum.SetFocus;
        CanClose:= false;
      end;
    end;
  end;
end;

end.
