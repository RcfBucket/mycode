unit SamplReg;

{---------------------------------------------------------------------------

 Created: 10/29/96  10:36:45 AM

 Purpose: Registers the sample component and its property/component editors

 Author : RCF
----------------------------------------------------------------------------}

INTERFACE

uses
  SysUtils, Controls, Classes, Forms, Dialogs, DsgnIntf, TypInfo, 
  Sample;

procedure Register;

IMPLEMENTATION

uses
  PropEd, PropEd2, CompEd;

type
  TSampPropEd = class(TStringProperty)
    function GetAttributes: TPropertyAttributes; override;
    procedure Edit; override;
    function GetValue: string; override;
  end;

  TSampPropEd2 = class(TClassProperty)
    function GetAttributes: TPropertyAttributes; override;
    procedure Edit; override;
  end;

  TSampPropEd3 = class(TStringProperty)
    function GetAttributes: TPropertyAttributes; override;
    procedure GetValues(proc: TGetStrProc); override;
    procedure SetValue(const value: string); override;
  end;

  TSampCompEd = class(TComponentEditor)
    function GetVerbCount: integer; override;
    function GetVerb(index: integer): string; override;
    procedure Edit; override;
    procedure ExecuteVerb(index: integer); override;
  end;

{---------------------------------------------------------- [ TSampCompEd ]--}

function TSampCompEd.GetVerbCount: integer;
begin
  result:= 2;
end;

function TSampCompEd.GetVerb(index: integer): string;
begin
  result:= '';
  case (index) of
    0   : result:= 'About Sample Component';
    1   : result:= 'Edit Sample Component';
  end;
end;

procedure TSampCompEd.Edit;
begin
  ExecuteVerb(1);
end;

procedure TSampCompEd.ExecuteVerb(index: integer);
var
  f     : TFrmCompEd;
  c     : TSample;
begin
  case (index) of
    0 : ShowMessage('About Component Editor');
    1 : begin
          f:= TFrmCompEd.Create(application);
          try
            c:= Component as TSample;
            f.Caption:= c.owner.name + '.' + c.name;

            f.edProp.text:= c.AProperty;
            f.edSpecProp.text:= c.ASpecialProp;
            f.edObjStr.text:= c.AnObject.Str;
            f.edObjNum.text:= IntToStr(c.AnObject.Num);
            f.edObjNonPub.text:= c.AnObject.NonPublishedStr;

            if (f.ShowModal = mrOK) then
            begin
              c.AProperty:= f.edProp.text;
              c.ASpecialProp:= f.edSpecProp.text;
              c.AnObject.Str:= f.edObjStr.text;
              c.AnObject.Num:= StrToInt(f.edObjNum.text);
              c.AnObject.NonPublishedStr:= f.edObjNonPub.text;
              designer.Modified;
            end;
          finally
            f.Free;
          end;
        end;
  end;
end;

{--[ TSampPropEd ]----------------------------------------------------------}

function TSampPropEd.GetValue: string;
begin
  result:= GetStrValue;
end;


function TSampPropEd.GetAttributes: TPropertyAttributes;
begin
  result:= [paDialog];
end;

procedure TSampPropEd.Edit;
var
  f   : TFrmPropEd;
begin
  f:= TFrmPropEd.Create(application);
  try
    f.edProp.Text:= GetStrValue;
    f.Caption:= TComponent(GetComponent(0)).owner.name + '.' +
                TComponent(GetComponent(0)).name +
                '.' + GetName;
    if f.ShowModal = mrOK then
    begin
      SetStrValue(f.edProp.text);
      Designer.Modified;
    end;
  finally
    f.Free;
  end;
end;

{--[ TSampPropEd2 ]----------------------------------------------------------}

function TSampPropEd2.GetAttributes: TPropertyAttributes;
begin
  result:= inherited GetAttributes + [paDialog];
end;

procedure TSampPropEd2.Edit;
var
  f   : TFrmPropEd2;
  obj : TSampleObject;
begin
  f:= TFrmPropEd2.Create(application);
  try
    obj:= TSampleObject(GetOrdValue);
    f.edStr.Text:= obj.Str;
    f.edNum.text:= IntToStr(obj.Num);
    f.edNonPub.text:= obj.NonPublishedStr;
    f.Caption:= TComponent(GetComponent(0)).owner.name + '.' +
                TComponent(GetComponent(0)).name +
                '.' + GetName;
    if f.ShowModal = mrOK then
    begin
      obj.Str:= f.edStr.text;
      obj.Num:= f.Updown1.position;
      obj.NonPublishedStr:= f.edNonPub.text;
      Designer.Modified;
    end;
  finally
    f.Free;
  end;
end;

{--[ TSampPropEd2 ]----------------------------------------------------------}

function TSampPropEd3.GetAttributes: TPropertyAttributes;
begin
  result:= inherited GetAttributes + [paValueList];
end;

procedure TSampPropEd3.GetValues(proc: TGetStrProc);
var
  k   : integer;
begin
  for k:= 1 to 8 do
  begin
    Proc('Sample Value ' + IntToStr(k));
  end;
end;

procedure TSampPropEd3.SetValue(const value: string);
var
  k       : integer;
  s1, s2  : string;
  tmpVal  : string;
  isGood  : boolean;
begin
  tmpVal:= UpperCase(value);
  isGood:= false;
  for k:= 1 to 8 do
  begin
    s1:= 'Sample Value ' + IntToStr(k);
    s2:= UpperCase(s1);
    if (tmpVal = s2) then
    begin
      SetStrValue(s1);
      isGood:= true;
    end;
  end;
  if (not isGood) then
  begin
    SetStrValue('');
    raise Exception.Create('"' + value + '" is not a valid property value.');
  end;
end;

procedure Register;
begin
  RegisterComponents('Custom', [TSample]);
  RegisterPropertyEditor( TypeInfo(string),
                          TSample,
                          'AProperty',
                          TSampPropEd);

  RegisterPropertyEditor( TypeInfo(TSampleObject),
                          TSample,
                          '',
                          TSampPropEd2);

  RegisterPropertyEditor( TypeInfo(string),
                          TSample,
                          'ASpecialProp',
                          TSampPropEd3);

  RegisterComponentEditor(TSample, TSampCompEd);
end;


// INITIALIZATION

// FINALIZATION

END.

