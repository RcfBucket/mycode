unit Sample;

{- This is a sample component that shows how to properly use Component and
   property editors }

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls;

type
  TSampleObject = class(TPersistent)
  private
    fAttr1  : string;
    fAttr2  : integer;
    fNonStr : string;
  protected
    procedure WriteNonPub(writer: TWriter);
    procedure ReadNonPub(reader: TReader);
  public
    procedure DefineProperties(filer: TFiler); override;
    property NonPublishedStr: string read fNonStr write fNonStr;
  published
    property Str: string read fAttr1 write fAttr1;
    property Num: integer read fAttr2 write fAttr2;
  end;

  TSample = class(TGraphicControl)
  private
    fProp   : string;
    fSProp  : string;
    fObj    : TSampleObject;
    procedure SetSpecProp(value: string);
  protected
    procedure Paint; override;
  public
    constructor Create(aOwner: TComponent); override;
    destructor Destroy; override;
  published
    property AProperty: string read fProp write fProp;
    property ASpecialProp: string read fSProp write SetSpecProp;
    property AnObject: TSampleObject read fObj write fObj;
  end;

implementation

procedure TSampleObject.DefineProperties(filer: TFiler);
begin
  inherited DefineProperties(filer);
  filer.DefineProperty('NonPublishedStr', ReadNonPub, WriteNonPub, true);
end;

procedure TSampleObject.WriteNonPub(writer: TWriter);
begin
  writer.WriteString(NonPublishedStr);
end;

procedure TSampleObject.ReadNonPub(reader: TReader);
begin
  NonPublishedStr:= reader.ReadString;
end;

constructor TSample.Create(aOwner: TComponent);
begin
  inherited Create(aOwner);
  fObj:= TSampleObject.Create;
  width:= 150;
  height:= 50;
end;

destructor TSample.Destroy;
begin
  fObj.Free;
  inherited Destroy;
end;

procedure TSample.SetSpecProp(value: string);
begin
  if value <> fSProp then
  begin
    fSProp:= value;
  end;
end;

procedure TSample.Paint;
var
  rect    : TRect;
  x, y    : integer;
  tstr    : string;
begin
  inherited Paint;
  rect:= ClientRect;
  Frame3D(canvas, rect, clWhite, clGray, 2);
  Frame3D(canvas, rect, clGray, clWhite, 2);
  with canvas do
  begin
    brush.Color:= clBtnFace;
    tstr:= 'Sample Component';
    x:= rect.left + ((rect.right - rect.left) div 2) - (canvas.TextWidth(tstr) div 2);
    y:= rect.top + ((rect.bottom - rect.top) div 2) - (canvas.TextHeight('S') div 2);
    canvas.TextRect(rect, x, y, tstr);
  end;
end;

end.
