unit PropEd2;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ComCtrls, StdCtrls, ExtCtrls;

type
  TFrmPropEd2 = class(TForm)
    Label1: TLabel;
    Bevel1: TBevel;
    BtnOK: TButton;
    BtnCancel: TButton;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    edStr: TEdit;
    edNum: TEdit;
    UpDown1: TUpDown;
    Label5: TLabel;
    edNonPub: TEdit;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FrmPropEd2: TFrmPropEd2;

implementation

{$R *.DFM}

end.
