Unit DebugIt;

interface

Uses
  Windows,
  SysUtils,
  Messages;

procedure DebugStr(const DebugMsg: String);

Implementation

procedure DebugStr(const DebugMsg: String);
var
  CDS	: TCopyDataStruct;
  DbWin	: hWnd;
  Msg	: PChar;
  LenStr	: Integer;
begin
  DbWin := FindWindow('TDbWinScrn', nil);
  if DbWin <> 0 then begin
    LenStr := Length(DebugMsg) + 1;
    CDS.cbData := LenStr;
    GetMem(Msg,LenStr);
    Try
      StrPCopy(Msg,DebugMsg);
      CDS.lpData := Msg;
      SendMessage(DbWin, WM_COPYDATA, 0, LParam(@CDS));
    Finally
      FreeMem(Msg,LenStr);
      end;

    end;
end;

end.
