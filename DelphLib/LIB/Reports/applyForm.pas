unit applyForm;

INTERFACE

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls;

type
  TApplyEvent = procedure(sender: TObject) of object;

  TFrmApply = class(TForm)
    BtnApply: TButton;
    procedure BtnApplyClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    fOnApply    : TApplyEvent;
    fModified   : boolean;
    procedure SetModified(value: boolean);
  protected
    procedure DoApply; virtual;
  public
    constructor Create(aOwner: TComponent); override;
    property OnApply: TApplyEvent read fOnApply write fOnApply;

    // modified will be true if the setting in the dialog change
    // it is up to the inherited classes to maintain this when
    // items change.
    property Modified: boolean read fModified write SetModified;
  end;

IMPLEMENTATION

{$R *.DFM}

constructor TFrmApply.Create(aOwner: TComponent);
begin
  inherited Create(aOwner);
  fOnApply:= nil;
end;

procedure TFrmApply.SetModified(value: boolean);
begin
  fModified:= value;
  BtnApply.Enabled:= fModified;
end;

procedure TFrmApply.DoApply;
// call the apply event (if one is set)
begin
  Modified:= false;
  if Assigned(fOnApply) then
    fOnApply(self);
end;

procedure TFrmApply.BtnApplyClick(Sender: TObject);
begin
  DoApply;
end;

procedure TFrmApply.FormShow(Sender: TObject);
begin
  Modified:= false;
end;

END.
