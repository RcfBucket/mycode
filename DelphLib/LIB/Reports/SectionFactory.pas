unit SectionFactory;

INTERFACE

uses
  SysUtils, Classes, Graphics, Windows, Forms, Dialogs, TwipLib, SectBaseEd;

type
  EReportSection = class(Exception);

  TReportSection = class
  private
    fLeft           : TLTwips;
    fTop            : TLTwips;
    fWidth          : TLTwips;
    fHeight         : TLTwips;
    fFont           : TFont;
    fBorderStyle    : TPenStyle;
    fBorderColor    : TColor;
    fOfsX           : TLTwips;
    fOfsY           : TLTwips;
    data            : pointer;  // s/b TGatherSpecimen
  protected
    fDrawn          : boolean;
    function GetDrawComplete: boolean; virtual;
    function GetSectionName: string;
  public
    constructor Create; virtual;
    destructor Destroy; override;

    procedure Read(s: TStream); virtual;
    procedure Write(s: TStream); virtual;
    procedure Draw(aCanvas: TCanvas); virtual;
    procedure Edit; virtual;

    property Left: TLTwips read fLeft write fLeft;
    property Top: TLTwips read fTop write fTop;
    property Width: TLTwips read fWidth write fWidth;
    property Height: TLTwips read fWidth write fWidth;
    property Name: string read GetSectionName;
    property Font: TFont read fFont write fFont;
    property BorderStyle: TPenStyle read fBorderStyle write fBorderStyle;
    property BorderColor: TColor read fBorderColor write fBorderColor;
    property DrawComplete: boolean read GetDrawComplete;
    property OffsetX: TLTwips read fOfsX write fOfsX;
    property OffsetY: TLTwips read fOfsY write fOfsY;
  end;

IMPLEMENTATION

constructor TReportSection.Create;
begin
  inherited Create;
  fFont:= TFont.Create;
  fFont.Name:= 'Arial';
  fLeft:= 0;
  fTop:= 0;
  fHeight:= 1440; // height and width undefined
  fWidth:= 1440;
  fBorderStyle:= psSolid;
  fBorderColor:= clBlack;
  fOfsX:= 0;
  fOfsY:= 0;
  fDrawn:= false;
end;

destructor TReportSection.Destroy;
begin
  fFont.Free;
  inherited Destroy;
end;

function TReportSection.GetSectionName: string;
// return the name of the section
begin
  result:= 'Report Section Default';
end;

procedure TReportSection.Read(s: TStream);
// read section data from stream
begin
end;

procedure TReportSection.Write(s: TStream);
// writes all section data to the stream
begin
end;

procedure TReportSection.Draw(aCanvas: TCanvas);
// draw the section of the canvas.  Assumes that the canvas has been set up in
// Logical Twips mapping mode.
// The base section type will setup the font pen styles and colors and
// also draw a frame rect (if specified)
var
  zoom  : double;
  x,y   : TLTwips;
begin
  if not IsLogicalTwips(aCanvas.handle, Zoom) then
    raise EReportSection.Create('ReportSection.Draw: Logical twips not selected into canvas');

  with aCanvas do
  begin
    font.Assign(fFont);
    font.Height:= -PointsToLTwips(fFont.Size);  // convert points to twips

    pen.Style:= fBorderStyle;
    pen.Color:= fBorderColor;
    brush.Style:= bsSolid;
    brush.Color:= clWhite;

    x:= fLeft + fOfsX;
    y:= fTop + fOfsY;

    // draw the border if specified
    if fBorderStyle <> psClear then
      Rectangle(x, y, x + fWidth, y + fHeight);
  end;
  fDrawn:= true;
end;

procedure TReportSection.Edit;
begin
  with TFrmSectBaseEd.Create(application) do
  begin
    fFont.Assign(self.fFont);
    ShowModal;
    Free;
  end;
end;

function TReportSection.GetDrawComplete: boolean;
begin
  result:= fDrawn;
end;

END.
