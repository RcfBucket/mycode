unit SectBaseEd;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, ExtCtrls, ColorGrd;

type
  TFrmSectBaseEd = class(TForm)
    RadioGroup1: TRadioGroup;
    BtnFont: TButton;
    Label1: TLabel;
    lblFontName: TLabel;
    ColorGrid2: TColorGrid;
    BtnOK: TButton;
    BtnCancel: TButton;
    procedure FormShow(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
  private
    { Private declarations }
  public
    fFont   : TFont;
  end;

var
  FrmSectBaseEd: TFrmSectBaseEd;

implementation

{$R *.DFM}


procedure TFrmSectBaseEd.FormShow(Sender: TObject);
begin
  lblFontName.caption:= Format('%s (%d)', [fFont.Name, fFont.Size]);
end;

procedure TFrmSectBaseEd.FormCreate(Sender: TObject);
begin
  fFont:= TFont.Create;
end;

procedure TFrmSectBaseEd.FormDestroy(Sender: TObject);
begin
  fFont.Free;
end;

end.
