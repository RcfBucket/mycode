unit RptMain;

(* TODO = add rubberband select
    locking multiple controls doesnt work
*)

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  boxer, StdCtrls, ComCtrls, ExtPage, ExtCtrls, DB, DBTables, BoxWin,
  PaperView, Buttons, Menus, TwipLib, ToolWin;

type
  TFrmMain = class(TForm)
    MainMenu1: TMainMenu;
    Edit2: TMenuItem;
    mnuLock: TMenuItem;
    N3: TMenuItem;
    Paste1: TMenuItem;
    Copy1: TMenuItem;
    Cut1: TMenuItem;
    File1: TMenuItem;
    MnuExit: TMenuItem;
    N1: TMenuItem;
    PrintSetup1: TMenuItem;
    Print1: TMenuItem;
    N2: TMenuItem;
    SaveAs1: TMenuItem;
    Save1: TMenuItem;
    Open1: TMenuItem;
    New1: TMenuItem;
    Help1: TMenuItem;
    About1: TMenuItem;
    HowtoUseHelp1: TMenuItem;
    SearchforHelpOn1: TMenuItem;
    Contents1: TMenuItem;
    MnuPrefs: TMenuItem;
    N5: TMenuItem;
    pView: TPaperView;
    Boxer1: TBoxer;
    Close1: TMenuItem;
    PopupMenu1: TPopupMenu;
    MnuEditSection: TMenuItem;
    N6: TMenuItem;
    Cancel1: TMenuItem;
    Lock1: TMenuItem;
    BarMainTools: TToolBar;
    ImgMainTools: TImageList;
    BtnExit: TToolButton;
    BtnPreferences: TToolButton;
    cboZoom: TComboBox;
    ToolButton1: TToolButton;
    BtnShowMargins: TToolButton;
    BtnShowGrid: TToolButton;
    BtnSnap: TToolButton;
    MnuDelete: TMenuItem;
    MnuSelectAll: TMenuItem;
    mnuUnlock: TMenuItem;
    MnuDeletePop: TMenuItem;
    MnuListAll: TMenuItem;
    BarStatus: TStatusBar;
    BtnListSections: TToolButton;
    ToolButton4: TToolButton;
    View1: TMenuItem;
    Preview1: TMenuItem;
    N7: TMenuItem;
    Zoom1: TMenuItem;
    MnuAlign: TMenuItem;
    BarAlign: TToolBar;
    BtnAlignLeft: TToolButton;
    BtnAlignHCenter: TToolButton;
    BtnAlignHWinCenter: TToolButton;
    BtnAlignRight: TToolButton;
    BtnAlignTop: TToolButton;
    BtnAlignVCenter: TToolButton;
    BtnAlignVWinCenter: TToolButton;
    ToolButton11: TToolButton;
    ImgAlignTools: TImageList;
    BtnSelect: TToolButton;
    ToolButton3: TToolButton;
    ToolButton2: TToolButton;
    ToolButton6: TToolButton;
    ToolButton7: TToolButton;
    ToolButton8: TToolButton;
    ToolButton5: TToolButton;
    BoxWin1: TBoxWin;
    ToolButton9: TToolButton;
    ToolButton10: TToolButton;
    procedure FormShow(Sender: TObject);
    procedure pViewMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure ExitClick(Sender: TObject);
    procedure MnuPrefsClick(Sender: TObject);
    procedure Lock1Click(Sender: TObject);
    procedure PopupMenu1Popup(Sender: TObject);
    procedure ToolButton5Click(Sender: TObject);
    procedure BtnShowMarginsClick(Sender: TObject);
    procedure BtnShowGridClick(Sender: TObject);
    procedure BtnSnapClick(Sender: TObject);
    procedure Boxer1BoxSizing(Sender: TObject; aRect: TRect);
    procedure Boxer1BoxCreated(Sender: TObject; aRect: TRect);
    procedure MnuSelectAllClick(Sender: TObject);
    procedure Boxer1BoxAborted(Sender: TObject);
    procedure mnuLockClick(Sender: TObject);
    procedure MnuDeleteClick(Sender: TObject);
    procedure MnuDeletePopClick(Sender: TObject);
    procedure BtnListSectionsClick(Sender: TObject);
    procedure MnuListAllClick(Sender: TObject);
    procedure Zoom1Click(Sender: TObject);
    procedure cboZoomChange(Sender: TObject);
    procedure cboZoomClick(Sender: TObject);
    procedure cboZoomKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure MnuAlignClick(Sender: TObject);
    procedure BtnAlignLeftClick(Sender: TObject);
    procedure BtnAlignTopClick(Sender: TObject);
    procedure ToolButton11Click(Sender: TObject);
    procedure BtnAlignRightClick(Sender: TObject);
    procedure BtnAlignHCenterClick(Sender: TObject);
    procedure BtnAlignHWinCenterClick(Sender: TObject);
    procedure BtnAlignVCenterClick(Sender: TObject);
    procedure BtnAlignVWinCenterClick(Sender: TObject);
    procedure BtnSelectClick(Sender: TObject);
    procedure About1Click(Sender: TObject);
    procedure pViewZoomChange(Sender: TObject);
    procedure ToolButton9Click(Sender: TObject);
    procedure MnuEditSectionClick(Sender: TObject);
  private
    r           : TRect;
    selecting   : TSpeedButton;
    procedure SetSnaps;
    procedure SetMultiSelects(value: boolean);
    procedure SetBoxerColor;
    function GetSelectMode: boolean;
  public
    procedure ApplySettings(sender: TObject);
    property SelectMode: boolean read GetSelectMode;
  end;

var
  FrmMain: TFrmMain;

implementation

uses RptPref, ListSections, Alignment, RptEditAbout, SectionFactory;

{$R *.DFM}

function PixelPointToLTwips(canvas: TCanvas; pt: TPoint; aZoom: double): TPoint;
// convert a pixel point to LTwips
begin
  with canvas do
  begin
    SetLogicalTwips(handle, aZoom);
    DPtoLP(handle, pt, 1);
    result:= pt;
  end;
end;

procedure TFrmMain.FormShow(Sender: TObject);
var
  k   : integer;
begin
  Caption:= application.Title;
  ApplySettings(self);
  SetBoxerColor;
  cboZoom.Items.Clear;
  k:= 25;
  while k <= PaperView.MaxZoom*100 do
  begin
    cboZoom.Items.Add(Format('%d%%', [k]));
    Inc(k, 25);
  end;
  cboZoom.ItemIndex:= 3;  // 100%
end;

procedure TFrmMain.SetBoxerColor;
begin
  if SelectMode then
    boxer1.BoxColor:= clBlack
  else
    boxer1.BoxColor:= clRed;
end;

procedure TFrmMain.pViewMouseDown(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  SetMultiSelects(false);
end;

procedure TFrmMain.ExitClick(Sender: TObject);
begin
  Close;
end;

procedure TFrmMain.MnuPrefsClick(Sender: TObject);
begin
  frmPreferences.OnApply:= ApplySettings;
  if FrmPreferences.ShowModal = mrOK then
    ApplySettings(self);
end;

procedure TFrmMain.SetSnaps;
var
  k : integer;
begin
  for k:= 0 to ComponentCount - 1 do
  begin
    if components[k] is TBoxWin then
    begin
      with TBoxWin(components[k]) do
      begin
        Units:= rptPrefs.Units;
        SnapToGrid:= rptPrefs.SnapToGrid;
        GridSize:= rptPrefs.GridSize;
      end;
    end;
  end;
end;

procedure TFrmMain.ApplySettings(sender: TObject);
// apply all settings from RptPreferences object
var
  k   : integer;
begin
  pView.GridVisible:= rptPrefs.ShowGrid;
  pView.MarginVisible:= rptPrefs.ShowMargins;
  pView.GridSize:= rptPrefs.GridSize;
  pView.PaperWidth:= rptPrefs.PaperWidth;
  pView.PaperHeight:= rptPrefs.PaperHeight;
  SetSnaps;
  BtnShowMargins.Down:= rptPrefs.ShowMargins;
  BtnShowGrid.Down:= rptPrefs.ShowGrid;
  BtnSnap.Down:= rptPrefs.SnapToGrid;
  ShowHint:= rptPrefs.ShowHints;
end;

procedure TFrmMain.Lock1Click(Sender: TObject);
begin
  if popupMenu1.PopupComponent is TBoxWin then
  begin
    with TBoxWin(popupMenu1.PopupComponent) do
      Locked:= not Locked;
  end;
end;

procedure TFrmMain.PopupMenu1Popup(Sender: TObject);
begin
  lock1.Checked:= TBoxWin(popupMenu1.PopupComponent).Locked;
end;

procedure TFrmMain.ToolButton5Click(Sender: TObject);
begin
beep;
end;

procedure TFrmMain.BtnShowMarginsClick(Sender: TObject);
begin
  rptPrefs.ShowMargins:= BtnShowMargins.Down;
  pView.MarginVisible:= rptPrefs.ShowMargins;
end;

procedure TFrmMain.BtnShowGridClick(Sender: TObject);
begin
  rptPrefs.ShowGrid:= BtnShowGrid.Down;
  pView.GridVisible:= rptPrefs.ShowGrid;
end;

procedure TFrmMain.BtnSnapClick(Sender: TObject);
begin
  rptPrefs.SnapToGrid:= BtnSnap.Down;
  SetSnaps;
end;

procedure TFrmMain.Boxer1BoxSizing(Sender: TObject; aRect: TRect);
var
  pt      : TPoint;
  x,y     : string;
  x1, y1  : string;
  k       : integer;
  cRect   : TRect;
  fmt     : string;
begin
  if not pView.Focused then
  begin
    pView.SetFocus;
    SetMultiSelects(false);
  end;
  if SelectMode then    // select mode on, select all rectangles in bounding rect
  begin
    for k:= 0 to pView.ControlCount - 1 do
    begin
      if pView.Controls[k] is TBoxWin then
      begin
        with TBoxWin(pView.controls[k]) do
        begin
          cRect:= Rect(left, top, left+width, top+height);
          if IntersectRect(cRect, cRect, aRect) then
            Selected:= true
          else
            Selected:= false;
        end;
      end;
    end;
  end
  else
  begin
    pt:= Point(aRect.left, aRect.top);
    pt:= pView.DevicePointToLTwips(pt);

    x:= FormatFloat(fmt, ConvertUnits(pt.x, rptPrefs.Units, fmt));
//    x:= FormatFloat('0.000', LTwipsToUnits(pt.x, rptPrefs.Units));
    y:= FormatFloat(fmt, ConvertUnits(pt.y, rptPrefs.Units, fmt));

    pt:= Point(aRect.right, aRect.bottom);
    pt:= pView.DevicePointToLTwips(pt);
    x1:= FormatFloat(fmt, ConvertUnits(pt.x, rptPrefs.Units, fmt));
    y1:= FormatFloat(fmt, ConvertUnits(pt.y, rptPrefs.Units, fmt));

    caption:= Format('Left/Top (%s,%s %s) x Right/Bottom (%s,%s %s)', [x, y, UnitString[rptPrefs.Units],
                      x1, y1, UnitString[rptPrefs.Units]]);
  end;
end;

procedure TFrmMain.Boxer1BoxCreated(Sender: TObject; aRect: TRect);
// User has created a box, now need to create a boxwin of the same size as
// the user defined box boundaries
var
  bWin  : TBoxWin;
  pt    : TPoint;
  sect  : TReportSection;
begin
  Caption:= application.Title;
  if not SelectMode then    // select mode on, select all rectangles in bounding rect
  begin
    with TBoxWin.Create(self) do
    begin
      zoom:= pView.zoom;
      left:= aRect.left - pView.HorzScrollBar.Position;
      top:= aRect.top - pView.VertScrollBar.Position;
      width:= aRect.Right - aRect.Left;
      height:= arect.Bottom - aRect.top;

      ParentOrigX:= pView.EdgeX;
      ParentOrigY:= pView.EdgeY;

      Units:= rptPrefs.Units;
      caption:= 'Test Section';
      parent:= pView;
      GridSize:= pView.GridSize;
      SnapToGrid:= rptPrefs.SnapToGrid;
      Ctl3d:= false;
      PopupMenu:= PopupMenu1;
      focusBoxColor:= clRed;
      SetFocus;

      // should get the section type from the active tool button
      sect:= TReportSection.Create;
      sect.left:= BoxLeft;
      sect.top:= BoxTop;
      sect.Width:= BoxWidth;

      // set the tag property to a pointer to the section editor
      tag:= longint(sect);
    end;
  end;
end;

procedure TFrmMain.SetMultiSelects(value: boolean);
// set the select state of all boxwins on the paper view
var
  k   : integer;
begin
  for k:= 0 to pView.ControlCount - 1 do
  begin
    if pView.Controls[k] is TBoxWin then
      TBoxWin(pView.Controls[k]).Selected:= value;
  end;
end;

procedure TFrmMain.MnuSelectAllClick(Sender: TObject);
begin
  SetMultiSelects(true);
end;

procedure TFrmMain.Boxer1BoxAborted(Sender: TObject);
begin
  Caption:= application.Title;
  pView.SetFocus;
end;

procedure TFrmMain.mnuLockClick(Sender: TObject);
var
  doLock  : boolean;
  k       : integer;
begin
  if (sender = mnuLock) or (sender = mnuUnlock) then
  begin
    doLock:= sender = mnuLock;
    for k:= 0 to pView.ControlCount - 1 do
    begin
      if pView.Controls[k] is TBoxWin then
        with TBoxWin(pView.Controls[k]) do
        begin
          if Selected or Focused then
            Locked:= doLock;
application.processmessages;            
        end;
    end;
  end;
end;

procedure TFrmMain.MnuDeleteClick(Sender: TObject);
var
  doLock  : boolean;
  k       : integer;
  sect    : TReportSection;
begin
  k:= 0;
  while k < pView.ControlCount do
  begin
    if pView.Controls[k] is TBoxWin then
    begin
      with TBoxWin(pView.Controls[k]) do
      begin
        if Selected or Focused then
        begin
          sect:= TReportSection(tag);
          sect.Free;
          Free;
          k:= 0;
        end
        else
          Inc(k);
      end;
    end
    else
      Inc(k);
  end;
end;


procedure TFrmMain.MnuDeletePopClick(Sender: TObject);
begin
  if screen.ActiveControl is TBoxWin then
    screen.ActiveControl.Free;
end;

procedure TFrmMain.BtnListSectionsClick(Sender: TObject);
begin
  ListReportSections;
end;

procedure TFrmMain.MnuListAllClick(Sender: TObject);
begin
  BtnListSections.Click;
end;

procedure TFrmMain.Zoom1Click(Sender: TObject);
begin
  cboZoom.SetFocus;
  cboZoom.DroppedDown:= true;
end;

procedure TFrmMain.cboZoomChange(Sender: TObject);
begin
  //

end;

procedure TFrmMain.cboZoomClick(Sender: TObject);
var
  num : double;
  k   : integer;
begin
  num:= ((cboZoom.ItemIndex+1) * 25.0) / 100.0;
  pView.Zoom:= num;
  for k:= 0 to pView.ControlCount - 1 do
  begin
    if pView.Controls[k] is TBoxWin then
      TBoxWin(pView.Controls[k]).Zoom:= num;
  end;
end;

procedure TFrmMain.cboZoomKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if key = VK_RETURN then
  begin
    pView.SetFocus;
    key:= 0;
    barStatus.Panels[2].text:= 'enter';
  end;
end;

procedure TFrmMain.MnuAlignClick(Sender: TObject);
begin
  if MnuAlign.Checked then
    BarAlign.Visible:= false
  else
  begin
    BarAlign.visible:= true;
  end;
  MnuAlign.Checked:= barAlign.Visible;
end;


procedure TFrmMain.BtnAlignLeftClick(Sender: TObject);
var
  bw    : TBoxWin;
  k     : integer;
begin
  if activeControl is TBoxWin then
  begin
    bw:= TBoxWin(ActiveControl);
    if bw.MultiSelected then
    begin
      for k:= 0 to pView.ControlCount - 1 do
      begin
        if pView.Controls[k] is TBoxWin then
          if (pView.Controls[k] <> bw) and (TBoxWin(pView.Controls[k]).Selected) then
            pView.Controls[k].Left:= bw.Left;
      end;
    end
    else
      bw.BoxLeft:= pView.MarginLeft;
  end;
end;

procedure TFrmMain.BtnAlignTopClick(Sender: TObject);
var
  bw    : TBoxWin;
  k     : integer;
begin
  if activeControl is TBoxWin then
  begin
    bw:= TBoxWin(ActiveControl);
    if bw.MultiSelected then
    begin
      for k:= 0 to pView.ControlCount - 1 do
      begin
        if pView.Controls[k] is TBoxWin then
          if (pView.Controls[k] <> bw) and (TBoxWin(pView.Controls[k]).Selected) then
            pView.Controls[k].Top:= bw.Top;
      end;
    end
    else
      bw.BoxTop:= pView.MarginTop;
  end;
end;

procedure TFrmMain.ToolButton11Click(Sender: TObject);
var
  bw    : TBoxWin;
  k     : integer;
begin
  if activeControl is TBoxWin then
  begin
    bw:= TBoxWin(ActiveControl);
    if bw.MultiSelected then
    begin
      for k:= 0 to pView.ControlCount - 1 do
      begin
        if pView.Controls[k] is TBoxWin then
          if (pView.Controls[k] <> bw) and (TBoxWin(pView.Controls[k]).Selected) then
            pView.Controls[k].Top:= (bw.Top + bw.Height) - pView.Controls[k].Height;
      end;
    end
    else
      bw.BoxTop:= pView.PaperHeight - pView.MarginBottom - bw.BoxHeight;
  end;
end;

procedure TFrmMain.BtnAlignRightClick(Sender: TObject);
var
  bw    : TBoxWin;
  k     : integer;
begin
  if activeControl is TBoxWin then
  begin
    bw:= TBoxWin(ActiveControl);
    if bw.MultiSelected then
    begin
      for k:= 0 to pView.ControlCount - 1 do
      begin
        if pView.Controls[k] is TBoxWin then
          if (pView.Controls[k] <> bw) and (TBoxWin(pView.Controls[k]).Selected) then
            pView.Controls[k].Left:= (bw.Left + bw.Width) - pView.Controls[k].Width;
      end;
    end
    else
      bw.BoxLeft:= pView.PaperWidth - pView.MarginRight - bw.BoxWidth;
  end;
end;

procedure TFrmMain.BtnAlignHWinCenterClick(Sender: TObject);
var
  bw    : TBoxWin;
  bwTmp : TBoxWin;
  k     : integer;
  minX  : integer;
  maxX   : integer;
  x     : integer;
  dx    : integer;
begin
  if activeControl is TBoxWin then
  begin
    bw:= TBoxWin(ActiveControl);
    if bw.MultiSelected then
    begin
      // first need to find the width and left of the imaginary box (bounding
      // rectangle) surrounding the selected boxes
      minX:= bw.BoxLeft;
      maxX:= minX + bw.BoxWidth;
      for k:= 0 to pView.ControlCount - 1 do
      begin
        if pView.Controls[k] is TBoxWin then
        begin
          bwTmp:= TBoxWin(pView.Controls[k]);
          if bwTmp.BoxLeft < minX then
            minX:= bwTmp.BoxLeft;
          if (bwTmp.BoxLeft + bwTmp.BoxWidth) > maxX then
            maxX:= bwTmp.BoxLeft + bwTmp.Boxwidth;
        end;
      end;
      // at this point minX is the left most point in bounding rectangle
      // maxX is the rightmost point in bounding rect

      // Now need to calculate the X value (left edge) that will center the
      // bounding rect on the page
      x:= (pView.PaperWidth div 2) - ((maxX - minX) div 2);

      // now, using this X position we can calculate a delta that is the amount
      // that all boxes will need to shift in order to center them to the page
      dx:= x - minX;

      // now, using the delta, apply it to all marked boxes.
      for k:= 0 to pView.ControlCount - 1 do
      begin
        if pView.Controls[k] is TBoxWin then
          if (TBoxWin(pView.Controls[k]).Selected) then
            with TBoxWin(pView.Controls[k]) do
              BoxLeft:= BoxLeft + dx;
      end;
    end
    else
      bw.BoxLeft:= (pView.PaperWidth div 2) - (bw.BoxWidth div 2);
  end;
end;

procedure TFrmMain.BtnAlignHCenterClick(Sender: TObject);
var
  bw    : TBoxWin;
  k     : integer;
  x     : integer;
begin
  if activeControl is TBoxWin then
  begin
    bw:= TBoxWin(ActiveControl);
    if bw.MultiSelected then
    begin
      x:= bw.Left + (bw.Width div 2); // center point to center all others to
      for k:= 0 to pView.ControlCount - 1 do
      begin
        if pView.Controls[k] is TBoxWin then
          if (pView.Controls[k] <> bw) and (TBoxWin(pView.Controls[k]).Selected) then
            pView.Controls[k].Left:= x - (pView.Controls[k].Width div 2);
      end;
    end;
  end;
end;

procedure TFrmMain.BtnAlignVCenterClick(Sender: TObject);
var
  bw    : TBoxWin;
  k     : integer;
  y     : integer;
begin
  if activeControl is TBoxWin then
  begin
    bw:= TBoxWin(ActiveControl);
    if bw.MultiSelected then
    begin
      y:= bw.Top + (bw.Height div 2); // center point to center all others to
      for k:= 0 to pView.ControlCount - 1 do
      begin
        if pView.Controls[k] is TBoxWin then
          if (pView.Controls[k] <> bw) and (TBoxWin(pView.Controls[k]).Selected) then
            pView.Controls[k].Top:= y - (pView.Controls[k].Height div 2);
      end;
    end;
  end;
end;

procedure TFrmMain.BtnAlignVWinCenterClick(Sender: TObject);
var
  bw    : TBoxWin;
  bwTmp : TBoxWin;
  k     : integer;
  minY  : integer;
  maxY   : integer;
  Y     : integer;
  dy    : integer;
begin
  if activeControl is TBoxWin then
  begin
    bw:= TBoxWin(ActiveControl);
    if bw.MultiSelected then
    begin
      // first need to find the height and top of the imaginary box (bounding
      // rectangle) surrounding the selected boxes
      minY:= bw.BoxTop;
      maxY:= minY + bw.BoxHeight;
      for k:= 0 to pView.ControlCount - 1 do
      begin
        if pView.Controls[k] is TBoxWin then
        begin
          bwTmp:= TBoxWin(pView.Controls[k]);
          if bwTmp.BoxTop < minY then
            minY:= bwTmp.BoxTop;
          if (bwTmp.BoxTop + bwTmp.BoxHeight) > maxY then
            maxY:= bwTmp.BoxTop + bwTmp.BoxHeight;
        end;
      end;
      // at this point minY is the top most point in bounding rectangle
      // maxY is the bottommost point in bounding rect

      // Now need to calculate the Y value (top edge) that will center the
      // bounding rect on the page
      y:= (pView.PaperHeight div 2) - ((maxY - minY) div 2);

      // now, using this Y position we can calculate a delta that is the amount
      // that all boxes will need to shift in order to center them to the page
      dy:= y - minY;

      // now, using the delta, apply it to all marked boxes.
      for k:= 0 to pView.ControlCount - 1 do
      begin
        if pView.Controls[k] is TBoxWin then
          if (TBoxWin(pView.Controls[k]).Selected) then
            with TBoxWin(pView.Controls[k]) do
              BoxTop:= BoxTop + dy;
      end;
    end
    else
      bw.BoxTop:= (pView.PaperHeight div 2) - (bw.BoxHeight div 2);
  end;
end;

function TFrmMain.GetSelectMode: boolean;
begin
  result:= btnSelect.Down;
end;

procedure TFrmMain.BtnSelectClick(Sender: TObject);
begin
  SetBoxerColor;
end;

procedure TFrmMain.About1Click(Sender: TObject);
begin
  FrmAboutRptEdit.ShowModal;
end;

procedure TFrmMain.pViewZoomChange(Sender: TObject);
begin
  cboZoom.ItemIndex:= (Trunc(pView.Zoom * 100) div 25) - 1;

end;

procedure TFrmMain.ToolButton9Click(Sender: TObject);
var
  rs    : TReportSection;
begin
  rs:= TReportSection.Create;
  rs.OffsetX:= pView.EdgeX;
  rs.OffsetY:= pView.EdgeY;
  SetLogicalTwips(pView.canvas.handle, pView.Zoom);
  rs.Draw(pView.canvas);

  rs.Free;
end;

procedure TFrmMain.MnuEditSectionClick(Sender: TObject);
var
  sect    : TReportSection;
begin
  with PopupMenu1.PopupComponent as TBoxWin do
  begin
    if tag <> 0 then
    begin
      sect:= TReportSection(tag);
      sect.Edit;
    end;
  end;
end;

END.
