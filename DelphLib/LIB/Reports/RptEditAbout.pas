unit RptEditAbout;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls;

type
  TFrmAboutRptEdit = class(TForm)
    Memo1: TMemo;
    Button1: TButton;
    Label1: TLabel;
    Label2: TLabel;
    procedure Button1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FrmAboutRptEdit: TFrmAboutRptEdit;

implementation

{$R *.DFM}

procedure TFrmAboutRptEdit.Button1Click(Sender: TObject);
begin
  Close;
end;

end.
