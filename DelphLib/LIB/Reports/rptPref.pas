unit rptPref;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  applyForm, StdCtrls, ExtCtrls, ComCtrls, ExtPage, TwipLib, BoxWin, Spin,
  Grids, Printers;

type
  TRptPrefs = class
  private
    fUnits      : TUnitTypes;
    fShowGrid   : boolean;
    fSnapToGrid : boolean;
    fShowMarg   : boolean;
    fGridSize   : TLTwips;
    fPaperIdx   : integer;
    fPaperWidth : TLTwips;
    fPaperHeight: TLTwips;
    fOrientation: TPrinterOrientation;
    fShowHints  : boolean;
  public
    constructor Create;
    property ShowGrid: boolean read fShowGrid write fShowGrid;
    property Units: TUnitTypes read fUnits write fUnits;
    property SnapToGrid: boolean read fSnapToGrid write fSnapToGrid;
    property ShowMargins: boolean read fShowMarg write fShowMarg;
    property GridSize: TLTwips read fGridSize write fGridSize;
    property Orientation: TPrinterOrientation read fOrientation write fOrientation;
    property PaperIndex: integer read fPaperIdx write fPaperIdx;
    property PaperHeight: TLTwips read fPaperHeight write fPaperHeight;
    property PaperWidth: TLTwips read fPaperWidth write fPaperWidth;
    property ShowHints: boolean read fShowHints write fShowHints;
  end;

  TFrmPreferences = class(TFrmApply)
    BtnCancel: TButton;
    BtnOK: TButton;
    ExtPageControl2: TExtPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    grdPapers: TStringGrid;
    ChkCustSize: TCheckBox;
    lblWidth: TLabel;
    lblHeight: TLabel;
    edWidth: TEdit;
    edHeight: TEdit;
    Label1: TLabel;
    lblUnits: TLabel;
    edtGridSize: TEdit;
    chkShowGrid: TCheckBox;
    chkShowMarg: TCheckBox;
    chkSnap: TCheckBox;
    rdoUnits: TRadioGroup;
    rdoPortrait: TRadioButton;
    rdoLandScape: TRadioButton;
    ChkFlyover: TCheckBox;
    grpMargins: TGroupBox;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    edMargLeft: TEdit;
    edMargRight: TEdit;
    edMargTop: TEdit;
    edMargBottom: TEdit;
    procedure rdoUnitsClick(Sender: TObject);
    procedure BtnApplyClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure BtnOKClick(Sender: TObject);
    procedure edtGridSizeExit(Sender: TObject);
    procedure CheckSettings(sender: TObject);
    procedure ChkCustSizeClick(Sender: TObject);
    procedure grdPapersClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure edWidthChange(Sender: TObject);
    procedure edWidthExit(Sender: TObject);
    procedure rdoPortraitClick(Sender: TObject);
  private
    fDisplayedUnits   : TUnitTypes;
    fCustIndex        : integer;
    fMargTitle        : string;
    procedure SetValues;
    procedure UpdatePaperSizes;
    procedure UpdateCustSizes;
    procedure NumEditExit(anEdit: TEdit; dflt: TLTwips);
  public
    { Public declarations }
  end;

var
  FrmPreferences  : TFrmPreferences;
  rptPrefs        : TRptPrefs;

IMPLEMENTATION

{$R *.DFM}

uses
  PrnUtils;

{--[ TRptPrefs ]------------------------------------------------------}

constructor TRptPrefs.Create;
begin
  fUnits:= uInches;
  fShowGrid:= true;
  fSnapToGrid:= false;
  fShowMarg:= true;
  fGridSize:= LT50;
  fPaperIdx:= DMPAPER_LETTER;
  fPaperWidth:= PaperSizes[fPaperIdx].x;
  fPaperHeight:= PaperSizes[fPaperIdx].y;
  fOrientation:= poPortrait;
  fShowHints:= true;
end;

{--[ TFrmPreferences ]------------------------------------------------}

procedure TFrmPreferences.rdoUnitsClick(Sender: TObject);
var
  fmt   : string;
  num   : double;
  lts   : TLTwips;
  w,h   : double;
  ltw, lth: TLTwips;
begin
  num:= StrToFloat(edtGridSize.text);
  lts:= UnitsToLTwips(num, fDisplayedUnits);
  w:= StrToFloat(grdPapers.cells[1, fCustIndex]);
  ltw:= UnitsToLTwips(w, fDisplayedUnits);
  h:= StrToFloat(grdPapers.cells[2, fCustIndex]);
  lth:= UnitsToLTwips(h, fDisplayedUnits);

  case rdoUnits.ItemIndex of
    1 : fDisplayedUnits:= uMM;
    2 : fDisplayedUnits:= uCM;
    3 : fDisplayedUnits:= uTwips;
  else
    fDisplayedUnits:= uInches;
  end;

  num:= ConvertUnits(lts, fDisplayedUnits, fmt);
  edtGridSize.text:= FormatFloat(fmt, num);
  lblUnits.caption:= UnitString[fDisplayedUnits];

  num:= ConvertUnits(ltw, fDisplayedUnits, fmt);
  grdPapers.cells[1, fCustIndex]:= FormatFloat(fmt, num);
  num:= ConvertUnits(lth, fDisplayedUnits, fmt);
  grdPapers.cells[2, fCustIndex]:= FormatFloat(fmt, num);

  UpdatePaperSizes;
  UpdateCustSizes;
  CheckSettings(self);
end;

procedure TFrmPreferences.SetValues;
// set the settings in the RptPrefs global object
var
  num : double;
begin
  with rptPrefs do
  begin
    case rdoUnits.ItemIndex of
      1 : Units:= uMM;
      2 : Units:= uCM;
      3 : Units:= uTwips;
    else
      Units:= uInches;
    end;
    ShowHints:= ChkFlyover.Checked;
    ShowGrid:= chkShowGrid.checked;
    SnapToGrid:= chkSnap.checked;
    ShowMargins:= chkShowMarg.checked;
    num:= StrToFloat(edtGridSize.text);
    GridSize:= UnitsToLTwips(num, rptPrefs.Units);
    if GridSize < TBoxWin.MinGridSize then
      GridSize:= TBoxWin.MinGridSize;

    num:= StrToFloat(edHeight.text);
    PaperHeight:= UnitsToLTwips(num, rptPrefs.Units);
    num:= StrToFloat(edWidth.text);
    PaperWidth:= UnitsToLTwips(num, rptPrefs.Units);
    PaperIndex:= StrToInt(grdPapers.cells[3, grdPapers.Row]);
    if rdoPortrait.Checked then
      Orientation:= poPortrait
    else
      Orientation:= poLandscape;
  end;
end;

procedure TFrmPreferences.BtnApplyClick(Sender: TObject);
begin
  SetValues;
  inherited;
end;

procedure TFrmPreferences.BtnOKClick(Sender: TObject);
begin
  inherited;
  SetValues;
end;

procedure TFrmPreferences.NumEditExit(anEdit: TEdit; dflt: TLTwips);
var
  num : double;
  fmt : string;
  tmp : integer; // used a place holder only so fmt can be aquired from convertunits
begin
  try
    num:= StrToFloat(anEdit.text);
  except
    num:= LTwipsToUnits(dflt, rptPrefs.Units);
  end;
  tmp:= 1440;
  ConvertUnits(tmp, fDisplayedUnits, fmt);
  anEdit.text:= FormatFloat(fmt, num);
end;

procedure TFrmPreferences.edtGridSizeExit(Sender: TObject);
var
  num : double;
  fmt : string;
  tmp : integer; // used a place holder only so fmt can be aquired from convertunits
begin
  try
    num:= StrToFloat(edtGridSize.text);
  except
    num:= LTwipsToUnits(rptPrefs.GridSize, rptPrefs.Units);
  end;
  tmp:= 1440;
  ConvertUnits(tmp, fDisplayedUnits, fmt);
  edtGridSize.text:= FormatFloat(fmt, num);
end;

procedure TFrmPreferences.FormShow(Sender: TObject);
var
  num   : double;
  k     : integer;
  tstr  : integer;
  fmt   : string;
begin
  ExtPageControl2.ActivePage:= Tabsheet1;
  with rptPrefs do
  begin
    lblUnits.caption:= UnitString[rptPrefs.Units];
    chkShowGrid.checked:= ShowGrid;
    chkSnap.checked:= SnapToGrid;
    chkShowMarg.checked:= ShowMargins;
    num:= ConvertUnits(rptPrefs.GridSize, rptPrefs.Units, fmt);
    edtGridSize.text:= FormatFloat(fmt, num);
    fDisplayedUnits:= rptPrefs.Units;
    case Units of
      uMM : rdoUnits.ItemIndex:= 1;
      uCM : rdoUnits.ItemIndex:= 2;
      uTwips: rdoUnits.ItemIndex:= 3;
      else
        rdoUnits.ItemIndex:= 0;
    end;
  end;

  chkFlyover.checked:= rptPrefs.ShowHints;
  rdoPortrait.Checked:= rptPrefs.Orientation = poPortrait;
  rdoLandscape.Checked:= rptPrefs.Orientation = poLandscape;

  with grdPapers do
  begin
    cells[0, 0]:= 'Paper';
    cells[1, 0]:= 'Width';
    cells[2, 0]:= 'Height';
    // +3 to acount for header and custom index rows
    RowCount:= Last_DMPaper - First_DMPaper + 3;
    for k:= First_DMPaper to Last_DMPaper do
    begin
      row:= k - First_DMPaper + 1;
      grdPapers.cells[0, row]:= PaperName(k);
      grdPapers.cells[3, row]:= IntToStr(k);
    end;

    grdPapers.cells[0, RowCount-1]:= 'Custom Size';
    fCustIndex:= RowCount - 1;
    row:= 1;
  end;

  num:= ConvertUnits(rptPrefs.PaperWidth, rptPrefs.Units, fmt);
  edWidth.text:= FormatFloat(fmt, num);
  num:= ConvertUnits(rptPrefs.PaperHeight, rptPrefs.Units, fmt);
  edHeight.text:= FormatFloat(fmt, num);
  grdPapers.cells[1, fCustIndex]:= edWidth.text;
  grdPapers.cells[2, fCustIndex]:= edHeight.text;
  grdPapers.cells[3, fCustIndex]:= '-1';

  UpdatePaperSizes;

  if rptPrefs.PaperIndex <> -1 then
    grdPapers.Row:= 1 + (rptPrefs.PaperIndex - First_DMPaper)
  else
    grdPapers.Row:= fCustIndex;

  chkCustSize.Checked:= (grdPapers.Row = fCustIndex);
  ChkCustSizeClick(self);
  UpdateCustSizes;

  inherited;
end;

procedure TFrmPreferences.UpdatePaperSizes;
// fill grid with paper sizes
var
  fmt   : string;
  ustr  : string;
  lt    : integer;
  num   : double;
  row   : integer;
  k     : integer;
  col   : integer;
begin
  for k:= First_DMPaper to Last_DMPaper do
  begin
    row:= k - First_DMPaper + 1;
    lt:= paperSizes[k].x;
    num:= ConvertUnits(lt, fDisplayedUnits, fmt);
    if rdoPortrait.Checked then
      col:= 1
    else
      col:= 2;
    grdPapers.cells[col, row]:= FormatFloat(fmt, num);
    lt:= paperSizes[k].y;
    num:= ConvertUnits(lt, fDisplayedUnits, fmt);
    if rdoPortrait.Checked then
      col:= 2
    else
      col:= 1;
    grdPapers.cells[col, row]:= FormatFloat(fmt, num);
  end;
end;

procedure TFrmPreferences.CheckSettings(sender: TObject);
begin
  Modified:= true;
end;

procedure TFrmPreferences.ChkCustSizeClick(Sender: TObject);
begin
  inherited;
  if chkCustSize.Checked then
    grdPapers.Row:= fCustIndex;
  edWidth.enabled:= chkCustSize.Checked;
  edHeight.enabled:= chkCustSize.Checked;
  lblHeight.enabled:= chkCustSize.Checked;
  lblWidth.enabled:= chkCustSize.Checked;
  UpdateCustSizes;
  CheckSettings(self);
end;

procedure TFrmPreferences.UpdateCustSizes;
begin
  edWidth.text:= grdPapers.cells[1, grdPapers.Row];
  edHeight.text:= grdPapers.cells[2, grdPapers.Row];
end;

procedure TFrmPreferences.grdPapersClick(Sender: TObject);
begin
  inherited;
  chkCustSize.checked:= grdPapers.Row = fCustIndex;
  if activeControl = grdPapers then
  begin
    UpdatePaperSizes;
    UpdateCustSizes;
    CheckSettings(self);    
  end;
end;

procedure TFrmPreferences.FormCreate(Sender: TObject);
begin
  inherited;
  grdPapers.ColWidths[3]:= -1;
end;

procedure TFrmPreferences.edWidthChange(Sender: TObject);
var
  num : double;
begin
  inherited;
  if (ActiveControl = edWidth) or (ActiveControl = edHeight) then
  begin
    CheckSettings(self);
    try
      StrToFloat(edWidth.text);
      grdPapers.cells[1, fCustIndex]:= edWidth.text;
    except

    end;
    try
      StrToFloat(edHeight.text);
      grdPapers.cells[2, fCustIndex]:= edHeight.text;
    except
    end;
  end
end;

procedure TFrmPreferences.edWidthExit(Sender: TObject);
begin
  inherited;
  NumEditExit(sender as TEdit, 1440);
  grdPapers.cells[1, fCustIndex]:= edWidth.text;
  grdPapers.cells[2, fCustIndex]:= edHeight.text;
end;

procedure TFrmPreferences.rdoPortraitClick(Sender: TObject);
var
  k     : integer;
  tstr  : string;
begin
  inherited;
  if (rdoPortrait = ActiveControl) or (rdoLandscape = ActiveControl) then
  begin
    for k:= 1 to grdPapers.RowCount - 1 do
    begin
      tstr:= grdPapers.Cells[1, k];
      grdPapers.cells[1, k]:= grdPapers.Cells[2, k];
      grdPapers.Cells[2, k]:= tstr;
    end;
  end;
  UpdateCustsizes;
  CheckSettings(self);
end;

INITIALIZATION
  rptPrefs:= TRptPrefs.Create;
FINALIZATION
  rptPrefs.Free;
END.