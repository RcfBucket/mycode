unit ListSections;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ComCtrls, StdCtrls, ExtCtrls;

type
  TFrmListSections = class(TForm)
    ListView1: TListView;
    StatusBar1: TStatusBar;
    Panel1: TPanel;
    Panel2: TPanel;
    BtnClose: TButton;
    BtnToFront: TButton;
    BtnEdit: TButton;
    procedure BtnEditClick(Sender: TObject);
    procedure ListView1Change(Sender: TObject; Item: TListItem;
      Change: TItemChange);
    procedure FormShow(Sender: TObject);
  private
    procedure EnableButtons;
  public
    { Public declarations }
  end;

procedure ListReportSections;

IMPLEMENTATION

{$R *.DFM}

procedure ListReportSections;
begin
  with TFrmListSections.Create(application) do
  begin
    ShowModal;
    Free;
  end;
end;

procedure TFrmListSections.BtnEditClick(Sender: TObject);
begin
  ShowMessage('This would bring up the Editor for the highlighted section');
end;

procedure TFrmListSections.EnableButtons;
begin
  BtnEdit.Enabled:= ListView1.ItemFocused <> nil;
  BtnToFront.Enabled:= ListView1.ItemFocused <> nil;
end;

procedure TFrmListSections.ListView1Change(Sender: TObject;
  Item: TListItem; Change: TItemChange);
begin
  EnableButtons;
end;

procedure TFrmListSections.FormShow(Sender: TObject);
begin
  EnableButtons;
end;

END.
