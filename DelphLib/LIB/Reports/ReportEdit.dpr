program ReportEdit;

uses
  Forms,
  RptMain in 'RptMain.pas' {FrmMain},
  applyForm in 'applyForm.pas' {FrmApply},
  rptPref in 'rptPref.pas' {FrmPreferences},
  ListSections in 'ListSections.pas' {FrmListSections},
  Alignment in 'Alignment.pas' {FrmAlignment},
  RptEditAbout in 'RptEditAbout.pas' {FrmAboutRptEdit},
  SectionFactory in 'SectionFactory.pas',
  SectBaseEd in 'SectBaseEd.pas' {FrmSectBaseEd};

{$R *.RES}

begin
  Application.Initialize;
  Application.Title := 'DMS Report Editor';
  Application.CreateForm(TFrmMain, FrmMain);
  Application.CreateForm(TFrmPreferences, FrmPreferences);
  Application.CreateForm(TFrmAlignment, FrmAlignment);
  Application.CreateForm(TFrmAboutRptEdit, FrmAboutRptEdit);
  Application.CreateForm(TFrmSectBaseEd, FrmSectBaseEd);
  Application.Run;
end.
