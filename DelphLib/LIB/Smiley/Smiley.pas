{-------------------------------------------------}
{                                                 }
{    The TSmiley component is Copyright � 1996    }
{         by Nick Hodges All Rights Reserved      }
{                                                 }
{-------------------------------------------------}

unit Smiley;

{$R SMILEY.RES}

INTERFACE

uses
  WinProcs, Classes, Graphics, Controls, StdCtrls, Messages, ExtCtrls;

procedure Register;

type
  TMood = (smHappy, smSad, smShades, smTongue, smIndifferent, smOoh,
           smFrazzled, smOutCold, smWink);

const
  MaxHeight = 26;
  MaxWidth = 26;

type
  TMoodChange = procedure(Sender: TObject; NewMood: TMood) of object;

type
  TSmiley = class(TGraphicControl)
  private
    Face           : TBitmap;
    FMood,
    OldMood        : TMood;
    FOnMoodChange  : TMoodChange;
    Clicking       : Boolean;
    procedure SetBitmap;
    procedure SetMood(NewMood: TMood);
    procedure WMPaint (var Message: TWMPaint); message WM_PAINT;

  public
    constructor Create(AOwner: TComponent); override;
    procedure Toggle;
    procedure MouseDown(Button: TMouseButton; Shift: TShiftState; X, Y: Integer); override;
    procedure MouseUp(Button: TMouseButton; Shift: TShiftState; X, Y: Integer); override;
    destructor Destroy; override;
  published
    property Mood: TMood read FMood write SetMood;
    property OnMoodChange: TMoodChange read FOnMoodChange write FOnMoodChange;
  end;

IMPLEMENTATION

uses
  Choose, DsgnIntf, TypInfo;

{-------------------------------------------------
                        TSmiley
--------------------------------------------------}

constructor TSmiley.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
  FMood := smHappy;
  Face := TBitmap.Create;
  {Old-fashioned API call}
  Face.Handle := LoadBitmap(hInstance,
                            PChar(GetEnumName(TypeInfo(TMood), Ord(smHappy))));
  Self.Height := MaxHeight;
  Self.Width := MaxWidth;
  SetBitmap;
  OldMood := smHappy;
end; {Create}

destructor TSmiley.Destroy;
begin
  Face.Free; {Use Free rather than Destroy, as Free checks for a nil pointer first}
  inherited Destroy;
end; {Destroy}


procedure TSmiley.Toggle;
begin
  if fMood = smWink then
    fMood := smHappy
  else
    Inc(fMood);  {Don't allow fMood to overflow}
  SetBitmap;
end; {Toggle}

procedure TSmiley.SetBitmap;
begin
  Face.Handle := LoadBitmap(hInstance, PChar(GetEnumName(TypeInfo(TMood), Ord(fMood))));
  Invalidate;
  if Assigned(FOnMoodChange) and not Clicking then
    FOnMoodChange(Self, fMood);
end; {SetBitmap}

procedure TSmiley.SetMood(NewMood: TMood);
begin
  if NewMood <> FMood then
  begin
    FMood := NewMood;
    SetBitmap;
  end;
end; {SetMood}

{This method will respond to a mouse push on the Smiley by storing the
old face for later use and giving the "Wink" face.  Smileys don't like to get
clicked on!}
procedure TSmiley.MouseDown(Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  inherited MouseDown(Button, Shift, X, Y);
  OldMood := Mood;
  Clicking := True;
  SetMood(smWink);
end; {MouseDown}

{This method restores the old face when the mouse comes back up}
procedure TSmiley.MouseUp(Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  inherited MouseUp(Button, Shift, X, Y);
  SetMood(OldMood);
  Clicking := False;
end; {MouseUp}

procedure TSmiley.WMPaint(var Message: TWMPaint);
begin
  inherited;
  Canvas.Draw(0,0, Face);
end; {WMPaint}

{------------------------------------}

procedure Register;
begin
  RegisterComponents('Custom', [TSmiley]);
  RegisterPropertyEditor( TypeInfo( TMood ), TSmiley, 'Mood', TMoodProperty );
end; {Register}

end.

