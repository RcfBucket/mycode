unit BitLib;
// Implements a bit array of unlimited size.
//
//
// rcf 12-8-96
// based in part from the TBits class in the VCL

INTERFACE

uses
  SysUtils, Classes;

type
  EBitsError = class(Exception);

  TBitArray = class
  private
    fSize   : integer;
    fBits   : pointer;

    procedure Error;
    procedure SetSize(value: integer);
    procedure SetBit(index: integer; value: boolean);
    function GetBit(index: integer): boolean;
  public
    destructor Destroy; override;
    function OpenBit: integer;
    procedure ShiftL(fromBit: integer);
    procedure ShiftR(fromBit: integer);
    procedure AndWith(source: TBitArray);
    procedure OrWith(source: TBitArray);
    procedure Invert;
    procedure Assign(source: TBitArray);
    procedure ClearAll;

    property Bits[idx: integer]: boolean read GetBit write SetBit; default;
    property Size: integer read fSize write SetSize;
  end;


IMPLEMENTATION

const
  BitsPerInt = SizeOf(integer) * 8;

type
  TBitEnum = 0..BitsPerInt - 1;
  TBitSet = set of TBitEnum;
  PArrayOfBits = ^TArrayOfBits;
  TArrayOfBits = array[0..4096] of TBitSet;

destructor TBitArray.Destroy;
begin
  SetSize(0);
  inherited Destroy;
end;

procedure TBitArray.Error;
begin
  raise EBitsError.Create('Bits index out of range');
end;

procedure TBitArray.SetSize(Value: integer);
// set the size of the bit array. value is the number of bits desired
var
  newMem    : pointer;
  newMemSize: integer;
  oldMemSize: integer;

  function Min(x, y: integer): integer;
  begin
    Result:= x;
    if X > Y then Result:= Y;
  end;

begin
  if value <> Size then
  begin
    if value < 0 then
      Error;
    newMemSize:= ((value + BitsPerInt - 1) div BitsPerInt) * SizeOf(integer);
    oldMemSize:= ((Size  + BitsPerInt - 1) div BitsPerInt) * SizeOf(integer);
    if NewMemSize <> OldMemSize then
    begin
      newMem:= nil;
      if NewMemSize <> 0 then
      begin
        GetMem(newMem, newMemSize);
        FillChar(newMem^, newMemSize, 0);
      end;
      if OldMemSize <> 0 then
      begin
        if NewMem <> nil then
          Move(fBits^, newMem^, Min(OldMemSize, NewMemSize));
        FreeMem(fBits, oldMemSize);
      end;
      FBits:= NewMem;
    end;
    fSize:= Value;
  end;
end;


procedure TBitArray.SetBit(index: integer; value: boolean); assembler;
asm
        CMP     index,[EAX].fSize  // is index withing range 0 <= index < fSize
        JAE     @@Size

@@1:    MOV     EAX,[EAX].fBits
        OR      Value,Value
        JZ      @@2
        BTS     [EAX],index
        RET

@@2:    BTR     [EAX],index
        RET

@@Size: CMP     index,0
        JL      TBitArray.Error
        PUSH    Self
        PUSH    index
        PUSH    ECX {Value}
        INC     index
        CALL    TBitArray.SetSize
        POP     ECX {Value}
        POP     index
        POP     Self
        JMP     @@1
end;

function TBitArray.GetBit(index: integer): boolean; assembler;
asm
        CMP     index,[EAX].fSize
        JAE     TBitArray.Error
        MOV     EAX,[EAX].fBits
        BT      [EAX],Index
        SBB     EAX,EAX
        AND     EAX,1
end;


function TBitArray.OpenBit: integer;
// return the index of the first open bit
var
  i: integer;
  b: TBitSet;
  j: TBitEnum;
  e: integer;
begin
  e:= (Size + BitsPerInt - 1) div BitsPerInt - 1;
  for i:= 0 to e do
  begin
    if PArrayOfBits(fBits)^[i] <> [0..BitsPerInt - 1] then
    begin
      b:= PArrayOfBits(fBits)^[i];
      for j:= Low(j) to High(j) do
      begin
        if not (j in b) then
        begin
          result:= i * BitsPerInt + j;
          if Result >= Size then
            Result:= Size;
          Exit;
        end;
      end;
    end;
  end;
  Result:= Size;
end;

procedure TBitArray.ShiftL(fromBit: integer);
// shift all bits left starting at the specified bit
var
  k   : integer;
begin
  if (fromBit >= size) or (fromBit < 0) then
    fromBit:= 0;
  for k:= size - 1 downto fromBit+1 do
  begin
    bits[k]:= bits[k-1];
  end;
  bits[fromBit]:= false;
end;

procedure TBitArray.ShiftR(fromBit: integer);
// Shift all bits right starting at the specified bit
var
  k   : integer;
begin
  if (fromBit >= size) or (fromBit < 0) then
    fromBit:= size - 1;
  for k:= 0 to fromBit - 1 do
  begin
    bits[k]:= bits[k + 1];
  end;
  bits[fromBit]:= false;
end;

procedure TBitArray.AndWith(source: TBitArray);
// 'and' the current array with the source
// the current and source arrays must be of the same size
var
  k     : integer;
  elems : integer;
  b     : TBitSet;
  b2    : TBitSet;
  res   : integer;
begin
  if source.size = Size then
  begin
    elems:= (Size + BitsPerInt - 1) div BitsPerInt - 1;
    for k:= 0 to elems do
    begin
      b:= PArrayOfBits(fBits)^[k];
      b2:= PArrayOfBits(source.fBits)^[k];
      res:= integer(b) and integer(b2);
      PArrayOfBits(fBits)^[k]:= TBitSet(res);
    end;
  end;
end;

procedure TBitArray.OrWith(source: TBitArray);
// 'and' the current array with the source
// the current and source arrays must be of the same size
var
  k     : integer;
  elems : integer;
  b     : TBitSet;
  b2    : TBitSet;
  res   : integer;
begin
  if source.size = Size then
  begin
    elems:= (Size + BitsPerInt - 1) div BitsPerInt - 1;
    for k:= 0 to elems do
    begin
      b:= PArrayOfBits(fBits)^[k];
      b2:= PArrayOfBits(source.fBits)^[k];
      res:= integer(b) or integer(b2);
      PArrayOfBits(fBits)^[k]:= TBitSet(res);
    end;
  end;
end;

procedure TBitArray.Invert;
// invert all bits in the array
var
  k     : integer;
  elems : integer;
  res   : integer;
begin
  elems:= (Size + BitsPerInt - 1) div BitsPerInt - 1;
  for k:= 0 to elems do
  begin
    res:= integer(PArrayOfBits(fBits)^[k]);
    res:= not res;
    PArrayOfBits(fBits)^[k]:= TBitSet(res);
  end;
end;

procedure TBitArray.Assign(source: TBitArray);
begin
  Size:= source.Size;
  OrWith(source);
end;

procedure TBitArray.ClearAll;
// clear all bits in the array
var
  k   : integer;
begin
  k:= Size;
  SetSize(0);
  SetSize(k);
end;

END.



