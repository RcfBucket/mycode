unit DragDrop;
{
	Copyright (c) 1996 by Freeman Ng (Orthanc Software) and
	Socratic Technologies.

	Functions for supporting file drag and drop between applications.

	GetDropWindow() can be called from within a mouse move handler
	to see if the window the mouse is moving over can accept a dropped
	file, so that you can change the cursor accordingly.  DropFiles()
	should be called within the mouse up handler to actually drop the
	files onto the target window.

	This code was derived from an article in _Microsoft Systems Journal_
	by Jeffrey Richter entitled "Drop Everything: How to Make Your
	Application Accept and Source Drag-and-Drop Files."  (I found the
	article on the Microsoft Developers Network CD, which did not give
	a reference to the MSJ issue the article came from.)

	Permission is granted to use this code freely as long as both the
	original article and my role in creating the Delphi version are
	credited.

	Freeman Ng
	Orthanc Software
	71162.1253@compuserve.com

	WARNING: DropFiles() is based on undocumented Windows API functionality.
	It could be broken by versions of Windows later than (or even *other*
	than, for all I know) 3.1 or 95 (the only versions of Windows I happen
	to have around to test with.)

	There is no code in this module for receiving a dropped file,
	because that is a fairly simple matter of using documented Win API
	functions.  Basically, you have to create a subclass of the type
	of component you want to have files dropped on, then grab the
	WM_DROPFILES message for that class.  For example, if you want
	a TScrollBox to receive dropped files, you would define the
	following class:

		type TDropFilesScrollBox = class(TScrollBox)
			procedure WMDropFiles(var Message: TMessage); message WM_DROPFILES;
		end;

	and create an instance of it where you want files to be dropped.
	Within the WMDropFiles handler, you'd use the DragQueryPoint() and
	DragQueryFile() API functions to figure out what files were dropped
	and where on the scroll box.
}

interface

uses
	Classes, WinTypes, Dialogs;

const
  ERR_BAD_HWND = -1;

function GetDropWindow : HWND;
function DropFiles(pathnameList : TStrings) : Boolean;

implementation

uses
	SysUtils, WinProcs, Messages, ShellAPI;

{This is the undocumented structure that gets passed to windows that
are being dropped on.  The pathnames of the files to be dropped are
appended to the end as ASCIIZ strings and fSize is set to the total
size of the structure including those pathnames.
}
type
  DropFileStruct = record
	  fSize 						: Word;
  	fMousePos 				:	TPoint;
	  fInNonClientArea 	: WordBool;
  end;

type
  DropFileStructPtr = ^DropFileStruct;

{Is the mouse presently positioned over a window that can be dropped on?
 Returns the handle if it is, and ERR_BAD_HWND if it isn't.
}
function GetDropWindow : HWND;
var
	mousePos 		: TPoint;
	destWindow 	: HWND;
begin
	{Figure out who we just dropped on	}
	GetCursorPos(mousePos);
	destWindow := WindowFromPoint(mousePos);

	if IsWindow(destWindow) and
		((GetWindowLong(destWindow, GWL_EXSTYLE) and WS_EX_ACCEPTFILES) <> 0) then
    result:= destWindow
	else
    result:= ERR_BAD_HWND;
end;

function GetLastErrorString: string;
// return a string representing the last windows system error
var
  tstr    : string;
  err     : integer;
begin
  err:= GetLastError;
  SetLength(tstr, 1024);
  if FormatMessage(FORMAT_MESSAGE_FROM_SYSTEM,
                nil,
                err,
                0,
                PChar(tstr),
                1024,
                nil) <> 0 then
  begin
    SetLength(tstr, StrLen(PChar(tstr)));
    result:= tstr;
  end
  else
    result:= 'System ID - ' + IntToStr(err);
end;

{Drop the specified files onto the window that the mouse is currently
positioned on, returning False if there is no window there or if
the window can't accept a file drop or if any other error occurs.
}
function DropFiles(pathnameList : TStrings) : Boolean;
var
	mousePos 			: TPoint;
	destWindow 		: HWND;
	pnameSize			: Integer;
	i, pos				: Integer;
	dropBlock			: HGLOBAL;
	dropStructPtr	: DropFileStructPtr;
	inNonClient		: Boolean;
	memPtr				: PChar;
	destRect			: TRect;
begin

	Result := False;		{various ways for this function to fail!}

	{Figure out who we're about to drop on	}
	GetCursorPos(mousePos);
	destWindow := WindowFromPoint(mousePos);

	{Adjust the mouse position to be relative to the destination window	}
	GetWindowRect(destWindow, destRect);
	mousePos.X := mousePos.X - destRect.Left;
	mousePos.Y := mousePos.Y - destRect.Top;

	if (not IsWindow(destWindow)) or
		((GetWindowLong(destWindow, GWL_EXSTYLE)
							and WS_EX_ACCEPTFILES) = 0) then Exit;

	{Are we dropping onto the target window's non-client area?	}
	inNonClient := (HTCLIENT <> SendMessage(
		destWindow, WM_NCHITTEST, 0, MAKELONG(mousePos.x, mousePos.y)));

	{Go through the pathname list, adding up how much space we'll need to
	store all the pathnames	}
	i := 0;
	pnameSize := 0;
	while i < pathnameList.Count do
	begin
		pnameSize := pnameSize + Length(pathnameList.Strings[i]) + 1;
		Inc(i);
	end;

	{Construct the buffer that will be passed to the dropped-on window.
	(GMEM_SHARE must be specified because the block will be passed to
	another application.)  This buffer will be freed by that other
	application.
	}
	dropBlock := GlobalAlloc(GMEM_SHARE or GMEM_MOVEABLE or GMEM_ZEROINIT,
												SizeOf(DropFileStruct) + pnameSize + 1);
	if dropBlock = 0 then Exit;

	{Lock the block and initialize the data members
	}
	dropStructPtr := DropFileStructPtr(GlobalLock(dropBlock));
  if dropStructPtr = nil then
  begin
    ShowMessage(GetLastErrorString);
    exit;
  end;
	dropStructPtr^.fSize := SizeOf(DropFileStruct);
	dropStructPtr^.fMousePos.X := mousePos.X;
	dropStructPtr^.fMousePos.Y := mousePos.Y;
	dropStructPtr^.fInNonClientArea := inNonClient;
	i := 0;
	pos := SizeOf(DropFileStruct);
	memPtr := PChar(dropStructPtr);
	while i < pathnameList.Count do
	begin
		StrPCopy(memPtr + pos, pathnameList.Strings[i]);
		pos := pos + Length(pathnameList.Strings[i]) + 1;
		Inc(i);
	end;
	StrPCopy(memPtr + pos, '');		{Add extra null-terminator to end list}
	GlobalUnlock(dropBlock);

	{Do it!
	}
	PostMessage(destWindow, WM_DROPFILES, dropBlock, Longint(0));

end;

end.

