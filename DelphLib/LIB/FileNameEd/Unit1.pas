unit Unit1;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, FileNameEd, DragDrop;

type
  TForm1 = class(TForm)
    Button1: TButton;
    Edit1: TEdit;
    FileNameEdit1: TFileNameEdit;
    Label1: TLabel;
    Button2: TButton;
    Label2: TLabel;
    procedure Button2Click(Sender: TObject);
    procedure FormMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure FormMouseMove(Sender: TObject; Shift: TShiftState; X,
      Y: Integer);
    procedure FormMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

{$R *.DFM}

procedure TForm1.Button2Click(Sender: TObject);
var
  lst   : TStringList;
begin
  lst:= TStringList.Create;
  lst.Add('FILE1.PAS');
  DropFiles(lst);
  lst.Free;
end;

procedure TForm1.FormMouseDown(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  screen.Cursor:= crDrag;
end;

procedure TForm1.FormMouseMove(Sender: TObject; Shift: TShiftState; X,
  Y: Integer);
var
  dWnd      : HWND;
	mousePos 	: TPoint;
begin
  if ssLeft in Shift then
  begin
  	GetCursorPos(mousePos);
	  if WindowFromPoint(mousePos) = handle then
      screen.Cursor:= crDrag
    else
    begin
      dWnd:= GetDropWindow;
      if (dWnd <> ERR_BAD_HWND) then
        screen.Cursor:= crDrag
      else
        screen.Cursor:= crNoDrop;
    end;
  end;
end;

procedure TForm1.FormMouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  screen.Cursor:= crDefault;
end;

end.
