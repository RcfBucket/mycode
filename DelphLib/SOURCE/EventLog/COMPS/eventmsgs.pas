unit eventmsgs;
// {-------------------------------------------------------------------------}
// (*
//    � 1998 Ted Houts
//    AUTHOR : Ted Houts - thouts@speedlink.com
//
//    Contains the message definitions for the NT Event Log Demo,
//    converted to Pascal from eventmsgs.h created by Message Compiler
//
// *)
// {-------------------------------------------------------------------------}
interface
const
CATEGORY_APP_START               = $00000001;
CATEGORY_APP_EXIT                = $00000002;
CATEGORY_SYSINFO                 = $00000003;
MSG_START_TIME                   = $00000021;
MSG_EXIT_TIME                    = $00000022;
MSG_DISK_SPACE                   = $00000028;
MSG_GENERIC                      = $00000032;

implementation
end.
