{------------------------------------------------------------------------------}
(*
   � 1998 Ted Houts
   AUTHOR : Ted Houts - thouts@speedlink.com

   This unit contains a set of components demonstrating writing to the
   NT Event Log.
*)
{------------------------------------------------------------------------------}
unit uEventLog;

interface

uses
   Windows,Classes,SysUtils,Registry;

const
//
// The types of events that can be logged,
// translated from winnt.h in MSVC++
//
                                         //EVENT VIEWER OUTPUT RESULT
                                   //type displayed  //icon shown
                                   //--------------  ---------------------------
EVENTLOG_SUCCESS          = $0000; //None            //Blue circle white "i"
EVENTLOG_ERROR_TYPE       = $0001; //Error           //stop sign
EVENTLOG_WARNING_TYPE     = $0002; //Warning         //Yellow circle w/black "!"
EVENTLOG_INFORMATION_TYPE = $0004; //Information     //Blue circle white "i"
EVENTLOG_AUDIT_SUCCESS    = $0008; //Success Audit   //An image of a key
EVENTLOG_AUDIT_FAILURE    = $0010; //Failure Audit   //An image of a lock




//The maximum number of distinct strings to be sent to event log
//This value is hardcoded for this specific application
MAX_MESSAGE_STRINGS = 5;




//These are hardcoded and should change depending on the
//deployment strategies of the application.

MSG_DLL_NAME = 'myapp_msg.dll';  //the path where the event message
                                 //dll will reside, in this application
                                 //the system directory
CAT_DLL_NAME = 'myapp_msg.dll';  //the path where the category message
                                 //dll will reside, in this application
                                 //the system directory

//the number of categories in the category file
CATEGORY_COUNT = 3;

//the event source, this is the "key" in the registry, where the
//event message files location can be determined (see TEventLog.CheckRegistry)
EVENT_SOURCE = 'MYAPP_REG';


//this function returns the system directory
function GetSystemDirectoryString:String;
//this function returns true if running on NT
function IsNT:Boolean;

type
  //the various types of events
  TEventType = (etSuccess,etError,etWarning,etInformation,
                  etAuditSuccess,etAuditFailure);

  //TEventLog contains all the common properties and methods
  //for the descendant event log components defined further, because
  //it is "generic", it isn't currently defined to be registered in the
  //component palette
  TEventLog = class(TComponent)
  private
    { Private declarations }
   FMsgDLLPath : String; //the path of the message dll
   FCatDLLPath : String; //the path of the categorymessage dll

   FEventType : TEventType; //the type of message you want to express
                           //NT Event log will show different icon based
                            //on this value

   FNTEventType : Integer; //this is the translation of the TEventType index, to
                           //NT value (etError -> EVENTLOG_ERROR_TYPE)

   FEventID : Integer;     //the Event ID for this event
   FCategoryID : Integer;  //the Category ID for this event

   FMsg : TStrings;        //The message the application sets up for the event


   FMessageStr : array[0..MAX_MESSAGE_STRINGS - 1] of PChar; //the array of
                           //custom message strings, if "merging" with message
                           //dll
   FMessageCount : Integer; //the number of custom message strings, if "merging"
                            //with message dll


   FIsNT : Boolean; //set in constructor, checked in LogEvent. This so that
                    //exception is raised when running in '95 while attempt
                    //to write to log, not when the component is created. Faster
                    //to check this varible, than to call IsNT function everytime

   procedure SetEventType(value : TEventType);
   procedure SetEventID(value : Integer);
   procedure SetCategoryID(value : Integer);

   procedure SetMsg;  //formats FMsg into a format for the ReportEvent API call
   procedure CheckRegistry;  //assure that the proper settings are configured
                             //for NT Event logging

  protected
    { Protected declarations }
    property CategoryID : Integer
      read FCategoryID
      write SetCategoryID;

    property EventID : Integer
      read FEventID
      write SetEventID;

    property EventType: TEventType
      read FEventType
      write SetEventType
      default etInformation;

    property MsgDLLPath  : String
      read FMsgDLLPath;

    property CatDLLPath  : String
      read FCatDLLPath;


  public
    { Public declarations }
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
    procedure LogEvent;  //the main procedure to write to the NT Event Log

  published
    { Published declarations }
    property Msg : TStrings
      read FMsg
      write FMsg;

  end;

  //a custom component which simplifies the Message Event for
  //the start up message
  TEvtAppStart = class(TEventLog)
  private
    { Private declarations }

  protected
    { Protected declarations }

  public
    { Public declarations }
   constructor Create(AOwner: TComponent);override;
    destructor Destroy; override;
    procedure Log;
  published
    { Published declarations }
  end;

  //a custom component which simplifies the Message Event for
  //the exit up message
  TEvtAppExit = class(TEventLog)
  private
    { Private declarations }

  protected
    { Protected declarations }

  public
    { Public declarations }
   constructor Create(AOwner: TComponent);override;
    destructor Destroy; override;
    procedure Log;
  published
    { Published declarations }
  end;

  //a custom component which simplifies the Message Event for
  //the drive info message
  TEvtDriveInfo = class(TEventLog)
  private
    { Private declarations }

  protected
    { Protected declarations }

  public
    { Public declarations }
   constructor Create(AOwner: TComponent);override;
    destructor Destroy; override;
    procedure Log;
  published
    { Published declarations }
  end;

  //a custom component which simplifies the Message Event for
  //generic messages
  TEvtGeneric = class(TEventLog)
  private
    { Private declarations }

  protected
    { Protected declarations }

  public
    { Public declarations }
   constructor Create(AOwner: TComponent);override;
    destructor Destroy; override;
    procedure Log;
  published
    { Published declarations }

    //"expose" the event type for generic events
    property EventType: TEventType
      read FEventType
      write SetEventType
      default etInformation;

  end;


procedure Register;

implementation
uses eventmsgs; // Pascal translated from eventmsgs.h, created by Message
               //Complier

{$R uEventLog.res}  //component bitmap resource file

{------------------------------------------------------------------------------}
(*

TEvtAppStart

*)
{------------------------------------------------------------------------------}
constructor TEvtAppStart.Create(AOwner: TComponent);
begin
   inherited Create(AOwner);

   //code these protected values to assure they
   //are "in sync" with Message File DLL
   EventType := etInformation;
   EventID := MSG_START_TIME;
   CategoryID := CATEGORY_APP_START;
end;

destructor TEvtAppStart.Destroy;
begin
   inherited Destroy;
end;

procedure  TEvtAppStart.Log;
begin
   //fake out a TStringList;
   Msg.Text := DateTimeToStr(Now);
   LogEvent;
end;

{------------------------------------------------------------------------------}
(*

TEvtAppExit

*)
{------------------------------------------------------------------------------}
constructor TEvtAppExit.Create(AOwner: TComponent);
begin
   inherited Create(AOwner);

   //code these protected values to assure they
   //are "in sync" with Message File DLL

   EventType := etInformation;
   EventID := MSG_EXIT_TIME;
   CategoryID := CATEGORY_APP_EXIT;
end;

destructor TEvtAppExit.Destroy;
begin
   inherited Destroy;
end;
procedure  TEvtAppExit.Log;
begin
   //fake out a TStringList;
   Msg.Text := DateTimeToStr(Now);
   LogEvent;
end;

{------------------------------------------------------------------------------}
(*

TEvtDriveInfo

*)
{------------------------------------------------------------------------------}
constructor TEvtDriveInfo.Create(AOwner: TComponent);
begin
   inherited Create(AOwner);

   //code these protected values to assure they
   //are "in sync" with Message File DLL
   EventType := etInformation;
   EventID := MSG_DISK_SPACE;
   CategoryID := CATEGORY_SYSINFO;
end;

destructor TEvtDriveInfo.Destroy;
begin
   inherited Destroy;
end;

procedure  TEvtDriveInfo.Log;
var
   i : Integer;
begin

{ Format it like the following, add some TABs and CRLF for
  better formatting
   Drive          %1
   File System:   %2
   Disk Size:     %3
   Free Space:    %4
   Used Space:    %5
   Percent Full:  %6
}
   for i := 0 to Msg.Count - 1 do
         Msg[i] := Chr(9) + Msg[i] + Chr(13) + Chr(10);
   LogEvent;
end;


{------------------------------------------------------------------------------}
(*

TEvtGeneric

*)
{------------------------------------------------------------------------------}
constructor TEvtGeneric.Create(AOwner: TComponent);
begin
   inherited Create(AOwner);

   //code these protected values to assure they
   //are "in sync" with Message File DLL
   //Event Type is a default though, as the
   //EventType property is available in TEvtGeneric

   EventType := etInformation;
   EventID := MSG_GENERIC;
   CategoryID := 0;
end;

destructor TEvtGeneric.Destroy;
begin

   inherited Destroy;
end;

procedure  TEvtGeneric.Log;
var
   i : Integer;
begin
   // 5 is the number of strings expected for the
   //generic event (see eventmsgs.mc file) , pad
   //with empty spaces
   for i := Msg.Count to 5 do
      Msg.Add('');

   LogEvent;
end;

{------------------------------------------------------------------------------}
(*

   Procedure : SetMsg

   This procedure formats the user TStrings into the proper format
   for the ReportEvent call

*)
{------------------------------------------------------------------------------}
procedure TEventLog.SetMsg;
var
   i : Integer;
begin
   //Track the count of messages
   FMessageCount := 0;

   for i := 0 to FMsg.Count - 1 do begin
      if i > MAX_MESSAGE_STRINGS  - 1 then begin
//         should raise this exception, but instead will "truncate"
//         raise Exception.Create('Number of message strings exceeds maximum of ' +
//                                 IntToStr(MAX_MESSAGE_STRINGS));
         break; //"truncate" extra strings
      end;
      FMessageStr[i] := PChar(FMsg[i]);
      Inc(FMessageCount);
   end;



end;
{------------------------------------------------------------------------------}
(*

   Procedure : LogEvent

   This procedure does the actual ReportEvent API call to log the event
*)
{------------------------------------------------------------------------------}
procedure TEventLog.LogEvent;
var
   hEventLog : THandle; //handle to the event log
   Data : array[0..40] of char;
begin

   if not FIsNT then //if not NT, don't bother attempting
      raise Exception.Create('Event Logging is only available on NT!');

   CheckRegistry;  //make sure the registry is configured correctly

   SetMsg;  //format the message strings to the expected type

   Data := 'A Delphi based NT Event Log example';  //not useful, but shows
                                                   //how to use data parameter


   hEventLog := RegisterEventSource(nil, //server name where message dll resides
                                         //nil indicates local machine
                                    EVENT_SOURCE); //application registry key

   if (hEventLog <= 0) then begin
      raise Exception.Create('RegisterEventSource call failed!');
      exit;
   end;

   if not ReportEvent(hEventLog,     //handle to EventLog
                                     //as retrieved by RegisterEventSource

                      FNTEventType,  //type of error

                      FCategoryID,   //category, this number shows up in the
                                     //category column if no
                                     //CategoryMessageFile.
                                     //If zero the category column will show
                                     //"None", else will be the CategoryID
                                     //string in the CategoryMessageFile

                      FEventID,      //event id, the ID defined in the
                                     //EventMessageFile.  If this ID is not
                                     //defined in the message file, your data
                                     //will appear, but with default format

                      nil,           //users security id,
                                     //not dicussed in this article

                      FMessageCount, //number of strings in message

                      sizeof(Data),  //size of data

                      @FMessageStr,  //array of our null terminated
                                     //message strings

                      @Data)         //buffer data
      then begin
         DeregisterEventSource(hEventLog);
         raise Exception.Create('Failure in Report Event');
   end;
   //return event log handle retrieved by RegisterEventSource
   DeregisterEventSource(hEventLog);

end;

{------------------------------------------------------------------------------}
(*

   Procedure : SetEventType

   This procedure maintains the event type, and sets the FNTEventType
   to the API value
*)
{------------------------------------------------------------------------------}
procedure TEventLog.SetEventType(value : TEventType);
begin
   FEventType := value;

   case FEventType of
      etSuccess:
         FNTEventType := EVENTLOG_SUCCESS;

      etError:
         FNTEventType := EVENTLOG_ERROR_TYPE;

      etWarning:
         FNTEventType := EVENTLOG_WARNING_TYPE;

      etInformation:
         FNTEventType := EVENTLOG_INFORMATION_TYPE;

      etAuditSuccess:
         FNTEventType := EVENTLOG_AUDIT_SUCCESS;

      etAuditFailure:
         FNTEventType := EVENTLOG_AUDIT_FAILURE;

      else
         FNTEventType := EVENTLOG_SUCCESS;  //just in case, shouldn't occur

   end;

end;

procedure TEventLog.SetEventID(value : Integer);
begin
   FEventID := value;
end;
procedure TEventLog.SetCategoryID(value : Integer);
begin
   FCategoryID := value;
end;

{------------------------------------------------------------------------------}
(*

   Procedure : CheckRegistry

   This procedure maintains the event type, and sets the FNTEventType
   to the API value

   This procedure shows the registry dependency for NT Event Logging

   The EventMessageFile, and CategoryMessageFile details are stored
   under the Application key...

      HKEY_LOCAL_MACHINE
        System
          CurrentControlSet
            Services
              EventLog
                Application
                  MYAPP_REG <- this file
                Security
                System

   This procedure will set up the registry if incorrect, and
   will raise an exception if the files don't exist

*)
{------------------------------------------------------------------------------}
procedure TEventLog.CheckRegistry;
var
   Reg : TRegistry;
const
   RegistryMsgRootPath = '\SYSTEM\CurrentControlSet\Services\EventLog\Application';
begin

   Reg := TRegistry.Create;
   try
      with Reg do begin
         RootKey := HKEY_LOCAL_MACHINE;
         OpenKey( RegistryMsgRootPath + '\' + EVENT_SOURCE, True );
         if ReadString('EventMessageFile') <> FMsgDLLPath then
            WriteString('EventMessageFile', FMsgDLLPath);
         if ReadString('CategoryMessageFile') <> FCatDLLPath then
            WriteString('CategoryMessageFile', FCatDLLPath);

         try //exception on readinteger if value is not there
            //7 indicates support of all event types
            if ReadInteger('TypesSupported') <> 7 then
               WriteInteger('TypesSupported',7);
            except on E:Exception do
               WriteInteger('TypesSupported',7);
         end;

         try //exception on readinteger if value is not there
            if ReadInteger('CategoryCount') <> CATEGORY_COUNT then
               WriteInteger('CategoryCount',CATEGORY_COUNT);
            except on E:Exception do
               WriteInteger('CategoryCount',CATEGORY_COUNT);
         end;

         CloseKey;
      end;
   finally
      Reg.Free;
   end;
   //make sure the files exist on the system, assuming local
   //server.  The messages will still log, but the output will
   //be incorrect as viewed through the NT Event Viewer
   if not FileExists(FMsgDLLPath) then
      raise Exception.Create('Event Message File does not exist!' + Chr(13) +
                              Chr(10) + FMsgDLLPath);

   if not FileExists(FCatDLLPath) then
      raise Exception.Create('Category Message File does not exist!' + Chr(13) +
                              Chr(10) + FCatDLLPath);

end;
constructor TEventLog.Create(AOwner: TComponent);
begin
   inherited Create(AOwner);
   FIsNT := IsNT; //will check in LogEvent;

   FMsg := TStringList.Create;
   FMsg.Clear;
   FMsg.Add('Default message!');

   FMsgDLLPath := GetSystemDirectoryString + '\' + MSG_DLL_NAME;
   FCatDLLPath := GetSystemDirectoryString + '\' + CAT_DLL_NAME;

   SetEventType(etInformation); //default to Information type message



end;

destructor TEventLog.Destroy;
begin
   FMsg.Free;
   inherited Destroy;
end;

{------------------------------------------------------------------------------}
(*
   Utility Functions
*)
{------------------------------------------------------------------------------}
{------------------------------------------------------------------------------}
(*
   Function: GetSystemDirectoryString
   Returns the system directory as a string
*)
{------------------------------------------------------------------------------}
function GetSystemDirectoryString:String;
var
   pBuff : PChar;
   BuffSize : DWORD;
begin
   BuffSize := MAX_PATH+1;
   pBuff  := StrAlloc(BuffSize);

   try
      if GetSystemDirectory(pBuff,BuffSize) <> 0 then begin
         result := pBuff;
      end
      else begin
         result := 'error';
         raise Exception.Create('Error in getting windows directory');
      end;

      finally
         StrDispose(pBuff);
   end;

end;
{------------------------------------------------------------------------------}
(*
   Function: IsNT
   Returns TRUE if the system is NT, FALSE otherwise

   Event Logging is only available in NT.
*)
{------------------------------------------------------------------------------}
function IsNT:Boolean;
var
   OSVersion: TOSVersionInfo;
   OSId : Integer;
begin
   with OSVersion do
   begin
      dwOSVersionInfoSize := sizeOf(TOSVersionInfo);
      if not getVersionEx(OSVersion) then
         OSId := -1
      else
         OSId := dwPlatformId;
   end;

   result := (OSId = VER_PLATFORM_WIN32_NT);
end;

procedure Register;
begin
  RegisterComponents('NT Event Log Demo', [TEvtAppStart,
                                 TEvtAppExit,
                                 TEvtDriveInfo,
                                 TEvtGeneric]);
end;


end.


