{------------------------------------------------------------------------------}
(*
   � 1998 Ted Houts
   AUTHOR : Ted Houts - thouts@speedlink.com

   This unit provides some disk functions to demonstrate Event Logging for
   the NT Event Log Demo
*)
{------------------------------------------------------------------------------}
unit uUtils;

interface


uses Windows,Classes,SysUtils;

//Record to hold some disk information
type TDiskInfo = record
   Drive             : String;
   FileSystem        : String;
   DiskSize          : String;
   FreeSpace         : String;
   UsedSpace         : String;
   PercentFull       : String;
end;

function GetDiskSpaceInfo(DriveChar : Char):TDiskInfo;
procedure FillStringsWithDrives(TheStrings:TStrings);
function GetFileSystemName(DriveChar: Char): string;
function AddCommas(value :String):String;

implementation
{------------------------------------------------------------------------------}
(*
   Function: AddCommas
   This function will return the string passed in formated
   with commas as in a dollar, or byte size style
   (i.e. 1234567 -> 1,234,567)
*)
{------------------------------------------------------------------------------}
function AddCommas(value :String):String;
var
i : integer;
DigitCounter : Integer;
begin
   result := '';
   DigitCounter := 0;
   for i := Length(value) downto 1 do begin
      Inc(DigitCounter);
      result := value[i] + result;
      if ((DigitCounter > 2) and (i > 1)) then begin
         result := ',' + result;
         DigitCounter := 0;
      end;

   end;

end;
{------------------------------------------------------------------------------}
(*
   Function: GetDiskSpaceInfo
   This function returns a TDiskInfo record with
   information about the drive parameter passed in
*)
{------------------------------------------------------------------------------}
function GetDiskSpaceInfo(DriveChar : Char):TDiskInfo;
var
SPC,BPS,FC,C : DWORD;
bRet : Boolean;
OldErrorMode: Integer;
begin

   //do not show "insert floppy" error dialog
   OldErrorMode := SetErrorMode(SEM_FAILCRITICALERRORS);
   try
      bRet := GetDiskFreeSpace(PChar(driveChar + ':\'),SPC,BPS,FC,C);
      if bRet then begin
         with result do begin

            DiskSize := AddCommas(IntToStr(Trunc(SPC * BPS * C )));
            FreeSpace := AddCommas(IntToStr(Trunc(SPC * BPS * FC )));
            UsedSpace := AddCommas(IntToStr(Trunc(SPC * BPS * (C - FC))));
            PercentFull := IntToStr(Trunc(1.0 - C/FC));
         end;
      end
      else begin  //probably a CD-ROM, or removed drive etc...
         with result do begin
            DiskSize := '-1';
            FreeSpace := '-1';
            UsedSpace := '-1';
         end;
      end;
   finally
      SetErrorMode(OldErrorMode);
   end;
   result.Drive := DriveChar + ':\';
   result.FileSystem := GetFileSystemName(DriveChar);
end;
{------------------------------------------------------------------------------}
(*
   Function: FillStringsWithDrives
   This function fills TStrings with the available drive
   letters
*)
{------------------------------------------------------------------------------}
procedure FillStringsWithDrives(TheStrings:TStrings);
var
driveBitSet: set of 0..25; //only 26 letters in the alphabet,
                           //one for each possible drive
driveNumber : Integer;
driveCharacter : char;
begin
   TheStrings.Clear;
   TheStrings.BeginUpdate;
   try

      Integer(driveBitSet) := GetLogicalDrives;
      //driveBitSet is now a bitmask representing the currently available
      //disk drives
      for driveNumber := 0 to 25 do begin
         if not (driveNumber in driveBitSet) then
            continue; //this letter not assigned to a drive
         driveCharacter := Char(driveNumber + Ord('A'));
         TheStrings.Add(driveCharacter + ':');
      end;
   finally
      TheStrings.EndUpdate;
   end;
end;
{------------------------------------------------------------------------------}
(*
   Function: GetFileSystemName
   This function returns a string describing the file system
   from the drive passed in
*)
{------------------------------------------------------------------------------}
function GetFileSystemName(DriveChar: Char): string;
var
  Buf: array [0..MAX_PATH] of Char;
  dummy : dword;
begin
   result := '';

   if GetVolumeInformation(PChar(DriveChar + ':\'), nil, 0,
               nil, dummy, dummy, Buf, sizeof(Buf)) then
      SetString(Result, Buf, StrLen(Buf))
   else
      result := '<error>'
end;


end.
