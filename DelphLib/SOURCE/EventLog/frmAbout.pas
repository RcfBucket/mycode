{------------------------------------------------------------------------------}
(*
   � 1998 Ted Houts
   AUTHOR : Ted Houts - thouts@speedlink.com

   About Box Unit for NT Event Log Demo
*)
{------------------------------------------------------------------------------}
unit frmAbout;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, ExtCtrls;

type
  TFrm_About = class(TForm)
    btnOK: TBitBtn;
    Bevel1: TBevel;
    Label1: TLabel;
    Image1: TImage;
    Label2: TLabel;
    Label3: TLabel;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Frm_About: TFrm_About;

implementation

{$R *.DFM}

end.
