ÿ
 TFRM_MAIN 0®  TPF0	TFrm_MainFrm_MainLeft¿ Top¨ BorderStylebsSingleCaptionNT Event Logging DemoClientHeightClientWidthFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Heightõ	Font.NameArial
Font.Style Menu	MainMenu1PositionpoScreenCenterOnCreate
FormCreate	OnDestroyFormDestroyPixelsPerInch`
TextHeight TBevelBevel1Left Top WidthHeight	AlignalTopShape	bsTopLine  	TGroupBox	GroupBox1LeftTopPWidthHeight=CaptionDisk InformationTabOrder  TLabelLabel1LeftTopWidthZHeightCaptionAvailable Drives:Font.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Heightõ	Font.NameArial
Font.StylefsBold 
ParentFont  	TComboBoxcbDrivesLeftpTopWidth1HeightStylecsDropDownList
ItemHeightTabOrder   TBitBtnbtnLogDiskInfoLeft¬ TopWidthKHeightCaptionLog It!TabOrderOnClickbtnLogDiskInfoClick
Glyph.Data
^  Z  BMZ      v   (               ä                                     ÀÀÀ   ÿ  ÿ   ÿÿ ÿ   ÿ ÿ ÿÿ  ÿÿÿ             ÿÿÿÿÿð   ïÿÿÿð   ð   ð   ð   ð   ïÿÿð   ï³ÿÿÿð   ïÿÿøð   Ìxð   ³ð   ³ð   ¸ð   ïÿÿüÿð   ÿÿÿÿÿð                  	TGroupBox	GroupBox2LeftTop WidthHeightÑ CaptionGenericTabOrder TLabelLabel2Left>TopWidth Height	AlignmenttaCenterAutoSizeCaptionMessage to LogFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Heightõ	Font.NameArial
Font.StylefsBold 
ParentFont  TLabelLabel3Left>Top Width Height	AlignmenttaCenterAutoSizeCaption
Event TypeFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Heightõ	Font.NameArial
Font.StylefsBold 
ParentFont  TMemommoMessagesLeft>Top Width HeightYLines.StringsAllsystemsarefunctioning
perfectly! TabOrder   	TComboBoxcbEventTypesLeft>Top Width HeightStylecsDropDownList
ItemHeightItems.StringsSuccessErrorWarningInformationAudit SuccessAudit Failure TabOrder  TBitBtnbtnLogGenericInfoLeftaTop° WidthKHeightCaptionLog It!TabOrderOnClickbtnLogGenericInfoClick
Glyph.Data
^  Z  BMZ      v   (               ä                                     ÀÀÀ   ÿ  ÿ   ÿÿ ÿ   ÿ ÿ ÿÿ  ÿÿÿ             ÿÿÿÿÿð   ïÿÿÿð   ð   ð   ð   ð   ïÿÿð   ï³ÿÿÿð   ïÿÿøð   Ìxð   ³ð   ³ð   ¸ð   ïÿÿüÿð   ÿÿÿÿÿð                  TBitBtnbtnCloseLeftÈ ToppWidthKHeightCaption&CloseModalResultTabOrderOnClickbtnCloseClick
Glyph.Data
â  Þ  BMÞ      v   (   $            h                                   ÀÀÀ    ÿ  ÿ   ÿÿ ÿ   ÿ ÿ ÿÿ  ÿÿÿ 8w÷÷ø33?  DD@ÿôDDó3ÿ  33MPÿô3338  33EÕÿô333338ó?  33M]ÿô3333x38ó?  33EÕïä333338ó?  33M]ÿô3333x38ó?  33EÕïä333338ó?  33M]ÿô3333x38ó?  33EÕïä333338ó?  33M]þô3333x38ó?  33EÕïä333338ó?  33M]þô3333x38ó?  33DDDDD3333ÿøó?  333333333333?  333   333333?ÿÿó3?  333
ª 333333ó3?  333   333333ÿøó3?  	NumGlyphs  	TGroupBox	GroupBox3LeftTopWidthHeight=CaptionMinimal InfoTabOrder TBitBtnbtnLogMinimalLeftaTopWidthKHeightCaptionLog It!TabOrder OnClickbtnLogMinimalClick
Glyph.Data
^  Z  BMZ      v   (               ä                                     ÀÀÀ   ÿ  ÿ   ÿÿ ÿ   ÿ ÿ ÿÿ  ÿÿÿ             ÿÿÿÿÿð   ïÿÿÿð   ð   ð   ð   ð   ïÿÿð   ï³ÿÿÿð   ïÿÿøð   Ìxð   ³ð   ³ð   ¸ð   ïÿÿüÿð   ÿÿÿÿÿð                  TEvtAppStartEvtAppStart1Msg.StringsDefault message! Leftè Top¤   TEvtAppExitEvtAppExit1Msg.StringsDefault message! Leftè TopÐ   TEvtDriveInfoEvtDriveInfo1Msg.StringsDefault message! Leftè Top  TEvtGenericEvtGeneric1Msg.StringsDefault message! Leftè Top4  	TMainMenu	MainMenu1LeftToph 	TMenuItemFile1Caption&File 	TMenuItemExit1CaptionE&xitOnClick
Exit1Click   	TMenuItemHelp1Caption&Help 	TMenuItemAbout1Caption	&About...OnClickAbout1Click     