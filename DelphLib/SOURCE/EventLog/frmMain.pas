{------------------------------------------------------------------------------}
(*
   � 1998 Ted Houts
   AUTHOR : Ted Houts - thouts@speedlink.com

   This is the main form of the NT Event Log Demo
*)
{------------------------------------------------------------------------------}
unit frmMain;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, uEventLog, Menus, ExtCtrls;

type
  TFrm_Main = class(TForm)
    EvtAppStart1: TEvtAppStart;
    EvtAppExit1: TEvtAppExit;
    EvtDriveInfo1: TEvtDriveInfo;
    EvtGeneric1: TEvtGeneric;
    GroupBox1: TGroupBox;
    cbDrives: TComboBox;
    btnLogDiskInfo: TBitBtn;
    MainMenu1: TMainMenu;
    File1: TMenuItem;
    Exit1: TMenuItem;
    Help1: TMenuItem;
    About1: TMenuItem;
    Bevel1: TBevel;
    Label1: TLabel;
    GroupBox2: TGroupBox;
    mmoMessages: TMemo;
    cbEventTypes: TComboBox;
    Label2: TLabel;
    Label3: TLabel;
    btnLogGenericInfo: TBitBtn;
    btnClose: TBitBtn;
    GroupBox3: TGroupBox;
    btnLogMinimal: TBitBtn;
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure btnLogDiskInfoClick(Sender: TObject);
    procedure Exit1Click(Sender: TObject);
    procedure About1Click(Sender: TObject);
    procedure btnLogGenericInfoClick(Sender: TObject);
    procedure btnCloseClick(Sender: TObject);
    procedure btnLogMinimalClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Frm_Main: TFrm_Main;

implementation

{$R *.DFM}
uses uUtils,frmAbout;

{------------------------------------------------------------------------------}
(*

   Procedure : TFrm_Main.FormCreate

   Creates the main form, and does some initilization
*)
{------------------------------------------------------------------------------}
procedure TFrm_Main.FormCreate(Sender: TObject);
var
nIndex : Integer;
begin
   FillStringsWithDrives(cbDrives.Items);

   //default the c drive
   nIndex := cbDrives.Items.IndexOf('C:');
   if nIndex < 0 then nIndex := 0;
   cbDrives.ItemIndex := nIndex;

   //default success
   cbEventTypes.ItemIndex := 0;

   //log to NT Event Log that the application has started
   EvtAppStart1.Log;

end;

procedure TFrm_Main.FormDestroy(Sender: TObject);
begin
   //log to NT Event Log that the application has terminated
   EvtAppExit1.Log;
end;


procedure TFrm_Main.Exit1Click(Sender: TObject);
begin
   Close;
end;

procedure TFrm_Main.About1Click(Sender: TObject);
begin
   with TFrm_About.Create(Self) do begin
      try
         ShowModal;
      finally
         free;
      end;
   end;
end;

procedure TFrm_Main.btnCloseClick(Sender: TObject);
begin
   Close;
end;
{------------------------------------------------------------------------------}
(*

   Procedure : TFrm_Main.btnLogDiskInfoClick

   Get Disk Information, and pass the set of data
   to the NT Event Log
*)
{------------------------------------------------------------------------------}
procedure TFrm_Main.btnLogDiskInfoClick(Sender: TObject);
begin

   EvtDriveInfo1.Msg.Clear;
   with GetDiskSpaceInfo(cbDrives.Text[1]) do begin
      //fill msg with drive info
      EvtDriveInfo1.Msg.Add(Drive);
      EvtDriveInfo1.Msg.Add(FileSystem);
      EvtDriveInfo1.Msg.Add(DiskSize);
      EvtDriveInfo1.Msg.Add(FreeSpace);
      EvtDriveInfo1.Msg.Add(UsedSpace);
      EvtDriveInfo1.Msg.Add(PercentFull);
   end;
   EvtDriveInfo1.Log;
   MessageDlg('Disk information written to NT Event Log!',mtInformation,[mbOK],0);
end;

{------------------------------------------------------------------------------}
(*

   Procedure : TFrm_Main.btnLogDiskInfoClick

   Send the information in the memo to the NT Event Log
*)
{------------------------------------------------------------------------------}
procedure TFrm_Main.btnLogGenericInfoClick(Sender: TObject);
begin
   //the TEvtGeneric component allows "free wheeling"
   //of setting the Event Type and Message from user interface
   EvtGeneric1.Msg.Assign(mmoMessages.Lines);
   EvtGeneric1.EventType := TEventType(cbEventTypes.ItemIndex);
   EvtGeneric1.Log;
   MessageDlg('Generic message written to NT Event Log!',mtInformation,[mbOK],0);
end;

{------------------------------------------------------------------------------}
(*

   Procedure : TFrm_Main.btnLogDiskInfoClick

   Call the ReportEvent with minimal information, and no
   registry confirmation
*)
{------------------------------------------------------------------------------}
procedure TFrm_Main.btnLogMinimalClick(Sender: TObject);
var
   hEventLog : THandle; //handle to the event log
   Msg : PChar;

begin
   //write directly to the log file, w/o any registry
   //verification

   if not IsNT then //if not NT, don't bother attempting, 
      raise Exception.Create('Event Logging is only available on NT!');


   Msg := 'Hello NT Event Log'; //a simple message

   hEventLog := RegisterEventSource(nil, //server name where message dll resides
                                         //nil indicates local machine
                                    '???'); //application registry key

   if (hEventLog <= 0) then begin
      raise Exception.Create('RegisterEventSource call failed!');
      exit;
   end;



   if not ReportEvent(hEventLog,     //handle to EventLog
                                     //as retrieved by RegisterEventSource

                      0,             //type of error

                      0,             //category, this number shows up in the
                                     //category column if no
                                     //CategoryMessageFile.
                                     //If zero the category column will show
                                     //"None", else will be the CategoryID
                                     //string in the CategoryMessageFile

                      0,             //event id, the ID defined in the
                                     //EventMessageFile.  If this ID is not
                                     //defined in the message file, your data
                                     //will appear, but with default format

                      nil,           //users security id,
                                     //not discussed in this article

                      1, //1 null terminated string in message

                      0,  //size of data

                      @Msg,  //the single terminated message

                      nil)         //buffer data
      then begin
         DeregisterEventSource(hEventLog);
         raise Exception.Create('Failure in Report Event');
   end;
   //return event log handle retrieved by RegisterEventSource
   DeregisterEventSource(hEventLog);
   MessageDlg('Minimial message written to NT Event Log!',mtInformation,[mbOK],0);

end;

end.

