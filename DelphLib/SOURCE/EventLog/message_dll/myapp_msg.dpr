{------------------------------------------------------------------------------}
(*
   � 1998 Ted Houts
   AUTHOR : Ted Houts - thouts@speedlink.com

   This is the project file for the simplest of DLLs.
   This DLL contains the Event Message File resource for
   NT Event Log Demo application
*)
{------------------------------------------------------------------------------}

library myapp_msg;

{$R eventmsgs.RES} //resource with event message info
begin
end.
