// {-------------------------------------------------------------------------}
// (*
//    � 1998 Ted Houts
//    AUTHOR : Ted Houts - thouts@speedlink.com
//
//    Contains the message definitions for the NT Event Log Demo
//
// *)
// {-------------------------------------------------------------------------}
//
//
//
//
// Category Strings - Must start with the number 1
//
//
//  Values are 32 bit values layed out as follows:
//
//   3 3 2 2 2 2 2 2 2 2 2 2 1 1 1 1 1 1 1 1 1 1
//   1 0 9 8 7 6 5 4 3 2 1 0 9 8 7 6 5 4 3 2 1 0 9 8 7 6 5 4 3 2 1 0
//  +---+-+-+-----------------------+-------------------------------+
//  |Sev|C|R|     Facility          |               Code            |
//  +---+-+-+-----------------------+-------------------------------+
//
//  where
//
//      Sev - is the severity code
//
//          00 - Success
//          01 - Informational
//          10 - Warning
//          11 - Error
//
//      C - is the Customer code flag
//
//      R - is a reserved bit
//
//      Facility - is the facility code
//
//      Code - is the facility's status code
//
//
// Define the facility codes
//
#define FACILITY_SYSTEM                  0x0
#define FACILITY_STUBS                   0x3
#define FACILITY_RUNTIME                 0x2
#define FACILITY_IO_ERROR_CODE           0x4


//
// Define the severity codes
//


//
// MessageId: CATEGORY_APP_START
//
// MessageText:
//
//  App Start
//
#define CATEGORY_APP_START               0x00000001L

//
// Category String #2
//
//
// MessageId: CATEGORY_APP_EXIT
//
// MessageText:
//
//  App Exit
//
#define CATEGORY_APP_EXIT                0x00000002L

//
// Category String #3
//
//
// MessageId: CATEGORY_SYSINFO
//
// MessageText:
//
//  SysInfo
//
#define CATEGORY_SYSINFO                 0x00000003L

//
// Used by TEvtAppStart
//
//
// MessageId: MSG_START_TIME
//
// MessageText:
//
//  Started at %1
//
#define MSG_START_TIME                   0x00000021L

//
// Used by TEvtAppExit
//
//
// MessageId: MSG_EXIT_TIME
//
// MessageText:
//
//  Terminated at %1
//
#define MSG_EXIT_TIME                    0x00000022L


// The line feeds will be actually
// be done in the application
//
//
//Used by TEvtDiskInfo
//
//
// MessageId: MSG_DISK_SPACE
//
// MessageText:
//
//  Drive Tested: %1
//  File System: %2
//  Disk Size: %3
//  Free Space: %4
//  Used Space: %5
//
#define MSG_DISK_SPACE                   0x00000028L

//
// Used by TEvtGeneric
//
//
// MessageId: MSG_GENERIC
//
// MessageText:
//
//  %1 %2 %3 %4 %5
//
#define MSG_GENERIC                      0x00000032L

