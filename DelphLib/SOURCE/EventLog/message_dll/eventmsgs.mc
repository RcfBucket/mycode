;// {-------------------------------------------------------------------------}
;// (*
;//    � 1998 Ted Houts
;//    AUTHOR : Ted Houts - thouts@speedlink.com
;//
;//    Contains the message definitions for the NT Event Log Demo
;//
;// *)
;// {-------------------------------------------------------------------------}
;//
;//
;//
SeverityNames=(
   Success=0x0
   Informational=0x1
   Warning=0x2
   Error=0x3
)

FacilityNames=(
   System=0x0:FACILITY_SYSTEM
   Runtime=0x2:FACILITY_RUNTIME
   Stubs=0x3:FACILITY_STUBS
   Io=0x4:FACILITY_IO_ERROR_CODE
)

;//
;// Category Strings - Must start with the number 1
;//
MessageId=1
Severity=Success
Facility=System
SymbolicName=CATEGORY_APP_START
Language=English
App Start
.

;//
;// Category String #2
;//
MessageId=2
Severity=Success
Facility=System
SymbolicName=CATEGORY_APP_EXIT
Language=English
App Exit
.

;//
;// Category String #3
;//
MessageId=3
Severity=Success
Facility=System
SymbolicName=CATEGORY_SYSINFO
Language=English
SysInfo
.

;//
;// Used by TEvtAppStart
;//
MessageId=33
Severity=Success
Facility=System
SymbolicName=MSG_START_TIME
Language=English
Started at %1
.

;//
;// Used by TEvtAppExit
;//
MessageId=34
Severity=Success
Facility=System
SymbolicName=MSG_EXIT_TIME
Language=English
Terminated at %1
.

;
;// The line feeds will be actually
;// be done in the application
;//
;//
;//Used by TEvtDiskInfo
;//
MessageId=40
Severity=Success
Facility=System
SymbolicName=MSG_DISK_SPACE
Language=English
Drive Tested: %1
File System: %2
Disk Size: %3
Free Space: %4
Used Space: %5
.

;//
;// Used by TEvtGeneric
;//
MessageId=50
Severity=Success
Facility=System
SymbolicName=MSG_GENERIC
Language=English
%1 %2 %3 %4 %5
.

