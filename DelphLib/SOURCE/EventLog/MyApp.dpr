program MyApp;

uses
  Forms,
  frmMain in 'frmMain.pas' {Frm_Main},
  uUtils in 'uUtils.pas',
  frmAbout in 'frmAbout.pas' {Frm_About},
  uEventLog in 'comps\uEventLog.pas';

{$R *.RES}

begin
  Application.Initialize;
  Application.CreateForm(TFrm_Main, Frm_Main);
  Application.Run;
end.
