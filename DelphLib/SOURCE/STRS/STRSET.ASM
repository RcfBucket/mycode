.model TPASCAL

PUBLIC  StrSet

.code
;========================================================== StrSet
;
; Function StrSet(ch: Char; len: Byte): String;
;
; return a string of ch's with length len
;----------------------------------------------------------------------
StrSet         PROC FAR
; Parameters
char		equ byte ptr ss:[bp+8]
len		equ byte ptr ss:[bp+6]
ResultStr	equ dword ptr ss:[bp+10]
pSize		equ 4

	push	bp
	mov	bp,sp
	push	ds

	cld				; go foreward ->
	mov	al,len			; get length
	les	di,resultStr		; ES:DI -> resultStr
	stosb				; save length in result string
	xor	ch,ch
	mov	cl,al			; put lenght in CL
	jcxz	done

	mov	al,char			; get character to fill
	rep	stosb  			; store bytes in return string
done:
	pop	ds
	pop	bp
	ret	pSize
StrSet         ENDP

		END
