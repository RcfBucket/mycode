.MODEL TPASCAL

PUBLIC  StrAddBackSlash

WPT	equ	word ptr

.CODE
;========================================================== StrAddBackSlash
;
; Function StrAddBackSlash(dirName: String): String;
;
; add backslash to dirname if needed
;----------------------------------------------------------------------
StrAddBackSlash    PROC FAR
; Parameters
resultStr	equ	dword	ptr	ss:[bp+10]
DirName	equ	dword	ptr	ss:[bp+6]
pSize		equ	4

	push	bp
	mov	bp,sp
	push	ds

	cld				; direction ->
	lds	si,DirName		; DS:SI -> DirName
	les	di,resultStr		; ES:DI -> result
	xor	ch,ch
	lodsb
	stosb
	mov	cl,al			; cl = Length(dirName)
	jcxz	done			; if length(DirName) = 0 then Exit
	mov	bl,al 			; store in BL too

	rep	movsb			; transfer string
	dec	si
	lodsb				; get last char of dirname
	cmp	al,":"
	je	done
	cmp	al,"\"
	je	done
	mov	al, "\"
	stosb
	mov	di,WPT resultStr
	mov	al,bl
	inc	al
	stosb
done:
	pop	ds
	pop	bp
	ret	psize
StrAddBackSlash    ENDP

		END
