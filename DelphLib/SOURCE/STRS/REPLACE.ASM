.model tpascal

PUBLIC  StrReplace

.code

;========================================================== StrReplace
;
; Function StrReplace(tstr: String; srcCh, trgCh: Char): String;
;
; Replace all occurances of srcCh with trgCh in tstr
;--------------------------------------------------------------------------
;    2       4        2       2       4       4
;  | BP | RetAddr | trgCh | srcCh | tstr  | Result | <----- Stack Usage
;  `-----------------------------------------------'
;                 ^=+6    ^=+8    ^=+10   ^=+14
;--------------------------------------------------------------------------
StrReplace         PROC FAR
; Parameters
resultStr	equ	dword ptr ss:[bp+14]
tstr		equ	dword ptr ss:[bp+10]
srcCh		equ	byte ptr ss:[bp+8]
trgCh		equ	byte ptr ss:[bp+6]
pSize		equ	8

	push	bp
	mov	bp,sp
	push	ds

	cld				; direction ->
	lds	si,tstr		; DS:SI -> tstr
	les	di,resultStr		; ES:DI -> result
	xor	ch,ch
	lodsb
	stosb
	mov	cl,al			; cl = Length(tstr)
	jcxz	done			; if length(tstr) = 0 then Exit
	mov	dl,srcCh		; get srcCh in DL
	mov	dh,trgCh		; and trgCh in DH
StrReplaceLoop:
	lodsb				; get next char
	cmp	al,dl			; is it srcCh
	jne	notSrc		; no, store it
	mov	al,dh			; yes, convert it to trgCh
notSrc:
	stosb
        loop    StrReplaceLoop
done:
	pop	ds
	pop	bp
	ret	psize
StrReplace         ENDP

	END

