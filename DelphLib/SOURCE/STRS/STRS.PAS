{$F+,A-}

unit Strs;
{- Quick string routines }

INTERFACE

type
  TStrJustify = (StrJustNone, StrJustLeft, StrJustCenter, StrJustRight);

{==[ Miscelaneous functions ]==================================================}
{==============================================================================}

{- return a string of len initialized to ch }

function StrCommas(numStr: string): string;
{- insert commas into string }

function StrStrip(tstr: string; low, high: char): string;
{- strip all occurances of chars in tstr that fall within range }

function StrReplace(tstr: string; srcCh, trgCh: char): string;
{- replace all occurances of srcCh with trgCh in tstr and return results }

procedure StrWordWrap(inSt: string; Var outSt, overlap: string;
                      margin: byte; PadToMargin: boolean);
{- Wrap InSt at Margin, storing the result in OutSt and the remainder
   in Overlap}

{=[ Trimming functions ]=======================================================}
{==============================================================================}

function StrTrimTail(tstr: string; trimchar: char): string;
{- Trim trailing trimchar from tstr }

function StrTrimLead(tstr: string; trimchar: char): string;
{- Trim leading trimchar from tstr }

function StrTrimAll(tstr: string; trimchar: char): string;
{- trim leading and trailing trimchar from tstr }

function StrTrimTailSpaces(tstr: string): string;
{- Trim trailing spaces from tstr }

function StrTrimLeadSpaces(tstr: string): string;
{- trim leading spaces from tstr }

function StrTrimAllSpaces(tstr: string): string;
{- trim leading and trailing spaces from tstr }

{==[ Pad functions ]===========================================================}
{==============================================================================}

function StrPadLead(tstr: string; padCh: char; padLen: byte): string;
{- Return tstr padded at left to padLen with padCh (truncating to padLen
   if neccessary) }

function StrPadTail(tstr: string; padCh: char; padLen: byte): string;
{- Return tstr padded at right to padLen with padCh (truncating to padLen
  if neccessary) }

function StrPadTailSpaces(tstr: string; padLen: byte): string;
{- Return tstr padded at right to padLen with spaces (truncating to padLen
   if neccessary) }

function StrPadLeadSpaces(tstr: string; padLen: byte): string;
{- Return tstr padded at left to padLen with spaces (truncating to padLen
   if neccessary) }

function StrCenter(tstr: string; centerCh: char; width: byte): string;
{- return tstr centered in a string of width and padded on either side with
   centerCh }

function StrCenterSpaces(tstr: string; width: byte): string;
{- return tstr centered in a string of width and padded on either side with
   spaces }

procedure StrJustify(jMode: TStrJustify; width: byte; ch: char; var res: string);
procedure StrJustifySpaces(jMode: TStrJustify; width: byte; var res: string);
{- justify strings in a particuar width }

{==[ Sub-string functions ]====================================================}

function StrRight(st: string; num: byte): string;
{- return the rightmost num charaters of st }

function StrLeft(st: string; num: byte): string;
{- return the leftmost num characters from st }

{==[ DOS file name and path functions ]========================================}

function StrAddBackSlash(dirName: string): string;
{- add backslash to dirName if it needs one }

function StrPackPath(s: string; width: byte): string;
function StrJustPath(PathName : string) : string;
function StrJustFile(PathName : string) : string;
function StrJustExt(PathName : string) : string;

function StrForceExtension(Name, Ext : string) : string;
{- force an extension onto a path with a file name }

function StrDefaultExtension(name, ext : string) : string;
{- set a default extension if one is not specified in name }

function StrHasExtension(name : string; var DotPos: word): boolean;
{-Return whether name has ext and position of extension separator dot in a pathname}

{==[ numeric conversion functions  ]===========================================}

function HVal(hstr: string; var code: integer): longint;
{- similar to Val procedure but takes a hex string rather than a decimal
   string }

function HStr2Long(hstr: string): longint;
{- returns a longint representation of the hex string parameter }

function StrHex(Var num; numSize: byte): string;
function HexW(num: Word): string;
function HexB(num: byte): string;
function HexL(num: LongInt): string;

function StrBinary(var num; numSize: byte): string;
function StrBinaryW(num: word): string;
function StrBinaryB(num: byte): string;
function StrBinaryL(num: longint): string;
function StrOctalB(B : byte) : string;

{==[ Tab related function ]====================================================}

(*function StrEntab(S: string; TabSize: byte): string;*)
(*function StrDetab(S: string; TabSize: byte): string;*)

IMPLEMENTATION

{$L strset.obj}
function StrSet(achar: char; aLen: byte): string; external;

{$L commas.obj}
function StrCommas(numStr: string): string; external;

{$L strstrip.obj}
function StrStrip(tstr: string; low, high: char): string; external;

{$L replace.obj}
function StrReplace(tstr: string; srcCh, trgCh: char): string;  external;

procedure StrWordWrap(InSt: string; var OutSt, Overlap: string;
                      margin: byte; PadToMargin: boolean);
{- Wrap InSt at Margin, storing the result in OutSt and the remainder in Overlap}
var
  InStLen   : byte absolute InSt;
  OutStLen  : byte absolute OutSt;
  OvrLen    : byte absolute Overlap;
  EOS, BOS  : word;
begin
  {- find the end of the output string}
  if InStLen > margin then
  begin
    {find the end of the word at the margin, if any}
    EOS:= Margin;
    while (EOS <= InStLen) and (InSt[EOS] <> ' ') do
      Inc(EOS);
    if EOS > InStLen then
      EOS:= InStLen;

    {trim trailing blanks}
    while (InSt[EOS] = ' ') and (EOS > 0) do
      Dec(EOS);

    if EOS > Margin then
    begin
      {look for the space before the current word}
      while (EOS > 0) and (InSt[EOS] <> ' ') do
        Dec(EOS);

      {if EOS = 0 then we can't wrap it}
      if EOS = 0 then
        EOS:= Margin
      else {trim trailing blanks}
        while (InSt[EOS] = ' ') and (EOS > 0) do
          Dec(EOS);
    end;
  end
  else
    EOS:= InStLen;

  {copy the unwrapped portion of the line}
  OutStLen:= EOS;
  Move(InSt[1], OutSt[1], OutStLen);

  {find the start of the next word in the line}
  BOS:= EOS+1;
  while (BOS <= InStLen) and (InSt[BOS] = ' ') do
    Inc(BOS);

  if BOS > InStLen then
    OvrLen:= 0
  else
  begin
    {copy from the start of the next word to the end of the line}
    OvrLen:= Succ(InStLen-BOS);
    Move(InSt[BOS], Overlap[1], OvrLen);
  end;

  {pad the end of the output string if requested}
  if PadToMargin and (OutStLen < Margin) then
  begin
    Fillchar(OutSt[OutStLen+1], Margin-OutStLen, ' ');
    OutStLen:= Margin;
  end;
end;

{$L trim.obj}
function StrTrimTail(tstr: string; trimChar: char): string; external;
function StrTrimLead(tstr: string; trimchar: char): string; external;

function StrTrimAll(tstr: string; trimChar: char): string;
begin
  result:= StrTrimLead(StrTrimTail(tstr, trimChar), trimChar);
end;

function StrTrimTailSpaces(tstr: string): string;
begin
  result:= StrTrimTail(tstr, ' ');
end;

function StrTrimLeadSpaces(tstr: string): string;
begin
  result:= StrTrimLead(tstr, ' ');
end;

function StrTrimAllSpaces(tstr: string): string;
begin
  result:= StrTrimLead(StrTrimTail(tstr, ' '), ' ');
end;

{$L pad.obj}
function StrPadLead(tstr: string; padCh: char; padLen: byte): string; external;
function StrPadTail(tstr: string; padCh: char; padLen: byte): string; external;

function StrPadTailSpaces(tstr: string; padLen: byte): string;
{- pad tstr with spaces }
begin
  result:= StrPadTail(tstr, ' ', padLen);
end;

function StrPadLeadSpaces(tstr: string; padLen: byte): string;
{- pad tstr with spaces }
begin
  result:= StrPadLead(tstr, ' ', padLen);
end;

{$L center.obj}
function StrCenter(tstr: string; centerCh: char; width: byte):string; external;

function StrCenterSpaces(tstr: string; width: byte): string;
begin
  result:= StrCenter(tstr, ' ', width);
end;

procedure StrJustify(jMode: TStrJustify; width: byte; ch: char; var res: string);
begin
  case jMode of
    StrJustLeft   : res:= StrPadTail(StrTrimAll(res, ch), ch, width);
    StrJustCenter : res:= StrCenter(StrTrimAll(res, ch), ch, width);
    StrJustRight  : res:= StrPadLead(StrTrimAll(res, ch), ch, width);
  end;
end;

procedure StrJustifySpaces(jMode: TStrJustify; width: byte; var res: string);
begin
  StrJustify(jMode, width, ' ', res);
end;

function StrRight(st: string; num: byte): string;
{- return rightmost [num] of string }
begin
  result:= Copy(st, Length(st) - num + 1, Length(st));
end;

function StrLeft(st: string; num: byte): string;
begin
  result:= Copy(st, 1, num);
end;

{$L backslsh.obj}
function StrAddBackSlash(dirName: string): string; external;

function StrPackPath(s: string; width: byte): string;
{- Truncate a path to a certain length, Minimum width is 20, drive must be
   included in [s] }
var
  len   : byte absolute s;
  k     : byte;
  num   : byte;
  tstr  : string;
begin
  if len > width then
  begin
    if width < 20 then
      width:= 20;    {- check for min width }
    tstr:= Copy(s, 1, 3) + '...';       {- extract drive }
    s:= Copy(s, len-(width-7), len);    {- cut down path }
    k:= Pos('\', s);
    Delete(s, 1, k-1);                  {- remove portion that doesnt fit }
    result:= tstr + s;
    result:= StrPadTail(tstr + s, ' ', width);
  end
  else
    result:= StrPadTail(s, ' ', width);
end;

{.$L striplst.obj}
(*function StripLast(fname: string): string; external;*)

function StrJustPath(pathName : string) : string;
{- Return just the drive:directory portion of a pathname}
var
  I : word;
begin
  I:= Succ(word(Length(PathName)));
  repeat
   Dec(I);
  until (PathName[I] in [#0, ':', '\']) or (I = 0);

  if I = 0 then   {Had no drive or directory name}
    result[0]:= #0
  else if I = 1 then  {Either the root directory of default drive or invalid pathname}
    result:= PathName[1]
  else if (PathName[I] = '\') then
  begin
    if pathName[Pred(I)] = ':' then
      {Root directory of a drive, leave trailing backslash}
      result:= Copy(PathName, 1, I)
    else
      {Subdirectory, remove the trailing backslash}
      result:= Copy(PathName, 1, Pred(I));
  end
  else  {Either the default directory of a drive or invalid pathname}
    result:= Copy(PathName, 1, I);
end;

function StrJustFile(PathName: string) : string;
{- return just the filename of a pathname}
var
  i  : word;
begin
  i:= Succ(word(Length(PathName)));
  repeat
    Dec(I);
  until (pathName[i] in [#0,':','\']) or (i = 0);
  pathName:= Copy(pathName, Succ(i), 64);
  i:= Pos('.', pathName);
  if i > 0 then
    pathName:= Copy(pathName, 1, Pred(i));
  result:= pathName;
end;

function StrJustExt(PathName: string): string;
{- Return just the extension of a pathname}
var
  i  : word;
begin
  if Pos('.', pathName) > 0 then
  begin
    i:= Succ(word(Length(PathName)));
    repeat
      Dec(I);
    until (pathName[i] in ['.']) or (i = 0);
    result:= Copy(pathName, Succ(i), 64);
  end
  else
    result:= '';
end;

function StrForceExtension(Name, Ext : string) : string;
{- Return a pathname with the specified extension attached}
var
  DotPos : word;
begin
  dotPos:= Pos('.', name);
  if dotPos > 0 then
    result:= Copy(Name, 1, DotPos) + Ext
  else
    result:= Name + '.' + Ext;
end;

function StrHasExtension(Name : string; var DotPos: word): boolean;
{-Return whether name has ext and position of extension separator dot in a pathname}
var
  I : word;
begin
  DotPos:= 0;
  for I:= Length(Name) downto 1 do
    if (Name[I] = '.') and (DotPos = 0) then
      DotPos:= I;
  result:= (DotPos > 0) and (Pos('\', Copy(Name, Succ(DotPos), 64)) = 0);
end;

function StrDefaultExtension(Name, Ext: string) : string;
{- Return a pathname with the specified extension attached }
var
  DotPos : word;
begin
  if StrHasExtension(Name, DotPos) then
    result:= Name
  else
    result:= Name + '.' + Ext;
end;

{$L hex2num}
function HVal(hstr: string; Var code: Integer): LongInt; external;

function HStr2Long(hstr: string): LongInt;
{- return hex string parameter as a longint, return -1 if error }
var
   k  : longint;
   c  : integer;
begin
  k:= HVal(hstr, c);
  if c = 0 then
    result:= k
  else
    result:= -1;
end;

(*function Str2Long(s: string): LongInt;
{- return s as a longint, return -1 if error occurs }
Var
  k    : LongInt;
  c    : Integer;
Begin
  Val(s, k, c);
  If c = 0 Then
    Str2Long:= k
  Else
      Str2Long:= -1;
End;
*)

(*function Long2Str(L : LongInt) : string;
{- Convert a long/word/integer/byte/shortint to a string}
Var
    S : string;
Begin
  Str(L, S);
  Long2Str:= S;
End;
*)

(*function Comp2Str(c : Comp) : string;*)
(*{- Convert a comp to a string}*)
(*Var*)
(*   S : string;*)
(*Begin*)
(*  Str(c:1:0, S);*)
(*  Comp2Str:= S;*)
(*End;*)

(*function Real2Str(r: float; width, places: byte): string;*)
(*{- Convert a real to a string}*)
(*Var*)
(*  s  : string;*)
(*Begin*)
(*  Str(r:width:places, s);*)
(*  Real2Str:= s;*)
(*End;*)

{$L StrHex.obj}
function StrHex(Var num; numSize: byte): string; external;

{$L binstr.obj}
function StrBinary(Var num; numSize: byte): string; external;

function HexB(num: byte): string;
begin
  result:= StrHex(num, SizeOf(byte));
end;

function HexW(num: Word): string;
begin
  result:= StrHex(num, SizeOf(word));
end;

function HexL(num: longint): string;
begin
  result:= StrHex(num, SizeOf(longint));
end;

{---- BINARY ----}

function StrBinaryB(num: byte): string;
begin
  result:= StrBinary(num, SizeOf(byte));
end;

function StrBinaryW(num: word): string;
begin
  result:= StrBinary(num, SizeOf(word));
end;

function StrBinaryL(num: longint): string;
begin
  result:= StrBinary(num, SizeOf(longint));
end;

function StrOctalB(b : byte): string;
{- Return octal string for byte}
const
  Digits : array[0..7] of char = '01234567';
var
  i  : word;
begin
  result[0]:= #3;
  for i:= 0 to 2 do
  begin
    result[3-I]:= digits[b and 7];
    b:= b shr 3;
  end;
end;

{.$L stcase.obj}
(*function StUpCase(tstr: string): string; external;*)
(*function StLoCase(tstr: string): string; external;*)

{.$L tptab.obj}
(*function Entab(S: string; TabSize: byte): string; external;*)
(*function Detab(S: string; TabSize: byte): string; external;*)

END.

{- rcf }
