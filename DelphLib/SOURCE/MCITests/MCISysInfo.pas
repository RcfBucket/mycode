unit MCISysInfo;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, MMSystem, MPlayer, Spin;

type
  TFrmMCISysInfo = class(TForm)
    BtnExecute: TButton;
    BtnClose: TButton;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    lbFlags: TListBox;
    lbDeviceType: TListBox;
    Label4: TLabel;
    mp: TMediaPlayer;
    edResult: TMemo;
    edDWNum: TSpinEdit;
    Label5: TLabel;
    procedure BtnExecuteClick(Sender: TObject);
    procedure BtnCloseClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FrmMCISysInfo: TFrmMCISysInfo;

procedure RunMCISysInfo;

implementation

uses Globals;

{$R *.DFM}

procedure RunMCISysInfo;
begin
  with TFrmMCISysInfo.Create(application) do
  begin
    ShowModal;
    Free;
  end;
end;

procedure TFrmMCISysInfo.BtnExecuteClick(Sender: TObject);
// Execute the MCI_SYSINFO command.
var
  err           : integer;
  sysInfoParams : Tmci_sysinfo_parms;
  dwFlags       : integer;
  tstr          : string;
begin
  mp.Open;
  FillChar(sysInfoParams, sizeof(sysInfoParams), 0);
  try
    case lbDeviceType.ItemIndex of
      0 : sysInfoParams.wDeviceType:= MCI_ALL_DEVICE_ID;
      1 : sysInfoParams.wDeviceType:= MCI_DEVTYPE_ANIMATION;
      2 : sysInfoParams.wDeviceType:= MCI_DEVTYPE_CD_AUDIO;
      3 : sysInfoParams.wDeviceType:= MCI_DEVTYPE_DAT;
      4 : sysInfoParams.wDeviceType:= MCI_DEVTYPE_DIGITAL_VIDEO;
      5 : sysInfoParams.wDeviceType:= MCI_DEVTYPE_OTHER;
      6 : sysInfoParams.wDeviceType:= MCI_DEVTYPE_OVERLAY;
      7 : sysInfoParams.wDeviceType:= MCI_DEVTYPE_SCANNER;
      8 : sysInfoParams.wDeviceType:= MCI_DEVTYPE_VCR;
      9 : sysInfoParams.wDeviceType:= MCI_DEVTYPE_VIDEODISC;
      10: sysInfoParams.wDeviceType:= MCI_DEVTYPE_WAVEFORM_AUDIO;
    end;

    dwFlags:= 0;
    case lbFlags.ItemIndex of
      0 :
        begin
          sysInfoParams.dwRetSize:= 512;
          dwFlags:= MCI_SYSINFO_INSTALLNAME;
        end;
      1 :
        begin
          sysInfoParams.dwRetSize:= 512;
          dwFlags:= MCI_SYSINFO_NAME;
        end;
      2 :
        begin
          sysInfoParams.dwRetSize:= 512;
          dwFlags:= MCI_SYSINFO_OPEN;
        end;
      3 :
        begin
          dwFlags:= MCI_SYSINFO_QUANTITY;
          sysInfoParams.dwRetSize:= 4;
        end;
    end;
    sysInfoParams.dwNumber:= edDWNum.Value;
    if sysInfoParams.dwRetSize > 0 then
      GetMem(sysInfoParams.lpstrReturn, sysInfoParams.dwRetSize);

    err:= MciSendCommand(mp.DeviceID, MCI_SYSINFO, dwFlags, DWORD(@sysInfoParams));
    edResult.Clear;
    if err <> 0 then
    begin
      edResult.Text:= MCIErrorToStr(err);
    end
    else
    begin
      case lbFlags.ItemIndex of
        0 : edResult.Text:= sysInfoParams.lpstrReturn;
        1 : dwFlags:= MCI_SYSINFO_NAME;
        2 : dwFlags:= MCI_SYSINFO_OPEN;
        3 : edResult.Text:= 'Quantity: ' + IntToStr(DWORD(sysInfoParams.lpstrReturn^));
      end;


    end;
  finally
    if sysInfoParams.dwRetSize <> 0 then
      FreeMem(sysInfoParams.lpstrReturn, sysInfoParams.dwRetSize);
    mp.Close;
  end;
end;

procedure TFrmMCISysInfo.BtnCloseClick(Sender: TObject);
begin
  Close;
end;

procedure TFrmMCISysInfo.FormShow(Sender: TObject);
begin
  lbDeviceType.ItemIndex:= 0;
  lbFlags.ItemIndex:= 0;
end;

end.
