unit Globals;

interface

uses
  SysUtils, Windows, mmSystem;

function GetLastErrorString: string;
function MCIErrorToStr(errCode: DWORD): string;


implementation

procedure RealizeLength(var s: string);
begin
  SetLength(s, StrLen(PChar(s)));
end;

function MCIErrorToStr(errCode: DWORD): string;
var
  tstr  : string;
begin
  SetLength(tstr, 1024);
  mciGetErrorString(errCode, PChar(tstr), 1024);
  RealizeLength(tstr);
  result:= tstr;
end;

function GetLastErrorString: string;
// return a string representing the last windows system error
var
  tstr    : string;
  err     : integer;
begin
  err:= GetLastError;
  SetLength(tstr, 1024);
  if FormatMessage(FORMAT_MESSAGE_FROM_SYSTEM,
                nil,
                err,
                0,
                PChar(tstr),
                1024,
                nil) <> 0 then
  begin
    SetLength(tstr, StrLen(PChar(tstr)));
    result:= tstr;
  end
  else
    result:= 'System ID - ' + IntToStr(err);
end;


end.
 