�
 TFORM1 0�  TPF0TForm1Form1Left'ToppWidth@Height-CaptionWindows File API Function Tests
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style OnShowFormShowPixelsPerInch`
TextHeight TLabel	lblTestIDLeft� Top0Width*HeightCaption	lblTestID  TListBoxListBox1LeftTopWidth� Height
Font.ColorclBlackFont.Height�	Font.NameMS Sans Serif
Font.Style 
ItemHeightItems.StringsAreFileApisANSICancelIOCopyFile
CopyFileExCopyProgressRoutineCreateDirectoryCreateDirectoryEx
CreateFileCreateIoCompletionPortDefineDosDevice
DeleteFileFileIOCompletionRoutine	FindCloseFindCloseChangeNotificationFindFirstChangeNotificationFindFirstFileFindFirstFileExFindNextChangeNotificationFindNextFileFlushFileBuffersGetBinaryTypeGetCompressedFileSizeGetCurrentDirectoryGetDiskFreeSpaceGetDiskFreeSpaceExGetDriveTypeGetFileAttributesGetFileInformationByHandleGetFileSizeGetFileTypeGetFullPathNameGetLogicalDrivesGetLogicalDriveStringsGetQueuedCompletionStatusGetShortPathNameGetTempFileNameGetTempPathGetVolumeInformationLockFile
LockFileExMoveFile
MoveFileExPostQueuedCompletionStatusQueryDosDeviceReadDirectoryChangesWReadFile
ReadFileExRemoveDirectory
SearchPathSetCurrentDirectorySetEndOfFileSetFileApisToANSISetFileApisToOEMSetFileAttributesSetFilePointerSetVolumeLabel
UnlockFileUnlockFileEx	WriteFileWriteFileEx  
ParentFontTabOrder   TButtonBtnTestLeft� TopWidthKHeightCaptionTestTabOrderOnClickBtnTestClick  TMemoMemo1Left� Top@WidthIHeight� Lines.StringsMemo1 TabOrder   