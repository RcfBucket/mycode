�
 TFRMLINECOUNT 0	  TPF0TFrmLineCountFrmLineCountLeft9TopnCaption
Line CountClientHeightClientWidthfColor	clBtnFaceConstraints.MinHeight� Constraints.MinWidth�Font.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style OldCreateOrderOnCreate
FormCreate	OnDestroyFormDestroy
DesignSizef PixelsPerInch`
TextHeight TLabelLabel1LeftTopWidthAHeightCaptionStarting folder  TLabelLabel2LeftTop�Width7HeightAnchorsakLeftakBottom CaptionTotal CountExplicitTop�  TLabellblTotalCountLeftPTop�WidthHeightAnchorsakLeftakBottom Caption0Font.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFontExplicitTop�  TtsGridgridLeftTop WidthbHeight�AnchorsakLeftakTopakRightakBottom CheckBoxStylestCheck	ColMovingColsColSelectModecsNoneExportDelimiter,GridMode	gmListBoxHeadingFont.CharsetDEFAULT_CHARSETHeadingFont.ColorclWindowTextHeadingFont.Height�HeadingFont.NameMS Sans SerifHeadingFont.Style HeadingParentFont
ResizeRowsrrNoneRowsRowSelectModersNoneTabOrderThumbTracking	Version3.01.08XMLExport.Version1.0XMLExport.DataPacketVersion2.0OnCellLoadedgridCellLoadedExplicitWidthEExplicitHeight�ColPropertiesDataCol	Col.Width DataCol	Col.Width�  DataCol	Col.Widthb    TButton	BtnBrowseLeftTopWidth<HeightAnchorsakTopakRight Caption&BrowseTabOrderOnClickBtnBrowseClick  TEditedFolderNameLeftXTopWidth�HeightAnchorsakLeftakTopakRight TabOrder TextedFolderNameExplicitWidth�  TButtonBtnGoLeft�Top�WidthKHeightAnchorsakRightakBottom Caption&GoDefault	TabOrderOnClick
BtnGoClickExplicitLeft�ExplicitTop�  TButtonBtnCloseLeftTop�WidthKHeightAnchorsakRightakBottom Cancel	CaptionCloseTabOrderOnClickBtnCloseClickExplicitLeft�ExplicitTop�  	TCheckBox	CheckBox1LeftETop�WidthaHeightAnchorsakRightakBottom CaptionVerbose modeTabOrderExplicitLeft(ExplicitTop�  TStDropFilesdrop
DropTargetOwnerOnDropFilesdropDropFilesLeft Topp  
TStBrowserbrowserCaptionSearch FolderSpecialRootFoldersfNoneLeftXTopp   