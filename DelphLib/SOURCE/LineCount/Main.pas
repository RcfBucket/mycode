unit Main;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Grids_ts, TSGrid, StBase, StDrop, StStrL, StUtils,
  StBrowsr, SsBase;

type
  TFrmLineCount = class(TForm)
    grid: TtsGrid;
    Label1: TLabel;
    BtnBrowse: TButton;
    edFolderName: TEdit;
    drop: TStDropFiles;
    BtnGo: TButton;
    BtnClose: TButton;
    Label2: TLabel;
    lblTotalCount: TLabel;
    CheckBox1: TCheckBox;
    browser: TStBrowser;
    procedure dropDropFiles(Sender: TObject; Point: TPoint);
    procedure BtnCloseClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtnGoClick(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure gridCellLoaded(Sender: TObject; DataCol, DataRow: Integer;
      var Value: Variant);
    procedure BtnBrowseClick(Sender: TObject);
  private
    fFiles  : TStringList;
    function CountEm(fileName: string): integer;
  public
    { Public declarations }
  end;

var
  FrmLineCount: TFrmLineCount;

implementation

{$R *.DFM}

const
  colLine   = 1;
  colName   = 3;
  colFolder = 4;

procedure TFrmLineCount.dropDropFiles(Sender: TObject; Point: TPoint);
begin
  if drop.Count = 1 then
  begin
    edFolderName.Text:= drop.Files[0];
  end;
end;

procedure TFrmLineCount.BtnCloseClick(Sender: TObject);
begin
  Close;
end;

procedure TFrmLineCount.FormCreate(Sender: TObject);
begin
  fFiles:= TStringList.Create;
  edFolderName.Text:= '\LabPro4.0';
  grid.Rows:= 0;

  grid.Col[colLine].Heading:= 'Count';
  grid.Col[colLine].Align:= true;
  grid.Col[colLine].Alignment:= taRightJustify;
  grid.Col[colLine].HeadingAlign:= true;
  grid.Col[colLine].HeadingAlignment:= taRightJustify;
  grid.Col[ColName].Heading:= 'File Name';
  grid.Col[ColFolder].Heading:= 'Folder';
end;

function MyFilter(const SR: TSearchRec; forInclusion: boolean): boolean;
begin
  if forInclusion then
  begin
    if (CompUCStringL(JustExtensionL(SR.Name), 'PAS') = 0) then
      result := True
    else
      result := False;
  end
  else
    result:= true;
end;

function TFrmLineCount.CountEm(fileName: string): integer;
var
  temp  : TStringList;
begin
  temp:= TStringList.Create;
  temp.LoadFromFile(fileName);
  result:= temp.Count;
  temp.Free;
end;

procedure TFrmLineCount.BtnGoClick(Sender: TObject);
var
  k   : integer;
  tot : integer;
begin
  screen.Cursor:= crHourGlass;
  tot:= 0;
  lblTotalCount.Caption:= '0';
  fFiles.Clear;
  grid.Rows:= 0;
  grid.RefreshData(roNone, rpNone);
  try
    EnumerateFiles(edFolderName.text, fFiles, True, MyFilter);
    for k:= 0 to fFiles.Count - 1 do
    begin
      fFiles.Objects[k]:= TObject(CountEm(fFiles[k]));
      tot:= tot + integer(fFiles.Objects[k]);
      if checkBox1.Checked then
      begin
        grid.Rows:= k + 1;
        grid.RefreshData(roNone, rpNone);
        grid.PutCellInView(1, k+1);
      end;
      lblTotalCount.Caption:= CommaizeL(tot);
      lblTotalCount.Refresh;
    end;
  finally
    screen.Cursor:= crDefault;
    grid.Rows:= fFiles.Count;
    grid.RefreshData(roNone, rpNone);
    lblTotalCount.Caption:= CommaizeL(tot);
  end;
end;

procedure TFrmLineCount.FormDestroy(Sender: TObject);
begin
  fFiles.Free;
end;

procedure TFrmLineCount.gridCellLoaded(Sender: TObject; DataCol,
  DataRow: Integer; var Value: Variant);
begin
  case dataCol of
    colLine:
      begin
        value:= CommaizeL(integer(fFiles.Objects[dataRow-1]));
      end;
    colFolder:
      begin
        value:= JustPathNameL(fFiles[dataRow-1])
      end;
    colName:
      value:= JustFileNameL(fFiles[dataRow-1])
  end;
end;


procedure TFrmLineCount.BtnBrowseClick(Sender: TObject);
begin
  if browser.Execute then
    edFolderName.Text:= browser.Path;
end;

end.
