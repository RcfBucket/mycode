unit Main;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, ExtCtrls, PaperView, TwipLib;

type
  TFrmMain = class(TForm)
    pView: TPaperView;
    Panel1: TPanel;
    lblCurY: TLabel;
    lblCurX: TLabel;
    lblScaleX: TLabel;
    lblScaleY: TLabel;
    Label5: TLabel;
    Edit1: TEdit;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Button1: TButton;
    chkSelected: TCheckBox;
    Button2: TButton;
    procedure pViewMouseMove(Sender: TObject; Shift: TShiftState; X,
      Y: Integer);
    procedure pViewMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure pViewPaintPage(sender: TPaperView; aCanvas: TCanvas);
    procedure pViewMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure chkSelectedClick(Sender: TObject);
    procedure Button2Click(Sender: TObject);
  private
    ptAnchor  : TPoint; // anchor point in device units
    ptCur     : TPoint;
    drawObjs  : TList;
  public
    { Public declarations }
  end;

var
  FrmMain: TFrmMain;

implementation

{$R *.DFM}

type
  TDrawShape = class
  private
    edgeOfsX  : integer;
    edgeOfsY  : integer;
    fColor    : TColor;
    fSelected : boolean;
    fCanvas   : TCanvas;  // pointer to a canvas to draw on.
    procedure SetColor(value: TColor);
    procedure SetCanvas(value: TCanvas);
    procedure SetSelected(value: boolean);
  public
    procedure Draw; virtual; abstract;
    property Color: TColor read fColor write SetColor;
    property Canvas: TCanvas read fCanvas write SetCanvas;
    property Selected: boolean read fSelected write SetSelected;
  end;

  TDrawLine = class(TDrawShape)
    endPoint  : TPoint;
    startPoint: TPoint;
    procedure Draw; override;
  end;

procedure TDrawShape.SetColor(value: TColor);
begin
  if fColor <> value then
  begin
    fColor:= value;
    Draw;
  end;
end;

procedure TDrawShape.SetSelected(value: boolean);
begin
  if fSelected <> value then
  begin
    fSelected:= value;
    Draw;
  end;
end;

procedure TDrawShape.SetCanvas(value: TCanvas);
begin
  if fCanvas <> value then
  begin
    fCanvas:= value;
    Draw;
  end;
end;

procedure TDrawLine.Draw;
var
  r   : TRect;
begin
  if fCanvas <> nil then
  begin
    with fCanvas do
    begin
      pen.Color:= fColor;
      pen.Style:= psSolid;
      MoveTo(startPoint.X, startPoint.Y);
      LineTo(endPoint.X, endPoint.y);
      if Selected then
      begin
        r:= rect(startPoint.x,startPoint.y,startPoint.x, startPoint.y);
        InflateRect(r, 4,4);
        brush.Color:= clBlue;
        FillRect(r);
      end;
    end;
  end;
end;

procedure TFrmMain.pViewMouseMove(Sender: TObject; Shift: TShiftState;
  X, Y: Integer);
var
  xx, yy : double;
  pt     : TPoint;
  scale  : double;
begin
  pt:= pView.DevicePointToLTwips(Point(x,y));

  xx:= LTwipsToUnits(pt.x, uInches);
  yy:= LTwipsToUnits(pt.y, uInches);
  lblCurX.caption:= FormatFloat('0.00', xx);
  lblCurY.caption:= FormatFloat('0.00', yy);

  scale:= StrToFloat(edit1.text);

  lblScaleX.caption:= FormatFloat('0.00', xx * scale);
  lblScaleY.caption:= FormatFloat('0.00', yy * scale);

  if GetCaptureControl = pView then
  begin
    with pView.canvas do
    begin
      pen.Color:= clBlack;
      pen.Style:= psSolid;
      pen.Mode:= pmNotXOR;
      MoveTo(ptAnchor.x + pView.edgeX, ptAnchor.y + pView.edgeY);
      LineTo(ptCur.x + pView.edgeX,ptCur.y + pView.edgeY);
      ptCur:= pt;

      MoveTo(ptAnchor.x + pView.edgeX, ptAnchor.y + pView.edgeY);
      LineTo(ptCur.x + pView.edgeX,ptCur.y + pView.edgeY);
    end;
  end;
end;

procedure TFrmMain.pViewMouseDown(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  ptAnchor:= pView.DevicePointToLTwips(Point(x,y));
  ptCur:= ptAnchor;
  SetCaptureControl(pView);
end;

procedure TFrmMain.pViewPaintPage(sender: TPaperView; aCanvas: TCanvas);
var
  k   : integer;
begin
  with aCanvas do
  begin
    pen.Style:= psSolid;
    MoveTo(1440,1440);
    LineTo(LT200, 1440);
    MoveTo(1440, 1440);
    LineTo(1440,LT200);
  end;
  for k:= 0 to drawObjs.Count - 1 do
  begin
    TDrawShape(drawObjs[k]).Canvas:= aCanvas;
    TDrawShape(drawObjs[k]).Draw;
  end;
end;

procedure TFrmMain.pViewMouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
var
  obj   : TDrawLine;
begin
  with pView.canvas do
  begin
    pen.Color:= clBlack;
    pen.Style:= psSolid;
    pen.Mode:= pmNotXOR;
    MoveTo(ptAnchor.x + pView.edgeX, ptAnchor.y + pView.edgeY);
    LineTo(ptCur.x + pView.edgeX,ptCur.y + pView.edgeY);

    ptCur:= pView.DevicePointToLTwips(Point(x,y));
    obj:= TDrawLine.Create;
    obj.Selected:= chkSelected.Checked;
    obj.Canvas:= pView.Canvas;
    obj.startPoint:= ptAnchor;
    obj.endPoint:= ptCur;
    obj.edgeOfsX:= pView.EdgeX;
    obj.edgeOfsY:= pView.EdgeY;
    drawObjs.Add(obj);
    pView.Refresh;
  end;
end;

procedure TFrmMain.FormCreate(Sender: TObject);
begin
  drawObjs:= TList.Create;
end;

procedure TFrmMain.FormDestroy(Sender: TObject);
var
  k   : integer;
begin
  for k:= 0 to drawObjs.Count - 1 do
    TDrawShape(drawObjs[k]).Free;
  drawObjs.Free;
end;

procedure TFrmMain.Button1Click(Sender: TObject);
var
  k   : integer;
begin
  for k:= 0 to drawObjs.Count - 1 do
  begin
    TDrawShape(drawObjs[k]).Free;
  end;
  drawObjs.Clear;
  pView.Invalidate;
end;

procedure TFrmMain.chkSelectedClick(Sender: TObject);
var
  k   : integer;
begin
  for k:= 0 to drawObjs.Count - 1 do
  begin
    TDrawShape(drawObjs[k]).Selected:= chkSelected.Checked;
  end;
end;

procedure TFrmMain.Button2Click(Sender: TObject);
begin
  pView.Invalidate;
end;

end.
