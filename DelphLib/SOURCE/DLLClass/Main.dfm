�
 TDEMOFORM 0,  TPF0	TDemoFormDemoFormLeftpTop� Width�Height,CaptionSpelling Checker Demo
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style MenuMainMenuPosition	poDefaultOnCreate
FormCreate	OnDestroyFormDestroyPixelsPerInch`
TextHeight TMemoMemoLeft Top Width�Height� AlignalClientTabOrder   	TMainMenuMainMenuLeft� Top 	TMenuItemFile1Caption&FileShortCut  	TMenuItemFileNewCaption&NewShortCutN@OnClickFileNewClick  	TMenuItemFileOpenCaption&Open...ShortCutO@OnClickFileOpenClick  	TMenuItem
FileSaveAsCaptionSave &As...ShortCutS@OnClickFileSaveAsClick  	TMenuItemN1Caption-ShortCut   	TMenuItemFileExitCaptionE&xitShortCutQ@OnClickFileExitClick   	TMenuItemEdit1Caption&EditShortCut OnClick
Edit1Click 	TMenuItemEditUndoCaption&UndoShortCutZ@OnClickEditUndoClick  	TMenuItemN3Caption-ShortCut   	TMenuItemEditCutCaptionCu&tShortCutX@OnClickEditCutClick  	TMenuItemEditCopyCaption&CopyShortCutC@OnClickEditCopyClick  	TMenuItem	EditPasteCaption&PasteShortCutV@OnClickEditPasteClick  	TMenuItemN2Caption-ShortCut   	TMenuItemEditCheckSpellingCaptionCheck &Spelling...ShortCutvOnClickEditCheckSpellingClick    TOpenDialog
OpenDialog
DefaultExttxtFileEditStylefsEditFilterText files|*.txt|All files|*.*OptionsofFileMustExist LeftTop  TSaveDialog
SaveDialog
DefaultExttxtFileEditStylefsEditFilterText files|*.txt|All files|*.*OptionsofOverwritePromptofPathMustExist LeftXTop   