program Demo;

uses
  Forms,
  Main in 'Main.pas' {DemoForm},
  Error in 'Error.pas' {SpellDlg};

{$R *.RES}

begin
  Application.Title := 'Spelling Checker Demo';
  Application.CreateForm(TDemoForm, DemoForm);
  Application.CreateForm(TSpellDlg, SpellDlg);
  Application.Run;
end.
