unit Main;

{ Demo program for the spelling checker DLL.
  This is a simple text editor, where the user can select
  a word and check its spelling. In this example, the DLL
  is always loaded, but a real application would be smarter
  about when to load and unload the DLL. }

interface

uses
  WinTypes, WinProcs, Messages, SysUtils, Classes, Graphics, Controls,
  Forms, Dialogs, SpelIntf, SpellChk, StdCtrls, Menus;

type
  TDemoForm = class(TForm)
    MainMenu: TMainMenu;
    File1: TMenuItem;
    FileExit: TMenuItem;
    N1: TMenuItem;
    FileSaveAs: TMenuItem;
    FileOpen: TMenuItem;
    FileNew: TMenuItem;
    Edit1: TMenuItem;
    EditPaste: TMenuItem;
    EditCopy: TMenuItem;
    EditCut: TMenuItem;
    N3: TMenuItem;
    EditUndo: TMenuItem;
    Memo: TMemo;
    OpenDialog: TOpenDialog;
    SaveDialog: TSaveDialog;
    N2: TMenuItem;
    EditCheckSpelling: TMenuItem;
    procedure FileNewClick(Sender: TObject);
    procedure FileOpenClick(Sender: TObject);
    procedure FileSaveAsClick(Sender: TObject);
    procedure FileExitClick(Sender: TObject);
    procedure EditUndoClick(Sender: TObject);
    procedure EditCutClick(Sender: TObject);
    procedure EditCopyClick(Sender: TObject);
    procedure EditPasteClick(Sender: TObject);
    procedure Edit1Click(Sender: TObject);
    procedure EditCheckSpellingClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
  private
    { Reference to the spelling checker interface. }
    fSpellChecker: TISpellChecker;

    { Callback for suggested corrections. }
    procedure SuggestCallback(Word: PChar); export;
        (* See the comment in SpellChk.pas. To allow the spell
           checker class to be used with languages other than Delphi,
           one should declare the callback with the STDCALL calling
           convention, but there is a compiler bug in Delphi 2.0 that
           causes incorrect code generation. Therefore omit the STDCALL
           directive until the bug is fixed.
          {$ifdef WIN32} stdcall; {$endif}
        *)
  end;

var
  DemoForm: TDemoForm;

implementation

{$R *.DFM}

uses Clipbrd, Error;

{ When the form opens, create the spelling checker interface. }
procedure TDemoForm.FormCreate(Sender: TObject);
begin
  fSpellChecker := InitSpellChecker;
  if (fSpellChecker <> nil) and not fSpellChecker.OpenDictionary('TestDict.txt') then
  begin
    DoneSpellChecker(fSpellChecker);
    fSpellChecker := nil;
  end;
end;

{ Unload the spelling checker DLL when the form closes. }
procedure TDemoForm.FormDestroy(Sender: TObject);
begin
  if fSpellChecker <> nil then
    DoneSpellChecker(fSpellChecker);
end;

procedure TDemoForm.FileNewClick(Sender: TObject);
begin
  Memo.Clear
end;

procedure TDemoForm.FileOpenClick(Sender: TObject);
begin
  if OpenDialog.Execute then
  begin
    Screen.Cursor := crHourglass;
    try
      Memo.Lines.LoadFromFile(OpenDialog.Filename);
      Memo.ReadOnly := ofReadOnly in OpenDialog.Options;
    finally
      Screen.Cursor := crDefault;
    end;
  end;
end;

procedure TDemoForm.FileSaveAsClick(Sender: TObject);
begin
  if SaveDialog.Execute then
  begin
    Screen.Cursor := crHourglass;
    try
      Memo.Lines.SaveToFile(SaveDialog.Filename);
    finally
      Screen.Cursor := crDefault;
    end;
  end;
end;

procedure TDemoForm.FileExitClick(Sender: TObject);
begin
  Close
end;

procedure TDemoForm.EditUndoClick(Sender: TObject);
begin
  Memo.Perform(Em_Undo, 0, 0);
end;

procedure TDemoForm.EditCutClick(Sender: TObject);
begin
  Memo.CutToClipboard;
end;

procedure TDemoForm.EditCopyClick(Sender: TObject);
begin
  Memo.CopyToClipboard
end;

procedure TDemoForm.EditPasteClick(Sender: TObject);
begin
  Memo.PasteFromClipboard
end;

procedure TDemoForm.Edit1Click(Sender: TObject);
begin
  EditCut.Enabled := Memo.SelLength > 0;
  EditCopy.Enabled := Memo.SelLength > 0;
  EditPaste.Enabled := Clipboard.HasFormat(Cf_Text);
  EditUndo.Enabled := Memo.Perform(Em_CanUndo, 0, 0) <> 0;
  EditCheckSpelling.Enabled := (Memo.SelLength > 0) and (fSpellChecker <> nil);
end;

{ Add suggested corrections to the suggestion list
  in the error dialog. See the Error unit. }
procedure TDemoForm.SuggestCallback(Word: PChar);
begin
  SpellDlg.SuggestionList.Items.Add(StrPas(Word));
end;

{ Set up a parent/child relationship between forms.
  This keeps the child above the parent, prevents the
  child from appearing on the Win95 task bar in Delphi 1.0. }
procedure SetParentForm(Child, Parent: HWND);
begin
{$ifdef WIN32}
  SetWindowLong(Child, Gwl_HwndParent, Parent);
{$else}
  SetWindowWord(Child, Gww_HwndParent, Parent);
{$endif}
end;

{ Trim non-alphabetic characters. In the Windows character set,
  #192..#255 are alphabetic characters. }
function Trim(const Text: string): string;
var
  I: Integer;
begin
  Result := '';
  for I := 1 to Length(Text) do
    if Text[I] in ['a'..'z', 'A'..'Z', '�'..'�'] then
      AppendStr(Result, Text[I]);
end;

{ Convert a string to a PChar. The spelling checker uses PChars
  in its interface, for maximum portability. Converting a Delphi string
  to a PChar is version-dependent. }
{$ifdef WIN32}
function StrToPChar(var Text: string): PChar;
begin
  Result := PChar(Text)
end;
{$else}
function StrToPChar(var Text: string): PChar;
begin
  { The string is too long, so truncate it. }
  if Length(Text) = 255 then
    Text[0] := #254;
  AppendStr(Text, #0);

  { Fix the length to omit the #0 byte. }
  Text[0] := Chr(Length(Text)-1);

  { Return a pointer to the first character. }
  Result := @Text[1];
end;
{$endif}

{ Check the spelling of the current selection. Trim non-alphabetic characters,
  convert to a PChar, and call the spelling checker. If the word is not in
  the dictionary, then open a modal dialog where the user can
  enter a correction. }
procedure TDemoForm.EditCheckSpellingClick(Sender: TObject);
var
  Word: string;
  SelStart: Integer;
begin
  SpellDlg.SuggestionList.Clear;
  Word := Trim(Memo.SelText);
  if fSpellChecker.CheckSpelling(StrToPChar(Word), SuggestCallback) then
    ShowMessage('Spelling OK')
  else
  begin
    { Display the spelling correction dialog. }
    SpellDlg.WordEdit.Text := Memo.SelText;
    SetParentForm(SpellDlg.Handle, Handle);
    if SpellDlg.ShowModal = mrOK then
    begin
      { Replace the selection with the corrected spelling. }
      SelStart := Memo.SelStart;
      Memo.SelText := SpellDlg.CorrectEdit.Text;
      { Select the newly corrected text. }
      Memo.SelStart := SelStart;
      Memo.SelLength := Length(SpellDlg.CorrectEdit.Text);
      { This could be smarter about preserving spaces & punctuation... }
    end;
  end;
end;

end.
