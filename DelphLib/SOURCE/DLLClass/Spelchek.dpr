library SpelChek;

{ Rudimentary spelling checker, to demonstrate how to use
  classes and DLLs. This DLL implements the TSpellChecker
  class, which is the concrete implementation of the interface
  class, TISpellChecker. This implementation allows for multiple
  language-specific spelling checkers, although only TEnglishSpellChecker
  is implemented. A language-specific spell checker can have
  hard-coded rules for plurals, verb conjugation, etc.

  An application uses the TISpellChecker interface to use
  the spelling checker. See the SpellChk unit for more information.

  Remember that this is a demonstration of DLLs, not spelling
  checkers. The algorithms used here are necessarily simple, and
  are not meant to represent a real spelling checker.
}
uses
  SysUtils, Classes, SpellChk;

type
  { Keep the list of words in a TDictionary. }
  TDictionary = class(TStringList)
  public
    constructor Create;
    function Add(const S: string): Integer; override;
  end;

  TSpellChecker = class(TISpellChecker)
  private
    fWordList: TDictionary;
  protected
    constructor Create;
    procedure LookupSuggestions(const Word: string; Suggest: TSuggestProc);
    function IsSimilar(const TestWord, DictionaryWord: string): Boolean;
    property WordList: TDictionary read fWordList;
  public
    destructor Destroy; override;
    function OpenDictionary(Path: PChar): Boolean; override;
    procedure ClearDictionary; override;
    function CheckSpelling(Word: PChar; Suggest: TSuggestProc): Boolean; override;
    function GetVersion: Integer; override;
    function IsEnglishSpellChecker: Boolean; override;
    function IsFrenchSpellChecker: Boolean; override;
    function IsGermanSpellChecker: Boolean; override;
  end;

  TEnglishSpellChecker = class(TSpellChecker)
  public
    function IsEnglishSpellChecker: Boolean; override;
    function CheckSpelling(Word: PChar; Suggest: TSuggestProc): Boolean; override;
  end;


{ The application calls InitSpellChecker to create and initialize
  the spelling checker object. The application frees the object
  when it is done using it. }
function InitSpellChecker: TISpellChecker; export;
{$ifdef WIN32}stdcall;{$endif}
begin
  try
    Result := TSpellChecker.Create;
  except
    Result := nil;
  end;
end;


{ A dictionary is a sorted string list that ignores duplicates,
  and does some extra conversions when adding a string to the list. }
constructor TDictionary.Create;
begin
  inherited Create;
  Sorted := True;
  Duplicates := dupIgnore;
end;

{ Convert a string to a PChar. }
{$ifdef WIN32}
function StrToPChar(const Str: string): PChar;
begin
  Result := PChar(Str);
end;
{$else}

var
  PCharStr: array[0..255] of Char;
function StrToPChar(const Str: string): PChar;
begin
  StrPLCopy(PCharStr, Str, SizeOf(PCharStr));
  Result := PCharStr;
end;
{$endif}

{ When adding a string to a dictionary, make it all lower case. }
function TDictionary.Add(const S: string): Integer;
begin
  Result := inherited Add(AnsiLowerCase(S))
end;



{ Create the word list. }
constructor TSpellChecker.Create;
begin
  inherited Create;
  fWordList := TDictionary.Create;
end;

{ Free the word list. }
destructor TSpellChecker.Destroy;
begin
  try
    fWordList.Free;
    inherited Destroy;
  except
    { Catch exceptions in the DLL, to prevent the application
      from crashing. }
  end;
end;

{ Return the version of the spelling checker interface that
  this DLL implements. The version is hardcoded in the
  SpelChek unit. When the TISpellChecker class changes,
  the ExpectedVersion is incremented. Recompiling this
  unit builds a DLL that matches the interface, but if the user
  accidentally loads the wrong DLL, then the version number
  returned by GetVersion does not match the expected version. }
function TSpellChecker.GetVersion: Integer;
begin
  Result := ExpectedVersion;
end;

{ Open a dictionary and load its word list. A dictionary file
  is a text file with one word per line. Use a temporary string
  list for loading the file, to add the strings to the list.
  This lets the user load multiple word lists (e.g., standard
  plus custom dictionaries). }
function TSpellChecker.OpenDictionary(Path: PChar): Boolean;
var
  Words: TStrings;
begin
  try
    Words := TStringList.Create;
    try
      Words.LoadFromFile(StrPas(Path));
      WordList.AddStrings(Words);
    finally
      Words.Free;
    end;
    Result := True;
  except
    Result := False;
  end;
end;

{ Clear the dictionary, usually just before reloading
  a standard dictionary. }
procedure TSpellChecker.ClearDictionary;
begin
  fWordList.Clear
end;

{ The base class returns False for all language checks.
  That way, a language-specific base class needs to override
  only one method, to return True. }
function TSpellChecker.IsEnglishSpellChecker: Boolean;
begin
  Result := False
end;

function TSpellChecker.IsFrenchSpellChecker: Boolean;
begin
  Result := False
end;

function TSpellChecker.IsGermanSpellChecker: Boolean;
begin
  Result := False
end;

{ Check the spelling of a single word. Return True if the word
  is in the dictionary, False if not. If the word is not in the
  dictionary, then call the Suggest callback once for each suggested
  correction. The suggested words are all the words in the dictionary
  that are similar to Word. }
function TSpellChecker.CheckSpelling(Word: PChar; Suggest: TSuggestProc): Boolean;
var
  WordStr: string;
begin
  try
    WordStr := AnsiLowerCase(StrPas(Word));
    if WordList.IndexOf(WordStr) >= 0 then
      Result := True
    else
    begin
      Result := False;
      LookupSuggestions(WordStr, Suggest);
    end;
  except
    Result := False;
  end;
end;

{ Lookup suggestions for words that are similar to Word, and pass
  them to Suggest. For portability to other languages, pass the
  word as a PChar, not a Delphi string. }
procedure TSpellChecker.LookupSuggestions(const Word: string; Suggest: TSuggestProc);
var
  I: Integer;
  Str: string;
begin
  for I := 0 to WordList.Count-1 do
    if IsSimilar(Word, WordList[I]) then
    begin
      Str := WordList[I];
      Suggest(StrToPChar(Str));
    end;
end;

{ Extremely crude test for word similarity. The words must be
  the same length, and must differ by only one or two characters. }
function TSpellChecker.IsSimilar(const TestWord, DictionaryWord: string): Boolean;
var
  I: Integer;
  NumDifferences: 0..3;
begin
  Result := False;
  if Length(TestWord) = Length(DictionaryWord) then
  begin
    NumDifferences := 0;
    for I := 1 to Length(TestWord) do
      if TestWord[I] <> DictionaryWord[I] then
      begin
        Inc(NumDifferences);
        if NumDifferences > 2 then
          Exit;
      end;
    Result := True;
  end;
end;


{ TEnglishSpellChecker }
function TEnglishSpellChecker.IsEnglishSpellChecker: Boolean;
begin
  Result := True
end;

{ This is even cruder, demonstrating one way to implement a
  language-specific spelling checker. This routine strips a trailing
  "s" from the word and tests it. The rationale is to change a plural
  into a singular, although this is obviously a simple-minded algorithm.
  Remember that this is a demonstration of classes and DLLs, not
  good ways to do spelling checking. }
function TEnglishSpellChecker.CheckSpelling(Word: PChar; Suggest: TSuggestProc): Boolean;
var
  Singular: PChar;
  Len: Integer;
begin
  if not inherited CheckSpelling(Word, Suggest) then
  begin
    Len := StrLen(Word);
    if (Len > 0) and (Word[Len-1] in ['s', 'S']) then
    begin
      { Make a copy of Word, stripping the trailing 's'. }
      Singular := StrNew(Word);
      try
        Dec(Len);
        Singular[Len] := #0;
        Result := CheckSpelling(Singular, Suggest);
      finally
        StrDispose(Singular);
      end;
    end;
  end;
end;

exports
  InitSpellChecker name InitSpellCheckerName;

begin
end.
