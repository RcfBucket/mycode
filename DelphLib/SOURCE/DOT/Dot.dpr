program DOT;

uses
  Forms,
  Main in 'MAIN.PAS' {MainForm},
  Options in 'OPTIONS.PAS' {frmOptions},
  Props in 'PROPS.PAS' {frmProperties},
  Sortopt in 'SORTOPT.PAS' {FrmSortOptions},
  Filewin in 'FILEWIN.PAS' {FrmFileWin},
  Dottools in 'DOTTOOLS.PAS',
  DStrs in 'DSTRS.PAS',
  Dotdata in 'DOTDATA.PAS',
  About in 'ABOUT.PAS' {frmAbout},
  Dotmask in 'DOTMASK.PAS',
  Testfrm in 'TESTFRM.PAS' {frmTest},
  Dotio in 'DOTIO.PAS';

{$R *.RES}

begin
  Application.Title := 'Disk-O-Tech';
  Application.CreateForm(TMainForm, MainForm);
  Application.CreateForm(TfrmTest, frmTest);
  Application.Run;
end.
