unit Testfrm;

interface

uses WinTypes, WinProcs, Classes, Graphics, Forms, Controls, Buttons,
  StdCtrls, ExtCtrls, DStrs, dotmask;

type
  TfrmTest = class(TForm)
    OKBtn: TBitBtn;
    Bevel1: TBevel;
    Edit1: TEdit;
    Button1: TButton;
    Button2: TButton;
    Button3: TButton;
    lblResult: TLabel;
    Label2: TLabel;
    Edit2: TEdit;
    Label1: TLabel;
    Label3: TLabel;
    procedure OKBtnClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmTest: TfrmTest;

implementation

{$R *.DFM}

procedure TfrmTest.OKBtnClick(Sender: TObject);
begin
  close;
end;

end.
