unit Unit1;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Menus, Grids;

type
  TMyListBox = class(TListBox)
    procedure WndProc(var msg: TMessage); override;
  end;

  TForm1 = class(TForm)
    Button1: TButton;
    Button2: TButton;
    Edit1: TEdit;
    Button3: TButton;
    StringGrid1: TStringGrid;
    procedure Button1Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure Button3Click(Sender: TObject);
  private
    listBox1  : TMyListBox;
    procedure WndProc(var msg: TMessage); override;
  public
    { Public declarations }
  end;

var
  Form1   : TForm1;
  count   : integer;

implementation

{$R *.DFM}

function HandledStr(msg: TMessage): string;
begin
  if msg.result = 0 then
    result:= ''
  else
    result:= '  Handled';
end;

function KeyStr(key: word): string;
begin
  result:= ShortCutToText(ShortCut(key, []));
end;

function Counter: string;
begin
  Inc(count);
  result:= FormatFloat('00000  ', count);
end;

procedure TMyListBox.WndProc(var msg: TMessage);
var
  tstr  : string;
begin
  tstr:= KeyStr(msg.wParam);
  if msg.msg = WM_KEYDOWN then
    Items.Insert(0, Counter +  '  LISTBOX.WNDPROC Enter - WM_KEYDOWN   [' + tstr + ']' + HandledStr(msg));
  if msg.msg = CN_KEYDOWN then
    Items.Insert(0, Counter +  '  LISTBOX.WNDPROC Enter - CN_KEYDOWN   [' + tstr + ']' + HandledStr(msg));
  if msg.msg = CN_CHAR then
    Items.Insert(0, Counter +  '  LISTBOX.WNDPROC Enter - CN_CHAR   [' + tstr + ']' + HandledStr(msg));
  if msg.msg = CM_DIALOGKEY then
    Items.Insert(0, Counter +  '  LISTBOX.WNDPROC Enter - CM_DIALOGKEY   [' + tstr + ']' + HandledStr(msg));
  if msg.msg = CM_DIALOGCHAR then
    Items.Insert(0, Counter +  '  LISTBOX.WNDPROC Enter - CM_DIALOGCHAR   [' + tstr + ']' + HandledStr(msg));
  if msg.msg = CM_CHILDKEY then
    Items.Insert(0, Counter +  '  LISTBOX.WNDPROC Enter - CM_CHILDKEY   [' + tstr + ']' + HandledStr(msg));
  if msg.msg = CM_WANTSPECIALKEY then
    Items.Insert(0, Counter +  '  LISTBOX.WNDPROC Enter - CM_WANTSPECIALKEY   [' + tstr + ']' + HandledStr(msg));
  if msg.msg = WM_GETDLGCODE then
    Items.Insert(0, Counter +  '  LISTBOX.WNDPROC Enter - WM_GETDLGCODE   [' + tstr + ']' + HandledStr(msg));
  if msg.msg = CN_KEYDOWN then
    items.Insert(0, Counter +  'LISTBOX.WNDPROC Enter - CN_KEYDOWN [' + tstr + ']' + HandledStr(msg));

  inherited;
{  if msg.result <> 0 then
    exit;}
  if msg.msg = WM_KEYDOWN then
    Items.Insert(0, Counter +  '  LISTBOX.WNDPROC Exit - WM_KEYDOWN   [' + tstr + ']' + HandledStr(msg));
  if msg.msg = CN_KEYDOWN then
    Items.Insert(0, Counter +  '  LISTBOX.WNDPROC Exit - CN_KEYDOWN   [' + tstr + ']' + HandledStr(msg));
  if msg.msg = CN_CHAR then
    Items.Insert(0, Counter +  '  LISTBOX.WNDPROC Exit - CN_CHAR   [' + tstr + ']' + HandledStr(msg));
  if msg.msg = CM_DIALOGKEY then
    Items.Insert(0, Counter +  '  LISTBOX.WNDPROC Exit - CM_DIALOGKEY   [' + tstr + ']' + HandledStr(msg));
  if msg.msg = CM_DIALOGCHAR then
    Items.Insert(0, Counter +  '  LISTBOX.WNDPROC Exit - CM_DIALOGCHAR   [' + tstr + ']' + HandledStr(msg));
  if msg.msg = CM_CHILDKEY then
    Items.Insert(0, Counter +  '  LISTBOX.WNDPROC Exit - CM_CHILDKEY   [' + tstr + ']' + HandledStr(msg));
  if msg.msg = CM_WANTSPECIALKEY then
    Items.Insert(0, Counter +  '  LISTBOX.WNDPROC Exit - CM_WANTSPECIALKEY   [' + tstr + ']' + HandledStr(msg));
  if msg.msg = WM_GETDLGCODE then
    Items.Insert(0, Counter +  '  LISTBOX.WNDPROC Exit - WM_GETDLGCODE   [' + tstr + ']' + HandledStr(msg));
  if msg.msg = CN_KEYDOWN then
    items.Insert(0, Counter +  'LISTBOX.WNDPROC Enter - CN_KEYDOWN [' + tstr + ']' + HandledStr(msg));
end;

procedure TForm1.WndProc(var msg: TMessage);
var
  tstr  : string;
begin
  tstr:= KeyStr(msg.wParam);
  if msg.msg = WM_KEYDOWN then
    listbox1.items.Insert(0, Counter +  'FORM.WNDPROC Enter - WM_KEYDOWN   [' + tstr + ']' + HandledStr(msg));
  if msg.msg = CN_KEYDOWN then
    listbox1.items.Insert(0, Counter +  'FORM.WNDPROC Enter - CN_KEYDOWN   [' + tstr + ']' + HandledStr(msg));
  if msg.msg = CN_CHAR then
    listbox1.items.Insert(0, Counter +  'FORM.WNDPROC Enter - CN_CHAR   [' + tstr + ']' + HandledStr(msg));
  if msg.msg = CM_DIALOGKEY then
    listbox1.items.Insert(0, Counter +  'FORM.WNDPROC Enter - CM_DIALOGKEY   [' + tstr + ']' + HandledStr(msg));
  if msg.msg = CM_DIALOGCHAR then
    listbox1.items.Insert(0, Counter +  'FORM.WNDPROC Enter - CM_DIALOGCHAR   [' + tstr + ']' + HandledStr(msg));
  if msg.msg = CM_CHILDKEY then
    listbox1.items.Insert(0, Counter +  'FORM.WNDPROC Enter - CM_CHILDKEY   [' + tstr + ']' + HandledStr(msg));
  if msg.msg = CM_WANTSPECIALKEY then
    listbox1.items.Insert(0, Counter +  'FORM.WNDPROC Enter - CM_WANTSPECIALKEY   [' + tstr + ']' + HandledStr(msg));
  if msg.msg = WM_GETDLGCODE then
    listbox1.items.Insert(0, Counter +  'FORM.WNDPROC Enter - WM_GETDLGCODE   [' + tstr + ']' + HandledStr(msg));
  if msg.msg = CN_KEYDOWN then
    listbox1.items.Insert(0, Counter +  'FORM.WNDPROC Enter - CN_KEYDOWN [' + tstr + ']' + HandledStr(msg));

  inherited;
{  if msg.result <> 0 then
    exit;}
  if msg.msg = WM_KEYDOWN then
    listbox1.items.Insert(0, Counter +  'FORM.WNDPROC Exit - WM_KEYDOWN   [' + tstr + ']' + HandledStr(msg));
  if msg.msg = CN_KEYDOWN then
    listbox1.items.Insert(0, Counter +  'FORM.WNDPROC Exit - CN_KEYDOWN   [' + tstr + ']' + HandledStr(msg));
  if msg.msg = CN_CHAR then
    listbox1.items.Insert(0, Counter +  'FORM.WNDPROC Exit - CN_CHAR   [' + tstr + ']' + HandledStr(msg));
  if msg.msg = CM_DIALOGKEY then
    listbox1.items.Insert(0, Counter +  'FORM.WNDPROC Exit - CM_DIALOGKEY   [' + tstr + ']' + HandledStr(msg));
  if msg.msg = CM_DIALOGCHAR then
    listbox1.items.Insert(0, Counter +  'FORM.WNDPROC Exit - CM_DIALOGCHAR   [' + tstr + ']' + HandledStr(msg));
  if msg.msg = CM_CHILDKEY then
    listbox1.items.Insert(0, Counter +  'FORM.WNDPROC Exit - CM_CHILDKEY   [' + tstr + ']' + HandledStr(msg));
  if msg.msg = CM_WANTSPECIALKEY then
    listbox1.items.Insert(0, Counter +  'FORM.WNDPROC Exit - CM_WANTSPECIALKEY   [' + tstr + ']' + HandledStr(msg));
  if msg.msg = WM_GETDLGCODE then
    listbox1.items.Insert(0, Counter +  'FORM.WNDPROC Exit - WM_GETDLGCODE   [' + tstr + ']' + HandledStr(msg));
  if msg.msg = CN_KEYDOWN then
    listbox1.items.Insert(0, Counter +  'FORM.WNDPROC Enter - CN_KEYDOWN [' + tstr + ']' + HandledStr(msg));
  if (msg.msg = CM_CHILDKEY) and (msg.wParam = VK_RETURN) then
    msg.result:= 1;
end;

procedure TForm1.Button1Click(Sender: TObject);
begin
  count:= 0;
  listbox1.Clear;
end;

procedure TForm1.FormCreate(Sender: TObject);
begin
  listBox1:= TMyListBox.Create(self);
  with listbox1 do
  begin
    left:= 8;
    top:= 8;
    width:= 537;
    height:= 193;
//    align:= alLeft;
    parent:= self;
  end;
end;

procedure TForm1.Button2Click(Sender: TObject);
begin
  Close;
end;

procedure TForm1.Button3Click(Sender: TObject);
begin
  listbox1.Items.Insert(0, '---------------------');
end;

end.
