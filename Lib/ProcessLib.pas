unit ProcessLib;
// Copied from LP4.0 Inc 27

interface

uses
  Windows, SysUtils, Classes, PsAPI, TlHelp32, Globals;

type
  // Note: If reading output (ReadOutputBuffer) and the program has input (eg, readlns) then
  // TProcess will not work.  Still trying to find a solution to this 10/2/2007
  TProcess = class
  private
    fName: string; // name of process,  eg "notepad.exe"
    fCmdLine: string; // command line to pass to process
    fWaitForComplete: boolean; // wait for app to complete on Execute (true=sync, false=async)
    fReadOutput: boolean; // read standard output into buffer
    fShowMainWin: boolean; // show/hide main window
    fOutBuf: TStringList; // StdOut buffer
    fErrBuf: TStringList; // StdErr buffer
    pInfo: TProcessInformation;
    fErrorCode: integer;
    fWorkingFolder: string;

    function GetErrorCode: integer;
    procedure SetReadOutput(const Value: boolean);
    procedure SetWaitForComplete(const Value: boolean);
  public
    constructor Create;
    destructor Destroy; override;
    procedure Execute;
    procedure Clear;
    procedure Terminate;
    function WaitFor(aTime: cardinal = INFINITE): integer;
    function IsRunning: boolean;
    property ErrorCode: integer read GetErrorCode;
    property name: string read fName write fName; // name of process to run (eg, foo.exe)
    property CommandLine: string read fCmdLine write fCmdLine;
    property WorkingFolder: string read fWorkingFolder write fWorkingFolder;
    property WaitForCompletion: boolean read fWaitForComplete write SetWaitForComplete;
    property ShowMainWindow: boolean read fShowMainWin write fShowMainWin;
    property ReadOuputBuffer: boolean read fReadOutput write SetReadOutput;
    property StdOutBuffer: TStringList read fOutBuf;
    property StdErrBuffer: TStringList read fErrBuf;
  end;

  // Find and kill the process
  // by Andreano Lanusse from CDN, Code Central
function FindProcesses(aKey: string; aList: TList = nil): integer;
function KillProcess(aProcessId: cardinal): boolean;

procedure FillProcessList(aStringList: TStrings);

implementation

{ TProcess }

constructor TProcess.Create;
begin
  fOutBuf:= TStringList.Create;
  fErrBuf:= TStringList.Create;
  fWaitForComplete:= true;
  fReadOutput:= false;
  fShowMainWin:= true;
  fErrorCode:= 0;
  FillChar(pInfo, SizeOf(pInfo), 0);
end;

destructor TProcess.Destroy;
begin
  Clear;
  fOutBuf.Free;
  fErrBuf.Free;
  inherited;
end;

procedure TProcess.Execute;
// Execute the process
var
  startInfo: TStartupInfo;
  SecurityAttr: TSecurityAttributes;
  PipeStdOutRead: THandle;
  PipeStdOutWrite: THandle;
  PipeStdErrRead: THandle;
  PipeStdErrWrite: THandle;
  Buffer: string;
  outStr: string;
  res: longbool;
  PipeBytesRead: cardinal;
  totalBytesAvail, BytesLeft: cardinal;
  bufSize: cardinal;
  param: string;
  done: boolean;
  origFolder: string;
begin
  if IsRunning then
    raise Exception.Create('Process is currently running');

  if fWorkingFolder <> '' then
  begin
    origFolder:= GetCurrentDir;
    SetCurrentDir(fWorkingFolder);
  end
  else
    origFolder:= '';

  fErrorCode:= 0;
  fOutBuf.Clear;
  fErrBuf.Clear;
  PipeStdOutRead:= 0;
  PipeStdOutWrite:= 0;
  PipeStdErrRead:= 0;
  PipeStdErrWrite:= 0;

  if (fName <> '') then
  begin
    FillChar(startInfo, SizeOf(startInfo), 0);
    startInfo.cb:= SizeOf(startInfo);

    SecurityAttr.nLength:= SizeOf(SecurityAttr);
    SecurityAttr.lpSecurityDescriptor:= nil;
    SecurityAttr.bInheritHandle:= true;

    if fReadOutput then
    begin
      if not CreatePipe(PipeStdOutRead, PipeStdOutWrite, @SecurityAttr, 0) then
        raise Exception.Create('Creating StdOut pipe failed: '#10#13 + GetLastErrorString);
      if not CreatePipe(PipeStdErrRead, PipeStdErrWrite, @SecurityAttr, 0) then
        raise Exception.Create('Creating StdOut pipe failed: '#10#13 + GetLastErrorString);
    end;
    try
      // set up standard handles so I can read the output buffer of the process
      if fReadOutput then
      begin
        startInfo.hStdInput:= GetStdHandle(STD_INPUT_HANDLE);
        startInfo.hStdOutput:= PipeStdOutWrite;
        startInfo.hStdError:= PipeStdErrWrite;
        startInfo.dwFlags:= STARTF_USESTDHANDLES;
      end;

      if not fShowMainWin then
      begin
        startInfo.dwFlags:= startInfo.dwFlags or STARTF_USESHOWWINDOW;
        startInfo.wShowWindow:= SW_HIDE;
      end;

      param:= AnsiQuotedStr(fName, '"') + ' ' + fCmdLine;
      if CreateProcess(PChar(fName), PChar(param), nil, nil, true, NORMAL_PRIORITY_CLASS, nil, nil, startInfo, pInfo) then
      begin
        if fWaitForComplete then
        begin
          done:= false;
          while not done do
          begin
            if WaitFor(250) = WAIT_OBJECT_0 then
              done:= true;

            if fReadOutput then
            begin
              bufSize:= 4096;
              SetLength(Buffer, bufSize);
              // read StdOut pipe
              if PeekNamedPipe(PipeStdOutRead, PChar(Buffer), bufSize, @PipeBytesRead, @totalBytesAvail, @BytesLeft) then
              begin
                if totalBytesAvail > 0 then
                begin
                  outStr:= '';
                  repeat
                    res:= ReadFile(PipeStdOutRead, Buffer[1], bufSize, PipeBytesRead, nil);
                    if not res then
                      outStr:= outStr + '-ERRORHERE-'
                    else
                    begin
                      SetLength(Buffer, PipeBytesRead);
                      outStr:= outStr + Buffer;
                      SetLength(Buffer, bufSize);
                    end;
                    PeekNamedPipe(PipeStdOutRead, PChar(Buffer), bufSize, @PipeBytesRead, @totalBytesAvail, @BytesLeft);
                  until totalBytesAvail <= 0;
                  fOutBuf.Text:= fOutBuf.Text + outStr;
                end;
              end;

              // read StdErr pipe
              SetLength(Buffer, bufSize);
              if PeekNamedPipe(PipeStdErrRead, PChar(Buffer), bufSize, @PipeBytesRead, @totalBytesAvail, @BytesLeft) then
              begin
                if totalBytesAvail > 0 then
                begin
                  outStr:= '';
                  repeat
                    res:= ReadFile(PipeStdErrRead, Buffer[1], bufSize, PipeBytesRead, nil);
                    if not res then
                      outStr:= outStr + '-ERRORHERE-'
                    else
                    begin
                      SetLength(Buffer, PipeBytesRead);
                      outStr:= outStr + Buffer;
                      SetLength(Buffer, bufSize); // reset size
                    end;
                    PeekNamedPipe(PipeStdErrRead, PChar(Buffer), bufSize, @PipeBytesRead, @totalBytesAvail, @BytesLeft);
                  until totalBytesAvail <= 0;
                  fErrBuf.Text:= fErrBuf.Text + outStr;
                end;
              end;
            end;
          end;
        end;

        if fWaitForComplete then
          GetExitCodeProcess(pInfo.hProcess, cardinal(fErrorCode));
      end
      else
      begin
        raise Exception.Create('Failed: ' + GetLastErrorString);
      end;
    finally
      if fReadOutput then
      begin
        CloseHandle(PipeStdOutRead);
        CloseHandle(PipeStdErrRead);
        CloseHandle(PipeStdOutWrite);
        CloseHandle(PipeStdErrWrite);
      end;
    end;

    if fWaitForComplete then
    begin
      Clear;
    end;
  end;

  if origFolder <> '' then
    SetCurrentDir(origFolder);
end;

procedure TProcess.Clear;
begin
  if pInfo.hProcess <> 0 then
  begin
    CloseHandle(pInfo.hThread);
    CloseHandle(pInfo.hProcess);
  end;
  pInfo.hProcess:= 0;
  pInfo.hProcess:= 0;
end;

function TProcess.GetErrorCode: integer;
var
  res: longbool;
begin
  if pInfo.hProcess <> 0 then
  begin
    res:= GetExitCodeProcess(pInfo.hProcess, cardinal(result));
    if not res then
      raise Exception.Create(GetLastErrorString)
    else if result = STILL_ACTIVE then
      raise Exception.Create('Application Still Active');
  end
  else
    result:= fErrorCode;
end;

procedure TProcess.Terminate;
// terminate the running process
begin
  if pInfo.hProcess > 0 then
    TerminateProcess(pInfo.hProcess, 0);
end;

function TProcess.IsRunning: boolean;
var
  res: integer;
begin
  if pInfo.hProcess <> 0 then
  begin
    res:= WaitForSingleObject(pInfo.hProcess, 0);
    if res = WAIT_TIMEOUT then
      result:= true
    else
      result:= false;
  end
  else
    result:= false;
end;

procedure TProcess.SetReadOutput(const Value: boolean);
// if reading output, we must also wait for output
begin
  if Value then
    fWaitForComplete:= true;
  fReadOutput:= Value;
end;

procedure TProcess.SetWaitForComplete(const Value: boolean);
// if reading output, then waitforcomplete must always be true
begin
  if fReadOutput then
    fWaitForComplete:= true
  else
    fWaitForComplete:= Value;
end;

function TProcess.WaitFor(aTime: cardinal = INFINITE): integer;
// wait for process to complete
begin
  if pInfo.hProcess <> 0 then
    result:= WaitForSingleObject(pInfo.hProcess, aTime)
  else
    result:= -1;
end;

function FindProcesses(aKey: string; aList: TList = nil): integer;
var
  szProcessName: array [0..1024] of char;
  ProcessName: string;
  hProcess: integer;
  aProcesses: array [0..1024] of DWORD;
  cProcesses: DWORD;
  cbNeeded: cardinal;
  i: UINT;
  hMod: HMODULE;
begin
  result:= 0;
  aKey:= LowerCase(aKey);

  if not(EnumProcesses(@aProcesses, SizeOf(aProcesses), cbNeeded)) then
    exit;

  // Calculate how many process identifiers were returned.
  cProcesses:= cbNeeded div SizeOf(DWORD);

  // Print the name and process identifier for each process.
  for i:= 0 to cProcesses - 1 do
  begin
    szProcessName:= 'unknown';
    hProcess:= OpenProcess(PROCESS_QUERY_INFORMATION or PROCESS_VM_READ, false, aProcesses[i]);

    // Get the process name.
    if (hProcess <> 0) then
      if (EnumProcessModules(hProcess, @hMod, SizeOf(hMod), cbNeeded)) then
        GetModuleBaseName(hProcess, hMod, szProcessName, SizeOf(szProcessName));

    ProcessName:= LowerCase(szProcessName);

    CloseHandle(hProcess);
    if pos(aKey, ProcessName) <> 0 then
    begin
      result:= aProcesses[i];
      if aList <> nil then
        aList.Add(Pointer(aProcesses[i]))
      else
        exit;
    end;
  end;
end;

procedure FillProcessList(aStringList: TStrings);
var
  tmpHandle: THandle;
  tmpProcess: TProcessEntry32;
  tmpString: string;
begin
  tmpHandle:= CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, 0);

  if tmpHandle <> longword(-1) then
    try
      FillMemory(Pointer(@tmpProcess), SizeOf(tmpProcess), 0);
      tmpProcess.dwSize:= SizeOf(tmpProcess);
      if Process32First(tmpHandle, tmpProcess) then
      begin
        tmpString:= tmpProcess.szExeFile;
        if tmpString <> '' then
          aStringList.Add(tmpString);

        FillMemory(Pointer(@tmpProcess), SizeOf(tmpProcess), 0);
        tmpProcess.dwSize:= SizeOf(tmpProcess);
        while Process32Next(tmpHandle, tmpProcess) do
        begin
          tmpString:= tmpProcess.szExeFile;
          if tmpString <> '' then
            aStringList.Add(tmpString);
          FillMemory(Pointer(@tmpProcess), SizeOf(tmpProcess), 0);
          tmpProcess.dwSize:= SizeOf(tmpProcess);
        end;
      end;
    finally
      CloseHandle(tmpHandle);
    end;
end;

function KillProcess(aProcessId: cardinal): boolean;
var
  hProcess: integer;
begin

  hProcess:= OpenProcess(PROCESS_ALL_ACCESS, true, aProcessId);
  result:= false;

  if (hProcess <> 0) then
  begin
    result:= TerminateProcess(hProcess, 0);
    exit;
  end;
end;

end.
