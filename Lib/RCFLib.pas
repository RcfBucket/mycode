unit RCFLib;

// library of useful functions

interface

uses
  Windows, SysUtils;

type
  TVersionInfo = record
    Major: integer;
    Minor: integer;
    IncrementRelease: integer;
    Build: integer;

    VersionStr: string;
    IncrementReleaseStr: string;
    BuildStr: string;

    FileDate: TDateTime;

    ProductName: string;
    Comments: string;
    CompanyName: string;
    FileDescription: string;
    LegalCopyright: string;
    FullVersionStr: string;
  end;

function GetVersionInfo(aFilename: string): TVersionInfo;
function CurrentModuleName: string;

implementation

resourcestring
  rsGeneralVersionFailure = 'Get version information failed on %s';

type
  PTranslationTable = ^TTranslationTable;

  TTranslationTable = record
    Language: Word;
    CharacterSet: Word;
  end;

function GetVersionInfo(aFilename: string): TVersionInfo;
var
  Handle: DWORD;
  verInfoSize: integer;
  pVerInfo: Pointer; // whole version info buffer
  pVerInfoElement: Pointer; // an element of the info buffer
  verInfoElementLen: UINT;
  verTranslation: PTranslationTable;
  verTranslationSize: integer;
  fixedFileInfo: TVSFixedFileInfo;
  fileTime: TFileTime;
  systemTime: TSystemTime;

  function ExtractVersionInfoElement(aField: string): string;
  var
    verInfoStr: string;
  begin
    verInfoStr:= Format('StringFileInfo\%.4x%.4x\', [verTranslation^.Language, verTranslation^.CharacterSet]) + aField;
    if VerQueryValue(pVerInfo, PChar(verInfoStr), pVerInfoElement, verInfoElementLen) and (verInfoElementLen <> 0) then
      result:= StrPas(PChar(pVerInfoElement))
    else
      result:= '';
  end;

begin
  if aFilename = '' then
    aFilename:= CurrentModuleName;

  verInfoSize:= GetFileVersionInfoSize(PChar(aFilename), Handle);
  if verInfoSize = 0 then // error
  begin
    raise Exception.CreateFmt(rsGeneralVersionFailure, [aFilename]);
  end;

  GetMem(pVerInfo, verInfoSize);
  verTranslation:= nil;
  try
    if not GetFileVersionInfo(PChar(aFilename), Handle, verInfoSize, pVerInfo) then
      raise Exception.CreateFmt(rsGeneralVersionFailure, [aFilename]);

    if not VerQueryValue(pVerInfo, '\VarFileInfo\Translation', pVerInfoElement, verInfoElementLen) then
      raise Exception.CreateFmt(rsGeneralVersionFailure, [aFilename]);
    verTranslationSize:= verInfoElementLen;
    GetMem(verTranslation, verTranslationSize);
    try
      Move(pVerInfoElement^, verTranslation^, verTranslationSize);

      verInfoElementLen:= SizeOf(fixedFileInfo);
      FillChar(fixedFileInfo, verInfoElementLen, 0);
      // Get root Path.
      if not VerQueryValue(pVerInfo, '\', pVerInfoElement, verInfoElementLen) then
        raise Exception.CreateFmt(rsGeneralVersionFailure, [aFilename]);

      Move(pVerInfoElement^, fixedFileInfo, verInfoElementLen);
      result.Major:= HiWord(fixedFileInfo.dwFileVersionMS);
      result.Minor:= LoWord(fixedFileInfo.dwFileVersionMS);
      result.IncrementRelease:= HiWord(fixedFileInfo.dwFileVersionLS);
      result.Build:= LoWord(fixedFileInfo.dwFileVersionLS);
      result.VersionStr:= Format('%d.%.2d', [result.Major, result.Minor]);
      result.IncrementReleaseStr:= Format('%d', [result.IncrementRelease]);
      result.BuildStr:= Format('%d', [result.Build]);
      result.FullVersionStr:= Format('%s.%s.%s', [result.VersionStr, result.IncrementReleaseStr, result.BuildStr]);

      fileTime.dwHighDateTime:= fixedFileInfo.dwFileDateMS;
      fileTime.dwLowDateTime:= fixedFileInfo.dwFileDateLS;
      FileTimeToSystemTime(fileTime, systemTime);
      result.FileDate:= SystemTimeToDateTime(systemTime);
      result.Comments:= ExtractVersionInfoElement('Comments');
      result.CompanyName:= ExtractVersionInfoElement('CompanyName');
      result.FileDescription:= ExtractVersionInfoElement('FileDescription');
      result.LegalCopyright:= ExtractVersionInfoElement('LegalCopyright');
      result.ProductName:= ExtractVersionInfoElement('ProductName');
    finally
      FreeMem(verTranslation, verTranslationSize);
    end;
  finally
    FreeMem(pVerInfo, verInfoSize);
  end;
end;

function CurrentModuleName: string;
// This method allows either a DLL or EXE to find out the application file path {AJD}
var
  tmp: array [0 .. MAX_PATH] of char;
begin
  result:= '';
  try
    if IsLibrary then
    begin
      if GetModuleFileName(hInstance, tmp, SizeOf(tmp) - 2) > 0 then
      begin
        result:= tmp;
      end;
    end
    else
    begin
      result:= ParamStr(0);
    end;
  except
    raise ;
  end;
end;

end.
