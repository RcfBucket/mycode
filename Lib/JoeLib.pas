{$B+}
unit JoeLib;

interface

uses Forms, Windows, StdCtrls, Dialogs, SysUtils, Graphics, Controls, Classes, Printers, WinSock,
     ShellAPI, ComCtrls, ExtCtrls;

function  FreeObject(var Thing): Boolean;

procedure MsgBox(const msg: String);
procedure ShowMessage(const msg: String);
procedure ShowRandomMessage;
procedure StatusMsg(memo: TMemo; msg: String);
procedure ShutDown;
procedure AddCharLineToMemo(c: Char; memo: TMemo);
procedure AddStringToRichEdit(const msg: String; TextColor: TColor; RichEdit: TRichEdit);
procedure AddBoldStringToRichEdit(const msg: String; TextColor: TColor; RichEdit: TRichEdit);
function  YesNoDialog(msg: String): Boolean;

procedure SyncScreen;                                                                        //Assembler
procedure AngleTextOut(CV: TCanvas; const sText: String; x, y, angle: Integer);

function  IsTimeout: Boolean;
function  IsTimeoutEx(const TimeoutPoint: TDateTime): Boolean;
procedure SetTimeout(const msec_delay: Integer);
function  SetTimeoutEx(msec_delay: Integer): TDateTime;

function  SetCurrentThreadPriority(NewPriority: Integer): Boolean;
function  GetCurrentThreadPriority: Integer;
procedure HardSleep(msec: Integer);
procedure SoftSleep(msec: Integer);

function  ColorLighten(OriginalColor: TColor; Phase: Integer): TColor;
function  ColorDarken(OriginalColor: TColor; Phase: Integer): TColor;
function  ColorBlend(const Color1, Color2: TColor; Percentage: Single): TColor;
function  ColorBlendEx(const R1, G1, B1, R2, G2, B2: Integer; Percentage: Single): TRgbTriple;
function  ColorGray(color: TColor): Integer;
function  ColorInvert(Color: TColor): TColor;
function  IsDark(Color: TColor; adjust: Integer): Boolean;
procedure ReplaceBitmapColor(bMap: TBitmap; Old, New: TColor);
procedure TintGreyscaleBitmap(greyMap, colorMap: TBitmap; tint: TColor);
procedure TintBlueGreyBitmap(Template, NewMap: TBitmap; Tint, BackColor: TColor);
procedure TintBlueGreyBitmapEx(Template, NewMap: TBitmap; Tint, BackColor: TColor; Masked: Boolean);
procedure TintBlueAlphaBitmap(Template, NewMap: TBitmap; Tint, BackColor: TColor);
function  CreateBitmapMask(bMap: TBitmap; color: TColor): TBitmap;
procedure DrawTransparent(x, y: Integer; Canvas: TCanvas; bmp, mask: TBitmap);
procedure DisplayBitmap(bMap: TBitmap; time: Integer);

procedure CenterControlHorizontal(MobileControl, Anchor: TControl);
procedure CenterControlVertical(MobileControl, Anchor: TControl);
procedure CenterControl(MobileControl, Anchor: TControl);
procedure CenterControlHorizontalInParent(Comp: TControl);
procedure CenterControlVerticalInParent(Comp: TControl);
procedure CenterControlInParent(Comp: TControl);
procedure OverlayControls(Comp1, Comp2: TControl; Offset: Integer);
function  GetLeftmostControl(Parent: TWinControl): TControl;
function  GetRightmostControl(Parent: TWinControl): TControl;
function  GetTopmostControl(Parent: TWinControl): TControl;
function  GetBottommostControl(Parent: TWinControl): TControl;
function  GetLeftmostControlPixel(Parent: TWinControl): Integer;
function  GetRightmostControlPixel(Parent: TWinControl): Integer;
function  GetTopmostControlPixel(Parent: TWinControl): Integer;
function  GetBottommostControlPixel(Parent: TWinControl): Integer;
procedure ControlSizeToTextFile(Control: TControl; FileName: TFileName);
procedure ChildControlSizeToTextFile(Parent: TWinControl; FileName: TFileName);

procedure AppendFileToFile(source, destination: TFileName);
procedure AppendTextToFile(fileName: TFileName; const msg: String);
function  FileSize(const filename: TFileName): Integer;

procedure DoPrintTest(const pages: Integer);

function IsAlpha(const c: Char): Boolean;
function IsNum(const c: Char): Boolean;
function IsNumStr(const s: String): Boolean;
function IsAlphaNumeric(const c: Char): Boolean;
function IsPunct(const c: Char): Boolean;
function IsPrintable(const c: Char): Boolean;
function LowerChar(const c: Char): Char;
function UpperChar(const c: Char): Char;
function StripChar(const s: String; const c: Char): String;
function Even(i: Integer): Boolean;
function StringReplaceFirst(victim, newguy, host: String): String;
function StringReplaceAll(victim, newguy, host: String): String;
function CRLFtoCommaText(crlf: String): String;

function GetLocalHost: string;

function GetTrayRect: TRect;
function GetClientScreenWidth: Integer;
function GetClientScreenHeight: Integer;

function GetUniqueStringID: String;
function GetUniqueFloatID: Extended;

function BoolToStr(const value: Boolean): String;
function BoolToStrPad(const value: Boolean): String;
function BoolToOkFail(const value: Boolean): String;
function ByteString(const s: String): String;
function ByteToBinary(b: Byte): String;
function ByteToBinaryEx(b: Byte; short: Boolean): String;
function WordToBinary(w: Word): String;
function IntegerToBinary(i: Integer): String;
function BoolBuf(var buf; size: Integer): String;
function BinaryBuf(var buf; size: Integer): String;
function WordBuf(var buf; size: Integer): String;
function BinaryBufEx(var buf; size: Integer; spaced, short: Boolean): String;
function HexBuf(var buf; size: Integer): String;
function PadIntToStr(const value, vLen: Integer): String;

function LargestStringInList(list: TStringList): String;
function LargestNameInList(list: TStringList): String;
function FindClosestFragInList(list: TStringList; fragment: String): Integer;
function FindFirstFragInList(list: TStringList; fragment: String): Integer;
function FindLeftmostFragInList(list: TStringList; fragment: String): Integer;
function GetName(const str: String): String;
function GetValue(const str: String): String;
function PadLeft(const str: String; MaxWidth: Integer; PadChar: Char): String;
function PadRight(const str: String; MaxWidth: Integer; PadChar: Char): String;
function PadSpace(s: String; sLen: Integer): String;

function BlockCRC(OldCrc: Word; Buf: Pointer; BufSize: Integer): Word; register;
function FileCRC(const filename: String): Word;
function TestBit(pBuffer: Pointer; SizeBytes, BitIndex: Integer): Boolean; assembler;
function OldTestBit(var p; bit: Word): Boolean;
procedure SetBit(pBuffer: Pointer; SizeBytes, BitIndex: Integer; Value: Boolean); assembler;

procedure SizeRect(var r: TRect; RadiusX, RadiusY: Integer);

function  GetUserName: String;
function  GetFileVersion: String;
function  GetFileVersionEx(FileName: String): String;
function  IsParam(const param: String): Boolean;
function  IsDelphiRunning: Boolean;

function  ScrollBarWidth: Integer;
function  ScrollBarHeight: Integer;

{$IFDEF VER90}
{$DEFINE DELPHI2}
{$ENDIF}

{$IFDEF VER100}
{$DEFINE DELPHI3}
{$ENDIF}

const
   MSEC_SEC = 1000;
   MSEC_MIN = MSEC_SEC * 60;    // ms per min
   MSEC_HOUR = MSEC_MIN * 60;   // ms per hour
   MSEC_DAY = MSEC_HOUR * 24;   // ms per day
   MSEC_WEEK = MSEC_DAY * 7;    // ms per week, heh heh.. like I'll ever need this
   HOUR = (1 / 24);             // hour,   TDateTime format
   MINUTE = (HOUR / 60);        // minute, TDateTime format
   SECOND = (MINUTE / 60);      // second, TDateTime format
   SEC10 = (SECOND * 10);       // 10 seconds, TDateTime format

type
   PColorArray24 = ^TColorArray24;
   TColorArray24 = packed array [0..5000] of TRgbTriple;

implementation

uses Beeps;

type
   TBytes = array[0..MAXINT - 1] of Byte;

var
   fBaseTime: TDateTime;

//---------------------------------------------------------------------------------------------
function  IsDelphiRunning: Boolean;
begin
   result := (FindWindow('TAppBuilder', PChar(0)) > 0);
end;

//---------------------------------------------------------------------------------------------
function IsParam(const param: String): Boolean;
var
   i: Integer;
begin
   for i := 1 to ParamCount do
       if (CompareText(param, ParamStr(i)) = 0) then begin
          result := True;
          exit;
       end; {if}
   result := False;
end;

//---------------------------------------------------------------------------------------------
function FreeObject(var Thing): Boolean;
begin
   result := False;
   if (TObject(Thing) = nil) then exit;
   try
      TObject(Thing).Free;
      TObject(Thing) := nil;
      result := True;
   except
      TObject(Thing) := nil;
   end; {try/except}
end;

//---------------------------------------------------------------------------------------------
function GetFileVersion: String;
begin
   result := GetFileVersionEx(ParamStr(0));
end;

//---------------------------------------------------------------------------------------------
function GetFileVersionEx(FileName: String): String;
var
   ResSize, FileInfoSize: cardinal;
   i: Cardinal;
   pVerInfo: Pointer;
   pFileInfo: PVSFixedFileInfo;
begin
   result := '';

   ResSize := GetFileVersionInfoSize(PChar(FileName), i);
   if (ResSize = 0) then exit;

   GetMem(pVerInfo, ResSize);
   try
      GetFileVersionInfo(PChar(FileName), 0, ResSize, pVerInfo);
      if VerQueryValue(pVerInfo, '\', pointer(pFileInfo), FileInfoSize) then begin
         result := result + IntToStr(HiWord(pFileInfo^.dwFileVersionMS)) + '.';
         result := result + IntToStr(LoWord(pFileInfo^.dwFileVersionMS)) + '.';
         result := result + IntToStr(HiWord(pFileInfo^.dwFileVersionLS)) + '.';
         result := result + IntToStr(LoWord(pFileInfo^.dwFileVersionLS));
      end {if}
   finally
      FreeMem(pVerInfo);
   end; {try/finally}
end;

//---------------------------------------------------------------------------------------------
function GetUserName: String;
var
   buf: array[0..255] of Char;
   i: cardinal;
begin
   result := '';
   i := sizeof(buf);
   Windows.GetUserName(PChar(@buf), i);
   result := String(PChar(@buf));
end;

//---------------------------------------------------------------------------------------------
procedure ChildControlSizeToTextFile(Parent: TWinControl; FileName: TFileName);
var
   i: Integer;
begin
   if (Parent = nil) then exit;

   // Give stats on our parent
   ControlSizeToTextFile(Parent, FileName);

   // If there are no child controls, exit
   if (Parent.ControlCount = 0) then exit;

   // Give stats on non TWinControl child controls
   for i := 0 to (Parent.ControlCount - 1) do begin
       if not (Parent.Controls[i] is TWinControl) then
          ControlSizeToTextFile(Parent.Controls[i], FileName);
   end; {for}

   // Give stats on TWinControl child controls
   for i := 0 to (Parent.ControlCount - 1) do begin
       if (Parent.Controls[i] is TWinControl) then
          ChildControlSizeToTextFile(TWinControl(Parent.Controls[i]), FileName);
   end; {for}

end;

//---------------------------------------------------------------------------------------------
procedure ControlSizeToTextFile(Control: TControl; FileName: TFileName);
var
   msg: String;
begin
   if (Control = nil) then exit;
   msg := Control.Name;
   if Length(msg) = 0 then msg := 'Control';
   msg := msg + ': ';
   if (control is TWinControl) then msg := msg + ' [win]';
   if (control is TForm) then msg := msg + ' [form]';
   msg := #13#10 + msg + #13#10;
   msg := msg + PadRight('  Top = ' + IntToStr(Control.Top), 12, ' ');
   msg := msg + PadRight('  Left = ' + IntToStr(Control.Left), 13, ' ');
   msg := msg + PadRight('  Width = ' + IntToStr(Control.Width), 14, ' ');
   msg := msg + PadRight('  Height = ' + IntToStr(Control.Height), 15, ' ');

   AppendTextToFile(FileName, msg);
end;

//---------------------------------------------------------------------------------------------
procedure SizeRect(var r: TRect; RadiusX, RadiusY: Integer);
begin
   with r do begin
      Dec(Left, RadiusX);
      Dec(Top, RadiusY);
      Inc(Right, RadiusX);
      Inc(Bottom, RadiusY);
   end; {with}
end;

//---------------------------------------------------------------------------------------------
procedure AddStringToRichEdit(const msg: String; TextColor: TColor; RichEdit: TRichEdit);
var
   OldColor: TColor;
begin
   with RichEdit do begin
        OldColor := SelAttributes.Color;
        SelAttributes.Color := TextColor;
        SetFocus;
        Lines.Add(msg);
        SelAttributes.Color := OldColor;
   end; {with}
end;

//---------------------------------------------------------------------------------------------
procedure AddBoldStringToRichEdit(const msg: String; TextColor: TColor; RichEdit: TRichEdit);
var
   OldColor: TColor;
   OldStyle: TFontStyles;
begin
   with RichEdit do begin
        OldColor := SelAttributes.Color;
        OldStyle := SelAttributes.Style;
        SelAttributes.Color := TextColor;
        SelAttributes.Style := SelAttributes.Style + [fsBold];
        SetFocus;
        Lines.Add(msg);
        SelAttributes.Color := OldColor;
        SelAttributes.Style := OldStyle;
   end; {with}
end;

//---------------------------------------------------------------------------------------------
function OldTestBit(var p; bit: Word): boolean;
type
  TArray = array[0..100] of byte;
begin
  result := (TArray(p)[bit div 8] and (1 shl (bit mod 8))) <> 0;
end;

//---------------------------------------------------------------------------------------------
function TestBit(pBuffer: Pointer; SizeBytes, BitIndex: Integer): Boolean;
(* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
   Zero based [0..(count - 1)]
(* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *)
asm
        SHL     SizeBytes, 3                     // Turn sizeBytes into sizeBits
        CMP     BitIndex, SizeBytes              // Exit if index is bigger than size
        JAE     @@end
        DEC     BitIndex
        CMP     BitIndex, 0                      // Exit if index is smaller than zero
        JB      @@end
        BT      [pBuffer], BitIndex              // is bit at location index on? Sets carry if yes.
        SBB     EAX, EAX                         // sub with borrow - EAX=0 (no carry)  EAX=1 (carry)
        AND     EAX, 1                           // Make sure it's clean
        MOV     result, AL                       // Set our result
@@end:                                           // exit
end;

//---------------------------------------------------------------------------------------------
procedure SetBit(pBuffer: Pointer; SizeBytes, BitIndex: Integer; Value: Boolean); assembler;
(* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
   Zero based
(* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *)
asm
        SHL     SizeBytes, 3                     // Turn sizeBytes into sizeBits
        CMP     BitIndex, SizeBytes              // Compare Index and Size
        JAE     @@end                            // If Index >= size, then exit
        DEC     BitIndex
        CMP     BitIndex, 0                      // Compare Index and Zero
        JB      @@end                            // If Index < Zero then exit
        PUSH    pBuffer

@@1:    MOV     AL, Value                        // Move Value into a register
        OR      AL, AL                           // Prime Value to be checked
        JZ      @@2                              // jump to @@2 if Value = 0
        POP     pBuffer
        BTS     [pBuffer], BitIndex              // set it to one
        JMP     @@end

@@2:    POP     pBuffer
        BTR     [pBuffer], BitIndex              // set it to zero
@@end:
end;

//---------------------------------------------------------------------------------------------
function FileCRC(const filename: String): Word;
(* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
   Using the BlockCRC routine, returns the CRC of the given file.
(* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *)
var
   fStream: TFileStream;
   buf: array [1..4096] of Byte;
   bytesRead: Integer;
   crc: Word;
begin
   result := 0;
   crc := 0;
   if (not FileExists(filename)) then exit;
   fStream := TFileStream.Create(filename, fmOpenRead);
   try
      repeat
         bytesRead := fStream.Read(buf, sizeof(buf));
         crc := BlockCRC(crc, @buf, bytesRead);
      until (bytesRead < sizeof(buf));
   finally
      fStream.Free;
   end; {try/finally}
   result := crc;
end;

//---------------------------------------------------------------------------------------------
function BlockCRC(OldCrc: Word; Buf: Pointer; BufSize: Integer): Word; register;
(* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
   This function is a recreation of the BlockCRC module in the old
   WAGPIB that is used to calculate the CRC for a w/a msg.

   This is to calculate the CRC of a block of data.
   This routine uses a byte-wise algorythm to calculate the CRC
   For information on how this works see
   Byte-wise CRC Calculations", IEEE Micro, June 83
   All subscripts have been decremented by 1
   the value to xor with CRC in <dx> is n15 through n0
   the byte of memory is m0 to m7, xi = mi xor ri

      Function block_crc( old_crc    : Integer;
                          block_addr : ptr;
                          block_len  : Integer ) : Integer;

   Compute the CRC of bytes starting at BLOCK_START for BLOCK_SIZE
   bytes, using OLD_CRC as the starting value (for blocks larger
   than 64K bytes.).  Return the new CRC value.

   1/Aug/91, Brett - modified to work with Borland C for Windows, where
                     caller may be using SI for register variable.
                     Would also save DI if used by this routine.

   30/Sept/97, Joseph Good - Modified code to work in Delphi (32 bit)

   NOTE: We must preserve the EDI, ESI, ESP, EBP, and EBX  (So Delphi says...)

   EAX   ................................
    AX                   ................
    AH                   ........
    AL                           ........

(* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *)
begin
     result := 0;
     if BufSize < 1 then exit;

     try
     asm
        push  EBX                  // preserve this one because we play with BX
        push  ESI                  // preserve ESI cuz Delphi says to
        xor   EDX, EDX             // Clear EDX
        mov   DX,  oldCrc          // Integer ptr 10[bp]; get initial CRC
        mov   ESI, buf             // get address of block
        mov   ECX, BufSize         // get the size of our buffer

@@crc1: lodsb                      // get next byte from buffer
        xor   AL, DL               // Start the CRC
        mov   BH, AL               // <bh> = x7 to x0
        add   AL, AL               // <al> = x6 to x0 <fPARITY> = n14 <fCARRY> = x7
        pushf                      // Save <FLAGS>, <fPARITY> <fCARRY>
        xor   BH, AL               // <bh> = n13 to n6
        xor   AL, AL               // make xx7 = 0 and xx6 = 0
        popf                       // Get <FLAGS>, <fPARITY> <fCARRY>
        jpe   @@crc2
        mov   AL, 011b             // <al1>,<al0> = n14
@@crc2: jnc   @@crc3
        xor   AL, 010b             // if x7 = 1 then xx8 = not xx8
@@crc3: mov   BL, AL               // <b1> = n15, <b0> = n14
        shr   AL, 1                // n0 = n15, <a0> = n0
        ror   BX, 1
        ror   BX, 1
        or    BL, AL               // <bx> = n15 to n0
        xor   BL, DH               // <bx> = r15 to r0
        mov   DX, BX               // save CRC in dx
        loop  @@crc1               // do all bytes
        xor   EAX, EAX
        mov   AX, DX
        pop   ESI
        pop   EBX
        mov   @result, AX          // return CRC in AX
     end; {asm}
     except
        result := 0;
     end;
end;

//---------------------------------------------------------------------------------------------
function PadSpace(s: String; sLen: Integer): String;
(* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
   Takes the string (s) and adds spaces onto the end until it reaches the desired
   length.
(* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *)
begin
     result := s;
     while length(result) < sLen do
           result := result + ' ';
end;

//---------------------------------------------------------------------------------------------
function ByteToBinary(b: Byte): String;
begin
   result := ByteToBinaryEx(b, False);
end;

//---------------------------------------------------------------------------------------------
function ByteToBinaryEx(b: Byte; short: Boolean): String;
   //- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
   function BinToStr(state: Boolean): String;
   begin
        if state then
           result := '1'
        else begin
           if short then
              result := '.'
           else
              result := '0';
        end; {if/else}
   end; {embeded}
begin
     result :=          BinToStr((b and $80) = $80);  // 10000000
     result := result + BinToStr((b and $40) = $40);  // 01000000
     result := result + BinToStr((b and $20) = $20);  // 00100000
     result := result + BinToStr((b and $10) = $10);  // 00010000
     result := result + BinToStr((b and $08) = $08);  // 00001000
     result := result + BinToStr((b and $04) = $04);  // 00000100
     result := result + BinToStr((b and $02) = $02);  // 00000010
     result := result + BinToStr((b and $01) = $01);  // 00000001
end;

//---------------------------------------------------------------------------------------------
function WordToBinary(w: Word): String;
var
   b: Byte;
begin
     b := Byte(w shr 8);
     result := ByteToBinary(b);
     b := Byte(w and $00FF);
     result := result + ByteToBinary(b);
end;

//---------------------------------------------------------------------------------------------
function IntegerToBinary(i: Integer): String;
var
   w: Word;
begin
     w := Word(i shr 16);
     result := WordToBinary(w);
     w := Word(i and $0000FFFF);
     result := result + WordToBinary(w);
end;

//---------------------------------------------------------------------------------------------
function HexBuf(var buf; size: Integer): String;
(* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
   Converts the buf into it's hex representation
(* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *)
var
   i: Integer;
begin
     result := '';
     if size < 1 then exit;

     for i:= 0 to (size - 1) do begin
         if length(result) > 0 then result := result + ' ';
         result := result + IntToHex(TBytes(buf)[i], 2);
     end; {for}
end;

//---------------------------------------------------------------------------------------------
function BinaryBuf(var buf; size: Integer): String;
(* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
   Converts the buf into it's Binary representation
(* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *)
begin
     result := BinaryBufEx(buf, size, True, False);
end;

//---------------------------------------------------------------------------------------------
function BoolBuf(var buf; size: Integer): String;
(* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
   Converts the buf into it's Binary representation
(* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *)
begin
     result := BinaryBufEx(buf, size, True, False);
end;

//---------------------------------------------------------------------------------------------
function BinaryBufEx(var buf; size: Integer; spaced, short: Boolean): String;
(* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
   Converts the buf into it's Binary representation.

   buf    = the area of memory to be displayed
   size   = how much to display
   spaced = puts a space between each byte
   short  = zero's are represented by a '.'
(* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *)
var
   i: Integer;
begin
     result := '';
     if size < 1 then exit;

     if spaced then begin
        for i := 0 to (size - 1) do
            result := ByteToBinaryEx(TBytes(buf)[i], short) + ' ' + result;
        Delete(result, Length(result), 1);  // delete the last space
     end
     else
        for i := 0 to (size - 1) do
            result := ByteToBinaryEx(TBytes(buf)[i], short) + result;
end;

//---------------------------------------------------------------------------------------------
function Even(i: Integer): Boolean;
begin
   result := not Odd(i);
end;

//---------------------------------------------------------------------------------------------
function WordBuf(var buf; size: Integer): String;
(* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
   Converts the buf into it's Binary representation, spaced by words
(* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *)
var
   i: Integer;
begin
     result := '';
     if size < 1 then exit;

     for i := 0 to (size - 1) do begin
         if (length(result) > 0) and (Even(i)) then
            result := result + ' ';
         result := result + IntToHex(TBytes(buf)[i], 2);
     end; {for}
end;

//---------------------------------------------------------------------------------------------
procedure HardSleep(msec: Integer);
begin
     sleep(msec);
end;

//---------------------------------------------------------------------------------------------
procedure SoftSleep(msec: Integer);
var
   FutureTime: TDateTime;
begin
     FutureTime := SetTimeoutEx(msec);
     repeat
           Application.ProcessMessages;
     until Now >= FutureTime;
end;

//---------------------------------------------------------------------------------------------
function GetUniqueFloatID: Extended;
(* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
   We whack off the decimal part of the result because it is never really precise
   and comparisons against two ID's with decimal values in there will not really
   work as well as thought.  But, the real part of the floating point variable
   works just fine.
(* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *)
const
   LastID: Extended = 0;
begin
     repeat
           result := Now * 100000 * 100000;
     until result <> LastID;

     LastID := result;
end;

//---------------------------------------------------------------------------------------------
function GetUniqueStringID: String;
begin
     result := FloatToStr(GetUniqueFloatID);
end;

//---------------------------------------------------------------------------------------------
function GetTrayRect: TRect;
var
   hTray: HWND;
begin
     with result do begin
          top := 0;
          left := 0;
          bottom := 0;
          right := 0;
     end; {with}

     hTray := FindWindow('Shell_TrayWnd', '');
     if hTray > 0 then
        GetWindowRect(hTray, result);
end;

//---------------------------------------------------------------------------------------------
function GetClientScreenWidth: Integer;
begin
     result := GetSystemMetrics(SM_CXFULLSCREEN) + 2 * GetSystemMetrics(SM_CXBORDER);
end;

//---------------------------------------------------------------------------------------------
function GetClientScreenHeight: Integer;
begin
     result := GetSystemMetrics(SM_CYFULLSCREEN) + GetSystemMetrics(SM_CYCAPTION) +
               2 * GetSystemMetrics(SM_CYBORDER);
end;

//---------------------------------------------------------------------------------------------
function GetLocalHost: String;
var
  LocalName: array[0..255] of Char;
begin
    result := '';
    if gethostname(LocalName, SizeOf(LocalName)) = 0 then result := LocalName;
end;

//---------------------------------------------------------------------------------------------
procedure OverlayControls(Comp1, Comp2: TControl; Offset: Integer);
(* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
   Sets the top, left, width, and height of Comp1 to match Comp2
(* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *)
begin
     Comp1.Top := Comp2.Top - Offset;
     Comp1.Left := Comp2.Left - Offset;
     Comp1.Width := Comp2.Width + Offset + Offset;
     Comp1.Height := Comp2.Height + Offset + Offset;
end;

//---------------------------------------------------------------------------------------------
procedure CenterControlHorizontal(MobileControl, Anchor: TControl);
begin
     // If either is nil, exit now
     if (MobileControl = nil) or (Anchor = nil) then exit;

     // Center our control
     MobileControl.Left := Anchor.Left + ((Anchor.Width div 2) - (MobileControl.Width div 2));
end;

//---------------------------------------------------------------------------------------------
procedure CenterControlVertical(MobileControl, Anchor: TControl);
begin
     // If either is nil, exit now
     if (MobileControl = nil) or (Anchor = nil) then exit;

     // Center our control
     MobileControl.Top := Anchor.Top + ((Anchor.Height div 2) - (MobileControl.Height div 2));
end;

//---------------------------------------------------------------------------------------------
procedure CenterControl(MobileControl, Anchor: TControl);
begin
     CenterControlHorizontal(MobileControl, Anchor);
     CenterControlVertical(MobileControl, Anchor);
end;

//---------------------------------------------------------------------------------------------
procedure CenterControlInParent(Comp: TControl);
begin
     CenterControlHorizontalInParent(Comp);
     CenterControlVerticalInParent(Comp);
end;

//---------------------------------------------------------------------------------------------
procedure CenterControlHorizontalInParent(Comp: TControl);
begin
     // If the component does not have a parent, exit now
     if Comp.Parent = nil then exit;

     CenterControlHorizontal(Comp, Comp.Parent);
end;

//---------------------------------------------------------------------------------------------
procedure CenterControlVerticalInParent(Comp: TControl);
begin
     // If the component does not have a parent, exit now
     if Comp.Parent = nil then exit;

     CenterControlVertical(Comp, Comp.Parent);
end;

//---------------------------------------------------------------------------------------------
function GetLeftmostControlPixel(Parent: TWinControl): Integer;
var
   tmpControl: TControl;
begin
   result := 0;
   tmpControl := GetLeftmostControl(Parent);
   if tmpControl = nil then exit;
   result := tmpControl.Left;
end;

//---------------------------------------------------------------------------------------------
function GetRightmostControlPixel(Parent: TWinControl): Integer;
var
   tmpControl: TControl;
begin
   result := 0;
   tmpControl := GetRightmostControl(Parent);
   if tmpControl = nil then exit;
   result := tmpControl.Left + tmpControl.Width;
end;

//---------------------------------------------------------------------------------------------
function GetTopmostControlPixel(Parent: TWinControl): Integer;
var
   tmpControl: TControl;
begin
   result := 0;
   tmpControl := GetTopmostControl(Parent);
   if tmpControl = nil then exit;
   result := tmpControl.Top;
end;

//---------------------------------------------------------------------------------------------
function GetBottommostControlPixel(Parent: TWinControl): Integer;
var
   tmpControl: TControl;
begin
   result := 0;
   tmpControl := GetBottommostControl(Parent);
   if tmpControl = nil then exit;
   result := tmpControl.Top + tmpControl.Height;
end;

//---------------------------------------------------------------------------------------------
function GetTopmostControl(Parent: TWinControl): TControl;
var
   i: Integer;
   TestTop, FarTop: Integer;
   TestControl: TControl;
begin
   result := nil;
   if Parent.ControlCount = 0 then exit;                             // If it's empty, exit
   FarTop := MAXINT;                                                 // Set our Top to extreme right
   for i := 0 to (Parent.ControlCount - 1) do begin                  // start cycling thru controls
       TestControl := Parent.Controls[i];                            //   Grab next control
       TestTop := TestControl.Top;                                   //   Get control's Top pos
       if TestTop < FarTop then begin                                //   If it's a new extreme
          FarTop := TestTop;                                         //   save a record
          result := TestControl;                                     //   keep this control
       end; {if}
   end; {for}
end;

//---------------------------------------------------------------------------------------------
function GetBottommostControl(Parent: TWinControl): TControl;
var
   i: Integer;
   TestBottom, FarBottom: Integer;
   TestControl: TControl;
begin
   result := nil;
   if Parent.ControlCount = 0 then exit;                             // If it's empty, exit
   FarBottom := 0;                                                   // Set our Bottom to extreme right
   for i := 0 to (Parent.ControlCount - 1) do begin                  // start cycling thru controls
       TestControl := Parent.Controls[i];                            //   Grab next control
       TestBottom := TestControl.Top + TestControl.Height;           //   Get control's Bottom pos
       if TestBottom < FarBottom then begin                          //   If it's a new extreme
          FarBottom := TestBottom;                                   //   save a record
          result := TestControl;                                     //   keep this control
       end; {if}
   end; {for}
end;

//---------------------------------------------------------------------------------------------
function GetLeftmostControl(Parent: TWinControl): TControl;
var
   i: Integer;
   TestLeft, FarLeft: Integer;
   TestControl: TControl;
begin
   result := nil;
   if Parent.ControlCount = 0 then exit;                             // If it's empty, exit
   FarLeft := MAXINT;                                                // Set our left to extreme right
   for i := 0 to (Parent.ControlCount - 1) do begin                  // start cycling thru controls
       TestControl := Parent.Controls[i];                            //   Grab next control
       TestLeft := TestControl.Left;                                 //   Get control's left pos
       if TestLeft < FarLeft then begin                              //   If it's a new extreme
          FarLeft := TestLeft;                                       //   save a record
          result := TestControl;                                     //   keep this control
       end; {if}
   end; {for}
end;

//---------------------------------------------------------------------------------------------
function GetRightmostControl(Parent: TWinControl): TControl;
var
   i: Integer;
   TestRight, FarRight: Integer;
   TestControl: TControl;
begin
   result := nil;
     if Parent.ControlCount = 0 then exit;                           // If it's empty, exit
     FarRight := 0;                                                  // Set our right to an extreme
     for i := 0 to (Parent.ControlCount - 1) do begin                // start cycling thru controls
         TestControl := Parent.Controls[i];                          //   grab our control
         TestRight := TestControl.Left + TestControl.Width;          //   get it's right pos
         if TestRight > FarRight then begin                          //   if it is a new extreme
            FarRight := TestRight;                                   //   save a record
            result := TestControl;                                   //   keep this control
         end; {if}
     end; {for}
end;

//---------------------------------------------------------------------------------------------
function  ColorInvert(Color: TColor): TColor;
(* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
   Inverts the given color
(* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *)
begin
   result := $FFFFFF - (Color and $FFFFFF);     // Make sure only RGB and invert
end;

//---------------------------------------------------------------------------------------------
function  ColorGray(color: TColor): Integer;
(* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
   returns an integer value that is just the average (quasi grayscale) of the
   three RGB values.
(* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *)
begin
     result := (GetRValue(color) + GetGValue(color) + GetBValue(color)) div 3;
end;

//---------------------------------------------------------------------------------------------
function ColorBlendEx(const R1, G1, B1, R2, G2, B2: Integer; Percentage: Single): TRgbTriple;
(* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
   Calculates the blending of two colors given the percentage of blend.
(* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *)
var
   NewR, NewG, NewB: Integer;
   ratio: Single;
begin
     // Seed our result
     result.rgbtRed := 0; result.rgbtGreen := 0; result.rgbtBlue := 0;

     // Calculate the ratio of the min to max via pos given
     if Percentage < 0.0 then Percentage := 0.0;
     if Percentage > 100.0 then Percentage := 100.0;
     ratio := Percentage / 100.0;

     // Get our new color
     if R1 > R2 then
        NewR := R1 - round((R1 - R2) * ratio)
     else
        NewR := R1 + round((R2 - R1) * ratio);

     if G1 > G2 then
        NewG := G1 - round((G1 - G2) * ratio)
     else
        NewG := G1 + round((G2 - G1) * ratio);

     if B1 > B2 then
        NewB := B1 - round((B1 - B2) * ratio)
     else
        NewB := B1 + round((B2 - B1) * ratio);

     // Make sure we are within limits
     if NewR > 255 then NewR := 255;
     if NewR < 0 then NewR := 0;
     if NewG > 255 then NewG := 255;
     if NewG < 0 then NewG := 0;
     if NewB > 255 then NewB := 255;
     if NewB < 0 then NewB := 0;

     // Set our result
     result.rgbtRed := NewR;
     result.rgbtBlue := NewB;
     result.rgbtGreen := NewG;
end;

//---------------------------------------------------------------------------------------------
function ColorBlend(const Color1, Color2: TColor; Percentage: Single): TColor;
(* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
   See ColorBlendEx
(* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *)
var
   R1, G1, B1: Integer;
   R2, G2, B2: Integer;
   WinColor: Integer;
   rgbt: TRgbTriple;
begin
     // Seed our result
     result := Color1;

     if (Color1 = Color2) then exit;

     // Get the RGB values for both colors
     WinColor := ColorToRGB(Color1);            // Convert the color into the WIN32 color spec
     R1 := GetRValue(WinColor);                 // Get red
     G1 := GetGValue(WinColor);                 // Get green
     B1 := GetBValue(WinColor);                 // Get blue
     WinColor := ColorToRGB(Color2);            // Convert the color into the WIN32 color spec
     R2 := GetRValue(WinColor);
     G2 := GetGValue(WinColor);
     B2 := GetBValue(WinColor);

     rgbt := ColorBlendEx(R1, G1, B1, R2, G2, B2, Percentage);

     result := RGB(rgbt.rgbtRed, rgbt.rgbtGreen, rgbt.rgbtBlue);
end;


//---------------------------------------------------------------------------------------------
function ColorLighten(OriginalColor: TColor; Phase: Integer): TColor;
(* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
   Lightens the given color by the given phase (R,G,B)
(* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *)
var
   R, G, B: Integer;
   WinColor: Integer;
begin
     // Convert it into the Win32 API color spec
     WinColor := ColorToRGB(OriginalColor);
     R := GetRValue(WinColor);
     G := GetGValue(WinColor);
     B := GetBValue(WinColor);

     R := R + Phase;
     G := G + Phase;
     B := B + Phase;

     if R > 255 then R := 255;
     if G > 255 then G := 255;
     if B > 255 then B := 255;

     result := RGB(R, G, B);
end;

//---------------------------------------------------------------------------------------------
function CreateBitmapMask(bMap: TBitmap; color: TColor): TBitmap;
var
   x, y: Integer;
begin
   // Create our result bitmap
   result := TBitmap.Create;
   if (bMap = nil) then exit;

   result.Width := bMap.Width;
   result.Height := bMap.Height;
   result.Canvas.Pen.Color := clBlack;
   result.Canvas.Brush.Color := clBlack;
   result.Canvas.Rectangle(0, 0, bMap.Width, bMap.Height);

   with bMap.Canvas do begin                                         // go thru each pixel in bitmap
        for y := 0 to bMap.Height do                                 // ...
            for x := 0 to bMap.Width do begin                        // ...
                if Pixels[x,y] <> color then
                   result.Canvas.Pixels[x,y] := clWhite;
            end; {for}
   end; {with}
end;

//---------------------------------------------------------------------------------------------
procedure DrawTransparent(x, y: Integer; Canvas: TCanvas; bmp, mask: TBitmap);
var
  ToRect, FromRect: TRect;
  OldMode: TCopyMode;
begin
  with Canvas do begin
       {Get what the old canvas CopyMode is, so we can restore}
       OldMode := CopyMode;

       FromRect.Left := 0;
       FromRect.Top := 0;
       FromRect.Bottom := bmp.Height;
       FromRect.Right := bmp.Width;
       ToRect.Left := x;
       ToRect.Top  := y;
       ToRect.Bottom := y + bmp.Height;
       ToRect.Right := x + bmp.Width;

       CopyMode := cmSrcAnd;
       CopyRect(ToRect, mask.canvas, FromRect);
       CopyMode := cmSrcPaint;
       CopyRect(ToRect, bmp.canvas, FromRect);

       {We need to reset the CopyMode so not to screw up things}
       CopyMode := OldMode;
  end; {with}
end;

//---------------------------------------------------------------------------------------------
procedure TintGreyscaleBitmap(greyMap, colorMap: TBitmap; tint: TColor);
(* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
   Takes a greyscale bitmap and tints it towards the given color.

   It works off the premise that a greyscale value of 128 will be the full tint
   color.  A value of 127 will be the tint color shifted slightly towards black and
   a value of 129 will be the color shifted slightly towards white.  A greyscale
   value of 0 will be pure black and 255 will be pure white.
(* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *)
const
   MIDGREY = 128;
var
   x, y: Integer;
   grey, R, G, B: Integer;
   NewColor: TColor;
   phase: Integer;
begin
   if (greyMap = nil) or (colorMap = nil) then exit;

   // Resize the colormap bitmap
   colorMap.Width := greyMap.Width;
   colorMap.Height := greyMap.Height;

   tint := ColorToRGB(tint);                                         // strip that borland stuff from it
   with greyMap.Canvas do begin                                      // go thru each pixel in bitmap
        for y := 0 to greyMap.Height do                              // ...
            for x := 0 to greyMap.Width do begin                     // ...
                // Convert it into the Win32 API color spec
                grey := ColorToRGB(Pixels[x,y]);                     // strip borland bits from color
                R := GetRValue(grey);                                // get color values
                G := GetGValue(grey);                                // ...
                B := GetBValue(grey);                                // ...

                // If it is not grey, leave it alone
                if not ((r = g) and (r = b)) then continue;

                // If the color is pure black or pure white
                if (r = 0) or (r = 255) then continue;

                // If the color is darker
                if (r < MIDGREY) then begin
                   phase := MIDGREY - r;                             // How far away from center
                   phase := phase * 2;                               // grow to 255 to get full range
                   NewColor := ColorDarken(tint, phase);             // Get our tint moved towards black
                end
                else
                // if the color is lighter
                if (r > MIDGREY) then begin
                   phase := r - MIDGREY;                             // How far from center
                   phase := phase * 2;                               // expand to 255 colors
                   NewColor := ColorLighten(tint, phase);            // move towards white
                end
                // The color needs to be pure tint
                else begin
                   NewColor := tint;
                end; {if/else/else}

                colorMap.Canvas.Pixels[x,y] := NewColor;
            end; {for}
   end; {with}
end;

//---------------------------------------------------------------------------------------------
procedure TintBlueGreyBitmap(Template, NewMap: TBitmap; Tint, BackColor: TColor);
(* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
   The same as TintBlueGreyBitmapEx except that it assumes the Masked parameter
   is false.
(* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *)
begin
   TintBlueGreyBitmapEx(Template, NewMap, Tint, BackColor, False);
end;

//---------------------------------------------------------------------------------------------
procedure TintBlueGreyBitmapEx(Template, NewMap: TBitmap; Tint, BackColor: TColor; Masked: Boolean);
(* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
   This does pretty much the same as the TintGreyscaleImage but as the greys start
   drifting towards the BLUE background color we will move the color towards the
   given BackColor instead.

   The BLUE background has to be pure blue (0, 0, 255) for this to work so that
   the bluephase is always positive.
(* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *)
const
   MIDGREY = 128;
var
   x, y: Integer;
   grey, R, G, B: Integer;
   NewColor: TColor;
   phase, bluephase: Integer;
begin
   if (Template = nil) or (NewMap = nil) then exit;

   // Resize the colormap bitmap
   NewMap.Width := Template.Width;
   NewMap.Height := Template.Height;

   Tint := ColorToRGB(tint);                                         // strip that borland stuff from it
   BackColor := ColorToRGB(BackColor);                               // ...

   with Template do begin                                            // go thru each pixel in template
        for y := 0 to Height do                                      // ...
            for x := 0 to Width do begin                             // ...
                grey := ColorToRGB(Canvas.Pixels[x,y]);              // strip borland bits from color

                if (grey = clWhite) then continue;                   // If color is pure white, skip

                if (grey = clBlack) then begin                       // If it is pure black
                   if Masked then                                    // And we are using a mask
                      NewMap.Canvas.Pixels[x,y] := $010101           // Move it away from pure black
                   else                                              // If no mask
                      Continue;                                      // go to next pixel
                end; {if}

                if (grey = clBlue) then begin                        // If we're pure BLUE
                   if Masked then                                    // If we're using this for a mask
                      NewMap.Canvas.Pixels[x,y] := clBlack           // Set background to pure black
                   else
                      NewMap.Canvas.Pixels[x,y] := BackColor;        // Set to back color
                   continue;                                         // Then go to next pixel
                end; {if}

                R := GetRValue(grey);                                // get color values
                G := GetGValue(grey);                                // ...
                B := GetBValue(grey);                                // ...

                if (R <> G) then continue;                           // We need R and G to be equal

                bluephase := B - R;

                if (r < MIDGREY) then begin                          // Pixel is dark
                   phase := MIDGREY - r;                             // How far away from center?
                   phase := phase * 2;                               // full range of 256 colors
                   NewColor := ColorDarken(tint, phase);             // tint the pixel towards black
                end
                else
                if (r > MIDGREY) then begin                          // Pixel is light
                   phase := r - MIDGREY;                             // How far from center
                   phase := phase * 2;                               // expand to 255 colors
                   NewColor := ColorLighten(tint, phase);            // tint towards white
                end
                else begin
                   NewColor := tint;                                 // It's pure, just tint it
                end; {if/else/else}

                if (bluephase > 0) then begin                        // shifting towards background?
                   bluephase := round((bluephase / 255) * 100);      // turn it into percentage
                   NewColor := ColorBlend(NewColor,
                                          BackColor, bluephase);     // do the shift
                end; {if}

                NewMap.Canvas.Pixels[x,y] := NewColor;
            end; {for}
   end; {with}
end;

//---------------------------------------------------------------------------------------------
procedure TintBlueAlphaBitmap(Template, NewMap: TBitmap; Tint, BackColor: TColor);
(* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
   This treats the blue channel of a bitmap as the transparency.  The red and
   green are treates as the grey levels.

   It is just another attempt at getting transparent bitmaps.. truely transparent
   bitmaps on my form.  Cool FX..

   Blue is a value of 0..255 which is the amount of transparency.
   Red, Green are values of 0..255.  When they are the same, they are treated
   as a transparent grey, and mapped to a tint vs backColor.

   Keep in mind that the level of gray represents a "phase" between the black
   and white.

   128 is the mid-point.  Wherever a color sits between black and 128-gray,
   it shifts that much of a percentage towards one.. not the actual amount.

(* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *)
const
   MIDGREY = 128;
var
   x, y: Integer;
   grey, R, G, B: Integer;
   NewColor: TColor;
   phase, bluePhase: Integer;
begin
   if (Template = nil) or (NewMap = nil) then exit;

   // Resize the colormap bitmap
   NewMap.Width := Template.Width;
   NewMap.Height := Template.Height;

   Tint := ColorToRGB(tint);                                         // strip that borland stuff from it
   BackColor := ColorToRGB(BackColor);                               // ...

   with Template do begin                                            // go thru each pixel in template
        for y := 0 to Height do                                      // ...
            for x := 0 to Width do begin                             // ...
                grey := ColorToRGB(Canvas.Pixels[x,y]);              // strip borland bits from color

                R := GetRValue(grey);                                // get color values
                G := GetGValue(grey);                                // ...
                B := GetBValue(grey);                                // ...
                if (R <> G) then continue;                           // We need R and G to be equal
                if (B = 0) then                                      // If no transparency
                   if (R = 0) then begin
                      NewMap.Canvas.Pixels[x, y] := clBlack;
                      continue;                                      //   Skip on pure black
                   end
                   else
                   if (R = 255) then begin
                      NewMap.Canvas.Pixels[x, y] := clWhite;
                      continue;                                      //   Skip on pure white
                   end; {if/else}

                if (B = 255) then begin                              // If we're pure transparent
                   NewMap.Canvas.Pixels[x,y] := BackColor;           // Set to back color
                   continue;                                         // Then go to next pixel
                end; {if}

                if (R < MIDGREY) then begin                          // Pixel is dark
                   phase := MIDGREY - r;                             // How far away from center?
                   phase := phase * 2;                               // full range of 256 colors
                   NewColor := ColorDarken(tint, phase);             // tint the pixel towards black
                end
                else
                if (R > MIDGREY) then begin                          // Pixel is light
                   phase := r - MIDGREY;                             // How far from center
                   phase := phase * 2;                               // expand to 255 colors
                   NewColor := ColorLighten(tint, phase);            // tint towards white
                end
                else begin
                   NewColor := tint;                                 // It's pure, just tint it
                end; {if/else/else}

                if (B > 0) then begin                                // shifting towards background?
                   bluePhase := round((B / 255) * 100);              // turn it into percentage
                   NewColor := ColorBlend(NewColor,
                                          BackColor, bluePhase);     // do the shift
                end; {if}

                NewMap.Canvas.Pixels[x,y] := NewColor;
            end; {for}
   end; {with}
end;

//---------------------------------------------------------------------------------------------
procedure ReplaceBitmapColor(bMap: TBitmap; Old, New: TColor);
(* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
   Replaces the old color with the new color all across the bitmap.
(* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *)
var
   x, y: Integer;
begin
   with bMap.Canvas do begin
      for y := 0 to bMap.Height do
          for x := 0 to bMap.Width do
              if Pixels[x, y] = Old then
                 Pixels[x, y] := New;
   end; {with}
end;

//---------------------------------------------------------------------------------------------
procedure DisplayBitmap(bMap: TBitmap; time: Integer);
var
   tmpForm: TForm;
   tmpImage: TImage;
begin
   if (bMap = nil) then exit;
   tmpForm := TForm.Create(Application);
   try
      with tmpForm do begin
           tmpImage := TImage.Create(tmpForm);
           with tmpImage do begin
                Parent := tmpForm;
                Align := alClient;
                Picture.Bitmap := bMap;
           end; {with}

           BorderStyle := bsSingle;
           FormStyle := fsStayOnTop;
           Position := poScreenCenter;
           ClientWidth := bMap.Width;
           ClientHeight := bMap.Height;

           Show;
           Application.ProcessMessages;
           SoftSleep(time);
      end; {with}
   finally
      tmpForm.Release;
   end; {try/finally}

end;

//---------------------------------------------------------------------------------------------
function IsDark(color: TColor; adjust: Integer): Boolean;
(* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
   Returns TRUE if the color is a dark color.  I assume a dark color is one that
   it's grayscale value is in the lower half of the gray-scale spectrum.
(* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *)
begin
     result := ColorGray(color) < (128 + adjust);
end;

//---------------------------------------------------------------------------------------------
function ColorDarken(OriginalColor: TColor; Phase: Integer): TColor;
(* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
   Darkens the given color by the given phase (R,G,B)
(* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *)
var
   R, G, B: Integer;
   WinColor: Integer;
begin
     // Convert it into the Win32 API color spec
     WinColor := ColorToRGB(OriginalColor);
     R := GetRValue(WinColor);
     G := GetGValue(WinColor);
     B := GetBValue(WinColor);

     R := R - Phase;
     G := G - Phase;
     B := B - Phase;

     if R < 0 then R := 0;
     if G < 0 then G := 0;
     if B < 0 then B := 0;

     result := RGB(R, G, B);
end;

//---------------------------------------------------------------------------------------------
function SetCurrentThreadPriority(NewPriority: Integer): Boolean;
(* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
   Grabs handle to current thread and sets it's priority
(* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *)
var
   hCurrentThread: Integer;
begin
     hCurrentThread := GetCurrentThread;                            // Get current thread
     SetThreadPriority(hCurrentThread, NewPriority);                // Set it's priority
     result := (GetThreadPriority(hCurrentThread) = NewPriority);   // base result on the match
end;

//---------------------------------------------------------------------------------------------
function GetCurrentThreadPriority: Integer;
(* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
   Returns the priority of the current thread
(* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *)
begin
     result := GetThreadPriority(GetCurrentThread);
end;

//---------------------------------------------------------------------------------------------
function ByteString(const s: String): String;
(* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
   Converts a string into it's integer character representation
(* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *)
var
   i: Integer;
   tmp: String;
begin
     result := '';
     for i := 1 to Length(s) do begin
         tmp := IntToStr(Ord(s[i])) + ' ';
         result := result + tmp;
     end; {for}
     if Length(result) = 0 then result := '<empty>';
end;

//---------------------------------------------------------------------------------------------
function BoolToStr(const value: Boolean): String;
(* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
   Converts a boolean variable into a string representation
(* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *)
begin
     if value then result := 'True'
     else result := 'False';
end;

//---------------------------------------------------------------------------------------------
function BoolToOkFail(const value: Boolean): String;
(* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
   Converts a boolean variable into a string representation, "ok" or "fail"
(* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *)
begin
     if value then result := 'OK'
     else result := 'FAIL';
end;

//---------------------------------------------------------------------------------------------
function BoolToStrPad(const value: Boolean): String;
(* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
   Converts a boolean variable into a string representation
(* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *)
begin
     if value then result := 'True '
     else result := 'False';
end;

//----------------------------------------------------------------------------------------
function IsTimeout: Boolean;
(* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
   Compares current tick count against a pre-determined timeout value
(* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *)
begin
     result := IsTimeoutEx(fBaseTime);
end;

//----------------------------------------------------------------------------------------
function IsTimeoutEx(const TimeoutPoint: TDateTime): Boolean;
(* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
   Checks to see if we've reached a timeout point
(* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *)
begin
     result := (Now > TimeoutPoint);                                 // True if We've over our timeout
end;

//----------------------------------------------------------------------------------------
procedure SetTimeout(const msec_delay: Integer);
(* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
   Sets a timeout and assigns it to a unit global var.
(* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *)
begin
     fBaseTime := SetTimeoutEx(msec_delay);
end;

//----------------------------------------------------------------------------------------
function SetTimeoutEx(msec_delay: Integer): TDateTime;
(* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
   Returns the time it will be when the delay expires.  If we give a 5000 msec
   delay, the time returned will be 5 seconds from now.
(* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *)
begin
     result := Now + (msec_delay / MSEC_DAY);
end;

//----------------------------------------------------------------------------------------
procedure SyncScreen; Assembler;
asm
        mov     DX, 03DAh
@sync1:
        in      AL, DX
        test    AL, 8
        jz      @sync1
@sync2:
        in      AL, DX
        test    AL, 8
        jnz     @sync2
end;

//----------------------------------------------------------------------------------------
procedure AddCharLineToMemo(c: Char; memo: TMemo);
(* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
    We will just grow a character string until it no longer adds
    just one line to a memo, but two.  Then we reduce it down by
    one char and add it permanently.
(* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *)
var
   TooShort: Boolean;
   TestString, SafeString: String;
   OldLines, NewLines: Integer;
begin
     {Initialize some things}
     TestString := c;
     SafeString := '';

     {Start our forever loop}
     TooShort := True;
     while TooShort do begin

           OldLines := memo.Lines.Count;

           memo.Lines.Add(TestString);

           NewLines := memo.Lines.Count;

           {If it only added one line, remove line, expand string, and try again}

           if ((NewLines - OldLines) = 1) then begin
              {remove line}
              memo.Lines.Delete(NewLines - 1);
              SafeString := TestString;
              TestString := TestString + c;

              TooShort := True;
              continue;
           end; {if}

           {If it added two lines, remove both and insert the safe string}
           if ((NewLines - OldLines) = 2) then begin
              memo.Lines.Delete(memo.Lines.Count - 1);
              memo.Lines.Delete(memo.Lines.Count - 1);
              memo.Lines.Add(SafeString);
//-              memo.Lines.Add('');
              TooShort := False;
           end; {if}
     end; {While}
end;

//----------------------------------------------------------------------------------------
procedure MsgBox(const msg: String);
begin
     ShowMessage(msg);
end;

//----------------------------------------------------------------------------------------
procedure ShowMessage(const msg: String);
begin
   Windows.MessageBox(0, PChar(msg), 'Some info for you...', MB_OK);
end;

//----------------------------------------------------------------------------------------
procedure ShowRandomMessage;
var
   msg: String;
begin
   case random(15) of
        0: msg := '(You) Don''t eat the yellow snow.';
        1: msg := 'You can pick your friends and you can pick your nose but you can''t pick your friends nose.';
        2: msg := 'If this were the full version, you would not be seeing this right now.';
        3: msg := 'You can purchase the full version for $19.95.  Register now!';
        4: msg := 'Quit scratching!';
        5: msg := 'DMS-2000!!!  Go Team go!!';
        6: msg := '(yawn)';
        7: msg := 'If I have to display one more worthless message, I think I''ll puke.';
        8: msg := 'One more step and I''m gonna do it!  I mean it!!';
        9: msg := 'You have no idea what you just did, do you?';
   else
           msg := 'That was the history erase button, you fool!!';
   end; {case}

   Windows.MessageBox(0, PChar(msg), 'FYI...', MB_OK);
end;

//----------------------------------------------------------------------------------------
procedure StatusMsg(memo: TMemo; msg: String);
begin
     memo.Lines.Add(msg);
end;

//----------------------------------------------------------------------------------------
procedure ShutDown;
begin
     Application.MainForm.Close;
end;

//---------------------------------------------------------------------------------------------
procedure AppendTextToFile(fileName: TFileName; const msg: String);
var
   Output: TextFile;
begin
   if Length(fileName) = 0 then exit;                                // If there is no filename, exit

   AssignFile(Output, fileName);                                     // Get our handle
   if not FileExists(fileName) then                                  // If the file not there
      ReWrite(Output);                                               //   create it
   Append(Output);                                                   // set the file up for append
   Writeln(Output, msg);                                             // write our msg
   CloseFile(Output);                                                // close the file
end;

//---------------------------------------------------------------------------------------------
procedure AppendFileToFile(source, destination: TFileName);
(* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
   Takes the source file and appends it to the destination file.  If the
   destination file does not exist, it creates it.
(* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *)
var
   InStream: TFileStream;
   OutStream: TFileStream;
begin
     OutStream := nil;

     if not FileExists(source) then exit;                                     // Exit if invalid input
     if not FileExists(destination) then                                      // If output is not there
        OutStream := TFileStream.Create(destination, fmCreate);               //    create it

     InStream := TFileStream.Create(source, fmOpenRead or fmShareExclusive);  // Open input
     if not assigned(OutStream) then                                          // Open output if it was
        OutStream := TFileStream.Create(destination, fmOpenReadWrite);        //      already there.
     try
        OutStream.Seek(0, soFromEnd);                                         // Go to end of output
        OutStream.CopyFrom(InStream, InStream.Size);                          // Copy over input file
     finally
        InStream.Free;                                                        // Free our smack
        OutStream.Free;
     end; {try/finally}
end;

//---------------------------------------------------------------------------------------------
procedure DoPrintTest(const pages: Integer);
var
   x, y: Integer;
   BigBitmap: TBitmap;
   PageRect: TRect;
   msgHeight, msgWidth: Integer;
begin
     BigBitmap := TBitmap.Create;
     BigBitmap.Width := Printer.PageWidth div 6;
     BigBitmap.Height := Printer.PageHeight div 6;

     Screen.Cursor := crHourglass;
     try
        for y := 0 to (BigBitmap.Height - 1) do
            for x := 0 to (BigBitmap.Width - 1) do
                BigBitmap.Canvas.Pixels[x,y] := RGB(random(255), random(255), random(255));
     finally
       Screen.Cursor := crDefault;
     end; {try/finally}

     with PageRect do begin
          Left := 0;
          top := 0;
          Right := Printer.PageWidth;
          Bottom := Printer.PageHeight;
     end; {with}

     with Printer.Canvas.Font do begin
          Name := 'Arial';
          Size := 144;
     end; {with}

     QuickBeepLow;

     Printer.Title := 'Kinda Big Print Test';
     Printer.BeginDoc;
     Screen.Cursor := crHourglass;
     try
        for x := 1 to pages do begin
            msgWidth := Printer.Canvas.TextWidth(IntToStr(x));
            msgHeight := Printer.Canvas.TextHeight(IntToStr(x));
            Printer.Canvas.Pen.Mode := pmMerge;
            Printer.Canvas.Brush.Style := bsClear;
            Printer.Canvas.StretchDraw(PageRect, BigBitmap);
            Printer.Canvas.TextOut(0, 100, 'Chewy');
            Printer.Canvas.TextOut( Printer.PageWidth - msgWidth,
                                    Printer.PageHeight - msgHeight - 100,
                                    IntToStr(x));
            if x < pages then Printer.NewPage;
            QuickBeepHigh;
        end; {for}
     finally
       Screen.Cursor := crDefault;
       Printer.EndDoc;
       BeepSweepUp(3);
     end; {try/finally}
end;

//---------------------------------------------------------------------------------------------
function FindClosestFragInList(list: TStringList; fragment: String): Integer;
var
   i, test, winner: Integer;
begin
   result := -1;
   winner := MAXINT;
   fragment := LowerCase(fragment);
   for i := 0 to (list.count - 1) do begin
      test := Pos(fragment, LowerCase(list.Strings[i]));
      if (test > 0) and (test < winner) then begin
         winner := test;
         result := i;
      end; {if}
   end; {for}
end;

//---------------------------------------------------------------------------------------------
function FindFirstFragInList(list: TStringList; fragment: String): Integer;
var
   i: Integer;
begin
   result := -1;
   fragment := LowerCase(fragment);
   for i := 0 to (list.count - 1) do
      if (Pos(fragment, LowerCase(list.Strings[i])) > 0) then begin
         result := i;
         exit;
      end; {if}
end;

//---------------------------------------------------------------------------------------------
function FindLeftmostFragInList(list: TStringList; fragment: String): Integer;
var
   i: Integer;
begin
   result := -1;
   fragment := LowerCase(fragment);
   for i := 0 to (list.count - 1) do
      if (Pos(fragment, LowerCase(list.Strings[i])) = 1) then begin
         result := i;
         exit;
      end; {if}
end;

//---------------------------------------------------------------------------------------------
function LargestStringInList(list: TStringList): String;
var
   i: Integer;
begin
     result := '';
     for i := 0 to (list.count - 1) do
         if Length(list.Strings[i]) > Length(result) then
            result := list.Strings[i];
end;

//---------------------------------------------------------------------------------------------
function LargestNameInList(list: TStringList): String;
var
   i: Integer;
begin
     result := '';
     for i := 0 to (list.count - 1) do
         if Length(list.Names[i]) > Length(result) then
            result := list.Names[i];
end;

//---------------------------------------------------------------------------------------------
function GetName(const str: String): String;
(* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
   returns the name part of a string in the format:
      name=value
(* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *)
var
  P: Integer;
begin
     result := str;
     P := AnsiPos('=', result);
     if P <> 0 then
        SetLength(result, P - 1)
     else
        SetLength(result, 0);
end;

//---------------------------------------------------------------------------------------------
function PadRight(const str: String; MaxWidth: Integer; PadChar: Char): String;
begin
   result := str;
   while Length(result) < MaxWidth do
         result := result + PadChar;
end;

//---------------------------------------------------------------------------------------------
function PadLeft(const str: String; MaxWidth: Integer; PadChar: Char): String;
begin
   result := str;
   while Length(result) < MaxWidth do
         result := PadChar + result;
end;

//---------------------------------------------------------------------------------------------
function GetValue(const str: String): String;
(* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
   returns the value part of a string in the format:
      name=value
(* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *)
var
  P: Integer;
begin
     result := str;
     P := AnsiPos('=', result);
     if P <> 0 then
        result := copy(result, P + 1, Length(result) - P)
     else
        SetLength(result, 0);
end;

//---------------------------------------------------------------------------------------------
function LowerChar(const c: Char): Char;
begin
   result := c;
   if (c >= 'A') and (c <= 'Z') then inc(result, 32);
end;

//---------------------------------------------------------------------------------------------
function UpperChar(const c: Char): Char;
begin
   result := c;
   if (c >= 'a') and (c <= 'z') then dec(result, 32);
end;

//---------------------------------------------------------------------------------------------
function CRLFtoCommaText(crlf: String): String;
(* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
   Converts a string with carriage-returns and line-feeds into comma delimited
(* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *)
var
   buf: String;
begin
   buf := Trim(crlf);
   buf := StringReplaceAll(#13#10, '","', buf);
   buf := StringReplaceAll(#13, '","', buf);
   buf := StringReplaceAll(#10, '","', buf);
   buf := '"' + buf + '"';
   buf := StringReplaceAll(',""', '', buf);
   result := buf;
end;

//---------------------------------------------------------------------------------------------
function StripChar(const s: String; const c: Char): String;
var
   i: Integer;
begin
   result := '';
   for i := 1 to Length(s) do
       if (s[i] <> c) then result := result + s[i];
end;

//---------------------------------------------------------------------------------------------
function StringReplaceFirst(victim, newguy, host: String): String;
(* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
   Replaces the first occurance of the victim string with the new string.
(* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *)
var
   i, lv: Integer;
begin
   result := host;
   lv := Length(victim);
   if (lv = 0) or (Length(host) = 0) then exit;

   i := Pos(victim, result);
   if (i = 0) then exit;

   Delete(result, i, lv);
   Insert(newguy, result, i);
end;

//---------------------------------------------------------------------------------------------
function StringReplaceAll(victim, newguy, host: String): String;
(* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
   Replaces all victim substrings with the new substrings
(* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *)
begin
   result := host;
   if (Length(victim) = 0) or (Length(result) = 0) then exit;

   while (Pos(victim, result) > 0) do
      result := StringReplaceFirst(victim, newguy, result);
end;

//---------------------------------------------------------------------------------------------
function IsAlpha(const c: Char): Boolean;
(* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
   Returns true if the given char is between 'A' and 'Z' or 'a' and 'z'
(* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *)
const
   Alpha = ['A'..'Z', 'a'..'z'];
begin
   result := c in Alpha;
end;

//---------------------------------------------------------------------------------------------
function IsPunct(const c: Char): Boolean;
(* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
   Returns true if the given char is a punctuation character
(* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *)
const
   Punct = ['~', '`', '!', '@', '#', '$', '%', '^', '&', '*',
            '(', ')', '-', '_', '=', '+', '[', ']', '{', '}',
            '\', '|', ';', ':', ',', '.', '/', '<', '>', '?',
            '"', ''''];
begin
   result := c in Punct;
end;

//---------------------------------------------------------------------------------------------
function IsNum(const c: Char): Boolean;
(* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
   Returns true if the given char is between '0' and '9'
(* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *)
const
   Numeric = ['0'..'9'];
begin
   result := c in Numeric;
end;

//---------------------------------------------------------------------------------------------
function IsNumStr(const s: String): Boolean;
(* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
   Returns true if the given string is solely comprised of numbers
(* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *)
const
   Numeric = ['0'..'9'];
var
   i: Integer;
begin
   result := False;
   if (Length(s) < 1) then exit;

   result := True;
   for i := 1 to Length(s) do
       result := result and (s[i] in Numeric);
end;

//---------------------------------------------------------------------------------------------
function IsAlphaNumeric(const c: Char): Boolean;
begin
   result := IsAlpha(c) or IsNum(c);
end;

//---------------------------------------------------------------------------------------------
function IsPrintable(const c: Char): Boolean;
begin
   result := IsAlpha(c) or IsNum(c) or IsPunct(c);
end;

//---------------------------------------------------------------------------------------------
procedure AngleTextOut(CV: TCanvas; const sText: String; x, y, angle: Integer);
var
   LogFont: TLogFont;
   SaveFont: TFont;
begin
     SaveFont := TFont.Create;
     SaveFont.Assign(CV.Font);
     GetObject(SaveFont.Handle, sizeof(TLogFont), @LogFont);
     with LogFont do begin
          lfEscapement := angle *10;
          lfPitchAndFamily := FIXED_PITCH or FF_DONTCARE;
          lfOutPrecision:=out_tt_precis;
          lfQuality:=proof_quality;
     end; {with}
     CV.Font.Handle := CreateFontIndirect(LogFont);
     SetBkMode(CV.Handle, TRANSPARENT);
     CV.TextOut(x, y, sText);
     CV.Font.Assign(SaveFont);
     SaveFont.Free;
end;

//---------------------------------------------------------------------------------------------
function  ScrollBarWidth: Integer;
begin
   result := GetSystemMetrics(SM_CXVSCROLL);
end;

//---------------------------------------------------------------------------------------------
function  ScrollBarHeight: Integer;
begin
   result := GetSystemMetrics(SM_CYHSCROLL);
end;

//---------------------------------------------------------------------------------------------
function PadIntToStr(const value, vLen: Integer): String;
begin
   result := IntToStr(value);
   while (Length(result) < vLen) do
      result := '0' + result;
end;

//---------------------------------------------------------------------------------------------
function  FileSize(const filename: TFileName): Integer;
var
   buf: TFileStream;
begin
   buf := TFileStream.Create(filename, fmOpenRead);
   result := buf.Size;
   buf.free;
end;

//---------------------------------------------------------------------------------------------
function YesNoDialog(msg: String): Boolean;
begin
   result := (MessageDlg(msg, mtConfirmation, [mbYes, mbNo], 0) = mrYes);
end;

initialization
   Randomize;
end.

