unit Unit1;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, SCM, ComCtrls;

type
  TForm1 = class(TForm)
    Memo1: TMemo;
    edFile1: TEdit;
    edFile2: TEdit;
    Label1: TLabel;
    Label2: TLabel;
    chkConvertTabs: TCheckBox;
    chkIgnoreCase: TCheckBox;
    chkUseLineNums: TCheckBox;
    chkIgnoreWhite: TCheckBox;
    Button1: TButton;
    edMinLines: TEdit;
    edTabSize: TEdit;
    edIgnoreChars: TEdit;
    edIgnoreString: TEdit;
    chkShowFileInfo: TCheckBox;
    chkOptimize: TCheckBox;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    StatusBar1: TStatusBar;
    procedure FormCreate(Sender: TObject);
    procedure Button1Click(Sender: TObject);
  private
    fScm: TSCM;
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

{$R *.dfm}

procedure TForm1.FormCreate(Sender: TObject);
begin
  fScm:= TSCM.Create(nil);
  chkConvertTabs.Checked:= fScm.ConvertTabs;
  chkIgnoreCase.Checked:= fScm.IgnoreCase;
  chkIgnoreWhite.Checked:= fScm.IgnoreWhiteSpace;
  chkOptimize.Checked:= fScm.Optimize;
  chkShowFileInfo.Checked:= fScm.ShowFileInfo;
  chkUseLineNums.Checked:= fScm.UseLineNumbers;
  edMinLines.Text:= IntToStr(fScm.MinLinesForMatch);
  edTabSize.Text:= IntToStr(fScm.TabSize);
  edIgnoreChars.Text:= fScm.CharsToIgnore;
  edIgnoreString.Text:= fScm.StringToIgnore;
end;

procedure TForm1.Button1Click(Sender: TObject);
var
  k : integer;
begin
  fScm.FileName1:= edFile1.Text;
  fScm.FileName2:= edFile2.Text;
  fScm.Execute;

  memo1.Clear;
  for k:= 0 to fScm.DiffCount-1 do
    memo1.Lines.Add(fScm.DiffText[k]);

  StatusBar1.SimpleText:= Format('Differences: %d', [fScm.NumDiffs]);
end;

end.
