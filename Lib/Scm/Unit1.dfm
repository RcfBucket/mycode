object Form1: TForm1
  Left = 231
  Top = 114
  Width = 893
  Height = 563
  Caption = 'Form1'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 16
    Top = 12
    Width = 25
    Height = 13
    Caption = 'File 1'
  end
  object Label2: TLabel
    Left = 16
    Top = 36
    Width = 25
    Height = 13
    Caption = 'File 2'
  end
  object Label3: TLabel
    Left = 120
    Top = 70
    Width = 92
    Height = 13
    Caption = 'Min Lines for match'
  end
  object Label4: TLabel
    Left = 312
    Top = 70
    Width = 42
    Height = 13
    Caption = 'Tab Size'
  end
  object Label5: TLabel
    Left = 448
    Top = 70
    Width = 71
    Height = 13
    Caption = 'Chars to ignore'
  end
  object Label6: TLabel
    Left = 664
    Top = 70
    Width = 76
    Height = 13
    Caption = 'String To Ignore'
  end
  object Memo1: TMemo
    Left = 0
    Top = 96
    Width = 885
    Height = 414
    Align = alBottom
    Anchors = [akLeft, akTop, akRight, akBottom]
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Lucida Console'
    Font.Style = []
    Lines.Strings = (
      'Memo1')
    ParentFont = False
    ScrollBars = ssBoth
    TabOrder = 0
    WordWrap = False
  end
  object edFile1: TEdit
    Left = 64
    Top = 8
    Width = 297
    Height = 21
    TabOrder = 1
    Text = 'E:\VisualStudioProjects\SCMTester\bin\Debug\text1.txt'
  end
  object edFile2: TEdit
    Left = 64
    Top = 32
    Width = 297
    Height = 21
    TabOrder = 2
    Text = 'E:\VisualStudioProjects\SCMTester\bin\Debug\text2.txt'
  end
  object chkConvertTabs: TCheckBox
    Left = 440
    Top = 8
    Width = 97
    Height = 17
    Caption = 'Convert Tabs'
    TabOrder = 3
  end
  object chkIgnoreCase: TCheckBox
    Left = 440
    Top = 24
    Width = 97
    Height = 17
    Caption = 'Ignore Case'
    TabOrder = 4
  end
  object chkUseLineNums: TCheckBox
    Left = 584
    Top = 40
    Width = 137
    Height = 17
    Caption = 'Use Line Numbers'
    TabOrder = 5
  end
  object chkIgnoreWhite: TCheckBox
    Left = 440
    Top = 40
    Width = 129
    Height = 17
    Caption = 'Ignore Whitespace'
    TabOrder = 6
  end
  object Button1: TButton
    Left = 16
    Top = 64
    Width = 75
    Height = 25
    Caption = 'Go'
    TabOrder = 7
    OnClick = Button1Click
  end
  object edMinLines: TEdit
    Left = 224
    Top = 66
    Width = 65
    Height = 21
    TabOrder = 8
    Text = 'edMinLines'
  end
  object edTabSize: TEdit
    Left = 360
    Top = 66
    Width = 57
    Height = 21
    TabOrder = 9
    Text = 'edTabSize'
  end
  object edIgnoreChars: TEdit
    Left = 528
    Top = 66
    Width = 121
    Height = 21
    TabOrder = 10
  end
  object edIgnoreString: TEdit
    Left = 744
    Top = 66
    Width = 121
    Height = 21
    TabOrder = 11
  end
  object chkShowFileInfo: TCheckBox
    Left = 584
    Top = 24
    Width = 97
    Height = 17
    Caption = 'Show File Info'
    TabOrder = 12
  end
  object chkOptimize: TCheckBox
    Left = 584
    Top = 8
    Width = 97
    Height = 17
    Caption = 'Optimize'
    TabOrder = 13
  end
  object StatusBar1: TStatusBar
    Left = 0
    Top = 510
    Width = 885
    Height = 19
    Panels = <>
  end
end
