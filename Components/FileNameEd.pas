unit FileNameEd;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, ShellAPI;

type
  TFileNameEdit = class(TEdit)
  private
    fIntLabel   : string;
    procedure SetIntLabel(value: string);
  protected

  public
    constructor Create(aOwner: TComponent); override;
    procedure CreateWnd; override;
    procedure WMDropFiles(var msg: TWMDropFiles); message WM_DROPFILES;
    procedure WMPaint(var msg: TWMPaint); message WM_PAINT;
    procedure DoExit; override;
    procedure PaintBack;
  published
    property InternalLabel: string read fIntLabel write SetIntLabel;
  end;

procedure Register;

implementation

constructor TFileNameEdit.Create(aOwner: TComponent);
begin
  inherited Create(aOwner);
  fIntLabel:= 'File Name Edit';
  ControlStyle:= ControlStyle - [csSetCaption];
end;

procedure TFileNameEdit.CreateWnd;
begin
  inherited CreateWnd;
  DragAcceptFiles(handle, true);
end;

procedure TFileNameEdit.SetIntLabel(value: string);
begin
  if (value <> fIntLabel) then
  begin
    fIntLabel:= value;
    Invalidate;
  end;
end;

procedure TFileNameEdit.WMDropFiles(var msg: TWMDropFiles);
var
  tstr  : string;
  count : integer;
begin
  try
    SetLength(tstr, MAX_PATH);
    // get number of files dropped
    count:= DragQueryFile(msg.drop, MAXDWORD, nil, 0);
    if count > 0 then
    begin
      DragQueryFile(msg.drop, 0, PChar(tstr), MAX_PATH);
      SetLength(tstr, StrLen(PChar(tstr)));
      text:= tstr;
    end;
  finally
    DragFinish(msg.Drop);
    msg.result:= 0;
  end;
end;

procedure TFileNameEdit.DoExit;
begin
  inherited;
  PaintBack;
end;

procedure TFileNameEdit.PaintBack;
var
  rect    : TRect;
  fCanvas : TControlCanvas;
begin
  if (GetFocus <> handle) and (text = '') then
  begin
    FCanvas := TControlCanvas.Create;
    try
      TControlCanvas(FCanvas).Control := Self;
      rect:= ClientRect;
      InflateRect(rect, -4, -1);
      with fCanvas do
      begin
        brush.Style:= bsSolid;

        font.Name:= 'Arial';
        font.Color:= clGray;
        font.Style:= [fsItalic];
        font.size:= 8;
        TextRect(rect, 4,2, fIntLabel);
      end;
    finally
      fCanvas.Free;
    end;
  end;
end;

procedure TFileNameEdit.WMPaint(var msg: TWMPaint);
begin
  inherited;
  PaintBack;
end;


procedure Register;
begin
  RegisterComponents('Custom', [TFileNameEdit]);
end;

end.

















