unit Btnmenu;

interface

uses
  Buttons, Classes, Menus, Controls;

type

TSpeedBtnMenu = class(TSpeedButton)
  private
    FItems : TStrings; {We'll use FItems.Objects as a flag to know if

                       an item is selected. Typecasting will be needed!}
    FTitle : String;
    FMenu : TPopupMenu;
    FSelectedItemIndex : Integer; { The index of the last selected item }
    FShowChecked : Boolean;
    FMultiSelect : Boolean;
    FOnSelect : TNotifyEvent;
    FBeforeShow : TNotifyEvent;
    procedure MenuClicked(sender: TObject);
    procedure SetItems(s: TStrings);
    procedure SetMultiSelect(b: Boolean);

    function  Count: Integer;
  protected
    procedure MouseUp(Button: TMouseButton; Shift: TShiftState; 
                      X, Y: Integer); override;
  public
    constructor Create(parent: TComponent); override;
    destructor  Destroy; override;
    procedure   Select(i: Integer);
    procedure   DeSelect(i: Integer);
    function    Selected(i: Integer): Boolean;
  published

    property MultiSelect: Boolean read FMultiSelect 
                                  write SetMultiSelect default False;
    property Title: String read FTitle write FTitle;
    property Items: TStrings read FItems write SetItems;
    property ItemIndex: Integer read FSelectedItemIndex default -1;
    property ShowChecked: Boolean read FShowChecked 

                                  write FShowChecked default False;
    property OnSelect: TNotifyEvent read FOnSelect write FOnSelect;
    property BeforeShow: TNotifyEvent read FBeforeShow write FBeforeShow;
end;

procedure Register;

implementation

uses
  WinTypes;

{ TSpeedBtnMenu }
procedure TSpeedBtnMenu.MouseUp(Button: TMouseButton; Shift: TShiftState; X, Y: Integer);

var
  p : TPoint;
  i : INteger;
  item : TMenuItem;
begin
  inherited MouseUp(Button,Shift, X, Y );

  { It's easier to free the menu and then recreate it each time }
  FMenu.Free;
  FMenu := nil;

  if Assigned(FBeforeShow) then FBeforeShow(self);

  if Count > 0 then 
    begin
      FMenu := TPopupMenu.Create(self);
      { If we want to show a menu title we'll have to append it to the top

        of the menu, followed by a separator }
      if FTitle <> '' then
        begin
          item := TMenuItem.Create(FMenu);
          item.Caption := Title;
          FMenu.Items.Add(item);
          item := TMenuItem.Create(FMenu);
          item.Caption := '-';
          FMenu.Items.Add(item);
        end;
      { We have to append the menu items to the menu }
      for i := 0 to Count -1 do
        begin

          item := TMenuItem.Create( FMenu );
          item.Caption := FItems[i];
          item.OnClick := MenuClicked;
          if ((i = ItemIndex) and (ShowChecked)) or
             (MultiSelect and Selected(i) ) then
            item.Checked := True;
          FMenu.Items.Add(item);
        end;
      { Let's show the popup menu just below the button }
      p.X := 0;
      p.Y := Self.Height;
      p := ClientToScreen(p);

      FMenu.Popup(p.X,p.Y-1);
    end;
end;

function  TSpeedBtnMenu.Count : Integer;
begin
  Result := FItems.Count;
end;

constructor TSpeedBtnMenu.Create(parent: TComponent);
begin
  inherited Create(parent);
  FItems := TStringList.Create;
  FMultiSelect := False;
  FMenu := nil;
  FSelectedItemIndex := -1;
  FTitle := '';
end;

destructor  TSpeedBtnMenu.Destroy;
begin

  FItems.Free;
  inherited Destroy;
end;

procedure TSpeedBtnMenu.MenuClicked(sender: TObject);
begin
  FSelectedItemIndex := FMenu.Items.IndexOf(sender as TMenuItem);
  if Title <> '' then Dec(FSelectedItemIndex, 2);
  if MultiSelect then Select(FSelectedItemIndex);
  if Assigned(FOnSelect) then FOnSelect(self);
end;

procedure TSpeedBtnMenu.SetItems(s: TStrings);
begin

  FItems.Assign(s);
end;

procedure TSpeedBtnMenu.SetMultiSelect(b: Boolean);
var
  i : Integer;
begin
  if ItemIndex >= 0 then Select(ItemIndex);
  if b then
    FMultiSelect := True
  else 
    begin
      FMultiSelect := False;
      { We reset the selected item list each time we assign a value
        to the MultiSelect property. An easy way to clear the selected
        item list is to set MultiSelect := True }

      for i := 0 to Count - 1 do DeSelect(i);
    end;
end;

function TSpeedBtnMenu.Selected(i : Integer ): Boolean;
begin
  if( i >= 0 ) and ( i < Count ) then
    Result := (FItems.Objects[i] = TObject(1))
  else
    Result := False;
end;

procedure TSpeedBtnMenu.Select(i: Integer);
begin
  if (i >= 0) and (i < Count) then FItems.Objects[i] := TObject(1);

end;

procedure TSpeedBtnMenu.DeSelect(i: Integer);
begin
  if (i >= 0) and (i < Count) then FItems.Objects[i] := TObject(0);
end;

procedure Register;
begin
  RegisterComponents('Custom', [TSpeedBtnMenu]);
end;

end.


