                          SECTION 01 - Non-Technical General
                                    
                                    
GENERAL INFORMATION

This document contains information that is most often provided to users of
Section 1 - Non-Technical General.  It contains a description of the most
frequently asked questions and their answers.

FREQUENTLY ASKED QUESTIONS

Q: "In which section should I post my question?"

A: Your questions are very welcome, and you should post your question to the
   section that seems most appropriate to the problem.  For your convenience
   here are the guidelines for each section:

   * Section 1 - Non-Technical General
        General conversation or NON-TECHNICAL questions related to the
        products.  If you post a technical question in this section, it
        may go ananswered, or you may get a message explaining that you
        need to re-post your question to another section.

   * Section 2 - Non-Technical Customer Service
        Post questions regarding product pricing, availability, and and 
        order information in this section.  Typical question: "How much is
        the latest version of Delphi?"

   * Section 3 - Installation
        Post a message to this section if you need help or have questions
        about getting the products installed on your hard disk.  Once the
        product is installed and runs on your computer, this section is
        no longer appropriate. Typical question: "I get an 'Out of Disk
        Space' error when installing Delphi, what should I do?"

   * Section 4 - Delphi IDE
        This section is intended for issues related to the Delphi Integrated
        Development Environment. Coding issues are not appropriate for this
        section.  Typical question: "How can I reconfigure Delphi's SpeedBar?"

   * Section 5 - Delphi VCL
        Issues related to Delphi's Visual Component Library framework.  Here
        you can get assistance with all of the component on Delphi Component
        Palette with the exception of the database components and VBXs.
        Typical question: "How can I dynamically create a Delphi component?"

   * Section 6 - Delphi Database
        In this section, you can post issues regarding Delphi's database
        components or the Borland Database Engine (IDAPI).  For issues
        directly related to Client/Server programming, please post in section
        10.  Typical question: "How can I connect to an Access table with
        Delphi?"

   * Section 7 - Object Pascal
        Use this section for discussion of Object Pascal language syntax not
        directly related to the use of VCL.  Typical question: "How do you
        raise an exception in Object Pascal?"

   * Section 8 - VBX Controls
        Post questions regarding using or creating VBX controls in Delphi in
        this section.  Typical question: "I get error X when I try to add
        my VBX control to the Component Palette; what's wrong?"

   * Section 9 - Component Design
        This section is for discussion of creating custom Delphi components
        and component and property editors.  If you wish to advertise your
        custom component, you should post a message in section 22.  Typical
        question: "How to a create a TEdit decendant where the Enter key
        works like the tab?"

   * Section 10 - Client/Server
        This section is dedicated to the discussion of using Delphi's
        database components and the BDE to create applications for
        Client/Server (ie, Oracle, Sybase, etc.) environments. You can
        get assistance on using Delphi to communicate with the Local
        InterBase Server (LIBS) in this section, but LIBS itself is 
        supported in section 9 of the BDevTools forum.  Typical question:
        "How can I execute a stored procedure on my Oracle server?"

   * Section 11 - OLE / DDE
        If you need help with using Object Linking and Embedding (OLE) or
        Dynamic Data Exchange (DDE) in your Delphi or Pascal applications,
        then this is the place to be.  Typical question: "How can I embed
        a Word document in my program using the TOleContainer component?"

   * Section 14 - Windows API
        Post a message here if you have a question directly related to the
        Windows API (ie, not related to VCL or OWL).  Typical question:
        "What API function should I use to execute another application?"

   * Section 15 - Debugger / Tools
        This section supports Turbo Debugger and the various tools that
        accompany Delphi and Pascal including Turbo Profiler, WinSight,
        WinSpector, and Resource Workshop.  Typical question: "What is the
        proper technique for creating a RCDATA resource in Resource
        Workshop?"

   * Section 16 - OWL / BWCC
        Post messages dealing with Object Windows Library (OWL) or Borland
        Windows Custom Controls (BWCC) in this section.  Typical question:
        "How can use the BWCC controls in my TDialog?"

   * Section 17 - TP/BP DOS Programming
        This section deals with programming for DOS real mode under Turbo
        and Borland Pascal, including graphics programming.  Typical question:
        "Is it possible to link C .OBJ files to my Turbo Pascal code?"

   * Section 18 - Turbo Vision
        Messages dealing with the Pascal Turbo Vision framework belong in this
        section.  Typical question: "How do I give focus to a particular
        TInputLine control?"

   * Section 19 - Protected Mode
        Discussion of programming for DOS Protected Mode Interface (DPMI)
        with Borland Pascal 7.0.  Typical question: "How can I simulate an
        array that breaks the 64k barrier?"

   * Section 20 - Networks
        Use this section for discussion of network programming NOT related
        to Delphi's Database components.  Typical question: "How can I allow
        two computers to simultaneously read the same file located on a 
        networked drive?"

  * Section 21 - Jobs / Help Wanted
        This section is a "Classifieds" area intended for use by companies
        seeking employees and individuals seeking employers.  Typical
        post: "Looking for an awesome Delphi programmer..."

   * Section 22 - 3rd Party Products
        This section is reserved for 3rd parties to advertise products they
        have that work in conjunction with Delphi and Pascal.  Typical post:
        "Cool new library of Delphi components for sale..."

Q: "What's the difference between Delphi and Delphi Client/Server?"

A: Delphi Client/Server contains everything in Delphi plus...

   * SQL Links native drivers for Oracle, Sybase, MS SQL Server, Informix,
     and InterBase.
   * Rights for UNLIMITED redistribution of above drivers.
   * Local InterBase Server Deployment Kit.
   * Team Development Tools (integrates with existing PVCS installation).
   * Visual Component Library source code.
   * Visual Query Builder.
        
Q: "Where are Delphi's VCL and Object Pascal reference manuals?"

A: Those manuals are included in the online help system.  Select 
   Help|Contents, and click either the "Object Pascal" or "Visual Component
   Library" hotlink.

   We will also be posting the Adobe Acrobat versions of these references to
   this forum shortly.

   For those that prefer printed documentation, you can purchase the Delphi
   Supplemental Documentation Set for $35 by calling the Order Desk at
   1-800-331-0877.

Q: "How can I apply to be a field tester for a future version of Delphi?"

A: Contact Borland at the following address, stating that you wish to
   become a Delphi beta tester.  In it describe why you want to become
   a beta tester, what kind of testing background you have, what kind
   of setup you are currently running, and any other pertinent information.
   Address it to:

   Delphi Research and Development
   100 Borland Way
   Scotts Valley, CA 95066

Q: "Will there be a 32-bit version of Delphi, and, if so, when will it be
   released?"

A: Yes, we will release a 32-bit version of Delphi for Windows 95 and Windows
   NT shortly after the general commercial release of Windows 95."

Q: "How much will 32-bit Delphi cost?"

A: Specific pricing information is not available at this time.  However,
   although the it will be for a new platform, you can be sure that Borland
   will have special upgrade pricing available for folks who bought the
   current version of Delphi.

Q: "Will there be a Delphi for OS/2 or the Macintosh?"

A: Borland has announced no plans at this time to produce OS/2 or Mac versions
   of Delphi.

Q: "Does Delphi come with a utility to help me install my applications?"

A: No, but there are a number of third party tools on the market that fit this
   bill, including:

      InstallSHIELD
      from Stirling Technologies Inc.
      (800)374-4353

      Eschalon Setup 1.0g
      from Eschalon Development Inc.
      (604) 945-3198

Q: "Where can I find a list of 3rd party books written for Delphi?"

A: To obtain such a list, download TI2776.ZIP from either Compuserve or
   TechFax (1-800-822-4269).

Q: "How can I find out about any possible Delphi training courses in my area?"

A: Download TI2841.ZIP from either Compuserve or TechFax (1-800-822-4269).
   This TI describes how you can get listings of Delphi consultants and
   training centers in your area.

Q: "How can I find out about possible Delphi users groups in my area?"

A: Contact the Borland User Group Relations department at (408) 431-1138.

Q: "What's a TI?"

A: A TI is a Technical Information document that addresses a particular issue,
   such as a common programming problem, an installation question, and
   product information.  TI's discuss common technical issues, often 
   providing solutions, workarounds, and pointers to additional information.
   Use TI's as a source of support if you are looking for information on a 
   particular technical issue, or for general information.  TI's can be 
   obtained through Borland's TechFax system by calling 1-(800) 822-4296.  
   Many TI's are also available online through Compuserve (GO BORLAND), ftp
   (ftp.borland.com), the World Wide Web (http://www.borland.com), or
   on Borland's Downloadable Bulletin Board System (408-431-5096).
   Online versions of TI's are of the form TIxxxx.ZIP, where xxxx denotes
   the TI number.  For a listing of all available TI's, call TechFax and 
   follow the automated voice instructions to obtain a catalog of all the
   Technical Information documents available from Borland.

Q: "Will there be an upgrade price for the 32 bit version of Delphi?"

A: There will be an special upgrade price available for current owners
   of Delphi who wish to upgrade to Delphi32.  As to what that cost will
   be is unannounced at this time.


Q: How can I print the output from my Turbo Pascal for Windows (TPW) 1.5
   program if I'm using the WinCrt unit?

A: Download WINCRT.ZIP from section 2, or WINCRT2.ZIP from 
   the Borland BBS (408-431-5096). Download it, unzip it, and replace 
   the WinCrt unit in your 'uses' clause with WinCrt2.
   Your program will work as before with the addition of a Print menu
   option for printing the running output of your program.  See the
   README file included in ZIP for more details.

