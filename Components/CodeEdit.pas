unit CodeEdit;

{ TDBCodeEdit - Is a database edit component that allows editing of fields
                linked to another table.

  There are three main parts to a TDBCodeEdit control.
    Raw Data - The actual (raw) data that is stored in the record. This is the
               physical 'link' between the data field and the lookup field.
    Display  - The visual representation of that raw data.  This data is
               usually extracted from a lookup table.
    ID (user)- This is an ID of some sort that the user can use to easily
               'look up' data.

  For example, a table has a field called 'CustRef'. This is a link into a
  customer table that has the following schema:

    RecNum   : integer
    ID       : string
    Name     : string

  RecNum is an auto-generated sequence number. The CustRef field is linked to
  the customer table via 'RecNum'.  It would not be useful for a user to type
  in the record numbers. It would be far more useful to use the ID field. And
  also, what is to be displayed in the edit field would be the Name.

  The following are the primary properties of the TDBCodeEdit field.

  DataSource     - This is a TDataSource that is linked to the table in which
                   you are editing.
  DataField      - This is the field of the table in which you are editing. (Raw Data)

  LookupDatabase - This is a database containing a lookup table table for the
                   datafield being edited.
  LookupTable    - The name of the lookup table for the datafield being edited.
  LookupDisplay  - This is a field from the lookup table that will be displayed
                   in the edit field.
  LookupIndex    - This specifies the index of the lookup table to use when
                   searching the lookup table for a record, based on what the
                   user types into the field.  When the user types in an ID,
                   that text is used to search the lookup table for a match on
                   the index specified in this property.
                   If this property is blank, the the primary index is used.
  LinkIndex      - This field serves a dual purpose. Directly, it specifies the
                   index of the lookup table to use when searching the lookup
                   table using the Raw Data of the field. Indirectly, it
                   specifies the link field of the control. When storing data
                   from this control to the actual database record, the data
                   from this 'link' field is transferred from the lookup table
                   to the actual record field (raw data).
                   If this property is blank, the the primary index is used.

  In many cases the LinkIndex and the LookupIndex are the same thing. They
  will only differ when you have cases where the actual field being stored in
  the database table is different that a field you would like to use for
  searching for data in the lookup table.
}

(*--- Need to add an event to fire when data is not found
  --- Need to raise an exception when there is no primary index in the lookup
      table.
*)

interface

uses
  SysUtils, WinTypes, WinProcs, Messages, Classes, Graphics, Controls, Forms,
  Dialogs, DB, DBTables, DBCtrls, Mask, StdCtrls, Menus;

type
  {- this type defines what to do to data when looking up values }
  TLookupCase = (luNone, luUpper, luLower);

  TDBCodeEdit = class(TCustomMaskEdit)
  private
    FDataLink       : TFieldDataLink;  { field for data link }

    fLinkIndex      : string; // index name used to search the lookup table
                              // if blank then the primary key will be used
    fLookupIndex    : string;
    fLookupDisplay  : string;

    fLUTbl          : TTable; // lookup table
    fDisplayFld     : TField; // fld of lookup table to display in the edit field
    fLookupFld      : TField; //
    fLinkFld        : TField; // fld of lookup table to xfer to data record being edited
    fLookupFail     : TNotifyEvent;
    fLookupCase     : TLookupCase;
    {}
    procedure DataChange(Sender: TObject); { called when data (record) changes }
    procedure UpdateData(Sender: TObject); { called when control changes }

    {- DataField property }
    function GetDataField: string;
    procedure SetDataField(const newValue: string);

    {- DataSource property }
    function GetDataSource: TDataSource;
    procedure SetDataSource(newValue: TDataSource);

    function GetLookupIndex: string;
    procedure SetLookupIndex(const newValue: string);
    function GetLinkIndex: string;
    procedure SetLinkIndex(const newValue: string);
    function GetLookupTable: string;
    procedure SetLookupTable(const newValue: string);
    function GetLookupDisplay: string;
    procedure SetLookupDisplay(const newValue: string);
    function GetLookupDatabase: string;
    procedure SetLookupDatabase(const newValue: string);
    procedure SetReadOnly(newValue: boolean);
    function GetReadOnly: boolean;
    function GetRaw: string;
    procedure SetRaw(newValue: string);

    procedure CMExit(var Message: TCMExit); message CM_EXIT; { called to update data }
    function InitFields(leaveOpen: boolean): boolean;
    procedure WMCut(var Message: TMessage); message WM_CUT;
    procedure WMPaste(var Message: TMessage); message WM_PASTE;
    function GetIndexedField(anIndex: string): TField;
  protected
    { Protected declarations }
    procedure Change; override;
    procedure KeyDown(var Key: Word; Shift: TShiftState); override;
    procedure KeyPress(var Key: Char); override;
    function EditCanModify: boolean; override;
    procedure Loaded; override;
  public
    { Public declarations }
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
    procedure Refresh;
    property RawValue: string read GetRaw write SetRaw;
  published
    { Published declarations }
    property DataField: string  read GetDataField write SetDataField;
    property DataSource: TDataSource  read GetDataSource write SetDataSource;

    property LookupIndex: string read GetLookupIndex write SetLookupIndex;
    property LinkIndex: string read GetLinkIndex write SetLinkIndex;
    property LookupTable: string read GetLookupTable write SetLookupTable;
    property LookupDisplay: string read GetLookupDisplay write SetLookupDisplay;
    property LookupDatabase: string read GetLookupDatabase write SetLookupDatabase;
    property LookupCase: TLookupCase read fLookupCase write fLookupCase default luNone;
    property LookupTableClass: TTable read fLUTbl;

    property AutoSelect;
    property AutoSize;
    property BorderStyle;
    property CharCase;
    property Color;
    property Ctl3D;
    property DragCursor;
    property DragMode;
    property Enabled;
    property Font;
    property MaxLength;
    property ParentColor;
    property ParentCtl3D;
    property ParentFont;
    property ParentShowHint;
    property PasswordChar;
    property PopupMenu;
    property ReadOnly: Boolean read GetReadOnly write SetReadOnly default False;
    property ShowHint;
    property TabOrder;
    property TabStop;
    property Visible;
    property OnChange;
    property OnClick;
    property OnDblClick;
    property OnDragDrop;
    property OnDragOver;
    property OnEndDrag;
    property OnEnter;
    property OnExit;
    property OnLookupFail: TNotifyEvent read fLookupFail write fLookupFail;
    property OnKeyDown;
    property OnKeyPress;
    property OnKeyUp;
    property OnMouseDown;
    property OnMouseMove;
    property OnMouseUp;
  end;


IMPLEMENTATION

type
  ENotFound = class(Exception);

constructor TDBCodeEdit.Create(AOwner: TComponent);
begin
  inherited Create(AOwner); { Always call the inherited constructor }

  fLUTbl:= TTable.Create(self);

  FDataLink:= TFieldDataLink.Create; { construct data-link object }
  FDataLink.OnDataChange:= DataChange; { attach method to event }
  FDataLink.OnUpdateData:= UpdateData; { attach method to event }

  fLinkIndex:= emptyStr;
  fLookupIndex:= emptyStr;
  fLookupDisplay:= emptyStr;
  fLookupCase:= luNone;
end;

destructor TDBCodeEdit.Destroy;
begin
  FDataLink.Free; { dispose of data-link object }
  fLUTbl.Free;
  inherited Destroy; { then call inherited destructor }
end;

procedure TDBCodeEdit.DataChange(Sender: TObject);
{- called by field link when record changes }
begin
  if (fDataLink.Field <> nil) and not (csLoading in ComponentState) then
  begin
    if (LookupDatabase = '') or (LookupTable = '') or (fDisplayFld = nil) then
      // if no lookup information is provided then just show the actual field data
      text:= fDataLink.Field.AsString
    else
    begin
      if InitFields(true) then
      begin
        fLUTbl.IndexFieldNames:= LinkIndex;
        try
          if fLUTbl.FindKey([fDataLink.Field.AsString]) then
            text:= fDisplayFld.AsString
          else
            text:= EmptyStr;
        except
          raise;
        end;
        fLUTbl.active:= false;
      end;
    end;
  end
  else
    text:= Name;
end;

procedure TDBCodeEdit.UpdateData(Sender: TObject);
{- transfer data from edit control to record }
{  UpdateData is only called after calls to both FDataLink.Modified and
  FDataLink.UpdateRecord. }
var
  luData  : string;
begin
  {- first, locate record in lookup table }
  if fDataLink.field = nil then exit;

  if (text = '') or (LookupTable = '') or (fLookupFld = nil) or
    (fDisplayFld = nil) then
    FDataLink.Field.text:= text
  else
  begin
    if InitFields(true) then
    begin
      fLUTbl.IndexFieldNames:= LookupIndex; // set lookup index
      luData:= text;
      if fLookupCase <> luNone then
        if fLookupCase = luUpper then
          luData:= AnsiUpperCase(luData)
        else
          luData:= AnsiLowerCase(luData);
      try
        if fLUTbl.FindKey([luData]) then
        begin
          fDataLink.Field.text:= fLinkFld.AsString;
          text:= fDisplayFld.AsString;
        end
        else
        begin
          raise ENotFound.Create('Value not fould in lookup table.');
        end;
      except
        raise;
      end;
      fLUTbl.active:= false;
    end;
  end;
end;

procedure TDBCodeEdit.Refresh;
{- refresh the control with the database data }
begin
  DataChange(self);
end;

procedure TDBCodeEdit.Loaded;
begin
  inherited;
  InitFields(false);
end;

procedure TDBCodeEdit.Change;
begin
  if fDataLink.Editing then
    fDataLink.Modified;
  inherited Change;   { and call inherited, which calls event handler }
end;

procedure TDBCodeEdit.CMExit(var Message: TCMExit);
begin
  try
    fDataLink.UpdateRecord;
  except
    on e:ENotFound do
    begin
      if Assigned(fLookupFail) then
        fLookupFail(self)
      else
      begin
        SelectAll;
        SetFocus;
        raise;
      end;
    end
    else
      raise;
  end;
{  SetFocused(False);
  SetCursor(0);}
  DoExit;
end;

{- property access methods }

function TDBCodeEdit.GetDataField: string;
begin
  result:= FDataLink.FieldName;
end;

procedure TDBCodeEdit.SetDataField(const newValue: string);
begin
  FDataLink.FieldName:= newValue; { pass through field name to data link }
end;

function TDBCodeEdit.GetDataSource: TDataSource;
begin
  result:= FDataLink.DataSource; { pass through data source from data link }
end;

procedure TDBCodeEdit.SetDataSource(newValue: TDataSource);
begin
  if newValue <> fDataLink.dataSource then
  begin
    FDataLink.DataSource:= newValue; { pass through data source to data link }
  end;
end;

function TDBCodeEdit.GetLookupIndex: string;
begin
  result:= fLookupIndex;
end;

procedure TDBCodeEdit.SetLookupIndex(const newValue: string);
begin
  fLookupIndex:= newValue;
  InitFields(false);
  DataChange(Self);
end;

function TDBCodeEdit.GetLinkIndex: string;
begin
  result:= fLinkIndex;
end;

procedure TDBCodeEdit.SetLinkIndex(const newValue: string);
begin
  fLinkIndex:= newValue;
  InitFields(false);
  DataChange(Self);
end;

function TDBCodeEdit.GetLookupTable: string;
begin
  result:= fLUtbl.tableName;
end;

procedure TDBCodeEdit.SetLookupTable(const newValue: string);
begin
  fLUTbl.tableName:= newValue;  {- table must at least have a primary key }
  InitFields(false);
  DataChange(Self);
end;

function TDBCodeEdit.GetLookupDatabase: string;
begin
  result:= fLUTbl.databaseName;
end;

procedure TDBCodeEdit.SetLookupDatabase(const newValue: string);
begin
  if newValue <> fLUTbl.DatabaseName then
  begin
    fLUTbl.DatabaseName:= newValue;
    InitFields(false);
    DataChange(Self);
  end;
end;

function TDBCodeEdit.GetLookupDisplay: string;
begin
  result:= fLookupDisplay;
end;

procedure TDBCodeEdit.SetLookupDisplay(const newValue: string);
begin
  fLookupDisplay:= newValue;
  try
    InitFields(false);
    DataChange(Self);
  except
    on e:EDatabaseError do
    begin
      application.MessageBox(PChar(e.Message), 'TDBCodeEdit', MB_ICONERROR or MB_OK);
      fLookupDisplay:= emptyStr;
    end
    else
      raise;
  end;
end;

function TDBCodeEdit.InitFields(leaveOpen: boolean): boolean;
begin
if csLoading in componentstate then exit;
  result:= true;
  fDisplayFld:= nil;
  fLookupFld:= nil;
  fLinkFld:= nil;

  if (fLUtbl = nil) or (LookupTable = '') or (LookupDatabase = '') then exit;
  try
    fLUTbl.active:= true;
  except
    result:= false;
    if not (csLoading in ComponentState) then
      raise;
  end;
  fLinkFld:= GetIndexedField(LinkIndex);
  fLookupFld:= GetIndexedField(LookupIndex);
  if LookupDisplay = '' then
    fDisplayFld:= fLookupFld
  else
    fDisplayFld:= fLUtbl.FieldByName(LookupDisplay);

  fLUTbl.active:= leaveOpen;
end;

function TDBCodeEdit.GetIndexedField(anIndex: string): TField;
begin
  fLUtbl.IndexFieldNames:= anIndex;
  fLUtbl.IndexDefs.Update;
  if fLUtbl.IndexFieldCount > 0 then
    result:= fLUtbl.IndexFields[0]
  else
    result:= nil;
end;

procedure TDBCodeEdit.WMPaste(var Message: TMessage);
begin
  fDataLink.Edit;
  inherited;
end;

procedure TDBCodeEdit.WMCut(var Message: TMessage);
begin
  fDataLink.Edit;
  inherited;
end;

procedure TDBCodeEdit.KeyDown(var Key: Word; Shift: TShiftState);
begin
  inherited KeyDown(Key, Shift);
  if (key = VK_BACK) or (Key = VK_DELETE) or ((Key = VK_INSERT) and (ssShift in Shift)) then
    fDataLink.Edit;
end;

procedure TDBCodeEdit.KeyPress(var Key: Char);
begin
  inherited KeyPress(Key);
  if {not (Key in [#32..#255]) or} (fDataLink.Field = nil) then
{    and not fDataLink.Field.IsValidChar(Key) then}
  begin
    MessageBeep(0);
    Key:= #0;
  end;
  case Key of
    ^G, ^H, ^V, ^X, #32..#255:
      fDataLink.Edit;
    #27:
      begin
        FDataLink.Reset;
        SelectAll;
        Key:= #0;
      end;
  end;
end;

function TDBCodeEdit.EditCanModify: Boolean;
begin
  result:= fDataLink.Edit;
result:= true;
end;

function TDBCodeEdit.GetReadOnly: Boolean;
begin
  Result:= FDataLink.ReadOnly;
end;

procedure TDBCodeEdit.SetReadOnly(newValue: boolean);
begin
  FDataLink.ReadOnly:= newValue;
end;

function TDBCodeEdit.GetRaw: string;
// return the raw value from the edit field.
// This is the field that will be stored in the database.
begin
  if (fDataLink.Field <> nil) then
    result:= fDataLink.Field.AsString
  else
    result:= '';
end;

procedure TDBCodeEdit.SetRaw(newValue: string);
begin
  if (fDataLink.Field <> nil) then
  begin
    fDataLink.Field.AsString:= newValue;
{    text:= newValue;
    DataChange(self);}
  end;
end;

end.
