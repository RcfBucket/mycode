unit FadeLabel;

(* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
   TFadeLabel - by Joseph Good, greendot@yahoo.com

     A label component derived from TCustomLabel that produces a nice gradient
     fade behind the label text.

     Date:     04/07/1998
     Version:  1.0

     Extra property:
       BackColor: TColor - the color the gradient fades into.

     Modified property:
       Color: TColor - the main background color.

     Extra Event(s):
       OnSysColorChange - fires off when the SYSCOLORCHANGE event goes off.

(* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *)

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, StdCtrls;

type
  TFadeLabel = class(TCustomLabel)
  private
    { Private declarations }
    fFadeTemplate: TBitmap;
    fTopTemplate: TBitmap;
    fEdgeTemplate: TBitmap;
    fSolidTemplate: TBitmap;
    fDisplayBmp: TBitmap;
    fFadeBmp: TBitmap;
    fTopBmp: TBitmap;
    fEdgeBmp: TBitmap;
    fSolidBmp: TBitmap;
    fFadeColor: TColor;
    fBackColor: TColor;
    fOnColorChange: TNotifyEvent;
    procedure fSetFadeColor(color: TColor);
    procedure fSetBackcolor(color: TColor);
  protected
    { Protected declarations }
    procedure MakeFade;
    procedure SysColorChange(var Message: TMessage); message CM_SYSCOLORCHANGE;
    procedure ParentColorChanged(var Message: TMessage); message CM_PARENTCOLORCHANGED;
    function  GetLabelText: String; override;
  public
    { Public declarations }
    constructor Create(aOwner: TComponent); override;
    destructor Destroy; override;
    procedure Paint; override;
  published
    { Published declarations }
    property Color: TColor read fFadeColor write fSetFadeColor;
    property BackColor: TColor read fBackColor write fSetBackColor;
    property OnSysColorChange: TNotifyEvent read fOnColorChange write fOnColorChange;
    property Align;
    property Alignment;
    property AutoSize;
    property Caption;
    property DragCursor;
    property DragMode;
    property Enabled;
    property FocusControl;
    property Font;
    property ParentColor;
    property ParentFont;
    property ParentShowHint;
    property PopupMenu;
    property ShowAccelChar;
    property ShowHint;
    property Layout;
    property Visible;
    property OnClick;
    property OnDblClick;
    property OnDragDrop;
    property OnDragOver;
    property OnEndDrag;
    property OnMouseDown;
    property OnMouseMove;
    property OnMouseUp;
    property OnStartDrag;
  end;

procedure Register;

implementation

{$R FadeLabel.RES}

function ColorDarken(OriginalColor: TColor; Phase: Integer): TColor;
(* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
   Darkens the given color by the given phase (R,G,B)
(* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *)
var
   R, G, B: Integer;
   WinColor: Integer;
begin
     // Convert it into the Win32 API color spec
     WinColor := ColorToRGB(OriginalColor);
     R := GetRValue(WinColor);
     G := GetGValue(WinColor);
     B := GetBValue(WinColor);

     R := R - Phase;
     G := G - Phase;
     B := B - Phase;

     if R < 0 then R := 0;
     if G < 0 then G := 0;
     if B < 0 then B := 0;

     result := RGB(R, G, B);
end;

function ColorLighten(OriginalColor: TColor; Phase: Integer): TColor;
(* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
   Lightens the given color by the given phase (R,G,B)
(* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *)
var
   R, G, B: Integer;
   WinColor: Integer;
begin
     // Convert it into the Win32 API color spec
     WinColor := ColorToRGB(OriginalColor);
     R := GetRValue(WinColor);
     G := GetGValue(WinColor);
     B := GetBValue(WinColor);

     R := R + Phase;
     G := G + Phase;
     B := B + Phase;

     if R > 255 then R := 255;
     if G > 255 then G := 255;
     if B > 255 then B := 255;

     result := RGB(R, G, B);
end;

function ColorBlendEx(const R1, G1, B1, R2, G2, B2: Integer; Percentage: Single): TRgbTriple;
(* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
   Calculates the blending of two colors given the percentage of blend.
(* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *)
var
   NewR, NewG, NewB: Integer;
   ratio: Single;
begin
     // Seed our result
     result.rgbtRed := 0; result.rgbtGreen := 0; result.rgbtBlue := 0;

     // Calculate the ratio of the min to max via pos given
     if Percentage < 0.0 then Percentage := 0.0;
     if Percentage > 100.0 then Percentage := 100.0;
     ratio := Percentage / 100.0;

     // Get our new color
     if R1 > R2 then
        NewR := R1 - round((R1 - R2) * ratio)
     else
        NewR := R1 + round((R2 - R1) * ratio);

     if G1 > G2 then
        NewG := G1 - round((G1 - G2) * ratio)
     else
        NewG := G1 + round((G2 - G1) * ratio);

     if B1 > B2 then
        NewB := B1 - round((B1 - B2) * ratio)
     else
        NewB := B1 + round((B2 - B1) * ratio);

     // Make sure we are within limits
     if NewR > 255 then NewR := 255;
     if NewR < 0 then NewR := 0;
     if NewG > 255 then NewG := 255;
     if NewG < 0 then NewG := 0;
     if NewB > 255 then NewB := 255;
     if NewB < 0 then NewB := 0;

     // Set our result
     result.rgbtRed := NewR;
     result.rgbtBlue := NewB;
     result.rgbtGreen := NewG;
end;

//---------------------------------------------------------------------------------------------
function ColorBlend(const Color1, Color2: TColor; Percentage: Single): TColor;
(* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
   See ColorBlendEx
(* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *)
var
   R1, G1, B1: Integer;
   R2, G2, B2: Integer;
   WinColor: Integer;
   rgbt: TRgbTriple;
begin
     // Seed our result
     result := Color1;

     if (Color1 = Color2) then exit;

     // Get the RGB values for both colors
     WinColor := ColorToRGB(Color1);            // Convert the color into the WIN32 color spec
     R1 := GetRValue(WinColor);                 // Get red
     G1 := GetGValue(WinColor);                 // Get green
     B1 := GetBValue(WinColor);                 // Get blue
     WinColor := ColorToRGB(Color2);            // Convert the color into the WIN32 color spec
     R2 := GetRValue(WinColor);
     G2 := GetGValue(WinColor);
     B2 := GetBValue(WinColor);

     rgbt := ColorBlendEx(R1, G1, B1, R2, G2, B2, Percentage);

     result := RGB(rgbt.rgbtRed, rgbt.rgbtGreen, rgbt.rgbtBlue);
end;

procedure TintBlueAlphaBitmap(Template, NewMap: TBitmap; Tint, BackColor: TColor);
(* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
   This treats the blue channel of a bitmap as the transparency.  The red and
   green are treated as the grey levels.

   It is just another attempt at getting transparent bitmaps.. truely transparent
   bitmaps on my form.  Cool FX..

   Blue is a value of 0..255 which is the amount of transparency.
   Red, Green are values of 0..255.  When they are the same, they are treated
   as a transparent grey, and mapped to a tint vs backColor.

   Keep in mind that the level of gray represents a "phase" between the black
   and white.

   128 is the mid-point.  Wherever a color sits between black and 128-gray,
   it shifts that much of a percentage towards one.. not the actual amount.

(* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *)
const
   MIDGREY = 128;
var
   x, y: Integer;
   grey, R, G, B: Integer;
   NewColor: TColor;
   phase, bluePhase: Integer;
begin
   if (Template = nil) or (NewMap = nil) then exit;

   // Resize the colormap bitmap
   NewMap.Width := Template.Width;
   NewMap.Height := Template.Height;

   Tint := ColorToRGB(tint);                                         // strip that borland stuff from it
   BackColor := ColorToRGB(BackColor);                               // ...

   with Template do begin                                            // go thru each pixel in template
        for y := 0 to Height do                                      // ...
            for x := 0 to Width do begin                             // ...
                grey := ColorToRGB(Canvas.Pixels[x,y]);              // strip borland bits from color

                R := GetRValue(grey);                                // get color values
                G := GetGValue(grey);                                // ...
                B := GetBValue(grey);                                // ...
                if (R <> G) then continue;                           // We need R and G to be equal
                if (B = 0) then                                      // If no transparency
                   if (R = 0) then begin
                      NewMap.Canvas.Pixels[x, y] := clBlack;
                      continue;                                      //   Skip on pure black
                   end
                   else
                   if (R = 255) then begin
                      NewMap.Canvas.Pixels[x, y] := clWhite;
                      continue;                                      //   Skip on pure white
                   end; {if/else}

                if (B = 255) then begin                              // If we're pure transparent
                   NewMap.Canvas.Pixels[x,y] := BackColor;           // Set to back color
                   continue;                                         // Then go to next pixel
                end; {if}

                if (R < MIDGREY) then begin                          // Pixel is dark
                   phase := MIDGREY - r;                             // How far away from center?
                   phase := phase * 2;                               // full range of 256 colors
                   NewColor := ColorDarken(tint, phase);             // tint the pixel towards black
                end
                else
                if (R > MIDGREY) then begin                          // Pixel is light
                   phase := r - MIDGREY;                             // How far from center
                   phase := phase * 2;                               // expand to 255 colors
                   NewColor := ColorLighten(tint, phase);            // tint towards white
                end
                else begin
                   NewColor := tint;                                 // It's pure, just tint it
                end; {if/else/else}

                if (B > 0) then begin                                // shifting towards background?
                   bluePhase := round((B / 255) * 100);              // turn it into percentage
                   NewColor := ColorBlend(NewColor,
                                          BackColor, bluePhase);     // do the shift
                end; {if}

                NewMap.Canvas.Pixels[x,y] := NewColor;
            end; {for}
   end; {with}
end;

//---------------------------------------------------------------------------------------------
constructor TFadeLabel.Create(aOwner: TComponent);
begin
   inherited;
   fFadeTemplate := TBitmap.Create;
   fTopTemplate := TBitmap.Create;
   fEdgeTemplate := TBitmap.Create;
   fSolidTemplate := TBitmap.Create;

   fFadeBmp := TBitmap.Create;
   fTopBmp := TBitmap.Create;
   fEdgeBmp := TBitmap.Create;
   fSolidBmp := TBitmap.Create;

   fDisplayBmp := TBitmap.Create;

   fFadeTemplate.LoadFromResourceName(HInstance, 'FADELABELBODYFADE');
   fSolidTemplate.LoadFromResourceName(HInstance, 'FADELABELBODYSOLID');
   fTopTemplate.LoadFromResourceName(HInstance, 'FADELABELTOP');
   fEdgeTemplate.LoadFromResourceName(HInstance, 'FADELABELEDGE');

   fFadeBmp.Assign(fFadeTemplate);
   fSolidBmp.Assign(fSolidTemplate);
   fTopBmp.Assign(fTopTemplate);
   fEdgeBmp.Assign(fEdgeTemplate);

   fBackColor := clBtnFace;
   fFadeColor := clWhite;
   MakeFade;

   Transparent := True;
   WordWrap := False;
end;

//---------------------------------------------------------------------------------------------
destructor TFadeLabel.Destroy;
begin
   if assigned(fFadeTemplate) then
      fFadeTemplate.Free;
   if assigned(fTopTemplate) then
      fTopTemplate.Free;
   if assigned(fDisplayBmp) then
      fDisplayBmp.Free;
   if assigned(fEdgeTemplate) then
      fEdgeTemplate.Free;
   if assigned(fSolidTemplate) then
      fSolidTemplate.Free;
   if assigned(fFadeBmp) then
      fFadeBmp.Free;
   if assigned(fTopBmp) then
      fTopBmp.Free;
   if assigned(fEdgeBmp) then
      fEdgeBmp.Free;
   if assigned(fSolidBmp) then
      fSolidBmp.Free;
   inherited;
end;

//---------------------------------------------------------------------------------------------
procedure TFadeLabel.SysColorChange(var Message: TMessage);
begin
   if assigned(fOnColorChange) then
      fOnColorChange(Self);
end;

//---------------------------------------------------------------------------------------------
procedure TFadeLabel.ParentColorChanged(var Message: TMessage);
var
   tmpColor: TColor;
begin
   if ParentColor then begin
      if (Message.wParam <> 0) then
         tmpColor := TColor(Message.lParam)
      else
         tmpColor := Parent.Brush.Color;

      if (tmpColor <> fBackColor) then begin
         fBackColor := tmpColor;
         MakeFade;
         Paint;
      end; {if}
  end;
end;

//---------------------------------------------------------------------------------------------
function TFadeLabel.GetLabelText: String;
(* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
   I add the extra space at the beginning so the text doesn't hug the border.
   The spaces at the end extend the fade so when AutoSize is set to true, things
   look OK.
(* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *)
begin
   result := ' ' + Caption + '          ';
end;

//---------------------------------------------------------------------------------------------
procedure TFadeLabel.Paint;
(* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
   We have four bitmap pieces:

     fFadeBmp   - the fade part of the body
     fTopBmp    - the fade top line
     fEdgeBmp   - the non-fade left edge
     fSolidBmp  - the non-fade part of body

   These bmp's should have been pre-tinted.  They get tinted upon creation and
   when the Color and BackColor properties are changed.

   Here, we just assemble them into the given client area.

(* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *)
var
   cRect, tmpRect: TRect;
begin
   if (csLoading in ComponentState) then exit;

   cRect := ClientRect;
   with Canvas do begin
        SetStretchBltMode(Handle, HALFTONE);
        SetBrushOrgEx(Handle, 0, 0, nil);

        // TOP EDGE
        tmpRect := cRect;
        tmpRect.Bottom := tmpRect.Top + fTopBmp.Height;
        StretchDraw(tmpRect, fTopBmp);

        // LEFT EDGE
        tmpRect := cRect;
        inc(tmpRect.Top);
        tmpRect.Right := tmpRect.Left + fEdgeBmp.Width;
        StretchDraw(tmpRect, fEdgeBmp);

        // BODY SOLID
        tmpRect := cRect;
        inc(tmpRect.Top);
        dec(tmpRect.Right, ((tmpRect.Right - tmpRect.Left) div 2));
        inc(tmpRect.Left);
        StretchDraw(tmpRect, fSolidBmp);

        // BODY FADE
        tmpRect := cRect;
        inc(tmpRect.Top);
        inc(tmpRect.Left, ((tmpRect.Right - tmpRect.Left) div 2));
        StretchDraw(tmpRect, fFadeBmp);
   end; {with}

   inherited;
end;

//---------------------------------------------------------------------------------------------
procedure TFadeLabel.MakeFade;
(* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
   Goes thru and translates all the weird yellow/blue combos into a tinted alpha-
   blended bitmap.  :)
(* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *)
begin
   TintBlueAlphaBitmap(fFadeTemplate, fFadeBmp, fFadeColor, fBackColor);
   TintBlueAlphaBitmap(fTopTemplate, fTopBmp, fFadeColor, fBackColor);
   TintBlueAlphaBitmap(fEdgeTemplate, fEdgeBmp, fFadeColor, fBackColor);
   TintBlueAlphaBitmap(fSolidTemplate, fSolidBmp, fFadeColor, fBackColor);
end;

//---------------------------------------------------------------------------------------------
procedure TFadeLabel.fSetFadeColor(color: TColor);
begin
   if (color = fFadeColor) then exit;
   fFadeColor := color;
   MakeFade;
   Paint;
end;

//---------------------------------------------------------------------------------------------
procedure TFadeLabel.fSetBackColor(color: TColor);
begin
   if (color = fBackColor) then exit;
   ParentColor := False;
   fBackColor := color;
   MakeFade;
   Paint;
end;

//---------------------------------------------------------------------------------------------
procedure Register;
begin
  RegisterComponents('Custom', [TFadeLabel]);
end;

end.
