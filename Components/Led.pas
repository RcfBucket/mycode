unit LED;

{.$DEFINE DEBUG}

{- Numeric LED Display

   This component displays numbers and letters in an LED style.
}

INTERFACE

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls;

const
  DefaultLEDValue   = '88-88';
  DefaultLEDWidth   = Length(DefaultLEDValue);

type
  EInvalidCellStyle = class(Exception);

  TLEDColor     = (clRed, clLime, clYellow, clBlue, clFuchsia, clAqua, clWhite);
  TLEDHorzAlign = (LEDLeft, LEDCenter, LEDRight);
  TLEDVertAlign = (LEDTop, LEDMiddle, LEDBottom);
  TLEDStyle     = (LEDNumeric, LEDAlpha);

  TLED = class(TCustomPanel)
  private
    fLEDColor     : TLEDColor;
    fLEDValue     : string;
    fLEDbm        : TBitMap;
    fLEDWidth     : integer;
    fVertAlign    : TLEDVertAlign;
    fHorzAlign    : TLEDHorzAlign;
    fAutoSize     : boolean;
    fLEDStyle     : TLEDStyle;
    fCellWidth    : byte;
    fCellHeight   : byte;
    fCellAlign    : TLEDHorzAlign;
    fInterior     : TColor;

    // Get/Set property routines
    procedure SetLEDColor(aColor: TLEDColor);
    procedure SetLEDValue(aValue: string);
    procedure SetLEDWidth(aWidth: integer);
    procedure SetHorzAlign(anAlign: TLEDHorzAlign);
    procedure SetVertAlign(anAlign: TLEDVertAlign);
    procedure SetLEDStyle(aStyle: TLEDStyle);
    procedure SetCellAlign(aAlign: TLEDHorzAlign);
    procedure SetInterior(anInterior: TColor);

    function  GetEdgeWidths(index: integer): integer;
    procedure SetEdgeWidths(index, aWidth: integer);
    function  GetPanelBevels(index: integer): TPanelBevel;
    procedure SetPanelBevels(index: integer; aBevel: TPanelBevel);
    procedure SetAutoSize(aAuto: boolean);
    procedure SetBorderStyle(aStyle: TBorderStyle);
    function  GetBorderStyle: TBorderStyle;

    // support routines
    procedure LoadLEDBitmap;
    function TotalBorderWidth : integer;
    procedure CalcClientRect(var rect: TRect);
    procedure CalcStartingRect(var rect: TRect);
    function GetBitmapRectFromChar(aChar: char): TRect;
    procedure CalcCellSizes;
    procedure PaintBitmap;
  protected
    procedure Paint; override;
  public
    constructor Create(aOwner: TComponent); override;
    destructor Destroy; override;
  published
    property LEDColor : TLEDColor
      read fLEDColor write SetLEDColor default clLime;
    property LEDValue: string
      read fLEDValue write SetLEDValue;
    property LEDWidth: integer
      read fLEDWidth write SetLEDWidth default DefaultLEDWidth;
    property LEDHorzAlign: TLEDHorzAlign
      read fHorzAlign write SetHorzAlign default LEDLeft;
    property LEDVertAlign: TLEDVertAlign
      read fVertAlign write SetVertAlign default LEDTop;
    property AutoSize: boolean
      read fAutoSize write SetAutoSize default true;
    property LEDStyle: TLEDStyle
      read fLEDStyle write SetLEDStyle default LEDNumeric;
    property LEDCellAlign: TLEDHorzAlign
      read fCellAlign write SetCellAlign default LEDLeft;
    property Interior: TColor
      read fInterior write SetInterior;

    // expose some properties from the Ancestor class
    property Align;
    property BevelWidth: integer index 0
      read GetEdgeWidths write SetEdgeWidths default 1;
    property BorderWidth: integer index 1
      read GetEdgeWidths write SetEdgeWidths default 2;
    property BevelInner: TPanelBevel index 0
      read GetPanelBevels write SetPanelBevels default bvLowered;
    property BevelOuter: TPanelBevel index 1
      read GetPanelBevels write SetPanelBevels default bvRaised;
    property BorderStyle: TBorderStyle
      read GetBorderStyle write SetBorderStyle default bsSingle;
    property Color;
    property OnClick;
    property OnDblClick;
    property OnDragDrop;
    property OnDragOver;
    property OnEndDrag;
    property OnEnter;
    property OnExit;
    property OnMouseDown;
    property OnMouseUp;
    property OnResize;
    property OnStartDrag;
  end;

procedure Register;

IMPLEMENTATION

{$R LED.RES}

const
  NumLEDResNames : array[TLEDColor] of string[25] =
    ('LED_NUM_RED', 'LED_NUM_LIME', 'LED_NUM_YELLOW', 'LED_NUM_BLUE',
     'LED_NUM_FUCHSIA', 'LED_NUM_AQUA', 'LED_NUM_WHITE');

  AlphaLEDResNames : array[TLEDColor] of string[25] =
    ('LED_ALPHA_RED', 'LED_ALPHA_LIME', 'LED_ALPHA_YELLOW', 'LED_ALPHA_BLUE',
     'LED_ALPHA_FUCHSIA', 'LED_ALPHA_AQUA', 'LED_ALPHA_WHITE');

constructor TLED.Create(aOwner: TComponent);
begin
  inherited Create(aOwner);

  // Setup some ancestor properties
  ControlStyle:= ControlStyle - [csSetCaption];
  inherited BevelInner:= bvLowered;
  inherited BevelOuter:= bvRaised;
  inherited BorderStyle:= bsSingle;
  inherited BevelWidth:= 1;
  inherited BorderWidth:= 2;
  parentCtl3D:= false;
  Ctl3D:= false;

  fLEDbm:= TBitMap.Create;    // load the propert bitmap
  fLEDStyle:= LEDNumeric;
  CalcCellSizes;
  fLEDColor:= clLime;
  LoadLEDBitmap;

  fCellAlign:= LEDLeft;
  fLEDValue:= DefaultLEDValue;
  fLEDWidth:= DefaultLEDWidth;
  fVertAlign:= LEDTop;
  fHorzAlign:= LEDLeft;
  fInterior:= Color;
  fAutoSize:= true;
  if fAutoSize then
    SetAutoSize(true)
  else
  begin
    Width:= TotalBorderWidth + (fLEDWidth * fCellWidth);
    Height:= TotalBorderWidth + fCellHeight;
  end;
end;

destructor TLED.Destroy;
begin
  inherited Destroy;
  fLEDbm.Free;
end;

procedure TLED.SetLEDColor(aColor: TLEDColor);
begin
  if aColor <> fLEDColor then
  begin
    if (aColor >= clRed) and (aColor <= clWhite) then
    begin
      fLEDColor:= aColor;
      LoadLEDBitmap;
    end;
  end;
end;

procedure TLED.SetLEDValue(aValue: string);
begin
  if (aValue <> fLEDValue) then
  begin
    fLEDValue:= UpperCase(aValue);
    PaintBitmap;
  end;
end;

procedure TLED.SetLEDWidth(aWidth: integer);
begin
  if (aWidth > 0) and (aWidth <= 255) then
  begin
    fLEDWidth:= aWidth;
    if fAutoSize then
      SetAutoSize(true)
    else
      Invalidate;
  end;
end;

procedure TLED.SetHorzAlign(anAlign: TLEDHorzAlign);
begin
  if (anAlign <> fHorzAlign) then
  begin
    fHorzAlign:= anAlign;
    Invalidate;
  end;
end;

procedure TLED.SetVertAlign(anAlign: TLEDVertAlign);
begin
  if (anAlign <> fVertAlign) then
  begin
    fVertAlign:= anAlign;
    Invalidate;
  end;
end;

procedure TLED.SetEdgeWidths(index, aWidth: integer);
// This function is used to intercept setting of the inherited
// BorderWidth and BevelWidth properties of TCustomPanel.
// The index is 0 when setting BevelWidth and 1 when setting BorderWidth
// The interception was required to perform auto-sizing of the TLED panel
begin
  if index = 0 then
    inherited BevelWidth:= aWidth
  else
    inherited BorderWidth:= aWidth;
  if fAutoSize then
    SetAutoSize(true);
end;

function TLED.GetEdgeWidths(index: integer): integer;
// see comment for SetBorderWidths
begin
  if index = 0 then
    result:= inherited BevelWidth
  else
    result:= inherited BorderWidth;
end;

procedure TLED.SetPanelBevels(index: integer; aBevel: TPanelBevel);
// This function is used to intercept setting of the inherited
// BevelInner and BevelOuter properties of TCustomPanel.
// The index is 0 when setting BevelInner and 1 when setting BevelOuter
// The interception was required to perform auto-sizing of the TLED panel
begin
  if index = 0 then
    inherited BevelInner:= aBevel
  else
    inherited BevelOuter:= aBevel;
  if fAutoSize then
    SetAutoSize(true);
end;

function TLED.GetPanelBevels(index: integer): TPanelBevel;
begin
  if index = 0 then
    result:= inherited BevelInner
  else
    result:= inherited BevelOuter;
end;

procedure TLED.SetBorderStyle(aStyle: TBorderStyle);
begin
  if aStyle <> BorderStyle then
  begin
    inherited BorderStyle:= aStyle;
    if fAutoSize then
      SetAutoSize(true);
  end;
end;

function  TLED.GetBorderStyle: TBorderStyle;
begin
  result:= inherited BorderStyle;
end;

procedure TLED.SetAutoSize(aAuto: boolean);
{- Size the control to fit around the LED display }
begin
  fAutoSize:= aAuto;
  if fAutoSize then // if autosize specified then resize control to fit LED
  begin
    Width:= TotalBorderWidth + (fCellWidth * fLEDWidth);
    Height:= TotalBorderWidth + fCellHeight;
  end;
end;

procedure TLED.SetLEDStyle(aStyle: TLEDStyle);
begin
  if aStyle <> fLEDStyle then
  begin
    fLEDStyle:= aStyle;
    CalcCellSizes;
    LoadLEDBitmap;
    if fAutoSize then
      SetAutoSize(true);
    Invalidate;
  end;
end;

procedure TLED.SetCellAlign(aAlign: TLEDHorzAlign);
begin
  if aAlign <> fCellAlign then
  begin
    fCellAlign:= aAlign;
    PaintBitmap;
  end;
end;

procedure TLED.SetInterior(anInterior: TColor);
begin
  if fInterior <> anInterior then
  begin
    fInterior:= anInterior;
    Invalidate;
  end;
end;

procedure TLED.CalcCellSizes;
begin
  if fLEDStyle = LEDNumeric then
  begin
    fCellWidth:= 13;
    fCellHeight:=23;
  end
  else if fLEDStyle = LEDAlpha then
  begin
    fCellWidth:= 16;
    fCellHeight:= 22;
  end
  else
    raise EInvalidCellStyle.Create('Invalid Cell Style');
end;

procedure TLED.LoadLEDBitmap;
begin
  try
    case fLEDStyle of
      LEDNumeric:
        fLEDbm.LoadFromResourceName(hInstance, NumLEDResNames[fLEDColor]);
      LEDAlpha:
        fLEDbm.LoadFromResourceName(hInstance, AlphaLEDResNames[fLEDColor]);
    end;
  except
    raise EResNotFound.Create('LED bitmap resource not found.');
  end;
  Invalidate;
end;

function TLED.TotalBorderWidth : integer;
// return the width of the panels borders.
begin
  result:= BorderWidth * 2;
  if BorderStyle = bsSingle then
    Inc(result, 2);
  if BevelInner <> bvNone then
    Inc(result, BevelWidth * 2);
  if BevelOuter <> bvNone then
    Inc(result, BevelWidth * 2);
end;

procedure TLED.CalcClientRect(var rect: TRect);
// calculate the workable client area based on current settings of bevel
// and border widths
var
  totBorderWidth : integer;
begin
  rect:= GetClientRect;
  totBorderWidth:= BorderWidth;
  if BevelInner <> bvNone then
    Inc(totBorderWidth, BevelWidth);
  if BevelOuter <> bvNone then
    Inc(totBorderWidth, BevelWidth);
  InflateRect(rect, -totBorderWidth, -totBorderWidth); // shrink rect
end;

procedure TLED.CalcStartingRect(var rect: TRect);
// calculate the position of a the starting rectangle for the first LED
// number.  This takes into account the border widths and the Alignment
var
  client  : TRect;
  wRect   : TRect;
  totWidth: integer;  // total width of LED display
begin
  CalcClientRect(client);  {- get workable client rectangle }
  wRect:= GetClientRect;
  totWidth:= fLEDWidth * fCellWidth;
  case fHorzAlign of
    LEDCenter:
      rect.Left:= ((wRect.Right - wRect.Left) div 2) - (totWidth div 2);

    LEDRight:
      rect.Left:= client.right - totWidth;

    else // LEDLeft:
      rect.Left:= client.Left;
  end;

  case fVertAlign of
    LEDMiddle:
      rect.Top:= ((wRect.Bottom - wRect.Top) div 2) -  (fCellHeight div 2);

    LEDBottom:
      rect.Top:= client.Bottom - fCellHEight - 1;

    else // LEDTop
      rect.Top:= client.Top;
  end;
  rect.Right:= rect.Left + fCellWidth;
  rect.Bottom:= rect.Top + (fCellHeight);
end;

function TLED.GetBitmapRectFromChar(aChar: char): TRect;
// get a TRect structure that will bound the rectangle from the source
// bitmap for a specified number. Valid values of aNum are:
// '-'  = negative sign
// '0'..'9'
// all other values will result in returning the TRect for the BLANK LED
var
  index : integer;
begin
  result.top:= 0;
  result.bottom:= fCellHeight;
  index:= 0;

  if fLEDStyle = LEDNumeric then
  begin
    if aChar = '-' then
      index:= 0
    else if (aChar >= '0') and (aChar <= '9') then
      index:= 11 - StrToInt(aChar)
    else
      index:= 1;  // all others get TRect for BLANK LED
  end
  else if fLEDStyle = LEDAlpha then
  begin
    if (aChar < ' ') or (aChar > '_') then
      aChar:= ' ';
    index:= byte(aChar) - byte(' ');
  end;

  result.left:= index * fCellWidth;
  result.right:= result.left + (fCellWidth);
end;

procedure TLED.PaintBitmap;
var
  src, dst  : TRect;
  col       : integer;
  bmIdx     : integer;
  txtIdx    : integer;
begin
  with canvas do
  begin
    CalcStartingRect(dst);

    case fCellAlign of
      LEDRight:
        bmIdx:= fLEDWidth - Length(fLEDValue) + 1;
      LEDCenter:
        bmIdx:= (fLEDWidth div 2) - (Length(fLEDValue) div 2) + 1;
      else  // LEDLeft:
        bmIdx:= 1;
    end;
    if bmIdx < 1 then // ensure that calc's above did not produce bogus results
      bmIdx:= 1;

    txtIdx:= 1;
    for col:= 1 to fLEDWidth do
    begin
      if (col >= bmIdx) and (txtIdx <= Length(fLEDValue)) and
         (Length(fLEDValue) > 0) then
      begin
        src:= GetBitmapRectFromChar(fLEDValue[txtIdx]);
        Inc(txtIdx);
      end
      else
        src:= GetBitmapRectFromChar(' ');

      CopyRect(dst, fLEDbm.Canvas, src); // BitBlt bitmap to screen

      Inc(dst.left, fCellWidth);
      Inc(dst.right, fCellWidth);
    end;

{$IFDEF DEBUG}
    if csDesigning in ComponentState then
    begin
      pen.Color:= clBlue;
      dst:= GetClientRect;
      dst.left:= (dst.right - dst.left) div 2;
      MoveTo(dst.left, 0);
      LineTo(dst.left, dst.bottom);
      dst.Top:= (dst.bottom - dst.top) div 2;
      MoveTo(0, dst.top);
      LineTo(dst.right, dst.top);
    end;
{$ENDIF}
  end;
end;

procedure TLED.Paint;
var
  cRect : TRect;
begin
  inherited Paint;

  if fInterior <> Color then  {- if interior differs from the panel color }
  begin
    canvas.Brush.Color:= fInterior;
    CalcClientRect(cRect);
    canvas.FillRect(cRect);
  end;

  PaintBitmap;
end;

// REGISTRATION

procedure Register;
begin
  RegisterComponents('Custom', [TLED]);
end;

END.
