{
 Function: A data-aware spin control.

 Notes:
  [1] Will work on integer and float fields.
  [2] Will allow min and max values to be set on the control, which
      consequently will apply to the field itself (so the user can't
      go above or below those parameters).
  [3] If the control is used on a record, which *already* contains a value
      outside of the min\max parameters, then the control will *wrongly*
      state the min or max value for that particular record.
********************************************************************************}
unit dbspin;

interface

uses
  SysUtils, WinTypes, WinProcs, Messages, Classes, Graphics, Controls,
  Forms, Dialogs, StdCtrls, Spin, DB, DBCtrls, DBTables;

type
  TDBSpinEdit = class(TSpinEdit)
  private
    { Private declarations }
    FMinValue: extended;
    FMaxValue: extended;
    FDatalink: TFieldDataLink;
    FIncrement: extended;
    function GetDataField: string;
    function GetDataSource: TDataSource;
    procedure SetDataField(const value: string);
    procedure SetDataSource(value: TDataSource);
    procedure DataChange(Sender: TObject);
    procedure UpdateData(Sender: TObject); { called when control changes }
    function GetValue: extended;
    function CheckValue(NewValue: extended): extended;
    procedure SetValue(NewValue: extended);
    procedure CMExit(var Message: TCMExit); message CM_EXIT; { called to update data }
    procedure CMTextChanged(var Message: TMessage); message CM_TEXTCHANGED;
    procedure EditingChange(Sender: TObject);

  protected
    procedure Notification(AComponent: TComponent; Operation: TOperation); override;
    procedure KeyDown(var Key: Word; Shift: TShiftState); override;
    procedure KeyPress(var Key: Char); override;
    procedure UpClick(Sender: TObject); override;
    procedure DownClick(Sender: TObject); override;
    procedure Change; override;
  public
    constructor Create(aOwner: TComponent); override;
    destructor Destroy; override;
  published
    property DataField: string read GetDataField write SetDataField;
    property DataSource: TDataSource read GetDataSource write SetDataSource;
    property Increment: extended read FIncrement write FIncrement;
    property Value: extended read GetValue write SetValue;
    property MaxValue: extended read FMaxValue write FMaxValue;
    property MinValue: extended read FMinValue write FMinValue;
  end;

procedure Register;

implementation


{***VCL preferences*************************************************************}


{register}
procedure Register;
begin
  RegisterComponents('Custom', [TDBSpinEdit]);
end;


constructor TDBSpinEdit.Create(aOwner: TComponent);
begin
  inherited Create(aOwner);
  FDataLink:= TFieldDataLink.Create;
  FDataLink.Control := Self;
  FDataLink.OnDataChange:= DataChange;
  FDataLink.OnEditingChange := EditingChange;
  FDataLink.OnUpdateData:= UpdateData; { attach method to event }

  MaxValue:= 0.0;
  MinValue:= 0.0;
  Increment:= 1.0;
end;


destructor TDBSpinEdit.Destroy;
begin
 FDataLink.Free;
 inherited Destroy;
end;

procedure TDBSpinEdit.Notification(AComponent: TComponent;
  Operation: TOperation);
begin
  inherited Notification(AComponent, Operation);
  if (Operation = opRemove) and (FDataLink <> nil) and
    (AComponent = DataSource) then DataSource := nil;
end;

procedure TDBSpinEdit.KeyDown(var Key: Word; Shift: TShiftState);
begin
  inherited KeyDown(Key, Shift);
  if (Key = VK_DELETE) or ((Key = VK_INSERT) and (ssShift in Shift)) then
    FDataLink.Edit;
end;

procedure TDBSpinEdit.KeyPress(var Key: Char);
begin
  inherited KeyPress(Key);
  if (Key in [#32..#255]) and (FDataLink.Field <> nil) and
    not FDataLink.Field.IsValidChar(Key) then
  begin
    MessageBeep(0);
    Key := #0;
  end;
  case Key of
    ^H, ^V, ^X, #32..#255:
      FDataLink.Edit;
    #27:
      begin
        FDataLink.Reset;
        SelectAll;
        Key := #0;
      end;
  end;
end;

procedure TDBSpinEdit.UpClick(Sender: TObject);
begin
  {- put in edit mode}
  if FDataLink.DataSource.State <> dsEdit then
    FDataLink.Edit;

  {increment by value}
  if (MinValue = MaxValue) or not (( Value + increment ) > MaxValue) then
    case FDataLink.Field.DataType of
      ftInteger:
        FDataLink.Field.AsInteger:= Trunc(Value) + round(increment);
      ftSmallint:
        FDataLink.Field.AsInteger:= Trunc(Value) + round(increment);
      ftFloat:
        FDataLink.Field.AsFloat:= Value + increment;
      ftCurrency:
        FDataLink.Field.AsFloat:= Value + increment;
    end;
end;

procedure TDBSpinEdit.DownClick(Sender: TObject);
begin
  {- put in edit mode}
  if FDataLink.DataSource.State <> dsEdit then
    FDataLink.Edit;

  {- decrease by value}
  if (MinValue = MaxValue) or not (( Value - increment ) < MinValue) then
  case FDataLink.Field.DataType of
    ftInteger:
      FDataLink.Field.AsInteger:= Trunc(Value) - Round(increment);
    ftSmallint:
      FDataLink.Field.AsInteger:= Trunc(Value) - Round(increment);
    ftFloat:
      FDataLink.Field.AsFloat:= Value - increment;
    ftCurrency:
      FDataLink.Field.AsFloat:= Value - increment;
  end;
end;

procedure TDBSpinEdit.EditingChange(Sender: TObject);
begin
  inherited ReadOnly:= not fDataLink.Editing;
end;

procedure TDBSpinEdit.DataChange(sender: TObject);
begin
  if FDataLink.Field <> nil then
  begin
    text:= FDataLink.Field.DisplayText;
  end;
end;

procedure TDBSpinEdit.UpdateData(Sender: TObject);
{- transfer data from edit control to record }
{  UpdateData is only called after calls to both FDataLink.Modified and
  FDataLink.UpdateRecord. }
var
  luData  : string;
begin
  if (fDataLink = nil) or (fDataLink.field = nil) then exit;

  FDataLink.Field.text:= text;
  text:= fDataLink.Field.DisplayText;
end;

{return field name}
function TDBSpinEdit.GetDataField: string;
begin
  result:= FDataLink.FieldName;
end;

{return datasource}
function TDBSpinEdit.GetDataSource: TDataSource;
begin
  result:= FDataLink.DataSource;
end;

{set field name}
procedure TDBSpinEdit.SetDataField(const value: string);
begin
  FDataLink.FieldName:= value;
end;

{set datasource}
procedure TDBSpinEdit.SetDataSource(value: TDataSource);
begin
  FDataLink.DataSource:= value;
end;

{get value}
function TDBSpinEdit.GetValue: extended;
begin
  if FDataLink.Field <> nil then
  begin
    try
      result:= FDataLink.Field.AsFloat;
    except
      result:= 0;
    end;
  end
  else
    result:= 0;
end;

{set value}
procedure TDBSpinEdit.SetValue (NewValue: extended);
begin
  if (newValue <> Value) and (fDataLink <> Nil) and (FDataLink.Field <> nil) then
  begin
    try
      fDataLink.Edit;
      fDataLink.Field.Value:= newValue;
      text:= fDataLink.Field.DisplayText;
    except
      raise;
    end;
  end;
end;

procedure TDBSpinEdit.Change;
begin
(*  if (fDataLink <> nil) then
  begin
    if (fDataLink.Editing) then
    else
      fDataLink.DataSource.Edit;
  end;*)
  if fDataLink <> nil then
    fDataLink.Modified;
  inherited Change;   { and call inherited, which calls event handler }
end;

procedure TDBSpinEdit.CMTextChanged(var Message: TMessage);
begin
  inherited;
//  if FDataLink.DataSource.State <> dsEdit then
//    FDataLink.DataSource.edit;
end;

procedure TDBSpinEdit.CMExit(var Message: TCMExit);
begin
  try
    fDataLink.UpdateRecord;
  except
    SelectAll;
    SetFocus;
    raise;
  end;
end;

{check min and max values}
function TDBSpinEdit.CheckValue(NewValue: extended): extended;
begin
  Result:= NewValue;
  if (FMaxValue <> FMinValue) then
  begin
    if NewValue < FMinValue then
      Result:= FMinValue
    else if NewValue > FMaxValue then
      Result:= FMaxValue;
  end;
end;

end.