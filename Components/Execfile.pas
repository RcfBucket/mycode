unit Execfile;

{- Component used to execute programs. }

INTERFACE

uses
  SysUtils, WinTypes, WinProcs, Messages, Classes, Graphics, Controls, Forms,
  Dialogs;

type
  TWindowStyle   = (wsNorm, wsMinimize, wsMaximize, wsHide,
                    wsMinNoActivate, wsShowNoActivate);
  TWaitStyle     = (wRegular, wSuspend);
  TPriorityClass = (pcNormal, pcIdle, pcHigh, pcRealTime);

  TExecFile = class(TComponent)
  private
    FStopWaiting    : boolean;
    FMsg            : TMsg;
    FAssoc          : boolean;
    FPriorityClass  : TPriorityClass;
    FPriorityValue  : integer;
    FError          : integer;
    FExitCode       : integer;
    FIsWaiting      : boolean;
    FWait           : boolean;
    FWaitStyle      : TWaitStyle;   // Wait method
    FOnFail         : TNotifyEvent;
    FCommandLine    : ShortString;  // Command Line of Executable File
    FFParams        : ShortString;  // Parameters to send to Executable File
    FAssocFName     : string;       // Name of associated executable
    FWindowStyle    : TWindowStyle; // Window style for Executable File
    StartUpInfo     : TStartUpInfo;
    ProcessInfo     : TProcessInformation;
    protected
    procedure SetWindowStyle( Value: TWindowStyle );
    procedure SetWaitStyle ( Value: TWaitStyle );
    procedure SetPriorityClass ( Value: TPriorityClass );
    function GetStatus: integer;
  public
    function Execute: boolean;
    function Terminate: boolean;
    function IsWaiting: boolean;
    procedure StopWaiting;
    function ErrorCode: LongInt;
    procedure FocusProcess;
    property Status: integer read GetStatus;
  published
    property Associate: boolean read FAssoc write FAssoc;
    property CommandLine: ShortString read FCommandLine write FCommandLine;
    property Parameters: ShortString read FFParams write FFParams;
    property Priority: TPriorityClass read FPriorityClass write SetPriorityClass default pcNormal;
    property Wait: boolean read FWait write FWait;
    property WaitStyle: TWaitStyle read FWaitStyle write SetWaitStyle default wRegular;
    property WindowStyle: TWindowStyle read FWindowStyle write SetWindowStyle default wsNorm;
    property OnFail: TNotifyEvent read FOnFail write FOnFail;
  end;

procedure Register;

IMPLEMENTATION

uses ShellAPI;


procedure TExecFile.SetWindowStyle(Value : TWindowStyle);
begin
  fWindowStyle:= Value;
end;

procedure TExecFile.SetWaitStyle(Value : TWaitStyle);
begin
  fWaitStyle:= Value;
end;

procedure TExecFile.SetPriorityClass(Value : TPriorityClass);
begin
  fPriorityClass:= Value;
end;

function TExecFile.Execute: boolean;
var
  zCommandLine  : array[0..512] of Char;  // 0-based arrays for win api calls
  zFAssocFName    : array[0..255] of Char;
  zFAssocFDir     : array[0..255] of Char;
  zFAssocFDoc     : array[0..255] of Char;
  FSuccess      : boolean;
begin
  result:= true;
  FillChar(StartupInfo, Sizeof(StartupInfo), #0);
  StartupInfo.cb:= Sizeof(StartupInfo);
  StartupInfo.dwFlags:= STARTF_USESHOWWINDOW;

  // Set process window style
  case fWindowStyle of
    wsNorm:           StartupInfo.wShowWindow:= SW_SHOWNORMAL;
    wsMinimize:       StartupInfo.wShowWindow:= SW_SHOWMINIMIZED;
    wsMaximize:       StartupInfo.wShowWindow:= SW_SHOWMAXIMIZED;
    wsHide:           StartupInfo.wShowWindow:= SW_HIDE;
    wsMinNoActivate:  StartupInfo.wShowWindow:= SW_SHOWMINNOACTIVE;
    wsShowNoActivate: StartupInfo.wShowWindow:= SW_SHOWNA;
  end;

  // Set the process priority
  case fPriorityClass of
    pcHigh:     FPriorityValue:= HIGH_PRIORITY_CLASS;
    pcIdle:     FPriorityValue:= IDLE_PRIORITY_CLASS;
    pcNormal:   FPriorityValue:= NORMAL_PRIORITY_CLASS;
    pcRealTime: FPriorityValue:= REALTIME_PRIORITY_CLASS;
  end;

  // build process cmd line.
  StrPCopy(zCommandLine, PAnsiChar(fCommandLine + ' ' + fFParams));

  // Create the process.
  fSuccess:= CreateProcess(
      nil,
      zCommandLine,                   // pointer to command line string
      nil,                            // pointer to process security attributes
      nil,                            // pointer to thread security attributes
      false,                          // handle inheritance flag
      CREATE_NEW_CONSOLE or           // creation flags
         fPriorityValue,
      nil,                            // pointer to new environment block
      nil,                            // pointer to current directory name
      StartupInfo,                    // pointer to STARTUPINFO
      ProcessInfo);

  if not FSuccess then
  begin
    if fAssoc then
    begin
      StrPCopy(zfAssocFDoc, fCommandLine);
      if FindExecutable(zFAssocFDoc, zFAssocFDir, zFAssocFName) < 32 then
      begin
        FError:= GetLastError();
        if Assigned(FOnFail) then
          fOnFail(Self);
        Result:= False;
        Exit;
      end
      else
      begin
        fAssocFName:= zFAssocFName;
        StrPCopy(zCommandLine, fAssocFName+ ' ' + fCommandLine + ' ' + fFParams);
        FSuccess:= CreateProcess(
                nil,
                zCommandLine,           // pointer to command line string
                nil,                    // pointer to process security attributes
                nil,                    // pointer to thread security attributes
                false,                  // handle inheritance flag
                CREATE_NEW_CONSOLE or   // creation flags
                  FPriorityValue,
                nil,                    // pointer to new environment block
                nil,                    // pointer to current directory name
                StartupInfo,            // pointer to STARTUPINFO
                ProcessInfo);
      end;
    end;
  end;

  if fSuccess then
  begin
    if fWait then
    begin
      fIsWaiting:= True;
      fStopWaiting:= False;
      if fWaitStyle = wRegular then
      begin
        repeat
          while PeekMessage(FMsg,0,0,0,PM_REMOVE) do
          begin
            if fMsg.Message = WM_QUIT then
              Halt(fMsg.wParam);
            TranslateMessage(FMsg); DispatchMessage(FMsg);
          end;

          if WaitforSingleObject(ProcessInfo.hProcess,0) <> WAIT_TIMEOUT then
          begin
            fStopWaiting:= True;
            Application.ProcessMessages;
          end;
        until fStopWaiting;
      end
      else
      begin
        WaitForSingleObject(ProcessInfo.hProcess, INFINITE);
      end;
      fIsWaiting:= false;
    end;
  end
  else
  begin
    fError:= GetLastError();
    if Assigned(fOnFail) then
      fOnFail(Self);
    Result:= False;
  end;
end;

function TExecFile.GetStatus: integer;
var
  fCode : cardinal;
begin
  GetExitCodeProcess(ProcessInfo.hProcess, fCode);
  result:= fCode;
end;

function TExecFile.Terminate: boolean;
begin
  result:= false;
//  GetExitCodeProcess(ProcessInfo.hProcess, fExitCode);
  if TerminateProcess(ProcessInfo.hProcess, fExitCode) then
  begin
    result:= true;
    FillChar(processInfo.hProcess, SizeOf(processInfo), #0);
  end;
end;

function TExecFile.IsWaiting: boolean;
begin
  Result:= fIsWaiting;
end;

procedure TExecFile.StopWaiting;
begin
  fStopWaiting:= true;
  fIsWaiting:= false;
end;

function TExecFile.ErrorCode: integer;
begin
  result:= fError;
end;

procedure TExecFile.FocusProcess;
begin
  if IsWaiting then
  begin

  end;
end;

procedure Register;
begin
  RegisterComponents('Custom', [TExecFile]);
end;

END.


