{
  Copyright � 1998 by Delphi 4 Developer's Guide - Xavier Pacheco and Steve Teixeira
}
unit Cards;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls;

type
  TCardSuit = (csDiamond, csClub, csHeart, csSpade);
  TCardValue = (cvTwo, cvThree, cvFour, cvFive, cvSix, cvSeven, cvEight, cvNine, cvTen, cvJack, cvQueen, cvKing, cvAce);

  TMyCard = class(TCustomControl)
  private
    fBackBrush: HBrush;
    fBackColor: TColor;
    fFaceUp: Boolean;
    fSuit: TCardSuit;
    fValue: TCardValue;
    fSymbolSize: integer;
    fFontSize: integer;
    fSelected: Boolean;
    fHollowSuit: Boolean;
    procedure SetBackColor(Value: TColor);
    procedure SetFaceUp(Value: Boolean);
    procedure SetSuit(Value: TCardSuit);
    procedure SetValue(Value: TCardValue);
    procedure SetSymbolSize(const Value: integer);
    procedure SetFontSize(const Value: integer);
    procedure SetSelected(const Value: Boolean);
    procedure SetHollowSuit(const Value: Boolean);
  protected
    procedure Paint; override;
  public
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
  published
    property BackColor: TColor read fBackColor write SetBackColor default clBlue;
    property Color default clWhite;
    property DragCursor;
    property DragMode;
    property FaceUp: Boolean read fFaceUp write SetFaceUp default True;
    property Height default 120;
    property Hint;
    property ParentColor default False;
    property ParentShowHint;
    property PopupMenu;
    property HollowSuit: Boolean read fHollowSuit write SetHollowSuit default False;
    property Selected: Boolean read fSelected write SetSelected default False;
    property ShowHint;
    property Suit: TCardSuit read fSuit write SetSuit;
    property Value: TCardValue read fValue write SetValue;
    property SymbolSize: integer read fSymbolSize write SetSymbolSize default 36;
    property FontSize: integer read fFontSize write SetFontSize default 24;
    property Width default 90;
    property Visible;
    property OnClick;
    property OnDblClick;
    property OnDragDrop;
    property OnDragOver;
    property OnEndDrag;
    property OnKeyDown;
    property OnKeyPress;
    property OnKeyUp;
    property OnMouseDown;
    property OnMouseMove;
    property OnMouseUp;
    property OnStartDrag;
  end;

const
  cCardName: array [TCardValue] of string = ('2', '3', '4', '5', '6', '7', '8', '9', '10', 'Jack', 'Queen', 'King', 'Ace');
  cSuitName: array [TCardSuit] of string = ('Diamonds', 'Clubs', 'Hearts', 'Spades');

var
  gSuitRank: array [TCardSuit] of integer = (
    1,
    2,
    3,
    4
  );
  gValueRank: array [TCardValue] of integer = (
    13,
    1,
    2,
    3,
    4,
    5,
    6,
    7,
    8,
    9,
    10,
    11,
    12
  );

procedure register;

implementation

const
  SValueArray: array [TCardValue] of PChar = ('A', '2', '3', '4', '5', '6', '7', '8', '9', '10', 'J', 'Q', 'K');

constructor TMyCard.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
  Height:= 120;
  Width:= 90;
  ParentColor:= False;
  fFaceUp:= True;
  fSymbolSize:= 36;
  fFontSize:= 24;
  fSelected:= False;
  fHollowSuit:= False;
  SetBackColor(clBlue);
end;

destructor TMyCard.Destroy;
begin
  if fBackBrush <> 0 then
    DeleteObject(fBackBrush);
  inherited Destroy;
end;

procedure TMyCard.SetBackColor(Value: TColor);
begin
  if Value <> fBackColor then
  begin
    fBackColor:= Value;
    // delete old HBrush, if there is one
    if fBackBrush <> 0 then
      DeleteObject(fBackBrush);
    // create an HBrush to use to fill the background.
    fBackBrush:= CreateSolidBrush(ColorToRGB(fBackColor));
    Invalidate;
  end;
end;

procedure TMyCard.SetFaceUp(Value: Boolean);
begin
  if Value <> fFaceUp then
  begin
    fFaceUp:= Value;
    Invalidate;
  end;
end;

procedure TMyCard.SetSelected(const Value: Boolean);
begin
  if Value <> fSelected then
  begin
    fSelected:= Value;
    Invalidate;
  end;
end;

procedure TMyCard.SetSuit(Value: TCardSuit);
begin
  if Value <> fSuit then
  begin
    fSuit:= Value;
    Invalidate;
  end;
end;

procedure TMyCard.SetValue(Value: TCardValue);
begin
  if Value <> fValue then
  begin
    fValue:= Value;
    Invalidate;
  end;
end;

procedure TMyCard.Paint;
const
  cClub = #$02663;
  cSpade = #$02660;
  cHeart = #$02665;
  cDiamond = #$02666;

  chClub = #$02667;
  chSpade = #$02664;
  chHeart = #$02661;
  chDiamond = #$02662;

  // ordinal values of "suit" characters in Symbol font:
  // SSuits: string = #0168#0167#0169#0170;
  BorderWidth = 4;
var
  ARect: TRect;
  SSuits: string;
begin
  if fHollowSuit then
    SSuits:= chDiamond + chClub + chHeart + chSpade
  else
    SSuits:= cDiamond + cClub + cHeart + cSpade;

  ARect:= ClientRect;
  canvas.Brush.Color:= clWhite;

  if fSelected then
  begin
    canvas.Pen.Width:= 3;
    canvas.Pen.Color:= clRed;
  end
  else
  begin
    canvas.Pen.Width:= 1;
    canvas.Pen.Color:= clBlack;
  end;

  // draw bounding rectangle
  canvas.RoundRect(ARect.Left, ARect.Top, ARect.Right, ARect.Bottom, 10, 10);
  // tighten drawing rect to allow for a border
  Inc(ARect.Top, BorderWidth);
  Inc(ARect.Left, BorderWidth);
  Dec(ARect.Right, BorderWidth);
  Dec(ARect.Bottom, BorderWidth);

  if fFaceUp then
  begin
    // draw card face if it is face-up
    with canvas do
    begin
      with Font do
      begin
        Font.name:= 'Arial Unicode MS';
        Size:= fSymbolSize;
        // set color of font to match that of suit
        if fSuit in [csDiamond, csHeart] then
          Color:= clRed
        else
          Color:= clBlack;
      end;
      Brush.Style:= bsClear;
      // draw suit symbol in center of card
      DrawText(Handle, SSuits[Ord(fSuit) + 1], 1, ARect, DT_SINGLELINE or DT_CENTER or DT_VCENTER);
      // use new typeface and size for value
      Font.Size:= fFontSize;
      // draw value in top,left and bottom,right of card
      DrawText(Handle, SValueArray[fValue], -1, ARect, DT_SINGLELINE or DT_TOP or DT_LEFT);
      DrawText(Handle, SValueArray[fValue], -1, ARect, DT_SINGLELINE or DT_BOTTOM or DT_RIGHT);
    end;
  end
  else
    // if card is face-down, then draw a big rectangle
    // Using the FillRect() API with an HBrush is slighly more efficient
    // than using the TCanvas methods.
    Windows.FillRect(canvas.Handle, ARect, fBackBrush);
end;

procedure register;
begin
  RegisterComponents('Custom', [TMyCard]);
end;

procedure TMyCard.SetSymbolSize(const Value: integer);
begin
  if fSymbolSize <> Value then
  begin
    fSymbolSize:= Value;
    Invalidate;
  end;
end;

procedure TMyCard.SetFontSize(const Value: integer);
begin
  if fFontSize <> Value then
  begin
    fFontSize:= Value;
    Invalidate;
  end;
end;

procedure TMyCard.SetHollowSuit(const Value: Boolean);
begin
  if Value <> fHollowSuit then
  begin
    fHollowSuit:= Value;
    Invalidate;
  end;
end;

end.
