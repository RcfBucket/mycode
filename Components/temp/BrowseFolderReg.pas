unit BrowseFolderReg;

interface

uses
  Windows, Classes, Forms, SysUtils, Dialogs, Controls;
  
procedure Register;

implementation

uses
  BrowseFolder,
  DesignIntf,
  DesignEditors;

type
	{Design-time testing by Double-clicking or from popup menu take Test}
	{Added by Ahto Tanner}
	TDialogTest=class(TDefaultEditor)
	public
		procedure ExecuteVerb(Index: Integer); override;
		function GetVerb(Index: Integer): string; override;
		function GetVerbCount: Integer; override;
		procedure Edit; override;
	end;

//--------------------------------------------------------------------------------------------------------//
procedure Register;
begin
	RegisterComponentEditor(TBrowseFolder, TDialogTest);
  RegisterComponents('Dialogs', [TBrowseFolder]);
end;

// Component editor to provide testing capabilities at design time
// Added by Ahto Tanner
//--------------------------------------------------------------------------------------------------------//
procedure TDialogTest.ExecuteVerb(Index: Integer);
begin
	if Index=0 then
		Edit
end;

//--------------------------------------------------------------------------------------------------------//
function TDialogTest.GetVerb(Index: Integer): String;
begin
	case Index of
 		0:	Result:= 'Set D&irectory';
	end;
end;

//--------------------------------------------------------------------------------------------------------//
function TDialogTest.GetVerbCount: Integer;
begin
	Result:=1;
end;

//--------------------------------------------------------------------------------------------------------//
procedure TDialogTest.Edit;
begin
	TBrowseFolder(Component).Execute;
end;

end.

