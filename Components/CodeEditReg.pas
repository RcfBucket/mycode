unit CodeEditReg;

INTERFACE

procedure Register;

IMPLEMENTATION

uses
  CodeEdit, Classes, DsgnIntf, TypInfo, DBTables;

type
  TDBStringProperty = class(TStringProperty)
  public
    function GetAttributes: TPropertyAttributes; override;
    procedure GetValueList(List: TStrings); virtual; abstract;
    procedure GetValues(Proc: TGetStrProc); override;
  end;

  TDatabaseNameProperty = class(TDBStringProperty)
  public
    procedure GetValueList(List: TStrings); override;
  end;

  TTableNameProperty = class(TDBStringProperty)
  public
    procedure GetValueList(List: TStrings); override;
  end;

  TLookupIndexProperty = class(TDBStringProperty)
  public
    procedure GetValueList(List: TStrings); override;
  end;

  TDataFieldProperty = class(TDBStringProperty)
    procedure GetValueList(List: TStrings); override;
  end;

function TDBStringProperty.GetAttributes: TPropertyAttributes;
begin
  Result := [paValueList, paSortList, paMultiSelect];
end;

procedure TDBStringProperty.GetValues(Proc: TGetStrProc);
var
  I: Integer;
  Values: TStringList;
begin
  Values := TStringList.Create;
  try
    GetValueList(Values);
    for I := 0 to Values.Count - 1 do Proc(Values[I]);
  finally
    Values.Free;
  end;
end;

procedure TTableNameProperty.GetValueList(List: TStrings);
const
  Masks: array[TTableType] of string[5] = ('', '*.DB', '*.DBF', '*.DB', '*.TXT');
var
  instance  : TDBCodeEdit;
  tbl       : TTable;
begin
  instance:= GetComponent(0) as TDBCodeEdit;  {- get pointer to TCodeEdit component}
  tbl:= instance.LookupTableClass;
  if (tbl <> nil) then
    Session.GetTableNames(tbl.DatabaseName, Masks[tbl.TableType],
                            tbl.TableType = ttDefault, False, List);
end;

procedure TLookupIndexProperty.GetValueList(List: TStrings);
var
  Instance: TDBCodeEdit;
  tbl     : TTable;
  k       : integer;
begin
  instance := GetComponent(0) as TDBCodeEdit;
  tbl:= instance.LookupTableClass;
  if tbl <> nil then
  begin
    tbl.IndexDefs.Update;
    if (tbl.IndexDefs.count > 0) then
    begin
      for k:= 0 to tbl.IndexDefs.count - 1 do
        list.Add(tbl.IndexDefs.items[k].fields);
    end;
  end;
end;

procedure TDatabaseNameProperty.GetValueList(List: TStrings);
begin
  Session.GetDatabaseNames(List);
end;

procedure TDataFieldProperty.GetValueList(List: TStrings);
var
  Instance: TDBCodeEdit;
  tbl     : TTable;
begin
  instance:= GetComponent(0) as TDBCodeEdit;  {- get pointer to TCodeEdit component}
  tbl:= instance.LookupTableClass;
  if (tbl <> nil) then
    tbl.GetFieldNames(list);
end;

procedure DoRegister;
begin
  RegisterComponents('Custom', [TDBCodeEdit]);
  RegisterPropertyEditor(TypeInfo(string), TDBCodeEdit, 'LookupTable', TTableNameProperty);
  RegisterPropertyEditor(TypeInfo(string), TDBCodeEdit, 'LookupIndex', TLookupIndexProperty);
  RegisterPropertyEditor(TypeInfo(string), TDBCodeEdit, 'LookupDatabase', TDatabasenameProperty);
  RegisterPropertyEditor(TypeInfo(string), TDBCodeEdit, 'LookupDisplay', TDataFieldProperty);
  RegisterPropertyEditor(TypeInfo(string), TDBCodeEdit, 'LinkIndex', TLookupIndexProperty);
end;


procedure Register;
begin
  DoRegister;
end;

END.
