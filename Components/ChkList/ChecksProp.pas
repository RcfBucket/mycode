unit ChecksProp;

// Property editor dialog for CheckList.Checked property

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, ChkList;

type
  TFrmEditCheckList = class(TForm)
    lstChecks: TCheckList;
    BtnInvert: TButton;
    BtnToggle: TButton;
    BtnAll: TButton;
    BtnNone: TButton;
    BtnOK: TButton;
    BtnCancel: TButton;
    procedure BtnInvertClick(Sender: TObject);
    procedure BtnToggleClick(Sender: TObject);
    procedure BtnAllClick(Sender: TObject);
    procedure BtnNoneClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FrmEditCheckList: TFrmEditCheckList;

implementation

{$R *.DFM}

procedure TFrmEditCheckList.BtnInvertClick(Sender: TObject);
var
  k   : integer;
begin
  for k:= 0 to lstChecks.Items.Count - 1 do
  begin
    lstChecks.Checked[k]:= not lstChecks.Checked[k];
  end;
end;

procedure TFrmEditCheckList.BtnToggleClick(Sender: TObject);
begin
  with lstChecks do
  begin
    if ItemIndex >= 0 then
    begin
      Checked[ItemIndex]:= not Checked[ItemIndex];
    end;
  end;
end;

procedure TFrmEditCheckList.BtnAllClick(Sender: TObject);
var
  k   : integer;
begin
  for k:= 0 to lstChecks.Items.Count - 1 do
  begin
    lstChecks.Checked[k]:= true;
  end;
end;

procedure TFrmEditCheckList.BtnNoneClick(Sender: TObject);
var
  k   : integer;
begin
  for k:= 0 to lstChecks.Items.Count - 1 do
  begin
    lstChecks.Checked[k]:= false;
  end;
end;

end.
