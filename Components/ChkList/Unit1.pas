unit Unit1;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ChkList, StdCtrls, ExtCtrls, ComCtrls;

type
  TForm1 = class(TForm)
    Button1: TButton;
    ListBox1: TListBox;
    BtnAdd: TButton;
    BtnInsert: TButton;
    BtnDelete: TButton;
    BtnToggle: TButton;
    BtnEdit: TButton;
    CheckList1: TCheckList;
    procedure Button1Click(Sender: TObject);
    procedure BtnToggleClick(Sender: TObject);
    procedure BtnAddClick(Sender: TObject);
    procedure BtnInsertClick(Sender: TObject);
    procedure BtnDeleteClick(Sender: TObject);
    procedure BtnEditClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

uses ChecksProp;

{$R *.DFM}

procedure TForm1.Button1Click(Sender: TObject);
var
  k   : TColor;
begin
  case checklist1.CheckColor of
    clBlack :  k:= clMaroon;
    clMaroon : k:= clGreen;
    clGreen :  k:= clOlive;
    clOlive :  k:= clNavy;
    clNavy :   k:= clPurple;
    clPurple : k:= clTeal;
    clTeal :   k:= clGray;
    clGray :   k:= clSilver;
    clSilver : k:= clRed;
    clRed :    k:= clLime;
    clLime :   k:= clYellow;
    clYellow : k:= clBlue;
    clBlue :   k:= clFuchsia;
    clFuchsia: k:= clAqua;
    clAqua :   k:= clWhite;
    clWhite :  k:= clBlack;
  end;
  checklist1.CheckColor:= k;
end;

procedure TForm1.BtnToggleClick(Sender: TObject);
var
  sel   : integer;
begin
  sel:= checklist1.ItemIndex;
  if sel >= 0 then
    checklist1.checked[sel]:= not checklist1.checked[sel];
end;

procedure TForm1.BtnAddClick(Sender: TObject);
begin
  checkList1.items.Add(IntToStr(checkList1.items.count+1));
end;

procedure TForm1.BtnInsertClick(Sender: TObject);
const
  inserted : integer = 0;

begin
  with checklist1 do
  begin
    Inc(inserted);
    items.Insert(ItemIndex, 'Inserted : ' + IntToStr(inserted));
  end;
end;

procedure TForm1.BtnDeleteClick(Sender: TObject);
var
  sel : integer;
begin
  sel:= checklist1.ItemIndex;
  if sel >= 0 then
    checklist1.items.delete(sel);
end;

procedure TForm1.BtnEditClick(Sender: TObject);
begin
  Close;
end;

end.

