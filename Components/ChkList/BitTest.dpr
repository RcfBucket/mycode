{$APPTYPE CONSOLE}
program BitTest;

uses
  BitLib,
  Windows;

var
  bits  : TBitArray;
  bits2 : TBitArray;

procedure DumpBits;
var
  k   : integer;
begin
  writeln('         1         2         3         4         5         6');
  writeln('123456789012345678901234567890123456789012345678901234567890');
  for k:= bits.size - 1 downto 0 do
  begin
    if bits[k] then
      write('1')
    else
      write('0');
  end;
  writeln;
end;

procedure DoTest;
begin
  bits:= TBitArray.Create;
  bits2:= TBitArray.Create;
  bits2.size:= 20;
  bits.Size:= 12;
  bits[1]:= true;
  bits[0]:= true;
  bits[10]:= true;
  writeln('Open Bit : ', bits.OpenBit);
  writeln('Size: ', bits.size);
  DumpBits;
  bits.size:= 20;
  bits[19]:= true;
  writeln;
  writeln('Size: ', bits.size);
  DumpBits;

  bits.ShiftR(-1);
  DumpBits;
  bits.ShiftL(-1);
  DumpBits;

  bits2[1]:= true;
  bits2[0]:= true;
  bits2[19]:= true;

  bits.AndWith(bits2);
  DumpBits;

  bits.OrWith(bits2);
  DumpBits;

  bits.Invert;
  DumpBits;

  bits.Free;
end;

begin
  writeLn('Bits Test');
  writeln;

  DoTest;
  readln;
end.

