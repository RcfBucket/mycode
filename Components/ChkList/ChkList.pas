unit ChkList;

(* Need to add DefineProperties for the Checked property
   Also need reader and writer functions
*)

interface

uses
  Messages, Windows, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, BitLib,
  extctrls;

type
  TCheckList = class(TCustomListBox)
  private
    fChecked    : TBitArray;
    il          : TImageList;
    fCheckColor : TColor;

    procedure SetChecked(idx: integer; value: boolean);
    procedure SetCheckColor(value: TColor);
    function GetChecked(idx: integer): boolean;
  protected
    procedure LBAddString(var msg: TMessage); message LB_ADDSTRING;
    procedure LBDeleteString(var msg: TMessage); message LB_DELETESTRING;
    procedure LBInsertString(var msg: TMessage); message LB_INSERTSTRING;
    procedure DefineProperties(filer: TFiler); override;
    procedure ReadChecked(reader: TReader);
    procedure WriteChecked(writer: TWriter);
  public
    constructor Create(aOwner: TComponent); override;
    destructor Destroy; override;
    procedure DrawItem(Idx: Integer; Rect: TRect; State: TOwnerDrawState); override;
    property Checked[index: integer]: boolean read GetChecked write SetChecked;
  published
    property CheckColor: TColor read fCheckColor write SetCheckColor default clBlack;

    property Align;
    property BorderStyle;
    property Color;
//    property Columns;
    property Ctl3D;
    property DragCursor;
    property DragMode;
    property Enabled;
    property ExtendedSelect;
    property Font;
    property IntegralHeight;
    property ItemHeight;
    property Items;
    property MultiSelect;
    property ParentColor;
    property ParentCtl3D;
    property ParentFont;
    property ParentShowHint;
    property PopupMenu;
    property ShowHint;
    property Sorted;
//    property Style;
    property TabOrder;
    property TabStop;
//    property TabWidth;
    property Visible;
    property OnClick;
    property OnDblClick;
    property OnDragDrop;
    property OnDragOver;
    property OnDrawItem;
    property OnEndDrag;
    property OnEnter;
    property OnExit;
    property OnKeyDown;
    property OnKeyPress;
    property OnKeyUp;
    property OnMeasureItem;
    property OnMouseDown;
    property OnMouseMove;
    property OnMouseUp;
    property OnStartDrag;
  end;

IMPLEMENTATION

{$R CHKLIST.RES}

constructor TCheckList.Create(aOwner: TComponent);
var
  bm    : TBitMap;
begin
  fCheckColor:= clBlack;
  bm:= TBitMap.Create;
  bm.LoadFromResourceName(hInstance,'BMPCHECK');
  il:= TImageList.Create(self);
  il.Width:= bm.Width;
  il.Height:= bm.Height;
  il.AddMasked(bm, clOlive);
  bm.Free;
  fChecked:= TBitArray.Create;

  inherited Create(aOwner);
  Style:= lbOwnerDrawVariable;
end;

destructor TCheckList.Destroy;
begin
  fChecked.Free;
  fChecked:= nil;
  inherited Destroy;
end;

procedure TCheckList.SetChecked(idx: integer; value: boolean);
begin
  fChecked[idx]:= value;
  Invalidate;
end;

function TCheckList.GetChecked(idx: integer): boolean;
begin
  if fChecked <> nil then
    result:= fChecked[idx]
  else
    result:= false;
end;

procedure TCheckList.DrawItem(idx: Integer; Rect: TRect; State: TOwnerDrawState);
var
  tstr    : string;
  y       : integer;
  txtRect : TRect;
begin
  canvas.FillRect(Rect);
  if idx < items.Count then
  begin
    tstr:= items[idx];
    txtRect:= rect;
    Inc(txtRect.left, (il.Width + 8));
    DrawText(canvas.handle, PChar(tstr), -1, txtRect, DT_WORDBREAK);

    try
      if (Checked[idx]) then
      begin
        y:= rect.top + ((ItemHeight div 2) - (il.Height div 2));
        if y < 0 then
          y:= 0;
        il.Draw(canvas, rect.left+2, y, 0);
      end;
    except
    end;
  end;

end;

procedure TCheckList.DefineProperties(filer: TFiler);
begin
  inherited DefineProperties(filer);
  filer.DefineProperty('Checked', ReadChecked, WriteChecked, true);
end;

procedure TCheckList.WriteChecked(writer: TWriter);
var
  k   : integer;
begin
  with writer do
  begin
    WriteListBegin;
    for k:= 0 to items.count - 1 do
    begin
      if Checked[k] then
        writeString('1')
      else
        writeString('0');
    end;
    writeListEnd;
  end;
end;

procedure TCheckList.ReadChecked(reader: TReader);
var
  tstr  : string;
  k     : integer;
begin
  with reader do
  begin
    ReadListBegin;
    k:= 0;
    while not reader.EndOfList do
    begin
      tstr:= ReadString;
      Checked[k]:= tstr = '1';
      Inc(k);
    end;
    ReadListEnd;
  end;
end;

procedure TCheckList.LBAddString(var msg: TMessage);
var
  idx : integer;
begin
  inherited;
  if (fChecked <> nil) then
  begin
    fChecked.Size:= items.count;
    idx:= msg.result;
    fChecked.ShiftL(idx);
  end;
end;

procedure TCheckList.LBDeleteString(var msg: TMessage);
var
  idx : integer;
  k   : integer;
begin
  idx:= msg.wParam;
  inherited;
  if (msg.result <> LB_ERR) and (fChecked <> nil) then
  begin
    for k:= idx to fChecked.Size - 2 do
    begin
      fChecked[k]:= fChecked[k + 1];
    end;
    fChecked.Size:= items.count;
  end;
end;

procedure TCheckList.LBInsertString(var msg: TMessage);
var
  idx : integer;
begin
  inherited;
  if fChecked <> nil then
  begin
    idx:= msg.result;
    fChecked.Size:= items.count;
    fChecked.ShiftL(idx);
  end;
end;

procedure TCheckList.SetCheckColor(value: TColor);
var
  k, j  : integer;
  bm    : TBitMap;
begin
  if value <> fCheckColor then
  begin
    fCheckColor:= value;
    bm:= TBitMap.Create;
    bm.LoadFromResourceName(hInstance,'BMPCHECK');
    for k:= 0 to bm.width - 1 do
      for j:= 0 to bm.Height - 1 do
      begin
        if bm.canvas.pixels[k, j] = clBlack then
          bm.canvas.pixels[k, j]:= fCheckColor;
      end;
    il.Free;
    il:= TImageList.Create(self);
    il.Width:= bm.Width;
    il.Height:= bm.Height;
    il.AddMasked(bm, clOlive);
    bm.Free;
    Invalidate;
  end;
end;

end.

