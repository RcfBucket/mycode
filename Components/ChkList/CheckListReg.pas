unit CheckListReg;

interface

uses
  SysUtils, Classes, Forms, Dialogs, DsgnIntf, TypInfo, Controls,
  ChkList;

procedure Register;

implementation

uses
  ChecksProp;

type
  TCheckListEditor = class(TComponentEditor)
    function GetVerbCount: integer; override;
    function GetVerb(index: integer): string; override;
    procedure Edit; override;
    procedure ExecuteVerb(index: integer); override;
  end;

function TCheckListEditor.GetVerbCount: integer;
begin
  result:= 2;
end;

function TCheckListEditor.GetVerb(index: integer): string;
begin
  result:= '';
  case (index) of
    0   : result:= 'CheckList v1.00';
    1   : result:= 'Edit CheckList';
  end;
end;

procedure TCheckListEditor.Edit;
begin
  ExecuteVerb(1);
end;

procedure TCheckListEditor.ExecuteVerb(index: integer);
var
  f     : TFrmEditCheckList;
  c     : TCheckList;
  k     : integer;
begin
  case (index) of
    0 : ;
    1 : begin
          f:= TFrmEditCheckList.Create(application);
          try
            c:= Component as TCheckList;
            f.Caption:= c.owner.name + '.' + c.name;

            f.lstChecks.Items.Assign(c.items);
            for k:= 0 to c.items.count - 1 do
              f.lstChecks.Checked[k]:= c.Checked[k];

            if (f.ShowModal = mrOK) then
            begin
              for k:= 0 to c.items.count - 1 do
                c.Checked[k]:= f.lstChecks.Checked[k];
              designer.Modified;
            end;
          finally
            f.Free;
          end;
        end;
  end;
end;

procedure Register;
begin
  RegisterComponents('Custom', [TCheckList]);
  RegisterComponentEditor(TCheckList, TCheckListEditor);
end;

end.
