unit EllipsisLabel;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls;

type
  TEllipsisStyle = (elPathEllipsis, elEndEllipsis);

  TEllipsisLabel = class(TCustomLabel)
  private
    fEllipsisStyle  : TEllipsisStyle;
    procedure SetEllipsisStyle(value: TEllipsisStyle);
//    procedure CMTextChanged(var Message: TMessage); message CM_TEXTCHANGED;
  protected
    function GetLabelText: string; override;
  public
    constructor Create(AOwner: TComponent); override;
  published
    property Style: TEllipsisStyle
      read    fEllipsisStyle
      write   SetEllipsisStyle
      default elPathEllipsis;

    property Align;
    property Alignment;
    property AutoSize;
    property Caption;
    property Color;
    property DragCursor;
    property DragMode;
    property Enabled;
    property FocusControl;
    property Font;
    property ParentColor;
    property ParentFont;
    property ParentShowHint;
    property PopupMenu;
    property ShowAccelChar;
    property ShowHint;
    property Transparent;
    property Layout;
    property Visible;
    property WordWrap;
    property OnClick;
    property OnDblClick;
    property OnDragDrop;
    property OnDragOver;
    property OnEndDrag;
    property OnMouseDown;
    property OnMouseMove;
    property OnMouseUp;
    property OnStartDrag;
  end;

procedure Register;

IMPLEMENTATION

constructor TEllipsisLabel.Create(AOwner: TComponent);
begin
  inherited Create(aOwner);
  fEllipsisStyle:= elPathEllipsis;
end;

procedure TEllipsisLabel.SetEllipsisStyle(value: TEllipsisStyle);
begin
  if value <> fEllipsisStyle then
  begin
    fEllipsisStyle:= value;
    Invalidate;
  end;
end;

function TEllipsisLabel.GetLabelText: string;
// override this function to provide modified captions to the parents paint
// method.
const
  Alignments: array[TAlignment] of Word = (DT_LEFT, DT_RIGHT, DT_CENTER);
  WordWraps: array[Boolean] of Word = (0, DT_WORDBREAK);
var
  R         : TRect;
  tstr      : string;
  drawStyle : integer;
begin
  tstr:= caption;
  if (parent <> nil) and (canvas <> nil) and (canvas.handle <> 0) and (tstr <> '') then
  begin
    DrawStyle := DT_EXPANDTABS or WordWraps[WordWrap] or Alignments[Alignment];
    if fEllipsisStyle = elPathEllipsis then
      DrawStyle:= DrawStyle or DT_PATH_ELLIPSIS
    else
      DrawStyle:= DrawStyle or DT_END_ELLIPSIS;
    with canvas do
    begin
      r:= Rect(0,0,Width, Height);
      SetLength(tstr, 500);
      DrawText(handle, PChar(tstr), -1, r, DrawStyle or
          DT_CALCRECT or DT_MODIFYSTRING);
      SetLength(tstr, StrLen(PChar(tstr)));
    end;
  end;
  result:= tstr;
end;

procedure Register;
begin
  RegisterComponents('Custom', [TEllipsisLabel]);
end;

end.
