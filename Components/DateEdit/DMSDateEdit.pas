unit DMSDateEdit;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls;

type
  TDateEntryFormat =
    (defMDY,
     defMYD,
     defDMY,
     defDYM,
     defYMD,
     defYDM);

  TDMSDateEdit = class(TEdit)
  private
    fFormat   : TDateEntryFormat;
  protected
    { Protected declarations }
  public
    { Public declarations }
  published
    property DateEnterFormat: TDateEntryFormat read fFormat write fFormat;
  end;

procedure Register;

implementation

procedure Register;
begin
  RegisterComponents('DMS', [TDMSDateEdit]);
end;

end.
