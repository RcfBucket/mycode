unit Linkbtn;

interface

uses
  SysUtils, WinTypes, WinProcs, Messages, Classes, Graphics, Controls,
  Forms, Dialogs, Buttons;

type
  TLinkButton = class(TSpeedButton)
  private
    linkCtl  : TComponent;
    procedure SetLinkComponent(newValue: TComponent);
    function GetLinkComponent: TComponent;
  protected
    { Protected declarations }
  public
    constructor Create(AOwner: TComponent); override;
  published
    property LinkComponent: TComponent read GetLinkComponent write SetLinkComponent;
  end;

procedure Register;

implementation

constructor TLinkButton.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
  linkCtl:= nil;
end;

procedure TLinkButton.SetLinkComponent(newValue: TComponent);
begin
  linkCtl:= newValue;
end;

function TLinkButton.GetLinkComponent: TComponent;
begin
  if linkCtl <> nil then
  begin
    if owner.FindComponent(linkCtl.name) = nil then
      linkCtl:= nil;
  end;
  result:= linkCtl;
end;

procedure Register;
begin
  RegisterComponents('Custom', [TLinkButton]);
end;

end.
