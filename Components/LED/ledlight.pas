unit LedLight;
(*
   TLedLight by Lars Posthuma; December 26, 1995.
   Copyright 1995, Lars Posthuma.
   All rights reserved.

   This source code may be freely distributed and used. The author
   accepts no responsibility for its use or misuse.
   No warranties whatsoever are offered for this unit.

   If you make any changes to this source code please inform me at:
   LPosthuma@COL.IB.COM.
*)


{$R LedLight.res}

interface

uses
  WinTypes, WinProcs, Classes, Graphics, Controls, ExtCtrls, Forms, SysUtils;

Const
  MinLedSize = 8;
  MaxLedSize = 30;

type
  String5 = String[5];

  TLedLightOrientation = (LedHorizontal,LedVertical);
  TLedLightState       = (LedOff,LedOn);

  TLedLight = class(TCustomPanel)
    private
      { Private declarations }
      fBorderWidth : Integer;
      fOrientation : TLedLightOrientation;
      fState       : TLedLightState;
      fLedSize     : Integer;
      fLedSpacing  : Integer;
      fHeight      : Integer;
      fWidth       : Integer;
      fValueChange : TNotifyEvent;
      fCtl3D       : Boolean;
      procedure PaintLeds(Led1,Led2: String5);

      procedure SetBorderWidth   (value: Integer); virtual;
      procedure SetBounds(Left,Top,fWidth,fHeight: integer); override;
      procedure SetCtl3D(value : boolean); virtual;
      procedure SetHeight(value: Integer); virtual;
      procedure SetOrientation(value: TLedLightOrientation);
      procedure SetLedSize(value: Integer);
      procedure SetLedSpacing(value : Integer);
      procedure SetState(value: TLedLightState); virtual;
      procedure SetWidth(value: Integer); virtual;
    protected
      { Protected declarations }
      procedure Paint; override;
    public
      { Public declarations }

      constructor Create(AOwner: TComponent); override;
      destructor Destroy; override;
      procedure ChangeState; virtual;
    published
      { Published declarations }
      property Align;
      property BevelInner;
      property BevelOuter;
      property BevelWidth;
      property Cursor;
      property Enabled;
      property Color default clBtnFace;
      property Height: Integer                               {Height of Outer Limits}
               read fHeight write SetHeight
               default 17;
      property Width : Integer                               {Width of Outer Limits}

               read fWidth write SetWidth
               default 36;
      property Orientation: TLedLightOrientation             {}
               read fOrientation write SetOrientation
               default LedHorizontal;
      property LedSize: Integer                              {True Size of Leds}
               read fLedSize write SetLedSize
               default 13;
      property LedSpacing: Integer                           {Space between the Leds}
               read fLedSpacing write SetLedSpacing
               default 6;
      property State: TLedLightState                         {Indicates Leds On or Off}

               read fState write SetState
               default LedOff;
      property BorderStyle;
      property BorderWidth: Integer
               read fBorderWidth write SetBorderWidth
               default 1;
      property Ctl3D: Boolean
               read fCtl3D write SetCtl3D
               default False;
      property OnValueChange: TNotifyEvent                   {Userdefined Method}
               read fValueChange write fValueChange;
      property Visible;
      property Hint;
      property ParentShowHint;
      property ShowHint;
      property Tag;

      property OnClick;

      property OnDragDrop;
      property OnDragOver;
      property OnEndDrag;
      property OnMouseDown;
      property OnMouseMove;
      property OnMouseUp;
  end;

procedure Register;

implementation

constructor TLedLight.Create(AOwner: TComponent);
begin
 inherited Create(AOwner);
 Color       := clBtnFace;                        {Set initial (default) values}
 fLedSize    := 13;
 fLedSpacing := 6;
 fHeight     := 17;
 fWidth      := 36;
 fBorderWidth:= 1;
 fOrientation:= LedHorizontal;
 fState      := LedOff;
 fCtl3D      := False;

 BevelInner  := bvNone;
 BevelOuter  := bvNone;
 Caption     := ' ';
end;

destructor TLedLight.Destroy;
begin
 inherited Destroy
end;

procedure TLedLight.SetHeight(value: integer);
begin
 if value <> fHeight then begin
   fHeight:= value;
   if fHeight < MinLedSize + 4
     then fHeight:= MinLedSize + 4;
   if fHeight > MaxLedSize + 4
     then fHeight:= MaxLedSize + 4;

   fLedSize:= fHeight - 4;
   SetBounds(Left,Top,fWidth,fHeight);
   Invalidate;
 end
end;

procedure TLedLight.SetWidth(value: integer);

Var
 MyWidth: Integer;
begin
 if value <> fWidth then begin
   fWidth:= value;
   MyWidth:= ((MinLedSize + 2) * 2) + LedSpacing;
   if fWidth < MyWidth
     then fWidth:= MyWidth;
   MyWidth:= ((MaxLedSize + 2) * 2) + LedSpacing;
   if fWidth > MyWidth
     then fWidth:= MyWidth;

   fLedSize:= (fWidth - 4 - LedSpacing) div 2;
   SetBounds(Left,Top,fWidth,fHeight);
   Invalidate;
 end
end;

procedure TLedLight.SetBounds(Left,Top,fWidth,fHeight : integer);
begin
 case fOrientation of
   LedHorizontal: begin

                    fWidth := (fLedSize + 2) * 2 + fLedSpacing;
                    fHeight:= fLedSize + 4;
                  end;
   LedVertical  : begin
                    fWidth := fLedSize + 4;
                    fHeight:= (fLedSize + 2) * 2 + fLedSpacing;
                  end;
 end;
 inherited SetBounds(Left,Top,fWidth,fHeight);
end;

procedure TLedLight.SetLedSize(value : Integer);
begin
 if value <> fLedSize then begin
   fLedSize:= value;
   if fLedSize < MinLedSize
     then fLedSize:= MinLedSize;

   if fLedSize > MaxLedSize
     then fLedSize:= MaxLedSize;
   if LedSpacing > fLedSize
     then LedSpacing:= fLedSize;

   SetBounds(Left,Top,fWidth,fHeight);
   Invalidate;
 end
end;

procedure TLedLight.SetLedSpacing(value : Integer);
begin
 if value <> fLedSpacing then begin
   fLedSpacing:= value;
   if fLedSpacing < 2                                      {Valid Range [2..fLedSize}
     then fLedSpacing:= 2;
   if fLedSpacing > LedSize
     then fLedSpacing:= LedSize;
   if (fLedSpacing mod 2) <> 0                             {Only Even values allowed}

     then Inc(fLedSpacing);

   SetBounds(Left,Top,fWidth,fHeight);                     { testen zonder f.. }
   Invalidate;
 end
end;

procedure TLedLight.SetOrientation(value : TLedLightOrientation);
begin
 if value <> fOrientation then begin
   fOrientation:= value;
   SetBounds(Left,Top,Height,Width);                       {Swap Width/Height}
   Invalidate;
 end
end;

procedure TLedLight.SetBorderWidth(value : Integer);
begin
 if value <> fBorderWidth then begin
   fBorderWidth:= value;
   if fBorderWidth < 0                                     {Valid Range: [0..2]}

     then fBorderWidth:= 1;
   if fBorderWidth > 2
     then fBorderWidth:= 2;
   Invalidate;
 end
end;

procedure TLedLight.SetState(value : TLedLightState);
begin
 if value <> fState then begin
   fState:= value;
   if Assigned(fValueChange)                               {Call Userdefined Method}
     then OnValueChange(Self);
   Invalidate;
 end
end;

procedure TLedLight.SetCtl3D (value : boolean);
begin
 if value <> fCtl3D then begin
   fCtl3D:= value;
   Invalidate;
 end
end;

procedure TLedLight.ChangeState;

begin
 if fState = LedOff
   then fState:= LedOn
   else fState:= LedOff;
 if Assigned(fValueChange)                                 {Call Userdefined Method}
   then OnValueChange(Self);
 Invalidate;
end;

procedure TLedLight.Paint;
begin
 inherited Paint;
 with Canvas do begin
   Brush.Color:= Color;
   with ClientRect do begin
     if Ctl3D = True then begin                            {Give 3d look}
       Pen.Color:= clBtnShadow;
       MoveTo(Left + 1, Bottom - 3);
       LineTo(Left + 1, Top + 1);
       LineTo(Right - 2, Top + 1);

       MoveTo(Left, Bottom - 1);
       LineTo(Right-1, Bottom - 1);
       LineTo(Right-1, Top - 1)
     end;
     Case fState of
       LedOff: PaintLeds('Green','Gray');
       LedOn : PaintLeds('Gray','Red');
     end;
   end;
 end;
end;

procedure TLedLight.PaintLeds(Led1,Led2: String5);
var
 BMP1, BMP2  : TBitmap;
 TheLed      : Array[0..5] of Char;
 MyOffs      : Integer;
 DRect, SRect: TRect;
begin
 (* Create the Leds from Resource file.
    Dynamic creation by means of Old-fashioned API call *)

 StrPCopy(TheLed,Led1);
 BMP1       := TBitmap.Create;
 BMP1.Handle:= LoadBitmap(hInstance, TheLed);

 StrPCopy(TheLed,Led2);
 BMP2       := TBitmap.Create;
 BMP2.Handle:= LoadBitmap(hInstance, TheLed);
 Try
   (* If BorderStyle is bsSingle the leds are somewhat shifted; to fix this
      "problem" paint the leds at a somewhat smaller offset *)
   MyOffs:= 2;
   if BorderStyle = bsSingle
     then Dec(MyOffs);

   (* Create Source Rectangle; used for both Leds *)
   SRect:= Rect(0,0,13,13);

   MyOffs:= 2;
   if BorderStyle = bsSingle
     then Dec(MyOffs);

   (* Create Destination Rectangle; used for drawing first Led *)
   DRect:= Rect(MyOffs,MyOffs,(MyOffs + LedSize),(MyOffs + LedSize));

   (* Draw first Led *)
   Canvas.BrushCopy(DRect,BMP1,SRect,clBtnFace);

   (* Create Destination Rectangle; used for drawing Second Led *)
   case fOrientation of
     LedHorizontal: DRect:= Rect((Width - 2 - LedSize),MyOffs,(Width - 2),(MyOffs + LedSize));

     LedVertical  : DRect:= Rect(MyOffs,(MyOffs + LedSize + LedSpacing),(2 + LedSize),(2 + (2 * LedSize) + LedSpacing));
   end;

   (* Draw second Led *)
   Canvas.BrushCopy(DRect,BMP2,SRect,clBtnFace);
   Finally
     BMP1.Free;
     BMP2.Free;
 end;
end;

procedure Register;
begin
 RegisterComponents('Lars', [TLedLight]);
end;

end.

Copyright �1995 - Prime Time Programming
