unit TwipLib;
{$I CompileOptions.inc}

INTERFACE

// LTwip library. There are 1440 LTwips per inch
// There are 20 LTwips per point.

uses
  SysUtils, Windows, Classes;

type
  TLTwips = type integer;   // force TLTwips to be considered a unique type

type
  TUnitTypes = (uInches, uMM, uCM, uTwips);

const
  UnitString : array[TUnitTypes] of string = ('in', 'mm', 'cm', 'twp');

  // common LTwip measurement constants
  LT16           = 1440 div 16;    {- 1/16 inches }
  LT08           = 1440 div 8;     {- 1/8  inches }
  LT25           = 1440 div 4;     {- 0.25 inches }
  LT50           = 1440 div 2;     {- 0.5  inches }
  LT75           = 1440 * 3 div 4; {- 0.75 inches }
  LT100          = 1440;           {- 1.0  inches }
  LT125          = LT100 + LT25;   {- 1.25 inches }
  LT150          = LT100 + LT50;   {- 1.5  inches }
  LT175          = LT100 + LT75;   {- 1.75 inches }
  LT200          = 1440 * 2;       {- 2.0  inches }
  LT300          = 1440 * 3;       {- 3.0  inches }
  LT400          = 1440 * 4;       {- 4.0  inches }
  LT500          = 1440 * 5;       {- 5.0  inches }
  LT600          = 1440 * 6;       {- 6.0  inches }
  LT700          = 1440 * 7;       {- 7.0  inches }
  LT800          = 1440 * 8;       {- 8.0  inches }

function ConvertUnits(lt: integer; aUnit: TUnitTypes; var fmt: string): double;
function UnitsToLTwips(units: double; unitType: TUnitTypes): TLTwips;
function LTwipsToUnits(lt: integer; unitType: TUnitTypes): double;
procedure SetLogicalTwips(dc: HDC; zoom: Real);
function IsLogicalTwips(dc: HDC; var aZoom: double): boolean;
function PointsToLTwips(aPt: integer): TLTwips;
procedure LTwipsPerPixel(dc: HDC; var LTpPX, LTpPY: integer);

IMPLEMENTATION

function ConvertUnits(lt: integer; aUnit: TUnitTypes; var fmt: string): double;
// convert LTs to units (as a double) and return an appropriate format mask
// for displaying the double (This mask is valid for FormatFloat function)
begin
  if aUnit = uTwips then
    fmt:= '0'
  else
    fmt:= '0.000';
  result:= LTwipsToUnits(lt, aUnit);
end;

function LTwipsToUnits(lt: integer; unitType: TUnitTypes): double;
// 1440 lt per inch  --- 1440 lt per 2.54
begin
  case unitType of
    uMM:      result:= lt / 1440.0 * 25.4;
    uCM:      result:= lt / 1440.0 * 2.54;
    uTwips:   result:= lt;
  else
    result:= lt / 1440.0;   // use inches as the default
  end;
end;

function UnitsToLTwips(units: double; unitType: TUnitTypes): TLTwips;
begin
  case unitType of
    uMM :     result:= Round(units / 25.4 * 1440.0);
    uCM :     result:= Round(units / 2.54 * 1440.0);
    uTwips:   result:= Trunc(units);
  else
    result:= Round(units * 1440);   // inches
  end;
end;

{-----------------------------------------------------------------------------}
{ Convert the specified number of 'points' to logical twips. This function    }
{ uses the fact that there are 20 Ltwips per point in the ltwips mapping mode.}
{ There are (1440 LT/in.) and (72 pt/in.) so 1440 / 72 = 20 LT/pt             }
{                                                                             }
{ InParms : aPt - number of points to convert.                                }
{ Returns : The function returns the number of logical twips for the number   }
{           of points specified.                                              }
{-----------------------------------------------------------------------------}
function PointsToLTwips(aPt: integer): TLTwips;
begin
  PointsToLTwips:= aPt * 20;
end;

{-----------------------------------------------------------------------------}
{ Return number of LTwips per pixel for the device. Accounts for any zoom     }
{ factor in the device context.                                               }
{                                                                             }
{ InParms : dc    - the (valid) device context in which a logical twips       }
{                   mapping mode has been set. (see SetLogicalTwips)          }
{ OutParms: LTpPX - logical twips per pixel (horizontal)                      }
{           LTpPY - logical twips per pixel (vertical)                        }
{-----------------------------------------------------------------------------}
procedure LTwipsPerPixel(dc: HDC; var LTpPX, LTpPY: integer);
var
  size : TSize;
begin
  GetWindowExtEx(dc, size);
  LTpPX:= Round(size.cx / GetDeviceCaps(dc, LOGPIXELSX));
  LTpPY:= Round(size.cy / GetDeviceCaps(dc, LOGPIXELSY));
end;

{-----------------------------------------------------------------------------}
{ Sets a logical twips mapping mode for a DC.                                 }
{                                                                             }
{ InParms : dc   - the (valid) device context in which to set the mapping mode}
{           zoom - the zoom factor (1.0 = 100%, 0.25 = 25%, 2.0 = 200% etc)   }
{ OutParms: none                                                              }
{-----------------------------------------------------------------------------}
procedure SetLogicalTwips(dc: HDC; zoom: Real);
var
  x  : integer;
  y  : integer;
begin
  x:= Round(1440.0 / zoom);
  y:= Round(1440.0 / zoom);
  SetMapMode(dc, MM_ANISOTROPIC);
  SetWindowExtEx(dc, x, y, nil);
  SetViewportExtEx(dc, GetDeviceCaps(dc, LOGPIXELSX),
                     GetDeviceCaps(dc, LOGPIXELSY), nil);
end;

function IsLogicalTwips(dc: HDC; var aZoom: double): boolean;
// return true if the DC has been set to a logical twips mapping mode
// also, if LTwips has been selected in to the DC then return the
// zoom factor that was applied.
var
  aSize   : TSize;
  num     : double;
begin
  result:= false;
  if (GetMapMode(dc) = MM_ANISOTROPIC) then
  begin
    GetViewPortExtEx(dc, aSize);
    if (aSize.cx = GetDeviceCaps(dc, LOGPIXELSX)) and
       (aSize.cy = GetDeviceCaps(dc, LOGPIXELSY)) then
    begin
      // here it gets a bit tricky, because I dont know what the zoom factor
      // may have been when the mapping mode was set up. So I back out the
      // numbers to see what zoom factor would have had to have been applied
      // to return the numbers that GetWindowExtEx returns. If these numbers
      // fall within a reasonable zoom range (0.25..3.0) then I assume this
      // DC has been set up in logical twips mode.
      GetWindowExtEx(dc, aSize);
      num:= 1440.0 / aSize.cx;
      if (num >= 0.25) and (num <= 3.0) then
      begin
        num:= 1440.0 / aSize.cy;
        if (num > 0.25) and (num <= 3.0) then
          result:= true;
      end;
    end;
  end;
end;

END.