object FrmPageNum: TFrmPageNum
  Left = 285
  Top = 161
  BorderStyle = bsDialog
  Caption = 'Goto Page'
  ClientHeight = 73
  ClientWidth = 293
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  object lblCurPage: TLabel
    Left = 16
    Top = 16
    Width = 107
    Height = 13
    Caption = 'Current page %d of %d'
  end
  object Label2: TLabel
    Left = 16
    Top = 44
    Width = 50
    Height = 13
    Caption = 'Goto page'
  end
  object BtnGoto: TButton
    Left = 200
    Top = 8
    Width = 75
    Height = 25
    Caption = '&Goto'
    Default = True
    TabOrder = 2
    OnClick = BtnGotoClick
  end
  object Button2: TButton
    Left = 200
    Top = 40
    Width = 75
    Height = 25
    Cancel = True
    Caption = 'Cancel'
    ModalResult = 2
    TabOrder = 3
  end
  object edtPageNum: TEdit
    Left = 80
    Top = 40
    Width = 73
    Height = 21
    ReadOnly = True
    TabOrder = 0
    Text = '0'
  end
  object spnPageNum: TUpDown
    Left = 153
    Top = 40
    Width = 15
    Height = 21
    Associate = edtPageNum
    Min = 0
    Position = 0
    TabOrder = 1
    Wrap = False
  end
end
