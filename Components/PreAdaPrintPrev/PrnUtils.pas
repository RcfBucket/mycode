unit PrnUtils;                                                          {- rcf}
{$I CompileOptions.inc}

INTERFACE

uses
  SysUtils, Windows, Classes, Printers, RCFStrings;

function PaperName(idx: integer): string;
procedure GetPrnDeviceHandle(var devHandle: THandle);
function SelectedPaperSize(var size: TPoint): integer;
function GetPrintDeviceName: string;
function DCMargins(handle: HDC): TRect;

const
  First_DMPaper   = DMPAPER_LETTER;
  Last_DMPaper    = DMPAPER_FANFOLD_LGL_GERMAN;
  PaperSizes : array[first_DMPaper..last_DMPaper] of TPoint =
    ( (x:Round(8.5 * 1440); y:Round(11  * 1440)), { dmPaper_Letter }
      (x:Round(8.5 * 1440); y:Round(11  * 1440)), { dmPaper_LetterSmall }
      (x:Round(11  * 1440); y:Round(17  * 1440)), { dmpaper_Tabloid }
      (x:Round(17  * 1440); y:Round(11  * 1440)), { dmPaper_dmpaper_Ledger }
      (x:Round(8.5 * 1440); y:Round(14  * 1440)), { dmpaper_Legal }
      (x:Round(5.5 * 1440); y:Round(8.5 * 1440)), { dmpaper_Statement }
      (x:Round(7.25* 1440); y:Round(10.5* 1440)), { dmpaper_Executive }
      (x:Round(297/25.4*1440); y:Round(420/25.4*1440)), { dmpaper_A3 }
      (x:Round(210/25.4*1440); y:Round(297/25.4*1440)), { dmpaper_A4 }
      (x:Round(210/25.4*1440); y:Round(297/25.4*1440)), { dmpaper_A4Small }
      (x:Round(148/25.4*1440); y:Round(210/25.4*1440)), { dmpaper_A5 }
      (x:Round(250/25.4*1440); y:Round(354/25.4*1440)), { dmpaper_B4 }
      (x:Round(182/25.4*1440); y:Round(257/25.4*1440)), { dmpaper_B5 }
      (x:Round(8.5 *1440); y:Round(13*1440)), { dmpaper_Folio }
      (x:Round(215/25.4*1440); y:Round(275/25.4*1440)), { dmpaper_Quarto }
      (x:Round(10*1440); y:Round(14*1440)), { dmpaper_10X14 }
      (x:Round(11*1440); y:Round(17*1440)), { dmpaper_11X17 }
      (x:Round(8.5*1440); y:Round(11*1440)), { dmpaper_Note }
      (x:Round(3.875*1440); y:Round(8.875*1440)), { dmpaper_Env_9 }
      (x:Round(4.125*1440); y:Round(9.5*1440)), { dmpaper_Env_10 }
      (x:Round(4.5*1440); y:Round(10.375*1440)), { dmpaper_Env_11 }
      (x:Round(4.5*1440); y:Round(11*1440)), { dmpaper_Env_12 }
      (x:Round(5*1440); y:Round(11.5*1440)), { dmpaper_Env_14 }
      (x:Round(-1); y:Round(-1)), { dmpaper_CSheet (unsupported) }
      (x:Round(-1); y:Round(-1)), { dmpaper_DSheet (unsupported) }
      (x:Round(-1); y:Round(-1)), { dmpaper_ESheet (unsupported) }
      (x:Round(110/25.4*1440); y:Round(220/25.4*1440)), { DMPAPER_ENV_DL }
      (x:Round(162/25.4*1440); y:Round(229/25.4*1440)), { DMPAPER_ENV_C5 }
      (x:Round(324/25.4*1440); y:Round(458/25.4*1440)), { DMPAPER_ENV_C3 }
      (x:Round(229/25.4*1440); y:Round(324/25.4*1440)), { DMPAPER_ENV_C4 }
      (x:Round(114/25.4*1440); y:Round(162/25.4*1440)), { DMPAPER_ENV_C6 }
      (x:Round(114/25.4*1440); y:Round(229/25.4*1440)), { DMPAPER_ENV_C65 }
      (x:Round(250/25.4*1440); y:Round(353/25.4*1440)), { DMPAPER_ENV_B4 }
      (x:Round(176/25.4*1440); y:Round(250/25.4*1440)), { DMPAPER_ENV_B5 }
      (x:Round(176/25.4*1440); y:Round(125/25.4*1440)), { DMPAPER_ENV_B6 }
      (x:Round(110/25.4*1440); y:Round(230/25.4*1440)), { DMPAPER_ENV_ITALY }
      (x:Round(3.875*1440); y:Round(7.5*1440)), { DMPAPER_ENV_MONARCH}
      (x:Round(3.625*1440); y:Round(6.5*1440)), { DMPAPER_ENV_PERSONAL }
      (x:Round(14.875*1440); y:Round(11*1440)), { DMPAPER_FANFOLD_US }
      (x:Round(8.5*1440); y:Round(12*1440)), { DMPAPER_FANFOLD_STD_GERMAN}
      (x:Round(8.5*1440); y:Round(13*1440)) { DMPAPER_FANFOLD_LGL_GERMAN }
      );

IMPLEMENTATION

function PaperName(idx: integer): string;
{- return a paper name based on its index value. idx is one of the
   standard paper constants found in WinTypes (dmPaper_...) }
begin
  case idx of
    dmpaper_Letter:       result:= 'Letter 8 1/2 x 11 in.';
    dmpaper_LetterSmall:  result:= 'Letter Small 8 1/2 x 11 in.';
    dmpaper_Tabloid:      result:= 'Tabloid 11 x 17 in.';
    dmpaper_Ledger:       result:= 'Ledger 17 x 11 in.';
    dmpaper_Legal:        result:= 'Legal 8 1/2 x 14 in.';
    dmpaper_Statement:    result:= 'Statement 5 1/2 x 8 1/2 in.';
    dmpaper_Executive:    result:= 'Executive 7 1/4 x 10 1/2 in.';
    dmpaper_A3:           result:= 'A3 297 x 420 mm';
    dmpaper_A4:           result:= 'A4 210 x 297 mm';
    dmpaper_A4Small:      result:= 'A4 Small 210 x 297 mm';
    dmpaper_A5:           result:= 'A5 148 x 210 mm';
    dmpaper_B4:           result:= 'B4 250 x 354 mm';
    dmpaper_B5:           result:= 'B5 182 x 257 mm';
    dmpaper_Folio:        result:= 'Folio 8 1/2 x 13 in.';
    dmpaper_Quarto:       result:= 'Quarto 215 x 275 mm';
    dmpaper_10X14:        result:= '10x14 in.';
    dmpaper_11X17:        result:= '11x17 in.';
    dmpaper_Note:         result:= 'Note 8 1/2 x 11 in.';
    dmpaper_Env_9:        result:= 'Envelope #9 3 7/8 x 8 7/8 in.';
    dmpaper_Env_10:       result:= 'Envelope #10 4 1/8 x 9 1/2 in.';
    dmpaper_Env_11:       result:= 'Envelope #11 4 1/2 x 10 3/8 in.';
    dmpaper_Env_12:       result:= 'Envelope #12 4 1/2 x 11 in.';
    dmpaper_Env_14:       result:= 'Envelope #14 5 x 11 1/2 in.';
    dmpaper_CSheet:       result:= 'C size sheet (unsupported)';
    dmpaper_DSheet:       result:= 'D size sheet (unsupported)';
    dmpaper_ESheet:       result:= 'E size sheet (unsupported)';
    DMPAPER_ENV_DL:       result:= 'Envelope DL, 110 x 220 mm';
    DMPAPER_ENV_C5:       result:= 'Envelope C5, 162 x 229 mm';
    DMPAPER_ENV_C3:       result:= 'Envelope C3, 324 x 458 mm';
    DMPAPER_ENV_C4:       result:= 'Envelope C4, 229 x 324 mm';
    DMPAPER_ENV_C6:       result:= 'Envelope C6, 114 x 162 mm';
    DMPAPER_ENV_C65:      result:= 'Envelope C65, 114 x 229 mm';
    DMPAPER_ENV_B4:       result:= 'Envelope B4, 250 x 353 mm';
    DMPAPER_ENV_B5:       result:= 'Envelope B5, 176 x 250 mm';
    DMPAPER_ENV_B6:       result:= 'Envelope B6, 176 x 125 mm';
    DMPAPER_ENV_ITALY:    result:= 'Envelope, 110 x 230 mm';
    DMPAPER_ENV_MONARCH:  result:= 'Envelope Monarch, 3 7/8 x 7 1/2 in.';
    DMPAPER_ENV_PERSONAL: result:= 'Envelope, 3 5/8 x 6 1/2 in.';
    DMPAPER_FANFOLD_US:   result:= 'U.S. Std Fanfold, 14 7/8 x 11 in.';
    DMPAPER_FANFOLD_STD_GERMAN: result:= 'German Std Fanfold, 8 1/2 x 12 in.';
    DMPAPER_FANFOLD_LGL_GERMAN: result:= 'German Legal Fanfold, 8 1/2 x 13 in.';
    dmpaper_User:         result:= 'User defined...';
    else                  result:= 'Unknown paper size';
  end;
end;

procedure GetPrnDeviceHandle(var devHandle: THandle);
// returns a printer device handle for the current printer
// This handle can then be used to access the TDevMode properties via
// GlobalLock
var
  dev, drv, port : array[0..255] of char;
begin
  // first obtain a handle to the TPrinter's devicemode structure
  printer.GetPrinter(dev, drv, port, devHandle);
(*
  if not printer.Printing then
  begin
    // The following is needed to force the printers unit to reload the DevMode
    printer.SetPrinter(dev, drv, port, 0);
    printer.GetPrinter(dev, drv, port, devHandle);
  end;

  // if deviceHandle is 0 then the driver was not loaded. Set the printer
  // index to force the printer driver to load making the handle available.
  if devHandle = 0 then
  begin
    printer.PrinterIndex:= printer.PrinterIndex;
    printer.GetPrinter(dev, drv, port, devHandle);
  end;
*)
  // if devHandle is still 0 then an error has occured. Otherwise, devHandle
  // is valid and now you can use GlobalLock on devHandle to get at the
  // DeviceMode structure.
  if devHandle = 0 then
    raise Exception.Create(RCF_CantInitPrnDevHandle);
end;

function SelectedPaperSize(var size: TPoint): integer;
// returns the currently selected paper size in the printer driver in LTwips
// Function result is the currently selected paper index (dmPaper_xxx)
var
  dc        : HDC;
  k         : integer;
  w         : integer;
  h         : integer;
  devHandle : THandle;
  devMode   : PDevMode;
begin
  dc:= printer.Handle;

  w:= GetDeviceCaps(dc, PHYSICALWIDTH);
  h:= GetDeviceCaps(dc, PHYSICALHEIGHT);

  k:= GetDeviceCaps(dc, LOGPIXELSX);
  w:= Round(w / k * 1440.0);    // convert to LTwips

  k:= GetDeviceCaps(dc, LOGPIXELSY);
  h:= Round(h / k * 1440.0);    // convert to LTwips

  size:= Point(w, h);

  GetPrnDeviceHandle(devHandle);
  devMode:= GlobalLock(devHandle);
  try
    result:= devMode^.dmPaperSize;
  finally
    GlobalUnlock(devHandle);
  end;
end;

function GetPrintDeviceName: string;
// return the current printer device name
var
  dev, drv, port : array[0..255] of char;
  devHandle : THandle;
begin
  printer.GetPrinter(dev, drv, port, devHandle);
  result:= dev;
end;

(*function PrintOffset(APrn: PPrinter; var pOffset: TPoint): boolean;
{- returns the minimum margin for a printer. }
var
  dc  : HDC;
  k   : integer;
begin
  if aPrn^.status = ps_OK then
  begin
    dc:= Aprn^.GetDC;
    Escape(dc, GETPRINTINGOFFSET, sizeof(tpoint), nil, @pOffset);

    k:= GetDeviceCaps(dc, LOGPIXELSX);
    pOffset.x:= Round(pOffset.x / k * 1440.0);  {- convert to LTwips }

    k:= GetDeviceCaps(dc, LOGPIXELSY);
    pOffset.y:= Round(pOffset.y / k * 1440.0);  {- convert to LTwips }

    DeleteDC(dc);
  end
  else
    FillChar(pOffset, sizeof(pOffset), 0);
end;
*)

function DCMargins(handle: HDC): TRect;
{- return minimum margins for a printer. The rect structure will
   hold the left, right, top and bottom margins. returns false if printer is
   invalid }
var
  r     : TRect;
  k     : integer;
begin
  // get margin info in Device units (pixels)
  begin
    r.Left:= GetDeviceCaps(handle, PHYSICALOFFSETX);
    r.Top:= GetDeviceCaps(handle, PHYSICALOFFSETY);

    r.bottom:= GetDeviceCaps(handle, PHYSICALHEIGHT);
    r.right:= GetDeviceCaps(handle, PHYSICALWIDTH);

    if (r.bottom = 0) or (r.right = 0) then // the handle is the screen
      r:= Rect(0,0,0,0)
    else
    begin
      r.bottom:= r.bottom - r.top - GetDeviceCaps(handle, VERTRES);
      r.right:= r.right - r.left - GetDeviceCaps(handle, HORZRES);

      // convert to LTwips
      k:= GetDeviceCaps(handle, LOGPIXELSX);
      r.left:= Round(r.left / k * 1440.0);
      r.right:= Round(r.right / k * 1440.0);
      k:= GetDeviceCaps(handle, LOGPIXELSY);
      r.top:= Round(r.top / k * 1440.0);
      r.bottom:= Round(r.bottom / k * 1440.0);
    end;
    result:= r;
  end;
end;

END.
