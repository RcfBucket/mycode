unit PaperView;
{$I CompileOptions.inc}

{- Need to have key assignments for zoomin and out rather than
   hardcoding them. Make a property for them.

    Known bugs:
    1. Clicking on paperview does not seem to bring it in focus.
}

INTERFACE

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, PrnUtils, Printers, TwipLib;

const
  MaxZoom   = 4;

type
  TPaperView = class;
  TPaperIndex = integer;

  TPaperPaintEvent = procedure(sender: TPaperView; aCanvas: TCanvas) of Object;
  TPaperView = class(TScrollBox)
  private
    pbx           : TPaintBox;
    fZoom         : double;
    fEdgeX        : TLTwips;
    fEdgeY        : TLTwips;
    fOnPaint      : TPaperPaintEvent;
    fGridVisible  : boolean;
    fMargVisible  : boolean;
    fGridColor    : TColor;
    fGridSize     : TLTwips;
    fMargLeft     : TLTwips;
    fMargTop      : TLTwips;
    fMargRight    : TLTwips;
    fMargBottom   : TLTwips;
    fFontName     : TFontName;
    fOnZoomChange : TNotifyEvent;
    fPaperWidth   : TLTwips;
    fPaperHeight  : TLTwips;
    procedure SetZoom(value: double);
    procedure SetEdge(index: integer; value: TLtwips);
    procedure SetRefresh(value: boolean);
    procedure GetPixSize(var w, h: integer);
    procedure UpdatePaintBoxSize;
    procedure SetVisibles(index: integer; value: boolean);
    procedure SetGridColor(value: TColor);
    procedure SetGridSize(value: TLTwips);
    procedure SetMargins(index: integer; value: TLTwips);
    procedure SetFont(value: TFontName);
//    function GetPBoxWidth: integer;
//    procedure SetVOrg(index, value: integer);
//    function GetLT(index: integer): integer;
    function GetCanvas: TCanvas;
    procedure SetPaperSize(index: integer; value: TLTWips);
  protected
    procedure Loaded; override;
    procedure CreateWnd; override;
    procedure CMChildKey(var msg: TCMChildKey); message CM_CHILDKEY;
    procedure DoKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure DoClick(sender: TObject);
    procedure DoDblClick(sender: TObject);
{    procedure MouseDown(Button: TMouseButton; Shift: TShiftState; X, Y: Integer); override;
    procedure MouseMove(Shift: TShiftState; X, Y: Integer); override;
    procedure MouseUp(Button: TMouseButton; Shift: TShiftState; X, Y: Integer); override;}
  public
    constructor Create(aOwner: TComponent); override;
    destructor Destroy; override;
    property Canvas: TCanvas read GetCanvas;
    function DevicePointToLTwips(aPt: TPoint): TPoint;
  published
//    property PaperIndex: TPaperIndex read fPaperIdx write SetPaperIdx;
    property Zoom: double read fZoom write SetZoom;
    property EdgeX: TLTwips index 0 read fEdgeX write SetEdge default LT08;
    property EdgeY: TLTwips index 1 read fEdgeY write SetEdge default LT08;

    // debug properties
    property DoRefresh: boolean read fGridVisible write SetRefresh;
{    property Orientation: TPrinterOrientation read fOrient write SetOrient;}

    property GridColor: TColor read fGridColor write SetGridColor;
    property GridSize: TLTwips read fGridSize write SetGridSize;
    property MarginLeft: TLTwips index 0 read fMargLeft write SetMargins;
    property MarginTop: TLTwips index 1 read fMargTop write SetMargins;
    property MarginRight: TLTwips index 2 read fMargRight write SetMargins;
    property MarginBottom: TLTwips index 3 read fMargBottom write SetMargins;

    property GridVisible: boolean index 0 read fGridVisible write SetVisibles;
    property MarginVisible: boolean index 1 read fMargVisible write SetVisibles;

//    property PBoxWidth: integer read GetPBoxWidth write temp;
//    property VOrgX: integer index 0 read fVOrgX write SetVOrg;
//    property VOrgY: integer index 1 read fVOrgY write SetVOrg;
//    property LTX: integer index 0 read GetLT write temp;
//    property LTY: integer index 1 read GetLT write temp;
    property Font: TFontName read fFontName write SetFont;
    property PaperWidth: TLTwips index 0 read fPaperWidth write SetPaperSize;
    property PaperHeight: TLTwips index 1 read fPaperHeight write SetPaperSize;

    // events
    property OnPaintPage: TPaperPaintEvent read fOnPaint write fOnPaint;
    property OnZoomChange: TNotifyEvent read fOnZoomChange write fOnZoomChange;
end;

procedure Register;

IMPLEMENTATION

uses
  DsgnIntf, PaperSize, LTwipEd;

type
  TLTwipsEd = class(TIntegerProperty)
    function GetAttributes: TPropertyAttributes; override;
    procedure Edit; override;
  end;

  TPaperPaintBox = class(TPaintBox)
    constructor Create(aOwner: TComponent); override;
    destructor Destroy; override;
    procedure Paint; override;
    procedure MouseDown(Button: TMouseButton; Shift: TShiftState; X, Y: Integer); override;
    procedure MouseUp(Button: TMouseButton; Shift: TShiftState; X, Y: Integer); override;
    procedure MouseMove(Shift: TShiftState; X, Y: Integer); override;
    property OnClick;
    property OnDblClick;
  end;

constructor TPaperPaintBox.Create(aOwner: TComponent);
begin
  inherited Create(aOwner);
end;

destructor TPaperPaintBox.Destroy;
begin
  inherited Destroy;
end;

procedure TPaperPaintBox.MouseDown(Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  TPaperView(parent).SetFocus;
  TPaperView(parent).MouseDown(button, shift, x, y);
end;

procedure TPaperPaintBox.MouseMove(Shift: TShiftState; X, Y: Integer);
begin
  TPaperView(parent).MouseMove(shift, x, y);
end;

procedure TPaperPaintBox.MouseUp(Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  TPaperView(parent).MouseUp(button, shift, x, y);
end;

procedure TPaperPaintBox.Paint;
// paint the data in the paint box
var
  pageSize    : TPoint;
  LTpPX, LTpPY: integer;
  rect        : TRect;
  ownr        : TPaperView;
  k           : integer;
  pt          : TPoint;
begin
  ownr:= owner as TPaperView;
//  pageSize:= PaperSizes[ownr.fPaperIdx];
  pageSize.x:= ownr.fPaperWidth;
  pageSize.y:= ownr.fPaperHeight;
(*  if ownr.fOrient = poLandscape then
  begin
    x:= pageSize.y;
    pageSize.y:= pageSize.x;
    pageSize.x:= x;
  end;
*)
  with canvas do
  begin
    canvas.Font:= Font;
    Canvas.Brush.Color := Color;

    rect:= ClientRect;

    InflateRect(rect, -1, -1);
    FillRect(rect);

    SetLogicalTwips(handle, ownr.fZoom);
    LTwipsPerPixel(handle, LTpPX, LTpPY);

    // set viewport because for some reason, the origin gets messed up after
    // changing the mapping mode.
    SetViewPortOrgEx(handle, Left, Top, nil);
    SetWindowOrgEx(handle, 0, 0, nil);

    brush.Color:= clWhite;
    pen.Mode:= pmCopy;
    pen.Style:= psSolid;
    pen.Width:= 1;

    pen.Color:= clBlack;
    // Draw rectangle representing paper
    Rectangle(ownr.EdgeX, ownr.edgeY,
              ownr.edgeX + pagesize.x + (LTpPX*2),
              ownr.edgeY + pageSize.y + (LTpPY*2));

    // Give paper a 3d look
    pen.Width:= LTpPX * 3;
    MoveTo(ownr.edgeX + pageSize.x + (LTpPX * 2), ownr.edgeY + LT08);
    LineTo(ownr.edgeX + pageSize.X + (LTpPX * 2), ownr.edgeY + pageSize.Y + (LTpPY*2));
    pen.Width:= LTpPY * 3;
    LineTo(ownr.edgeX + LT08, ownr.edgeY + pageSize.Y + (LTpPY*2));

    if ownr.fGridVisible then
    begin
      pen.Color:= ownr.GridColor;
      pen.Width:= 1;
      pen.Style:= psDot;
      k:= ownr.GridSize;
      while (k < pageSize.x) or (k < pageSize.y) do
      begin
        if k < pageSize.x then  // vertical grid
        begin
          MoveTo(ownr.EdgeX + k, ownr.edgeY + LTpPY);
          LineTo(ownr.EdgeX + k, ownr.edgeY + pageSize.y + LTpPY);
        end;
        if k < pageSize.y then  // horizontal grid
        begin
          MoveTo(ownr.edgeX + LTpPX, ownr.edgeY + k);
          LineTo(ownr.edgeX + pageSize.x + LTpPX, ownr.edgeY + k);
        end;
        Inc(k, ownr.GridSize);
      end;
    end;
    if ownr.fMargVisible then
    begin
      pen.Width:= 1;
      pen.Style:= psDot;
      pen.Color:= clNavy;
      brush.Style:= bsClear;
      with ownr do
        Rectangle(EdgeX + fMargLeft, edgeY + fMargTop,
                  edgeX + pagesize.x - fMargRight + LTpPX,
                  edgeY + pageSize.y - fMargBottom + LTpPY);
    end;
  end;

  if Assigned(ownr.fOnPaint) then
  begin
    // calculate the top left of the printable area. This takes into
    // account the margins and the edge of the paper.
    with ownr do
    begin
      // ask windows what the device coords are of the start of the printable
      // area of the paper.
      pt.x:= edgeX;
      pt.y:= edgeY;
      LPtoDP(canvas.handle, pt, 1);
    end;


    // Set window origin to the start of the printable area.
//    SetViewPortOrgEx(canvas.handle, pt.x + 1, pt.y + 1, nil);
    SetViewPortOrgEx(canvas.handle, pt.x, pt.y, nil);
    SetWindowOrgEx(canvas.handle, 0, 0, nil);

    ownr.fOnPaint(ownr, canvas);
  end;
end;

constructor TPaperView.Create(aOwner: TComponent);
begin
  inherited Create(aOwner);
  pbx:= TPaperPaintBox.Create(self);
  pbx.left:= 0;
  pbx.top:= 0;
  pbx.parent:= self;
  pbx.ParentFont:= false;
//  fPaperIdx:= dmPaper_Letter;
  fPaperWidth:= paperSizes[dmPaper_Letter].x;
  fPaperHeight:= paperSizes[dmPaper_Letter].y;
  fZoom:= 1.0;
  fEdgeX:= LT08;
  fEdgeY:= LT08;
  VertScrollBar.Tracking:= true;
  HorzScrollBar.Tracking:= true;
//  fOrient:= poPortrait;
  fGridVisible:= false;
  fMargVisible:= false;
  fGridColor:= clSilver;
  fGridSize:= LT50;   // 0.5 inch default grid
  fMargLeft:= LT50;
  fMargTop:= LT50;
  fMargRight:= LT50;
  fMargBottom:= LT50;
  pbx.Font.Name:= 'Arial';
  OnKeyDown:= DoKeyDown;
  pbx.OnClick:= DoClick;
  pbx.OnDblClick:= DoDblClick;
end;

procedure TPaperView.DoClick(Sender: TObject);
begin
  if Assigned(OnClick) then
    OnClick(sender);
end;

procedure TPaperView.DoDblClick(Sender: TObject);
begin
  if Assigned(OnDblClick) then
    OnDblClick(sender);
end;

destructor TPaperView.Destroy;
begin
  inherited Destroy;
end;

procedure TPaperView.CreateWnd;
begin
  inherited CreateWnd;
  UpdatePaintBoxSize;
//  PaperIndex:= fPaperIdx;
end;

procedure TPaperView.Loaded;
begin
  inherited Loaded;
  UpdatePaintBoxSize;
//  ** need to update width and height of pbx
//  PaperIndex:= fPaperIdx;
end;

procedure TPaperView.GetPixSize(var w, h: integer);
// given the current paper size, edge margins, return a width
// and height that can accomodate the paint box.
var
  LTpPX, LTpPY: integer;
begin
  with pbx do
  begin
    SetLogicalTwips(canvas.handle, fZoom);
    LTwipsPerPixel(canvas.handle, LTpPX, LTpPY);
    w:= (fPaperWidth + edgeX + LT100) div LTpPX;
    h:= (fPaperHeight + edgeY + LT100) div LTpPY;
{    w:= (PaperSizes[fPaperIdx].x + edgeX  + LT100) div LTpPX;
    h:= (PaperSizes[fPaperIdx].y + edgeY  + LT100) div LTpPY;}
  end;
end;

function TPaperView.GetCanvas: TCanvas;
// return a pointer to the canvas which holds the painted data.
begin
  result:= pbx.Canvas;
end;

procedure TPaperView.UpdatePaintBoxSize;
// update the paint box width and height to reflect the current paper
var
  w, h  : integer;
begin
  GetPixSize(w, h);
  pbx.Width:= w;
  pbx.Height:= h;
{  if fOrient = poLandscape then
  begin
    pbx.Width:= h;
    pbx.Height:= w;
  end
  else
  begin
    pbx.Width:= w;
    pbx.Height:= h;
  end;}
end;

(*procedure TPaperView.SetPaperIdx(value: TPaperIndex);
// set the index for the paper represented on the component.
// this is one of the dmXXXX constants
begin
  if (value >= First_DMPaper) and (value <= Last_DMPaper) then
  begin
    fPaperIdx:= value;
    VertScrollBar.Position:= 0;
    HorzScrollBar.Position:= 0;
    pbx.Top:= 0;
    pbx.Left:= 0;
    UpdatePaintboxSize;
    pbx.Invalidate;
  end;
end;
*)
procedure TPaperView.SetZoom(value: double);
begin
  if (value <> fZoom) and (value >= 0.25) and (value <= MaxZoom) then
  begin
    HorzScrollBar.Position:= 0;
    VertScrollBar.Position:= 0;
    fZoom:= value;
    UpdatePaintBoxSize;
//    PaperIndex:= fPaperIdx; // force to recalc width of control.
    if Assigned(fOnZoomChange) then
      fOnZoomChange(self);
  end;
end;

procedure TPaperView.SetEdge(index: integer; value: TLTwips);
begin
  case index of
    0 : fEdgeX:= value;
    1 : fEdgeY:= value;
  end;
  pbx.Invalidate;
end;

procedure TPaperView.SetRefresh(value: boolean);
begin
  pbx.Refresh;
end;

{procedure TPaperView.SetOrient(value: TPrinterOrientation);
begin
  if (value <> fOrient) then
  begin
    fOrient:= value;
    VertScrollBar.Position:= 0;
    HorzScrollBar.Position:= 0;
    pbx.Top:= 0;
    pbx.Left:= 0;
    UpdatePaintboxSize;
    Invalidate;
  end;
end;}

procedure TPaperView.SetVisibles(index: integer; value: boolean);
begin
  case index of
    0 :   if value <> fGridVisible then
          begin
            fGridVisible:= value;
            pbx.Refresh;
          end;
    1 :   if value <> fMargVisible then
          begin
            fMargVisible:= value;
            pbx.Refresh;
          end;
  end;
end;

procedure TPaperView.SetGridSize(value: TLTwips);
begin
  if value <> fGridSize then
  begin
    fGridSize:= value;
    pbx.Refresh;
  end;
end;


procedure TPaperView.SetGridColor(value: TColor);
begin
  if value <> fGridColor then
  begin
    fGridColor:= value;
    pbx.Refresh;
  end;
end;

procedure TPaperView.SetMargins(index: integer; value: TLTwips);
begin
  case index of
    0 : if value <> fMargLeft then
        begin
          fMargLeft:= value;
          pbx.Refresh;
        end;
    1 : if value <> fMargTop then
        begin
          fMargTop:= value;
          pbx.Refresh;
        end;
    2 : if value <> fMargRight then
        begin
          fMargRight:= value;
          pbx.Refresh;
        end;
    3 : if value <> fMargBottom then
        begin
          fMargBottom:= value;
          pbx.Refresh;
        end;
  end;
end;

procedure TPaperView.SetFont(value: TFontName);
begin
  pbx.Font.Name:= value;
  fFontName:= pbx.Font.Name;
end;

procedure TPaperView.CMChildKey(var msg: TCMChildKey);
var
  shift : boolean;
  ctrl  : boolean;
  alt   : boolean;
begin
  msg.result:= 1;
  ctrl:= GetKeyState(VK_CONTROL) and $80000000 <> 0;
  shift:= GetKeyState(VK_SHIFT) and $80000000 <> 0;
  alt:=   GetKeyState(VK_MENU) < 0;
  case msg.charcode of
    VK_DOWN:
      if ctrl then
        VertScrollBar.position:= VertScrollBar.position + ClientHeight
      else
        VertScrollBar.position:= VertScrollBar.position + VertScrollBar.Increment;
    VK_UP  :
      if ctrl then
        VertScrollBar.position:= VertScrollBar.position - ClientHeight
      else
        VertScrollBar.position:= VertScrollBar.position - VertScrollBar.Increment;
    VK_LEFT:
      if ctrl then
        HorzScrollBar.position:= HorzScrollBar.position - ClientWidth
      else
        HorzScrollBar.position:= HorzScrollBar.position - HorzScrollBar.Increment;
    VK_RIGHT:
      if ctrl then
        HorzScrollBar.position:= HorzScrollBar.position + ClientWidth
      else
        HorzScrollBar.position:= HorzScrollBar.position + HorzScrollBar.Increment;
    VK_NEXT:
      if not (shift or alt or ctrl) then
        VertScrollBar.position:= VertScrollBar.position + ClientHeight
      else
        msg.result:= 0;
    VK_PRIOR:
      if not (shift or alt or ctrl) then
        VertScrollBar.position:= VertScrollBar.position - ClientHeight
      else
        msg.result:= 0;
  else
    msg.result:= 0;
  end;
end;

procedure TPaperView.SetPaperSize(index: integer; value: TLTWips);
// minimum size is 1 inch
begin
  if value > LT100 then
  begin
    case index of
      0 : fPaperWidth:= value;
      1 : fPaperHeight:= value;
    end;
    VertScrollBar.Position:= 0;
    HorzScrollBar.Position:= 0;
    pbx.Top:= 0;
    pbx.Left:= 0;
    UpdatePaintboxSize;
    pbx.Invalidate;
  end;
end;

(*procedure TPaperView.MouseDown(Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  inherited;
end;*)

procedure TPaperView.DoKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
// handle keys for zooming in and out through the keyboard.
// Zoom in Ctrl-,  zoom out Ctrl-U
var
  num   : integer;
begin
  if ssCtrl in Shift then
  begin
    num:= Trunc(fZoom * 100);
    num:= ((num + (25 - 1)) div 25) * 25;  // round up to multiple of 25
    key:= word(UpCase(char(key)));
    case key of
      Ord('I'), Ord('i'):
        begin
          if num < Trunc(MaxZoom*100) then
            Inc(num, 25)
          else
            num:= MaxZoom * 100;
          Zoom:= num / 100.0;
        end;
      Ord('O'), Ord('o'):
        begin
          if num > 25 then
            Dec(num, 25)
          else
            num:= 25;
          Zoom:= num / 100.0;
        end;
    end;
  end;
end;

function TPaperView.DevicePointToLTwips(aPt: TPoint): TPoint;
// convert a pixel point in Device units to LTwips
// Return value is relative to the upperleft of the paper represented
// by the component, not the origin of the client area
var
  hndl  : HDC;  // pointer to canvas handle
begin
  hndl:= pbx.canvas.Handle;
  SetLogicalTwips(hndl, fZoom);
  Dec(aPt.x, HorzScrollBar.position);
  Dec(aPt.y, VertScrollBar.position);
  DPtoLP(hndl, aPt, 1);
  apt.x:= apt.x - EdgeX;
  apt.y:= apt.y - EdgeY;
  result:= apt;
end;

{--[ TLTwipsEd ]----------------------------------------------------------}

function TLTwipsEd.GetAttributes: TPropertyAttributes;
begin
  result:= inherited GetAttributes + [paDialog];
end;

procedure TLTwipsEd.Edit;
var
  f   : TFrmLTwips;
  c   : TComponent;
begin
  f:= TFrmLTwips.Create(application);
  try
    c:= GetComponent(0) as TComponent;
    f.lblResult.caption:= IntToStr(GetOrdValue);
    f.Caption:= c.owner.name + '.' + c.name + '.' + GetName;
    if f.ShowModal = mrOK then
    begin
      SetOrdValue(StrToInt(f.lblResult.caption));
      Designer.Modified;
    end;
  finally
    f.Free;
  end;
end;

procedure Register;
begin
  RegisterComponents('DMS', [TPaperView]);
  RegisterPropertyEditor( TypeInfo(TLTwips),
                          nil, {TPaperView,}
                          '',
                          TLTwipsEd);
end;

end.

