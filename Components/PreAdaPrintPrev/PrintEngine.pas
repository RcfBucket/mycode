unit PrintEngine;
{$I CompileOptions.inc}

interface

uses
  Windows, SysUtils, Classes, Graphics, Printers, PrnData, PrnUtils;

type
  EPrintEngine = class(Exception);

  TPrintEngine = class(TComponent)
  private
    pData           : TPrintData;
    fCurPage        : integer;
    fTotalPages     : integer;
    fCurDev         : string;

    bdyInfo         : TList;  // pagination information
    ftrInfo         : TList;
    hdrInfo         : TList;
    footSize        : integer;
    headSize        : integer;
    pageSize        : TPoint;
    canvas          : TCanvas;
    startAt         : TPoint;
    fMinMarg         : TRect;
    rLimit          : integer;
    bLimit          : integer;
    startPrintDate  : TDateTime;
    procedure SetFontData(aName: string; aFmt, aPtSize: integer);
    function CompareFont(aName: string; aFmt, aPtSize: integer): boolean;
    procedure ClearInfoList(aList: TList);
    procedure CalcHeaderSize;
    procedure CalcFooterSize;
    function CalcRowHeight(cells: TCellList; aRec: longint;
                           var maxDescent: integer): integer;
    function CalcWrappedCellHeight(aRec, rowH: integer): integer;
    procedure Paginate;
    procedure SetPrintData(value: TPrintData);
    procedure SetPrintRect;
    procedure PerformPainting;
    procedure ExpandFormulae(var src: string; flags, line, lOnPage, page: integer);
  public
    constructor Create(aOwner: TComponent); override;
    destructor Destroy; override;
    procedure PaintPage(aCanvas: TCanvas; aPageNum: integer);
    function CalcTotalPages(aCanvas: TCanvas): integer;
    property PrintData: TPrintData read pData write SetPrintData;
    property MinMarg: TRect read fMinMarg write fMinMarg;
//    property PageNum: integer read fCurPage write fCurPage;
  end;

IMPLEMENTATION

uses
  TwipLib;

const
  intMarg        = 1;     // interior margin for cells (in PTs)
  fontExtra      = 2;     // amount to add to point size to correct

  R_GROUPBREAK  = $80000000;  // indicates a group page break

  FORMULA_MASK  = $03F00000;  // used as mask for formula flags
  FMT_MASK      = $0F;        // used to mask off style info from flags
                              // for proper cell size (in points)
type
  // TRowInfo describes a row of output.
  TRowInfo = class
    height     : integer;  {- maximum height of line at print time }
    maxDescent : integer;  {- maximum font descent for line }
    newPageRec : integer;  {- if non zero, then specifies the cell starting record
                              for a new page.}
    width      : integer;
    pageNum    : integer;
    constructor Create;
  end;

{--[ TRowInfo ]---------------------------------------------------------------}

constructor TRowInfo.Create;
begin
  inherited Create;
  height:= 0;
  maxDescent:= 0;
  newPageRec:= 0;
  pageNum:= 0;
  width:= 0;
end;

function MakeFontStyle(aFmt: integer): TFontStyles;
var
  s   : TFontStyles;
begin
  s:= [];
  if aFmt and c_Italic <> 0 then
    Include(s, fsItalic);
  if aFmt and c_Bold <> 0 then
    Include(s, fsBold);
  if aFmt and c_Underline <> 0 then
    Include(s, fsUnderline);
  if aFmt and c_Strikeout <> 0 then
    Include(s, fsStrikeout);
  result:= s;
end;

{--[ TPrintEngine ]-----------------------------------------------------------}

constructor TPrintEngine.Create(aOwner: TComponent);
begin
  inherited Create(aOwner);
//  fCurPage:= 1;
  fTotalPages:= 0;
//  fFont:= TFont.Create;
  fcurDev:= '';
  pData:= nil;
  pageSize:= Point(0, 0);
  bdyInfo:= TList.Create;
  ftrInfo:= TList.Create;
  hdrInfo:= TList.Create;
  fMinMarg:= Rect(0,0,0,0);
end;

destructor TPrintEngine.Destroy;
begin
  ClearInfoList(bdyInfo);
  ClearInfoList(ftrInfo);
  ClearInfoList(hdrInfo);
  bdyInfo.Free;
  ftrInfo.Free;
  hdrInfo.Free;
//  fFont.Free;
  inherited Destroy;
end;

procedure TPrintEngine.ClearInfoList(aList: TList);
var
  r   : TRowInfo;
  k   : integer;
begin
  for k:= 0 to aList.count - 1 do
  begin
    r:= aList[k];
    r.Free;
  end;
  aList.Clear;
end;

procedure TPrintEngine.SetFontData(aName: string; aFmt, aPtSize: integer);
{- set the cur Font info to the specified font information.
   aFmt is a format mask from the print data and should contain format specs
   like c_Bold or c_Underline }
begin
  canvas.Font.Name:= aName;
  canvas.Font.Height:= -PointsToLTwips(aPtSize);
  canvas.Font.Style:= MakeFontStyle(aFmt);
end;

function TPrintEngine.CompareFont(aName: string; aFmt, aPtSize: integer): boolean;
{- compare the specified info with the current font. Return true if the
   passed info differs fron the current font }
begin
  with canvas do
    result:= (Font.Name <> aName) or
             (Font.Height <> -PointsToLTwips(aPtSize)) or
             (Font.Style <> MakeFontStyle(aFmt));
end;

function TPrintEngine.CalcRowHeight(cells: TCellList; aRec: longint;
                                    var maxDescent: integer): integer;
{- calculate the maximum rectangle height required for the row to
   which aRec belongs. Also returns the maximum descent for the row.
   Assumes the correct font has already been selected into the canvas }
var
  maxPt: integer;
  tm   : TTextMetric;
  maxH : integer;  {- max height  }
//  tMax : integer;
  rowRec: TCell;
  w    : integer;
  pWid : integer;
  k    : integer;
  j    : integer;
  cell : TCell;
  tr   : TRect;
  tstr : string;
  wExt : TSize;
  vExt : TSize;
begin
  GetWindowExtEx(canvas.Handle, wExt);  // save current window and viewport extents
  GetViewPortExtEx(canvas.Handle, vExt);

  SetLogicalTwips(canvas.Handle, 1.0);  // set to zoom 1.0 for consistent measurements

  rowRec:= cells.cell[cells.FirstRecInLine(aRec)];
  if rowRec.maxHeight > 0 then
    maxPt:= rowrec.maxHeight
  else
    maxPt:= cells.MaxLinePtSize(aRec);  // calculate the maxumum point size for the row

  if CompareFont(canvas.Font.Name, 0, maxPt) then
    SetFontData(canvas.Font.Name, 0, maxPt);

  GetTextMetrics(canvas.handle, tm);

  if rowRec.maxheight >= 0 then
    maxH:= tm.tmHeight + tm.tmExternalLeading + PointsToLTwips(fontExtra)
  else
    maxH:= tm.tmHeight;
  maxDescent:= tm.tmDescent;  // get max char descent (used to align text baselines }

  {- now given the maximum height for single row, see if any lines
     are specified as wrapped text. If so, calulcate the height of
     the rectangle based on its font. if this height is larger than
     the single row max, then adjust max to this new height }

  w:= 0;
  pWid:= pageSize.x - pData.MarginLeft - pData.MarginRight; {- printable width }

  if rowRec.maxHeight <= 0 then
  begin
    {- add one (in for loop init) to skip the row desc record }
    for k:= cells.FirstRecInLine(aRec) + 1 to cells.LastRecInLine(aRec) do
    begin
      cell:= cells.cell[k];
      if cell.flags and c_WordWrap <> 0 then
      begin
        // see if the current font is accurate for this cell
        if CompareFont(canvas.Font.Name, cell.flags, cell.ptSize) then
          SetFontData(canvas.Font.Name, cell.flags, cell.ptSize);
        GetTextMetrics(canvas.Handle, tm);

        tstr:= cell.text;

        if cell.flags and c_Stretch <> 0 then
        begin
          cell.width:= pWid - w;
          if cell.width < 0 then
            cell.width:= 0;
          Inc(w, cell.width);
        end;

        {- adjust the width of the rectangle until CALCRECT comes up with
           a rectangle that has a width at least as wide as width specified}
        j:= 0;
        repeat
          SetRect(tr, 0, 0, cell.width - (j*LT08), 0);
          tstr:= tstr + 'W';  // add some fluff
          DrawTextEx(canvas.Handle, PChar(tstr), Length(tstr), tr,
            DT_CALCRECT or DT_NOPREFIX or DT_EXTERNALLEADING or DT_WORDBREAK or
            DT_EDITCONTROL, nil);
          Inc(j);
        until (tr.right - tr.left <= cell.width) or (j > 8);

  (*---      tMax:= tr.bottom div tm.tmHeight; {- number of lines in wrapped cell}
        tMax:= tMax * p^.ptSize * 20; {- mult by point size and convert to lTwips}
        Inc(tMax, fontExtra * 20);
        if tMax > maxH then
          maxH:= tMax;*)

        if tr.bottom - tr.top > maxH then
          maxH:= (tr.bottom - tr.top) + PointsToLTwips(fontExtra);
      end
  (*    else if cell.flags and c_Bitmap <> 0 then
      begin
        Inc(w, LT100);
        if LT100 > maxH then
          maxH:= LT100;
  //      if cell.bmp.height > maxH then
  //        maxH:= cell.bmp.height;
      end*)
      else
        Inc(w, cell.width);
    end;
  end;
  CalcRowHeight:= maxH + PointsToLTwips(pData.rowSpacing);

  {- restore original extents }
  SetWindowExtEx(canvas.handle, wExt.cx, wExt.cy, nil);
  SetViewPortExtEx(canvas.handle, vExt.cx, vExt.cy, nil);
end;

procedure TPrintEngine.CalcHeaderSize;
var
  k    : integer;
  r    : TRowInfo;
begin
  ClearInfoList(hdrInfo);
  headSize:= 0;
  for k:= 0 to pData.header.TotalRecords - 1 do
  begin
    if pData.Header.IsBOL(k) then
    begin
      r:= TRowInfo.Create;
      r.height:= CalcRowHeight(pData.header, k, r.maxDescent);
      r.width:= pData.Header.TotalRowWidth(k);
      Inc(headSize, r.height);
      hdrInfo.Add(r);
    end;
  end;
end;

procedure TPrintEngine.CalcFooterSize;
var
  k     : integer;
  r     : TRowInfo;
begin
  ClearInfoList(ftrInfo);
  footSize:= 0;
  for k:= 0 to pData.footer.TotalRecords - 1 do
  begin
    if pData.footer.IsBOL(k) then
    begin
      r:= TRowInfo.Create;
      r.height:= CalcRowHeight(pData.Footer, k, r.maxDescent);
      r.width:= pData.Footer.TotalRowWidth(k);
      Inc(footSize, r.height);
      ftrInfo.Add(r);
    end;
  end;
end;

function TPrintEngine.CalcWrappedCellHeight(aRec, rowH: integer): integer;
{- rowH is maximum height calculated from CalcRowHeight. aRec specifies a record
   in the row being calculated. Assumes rLimit has already been set }
var
  k, j     : integer;
  h, w     : integer;
  p        : TCell;
  wrapMarg : integer;
begin
  w:= startAt.x;
  h:= rowH;
  j:= pData.body.FirstRecInLine(aRec);
  p:= pData.body.cell[j];   // get wrap margin (indent)
  wrapMarg:= p.width;

  for k:= j + 1 to pData.body.LastRecInLine(aRec) do
  begin
    p:= pData.body.cell[k];
(* what if a one cell is wider than paper??? will this wrap forever ?*)
    if w + p.width > rLimit then  // will need to wrap this cell
    begin
      Inc(h, rowH);
      w:= startAt.x + wrapMarg;
    end
    else
      Inc(w, p.width);
  end;
  result:= h;
end;

procedure TPrintEngine.SetPrintRect;
{- initialize the actual starting coordinates for a page (in LTwips)
   and calculates a right limit (rLimit) and a bottom limit (bLimit).
   These two values are the out of bounds markers for printing.

   rLimit- The rightmost limit for printing. No printing will
           exceed past this limit.
   bLimit- the bottom limit for printing. Printing of the body
           will not exceed this limit. The footer will start
           at bLimit.
   Assumes that the correct footer size has already been calculated.
}
begin
  {- adjust starting position by subtracting the minimum margin for the
     dc (if any) from the actual margin thus calculating the actual
     "real" distance from the DC's origin }
  startAt.x:= pData.MarginLeft - fMinMarg.left;
  startAt.y:= pData.MarginTop - fMinMarg.top;
  rLimit:= startAt.x + pageSize.x - pData.MarginRight - pData.MarginLeft;
  bLimit:= startAt.y + pageSize.y - pData.MarginBottom - pData.MarginTop - footSize;
end;

procedure TPrintEngine.Paginate;
// Scan the print data and build internal structures for proper pagination.
// All pagination is based on the currently selected printer.
// Assumes that Canvas has been set to a valid canvas.
var
  rec        : integer;  // current cell record num (should always be pointing
                         // to a row desc record.
  pr         : TRowInfo; // holds pointer to a row record.
  pc         : TCell;    // holds pointer for a cell rec
  curHeight  : integer;  // hold cur calc'ed height for the cur page
  sGrp       : integer;  // starting group record
  sGrpRow    : integer;  // row where group started
  htAtGrp    : integer;  // current height at start of group
  inGrp      : boolean;  // true if processing in a group
  grpRec     : TCell;    // pointer to cell at start of group
  rowOnPage  : integer;  // keeps track of the current row on the page
  lookForEnd : boolean;  // true when looking for an end group. used for syntax
                         // checking start/end group pairs
  pgNum      : integer;
//  orient     : integer;
begin
  // Setup page specific information. Information is gathered from the
  // currently selected printer.
  fCurDev:= GetPrintDeviceName;
  SelectedPaperSize(pageSize);
//  fMinMarg:= DCMargins(canvas.Handle);
  SetFontData(pData.FontName, 0, pData.DefaultPointSize);
  fTotalPages:= 0;
  ClearInfoList(bdyInfo);
  startPrintDate:= Now;

  CalcFooterSize;  {- set footer size }
  CalcHeaderSize;  {- set header size }
  SetPrintRect;    {- set bLimit/rLimit and startX/Y }

  {- clear any system flags set by previous scan}
  pData.body.ClearFlags(r_SysFlags);

  curHeight:= headSize + startAt.y;
  rec:= 0;
  inGrp:= false;
  lookForEnd:= false;
  sGrp:= 0;
  sGrpRow:= 0;
  grpRec:= nil;
  htAtGrp:= 0;
  pgNum:= 1;
  rowOnPage:= 1;
  while rec < pData.body.TotalRecords do
  begin
    pc:= pData.body.cell[rec];  // get cell pointer
    pr:= TRowInfo.Create;       // initialize a row element

    // if in a group and a manual page break found, desregard the group.
    if inGrp and (pc.flags and r_PageBreak <> 0) then
    begin
      inGrp:= false;
      sGrp:= 0;
      sGrpRow:= 0;
      grpRec:= nil;
    end;

    pr.Height:= CalcRowHeight(pData.body, rec, pr.maxdescent);
    pr.width:= pData.body.TotalRowWidth(rec);
    if pc.flags and r_CellWrap <> 0 then
    begin
      Inc(curHeight, CalcWrappedCellHeight(rec, pr.height));
    end
    else
      Inc(curHeight, pr.height);

    if (pc.flags and r_PageBreak <> 0) or (curHeight > bLimit) then
    begin
      Inc(pgNum);
      rowOnPage:= 0;
      if not inGrp then
      begin
        pr.newPageRec:= rec;
        pr.pageNum:= pgNum;
      end
      else
      begin
        // natural break inside of a group. need to adjust the group
        TRowInfo(bdyInfo[sGrpRow]).newPageRec:= sGrp;
        TRowInfo(bdyInfo[sGrpRow]).pageNum:= pgNum;
        grpRec.flags:= grpRec.flags or r_GroupBreak;
        inGrp:= false;
        sGrp:= 0;
        grpRec:= nil;
        {- reset curHeight }
        curHeight:= startAt.y + headSize + (curHeight - htAtGrp);
        htAtGrp:= 0;
      end;
    end;

    {- if new page started }
    if pr.newPageRec <> 0 then
      {- reset curHeight}
      curHeight:= headSize + startAt.y + pr.height;

    {- see if a group is specified }
    if pc.rowDesc and (pc.flags and (r_StartGroup or r_EndGroup) <> 0) then
    begin
      {- if end group found and already in a group }
      if (pc.flags and r_EndGroup <> 0) and (inGrp or lookForEnd) then
      begin
        inGrp:= false;
        lookForEnd:= false;
        sGrp:= 0;
        sGrpRow:= 0;
        grpRec:= nil;
        htAtGrp:= 0;
      end;
      {- if not already looking for an End Group then process the Start group }
      if (pc.flags and r_StartGroup <> 0) and not lookForEnd then
      begin
        {- if this is already the first row on the page, there is no need to
           process the group }
        if rowOnPage > 1 then
        begin
          inGrp:= true;
          lookForEnd:= true;
          sGrp:= rec;  {- save group cell start record }
          sGrpRow:= bdyInfo.count; {- save row record }
          grpRec:= pc;
          htAtGrp:= curHeight - pr.height;
        end
        else
        begin
          lookForEnd:= true;
          inGrp:= false;
          sGrp:= 0;
          sGrpRow:= 0;
          grpRec:= nil;
        end;
      end;
    end;

    bdyInfo.Add(pr);
    rec:= pData.body.LastRecInLine(rec) + 1;  {- go to next line }
    Inc(rowOnPage);
  end;
  fTotalPages:= pgNum;
end;

procedure TPrintEngine.ExpandFormulae(var src: string; flags, line, lOnPage, page: integer);
{- expand any formula codes in flags, inserting them in the specified place in the
   src string. src string should be formatted with substitution characters as follows:
   ^p  - page number
   ^n  - total pages
   ^l  - line number
   ^c  - current line on page
   ^d  - date
   ^t  - time
   if src does not specify a substitution char, then the expanded formula will just
   be appended to src.  result MUST be large enough to accept the resultant string. }
const
  hideChar = #1;
var
  src2  : string;
//  format: string;
  sub   : string;
  sStr  : string;

  procedure HidePercents;
  // hide all percent chars in the original string
  var
    i  : integer;
  begin
    repeat
      i:= Pos('%', src);
      if i > 0 then
        src[i]:= hideChar;
    until i = 0;
  end;

  procedure UnhidePercents;
  var
    i  : integer;
  begin
    repeat
      i:= Pos(hideChar, src);
      if i > 0 then
        src[i]:= '%';
    until i = 0;
  end;

  procedure InsSubString(substitute, subStr: string);
  {- insert subStr into result replacing the substitution string [substitute] }
  {- on entry, src has the current format string }
  {- on exit, src will have the new format string }
  type
    TArgList = array[0..1] of PChar;
  var
    p     : integer;
    count : integer;
//    arg   : ^TArglist;
//    k     : integer;
  begin
    count:= 0;
    repeat
      p:= Pos(substitute, src);  {- find substitute code }
      if p = 0 then
        p:= Pos(UpperCase(substitute), src);
      if (p > 0) then
      begin
        Delete(src, p, 2);
        system.Insert(subStr, src, p);
        Inc(count);
      end;
    until p = 0;
    if count = 0 then
      {- no format substitution code specified, so just add string to end of src}
      src:= src + subStr;
  end;

begin
  HidePercents;
  src2:= '';

  if flags and c_PageNum <> 0 then
  begin
    sub:= FormatFloat('#,##0', page);
    sStr:= '^p';
    InsSubString(sStr, sub);
  end;
  if flags and c_NumPages <> 0 then
  begin
    sub:= FormatFloat('#,##0', fTotalPages);
    sStr:= '^n';
    InsSubString(sStr, sub);
  end;
  if flags and c_LineNum <> 0 then
  begin
    sub:= FormatFloat('#,##0', line);
    sStr:= '^l';
    InsSubString(sStr, sub);
  end;
  if flags and c_LineOnPage <> 0 then
  begin
    sub:= FormatFloat('#,##0', lOnPage);
    sStr:= '^c';
    InsSubString(sStr, sub);
  end;
  if flags and c_Date <> 0 then
  begin
    sub:= DateToStr(startPrintDate);
    sStr:= '^d';
    InsSubString(sStr, sub);
  end;
  if flags and c_Time <> 0 then
  begin
    sub:= TimeToStr(startPrintDate);
    sStr:= '^t';
    InsSubString(sStr, sub);
  end;
  UnhidePercents;
end;

procedure TPrintEngine.PerformPainting;
// paint the current data.  Assumes pagination has occured
// painging will start at the page specified by fCurPage
// Assumes logical pixels are already selected in the canvas.
var
  r           : TRect;     {- used for printing rectangle }
  curDescent  : integer;   {- descent for current cell }
  LTpPX, LTpPY: integer;

  procedure CheckFont(cur: TCell);
  {- check current cell's font requirements to that of the
     currently selected font. if different, build and select
     a new font }
  var
    tm  : TTextMetric;
  begin
    if CompareFont(canvas.Font.Name, cur.flags, cur.ptSize) then
    begin
      SetFontData(canvas.Font.Name, cur.flags, cur.ptSize);
      GetTextMetrics(canvas.handle, tm);
      curDescent:= tm.tmDescent;
    end;
  end;

  procedure SetRectWidth(cur: TCell);
  {- calculate the width of the rectangle for the current cell }
  var
    k   : integer;
  begin
    {- first check for cell alignment options }
    if cur.flags and c_PageLeft <> 0 then
      r.left:= startAt.x
    else if cur.flags and c_PageRight <> 0 then
      r.left:= rLimit - cur.width
    else if cur.flags and c_PageCenter <> 0 then
    begin
      k:= startAt.x + ((rLimit - startAt.x) div 2);
      Dec(k, cur.width div 2);
      r.left:= k;
    end;

    if cur.flags and c_Stretch <> 0 then
      r.right:= rLimit
    else
      r.right:= r.left + cur.width + LTpPX;
  end;

  procedure InitPrint;
  {- set up for printing, create the default font and set the
     default font styles (curFmt) and set the initial rectangle }
  var
    tm  : TTextMetric;
  begin
    LTwipsPerPixel(canvas.handle, LTpPX, LTpPY);
//    canvas.Brush.Style:= bsClear; <- this messes up a deskjet in win95 for some reason???
    canvas.Pen.Style:= psSolid;
    canvas.Pen.Width:= 1;
    canvas.Pen.Color:= clBlack;

    {- create font with default height and select into DC }
    SetFontData(pData.FontName, 0, pData.DefaultPointSize);
    GetTextMetrics(canvas.handle, tm);
    curDescent:= tm.tmDescent;

    r:= Rect(startAt.x, startAt.y, startAt.x, startAt.y);
  end;

  procedure PrintOne(cells: TCellList; curRowInfo: TList; curRowNum: integer);
  var
    pRect      : TRect;
    dt_Fmt     : integer;
    tstr       : string;
    cur        : TCell;     {- current print cell }
    rec        : integer;
    row        : TRowInfo;
    breakPage  : boolean;
    firstLine  : boolean;
    curLine    : integer;
    tWidth     : integer;
    inWrapRow  : boolean;
    wrapMarg   : integer;
  begin
    wrapMarg:= 0;
    inWrapRow:= false;
    breakPage:= false;
    firstLine:= true;
    row:= curRowInfo[curRowNum];
    rec:= row.newPageRec;
    curLine:= 0;

    Dec(curRowNum);  {- this needs to be done because of the structure of the
                        following loop and because line break cells come before
                        the actual data. the curRowNum will be incremented back
                        immediately when the loop first starts.
                     }
    while (rec < cells.TotalRecords) and
          (((r.bottom <= bLimit) and (cells <> pData.footer)) or
          (cells = pData.footer)) and (not breakPage) do
    begin
      cur:= cells.cell[rec];

      {- if a new row is found or current row exceeds the right margin }
      {- then go on to next row }
      if cur.rowDesc or (r.left > (rLimit + LTpPX)) then
      begin
        breakPage:=  not firstLine and
                     (cur.flags and (r_PageBreak or r_GroupBreak) <> 0);
        firstLine:= false;

        inWrapRow:= (cur.flags and r_CellWrap <> 0);
        if inWrapRow then
          wrapMarg:= cur.width;

        Inc(curLine);
        Inc(curRowNum);
        row:= curRowInfo[curRowNum];  {- get next for information record }

        r.left:= startAt.x;
        r.top:= r.bottom - LTpPY;
        r.bottom:= r.top + row.height + LTpPY;

        if not breakPage then
        begin
          {- check for row borders }
          if cur.flags and r_BorderTop <> 0 then
          begin
            canvas.MoveTo(r.left, r.top);
            canvas.LineTo(rLimit, r.top);
          end;
          if cur.flags and r_BorderBottom <> 0 then
          begin
            canvas.MoveTo(r.left, r.bottom - LTpPY);
            canvas.LineTo(rLimit, r.bottom - LTpPY);
          end;
          if cur.flags and r_BorderLeft <> 0 then
          begin
            canvas.MoveTo(r.left, r.top);
            canvas.LineTo(r.left, r.bottom);
          end;
          if cur.flags and r_BorderRight <> 0 then
          begin
            canvas.MoveTo(rLimit, r.top);
            canvas.LineTo(rLimit, r.bottom);
          end;
        end;
        {- if center row specified, adjust current rectangle accordingly }
        if cur.flags and r_CenterRow <> 0 then
        begin
          tWidth:= row.width;
          r.left:= r.left + ((rLimit - r.left) div 2) - (tWidth div 2);
          Inc(r.left, 2 * LTpPX);  { ?? }
        end;
      end
      else
      begin {- actual cell data }
        CheckFont(cur);     // make sure font is ok for current cell
        SetRectWidth(cur);

        if (r.right > (rLimit + LTpPX)) and inWrapRow then
        begin
          {- need to adjust rectangle to next line (wrap it) }
          r.left:= startAt.x;
          if wrapMarg > 0 then
            Inc(r.left, wrapMarg);
          r.top:= r.bottom - LTpPY;
          r.bottom:= r.top + row.height + LTpPY;
          if cur.flags and c_Stretch <> 0 then
            r.right:= rLimit
          else
            r.right:= r.left + cur.width + LTpPX;
        end
        else  {- if right goes beyond the right margin then truncate }
          if r.right > (rLimit + LTpPX) then
            r.right:= rLimit;

        tstr:= cur.text;    // get text of cell

        if cur.flags and Formula_Mask <> 0 then
          ExpandFormulae(tstr, cur.flags, curRowInfo.IndexOf(row), curLine, fCurPage);

        pRect:= r;  {- get printing rectangle }
        InflateRect(pRect, -intMarg*20, -intMarg*20); {- deflate for interior margin}

        if (cur.flags and c_BorderAll <> 0) then
        begin
          if cur.flags and c_BorderAll = c_BorderAll then
            canvas.Rectangle(r.left, r.top, r.right, r.bottom)
          else
          begin
            if cur.flags and c_BorderTop <> 0 then
            begin
              canvas.MoveTo(r.left, r.top);
              canvas.LineTo(r.right, r.top);
            end;
            if cur.flags and c_BorderBottom <> 0 then
            begin
              canvas.MoveTo(r.left, r.bottom - LTpPY);
              canvas.LineTo(r.right, r.bottom - LTpPY);
            end;
            if cur.flags and c_BorderLeft <> 0 then
            begin
              canvas.MoveTo(r.left, r.top);
              canvas.LineTo(r.left, r.bottom);
            end;
            if cur.flags and c_BorderRight <> 0 then
            begin
              canvas.MoveTo(r.right - LTpPX, r.top);
              canvas.LineTo(r.right - LTpPX, r.bottom);
            end;
          end;
        end;

        with cur do
        begin
          {- adjust the text in the cell to line up with the font baseline for
             the row (n/a for wordwrap cells }
          if (flags and c_WordWrap = 0) and (flags and c_Baseline <> 0) then
            Dec(pRect.bottom, row.maxDescent - curDescent);

          dt_Fmt:= DT_NOPREFIX or DT_SINGLELINE; {- default }

          if flags and c_WordWrap <> 0 then
            dt_Fmt:= (dt_Fmt or DT_WORDBREAK or DT_EDITCONTROL or DT_EXTERNALLEADING) and not (DT_SINGLELINE or DT_BOTTOM or DT_VCENTER);

          if flags and c_Left <> 0 then
            dt_Fmt:= (dt_Fmt or DT_LEFT) and not (DT_RIGHT or DT_CENTER);

          if flags and c_Right <> 0 then
            dt_Fmt:= (dt_Fmt or DT_RIGHT) and not (DT_LEFT or DT_CENTER);

          if flags and c_Top <> 0 then
            dt_Fmt:= (dt_Fmt or DT_TOP) and not (DT_BOTTOM or DT_VCENTER);

          if (flags and c_Bottom <> 0) or (flags and c_Baseline <> 0) then
            dt_Fmt:= (dt_Fmt or DT_BOTTOM) and not (DT_TOP or DT_VCENTER);

          if flags and c_Center <> 0 then
            dt_Fmt:= (dt_Fmt or DT_CENTER) and not DT_LEFT and not DT_RIGHT;

          if (flags and c_VCenter <> 0) and (flags and c_WordWrap = 0) then
            dt_Fmt:= (dt_Fmt or DT_VCENTER) and not DT_TOP and not DT_BOTTOM;
        end;

        // actually get to draw the cell text here (whew!)
        DrawTextEx(canvas.handle, PChar(tstr), length(tstr), pRect, dt_Fmt, nil);

        r.left:= r.right - LTpPX; {- increment to next position to
                                     the right of current cell}
      end;

      Inc(rec); {- go to next cell record }
    end;
  end;

  function FindRowForPage(aPageNum: integer): integer;
  {- find first row information record for the body of report for
     the specified page number }
  var
    k       : integer;
  begin
    result:= 0;
    for k:= 0 to bdyInfo.count - 1 do
    begin
      if TRowInfo(bdyInfo[k]).pageNum = aPageNum then
      begin
        result:= k;
        break;
      end;
    end;
  end;

begin
  InitPrint;
  if pData.header.TotalRecords > 0 then
    PrintOne(pData.header, hdrInfo, 0);

  if pData.body.TotalRecords > 0 then
    PrintOne(pData.body, bdyInfo, FindRowForPage(fCurPage));

  if pData.footer.TotalRecords > 0 then
  begin
    r.top:= bLimit;  {- set distance for footer }
    r.bottom:= bLimit;
    PrintOne(pData.footer, ftrInfo, 0);
  end;
end;

function TPrintEngine.CalcTotalPages(aCanvas: TCanvas): integer;
var
  tSize   : TPoint;
//  orient  : integer;
begin
  if pData = nil then   // make sure print data has been specified.
    result:= 0
  else
  begin
    if aCanvas = nil then
      raise EPrintEngine.Create('Invalid canvas passed to print engine.');
    canvas:= aCanvas;

    (* check to see if minMarg has changed? *)

    // first see if the engine has been initialized with the current
    // printer settings and the print data has been paginated.
    SelectedPaperSize(tSize);
    if (fCurDev <> GetPrintDeviceName) or (tSize.x <> pageSize.x) or
       (tSize.y <> pageSize.y) then
    begin
      Paginate;
    end;
    result:= fTotalPages;
  end;
end;

procedure TPrintEngine.PaintPage(aCanvas: TCanvas; aPageNum: integer);
// Assumes that a logical twips mapping mode has been set on the canvas
var
  tSize   : TPoint;
//  orient  : integer;
begin
  if pData = nil then   // make sure print data has been specified.
    exit;
  if aCanvas = nil then
    raise EPrintEngine.Create('Invalid canvas passed to print engine.');
  canvas:= aCanvas;

  // first see if the engine has been initialized with the current
  // printer settings and the print data has been paginated.
  SelectedPaperSize(tSize);
  if (fCurDev <> GetPrintDeviceName) or (tSize.x <> pageSize.x) or
     (tSize.y <> pageSize.y) then
  begin
    Paginate;
  end;

  if (aPageNum <= 0) or (aPageNum > fTotalPages) then
    exit;
//    raise EPrintEngine.Create('Invalid page number.');

  fCurPage:= aPageNum;

  PerformPainting;
end;

procedure TPrintEngine.SetPrintData(value: TPrintData);
begin
  fCurDev:= '';           // this will force a repagination on next paint.
  PageSize:= Point(0,0);
  fCurPage:= 0;
  pData:= value;
end;


END.

