unit PrnPrev;                                                           {- rcf}
{$I CompileOptions.inc}

(* look into the cmchildkey and make sure it is correct

   Error - form wont repaint after,
           Set zoom to 125, then do File|Print then cancel.
*)

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Buttons, ExtCtrls, Printers, StdCtrls, PrnUtils, PrnData, ComCtrls,
  PaperView, PrintEngine, Menus, ToolWin, ImgList, ActnList;

type
  TFrmPrintPrev = class(TForm)
    dlgPrintSetup: TPrinterSetupDialog;
    dlgPrint: TPrintDialog;
    barStatus: TStatusBar;
    PaperView1: TPaperView;
    MainMenu1: TMainMenu;
    File1: TMenuItem;
    View1: TMenuItem;
    Print2: TMenuItem;
    PrintSetup1: TMenuItem;
    N1: TMenuItem;
    Close1: TMenuItem;
    Margins1: TMenuItem;
    Gridlines1: TMenuItem;
    N2: TMenuItem;
    NextPage1: TMenuItem;
    PreviousPage1: TMenuItem;
    FirstPage1: TMenuItem;
    LastPage1: TMenuItem;
    N3: TMenuItem;
    GotoPage1: TMenuItem;
    ToolBar1: TToolBar;
    BtnPrint: TToolButton;
    BtnSetup: TToolButton;
    ToolButton3: TToolButton;
    BtnMargins: TToolButton;
    BtnGrid: TToolButton;
    ToolButton6: TToolButton;
    cbxZoom: TComboBox;
    ToolButton7: TToolButton;
    BtnFirst: TToolButton;
    BtnPrev: TToolButton;
    BtnGoto: TToolButton;
    BtnNext: TToolButton;
    BtnLast: TToolButton;
    ImageList1: TImageList;
    ActionList1: TActionList;
    ActGridLines: TAction;
    ActMargins: TAction;
    ActGoto: TAction;
    ActFirst: TAction;
    ActLast: TAction;
    ActPrev: TAction;
    ActNext: TAction;
    ActSetup: TAction;
    ActPrint: TAction;
    ActClose: TAction;
    procedure cbxZoomChange(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure cbxZoomKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure PaperView1PaintPage(sender: TPaperView; aCanvas: TCanvas);
    procedure cbxZoomExit(Sender: TObject);
    procedure PaperView1ZoomChange(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtnMarginsClick(Sender: TObject);
    procedure ActGridLinesExecute(Sender: TObject);
    procedure ActMarginsExecute(Sender: TObject);
    procedure ActGotoExecute(Sender: TObject);
    procedure ActFirstExecute(Sender: TObject);
    procedure ActPrevExecute(Sender: TObject);
    procedure ActNextExecute(Sender: TObject);
    procedure ActLastExecute(Sender: TObject);
    procedure ActSetupExecute(Sender: TObject);
    procedure ActPrintExecute(Sender: TObject);
    procedure ActCloseExecute(Sender: TObject);
  private
    engine     : TPrintEngine;
    fPageNum    : integer;
    fTotalPages : integer;
    fPrnDlgOnPrint : boolean;
    procedure ShowPage;
    procedure ShowPaperSize;
    procedure CMChildKey(var Message: TCMChildKey); message CM_CHILDKEY;
  public
    procedure WMGetMinMaxInfo(var msg: TWMGetMinMaxInfo); message WM_GETMINMAXINFO;
    destructor Destroy; override;
  end;

  TPrevFormOpts = class
    public
    WindowStyle : TWindowState;
    FlatButtons : boolean;
    PreviewWinTitle: string;
    PrnDlgOnPrint  : boolean; // show print selection dlg on print button
    ShowHints      : boolean; // show tool bar hints
  end;

const
  FirstPage = -1;   // use these two constants as parameters to PrintPreview
  LastPage  = -1;   // function. If specified, printpreview will start at
                    // page one and end on the last page.

function PrintPreview(data: TPrintData; printType: integer;
                       startPage, endPage: integer; opts: TPrevFormOpts;
                       aCanv: TCanvas): integer;

IMPLEMENTATION

uses GotoPage, TwipLib, prndlg;

{$R *.DFM}

destructor TFrmPrintPrev.Destroy;
begin
  inherited Destroy;
end;

procedure PrintPrim(engine: TPrintEngine; startPage, endPage: integer);
// send data directly to the printer
var
  pgs, k  : integer;
  frm     : TFrmPrinting;
begin
  frm:= TFrmPrinting.Create(application);
  frm.lblDocName.caption:= engine.PrintData.DocumentName;
  frm.Show;
  try
    with printer do
    begin
      title:= frm.lblDocName.caption;
      BeginDoc;
      SetLogicalTwips(printer.canvas.handle, 1.0);
      pgs:= engine.CalcTotalPages(canvas);
      if (endPage <= 0) or (endPage > pgs) then
        endPage:= pgs;
      if (startPage <= 0) or (startPage > endPage) then
        startPage:= 1;
      k:= startPage;
      while not frm.PrintAborted and (k <= endPage) do
      begin
        frm.SetPages(k, pgs);
        Application.Processmessages;
        if not frm.printAborted then
        begin
          try
            SetLogicalTwips(printer.canvas.handle, 1.0);
            engine.PaintPage(printer.canvas, k);
            if k <> endPage then
              NewPage;
          except
            raise;
          end;
        end;
        Inc(k);
      end;
      if frm.PrintAborted then
        printer.Abort
      else
        EndDoc;
    end;
  finally
    frm.Hide;
    frm.Free;
  end;
end;

function PrintPreview(data: TPrintData; printType: integer;
                       startPage, endPage: integer; opts: TPrevFormOpts;
                       aCanv: TCanvas): integer;
var
  eng   : TPrintEngine;
  dlg   : TPrintDialog;
begin
  Result := mrCancel;
  if data.DefaultOrientation <> ppDontCare then
  begin
    if data.DefaultOrientation = ppPortrait then
      printer.Orientation:= poPortrait
    else
      printer.Orientation:= poLandscape;
  end;

  case printType of
    0:
      with TFrmPrintPrev.Create(application) do
      begin
        try
          screen.Cursor:= crHourGlass;
          if opts <> nil then
          begin
            fPrnDlgOnPrint:= opts.PrnDlgOnPrint;
            ShowHint:= opts.ShowHints;
            if opts.PreviewWinTitle <> '' then
            begin
              caption:= opts.PreviewWinTitle;
              if data.DocumentName <> '' then
                caption:= caption + ' - ' + data.DocumentName;
            end
            else if data.DocumentName <> '' then
              caption:= data.DocumentName;

            windowState:= opts.windowStyle;
            toolBar1.Flat:= opts.FlatButtons;
          end
          else
            caption:= data.DocumentName;

          paperView1.MarginLeft:= data.MarginLeft;
          paperView1.MarginTop:= data.MarginTop;
          paperView1.MarginRight:= data.MarginRight;
          paperView1.MarginBottom:= data.MarginBottom;
          engine.PrintData:= data;
          fTotalPages:= engine.CalcTotalPages(paperView1.canvas);
          if (startPage > 0) and (startPage <= fTotalPages) then
            fPageNum:= startPage
          else
            fPageNum:= 1;
          screen.Cursor:= crDefault;
          result:= ShowModal;
(*          Result := mrOk;*)
        finally
          Free;
        end;
      end;
    1:   // print directly (no prompting)
      begin
        eng:= TPrintEngine.Create(nil);
        try
          eng.MinMarg:= DCMargins(printer.handle);
          eng.PrintData:= data;
          PrintPrim(eng, startPage, endPage);
          Result := mrOk;
        finally
          eng.Free;
        end;
      end;
    2:    // call print dialog before printing
      begin
        eng:= TPrintEngine.Create(nil);
        eng.MinMarg:= DCMargins(printer.handle);
        dlg:= TPrintDialog.Create(application);
        try
          eng.PrintData:= data;
          dlg.Options:= [poPrintToFile, poPageNums];
          dlg.Copies:= 1;
          dlg.MinPage:= 1;
          dlg.MaxPage:= eng.CalcTotalPages(aCanv);
          dlg.FromPage:= 1;
          dlg.ToPage:= eng.CalcTotalPages(aCanv);
          if dlg.Execute then
            Result := mrOk
          else
            Result := mrCancel;
          if Result=mrOk then
            PrintPrim(eng, dlg.FromPage, dlg.ToPage);
        finally
          eng.Free;
          dlg.Free;
        end;
      end;
  end;
end;

procedure TFrmPrintPrev.cbxZoomChange(Sender: TObject);
var
  aZoom : integer;
  tstr  : string;
  k     : integer;
begin
  tstr:= cbxZoom.text;
  aZoom:= 1;
  if tstr <> '' then
  begin
    k:= Pos('%', tstr);
    if k > 0 then
      tstr:= Copy(tstr, 1, k-1);
    try
      aZoom:= StrToInt(tstr);
    except
{      cbxZoom.text:= IntToStr(Trunc(paperView1.Zoom * 100)) + '%';
      raise;}
    end;
    if (aZoom >= 25) and (aZoom <= 300) then
      paperView1.Zoom:= aZoom / 100.0
    else
    begin
{      ShowMessage('Invalid zoom range. Range is 25% - 300%.');
      cbxZoom.text:= IntToStr(Trunc(paperView1.Zoom * 100)) + '%';}
    end;
  end;
end;

procedure TFrmPrintPrev.ShowPaperSize;
begin
//  barStatus.Panels[1].text:= PaperName(paperView1.PaperIndex);
end;

procedure TFrmPrintPrev.FormShow(Sender: TObject);
begin
  ShowPaperSize;
  cbxZoom.text:= IntToStr(Trunc(paperView1.Zoom * 100)) + '%';
  BtnMargins.Down:= paperView1.MarginVisible;
  BtnGrid.Down:= paperView1.GridVisible;
  ShowPage;
  Margins1.Checked:= paperView1.MarginVisible;
  GridLines1.Checked:= paperView1.GridVisible;
end;

procedure TFrmPrintPrev.CMChildKey(var Message: TCMChildKey);
begin
  if message.CharCode = VK_RETURN then
  begin
    if activeControl = cbxZoom then
      paperView1.SetFocus;
    message.result:= 1;
  end;
end;


procedure TFrmPrintPrev.cbxZoomKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
//exit;
  if (key = VK_DOWN) and not (ssAlt in Shift) then
  begin
    if cbxZoom.Perform(CB_GETDROPPEDSTATE, 0, 0) = 0 then
    begin
      cbxZoom.Perform(CB_SHOWDROPDOWN, longint(1), 0);
    end;
  end
  else if (key = VK_RETURN) then
  begin
    PaperView1.SetFocus;
    key:= 0;
  end;
end;

procedure TFrmPrintPrev.PaperView1PaintPage(sender: TPaperView; aCanvas: TCanvas);
begin
  engine.PaintPage(aCanvas, fPageNum);
end;


procedure TFrmPrintPrev.cbxZoomExit(Sender: TObject);
begin
  cbxZoom.text:= IntToStr(Trunc(paperView1.Zoom * 100)) + '%';
end;

procedure TFrmPrintPrev.PaperView1ZoomChange(Sender: TObject);
begin
  cbxZoom.text:= IntToStr(Trunc(paperView1.Zoom * 100)) + '%';
end;

procedure TFrmPrintPrev.FormCreate(Sender: TObject);
var
  size  : TPoint;
begin
  engine:= TPrintEngine.Create(self);
  fPrnDlgOnPrint:= true;
  SelectedPaperSize(size);
  paperView1.PaperWidth:= size.x;
  paperView1.PaperHeight:= size.y;
//  data:= TPrintData.Create(self);
end;

procedure TFrmPrintPrev.ShowPage;
begin
  BtnGoto.Enabled:= fTotalPages > 1;
  BtnFirst.Enabled:= (fPageNum > 1) and (fTotalPages > 1);
  BtnPrev.Enabled:= BtnFirst.Enabled;
  BtnNext.Enabled:= fPageNum < fTotalPages;
  BtnLast.Enabled:= BtnNext.Enabled;
  if fTotalPages = 0 then
  begin
    barStatus.Panels[0].text:= '';
  end
  else
  begin
    barStatus.Panels[0].text:= Format('Page %d of %d', [fPageNum, fTotalPages]);
  end;
end;

procedure TFrmPrintPrev.BtnMarginsClick(Sender: TObject);
begin
  paperView1.MarginVisible:= BtnMargins.Down;
  margins1.Checked:= paperView1.MarginVisible;
end;

procedure TFrmPrintPrev.WMGetMinMaxInfo(var msg: TWMGetMinMaxInfo);
begin
  inherited;
  with msg.minMaxInfo^ do
  begin
    if visible then
    begin
      ptMinTrackSize.x:= BtnLast.Left + BtnLast.width + 15;
      ptMinTrackSize.y:= ToolBar1.Height + 200;
    end;
  end;
end;

procedure TFrmPrintPrev.ActGridLinesExecute(Sender: TObject);
begin
  ActGridLines.Checked:= not ActGridLines.Checked;
  paperView1.GridVisible:= ActGridLines.Checked;
end;

procedure TFrmPrintPrev.ActMarginsExecute(Sender: TObject);
begin
  ActMargins.Checked:= not ActMargins.Checked;
  paperView1.MarginVisible:= ActMargins.Checked;
end;

procedure TFrmPrintPrev.ActGotoExecute(Sender: TObject);
var
  frm  : TFrmPageNum;
  tstr : string;
begin
  if fTotalPages > 1 then
  begin
    frm:= TFrmPageNum.Create(self);
    try
      tstr:= Format(frm.lblCurPage.caption, [fPageNum, fTotalPages]);
      frm.lblCurPage.caption:= tstr;
      frm.spnPageNum.Min:= 1;
      frm.spnPageNum.Max:= fTotalPages;
      frm.edtPageNum.text:= IntToStr(fPageNum);
      if frm.ShowModal = mrOK then
      begin
        fPageNum:= frm.spnPageNum.position;
        paperView1.Refresh;
        ShowPage;
      end;
    finally
      frm.Free;
    end;
  end;
end;

procedure TFrmPrintPrev.ActFirstExecute(Sender: TObject);
begin
  fPageNum:= 1;
  paperView1.Refresh;
  ShowPage;
end;

procedure TFrmPrintPrev.ActPrevExecute(Sender: TObject);
begin
  if fPageNum > 1 then
  begin
    Dec(fPageNum);
    paperView1.Refresh;
    ShowPage;
  end;
end;

procedure TFrmPrintPrev.ActNextExecute(Sender: TObject);
begin
  if fPageNum < fTotalPages then
  begin
    Inc(fPageNum);
    paperView1.Refresh;
    ShowPage;
  end;
end;

procedure TFrmPrintPrev.ActLastExecute(Sender: TObject);
begin
  fPageNum:= fTotalPages;
  paperView1.Refresh;
  ShowPage;
end;

procedure TFrmPrintPrev.ActSetupExecute(Sender: TObject);
var
  size  : TPoint;
begin
  dlgPrintSetup.Execute;
  SelectedPaperSize(size);
  paperView1.PaperWidth:= size.x;
  paperView1.PaperHeight:= size.y;
  fTotalPages:= engine.CalcTotalPages(paperView1.canvas);
  ShowPage;
  ShowPaperSize;
end;

procedure TFrmPrintPrev.ActPrintExecute(Sender: TObject);
var
  size    : TPoint;
begin
(*  dlgPrint.MinPage:= 1;
  dlgPrint.MaxPage:= fTotalPages;
  dlgPrint.FromPage:= 1;
  dlgPrint.ToPage:= fTotalPages;
  if dlgPrint.Execute then
  begin
    pIdx:= SelectedPaperSize(size);
    paperView1.PaperWidth:= size.x;
    paperView1.PaperHeight:= size.y;
    fTotalPages:= engine.CalcTotalPages(paperView1.canvas);
    ShowPage;
    ShowPaperSize;
    PrintPreview(engine.PrintData, ptPrintOnly, dlgPrint.FromPage, dlgPrint.ToPage);
  end;
  *)
  if fPrnDlgOnPrint then
    PrintPreview(engine.PrintData, 2, 0, 0, nil, paperView1.canvas)
  else
    PrintPreview(engine.PrintData, 1, 0, 0, nil, paperView1.canvas);

  SelectedPaperSize(size);
  paperView1.PaperWidth:= size.x;
  paperView1.PaperHeight:= size.y;
  fTotalPages:= engine.CalcTotalPages(paperView1.canvas);
  ShowPage;
  ShowPaperSize;
end;

procedure TFrmPrintPrev.ActCloseExecute(Sender: TObject);
begin
  Close;
end;

END.

