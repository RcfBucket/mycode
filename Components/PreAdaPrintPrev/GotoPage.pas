unit GotoPage;
{$I CompileOptions.inc}

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ComCtrls, StdCtrls;

type
  TFrmPageNum = class(TForm)
    lblCurPage: TLabel;
    BtnGoto: TButton;
    Button2: TButton;
    Label2: TLabel;
    edtPageNum: TEdit;
    spnPageNum: TUpDown;
    procedure BtnGotoClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

implementation

{$R *.DFM}

procedure TFrmPageNum.BtnGotoClick(Sender: TObject);
begin
  try
    StrToInt(edtPageNum.text);
    ModalResult:= mrOK;
  except
    ShowMessage('Invalid page number entered.');
    edtPageNum.SetFocus;
  end;
end;

end.
