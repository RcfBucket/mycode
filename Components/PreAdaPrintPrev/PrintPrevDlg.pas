unit PrintPrevDlg;
{$I CompileOptions.inc}

{- This is the starting place for PrintPreview stuff. With this component you
   have access to all print/preview capabilites. The only requirement to use
   this stuff is to have a valid PrintData structure filled out. See PrnData.PAS}

INTERFACE

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  PrnData, PrnPrev;

const
  PrnFirstPage = -1; // use these two constants as parameters to PrintPreview
  PrnLastPage  = -1; // function. If specified, printpreview will start at
                     // page one and end on the last page.
type
  TPrintTypes = (ptPreview, ptPrintOnly, ptPrintDlg);

  TOnNeedCanvas = procedure(owner: TObject; var aCanvas: TCanvas) of object;

  TPrintPrevDlg = class(TComponent)
  private
    fPrevTitle  : string;
    fFirstPage  : integer;
    fLastPage   : integer;
    fPrintType  : TPrintTypes;
    fFlat       : boolean;
    fWindowStyle: TWindowState;
    fOnNeedCanvas: TOnNeedCanvas;
    fPrnDlgOnPrevPrint: boolean;
    fShowhints        : boolean;
    fOnPrinted  : TNotifyEvent;
  protected
    { Protected declarations }
  public
    constructor Create(aOwner: TComponent); override;
    function Execute(data: TPrintData): integer;
  published
    property PreviewTitle: string read fPrevTitle write fPrevTitle;
    property StartPage: integer read fFirstPage write fFirstPage;
    property EndPage: integer read fLastPage write fLastPage;
    property PrintType: TPrintTypes read fPrintType write fPrintType default ptPreview;
    property FlatButtons: boolean read fFlat write fFlat default true;
    property WindowStyle: TWindowState read fWindowStyle write fWindowStyle default wsMaximized;
    // This prop controls whether or not a print selection dialog will be shown
    // when printing from the preview screen.
    property PrintDlgOnPreview: boolean read fPrnDlgOnPrevPrint write fPrnDlgOnPrevPrint;
    property ShowHints: boolean read fShowHints write fShowHints;
    property OnNeedCanvas: TOnNeedCanvas read fOnNeedCanvas write fOnNeedCanvas;
    property OnPrinted: TNotifyEvent read fOnPrinted write fOnPrinted;
  end;

procedure Register;

IMPLEMENTATION

constructor TPrintPrevDlg.Create(aOwner: TComponent);
begin
  inherited Create(aOwner);
  fFlat:= true;
  fFirstPage:= prnFirstPage;
  fLastPage:= prnLastPage;
  fPrintType:= ptPreview;
  PreviewTitle:= '';
  fWindowStyle:= wsMaximized;
  fPrnDlgOnPrevPrint:= true;
  fShowhints:= true;
end;

function TPrintPrevDlg.Execute(data: TPrintData): integer;
var
  opts  : TPrevFormOpts;
  aCanv : TCanvas;
begin
  screen.Cursor:= crHourGlass;
  opts:= TPrevFormOpts.Create;
  try
    opts.windowStyle:= fWindowStyle;
    opts.flatButtons:= fFlat;
    opts.PreviewWinTitle:= fPrevTitle;
    opts.PrnDlgOnPrint:= fPrnDlgOnPrevPrint;
    opts.ShowHints:= fShowhints;
    if fPrintType = ptPrintDlg then
    begin
      if Assigned(fOnNeedCanvas) then
        fOnNeedCanvas(self, aCanv)
      else
        aCanv:= application.mainForm.Canvas;
    end;
    Result:= PrintPreview(data, Ord(fPrintType), FirstPage, LastPage, opts, aCanv);
    if (result = mrOK) and Assigned(fOnPrinted) then
      fOnPrinted(self);
  finally
    screen.Cursor:= crDefault;
    opts.Free;
  end;
end;

procedure Register;
begin
  RegisterComponents('DMS', [TPrintPrevDlg]);
end;

END.

