unit PrnData;
{$I CompileOptions.inc}

INTERFACE
{- Measurements are in LTwips. There are 1440 LTwips per inch
   There are 20 LTwips per point. }

uses
  Windows, SysUtils, Classes, Graphics, PrnUtils, TwipLib, Printers;

const
  DefaultMargin  = LT50;  {- default .5 inch margin }
  DefaultPoints  = 10;    // Default points value
  intMarg        = 1;     {- interior margin for cells (in PTs) }
  fontExtra      = 2;     {- amount to add to point size to correct
                             for proper cell size (in points) }

  {- CELL FLAGS }
  c_Normal       = $00000000;  {- normal font }
  c_Italic       = $00000001;  {- italic }
  c_Bold         = $00000002;  {- bold }
  c_Underline    = $00000004;  {- underline }
  c_Strikeout    = $00000008;  {- strike out }

  {- text alignment flags (within cell) }
  c_Left         = $00000010;  {- left align (default) }
  c_Top          = $00000020;  {- top align }
  c_Right        = $00000040;  {- right align }
  c_Bottom       = $00000080;  {- bottom align (default) }
  c_Center       = $00000100;  {- horizontal center }
  c_VCenter      = $00000200;  {- vertical center }
  c_Baseline     = $00000400;  {- align text to font baseline of entire row }

  {- cell alignment flags (alignment on page) }
  c_PageLeft     = $00000800;  {- align cell to left margin }
  c_PageRight    = $00001000;  {- align cell to right margin }
  c_PageCenter   = $00002000;  {- align cell to center of page }
  c_Stretch      = $00004000;  {- stretch cell to right margin }

  {- misc cell format flags }
  c_BorderLeft   = $00008000;
  c_BorderTop    = $00010000;
  c_BorderRight  = $00020000;
  c_BorderBottom = $00040000;
  c_BorderAll    = c_BorderLeft or c_BorderTop or
                   c_BorderRight or c_BorderBottom;  {- outline the cell }
  c_WordWrap     = $00080000;  {- wrap text in cell }

  {- print-time calculated variables }
  {- the following flags rely on the fact that each flag has a corresponding
     format character in the cell text. The format characters are shown below
     as ^x where x is defined below.
     For example:  Cell string = 'Page ^p of ^n'
                   Cell flag   = c_PageNum or c_NumPages }
  c_PageNum      = $00100000;  {- ^p  inserts the page number into the cell }
  c_NumPages     = $00200000;  {- ^n  inserts the total pages into the cell }
  c_LineNum      = $00400000;  {- ^l  inserts the line number into the cell }
  c_LineOnPage   = $00800000;  {- ^c  inserts the cells relative line number on page}
  c_Date         = $01000000;  {- ^d  inserts the date for report (uses intl short fmt)}
  c_Time         = $02000000;  {- ^t  inserts the time for report (uses intl time fmt)}

//  c_BitMap       = $04000000;  {- cell contains a bitmap }


  {- ROW FLAGS }
  r_Normal       = 0;          {- row border flags ----- }
  r_BorderTop    = $00000001;
  r_BorderBottom = $00000002;
  r_BorderLeft   = $00000004;
  r_BorderRight  = $00000008;

  r_BorderAll    = $0000000F;

  r_PageBreak    = $00000010;  {- force page break *before* line. page breaks within a
                                  group are not allowed }
  r_CenterRow    = $00000020;  {- center entire line on page }
                               {- the following flags are ignored for the entire row
                                  when this option is ON.
                                  c_PageLeft, c_PageRight, c_PageCenter, c_Stretch }
  r_StartGroup   = $00000040;  {- start a logical group of rows }
  r_EndGroup     = $00000080;  {- end a logical group of rows }
  r_CellWrap     = $00000100;  {- wrap cells that exceed right border }
                               {- all wrapped cells are same height as largest cell}
                               {- cell page alignment/stretching  should not be used when wrapping
                                  cells on a page }
  r_SysFlags     = $F0000000;  {- reserved by system for special formatting }


const
  AvgTextFluff   = 1.33;       {- amount of fluff to add to text when calculating
                                  average widths }

type
  TPrnPrevOrientation = (ppDontCare, ppPortrait, ppLandscape);

  // TCell contains information about each cell that is to be printed
  TCell = class
    flags   : longword; {- print flags (see c_xxx constants or r_xxx constants) }
    width   : integer;  {- width of cell (in LTwips). if cell specifies a row
                           description record, then width specifies a wrap margin}
    ptSize  : integer;  {- point size for string }
    rowDesc : boolean;  {- true if object defines a row. false if objet describes
                           a cell in a row }
    text    : string;   {- cell text }
    maxHeight: integer; {- used for row records only}
//    bmp     : TBitMap;
    constructor Create;
    destructor Destroy; override;
  end;

  TCellList = class(TPersistent)
  private
    data        : TList;
    fDefPtSize  : integer;
    function GetTotalRecords: integer;
    function GetCellStr(idx: integer): string;
    function GetCell(idx: integer): TCell;
  public
    constructor Create(aDefPtSize: integer);
    destructor Destroy; override;
    procedure AddRow(aflags: integer = c_Normal; wrapMarg: integer = 0; aMaxHeight: integer = 0);
    function AddCell(aStr: string; aWidth: integer; flags: integer = c_Normal;
                     aPtSize: word = 0; aPtOffset: integer = 0): integer;
    procedure AddEndGroupToLastRow;
    procedure AdjustRowMaxHeight(aMaxHeight: integer);
//    procedure AddBitmap(aBmp: TBitMap; flags: integer);
    procedure Clear;
    procedure ClearFlags(flagsToClear: longword);
    function IsBOL(aRecNum: integer): boolean;
    function MaxLinePtSize(aRecNum: integer): integer;
    function FirstRecInLine(aRecNum: integer): integer;
    function LastRecInLine(aRecNum: integer): integer;
    function TotalRowWidth(aRecNum: integer): integer;

    property DefaultPointSize: integer read fDefPtSize write fDefPtSize;
    property TotalRecords: integer read GetTotalRecords;
    property Cell[idx: integer]: TCell read GetCell;
    property CellStr[idx: integer]: string read GetCellStr;
  end;

  {- main print information object }
//  TPrintData = class(TComponent)
  TPrintData = class
  private
    fDefPtSize  : integer;    // default point size for component
    fFontName   : TFontName;  // default font for report
    fRowSpacing : TLTwips;    // spacing between rows (in points)
    fMargLeft   : integer;    // left margin (in LTwips)
    fMargTop    : integer;    //  top margin (in LTwips)
    fMargBottom : integer;    // bottom margin (in LTwips)
    fMargRight  : integer;    // right margin (in LTwips)
    fDocName    : string;     // this is the name of the doc being printed
    fDefOrient  : TPrnPrevOrientation;
    function GetMetrics(aPtSize: integer): TTextMetric;
    procedure SetDefPtSize(value: integer);
  public
    header  : TCellList;
    footer  : TCellList;
    body    : TCellList;
    constructor Create;
    destructor Destroy; override;
    function GetMaxCharWidth(numChars: integer; ptSize: integer): integer;
    function GetAvgCharWidth(numChars: integer; ptSize: integer): integer;
    procedure SetRowSpacing(value: TLTwips);
    procedure Clear;
  published
    property FontName: TFontName read fFontName write fFontName;
    property RowSpacing: TLTwips read fRowSpacing write SetRowSpacing;
    property DefaultPointSize: integer read fDefPtSize write SetDefPtSize default DefaultPoints;
    property MarginLeft: integer read fMargLeft write fMargLeft default DefaultMargin;
    property MarginRight: integer read fMargRight write fMargRight default DefaultMargin;
    property MarginTop: integer read fMargTop write fMargTop default DefaultMargin;
    property MarginBottom: integer read fMargBottom write fMargBottom default DefaultMargin;
    property DocumentName: string read fDocName write fDocName;
    property DefaultOrientation: TPrnPrevOrientation read fDefOrient write fDefOrient default ppDontCare;
  end;

(*procedure Register;*)

implementation

{--[ TCellRec ]---------------------------------------------------------------}

constructor TCell.Create;
begin
  //inherited Create;
  flags:= 0;
  width:= 0;
  width:= 0;
  text:= '';
  ptSize:= 0;
  rowDesc:= false;
(*  bmp:= nil;*)
end;

destructor TCell.Destroy;
begin
(*  if (bmp <> nil) then
    bmp.Free;*)
  inherited Destroy;
end;

{--[ TPrintData ]----------------------------------------------------------}

constructor TCellList.Create(aDefPtSize: integer);
begin
//  inherited Create;
  data:= TList.Create;
  fDefPtSize:= aDefPtSize;
end;

destructor TCellList.Destroy;
begin
  Clear;
  data.Free;
  inherited Destroy;
end;

function TCellList.GetCellStr(idx: integer): string;
begin
  result:= Cell[idx].text;
end;

function TCellList.GetCell(idx: integer): TCell;
begin
  result:= data[idx];
end;

procedure TCellList.AddRow(aflags: integer = c_Normal; wrapMarg: integer = 0; aMaxHeight: integer = 0);
// wrap marg is the margin that will be used for any wrapped cells in
// the row. The first row is not affected by this number, only subsequent
// wrapped cells.
// aMaxHeight specifies the maximum height of the row in pts.  If zero then
// the height will be calculated from the contents of the cells in the row.
var
  c   : TCell;
begin
  c:= TCell.Create;
  c.RowDesc:= true;

  if data.count = 0 then
    aflags:= aflags and not r_PageBreak;  // remove page break if first row

  aFlags:= aFlags and not r_SysFlags;  // dont allow user to specify system flags

  // cannot specify start and end group for one row
  if (aFlags and r_StartGroup and r_EndGroup) <> 0 then
    aFlags:= aFlags and not (r_StartGroup or r_EndGroup);

  if (aFlags and r_CellWrap <> 0) and (wrapMarg > 0) then
    c.width:= wrapMarg;

  c.flags:= aFlags;
  c.maxHeight:= aMaxHeight;
  data.Add(c);
end;

(*procedure TCellList.AddBitmap(aBmp: TBitMap; flags: integer);
var
  c   : TCell;
begin
  c:= TCell.Create;
  c.rowDesc:= false;
  c.flags:= flags or c_BitMap and not c_WordWrap;
  c.bmp:= TBitmap.Create;
  c.bmp.Assign(aBmp);
  c.Width:= aBmp.Width;
end;*)

function TCellList.AddCell(aStr: string; aWidth: integer; flags: integer = c_Normal;
                            aPtSize: word = 0; aPtOffset: integer = 0): integer;
{- add a string to the current row.
   aWidth    - specifies the "cell" width for the string being added.
   flags     - any of the CELL FLAGS (c_xxx constants)
   aPtSize   - if 0 then the default point size will be used.
   aPtOffset - This specifies an offset to the specified point size. If 0 then
               the point size for the cell will be aPtSize, otherwise the
               point size for the cell will be (aPtSize + aPtOffset)
   returns -1 if no row description record has been added yet.
           -2 if a memory error occured.
            0 if everything is OK }
var
  c  : TCell;
  r  : TCell;
begin
  c:= nil;
  result:= 0;
  if data.count = 0 then
    result:= -1;

  if result = 0 then
  begin
    c:= TCell.Create;
    c.rowDesc:= false;
    c.text:= aStr;

    if aPtSize = 0 then
      aPtSize:= DefaultPointSize;

    c.ptSize:= aPtSize + aPtOffset;

    c.width:= aWidth;

    {- set default alignment for text (top/left) }
    if flags and (c_Top or c_Bottom or c_VCenter or c_Baseline) = 0 then
      flags:= flags or c_Top;

    if (flags and (c_Left or c_Right or c_Center)) = 0 then
      flags:= flags or c_Left;

    c.flags:= flags;

    try
      data.Add(c);
    except
      result:= -2;
    end;
  end;

  if result = 0 then
  begin
    {- find row description record }
    r:= data[FirstRecInLine(data.count - 1)];
    if r.flags and r_CenterRow <> 0 then
    begin
      {- if row center is specified, then remove invalid flags from the cell that
        was just added.}
      c.flags:= c.flags and
                not (c_PageCenter or c_PageLeft or c_PageRight or c_Stretch);
    end;
  end;
end;

procedure TCellList.AddEndGroupToLastRow;
{- adds an EndGroup flag the the last row added }
var
  c   : TCell;
  k   : integer;
begin
  k:= data.count;
  if k > 0 then
  begin
    repeat
      c:= data[k - 1];
      if c.rowDesc then
      begin
        c.flags:= (c.flags or r_EndGroup) and not r_StartGroup;
        k:= 0;
      end;
      Dec(k);
    until k <= 0;
  end;
end;

procedure TCellList.AdjustRowMaxHeight(aMaxHeight: integer);
{- adjusts the MaxHeight of a row }
var
  c   : TCell;
  k   : integer;
begin
  k := data.Count - 1;
  c := Cell[k];
  while not c.rowDesc do
    begin
    k := k - 1;
    if k < 0 then exit;
    c := Cell[k];
    end;
  c.maxHeight := aMaxHeight;
end;

function TCellList.FirstRecInLine(aRecNum: integer): integer;
{- return the first cell record for the line containing aRecNum.
   this rec will be the row Description record for the line
   return -1 if aRecNum is invalid }
var
  k  : integer;
  c  : TCell;
begin
  if (aRecNum >= 0) and (aRecNum < data.count) then
  begin
    k:= aRecNum;
    if k >= 0 then
    begin
      repeat
        c:= data[k];
        if not c.rowDesc then
          Dec(k);
      until (k < 0) or c.rowDesc;
      result:= k;
    end
    else
      result:= -1;
  end
  else
    result:= -1;
end;

function TCellList.LastRecInLine(aRecNum: integer): integer;
{- return the last cell record for the line containing aRecNum.
   return -1 if aRecNum is invalid }
var
  k  : longint;
  c  : TCell;
begin
  if (aRecNum >= 0) and (aRecNum < data.count) then
  begin
    k:= aRecNum;
    if k < (data.count - 1) then
    begin
      Inc(k);
      repeat
        c:= data[k];
        if not c.rowDesc then
          Inc(k);
      until (k >= data.count) or c.rowDesc;
      result:= k - 1;
    end
    else
      result:= k;  {- aRecNum was last rec in list }
  end
  else
    result:= -1;
end;

function TCellList.GetTotalRecords: integer;
{- return the total number of cells in the list of cells. }
begin
  result:= data.count;
end;

function TCellList.TotalRowWidth(aRecNum: integer): integer;
{- calculate the total user specified width of a row. Does not take into account
   cells that are to be stretched at print time }
var
  k  : longint;
  c  : TCell;
begin
  result:= 0;
  if (aRecNum >= 0) and (aRecNum < data.count) then
  begin
    for k:= FirstRecInLine(aRecNum) + 1 to LastRecInLine(aRecNum) do
    begin
      c:= data[k];
      Inc(result, c.width + PointsToLTwips(intMarg));
    end;
  end;
end;

function TCellList.MaxLinePtSize(aRecNum: integer): integer;
{- returns the maximum height (in points) for a line.
   aRecNum : This parameter indicates a cell in the cell list.
   returns : -1    if an invalid record number is passed,
              0    for default line height to be used
              > 0  the maximum point size for all cells in the line }
var
  max : integer;
  k   : longint;
  c   : TCell;
begin
  result:= -1;
  if (aRecNum >= 0) and (aRecNum < data.count) then
  begin
    k:= FirstRecInLine(aRecNum);
    if k >= 0 then
    begin
      max:= 0;
      if k < data.count - 1 then // if not last record
      begin
        Inc(k);
        {- now k is pointing the first text cell record for the line
           containing aRecNum }
        c:= data[k];
        if not c.rowDesc then
        begin
          repeat
            if c.ptSize > max then
              max:= c.ptSize;
            Inc(k);
            if k < data.count then
              c:= data[k];
          until (k >= data.count) or c.rowDesc;
        end;
      end;
      if max = 0 then
        max:= DefaultPointSize;
      result:= max;
    end;
  end;
end;

function TCellList.IsBOL(aRecNum: integer): boolean;
{- return true if specified record is the first cell of row (BOL) }
begin
  if (aRecNum >= 0) and (aRecNum < data.count) then
    IsBOL:= TCell(data[aRecNum]).rowDesc
  else
    IsBOL:= false;
end;

procedure TCellList.Clear;
var
  k     : integer;
  cell  : TCell;
begin
  for k:= data.count - 1 downto 0 do
  begin
    cell:= data[k];
    cell.Free;
  end;
  data.Clear;
end;

procedure TCellList.ClearFlags(flagsToClear: longword);
// clear specified flags from data list
var
  k   : integer;
begin
  for k:= 0 to data.count - 1 do
  begin
    cell[k].flags:= cell[k].flags and not flagsToClear;
  end;
end;

{--[ TPrintData ]-------------------------------------------------------}

constructor TPrintData.Create;
begin
  fDefOrient:= ppDontCare;
  fDefPtSize:= DefaultPoints;
  body:= TCellList.Create(fDefPtSize);
  footer:= TCellList.Create(fDefPtSize);
  header:= TCellList.Create(fDefPtSize);
  fFontName:= 'Arial';

  fRowSpacing:= 0;
  fMargLeft:= DefaultMargin;
  fMargTop:= DefaultMargin;
  fMargRight:= DefaultMargin;
  fMargBottom:= DefaultMargin;
end;

destructor TPrintData.Destroy;
begin
  body.Free;
  footer.Free;
  header.Free;
  inherited Destroy;
end;

procedure TPrintData.Clear;
begin
  header.Clear;
  body.Clear;
  footer.Clear;
end;

procedure TPrintData.SetRowSpacing(value: TLTwips);
// set row spacing (in LTwips)
begin
  if (value >= 0) then
    fRowSpacing:= value;
end;

procedure TPrintData.SetDefPtSize(value: integer);
begin
  fDefPtSize:= value;
  header.DefaultPointSize:= value;
  footer.DefaultPointSize:= value;
  body.DefaultPointSize:= value;
end;

function TPrintData.GetMetrics(aPtSize: integer): TTextMetric;
// return a text metrics record for the current font (at 100% zoom)
var
  dc    : HDC;
  tm    : TTextMetric;
  font  : TFont;
begin
  dc:= GetDC(0);    // get a screen DC
  SetLogicalTwips(dc, 1.0); {- set to zoom 1.0 for consistent measurements }

  if aPtSize = 0 then
    aPtSize:= fDefPtSize;

  font:= TFont.Create;
  font.Name:= fFontName;
  font.Height:= -PointsToLTwips(aPtSize);

  if font.Handle = 0 then
    SelectObject(dc, GetStockObject(SYSTEM_FONT))
  else
    SelectObject(dc, font.Handle);

  GetTextMetrics(dc, tm);

  font.Free;
  ReleaseDC(0, dc);

  result:= tm;
end;

function TPrintData.GetAvgCharWidth(numChars: integer; ptSize: integer): integer;
{- This function returns an appoximate width for a specified number of characters
   and a specific point size. This width can be used for cell widths when adding
   cells to a report }
var
  tm    : TTextMetric;
begin
  tm:= GetMetrics(ptSize);
  result:= integer(Round(AvgTextFluff * (numChars * tm.tmAveCharWidth)));
end;

function TPrintData.GetMaxCharWidth(numChars: integer; ptSize: integer): integer;
{- This function returns a maximum width for a specified number of characters
   and a specific point size. This width can be used for cell widths when adding
   cells to a report }
var
  tm    : TTextMetric;
begin
  tm:= GetMetrics(ptSize);
  result:= (numChars * tm.tmMaxCharWidth);
end;

(*procedure Register;
begin
  RegisterComponents('DMS', [TPrintData]);
end;
*)
END.

