unit prnDlg;
{$I CompileOptions.inc}

INTERFACE

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ComCtrls, StdCtrls, Printers;

type
  TFrmPrinting = class(TForm)
    Label1: TLabel;
    lblDocName: TLabel;
    BtnCancel: TButton;
    Label2: TLabel;
    lblCurPrinter: TLabel;
    barStatus: TProgressBar;
    lblPage: TLabel;
    procedure FormShow(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtnCancelClick(Sender: TObject);
  private
    { Private declarations }
  public
    DoAbortPrint : boolean;
    PrintAborted  : boolean;
    procedure SetPages(cur, max: integer);
  end;

IMPLEMENTATION

uses PrnUtils;

{$R *.DFM}

procedure TFrmPrinting.FormShow(Sender: TObject);
begin
  lblCurPrinter.caption:= GetPrintDeviceName;
end;

procedure TFrmPrinting.SetPages(cur, max: integer);
begin
  barStatus.max:= max;
  barStatus.min:= 1;
  barStatus.position:= cur;
  lblPage.caption:= Format('Page %d of %d', [cur, max]);
  lblPage.Refresh;
  barStatus.Refresh;
  if (doAbortPrint) and (cur = 3) then
    BtnCancel.Click;
end;

procedure TFrmPrinting.FormCreate(Sender: TObject);
begin
  lblPage.caption:= '';
  barStatus.position:= 0;
  barStatus.min:= 0;
  doAbortPrint:= false;
  printAborted:= false;
end;

procedure TFrmPrinting.BtnCancelClick(Sender: TObject);
begin
  if not printAborted then
  begin
    if MessageDlg('Cancel Printing?',mtConfirmation,[mbYes,mbNo],0) = mrYes then
    begin
      printAborted:= true;
    end;
  end;
end;

{- rcf}
END.


