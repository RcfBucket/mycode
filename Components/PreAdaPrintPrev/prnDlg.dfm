object FrmPrinting: TFrmPrinting
  Left = 554
  Top = 110
  BorderStyle = bsDialog
  Caption = 'Printing'
  ClientHeight = 160
  ClientWidth = 294
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = True
  Position = poScreenCenter
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 16
    Top = 16
    Width = 49
    Height = 13
    Caption = 'Document'
  end
  object lblDocName: TLabel
    Left = 96
    Top = 16
    Width = 177
    Height = 26
    AutoSize = False
    Caption = 'lblDocName'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label2: TLabel
    Left = 16
    Top = 48
    Width = 30
    Height = 13
    Caption = 'Printer'
  end
  object lblCurPrinter: TLabel
    Left = 96
    Top = 48
    Width = 56
    Height = 13
    Caption = 'lblCurPrinter'
  end
  object lblPage: TLabel
    Left = 16
    Top = 72
    Width = 35
    Height = 13
    Caption = 'lblPage'
  end
  object BtnCancel: TButton
    Left = 108
    Top = 120
    Width = 75
    Height = 25
    Caption = 'Cancel'
    Default = True
    TabOrder = 0
    OnClick = BtnCancelClick
  end
  object barStatus: TProgressBar
    Left = 18
    Top = 88
    Width = 257
    Height = 16
    Min = 0
    Max = 100
    TabOrder = 1
  end
end
