unit PaperSize;
{$I CompileOptions.inc}

INTERFACE

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Grids, StdCtrls, ExtCtrls, TwipLib;

type
  TFrmPaperSize = class(TForm)
    grdPapers: TStringGrid;
    grpUnits: TRadioGroup;
    BtnOK: TButton;
    BtnCancel: TButton;
    ChkCustSize: TCheckBox;
    edWidth: TEdit;
    edHeight: TEdit;
    Label1: TLabel;
    Label2: TLabel;
    procedure FormShow(Sender: TObject);
    procedure grpUnitsClick(Sender: TObject);
    procedure BtnOKClick(Sender: TObject);
    procedure grdPapersDblClick(Sender: TObject);
    procedure ChkCustSizeClick(Sender: TObject);
  private
    function ConvertUnits(lt: integer; var fmt: string): double;
    procedure UpdatePaperSizes;
  public
    fPaperIdx   : integer;
  end;

IMPLEMENTATION

uses
  PrnUtils;

{$R *.DFM}

procedure TFrmPaperSize.FormShow(Sender: TObject);
var
  k     : integer;
begin
  with grdPapers do
  begin
    cells[0, 0]:= 'Paper';
    cells[1, 0]:= 'Width';
    cells[2, 0]:= 'Height';
    RowCount:= Last_DMPaper - First_DMPaper + 2;
    for k:= First_DMPaper to Last_DMPaper do
    begin
      row:= k - First_DMPaper + 1;
      grdPapers.cells[0, row]:= PaperName(k);
    end;
    row:= 1;
  end;
  UpdatePaperSizes;
  grdPapers.Row:= 1 + (fPaperIdx - First_DMPaper);
  ChkCustSizeClick(self);
end;

function TFrmPaperSize.ConvertUnits(lt: integer; var fmt: string): double;
begin
  fmt:= '#,##0.00';
  case grpUnits.ItemIndex of
    0 : // inches
      begin
        result:= LTwipsToUnits(lt, uInches);
      end;
    1 :
      begin
        result:= LTwipsToUnits(lt, uMM);
      end;
    2 :
      begin
        result:= LTwipsToUnits(lt, uCM);
      end;
    else
      begin
        result:= lt;
        fmt:= '#,##0';
      end;
  end;
end;

procedure TFrmPaperSize.UpdatePaperSizes;
var
  fmt   : string;
  lt    : integer;
  num   : double;
  row   : integer;
  k     : integer;
begin
  for k:= First_DMPaper to Last_DMPaper do
  begin
    row:= k - First_DMPaper + 1;
    lt:= paperSizes[k].x;
    num:= ConvertUnits(lt, fmt);
    grdPapers.cells[1, row]:= FormatFloat(fmt, num);
    lt:= paperSizes[k].y;
    num:= ConvertUnits(lt, fmt);
    grdPapers.cells[2, row]:= FormatFloat(fmt, num);
  end;
end;


procedure TFrmPaperSize.grpUnitsClick(Sender: TObject);
begin
  UpdatePaperSizes;
end;

(*procedure TFrmPaperSize.SetPaperIndex(value: integer);
begin
  fPaperIdx:= value;
  if Showing then
    grdPapers.row:= 1 + (value - First_DMPaper);
end; *)

procedure TFrmPaperSize.BtnOKClick(Sender: TObject);
begin
  fPaperIdx:= First_DMPaper + (grdPapers.row - 1);
end;

procedure TFrmPaperSize.grdPapersDblClick(Sender: TObject);
begin
  BtnOK.Click;
end;

procedure TFrmPaperSize.ChkCustSizeClick(Sender: TObject);
begin
  edWidth.Enabled:= ChkCustSize.Checked;
  edHeight.Enabled:= ChkCustSize.Checked;
  label1.Enabled:= ChkCustSize.Checked;
  label2.Enabled:= ChkCustSize.Checked;
end;

END.

