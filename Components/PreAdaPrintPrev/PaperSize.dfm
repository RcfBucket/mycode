object FrmPaperSize: TFrmPaperSize
  Left = 330
  Top = 119
  BorderIcons = [biSystemMenu]
  BorderStyle = bsDialog
  Caption = 'Paper Size'
  ClientHeight = 274
  ClientWidth = 482
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 360
    Top = 146
    Width = 28
    Height = 15
    Caption = 'Width'
  end
  object Label2: TLabel
    Left = 360
    Top = 171
    Width = 31
    Height = 13
    Caption = 'Height'
  end
  object grdPapers: TStringGrid
    Left = 8
    Top = 8
    Width = 337
    Height = 257
    ColCount = 3
    DefaultRowHeight = 16
    FixedCols = 0
    Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goColSizing, goRowSelect]
    TabOrder = 0
    OnDblClick = grdPapersDblClick
    ColWidths = (
      203
      64
      64)
  end
  object grpUnits: TRadioGroup
    Left = 360
    Top = 8
    Width = 113
    Height = 89
    Caption = 'Units'
    ItemIndex = 0
    Items.Strings = (
      'Inches'
      'Millimeters'
      'Centimeters'
      'Logical Twips')
    TabOrder = 1
    OnClick = grpUnitsClick
  end
  object BtnOK: TButton
    Left = 384
    Top = 208
    Width = 91
    Height = 25
    Caption = '&OK'
    Default = True
    ModalResult = 1
    TabOrder = 5
    OnClick = BtnOKClick
  end
  object BtnCancel: TButton
    Left = 384
    Top = 240
    Width = 91
    Height = 25
    Cancel = True
    Caption = 'Cancel'
    ModalResult = 2
    TabOrder = 6
  end
  object ChkCustSize: TCheckBox
    Left = 360
    Top = 120
    Width = 89
    Height = 17
    Caption = 'Custom Size'
    TabOrder = 2
    OnClick = ChkCustSizeClick
  end
  object edWidth: TEdit
    Left = 400
    Top = 142
    Width = 73
    Height = 21
    TabOrder = 3
  end
  object edHeight: TEdit
    Left = 400
    Top = 166
    Width = 73
    Height = 21
    TabOrder = 4
  end
end
