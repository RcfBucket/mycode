unit LTwipEd;
{$I CompileOptions.inc}

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, TwipLib;

type
  TFrmLTwips = class(TForm)
    Label1: TLabel;
    Label2: TLabel;
    Edit1: TEdit;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Edit2: TEdit;
    Edit3: TEdit;
    Edit4: TEdit;
    Label6: TLabel;
    lblResult: TLabel;
    BtnOK: TButton;
    BtnCancel: TButton;
    procedure FormShow(Sender: TObject);
    procedure EditChange(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
  private
    fUpdating   : boolean;
    procedure UpdateVals(allBut: TEdit);
  public
    { Public declarations }
  end;

var
  FrmLTwips: TFrmLTwips;

implementation

{$R *.DFM}

procedure TFrmLTwips.FormShow(Sender: TObject);
begin
  UpdateVals(nil);
end;

procedure TFrmLTwips.UpdateVals(allBut: TEdit);
var
  lt    : integer;
begin
  fUpdating:= true;
  try
    lt:= StrToInt(lblResult.caption);
    if allBut <> edit1 then
      edit1.text:= lblResult.caption;
    if allBut <> edit2 then
      edit2.text:= FormatFloat('###0.000', LTwipsToUnits(lt, uInches));
    if allBut <> edit3 then
      edit3.text:= FormatFloat('###0.000', LTwipsToUnits(lt, uMM));
    if allBut <> edit4 then
      edit4.text:= FormatFloat('###0.000', LTwipsToUnits(lt, uCM));
  finally
    fUpdating:= false;
  end;
end;

procedure TFrmLTwips.EditChange(Sender: TObject);
var
  num   : double;
  lt    : integer;
  units : TUnitTypes;
  ed    : TEdit;
begin
  if fUpdating then exit;

  ed:= sender as TEdit;
  if sender = edit1 then
    lblResult.caption:= ed.text
  else
  begin
    num:= StrToFloat(ed.text);
    units:= uInches;
    if sender = edit2 then
      units:= uInches
    else if sender = edit3 then
      units:= uMM
    else if sender = edit4 then
      units:= uCM;
    lt:= UnitsToLTwips(num, units);
    lblResult.caption:= IntToStr(lt);
  end;

  UpdateVals(ed);
end;

procedure TFrmLTwips.FormCreate(Sender: TObject);
begin
  fUpdating:= false;
end;

procedure TFrmLTwips.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin
  canClose:= true;
  if ModalResult = mrOK then
  begin
    try
      StrToInt(lblResult.caption);
    except
      CanClose:= false;
      raise;
    end;
  end;
end;

end.
