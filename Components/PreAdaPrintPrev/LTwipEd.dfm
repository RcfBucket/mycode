object FrmLTwips: TFrmLTwips
  Left = 339
  Top = 121
  BorderIcons = [biSystemMenu]
  BorderStyle = bsDialog
  Caption = 'Logical Twips'
  ClientHeight = 189
  ClientWidth = 327
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  OnCloseQuery = FormCloseQuery
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 16
    Top = 168
    Width = 196
    Height = 13
    Caption = 'There are 1440 logical twips (LT) per inch'
  end
  object Label2: TLabel
    Left = 16
    Top = 20
    Width = 65
    Height = 13
    Caption = 'Logical Twips'
  end
  object Label3: TLabel
    Left = 16
    Top = 44
    Width = 32
    Height = 13
    Caption = 'Inches'
  end
  object Label4: TLabel
    Left = 16
    Top = 68
    Width = 48
    Height = 13
    Caption = 'Millimeters'
  end
  object Label5: TLabel
    Left = 16
    Top = 92
    Width = 55
    Height = 13
    Caption = 'Centimeters'
  end
  object Label6: TLabel
    Left = 16
    Top = 136
    Width = 30
    Height = 13
    Caption = 'Result'
  end
  object lblResult: TLabel
    Left = 96
    Top = 136
    Width = 8
    Height = 13
    Caption = '0'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Edit1: TEdit
    Left = 96
    Top = 16
    Width = 121
    Height = 21
    TabOrder = 0
    Text = 'Edit1'
    OnChange = EditChange
  end
  object Edit2: TEdit
    Left = 96
    Top = 40
    Width = 121
    Height = 21
    TabOrder = 1
    Text = 'Edit2'
    OnChange = EditChange
  end
  object Edit3: TEdit
    Left = 96
    Top = 64
    Width = 121
    Height = 21
    TabOrder = 2
    Text = 'Edit3'
    OnChange = EditChange
  end
  object Edit4: TEdit
    Left = 96
    Top = 88
    Width = 121
    Height = 21
    TabOrder = 3
    Text = 'Edit4'
    OnChange = EditChange
  end
  object BtnOK: TButton
    Left = 240
    Top = 16
    Width = 75
    Height = 25
    Caption = '&OK'
    Default = True
    ModalResult = 1
    TabOrder = 4
  end
  object BtnCancel: TButton
    Left = 240
    Top = 48
    Width = 75
    Height = 25
    Cancel = True
    Caption = 'Cancel'
    ModalResult = 2
    TabOrder = 5
  end
end
