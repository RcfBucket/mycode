unit Pzdtview;

interface

uses
  SysUtils, WinTypes, WinProcs, Messages, Classes, Graphics, Controls,
  Forms, Dialogs, ExtCtrls, PZBPanel, PZShaded, StdCtrls, PZLabel, PZSpButt,
  PZLView, PZTView, PZImList, Buttons;

type
  TPZTreeViewForm = class(TForm)
    HeaderPanel: TPZBitmapPanel;
    PZShaded1: TPZShaded;
    DescrPanel: TPZBitmapPanel;
    PZLabel4: TPZLabel;
    ResultsPanel: TPZBitmapPanel;
    PZLabel2: TPZLabel;
    PZLabel1: TPZLabel;
    OutputPanel1: TPZBitmapPanel;
    OutputPanel2: TPZBitmapPanel;
    BackgroundCheck: TCheckBox;
    Label1: TLabel;
    PZLabel5: TPZLabel;
    TreeView: TPZTreeView;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormCreate(Sender: TObject);
    procedure FormDeactivate(Sender: TObject);
    procedure Label1Click(Sender: TObject);
    procedure BackgroundCheckClick(Sender: TObject);
  private
    { Private declarations }
  Furniture,Bedroom,Lounge,Dining :TPZTreeViewItem;
    procedure FillTree;
  public
    { Public declarations }
  end;

var
  PZTreeViewForm: TPZTreeViewForm;

implementation

Uses PZDMain;

{$R *.DFM}

procedure TPZTreeViewForm.FormCreate(Sender: TObject);

Begin
  SetBounds(10,10,MainForm.ClientWidth-20,MainForm.ClientHeight-MainForm.MainPanel.Height-20);
  HeaderPanel.Background.LoadBitmap('PZD_STONE');
  DescrPanel.Background.LoadBitmap('PZD_STONE');
  ResultsPanel.Background.LoadBitmap('PZD_STONE');
  OutputPanel1.Background.LoadBitmap('PZD_STONE');
  OutputPanel2.Background.LoadBitmap('PZD_STONE');
  FillTree;
End;

procedure TPZTreeViewForm.FillTree;


Begin
  With TreeView Do
    Begin
      Furniture:=AddItem(RootItem,'Furniture');
      Bedroom:=AddItem(Furniture,'Bedroom');
      AddItem(Bedroom,'Two drawer chest');
      AddItem(Bedroom,'Three drawer chest');
      AddItem(Bedroom,'Five drawer chest');
      AddItem(Bedroom,'Dressing table');
      AddItem(Bedroom,'Tallboy');
      AddItem(Bedroom,'Wardrobe');
      AddItem(Bedroom,'Ottoman');
      Lounge:=AddItem(Furniture,'Lounge');
      AddItem(Lounge,'Dutch dresser');
      AddItem(Lounge,'TV cabinet');
      AddItem(Lounge,'Nest of tables');
      AddItem(Lounge,'Writing bureau');
      AddItem(Lounge,'Tall bookcase');
      AddItem(Lounge,'Sideboard');
      AddItem(Lounge,'Occasional table');
      AddItem(Lounge,'Magazine rack');
      AddItem(Lounge,'Coffee table');
      Dining:=AddItem(Furniture,'Dining room');
      AddItem(Dining,'Dining table');
      AddItem(Dining,'Pair of dining chairs');
      AddItem(Dining,'Farmhouse table');
      AddItem(Dining,'Solid pine dresser');
      AddItem(Dining,'Solid pine sideboard');
      AddItem(Dining,'Drop leaf table');
      AddItem(Dining,'Bar chair');
      AddItem(Dining,'Queen Anne dining table');
      AddItem(Dining,'Buttefly table and chairs');
  End;
End;

procedure TPZTreeViewForm.FormClose(Sender: TObject;  var Action: TCloseAction);

Begin
  Action:=caFree;
End;

procedure TPZTreeViewForm.FormDeactivate(Sender: TObject);
begin
  Close;
end;

procedure TPZTreeViewForm.Label1Click(Sender: TObject);
begin
  If BackgroundCheck.State=cbChecked Then
    BackgroundCheck.State:=cbUnchecked
  Else
    BackgroundCheck.State:=cbChecked;
  BackgroundCheck.SetFocus;
end;

procedure TPZTreeViewForm.BackgroundCheckClick(Sender: TObject);
begin
  If BackgroundCheck.State=cbChecked Then
    Begin
      TreeView.Background.LoadBitmap('PZD_PARQUET');
      TreeView.Font.Color:=clWhite;
    End
  Else
    Begin
      TreeView.Background.Bitmap.Assign(Nil);
      TreeView.Font.Color:=clWindowText;
    End;
end;

end.
