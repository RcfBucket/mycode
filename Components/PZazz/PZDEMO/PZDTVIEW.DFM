�
 TPZTREEVIEWFORM 0;  TPF0TPZTreeViewFormPZTreeViewFormLeft� Top� WidthUHeight�Caption
PZTreeView
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 	FormStyle
fsMDIChildPixelsPerInch`Position	poDefaultVisible	OnClose	FormCloseOnCreate
FormCreateOnDeactivateFormDeactivate
TextHeight TPZBitmapPanelHeaderPanelLeft Top WidthMHeight%AlignalTopBackground.TransparentBackground.StylebsTiled
BevelInner	bvLowered
BevelOuterbvNoneBorderWidthTabOrder  	TPZShaded	PZShaded1LeftTopWidthCHeightAlignalClientCaption
PZTreeView
Font.ColorclBlackFont.Height�	Font.NameTimes New Roman
Font.StylefsBold 
ParentFont
StartColorclWhiteEndColor��� 
Graduation
	DirectiondrHorizontalInOut   TPZBitmapPanel
DescrPanelLeft Top%Width� HeightdAlignalLeftBackground.TransparentBackground.StylebsTiled
BevelInner	bvLowered
BevelOuterbvNone
BevelWidthBorderWidthTabOrder TPZLabelPZLabel4LeftTopWidth� Height� AlignalTopCaption�\lbTPZTreeView is an alternative to Delphi's standard TOutline component.\lb\lbThe background can be plain or contain a bitmap.\lb\lbEach item can have a different image associated with it.
Font.ColorclBlackFont.Height�	Font.NameArial
Font.Style HorzAlignmenthaCenter
ParentFontVertAlignmentvaTop  TPZLabelPZLabel2LeftTopWidth� HeightAlignalTopCaptionDescription:
Font.ColorclNavyFont.Height�	Font.NameArial
Font.StylefsBold HorzAlignmenthaCenter
ParentFontVertAlignmentvaTop   TPZBitmapPanelResultsPanelLeft� Top%WidthxHeightdAlignalClientBackground.TransparentBackground.StylebsTiled
BevelInner	bvLowered
BevelOuterbvNone
BevelWidthBorderWidthTabOrder TPZLabelPZLabel1LeftTopHWidthhHeightAlignalBottomCaptionTry it now!
Font.ColorclBlackFont.Height�	Font.NameArial
Font.StylefsBold HorzAlignmenthaCenter
ParentFontVertAlignmentvaTop  TPZLabelPZLabel5LeftTopWidthhHeightAlignalTopCaptionExample:
Font.ColorclNavyFont.Height�	Font.NameArial
Font.StylefsBold HorzAlignmenthaCenter
ParentFontVertAlignmentvaTop  TPZBitmapPanelOutputPanel1LeftTop9WidthhHeightAlignalClientBackground.TransparentBackground.StylebsTiled
BevelOuterbvNoneBorderWidthTabOrder  TPZTreeViewTreeViewLeftTopWidthXHeight� AlignalClientBackground.TransparentBackground.StylebsTiledCtl3D
Font.ColorclBlackFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ItemHeight
LevelWidthOptionstoShowImagestoThumbTracking
toShowTree ParentColorParentCtl3D
ParentFontTabOrder TabStop	   TPZBitmapPanelOutputPanel2LeftTopWidthhHeightAlignalTopBackground.TransparentBackground.StylebsTiled
BevelOuterbvNoneBorderWidthTabOrder TLabelLabel1LeftTopWidthIHeightCaption BackgroundTransparent	OnClickLabel1Click
OnDblClickLabel1Click  	TCheckBoxBackgroundCheckLeftTopWidthHeightCtl3DParentCtl3DTabOrder OnClickBackgroundCheckClick     