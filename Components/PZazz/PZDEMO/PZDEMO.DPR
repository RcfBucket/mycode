program Pzdemo;

uses
  Forms,
  PZDShade in 'PZDSHADE.PAS' {PZShadedForm},
  Pzdbpane in 'PZDBPANE.PAS' {PZBitmapPanelForm},
  Pzdspbut in 'PZDSPBUT.PAS' {PZSpeedButtonForm},
  Pzdlview in 'PZDLVIEW.PAS' {PZListViewForm},
  Pzdtview in 'PZDTVIEW.PAS' {PZTreeViewForm},
  PZDMain in 'PZDMAIN.PAS' {MainForm},
  PZDLabel in 'PZDLABEL.PAS' {PZLabelForm},
  Pzdbitbt in 'PZDBITBT.PAS' {PZBitmapButtonForm};

{$R *.RES}

begin
  Application.Title := 'Pzazz!';
  Application.CreateForm(TMainForm, MainForm);
  Application.Run;
end.
