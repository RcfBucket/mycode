unit PZDShade;

interface

uses
  SysUtils, WinTypes, WinProcs, Messages, Classes, Graphics, Controls,
  Forms, Dialogs, ExtCtrls, PZBPanel, PZShaded, StdCtrls, PZLabel, PZSpButt;

type
  TPZShadedForm = class(TForm)
    HeaderPanel: TPZBitmapPanel;
    PZShaded1: TPZShaded;
    DescrPanel: TPZBitmapPanel;
    PZLabel4: TPZLabel;
    ResultsPanel: TPZBitmapPanel;
    PZLabel5: TPZLabel;
    PZLabel2: TPZLabel;
    OutputPanel1: TPZBitmapPanel;
    PZShaded2: TPZShaded;
    OutputPanel3: TPZBitmapPanel;
    PZShaded3: TPZShaded;
    PZShaded4: TPZShaded;
    OutputPanel2: TPZBitmapPanel;
    OutputPanel4: TPZBitmapPanel;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormCreate(Sender: TObject);
    procedure FormDeactivate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  PZShadedForm: TPZShadedForm;

implementation

Uses PZDMain;

{$R *.DFM}

procedure TPZShadedForm.FormCreate(Sender: TObject);

Begin
  SetBounds(10,10,MainForm.ClientWidth-20,MainForm.ClientHeight-MainForm.MainPanel.Height-20);
  HeaderPanel.Background.LoadBitmap('PZD_STONE');
  DescrPanel.Background.LoadBitmap('PZD_STONE');
  ResultsPanel.Background.LoadBitmap('PZD_STONE');
  OutputPanel1.Background.LoadBitmap('PZD_STONE');
  OutputPanel2.Background.LoadBitmap('PZD_STONE');
  OutputPanel3.Background.LoadBitmap('PZD_STONE');
  OutputPanel4.Background.LoadBitmap('PZD_STONE');
End;

procedure TPZShadedForm.FormClose(Sender: TObject;  var Action: TCloseAction);

Begin
  Action:=caFree;
End;

procedure TPZShadedForm.FormDeactivate(Sender: TObject);
begin
  Close;
end;

end.
