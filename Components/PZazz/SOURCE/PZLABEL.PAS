unit PZLabel;

interface

uses
  SysUtils, WinTypes, WinProcs, Messages, Classes, Graphics, Controls,
  Forms, Dialogs, Menus;

type
  TPZHorzAlignment =(haLeft,haRight,haCenter);
  TPZVertAlignment =(vaTop,vaBottom,vaCenter);
  TPZPaintType     =(ptPaint,ptCalcRect);

  TPZLabel = class(TGraphicControl)
  private
    { Private declarations }
    FHorzAlignment :TPZHorzAlignment;
    FVertAlignment :TPZVertAlignment;
    FUnregistered  :Boolean;
    function GetTransparent: Boolean;
    procedure SetTransparent(Value: Boolean);
    procedure SetHorzAlignment(Value :TPZHorzAlignment);
    procedure SetVertAlignment(Value :TPZVertAlignment);
    Function PaintText(Str :String; r :TRect; PaintType :TPZPaintType) :TRect;
    procedure CMTextChanged(var Message: TMessage); message CM_TEXTCHANGED;
  protected
    { Protected declarations }
    procedure Paint; override;
  public
    { Public declarations }
    constructor Create(AOwner: TComponent); override;
  published
    { Published declarations }
    property Align;
    property Caption;
    property Color;
    property DragCursor;
    property DragMode;
    property Enabled;
    property Font;
    property HorzAlignment :TPZHorzAlignment read FHorzAlignment write SetHorzAlignment;
    property ParentColor;
    property ParentFont;
    property ParentShowHint;
    property PopupMenu;
    property ShowHint;
    property Transparent: Boolean read GetTransparent write SetTransparent default True;
    property VertAlignment :TPZVertAlignment read FVertAlignment write SetVertAlignment;
    property Visible;
    property OnClick;
    property OnDblClick;
    property OnDragDrop;
    property OnDragOver;
    property OnEndDrag;
    property OnMouseDown;
    property OnMouseMove;
    property OnMouseUp;
  end;

implementation

{$I PZUnReg}

Procedure FindReplace(Var Str :String; Find,Replace :String; CS :Boolean);

Var
  CSStr :String;
  p     :Integer;

Begin
  If Not CS Then
    CSStr:=Str
  Else
    Begin
      CSStr:=UpperCase(Str);
      Find:=UpperCase(Find);
    End;
  Repeat
    p:=Pos(Find,CSStr);
    If p>0 Then
      Begin
        Delete(Str,p,Length(Find));
        Delete(CSStr,p,Length(Find));
        Insert(Replace,Str,p);
        Insert(Replace,CSStr,p);
      End;
  Until p=0;
End;

constructor TPZLabel.Create(AOwner: TComponent);

Begin
  Inherited Create(AOwner);
  ControlStyle:=ControlStyle-[csOpaque];
  Width:=65;
  Height:=17;
  Transparent:=True;
  FUnregistered:=GetUnregistered;
End;

procedure TPZLabel.Paint;

Var
  xadd :Integer;
  yadd :Integer;
  tr,r :TRect;

Begin
  With Canvas Do
    Begin
      r:=GetClientRect;
      If Transparent Then
        Brush.Style:=bsClear
      Else
        Begin
          Brush.Style:=bsSolid;
          Brush.Color:=Self.Color;
          FillRect(r);
        End;
      tr:=PaintText(Caption,r,ptCalcRect);
      Case FHorzAlignment Of
        haLeft   :xadd:=0;
        haRight  :xadd:=r.Right-tr.Right;
        haCenter :xadd:=(r.Right Div 2)-(tr.Right Div 2);
      End;
      Case FVertAlignment Of
        vaTop    :yadd:=0;
        vaBottom :yadd:=r.Bottom-tr.Bottom;
        vaCenter :yadd:=(r.Bottom Div 2)-(tr.Bottom Div 2);
      End;
      OffsetRect(tr,xadd,yadd);
      PaintText(Caption,tr,ptPaint);
    End;
  If FUnregistered Then
    PaintUnregistered(Canvas,GetClientRect);
End;

Function TPZLabel.PaintText(Str :String; r :TRect; PaintType :TPZPaintType) :TRect;

Var
  OldStyle :TFontStyles;
  OldColor :TColor;
  OneWord  :String;
  OneLine  :String;
  tr       :TRect;
  th,tw    :Integer;
  w,x      :Integer;

Function Min(a,b :Integer) :Integer;

Begin
  If a<b Then
    Result:=a
  Else
    Result:=b
End;

Function GetNextWord(Var Str :String) :String;

Var
  lb,sp,i :Integer;

Begin
  Result:='';
  If Str='' Then
    Exit;
  i:=1;
  While (i<=Length(Str)) And (Str[i]=' ') Do
    Inc(i);
  sp:=Pos(' ',Str);
  lb:=Pos(#$0A,Str);
  If (sp=0) And (lb=0) Then
    Begin
      Result:=Str;
      Str:='';
    End;
  If (sp=0) And (lb<>0) Then
    Begin
      Result:=Copy(Str,1,lb);
      Delete(Str,1,lb);
    End;
  If (lb=0) And (sp<>0) Then
    Begin
      Result:=Copy(Str,1,sp);
      Delete(Str,1,sp);
    End;
  If (lb<>0) And (sp<>0) Then
    Begin
      Result:=Copy(Str,1,Min(lb,sp));
      Delete(Str,1,Min(lb,sp));
    End;
End;

Function GetNextPart(Var Str :String) :String;

Var
  i :Integer;

Begin
  i:=1;
  While (i<=Length(Str)) And (Not (Str[i] In [#1,#$0A])) Do
    Inc(i);
  Result:=Copy(Str,1,i-1);
  Delete(Str,1,i-1);
End;

Procedure ProcessToken(Var Str :String);

Const
  FontColors :Array[0..17] Of TColor =(clBlack,clMaroon,clGreen,clOlive,
                                       clNavy,clPurple,clTeal,clGray,clSilver,
                                       clRed,clLime,clYellow,clBlue,clFuchsia,
                                       clAqua,clLtGray,clDkGray,clWhite);

Begin
  If (Length(Str)>=1) And (Str[1]=#$0A) Then
    Delete(Str,1,1)
  Else If (Length(Str)>=2) And (Str[1]=#1) Then
    With Canvas Do
      Begin
        Case Str[2] Of
          #100 :Font.Style:=Font.Style+[fsBold];
          #101 :Font.Style:=Font.Style-[fsBold];
          #102 :Font.Style:=Font.Style+[fsItalic];
          #103 :Font.Style:=Font.Style-[fsItalic];
          #104 :Font.Style:=Font.Style+[fsUnderline];
          #105 :Font.Style:=Font.Style-[fsUnderline];
          #106 :Font.Style:=Self.Font.Style;
          #107..#124 :Font.Color:=FontColors[Byte(Str[2])-107];
          #125 :Font.Color:=Self.Font.Color;
        End;
        Delete(Str,1,2);
      End;
End;

Procedure PaintLine(Str :String; x,y :Integer);

Var
  OnePart :String;

Begin
  With Canvas Do
    While Str<>'' Do
      Begin
        OnePart:=GetNextPart(Str);
        If PaintType=ptPaint Then
          TextOut(x,y,OnePart);
        Inc(x,TextWidth(OnePart));
        ProcessToken(Str);
      End;
End;

Function LineWidth(Str :String) :Integer;

Var
  OldStyle :TFontStyles;
  OldColor :TColor;
  OnePart  :String;

Begin
  Result:=0;
  With Canvas Do
    Begin
      OldStyle:=Font.Style;
      OldColor:=Font.Color;
      While Str<>'' Do
        Begin
          OnePart:=GetNextPart(Str);
          Inc(Result,TextWidth(OnePart));
          ProcessToken(Str);
        End;
      Font.Style:=OldStyle;
      Font.Color:=OldColor;
    End;
End;

Procedure TokeniseString(Var Str :String);

Const
{ Case sensitive tokens }
  CSTokens :Array[0..5] Of String = ('\B','\b','\I','\i','\U','\u');
{ Case insensitive tokens }
  CITokens :Array[6..25] Of String = ('\N','\cBlack','\cMaroon','\cGreen',
                                     '\cOlive','\cNavy','\cPurple','\cTeal',
                                     '\cGray','\cSilver','\cRed','\cLime',
                                     '\cYellow','\cBlue','\cFuchsia','\cAqua',
                                     '\cLtGray','\cDkGray','\cWhite','\cN');

Var
  t :Byte;

Begin
  FindReplace(Str,'\\',#0,False);
  For t:=Low(CSTokens) To High(CSTokens) Do
    FindReplace(Str,CSTokens[t],#1+Char(t+100),False);
  For t:=Low(CITokens) To High(CITokens) Do
    FindReplace(Str,CITokens[t],#1+Char(t+100),True);
  FindReplace(Str,#$0D+#$0A,#$0A,False);
  FindReplace(Str,#$0A+#$0D,#$0A,False);
  FindReplace(Str,#$0D,#$0A,False);
  FindReplace(Str,'\lb',#$0A,True);
  FindReplace(Str,#0,'\',False);
End;

Begin
  tr:=Rect(0,0,0,0);
  TokeniseString(Str);
  With Canvas Do
    Begin
      Font:=Self.Font;
      OldStyle:=Font.Style;
      OldColor:=Font.Color;
      th:=TextHeight('A');
      w:=r.Right-r.Left;
      OneWord:=GetNextWord(Str);
      While (OneWord<>'') Do
        Begin
          r.Bottom:=r.Top+th;
          OneLine:='';
          If LineWidth(OneWord)>w Then
            Begin
              OneLine:=OneWord;
              OneWord:='';
              While (OneLine<>'') And (LineWidth(OneLine)>w) Do
                Repeat
                  Insert(OneLine[Length(OneLine)],OneWord,1);
                  Delete(OneLine,Length(OneLine),1);
                Until OneLine[Length(OneLine)]<>#1;   { Avoid splitting tokens }
              If OneLine='' Then
                Begin
                  OneLine:=OneWord;
                  OneWord:=GetNextWord(Str);
                End;
            End
          Else
            While (OneWord<>'') And (LineWidth(OneLine+OneWord)<=w) And (Pos(#$0A,OneLine)=0) Do
              Begin
                OneLine:=OneLine+OneWord;
                OneWord:=GetNextWord(Str);
              End;
          tw:=LineWidth(OneLine);
          If tw>tr.Right Then
            tr.Right:=tw;
          If (OneLine<>'') And (OneLine[Length(OneLine)] In [#$0A,' ']) Then
            Delete(OneLine,Length(OneLine),1);
          tw:=LineWidth(OneLine);
          Case FHorzAlignment Of
            haLeft   :x:=r.Left;
            haRight  :x:=r.Right-tw;
            haCenter :x:=r.Left+((w Div 2)-(tw Div 2));
          End;
          PaintLine(OneLine,x,r.Top);
          Inc(r.Top,th);
          Inc(tr.Bottom,th);
        End;
      Font.Style:=OldStyle;
      Font.Color:=OldColor;
    End;
  Result:=tr;
End;

Procedure TPZLabel.SetVertAlignment(Value :TPZVertAlignment);

Begin
  If Value<>FVertAlignment Then
    Begin
      FVertAlignment:=Value;
      Invalidate;
    End;
End;

Procedure TPZLabel.SetHorzAlignment(Value :TPZHorzAlignment);

Begin
  If Value<>FHorzAlignment Then
    Begin
      FHorzAlignment:=Value;
      Invalidate;
    End;
End;

procedure TPZLabel.CMTextChanged(var Message: TMessage);

Begin
  Invalidate;
End;

Function TPZLabel.GetTransparent: Boolean;

Begin
  Result:=Not (csOpaque In ControlStyle);
End;

Procedure TPZLabel.SetTransparent(Value: Boolean);

Begin
  If Transparent<>Value Then
    Begin
      If Value then
        ControlStyle:=ControlStyle-[csOpaque]
      Else
        ControlStyle:=ControlStyle+[csOpaque];
      Invalidate;
    End;
End;

End.
