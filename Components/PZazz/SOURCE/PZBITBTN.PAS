unit Pzbitbtn;

interface

uses
  SysUtils, WinTypes, WinProcs, Messages, Classes, Graphics, Controls,
  Forms, Dialogs, StdCtrls, PZBgnd;

type
  TPZBitmapButton = class(TButton)
  private
    { Private declarations }
    FCanvas        :TCanvas;
    FBackground    :TPZBackground;
    IsDefault      :Boolean;
    FUnregistered  :Boolean;
    procedure BackgroundChanged(Sender: TObject);
    function GetBitmap :TBitmap;
    procedure SetBitmap(Value :TBitmap);
    procedure DrawUpButton(Var r :TRect);
    procedure DrawDownButton(Var r :TRect);
    procedure DrawDefaultButton(Var r :TRect);
    procedure CMFontChanged(var Message: TMessage); message CM_FONTCHANGED;
    procedure CMEnabledChanged(var Message: TMessage); message CM_ENABLEDCHANGED;
    procedure CNMeasureItem(var Message: TWMMeasureItem); message CN_MEASUREITEM;
    procedure CNDrawItem(var Message: TWMDrawItem); message CN_DRAWITEM;
    procedure WMLButtonDblClk(var Message: TWMLButtonDblClk); message WM_LBUTTONDBLCLK;
    procedure WMEraseBkgnd(var Message: TWMEraseBkgnd); message WM_ERASEBKGND;
  protected
    { Protected declarations }
    procedure CreateParams(var Params: TCreateParams); override;
    procedure SetButtonStyle(ADefault: Boolean); override;
  public
    { Public declarations }
    constructor Create(AOwner :TComponent); override;
    destructor Destroy; override;
    procedure LoadBitmap(BitmapName :String);
  published
    { Published declarations }
    property Bitmap :TBitmap read GetBitmap write SetBitmap;
  end;

implementation

{$I PZUnReg}

constructor TPZBitmapButton.Create(AOwner :TComponent);

Begin
  inherited Create(AOwner);
  FCanvas:=TCanvas.Create;
  FBackground:=TPZBackground.Create(Self,FCanvas,[100,60,140]);
  FBackground.OnChange:=BackgroundChanged;
  FUnregistered:=GetUnregistered;
End;

destructor TPZBitmapButton.Destroy;

begin
  FCanvas.Free;
  FBackground.Free;
  inherited Destroy;
end;

procedure TPZBitmapButton.CreateParams(var Params: TCreateParams);

Begin
  inherited CreateParams(Params);
  With Params Do
    Style:=Style Or BS_OWNERDRAW;
End;

procedure TPZBitmapButton.LoadBitmap(BitmapName :String);

begin
  FBackground.LoadBitmap(BitmapName);
end;

procedure TPZBitmapButton.BackgroundChanged(Sender: TObject);

begin
  Invalidate;
end;

function TPZBitmapButton.GetBitmap :TBitmap;

Begin
  Result:=FBackground.Bitmap;
End;

procedure TPZBitmapButton.SetBitmap(Value :TBitmap);

Begin
  FBackground.Bitmap.Assign(Value);
End;

procedure TPZBitmapButton.CNMeasureItem(var Message: TWMMeasureItem);

Begin
  With Message.MeasureItemStruct^ Do
    Begin
      itemWidth:=Width;
      itemHeight:=Height;
    End;
End;

procedure TPZBitmapButton.CNDrawItem(var Message: TWMDrawItem);

Var
  IsDown    :Boolean;
  IsFocused :Boolean;
  r         :TRect;
  s         :Array[0..255] Of Char;

Begin
  With Message.DrawItemStruct^ Do
    Begin
      FCanvas.Handle:=hDC;
      IsDown:=(itemState And ODS_SELECTED)<>0;
      IsFocused:=(itemState And ODS_FOCUS)<>0;
      FBackground.SetOffset(Ord(IsDown),Ord(IsDown));
      r:=ClientRect;
      If IsDown Then
        DrawDownButton(r)
      Else If (IsDefault Or IsFocused) Then
        DrawDefaultButton(r)
      Else
        DrawUpButton(r);
      FBackground.Draw(GetClientRect,r,100,Self.Color);
      r:=ClientRect;
      InflateRect(r,-4,-4);
      If IsFocused And (Not (csDesigning In ComponentState)) Then
        Begin
          FCanvas.Brush.Color:=Self.Color;
          FCanvas.FrameRect(r);
          DrawFocusRect(FCanvas.Handle,r);
        End;
      r:=ClientRect;
      If IsDown Then
        r:=Rect(r.Left+2,r.Top+2,r.Right-1,r.Bottom-1)
      Else
        r:=Rect(r.Left+1,r.Top+1,r.Right-2,r.Bottom-2);
      FCanvas.Brush.Style:=bsClear;
      FCanvas.Font:=Self.Font;
      If Enabled Or (csDesigning In ComponentState) Then
        DrawText(FCanvas.Handle,StrPCopy(s,Caption),-1,r,DT_CENTER+DT_VCENTER+DT_SINGLELINE)
      Else
        Begin
          OffsetRect(r,1,1);
          If FBackground.Bitmap.Empty Then
            FCanvas.Font.Color:=clBtnHighlight
          Else
            FCanvas.Font.Color:=clSilver;
          DrawText(FCanvas.Handle,StrPCopy(s,Caption),-1,r,DT_CENTER+DT_VCENTER+DT_SINGLELINE);
          OffsetRect(r,-1,-1);
          If FBackground.Bitmap.Empty Then
            FCanvas.Font.Color:=clBtnShadow
          Else
           FCanvas.Font.Color:=clDkGray;
          DrawText(FCanvas.Handle,StrPCopy(s,Caption),-1,r,DT_CENTER+DT_VCENTER+DT_SINGLELINE);
        End;
      FCanvas.Brush.Style:=bsSolid;
      If FUnregistered Then
        PaintUnregistered(FCanvas,GetClientRect);
      FCanvas.Handle:=0;
    End;
end;

procedure TPZBitmapButton.DrawUpButton(Var r :TRect);

Begin
  With FCanvas Do
    Begin
      FBackground.Draw(GetClientRect,Rect(r.Left,r.Top,r.Right,r.Top+1),140,clBtnHighlight);
      FBackground.Draw(GetClientRect,Rect(r.Left,r.Top,r.Left+1,r.Bottom),140,clBtnHighlight);
      Pen.Color:=clBlack;
      MoveTo(r.Right-1,0);
      LineTo(r.Right-1,r.Bottom);
      MoveTo(r.Left,r.Bottom-1);
      LineTo(r.Right,r.Bottom-1);
      FBackground.Draw(GetClientRect,Rect(r.Right-2,r.Top+1,r.Right-1,r.Bottom-1),60,clBtnShadow);
      FBackground.Draw(GetClientRect,Rect(r.Left+1,r.Bottom-2,r.Right-1,r.Bottom-1),60,clBtnShadow);
      Inc(r.Left);
      Inc(r.Top);
      Dec(r.Right,2);
      Dec(r.Bottom,2);
    End;
End;

procedure TPZBitmapButton.DrawDefaultButton(Var r :TRect);

Begin
  With FCanvas Do
    Begin
      Pen.Color:=clBlack;
      MoveTo(r.Left,r.Top);
      LineTo(r.Right-1,r.Top);
      LineTo(r.Right-1,r.Bottom-1);
      LineTo(r.Left,r.Bottom-1);
      LineTo(r.Left,r.Top);
      MoveTo(r.Right-2,r.Top+1);
      LineTo(r.Right-2,r.Bottom-2);
      LineTo(r.Left,r.Bottom-2);
      FBackground.Draw(GetClientRect,Rect(r.Left+1,r.Top+1,r.Right-2,r.Top+2),140,clBtnHighlight);
      FBackground.Draw(GetClientRect,Rect(r.Left+1,r.Top+2,r.Left+2,r.Bottom-2),140,clBtnHighlight);
      FBackground.Draw(GetClientRect,Rect(r.Right-3,r.Top+2,r.Right-2,r.Bottom-2),60,clBtnShadow);
      FBackground.Draw(GetClientRect,Rect(r.Left+2,r.Bottom-3,r.Right-2,r.Bottom-2),60,clBtnShadow);
      InflateRect(r,-2,-2);
      Dec(r.Right);
      Dec(r.Bottom);
    End;
End;

procedure TPZBitmapButton.DrawDownButton(Var r :TRect);

Begin
  With FCanvas Do
    Begin
      Pen.Color:=clBlack;
      MoveTo(r.Left,r.Top);
      LineTo(r.Right-1,r.Top);
      LineTo(r.Right-1,r.Bottom-1);
      LineTo(r.Left,r.Bottom-1);
      LineTo(r.Left,r.Top);
      FBackground.Draw(GetClientRect,Rect(r.Left+1,r.Top+1,r.Right-2,r.Top+2),60,clBtnShadow);
      FBackground.Draw(GetClientRect,Rect(r.Right-2,r.Top+1,r.Right-1,r.Bottom-2),60,clBtnShadow);
      FBackground.Draw(GetClientRect,Rect(r.Left+1,r.Bottom-2,r.Right-1,r.Bottom-1),60,clBtnShadow);
      FBackground.Draw(GetClientRect,Rect(r.Left+1,r.Top+1,r.Left+2,r.Bottom-2),60,clBtnShadow);
      InflateRect(r,-2,-2);
    End;
End;

procedure TPZBitmapButton.SetButtonStyle(ADefault: Boolean);

Begin
  If ADefault<>IsDefault Then
    Begin
      IsDefault:=ADefault;
      Refresh;
    End;
End;

procedure TPZBitmapButton.WMLButtonDblClk(var Message: TWMLButtonDblClk);

Begin
  Perform(WM_LBUTTONDOWN,Message.Keys,Longint(Message.Pos));
End;

procedure TPZBitmapButton.CMFontChanged(var Message: TMessage);

Begin
  inherited;
  Invalidate;
End;

procedure TPZBitmapButton.CMEnabledChanged(var Message: TMessage);

Begin
  inherited;
  Invalidate;
End;

procedure TPZBitmapButton.WMEraseBkgnd(var Message: TWMEraseBkgnd);

Begin
  Message.Result:=1;
End;

end.
