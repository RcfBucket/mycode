unit PZSpButt;

interface

uses
  SysUtils, WinTypes, WinProcs, Messages, Classes, Graphics, Controls,
  Forms, Dialogs, Buttons, PZImList, Menus;

type
  TPZSpeedButtonState =(sbNone,sbUp,sbUpClick,sbDown,sbDownClick,sbPopup);
  TPZDrawState        =(dsNone,dsUp,dsDown,dsStayDown);
  TPZButtonLayout     =(blLeft,blRight,blTop,blBottom);
  TPZNumGlyphs        =1..3;

  TPZSpeedButton = class(TGraphicControl)
  private
    { Private declarations }
    FGlyph        :TBitmap;
    FGlyphList    :TPZImageList;
    FNumGlyphs    :TPZNumGlyphs;
    FButtonState  :TPZSpeedButtonState;
    FDrawState    :TPZDrawState;
    FGroupIndex   :Integer;
    FDown         :Boolean;
    FAllowAllUp   :Boolean;
    FSpacing      :Integer;
    FMargin       :Integer;
    FLayout       :TPZButtonLayout;
    FUnregistered :Boolean;
    function GetGlyph: TBitmap;
    procedure SetGlyph(Value: TBitmap);
    procedure GlyphChanged(Sender :TObject);
    procedure SetNumGlyphs(Value: TPZNumGlyphs);
    procedure UpdateGlyphList;
    procedure SetGroupIndex(Value :Integer);
    procedure SetDown(Value :Boolean);
    procedure UpdateGroup;
    procedure SetButtonState(Value :TPZSpeedButtonState);
    procedure SetDrawState(Value :TPZDrawState);
    Procedure AddSelectedGlyph;
    Procedure AddDisabledGlyph;
    procedure SetAllowAllUp(Value: Boolean);
    procedure SetMargin(Value: Integer);
    procedure SetSpacing(Value: Integer);
    procedure SetLayout(Value: TPZButtonLayout);
    procedure CalcButtonLayout(Var gx,gy :Integer; Var tr :TRect);
    procedure CMFontChanged(var Message: TMessage); message CM_FONTCHANGED;
    procedure CMTextChanged(var Message: TMessage); message CM_TEXTCHANGED;
    procedure CMEnabledChanged(var Message: TMessage); message CM_ENABLEDCHANGED;
    procedure CMDialogChar(var Message: TCMDialogChar); message CM_DIALOGCHAR;
    procedure CMMouseEnter(var Message: TMessage); message CM_MOUSEENTER;
    procedure CMMouseLeave(var Message: TMessage); message CM_MOUSELEAVE;
  protected
    { Protected declarations }
    procedure Loaded; override;
    procedure Paint; override;
    function GetPalette: HPALETTE; override;
    procedure MouseDown(Button: TMouseButton; Shift: TShiftState; X, Y: Integer); override;
    procedure MouseUp(Button: TMouseButton; Shift: TShiftState; X, Y: Integer); override;
  public
    { Public declarations }
    procedure Click; override;
  published
    { Published declarations }
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
    property AllowAllUp: Boolean read FAllowAllUp write SetAllowAllUp default False;
    property Caption;
    property GroupIndex: Integer read FGroupIndex write SetGroupIndex default 0;
    property Down :Boolean read FDown write SetDown default False;
    property Enabled;
    property Font;
    property Glyph: TBitmap read GetGlyph write SetGlyph;
    property Layout: TPZButtonLayout read FLayout write SetLayout default blTop;
    property Margin: Integer read FMargin write SetMargin default -1;
    property NumGlyphs :TPZNumGlyphs read FNumGlyphs write SetNumGlyphs default 1;
    property PopupMenu;
    property ParentFont;
    property ParentShowHint;
    property ShowHint;
    property Spacing: Integer read FSpacing write SetSpacing default 4;
    property Visible;
    property OnClick;
    property OnMouseDown;
    property OnMouseMove;
    property OnMouseUp;
 end;

implementation

Var
  DownPattern :TBitmap;
  ButtonCount :Integer;

{$I PZUnReg}

procedure CreateDownPattern;

Var
  x,y :Integer;

Begin
  DownPattern:=TBitmap.Create;
  DownPattern.Width:=8;
  DownPattern.Height:=8;
  With DownPattern.Canvas Do
    Begin
      Brush.Style:=bsSolid;
      Brush.Color:=clBtnFace;
      FillRect(Rect(0,0,DownPattern.Width,DownPattern.Height));
      For y:=0 To 7 Do
        For x:=0 To 8 do
          If (y Mod 2)=(x Mod 2) Then
            Pixels[x,y]:=clBtnHighlight;
    End;
End;

constructor TPZSpeedButton.Create(AOwner: TComponent);

Begin
  inherited Create(AOwner);
  SetBounds(0,0,25,25);
  ControlStyle:=[csDoubleClicks];
  FSpacing:=4;
  FMargin:=-1;
  FNumGlyphs:=1;
  FLayout:=blTop;
  FGlyph:=TBitmap.Create;
  FGlyph.OnChange:=GlyphChanged;
  Inc(ButtonCount);
  FUnregistered:=GetUnregistered;
End;

destructor TPZSpeedButton.Destroy;

Begin
  FGlyph.Free;
  FGlyphList.Free;
  Dec(ButtonCount);
  If ButtonCount=0 Then
    Begin
      DownPattern.Free;
      DownPattern:=nil;
    End;
  Inherited Destroy;
End;

procedure TPZSpeedButton.Loaded;

Begin
  UpdateGlyphList; 
  inherited Loaded;
End;

function TPZSpeedButton.GetGlyph: TBitmap;

begin
  Result:=FGlyph;
end;

procedure TPZSpeedButton.SetGlyph(Value: TBitmap);

begin
  FGlyph.Assign(Value);
  If (Value<>Nil) And (Not Value.Empty) And (Value.Width Mod Value.Height=0) Then
    Begin
      FNumGlyphs:=Value.Width Div Value.Height;
      If FNumGlyphs>3 Then
        FNumGlyphs:=1;
    End;
  UpdateGlyphList;
end;

procedure TPZSpeedButton.SetNumGlyphs(Value :TPZNumGlyphs);

Begin
  If (FNumGlyphs<>Value) And (Value>0) Then
    Begin
      FNumGlyphs:=Value;
      UpdateGlyphList;
    End;
End;

procedure TPZSpeedButton.GlyphChanged(Sender :TObject);

Begin
  UpdateGlyphList;
End;

procedure TPZSpeedButton.UpdateGlyphList;

Var
  w,h  :Integer;
  Temp :TBitmap;
  g    :Integer;
  sx   :Integer;

Begin
  If csReadingState In ControlState Then
    Exit;
  FGlyphList.Free;
  FGlyphList:=Nil;
  If (FGlyph<>Nil) And (Not FGlyph.Empty) Then
    Begin
      If FGlyph.Monochrome Then
        Begin
          FGlyph.Canvas.Brush.Color:=clWhite;
          FGlyph.Canvas.Font.Color:=clBlack;
          FGlyph.Monochrome:=False;
        End;
      w:=FGlyph.Width Div FNumGlyphs;
      h:=FGlyph.Height;
      FGlyphList:=TPZImageList.Create(w,h);
      Temp:=TBitmap.Create;
      Try
        Temp.Assign(FGlyph);
        Temp.Width:=w;
        Temp.Height:=h;
        sx:=0;
        For g:=1 To FNumGlyphs Do
          Begin
            Temp.Canvas.CopyRect(Rect(0,0,w,h),FGlyph.Canvas,Rect(sx,0,sx+w,h));
            FGlyphList.AddMasked(Temp,FGlyph.TransparentColor);
            Inc(sx,w);
          End;
      Finally
        Temp.Free;
      End;
      If FNumGlyphs<2 Then
        AddSelectedGlyph;
      If FNumGlyphs<3 Then
        AddDisabledGlyph;
    End;
  Invalidate;
End;

Procedure TPZSpeedButton.AddSelectedGlyph;

Var
  Temp :TBitmap;

Begin
  Temp:=TBitmap.Create;
  Try                     
    Temp.Assign(FGlyph);
    Temp.Width:=FGlyphList.Width;
    FGlyphList.AddMasked(Temp,FGlyph.TransparentColor);
  Finally
    Temp.Free;
  End;
End;

Procedure TPZSpeedButton.AddDisabledGlyph;

Var
  Mono,Temp :TBitmap;

Begin
  Mono:=TBitmap.Create;
  Temp:=TBitmap.Create;
  Try
    With Mono Do
      Begin
        Assign(FGlyph);
        Canvas.Brush.Color:=clBlack;
        Width:=FGlyphList.Width;
        If Monochrome Then
          Begin
            Canvas.Font.Color:=clWhite;
            Monochrome:=False;
            Canvas.Brush.Color:=clWhite;
          End;
        Monochrome:=True;
      End;
    With Temp.Canvas Do
      Begin
        Temp.Width:=FGlyphList.Width;
        Temp.Height:=FGlyphList.Height;
        Brush.Color:=clOlive;
        FillRect(Rect(0,0,Temp.Width,Temp.Height));
        Brush.Color:=clBlack;
        Font.Color:=clBtnHighlight;
        CopyMode:=MergePaint;
        Draw(1,1,Mono);
        CopyMode:=SrcAnd;
        Draw(0,0,Mono);
        Brush.Color:=clBtnShadow;
        Font.Color:=clBlack;
        CopyMode:=SrcPaint;
        Draw(0,0,Mono);
        CopyMode:=SrcCopy;
      End;
    FGlyphList.AddMasked(Temp,Temp.TransparentColor);
  Finally
    Mono.Free;
    Temp.Free;
  End;
End;

procedure TPZSpeedButton.Paint;

Var
  gx,gy  :Integer;
  w,h    :Integer;
  tr     :TRect;
  DState :TPZDrawState;
  s      :Array[0..255] Of Char;

Procedure DrawGlyph(x,y :Integer; Index :Integer);

Var
  Palette,OldPalette :HPalette;

Begin
  If FGlyphList=Nil Then
    Exit;
  Palette:=GetPalette;
  If Palette=0 Then
    OldPalette:=0
  Else
    Begin
      OldPalette:=SelectPalette(Canvas.Handle,Palette,True);
      RealizePalette(Canvas.Handle);
    End;
  FGlyphList.Draw(Canvas,x,y,Index);
  If OldPalette<>0 Then
    SelectPalette(Canvas.Handle,OldPalette,True);
End;

procedure DrawButton(Up :Boolean);

Const
  Colors1 :Array[0..1] Of TColor =(clBtnShadow,clBtnHighlight);
  Colors2 :Array[0..1] Of TColor =(clBtnHighlight,clBtnShadow);

Var
  w,h :Integer;
  c1,c2 :TColor;

Begin
  With Canvas Do
    Begin
      w:=ClientWidth;
      h:=ClientHeight;
      Pen.Color:=Colors1[Ord(Up)];
      MoveTo(0,h-1);
      LineTo(0,0);
      LineTo(w-1,0);
      Pen.Color:=Colors2[Ord(Up)];
      LineTo(w-1,h-1);
      LineTo(0,h-1);
    End;
End;

Begin
  With Canvas Do
    Begin
      w:=ClientWidth;
      h:=ClientHeight;
      CalcButtonLayout(gx,gy,tr);
      DState:=FDrawState;
      If (csDesigning In ComponentState) And (Not FDown) Then
        DState:=dsUp;
      If Not Enabled Then
        Begin
          If (csDesigning In ComponentState) Then
            DrawButton(True);
          DrawGlyph(gx,gy,2);
        End
      Else
        Case DState Of
          dsNone     :DrawGlyph(gx,gy,1);
          dsUp       :Begin
                        DrawButton(True);
                        DrawGlyph(gx,gy,0);
                      End;
          dsDown     :Begin
                        DrawButton(False);
                        DrawGlyph(gx+1,gy+1,0);
                        OffsetRect(tr,1,1);
                      End;
          dsStayDown :Begin
                        If DownPattern=Nil Then
                          CreateDownPattern;
                        Canvas.Brush.Bitmap:=DownPattern;
                        Canvas.FillRect(Rect(1,1,w-1,h-1));
                        Canvas.Brush.Style:=bsClear;
                        DrawButton(False);
                        DrawGlyph(gx+1,gy+1,0);
                        OffsetRect(tr,1,1);
                      End;
        End;
      Font:=Self.Font;
      Brush.Style:=bsClear;
      If Enabled Then
        DrawText(Handle,StrPCopy(s,Caption),-1,tr,DT_CENTER+DT_WORDBREAK)
      Else
        Begin
          OffsetRect(tr,1,1);
          Font.Color:=clBtnHighlight;
          DrawText(Handle,StrPCopy(s,Caption),-1,tr,DT_CENTER+DT_WORDBREAK);
          OffsetRect(tr,-1,-1);
          Font.Color:=clBtnShadow;
          DrawText(Handle,StrPCopy(s,Caption),-1,tr,DT_CENTER+DT_WORDBREAK);
        End
    End;
  If FUnregistered Then
    PaintUnregistered(Canvas,GetClientRect);
End;

procedure TPZSpeedButton.CalcButtonLayout(Var gx,gy :Integer; Var tr :TRect);

Var
  gw,gh,tw,th,sp,Size,d,cw,ch :Integer;
  r                           :TRect;
  s                           :Array[0..255] Of Char;

Begin
  gw:=0;
  gh:=0;
  tw:=0;
  th:=0;
  cw:=ClientWidth;
  ch:=ClientHeight;
  sp:=FSpacing;
  If (Caption='') Or (FGlyphList=Nil) Then
    sp:=0;
  If FGlyphList<>Nil Then
    Begin
      gw:=FGlyphList.Width;
      gh:=FGlyphList.Height;
    End;
  If Caption<>'' Then
    With Canvas Do
      Begin
        Font:=Self.Font;
        If (FLayout=blLeft) Or (FLayout=blRight) Then
          Begin
            tw:=TextWidth(Caption);
            th:=TextHeight(Caption);
          End
        Else
          Begin
            r:=Rect(2,0,cw-2,0);
            DrawText(Handle,StrPCopy(s,Caption),-1,r,DT_CALCRECT+DT_CENTER+DT_WORDBREAK);
            tw:=r.Right-r.Left;
            th:=r.Bottom-r.Top;
          End;
      End;
  If (FLayout=blLeft) Or (FLayout=blRight) Then
    Size:=gw+sp+tw
  Else
    Size:=gh+sp+th;
  Case FLayout Of
     blLeft  :Begin
                gx:=(cw Div 2)-(Size Div 2);
                gy:=(ch Div 2)-(gh Div 2);
                tr:=Rect(gx+gw+sp,(ch Div 2)-(th Div 2),gx+gw+sp+tw,(ch Div 2)+(th Div 2));
              End;
     blRight :Begin
                gx:=(cw Div 2)+(Size Div 2)-gw;
                gy:=(ch Div 2)-(gh Div 2);
                tr:=Rect(gx-sp-tw,(ch Div 2)-(th Div 2),gx-sp,(ch Div 2)+(th Div 2));
              End;
     blTop   :Begin
                gx:=(cw Div 2)-(gw Div 2);
                gy:=(ch Div 2)-(Size Div 2);
                tr:=Rect(2,gy+gh+sp,cw-2,gy+gh+sp+th);
              End;
    blBottom :Begin
                gx:=(cw Div 2)-(gw Div 2);
                gy:=(ch Div 2)+(Size Div 2)-gh;
                tr:=Rect(2,gy-sp-th,cw-2,gy-sp);
              End;
  End;
  If FMargin<>-1 Then
    Case FLayout Of
      blLeft   :Begin
                  d:=gx-FMargin;
                  Dec(gx,d);
                  OffsetRect(tr,-d,0);
                End;
      blRight  :Begin
                  d:=(cw-FMargin)-(gx+gw);
                  Inc(gx,d);
                  OffsetRect(tr,d,0);
                End;
      blTop    :Begin
                  d:=gy-FMargin;
                  Dec(gy,d);
                  OffsetRect(tr,0,-d);
                End;
      blBottom :Begin
                  d:=(ch-FMargin)-(gy+gh);
                  Inc(gy,d);
                  OffsetRect(tr,0,d);
                End;
    End;
End;

procedure TPZSpeedButton.CMMouseEnter(var Message: TMessage);

Begin
  If Enabled And (FButtonState=sbNone) Then
    SetButtonState(sbUp);
  inherited;
End;

procedure TPZSpeedButton.CMMouseLeave(var Message: TMessage);

Begin
  If Enabled Then
    Case FButtonState Of
      sbUp        :SetButtonState(sbNone);
      sbUpClick   :SetButtonState(sbNone);
      sbDownClick :SetButtonState(sbDown);
      sbPopup     :SetButtonState(sbNone);
    End;
  inherited;
End;

procedure TPZSpeedButton.MouseDown(Button: TMouseButton; Shift: TShiftState; X, Y: Integer);

Var
  pt :TPoint;

Begin
  If Button=mbLeft Then
    Case FButtonState Of
      sbUp   :If PopupMenu=Nil Then
                SetButtonState(sbUpClick)
              Else
                Begin
                  pt:=Parent.ClientToScreen(Point(Left,Top+Height));
                  SetButtonState(sbPopup);
                  PopupMenu.Popup(pt.x,pt.y);
                  GetCursorPos(pt);
                  pt:=ScreenToClient(pt);
                  If PtInRect(ClientRect,pt) Then
                    SetButtonState(sbUp)
                  Else
                    SetButtonState(sbNone)
                End;
      sbDown :If FAllowAllUp Then
                SetButtonState(sbDownClick);
      sbPopup :SetButtonState(sbUp);
    End;
  Inherited MouseDown(Button,Shift,X,Y);
End;

procedure TPZSpeedButton.MouseUp(Button: TMouseButton; Shift: TShiftState; X, Y: Integer);

Begin
  If Button=mbLeft Then
    Case FButtonState Of
      sbUpClick :Begin
                  If FGroupIndex<>0 Then
                     SetButtonState(sbDown)
                   Else
                     SetButtonState(sbUp);
                   Click;
                 End;
      sbDownClick :Begin
                    SetButtonState(sbUp);
                    Click;
                  End;
    End;
  Inherited MouseUp(Button,Shift,X,Y);
End;

procedure TPZSpeedButton.SetButtonState(Value :TPZSpeedButtonState);

Const
  DrawStates :Array[TPZSpeedButtonState] Of TPZDrawState =(dsNone,dsUp,dsDown,dsStayDown,dsDown,dsDown);

Begin
  If Value<>FButtonState Then
    Begin
      FButtonState:=Value;
      FDown:=(FButtonState=sbDown);
      If FButtonState=sbDown Then
        UpdateGroup;
      SetDrawState(DrawStates[FButtonState]);
    End;
End;

procedure TPZSpeedButton.SetDrawState(Value :TPZDrawState);

Begin
  If Value<>FDrawState Then
    Begin
      FDrawState:=Value;
      Invalidate;
    End;
End;

procedure TPZSpeedButton.CMFontChanged(var Message: TMessage);

begin
  Invalidate;
end;

procedure TPZSpeedButton.CMTextChanged(var Message: TMessage);
begin
  Invalidate;
end;

procedure TPZSpeedButton.CMEnabledChanged(var Message: TMessage);
begin
  Invalidate;
end;

procedure TPZSpeedButton.Click;

Begin
  inherited Click;
End;

procedure TPZSpeedButton.CMDialogChar(var Message: TCMDialogChar);

Begin
  With Message do
    If IsAccel(CharCode,Caption) And Enabled And Visible Then
      Begin
        Click;
        Result:=1;
      End
    Else
      Inherited;
End;

function TPZSpeedButton.GetPalette: HPALETTE;

Begin
  Result:=FGlyph.Palette;
End;

procedure TPZSpeedButton.UpdateGroup;

Var
  c :Integer;

Begin
  If (FGroupIndex<>0) And (Parent<>Nil) Then
    With Parent Do
      For c:=0 To ControlCount-1 Do
        If (Controls[c]<>Self) And (Controls[c] Is TPZSpeedButton) Then
          With Controls[c] As TPZSpeedButton Do
            If FGroupIndex=Self.FGroupIndex Then
              Begin
                 If (Self.FButtonState=sbDown) And (FButtonState=sbDown) Then
                    SetButtonState(sbNone);
                  FAllowAllUp:=Self.FAllowAllUp;
              End;
End;

procedure TPZSpeedButton.SetGroupIndex(Value: Integer);

Begin
  If FGroupIndex<>Value Then
    Begin
      FGroupIndex:=Value;
      UpdateGroup;
   End;
End;

procedure TPZSpeedButton.SetDown(Value :Boolean);

Begin
  If FGroupIndex=0 Then
    Value:=False;
  If Value<>FDown Then
    Begin
      If FDown And (Not FAllowAllUp) Then
        Exit;
      If Value Then
        SetButtonState(sbDown)
      Else
        SetButtonState(sbNone);
    End;
End;

procedure TPZSpeedButton.SetAllowAllUp(Value: Boolean);

Begin
  If FAllowAllUp<>Value Then
    Begin
      FAllowAllUp:=Value;
      UpdateGroup;
    End;
End;

procedure TPZSpeedButton.SetMargin(Value: Integer);

Begin
  If (Value<>FMargin) And (Value>=-1) Then
    Begin
      FMargin:=Value;
      Invalidate;
    End;
End;

procedure TPZSpeedButton.SetSpacing(Value: Integer);

Begin
  If Value<>FSpacing then
    Begin
      FSpacing:=Value;
      Invalidate;
    End;
End;

procedure TPZSpeedButton.SetLayout(Value: TPZButtonLayout);

Begin
  If Value<>FLayout Then
    Begin
      FLayout:=Value;
      Invalidate;
    End;
End;

End.

