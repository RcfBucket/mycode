{Written by David R. Faulkner, June 1996}
{P.O. Box 434, Kula HI, 96790}
{Internet: davef@maui.net}
{This unit implements TBarCode, a components that paints Code 39 barcodes}

unit QRBarcod;

interface

uses Messages, Classes, Controls, StdCtrls, BarCodeR, Quickrep;

type
  TQRBarCode = class(TQRLabel)
  private
    { Private declarations }
    procedure CaptionChanged(var Message: TMessage); message CM_TEXTCHANGED;

  protected
    { Protected declarations }
    procedure Paint; override;  {override paint so can draw barcode}
    procedure Print(x,y:integer); override;
  public
    { Public declarations }
    constructor Create(AOwner: TComponent); override; {so can set default}

  published
    { Published declarations }
    property Caption;
    property Alignment;
end;

procedure Register;

implementation

{******************************************************************************}

procedure Register;
begin        {Put TBarCode on Delphi Informant tab of Component Palette}
  RegisterComponents('QReport', [TQRBarCode]);
end;

procedure TQRBarCode.Paint;
begin
  {TBarCode's canvas is already located at Top,Left on the form, so we send
   0,0 to BarCodePaint}
  BarCodePaint(Caption,Canvas,0,0,Width,Height,Alignment);
end;

procedure TQRBarCode.Print(x,y:integer);
begin
  BarCodePaint(Caption,QRPrinter.Canvas,x,y,Width,Height,Alignment);
end;

constructor TQRBarCode.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
  ControlStyle := ControlStyle - [csOpaque]; {let windows draw background}
  Alignment :=taCenter;                      {default to centered barcode}
  Autosize:=false;                           {don't let tCustomLabel change size}
  Height:=50;                                {a reasonable height}
  Width:=200;                                {a reasonable width}
end;

procedure TQRBarCode.CaptionChanged(var Message: TMessage);
var
  x:integer;
  CaptionCopy:string;               {will build filtered caption in here}
begin
  CaptionCopy:='';
  for x:=1 to length(Caption) do    {filter out any unsupported characters}
    if (pos(upcase(Caption[x]),cValidCode39Characters)<>0) and
       (Caption[x]<>'*') then
      CaptionCopy:=CaptionCopy+upcase(Caption[x]);
  Caption:=CaptionCopy;
  inherited;                        {tCustomLabel's CMTextChanged will cause repaint}

end;


end.
