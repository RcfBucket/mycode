{Written by David R. Faulkner, June 1996}
{P.O. Box 434, Kula HI, 96790}
{Internet: davef@maui.net}

{This file is used by all the other barcode files
 to link in the component pallette icons and general routines}

unit Barcoder;  {Barcode Resourses}

{$IFDEF WIN32}
{$R BARCOD32.DCR}      {Component Palette icons}
{$ELSE}
{$R BARCOD16.DCR}
{$ENDIF}

interface
uses
  WinTypes, WinProcs, Classes, Graphics;

const
                         {         11111111112222222222333333333344444}
                         {12345678901234567890123456789012345678901234}
  cValidCode39Characters='0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ-. *$/+%';
  {The above string list all the characters supported by Code 39.  The position
   of each character corresponds to an entry in the following table}

  {N=Narrow Bar, W=WideBar}
  BarCodeTable: array[1..44, 1..9] of char = (
    'NNNWWNWNN',  {1 - 0}
    'WNNWNNNNW',  {2 - 1}
    'NNWWNNNNW',  {3 - 2}
    'WNWWNNNNN',  {4 - 3}
    'NNNWWNNNW',  {5 - 4}
    'WNNWWNNNN',  {6 - 5}
    'NNWWWNNNN',  {7 - 6}
    'NNNWNNWNW',  {8 - 7}
    'WNNWNNWNN',  {9 - 8}
    'NNWWNNWNN',  {10 - 9}
    'WNNNNWNNW',  {11 - A}
    'NNWNNWNNW',  {12 - B}
    'WNWNNWNNN',  {13 - C}
    'NNNNWWNNW',  {14 - D}
    'WNNNWWNNN',  {15 - E}
    'NNWNWWNNN',  {16 - F}
    'NNNNNWWNW',  {17 - G}
    'WNNNNWWNN',  {18 - H}
    'NNWNNWWNN',  {19 - I}
    'NNNNWWWNN',  {20 - J}
    'WNNNNNNWW',  {21 - K}
    'NNWNNNNWW',  {22 - L}
    'WNWNNNNWN',  {23 - M}
    'NNNNWNNWW',  {24 - N}
    'WNNNWNNWN',  {25 - O}
    'NNWNWNNWN',  {26 - P}
    'NNNNNNWWW',  {27 - Q}
    'WNNNNNWWN',  {28 - R}
    'NNWNNNWWN',  {29 - S}
    'NNNNWNWWN',  {30 - T}
    'WWNNNNNNW',  {31 - U}
    'NWWNNNNNW',  {32 - V}
    'WWWNNNNNN',  {33 - W}
    'NWNNWNNNW',  {34 - X}
    'WWNNWNNNN',  {35 - Y}
    'NWWNWNNNN',  {36 - Z}
    'NWNNNNWNW',  {37 - -}
    'WWNNNNWNN',  {38 - .}
    'NWWNNNWNN',  {39 - blank space}
    'NWNNWNWNN',  {40 - *}
    'NWNWNWNNN',  {41 - $}
    'NWNWNNNWN',  {42 - /}
    'NWNNNWNWN',  {43 - +}
    'NNNWNWNWN'); {44 - %}

procedure BarCodePaint(Caption:String;                 {barcode value to paint}
                       Canvas:TCanvas;                {canvas to paint it on}
                       left,top,width,height:integer;  {rectangle to paint it in}
                       Alignment:tAlignment);          {taLeftJustify,taRightJustify,taCenter}

implementation

procedure PaintABar(WideOrNarrow:char;ThisBarIsBlack:boolean;Acanvas:tcanvas;
                    var LeftEdge:integer;
                    Top,Height,NarrowWidth,WideWidth:integer);
var
  CurrentWidth:integer;
begin
  if WideOrNarrow='W' then
    CurrentWidth:=WideWidth
  else
    CurrentWidth:=NarrowWidth;

  if ThisBarIsBlack then       {don't paint a rectangle if bar is white}
    aCanvas.rectangle(LeftEdge,Top,LeftEdge+CurrentWidth,Top+Height);

  inc(LeftEdge,CurrentWidth);  {setup LeftEdge for next bar}
end;

{This routine is global in any project that uses this unit}
procedure BarCodePaint(Caption:String;                 {barcode value to paint}
                       Canvas:TCanvas;                 {canvas to paint it on}
                       left,top,width,height:integer;  {rectangle to paint it in}
                       Alignment:tAlignment);          {how to align it}
var
  x,y:integer;
  NarrowWidth,WideWidth:integer;  {width of narrow & wide bars}
  PixelsPerInch:integer;          {measure of pixels/inch for canvas being painted on}
  LeftEdge:integer;               {left edge of barcode, increases with each bar}
  Index:integer;                  {index in BarCodeTable for current char}
  BarCodeWidth:integer;           {width of painted area of barcode}
begin
  if Caption='' then exit;        {nothing to do}

  {Calculate widths of bars, take into account Pixels per inch, i.e. on the
   screen NarrowWidth=1, on a 600dpi printer, NarrowWidth=6}
  PixelsPerInch := GetDeviceCaps(Canvas.Handle, LOGPIXELSX);
  NarrowWidth:=trunc(1.0*PixelsPerInch/96.0);
  WideWidth:=trunc(3.0*PixelsPerInch/96.0);

  {set Brush and Pen for printing}
  Canvas.Brush.color:=clBlack;
  Canvas.Brush.style:=bsSolid;
  Canvas.Pen.color  :=clBlack;
  Canvas.Pen.style  :=psSolid;
  Canvas.Pen.width  :=0;

  {Code 39 wants an '*' as the start and stop character}
  Caption:='*'+Caption+'*';

  {for each character in caption need 3 wide bars, 6 narrow bar + 1 narrow for white space}
  BarCodeWidth:=Length(Caption)*(3*WideWidth + 7*NarrowWidth);
  if BarCodeWidth > Width then
    exit; {if we can't fit the whole thing then give up}

  {set LeftEdge according to Alignment}
  case Alignment of
    taLeftJustify:  LeftEdge:=Left;
    taCenter:       LeftEdge:=Left+((Width-BarCodeWidth) div 2);
    taRightJustify: LeftEdge:=Left+Width-BarCodeWidth;
  end;

  {Now print a series of 9 bars for each character in caption}
  for x:=1 to length(Caption) do begin
    Index:=pos(Caption[x],cValidCode39Characters); {make sure this is a printable character}
    if Index=0 then continue;  {if not printable character then don't print it}
    for y:=1 to 9 do
      PaintABar(BarCodeTable[Index][y],odd(y),Canvas,LeftEdge,Top,Height,NarrowWidth,WideWidth);
    inc(LeftEdge,NarrowWidth);  {white space between characters}
  end;
end;



end.
