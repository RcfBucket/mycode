unit Unit1;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, IdBaseComponent, IdComponent, IdTCPConnection,
  IdTCPClient, IdExplicitTLSClientServerBase, IdMessageClient, IdPOP3, IdMessage,
  IdText, IdMessageParts,
  Spin;

type
  TForm1 = class(TForm)
    Label1: TLabel;
    Label2: TLabel;
    BtnConnect: TButton;
    edHost: TEdit;
    edUser: TEdit;
    edPassword: TEdit;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Memo1: TMemo;
    Label6: TLabel;
    Label7: TLabel;
    p3: TIdPOP3;
    BtnGetHeader: TButton;
    BtnDisconnect: TButton;
    BtnGetAllSubjects: TButton;
    BtnGetMessage: TButton;
    edMsgIdx: TSpinEdit;
    Label8: TLabel;
    procedure BtnConnectClick(Sender: TObject);
    procedure BtnGetHeaderClick(Sender: TObject);
    procedure BtnDisconnectClick(Sender: TObject);
    procedure BtnGetAllSubjectsClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure BtnGetMessageClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;    

implementation

{$R *.DFM}

procedure TForm1.BtnConnectClick(Sender: TObject);
//var
//  s : TSummary;
begin
  label6.Caption:= 'Connecting...';
  Application.ProcessMessages;
  memo1.Clear;
  if p3.Connected then
    p3.Disconnect;

  p3.Host:= edHost.Text;
  p3.Username:= edUser.Text;
  p3.Password:= edPassword.Text;

  p3.Connect;
  try
    label2.caption:= IntToStr(p3.CheckMessages);

  //  p3.GetMailMessage(StrToInt(edMailNum.Text));

  //  s:= p3.Summary;
  //  label6.caption:= s.Subject;
    memo1.Lines.Assign(p3.Capabilities);
    memo1.Lines.Add('Local IP: ' + p3.BoundIP);
    memo1.Lines.Add('Bound Port: ' + IntToStr(p3.BoundPort));
    label6.Caption:= 'Connected';
  except
    on E:Exception do
      label6.Caption:= e.Message;
  end;
end;

procedure TForm1.BtnGetHeaderClick(Sender: TObject);
var
  msgID : integer;
  msg   : TIdMessage;
begin
  try
    msgID:= edMsgIdx.Value;
    msg:=  TIdMessage.Create(nil);
    if p3.Retrieve(msgID, msg) then
    begin
      Memo1.Text:= msg.Headers.Text;
    end
    else
      label6.Caption:= 'Cannot retrieve message';
  except
    on E:Exception do
      label6.Caption:= e.message;
  end;
  msg.Free;
end;

procedure TForm1.BtnGetMessageClick(Sender: TObject);
var
  msgID : integer;
  msg   : TIdMessage;
  parts: integer;
  txt  : TidText;
  part : TidMessagePart;
begin
  try
    msgID:= edMsgIdx.Value;
    msg:=  TIdMessage.Create(nil);
    if p3.Retrieve(msgID, msg) then
    begin
      label8.Caption:= msg.ContentType;
      if msg.ContentType = 'text/plain' then
        Memo1.Lines.Assign(msg.Body)
      else
      begin
        part:= msg.MessageParts[0];
        txt:= TidText(part.Collection.Items[0]);
        memo1.Lines.Assign(txt.Body);
      end;
    end
    else
      label6.Caption:= 'Cannot retrieve message';
  except
    on E:Exception do
      label6.Caption:= e.message;
  end;
  msg.Free;
end;

procedure TForm1.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  if p3.Connected then
    p3.Disconnect;
end;

procedure TForm1.BtnDisconnectClick(Sender: TObject);
begin
  p3.Disconnect;
end;

procedure TForm1.BtnGetAllSubjectsClick(Sender: TObject);
var
  msgs: integer;
  k: Integer;
  msg: TidMessage;
begin
  msgs:= p3.CheckMessages;
  msg:= TidMessage.Create(nil);
  try
    memo1.Clear;
    for k:= 1 to msgs do
    begin
      p3.RetrieveHeader(k, msg);
      memo1.Lines.Add(Format('%d-%s: %s', [k, msg.From.Text, msg.Subject]));
    end;
  finally
    msg.Free;
  end;
end;

end.
