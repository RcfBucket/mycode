object Form1: TForm1
  Left = 278
  Top = 116
  Caption = 'Form1'
  ClientHeight = 378
  ClientWidth = 986
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnClose = FormClose
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 16
    Top = 72
    Width = 50
    Height = 13
    Caption = 'Mail Count'
  end
  object Label2: TLabel
    Left = 88
    Top = 72
    Width = 32
    Height = 13
    Caption = 'Label2'
  end
  object Label3: TLabel
    Left = 16
    Top = 100
    Width = 22
    Height = 13
    Caption = 'Host'
  end
  object Label4: TLabel
    Left = 16
    Top = 124
    Width = 22
    Height = 13
    Caption = 'User'
  end
  object Label5: TLabel
    Left = 16
    Top = 148
    Width = 46
    Height = 13
    Caption = 'Password'
  end
  object Label6: TLabel
    Left = 16
    Top = 16
    Width = 32
    Height = 13
    Caption = 'Label6'
  end
  object Label7: TLabel
    Left = 16
    Top = 172
    Width = 53
    Height = 13
    Caption = 'Message #'
  end
  object Label8: TLabel
    Left = 312
    Top = 8
    Width = 32
    Height = 13
    Caption = 'Label8'
  end
  object BtnConnect: TButton
    Left = 16
    Top = 288
    Width = 75
    Height = 25
    Caption = 'Connect'
    TabOrder = 0
    OnClick = BtnConnectClick
  end
  object edHost: TEdit
    Left = 88
    Top = 96
    Width = 209
    Height = 21
    TabOrder = 1
    Text = 'pop.surewest.net'
  end
  object edUser: TEdit
    Left = 88
    Top = 120
    Width = 209
    Height = 21
    TabOrder = 2
    Text = 'deejaydog@surewest.net'
  end
  object edPassword: TEdit
    Left = 88
    Top = 144
    Width = 209
    Height = 21
    TabOrder = 3
    Text = 'sottis2@'
  end
  object Memo1: TMemo
    Left = 312
    Top = 24
    Width = 657
    Height = 337
    Lines.Strings = (
      'Memo1')
    ScrollBars = ssBoth
    TabOrder = 4
  end
  object BtnGetHeader: TButton
    Left = 88
    Top = 200
    Width = 129
    Height = 25
    Caption = 'Get Message Header'
    TabOrder = 5
    OnClick = BtnGetHeaderClick
  end
  object BtnDisconnect: TButton
    Left = 104
    Top = 288
    Width = 75
    Height = 25
    Caption = 'Disconnect'
    TabOrder = 6
    OnClick = BtnDisconnectClick
  end
  object BtnGetAllSubjects: TButton
    Left = 16
    Top = 328
    Width = 129
    Height = 25
    Caption = 'Get Message Subject'
    TabOrder = 7
    OnClick = BtnGetAllSubjectsClick
  end
  object BtnGetMessage: TButton
    Left = 88
    Top = 232
    Width = 129
    Height = 25
    Caption = 'Get Message'
    TabOrder = 8
    OnClick = BtnGetMessageClick
  end
  object edMsgIdx: TSpinEdit
    Left = 88
    Top = 168
    Width = 121
    Height = 22
    MaxValue = 0
    MinValue = 0
    TabOrder = 9
    Value = 1
  end
  object p3: TIdPOP3
    AutoLogin = True
    SASLMechanisms = <>
    Left = 680
  end
end
