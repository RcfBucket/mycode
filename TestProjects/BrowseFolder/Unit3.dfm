object Form3: TForm3
  Left = 0
  Top = 0
  Caption = 'Form3'
  ClientHeight = 292
  ClientWidth = 554
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 64
    Top = 104
    Width = 31
    Height = 13
    Caption = 'Label1'
  end
  object Button1: TButton
    Left = 32
    Top = 64
    Width = 75
    Height = 25
    Caption = 'Button1'
    TabOrder = 0
    OnClick = Button1Click
  end
  object Button2: TButton
    Left = 272
    Top = 64
    Width = 75
    Height = 25
    Caption = 'Button2'
    TabOrder = 1
    OnClick = Button2Click
  end
  object Edit1: TEdit
    Left = 32
    Top = 16
    Width = 297
    Height = 21
    TabOrder = 2
    Text = 'E:\LabPro4.0'
  end
  object BrowseFolder1: TBrowseFolder
    Folder = 'E:\Code'
    Title = 'My Custom Title'
    Flags = [bfFileSysDirsOnly, bfStatusText]
    ShowPathInStatusArea = True
    CustomButtonVisible = True
    CustomButtonCaption = 'Hello'
    SyncCustomButton = False
    Left = 72
    Top = 152
  end
end
