unit Unit3;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, BrowseFolder, StdCtrls, ShellAPI, ShlObj;

type
  TForm3 = class(TForm)
    Button1: TButton;
    BrowseFolder1: TBrowseFolder;
    Label1: TLabel;
    Button2: TButton;
    Edit1: TEdit;
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form3: TForm3;

implementation

{$R *.dfm}

var
  myPath: string;

procedure BrowserCBProc(HWindow: HWND; uMsg: Integer; lParameter: LPARAM; lpBrowseFolder: LPARAM); stdcall;
var
  Instance: TBrowseFolder;
  Path: String;
  ValidPath: Boolean;
  TempPath: array[0..MAX_PATH] of Char; // To avoid some odd problems I've encountered casting strings as PChar's.  Sometimes, cast values do not contain the correct text
begin
  // Get the current instance from the lpData pointer that was saved in
  // the Execute method
//  Instance:=TBrowseFolder(lpBrowseFolder);

  // Process the standard dialog messages
  case uMsg of
    BFFM_INITIALIZED:
      begin
        SendMessage(HWindow, BFFM_SETSELECTION, Ord(true), Longint(PChar(myPath)));
(*        Instance.DialogHandle:=HWindow;
        CenterWindow(HWindow); {Added by Manuel Duarte}

        // Save the pointer to the BrowseFolder instance with the window
        SetWindowLong(HWindow,GWL_USERDATA,lpBrowseFolder);

        // Set the initial path
        if DirectoryExists(Instance.Folder) then
          Instance.SetSelectionPath(HWindow, Instance.Folder);

        // Show the custom button
        if Instance.CustomButtonVisible then
          AddCustomControls(HWindow,Instance);

         // Fire the user event
        if Assigned(Instance.OnInitialized) then
          Instance.OnInitialized(Instance,HWindow);*)
      end;

    BFFM_SELCHANGED:
      begin
        // Set the folder path
        SHGetPathFromIDList(PItemIDList(lParameter), TempPath);

        // Allow the user to read the currently selected path from the
        // OnSelectionChange event
//        myPath:= StrPas(TempPath);
//        Instance.FSelectedFolder:=StrPas(TempPath);
        SendMessage(HWindow, BFFM_SETSTATUSTEXT, 0, Longint(PChar(myPath)));

(*        if Instance.ShowPathInStatusArea then
        begin
          SetLength(Path,MAX_PATH);
          Path:= CompressString(StrPas(TempPath), '\', '...', MAX_PATH_DISPLAY_LENGTH);
          SendMessage(HWindow, BFFM_SETSTATUSTEXT, 0, Longint(PChar(Path)));
        end;*)

        // {Added by Atoh Tanner}
        // here is bux-fix to the problem, when you want to enable OK
        // button only on FileSysDirs. But by default while selecting
        // computer name in Network neighbourhood, then it enables OK, but
        // returns nothing. This hack disables OK button if not valid
        // item is selected. I didn't spend time on global FFlags,
        // so it manages with button every time. I always want only filesysdirs ;)
        //if not (bfFileSysDirsOnly in FFlags) then
//        ValidPath:=SHGetPathFromIDList(PItemIDList(lParameter), TempPath);
//        Instance.EnableOK(HWIndow,ValidPath);
//
//         Fire the user event
//        if Assigned(Instance.OnSelectionChanged) then
//          Instance.OnSelectionChanged(Instance,HWindow,PItemIDList(lParameter),Instance.SelectedFolder);
      end;
  end;
end;

function PromptForFolder(hndl: THandle; const Title: string; var aPath: string; const Flag: integer): boolean;
// see BIF_XXX constants under the SHBrowseForFolder topic for flag values
var
  lpItemID: PItemIDList;
  BrowseInfo: TBrowseInfo;
  DisplayName: array [0 .. MAX_PATH] of char;
  TempPath: array [0 .. MAX_PATH] of char;
begin
  myPath:= aPath;
  result:= false;
  FillChar(BrowseInfo, sizeof(TBrowseInfo), #0);
  with BrowseInfo do
  begin
    hwndOwner:= hndl;
    StrCopy(DisplayName, PChar(aPath));
    pszDisplayName:= @DisplayName;
    lpszTitle:= PChar(Title);
    ulFlags:= Flag;
    lpfn:= @BrowserCBProc;
  end;
  lpItemID:= SHBrowseForFolder(BrowseInfo);
  if lpItemID <> nil then
  begin
    SHGetPathFromIDList(lpItemID, TempPath);
    aPath:= TempPath;
    GlobalFreePtr(lpItemID);
    result:= true;
  end;
end;


procedure TForm3.Button1Click(Sender: TObject);
var
  tstr: string;
begin
  BrowseFolder1.Folder:= edit1.Text;
  if BrowseFolder1.Execute then
    label1.Caption:= BrowseFolder1.Folder;

end;

procedure TForm3.Button2Click(Sender: TObject);
var
  tstr: string;
begin
  tstr:= Edit1.Text;
  if PromptForFolder(handle, 'My Folder Browser', tstr, BIF_NEWDIALOGSTYLE) then
    label1.Caption:= tstr;

end;

end.
