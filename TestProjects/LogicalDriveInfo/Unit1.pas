unit Unit1;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, StStrL;

type
  TForm1 = class(TForm)
    Label1: TLabel;
    Button1: TButton;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    procedure Button1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

{$R *.DFM}

function GetRemovableDrive : String;
var
  LogicalDriveMap : Longint;     // holds bitmap representing valid drives.
  idx : integer;                 // loop counter.
  tempvar : pChar;
begin
  LogicalDriveMap:= GetLogicalDrives;  // Returns a bitmap that represents drives available. Bit 0=A, 1=B, etc.
  for idx:= 2 to 25 do  // start with drive letter 'C'.
  begin
    if (LogicalDriveMap AND $01) > 0 then  // We could compare the first bit like this or check to see if # is ODD.
    begin
      tempvar:= PChar(Format('%s:\',[Chr(65+idx)]));
      If (GetDriveType(tempvar) = DRIVE_REMOVABLE) then
      begin
        GetRemovableDrive:= Chr(65+idx);
        Exit;
      end;
    end;
    LogicalDriveMap:=LogicalDriveMap shr 1;  // Look at the next bit.
  end;
  GetRemovableDrive:= 'C';  // default, since removable was not found.
end;  // GetRemovableDrive

procedure TForm1.Button1Click(Sender: TObject);
var
  drives: DWORD;
begin
  drives:= GetLogicalDrives;
  label1.caption:= BinaryLL(drives);
  label4.caption:= GetRemovableDrive;
end;

end.
