﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Text.RegularExpressions;

namespace RegexTest
{
  public partial class Form1 : Form
  {
    public Form1()
    {
      InitializeComponent();
    }

    private void btnGo_Click(object sender, EventArgs e)
    {
      Regex re = new Regex(textBox1.Text, RegexOptions.Multiline | RegexOptions.IgnoreCase);
      MatchCollection mc = re.Matches(textBox2.Text);

      int start = (checkBox1.Checked ? 1 : 0);

      edData.Text = string.Format("Matches found: {0}\r\n", mc.Count);
      for (int mck = 0; mck < mc.Count; mck++)
      {
        edData.Text += string.Format("Match {0}  \"{1}\"\r\n", mck, mc[mck].Value);
        GroupCollection gc = mc[mck].Groups;
        edData.Text += string.Format("  Groups found: {0}\r\n", gc.Count);
        for (int gck = start; gck < gc.Count; gck++)
        {
          edData.Text += string.Format("  Group {0}  \"{1}\"\r\n", gck, gc[gck], gc[gck].Value);

          CaptureCollection cc = gc[gck].Captures;
          edData.Text += string.Format("    Captures found: {0}\r\n", cc.Count);
          for (int cck = 0; cck < cc.Count; cck++)
          {
            edData.Text += string.Format("    Capture {0}  \"{1}\"\r\n", cck, cc[cck], cc[cck].Value);
          }

        }
      }
    }
  }
}
