// Keep the GUID's and Interfaces in seperate file for use between the COM
// server and the COM client.  If you use the COM Server in the uses clause
// of the COM client then the server is built into the client exe instead of
// its own dll.
unit interfaces;

interface

uses ActiveX;

const
  // Use CoCreateGUID() to get a unique ID programmatically
  CLSID_Nubin : TGUID = '{772C5F00-740C-11D2-B6E9-00609767757E}';

type
  // Have to use WideString(easier) or POleStr
  // Always return HResult, if windows has unexpected error it fills the
  // return variable with the error code.  See Windows.pas for codes.
  INubin = interface(IUnknown)
    ['{D76702B0-7410-11D2-B6E9-00609767757E}']
    function Nubit(out aNubName : WideString): HResult; stdcall;
  end;

implementation

end.
 