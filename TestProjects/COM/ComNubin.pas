unit ComNubin;

interface

uses ComObj, Interfaces, ActiveX, Windows, ComServ;

type
  TFactory = class(TComObjectFactory)
  end;

  TComNubin = class(TComObject, INubin)
  public
    {INubin}
    procedure Initialize; override;
    function Nubit(out aNubName : WideString): HResult; stdcall;
  private
    mNubName : string;
  end;

implementation

// Common to all TComObject children, first thing that is called in the
// COM object since YOU don't call create method to create it.
procedure TComNubin.Initialize;
begin
  mNubName := 'Nubin it';
end;

function TComNubin.Nubit(out aNubName : WideString): HResult;
begin
  aNubName := mNubName;
  Result := S_OK;  // Good to go, use E_FAIL for general failure
end;

initialization
  TFactory.Create(ComServer, TComNubin, CLSID_Nubin, 'NubinClass', 'Nubin Description', ciMultiInstance);

end.
