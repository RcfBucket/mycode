unit Unit1;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Interfaces, ComObj;

type
  TForm1 = class(TForm)
    Button1: TButton;
    Button2: TButton;
    Edit1: TEdit;
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

{$R *.DFM}

procedure TForm1.Button1Click(Sender: TObject);
var
  Nubin      : INubin;
  res        : HResult;
  strNubName : WideString;
begin
  Nubin := CreateComObject(CLSID_Nubin) as INubin;
  if Assigned(Nubin) then
  begin
    res := Nubin.Nubit(strNubName);
    if Succeeded(res) then
      Edit1.Text := strNubName
    else
      Edit1.Text := Format('%s: %x', ['Error', res]);
  end;
end;

procedure TForm1.Button2Click(Sender: TObject);
begin
  Edit1.Text := '';
end;

end.
