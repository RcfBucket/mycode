{
  File:       TaskIco.pas
  Function:   Component to show an icon in the taskbar notification area (beside the clock icon)
  Source:     none
  Language:   Delphi 2.0
  Author:     Rudolph Velthuis (rvelthuis@bigfoot.de)
  Style:      Borland
  Copyright:  (c) 1997 drs. Rudolph Velthuis
  Disclaimer: This code is freeware. All rights are reserved.
              This code is provided as is, expressly without a warranty of any kind.
              You use it at your own risk.
              If you use this code, please credit me.
}


unit TaskIco;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ShellAPI;

type
  TTaskbarIcon = class(TComponent)
  private
    FEnabled: Boolean;
    FHint: string;
    FIcon: TIcon;
    FHandle: HWnd;
    FOnClick: TNotifyEvent;
    FOnDblClick: TNotifyEvent;
    FOnRightClick: TNotifyEvent;
    procedure SetEnabled(Value: Boolean);
    procedure SetHint(Value: string);
    procedure SetIcon(Value: TIcon);
    procedure PrivateWndProc(var Message: TMessage);
  protected
    function AddIcon: Boolean; dynamic;
    function DeleteIcon: Boolean; dynamic;
    function ModifyIcon(Aspect: Integer): Boolean; dynamic;
    procedure WndProc(var Message: TMessage); dynamic;
    property Handle: HWnd read FHandle;
  public
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
  published
    property Enabled: Boolean read FEnabled write SetEnabled;
    property Hint: string read FHint write SetHint;
    property Icon: TIcon read FIcon write SetIcon;
    property OnClick: TNotifyEvent read FOnClick write FOnClick;
    property OnDblClick: TNotifyEvent read FOnDblClick write FOnDblClick;
    property OnRightClick: TNotifyEvent read FOnRightClick write FOnRightClick;
  end;

procedure Register;

implementation

const
  WM_TASKICON = WM_USER;

constructor TTaskbarIcon.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
  FHandle := AllocateHWnd(PrivateWndProc);
  FIcon := TIcon.Create;
end;

destructor TTaskbarIcon.Destroy;
begin
  Enabled := False;
  FIcon.Free;
  if FHandle <> 0 then
    DeallocateHwnd(FHandle);
  inherited Destroy;
end;

procedure TTaskbarIcon.PrivateWndProc(var Message: TMessage);
begin
  WndProc(Message);
end;

procedure TTaskbarIcon.SetEnabled(Value: Boolean);
begin
  if Value <> FEnabled then
  begin
    FEnabled := Value;
    if Value then
      AddIcon
    else
      DeleteIcon;
  end;
end;

procedure TTaskbarIcon.SetHint(Value: string);
begin
  FHint := Value;
  ModifyIcon(NIF_TIP);
end;

procedure TTaskbarIcon.SetIcon(Value: TIcon);
begin
  FIcon.Assign(Value);
  ModifyIcon(NIF_ICON);
end;

function TTaskbarIcon.AddIcon: Boolean;
var
  Data: TNotifyIconData;
begin
  with Data do
  begin
    cbSize := SizeOf(TNotifyIconData);
    wnd := FHandle;
    uID := 0;
    uFlags := NIF_ICON or NIF_MESSAGE or NIF_TIP;
    uCallbackMessage := WM_TASKICON;
    if FIcon.Handle <> 0 then
      hIcon := FIcon.Handle
    else
      hIcon := LoadIcon(0, IDI_WINLOGO);
    StrLCopy(szTip, PChar(FHint), 63);
  end;
  Result := Shell_NotifyIcon(NIM_ADD, @Data);
end;

function TTaskbarIcon.ModifyIcon(Aspect: Integer): Boolean;
var
  Data: TNotifyIconData;
begin
  if FEnabled then
  begin
    with Data do
    begin
      cbSize := SizeOf(TNotifyIconData);
      wnd := FHandle;
      uID := 0;
      uFlags := Aspect or NIF_MESSAGE;
      uCallbackMessage := WM_TASKICON;
      if Aspect and NIF_ICON <> 0 then
        if FIcon.Handle <> 0 then
          hIcon := FIcon.Handle
        else
          hIcon := LoadIcon(0, IDI_WINLOGO);
      if Aspect and NIF_TIP <> 0 then
        StrLCopy(szTip, PChar(FHint), 63);
    end;
    Result := Shell_NotifyIcon(NIM_MODIFY, @Data);
  end
  else
    Result := False;
end;

function TTaskbarIcon.DeleteIcon: Boolean;
var
  Data: TNotifyIconData;
begin
  with Data do
  begin
    cbSize := SizeOf(TNotifyIconData);
    wnd := FHandle;
    uID := 0;
  end;
  Result := Shell_NotifyIcon(NIM_DELETE, @Data);
end;

procedure TTaskbarIcon.WndProc(var Message: TMessage);
begin
  with Message do
  begin
    if Msg = WM_TASKICON then
    case LParam of
      WM_LBUTTONUP:
        if Assigned(FOnClick) then FOnClick(Self);
      WM_LBUTTONDBLCLK:
        if Assigned(FOnDblClick) then FOnDblClick(Self);
      WM_RBUTTONUP:
        if Assigned(FOnRightClick) then FOnRightClick(Self);
    end
    else
      Result := DefWindowProc(Handle, Msg, WParam, LParam);
  end;
end;

procedure Register;
begin
  RegisterComponents('Win32', [TTaskbarIcon]);
end;

end.
