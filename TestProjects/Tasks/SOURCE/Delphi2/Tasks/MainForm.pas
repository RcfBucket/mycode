unit MainForm;

{
   1. Set the Icon of the TaskbarIcon component.
   2. Change the strings in AboutItemClick to your gusto.
   3. Change the menu items.
   4. Add your code.
}

interface

uses Windows, SysUtils, Classes, Graphics, Forms, Controls, StdCtrls,
  Buttons, ExtCtrls, Menus, ShellAPI, OLE2, TaskIco;

type
  TTaskbarMainForm = class(TForm)
    LeftPopupMenu: TPopupMenu;
    RightPopupMenu: TPopupMenu;
    CloseItem: TMenuItem;
    ExitItem: TMenuItem;
    SeparatorItem: TMenuItem;
    AboutItem: TMenuItem;
    HelpItem: TMenuItem;
    TaskbarIcon: TTaskbarIcon;
    procedure TaskbarIconClick(Sender: TObject);
    procedure TaskbarIconRightClick(Sender: TObject);
    procedure ExitItemClick(Sender: TObject);
    procedure AboutItemClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    procedure AppMinimize(Sender: TObject);
  public
    { Public declarations }
  end;

var
  TaskbarMainForm: TTaskbarMainForm;

implementation

{$R *.DFM}

procedure TTaskbarMainForm.ExitItemClick(Sender: TObject);
begin
  Close;
end;

procedure TTaskbarMainForm.TaskbarIconClick(Sender: TObject);
var
  Pos: TPoint;
begin
  GetCursorPos(Pos);
  LeftPopupMenu.Popup(Pos.X, Pos.Y);
end;

procedure TTaskbarMainForm.AboutItemClick(Sender: TObject);
var
  Title, Copyright: PChar;
begin
  Title := CoTaskMemAlloc(255);
  Copyright := CoTaskMemAlloc(255);
  try
    StrLCopy(Title, 'Taskbar Application#Application Name', 255);
    StrLCopy(Copyright, 'Taskbar Application � 1997 Your Name', 255);
    ShellAbout(0, Title, Copyright, LoadIcon(HInstance, 'MainIcon'));
  finally
    CoTaskMemFree(Copyright);
    CoTaskMemFree(Title);
  end;
end;

procedure TTaskBarMainForm.AppMinimize(Sender: TObject);
begin
  ShowWindow(Application.Handle, SW_HIDE);
end;

procedure TTaskbarMainForm.FormCreate(Sender: TObject);
begin
  Application.OnMinimize := AppMinimize;
  TaskbarIcon.Enabled := True;
end;

procedure TTaskbarMainForm.TaskbarIconRightClick(Sender: TObject);
var
  Pos: TPoint;
begin
  GetCursorPos(Pos);
  RightPopupMenu.Popup(Pos.X, Pos.Y);
end;

end.
