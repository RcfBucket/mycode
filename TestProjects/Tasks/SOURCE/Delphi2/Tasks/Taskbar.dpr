program Taskbar;

uses
  Forms,
  Windows,
  MainForm in 'MainForm.pas' {TaskbarMainForm};

{$R *.RES}

var
  Mutex: THandle;

// Generate mutex name.
function GetPrevInstMutexName: string;
var
  WindowBuf: array[0..MAX_PATH] of Char; // Local Buffer
begin
  // Get Window Text.
  GetWindowText(Application.Handle, WindowBuf, SizeOf(WindowBuf));

  // Add Previnst identifier.
  Result := 'PREVINST:' + WindowBuf;
end;

begin
  // Create a mutex on this app.
  Mutex := CreateMutex(nil, True, PChar(GetPrevInstMutexName));

  // If succes we are NOT running.
  if (Mutex <> 0) and (GetLastError = 0) then
  begin
    // Init.
    Application.Initialize;
    Application.CreateForm(TTaskbarMainForm, TaskbarMainForm);
    Application.Minimize;

    // Handle messages until we are done.
    repeat
      Application.HandleMessage;
    until Application.Terminated;

    // If we created a mutex, loose it.
    if Mutex <> 0 then CloseHandle(Mutex);
  end
end.

