unit proveform;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls, ComCtrls, UxTheme, Buttons;

const
  WM_BUFGIX = WM_APP + 1;

type
  TForm1 = class(TForm)
    Button1: TButton;
    ListBox1: TListBox;
    Edit1: TEdit;
    Button2: TButton;
    Button3: TButton;
    Button4: TButton;
    Button5: TButton;
    OpenDialog1: TOpenDialog;
    Button6: TButton;
    Button7: TButton;
    Shape1: TShape;
    mbox: TButton;
    TreeView1: TTreeView;
    Label1: TLabel;
    Label2: TLabel;
    BitBtn1: TBitBtn;
    SpeedButton1: TSpeedButton;
    procedure Button1Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure Button3Click(Sender: TObject);
    procedure Button4Click(Sender: TObject);
    procedure Button5Click(Sender: TObject);
    procedure Button6Click(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure Button7Click(Sender: TObject);
    procedure mboxClick(Sender: TObject);
  private
    timer: TTimer;
  public
    procedure FixUp(Sender: TObject);
    procedure CreateParams (var params: TCreateParams ); override;
    procedure WMSyscommand (var msg: TWmSysCommand);
      message WM_SYSCOMMAND;
    procedure WmBugfix (var msg: TMessage); message WM_BUFGIX;
  end;

var
  Form1: TForm1;

implementation

uses
  VistaUtils, modalForm, uVistaFuncs;

{$R *.dfm}

{$R WinManifest.res}

function MessageBox(hWnd: HWND; lpText, lpCaption: PChar; uType: UINT): Integer;
begin
  Result := TaskDialog(Application.MainForm, Application.title, lpCaption, lpText,
    uType, TD_ICON_INFORMATION);
end;

procedure TForm1.Button1Click(Sender: TObject);
begin
  ListBox1.items.add (IntToStr(
    lobyte(loword (GetVersion))));
end;

procedure TForm1.WmBugfix(var msg: TMessage);
var
  i: Integer;
begin
    for i := 0 to ControlCount - 1 do
    begin
      Controls[i].Invalidate; // Refresh;
    end;
end;

procedure TForm1.WMSyscommand(Var msg: TWmSysCommand); 
  begin 
    case (msg.cmdtype and $FFF0) of 
      SC_MINIMIZE: Begin 
          ShowWindow( handle, SW_MINIMIZE ); 
          msg.result := 0; 
        end;   
      SC_RESTORE: Begin 
          ShowWindow( handle, SW_RESTORE ); 
          msg.result := 0; 
        end;   
      else 
        inherited;
    end;
  end;

procedure TForm1.Button2Click(Sender: TObject);
begin
  with tForm2.Create (nil) do
  begin
    // PopupParent := self;
    ShowModal;
    Free;
  end;
end;

procedure TForm1.Button3Click(Sender: TObject);
begin
  with tForm2.Create (nil) do
  begin
    // PopupParent := self;
    Show;
  end;
end;

procedure TForm1.Button4Click(Sender: TObject);
begin
  // TaskMessage (self, 'messaggio per l''utente');
 if TaskDialog(self, 'Hello world','Ready to enjoy the new Vista task dialog ?',
   'The new Vista task dialog presents an easy to use and user-friendly replacement for messageboxes.',
   TD_YES + TD_NO, TD_ICON_QUESTION) = mrYes then
     TaskMessage(self,'yes');
end;

procedure TForm1.Button5Click(Sender: TObject);
begin
  OpenDialog1.Execute;
end;

procedure TForm1.Button6Click(Sender: TObject);
begin
  ListBox1.Items.SaveToFile('c:\test.txt');
end;

procedure TForm1.CreateParams(var params: TCreateParams);
begin
  inherited CreateParams( params );
  params.ExStyle := params.ExStyle or WS_EX_APPWINDOW;
end;

procedure TForm1.FixUp(Sender: TObject);
var
  i: Integer;
begin
    for i := 0 to ControlCount - 1 do
    begin
      Controls[i].Refresh;
    end;
  timer.Enabled := False;
  timer.Free;  
end;

procedure TForm1.FormCreate(Sender: TObject);
begin
  if IsVista then
  begin
    Font.Name := 'Segoe UI';
    Font.Size := 9;
  end;

  ShowWindow( Application.handle, SW_HIDE );
  SetWindowLong( Application.handle,
                 GWL_EXSTYLE,
                 GetWindowLong( application.handle, GWL_EXSTYLE ) and
                   not WS_EX_APPWINDOW or WS_EX_TOOLWINDOW);
  ShowWindow( Application.handle, SW_SHOW );

  if IsVista then
    SetWindowTheme(TreeView1.Handle, 'explorer', nil);
end;

procedure TForm1.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
  var
  i: Integer;
begin
  if (Key = VK_MENU) then
  begin
    for i := 0 to ControlCount - 1 do
    begin
      Controls[i].Invalidate; // Refresh;
    end;
  end;

    // PostMessage (Handle, WM_BUFGIX, 0, 0);
//  begin
//    timer := TTimer.Create(self);
//    timer.OnTimer := FixUp;
//    timer.Interval := 10;
//    timer.Enabled := True;
//  end;
end;

procedure TForm1.Button7Click(Sender: TObject);
begin
  Button7.DoubleBuffered := True;
  if CompositingEnabled then
    ExtendGlass(Handle, Rect(0, 0, 0, 40));
end;

procedure TForm1.mboxClick(Sender: TObject);
begin
  MessageBox(Handle, 'do you want to go?', 'caption', TD_YES or TD_NO);
  // Application.MessageBox('testo', 'caption');
end;

end.
