object Form1: TForm1
  Left = 208
  Top = 169
  Width = 479
  Height = 381
  Caption = 'Delphi7Vista'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  OnCreate = FormCreate
  OnKeyDown = FormKeyDown
  DesignSize = (
    463
    345)
  PixelsPerInch = 96
  TextHeight = 13
  object Shape1: TShape
    Left = 0
    Top = 305
    Width = 465
    Height = 84
    Anchors = []
    Brush.Color = clBlack
  end
  object Label1: TLabel
    Left = 24
    Top = 64
    Width = 31
    Height = 13
    Caption = 'L&abel1'
  end
  object Label2: TLabel
    Left = 312
    Top = 8
    Width = 22
    Height = 13
    Caption = '&Tree'
  end
  object SpeedButton1: TSpeedButton
    Left = 336
    Top = 240
    Width = 73
    Height = 22
    Caption = 'asdasd'
  end
  object Button1: TButton
    Left = 40
    Top = 16
    Width = 75
    Height = 25
    Caption = '&Button1'
    TabOrder = 0
    OnClick = Button1Click
  end
  object ListBox1: TListBox
    Left = 168
    Top = 24
    Width = 121
    Height = 77
    ItemHeight = 13
    TabOrder = 1
  end
  object Edit1: TEdit
    Left = 24
    Top = 80
    Width = 121
    Height = 21
    TabOrder = 2
    Text = 'Edit1'
  end
  object Button2: TButton
    Left = 24
    Top = 128
    Width = 75
    Height = 25
    Caption = '&dialog'
    TabOrder = 3
    OnClick = Button2Click
  end
  object Button3: TButton
    Left = 200
    Top = 128
    Width = 75
    Height = 25
    Caption = 'non modal'
    TabOrder = 4
    OnClick = Button3Click
  end
  object Button4: TButton
    Left = 32
    Top = 192
    Width = 75
    Height = 25
    Caption = 'task'
    TabOrder = 5
    OnClick = Button4Click
  end
  object Button5: TButton
    Left = 40
    Top = 232
    Width = 75
    Height = 25
    Caption = 'comm dlg'
    TabOrder = 6
    OnClick = Button5Click
  end
  object Button6: TButton
    Left = 192
    Top = 192
    Width = 75
    Height = 25
    Caption = 'file'
    TabOrder = 7
    OnClick = Button6Click
  end
  object Button7: TButton
    Left = 280
    Top = 312
    Width = 75
    Height = 25
    Caption = 'Glass'
    TabOrder = 8
    OnClick = Button7Click
  end
  object mbox: TButton
    Left = 192
    Top = 240
    Width = 75
    Height = 25
    Caption = 'mbox'
    TabOrder = 9
    OnClick = mboxClick
  end
  object TreeView1: TTreeView
    Left = 312
    Top = 40
    Width = 121
    Height = 161
    Indent = 19
    TabOrder = 10
    Items.Data = {
      05000000230000000000000000000000FFFFFFFFFFFFFFFF0000000000000000
      0A73666473646673667366210000000000000000000000FFFFFFFFFFFFFFFF00
      000000020000000873667364667364661F0000000000000000000000FFFFFFFF
      FFFFFFFF0000000000000000067364667364661F0000000000000000000000FF
      FFFFFFFFFFFFFF00000000000000000673646673646620000000000000000000
      0000FFFFFFFFFFFFFFFF000000000200000007736673646664661F0000000000
      000000000000FFFFFFFFFFFFFFFF0000000002000000067364667364661E0000
      000000000000000000FFFFFFFFFFFFFFFF00000000010000000573667364661F
      0000000000000000000000FFFFFFFFFFFFFFFF00000000000000000673646673
      64661F0000000000000000000000FFFFFFFFFFFFFFFF00000000000000000673
      64667364661F0000000000000000000000FFFFFFFFFFFFFFFF00000000000000
      00067364667364661F0000000000000000000000FFFFFFFFFFFFFFFF00000000
      0000000006736466736466200000000000000000000000FFFFFFFFFFFFFFFF00
      000000000000000764667364667364}
  end
  object BitBtn1: TBitBtn
    Left = 336
    Top = 208
    Width = 75
    Height = 25
    Caption = 'BitBtn1'
    TabOrder = 11
  end
  object OpenDialog1: TOpenDialog
    Left = 144
    Top = 128
  end
end
