unit ServiceMain;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, SvcMgr, Dialogs,
  IdBaseComponent, IdComponent, IdCustomTCPServer, IdTCPServer, IdCmdTCPServer;

type
  TBobsDelphiService = class(TService)
    IdCmdTCPServer1: TIdCmdTCPServer;
    procedure ServiceStart(Sender: TService; var Started: Boolean);
    procedure ServiceStop(Sender: TService; var Stopped: Boolean);
    procedure ServiceExecute(Sender: TService);
  private
    { Private declarations }
  public
    function GetServiceController: TServiceController; override;
    { Public declarations }
  end;

var
  BobsDelphiService: TBobsDelphiService;

implementation

{$R *.DFM}

procedure ServiceController(CtrlCode: DWord); stdcall;
begin
  BobsDelphiService.Controller(CtrlCode);
end;

function TBobsDelphiService.GetServiceController: TServiceController;
begin
  Result := ServiceController;
end;

procedure TBobsDelphiService.ServiceExecute(Sender: TService);
const
  SecBetweenRuns = 10;
var
  Count: Integer;
begin
  Count := 0;
  while not Terminated do
  begin
    Inc(Count);
    if Count >= SecBetweenRuns then
    begin
      Count := 0;

//      { place your service code here }
//      { this is where the action happens }
//      SomeProcedureInAnotherUnit;

    end;
    Sleep(1000);
    ServiceThread.ProcessRequests(False);
  end;
end;

procedure TBobsDelphiService.ServiceStart(Sender: TService; var Started: Boolean);
var
  s : TStringList;
begin
  s:= TStringList.Create;
  s.LoadFromFile('E:\TestFile.txt');
  s.Add('---');
  s.Add('From Delphi: STARTED - ' + DateTimeToStr(Now));
  s.SaveToFile('E:\TestFile.txt');
  s.Free;
end;

procedure TBobsDelphiService.ServiceStop(Sender: TService; var Stopped: Boolean);
var
  s : TStringList;
begin
  s:= TStringList.Create;
  s.LoadFromFile('E:\TestFile.txt');
  s.Add('---');
  s.Add('From Delphi: STOPPED - ' + DateTimeToStr(Now));
  s.SaveToFile('E:\TestFile.txt');
  s.Free;
end;

end.
