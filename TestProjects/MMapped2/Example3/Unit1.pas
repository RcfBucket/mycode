unit Unit1;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, SharedMem, SharedObjects;

type
  TForm1 = class(TForm)
    btnSet: TButton;
    Edit1: TEdit;
    Edit2: TEdit;
    Label1: TLabel;
    Label2: TLabel;
    procedure btnSetClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    si: TSharedInt;
    ss: TSharedString;
    procedure siOnChange(Sender: TObject);
    procedure ssOnChange(Sender: TObject);
  end;

var
  Form1: TForm1;

implementation

{$R *.DFM}

procedure TForm1.FormCreate(Sender: TObject);
begin
  si := TSharedInt.Create('SharedInt', 1997); // Create the shared memory.
  si.OnChange := siOnChange;                  // Assign its OnChange event.
  siOnChange(si);                             // Call the OnChange event. (for the first time)
  ss := TSharedString.Create('SharedString', 50, 'Hello world');
  ss.OnChange := ssOnChange;
  ssOnChange(ss);
end;

procedure TForm1.btnSetClick(Sender: TObject);
begin
  if ss.Value <> Edit1.Text then
    ss.Value := Edit1.Text;
  if si.Value <> StrToInt(Edit2.Text) then
    si.Value := StrToInt(Edit2.Text);
end;

procedure TForm1.FormDestroy(Sender: TObject);
begin
  ss.Free;
  si.Free;
end;

procedure TForm1.ssOnChange(Sender: TObject);
begin
  TSharedMem(Sender).Lock;
  try
    Edit1.Text := TSharedString(Sender).Value;
  finally
    TSharedMem(Sender).Unlock;
  end;
end;

procedure TForm1.siOnChange(Sender: TObject);
begin
  TSharedMem(Sender).Lock;
  try
    Edit2.Text := IntToStr(TSharedInt(Sender).Value);
  finally
    TSharedMem(Sender).Unlock;
  end;
end;

end.

