{ Shared Memory unit, used in conjunction with SharedObjects.pas
}
unit SharedMem;

INTERFACE

uses
  Windows, SysUtils, Classes;

type
  // We want to "publish" the InternalThreadCallback parameter, but in general
  // it should not be available. This allows objects to explicitly call this
  // procedure, but it doesn't interfere with the intended meanings below.
  TSharedMemPrivateInterface = class(TObject)
  public
    function InternalThreadCallback: integer; virtual; abstract;
  end;

  TSharedMem = class(TSharedMemPrivateInterface)
  private
    // Thread critical section ("read" the local buffer)
    FName         : string;   // The name of the shared memory

    // When assigned, FOnChanged is called whenever the global memory changes.
    FOnChange     : TNotifyEvent;

    // The address of the shared buffer, the local mapping of the MMF.
    FSharedBuffer : PChar;

    FSize         : integer;  // size of the piece of memory
    hEvent        : THandle;  // Event (write the memory)
    hMap          : THandle;  // Handle to the MMF
    hMutex        : THandle;  // Mutex (critical access to shared mem)
    hThreadID     : THandle;  // Thread ID
    hThread       : THandle;  // Thread that responds to write events

    // InternalThreadCallback is to be called whenever the hEvent is broadcast.
    // It calls FOnChange, if assigned.
    function InternalThreadCallback: Integer; override;
  protected
    property Name : String read FName;
    property OnChange: TNotifyEvent read FOnChange write FOnChange;
    property Size : Integer read FSize;
    { GetValue and SetValue are the interface functions
      to the underlying shared memory, and they are the only functions
      that should be used for accessing the MMF. }
    function GetValue: PChar;
    procedure SetValue(Value: PChar; CountBytes: Integer); virtual;
  public
    constructor Create(AName: String; ASize: Integer; InitVal: Pointer; InitValLength: Integer); virtual;
    destructor Destroy; override;

    // Wrapper functions for locking/unlocking the local buffer.
    // They call EnterCriticalSection and LeaveCriticalSection, respectively.
    procedure Lock;    // Locks the local copy
    procedure Unlock;  // Unlocks the local copy
  end;

implementation

{ A windows thread as defined using Delphi's BeginThread
  can only call a function of type
    function(Parameter: Pointer): Integer;
  In other words, the callback cannot be "of object".
  So, LocalThreadFunc will call the object's (as referenced by Parameter).
  "InternalThreadCallback" function. }
function LocalThreadFunc(Parameter: Pointer): Integer;
begin
  result := TSharedMemPrivateInterface(Parameter).InternalThreadCallback;
end;

{ TSharedMem }

constructor TSharedMem.Create(AName: String; ASize: Integer; InitVal: Pointer;
                                     InitValLength: Integer);
var
  MapError: Integer;
begin
  { Make sure everything is zero, so that if any errors occur, I can
    undo whatever was done. }
  hEvent := 0;
  hMutex := 0;
  hMap   := 0;
  FSharedBuffer := nil;
  // Set the name, size and initialize the OnChange event.
  FName := AName;
  FSize := ASize;
  FOnChange := nil;
  try
    // Create the event that responds when a shared variable has been written.
    // Note that this is a "manually" reset event (arg 2).
    hEvent := CreateEvent(nil, True, False, PChar(FName + 'Event'));
    if (hEvent = 0) then
      raise Exception.Create('Cannot create event. Error #' +
                                   IntToStr(GetLastError) + '.');
    // Create a mutex for mutually exclusive access to my shared mem.
    hMutex := CreateMutex(nil, False, PChar(FName + 'Mutex'));
    if (hMutex = 0) then
      raise Exception.Create('Cannot create mutex. Error #' +
                                   IntToStr(GetLastError) + '.');
    // Make sure that NO ONE is touching shared memory during this section of
    // code.
    WaitForSingleObject(hMutex, INFINITE);
    try
      // Begin a nice easy thread to respond when SharedPChar is written.
      // The thread starts by calling LocalThreadFunc with "Self" as a
      // parameter, and we get a handle to the thread in hThread.
      hThread:= BeginThread(nil, 0, LocalThreadFunc, Self, 0, hThreadID);
      // Create the file mapping.
      hMap:= CreateFileMapping($FFFFFFFF, nil, PAGE_READWRITE, 0, FSize, PChar(FName));
      // Determine the last error (if any)
      if hMap = 0 then
        raise Exception.Create('Unable to create shared memory. Error #' +
                                     IntToStr(GetLastError) + '.')
      else
        // Everything was fine and just remember the "error" result.
        // Because if this is the first instantiation of the shared file, then
        // we'll want to initialize its value.
        MapError:= GetLastError();
      // Map a view of the memory mapped file.
      FSharedBuffer:= MapViewOfFile(hMap, FILE_MAP_READ + FILE_MAP_WRITE, 0, 0, 0);
      // If the MMF was already created, just update the local buffer; otherwise,
      // initialize the entry with the initial value specified in InitVal.
      if MapError <> ERROR_ALREADY_EXISTS then
        SetValue(InitVal, InitValLength);
    finally
      ReleaseMutex(hMutex); // Because I requested initial ownership of it.
    end;
  except
    on E: Exception do begin
      if FSharedBuffer <> nil then
        UnmapViewOfFile(FSharedBuffer);
      if hMap <> 0 then
        CloseHandle(hMap);
      if hThread <> 0 then
        CloseHandle(hThread);
      if hMutex <> 0 then
        CloseHandle(hMutex);
      if hEvent <> 0 then
        CloseHandle(hEvent);
    end;
  end;
end;

destructor TSharedMem.Destroy;
begin
  CloseHandle(hThread);           // Kill the thread that responds to the write-event.
  CloseHandle(hMutex);            // Close the mutex.
  CloseHandle(hEvent);            // Close the event.
  UnmapViewOfFile(FSharedBuffer); // unmap the shared buffer
  CloseHandle(hMap);              // close the MMF
  inherited;                      // call the inherited destroy method.
end;

function TSharedMem.GetValue: PChar;
var
  i: Integer;
begin
  // Lock the shared buffer before returning it.
  // The caller MUST call Unlock.
  Lock;
  result:= fSharedBuffer;
end;

function TSharedMem.InternalThreadCallback: Integer;
var
  i, len: Integer;
begin
  while True do
  begin
    // When the thread is first entered it should make sure that its
    // local buffer is up-to-date.
    // Wait to be notified that your shared memory has been written.
    WaitForSingleObject(hEvent, INFINITE);
    if Assigned(FOnChange) then
    begin
      Lock;
      try
        fOnChange(Self);
      finally
        Unlock;
      end;
    end;
  end;
  result:= 0;
end;

procedure TSharedMem.Lock;
begin
  WaitForSingleObject(hMutex, INFINITE);
end;

procedure TSharedMem.SetValue(Value: PChar; CountBytes: Integer);
var
  i: Integer;
begin
  Lock;                                  // Get mutex access to MMF
  if (CountBytes > FSize) then           // Guarantee that the write
    CountBytes:= FSize;                 // process doesn't go over its boundaries.
  for i:= 0 to CountBytes - 1 do         // Now copy the data in Value to
    fSharedBuffer[i]:= Value[i];        // FSharedBuffer
  PulseEvent(hEvent);                    // Tell everyone that the MMF has changed.
  Unlock;                                // Release the mutex.
end;

procedure TSharedMem.Unlock;
begin
  ReleaseMutex(hMutex);
end;

end.

