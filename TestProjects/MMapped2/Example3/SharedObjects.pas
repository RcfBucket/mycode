{ SharedObjects
    provides two classes
      TSharedString and
      TSharedInteger
    both based off of the TSharedMem class
    declared in SharedMem.pas.

  To use these files, place them both in a directory in your
  Library path (_Tools, _Options, Library).

  copyright (c) 1997
  Gregory Deatz
  Hoagland, Longo, Moran, Dunst & Doukas

  This product is provided as-is, with no express or implied warranties of any
  kind. It can be used freely by anyone, provided that this copyright
  statement remains, and that resulting products are not then sold for profit. }
unit SharedObjects;

interface

uses
  SharedMem, StdCtrls, SysUtils;

type
  PInteger = ^Integer; // Just because I want to declare PInteger.

  { TCustomSharedString - define the "custom" interface. }
  TCustomSharedString = class(TSharedMem)
  private
    // GetString returns a copy of the buffer, so the buffer
    // is implicity unlocked after calling.
    function GetString: String;
    procedure SetString(Value: String);
  protected
    property Name;
    property Size;
    property Value: String read GetString write SetString;
  public
    constructor Create(AName: String; ASize: Integer; InitVal: String);
  end;

  { TSharedString - what the developer uses. }
  TSharedString = class(TCustomSharedString)
  public
    property Name;
    property OnChange;
    property Size;
    property Value;
  end;

  { TCustomSharedInt - define the "custom" interface. }
  TCustomSharedInt = class(TSharedMem)
  private
    function GetInt: Integer;
    procedure SetInt(Value: Integer);
  protected
    property Value: Integer read GetInt write SetInt;
  public
    constructor Create(AName: String; InitVal: Integer);
  end;

  { TSharedInt - what the developer uses. }
  TSharedInt = class(TCustomSharedInt)
  public
    property Value;
    property Name;
    property OnChange;
    property Size;
  end;

implementation

{ TCustomSharedString }

constructor TCustomSharedString.Create(AName: String; ASize: Integer; InitVal: String);
begin
  inherited Create(AName, ASize, PChar(Copy(InitVal, 1, ASize - 1)), Length(InitVal) + 1);
end;

function TCustomSharedString.GetString: String;
begin
  result := String(PChar(GetValue));
  Unlock;
end;

procedure TCustomSharedString.SetString(Value: String);
begin
  // Remember, SetValue already locks/unlocks the buffer, so
  // I just have to call it
  SetValue(PChar(Copy(Value, 1, Size - 1)), Length(Value) + 1);
end;

{ TCustomSharedInt }

constructor TCustomSharedInt.Create(AName: String; InitVal: Integer);
begin
  inherited Create(AName, SizeOf(Integer), PChar(@InitVal),
                   SizeOf(Integer));
end;

function TCustomSharedInt.GetInt: Integer;
begin
  result := PInteger(GetValue)^;
  Unlock;
end;

procedure TCustomSharedInt.SetInt(Value: Integer);
begin
  SetValue(PChar(@Value), Size);
end;

end.
