unit Unit1;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls;

type
  TForm1 = class(TForm)
    btnRefresh: TButton;
    btnSet: TButton;
    Edit1: TEdit;
    Label1: TLabel;
    procedure btnRefreshClick(Sender: TObject);
    procedure btnSetClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    hFileMap: THandle;
    MapError: Integer;
    SharedPChar: PChar
  end;

const
  share_len = 20;

var
  Form1: TForm1;

implementation

{$R *.DFM}

procedure TForm1.btnRefreshClick(Sender: TObject);
begin
  Edit1.Text := SharedPChar;
end;

procedure TForm1.btnSetClick(Sender: TObject);
begin
  StrPLCopy(SharedPChar, Edit1.Text, share_len);
end;

procedure TForm1.FormCreate(Sender: TObject);
begin
  hFileMap := 0;
  SharedPChar := nil;
  try
    // Create the file mapping.
    hFileMap := CreateFileMapping(
                                  $FFFFFFFF,  // Memory is backed by paging file
                                  nil,        // Default security
                                  PAGE_READWRITE,  // Read and write it
                                  0,
                                  share_len + 1,  // Allocate shared_len chars + null terminator for shared string
                                  'MyMemoryMappedFile' // It's name.
                                 );
    // Determine the last error (if any)
    if hFileMap = 0 then
      raise Exception.Create('Unable to create shared memory. Error #' +
                                     IntToStr(GetLastError) + '.')
    else
      // Everything was fine and just remember the "error" result.
      // Because if this is the first instantiation of the shared file, then
      // we'll want to initialize its value.
      MapError := GetLastError();
    // Map a view of the memory mapped file.
    SharedPChar := PChar(MapViewOfFile(
                           hFileMap, // The handle to the file map
                           FILE_MAP_READ + FILE_MAP_WRITE, // read and write.
                           0, // Don't specify the offset
                           0, // ...
                           0  // map the "entire" file
                           ));
    if MapError <> ERROR_ALREADY_EXISTS then
      StrPLCopy(SharedPChar, 'Hello world', share_len);
    Edit1.Text := SharedPChar;
  except
    on E: Exception do begin
      if SharedPChar <> nil then
        UnmapViewOfFile(SharedPChar);
      if hFileMap <> 0 then
        CloseHandle(hFileMap);
    end;
  end;
end;

procedure TForm1.FormDestroy(Sender: TObject);
begin
  UnmapViewOfFile(SharedPChar);
  CloseHandle(hFileMap);
end;

end.
