unit Unit1;
{
  Need to download the full FASTMM4 from ...
  get FASTMM4 options editor from
    http://jedqc.blogspot.com/2007/07/new-fastmm4-options-interface.html
  Make use of the Logging ability in FastMM4, 
}

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, JPEG, StdCtrls;

type
  TForm1 = class(TForm)
    Button1: TButton;
    procedure FormShow(Sender: TObject);
    procedure Button1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

{$R *.dfm}

procedure TForm1.Button1Click(Sender: TObject);
begin
  ReportMemoryLeaksOnShutdown:= true;

end;

procedure TForm1.FormShow(Sender: TObject);
var
  ms  : TMemoryStream;
begin
  ms:= TMemoryStream.Create;
  ms.LoadFromFile(Application.ExeName);
  
  RegisterExpectedMemoryLeak(ms); // if you have a known leak and cant fix
                                  // tell memmngr to not tell us about it

end;

end.
