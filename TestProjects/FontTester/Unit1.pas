unit Unit1;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, ExtCtrls;

type
  TForm1 = class(TForm)
    Label1: TLabel;
    Label2: TLabel;
    Label13: TLabel;
    edHeight: TEdit;
    edWidth: TEdit;
    rdoWeight: TRadioGroup;
    Panel1: TPanel;
    edSample: TEdit;
    Label14: TLabel;
    Label15: TLabel;
    Label16: TLabel;
    Label17: TLabel;
    Label18: TLabel;
    Label19: TLabel;
    Label20: TLabel;
    Label21: TLabel;
    Label22: TLabel;
    Label23: TLabel;
    Label24: TLabel;
    Label25: TLabel;
    Label26: TLabel;
    chkItalic: TCheckBox;
    chkStrike: TCheckBox;
    chkUnderline: TCheckBox;
    rdoCharset: TRadioGroup;
    rdoPitch: TRadioGroup;
    rdoFamily: TRadioGroup;
    cboFonts: TComboBox;
    Bevel1: TBevel;
    Label5: TLabel;
    Button1: TButton;
    procedure Button1Click(Sender: TObject);
    procedure edSampleChange(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    function CreateTheFont: TLogFont;
    procedure ShowResults;
    function CharSetName(aCharSet: integer): string;
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

{$R *.DFM}

function TForm1.CreateTheFont: TLogFont;
var
  lf  : TLogFont;
  dc  : THandle;
  k   : integer;
  f   : HFont;

  function GetCharset: integer;
  begin
    case rdoCharSet.itemindex of
      0: result:= DEFAULT_CHARSET;
      1: result:= ANSI_CHARSET;
      2: result:= SYMBOL_CHARSET;      3: result:= SHIFTJIS_CHARSET;      4: result:= GB2312_CHARSET;      5: result:= HANGEUL_CHARSET;      6: result:= CHINESEBIG5_CHARSET;      7: result:= OEM_CHARSET;
      8: result:= JOHAB_CHARSET;
      9: result:= HEBREW_CHARSET;      10: result:= ARABIC_CHARSET;      11: result:= GREEK_CHARSET;      12: result:= TURKISH_CHARSET;      13: result:= THAI_CHARSET;      14: result:= EASTEUROPE_CHARSET;      15: result:= RUSSIAN_CHARSET;      16: result:= MAC_CHARSET;      17: result:= BALTIC_CHARSET;
    else
      result:= DEFAULT_CHARSET;
    end;
  end;

  function GetPitch: integer;
  begin
    case rdoPitch.itemindex of
      0 : result:= DEFAULT_PITCH;
      1 : result:= FIXED_PITCH;
      2 : result:= VARIABLE_PITCH;
    else
      result:= DEFAULT_PITCH;
    end;
  end;

  function GetFamily: integer;
  begin
    case rdoFamily.ItemIndex of
      0: result:= FF_DECORATIVE;
      1: result:= FF_DONTCARE;
      2: result:= FF_MODERN;
      3: result:= FF_ROMAN;
      4: result:= FF_SCRIPT;
      5: result:= FF_SWISS;
    else
      result:= FF_DONTCARE;
    end;
  end;

  function GetWeight: integer;
  begin
    case rdoWeight.itemindex of
      1: result:= FW_THIN;
      2: result:= FW_THIN;
      3: result:= FW_EXTRALIGHT;
      4: result:= FW_ULTRALIGHT;
      5: result:= FW_LIGHT;
      6: result:= FW_NORMAL;
      7: result:= FW_REGULAR;
      8: result:= FW_MEDIUM;
      9: result:= FW_SEMIBOLD;
      10: result:= FW_DEMIBOLD;
      11: result:= FW_BOLD;
      12: result:= FW_EXTRABOLD;
      13: result:= FW_ULTRABOLD;
      14: result:= FW_HEAVY;
      15: result:= FW_BLACK;
    else
      result:= FW_DONTCARE;
    end;
  end;

begin
{
   LONG lfHeight;
   LONG lfWidth;
   LONG lfEscapement;
   LONG lfOrientation;
   LONG lfWeight;
   BYTE lfItalic;
   BYTE lfUnderline;
   BYTE lfStrikeOut;
   BYTE lfCharSet;
   BYTE lfOutPrecision;
   BYTE lfClipPrecision;
   BYTE lfQuality;
   BYTE lfPitchAndFamily;
   TCHAR lfFaceName[LF_FACESIZE];
}
  FillChar(lf, sizeof(lf), 0);

  lf.lfWeight:= GetWeight;
  lf.lfItalic:= byte(chkItalic.Checked);
  lf.lfUnderline:= byte(chkUnderline.Checked);
  lf.lfStrikeout:= byte(chkStrike.Checked);
  if edWidth.text <> '' then
    lf.lfWidth:= StrToInt(edWidth.text);

  lf.lfPitchAndFamily:= GetPitch or GetFamily;
  lf.lfcharSet:= GetCharSet;
  if cboFonts.ItemIndex > 0 then
    StrCopy(lf.lfFaceName, PChar(cboFonts.text));

  {- get the face name windows assigns to the font }
  dc:= GetDC(0);
  if edHeight.text <> '' then
  begin
    k:= StrToInt(edHeight.text);
    lf.lfHeight:= -MulDiv(k, GetDeviceCaps(dc, LOGPIXELSY), 72);
  end;

  f:= CreateFontIndirect(lf);
  SelectObject(dc, f);
  GetTextFace(dc, lf_FaceSize, lf.lfFaceName);

  ReleaseDC(0, dc);
  DeleteObject(f);
  result:= lf;
end;

procedure TForm1.Button1Click(Sender: TObject);
var
  lf  : TLogFont;
begin
  lf:= CreateTheFont;
  panel1.font.Handle:= CreateFontIndirect(lf);
  panel1.Refresh;
  ShowResults;             
end;

procedure TForm1.edSampleChange(Sender: TObject);
begin
  panel1.caption:= ' ' + edSample.Text;
end;

procedure TForm1.FormShow(Sender: TObject);
begin
  edSample.OnChange(self);
  cboFonts.items.Assign(screen.Fonts);
  cboFonts.items.Insert(0, '<dont care>');
  Button1.Click;
end;

procedure TForm1.ShowResults;
var
  tstr  : string;
begin
  with panel1.font do
  begin
    label20.caption:= Format('%d - %s', [CharSet, CharSetName(charSet)]);
    label21.Caption:= Name;
    case pitch of
      fpDefault : label23.caption:= 'Default';
      fpFixed: label23.caption:= 'Fixed';
      fpVariable: label23.caption:= 'Variable';
    else
      label23.caption:= 'Unknown';
    end;
    label22.caption:= IntToStr(size);
    label25.caption:= IntToStr(height);
    tstr:= '';
    if fsBold in style then
      tstr:= tstr + 'Bold, ';
    if fsItalic in style then
      tstr:= tstr + 'Italic, ';
    if fsUnderline in style then
      tstr:= tstr + 'Underline, ';
    if fsStrikeout in style then
      tstr:= tstr + 'Strikeout, ';
    if tstr = '' then
      tstr:= 'None';
    tstr:= Trim(tstr);
    if (length(tstr) > 0) and (tstr[length(tstr)] = ',') then
      Delete(tstr, Length(tstr), 1);
    label24.caption:= tstr;
  end;
end;

function TForm1.CharSetName(aCharSet: integer): string;
begin
  case aCharSet of
    DEFAULT_CHARSET: result:= 'DEFAULT_CHARSET';
    ANSI_CHARSET: result:= 'ANSI_CHARSET';
    SYMBOL_CHARSET: result:= 'SYMBOL_CHARSET';    SHIFTJIS_CHARSET: result:= 'SHIFTJIS_CHARSET';    GB2312_CHARSET: result:= 'GB2312_CHARSET';    HANGEUL_CHARSET: result:= 'HANGEUL_CHARSET';    CHINESEBIG5_CHARSET: result:= 'CHINESEBIG5_CHARSET';    OEM_CHARSET: result:= 'OEM_CHARSET';
    JOHAB_CHARSET: result:= 'JOHAB_CHARSET';
    HEBREW_CHARSET: result:= 'HEBREW_CHARSET';    ARABIC_CHARSET: result:= 'ARABIC_CHARSET';    GREEK_CHARSET: result:= 'GREEK_CHARSET';    TURKISH_CHARSET: result:= 'TURKISH_CHARSET';    THAI_CHARSET: result:= 'THAI_CHARSET';    EASTEUROPE_CHARSET: result:= 'EASTEUROPE_CHARSET';    RUSSIAN_CHARSET: result:= 'RUSSIAN_CHARSET';    MAC_CHARSET: result:= 'MAC_CHARSET';    BALTIC_CHARSET: result:= 'BALTIC_CHARSET';
  else
    result:= 'Unknkown';
  end;
end;

end.
