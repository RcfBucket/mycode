unit DebugClient;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Interfaces;

type
  TForm1 = class(TForm)
    Button1: TButton;
    procedure Button1Click(Sender: TObject);
  private
    { Private declarations }
    mDebugTest : IDebugTest;
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

{$R *.DFM}

uses ActiveX, ComObj;

procedure TForm1.Button1Click(Sender: TObject);
var
  ptrTemp : POleStr;
  tmp     : IDebugTest;
begin
  tmp := CreateComObject(Class_DebugTest) as IDebugTest;
  if Assigned(tmp) and Succeeded(tmp.GetText(ptrTemp)) then
    Caption := OleStrToString(ptrTemp)
  else
    Caption := 'Error';
end;

end.
