unit Interfaces;

interface

uses ActiveX;

const
  Class_DebugTest: TGUID = '{B20050C1-7CD4-11D3-ADFF-00A40080D29C}';

type
  IDebugTest = interface
    ['{BB95CB31-7CDB-11D3-ADFF-00A40080D29C}']
    function GetText(var aString: POleStr): HResult; stdcall;
  end;

implementation

end.
 