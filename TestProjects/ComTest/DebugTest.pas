unit DebugTest;
{ Need to register this DLL using regsvr32
  Under vista, you must be in an elevated (run as admin) command window to register
}

interface

uses
  Windows, ActiveX, Classes, ComObj, Interfaces;

type
  TDebugTest = class(TComObject, IDebugTest)
  protected
    {IDebugTest}
    function GetText(var aString: POleStr): HResult; stdcall;
  end;

implementation

uses ComServ;

{ TDebugTest }

function TDebugTest.GetText(var aString: POleStr): HResult;
begin
  aString := 'DebugTest';
  Result := S_OK;
end;

initialization
  TComObjectFactory.Create(ComServer, TDebugTest, Class_DebugTest,
    'DebugTest', '', ciMultiInstance, tmApartment);
end.
 
