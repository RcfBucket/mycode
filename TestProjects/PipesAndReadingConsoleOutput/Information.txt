borland.publ...hi.win32  

 Groups Alerts 
Create a group...  Recently Visited Groups | Help | Sign in  

borland.public.delphi.language.delphi.win32        

   

Message from discussion Problem with ShellExecute


View parsed - Show only message text

From: "Craig Stuntz [TeamB]" <craig_stu...@nospam.please [a.k.a. acm.org]>
Subject: Re: Problem with ShellExecute
Newsgroups: borland.public.delphi.language.delphi.win32
References: <473b2b69$1@newsgroups.borland.com>
Organization: Vertex Systems Corporation
User-Agent: XanaNews/1.18.1.9
X-Face: "huzh55Xi]!ZNni78>a*5+).`SK?<^q2MlHk-C]Z2M9T@\J5ZCB$95Ey+s"XQuhGFA[eutZ:0^AV!Q;t>gE`1-=8d;HZ6~*o1dH<X9\b$!|(}+I'!Xq)@3Op}%0ArW&nf'Qh*EwR
MIME-Version: 1.0
Content-Type: text/plain; charset=iso-8859-1
Content-Transfer-Encoding: 8bit
NNTP-Posting-Host: 64.199.116.234
Message-ID: <473b2db1@newsgroups.borland.com>
Date: 14 Nov 2007 09:17:37 -0700
X-Trace: newsgroups.borland.com 1195060657 64.199.116.234 (14 Nov 2007 09:17:37 -0700)
Lines: 144
X-Authenticated-User: cstuntz
Path: g2news1.google.com!news3.google.com!feeder1-2.proxad.net!proxad.net!feeder1-1.proxad.net!club-internet.fr!feedme-small.clubint.net!newsfeed.freenet.de!newsfeed00.sul.t-online.de!newsfeed01.sul.t-online.de!t-online.de!newsgroups.borland.com!not-for-mail

John Venture wrote:

> is there a way to make it waiting till the task is done ?

	One way is to capture the console output to a pipe. Then you can
display the output in your app instead of showing the console. Delphi,
for example, does this when it runs a command-line app during a build.

	Here's some code I wrote to validate an IB DB with the command-line
gfix application:

procedure TdmIBMaintenance.ValidateDBWithGfix;
var
  bHandleCloseError: boolean;
  ExitCode: Cardinal;
  sApp, sOutput, sParams: string;
  hReadStdOut, hWriteStdOut, hReadStdErr, hWriteStdErr: THandle;
  PI: TProcessInformation;
  SA: TSecurityAttributes;
  SD: TSecurityDescriptor;
  SI: TStartupInfo;

  procedure InitializeHandles;
  begin
    hReadStdErr := INVALID_HANDLE_VALUE;
    hReadStdOut := INVALID_HANDLE_VALUE;
    hWriteStdErr := INVALID_HANDLE_VALUE;
    hWriteStdOut := INVALID_HANDLE_VALUE;
  end;

  procedure CreateStdPipes;
  begin
    if Win32Platform = VER_PLATFORM_WIN32_NT then begin
      InitializeSecurityDescriptor(@SD, SECURITY_DESCRIPTOR_REVISION);
      SetSecurityDescriptorDacl(@SD, True, nil, False);
      SA.lpSecurityDescriptor := @SD;
    end else begin
      SA.lpSecurityDescriptor := nil;
    end; // if
    SA.nLength := SizeOf(SA);
    SA.bInheritHandle := True;

    if not CreatePipe(hReadStdOut, hWriteStdOut, @SA, 0) then begin
      RaiseLastOSError;
    end; // if
    SetHandleInformation(hReadStdOut, HANDLE_FLAG_INHERIT, 0);
    if not CreatePipe(hReadStdErr, hWriteStdErr, @SA, 0) then begin
      RaiseLastOSError;
    end; // if
    SetHandleInformation(hReadStdErr, HANDLE_FLAG_INHERIT, 0);
  end;

  function ReadFromPipe(const AHandle: THandle): string;
  const
    BYTESTOREAD = 1024;
  var
    BytesRead, LastError: Cardinal;
    // one byte bigger than bytes read for null terminator
    OutputBuffer: array[0..BYTESTOREAD] of Char;
    sChunk: string;
  begin
    repeat
      ZeroMemory(@OutputBuffer, SizeOf(OutputBuffer));
      if ReadFile(AHandle, OutputBuffer, BYTESTOREAD, BytesRead, nil) 
        then begin
        if BytesRead > 0 then begin
          // terminate string
          OutputBuffer[BytesRead] := #0;
          sChunk := StrPas(OutputBuffer);
        end else begin
          sChunk := '';
        end; // if
      end else begin
        LastError := GetLastError;
        if (LastError = ERROR_HANDLE_EOF) or (LastError = 
          ERROR_BROKEN_PIPE) then begin
          // nothing to see here, move along
          sChunk := '';
        end else begin
          // freeze right there, sir
          RaiseLastOSError;
        end;
      end; // if
      Result := Result + sChunk;
    until sChunk = '';
  end;

  procedure FreeHandles(const AFirstHandle, ASecondHandle: THandle);
  begin
    bHandleCloseError := not CloseHandle(AFirstHandle);
    bHandleCloseError := bHandleCloseError or 
      (not CloseHandle(ASecondHandle));
    if bHandleCloseError then begin
      RaiseLastOSError;
    end; // if
  end;

begin
  InitializeHandles;
  sApp := GFixPath;
  sParams := BuildGfixCommandLine;
  CreateStdPipes;
  try
    try
      FillChar(SI, SizeOf(SI), 0);
      SI.cb := SizeOf(SI);
      SI.dwFlags := STARTF_USESTDHANDLES or STARTF_USESHOWWINDOW;
      SI.wShowWindow := SW_HIDE;
      SI.hStdInput := GetStdHandle(STD_INPUT_HANDLE);
      SI.hStdOutput := hWriteStdOut;
      SI.hStdError := hWriteStdErr;
      // display the full command line
      DoOnValidateMessage(sApp + ' ' +
        // ...but obfuscate password in UI display
        StringReplace(sParams,
          '-pass ' + Password, // replace this
          '-pass XXXXXXXX', [])); // with this
      if not CreateProcess(PAnsiChar(sApp), PAnsiChar(sParams), nil,    
        nil, True,
        0, nil, PChar(ExtractFilePath(sApp)), SI, PI) then begin
        RaiseLastOSError;
      end; // if
    finally
      FreeHandles(hWriteStdOut, hWriteStdErr);
    end;
    repeat
      sOutput := sOutput + ReadFromPipe(hReadStdErr);
      sOutput := sOutput + ReadFromPipe(hReadStdOut);
      DoOnValidateMessage(sOutput);
      if not GetExitCodeProcess(PI.hProcess, ExitCode) then begin
        RaiseLastOSError;
      end; // if
    until ExitCode <> STILL_ACTIVE;
  finally
    FreeHandles(PI.hProcess, PI.hThread);
    FreeHandles(hReadStdOut, hReadStdErr);
  end;
end;

-- 
Craig Stuntz [TeamB] � Vertex Systems Corp. � Columbus, OH
  Delphi/InterBase Weblog : http://blogs.teamb.com/craigstuntz
IB 6 versions prior to 6.0.1.6 are pre-release and may corrupt 
  your DBs!  Open Edition users, get 6.0.1.6 from http://mers.com


Create a group - Google Groups - Google Home - Terms of Service - Privacy Policy  
�2008 Google 
