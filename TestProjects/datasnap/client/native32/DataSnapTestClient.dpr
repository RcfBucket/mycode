{*******************************************************}
{                                                       }
{       CodeGear Delphi Visual Component Library        }
{                                                       }
{           Copyright (c) 1995-2008 CodeGear            }
{                                                       }
{*******************************************************}

program DataSnapTestClient;

uses
{$IF DEFINED(DEBUG)}
//  FastMM4,
{$IFEND}
  SysUtils,
  Forms,
  ClientForm in '..\..\..\..\..\src\pas\samples\datasnap\client\ClientForm.pas' {Form13},
  ClientClasses in '..\..\..\..\..\src\pas\samples\datasnap\client\ClientClasses.pas',
  DataSnapTestData in '..\..\..\..\..\src\pas\samples\datasnap\examples\DataSnapTestData.pas';

{$R *.res}

begin
  try
    Application.Initialize;
    Application.MainFormOnTaskbar := True;
    Application.CreateForm(TForm13, Form13);
  Application.Run;
  except on Ex: Exception do
    Application.ShowException(Ex);
  end;
end.
