unit Unit1;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls;

type
  TForm1 = class(TForm)
    Label1: TLabel;
    Label2: TLabel;
    Button1: TButton;
    Memo1: TMemo;
    Memo2: TMemo;
    Label3: TLabel;
    procedure Button1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

{$R *.dfm}

uses
  ShellApi;

function LPFileOp(aSourceFile, aDestinationFile: string; aFileOp: Integer; aFlags: FILEOP_FLAGS = FOF_NOCONFIRMATION { ; aWindow: TWinControl = nil } ): integer;
// Copy, delete, or rename file operation  (FO_MOVE,FO_COPY,FO_DELETE,FO_RENAME)
// to supress the progress dialog, add the FOF_SILENT Flag to Fileop_Flags
// Note: This function is currently designed to support copying single files at a time.  Due to the
// call to SetFileAttributes
var
  FileOpStruct: TSHFileOpStruct;
begin
  try
    // Setup source file
    FileOpStruct.pFrom:= PChar(aSourceFile + #0);

    // Setup destination file
    FileOpStruct.pTo:= PChar(aDestinationFile + #0);
    // if aWindow <> nil then
    // FileOpStruct.Wnd:= aWindow.Handle
    // else
    FileOpStruct.Wnd:= 0;
    FileOpStruct.wFunc:= aFileOp;
    FileOpStruct.fFlags:= aFlags;
    FileOpStruct.hNameMappings:= nil;
    FileOpStruct.lpszProgressTitle:= nil;
    case aFileOp of
      FO_MOVE, FO_COPY, FO_DELETE, FO_RENAME:
        begin
          result:= SHFileOperation(FileOpStruct);
          if (aFileOp = FO_COPY) then
            SetFileAttributes(PChar(aDestinationFile), FILE_ATTRIBUTE_NORMAL);
        end;
    else
      result:= -1;
    end;
  except
    result:= -2;
  end;
end;

procedure DumpIt(srcFiles: string);
var
  list: TFileStream;
begin
  list:= TFileStream.Create('D:\src.txt', fmCreate, fmShareDenyNone);
  list.Write(srcFiles[1], ByteLength(srcFiles));
  list.Free;
end;


procedure TForm1.Button1Click(Sender: TObject);
var
  src, dst: string;
  k: Integer;
  err: integer;
begin
  src:= '';
  dst:= '';
  for k := 0 to memo1.Lines.Count do
  begin
    if (Trim(memo1.Lines[k]) <> '') then
      src:= src + memo1.Lines[k] + #0;
  end;
  for k := 0 to memo2.Lines.Count do
  begin
    if (Trim(memo2.Lines[k]) <> '') then
      dst:= dst + memo2.Lines[k] + #0;
  end;
  src:= src + #0;
  dst:= dst + #0;

  DumpIt(src);

  err:= LPFileOp(src, dst, FO_COPY, FOF_FILESONLY or FOF_MULTIDESTFILES or FOF_NOCONFIRMATION);
  label3.Caption:= Format('Error: %d (%x)', [err, err]);
end;

end.
