{*
Copyright (c) 2007 Cary Jensen. All rights reserved.

This code sample is associated with the presentation
"Building Reusable Data Layers for ASP.NET Applications"
by Cary Jensen, Jensen Data Systems, Inc.

presented at CodeRage 2007, November 26-30, 2007


This code sample is provided for demonstration purposes only

No guarantees or warranties are expressed or implied concerning
the applicability of techniques or code included in this code
sample. If you wish to use techniques or code included in this
code sample, it is your responsibility to test and certify any
code, techniques, or designs adopted as a result of this code sample

For information about training, consulting, or development services
for all versions of Delphi, please visit
http://www.JensenDataSystems.com

*}
unit login;

interface

uses
  System.Collections, System.ComponentModel, SysUtils,
  System.Data, System.Drawing, System.Web, System.Web.SessionState,
  System.Web.UI, System.Web.UI.WebControls, System.Web.UI.HtmlControls,
  System.Web.Security, System.Web.UI.WebControls.WebParts, System.Configuration;

type
  TLogin = class(System.Web.UI.Page)
  {$REGION 'Designer Managed Code'}
  strict private
    procedure InitializeComponent;
    procedure LoginButton_Click(sender: TObject; e: System.EventArgs);
  {$ENDREGION}
  strict private
    procedure Page_Load(sender: System.Object; e: System.EventArgs);
  strict protected
    ErrorMessageLabel: System.Web.UI.WebControls.&Label;
    LoginButton: System.Web.UI.WebControls.Button;
    UserNameTextBox: System.Web.UI.WebControls.TextBox;
  protected
    procedure OnInit(e: EventArgs); override;
  private
    { Private Declarations }
  public
    { Public Declarations }
  end;

implementation

uses JDSI.Modules.SqlDataModule;

{$REGION 'Designer Managed Code'}
/// <summary>
/// Required method for Designer support -- do not modify
/// the contents of this method with the code editor.
/// </summary>
procedure TLogin.InitializeComponent;
begin
  Include(Self.LoginButton.Click, Self.LoginButton_Click);
  Include(Self.Load, Self.Page_Load);
end;
{$ENDREGION}

procedure TLogin.Page_Load(sender: System.Object; e: System.EventArgs);
begin
  // TODO: Put user code to initialize the page here
end;

procedure TLogin.OnInit(e: EventArgs);
begin
  //
  // Required for Designer support
  //
  InitializeComponent;
  inherited OnInit(e);
end;

procedure TLogin.LoginButton_Click(sender: TObject; e: System.EventArgs);
var
  SQLDataModule: TSqlDataModule;
begin
  //prevent SQL injection hack
  if UserNameTextBox.Text.IndexOf(';') > -1 then
  begin
    ErrorMessageLabel.Text := 'Do not include semicolons in your user name';
    Exit;
  end;
  try
    SqlDataModule := TSqlDataModule.Create(Request, Response, Session, Trace);
    if Integer(SqlDataModule.ExecuteScalar('select count(*) from Customers where [CustomerID] = '+
       QuotedStr(UserNameTextBox.Text))) = 0 then
    begin
      ErrorMessageLabel.Text := 'Please enter a valid account number';
    end
    else
    begin
      //Write username to Session
      Session['username'] := UserNameTextBox.Text;
      Response.Redirect('DataForm.aspx?id='+ UserNameTextBox.Text);
    end;
  finally
    SqlDataModule.Dispose;
  end;
end;

end.

