{*
Copyright (c) 2007 Cary Jensen. All rights reserved.

This code sample is associated with the presentation
"Building Abstract Data Layers for ASP.NET Applications"
by Cary Jensen, Jensen Data Systems, Inc.

presented at CodeRage 2007, Tuesday, March 13 2007


This code sample is provided for demonstration purposes only

No guarantees or warranties are expressed or implied concerning
the applicability of techniques or code included in this code
sample. If you wish to use techniques or code included in this
code sample, it is your responsibility to test and certify any
code, techniques, or designs adopted as a result of this code sample

For information about training or consulting services
for all versions of Delphi, please visit
http://www.JensenDataSystems.com


*}
unit JDSI.Modules.OrderObject;

interface

uses
  JDSI.Modules.SqlDataModule, System.Data;

type
  TOrderObject = class(TObject)
  private
    FSqlDataModule: TSqlDataModule;
    FShipName: String;
    FShipAddress: String;
    FShipCity: String;
    FShipRegion: String;
    FShipCountry: String;
    FErrorMessage: String;
    FIsValid: Boolean;
    FChanged: Boolean;
    procedure Set_ShipName(Value: String);
    procedure Set_ShipAddress(Value: String);
    procedure Set_ShipCity(Value: String);
    procedure Set_ShipRegion(Value: String);
    procedure Set_ShipCountry(Value: String);
    procedure BuildErrorMessage(Msg: String); virtual;
    function GetStringField(FieldName: String; DataReader: IDataReader): String;
  public
    constructor Create(SqlDataModule: TSqlDataModule);
    function LoadData(OrderID: String): Boolean;
    procedure Validate;
    procedure ResetValidation;
    procedure ResetChanged;
    procedure SaveData(OrderID: String);
    procedure SaveNew(OrderID: String);
    property Changed: Boolean read FChanged;
    property IsValid: Boolean read FIsValid;
    property ErrorMessage: String read FErrorMessage;
    property ShipName: String read FShipName write Set_ShipName;
    property ShipAddress: String read FShipAddress write Set_ShipAddress;
    property ShipCity: String read FShipCity write Set_ShipCity;
    property ShipRegion: String read FShipRegion write Set_ShipRegion;
    property ShipCountry: String read FShipCountry write Set_ShipCountry;
  end;

implementation

{ TOrderObject }

procedure TOrderObject.BuildErrorMessage(Msg: String);
begin
  FIsValid := False;
  if FErrorMessage = '' then
    FErrorMessage := Msg
  else
    FErrorMessage := FErrorMessage + '<br>' + Msg;
end;

constructor TOrderObject.Create(SqlDataModule: TSqlDataModule);
begin
  inherited create;
  FSqlDataModule := SqlDataModule;
end;

function TOrderObject.LoadData(OrderID: String): Boolean;
var
  DataReader: IDataReader;
begin
  DataReader := FSqlDataModule.ExecuteDataReader('SELECT [ShipName], ' +
    '[ShipAddress], [ShipCity], [ShipRegion], [ShipCountry] ' +
    'FROM [ORDERS] ' +
    'WHERE [OrderID] = ''' + OrderID + '''');
  if not DataReader.Read then
  begin
     FSqlDataModule.Trace.Warn('TOrderOBject.LoadData: empty result set');
     Result := False;
     Exit;
  end
  else
  begin
    FShipName := GetStringField('ShipName', DataReader);
    FShipAddress := GetStringField('ShipAddress', DataReader);
    FShipCity := GetStringField('ShipCity', DataReader);
    FShipRegion := GetStringField('ShipRegion', DataReader);
    FShipCountry := GetStringField('ShipCountry', DataReader);
  end;
end;

procedure TOrderObject.ResetChanged;
begin
  FChanged := False;
end;

procedure TOrderObject.ResetValidation;
begin
  FIsValid := True;
end;

procedure TOrderObject.SaveData(OrderID: String);
var
  Stmt: String;
begin
    Stmt := 'UPDATE [ORDERS] SET [ShipName] = ''' + FShipName + ''', ' +
     ' [ShipAddress] = ''' + FShipAddress + ''', ' +
     ' [ShipCity] = ''' + FShipCity + ''', ' +
     ' [ShipRegion] = ''' + FShipRegion + ''', ' +
     ' [ShipCountry] = ''' + FShipCountry + '''' +
     ' WHERE [OrderID] = ''' + OrderID + '''';
    FSqlDataModule.ExecuteNonQuery(Stmt);
end;

procedure TOrderObject.SaveNew(OrderID: String);
var
  Stmt: String;
begin
    Stmt := 'INSERT INTO [ORDERS] ([OrderID], [ShipName], '+
     '[ShipAddress], [ShipCity], [ShipRegion], [ShipCountry]) ' +
     'VALUES ( ''' + OrderID + ''', ''' + FShipName + ''', ' +
     '''' + FShipAddress + ''', ''' + FShipCity + ''', ' +
     '''' + FShipRegion + ''', ''' + FShipCountry + ''')';
    FSqlDataModule.ExecuteNonQuery(Stmt);
end;

procedure TOrderObject.Set_ShipAddress(Value: String);
begin
if FShipAddress <> Value then
begin
  FShipAddress := Value;
  FChanged := True;
end;
end;

procedure TOrderObject.Set_ShipCity(Value: String);
begin
if FShipCity <> Value then
begin
  FShipCity := Value;
  FChanged := True;
end;
end;

procedure TOrderObject.Set_ShipCountry(Value: String);
begin
if FShipCountry <> Value then
begin
  FShipCountry := Value;
  FChanged := True;
end;
end;

procedure TOrderObject.Set_ShipName(Value: String);
begin
if FShipName <> Value then
begin
  FShipName := Value;
  FChanged := True;
end;
end;

procedure TOrderObject.Set_ShipRegion(Value: String);
begin
if FShipRegion <> Value then
begin
  FShipRegion := Value;
  FChanged := True;
end;
end;

procedure TOrderObject.Validate;
begin
  FIsValid := True;
  if FShipName = '' then
    BuildErrorMessage('ShipName is a required field');
  if FShipAddress = '' then
    BuildErrorMessage('ShipAddress is a required field');
  if FShipCity = '' then
    BuildErrorMessage('ShipCity is a required field');
  if FShipCountry = '' then
    BuildErrorMessage('ShipCountry is a required field');
end;

function TOrderObject.GetStringField(FieldName: String; DataReader: IDataReader): String;
var
  FieldPos: Integer;
begin
  Result := '';
  try
    FieldPos := DataReader.GetOrdinal(FieldName);
    if DataReader.IsDBNull(FieldPos) then
      Result := ''
    else
      Result := DataReader.GetString(FieldPos);
  except
    on e: Exception do
    begin
      FSqlDataModule.Trace.Warn(FieldName + ': ' + e.Message);
    end;
  end;
end;


end.
