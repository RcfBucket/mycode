{*
Copyright (c) 2007 Cary Jensen. All rights reserved.

This code sample is associated with the presentation
"Building Reusable Data Layers for ASP.NET Applications"
by Cary Jensen, Jensen Data Systems, Inc.

presented at CodeRage 2007, November 26-30, 2007


This code sample is provided for demonstration purposes only

No guarantees or warranties are expressed or implied concerning
the applicability of techniques or code included in this code
sample. If you wish to use techniques or code included in this
code sample, it is your responsibility to test and certify any
code, techniques, or designs adopted as a result of this code sample

For information about training, consulting, or development services
for all versions of Delphi, please visit
http://www.JensenDataSystems.com

*}
unit EditOrder;

interface

uses
  System.Collections, System.ComponentModel,
  System.Data, System.Drawing, System.Web, System.Web.SessionState,
  System.Web.UI, System.Web.UI.WebControls, System.Web.UI.HtmlControls,
  System.Web.Security, System.Web.UI.WebControls.WebParts, System.Configuration,
  JDSI.Modules.SqlDataModule,
  JDSI.Modules.OrderObject;

type
  TEditOrder = class(System.Web.UI.Page)
  {$REGION 'Designer Managed Code'}
  strict private
    procedure InitializeComponent;
    procedure TEditOrder_Unload(sender: TObject; e: System.EventArgs);
    procedure CancelButton_Click(sender: TObject; e: System.EventArgs);
    procedure SaveButton_Click(sender: TObject; e: System.EventArgs);
  {$ENDREGION}
  strict private
    procedure Page_Load(sender: System.Object; e: System.EventArgs);
  strict protected
    ErrorMessageLabel: System.Web.UI.WebControls.&Label;
    CancelButton: System.Web.UI.WebControls.Button;
    SaveButton: System.Web.UI.WebControls.Button;
    ShipCountryTextBox: System.Web.UI.WebControls.TextBox;
    ShipRegionTextBox: System.Web.UI.WebControls.TextBox;
    ShipCityTextBox: System.Web.UI.WebControls.TextBox;
    ShipAddressTextBox: System.Web.UI.WebControls.TextBox;
    ShipNameTextBox: System.Web.UI.WebControls.TextBox;
  protected
    procedure OnInit(e: EventArgs); override;
  private
    { Private Declarations }
    SqlDataModule: TSqlDataModule;
    OrderID: String;
    procedure SetErrorMessage(Value: String);
  public
    { Public Declarations }
  end;

implementation

{$REGION 'Designer Managed Code'}
/// <summary>
/// Required method for Designer support -- do not modify
/// the contents of this method with the code editor.
/// </summary>
procedure TEditOrder.InitializeComponent;
begin
  Include(Self.SaveButton.Click, Self.SaveButton_Click);
  Include(Self.CancelButton.Click, Self.CancelButton_Click);
  Include(Self.Unload, Self.TEditOrder_Unload);
  Include(Self.Load, Self.Page_Load);
end;
{$ENDREGION}

procedure TEditOrder.Page_Load(sender: System.Object; e: System.EventArgs);
var
  OrderObject: TOrderObject;
begin
  if not IsPostBack then
  begin
    OrderID := Request.QueryString.Item['orderid'];
    OrderObject := TOrderObject.Create(SqlDataModule);
    OrderObject.LoadData(OrderID);
    ShipNameTextBox.Text := OrderObject.ShipName;
    ShipAddressTextBox.Text := OrderObject.ShipAddress;
    ShipCityTextBox.Text := OrderObject.ShipCity;
    ShipRegionTextBox.Text := OrderObject.ShipRegion;
    ShipCountryTextBox.Text := OrderObject.ShipCountry;
  end;
end;

procedure TEditOrder.SetErrorMessage(Value: String);
begin
  ErrorMessageLabel.Text := Value;
end;

procedure TEditOrder.SaveButton_Click(sender: TObject; e: System.EventArgs);
var
  OrderObject: TOrderObject;
begin
  try
    OrderID := Request.QueryString.Item['orderid'];
    OrderObject := TOrderObject.Create(SqlDataModule);
    OrderObject.ShipName := ShipNameTextBox.Text;
    OrderObject.ShipAddress := ShipAddressTextBox.Text;
    OrderObject.ShipCity := ShipCityTextBox.Text;
    OrderObject.ShipRegion := ShipRegionTextBox.Text;
    OrderObject.ShipCountry := ShipCountryTextBox.Text;
    OrderObject.Validate;
    if not OrderObject.IsValid then
    begin
      SetErrorMessage(OrderObject.ErrorMessage);
      Exit;
    end;
    OrderObject.SaveData(OrderID);
  except
    on e: Exception do
    begin
      SetErrorMessage('Save error: ' + e.Message);
      Exit;
    end;
  end;
  Response.Redirect('DataForm.aspx');
end;

procedure TEditOrder.CancelButton_Click(sender: TObject; e: System.EventArgs);
begin
  Response.Redirect('DataForm.aspx');
end;

procedure TEditOrder.TEditOrder_Unload(sender: TObject; e: System.EventArgs);
begin
  SqlDataModule.Dispose;
end;

procedure TEditOrder.OnInit(e: EventArgs);
begin
  SqlDataModule:= TSqlDataModule.Create(Request, Response, Session, Trace);
  SqlDataModule.VerifyLogin;
  InitializeComponent;
  inherited OnInit(e);
end;

end.

