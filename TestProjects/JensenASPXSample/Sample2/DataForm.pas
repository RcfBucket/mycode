{*
Copyright (c) 2007 Cary Jensen. All rights reserved.

This code sample is associated with the presentation
"Building Reusable Data Layers for ASP.NET Applications"
by Cary Jensen, Jensen Data Systems, Inc.

presented at CodeRage 2007, November 26-30, 2007


This code sample is provided for demonstration purposes only

No guarantees or warranties are expressed or implied concerning
the applicability of techniques or code included in this code
sample. If you wish to use techniques or code included in this
code sample, it is your responsibility to test and certify any
code, techniques, or designs adopted as a result of this code sample

For information about training, consulting, or development services
for all versions of Delphi, please visit
http://www.JensenDataSystems.com

*}
unit DataForm;

interface

uses
  System.Collections, System.ComponentModel,
  System.Data, System.Drawing, System.Web, System.Web.SessionState,
  System.Web.UI, System.Web.UI.WebControls, System.Web.UI.HtmlControls,
  System.Web.Security, System.Web.UI.WebControls.WebParts, System.Configuration,
  System.Data.SqlClient, JDSI.Modules.SqlDataModule;

type
  TDataForm = class(System.Web.UI.Page)
  {$REGION 'Designer Managed Code'}
  strict private
    procedure InitializeComponent;
    procedure GetCustomerOrders_Click(sender: TObject; e: System.EventArgs);
    procedure TDataForm_Unload(sender: TObject; e: System.EventArgs);
  {$ENDREGION}
  strict private
    procedure Page_Load(sender: System.Object; e: System.EventArgs);
  strict protected
    ErrorMessageLabel: System.Web.UI.WebControls.&Label;
    GetCustomerOrders: System.Web.UI.WebControls.Button;
    CustomerDropDown: System.Web.UI.WebControls.DropDownList;
  protected
    procedure OnInit(e: EventArgs); override;
  private
    { Private Declarations }
    SqlDataModule: TSqlDataModule;
  public
    { Public Declarations }
  end;

implementation

{$REGION 'Designer Managed Code'}
/// <summary>
/// Required method for Designer support -- do not modify
/// the contents of this method with the code editor.
/// </summary>
procedure TDataForm.InitializeComponent;
begin
  Include(Self.GetCustomerOrders.Click, Self.GetCustomerOrders_Click);
  Include(Self.Unload, Self.TDataForm_Unload);
  Include(Self.Load, Self.Page_Load);
end;
{$ENDREGION}

procedure TDataForm.Page_Load(sender: System.Object; e: System.EventArgs);
begin
  // TODO: Put user code to initialize the page here
  if not IsPostBack then
  begin
    CustomerDropDown.DataSource := SqlDataModule.ExecuteDataReader('select CustomerID, CompanyName FROM Customers');
    CustomerDropDown.DataTextField := 'CompanyName';
    CustomerDropDown.DataValueField := 'CustomerID';
    DataBind;
    CustomerDropDown.Items.Insert(0, '-- SELECT a Company --');
  end;
end;

procedure TDataForm.OnInit(e: EventArgs);
begin
  // Validation
  SqlDataModule := TSqlDataModule.Create(Request, Response, Session, Trace);
  SqlDataModule.VerifyLogin;
  //
  // Required for Designer support
  //
  InitializeComponent;
  inherited OnInit(e);
end;

procedure TDataForm.TDataForm_Unload(sender: TObject; e: System.EventArgs);
begin
  SqlDataModule.Dispose;
end;

procedure TDataForm.GetCustomerOrders_Click(sender: TObject; e: System.EventArgs);
begin
if CustomerDropDown.SelectedIndex = 0 then
  begin
     ErrorMessageLabel.Text := 'Please select a company';
     Exit;
  end;
  Response.Redirect('CustomerOrders.aspx?cust=' + CustomerDropDown.SelectedValue);
end;

end.

