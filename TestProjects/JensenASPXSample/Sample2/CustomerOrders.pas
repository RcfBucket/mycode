{*
Copyright (c) 2007 Cary Jensen. All rights reserved.

This code sample is associated with the presentation
"Building Reusable Data Layers for ASP.NET Applications"
by Cary Jensen, Jensen Data Systems, Inc.

presented at CodeRage 2007, November 26-30, 2007


This code sample is provided for demonstration purposes only

No guarantees or warranties are expressed or implied concerning
the applicability of techniques or code included in this code
sample. If you wish to use techniques or code included in this
code sample, it is your responsibility to test and certify any
code, techniques, or designs adopted as a result of this code sample

For information about training, consulting, or development services
for all versions of Delphi, please visit
http://www.JensenDataSystems.com

*}
unit CustomerOrders;

interface

uses
  System.Collections, System.ComponentModel,
  System.Data, System.Drawing, System.Web, System.Web.SessionState,
  System.Web.UI, System.Web.UI.WebControls, System.Web.UI.HtmlControls,
  System.Web.Security, System.Web.UI.WebControls.WebParts, System.Configuration,
  JDSI.Modules.SqlDataModule;

type
  TCustomerOrders = class(System.Web.UI.Page)
  {$REGION 'Designer Managed Code'}
  strict private
    procedure InitializeComponent;
    procedure TCustomerOrders_Unload(sender: TObject; e: System.EventArgs);
    procedure DataGrid1_SelectedIndexChanged(sender: TObject; e: System.EventArgs);
  {$ENDREGION}
  strict private
    procedure Page_Load(sender: System.Object; e: System.EventArgs);
  strict protected
    DataGrid1: System.Web.UI.WebControls.DataGrid;
  protected
    procedure OnInit(e: EventArgs); override;
  private
    { Private Declarations }
    SqlDataModule: TSqlDataModule;
    CustID: String;
  public
    { Public Declarations }
  end;

implementation

{$REGION 'Designer Managed Code'}
/// <summary>
/// Required method for Designer support -- do not modify
/// the contents of this method with the code editor.
/// </summary>
procedure TCustomerOrders.InitializeComponent;
begin
  Include(Self.DataGrid1.SelectedIndexChanged, Self.DataGrid1_SelectedIndexChanged);
  Include(Self.Unload, Self.TCustomerOrders_Unload);
  Include(Self.Load, Self.Page_Load);
end;
{$ENDREGION}

procedure TCustomerOrders.Page_Load(sender: System.Object; e: System.EventArgs);
begin
  // TODO: Put user code to initialize the page here
    if not IsPostBack then
    begin
      CustID := Request.QueryString.Item['cust'];    
      DataGrid1.DataSource := SqlDataModule.ExecuteDataTable('select OrderID, EmployeeID, ' +
          'OrderDate, ShippedDate from [orders] where [customerid] = '''+custid+'''');
      DataBind;
    end;
end;

procedure TCustomerOrders.OnInit(e: EventArgs);
begin
  SqlDataModule:= TSqlDataModule.Create(Request, Response, Session, Trace);
  SqlDataModule.VerifyLogin;
  InitializeComponent;
  inherited OnInit(e);
end;

procedure TCustomerOrders.DataGrid1_SelectedIndexChanged(sender: TObject; e: System.EventArgs);
begin

end;

procedure TCustomerOrders.TCustomerOrders_Unload(sender: TObject; e: System.EventArgs);
begin
  SqlDataModule.Dispose;
end;

end.

