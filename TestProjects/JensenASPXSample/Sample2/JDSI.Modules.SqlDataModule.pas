{*
Copyright (c) 2007 Cary Jensen. All rights reserved.

This code sample is associated with the presentation
"Building Reusable Data Layers for ASP.NET Applications"
by Cary Jensen, Jensen Data Systems, Inc.

presented at CodeRage II, November 26-30, 2007


This code sample is provided for demonstration purposes only

No guarantees or warranties are expressed or implied concerning
the applicability of techniques or code included in this code
sample. If you wish to use techniques or code included in this
code sample, it is your responsibility to test and certify any
code, techniques, or designs adopted as a result of this code sample

For information about training, consulting, or development services
for all versions of Delphi, please visit
http://www.JensenDataSystems.com


*}
unit JDSI.Modules.SqlDataModule;

interface

uses System.Data, System.Data.SqlClient, System.Configuration,
  System.Web, System.Web.SessionState, System.Threading,
  System.Data.Common;

type
  TSqlDataModule = class(TObject, IDisposable)
  private
    FConnection: SqlConnection;
    FRequest: HttpRequest;
    FResponse: HttpResponse;
    FSession: HttpSessionState;
    FTrace: TraceContext;
  public
    constructor Create(Request: HttpRequest;
      Response: HttpResponse; Session: HttpSessionState;
      Trace: TraceContext);
    procedure VerifyLogin;
    procedure Dispose;
    function ExecuteNonQuery(Stmt: String): Integer;
    function ExecuteScalar(Stmt: String): TObject;
    function ExecuteDataReader(Stmt: String): SqlDataReader;
    function ExecuteDataTable(Stmt: String): DataTable;
    //The following is a typical domain-related method
    function GetCustomerReader(CustID: String): DbDataReader;
    //
    //Please see comment associated with the GetConnection
    //method for warnings about using this method
    function GetConnection: SqlConnection;
    procedure ReleaseConnection;
    property Request: HttpRequest read FRequest;
    property Response: HttpResponse read FResponse;
    property Session: HttpSessionState read FSession;
    property Trace: TraceContext read FTrace;
  end;

implementation

{ TSqlDataModule }

constructor TSqlDataModule.Create(Request: HttPRequest; Response:
  HttpResponse; Session: HttpSessionState; Trace: TraceContext);
begin
  inherited Create;
  FRequest := Request;
  FResponse := Response;
  FSession := Session;
  FTrace := Trace;
  try
    FConnection := SqlConnection.Create;
    FConnection.ConnectionString :=
      System.Configuration.ConfigurationManager.ConnectionStrings['Northwind'].ConnectionString;
    FConnection.Open;
  except
    on e: Exception do
    begin
      FTrace.Warn('SqlDataModule: Constructor error, ' + e.Message);
    end;
  end;
end;

procedure TSqlDataModule.Dispose;
begin
  FConnection.Close;
  FConnection := nil;
end;

function TSqlDataModule.ExecuteDataReader(Stmt: String): SqlDataReader;
var
  Command: SqlCommand;
begin
  try
    Command := FConnection.CreateCommand;
    Command.CommandText := Stmt;
    Result := Command.ExecuteReader;
  except
    on e: Exception do
    begin
      Trace.Warn('SqlDataModule: ExecuteDataReader error, ' + e.Message);
    end;
  end;
end;

function TSqlDataModule.ExecuteDataTable(Stmt: String): DataTable;
var
  DataAdapter: SqlDataAdapter;
begin
  try
  DataAdapter := SqlDataAdapter.Create(Stmt, FConnection);
  Result := DataTable.Create;
  DataAdapter.Fill(Result);
  except
    on e: Exception do
    begin
      Trace.Warn('SqlDataModule: ExecuteDataTable error, ' + e.Message);
    end;
  end;
end;

function TSqlDataModule.ExecuteNonQuery(Stmt: String): Integer;
var
  Command: SqlCommand;
begin
  try
    Command := FConnection.CreateCommand;
    Command.CommandText := Stmt;
    Result := Command.ExecuteNonQuery;
  except
    on e: Exception do
    begin
      Trace.Warn('SqlDataModule: ExecuteNonQuery error, ' + e.Message);
    end;
  end;
end;

function TSqlDataModule.ExecuteScalar(Stmt: String): TObject;
var
  Command: SqlCommand;
begin
  try
    Command := FConnection.CreateCommand;
    Command.CommandText := Stmt;
    Result := Command.ExecuteScalar;
  except
    on e: Exception do
    begin
      Trace.Warn('SqlDataModule: ExecuteScalar error, ' + e.Message);
    end;
  end;
end;

function TSqlDataModule.GetConnection: SqlConnection;
begin
  Result := nil;
  //Try to enter monitor for only 500 milliseconds
  if not Monitor.TryEnter(Self, 500) then
  begin
    Trace.Warn('Attemping to enter the Monitor already ' +
      'locked on ' + Self.ClassName + '. Check your code ' +
      'for use of ' + Self.ClassName + '.GetConnection without a ' +
      'guaranteed call to ' + Self.ClassName + '.ReleaseConnection');
  end;
  Result := FConnection;
end;

function TSqlDataModule.GetCustomerReader(CustID: String): DbDataReader;
var
  Command: SqlCommand;
begin
  try
    Command := FConnection.CreateCommand;
    Command.CommandText := 'SELECT * FROM Customers ' +
                           'WHERE CustomerID = ''' + CustID + '''';
    Result := Command.ExecuteReader;
  except
    on e: Exception do
    begin
      Trace.Warn('SqlDataModule: ExecuteScalar error, ' + e.Message);
    end;
  end;
end;

procedure TSqlDataModule.ReleaseConnection;
begin
  Monitor.Exit(Self);
end;

procedure TSqlDataModule.VerifyLogin;
var
  s: String;
begin
  if FSession['username'] = nil then
    Response.Redirect('login.aspx');
end;

end.
