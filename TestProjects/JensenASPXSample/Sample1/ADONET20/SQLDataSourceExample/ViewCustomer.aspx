<%@ Page language="c#" Debug="true" Codebehind="ViewCustomer.pas" AutoEventWireup="false" Inherits="ViewCustomer.TViewCustomer" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">

<html>
  <head runat="server">
    <title></title>
  </head>

  <body>
     <form runat="server">
      <p>&nbsp;
</p>
      <p align="center">
        <asp:Label id="IDLabel" runat="server" forecolor="#0000C0"></asp:Label>
      </p>
      <table style="WIDTH: 409px; HEIGHT: 160px" bordercolor="cornflowerblue" cellspacing="1" bordercolordark="#ffff99" cellpadding="1" width="409" align="center" bordercolorlight="royalblue" border="2">
          <tr>
            <td>Company Name
</td>
            <td>
              <asp:TextBox id="CompanyNameTextBox" runat="server"></asp:TextBox></td>
          </tr>
          <tr>
            <td>Contact Name
</td>
            <td>
              <asp:TextBox id="ContactNameTextBox" runat="server"></asp:TextBox></td>
          </tr>
          <tr>
            <td>Address
</td>
            <td>
              <asp:TextBox id="AddressTextBox" runat="server"></asp:TextBox></td>
          </tr>
          <tr>
            <td>City
</td>
            <td>
              <asp:TextBox id="CityTextBox" runat="server"></asp:TextBox></td>
          </tr>
          <tr>
            <td>Country
</td>
            <td>
              <asp:TextBox id="CountryTextBox" runat="server"></asp:TextBox></td>
          </tr>
      </table>
      <div align="center">
        <asp:Button id="Button1" runat="server" text="Return" width="91px"></asp:Button>
      </div>
     </form>
  </body>
</html>
