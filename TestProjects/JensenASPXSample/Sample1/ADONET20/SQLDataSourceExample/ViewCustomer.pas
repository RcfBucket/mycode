
unit ViewCustomer;

interface

uses
  System.Collections, System.ComponentModel,
  System.Data, System.Drawing, System.Web, System.Web.SessionState,
  System.Web.UI, System.Web.UI.WebControls, System.Web.UI.HtmlControls,
  System.Web.Security, System.Web.UI.WebControls.WebParts, System.Configuration, 
 System.Data.SqlClient;

type
  TViewCustomer = class(System.Web.UI.Page)
  {$REGION 'Designer Managed Code'}
  strict private
    procedure InitializeComponent;
  procedure Button1_Click(sender: TObject; e: System.EventArgs);
  {$ENDREGION}
  strict private
    procedure Page_Load(sender: System.Object; e: System.EventArgs);
 strict protected
  SqlConnection1: System.Data.SqlClient.SqlConnection;
  CompanyNameTextBox: System.Web.UI.WebControls.TextBox;
  ContactNameTextBox: System.Web.UI.WebControls.TextBox;
  AddressTextBox: System.Web.UI.WebControls.TextBox;
  CityTextBox: System.Web.UI.WebControls.TextBox;
  CountryTextBox: System.Web.UI.WebControls.TextBox;
  IDLabel: System.Web.UI.WebControls.&Label;
  Button1: System.Web.UI.WebControls.Button;
  protected
    procedure OnInit(e: EventArgs); override;
  private
    { Private Declarations }
  public
    { Public Declarations }
  end;

implementation

{$REGION 'Designer Managed Code'}
/// <summary>
/// Required method for Designer support -- do not modify
/// the contents of this method with the code editor.
/// </summary>
procedure TViewCustomer.InitializeComponent;
begin
 Self.SqlConnection1 := System.Data.SqlClient.SqlConnection.Create;
 // 
 // SqlConnection1
 // 
 Self.SqlConnection1.ConnectionString := 'integrated security=SSPI;data sour' +
 'ce=".\SQLEXPRESS";persist security info=False;initial catalog=Northwind';
 Self.SqlConnection1.FireInfoMessageEventOnUserErrors := False;
 Include(Self.Button1.Click, Self.Button1_Click);
 Include(Self.Load, Self.Page_Load);
end;
{$ENDREGION}

procedure TViewCustomer.Page_Load(sender: System.Object; e: System.EventArgs);
var
  Command1: SqlCommand;
  DataReader1: SqlDataReader;
begin
if not IsPostBack then
begin
   SqlConnection1.Open;
   Command1 := SqlConnection1.CreateCommand;
   Command1.CommandText := 'SELECT * FROM CUSTOMERS WHERE CustomerID = ''' +
                             Request.QueryString['id'].ToString + '''';
   DataReader1 := Command1.ExecuteReader;
   if DataReader1.Read then
   begin
     IdLabel.Text := 'Information for customer ' + DataReader1.GetString(1);
     CompanyNameTextBox.Text := DataReader1.GetString(1);
     ContactNameTextBox.Text := DataReader1.GetString(2);
     AddressTextBox.Text := DataReader1.GetString(3);
     CityTextBox.Text := DataReader1.GetString(4);
     CountryTextBox.Text := DataReader1.GetString(5);
   end;
   DataReader1.Close;
end;
  // TODO: Put user code to initialize the page here
end;

procedure TViewCustomer.OnInit(e: EventArgs);
begin
  //
  // Required for Designer support
  //
  InitializeComponent;
  inherited OnInit(e);
end;

procedure TViewCustomer.Button1_Click(sender: TObject; e: System.EventArgs);
begin
  Response.Redirect('default.aspx');
end;

end.

