<%@ Page language="c#" Debug="true" Codebehind="Default.pas" AutoEventWireup="false" Inherits="Default.TDefault" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">

<html>
  <head runat="server">
    <title></title>
  </head>

  <body>
     <form runat="server">
      <asp:SqlDataSource id="SqlDataSource1" runat="server" connectionstring="<%$ ConnectionStrings:NorthwindConnectionString %>" providername="<%$ ConnectionStrings:NorthwindConnectionString.ProviderName %>"
                         selectcommand='SELECT "CustomerID", "CompanyName", "City", "Region", "Country" FROM "Customers"'></asp:SqlDataSource>
      <div align="center">
      <asp:GridView id="GridView1" runat="server" autogeneratecolumns="False" datasourceid="SqlDataSource1" datakeynames="CustomerID" backcolor="White" bordercolor="#3366CC" borderstyle="None" borderwidth="1px" cellpadding="4">
        <FooterStyle backcolor="#99CCCC" forecolor="#003399"></FooterStyle>
        <Columns>
          <asp:HyperLinkField datatextfield="CustomerID" datanavigateurlformatstring="ViewCustomer.aspx?id={0}" datanavigateurlfields="CustomerID"></asp:HyperLinkField>
          <asp:BoundField datafield="CompanyName" sortexpression="CompanyName" headertext="CompanyName"></asp:BoundField>
          <asp:BoundField datafield="City" sortexpression="City" headertext="City"></asp:BoundField>
          <asp:BoundField datafield="Region" sortexpression="Region" headertext="Region"></asp:BoundField>
          <asp:BoundField datafield="Country" sortexpression="Country" headertext="Country"></asp:BoundField>
        </Columns>
        <RowStyle backcolor="White" forecolor="#003399"></RowStyle>
        <SelectedRowStyle backcolor="#009999" forecolor="#CCFF99" font-bold="True"></SelectedRowStyle>
        <PagerStyle backcolor="#99CCCC" forecolor="#003399" horizontalalign="Left"></PagerStyle>
        <HeaderStyle backcolor="#003399" forecolor="#CCCCFF" font-bold="True"></HeaderStyle>
      </asp:GridView>
      </div>
     </form>
  </body>
</html>
