<%@ Page language="c#" Debug="true" Codebehind="Default.pas" AutoEventWireup="false" Inherits="Default.TDefault" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">

<html>
  <head runat="server">
    <title></title>
  </head>

  <body>
     <form runat="server">
      <p>Click to load DataTable
</p>
      <p>
        <asp:Button id="LoadTableButton" runat="server" text="Load Table" width="152px"></asp:Button>
      </p>
      <p>Click to load DataTableReader from DataTable
</p>
      <p>
        <asp:Button id="UseDataTableReader" runat="server" text="Use DataTableReader" width="152px"></asp:Button>
        <asp:Label id="ErrorMessage" runat="server" forecolor="Red"></asp:Label>
      </p>
      <p>
        <asp:ListBox id="ListBox1" runat="server" width="328px" height="127px"></asp:ListBox>
      </p>
     </form>
  </body>
</html>
