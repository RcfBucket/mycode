
unit Default;

interface

uses
  System.Collections, System.ComponentModel,
  System.Data, System.Drawing, System.Web, System.Web.SessionState,
  System.Web.UI, System.Web.UI.WebControls, System.Web.UI.HtmlControls,
  System.Web.Security, System.Web.UI.WebControls.WebParts, System.Configuration,
  System.Data.SqlClient;

type
  TDefault = class(System.Web.UI.Page)
  {$REGION 'Designer Managed Code'}
  strict private
    procedure InitializeComponent;
  procedure LoadTableButton_Click(sender: TObject; e: System.EventArgs);
  procedure UseDataTableReader_Click(sender: TObject; e: System.EventArgs);
  {$ENDREGION}
  strict private
    procedure Page_Load(sender: System.Object; e: System.EventArgs);
 strict protected
  ListBox1: System.Web.UI.WebControls.ListBox;
  LoadTableButton: System.Web.UI.WebControls.Button;
  UseDataTableReader: System.Web.UI.WebControls.Button;
  ErrorMessage: System.Web.UI.WebControls.&Label;
  protected
    procedure OnInit(e: EventArgs); override;
  private
    { Private Declarations }
  public
    { Public Declarations }
  end;

implementation

{$REGION 'Designer Managed Code'}
/// <summary>
/// Required method for Designer support -- do not modify
/// the contents of this method with the code editor.
/// </summary>
procedure TDefault.InitializeComponent;
begin
 Include(Self.LoadTableButton.Click, Self.LoadTableButton_Click);
 Include(Self.UseDataTableReader.Click, Self.UseDataTableReader_Click);
 Include(Self.Load, Self.Page_Load);
end;
{$ENDREGION}

procedure TDefault.Page_Load(sender: System.Object; e: System.EventArgs);
begin
  // TODO: Put user code to initialize the page here
end;

procedure TDefault.OnInit(e: EventArgs);
begin
  //
  // Required for Designer support
  //
  InitializeComponent;
  inherited OnInit(e);
end;

procedure TDefault.UseDataTableReader_Click(sender: TObject; e: System.EventArgs);
var
  DataTable1: DataTable;
  DataTableReader1: DataTableReader;
begin
  if ViewState['table'] = nil then
    ErrorMessage.Text := 'Load table before continuing...'
  else
  begin
    ErrorMessage.Text := '';
    DataTable1 := DataTable(ViewState['table']);
    DataTableReader1 := DataTableReader.Create(DataTable1);
    while DataTableReader1.Read do
      ListBox1.Items.Add(DataTableReader1.GetString(0) + ' : ' +
        DataTableReader1.GetString(1));
  end;
end;

procedure TDefault.LoadTableButton_Click(sender: TObject; e: System.EventArgs);
var
  SqlConnection1: SQlConnection;
  SqlDataAdapter1: SqlDataAdapter;
  DataTable1: DataTable;
begin
  try
    SqlConnection1 := SqlConnection.Create('Persist Security Info=False;' +
        'Integrated Security=SSPI;database=northwind;' +
        'server=.\SQLEXPRESS;Connect Timeout=30');
    SqlConnection1.Open;
    SqlDataAdapter1 := SqlDataAdapter.Create('SELECT * FROM [Customers] WHERE [City] = ''London''',
      SqlConnection1);
    DataTable1 := DataTable.Create;
    SqlDataAdapter1.Fill(DataTable1);
    ViewState['table'] := DataTable1;
    LoadTableButton.Text := 'Loaded';
    Exclude(Self.LoadTableButton.Click, Self.LoadTableButton_Click);
  finally
    SqlConnection1.Close;
  end;
end;


end.

