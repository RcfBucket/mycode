unit Default;

interface

uses
  System.Collections, System.ComponentModel,
  System.Data, System.Drawing, System.Web, System.Web.SessionState,
  System.Web.UI, System.Web.UI.WebControls, System.Web.UI.HtmlControls,
  System.Web.Security, System.Web.UI.WebControls.WebParts, System.Configuration,
  Borland.Data.AdoDbxClientProvider, System.Data.Common;

type
  TDefault = class(System.Web.UI.Page)
  {$REGION 'Designer Managed Code'}
  strict private
    procedure InitializeComponent;
  {$ENDREGION}
  strict private
    procedure Page_Load(sender: System.Object; e: System.EventArgs);
 strict protected
  GridView1: System.Web.UI.WebControls.GridView;
  protected
    procedure OnInit(e: EventArgs); override;
  private
    { Private Declarations }
  public
    { Public Declarations }
    ProviderFactory: TAdoDbxProviderFactory;
  end;

implementation

{$REGION 'Designer Managed Code'}
/// <summary>
/// Required method for Designer support -- do not modify
/// the contents of this method with the code editor.
/// </summary>
procedure TDefault.InitializeComponent;
begin
 Include(Self.Load, Self.Page_Load);
end;
{$ENDREGION}

//This code uses the Borland.Data.AdoDbxClient DbProviderFactory
//In order for this code to run, you have to configure a DBX4
//connection using the name TempNorthwind (this example defined
//the connection using the MSSQL driver.
//The easiest way to configure a new DBX4 connection is to
//use the Data Explorer in RAD Studio.

procedure TDefault.Page_Load(sender: System.Object; e: System.EventArgs);
var
   DataTable1: DataTable;
   Factory: System.Data.Common.DbProviderFactory;
   Connection: System.Data.Common.DbConnection;
   Command: DbCommand;
   DataAdapter: DbDataAdapter;
begin
  if not isPostBack  then
  begin
      Factory := System.Data.Common.DbProviderFactories.GetFactory('Borland.Data.AdoDbxClient');
      Connection := Factory.CreateConnection();
      Connection.ConnectionString := 'ConnectionName=TempNorthwind';
      Connection.Open;
      Command := Connection.CreateCommand;
      Command.CommandText := 'SELECT * FROM CUSTOMERS';
      DataAdapter :=Factory.CreateDataAdapter;
      DataAdapter.SelectCommand := Command;
      DataTable1 := DataTable.Create;
      DataAdapter.Fill(DataTable1);
      GridView1.DataSource := DataTable1;
      DataBind;
  end;
end;

procedure TDefault.OnInit(e: EventArgs);
begin
  //
  // Required for Designer support
  //
  InitializeComponent;
  inherited OnInit(e);
end;

end.

